﻿CREATE TABLE [inter].[PricingStagingStreamRegion] (
    [FactorSetId]         VARCHAR (12)       NOT NULL,
    [RegionId]            VARCHAR (5)        NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [CurrencyRpt]         VARCHAR (4)        NOT NULL,
    [StreamId]            VARCHAR (42)       NOT NULL,
    [Raw_Amount_Cur]      REAL               NOT NULL,
    [Adj_MatlBal_Cur]     REAL               NULL,
    [_MatlBal_Amount_Cur] AS                 (CONVERT([real],[Raw_Amount_Cur]+coalesce([Adj_MatlBal_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_PricingStagingStreamRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_PricingStagingStreamRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_PricingStagingStreamRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_PricingStagingStreamRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingStagingStreamRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingStagingStreamRegion_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingStagingStreamRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingStagingStreamRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingStagingStreamRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingStagingStreamRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingStagingStreamRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingStagingStreamRegion_u
	ON inter.PricingStagingStreamRegion
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingStagingStreamRegion
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingStagingStreamRegion.FactorSetId	= INSERTED.FactorSetId
		AND inter.PricingStagingStreamRegion.RegionId		= INSERTED.RegionId
		AND inter.PricingStagingStreamRegion.CalDateKey		= INSERTED.CalDateKey
		AND inter.PricingStagingStreamRegion.StreamId		= INSERTED.StreamId
		AND inter.PricingStagingStreamRegion.CurrencyRpt	= INSERTED.CurrencyRpt;

END;