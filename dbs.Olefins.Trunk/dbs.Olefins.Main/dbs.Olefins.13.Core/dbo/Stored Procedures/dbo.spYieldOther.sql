﻿



CREATE              PROC [dbo].[spYieldOther](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON
DECLARE @Currency as varchar(5)
SELECT @Currency = 'USD'

DELETE FROM dbo.YieldOther WHERE Refnum = @Refnum
Insert into dbo.YieldOther (Refnum
, DataType
, FeedLtOther
, FeedLiqOther1, FeedLiqOther2, FeedLiqOther3
, ProdOther1, ProdOther2
)

SELECT Refnum = @Refnum
, DataType					= 'Name'
, FeedLtOther		= MAX(CASE WHEN q.StreamId = 'FeedLtOther'  THEN RTRIM(q.StreamDescription) END) 
, FeedLiqOther1		= MAX(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 1)%' THEN RTRIM(SUBSTRING(q.StreamDescription, 1, CHARINDEX('(Other', q.StreamDescription)-1)) END) 
, FeedLiqOther2		= MAX(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 2)%' THEN RTRIM(SUBSTRING(q.StreamDescription, 1, CHARINDEX('(Other', q.StreamDescription)-1)) END) 
, FeedLiqOther3		= MAX(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 3)%' THEN RTRIM(SUBSTRING(q.StreamDescription, 1, CHARINDEX('(Other', q.StreamDescription)-1)) END) 
, ProdOther1		= MAX(CASE WHEN q.StreamId = 'ProdOther'    AND q.StreamDescription like '%(Other Prod 1)%' THEN RTRIM(SUBSTRING(q.StreamDescription, 1, CHARINDEX('(Other', q.StreamDescription)-1)) END) 
, ProdOther2		= MAX(CASE WHEN q.StreamId = 'ProdOther'    AND q.StreamDescription like '%(Other Prod 2)%' THEN RTRIM(SUBSTRING(q.StreamDescription, 1, CHARINDEX('(Other', q.StreamDescription)-1)) END) 
FROM fact.Quantity q WHERE q.Refnum=@Refnum
GROUP BY Refnum

SET NOCOUNT OFF


















