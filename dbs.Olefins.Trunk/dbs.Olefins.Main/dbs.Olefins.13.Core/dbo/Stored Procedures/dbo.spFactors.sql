﻿


CREATE             PROC [dbo].[spFactors](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DELETE FROM dbo.Factors WHERE Refnum = @Refnum

Insert into dbo.Factors (Refnum
, DataType
, FreshPyroFeed
, SuppTot
, PlantFeed
, FreshPyroFeedPur
, TowerPyroGasHT
, HydroPur
, Auxiliary
, RedPropyleneCG
, RedPropyleneRG
, RedPropylene
, RedEthyleneCG
, RedCrackedGasTrans
, Reduction
, ProcessUnits
, BoilFiredSteam
, ElecGen
, Utilities
, Total
, Ethane
, LPG
, Liquid
, SuppEthane
, SuppLPG
, SuppLiquid
, TowerNAPS
, TowerPyroGasB
, TowerPyroGasSS
, TowerDeethanizer
, TowerDepropanizer
, HydroPurPSA
, HydroPurMembrane
, HydroPurCryogenic
, ElecGenDist
)

SELECT Refnum = @Refnum
, DataType					= 'kEdc'
, FreshPyroFeed = SUM(CASE WHEN e.EdcId = 'FreshPyroFeed' THEN kEdc END)
, SuppTot = SUM(CASE WHEN e.EdcId = 'SuppTot' THEN kEdc END)
, PlantFeed = SUM(CASE WHEN e.EdcId = 'PlantFeed' THEN kEdc END)
, FreshPyroFeedPur = SUM(CASE WHEN e.EdcId = 'FreshPyroFeedPur' THEN kEdc END)
, TowerPyroGasHT = SUM(CASE WHEN e.EdcId = 'TowerPyroGasHT' THEN kEdc END)
, HydroPur = SUM(CASE WHEN e.EdcId = 'HydroPur' THEN kEdc END)
, Auxiliary = SUM(CASE WHEN e.EdcId = 'Auxiliary' THEN kEdc END)
, RedPropyleneCG = SUM(CASE WHEN e.EdcId = 'RedPropyleneCG' THEN kEdc END)
, RedPropyleneRG = SUM(CASE WHEN e.EdcId = 'RedPropyleneRG' THEN kEdc END)
, RedPropylene = SUM(CASE WHEN e.EdcId = 'RedPropylene' THEN kEdc END)
, RedEthyleneCG = SUM(CASE WHEN e.EdcId = '---' THEN kEdc END)
, RedCrackedGasTrans = SUM(CASE WHEN e.EdcId = 'RedCrackedGasTrans' THEN kEdc END)
, Reduction = SUM(CASE WHEN e.EdcId = 'Reduction' THEN kEdc END)
, ProcessUnits = SUM(CASE WHEN e.EdcId = 'Facilities' THEN kEdc END)
, BoilFiredSteam = SUM(CASE WHEN e.EdcId = 'BoilFiredSteam' THEN kEdc END)
, ElecGen = SUM(CASE WHEN e.EdcId = 'ElecGen' THEN kEdc END)
, Utilities = SUM(CASE WHEN e.EdcId = 'Utilities' THEN kEdc END)
, Total = SUM(CASE WHEN e.EdcId = 'kEDCTot' THEN kEdc END)
, Ethane = SUM(CASE WHEN e.EdcId = 'Ethane' THEN kEdc END)
, LPG = SUM(CASE WHEN e.EdcId = 'LPG' THEN kEdc END)
, Liquid = SUM(CASE WHEN e.EdcId = 'Liquid' THEN kEdc END)
, SuppEthane = SUM(CASE WHEN e.EdcId = 'SuppEthane' THEN kEdc END)
, SuppLPG = SUM(CASE WHEN e.EdcId = 'SuppLPG' THEN kEdc END)
, SuppLiquid = SUM(CASE WHEN e.EdcId = 'SuppLiquid' THEN kEdc END)
, TowerNAPS = SUM(CASE WHEN e.EdcId = 'TowerNAPS' THEN kEdc END)
, TowerPyroGasB = SUM(CASE WHEN e.EdcId = 'TowerPyroGasB' THEN kEdc END)
, TowerPyroGasSS = SUM(CASE WHEN e.EdcId = 'TowerPyroGasSS' THEN kEdc END)
, TowerDeethanizer = SUM(CASE WHEN e.EdcId = 'TowerDeethanizer' THEN kEdc END)
, TowerDepropanizer = SUM(CASE WHEN e.EdcId = 'TowerDepropanizer' THEN kEdc END)
, HydroPurPSA = SUM(CASE WHEN e.EdcId = 'HydroPurPSA' THEN kEdc END)
, HydroPurMembrane = SUM(CASE WHEN e.EdcId = 'HydroPurMembrane' THEN kEdc END)
, HydroPurCryogenic = SUM(CASE WHEN e.EdcId = 'HydroPurCryogenic' THEN kEdc END)
, ElecGenDist = SUM(CASE WHEN e.EdcId = 'ElecGenDist' THEN kEdc END)
FROM calc.EdcAggregate e 
WHERE e.FactorSetId = @FactorSetId and e.Refnum = @Refnum

Insert into dbo.Factors (Refnum
, DataType
, FreshPyroFeed
, SuppTot
, PlantFeed
, FreshPyroFeedPur
, TowerPyroGasHT
, HydroPur
, Auxiliary
, RedPropyleneCG
, RedPropyleneRG
, RedPropylene
, RedEthyleneCG
, RedCrackedGasTrans
, Reduction
, ProcessUnits
, BoilFiredSteam
, ElecGen
, Utilities
, Total
, Ethane
, LPG
, Liquid
, SuppEthane
, SuppLPG
, SuppLiquid
, TowerNAPS
, TowerPyroGasB
, TowerPyroGasSS
, TowerDeethanizer
, TowerDepropanizer
, HydroPurPSA
, HydroPurMembrane
, HydroPurCryogenic
, ElecGenDist
)

SELECT Refnum = @Refnum
, DataType					= 'kUEdc'
, FreshPyroFeed = SUM(CASE WHEN e.EdcId = 'FreshPyroFeed' THEN kUEdc END)
, SuppTot = SUM(CASE WHEN e.EdcId = 'SuppTot' THEN kUEdc END)
, PlantFeed = SUM(CASE WHEN e.EdcId = 'PlantFeed' THEN kUEdc END)
, FreshPyroFeedPur = SUM(CASE WHEN e.EdcId = 'FreshPyroFeedPur' THEN kUEdc END)
, TowerPyroGasHT = SUM(CASE WHEN e.EdcId = 'TowerPyroGasHT' THEN kUEdc END)
, HydroPur = SUM(CASE WHEN e.EdcId = 'HydroPur' THEN kUEdc END)
, Auxiliary = SUM(CASE WHEN e.EdcId = 'Auxiliary' THEN kUEdc END)
, RedPropyleneCG = SUM(CASE WHEN e.EdcId = 'RedPropyleneCG' THEN kUEdc END)
, RedPropyleneRG = SUM(CASE WHEN e.EdcId = 'RedPropyleneRG' THEN kUEdc END)
, RedPropylene = SUM(CASE WHEN e.EdcId = 'RedPropylene' THEN kUEdc END)
, RedEthyleneCG = SUM(CASE WHEN e.EdcId = '---' THEN kUEdc END)
, RedCrackedGasTrans = SUM(CASE WHEN e.EdcId = 'RedCrackedGasTrans' THEN kUEdc END)
, Reduction = SUM(CASE WHEN e.EdcId = 'Reduction' THEN kUEdc END)
, ProcessUnits = SUM(CASE WHEN e.EdcId = 'Facilities' THEN kUEdc END)
, BoilFiredSteam = SUM(CASE WHEN e.EdcId = 'BoilFiredSteam' THEN kUEdc END)
, ElecGen = SUM(CASE WHEN e.EdcId = 'ElecGen' THEN kUEdc END)
, Utilities = SUM(CASE WHEN e.EdcId = 'Utilities' THEN kUEdc END)
, Total = SUM(CASE WHEN e.EdcId = 'kEDCTot' THEN kUEdc END)
, Ethane = SUM(CASE WHEN e.EdcId = 'Ethane' THEN kUEdc END)
, LPG = SUM(CASE WHEN e.EdcId = 'LPG' THEN kUEdc END)
, Liquid = SUM(CASE WHEN e.EdcId = 'Liquid' THEN kUEdc END)
, SuppEthane = SUM(CASE WHEN e.EdcId = 'SuppEthane' THEN kUEdc END)
, SuppLPG = SUM(CASE WHEN e.EdcId = 'SuppLPG' THEN kUEdc END)
, SuppLiquid = SUM(CASE WHEN e.EdcId = 'SuppLiquid' THEN kUEdc END)
, TowerNAPS = SUM(CASE WHEN e.EdcId = 'TowerNAPS' THEN kUEdc END)
, TowerPyroGasB = SUM(CASE WHEN e.EdcId = 'TowerPyroGasB' THEN kUEdc END)
, TowerPyroGasSS = SUM(CASE WHEN e.EdcId = 'TowerPyroGasSS' THEN kUEdc END)
, TowerDeethanizer = SUM(CASE WHEN e.EdcId = 'TowerDeethanizer' THEN kUEdc END)
, TowerDepropanizer = SUM(CASE WHEN e.EdcId = 'TowerDepropanizer' THEN kUEdc END)
, HydroPurPSA = SUM(CASE WHEN e.EdcId = 'HydroPurPSA' THEN kUEdc END)
, HydroPurMembrane = SUM(CASE WHEN e.EdcId = 'HydroPurMembrane' THEN kUEdc END)
, HydroPurCryogenic = SUM(CASE WHEN e.EdcId = 'HydroPurCryogenic' THEN kUEdc END)
, ElecGenDist = SUM(CASE WHEN e.EdcId = 'ElecGenDist' THEN kUEdc END)
FROM calc.EdcAggregate e 
WHERE e.FactorSetId = @FactorSetId and e.Refnum = @Refnum

Insert into dbo.Factors (Refnum
, DataType
, FreshPyroFeed
, SuppTot
, PlantFeed
, FreshPyroFeedPur
, TowerPyroGasHT
, HydroPur
, Auxiliary
, RedPropyleneCG
, RedPropyleneRG
, RedPropylene
, RedEthyleneCG
, RedCrackedGasTrans
, Reduction
, ProcessUnits
, BoilFiredSteam
, ElecGen
, Utilities
, Total
)

SELECT Refnum = @Refnum
, DataType = 'EII'
, FreshPyroFeed = SUM(CASE WHEN e.StandardEnergyId = 'FreshPyroFeed' THEN StandardEnergy END) / t.NumDays
, SuppTot = SUM(CASE WHEN e.StandardEnergyId = 'SuppTot' THEN StandardEnergy END) / t.NumDays
, PlantFeed = SUM(CASE WHEN e.StandardEnergyId = 'PlantFeed' THEN StandardEnergy END) / t.NumDays
, FreshPyroFeedPur = SUM(CASE WHEN e.StandardEnergyId = 'FracFeed' THEN StandardEnergy END) / t.NumDays
, TowerPyroGasHT = SUM(CASE WHEN e.StandardEnergyId = 'TowerPyroGasHT' THEN StandardEnergy END) / t.NumDays
, HydroPur = SUM(CASE WHEN e.StandardEnergyId = 'HydroPur' THEN StandardEnergy END) / t.NumDays
, Auxiliary = SUM(CASE WHEN e.StandardEnergyId = 'Vessel' THEN StandardEnergy END) / t.NumDays
, RedPropyleneCG = SUM(CASE WHEN e.StandardEnergyId = 'RedPropyleneCG' THEN StandardEnergy END) / t.NumDays
, RedPropyleneRG = SUM(CASE WHEN e.StandardEnergyId = 'RedPropyleneRG' THEN StandardEnergy END) / t.NumDays
, RedPropylene = SUM(CASE WHEN e.StandardEnergyId = 'RedPropylene' THEN StandardEnergy END) / t.NumDays
, RedEthyleneCG = SUM(CASE WHEN e.StandardEnergyId = 'RedEthyleneCG' THEN StandardEnergy END) / t.NumDays
, RedCrackedGasTrans = SUM(CASE WHEN e.StandardEnergyId = 'RedCrackedGasTrans' THEN StandardEnergy END) / t.NumDays
, Reduction = SUM(CASE WHEN e.StandardEnergyId = 'Reduction' THEN StandardEnergy END) / t.NumDays
, ProcessUnits = SUM(CASE WHEN e.StandardEnergyId = 'StandardEnergyTot' THEN StandardEnergy END) / t.NumDays
, BoilFiredSteam = SUM(CASE WHEN e.StandardEnergyId = '' THEN StandardEnergy END) / t.NumDays
, ElecGen = SUM(CASE WHEN e.StandardEnergyId = '' THEN StandardEnergy END) / t.NumDays
, Utilities = SUM(CASE WHEN e.StandardEnergyId = '' THEN StandardEnergy END) / t.NumDays
, Total = SUM(CASE WHEN e.StandardEnergyId = 'StandardEnergyTot' THEN StandardEnergy END) / t.NumDays
FROM calc.StandardEnergyAggregate e JOIN dbo.TSort t on t.Refnum=e.Refnum
WHERE e.FactorSetId = @FactorSetId and e.Refnum = @Refnum
GROUP BY t.NumDays

Insert into dbo.Factors (Refnum
, DataType
, FreshPyroFeed
, SuppTot
, PlantFeed
, FreshPyroFeedPur
, TowerPyroGasHT
, HydroPur
, Auxiliary
, RedPropyleneCG
, RedPropyleneRG
, RedPropylene
, RedEthyleneCG
, RedCrackedGasTrans
, Reduction
, ProcessUnits
, BoilFiredSteam
, ElecGen
, Utilities
, Total
, Ethane
, LPG
, Liquid
, SuppEthane
, SuppLPG
, SuppLiquid
, TowerNAPS
, TowerPyroGasB
, TowerPyroGasSS
, TowerDeethanizer
, TowerDepropanizer
, HydroPurPSA
, HydroPurMembrane
, HydroPurCryogenic
, ElecGenDist
)

SELECT Refnum = @Refnum
, DataType					= CASE e.EfficiencyId
								WHEN 'MES'			THEN 'MEI'
								WHEN 'NonEnergy'	THEN 'NEI'
								WHEN 'PersMaint'	THEN 'mPEI'
								WHEN 'PersNonMaint'	THEN 'nmPEI'
								WHEN 'PersTot'		THEn 'PEI'
							END
, FreshPyroFeed = SUM(CASE WHEN e.UnitId = 'FreshPyroFeed' THEN EfficiencyStandard END)
, SuppTot = SUM(CASE WHEN e.UnitId = 'SuppTot' THEN EfficiencyStandard END)
, PlantFeed = SUM(CASE WHEN e.UnitId = 'PlantFeed' THEN EfficiencyStandard END)
, FreshPyroFeedPur = SUM(CASE WHEN e.UnitId = 'FreshPyroFeedPur' THEN EfficiencyStandard END)
, TowerPyroGasHT = SUM(CASE WHEN e.UnitId = 'TowerPyroGasHT' THEN EfficiencyStandard END)
, HydroPur = SUM(CASE WHEN e.UnitId = 'HydroPur' THEN EfficiencyStandard END)
, Auxiliary = SUM(CASE WHEN e.UnitId = 'Auxiliary' THEN EfficiencyStandard END)
, RedPropyleneCG = SUM(CASE WHEN e.UnitId = 'RedPropyleneCG' THEN EfficiencyStandard END)
, RedPropyleneRG = SUM(CASE WHEN e.UnitId = 'RedPropyleneRG' THEN EfficiencyStandard END)
, RedPropylene = SUM(CASE WHEN e.UnitId = 'RedPropylene' THEN EfficiencyStandard END)
, RedEthyleneCG = SUM(CASE WHEN e.UnitId = '' THEN EfficiencyStandard END)
, RedCrackedGasTrans = SUM(CASE WHEN e.UnitId = 'RedCrackedGasTrans' THEN EfficiencyStandard END)
, Reduction = SUM(CASE WHEN e.UnitId = 'Reduction' THEN EfficiencyStandard END)
, ProcessUnits = SUM(CASE WHEN e.UnitId = 'ProcessUnits' THEN EfficiencyStandard END)
, BoilFiredSteam = SUM(CASE WHEN e.UnitId = 'BoilFiredSteam' THEN EfficiencyStandard END)
, ElecGen = SUM(CASE WHEN e.UnitId = 'ElecGen' THEN EfficiencyStandard END)
, Utilities = SUM(CASE WHEN e.UnitId = 'Utilities' THEN EfficiencyStandard END)
, Total = SUM(CASE WHEN e.UnitId = 'UnitTot' THEN EfficiencyStandard END)
, Ethane = SUM(CASE WHEN e.UnitId = 'Ethane' THEN EfficiencyStandard END)
, LPG = SUM(CASE WHEN e.UnitId = 'LPG' THEN EfficiencyStandard END)
, Liquid = SUM(CASE WHEN e.UnitId = 'Liquid' THEN EfficiencyStandard END)
, SuppEthane = SUM(CASE WHEN e.UnitId = 'SuppEthane' THEN EfficiencyStandard END)
, SuppLPG = SUM(CASE WHEN e.UnitId = 'SuppLPG' THEN EfficiencyStandard END)
, SuppLiquid = SUM(CASE WHEN e.UnitId = 'SuppLiquid' THEN EfficiencyStandard END)
, TowerNAPS = SUM(CASE WHEN e.UnitId = 'TowerNAPS' THEN EfficiencyStandard END)
, TowerPyroGasB = SUM(CASE WHEN e.UnitId = 'TowerPyroGasB' THEN EfficiencyStandard END)
, TowerPyroGasSS = SUM(CASE WHEN e.UnitId = 'TowerPyroGasSS' THEN EfficiencyStandard END)
, TowerDeethanizer = SUM(CASE WHEN e.UnitId = 'TowerDeethanizer' THEN EfficiencyStandard END)
, TowerDepropanizer = SUM(CASE WHEN e.UnitId = 'TowerDepropanizer' THEN EfficiencyStandard END)
, HydroPurPSA = SUM(CASE WHEN e.UnitId = 'HydroPurPSA' THEN EfficiencyStandard END)
, HydroPurMembrane = SUM(CASE WHEN e.UnitId = 'HydroPurMembrane' THEN EfficiencyStandard END)
, HydroPurCryogenic = SUM(CASE WHEN e.UnitId = 'HydroPurCryogenic' THEN EfficiencyStandard END)
, ElecGenDist = SUM(CASE WHEN e.UnitId = '' THEN EfficiencyStandard END)
FROM calc.EfficiencyAggregate e 
WHERE e.FactorSetId = @FactorSetId and e.Refnum = @Refnum
GROUP BY e.Refnum, e.EfficiencyId


SET NOCOUNT OFF

















