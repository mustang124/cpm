﻿






CREATE              PROC [dbo].[spEnergy](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON


DECLARE @Divisors TABLE 
(
	DataType varchar(20) NOT NULL,
	DivisorValue real NOT NULL,
	PlantCnt real NULL
)
INSERT @Divisors
SELECT	DataType = CASE d.DataType
			WHEN 'HVC_MT'		THEN	'kBtuPerHVCMt'
			WHEN 'HVC_LB'		THEN	'BtuPerHVCLb'
			WHEN 'ETHDiv_MT'	THEN	'kBtuPerETHMt'
			WHEN 'OLEDiv_MT'	THEN	'kBtuPerOLEMt'
			WHEN 'ADJ'			THEN	'MBtu'
			ELSE d.DataType
			END, 
		DivisorValue = SUM(CASE DataType	
			WHEN 'UEDC' THEN 1000.0 
			WHEN 'EDC' then 1000.0 
			WHEN 'EII' THEN 365.0/100.0 
			ELSE 1.0 
			END * d.DivisorValue),
		PlantCnt = 1
      FROM dbo.DivisorsUnPivot d 
      WHERE d.DataType in ('UEDC','EDC','HVC_MT','HVC_LB','ETHDiv_MT','ADJ','OLEDiv_MT','EII')
      AND d.Refnum=@Refnum
      GROUP BY DataType
      
      --select * from @Divisors
      
DECLARE @EIIStdEnergyPerUEDC as real, @EII as real, @EII_StdEnergy as real
Select @EIIStdEnergyPerUEDC = CASE WHEN d.kUEdc > 0 THEN StandardEnergy/d.kUEdc/1000.0 END,
@EII = Eii,
@EII_StdEnergy = StandardEnergy
from calc.Eii e JOIN dbo.Divisors d on d.Refnum=e.Refnum
where SimModelId = 'Plant' and e.FactorSetId = @FactorSetId and e.Refnum=@Refnum

Declare @EEI_SPSL real, @EEI_PYPS real, @EEI_SPSL_StdEnergy real, @EEI_PYPS_StdEnergy real, @EEI_SPSL_StdProd_kMT real, @EEI_PYPS_StdProd_kMT real
Select 
  @EEI_SPSL = SUM(CASE WHEN e.SimModelId = 'SPSL' then e._Eei ELSE 0 END)
, @EEI_SPSL_StdEnergy  = SUM(CASE WHEN e.SimModelId = 'SPSL' then e._Model_Energy_MBtu_kMT * [$(DbGlobal)].dbo.UnitsConv(e._TotalModel_kMT, 'KG', 'LB') ELSE 0 END)  -- I think the Model Energy is in actually kLBs
, @EEI_SPSL_StdProd_kMT = SUM(CASE WHEN e.SimModelId = 'SPSL' then e._TotalModel_kMT ELSE 0 END)  
, @EEI_PYPS = SUM(CASE WHEN e.SimModelId = 'PYPS' then e._Eei ELSE 0 END)
, @EEI_PYPS_StdEnergy  = SUM(CASE WHEN e.SimModelId = 'PYPS' then e._Model_Energy_MBtu_kMT * [$(DbGlobal)].dbo.UnitsConv(e._TotalModel_kMT, 'KG', 'LB') ELSE 0 END) -- I think the Model Energy is in Actuallay kLBs
, @EEI_PYPS_StdProd_kMT = SUM(CASE WHEN e.SimModelId = 'PYPS' then e._TotalModel_kMT ELSE 0 END)  
from calc.Eei e JOIN dbo.Divisors d on d.Refnum=e.Refnum
where e._IsEei_Bit = 1
and e.Refnum=@Refnum AND e.FactorSetId = @FactorSetId

DECLARE @SeparationEnergy_Supp_MBtu REAL;

SELECT @SeparationEnergy_Supp_MBtu = es.[EnergySeparation_MBtu]
FROM [calc].[EnergySeparationAggregate] es WITH (NOEXPAND)
WHERE	es.[StandardEnergyId]	= 'SuppTot'
	AND	es.[FactorSetId]		= @FactorSetId
	AND	es.[Refnum]				= @Refnum;

DECLARE @PurTot_CostPer real, @PPFC_CostPer real, @TotConsumption_CostPer real, @ExportTot_CostPer real, @NetEnergyCons_CostPer real
Select 
@PurTot_CostPer				= SUM(CASE WHEN ea.AccountId = 'PurchasedEnergy' THEN ec.UnitEnergyCost_CurMBtu END), 
@PPFC_CostPer				= SUM(CASE WHEN ea.AccountId = 'PPFC' THEN ec.UnitEnergyCost_CurMBtu END),
@TotConsumption_CostPer		= SUM(CASE WHEN ea.AccountId = 'EnergyConsumption' THEN ec.UnitEnergyCost_CurMBtu END),
@ExportTot_CostPer			= SUM(CASE WHEN ea.AccountId = 'ExportCredits' THEN ec.UnitEnergyCost_CurMBtu END),
@NetEnergyCons_CostPer		= SUM(CASE WHEN ea.AccountId = 'EnergyBalance' THEN ec.UnitEnergyCost_CurMBtu END)
FROM calc.EnergyCost ec JOIN fact.EnergyLHValueAggregate ea on ea.Refnum=ec.Refnum and ea.CalDateKey = ec.CalDateKey and ea.AccountId=ec.AccountId
where ec.FactorSetId = ea.FactorSetId AND ec.Refnum = @Refnum and ec.FactorSetId = @FactorSetId

DECLARE @PyroFurnInletTemp_C real, @PyroFurnInletTemp_F real, @PyroFurnPreheat_Pcnt real, @PyroFurnFlueGasTemp_C real, @PyroFurnFlueGasTemp_F real, @PyroFurnFlueGasO2_Pcnt real, @PyroFurnPrimaryTLE_Pcnt real, @PyroFurnSecondaryTLE_Pcnt real, @PyroFurnTLESteam_PSIa real, @TLEOutletTemp_C real, @TLEOutletTemp_F real, @CoolingWater_MTPerHVCMT real, @CoolingWaterDelta_C real, @CoolingWaterDelta_F real, @CoolingWaterEnergy_BtuLbHVC real, @CoolingWaterEnergy_GJMTHVC real, @FinFanEnergy_BtuLb real, @FinFanEnergy_GJMT real, @CompGasEfficiency_Pcnt real, @EffCalcCompany_Pcnt real, @EffCalcAPI_Pcnt real, @SteamExtraction_MtMtHVC real, @SteamCondensing_MtMtHVC real, @ProcessedHeatExchanged_BTULbHVC real, @ProcessedHeatExchanged_GJMtHVC real, @QuenchHeatMatl_Pcnt real, @QuenchDilution_Pcnt real, @EnergyDeteriation_Pcnt real ,@IBSLSteamRequirement_Pcnt real
SELECT 
  @PyroFurnInletTemp_C			= gpee.PyroFurnInletTemp_C
, @PyroFurnInletTemp_F			= [$(DbGlobal)].dbo.UnitsConv(gpee.PyroFurnInletTemp_C, 'TC', 'TF')
, @PyroFurnPreheat_Pcnt			= gpee.PyroFurnPreheat_Pcnt
, @PyroFurnFlueGasTemp_C		= gpee.PyroFurnFlueGasTemp_C
, @PyroFurnFlueGasTemp_F		= [$(DbGlobal)].dbo.UnitsConv(gpee.PyroFurnFlueGasTemp_C, 'TC', 'TF')
, @PyroFurnFlueGasO2_Pcnt		= gpee.PyroFurnFlueGasO2_Pcnt
, @PyroFurnPrimaryTLE_Pcnt		= gpee.PyroFurnPrimaryTLE_Pcnt
, @PyroFurnSecondaryTLE_Pcnt	= gpee.PyroFurnSecondaryTLE_Pcnt
, @PyroFurnTLESteam_PSIa		= gpee.PyroFurnTLESteam_PSIa
, @TLEOutletTemp_C				= gpee.TLEOutletTemp_C
, @TLEOutletTemp_F				= [$(DbGlobal)].dbo.UnitsConv(gpee.TLEOutletTemp_C,'TC','TF')
, @CoolingWater_MTPerHVCMT		= gpe.CoolingWater_MTd * dv.NumDays / dv.HvcProd_kMT / 1000.0
, @CoolingWaterDelta_C			= gpe.CoolingWaterReturnTemp_C - gpe.CoolingWaterSupplyTemp_C
, @CoolingWaterDelta_F			= [$(DbGlobal)].dbo.UnitsConv(gpe.CoolingWaterReturnTemp_C - gpe.CoolingWaterSupplyTemp_C,'DTC','DTF')  -- ??
, @CoolingWaterEnergy_BtuLbHVC	= en.CoolingWaterEnergy_BtuLb
, @CoolingWaterEnergy_GJMTHVC	= [$(DbGlobal)].dbo.UnitsConv(en.CoolingWaterEnergy_BtuLb, 'BTU/LB', 'GJ/MT')
, @FinFanEnergy_BtuLb			= en.FinFanEnergy_BtuLb
, @FinFanEnergy_GJMT			= [$(DbGlobal)].dbo.UnitsConv(en.FinFanEnergy_BtuLb, 'BTU/LB', 'GJ/MT')
, @CompGasEfficiency_Pcnt		= gpe.CompGasEfficiency_Pcnt * 100.0
, @EffCalcCompany_Pcnt			= CASE WHEN gpe.CalcTypeID = 'Company' THEN 100.0 ELSE 0 END
, @EffCalcAPI_Pcnt				= CASE WHEN gpe.CalcTypeID = 'API' THEN 100.0 ELSE 0 END
, @SteamExtraction_MtMtHVC		= en.SteamExtraction_MtMtHVC
, @SteamCondensing_MtMtHVC		= en.SteamCondensing_MtMtHVC
, @ProcessedHeatExchanged_BTULbHVC = ProcessedHeatExchanged_MtMtHVC  --?
, @ProcessedHeatExchanged_GJMtHVC = [$(DbGlobal)].dbo.UnitsConv(ProcessedHeatExchanged_MtMtHVC, 'BTU/LB','GJ/MT')
, @QuenchHeatMatl_Pcnt			= QuenchHeatMatl_Pcnt
, @QuenchDilution_Pcnt			= QuenchDilution_Pcnt
, @EnergyDeteriation_Pcnt		= gpee.EnergyDeterioration_Pcnt
, @IBSLSteamRequirement_Pcnt	= gpee.IBSLSteamRequirement_Pcnt
FROM fact.GenPlantEnergy gpe
INNER JOIN fact.GenPlantEnergyEfficiency gpee on gpe.Refnum=gpee.Refnum
INNER JOIN calc.EnergyNormal en on gpe.Refnum=en.Refnum
INNER JOIN	dbo.Divisors dv ON	dv.Refnum=gpe.Refnum
WHERE gpe.Refnum = @Refnum

DELETE FROM dbo.Energy WHERE Refnum = @Refnum

INSERT INTO dbo.Energy (Refnum, Currency, Scenario, DataType
, EII, EII_StdEnergy, EEI_SPSL, EEI_SPSL_StdEnergy, EEI_SPSL_StdProd_kMT, EEI_PYPS, EEI_PYPS_StdEnergy, EEI_PYPS_StdProd_kMT
,PurFuelNatural,PurSteamSHP, PurSteamHP,PurSteamIP,PurSteamLP,PurElec
,PurEthane,PurPropane,PurButane,PurNaphtha,PurDistallate,PurResidual,PurCoalCoke,PurOther
,PurchasedEnergy
,PPFCFuelGas,PPFCEthane,PPFCPropane
,PPFCButane,PPFCPyroFuelOil,PPFCPyroGasOil,PPFCPyroNaphtha,PPFCOther,PPFC
,TotConsumption
,ExportSteamSHP,ExportSteamHP,ExportSteamIP,ExportSteamLP,ExportElectric,ExportCredits
,NetEnergyCons
,[SeparationEnergy_Supp_MBtu]
,EIIStdPerUEDC
,PurFuelNatural_CostPer,PurSteamSHP_CostPer,PurSteamHP_CostPer,PurSteamIP_CostPer,PurSteamLP_CostPer,PurElec_CostPerkWh,PurOther_CostPer, PurTot_CostPer
,PPFCFuelGas_CostPer,PPFCOther_CostPer,PPFC_CostPer
,TotConsumption_CostPer
,ExportSteamSHP_CostPer,ExportSteamHP_CostPer,ExportSteamIP_CostPer,ExportSteamLP_CostPer,ExportElectric_CostPerkWh,ExportTot_CostPer
,NetEnergyCons_CostPer
,PyroFurnInletTemp_C
,PyroFurnInletTemp_F
,PyroFurnPreheat_Pcnt
,PyroFurnFlueGasTemp_C
,PyroFurnFlueGasTemp_F
,PyroFurnFlueGasO2_Pcnt
,PyroFurnPrimaryTLE_Pcnt
,PyroFurnSecondaryTLE_Pcnt
,PyroFurnTLESteam_PSIa
,TLEOutletTemp_C
,TLEOutletTemp_F
,CoolingWater_MTPerHVCMT
,CoolingWaterDelta_C
,CoolingWaterDelta_F
,CoolingWaterEnergy_BtuLbHVC
,CoolingWaterEnergy_GJMTHVC
,FinFanEnergy_BtuLb
,FinFanEnergy_GJMT
,CompGasEfficiency_Pcnt
,EffCalcCompany_Pcnt
,EffCalcAPI_Pcnt
,SteamExtraction_MtMtHVC
,SteamCondensing_MtMtHVC
,ProcessedHeatExchanged_BTULbHVC
,ProcessedHeatExchanged_GJMtHVC
,QuenchHeatMatl_Pcnt
,QuenchDilution_Pcnt
,PurFuelNatural_cnt
,PurEthane_cnt
,PurPropane_cnt
,PurResidual_cnt
,PurOther_cnt
,PurSteamSHP_cnt
,PurSteamHP_cnt
,PurSteamIP_cnt
,PurSteamLP_cnt
,PurElec_cnt
,PPFCFuelGas_cnt
,PPFCOther_cnt
,ExportSteamSHP_cnt
,ExportSteamHP_cnt
,ExportSteamIP_cnt
,ExportSteamLP_cnt
,ExportElectric_cnt
,EnergyDeteriation_Pcnt
,IBSLSteamRequirement_Pcnt
, DivisorValue
, DivisorAvgValue
)

SELECT @Refnum, e.Currency, 'BASE', d.DataType 
, EII = @EII
, EII_StdEnergy = @EII_StdEnergy
, EEI_SPSL = @EEI_SPSL
, EEI_SPSL_StdEnergy = @EEI_SPSL_StdEnergy
, EEI_SPSL_StdProd_kMT = @EEI_SPSL_StdProd_kMT
, EEI_PYPS = @EEI_PYPS
, EEI_PYPS_StdEnergy = @EEI_PYPS_StdEnergy
, EEI_PYPS_StdProd_kMT = @EEI_PYPS_StdProd_kMT
, PurFuelNatural			= SUM(CASE WHEN e.AccountId = 'PurFuelNatural' THEN e.Amount_MBtu ELSE 0 END)
, PurSteamSHP				= SUM(CASE WHEN e.AccountId = 'PurSteamSHP' THEN e.Amount_MBtu ELSE 0 END)
, PurSteamHP				= SUM(CASE WHEN e.AccountId = 'PurSteamHP' THEN e.Amount_MBtu ELSE 0 END)
, PurSteamIP				= SUM(CASE WHEN e.AccountId = 'PurSteamIP' THEN e.Amount_MBtu ELSE 0 END)
, PurSteamLP				= SUM(CASE WHEN e.AccountId = 'PurSteamLP' THEN e.Amount_MBtu ELSE 0 END)
, PurElec					= SUM(CASE WHEN e.AccountId = 'PurElec' THEN e.Amount_MBtu ELSE 0 END)
, PurEthane				= SUM(CASE WHEN e.AccountId = 'PurEthane' THEN e.Amount_MBtu ELSE 0 END)
, PurPropane				= SUM(CASE WHEN e.AccountId = 'PurPropane' THEN e.Amount_MBtu ELSE 0 END)
, PurButane				= SUM(CASE WHEN e.AccountId = 'PurButane' THEN e.Amount_MBtu ELSE 0 END)
, PurNaphtha				= SUM(CASE WHEN e.AccountId = 'PurNaphtha' THEN e.Amount_MBtu ELSE 0 END)
, PurDistallate			= SUM(CASE WHEN e.AccountId = 'PurDistallate' THEN e.Amount_MBtu ELSE 0 END)
, PurResidual				= SUM(CASE WHEN e.AccountId = 'PurResidual' THEN e.Amount_MBtu ELSE 0 END)
, PurCoalCoke				= SUM(CASE WHEN e.AccountId = 'PurEPurCoalCokelec' THEN e.Amount_MBtu ELSE 0 END)
, PurOther					= SUM(CASE WHEN e.AccountId in ('PurEthane','PurPropane','PurButane','PurNaphtha','PurDistallate','PurResidual','PurCoalCoke')  THEN e.Amount_MBtu ELSE 0 END)
, PurchasedEnergy			= SUM(CASE WHEN e.AccountId = 'PurchasedEnergy'  THEN e.Amount_MBtu ELSE 0 END)

, PPFCFuelGas				= SUM(CASE WHEN e.AccountId = 'PPFCFuelGas' THEN e.Amount_MBtu ELSE 0 END)
, PPFCEthane				= SUM(CASE WHEN e.AccountId = 'PPFCEthane' THEN e.Amount_MBtu ELSE 0 END)
, PPFCPropane				= SUM(CASE WHEN e.AccountId = 'PPFCPropane' THEN e.Amount_MBtu ELSE 0 END)
, PPFCButane				= SUM(CASE WHEN e.AccountId = 'PPFCButane' THEN e.Amount_MBtu ELSE 0 END)
, PPFCPyroFuelOil			= SUM(CASE WHEN e.AccountId = 'PPFCPyroFuelOil' THEN e.Amount_MBtu ELSE 0 END)
, PPFCPyroGasOil			= SUM(CASE WHEN e.AccountId = 'PPFCPyroGasOil' THEN e.Amount_MBtu ELSE 0 END)
, PPFCPyroNaphtha			= SUM(CASE WHEN e.AccountId = 'PPFCPyroNaphtha' THEN e.Amount_MBtu ELSE 0 END)
, PPFCOther				= SUM(CASE WHEN e.AccountId in ('PPFCButane','PPFCOther','PPFCPyroFuelOil','PPFCPyroGasOil','PPFCPyroNaphtha')   THEN e.Amount_MBtu ELSE 0 END)
, PPFC						= SUM(CASE WHEN e.AccountId = 'PPFC' THEN e.Amount_MBtu ELSE 0 END)
, TotConsumption			= SUM(CASE WHEN e.AccountId = 'EnergyConsumption' THEN e.Amount_MBtu ELSE 0 END)

, ExportSteamSHP			= SUM(CASE WHEN e.AccountId = 'ExportSteamSHP' THEN e.Amount_MBtu ELSE 0 END)
, ExportSteamHP			= SUM(CASE WHEN e.AccountId = 'ExportSteamHP' THEN e.Amount_MBtu ELSE 0 END)
, ExportSteamIP			= SUM(CASE WHEN e.AccountId = 'ExportSteamIP' THEN e.Amount_MBtu ELSE 0 END) 
, ExportSteamLP			= SUM(CASE WHEN e.AccountId = 'ExportSteamLP' THEN e.Amount_MBtu ELSE 0 END)
, ExportElectric			= SUM(CASE WHEN e.AccountId = 'ExportElectric' THEN e.Amount_MBtu ELSE 0 END) 
, ExportCredits			= SUM(CASE WHEN e.AccountId = 'ExportCredits' THEN e.Amount_MBtu ELSE 0 END)

, NetEnergyCons			= SUM(CASE WHEN e.AccountId = 'EnergyBalance' THEN e.Amount_MBtu ELSE 0 END) 

, @SeparationEnergy_Supp_MBtu

, EIIStdPerUEDC					= @EIIStdEnergyPerUEDC
, PurFuelNatural_CostPer	= SUM(CASE WHEN e.AccountId = 'PurFuelNatural' THEN e.CostPerMBtu END)
, PurSteamSHP_CostPer		= SUM(CASE WHEN e.AccountId = 'PurSteamSHP' THEN e.CostPerMBtu END)
, PurSteamHP_CostPer		= SUM(CASE WHEN e.AccountId = 'PurSteamHP' THEN e.CostPerMBtu END)
, PurSteamIP_CostPer		= SUM(CASE WHEN e.AccountId = 'PurSteamIP' THEN e.CostPerMBtu END)
, PurSteamLP_CostPer		= SUM(CASE WHEN e.AccountId = 'PurSteamLP' THEN e.CostPerMBtu END)
, PurElec_CostPerkWh			= SUM(CASE WHEN e.AccountId = 'PurElec' THEN e.CostkWh END) * 100
, PurOther_CostPer			= [$(DbGlobal)].dbo.WtAvg(e.CostPerMBtu, CASE WHEN e.AccountId in ('PurEthane','PurPropane','PurButane','PurNaphtha','PurDistallate','PurResidual','PurCoalCoke') THEN e.Amount_MBtu ELSE 0 END)
, PurTot_CostPer			= @PurTot_CostPer
, PPFCFuelGas_CostPer		= SUM(CASE WHEN e.AccountId = 'PPFCFuelGas' THEN e.CostPerMBtu END)
, PPFCOther_CostPer			= [$(DbGlobal)].dbo.WtAvg(e.CostPerMBtu, CASE WHEN e.AccountId in ('PPFCEthane','PPFCPropane','PPFCButane','PPFCOther','PPFCPyroFuelOil','PPFCPyroGasOil','PPFCPyroNaphtha') THEN e.Amount_MBtu ELSE 0 END)
, PPFC_CostPer				= @PPFC_CostPer
, TotConsumption_CostPer	= @TotConsumption_CostPer
, ExportSteamSHP_CostPer	= SUM(CASE WHEN e.AccountId = 'ExportSteamSHP' THEN e.CostPerMBtu END)
, ExportSteamHP_CostPer		= SUM(CASE WHEN e.AccountId = 'ExportSteamHP' THEN e.CostPerMBtu END)
, ExportSteamIP_CostPer		= SUM(CASE WHEN e.AccountId = 'ExportSteamIP' THEN e.CostPerMBtu END)
, ExportSteamLP_CostPer		= SUM(CASE WHEN e.AccountId = 'ExportSteamLP' THEN e.CostPerMBtu END)
, ExportElectric_CostPerkWh		= SUM(CASE WHEN e.AccountId = 'ExportElectric' THEN e.CostkWh END) * 100
, ExportTot_CostPer = @ExportTot_CostPer
, NetEnergyCons_CostPer = @NetEnergyCons_CostPer
, PyroFurnInletTemp_C = @PyroFurnInletTemp_C
, PyroFurnInletTemp_F = @PyroFurnInletTemp_F
, PyroFurnPreheat_Pcnt = @PyroFurnPreheat_Pcnt
, PyroFurnFlueGasTemp_C = @PyroFurnFlueGasTemp_C
, PyroFurnFlueGasTemp_F = @PyroFurnFlueGasTemp_F
, PyroFurnFlueGasO2_Pcnt = @PyroFurnFlueGasO2_Pcnt
, PyroFurnPrimaryTLE_Pcnt = @PyroFurnPrimaryTLE_Pcnt
, PyroFurnSecondaryTLE_Pcnt = @PyroFurnSecondaryTLE_Pcnt
, PyroFurnTLESteam_PSIa = @PyroFurnTLESteam_PSIa
, TLEOutletTemp_C = @TLEOutletTemp_C
, TLEOutletTemp_F = @TLEOutletTemp_F
, CoolingWater_MTPerHVCMT = @CoolingWater_MTPerHVCMT
, CoolingWaterDelta_C = @CoolingWaterDelta_C
, CoolingWaterDelta_F = @CoolingWaterDelta_F
, CoolingWaterEnergy_BtuLbHVC = @CoolingWaterEnergy_BtuLbHVC
, CoolingWaterEnergy_GJMTHVC = @CoolingWaterEnergy_GJMTHVC
, FinFanEnergy_BtuLb = @FinFanEnergy_BtuLb
, FinFanEnergy_GJMT = @FinFanEnergy_GJMT
, CompGasEfficiency_Pcnt = @CompGasEfficiency_Pcnt
, EffCalcCompany_Pcnt = @EffCalcCompany_Pcnt
, EffCalcAPI_Pcnt = @EffCalcAPI_Pcnt
, SteamExtraction_MtMtHVC = @SteamExtraction_MtMtHVC
, SteamCondensing_MtMtHVC = @SteamCondensing_MtMtHVC
, ProcessedHeatExchanged_BTULbHVC = @ProcessedHeatExchanged_BTULbHVC
, ProcessedHeatExchanged_GJMtHVC = @ProcessedHeatExchanged_GJMtHVC
, QuenchHeatMatl_Pcnt = @QuenchHeatMatl_Pcnt
, QuenchDilution_Pcnt = @QuenchDilution_Pcnt
, PurFuelNatural_cnt = SUM(CASE WHEN e.AccountId = 'PurFuelNatural' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurEthane_cnt = SUM(CASE WHEN e.AccountId = 'PurEthane' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurPropane_cnt = SUM(CASE WHEN e.AccountId = 'PurPropane' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurResidual_cnt = SUM(CASE WHEN e.AccountId = 'PurResidual' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurOther_cnt = COUNT(DISTINCT CASE WHEN e.AccountId in ('PurEthane','PurPropane','PurButane','PurNaphtha','PurDistallate','PurResidual','PurCoalCoke') AND e.Amount_MBtu > 0 THEN e.Refnum END)
, PurSteamSHP_cnt = SUM(CASE WHEN e.AccountId = 'PurSteamSHP' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurSteamHP_cnt = SUM(CASE WHEN e.AccountId = 'PurSteamHP' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurSteamIP_cnt = SUM(CASE WHEN e.AccountId = 'PurSteamIP' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurSteamLP_cnt = SUM(CASE WHEN e.AccountId = 'PurSteamLP' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PurElec_cnt = SUM(CASE WHEN e.AccountId = 'PurElec' AND e.Amount_MBtu > 0 THEN 1 ELSE 0 END)
, PPFCFuelGas_cnt = SUM(CASE WHEN e.AccountId = 'PPFCFuelGas' AND e.CostPerMBtu > 0 THEN 1 ELSE 0 END)
, PPFCOther_cnt = COUNT(DISTINCT CASE WHEN e.AccountId in ('PPFCEthane','PPFCPropane','PPFCButane','PPFCOther','PPFCPyroFuelOil','PPFCPyroGasOil','PPFCPyroNaphtha')  AND e.CostPerMBtu > 0 THEN e.Refnum END)
, ExportSteamSHP_cnt = SUM(CASE WHEN e.AccountId = 'ExportSteamSHP' AND e.Amount_MBtu <> 0 THEN 1 ELSE 0 END)
, ExportSteamHP_cnt = SUM(CASE WHEN e.AccountId = 'ExportSteamHP' AND e.Amount_MBtu <> 0 THEN 1 ELSE 0 END)
, ExportSteamIP_cnt = SUM(CASE WHEN e.AccountId = 'ExportSteamIP' AND e.Amount_MBtu <> 0 THEN 1 ELSE 0 END)
, ExportSteamLP_cnt = SUM(CASE WHEN e.AccountId = 'ExportSteamLP' AND e.Amount_MBtu <> 0 THEN 1 ELSE 0 END)
, ExportElectric_cnt = SUM(CASE WHEN e.AccountId = 'ExportElectric' AND e.Amount_MBtu <> 0 THEN 1 ELSE 0 END)
, EnergyDeteriation_Pcnt = @EnergyDeteriation_Pcnt
, IBSLSteamRequirement_Pcnt = @IBSLSteamRequirement_Pcnt
, DivisorValue = MIN(d.DivisorValue)/1000.0
, DivisorAvgValue = MIN(CASE WHEN d.PlantCnt > 0 THEN d.DivisorValue/d.PlantCnt ELSE 0 END / CASE d.DataType WHEN 'UEDC' THEN 1000.0 WHEN 'EDC' then 1000.0 WHEN 'EII' THEN 365.0*100.0 ELSE 1.0 END)
FROM dbo.EnergyCalc e INNER JOIN @Divisors d on d.DataType=e.DataType 
WHERE e.Refnum=@Refnum
GROUP BY e.Currency, d.DataType

INSERT INTO dbo.Energy (Refnum
, Currency
, Scenario
, DataType
, EII
, EII_StdEnergy
, EEI_SPSL
, EEI_SPSL_StdEnergy
, EEI_SPSL_StdProd_kMT
, EEI_PYPS
, EEI_PYPS_StdEnergy
, EEI_PYPS_StdProd_kMT
, PurFuelNatural
, PurSteamSHP
, PurSteamHP
, PurSteamIP
, PurSteamLP
, PurElec
, PurEthane
, PurPropane
, PurButane
, PurNaphtha
, PurDistallate
, PurResidual
, PurCoalCoke
, PurOther
, PurchasedEnergy
, PPFCFuelGas
, PPFCEthane
, PPFCPropane
, PPFCButane
, PPFCPyroNaphtha
, PPFCPyroGasOil
, PPFCPyroFuelOil
, PPFCOther
, PPFC
, TotConsumption
, ExportSteamSHP
, ExportSteamHP
, ExportSteamIP
, ExportSteamLP
, ExportElectric
, ExportCredits
, NetEnergyCons
, SeparationEnergy_Supp_MBtu
, EIIStdPerUEDC
, PurFuelNatural_CostPer
, PurSteamSHP_CostPer
, PurSteamHP_CostPer
, PurSteamIP_CostPer
, PurSteamLP_CostPer
, PurElec_CostPerkWh
, PurOther_CostPer
, PurTot_CostPer
, PPFCFuelGas_CostPer
, PPFCOther_CostPer
, PPFC_CostPer
, TotConsumption_CostPer
, ExportSteamSHP_CostPer
, ExportSteamHP_CostPer
, ExportSteamIP_CostPer
, ExportSteamLP_CostPer
, ExportElectric_CostPerkWh
, ExportTot_CostPer
, NetEnergyCons_CostPer
, PyroFurnInletTemp_C
, PyroFurnInletTemp_F
, PyroFurnPreheat_Pcnt
, PyroFurnFlueGasTemp_C
, PyroFurnFlueGasTemp_F
, PyroFurnFlueGasO2_Pcnt
, PyroFurnPrimaryTLE_Pcnt
, PyroFurnSecondaryTLE_Pcnt
, PyroFurnTLESteam_PSIa
, TLEOutletTemp_C
, TLEOutletTemp_F
, CoolingWater_MTPerHVCMT
, CoolingWaterDelta_C
, CoolingWaterDelta_F
, CoolingWaterEnergy_BtuLbHVC
, CoolingWaterEnergy_GJMTHVC
, FinFanEnergy_BtuLb
, FinFanEnergy_GJMT
, CompGasEfficiency_Pcnt
, EffCalcCompany_Pcnt
, EffCalcAPI_Pcnt
, SteamExtraction_MtMtHVC
, SteamCondensing_MtMtHVC
, ProcessedHeatExchanged_BTULbHVC
, ProcessedHeatExchanged_GJMtHVC
, QuenchHeatMatl_Pcnt
, QuenchDilution_Pcnt
, PurFuelNatural_cnt
, PurEthane_cnt
, PurPropane_cnt
, PurResidual_cnt
, PurOther_cnt
, PurSteamSHP_cnt
, PurSteamHP_cnt
, PurSteamIP_cnt
, PurSteamLP_cnt
, PurElec_cnt
, PPFCFuelGas_cnt
, PPFCOther_cnt
, ExportSteamSHP_cnt
, ExportSteamHP_cnt
, ExportSteamIP_cnt
, ExportSteamLP_cnt
, ExportElectric_cnt
, EnergyDeteriation_Pcnt
, IBSLSteamRequirement_Pcnt
, DivisorValue
, DivisorAvgValue
)
SELECT Refnum = Refnum
, Currency = Currency
, Scenario = Scenario
, DataType = 'GJperHVCMt'
, EII = EII
, EII_StdEnergy = [$(DbGlobal)].dbo.UnitsConv(EII_StdEnergy, 'MBtu', 'GJ')
, EEI_SPSL = EEI_SPSL
, EEI_SPSL_StdEnergy = [$(DbGlobal)].dbo.UnitsConv(EEI_SPSL_StdEnergy, 'MBtu', 'GJ')
, EEI_SPSL_StdProd_kMT = EEI_SPSL_StdProd_kMT
, EEI_PYPS = EEI_PYPS
, EEI_PYPS_StdEnergy = [$(DbGlobal)].dbo.UnitsConv(EEI_PYPS_StdEnergy, 'MBtu', 'GJ')
, EEI_PYPS_StdProd_kMT = EEI_PYPS_StdProd_kMT
, PurFuelNatural = [$(DbGlobal)].dbo.UnitsConv(PurFuelNatural, 'KBTU', 'GJ')
, PurSteamSHP = [$(DbGlobal)].dbo.UnitsConv(PurSteamSHP, 'KBTU', 'GJ')
, PurSteamHP = [$(DbGlobal)].dbo.UnitsConv(PurSteamHP, 'KBTU', 'GJ')
, PurSteamIP = [$(DbGlobal)].dbo.UnitsConv(PurSteamIP, 'KBTU', 'GJ')
, PurSteamLP = [$(DbGlobal)].dbo.UnitsConv(PurSteamLP, 'KBTU', 'GJ')
, PurElec = [$(DbGlobal)].dbo.UnitsConv(PurElec, 'KBTU', 'GJ')
, PurEthane = [$(DbGlobal)].dbo.UnitsConv(PurEthane, 'KBTU', 'GJ')
, PurPropane = [$(DbGlobal)].dbo.UnitsConv(PurPropane, 'KBTU', 'GJ')
, PurButane = [$(DbGlobal)].dbo.UnitsConv(PurButane, 'KBTU', 'GJ')
, PurNaphtha = [$(DbGlobal)].dbo.UnitsConv(PurNaphtha, 'KBTU', 'GJ')
, PurDistallate = [$(DbGlobal)].dbo.UnitsConv(PurDistallate, 'KBTU', 'GJ')
, PurResidual = [$(DbGlobal)].dbo.UnitsConv(PurResidual, 'KBTU', 'GJ')
, PurCoalCoke = [$(DbGlobal)].dbo.UnitsConv(PurCoalCoke, 'KBTU', 'GJ')
, PurOther = [$(DbGlobal)].dbo.UnitsConv(PurOther, 'KBTU', 'GJ')
, PurchasedEnergy = [$(DbGlobal)].dbo.UnitsConv(PurchasedEnergy, 'KBTU', 'GJ')
, PPFCFuelGas = [$(DbGlobal)].dbo.UnitsConv(PPFCFuelGas, 'KBTU', 'GJ')
, PPFCEthane = [$(DbGlobal)].dbo.UnitsConv(PPFCEthane, 'KBTU', 'GJ')
, PPFCPropane = [$(DbGlobal)].dbo.UnitsConv(PPFCPropane, 'KBTU', 'GJ')
, PPFCButane = [$(DbGlobal)].dbo.UnitsConv(PPFCButane, 'KBTU', 'GJ')
, PPFCPyroNaphtha = [$(DbGlobal)].dbo.UnitsConv(PPFCPyroNaphtha, 'KBTU', 'GJ')
, PPFCPyroGasOil = [$(DbGlobal)].dbo.UnitsConv(PPFCPyroGasOil, 'KBTU', 'GJ')
, PPFCPyroFuelOil = [$(DbGlobal)].dbo.UnitsConv(PPFCPyroFuelOil, 'KBTU', 'GJ')
, PPFCOther = [$(DbGlobal)].dbo.UnitsConv(PPFCOther, 'KBTU', 'GJ')
, PPFC = [$(DbGlobal)].dbo.UnitsConv(PPFC, 'KBTU', 'GJ')
, TotConsumption = [$(DbGlobal)].dbo.UnitsConv(TotConsumption, 'KBTU', 'GJ')
, ExportSteamSHP = [$(DbGlobal)].dbo.UnitsConv(ExportSteamSHP, 'KBTU', 'GJ')
, ExportSteamHP = [$(DbGlobal)].dbo.UnitsConv(ExportSteamHP, 'KBTU', 'GJ')
, ExportSteamIP = [$(DbGlobal)].dbo.UnitsConv(ExportSteamIP, 'KBTU', 'GJ')
, ExportSteamLP = [$(DbGlobal)].dbo.UnitsConv(ExportSteamLP, 'KBTU', 'GJ')
, ExportElectric = [$(DbGlobal)].dbo.UnitsConv(ExportElectric, 'KBTU', 'GJ')
, ExportCredits = [$(DbGlobal)].dbo.UnitsConv(ExportCredits, 'KBTU', 'GJ')
, NetEnergyCons = [$(DbGlobal)].dbo.UnitsConv(NetEnergyCons, 'KBTU', 'GJ')
, SeparationEnergy_Supp_MBtu = [$(DbGlobal)].dbo.UnitsConv(SeparationEnergy_Supp_MBtu, 'KBTU', 'GJ')
, EIIStdPerUEDC = [$(DbGlobal)].dbo.UnitsConv(EIIStdPerUEDC, 'MBtu', 'GJ')
, PurFuelNatural_CostPer = [$(DbGlobal)].dbo.UnitsConv(PurFuelNatural_CostPer, '1/MBtu', '1/GJ')
, PurSteamSHP_CostPer = [$(DbGlobal)].dbo.UnitsConv(PurSteamSHP_CostPer, '1/MBtu', '1/GJ')
, PurSteamHP_CostPer = [$(DbGlobal)].dbo.UnitsConv(PurSteamHP_CostPer, '1/MBtu', '1/GJ')
, PurSteamIP_CostPer = [$(DbGlobal)].dbo.UnitsConv(PurSteamIP_CostPer, '1/MBtu', '1/GJ')
, PurSteamLP_CostPer = [$(DbGlobal)].dbo.UnitsConv(PurSteamLP_CostPer, '1/MBtu', '1/GJ')
, PurElec_CostPerkWh = PurElec_CostPerkWh
, PurOther_CostPer = [$(DbGlobal)].dbo.UnitsConv(PurOther_CostPer, '1/MBtu', '1/GJ')
, PurTot_CostPer = [$(DbGlobal)].dbo.UnitsConv(PurTot_CostPer, '1/MBtu', '1/GJ')
, PPFCFuelGas_CostPer = [$(DbGlobal)].dbo.UnitsConv(PPFCFuelGas_CostPer, '1/MBtu', '1/GJ')
, PPFCOther_CostPer = [$(DbGlobal)].dbo.UnitsConv(PPFCOther_CostPer, '1/MBtu', '1/GJ')
, PPFC_CostPer = [$(DbGlobal)].dbo.UnitsConv(PPFC_CostPer, '1/MBtu', '1/GJ')
, TotConsumption_CostPer = [$(DbGlobal)].dbo.UnitsConv(TotConsumption_CostPer, '1/MBtu', '1/GJ')
, ExportSteamSHP_CostPer = [$(DbGlobal)].dbo.UnitsConv(ExportSteamSHP_CostPer, '1/MBtu', '1/GJ')
, ExportSteamHP_CostPer = [$(DbGlobal)].dbo.UnitsConv(ExportSteamHP_CostPer, '1/MBtu', '1/GJ')
, ExportSteamIP_CostPer = [$(DbGlobal)].dbo.UnitsConv(ExportSteamIP_CostPer, '1/MBtu', '1/GJ')
, ExportSteamLP_CostPer = [$(DbGlobal)].dbo.UnitsConv(ExportSteamLP_CostPer, '1/MBtu', '1/GJ')
, ExportElectric_CostPerkWh = ExportElectric_CostPerkWh
, ExportTot_CostPer = [$(DbGlobal)].dbo.UnitsConv(ExportTot_CostPer, '1/MBtu', '1/GJ')
, NetEnergyCons_CostPer = [$(DbGlobal)].dbo.UnitsConv(NetEnergyCons_CostPer, '1/MBtu', '1/GJ')
, PyroFurnInletTemp_C = PyroFurnInletTemp_C
, PyroFurnInletTemp_F = PyroFurnInletTemp_F
, PyroFurnPreheat_Pcnt = PyroFurnPreheat_Pcnt
, PyroFurnFlueGasTemp_C = PyroFurnFlueGasTemp_C
, PyroFurnFlueGasTemp_F = PyroFurnFlueGasTemp_F
, PyroFurnFlueGasO2_Pcnt = PyroFurnFlueGasO2_Pcnt
, PyroFurnPrimaryTLE_Pcnt = PyroFurnPrimaryTLE_Pcnt
, PyroFurnSecondaryTLE_Pcnt = PyroFurnSecondaryTLE_Pcnt
, PyroFurnTLESteam_PSIa = PyroFurnTLESteam_PSIa
, TLEOutletTemp_C = TLEOutletTemp_C
, TLEOutletTemp_F = TLEOutletTemp_F
, CoolingWater_MTPerHVCMT = CoolingWater_MTPerHVCMT
, CoolingWaterDelta_C = CoolingWaterDelta_C
, CoolingWaterDelta_F = CoolingWaterDelta_F
, CoolingWaterEnergy_BtuLbHVC = CoolingWaterEnergy_BtuLbHVC
, CoolingWaterEnergy_GJMTHVC = CoolingWaterEnergy_GJMTHVC
, FinFanEnergy_BtuLb = FinFanEnergy_BtuLb
, FinFanEnergy_GJMT = FinFanEnergy_GJMT
, CompGasEfficiency_Pcnt = CompGasEfficiency_Pcnt
, EffCalcCompany_Pcnt = EffCalcCompany_Pcnt
, EffCalcAPI_Pcnt = EffCalcAPI_Pcnt
, SteamExtraction_MtMtHVC = SteamExtraction_MtMtHVC
, SteamCondensing_MtMtHVC = SteamCondensing_MtMtHVC
, ProcessedHeatExchanged_BTULbHVC = ProcessedHeatExchanged_BTULbHVC
, ProcessedHeatExchanged_GJMtHVC = ProcessedHeatExchanged_GJMtHVC
, QuenchHeatMatl_Pcnt = QuenchHeatMatl_Pcnt
, QuenchDilution_Pcnt = QuenchDilution_Pcnt
, PurFuelNatural_cnt = PurFuelNatural_cnt
, PurEthane_cnt = PurEthane_cnt
, PurPropane_cnt = PurPropane_cnt
, PurResidual_cnt = PurResidual_cnt
, PurOther_cnt = PurOther_cnt
, PurSteamSHP_cnt = PurSteamSHP_cnt
, PurSteamHP_cnt = PurSteamHP_cnt
, PurSteamIP_cnt = PurSteamIP_cnt
, PurSteamLP_cnt = PurSteamLP_cnt
, PurElec_cnt = PurElec_cnt
, PPFCFuelGas_cnt = PPFCFuelGas_cnt
, PPFCOther_cnt = PPFCOther_cnt
, ExportSteamSHP_cnt = ExportSteamSHP_cnt
, ExportSteamHP_cnt = ExportSteamHP_cnt
, ExportSteamIP_cnt = ExportSteamIP_cnt
, ExportSteamLP_cnt = ExportSteamLP_cnt
, ExportElectric_cnt = ExportElectric_cnt
, EnergyDeteriation_Pcnt = EnergyDeteriation_Pcnt
, IBSLSteamRequirement_Pcnt = IBSLSteamRequirement_Pcnt
, DivisorValue = DivisorValue
, DivisorAvgValue = DivisorAvgValue
FROM dbo.Energy e 
Where DataType = 'kBtuPerHVCMt'
AND e.Refnum=@Refnum



SET NOCOUNT OFF

