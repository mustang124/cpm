﻿CREATE TABLE [dbo].[Financial] (
    [Refnum]                        VARCHAR (25)  NOT NULL,
    [Currency]                      VARCHAR (5)   NOT NULL,
    [NCMPerEthyleneMT]              REAL          NULL,
    [NCMPerOlefinsMT]               REAL          NULL,
    [NCMPerHVChemMT]                REAL          NULL,
    [EthyleneProdCostPerUEDC]       REAL          NULL,
    [OlefinsProdCostPerUEDC]        REAL          NULL,
    [HVChemProdCostPerUEDC]         REAL          NULL,
    [EthyleneProdCostPerMT]         REAL          NULL,
    [OlefinsProdCostPerMT]          REAL          NULL,
    [HVChemProdCostPerMT]           REAL          NULL,
    [GPVPerUEDC]                    REAL          NULL,
    [RMCPerUEDC]                    REAL          NULL,
    [OpExPerUEDC]                   REAL          NULL,
    [GPVPerRVUsgc]                  REAL          NULL,
    [RMCPerRVUsgc]                  REAL          NULL,
    [GMPerRVUsgc]                   REAL          NULL,
    [OpExPerRVUsgc]                 REAL          NULL,
    [NCMPerRVUsgc]                  REAL          NULL,
    [NMPerRVUsgc]                   REAL          NULL,
    [GPVPerRV]                      REAL          NULL,
    [RMCPerRV]                      REAL          NULL,
    [GMPerRV]                       REAL          NULL,
    [OpExPerRV]                     REAL          NULL,
    [NCMperRV]                      REAL          NULL,
    [ROI]                           REAL          NULL,
    [NCMperRVTAAdj]                 REAL          NULL,
    [NMPerRV]                       REAL          NULL,
    [ProformaOpExPerUEDC]           REAL          NULL,
    [ProformaOpExPerEthyleneMT]     REAL          NULL,
    [ProformaNCMPerEthyleneMT]      REAL          NULL,
    [ProformaProdCostPerEthyleneMT] REAL          NULL,
    [ProformaOpExPerHVChemMT]       REAL          NULL,
    [ProformaNCMPerHVChemMT]        REAL          NULL,
    [ProformaProdCostPerHVChemMT]   REAL          NULL,
    [ProformaROI]                   REAL          NULL,
    [tsModified]                    SMALLDATETIME DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Financial] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Currency] ASC) WITH (FILLFACTOR = 70)
);

