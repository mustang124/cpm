﻿


CREATE view [dbo].[TSort] as
Select ts.Refnum
, SmallRefnum = SUBSTRING(ts.Refnum, 3, 20)
, Company = RTRIM(sa.SubscriberAssetName)		
, Location = RTRIM(sa.SubscriberAssetDetail)		
, CoLoc = RTRIM(sa.SubscriberAssetName) + ' - ' + 	RTRIM(sa.SubscriberAssetDetail)
, StudyId = sa.StudyId
, StudyYear = sa._StudyYear	
, DataYear = tw.DataYear		
, Region = tw.[EconRegionId]
, Country = tw.CountryName
, CountryId = tw.CountryId
, CompanyId = sa.CompanyId
, ContactCode = ts.ContactCode
, AssetId = sa.AssetId
, IsPlant = CASE WHEN LEFT(ts.Refnum,4) = LEFT(sa._StudyYear,4) THEN 1 else 0 END
, RefDirectory = ts.RefDirectory
, UomId = tw.UomId
, HomeCurrency = tw.CurrencyFcn
, RptCurrency = tw.CurrencyRpt
, FactorSetId = ts.FactorSetId
, NumDays = [$(DbGlobal)].dbo.DaysInYear(tw.DataYear)	
, Consultant = ts.Consultant
, AssetPassWord = tw.AssetPassWord_PlainText
, Comment = ts.Comments
, RefNumber = sa.AssetId
, GroupId = ts.Refnum
from cons.TSortSolomon ts JOIN cons.SubscriptionsAssets sa on sa.Refnum=ts.Refnum
LEFT JOIN (Select t.Refnum, t.DataYear, t.UomId, t.CurrencyFcn, t.CurrencyRpt, t.AssetPassWord_PlainText, cr.FactorSetId, cr.[EconRegionId], cn.CountryName, t.CountryId from fact.TSort t JOIN ante.CountryEconRegion cr ON cr.CountryId = t.CountryId	
JOIN dim.Country_LookUp cn on cn.CountryId=t.CountryId) tw on tw.Refnum=ts.Refnum and tw.FactorSetId = ts.FactorSetId













