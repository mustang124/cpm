﻿CREATE PROCEDURE [fact].[Insert_MetaOpEx]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.MetaOpEx(Refnum, CalDateKey, AccountId, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.AccountId
			, 'USD'
			, u.AmountRPT
		FROM(
			SELECT 
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum]
				, CONVERT(INT, CONVERT(VARCHAR(4), t.StudyYear) + CONVERT(VARCHAR(4),
					CASE m.[Type]
						WHEN 'Qtr1' THEN '0331'
						WHEN 'Qtr2' THEN '0630'
						WHEN 'Qtr3' THEN '0930'
						WHEN 'Qtr4' THEN '1231'
					END)
					)												[CalDateKey]
				, m.Vol												[MetaSTVol]
				, m.NVE												[MetaNonVol]
			FROM stgFact.MetaOpEx m
			INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
			WHERE m.[Type] <> 'Total'
				AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT (
			AmountRPT FOR AccountId IN (
				[MetaSTVol], [MetaNonVol]
				)
			) u
		WHERE u.AmountRPT <> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;