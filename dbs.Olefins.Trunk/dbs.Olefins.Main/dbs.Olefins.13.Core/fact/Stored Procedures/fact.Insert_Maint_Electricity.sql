﻿CREATE PROCEDURE [fact].[Insert_Maint_Electricity]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		PRINT NCHAR(9) + 'INSERT INTO fact.Maint (1995 - 1999 (ElecGen/ElecDist))';

		INSERT INTO fact.Maint(Refnum, CalDateKey, FacilityId, CurrencyRpt, MaintMaterial_Cur, MaintLabor_Cur, TaMaterial_Cur, TaLabor_Cur)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, etl.ConvFacilityID(m.ProjectID)
			, 'USD'
			, m.RoutMaintMatl
			, m.RoutMaintLabor
			, m.TAMatl
			, m.TALabor
		FROM stgFact.Maint m
		INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
		WHERE	(m.RoutMaintMatl IS NOT NULL
			OR	m.RoutMaintLabor IS NOT NULL
			OR	m.TAMatl IS NOT NULL
			OR	m.TALabor IS NOT NULL)
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

		PRINT NCHAR(9) + 'INSERT INTO fact.Maint (2001 - 2009 (ElectGenDist))';

		INSERT INTO fact.Maint(Refnum, CalDateKey, FacilityId, CurrencyRpt, MaintMaterial_Cur, MaintLabor_Cur, TaMaterial_Cur, TaLabor_Cur)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, etl.ConvFacilityID(CASE m.ProjectID WHEN 'ElecGen' THEN 'ElectGenDist' ELSE m.ProjectID END)
			, 'USD'
			, m.RoutMaintMatl
			, m.RoutMaintLabor
			, m.TAMatl
			, m.TALabor
		FROM stgFact.Maint01 m
		INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
		WHERE	(m.RoutMaintMatl IS NOT NULL
			OR	m.RoutMaintLabor IS NOT NULL
			OR	m.TAMatl IS NOT NULL
			OR	m.TALabor IS NOT NULL)
			AND	m.ProjectID	<> 'Tubes'
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;


	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;