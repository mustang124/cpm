﻿CREATE PROCEDURE [fact].[Insert_Capacity]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Capacity(Refnum, CalDateKey, StreamId, Capacity_kMT, Stream_MTd, Record_MTd)
		SELECT
			  ISNULL(a.Refnum, ISNULL(s.Refnum, r.Refnum))				[Refnum]
			, ISNULL(a.CalDateKey, ISNULL(s.CalDateKey, r.CalDateKey))	[CalDateKey]
			, ISNULL(a.StreamId, ISNULL(s.StreamId, r.StreamId))		[StreamId]
			, a.Capacity_KMT
			, CASE WHEN s.Stream_MTd > 0.0
				THEN s.Stream_MTd
				ELSE CASE WHEN a.StreamId = 'Ethylene' THEN a.Capacity_KMT / a.SvcFactor / 356.0 * 1000.0 END
				END														[Stream_MTd]
			, r.Record_MTd
		FROM (
			SELECT
					u.Refnum
				, u.CalDateKey
				, u.StreamId
				, u.Capacity_KMT
				, u.SvcFactor
			FROM(
				SELECT
						etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
					, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
					, c.EthylCapKMTA		[Ethylene]
					, c.PropylCapKMTA		[Propylene]
					, c.FurnaceCapKMTA		[FreshPyroFeed]
					, c.RecycleCapKMTA		[Recycle]
					, c.SvcFactor
				FROM stgFact.Capacity c
				INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
				) p
				UNPIVOT(
				Capacity_KMT FOR StreamId IN(
						[Ethylene]
					, [Propylene]
					, [FreshPyroFeed]
					, [Recycle]
					)
				) u
			) a
		FULL OUTER JOIN (
			SELECT
					u.Refnum
				, u.CalDateKey
				, u.StreamId
				, u.Stream_MTd
			FROM(
				SELECT
						etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
					, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
					, c.EthylCapMTD						[Ethylene]
					, c.OlefinsCapMTD - c.EthylCapMTD	[Propylene]
				FROM stgFact.Capacity c
				INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
				) p
				UNPIVOT(
				Stream_MTd FOR StreamId IN(
						[Ethylene]
					, [Propylene]
					)
				) u
			) s
			ON	s.Refnum = a.Refnum
			AND	s.CalDateKey = a.CalDateKey
			AND s.StreamId = a.StreamId
		FULL OUTER JOIN(
			SELECT
					u.Refnum
				, u.CalDateKey
				, u.StreamId
				, u.Record_MTd
			FROM(
				SELECT
						etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
					, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
					, c.EthylMaxCapMTD							[Ethylene]
					, CASE WHEN c.OlefinsMaxCapMTD > c.EthylMaxCapMTD THEN c.OlefinsMaxCapMTD - c.EthylMaxCapMTD END		[Propylene]
				FROM stgFact.Capacity c
				INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
				) p
				UNPIVOT(
				Record_MTd FOR StreamId IN(
						[Ethylene]
					, [Propylene]
					)
				) u
			) r
			ON	r.Refnum = ISNULL(a.Refnum, s.Refnum)
			AND	r.CalDateKey = ISNULL(a.CalDateKey, s.CalDateKey)
			AND r.StreamId = ISNULL(a.StreamId, s.StreamId)
		WHERE ISNULL(a.Refnum, ISNULL(s.Refnum, r.Refnum)) = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;