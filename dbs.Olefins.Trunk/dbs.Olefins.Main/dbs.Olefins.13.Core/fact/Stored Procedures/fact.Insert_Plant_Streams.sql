﻿CREATE PROCEDURE [fact].[Insert_Plant_Streams]
(
	@Refnum		VARCHAR(18)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	PRINT NCHAR(9) + 'INSERT INTO fact.Quantity';

	INSERT INTO fact.Quantity(Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT)
	SELECT
		  u.Refnum
		, CONVERT(INT, CONVERT(VARCHAR(4), u.StudyYear) + CONVERT(VARCHAR(4), u.CalDateKey))
																[CalDateKey]
		, etl.ConvStreamID(u.FeedProdID)						[StreamId]
		, ISNULL(u.StreamDescription, u.FeedProdID)				[StreamDescription]

		, ABS(u.kMT)											[kMT]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum]
			, t.StudyYear
			, q.FeedProdID	
			, CASE q.FeedProdID
				WHEN 'OthSpl'		THEN q.MiscFeed
				WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
				WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				
				WHEN 'OthLiqFeed1'	THEN f.OthLiqFeedDESC + ' (Other Feed 1)'
				WHEN 'OthLiqFeed2'	THEN f.OthLiqFeedDESC + ' (Other Feed 2)'
				WHEN 'OthLiqFeed3'	THEN f.OthLiqFeedDESC + ' (Other Feed 3)'
				WHEN 'OthLtFeed'	THEN f.OthLiqFeedDESC
				ELSE q.FeedProdID
				END												[StreamDescription]
				
			, ISNULL(q.Q1Feed, 0.0)								[0331]
			, ISNULL(q.Q2Feed, 0.0)								[0630]
			, ISNULL(q.Q3Feed, 0.0)								[0930]
			, ISNULL(q.Q4Feed, 0.0)								[1231]
			
		FROM stgFact.Quantity q
		LEFT OUTER JOIN stgFact.FeedQuality f
			ON	f.Refnum = q.Refnum
			AND f.FeedProdID = q.FeedProdID
		INNER JOIN stgFact.TSort t
			ON	t.Refnum = q.Refnum
		WHERE (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
			AND q.FeedProdID <> 'CO & CO2'
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT(
		kMT FOR CalDateKey IN (
			[0331], [0630], [0930], [1231]
			)
		) u;

	PRINT NCHAR(9) + 'INSERT INTO fact.QuantitySuppRecovery';
	
	INSERT INTO fact.QuantitySuppRecovery(Refnum, CalDateKey, StreamId, StreamDescription, Recovered_WtPcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		
		, u.StreamId
		, ISNULL(u.StreamDescription, u.StreamId)					[StreamDescription]
		
		, u.RecPcnt	* 100.0											[RecoveredPcnt]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvStreamID(q.FeedProdID)						[StreamId]
			
			, CASE q.FeedProdID
				WHEN 'OthSpl'		THEN q.MiscFeed
				WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
				WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				ELSE q.FeedProdID
				END													[StreamDescription]
				
			, CASE WHEN q.RecPcnt = 100 THEN 1 ELSE q.RecPcnt END
																	[RecPcnt]
			
		FROM stgFact.Quantity q
		INNER JOIN stgFact.TSort t		ON	t.Refnum = q.Refnum
		WHERE (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
			AND q.RecPcnt > 0.0
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) u;
		
	PRINT NCHAR(9) + 'INSERT INTO fact.QuantitySuppRecycled';
	
	INSERT INTO fact.QuantitySuppRecycled(Refnum, CalDateKey, StreamId, ComponentId, Recycled_WtPcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, u.StreamId
		, CASE u.StreamId
			WHEN 'SuppToRecEthane'	THEN 'C2H6'
			WHEN 'SuppToRecPropane'	THEN 'C3H8'
			WHEN 'SuppToRecButane'	THEN 'C4H10'
			END														[ComponentId]
			
		, CASE RecycledPcnt WHEN 100.0 THEN 1.0 ELSE RecycledPcnt END * 100.0
																	[RecycledPcnt]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, m.EthPcntTot	[SuppToRecEthane]
			, m.ProPcntTot	[SuppToRecPropane]
			, m.ButPcntTot	[SuppToRecButane]
		FROM stgFact.Misc m
		INNER JOIN stgFact.TSort t		ON	t.Refnum = m.Refnum
		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT(
		RecycledPcnt FOR StreamId IN (
			  [SuppToRecEthane]
			, [SuppToRecPropane]
			, [SuppToRecButane]
		)
		) u;

	PRINT NCHAR(9) + 'INSERT INTO fact.QuantityLHValue';

	INSERT INTO fact.QuantityLHValue(Refnum, CalDateKey, StreamId, StreamDescription, LHValue_MBtuLb)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.FeedProdID)							[StreamId]
		, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
		, u.HeatValue
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				
			, p.FeedProdID
			
			, CASE q.FeedProdID
				WHEN 'OthSpl'		THEN q.MiscFeed
				WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
				WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				END													[StreamDescription]
				
			, p.HeatValue
		FROM stgFact.ProdQuality p
		INNER JOIN stgFact.Quantity q
			ON	q.Refnum = p.Refnum
			AND q.FeedProdID = p.FeedProdID
			AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
		INNER JOIN stgFact.TSort t		ON t.Refnum = p.Refnum
		WHERE p.HeatValue > 0.0
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) u;

	PRINT NCHAR(9) + 'INSERT INTO fact.QuantityValue (0)';

	INSERT INTO fact.QuantityValue(Refnum, CalDateKey, StreamId, StreamDescription, CurrencyRpt, Amount_Cur)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.FeedProdID)							[StreamId]
		, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
		, 'USD'
		, Amount_Cur
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			
			, p.FeedProdID
			
			, etl.ConvStreamDescription(q.FeedProdID, p.OthLiqFeedDESC, q.MiscProd1, q.MiscProd2, q.MiscFeed) [StreamDescription]
				
			, p.OthLiqFeedPriceBasis								[Amount_Cur]
		FROM stgFact.FeedQuality p
		INNER JOIN stgFact.Quantity q
			ON	q.Refnum = p.Refnum
			AND q.FeedProdID = p.FeedProdID
			AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
		INNER JOIN stgFact.TSort t
			ON t.Refnum = p.Refnum
		WHERE p.OthLiqFeedPriceBasis > 0.0
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) u;

	PRINT NCHAR(9) + 'INSERT INTO fact.QuantityValue (1)';

	INSERT INTO fact.QuantityValue(Refnum, CalDateKey, StreamId, StreamDescription, CurrencyRpt, Amount_Cur)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.FeedProdID)							[StreamId]
		, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
		, 'USD'
		, Amount_Cur
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				
			, p.FeedProdID
			
			, CASE q.FeedProdID
				WHEN 'OthSpl'		THEN q.MiscFeed
				WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
				WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				END													[StreamDescription]
				
			, ISNULL(p.H2Value, ISNULL(p.OthProd1Value, p.OthProd2Value))
																	[Amount_Cur]
			
		FROM stgFact.ProdQuality p
		INNER JOIN stgFact.Quantity q
			ON	q.Refnum = p.Refnum
			AND q.FeedProdID = p.FeedProdID
			AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
		INNER JOIN stgFact.TSort t		ON t.Refnum = p.Refnum
		WHERE (p.OthProd1Value > 0.0 OR p.OthProd2Value > 0.0 OR p.H2Value > 0.0)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) u;

	-- Table 2A-1 Light Feed, 2A-2 Liquid Feed, and 2B Recycle Feed Composition
	-- Includes Hydrogen for Liquids
	PRINT NCHAR(9) + 'INSERT INTO fact.CompositionQuantity (Table 2A-1 Light Feed, 2A-2 Liquid Feed, and 2B Recycle Feed Composition)';

	INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.FeedProdID)							[StreamId]
		, ISNULL(u.[StreamDescription], u.FeedProdID)					[StreamDescription]
		, etl.ConvComponentID(u.ComponentId)					[ComponentId]
		, u.WtPcnt * 100.0											[WtPcnt]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, q.FeedProdID
			--, q.OthLiqFeedDESC
			, etl.ConvStreamDescription(z.FeedProdID, q.OthLiqFeedDESC, z.MiscProd1, z.MiscProd2, z.MiscFeed) [StreamDescription]

			, q.Methane			[CH4]
			, q.Ethane			[C2H6]
			, q.Ethylene		[C2H4]
			, q.Propane			[C3H8]
			, q.Propylene		[C3H6]
			, q.nButane			[NBUTA]
			, q.iButane			[IBUTA]
			, q.Isobutylene		[IB]
			, q.Butene1			[B1]
			, q.Butadiene		[C4H6]
			, q.nPentane		[NC5]
			, q.iPentane		[IC5]
			, q.nHexane			[NC6]
			, q.iHexane			[C6ISO]
			, q.Septane			[C7H16]
			, q.Octane			[C8H18]
			, q.CO2				[CO2]
			, q.Hydrogen		[H2]
			, q.Sulfur			[S]
			
			, q.NParaffins		[P]
			, q.IsoParaffins	[I]
			, q.Aromatics		[A]
			, q.Naphthenes		[N]
			, q.Olefins			[O]
			
		FROM stgFact.FeedQuality q
		INNER JOIN stgFact.TSort t
			ON	t.Refnum = q.Refnum
		INNER JOIN stgFact.Quantity z
			ON	z.Refnum = q.Refnum
			AND	z.FeedProdID = q.FeedProdID
			AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0

		WHERE	(q.Methane		> 0.0
			OR	q.Ethane		> 0.0
			OR	q.Ethylene		> 0.0
			OR	q.Propane		> 0.0
			OR	q.Propylene		> 0.0
			OR	q.nButane		> 0.0
			OR	q.iButane		> 0.0
			OR	q.Isobutylene	> 0.0
			OR	q.Butene1		> 0.0
			OR	q.Butadiene		> 0.0
			OR	q.nPentane		> 0.0
			OR	q.iPentane		> 0.0
			OR	q.nHexane		> 0.0
			OR	q.iHexane		> 0.0
			OR	q.Septane		> 0.0
			OR	q.Octane		> 0.0
			OR	q.CO2			> 0.0
			OR	q.Hydrogen		> 0.0
			OR	q.Sulfur		> 0.0
			
			OR	q.NParaffins	> 0.0
			OR	q.IsoParaffins	> 0.0
			OR	q.Aromatics		> 0.0
			OR	q.Naphthenes	> 0.0
			OR	q.Olefins		> 0.0)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
	) p
		UNPIVOT(
		WtPcnt FOR ComponentId IN(
			  [CH4]
			, [C2H6]
			, [C2H4]
			, [C3H8]
			, [C3H6]
			, [NBUTA]
			, [IBUTA]
			, [IB]
			, [B1]
			, [C4H6]
			, [NC5]
			, [IC5]
			, [NC6]
			, [C6ISO]
			, [C7H16]
			, [C8H18]
			, [CO2]
			, [H2]
			, [S]
			
			, [P]
			, [I]
			, [A]
			, [N]
			, [O]
			)
		) u
	WHERE u.WtPcnt > 0.0 AND u.WtPcnt <= 1.0;

	PRINT NCHAR(9) + 'INSERT INTO fact.CompositionQuantity (Sulfur PPM)';

	INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.FeedProdID)							[StreamId]
		, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
		, etl.ConvComponentID(u.ComponentId)						[ComponentId]
		, u.Component_WtPcnt										[WtPcnt]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, q.FeedProdID
			, CASE q.FeedProdID
				--WHEN 'OthSpl'		THEN q.MiscFeed
				--WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
				--WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				
				WHEN 'OthLiqFeed1'	THEN q.OthLiqFeedDESC + ' (Other Feed 1)'
				WHEN 'OthLiqFeed2'	THEN q.OthLiqFeedDESC + ' (Other Feed 2)'
				WHEN 'OthLiqFeed3'	THEN q.OthLiqFeedDESC + ' (Other Feed 3)'
				WHEN 'OthLtFeed'	THEN q.OthLiqFeedDESC
				ELSE q.FeedProdID
				END												[StreamDescription]
			
		, 'S'													[ComponentId]
		, q.SulfurPPM / 10000.0									[Component_WtPcnt]
			
		FROM stgFact.FeedQuality q
		INNER JOIN stgFact.TSort t
			ON	t.Refnum = q.Refnum
		INNER JOIN stgFact.Quantity z
			ON	z.Refnum = q.Refnum
			AND	z.FeedProdID = q.FeedProdID
			AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
			AND q.SulfurPPM > 0.0
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) u;

	-- Other Products
	PRINT NCHAR(9) + 'INSERT INTO fact.CompositionQuantity (Other Products)';

	INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.FeedProdID)							[StreamId]
		, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
		, etl.ConvComponentID(u.ComponentId)						[ComponentId]
		, u.WtPcnt * 100.0											[WtPcnt]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				
			, c.FeedProdID
			
			, CASE q.FeedProdID
				WHEN 'OthSpl'		THEN q.MiscFeed
				WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
				WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				END													[StreamDescription]
			
			, c.H2			[H2]			-- Hydrogen
			, c.CH4			[CH4]			-- Methane
			
			, c.C2H2		[C2H2]			-- Acetylene
			, c.C2H4		[C2H4]			-- Ethylene
			, c.C2H6		[C2H6]			-- Ethane
			
			, c.C3H6		[C3H6]			-- Propylene
			, c.C3H8		[C3H8]			-- Propane
			
			, c.BUTAD		[C4H6]			-- Butadiene
			, c.C4S			[C4H8]			-- Butene
			, c.C4H10		[C4H10]			-- Butane
			, c.BZ			[C6H6]			-- Benzene
			, c.PYGAS		[C9_400F]		-- PyroGasOil	/* Component discreptancy in calcs!YIRCalc	*/
			, c.PYOIL		[PyroFuelOil]
			, c.INERT		[Inerts]
			
		FROM stgFact.Composition c
		INNER JOIN stgFact.TSort t
			ON t.Refnum = c.Refnum
		INNER JOIN stgFact.Quantity q
			ON q.Refnum = c.Refnum
			AND q.FeedProdID = c.FeedProdID
			AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
		WHERE	(c.H2		> 0.0
			OR	c.CH4		> 0.0
			OR	c.C2H2		> 0.0
			OR	c.C2H4		> 0.0
			OR	c.C2H6		> 0.0
			OR	c.C3H6		> 0.0
			OR	c.C3H8		> 0.0
			OR	c.BUTAD		> 0.0
			OR	c.C4S		> 0.0
			OR	c.C4H10		> 0.0
			OR	c.BZ		> 0.0
			OR	c.PYGAS		> 0.0
			OR	c.PYOIL		> 0.0
			OR	c.INERT		> 0.0)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT(
		WtPcnt FOR ComponentId IN (
			H2,
			CH4,
			C2H2,
			C2H4,
			C2H6,
			C3H6,
			C3H8,
			C4H6,
			C4H8,
			C4H10,
			C6H6,
			C9_400F,
			-- PyroGasOil, /* Component discreptancy in calcs!YIRCalc	*/
			PyroFuelOil,
			Inerts
			)
		) u
	WHERE u.WtPcnt > 0.0;

	-- Table 3 (QualityWt)
	PRINT NCHAR(9) + 'INSERT INTO fact.CompositionQuantity (T3 QualityWt)';

	INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvStreamID(q.FeedProdID)						[StreamId]
		, q.FeedProdID											[StreamDescription]
			
		, CASE q.FeedProdID
			WHEN 'Hydrogen'			THEN 'H2'
			WHEN 'FuelGasSales'		THEN 'CH4'
			END													[ComponentId]
			
		, CASE q.FeedProdID
			WHEN 'Hydrogen'			THEN calc.ConvMoleWeight('H2', q.MolePcnt, 'W')
			WHEN 'FuelGasSales'		THEN calc.ConvMoleWeight('CH4', q.MolePcnt, 'W')
			END													[WtPcnt]

	FROM stgFact.ProdQuality q
	INNER JOIN stgFact.TSort t
		ON	t.Refnum = q.Refnum
	INNER JOIN stgFact.Quantity v
		ON	v.Refnum = q.Refnum
		AND v.FeedProdID = q.FeedProdID
		AND (ISNULL(v.Q1Feed, 0.0) + ISNULL(v.Q2Feed, 0.0) + ISNULL(v.Q3Feed, 0.0) + ISNULL(v.Q4Feed, 0.0)) > 0.0
	WHERE q.MolePcnt > 0.0
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	-- Table 3 (ProdQuality)
	PRINT NCHAR(9) + 'INSERT INTO fact.CompositionQuantity (T3 ProdQuality)';

	INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId,  Component_WtPcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.StreamId)								[StreamId]
		, u.StreamId												[StreamDescription]
		, u.ComponentId											[ComponentId]
		, u.WtPcnt													[WtPcnt]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			
			, CASE q.FeedProdID
				WHEN 'PPCFuel_CH4'		THEN 'PPCFuel'
				WHEN 'PPCFuel_ETH'		THEN 'PPCFuel'
				--WHEN 'PPCFuel_H2'		THEN 'PPCFuel'
				WHEN 'PPCFuel_Inerts'	THEN 'PPCFuel'
				WHEN 'PPCFuel_Other'	THEN 'PPCFuel'
				ELSE q.FeedProdID
				END												[StreamId]
				
			, CASE q.FeedProdID
				WHEN 'Ethylene'			THEN 'C2H4'
				WHEN 'EthyleneCG'		THEN 'C2H4'
				WHEN 'PPCFuel'			THEN 'H2'
				WHEN 'PPCFuel_CH4'		THEN 'CH4'
				WHEN 'PPCFuel_ETH'		THEN 'C2H4'
				--WHEN 'PPCFuel_H2'		THEN 'H2'
				WHEN 'PPCFuel_Inerts'	THEN 'Inerts'
				WHEN 'PPCFuel_Other'	THEN 'Other'
				WHEN 'PropaneC3'		THEN 'C3H8'
				WHEN 'Propylene'		THEN 'C3H6'
				WHEN 'PropyleneCG'		THEN 'C3H6'
				WHEN 'PropyleneRG'		THEN 'C3H6'
				END												[ComponentId]
				
			, ISNULL(q.WtPcnt, q.H2Content) * 
				CASE WHEN ISNULL(q.WtPcnt, q.H2Content) <= 1.0
					THEN 100.0
					ELSE 1.0
					END											[WtPcnt]
			
		FROM stgFact.ProdQuality q
		INNER JOIN stgFact.TSort t
			ON	t.Refnum = q.Refnum
		INNER JOIN stgFact.Quantity v
			ON	v.Refnum = q.Refnum
			AND v.FeedProdID = CASE q.FeedProdID
								WHEN 'PPCFuel_CH4'		THEN 'PPCFuel'
								WHEN 'PPCFuel_ETH'		THEN 'PPCFuel'
								--WHEN 'PPCFuel_H2'		THEN 'PPCFuel'
								WHEN 'PPCFuel_Other'	THEN 'PPCFuel'
								WHEN 'PPCFuel_Inerts'	THEN 'PPCFuel'
								ELSE q.FeedProdID
								END	
			AND (ISNULL(v.AnnFeedProd, 0.0) + ISNULL(v.Q1Feed, 0.0) + ISNULL(v.Q2Feed, 0.0) + ISNULL(v.Q3Feed, 0.0) + ISNULL(v.Q4Feed, 0.0)) <> 0.0

		WHERE (q.WtPcnt > 0.0 OR q.H2Content > 0.0)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		)u;

	PRINT NCHAR(9) + 'INSERT INTO fact.FeedStockDistillation';

	INSERT INTO fact.FeedStockDistillation(Refnum, CalDateKey, StreamId, StreamDescription, DistillationID, Temp_C)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvStreamID(u.FeedProdID)							[StreamId]
		, u.[StreamDescription]
		, u.DistillationID
		, u.Temp_C
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			
			, q.FeedProdID
			, etl.ConvStreamDescription(q.FeedProdID, q.OthLiqFeedDESC, NULL, NULL, NULL) [StreamDescription]
			
			, q.IBP			[D000]
			, q.D5pcnt		[D005]
			, q.D10Pcnt		[D010]
			, q.D30Pcnt		[D030]
			, q.D50Pcnt		[D050]
			, q.D70Pcnt		[D070]
			, q.D90Pcnt		[D090]
			, q.D95Pcnt		[D095]
			, q.EP			[D100]
			
		FROM stgFact.FeedQuality q
		INNER JOIN stgFact.TSort t
			ON	t.Refnum = q.Refnum
		INNER JOIN stgFact.Quantity z
			ON	z.Refnum = q.Refnum
			AND	z.FeedProdID = q.FeedProdID
			AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
		WHERE	(q.IBP		> 0.0  
			OR	q.D5pcnt	> 0.0  
			OR	q.D10Pcnt	> 0.0  
			OR	q.D30Pcnt	> 0.0  
			OR	q.D50Pcnt	> 0.0  
			OR	q.D70Pcnt	> 0.0  
			OR	q.D90Pcnt	> 0.0  
			OR	q.D95Pcnt	> 0.0  
			OR	q.EP		> 0.0)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT(
		Temp_C FOR DistillationID IN(
			  [D000]
			, [D005]
			, [D010]
			, [D030]
			, [D050]
			, [D070]
			, [D090]
			, [D095]
			, [D100]
			)
		) u
	WHERE u.Temp_C > 0.0;

	PRINT NCHAR(9) + 'INSERT INTO fact.ROGEntryPoint';

	INSERT INTO fact.ROGEntryPoint(Refnum, CalDateKey, CompGasInlet_Bit, CompGasDischarge_Bit, C2Recovery_Bit, C3Recovery_Bit)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvBit(m.ROG_CGCI)
		, etl.ConvBit(m.ROG_CGCD)
		, etl.ConvBit(m.ROG_C2RSF)
		, etl.ConvBit(m.ROG_C3RSF)	
	FROM stgFact.Misc m
	INNER JOIN stgFact.TSort t ON	t.Refnum = m.Refnum
	WHERE	(m.ROG_CGCI IS NOT NULL
		OR	m.ROG_CGCD IS NOT NULL
		OR	m.ROG_C2RSF IS NOT NULL
		OR	m.ROG_C3RSF IS NOT NULL)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum

	PRINT NCHAR(9) + 'INSERT INTO fact.FeedStockCrackingParameters';

	INSERT INTO fact.FeedStockCrackingParameters(Refnum, CalDateKey, StreamId, StreamDescription
		, CoilOutletPressure_Psia
		, CoilOutletTemp_C
		, SteamHydrocarbon_Ratio
		, EthyleneYield_WtPcnt
		, PropyleneEthylene_Ratio
		, PropyleneMethane_Ratio
		, FeedConv_WtPcnt
		, Density_SG
		--, Sulfur_ppm
		, DistMethodID
		, FurnConstruction_Year
		, ResidenceTime_s
		, Recycle_Bit)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		
		, etl.ConvStreamID(q.FeedProdID)						[StreamId]
		, etl.ConvStreamDescription(q.FeedProdID, q.OthLiqFeedDESC, NULL, NULL, NULL)
																[StreamDescription]
		, q.CoilOutletPressure
		, q.CoilOutletTemp
		, q.SteamHCRatio
		, q.EthyleneYield * CASE WHEN q.EthyleneYield < 1.0 THEN 100.0 ELSE 1.0 END		[EthyleneYield]
		, q.PropylEthylRatio
		, q.PropylMethaneRatio
		, q.FeedConv			* 100.0							[FeedConv]
		, q.SG
		--, q.SulfurPPM
		, etl.ConvDistillationMethod(q.ASTMethod)				[DistMethodID]
		, q.ConstrYear
		, q.ResTime
		, CASE WHEN etl.ConvStreamID(q.FeedProdID) IN ('EthRec', 'ProRec', 'ButRec') THEN 1 ELSE 0 END	[Recycle_Bit]
		
	FROM stgFact.FeedQuality q
	INNER JOIN stgFact.TSort t
		ON t.Refnum = q.Refnum
	INNER JOIN stgFact.Quantity z
		ON	z.Refnum = q.Refnum
		AND	z.FeedProdID = q.FeedProdID
		AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
	WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

END;