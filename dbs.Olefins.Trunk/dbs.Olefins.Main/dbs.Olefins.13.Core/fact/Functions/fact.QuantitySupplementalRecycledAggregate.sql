﻿
CREATE FUNCTION [fact].[QuantitySupplementalRecycledAggregate]
(
	@Refnum					VARCHAR (18),
	@PartialAggregate		TINYINT
)
RETURNS @ReturnTable TABLE 
(
	[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
	[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
	[StreamId]				VARCHAR (42)	NOT	NULL	CHECK(LEN([StreamId]) > 0),
	[SuppRecycled_kMT]		FLOAT			NOT	NULL	CHECK([SuppRecycled_kMT] >= 0.0),

	PRIMARY KEY CLUSTERED ([Refnum] DESC, [CalDateKey] DESC, [StreamId] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	INSERT INTO @ReturnTable(Refnum, CalDateKey, StreamId, SuppRecycled_kMT)
	SELECT 
		sr.Refnum,
		sr.CalDateKey,
		CASE calc.GeometricPrevious(sr.RecycleId, @PartialAggregate)
			WHEN 0 THEN 'NonRec'
			WHEN 1 THEN 'EthRec'
			WHEN 2 THEN 'ProRec'
			WHEN 4 THEN 'ButRec'
		END				[StreamId],
		SUM(sr.SuppRecycled_kMT)				[SuppRecycled_kMT]
	FROM	fact.QuantitySupplementalRecycled	sr
	WHERE sr.Refnum = @Refnum
	GROUP BY
		sr.Refnum,
		sr.CalDateKey,
		CASE calc.GeometricPrevious(sr.RecycleId, @PartialAggregate)
			WHEN 0 THEN 'NonRec'
			WHEN 1 THEN 'EthRec'
			WHEN 2 THEN 'ProRec'
			WHEN 4 THEN 'ButRec'
		END
		
	RETURN;

END;