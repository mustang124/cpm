﻿CREATE TABLE [fact].[EnergyPrice] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyPrice_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EnergyPrice_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EnergyPrice_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EnergyPrice_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyPrice] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyPrice_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_EnergyPrice_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_EnergyPrice_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyPrice_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_EnergyPrice_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergyPrice_u]
	ON [fact].[EnergyPrice]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyPrice]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyPrice].Refnum			= INSERTED.Refnum
		AND [fact].[EnergyPrice].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[EnergyPrice].AccountId		= INSERTED.AccountId
		AND [fact].[EnergyPrice].CalDateKey		= INSERTED.CalDateKey;

END;