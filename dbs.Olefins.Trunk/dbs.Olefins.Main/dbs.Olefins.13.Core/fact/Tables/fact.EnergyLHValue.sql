﻿CREATE TABLE [fact].[EnergyLHValue] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [LHValue_MBtu]   REAL               NOT NULL,
    [_LHValue_GJ]    AS                 (CONVERT([real],[LHValue_MBtu]*(1.05499994754791),(1))) PERSISTED NOT NULL,
    [_LHValue_BTU]   AS                 (CONVERT([real],[LHValue_MBtu]/(1000000.0),(1))) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyLHValue_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EnergyLHValue_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EnergyLHValue_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EnergyLHValue_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyLHValue] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyLHValue_LHValue_MBtu] CHECK ([LHValue_MBtu]>=(0.0)),
    CONSTRAINT [FK_EnergyLHValue_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_EnergyLHValue_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyLHValue_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_EnergyLHValue_PlantPricing_PPFC]
    ON [fact].[EnergyLHValue]([AccountId] ASC, [LHValue_MBtu] ASC)
    INCLUDE([Refnum], [CalDateKey]);


GO

CREATE TRIGGER [fact].[t_EnergyLHValue_u]
	ON [fact].[EnergyLHValue]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyLHValue]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyLHValue].Refnum		= INSERTED.Refnum
		AND [fact].[EnergyLHValue].AccountId	= INSERTED.AccountId
		AND [fact].[EnergyLHValue].CalDateKey	= INSERTED.CalDateKey;

END;