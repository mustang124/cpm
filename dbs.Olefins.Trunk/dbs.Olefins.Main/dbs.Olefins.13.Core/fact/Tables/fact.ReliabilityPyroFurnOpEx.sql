﻿CREATE TABLE [fact].[ReliabilityPyroFurnOpEx] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [FurnID]         INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityPyroFurnOpEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnOpEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnOpEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnOpEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityPyroFurnOpEx] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FurnID] ASC, [AccountId] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ReliabilityPyroFurnOpEx_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurnOpEx_FurnID] CHECK ([FurnID]>=(0)),
    CONSTRAINT [FK_ReliabilityPyroFurnOpEx_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_ReliabilityPyroFurnOpEx_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityPyroFurnOpEx_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_ReliabilityPyroFurnOpEx_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_ReliabilityPyroFurnOpEx_Calc_Furnaces]
    ON [fact].[ReliabilityPyroFurnOpEx]([AccountId] ASC)
    INCLUDE([Refnum], [CalDateKey], [FurnID], [CurrencyRpt], [Amount_Cur]);


GO

CREATE TRIGGER [fact].[t_ReliabilityPyroFurnOpEx_u]
	ON [fact].[ReliabilityPyroFurnOpEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityPyroFurnOpEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityPyroFurnOpEx].Refnum			= INSERTED.Refnum
		AND [fact].[ReliabilityPyroFurnOpEx].FurnID			= INSERTED.FurnID
		AND [fact].[ReliabilityPyroFurnOpEx].AccountId		= INSERTED.AccountId
		AND [fact].[ReliabilityPyroFurnOpEx].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[ReliabilityPyroFurnOpEx].CalDateKey		= INSERTED.CalDateKey;

END;