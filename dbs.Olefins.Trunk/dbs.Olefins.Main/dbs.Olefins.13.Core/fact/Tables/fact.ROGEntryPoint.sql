﻿CREATE TABLE [fact].[ROGEntryPoint] (
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [CompGasInlet_Bit]     BIT                NULL,
    [CompGasDischarge_Bit] BIT                NULL,
    [C2Recovery_Bit]       BIT                NULL,
    [C3Recovery_Bit]       BIT                NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_ROGEntryPoint_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_ROGEntryPoint_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_ROGEntryPoint_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_ROGEntryPoint_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ROGEntryPoint] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ROGEntryPoint_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ROGEntryPoint_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ROGEntryPoint_u]
	ON [fact].[ROGEntryPoint]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ROGEntryPoint]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ROGEntryPoint].Refnum		= INSERTED.Refnum
		AND [fact].[ROGEntryPoint].CalDateKey	= INSERTED.CalDateKey;

END;