﻿CREATE TABLE [fact].[GenPlantExpansion] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [AccountId]         VARCHAR (42)       NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [CapacityAdded_kMT] REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantExpansion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_GenPlantExpansion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_GenPlantExpansion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_GenPlantExpansion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantExpansion] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantExpansion_CapacityAdded_kMT] CHECK ([CapacityAdded_kMT]>=(0.0)),
    CONSTRAINT [FK_GenPlantExpansion_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_GenPlantExpansion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantExpansion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_GenPlantExpansion_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantExpansion_u]
	ON [fact].[GenPlantExpansion]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantExpansion]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantExpansion].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlantExpansion].StreamId		= INSERTED.StreamId
		AND [fact].[GenPlantExpansion].AccountId	= INSERTED.AccountId
		AND [fact].[GenPlantExpansion].CalDateKey	= INSERTED.CalDateKey;

END;