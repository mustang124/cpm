﻿CREATE TABLE [fact].[EnergyElec] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_MWh]     REAL               NOT NULL,
    [Rate_BTUkWh]    REAL               NOT NULL,
    [_LHValue_MBtu]  AS                 (CONVERT([real],[Amount_MWh]*[Rate_BTUkWh],(1))) PERSISTED NOT NULL,
    [_LHValue_GJ]    AS                 (CONVERT([real],([Amount_MWh]*[Rate_BTUkWh])*(1.05499994754791),(1))) PERSISTED NOT NULL,
    [Price_Cur]      REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyElec_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EnergyElec_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EnergyElec_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EnergyElec_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyElec] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyElec_Amount_MWh] CHECK ([Amount_MWh]>=(0.0)),
    CONSTRAINT [CR_EnergyElec_Price_cur] CHECK ([Price_Cur]>=(0.0)),
    CONSTRAINT [CR_EnergyElec_Rate_BTUkWh] CHECK ([Rate_BTUkWh]>=(0.0)),
    CONSTRAINT [FK_EnergyElec_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_EnergyElec_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyElec_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_EnergyElec_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergyElec_u]
	ON [fact].[EnergyElec]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyElec]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyElec].Refnum		= INSERTED.Refnum
		AND [fact].[EnergyElec].AccountId	= INSERTED.AccountId
		AND [fact].[EnergyElec].CalDateKey	= INSERTED.CalDateKey;

END;