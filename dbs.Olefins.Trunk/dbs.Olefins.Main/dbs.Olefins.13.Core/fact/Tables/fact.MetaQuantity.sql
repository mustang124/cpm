﻿CREATE TABLE [fact].[MetaQuantity] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [Quantity_kMT]   REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MetaQuantity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MetaQuantity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MetaQuantity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MetaQuantity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MetaQuantity] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_MetaQuantity_Quantity_kMT] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [FK_MetaQuantity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MetaQuantity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_MetaQuantity_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_MetaQuantity_u]
	ON [fact].[MetaQuantity]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[MetaQuantity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[MetaQuantity].Refnum		= INSERTED.Refnum
		AND [fact].[MetaQuantity].StreamId	= INSERTED.StreamId
		AND [fact].[MetaQuantity].CalDateKey	= INSERTED.CalDateKey;

END;