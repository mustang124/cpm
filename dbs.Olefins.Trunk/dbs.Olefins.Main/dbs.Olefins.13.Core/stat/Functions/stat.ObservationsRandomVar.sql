﻿
CREATE FUNCTION [stat].[ObservationsRandomVar]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics
	
	@Population			stat.PopulationRandomVar		READONLY,							--	Data points to examine
	@SortOrder			SMALLINT			= 1,											--	Sort order of data 1 (Ascending), -1 Descending
	@Tiles				TINYINT				= NULL,											--	Number of nTiles

	@Confidence_Pcnt	FLOAT				= 90.0,											--	Prediction/confidence interval level
	@PearsonThreshold	FLOAT				= 0.90,											--	Pearson threshold					[0.0, 1.0]
	@ResidualThreshold	FLOAT				= 2.00											--	Residual identification threshold
)
RETURNS @ReturnTable TABLE 
(
	[FactorSetId]		VARCHAR (12)		NOT	NULL	CHECK([FactorSetId]	<> ''),
	[Refnum]			VARCHAR (25)		NOT	NULL	CHECK([Refnum]		<> ''),
	[x]					FLOAT				NOT	NULL,										--	Raw value

	[iPcntTile]			FLOAT				NOT	NULL	CHECK(ROUND([iPcntTile], 5) BETWEEN 0.0 AND 100.0),	--	Item-tile			http://en.wikipedia.org/wiki/Percentile
	[zPcntTile]			FLOAT				NOT	NULL	CHECK(ROUND([zPcntTile], 5) BETWEEN 0.0 AND 100.0),	--	Percentile			http://en.wikipedia.org/wiki/Percentile
	[xQuartile]			TINYINT				NOT	NULL	CHECK([xQuartile] >= 1),			--	Quartile value						http://en.wikipedia.org/wiki/Quantile

	[xRow]				SMALLINT			NOT	NULL	CHECK([xRow]		>= 1),			--	Row Number							http://msdn.microsoft.com/en-us/library/ms186734.aspx
	[xRank]				SMALLINT			NOT	NULL	CHECK([xRank]		>= 1),			--	Value Ranking						http://msdn.microsoft.com/en-us/library/ms176102.aspx
	[xRankDense]		SMALLINT			NOT	NULL	CHECK([xRankDense]	>= 1),			--	Value Ranking (Dense)				http://msdn.microsoft.com/en-us/library/ms173825.aspx

	[zScore]			FLOAT				NOT	NULL,										--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
	[zQuartile]			TINYINT					NULL	CHECK([zQuartile] >= 1),			--	Quartile for Z-Test
	[zHypothesis]		BIT					NOT	NULL,										--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

	[ErrorAbsolute]		FLOAT				NOT	NULL,										--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
	[ErrorRelative]		FLOAT					NULL,										--	Realitve approximation error		http://en.wikipedia.org/wiki/Approximation_error

	[ResidualStandard]	FLOAT					NULL,										--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
	[ResidualAbsolute]	FLOAT				NOT	NULL,
	[ResidualRelative]	FLOAT				NOT	NULL,

	[ResidualThreshold]	FLOAT				NOT	NULL	CHECK([ResidualThreshold] > 0.0) ,
	[_ResidualStdIn]	AS CAST(CASE WHEN ABS([ResidualStandard]) <  [ResidualThreshold] THEN [ResidualStandard] END	AS FLOAT)
						PERSISTED				,
	[_ResidualStdOut]	AS CAST(CASE WHEN ABS([ResidualStandard]) >= [ResidualThreshold] THEN [ResidualStandard] END	AS FLOAT)
						PERSISTED				,

	[xMean]				FLOAT				NOT	NULL,										--	Mean								http://en.wikipedia.org/wiki/Mean
	
	[iConfidence]		FLOAT					NULL	CHECK([iConfidence]	>= 0.0),		--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[iPrediction]		FLOAT					NULL	CHECK([iPrediction]	>= 0.0),		--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval
	[_iPredictionLower]	AS CAST([xMean] - [iPrediction] AS FLOAT)							--	Lower Prediction Limit
						PERSISTED					,
	[_iConfidenceLower]	AS CAST([xMean] - [iConfidence] AS FLOAT)							--	Lower Confidence Limit
						PERSISTED					,
	[_iConfidenceUpper]	AS CAST([xMean] + [iConfidence] AS FLOAT)							--	Upper Confidence Limit
						PERSISTED					,
	[_iPredictionUpper]	AS CAST([xMean] + [iPrediction] AS FLOAT)							--	Upper Prediction Limit
						PERSISTED					,

	[Basis]				BIT					NOT	NULL,
	[_BasisQ1]			AS CAST(CASE WHEN [Basis] = 1 AND [zQuartile] = 1 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Green
	[_BasisQ2]			AS CAST(CASE WHEN [Basis] = 1 AND [zQuartile] = 2 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Blue
	[_BasisQ3]			AS CAST(CASE WHEN [Basis] = 1 AND [zQuartile] = 3 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Yellow
	[_BasisQ4]			AS CAST(CASE WHEN [Basis] = 1 AND [zQuartile] = 4 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Red

	[Audit]				BIT					NOT	NULL,
	[_AuditQ1]			AS CAST(CASE WHEN [Audit] = 1 AND [zQuartile] = 1 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Green
	[_AuditQ2]			AS CAST(CASE WHEN [Audit] = 1 AND [zQuartile] = 2 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Blue
	[_AuditQ3]			AS CAST(CASE WHEN [Audit] = 1 AND [zQuartile] = 3 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Yellow
	[_AuditQ4]			AS CAST(CASE WHEN [Audit] = 1 AND [zQuartile] = 4 THEN [x] END	AS FLOAT	)
						PERSISTED					,	--	Red

	[PearsonThreshold]	FLOAT				NOT	NULL	CHECK(ROUND([PearsonThreshold], 5) BETWEEN 0.0 AND 1.0),

	[_OutPearson]		AS CAST(CASE WHEN [Audit] = 1 AND ([ResidualAbsolute] >= [PearsonThreshold] OR [ResidualRelative] >= [PearsonThreshold]) THEN [x] END	AS FLOAT)
						PERSISTED					,	--	Ring (Black)
	[_OutPrediction]	AS CAST(CASE WHEN [Audit] = 1 AND ([x] NOT BETWEEN [xMean] - [iPrediction] AND [xMean] + [iPrediction]) THEN [x] END AS FLOAT)
						PERSISTED					,	--	Ring (Red)

	CHECK ([iConfidence] <= [iPrediction]),
	PRIMARY KEY CLUSTERED ([x] ASC, [FactorSetId] ASC, [Refnum] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Order			FLOAT	= CAST(ABS(@SortOrder) / @SortOrder AS FLOAT);
	DECLARE @nTiles			TINYINT	= COALESCE(@Tiles, 4);

	DECLARE	@xCount			FLOAT;
	DECLARE @xMin			FLOAT;
	DECLARE @xSum			FLOAT;
	DECLARE @xMean			FLOAT;
	DECLARE @xStDevP		FLOAT;
	DECLARE @SumXiX			FLOAT;

	DECLARE @tStat			FLOAT;
	DECLARE @iConfidence	FLOAT;
	DECLARE @iPrediction	FLOAT;

	SELECT
		@xCount		= COUNT(1),
		@xMin		= MIN(o.x),
		@xSum		= SUM(o.x),
		@xMean		= AVG(o.x),
		@xStDevP	= STDEVP(o.x)
	FROM @Population o
	WHERE o.Basis = 1;

	SELECT	@SumXiX = SUM(SQUARE(o.x - @xMean))
	FROM @Population o
	WHERE o.Basis = 1;

	DECLARE @tAlpha			FLOAT = 50.0 + @Confidence_Pcnt / 2.0;
	
	IF (@xCount >= 1)
	BEGIN
		SET @tStat			= stat.Student_Statistic(@xCount - 1.0, @tAlpha, 2);
		SET @iConfidence	= @tStat * @xStDevP / SQRT(@xCount);
		SET @iPrediction	= @tStat * @xStDevP * SQRT(1.0 + 1.0/@xCount);
	END;

	INSERT INTO @ReturnTable(FactorSetId, Refnum, x,
		iPcntTile, zPcntTile, xQuartile,
		xRow, xRank, xRankDense,
		zScore, zHypothesis,
		ErrorAbsolute, ErrorRelative,
		ResidualStandard, ResidualAbsolute, ResidualRelative, ResidualThreshold,
		xMean,
		iConfidence, iPrediction,
		PearsonThreshold,
		Basis, Audit)
	SELECT
		o.FactorSetId,
		o.Refnum,
		o.x,

		(ROW_NUMBER()	OVER(ORDER BY o.x * @Order ASC, o.Refnum ASC) - 0.5) / @xCount * 100.0			[iPcntTile],
		[stat].[Student_Probability](@xCount, CASE WHEN @xStDevP <> 0.0 THEN (o.x - @xMean) / @xStDevP ELSE 0.0 END, 1)
																										[zPcntTile],
		NTILE(@nTiles)	OVER(ORDER BY o.x * @Order ASC, o.Refnum ASC)									[xQuartile],

		ROW_NUMBER()	OVER(ORDER BY o.x * @Order ASC, o.Refnum ASC)									[xRow],
		RANK()			OVER(ORDER BY o.x * @Order ASC, o.Refnum ASC)									[xRank],
		DENSE_RANK()	OVER(ORDER BY o.x * @Order ASC, o.Refnum ASC)									[xRankDense],

		CASE WHEN @xStDevP <> 0.0 THEN (o.x - @xMean) / @xStDevP ELSE 0.0 END							[zScore],
		CASE WHEN CASE WHEN @xStDevP <> 0.0 THEN ABS((o.x - @xMean) / @xStDevP) ELSE 0.0 END < @tStat
			THEN 1
			ELSE 0
			END																							[zHypothesis],

		(o.x -	@xMean)																					[ErrorAbsolute],
		CASE WHEN @xMean <> 0.0 THEN (o.x -	@xMean) / @xMean END										[ErrorRelative],

		CASE WHEN @SumXiX <> 0.0 AND SQRT(SQUARE(@xStDevP) * (1.0 - 1.0/@xCount - SQUARE(o.x - @xMean) / @SumXiX)) <> 0.0
			THEN
				(o.x - @xMean) /
				SQRT(SQUARE(@xStDevP) * (1.0 - 1.0/@xCount - SQUARE(o.x - @xMean) / @SumXiX))
			END																							[ResidualStandard],

		(SELECT COUNT(1) FROM @Population b
			WHERE	b.Basis = 1
				AND SQUARE(b.x - @xMean)
				<=	SQUARE(o.x - @xMean)
			)	/	@xCount																				[ResidualAbsolute],

		(SELECT COUNT(1) FROM @Population b
			WHERE	b.Basis = 1
				AND CASE WHEN b.x <> 0.0 THEN SQUARE((b.x - @xMean) / b.x) ELSE 1.0 END
				<=	CASE WHEN o.x <> 0.0 THEN SQUARE((o.x - @xMean) / o.x) ELSE 1.0 END
			)	 /	@xCount																				[ResidualRelative],
		
		@ResidualThreshold,

		@xMean,

		@iConfidence																					[iConfidence],
		@iPrediction																					[iPrediction],

		@PearsonThreshold,
		o.Basis,
		o.Audit

	FROM @Population o
	WHERE o.Basis = 1;

	--------------------------------------------------------------------------------

	IF (@Tiles IS NULL)
	BEGIN

		DECLARE @MinCompanies	TINYINT = 2;
		DECLARE @MinItems		TINYINT = 4;

		DECLARE @Criteria TABLE
		(
			[Refnum]			VARCHAR (18)		NOT	NULL	CHECK([Refnum]		<> ''),
			[CompanyId]			VARCHAR (42)		NOT	NULL	CHECK([CompanyId]	<> ''),
			PRIMARY KEY CLUSTERED ([Refnum] ASC)
		)

		/*	Slow due to selecting from fact.TSort (view)
		*/
		--INSERT INTO @Criteria(Refnum, CompanyId)
		--SELECT o.Refnum, t.CompanyId
		--FROM @Population	o
		--INNER JOIN  fact.TSort	t
		--	ON	t.Refnum = o.Refnum
		--WHERE o.Basis = 1;

		WHILE @nTiles > 1 AND EXISTS
		(
			SELECT
				r.xQuartile
			FROM @ReturnTable		r
			INNER JOIN @Criteria	c
				ON	c.Refnum = r.Refnum
			GROUP BY
				r.xQuartile
			HAVING	COUNT(1) < @MinItems
				OR	COUNT(DISTINCT c.CompanyId) < @MinCompanies
		)
		BEGIN
	
			SET @nTiles = @nTiles - 1;

			UPDATE	r
			SET	r.xQuartile = t.xQuartile
			FROM @ReturnTable	r
			INNER JOIN (
				SELECT
					r.FactorSetId,
					r.Refnum,
					r.x,
					NTILE(@nTiles)	OVER(ORDER BY r.x * @Order ASC, r.Refnum ASC)	[xQuartile]
				FROM @ReturnTable r
				) t
				ON	t.FactorSetId = r.FactorSetId
				AND	t.Refnum = r.Refnum
				AND	t.x = r.x;

		END;
	
	END;

	--------------------------------------------------------------------------------

	DECLARE @zQuartile		TINYINT = 4;

	UPDATE	r
	SET	r.zQuartile = t.zQuartile
	FROM @ReturnTable	r
	INNER JOIN (
		SELECT
			r.FactorSetId,
			r.Refnum,
			r.x,
			NTILE(@zQuartile) OVER(ORDER BY ABS(r.zScore) ASC)	[zQuartile]
		FROM @ReturnTable r
		) t
		ON	t.FactorSetId = r.FactorSetId
		AND	t.Refnum = r.Refnum
		AND	t.x = r.x;

	RETURN;

END;
