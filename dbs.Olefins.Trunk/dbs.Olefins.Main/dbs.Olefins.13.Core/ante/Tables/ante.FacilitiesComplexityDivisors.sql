﻿CREATE TABLE [ante].[FacilitiesComplexityDivisors] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [Complexity_Div] REAL               NULL,
    [Replacement_Wt] REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesComplexityDivisors_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FacilitiesComplexityDivisors_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FacilitiesComplexityDivisors_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FacilitiesComplexityDivisors_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FacilitiesComplexityDivisors] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [FacilityId] ASC),
    CONSTRAINT [FK_FacilitiesComplexityDivisors_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesComplexityDivisors_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER ante.t_FacilitiesComplexityDivisors_u
	ON [ante].[FacilitiesComplexityDivisors]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [ante].[FacilitiesComplexityDivisors]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[FacilitiesComplexityDivisors].FactorSetId		= INSERTED.FactorSetId
		AND	[ante].[FacilitiesComplexityDivisors].FacilityId		= INSERTED.FacilityId;

END;