﻿CREATE TABLE [ante].[PricingAdjDutiesStreamCountry] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CountryId]      CHAR (3)           NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PricingAdjDutiesStreamCountry_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PricingAdjDutiesStreamCountry_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PricingAdjDutiesStreamCountry_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PricingAdjDutiesStreamCountry_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingAdjDutiesStreamCountry] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [CountryId] ASC, [StreamId] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_PricingAdjDutiesStreamCountry_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_PricingAdjDutiesStreamCountry_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingAdjDutiesStreamCountry_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingAdjDutiesStreamCountry_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingAdjDutiesStreamCountry_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingAdjDutiesStreamCountry_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER ante.t_PricingAdjDutiesStreamCountry_u
	ON [ante].[PricingAdjDutiesStreamCountry]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingAdjDutiesStreamCountry]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingAdjDutiesStreamCountry].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[PricingAdjDutiesStreamCountry].CountryId	= INSERTED.CountryId
		AND	[ante].[PricingAdjDutiesStreamCountry].CalDateKey	= INSERTED.CalDateKey
		AND	[ante].[PricingAdjDutiesStreamCountry].StreamId		= INSERTED.StreamId
		AND	[ante].[PricingAdjDutiesStreamCountry].CurrencyRpt	= INSERTED.CurrencyRpt
		AND	[ante].[PricingAdjDutiesStreamCountry].AccountId	= INSERTED.AccountId;

END;