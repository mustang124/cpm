﻿CREATE TABLE [ante].[CountryEconRegion] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CountryId]      CHAR (3)           NOT NULL,
    [EconRegionId]   VARCHAR (5)        NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_CountryEconRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_CountryEconRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_CountryEconRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_CountryEconRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_CountryEconRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CountryId] ASC),
    CONSTRAINT [FK_CountryEconRegion_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_CountryEconRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_CountryEconRegion_Region_LookUp] FOREIGN KEY ([EconRegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId])
);


GO

CREATE TRIGGER [ante].[t_CountryEconRegion_u]
	ON [ante].[CountryEconRegion]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[CountryEconRegion]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[CountryEconRegion].FactorSetId		= INSERTED.FactorSetId
		AND	[ante].[CountryEconRegion].CountryId		= INSERTED.CountryId;

END;