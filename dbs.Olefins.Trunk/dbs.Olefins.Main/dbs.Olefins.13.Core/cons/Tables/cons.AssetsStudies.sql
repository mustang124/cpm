﻿CREATE TABLE [cons].[AssetsStudies] (
    [StudyId]        VARCHAR (4)        NOT NULL,
    [AssetId]        VARCHAR (30)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_AssetsStudies_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_AssetsStudies_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_AssetsStudies_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_AssetsStudies_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_AssetsStudies] PRIMARY KEY CLUSTERED ([StudyId] ASC, [AssetId] ASC),
    CONSTRAINT [FK_AssetsStudies_Study_LookUp] FOREIGN KEY ([StudyId]) REFERENCES [dim].[Study_LookUp] ([StudyId])
);


GO

CREATE TRIGGER [cons].[t_AssetsStudies_u]
	ON [cons].[AssetsStudies]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[AssetsStudies]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[AssetsStudies].StudyId	= INSERTED.StudyId
		AND	[cons].[AssetsStudies].AssetId	= INSERTED.AssetId;

END;