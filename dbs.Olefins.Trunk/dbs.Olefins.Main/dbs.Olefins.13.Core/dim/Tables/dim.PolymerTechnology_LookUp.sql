﻿CREATE TABLE [dim].[PolymerTechnology_LookUp] (
    [PolymerTechnologyId]     VARCHAR (42)       NOT NULL,
    [PolymerTechnologyName]   NVARCHAR (84)      NOT NULL,
    [PolymerTechnologyDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_PolymerTechnology_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_PolymerTechnology_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_PolymerTechnology_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_PolymerTechnology_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]            ROWVERSION         NOT NULL,
    CONSTRAINT [PK_PolymerTechnology_LookUp] PRIMARY KEY CLUSTERED ([PolymerTechnologyId] ASC),
    CONSTRAINT [CL_PolymerTechnology_LookUp_PolymerTechnologyDetail] CHECK ([PolymerTechnologyDetail]<>''),
    CONSTRAINT [CL_PolymerTechnology_LookUp_PolymerTechnologyId] CHECK ([PolymerTechnologyId]<>''),
    CONSTRAINT [CL_PolymerTechnology_LookUp_PolymerTechnologyName] CHECK ([PolymerTechnologyName]<>''),
    CONSTRAINT [UK_PolymerTechnology_LookUp_PolymerTechnologyDetail] UNIQUE NONCLUSTERED ([PolymerTechnologyDetail] ASC),
    CONSTRAINT [UK_PolymerTechnology_LookUp_PolymerTechnologyName] UNIQUE NONCLUSTERED ([PolymerTechnologyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_PolymerTechnology_LookUp_u]
	ON [dim].[PolymerTechnology_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[PolymerTechnology_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[PolymerTechnology_LookUp].[PolymerTechnologyId]		= INSERTED.[PolymerTechnologyId];
		
END;
