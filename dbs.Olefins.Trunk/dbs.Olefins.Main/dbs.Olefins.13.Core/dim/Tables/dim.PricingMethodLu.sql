﻿CREATE TABLE [dim].[PricingMethodLu] (
    [MethodID]       VARCHAR (42)        NOT NULL,
    [MethodName]     NVARCHAR (84)       NOT NULL,
    [MethodDetail]   NVARCHAR (256)      NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_PricingMethodLu_Operator] DEFAULT ('~') NOT NULL,
    [Parent]         VARCHAR (42)        NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_PricingMethodLu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)      CONSTRAINT [DF_PricingMethodLu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)      CONSTRAINT [DF_PricingMethodLu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)      CONSTRAINT [DF_PricingMethodLu_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingMethodLu] PRIMARY KEY CLUSTERED ([MethodID] ASC),
    CONSTRAINT [CL_PricingMethodLu_MethodDetail] CHECK (len([MethodDetail])>(0)),
    CONSTRAINT [CL_PricingMethodLu_MethodID] CHECK (len([MethodID])>(0)),
    CONSTRAINT [CL_PricingMethodLu_MethodName] CHECK (len([MethodName])>(0)),
    CONSTRAINT [CR_PricingMethodLu_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_PricingMethodLu_Parents] FOREIGN KEY ([Parent]) REFERENCES [dim].[PricingMethodLu] ([MethodID]),
    CONSTRAINT [UK_PricingMethodLu_MethodDetail] UNIQUE NONCLUSTERED ([MethodDetail] ASC),
    CONSTRAINT [UK_PricingMethodLu_MethodName] UNIQUE NONCLUSTERED ([MethodName] ASC)
);


GO

CREATE TRIGGER [dim].[t_PricingMethodLu_u]
	ON [dim].[PricingMethodLu]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[PricingMethodLu]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[PricingMethodLu].[MethodID]		= INSERTED.[MethodID];
		
END;