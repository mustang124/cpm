﻿CREATE TABLE [dim].[Country_LookUp] (
    [CountryId]      CHAR (3)           NOT NULL,
    [CountryNameEn]  VARCHAR (84)       NOT NULL,
    [CountryName]    NVARCHAR (84)      NOT NULL,
    [CountryDetail]  NVARCHAR (256)     NOT NULL,
    [Alpha2]         CHAR (2)           NOT NULL,
    [ISONumeric]     CHAR (3)           NULL,
    [IDD]            VARCHAR (5)        NULL,
    [Active]         BIT                CONSTRAINT [DF_Country_LookUp_Active] DEFAULT ((0)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Country_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Country_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Country_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Country_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Country_LookUp] PRIMARY KEY CLUSTERED ([CountryId] ASC),
    CONSTRAINT [CL_Country_LookUp_CountryDetail] CHECK ([CountryDetail]<>''),
    CONSTRAINT [CL_Country_LookUp_CountryID] CHECK ([CountryId]<>''),
    CONSTRAINT [CL_Country_LookUp_CountryName] CHECK ([CountryName]<>''),
    CONSTRAINT [CL_Country_LookUp_CountryNameEn] CHECK ([CountryNameEn]<>''),
    CONSTRAINT [CL_Country_LookUp_ISONumeric] CHECK (len([ISONumeric])=(3)),
    CONSTRAINT [UK_Country_LookUp_Alpha2] UNIQUE NONCLUSTERED ([Alpha2] ASC),
    CONSTRAINT [UK_Country_LookUp_CountryDetail] UNIQUE NONCLUSTERED ([CountryDetail] ASC),
    CONSTRAINT [UK_Country_LookUp_CountryName] UNIQUE NONCLUSTERED ([CountryName] ASC),
    CONSTRAINT [UK_Country_LookUp_CountryNameEn] UNIQUE NONCLUSTERED ([CountryNameEn] ASC)
);


GO

CREATE TRIGGER [dim].[t_Country_LookUp_u]
	ON [dim].[Country_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Country_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Country_LookUp].[CountryId]		= INSERTED.[CountryId];
		
END;
