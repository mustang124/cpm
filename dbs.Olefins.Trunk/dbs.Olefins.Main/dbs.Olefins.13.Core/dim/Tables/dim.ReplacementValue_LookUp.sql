﻿CREATE TABLE [dim].[ReplacementValue_LookUp] (
    [ReplacementValueId]     VARCHAR (42)       NOT NULL,
    [ReplacementValueName]   NVARCHAR (84)      NOT NULL,
    [ReplacementValueDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_ReplacementValue_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_ReplacementValue_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_ReplacementValue_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_ReplacementValue_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReplacementValue_LookUp] PRIMARY KEY CLUSTERED ([ReplacementValueId] ASC),
    CONSTRAINT [CL_ReplacementValue_LookUp_ReplacementValueDetail] CHECK ([ReplacementValueDetail]<>''),
    CONSTRAINT [CL_ReplacementValue_LookUp_ReplacementValueId] CHECK ([ReplacementValueId]<>''),
    CONSTRAINT [CL_ReplacementValue_LookUp_ReplacementValueName] CHECK ([ReplacementValueName]<>''),
    CONSTRAINT [UK_ReplacementValue_LookUp_ReplacementValueDetail] UNIQUE NONCLUSTERED ([ReplacementValueDetail] ASC),
    CONSTRAINT [UK_ReplacementValue_LookUp_ReplacementValueName] UNIQUE NONCLUSTERED ([ReplacementValueName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReplacementValue_LookUp_u]
	ON [dim].[ReplacementValue_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReplacementValue_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReplacementValue_LookUp].[ReplacementValueId]		= INSERTED.[ReplacementValueId];
		
END;
