﻿CREATE VIEW [calc].[EfficiencyAggregate]
WITH SCHEMABINDING
AS
SELECT
	t.FactorSetId,
	t.Refnum,
	t.CalDateKey,
	p.UnitId,
	t.EfficiencyId,
	SUM(t.EfficiencyStandard)		[EfficiencyStandard]
FROM (
	SELECT
		t.FactorSetId,
		t.Refnum,
		t.CalDateKey,
		t.UnitId,
		e.EfficiencyId,
		SUM(t.EfficiencyStandard)	[EfficiencyStandard]
	FROM calc.Efficiency					t
	INNER JOIN dim.Efficiency_Bridge		e
		ON	e.FactorSetId	= t.FactorSetId
		AND	e.DescendantId	= t.EfficiencyId
	GROUP BY
		t.FactorSetId,
		t.Refnum,
		t.CalDateKey,
		t.UnitId,
		e.EfficiencyId
	) t
INNER JOIN dim.ProcessUnits_Bridge		p
ON	p.FactorSetId	= t.FactorSetId
AND	p.DescendantId	= t.UnitId
GROUP BY
	t.FactorSetId,
	t.Refnum,
	t.CalDateKey,
	p.UnitId,
	t.EfficiencyId;