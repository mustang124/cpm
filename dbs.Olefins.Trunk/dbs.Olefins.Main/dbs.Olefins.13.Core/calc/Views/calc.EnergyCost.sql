﻿CREATE VIEW [calc].[EnergyCost]
WITH SCHEMABINDING
AS
SELECT
	o.FactorSetId,
	o.Refnum,
	o.CalDateKey,
	o.CurrencyRpt,
	o.AccountId,
	o.Amount_Cur / lhv.LHValue_MBtu * 1000.0	[UnitEnergyCost_CurMBtu]
FROM fact.EnergyLHValueAggregate		lhv
INNER JOIN calc.OpExAggregate			o
	ON	o.FactorSetId = lhv.FactorSetId
	AND	o.Refnum = lhv.Refnum
	AND	o.CalDateKey = lhv.CalDateKey
	AND	o.AccountId = lhv.AccountId
WHERE lhv.AccountId IN ('PurchasedEnergy', 'PPFC', 'EnergyConsumption', 'ExportCredits', 'EnergyBalance')
