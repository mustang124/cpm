﻿CREATE VIEW calc.StandardEnergyAggregate
WITH SCHEMABINDING
AS
SELECT
	e.FactorSetId,
	e.Refnum,
	e.CalDateKey,
	e.SimModelId,
	b.StandardEnergyId,
	CASE WHEN b.StandardEnergyId IN ('FeedLtOther', 'FeedLiqOther') THEN e.StandardEnergyDetail END [StandardEnergyDetail],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + e.StandardEnergy
		WHEN '-' THEN - e.StandardEnergy
		END)									[StandardEnergy]
FROM	dim.StandardEnergy_Bridge			b
INNER JOIN calc.StandardEnergy				e
	ON	e.FactorSetId		= b.FactorSetId
	AND	e.StandardEnergyId	= b.DescendantId
GROUP BY
	e.FactorSetId,
	e.Refnum,
	e.CalDateKey,
	e.SimModelId,
	b.StandardEnergyId,
	CASE WHEN b.StandardEnergyId IN ('FeedLtOther', 'FeedLiqOther') THEN e.StandardEnergyDetail END;