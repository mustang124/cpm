﻿CREATE PROCEDURE [calc].[CalculatePlant]
(
	@FactorSetId			VARCHAR(12) = NULL,
	@Refnum					VARCHAR(25) = NULL
)
AS
BEGIN

	PRINT '---------------------------------------------------------------------------';
	PRINT '';
	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'(CALCULATING) EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N') ' + COALESCE(@FactorSetId + ', ', '') + @Refnum
	PRINT @ProcedureDesc;
	PRINT '';

	SET NOCOUNT ON;

	BEGIN TRY

		EXECUTE [cons].[VerifyTSort]								@Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO fact.TSortClient (calc.CalculatePlant)';
		PRINT @ProcedureDesc;

		EXECUTE [fact].[Insert_TSortClient]							@Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO fact.TSortClient_DC (calc.CalculatePlant)';
		PRINT @ProcedureDesc;

		EXECUTE [fact].[Insert_TsortContact_DC]						@Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO fact.TSortClient_Pricing (calc.CalculatePlant)';
		PRINT @ProcedureDesc;

		EXECUTE [fact].[Insert_TsortContact_Pricing]				@Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO fact.TSortClient_Secondary (calc.CalculatePlant)';
		PRINT @ProcedureDesc;

		EXECUTE [fact].[Insert_TsortContact_Secondary]				@Refnum;
		
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @fpl (calc.CalculatePlant)';
		PRINT @ProcedureDesc;

		DECLARE @fpl	[calc].[FoundationPlantList];

		INSERT INTO @fpl
		(
			[FactorSetId],
			[FactorSetName],
			[FactorSet_AnnDateKey],
			[FactorSet_QtrDateKey],
			[FactorSet_QtrDate],
			[Refnum],
			[Plant_AnnDateKey],
			[Plant_QtrDateKey],
			[Plant_QtrDate],
			[CompanyId],
			[AssetId],
			[AssetName],
			[SubscriberCompanyName],
			[PlantCompanyName],
			[SubscriberAssetName],
			[PlantAssetName],
			[CountryId],
			[StateName],
			[EconRegionId],
			[UomId],
			[CurrencyFcn],
			[CurrencyRpt],
			[AssetPassWord_PlainText],
			[StudyYear],
			[DataYear],
			[Consultant],
			[StudyYearDifference],
			[CalQtr]
		)
		SELECT
			tsq.[FactorSetId],
			tsq.[FactorSetName],
			tsq.[FactorSet_AnnDateKey],
			tsq.[FactorSet_QtrDateKey],
			tsq.[FactorSet_QtrDate],
			tsq.[Refnum],
			tsq.[Plant_AnnDateKey],
			tsq.[Plant_QtrDateKey],
			tsq.[Plant_QtrDate],
			tsq.[CompanyId],
			tsq.[AssetId],
			tsq.[AssetName],
			tsq.[SubscriberCompanyName],
			tsq.[PlantCompanyName],
			tsq.[SubscriberAssetName],
			tsq.[PlantAssetName],
			tsq.[CountryId],
			tsq.[StateName],
			tsq.[EconRegionId],
			tsq.[UomId],
			tsq.[CurrencyFcn],
			tsq.[CurrencyRpt],
			tsq.[AssetPassWord_PlainText],
			tsq.[StudyYear],
			tsq.[DataYear],
			tsq.[Consultant],
			tsq.[StudyYearDifference],
			tsq.[CalQtr]
		FROM [calc].[FoundationPlantListSource]						tsq
		WHERE	tsq.Refnum = ISNULL(@Refnum, tsq.Refnum)
			AND	tsq.FactorSetId = ISNULL(@FactorSetId, tsq.FactorSetId);

		EXECUTE [calc].[Delete_CalculatePlant]						@Refnum, @fpl;

		EXECUTE [stgFact].[Delete_SimulationResultsOnInputChange]	@Refnum;

		EXECUTE [sim].[Delete_SimulationDataOnSimulationChange]		@Refnum, 'PYPS';
		EXECUTE [sim].[Delete_SimulationDataOnSimulationChange]		@Refnum, 'SPSL';

		EXECUTE [fact].[Delete_OSIM]								@Refnum;

		EXECUTE [fact].[Insert_OSIM]								@Refnum;

		EXECUTE [sim].[Insert_SimulationData]						@Refnum, 'PYPS';
		EXECUTE [sim].[Insert_SimulationData]						@Refnum, 'SPSL';

		EXECUTE [calc].[Insert_CalculatePlant]						@Refnum, @fpl;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
