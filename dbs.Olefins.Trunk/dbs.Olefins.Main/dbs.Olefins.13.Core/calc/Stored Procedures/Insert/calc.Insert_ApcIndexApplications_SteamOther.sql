﻿CREATE PROCEDURE [calc].[Insert_ApcIndexApplications_SteamOther]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ApcIndexSteamOther]([FactorSetId], [Refnum], [CalDateKey], [ApcId], [AbsenceCorrectionFactor], [OnLine_Pcnt], [Mpc_Int], [Mpc_Value], [Apc_Index])
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			w.ApcId,
			c._AbsenceCorrectionFactor,
			COALESCE(o.OnLine_Pcnt, 0.0)					[OnLine_Pcnt],
			CASE WHEN k.Controller_Count >= 1 THEN 1.0 ELSE 0.0 END
															[Mpc_Int],
			CASE WHEN c._AbsenceCorrectionFactor = 1.0
				THEN 0.25
				ELSE 0.5
				END											[Mpc_Value],
			
			CASE WHEN c._AbsenceCorrectionFactor = 1.0
				THEN 0.25
				ELSE 0.5
				END / c._AbsenceCorrectionFactor
			* CASE WHEN k.Controller_Count >= 1 THEN 1.0 ELSE 0.0 END
			* CASE WHEN t.Controller_Count >= 2 THEN 1.0 ELSE 2.0 END
															[Apc_Index]
		FROM @fpl												tsq
		INNER JOIN calc.ApcAbsenceCorrection					c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_AnnDateKey
		INNER JOIN ante.ApcWeighting							w
			ON	w.FactorSetId	= tsq.FactorSetId
			AND	w.ApcId			IN ('ApcSteamHeat', 'ApcOtherOnSite')
		LEFT OUTER JOIN fact.ApcControllers						k
			ON	k.Refnum		= tsq.Refnum
			AND	k.CalDateKey	= tsq.Plant_QtrDateKey
			AND	k.ApcId			= w.ApcId
		LEFT OUTER JOIN fact.ApcOnLinePcnt						o
			ON	o.Refnum		= tsq.Refnum
			AND	o.CalDateKey	= tsq.Plant_QtrDateKey
			AND	o.ApcId			= 'ProdRecovery'
		LEFT OUTER JOIN (
			SELECT
				so.Refnum,
				so.CalDateKey,
				COUNT(1) [Controller_Count]
			FROM fact.ApcControllers	so	/*	Correction for combining Steam and Other Onsites*/
			WHERE	so.ApcId	IN ('ApcSteamHeat', 'ApcOtherOnSite')
				AND	so.Controller_Count	> 0
			GROUP BY
				so.Refnum,
				so.CalDateKey
			) t
			ON	t.Refnum		= tsq.Refnum
			AND	t.CalDateKey	= tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;