﻿CREATE PROCEDURE calc.Insert_CapacityProductionRatio
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.CapacityProductionRatio(FactorSetId, Refnum, CalDateKey, Ethylene_kMT, Propylene_kMT, OLEProd_kMT, HVC_kMT, FreshPyro_kMT, FreshSupp_kMT, EthyleneLoss_kMT)
		SELECT
			  tsq.FactorSetId
			, tsq.Refnum
			, tsq.Plant_QtrDateKey
		
			, p.C2H4				[Ethylene_kMT]
			, p.C3H6				[Propylene_kMT]
			, p.ProdOlefins			[OLEProd_kMT]
			, p.ProdHVC				[ProdHVC_kMT]
			, f.FeedPyrolysis_kMT
			, f.FeedAnalysis_kMT
			, l._TotLoss_kMT
		FROM @fpl										tsq
		INNER JOIN (
			SELECT
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey,
				p.ComponentId,
				p.[ProductionSold_kMT]
			FROM calc.DivisorsProduction p
			) u
			PIVOT (
			MAX(u.[ProductionSold_kMT]) FOR ComponentId IN(
				C2H4, C3H6, ProdOlefins, ProdHVC
				)
			) p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_AnnDateKey
		INNER JOIN calc.DivisorsFeed f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_AnnDateKey
			AND	f.StreamId = 'PlantFeed'
		LEFT OUTER JOIN fact.ReliabilityOppLoss			l
			ON	l.Refnum = tsq.Refnum
			AND	l.CalDateKey = tsq.Plant_AnnDateKey
			AND	l.StreamId = 'Ethylene'
			AND	l.OppLossId = 'TurnAround'
		WHERE tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;