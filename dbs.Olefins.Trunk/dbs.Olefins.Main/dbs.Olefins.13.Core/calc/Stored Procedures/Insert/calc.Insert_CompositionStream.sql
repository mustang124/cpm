﻿CREATE PROCEDURE calc.Insert_CompositionStream
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Composition TABLE
		(
			FactorSetId				VARCHAR(12)		NOT NULL	CHECK(FactorSetId <> ''),
			Refnum					VARCHAR(25)		NOT NULL	CHECK(Refnum <> ''),
			CalDateKey				INT				NOT	NULL	CHECK(LEN(CalDateKey) = 8 AND CalDateKey > 19000101 AND CalDateKey < 99991231),

			StreamId				VARCHAR(42)		NOT	NULL	CHECK(StreamId <> ''),
			StreamDescription		VARCHAR(256)	NOT	NULL	CHECK(StreamDescription <> ''),
			Quantity_kMT			REAL			NOT	NULL	CHECK(Quantity_kMT >= 0.0),

			ComponentId				VARCHAR(42)		NOT	NULL	CHECK(ComponentId <> ''),

			Component_WtPcnt		REAL			NOT	NULL	CHECK(Component_WtPcnt >= 0.0 AND Component_WtPcnt <= 100.0),

			_Component_kMT			AS CONVERT(REAL, [Quantity_kMT] * [Component_WtPcnt] / 100.0, 1)
									PERSISTED		NOT	NULL	CHECK(_Component_kMT >= 0.0),

			--Stream_HierarchyID		HIERARCHYID			NULL,
			OpCondId				VARCHAR(42)			NULL,
			RecycleId				TINYINT				NULL,

			PRIMARY KEY CLUSTERED (FactorSetId DESC, Refnum DESC, CalDateKey ASC, StreamId ASC, StreamDescription ASC, ComponentId ASC)
			);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Composition (inter.CompositionCalc)';
		PRINT @ProcedureDesc;

		INSERT INTO @Composition(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT, ComponentId, Component_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT,
			k.ComponentId,
			k.Component_WtPcnt
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN inter.CompositionCalc					k		-- Calculated Components
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.Refnum = tsq.Refnum
			AND	k.CalDateKey = tsq.Plant_AnnDateKey
			AND	k.StreamId = q.StreamId
			AND	k.StreamDescription = q.StreamDescription;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Composition - PPFC Liquid Fuels (Table 7)';
		PRINT @ProcedureDesc;

		INSERT INTO @Composition(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT, ComponentId, Component_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			'PPFC',
			'PPCFuel',
			ehv.LHValue_MBtu / nhv.NHValue_MBtulb / 2.2046	[Quantity_kMT],
			nhv.ComponentId,
			nhv.Component_WtPcnt
		FROM	@fpl									tsq
		INNER JOIN fact.EnergyLHValue					ehv
			ON	ehv.Refnum = tsq.Refnum
			AND	ehv.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN ante.PricingNetHeatingValueStream	nhv
			ON	nhv.FactorSetId = tsq.FactorSetId
			AND	nhv.CalDateKey = tsq.FactorSet_QtrDateKey
			AND	nhv.StreamId = ehv.AccountId
		WHERE	tsq.CalQtr = 4;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Composition - PPFC Inerts (Table 3)';
		PRINT @ProcedureDesc;

		INSERT INTO @Composition(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT, ComponentId, Component_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT,
			'Inerts'										[ComponentId],
			100.0 - SUM(c.Component_WtPcnt)					[Component_WtPcnt]
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN fact.CompositionQuantity					c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_AnnDateKey
			AND	c.StreamId = q.StreamId
			AND	c.StreamDescription = q.StreamDescription
		LEFT OUTER JOIN inter.CompositionCalc				k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.Refnum = tsq.Refnum
			AND	k.CalDateKey = tsq.Plant_AnnDateKey
			AND	k.StreamId = q.StreamId
			AND	k.StreamDescription = q.StreamDescription
		WHERE	k.FactorSetId IS NULL
			AND	q.StreamId = 'PPFC'
			AND	tsq.Refnum NOT IN (SELECT i.Refnum FROM fact.CompositionQuantity i WHERE i.StreamId = 'PPFC' AND i.ComponentId = 'Inerts')
		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT
		HAVING	100.0 - SUM(c.Component_WtPcnt)	> 0.0001;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Composition (fact.CompositionQuantity)';
		PRINT @ProcedureDesc;

		INSERT INTO @Composition(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT, ComponentId, Component_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT,
			c.ComponentId,
			c.Component_WtPcnt
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND	q.StreamId <> 'SuppOther'
		INNER JOIN fact.CompositionQuantity					c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_AnnDateKey
			AND	c.StreamId = q.StreamId
			AND	c.StreamDescription = q.StreamDescription
		LEFT OUTER JOIN inter.CompositionCalc				k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.Refnum = tsq.Refnum
			AND	k.CalDateKey = tsq.Plant_AnnDateKey
			AND	k.StreamId = q.StreamId
			AND	k.StreamDescription = q.StreamDescription
		WHERE	k.FactorSetId IS NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Composition (Super.SuppOther)';
		PRINT @ProcedureDesc;

		INSERT INTO @Composition(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT, ComponentId, Component_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT,
			c.ComponentId,
			c.Component_WtPcnt
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND	q.StreamId = 'SuppOther'
		INNER JOIN super.CompositionPivot					c
			ON	c.Refnum = tsq.Refnum
			AND(c.StreamId = q.StreamId
				OR etl.ConvStreamID(c.StreamId) = q.StreamId);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Composition (fact.SuppOther)';
		PRINT @ProcedureDesc;

		INSERT INTO @Composition(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT, ComponentId, Component_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT,
			c.ComponentId,
			c.Component_WtPcnt
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND	q.StreamId = 'SuppOther'
		INNER JOIN fact.CompositionQuantity					c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_AnnDateKey
			AND	c.StreamId = q.StreamId
			AND	c.StreamDescription = q.StreamDescription
		LEFT OUTER JOIN super.Composition					s
			ON	s.Refnum = tsq.Refnum
			AND	s.StreamId = q.StreamId
		WHERE	s.StreamId IS NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Composition (ante.Composition)';
		PRINT @ProcedureDesc;

		INSERT INTO @Composition(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT, ComponentId, Component_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT,
			c.ComponentId,
			c.Component_WtPcnt
		FROM	@fpl									tsq
		INNER JOIN fact.Quantity						q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN ante.Composition						c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.CalDateKey = tsq.FactorSet_AnnDateKey
			AND	c.StreamId = q.StreamId
		LEFT OUTER JOIN @Composition					k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.Refnum = tsq.Refnum
			AND	k.CalDateKey = tsq.Plant_QtrDateKey
			AND	k.StreamId = q.StreamId
			AND	k.StreamDescription = q.StreamDescription
			AND	k.ComponentId = c.ComponentId
		WHERE	k.FactorSetId IS NULL;

		SET @ProcedureDesc = NCHAR(9) + N'UPDATE @Composition';
		PRINT @ProcedureDesc;

		UPDATE @Composition
		SET OpCondId	= CASE WHEN (b.StreamId IS NOT NULL) THEN 'OSOP' ELSE 'PlantFeed' END,
			RecycleId	= CASE WHEN (b.StreamId IS NOT NULL) THEN r.RecycleId ELSE 0 END
		FROM @Composition						c
		INNER JOIN calc.PlantRecycles			r
			ON	r.FactorSetId = c.FactorSetId
			AND	r.Refnum = c.Refnum
		LEFT OUTER JOIN dim.Stream_Bridge		b
			ON	b.FactorSetId = c.FactorSetId
			AND	b.StreamId = 'ProdLoss'
			AND	b.DescendantId = c.StreamId

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.CompositionStream';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[CompositionStream](FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, StreamDescription,
			RecycleId,
			ComponentId,
			Component_WtPcnt, Component_kMT,
			Priced_WtPcnt, Priced_kMT,
			Purity_WtPcnt, Purity_kMT)
		SELECT
			c.FactorSetId,
			c.Refnum,
			c.CalDateKey,
			'Plant',
			c.OpCondId,
			c.StreamId,
			c.StreamDescription,
			c.RecycleId,

			c.ComponentId,

			c.Component_WtPcnt,
			c._Component_kMT,

			CASE WHEN c.Component_WtPcnt >= l.Purity_WtPcnt			THEN c.Component_WtPcnt	END	[Priced_WtPcnt],
			CASE WHEN c.Component_WtPcnt >= l.Purity_WtPcnt			THEN c._Component_kMT	END	[Priced_kMT],

			CASE WHEN c.Component_WtPcnt >= l.PurityProduct_WtPcnt	THEN c.Component_WtPcnt	END	[Purity_WtPcnt],
			CASE WHEN c.Component_WtPcnt >= l.PurityProduct_WtPcnt	THEN c._Component_kMT	END	[Purity_kMT]

		FROM @Composition								c
		LEFT OUTER JOIN ante.PricingStreamPurityLimits	l
			ON	l.FactorSetId = c.FactorSetId
			AND	l.SimModelId = 'Plant'
			AND	l.StreamId = c.StreamId
			AND	l.ComponentId = c.ComponentId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;