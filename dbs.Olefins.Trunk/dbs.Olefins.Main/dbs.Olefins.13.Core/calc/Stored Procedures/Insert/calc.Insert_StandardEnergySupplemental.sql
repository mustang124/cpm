﻿CREATE PROCEDURE [calc].[Insert_StandardEnergySupplemental]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		-------------------------------------------------------------------------------
		--	F309: Supplemental

		INSERT INTO [calc].[StandardEnergySupplemental]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId],
			[ComponentId], [Component_kMT], [Energy_BTUlb], [StandardEnergy_MBtuDay])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			map.[StreamId],
			map.[ComponentId],
			sup.[Quantity_kMT],
			sec.[Energy_BTUlb],
			sup.[Quantity_kMT] * sec.[Energy_BTUlb] * 2.2046 / 365.0	[StandardEnergy_MBtuDay]
		FROM @fpl									fpl
		INNER JOIN [fact].[Quantity]					sup
			ON	sup.[Refnum]		= fpl.[Refnum]
			AND	sup.CalDateKey		= fpl.Plant_QtrDateKey
			AND	sup.[StreamId]		IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'SuppTot')
		INNER JOIN [ante].[MapStreamComponent]			map
			ON	map.[FactorSetId]	= fpl.[FactorSetId]
			AND	map.[StreamId]		= sup.[StreamId]
		INNER JOIN [ante].[StandardEnergyComponents]	sec
			ON	sec.[FactorSetId]	= fpl.[FactorSetId]
			AND	sec.[ComponentId]	= map.[ComponentId]
		WHERE	fpl.[FactorSet_AnnDateKey] > 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;