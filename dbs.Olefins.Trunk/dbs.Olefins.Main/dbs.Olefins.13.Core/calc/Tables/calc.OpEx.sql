﻿CREATE TABLE [calc].[OpEx] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [Reported_Cur]   REAL               NULL,
    [Adjusted_Cur]   REAL               NULL,
    [_Amount_Cur]    AS                 (isnull([Adjusted_Cur],[Reported_Cur])) PERSISTED,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Calc_OpEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Calc_OpEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Calc_OpEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Calc_OpEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Calc_OpEx] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_OpEx_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_calc_OpEx_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_OpEx_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_OpEx_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_OpEx_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_CalcOpEx_u
	ON  calc.OpEx
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.OpEx
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.OpEx.FactorSetId	= INSERTED.FactorSetId
		AND	calc.OpEx.Refnum		= INSERTED.Refnum
		AND calc.OpEx.CalDateKey	= INSERTED.CalDateKey
		AND calc.OpEx.AccountId		= INSERTED.AccountId
		AND calc.OpEx.CurrencyRpt	= INSERTED.CurrencyRpt;
	
END