﻿CREATE TABLE [calc].[StandardEnergyFracFeed] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [StandardEnergyId]       VARCHAR (42)       NOT NULL,
    [FeedProcessed_kMT]      REAL               NOT NULL,
    [FeedDensity_SG]         REAL               NOT NULL,
    [Energy_kBtuBBL]         REAL               NOT NULL,
    [StandardEnergy_MBtuDay] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyFracFeed_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyFracFeed_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyFracFeed_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyFracFeed_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyFracFeed] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StandardEnergyId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_StandardEnergyFracFeed_Energy_kBtuBBL] CHECK ([Energy_kBtuBBL]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyFracFeed_FeedDensity_SG] CHECK ([FeedDensity_SG]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyFracFeed_FeedProcessed_kMT] CHECK ([FeedProcessed_kMT]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyFracFeed_StandardEnergy_MBtuDay] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [FK_StandardEnergyFracFeed_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_StandardEnergyFracFeed_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergyFracFeed_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergyFracFeed_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_CalcStandardEnergyFracFeed_u
	ON  calc.StandardEnergyFracFeed
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.StandardEnergyFracFeed
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.StandardEnergyFracFeed.[FactorSetId]		= INSERTED.[FactorSetId]
		AND	calc.StandardEnergyFracFeed.[Refnum]			= INSERTED.[Refnum]
		AND calc.StandardEnergyFracFeed.[StandardEnergyId]	= INSERTED.[StandardEnergyId]
		AND calc.StandardEnergyFracFeed.[CalDateKey]		= INSERTED.[CalDateKey];
				
END;