﻿CREATE TABLE [calc].[ApcIndex] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [ApcId]           VARCHAR (42)       NOT NULL,
    [Mpc_Value]       REAL               CONSTRAINT [DF_calc_ApcIndex_Mpc_Value] DEFAULT ((0.0)) NOT NULL,
    [Sep_Value]       REAL               NULL,
    [Mpc_Int]         INT                CONSTRAINT [DF_calc_ApcIndex_Mpc_Int] DEFAULT ((0)) NOT NULL,
    [Sep_Int]         INT                CONSTRAINT [DF_calc_ApcIndex_Sep_Int] DEFAULT ((0)) NOT NULL,
    [ControllerTypes] AS                 (CONVERT([varchar](8),case when [Mpc_Int]=(1) then 'MPC' else case when [Sep_Int]=(1) then 'SC' else '-' end end,(0))) PERSISTED NOT NULL,
    [OnLine_Pcnt]     REAL               NULL,
    [Apc_Index]       REAL               NOT NULL,
    [ApcOnLine_Index] REAL               NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_ApcIndex_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_ApcIndex_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_ApcIndex_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_ApcIndex_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ApcIndex] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ApcId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_ApcIndex_Apc_Index] CHECK ([Apc_Index]>=(0.0)),
    CONSTRAINT [CR_calc_ApcIndex_ApcOnLine_Index] CHECK ([ApcOnLine_Index]>=(0.0)),
    CONSTRAINT [CR_calc_ApcIndex_Mpc_Value] CHECK ([Mpc_Value]>=(0.0)),
    CONSTRAINT [CR_calc_ApcIndex_OnLine_Pcnt] CHECK ([OnLine_Pcnt]>=(0.0) AND [OnLine_Pcnt]<=(100.0)),
    CONSTRAINT [CR_calc_ApcIndex_Sep_Value] CHECK ([Sep_Value]>=(0.0)),
    CONSTRAINT [FK_calc_ApcIndex_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_calc_ApcIndex_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ApcIndex_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ApcIndex_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ApcIndex_u
	ON  calc.ApcIndex
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ApcIndex
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ApcIndex.FactorSetId	= INSERTED.FactorSetId
		AND calc.ApcIndex.Refnum		= INSERTED.Refnum
		AND calc.ApcIndex.CalDateKey	= INSERTED.CalDateKey
		AND calc.ApcIndex.ApcId			= INSERTED.ApcId;
				
END;