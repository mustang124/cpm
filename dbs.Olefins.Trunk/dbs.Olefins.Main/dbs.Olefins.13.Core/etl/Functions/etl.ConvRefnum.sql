﻿CREATE FUNCTION etl.ConvRefnum
(
	@Refnum		VARCHAR(25),
	@StudyYear	SMALLINT,
	@StudyId	VARCHAR(4)
)
RETURNS VARCHAR(25)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @StrYear	VARCHAR(4) = COALESCE(CONVERT(VARCHAR(4), @StudyYear, 0), '----');

	SET @StudyId	= COALESCE(@StudyId, '----');
	SET @Refnum		= RTRIM(LTRIM(@Refnum));

	DECLARE @AssetBeg	INT			= CHARINDEX(@StudyId, @Refnum) + LEN(@StudyId);

	DECLARE @AssetId	VARCHAR(5)	= SUBSTRING(@Refnum, @AssetBeg, LEN(@Refnum) - @AssetBeg + 1);

	SET @AssetId = CASE UPPER(RIGHT(@AssetId, 1))
		WHEN 'P' THEN REPLACE(@AssetId, 'P', '')
		WHEN 'U' THEN REPLACE(@AssetId, 'U', '')
		ELSE @AssetId
		END;

	IF LEN(@AssetId) < 3
	BEGIN

		SET @AssetId = RIGHT('000' + @AssetId, 3);
		
	END;

	DECLARE @AssetSuffix	VARCHAR(1) =
		CASE RIGHT(@Refnum, 1)
		WHEN 'U' THEN 'U'
		WHEN 'P' THEN 'P'
		ELSE ''
		END;

	RETURN @StrYear + @StudyId + @AssetId + @AssetSuffix;
	
END;