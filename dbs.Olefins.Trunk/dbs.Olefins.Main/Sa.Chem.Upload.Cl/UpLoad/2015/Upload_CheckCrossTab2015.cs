﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadCheckCrossTab2015(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					string cTCCT = (includeSpace) ? "Check Cross Tab" : "CheckCrossTab";
					wks = wkb.Worksheets[cTCCT];

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_CheckCrossTab]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum				CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;


						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCheckCrossTab2015", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Facilities]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}