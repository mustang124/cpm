﻿CREATE PROCEDURE [web].[Get_CompanyPlantsUser]
(
	@CompanyId		INT,
	@UserId			INT
)
AS
BEGIN

	SET NOCOUNT ON;

	--SELECT PlantID, PlantName, '' AS Active
	--FROM [auth].Plants
	--WHERE CompanyID = @CompanyID
	--AND Active = 1
	--AND PlantID NOT IN (SELECT PlantID FROM PlantPermissions WHERE UserID = @UserID)
	--UNION
	--SELECT PlantPermissions.PlantID, Plants.PlantName, PlantPermissions.Active
	--FROM PlantPermissions
	--INNER JOIN Plants ON Plants.PlantID = PlantPermissions.PlantID
	--WHERE UserID = @UserID --AND PlantPermissions.Active = 1
	--AND Plants.Active = 1

	DECLARE @LoginId	INT = @UserId;

	SELECT
		cpu.[PlantId],
		cpu.[PlantName],
		cpu.[Active]
	FROM [auth].[Get_CompanyPlantsUser](@CompanyId, @LoginId) cpu;

END;