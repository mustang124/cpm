﻿CREATE TABLE [ante].[Factors_EiiIntercepts] (
    [MethodologyId]      INT                NOT NULL,
    [HvcYield_Intercept] FLOAT (53)         NOT NULL,
    [Energy_Intercept]   FLOAT (53)         NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_Factors_EiiIntercepts_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_Factors_EiiIntercepts_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_Factors_EiiIntercepts_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_Factors_EiiIntercepts_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Factors_EiiIntercepts] PRIMARY KEY CLUSTERED ([MethodologyId] DESC),
    CONSTRAINT [CR_Factors_EiiIntercepts_Energy_Intercept_MinExcl_0.0] CHECK ([Energy_Intercept]>(0.0)),
    CONSTRAINT [CR_Factors_EiiIntercepts_HvcYield_Intercept_MinExcl_0.0] CHECK ([HvcYield_Intercept]>(0.0)),
    CONSTRAINT [FK_Factors_EiiIntercepts_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId])
);


GO

CREATE TRIGGER [ante].[t_Factors_EiiIntercepts_u]
	ON [ante].[Factors_EiiIntercepts]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Factors_EiiIntercepts]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Factors_EiiIntercepts].[MethodologyId]	= INSERTED.[MethodologyId];

END;