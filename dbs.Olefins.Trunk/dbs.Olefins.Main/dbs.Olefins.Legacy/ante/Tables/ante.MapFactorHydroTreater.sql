﻿CREATE TABLE [ante].[MapFactorHydroTreater] (
    [MethodologyId]      INT                NOT NULL,
    [HydroTreaterTypeId] INT                NOT NULL,
    [FactorId]           INT                NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_MapFactorHydroTreater_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_MapFactorHydroTreater_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_MapFactorHydroTreater_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_MapFactorHydroTreater_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapFactorHydroTreater] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC),
    CONSTRAINT [FK_MapFactorHydroTreater_Factor_LookUp] FOREIGN KEY ([FactorId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_MapFactorHydroTreater_HydroTreater_LookUp] FOREIGN KEY ([HydroTreaterTypeId]) REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId]),
    CONSTRAINT [FK_MapFactorHydroTreater_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId])
);


GO

CREATE TRIGGER [ante].[t_MapFactorHydroTreater_u]
	ON [ante].[MapFactorHydroTreater]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorHydroTreater].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorHydroTreater].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId];

END;