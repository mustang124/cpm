﻿CREATE FUNCTION [fact].[Get_StreamCompositionMol]
(
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		c.[SubmissionId],
		c.[StreamNumber],
		c.[ComponentId],
		c.[Component_MolPcnt]
	FROM [fact].[StreamCompositionMol]		c
	WHERE	c.[SubmissionId] = @SubmissionId
);