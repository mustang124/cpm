﻿CREATE FUNCTION [fact].[Get_StreamRecovered]
(
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		r.[SubmissionId],
		r.[StreamNumber],
		r.[Recovered_WtPcnt]
	FROM [fact].[StreamRecovered]		r
	WHERE	r.[SubmissionId] = @SubmissionId
);