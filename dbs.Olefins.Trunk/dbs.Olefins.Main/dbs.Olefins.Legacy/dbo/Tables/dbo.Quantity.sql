﻿CREATE TABLE [dbo].[Quantity] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [FeedProdID]  VARCHAR (20)   NOT NULL,
    [Q1Feed]      REAL           NULL,
    [Q2Feed]      REAL           NULL,
    [Q3Feed]      REAL           NULL,
    [Q4Feed]      REAL           NULL,
    [AnnFeedProd] REAL           NULL,
    [RecPcnt]     REAL           NULL,
    [MiscFeed]    VARCHAR (35)   NULL,
    [OthProdDesc] VARCHAR (35)   NULL,
    [MiscProd1]   VARCHAR (50)   NULL,
    [MiscProd2]   VARCHAR (50)   NULL,
    CONSTRAINT [PK___2__10] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

