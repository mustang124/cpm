﻿CREATE TABLE [dbo].[ProdQuality] (
    [Refnum]        [dbo].[Refnum] NOT NULL,
    [FeedProdID]    VARCHAR (20)   NOT NULL,
    [WtPcnt]        REAL           NULL,
    [MolePcnt]      REAL           NULL,
    [H2Value]       REAL           NULL,
    [HeatValue]     REAL           NULL,
    [H2Content]     REAL           NULL,
    [OthProd1Value] REAL           NULL,
    [OthProd2Value] REAL           NULL,
    CONSTRAINT [PK___21__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

