﻿CREATE TABLE [dbo].[PracFurnInfo] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [FeedProdID] VARCHAR (20)   NOT NULL,
    [Run]        REAL           NULL,
    [Inj]        CHAR (5)       NULL,
    [Pretreat]   CHAR (1)       NULL,
    [AntiFoul]   CHAR (1)       NULL,
    CONSTRAINT [PK___22__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

