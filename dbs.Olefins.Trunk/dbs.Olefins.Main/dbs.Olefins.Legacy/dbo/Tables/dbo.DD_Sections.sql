﻿CREATE TABLE [dbo].[DD_Sections] (
    [SectionID]     TINYINT      IDENTITY (0, 1) NOT NULL,
    [SectionName]   VARCHAR (30) NOT NULL,
    [ImplicitBreak] VARCHAR (30) NULL,
    CONSTRAINT [PK_Sections] PRIMARY KEY CLUSTERED ([SectionID] ASC),
    CONSTRAINT [UniqueSectionName] UNIQUE NONCLUSTERED ([SectionName] ASC)
);

