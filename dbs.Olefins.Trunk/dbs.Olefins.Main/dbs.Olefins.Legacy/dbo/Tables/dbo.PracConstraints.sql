﻿CREATE TABLE [dbo].[PracConstraints] (
    [Refnum]   [dbo].[Refnum] NOT NULL,
    [Decision] CHAR (3)       NOT NULL,
    [Tbl9072]  CHAR (1)       NULL,
    [Tbl9073]  CHAR (1)       NULL,
    [Tbl9074]  CHAR (1)       NULL,
    [Tbl9075]  CHAR (1)       NULL,
    [Tbl9076]  CHAR (1)       NULL,
    [Tbl9077]  CHAR (1)       NULL,
    [Tbl9078]  CHAR (1)       NULL,
    [Tbl9079]  CHAR (1)       NULL,
    [Tbl9080]  CHAR (1)       NULL,
    [Tbl9081]  CHAR (1)       NULL,
    [Tbl9082]  CHAR (1)       NULL,
    [Tbl9083]  CHAR (1)       NULL,
    [Tbl9084]  CHAR (1)       NULL,
    CONSTRAINT [PK___17__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Decision] ASC)
);

