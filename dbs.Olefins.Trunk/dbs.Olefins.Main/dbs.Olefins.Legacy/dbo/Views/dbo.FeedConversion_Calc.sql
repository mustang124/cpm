﻿
CREATE VIEW [dbo].[FeedConversion_Calc]
AS
SELECT
 f.Refnum,
 f.Scenario,
 f.FeedProdID,
 f.EthyleneYield,
 f.PropylEthylRatio,
 f.PropylMethaneRatio,
 f.FeedConv,
 
 f.Ethane * 100.0  [EthaneFeed_WtPcnt],
 --p.Ethane    [EthanePYPS_WtPcnt],
 s.Ethane    [Once_EthaneSPSL_WtPcnt],
 
 --CASE WHEN f.Ethane <> 0.0 THEN (f.Ethane - p.Ethane / 100.0) / f.Ethane * 100.0 END [EthaneConversion_PYPS],
 CASE WHEN f.Ethane <> 0.0 THEN (f.Ethane - s.Ethane / 100.0) / f.Ethane * 100.0 END [EthaneConversion_SPSL],
 
 f.Propane * 100.0  [PropaneFeed_WtPcnt],
 --p.Propane    [PropanePYPS_WtPcnt],
 s.Propane    [Once_PropaneSPSL_WtPcnt],
 
 --CASE WHEN f.Propane <> 0.0 THEN (f.Propane - p.Propane / 100.0) / f.Propane * 100.0 END [PropaneConversion_PYPS],
 CASE WHEN f.Propane <> 0.0 THEN (f.Propane - s.Propane / 100.0) / f.Propane * 100.0 END [PropaneConversion_SPSL],
 
 f.Butane * 100.0  [ButaneFeed_WtPcnt],
 --p.Butane    [ButanePYPS_WtPcnt],
 s.Butane    [Once_ButaneSPSL_WtPcnt],
 
 --CASE WHEN f.Butane <> 0.0 THEN (f.Butane - p.Butane / 100.0) / f.Butane * 100.0 END [ButaneConversion_PYPS],
 CASE WHEN f.Butane <> 0.0 THEN (f.Butane - s.Butane / 100.0) / f.Butane * 100.0 END [ButaneConversion_SPSL]
 
FROM (
 SELECT
  f.Refnum,
  z.Scenario,
  f.FeedProdID,
  l.FeedType,
  f.Ethane,
  f.Propane,
  ISNULL(f.nButane, 0.0) + ISNULL(f.iButane, 0.0) [Butane],
  f.EthyleneYield,
  f.PropylEthylRatio,
  f.PropylMethaneRatio,
  f.FeedConv
 FROM dbo.FeedQuality f
 INNER JOIN dbo.FeedProd_LU l
  ON l.FeedProdID = f.FeedProdID,
 (SELECT Scenario FROM (VALUES('MS-25'), ('OS-25'), ('OS-OP')) AS T(Scenario)) z
 WHERE (f.Ethane > 0.0
  OR f.Propane > 0.0
  OR ISNULL(f.nButane, 0.0) + ISNULL(f.iButane, 0.0) > 0.0)
 ) f
LEFT OUTER JOIN (
 SELECT
  p.Refnum,
  p.Scenario,
  p.FeedType,
  p.FeedMT,
  p.FeedPcnt,
  p.Ethane,
  p.Propane,
  p.Butane
 FROM dbo.PYPSDetail p
 WHERE (p.Ethane > 0.0
  OR p.Propane > 0.0
  OR p.Butane > 0.0)
 ) p
 ON p.Refnum = f.Refnum
 AND p.Scenario = f.Scenario
 AND p.FeedType = f.FeedType
LEFT OUTER JOIN ( 
 SELECT
  s.Refnum,
  s.Scenario,
  s.FeedType,
  s.FeedMT,
  s.FeedPcnt,
  s.Ethane,
  s.Propane,
  s.Butane
 FROM dbo.SPSLDetail s
 WHERE (s.Ethane > 0.0
  OR s.Propane > 0.0
  OR s.Butane > 0.0)
 ) s
 ON s.Refnum = f.Refnum
 AND s.Scenario = f.Scenario
 AND s.FeedType = f.FeedType;

