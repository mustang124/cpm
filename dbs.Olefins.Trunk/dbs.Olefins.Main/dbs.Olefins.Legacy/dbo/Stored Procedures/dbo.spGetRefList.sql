﻿
/****** Object:  Stored Procedure dbo.spGetRefList    Script Date: 4/18/2003 4:32:53 PM ******/


CREATE PROCEDURE spGetRefList @ListName RefListName AS
SELECT l.Refnum, CoLoc = (SELECT s.CoLoc FROM TSort s WHERE s.Refnum = l.Refnum), l.UserGroup
FROM RefList_LU rl INNER JOIN RefList l ON l.RefListNo = rl.RefListNo
WHERE rl.ListName = @ListName
ORDER BY l.UserGroup, l.Refnum

