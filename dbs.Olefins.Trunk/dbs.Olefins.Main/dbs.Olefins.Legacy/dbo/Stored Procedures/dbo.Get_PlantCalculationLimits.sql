﻿CREATE PROCEDURE [dbo].[Get_PlantCalculationLimits]
(
	@PlantId		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SElECT
		pcl.[PlantId],
		pcl.[CalcsPerPeriod_Count],
		pcl.[PeriodLen_Days],
		pcl.[LastSubmission_Date],
		pcl.[SubmissionRoll_Date],
		pcl.[Submissions_Count],
		pcl.[SubmissionsRemaining_Count],
		pcl.[SubmissionsBalance_Count]
	FROM [auth].[Get_PlantCalculationLimits](@PlantId) pcl;

END;
