﻿CREATE PROC UpdateProdLossPre2007 (@Refnum Refnum) AS
--
--	Move the DTs and SDs inot the Annualized field proir to 2007 
--	Only asked for 1 year up until then
--
Declare @SY as integer
Select @SY = StudyYear from TSort Where Refnum = @Refnum
IF @SY < 2007 
	Update ProdLoss
	SET AnnDTLoss = DTLoss, AnnSDLoss = SDLoss, AnnTotLoss = TotLoss
	WHERE Refnum = @Refnum
