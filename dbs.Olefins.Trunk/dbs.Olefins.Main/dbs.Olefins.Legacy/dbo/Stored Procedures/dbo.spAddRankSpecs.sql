﻿
/****** Object:  Stored Procedure dbo.spAddRankSpecs    Script Date: 4/18/2003 4:32:52 PM ******/

/****** Object:  Stored Procedure dbo.spAddRankSpecs    Script Date: 12/28/2001 7:34:24 AM ******/
/****** Object:  Stored Procedure dbo.spAddRankSpecs    Script Date: 04/13/2000 8:32:49 AM ******/
CREATE PROCEDURE spAddRankSpecs @RefListNo integer,
			 @BreakID integer,
			 @BreakValue varchar(12),
			 @RankVariableID integer,
			 @RankSpecsId integer OUTPUT
AS
IF NOT EXISTS (SELECT * FROM RankSpecs_LU 
	WHERE RefListNo = @RefListNo AND BreakID = @BreakID
	AND BreakValue = @BreakValue AND RankVariableID = @RankVariableID)
	INSERT INTO RankSpecs_LU (RefListNo, BreakID, BreakValue, RankVariableID, LastTime)
	VALUES (@RefListNo, @BreakID, @BreakValue, @RankVariableID, GetDate())
	
SELECT @RankSpecsId = RankSpecsId
FROM RankSpecs_LU 
WHERE RefListNo = @RefListNo 
AND BreakID = @BreakID AND BreakValue = @BreakValue
AND RankVariableID = @RankVariableID

