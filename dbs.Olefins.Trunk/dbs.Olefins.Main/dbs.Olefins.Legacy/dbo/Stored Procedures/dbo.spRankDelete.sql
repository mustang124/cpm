﻿
/****** Object:  Stored Procedure dbo.spRankDelete    Script Date: 4/18/2003 4:32:53 PM ******/

/****** Object:  Stored Procedure dbo.spRankDelete    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.spRankDelete    Script Date: 04/13/2000 8:32:50 AM ******/
/****** Object:  Stored Procedure dbo.RankDelete    Script Date: 8/8/97 8:26:09 AM ******/
CREATE PROCEDURE spRankDelete
@RefListNo integer,
@BreakID integer,
@RankVariableID integer
AS
DELETE FROM Rank
WHERE RankSpecsId IN (
	SELECT RankSpecsId 

	FROM RankSpecs_LU
	WHERE RefListNo = @RefListNo 
	AND BreakID = @BreakID AND RankVariableID = @RankVariableID)

