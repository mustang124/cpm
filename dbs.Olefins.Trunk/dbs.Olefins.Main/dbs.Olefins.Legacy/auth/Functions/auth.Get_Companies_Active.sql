﻿CREATE FUNCTION [auth].[Get_Companies_Active]
(
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		c.[CompanyId],
		c.[CompanyName]
	FROM [auth].[Companies]	c
	WHERE c.[Active] = 1
);