﻿CREATE FUNCTION [auth].[Get_Roles]
(
)
RETURNS TABLE
AS
RETURN
SELECT
	r.[RoleId],
	r.[RoleTag],
	r.[RoleName],
	r.[RoleDetail],
	r.[RoleLevel]
FROM [auth].[Roles] r;