﻿CREATE TABLE [calc].[Shadow_Audit] (
    [MethodologyId]  INT                NOT NULL,
    [SubmissionId]   INT                NOT NULL,
    [TimeBeg]        DATETIMEOFFSET (7) NOT NULL,
    [TimeEnd]        DATETIMEOFFSET (7) NULL,
    [_Duration_mcs]  AS                 (datediff(microsecond,[TimeBeg],[TimeEnd])),
    [Error_Count]    INT                NULL,
    [Active]         BIT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Shadow_Audit_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Shadow_Audit_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Shadow_Audit_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Shadow_Audit_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    [tsAction]       CHAR (1)           NOT NULL,
    CONSTRAINT [PK_Shadow_Audit] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [tsModifiedRV] DESC),
    CONSTRAINT [CV_Shadow_Audit_tsAction] CHECK ([tsAction]='D' OR [tsAction]='U' OR [tsAction]='I')
);

