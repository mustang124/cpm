﻿CREATE TABLE [calc].[Capacity] (
    [MethodologyId]           INT                NOT NULL,
    [SubmissionId]            INT                NOT NULL,
    [StreamId]                INT                NOT NULL,
    [ComponentId]             INT                NOT NULL,
    [StreamDay_MTSD]          FLOAT (53)         NOT NULL,
    [Recovered_Ann_kMT]       FLOAT (53)         NULL,
    [Utilization_Pcnt]        FLOAT (53)         NOT NULL,
    [_Supplemental_MTSD]      AS                 (([Recovered_Ann_kMT]/(365.0))*(1000.0)) PERSISTED,
    [_SupplementalInfer_kMT]  AS                 (([Recovered_Ann_kMT]/[Utilization_Pcnt])*(100.0)) PERSISTED,
    [_SupplementalInfer_MTSD] AS                 (((([Recovered_Ann_kMT]/(365.0))*(1000.0))/[Utilization_Pcnt])*(100.0)) PERSISTED,
    [_PlantCapacity_MTSD]     AS                 (CONVERT([float],[StreamDay_MTSD]-(((coalesce([Recovered_Ann_kMT],(0.0))/(365.0))*(1000.0))/[Utilization_Pcnt])*(100.0),0)) PERSISTED NOT NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_Capacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (128)     CONSTRAINT [DF_Capacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (128)     CONSTRAINT [DF_Capacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (128)     CONSTRAINT [DF_Capacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]            ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Capacity] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_Capacity_Recovered_Ann_kMT_MinIncl_0.0] CHECK ([Recovered_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_Capacity_StreamDay_MTSD_MinIncl_0.0] CHECK ([StreamDay_MTSD]>=(0.0)),
    CONSTRAINT [CR_Capacity_Utilization_Pcnt_MinIncl_0.0] CHECK ([Utilization_Pcnt]>=(0.0)),
    CONSTRAINT [FK_Capacity_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Capacity_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Capacity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Capacity_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_Capacity_u]
	ON [calc].[Capacity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[Capacity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[Capacity].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[calc].[Capacity].[SubmissionId]		= INSERTED.[SubmissionId];

END;