﻿CREATE FUNCTION [calc].[Get_StandardEnergy_BtuHvc]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[EIIStdEnergy_BtuHvc]
	FROM [calc].[StandardEnergy_BtuHvc] c	WITH (NOEXPAND)
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
);