﻿CREATE PROCEDURE [calc].[Insert_StreamComposition_Expansion]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Comp			TABLE
	(
		[MethodologyId]			INT					NOT	NULL,
		[SubmissionId]			INT					NOT	NULL,
		[StreamNumber]			INT					NOT	NULL,
		[StreamId]				INT					NOT	NULL,
		[ComponentId]			INT					NOT	NULL,
		[Component_WtPcnt]		FLOAT				NOT	NULL	CHECK([Component_WtPcnt] >= 0.0 AND [Component_WtPcnt] <= 100.0),
		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [StreamNumber] ASC, [ComponentId] ASC)
	);

	DECLARE @StreamId		INT;

	DECLARE @H2				INT	= [dim].[Return_ComponentId]('H2');
	DECLARE @CH4			INT	= [dim].[Return_ComponentId]('CH4');
	DECLARE @C2H4			INT	= [dim].[Return_ComponentId]('C2H4');
	DECLARE @C3H6			INT	= [dim].[Return_ComponentId]('C3H6');
	DECLARE @C3H8			INT	= [dim].[Return_ComponentId]('C3H8');
	DECLARE @Inerts			INT	= [dim].[Return_ComponentId]('Inerts');

	--	Hydrogen
	SET @StreamId		= [dim].[Return_StreamId]('Hydrogen');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		a.[SubmissionId],
		a.[StreamNumber],
		a.[StreamId],
		m.[ComponentId],
		2.0 * m.[Component_MolPcnt] / (16.0 - 14.0 * m.[Component_MolPcnt] / 100.0)
	FROM [fact].[StreamQuantity]					a
	INNER JOIN [fact].[StreamCompositionMol]		m
		ON	m.[SubmissionId]		= a.[SubmissionId]
		AND	m.[StreamNumber]		= a.[StreamNumber]
	WHERE	a.[SubmissionId]		= @SubmissionId
		AND	a.[StreamId]			= @StreamId
		AND	m.[ComponentId]			= @H2
		AND	m.[Component_MolPcnt]	> 0.0;

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamNumber],
		c.[StreamId],
		@CH4,
		100.0 - c.[Component_WtPcnt]
	FROM @Comp		c
	WHERE	c.[SubmissionId]		= @SubmissionId
		AND	c.[StreamId]			= @StreamId
		AND	c.[ComponentId]			= @H2
		AND	c.[Component_WtPcnt]	< 100.0;

	--	Methane (Fuel Gas Sales or Transfers)
	SET @StreamId		= [dim].[Return_StreamId]('Methane');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		u.[SubmissionId],
		u.[StreamNumber],
		u.[StreamId],
		[dim].[Return_ComponentId](u.[ComponentTag])	[ComponentId],
		u.[Component_WtPcnt]
	FROM (
		SELECT
			a.[SubmissionId],
			a.[StreamNumber],
			a.[StreamId],
			16.0 * m.[Component_MolPcnt] / (13.48 * m.[Component_MolPcnt] / 100.0 + 2.52)				[CH4],
			1.96 * (100.0 - m.[Component_MolPcnt]) / (13.48 * m.[Component_MolPcnt] / 100.0 + 2.52)		[H2],
			0.56 * (100.0 - m.[Component_MolPcnt]) / (13.48 * m.[Component_MolPcnt] / 100.0 + 2.52)		[C2H6]
		FROM [fact].[StreamQuantity]				a
		INNER JOIN [fact].[StreamCompositionMol]	m
			ON	m.[SubmissionId]		= a.[SubmissionId]
			AND	m.[StreamNumber]		= a.[StreamNumber]
		WHERE	a.[SubmissionId]		= @SubmissionId
			AND	a.[StreamId]			= @StreamId
			AND	m.[ComponentId]			= @CH4
			AND	m.[Component_MolPcnt]	> 0.0
		) p
		UNPIVOT (Component_WtPcnt FOR ComponentTag IN
			(
				[CH4], [H2], [C2H6]
			)
		) u;

	--	Polymer Grade Ethylene Composition, % by wt
	SET @StreamId		= [dim].[Return_StreamId]('EthylenePG');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		u.[SubmissionId],
		u.[StreamNumber],
		u.[StreamId],
		[dim].[Return_ComponentId](u.[ComponentTag])	[ComponentId],
		u.[Component_WtPcnt]
	FROM (
		SELECT
			a.[SubmissionId],
			a.[StreamNumber],
			a.[StreamId],
					 c.[Component_WtPcnt]				[C2H4],		--	Ethylene
			(100.0 - c.[Component_WtPcnt]) * 0.25		[CH4],		--	Methane
			(100.0 - c.[Component_WtPcnt]) * 0.50		[C2H6],		--	Ethane
			(100.0 - c.[Component_WtPcnt]) * 0.25		[C3H8]		--	Propane
		FROM [fact].[StreamQuantity]			a
		INNER JOIN [fact].[StreamComposition]	c
			ON	c.[SubmissionId]		= a.[SubmissionId]
			AND	c.[StreamNumber]		= a.[StreamNumber]
		WHERE	a.[SubmissionId]		= @SubmissionId
			AND	a.[StreamId]			= @StreamId
			AND	c.[ComponentId]			= @C2H4
			AND	c.[Component_WtPcnt]	> 0.0
		) p
		UNPIVOT (Component_WtPcnt FOR ComponentTag IN
			(
				[C2H4], [CH4], [C2H6], [C3H8]
			)
		) u;

	--	Chemical Grade Ethylene Composition, % by wt
	SET @StreamId		= [dim].[Return_StreamId]('EthyleneCG');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		u.[SubmissionId],
		u.[StreamNumber],
		u.[StreamId],
		[dim].[Return_ComponentId](u.[ComponentTag])	[ComponentId],
		u.[Component_WtPcnt]
	FROM (
		SELECT
			a.[SubmissionId],
			a.[StreamNumber],
			a.[StreamId],
					 CASE WHEN c.[Component_WtPcnt] >= 80.0 THEN c.[Component_WtPcnt] ELSE 80.0 END				[C2H4],		--	Ethylene
			(100.0 - CASE WHEN c.[Component_WtPcnt] >= 80.0 THEN c.[Component_WtPcnt] ELSE 80.0 END) * 0.05		[CH4],		--	Methane
			(100.0 - CASE WHEN c.[Component_WtPcnt] >= 80.0 THEN c.[Component_WtPcnt] ELSE 80.0 END) * 0.80		[C2H6],		--	Ethane
			(100.0 - CASE WHEN c.[Component_WtPcnt] >= 80.0 THEN c.[Component_WtPcnt] ELSE 80.0 END) * 0.05		[C3H6],		--	Propylene
			(100.0 - CASE WHEN c.[Component_WtPcnt] >= 80.0 THEN c.[Component_WtPcnt] ELSE 80.0 END) * 0.10		[C3H8]		--	Propane
		FROM [fact].[StreamQuantity]			a
		INNER JOIN [fact].[StreamComposition]	c
			ON	c.[SubmissionId]		= a.[SubmissionId]
			AND	c.[StreamNumber]		= a.[StreamNumber]
		WHERE	a.[SubmissionId]		= @SubmissionId
			AND	a.[StreamId]			= @StreamId
			AND	c.[ComponentId]			= @C2H4
			AND	c.[Component_WtPcnt]	> 0.0
		) p
		UNPIVOT (Component_WtPcnt FOR ComponentTag IN
			(
				[C2H4], [CH4], [C2H6], [C3H6], [C3H8]
			)
		) u;

	--	Polymer Grade Propylene Composition, % by wt
	SET @StreamId		= [dim].[Return_StreamId]('PropylenePG');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		u.[SubmissionId],
		u.[StreamNumber],
		u.[StreamId],
		[dim].[Return_ComponentId](u.[ComponentTag])	[ComponentId],
		u.[Component_WtPcnt]
	FROM (
		SELECT
			a.[SubmissionId],
			a.[StreamNumber],
			a.[StreamId],
					 c.[Component_WtPcnt]				[C3H6],		--	Propylene
			(100.0 - c.[Component_WtPcnt]) * 0.25		[C2H6],		--	Ethane
			(100.0 - c.[Component_WtPcnt]) * 0.50		[C3H8],		--	Propane
			(100.0 - c.[Component_WtPcnt]) * 0.25		[C4H10]		--	Butane
		FROM [fact].[StreamQuantity]			a
		INNER JOIN [fact].[StreamComposition]	c
			ON	c.[SubmissionId]		= a.[SubmissionId]
			AND	c.[StreamNumber]		= a.[StreamNumber]
		WHERE	a.[SubmissionId]		= @SubmissionId
			AND	a.[StreamId]			= @StreamId
			AND	c.[ComponentId]			= @C3H6
			AND	c.[Component_WtPcnt]	> 0.0
		) p
		UNPIVOT (Component_WtPcnt FOR ComponentTag IN
			(
				[C3H6], [C2H6], [C3H8], [C4H10]
			)
		) u;

	--	Chemical Grade Propylene Composition, % by wt
	SET @StreamId		= [dim].[Return_StreamId]('PropyleneCG');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		u.[SubmissionId],
		u.[StreamNumber],
		u.[StreamId],
		[dim].[Return_ComponentId](u.[ComponentTag])	[ComponentId],
		u.[Component_WtPcnt]
	FROM (
		SELECT
			a.[SubmissionId],
			a.[StreamNumber],
			a.[StreamId],
							   c.[Component_WtPcnt]																				[C3H6],		--	Propylene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.050 <= 3.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.050 ELSE 3.0 END	[C2H4],		--	Ethylene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.100 <= 2.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.100 ELSE 2.0 END	[C2H6],		--	Ethane
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.050 <= 0.5 THEN (100.0 - c.[Component_WtPcnt]) * 0.050 ELSE 0.5 END	[C3H4],		--	Methyl-Acetylene/Propadiene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.015 <= 0.2 THEN (100.0 - c.[Component_WtPcnt]) * 0.015 ELSE 0.2 END	[C4H6],		--	Butadiene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.050 <= 1.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.050 ELSE 1.0 END	[C4H8],		--	Butene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.100 <= 2.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.100 ELSE 2.0 END	[C4H10]		--	Butane
		FROM [fact].[StreamQuantity]			a
		INNER JOIN [fact].[StreamComposition]	c
			ON	c.[SubmissionId]		= a.[SubmissionId]
			AND	c.[StreamNumber]		= a.[StreamNumber]
		WHERE	a.[SubmissionId]		= @SubmissionId
			AND	a.[StreamId]			= @StreamId
			AND	c.[ComponentId]			= @C3H6
			AND	c.[Component_WtPcnt]	> 0.0
		) p
		UNPIVOT (Component_WtPcnt FOR ComponentTag IN
			(
				[C3H6], [C2H4], [C2H6], [C3H4], [C4H6], [C4H8], [C4H10]
			)
		) u;

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamNumber],
		c.[StreamId],
		@C3H8,
		100.0 - SUM(c.[Component_WtPcnt])		[Component_WtPcnt]
	FROM @Comp										c
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
		AND	c.[StreamId]		= @StreamId
	GROUP BY
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamNumber],
		c.[StreamId]
	HAVING
		SUM(c.[Component_WtPcnt]) <> 100.0;

	--	Refinery Grade Propylene Composition, % by wt
	SET @StreamId		= [dim].[Return_StreamId]('PropyleneRG');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		u.[SubmissionId],
		u.[StreamNumber],
		u.[StreamId],
		[dim].[Return_ComponentId](u.[ComponentTag])	[ComponentId],
		u.[Component_WtPcnt]
	FROM (
		SELECT
			a.[SubmissionId],
			a.[StreamNumber],
			a.[StreamId],
							   c.[Component_WtPcnt]																				[C3H6],		--	Propylene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.050 <= 3.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.050 ELSE 3.0 END	[C2H4],		--	Ethylene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.100 <= 2.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.100 ELSE 2.0 END	[C2H6],		--	Ethane
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.050 <= 0.5 THEN (100.0 - c.[Component_WtPcnt]) * 0.050 ELSE 0.5 END	[C3H4],		--	Methyl-Acetylene/Propadiene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.015 <= 0.2 THEN (100.0 - c.[Component_WtPcnt]) * 0.015 ELSE 0.2 END	[C4H6],		--	Butadiene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.050 <= 1.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.050 ELSE 1.0 END	[C4H8],		--	Butene
			CASE WHEN (100.0 - c.[Component_WtPcnt]) * 0.100 <= 2.0 THEN (100.0 - c.[Component_WtPcnt]) * 0.100 ELSE 2.0 END	[C4H10]		--	Butane
		FROM [fact].[StreamQuantity]			a
		INNER JOIN [fact].[StreamComposition]	c
			ON	c.[SubmissionId]		= a.[SubmissionId]
			AND	c.[StreamNumber]		= a.[StreamNumber]
		WHERE	a.[SubmissionId]		= @SubmissionId
			AND	a.[StreamId]			= @StreamId
			AND	c.[ComponentId]			= @C3H6
			AND	c.[Component_WtPcnt]	> 0.0
		) p
		UNPIVOT (Component_WtPcnt FOR ComponentTag IN
			(
				[C3H6], [C2H4], [C2H6], [C3H4], [C4H6], [C4H8], [C4H10]
			)
		) u;

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamNumber],
		c.[StreamId],
		@C3H8,
		100.0 - SUM(c.[Component_WtPcnt])		[Component_WtPcnt]
	FROM @Comp										c
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
		AND	c.[StreamId]		= @StreamId
	GROUP BY
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamNumber],
		c.[StreamId]
	HAVING
		SUM(c.[Component_WtPcnt]) <> 100.0;

	--	Residue C3 Product Composition, % by wt
	SET @StreamId		= [dim].[Return_StreamId]('PropaneC3Resid');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		u.[SubmissionId],
		u.[StreamNumber],
		u.[StreamId],
		[dim].[Return_ComponentId](u.[ComponentTag])	[ComponentId],
		u.[Component_WtPcnt]
	FROM (
		SELECT
			a.[SubmissionId],
			a.[StreamNumber],
			a.[StreamId],
					CASE WHEN c.[Component_WtPcnt] <> 0.0 THEN c.[Component_WtPcnt] ELSE 100.0 END					[C3H8],		--	Propane
			100.0 - CASE WHEN c.[Component_WtPcnt] <> 0.0 THEN c.[Component_WtPcnt] ELSE 100.0 END * 15.0 / 16.0	[C3H6],		--	Propylene
			100.0 - CASE WHEN c.[Component_WtPcnt] <> 0.0 THEN c.[Component_WtPcnt] ELSE 100.0 END *  1.0 / 16.0	[C4H10]		--	Butane
		FROM [fact].[StreamQuantity]			a
		INNER JOIN [fact].[StreamComposition]	c
			ON	c.[SubmissionId]		= a.[SubmissionId]
			AND	c.[StreamNumber]		= a.[StreamNumber]
		WHERE	a.[SubmissionId]		= @SubmissionId
			AND	a.[StreamId]			= @StreamId
			AND	c.[ComponentId]			= @C3H8
			AND	c.[Component_WtPcnt]	> 0.0
		) p
		UNPIVOT (Component_WtPcnt FOR ComponentTag IN
			(
				[C3H8], [C3H6], [C4H10]
			)
		) u;

	--	PP&C Fuel Composition, % by wt
	SET @StreamId		= [dim].[Return_StreamId]('PPFC');

	INSERT INTO @Comp([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt])
	SELECT
		@MethodologyId,
		a.[SubmissionId],
		a.[StreamNumber],
		a.[StreamId],
		@Inerts,
		100.0 - SUM(c.[Component_WtPcnt])		[Component_WtPcnt]
	FROM [fact].[StreamQuantity]			a
	INNER JOIN [fact].[StreamComposition]	c
		ON	c.[SubmissionId]	= a.[SubmissionId]
		AND	c.[StreamNumber]	= a.[StreamNumber]
	WHERE	a.[SubmissionId]	= @SubmissionId
		AND	a.[StreamId]		= @StreamId
	GROUP BY
		a.[SubmissionId],
		a.[StreamNumber],
		a.[StreamId]
	HAVING	SUM(c.[Component_WtPcnt]) < 100.0;

	SET @StreamId		= 0;

	INSERT INTO [calc].[StreamComposition]([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt], [Component_Dur_kMT], [Component_Ann_kMT])
	SELECT
		@MethodologyId,
		c.[SubmissionId],
		c.[StreamNumber],
		c.[StreamId],
		c.[ComponentId],
		c.[Component_WtPcnt],
		c.[Component_WtPcnt] * q.[Quantity_kMT] / 100.0,
		c.[Component_WtPcnt] * q.[Quantity_kMT] / 100.0 * z.[_Duration_Multiplier]
	FROM @Comp										c
	INNER JOIN [fact].[StreamQuantity]				q
		ON	q.[SubmissionId] = c.[SubmissionId]
		AND	q.[StreamNumber] = c.[StreamNumber]
		AND	q.[Quantity_kMT] > 0.0
	INNER JOIN [fact].[Submissions]					z
		ON	z.[SubmissionId] = c.[SubmissionId];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
			+ COALESCE(', @StreamId:'		+ CONVERT(VARCHAR, @StreamId),		'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;