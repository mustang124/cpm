﻿CREATE PROCEDURE [calc].[Insert_Standard_kEdc_ElecGen]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId		INT	= dim.Return_StandardId('kEdc');
	DECLARE	@ProcessUnitId	INT	= dim.Return_ProcessUnitId('ElecGen');;
	DECLARE	@FactorId		INT	= dim.Return_FactorId('ElecGen');;

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		f.[MethodologyId],
		ele.[SubmissionId],
		f.[StandardId],
		@ProcessUnitId,
		f.[Coefficient] * ele.[Capacity_MW]
	FROM	[fact].[FacilitiesElecGeneration]		ele
	CROSS JOIN	[ante].[Factors]					f
	WHERE	ele.[SubmissionId]	= @SubmissionId
		AND	f.[MethodologyId]	= @MethodologyId
		AND	f.[StandardId]		= @StandardId
		AND	f.[FactorId]		= @FactorId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
			+ COALESCE(', @ProcessUnitId:'	+ CONVERT(VARCHAR, @ProcessUnitId),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;