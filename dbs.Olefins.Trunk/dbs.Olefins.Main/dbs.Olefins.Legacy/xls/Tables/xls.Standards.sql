﻿CREATE TABLE [xls].[Standards] (
    [Refnum]         VARCHAR (12)       NOT NULL,
    [StandardId]     INT                NOT NULL,
    [ProcessUnitId]  INT                NOT NULL,
    [StandardValue]  FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Standards_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Standards_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Standards_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Standards_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Standards] PRIMARY KEY CLUSTERED ([Refnum] DESC, [StandardId] ASC, [ProcessUnitId] ASC),
    CONSTRAINT [CL_Standards_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [FK_Standards_ProcessUnit] FOREIGN KEY ([ProcessUnitId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId]),
    CONSTRAINT [FK_Standards_Standard_LookUp] FOREIGN KEY ([StandardId]) REFERENCES [dim].[Standard_LookUp] ([StandardId])
);


GO

CREATE TRIGGER [xls].[t_Standards_u]
	ON [xls].[Standards]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[Standards]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[Standards].[Refnum]			= INSERTED.[Refnum]
		AND	[xls].[Standards].[StandardId]		= INSERTED.[StandardId]
		AND	[xls].[Standards].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;