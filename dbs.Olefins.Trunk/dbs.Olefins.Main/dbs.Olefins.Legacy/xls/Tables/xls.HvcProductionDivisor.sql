﻿CREATE TABLE [xls].[HvcProductionDivisor] (
    [Refnum]             VARCHAR (12)       NOT NULL,
    [HvcProdDivisor_kMT] FLOAT (53)         NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_HvcProductionDivisor_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_HvcProductionDivisor_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_HvcProductionDivisor_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_HvcProductionDivisor_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_HvcProductionDivisor] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_HvcProductionDivisor_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_HvcProductionDivisor_HvcProdDivisor_kMT_MinIncl_0.0] CHECK ([HvcProdDivisor_kMT]>=(0.0))
);


GO

CREATE TRIGGER [xls].[t_HvcProductionDivisor_u]
	ON [xls].[HvcProductionDivisor]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[HvcProductionDivisor]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[HvcProductionDivisor].[Refnum]	= INSERTED.[Refnum];

END;