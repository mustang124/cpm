﻿CREATE PROCEDURE [Console].[InsertValidationNotes]

	@RefNum dbo.Refnum,
	@Notes text

AS
BEGIN

	INSERT INTO Val.Notes (Refnum, ValidationNotes) VALUES (@RefNum, @Notes)

END
