﻿CREATE PROCEDURE [Console].[GetMessageLog]
    @MessageType int,
	@RefNum dbo.Refnum

AS
BEGIN

	SELECT * FROM dbo.MessageLog
	WHERE (Refnum = @RefNum OR Refnum = dbo.FormatRefNum(@Refnum, 0))
	AND MessageID = @MessageType 
	Order by MessageTime Desc
	
END
