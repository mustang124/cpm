﻿CREATE PROCEDURE [Console].[GetValidatorData]

	@RefNum dbo.Refnum,
	@Consultant varchar(5)

AS
BEGIN

	SELECT t.Consultant, /*rtrim(t.RefID) + ltrim(convert(varchar,t.StudyYear))*/ t.Refnum as [Ref Num], revNo as [Revision], CoLoc as [Company], Starttime as Start, stoptime as [End], convert(varchar,ValTime) +'s' as [Validation Time] 
	FROM [Val].[ValTime] v INNER JOIN TSort t ON dbo.FormatRefNum(t.Refnum,0) = dbo.FormatRefNum(v.Refnum,0)
    WHERE t.Consultant=@Consultant and t.Refnum = @RefNum

END
