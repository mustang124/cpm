﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem
{
	public partial class OsimControl : Form
	{
		public OsimControl()
		{
			InitializeComponent();
		}

		const string pathOsimTemplate = @"\\Dallas2\data\Data\STUDY\Olefins\2015\InputForms\OSIM2015 Master.xlsm";

		private List<RefnumPair> OsimTestList
		{
			get
			{
				List<RefnumPair> refnums = new List<RefnumPair>();

				refnums.Add(new RefnumPair("2015PCH012", "2013PCH012"));
				refnums.Add(new RefnumPair("2015PCH053", "2013PCH053"));
				refnums.Add(new RefnumPair("2015PCH099", "2013PCH099"));
				refnums.Add(new RefnumPair("2015PCH148", "2013PCH148"));
				refnums.Add(new RefnumPair("2015PCH152", "2013PCH152"));
				refnums.Add(new RefnumPair("2015PCH175", "2013PCH175"));
				refnums.Add(new RefnumPair("2015PCH196", "2013PCH196"));
				refnums.Add(new RefnumPair("2015PCH205", "2013PCH205"));
				refnums.Add(new RefnumPair("2015PCH222", "2013PCH222"));
				refnums.Add(new RefnumPair("2015PCH228", "2013PCH228"));
				refnums.Add(new RefnumPair("2015PCH231", "2013PCH231"));

				refnums.Add(new RefnumPair("2015PCH126", "2009PCH126"));
				refnums.Add(new RefnumPair("2015PCH147", string.Empty));
				refnums.Add(new RefnumPair("2015PCH181", "2011PCH181"));

				refnums.Add(new RefnumPair("2015PCH213", "2013PCH213"));
				refnums.Add(new RefnumPair("2015PCH214", "2013PCH214"));

				return refnums;
			}
		}

		private List<RefnumPair> OsimStudyCreation
		{
			get
			{
				List<RefnumPair> refnums = new List<RefnumPair>();
				string refnum;
				string previousRefnum;
				int r;
				int p;

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					using (SqlCommand cmd = new SqlCommand("[fact].[Select_Refnums]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;
						cn.Open();

						using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							r = rdr.GetOrdinal("Refnum");
							p = rdr.GetOrdinal("PreviousRefnum");

							while (rdr.Read())
							{
								refnum = rdr.GetString(r);

								if (!rdr.IsDBNull(p))
								{
									previousRefnum = rdr.GetString(p);
								}
								else
								{
									previousRefnum = string.Empty;
								}

								refnums.Add(new RefnumPair(refnum, previousRefnum));
							}
						}
						cn.Close();
					}
				}

				return refnums;
			}
		}

		private void btnUpload_Click(object sender, EventArgs e)
		{
			//string path = @"C:\Users\RRH\Desktop\Upload\OSIM2013PCH003.xlsm";
			//string refnum = "OSIM2013PCH003";

			string path = @"C:\Users\RRH\Desktop\Upload\OSIM2013PCH196.xlsm";
			string refnum = "OSIM2013PCH196";

			Excel.Application xla = XL.NewExcelApplication(false);
			Excel.Workbook wkb = XL.OpenWorkbook_ReadOnly(xla, path);

			Sa.Chem.Osim.Upload(wkb, refnum);

			XL.CloseExcel(ref xla, ref wkb);

			MessageBox.Show("DONE");
		}

		private void btnCreateTestDist_Click(object sender, EventArgs e)
		{
			Sa.Chem.Osim.Create(pathOsimTemplate, OsimTestList, false);
			MessageBox.Show("DONE");
		}

		private void btnCreateOneDist_Click(object sender, EventArgs e)
		{
			RefnumPair rp = new RefnumPair(txtCurrent.Text.Trim(), txtHistory.Text.Trim());
			Sa.Chem.Osim.Create(pathOsimTemplate, rp, false);
			MessageBox.Show("DONE");
		}

		private void btnCreateOneDcs_Click(object sender, EventArgs e)
		{
			RefnumPair rp = new RefnumPair(txtCurrent.Text.Trim(), txtHistory.Text.Trim());
			Sa.Chem.Osim.Create(pathOsimTemplate, rp, true);
			MessageBox.Show("DONE");
		}

		private void btnCreateAllDist_Click(object sender, EventArgs e)
		{
			string begTime = DateTime.Now.ToString();
			Sa.Chem.Osim.Create(pathOsimTemplate, OsimStudyCreation, false);
			string endTime = DateTime.Now.ToString();
			MessageBox.Show("DONE" + Environment.NewLine + Environment.NewLine + "BEG: " + begTime + Environment.NewLine + "END: " + endTime + Environment.NewLine);
		}

		private class Common
		{
			internal static string cnString()
			{
				return dbConnectionString("DBS5", "Olefins", "rrh", "rrh#4279");
				//return dbConnectionString(@"(localdb)\ProjectsV12", "dbs.Olefins");
			}

			internal static string dbConnectionString(string dbInst, string dbName)
			{
				string cn = "Data Source=" + dbInst + ";";
				cn = cn + "Initial Catalog=" + dbName + ";";
				cn = cn + "Integrated Security=True;";
				cn = cn + "MultipleActiveResultSets=True;";
				cn = cn + "Application Name = " + ProdNameVer() + ";";

				return cn;
			}

			internal static string dbConnectionString(string dbInst, string dbName, string UserName, string UserPass)
			{
				string cn = "Data Source=" + dbInst + ";";
				cn = cn + "Initial Catalog=" + dbName + ";";
				cn = cn + "User Id=" + UserName + ";";
				cn = cn + "Password=" + UserPass + ";";
				cn = cn + "MultipleActiveResultSets=True;";
				cn = cn + "Application Name = " + ProdNameVer() + ";";

				return cn;
			}

			// HACK: set Version number  = SELECT DATEDIFF(d, '1/1/2000', SYSDATETIME())
			private const string ver = "5507";

			internal static string ProdNameVer()
			{
				string rtn = string.Empty;

				try
				{
					Assembly ai = Assembly.GetExecutingAssembly();
					rtn = ai.GetName().Name + " (" + ai.GetName().Version + ")";
				}
				catch
				{
					rtn = "Chem.Upload.Cl 1.0." + ver + ".x";
				}

				return rtn;

			}

			internal static string ProdVer()
			{
				string rtn = string.Empty;

				try
				{
					Assembly ai = Assembly.GetExecutingAssembly();
					rtn = Convert.ToString(ai.GetName().Version);
				}
				catch
				{
					rtn = "1.0." + ver + ".x";
				}

				return rtn;
			}

			internal static string GetDateTimeStamp()
			{
				return DateTime.Now.ToString("yyyyMMdd.HHmmss");
			}
		}
	}
}