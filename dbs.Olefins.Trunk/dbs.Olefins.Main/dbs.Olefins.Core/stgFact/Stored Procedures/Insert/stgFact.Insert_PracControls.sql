﻿CREATE PROCEDURE [stgFact].[Insert_PracControls]
(
	@Refnum   VARCHAR (25),
	@ContType VARCHAR (3),

	@Tbl9038  CHAR (1) = NULL,
	@Tbl9039  CHAR (1) = NULL,
	@Tbl9040  CHAR (1) = NULL,
	@Tbl9041  CHAR (1) = NULL,
	@Tbl9042  CHAR (1) = NULL,
	@Tbl9043  CHAR (1) = NULL,
	@Tbl9044  CHAR (1) = NULL,
	@Tbl9045  CHAR (1) = NULL,
	@Tbl9046  CHAR (1) = NULL,
	@Tbl9047  CHAR (1) = NULL,
	@Tbl9048  CHAR (1) = NULL,
	@Tbl9049  CHAR (1) = NULL,
	@Tbl9050  CHAR (1) = NULL,
	@Tbl9052  CHAR (1) = NULL,
	@Tbl9053  CHAR (1) = NULL,
	@Tbl9054  CHAR (1) = NULL,
	@Tbl9055  CHAR (1) = NULL,
	@Tbl9056  CHAR (1) = NULL,
	@Tbl9057  CHAR (1) = NULL,
	@Tbl9058  CHAR (1) = NULL,
	@Tbl9059  CHAR (1) = NULL,
	@Tbl9060  CHAR (1) = NULL,
	@Tbl9062  CHAR (1) = NULL,
	@Tbl9063  CHAR (1) = NULL,
	@Tbl9064  CHAR (1) = NULL,
	@Tbl9065  CHAR (1) = NULL,
	@Tbl9066  CHAR (1) = NULL,
	@Tbl9067  CHAR (1) = NULL,
	@Tbl9068  CHAR (1) = NULL,
	@Tbl9069  CHAR (1) = NULL,
	@Tbl9070  CHAR (1) = NULL,
	@Tbl9071n CHAR (1) = NULL,
	@Tbl9072n CHAR (1) = NULL,
	@Tbl9073n CHAR (1) = NULL,
	@Tbl9074n CHAR (1) = NULL,
	@Tbl9075n CHAR (1) = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PracControls]([Refnum], [ContType], [Tbl9038], [Tbl9039], [Tbl9040], [Tbl9041], [Tbl9042], [Tbl9043], [Tbl9044], [Tbl9045], [Tbl9046], [Tbl9047], [Tbl9048], [Tbl9049], [Tbl9050], [Tbl9052], [Tbl9053], [Tbl9054], [Tbl9055], [Tbl9056], [Tbl9057], [Tbl9058], [Tbl9059], [Tbl9060], [Tbl9062], [Tbl9063], [Tbl9064], [Tbl9065], [Tbl9066], [Tbl9067], [Tbl9068], [Tbl9069], [Tbl9070], [Tbl9071n], [Tbl9072n], [Tbl9073n], [Tbl9074n], [Tbl9075n])
	VALUES(@Refnum, @ContType, @Tbl9038, @Tbl9039, @Tbl9040, @Tbl9041, @Tbl9042, @Tbl9043, @Tbl9044, @Tbl9045, @Tbl9046, @Tbl9047, @Tbl9048, @Tbl9049, @Tbl9050, @Tbl9052, @Tbl9053, @Tbl9054, @Tbl9055, @Tbl9056, @Tbl9057, @Tbl9058, @Tbl9059, @Tbl9060, @Tbl9062, @Tbl9063, @Tbl9064, @Tbl9065, @Tbl9066, @Tbl9067, @Tbl9068, @Tbl9069, @Tbl9070, @Tbl9071n, @Tbl9072n, @Tbl9073n, @Tbl9074n, @Tbl9075n);

END;