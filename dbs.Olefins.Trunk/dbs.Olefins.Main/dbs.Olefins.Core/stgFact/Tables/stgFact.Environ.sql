﻿CREATE TABLE [stgFact].[Environ] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Emis]           REAL               NULL,
    [Waste]          REAL               NULL,
    [NOx]            REAL               NULL,
    [Water]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Environ_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Environ_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Environ_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Environ_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Environ] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Environ_u]
	ON [stgFact].[Environ]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Environ]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Environ].Refnum		= INSERTED.Refnum;

END;