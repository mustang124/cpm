﻿CREATE TABLE [stgFact].[Maint01] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [ProjectId]      VARCHAR (15)       NOT NULL,
    [RoutMaintMatl]  REAL               NULL,
    [RoutMaintLabor] REAL               NULL,
    [RoutMaintTot]   REAL               NULL,
    [TAMatl]         REAL               NULL,
    [TALabor]        REAL               NULL,
    [TATot]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Maint01_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Maint01_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Maint01_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Maint01_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Maint01] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ProjectId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Maint01_u]
	ON [stgFact].[Maint01]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Maint01]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Maint01].Refnum		= INSERTED.Refnum
		AND	[stgFact].[Maint01].ProjectId	= INSERTED.ProjectId;

END;