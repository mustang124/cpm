﻿CREATE TABLE [stgFact].[CapGrowth] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Year]           INT                NOT NULL,
    [EthylProdn]     REAL               NULL,
    [PropylProdn]    REAL               NULL,
    [EthylCap]       REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_CapGrowth_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_CapGrowth_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_CapGrowth_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_CapGrowth_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_CapGrowth] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Year] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_CapGrowth_u]
	ON [stgFact].[CapGrowth]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[CapGrowth]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[CapGrowth].Refnum		= INSERTED.Refnum
		AND	[stgFact].[CapGrowth].[Year]		= INSERTED.[Year];

END;