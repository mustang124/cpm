﻿CREATE TABLE [stgFact].[OpEx] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [OCCSal]            REAL               NULL,
    [MpsSal]            REAL               NULL,
    [OCCBen]            REAL               NULL,
    [MpsBen]            REAL               NULL,
    [MaintMatl]         REAL               NULL,
    [ContMaintLabor]    REAL               NULL,
    [ContMaintMatl]     REAL               NULL,
    [ContTechSvc]       REAL               NULL,
    [Envir]             REAL               NULL,
    [OthCont]           REAL               NULL,
    [Equip]             REAL               NULL,
    [Tax]               REAL               NULL,
    [Insur]             REAL               NULL,
    [OthNonVol]         REAL               NULL,
    [STNonVol]          REAL               NULL,
    [TAAccrual]         REAL               NULL,
    [TACurrExp]         REAL               NULL,
    [STTaExp]           REAL               NULL,
    [Chemicals]         REAL               NULL,
    [Catalysts]         REAL               NULL,
    [Royalties]         REAL               NULL,
    [PurElec]           REAL               NULL,
    [PurSteam]          REAL               NULL,
    [PurCoolingWater]   REAL               NULL,
    [PurOth]            REAL               NULL,
    [PurFG]             REAL               NULL,
    [PurLiquid]         REAL               NULL,
    [PurSolid]          REAL               NULL,
    [PPCFuelGas]        REAL               NULL,
    [PPCFuelOth]        REAL               NULL,
    [SteamExports]      REAL               NULL,
    [PowerExports]      REAL               NULL,
    [OthVol]            REAL               NULL,
    [STVol]             REAL               NULL,
    [TotCashOpEx]       REAL               NULL,
    [InvenCarry]        REAL               NULL,
    [GANonPers]         REAL               NULL,
    [Depreciation]      REAL               NULL,
    [Interest]          REAL               NULL,
    [TotRefExp]         REAL               NULL,
    [ForexRate]         REAL               NULL,
    [NonMaintEquipRent] REAL               NULL,
    [ChemBFWCW]         REAL               NULL,
    [ChemWW]            REAL               NULL,
    [ChemCaustic]       REAL               NULL,
    [ChemPretreatment]  REAL               NULL,
    [ChemAntiFoul]      REAL               NULL,
    [ChemOthProc]       REAL               NULL,
    [ChemAdd]           REAL               NULL,
    [ChemOth]           REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_OpEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_OpEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_OpEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_stgFact_OpEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_OpEx] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_OpEx_u]
	ON [stgFact].[OpEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[OpEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[OpEx].Refnum		= INSERTED.Refnum;

END;