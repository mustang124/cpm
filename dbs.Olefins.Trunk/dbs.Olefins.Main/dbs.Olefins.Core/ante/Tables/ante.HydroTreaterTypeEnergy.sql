﻿CREATE TABLE [ante].[HydroTreaterTypeEnergy] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [HTTypeId]          VARCHAR (2)        NOT NULL,
    [StdEnergy_kBTUbbl] REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_HydroTreaterTypeEnergy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_HydroTreaterTypeEnergy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_HydroTreaterTypeEnergy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_HydroTreaterTypeEnergy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_HydroTreaterTypeEnergy] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [HTTypeId] ASC),
    CONSTRAINT [CR_HydroTreaterTypeEnergy_StdEnergy] CHECK ([StdEnergy_kBTUbbl]>=(0.0)),
    CONSTRAINT [FK_HydroTreaterTypeEnergy_FacilitiesHydroTreaterType_LookUp] FOREIGN KEY ([HTTypeId]) REFERENCES [dim].[FacilityHydroTreaterType_LookUp] ([HTTypeId]),
    CONSTRAINT [FK_HydroTreaterTypeEnergy_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_HydroTreaterTypeEnergy_u]
	ON [ante].[HydroTreaterTypeEnergy]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[HydroTreaterTypeEnergy]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[HydroTreaterTypeEnergy].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[HydroTreaterTypeEnergy].HTTypeId		= INSERTED.HTTypeId;

END;