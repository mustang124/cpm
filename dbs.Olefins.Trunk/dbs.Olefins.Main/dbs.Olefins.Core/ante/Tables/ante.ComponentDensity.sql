﻿CREATE TABLE [ante].[ComponentDensity] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [Density_SG]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ComponentDensity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ComponentDensity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ComponentDensity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ComponentDensity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ComponentDensity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
    CONSTRAINT [CK_ComponentDensity_Density_SG] CHECK ([Density_SG]>=(0.0)),
    CONSTRAINT [FK_ComponentDensity_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_ComponentDensity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_ComponentDensity_u]
	ON [ante].[ComponentDensity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ComponentDensity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[ComponentDensity].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[ComponentDensity].ComponentId	= INSERTED.ComponentId;

END;
