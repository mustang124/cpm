﻿CREATE TABLE [ante].[NicenessStreamCoefficients] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [Coefficient]    REAL               NOT NULL,
    [HistMin]        REAL               NOT NULL,
    [HistMax]        REAL               NOT NULL,
    [AllowMin]       REAL               NOT NULL,
    [AllowMax]       REAL               NOT NULL,
    [EnergyMin]      REAL               NOT NULL,
    [EnergyMax]      REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessStreamCoefficients_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_NicenessStreamCoefficients_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_NicenessStreamCoefficients_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_NicenessStreamCoefficients_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_NicenessStreamCoefficients] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CONSTRAINT [FK_NicenessStreamCoefficients_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_NicenessStreamCoefficients_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_NicenessStreamCoefficients_u]
	ON [ante].[NicenessStreamCoefficients]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessStreamCoefficients]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[NicenessStreamCoefficients].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[NicenessStreamCoefficients].[StreamId]		= INSERTED.[StreamId];

END;
