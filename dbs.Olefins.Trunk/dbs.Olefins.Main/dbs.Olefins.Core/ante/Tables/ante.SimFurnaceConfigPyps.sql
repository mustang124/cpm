﻿CREATE TABLE [ante].[SimFurnaceConfigPyps] (
    [FactorSetId]              VARCHAR (12)       NOT NULL,
    [StreamId]                 VARCHAR (42)       NOT NULL,
    [FeedType]                 TINYINT            NOT NULL,
    [SteamHydrocarbon_Ratio]   REAL               NOT NULL,
    [CoilOutletPressure_kgcm2] REAL               NOT NULL,
    [FeedConvId]               REAL               NULL,
    [FeedConv_Pcnt]            REAL               NULL,
    [PropyleneMethane_Ratio]   REAL               NULL,
    [Density_SG]               REAL               NULL,
    [P]                        REAL               NULL,
    [I]                        REAL               NULL,
    [A]                        REAL               NULL,
    [N]                        REAL               NULL,
    [O]                        REAL               NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_SimFurnaceConfigPyps_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceConfigPyps_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceConfigPyps_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceConfigPyps_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SimFurnaceConfigPyps] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CONSTRAINT [CR_SimFurnaceConfigPyps_A] CHECK ([A]>=(0.0) AND [A]<=(100.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_CoilOutletPressure_kgcm2] CHECK ([CoilOutletPressure_kgcm2]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_Density_SG] CHECK ([Density_SG]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_FeedConv_Pcnt] CHECK ([FeedConv_Pcnt]>=(0.0) AND [FeedConv_Pcnt]<=(100.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_I] CHECK ([I]>=(0.0) AND [I]<=(100.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_N] CHECK ([N]>=(0.0) AND [N]<=(100.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_O] CHECK ([O]>=(0.0) AND [O]<=(100.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_P] CHECK ([P]>=(0.0) AND [P]<=(100.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_PropyleneMethane_Ratio] CHECK ([PropyleneMethane_Ratio]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigPyps_SteamHydrocarbon_Ratio] CHECK ([SteamHydrocarbon_Ratio]>=(0.0)),
    CONSTRAINT [FK_SimFurnaceConfigPyps_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_SimFurnaceConfigPyps_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_SimFurnaceConfigPyps_u]
	ON [ante].[SimFurnaceConfigPyps]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[SimFurnaceConfigPyps]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[SimFurnaceConfigPyps].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[SimFurnaceConfigPyps].StreamId		= INSERTED.StreamId;

END;