﻿CREATE TABLE [ante].[PeerGroupCogen] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [LowerLimit_MW]  REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupCogen_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupCogen_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupCogen_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupCogen_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PeerGroupCogen] PRIMARY KEY CLUSTERED ([FactorSetId] ASC),
    CONSTRAINT [FK_PeerGroupCogen_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO






CREATE TRIGGER ante.[t_PeerGroupCogen_u]
	ON [ante].[PeerGroupCogen]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PeerGroupCogen]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PeerGroupCogen].[FactorSetId]		= INSERTED.[FactorSetId]

END;