﻿CREATE TABLE [ante].[MapNicenessStreamDensity] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [MinDensity_SG]  REAL               NOT NULL,
    [MaxDensity_SG]  REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapNicenessStreamDensity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MapNicenessStreamDensity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MapNicenessStreamDensity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MapNicenessStreamDensity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MapNicenessStreamDensity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CHECK ([MaxDensity_SG]>=(0.0)),
    CHECK ([MinDensity_SG]>=(0.0)),
    CONSTRAINT [CK_MapNicenessStreamDensity_Density] CHECK ([MinDensity_SG]<[MaxDensity_SG]),
    CONSTRAINT [FK_MapNicenessStreamDensity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MapNicenessStreamDensity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_MapNicenessStreamDensity_u]
	ON [ante].[MapNicenessStreamDensity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapNicenessStreamDensity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[MapNicenessStreamDensity].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[MapNicenessStreamDensity].[StreamId]	= INSERTED.[StreamId];

END;
