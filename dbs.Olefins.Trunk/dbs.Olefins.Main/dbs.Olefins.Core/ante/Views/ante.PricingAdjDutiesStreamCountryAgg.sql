﻿CREATE VIEW ante.PricingAdjDutiesStreamCountryAgg
WITH SCHEMABINDING
AS
SELECT
	c.[FactorSetId],
	c.CountryId,
	c.CalDateKey,
	'Logistics'			[AccountId],
	c.CurrencyRpt,
	c.StreamId,
	SUM(c.Amount_Cur)	[Amount_Cur]
FROM ante.PricingAdjDutiesStreamCountry c
GROUP BY
	c.[FactorSetId],
	c.CountryId,
	c.CalDateKey,
	c.CurrencyRpt,
	c.StreamId;
