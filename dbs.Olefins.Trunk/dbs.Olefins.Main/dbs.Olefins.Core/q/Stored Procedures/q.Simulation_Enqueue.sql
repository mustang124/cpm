﻿
CREATE PROCEDURE q.Simulation_Enqueue
AS 
	SELECT
		  q.QueueId
		, q.Refnum
		, q.FactorSetId
		, q.SimModelId
	FROM q.Simulation q
	WHERE	q.tsQueued		IS NULL
		AND	q.tsComplete	IS NULL
	ORDER BY q.QueueId ASC;
