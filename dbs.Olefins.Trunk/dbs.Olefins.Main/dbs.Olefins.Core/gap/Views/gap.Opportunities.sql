﻿CREATE VIEW [gap].[Opportunities]
WITH SCHEMABINDING
AS
SELECT
	a.[FactorSetId],
	a.[GroupId],
	a.[TargetId],
	[Opportunity]		= SUM(a.[GapAmount_Cur]),
	[Opportunity_HVCMT]	= SUM(a.[GapAmount_Div])
FROM [gap].[AnalysisResultsDiv]	a
WHERE	a.[GapId] in (SELECT d.[DescendantId] FROM [dim].[Gap_Bridge] d WHERE d.[FactorSetId] = a.[FactorSetId] AND d.[GapId] = 'EconGaps')
	AND	a.GapAmount_Cur < 0.0
	AND	a.GapAmount_Div < 0.0
GROUP BY
	a.[FactorSetId],
	a.[GroupId],
	a.[TargetId];
