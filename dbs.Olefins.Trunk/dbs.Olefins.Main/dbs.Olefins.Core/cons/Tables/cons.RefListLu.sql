﻿CREATE TABLE [cons].[RefListLu] (
    [ListId]         VARCHAR (42)        NOT NULL,
    [ListName]       NVARCHAR (84)       NOT NULL,
    [ListDetail]     NVARCHAR (192)      NOT NULL,
    [BaseList_Bit]   BIT                 CONSTRAINT [DF_RefListLu_BaseList_Bit] DEFAULT ((0)) NOT NULL,
    [JobOwner]       NVARCHAR (256)      NULL,
    [JobNumber]      NVARCHAR (84)       NULL,
    [SortKey]        INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_RefListLu_Operator] DEFAULT ('~') NOT NULL,
    [Parent]         VARCHAR (42)        NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_RefListLu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)      CONSTRAINT [DF_RefListLu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)      CONSTRAINT [DF_RefListLu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)      CONSTRAINT [DF_RefListLu_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_RefListLu] PRIMARY KEY CLUSTERED ([ListId] ASC),
    CONSTRAINT [CL_RefListLu_JobNumber] CHECK ([JobNumber]<>''),
    CONSTRAINT [CL_RefListLu_JobOwner] CHECK ([JobOwner]<>''),
    CONSTRAINT [CL_RefListLu_ListDetail] CHECK ([ListDetail]<>''),
    CONSTRAINT [CL_RefListLu_ListId] CHECK ([ListId]<>''),
    CONSTRAINT [CL_RefListLu_ListName] CHECK ([ListName]<>''),
    CONSTRAINT [CR_RefListLu_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_RefListLu_Parents] FOREIGN KEY ([Parent]) REFERENCES [cons].[RefListLu] ([ListId]),
    CONSTRAINT [UK_RefListLu_ListDetail] UNIQUE NONCLUSTERED ([ListDetail] ASC),
    CONSTRAINT [UK_RefListLu_ListName] UNIQUE NONCLUSTERED ([ListName] ASC)
);


GO

CREATE TRIGGER [cons].[t_RefListLu_u]
	ON [cons].[RefListLu]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[RefListLu]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[RefListLu].ListId		= INSERTED.ListId;

END;