﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonStandard_LightFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonStandard(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_kMT, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			'PYPS',
			fpl.Plant_AnnDateKey,
			c.StreamId				[EmissionsId],
			SUM(c.Component_kMT),
			MAX(CASE WHEN c.ComponentId = 'CO_CO2' THEN c.Component_WtPcnt END),
			SUM(CASE WHEN c.ComponentId = 'CO_CO2' THEN c.Component_kMT END)
		FROM @fpl									fpl
		INNER JOIN calc.CompositionStream			c
			ON	c.FactorSetId = fpl.FactorSetId
			AND	c.Refnum = fpl.Refnum
			AND	c.CalDateKey = fpl.Plant_QtrDateKey
			AND	c.SimModelId = 'Plant'
			AND	c.OpCondId = 'PlantFeed'
			AND c.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'Light')
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			c.StreamId,
			c.SimModelId,
			c.OpCondId
		HAVING SUM(CASE WHEN c.ComponentId = 'CO_CO2' THEN c.Component_kMT END) IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;