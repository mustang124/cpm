﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonStandard_Direct]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonStandard(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_MBtu, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			eei.SimModelId,
			fpl.Plant_QtrDateKey,
			stD.EmissionsId,
			ecp.Quantity_MBtu * esd.EeiScaleDown / eei._Eei * stD.Energy_Pcnt,

			COALESCE(efM.CEF_MTCO2_MBtu, efE.CEF_MTCO2_MBtu * 44.01 / 12.01 / 9.09, 0.0)	[CefGwp],

			ecp.Quantity_MBtu * esd.EeiScaleDown / eei._Eei * stD.Energy_Pcnt * 
			COALESCE(efM.CEF_MTCO2_MBtu, efE.CEF_MTCO2_MBtu * 44.01 / 12.01 / 9.09, 0.0)	[CEF_MTCO2_MBtu]
		FROM @fpl										fpl
		INNER JOIN calc.Eei								eei
			ON	eei.FactorSetId		= fpl.FactorSetId
			AND	eei.Refnum			= fpl.Refnum
			AND	eei.CalDateKey		= fpl.Plant_QtrDateKey
			AND	eei.OpCondId		= 'OS25'
			AND	eei.ComponentId		= 'ProdHVC'
			AND	eei.SimModelId		= 'PYPS'
		INNER JOIN ante.EeiScaleDown					esd
			ON	esd.FactorSetId		= fpl.FactorSetId
			AND	esd.SimModelId		= eei.SimModelId
		INNER JOIN calc.EmissionsCarbonPlantAggregate	ecp
			ON	ecp.FactorSetId		= fpl.FactorSetId
			AND	ecp.Refnum			= fpl.Refnum
			AND	ecp.CalDateKey		= fpl.Plant_QtrDateKey
			AND	ecp.EmissionsId		= 'NetEmissions'
		INNER JOIN ante.EmissionsStdEnergyDistribution	stD
			ON	stD.FactorSetId		= fpl.FactorSetId
		LEFT OUTER JOIN ante.EmissionsFactorCarbon		efM
			ON	efM.FactorSetId		= fpl.FactorSetId
			AND	efM.EmissionsId		= stD.EmissionsId
		LEFT OUTER JOIN ante.EmissionsFactorElectricity	efE
			ON	efE.FactorSetId		= fpl.FactorSetId
			AND	efE.CountryId		= fpl.CountryId
			AND	efE.EmissionsId		= stD.EmissionsId
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;