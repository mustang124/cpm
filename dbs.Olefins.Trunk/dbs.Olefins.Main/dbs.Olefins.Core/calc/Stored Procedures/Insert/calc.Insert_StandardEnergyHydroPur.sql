﻿CREATE PROCEDURE [calc].[Insert_StandardEnergyHydroPur]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		-------------------------------------------------------------------------------
		--	F333: HydroPurification
		
		INSERT INTO [calc].[StandardEnergyHydroPur]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [Quantity_kMT], [Component_WtPcnt], [kSCF], [kSCFDay], [StandardEnergy_MBtuDay])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'HydroPur',
			ABS(sqa.[Quantity_kMT])	[Quantity_kMT],
			c.[Component_WtPcnt],
			(423.3 + 49.33 * 3.0) * 1.1 * ABS(sqa.[Quantity_kMT]) * c.[Component_WtPcnt] / 100.0				[kSCF],
			(423.3 + 49.33 * 3.0) * 1.1 * ABS(sqa.[Quantity_kMT]) * c.[Component_WtPcnt] * 10.0 / 365.0			[kSCFDay],
			(423.3 + 49.33 * 3.0) * 1.1 * ABS(sqa.[Quantity_kMT]) * c.[Component_WtPcnt] / 100.0 / 365.0 * 20.0 [StandardEnergy_MBtuDay]
		FROM @fpl										fpl
		INNER JOIN [fact].[FacilitiesCount]				fc
			ON	fc.[FactorSetId]		= fpl.[FactorSetId]
			AND	fc.[Refnum]				= fpl.[Refnum]
			AND	fc.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			AND	fc.[FacilityId]			= 'HydroPur'
			AND	fc.[Unit_Count]			> 0
		INNER JOIN [fact].[StreamQuantityAggregate]		sqa
			ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
			AND	sqa.[Refnum]			= fpl.[Refnum]
			AND	sqa.[StreamId]			= 'Hydrogen'
			AND	sqa.[Quantity_kMT]		<> 0.0
		INNER JOIN [fact].[CompositionQuantity]			c
			ON	c.[Refnum]				= fpl.[Refnum]
			AND	c.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			AND	c.[StreamId]			= 'Hydrogen'
			AND	c.[ComponentId]			= 'H2'
			AND	c.[Component_WtPcnt]	> 40.0
		WHERE	fpl.[CalQtr]			= 4
			AND	fpl.[FactorSet_AnnDateKey] > 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;