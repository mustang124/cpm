﻿CREATE PROCEDURE [calc].[Insert_Efficiency_RedPropyleneCG]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Efficiency([FactorSetId], [Refnum], [CalDateKey], [EfficiencyId], [UnitId], [EfficiencyStandard])
		SELECT
			t.[FactorSetId],
			t.[Refnum],
			t.[Plant_AnnDateKey],
			wCdu.[EfficiencyId],
			pcg.[UnitId],
			(
				pcg.[Coefficient] * POWER(t.[Stream_bbl] / CONVERT(FLOAT, tra.[Train_Count]), pcg.[Exponent]) -
				prg.[Coefficient] * POWER(t.[Stream_bbl] / CONVERT(FLOAT, tra.[Train_Count]), prg.[Exponent])
			)
			* t.[PropyleneCG_bsd]
			* wCdu.[Coefficient] / 1000.0
		FROM (
			SELECT
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey],

				(CASE WHEN ccg.[Component_WtPcnt] >= 90.0
				THEN ABS(qcg.[Quantity_kMT])
				ELSE 0.0
				END +
				CASE WHEN crg.[Component_WtPcnt] >= 90.0
				THEN ABS(qrg.[Quantity_kMT])
				ELSE 0.0
				END)
				/ cutil.[_UtilizationStream_Pcnt] * 100.0 * 1.46 * 10.24 * 1000.0 / 365.0
												[PropyleneCG_bsd],

				c.[Stream_MTd] * 10.24 * 1.54	[Stream_bbl]

			FROM @fpl											fpl
			INNER JOIN [calc].[PeerGroupFeedClass]				fc
				ON	fc.[FactorSetId]	= fpl.[FactorSetId]
				AND	fc.[Refnum]			= fpl.[Refnum]
				AND	fc.[CalDateKey]		= fpl.[Plant_AnnDateKey]
			LEFT OUTER JOIN [super].[PeerGroupFeedClass]		sf
				ON	sf.[FactorSetId]	= fpl.[FactorSetId]
				AND	sf.[Refnum]			= fpl.[Refnum]
				AND	sf.[CalDateKey]		= fpl.[Plant_AnnDateKey]

			INNER JOIN [fact].[Capacity]						c
				ON	c.[Refnum]			= fpl.[Refnum]
				AND	c.[CalDateKey]		= fpl.[Plant_AnnDateKey]
				AND	c.[StreamId]		= 'Propylene'

			INNER JOIN [calc].[CapacityUtilization]				cUtil
				ON	cUtil.[FactorSetId]	= fpl.[FactorSetId]
				AND	cUtil.[Refnum]		= fpl.[Refnum]
				AND	cUtil.[CalDateKey]	= fpl.[Plant_AnnDateKey]
				AND	cUtil.[SchedId]		= 'P'
				AND	cUtil.[StreamId]	= 'ProdOlefins'

			LEFT OUTER JOIN [fact].[StreamQuantityAggregate]	qcg	WITH (NOEXPAND)
				ON	qcg.[FactorSetId]	= fpl.[FactorSetId]
				AND	qcg.[Refnum]		= fpl.[Refnum]
				AND	qcg.[StreamId]		= 'PropyleneCG'

			LEFT OUTER JOIN [fact].CompositionQuantity			ccg
				ON	ccg.[Refnum]		= fpl.[Refnum]
				AND	ccg.[StreamId]		= qcg.[StreamId]
				AND	ccg.[ComponentId]	= 'C3H6'

			LEFT OUTER JOIN [fact].[StreamQuantityAggregate]	qrg	WITH (NOEXPAND)
				ON	qrg.[FactorSetId]	= fpl.[FactorSetId]
				AND	qrg.[Refnum]		= fpl.[Refnum]
				AND	qrg.[StreamId]		= 'PropyleneRG'

			LEFT OUTER JOIN [fact].CompositionQuantity			crg
				ON	crg.[Refnum]		= fpl.[Refnum]
				AND	crg.[StreamId]		= qrg.[StreamId]
				AND	crg.[ComponentId]	= 'C3H6'

			WHERE	fpl.CalQtr			= 4
				AND COALESCE(sf.[PeerGroup], fc.[PeerGroup])	<> 1
			) t
		INNER JOIN [fact].[ReliabilityTAAggregate]		tra
			ON	tra.[Refnum]		= t.[Refnum]
			AND	tra.[CalDateKey]	= t.[Plant_AnnDateKey]
			AND	tra.[SchedId]		= 'P'

		INNER JOIN ante.UnitEfficiencyWWCdu					wCdu
			ON	wCdu.[FactorSetId]	= t.[FactorSetId]

		INNER JOIN ante.UnitEfficiencyFactors			pcg
			ON	pcg.[FactorSetId]	= t.[FactorSetId]
			AND	pcg.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	pcg.[UnitId]		= 'RedPropyleneCG'

		INNER JOIN ante.UnitEfficiencyFactors			prg
			ON	prg.[FactorSetId]	= t.[FactorSetId]
			AND	prg.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	prg.[UnitId]		= 'RedPropyleneRG'

		WHERE t.[PropyleneCG_bsd] > 0.0;
		
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;