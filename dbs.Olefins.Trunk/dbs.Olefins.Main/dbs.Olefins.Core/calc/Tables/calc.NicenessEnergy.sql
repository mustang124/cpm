﻿CREATE TABLE [calc].[NicenessEnergy] (
    [FactorSetId]         VARCHAR (12)       NOT NULL,
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [StreamId]            VARCHAR (42)       NOT NULL,
    [Stream_kMT]          REAL               NOT NULL,
    [Stream_Pcnt]         REAL               NOT NULL,
    [NicenessYield_kMT]   REAL               NOT NULL,
    [NicenessYield_Pcnt]  REAL               NOT NULL,
    [NicenessYield_Tot]   REAL               NULL,
    [NicenessEnergy_kMT]  REAL               NOT NULL,
    [NicenessEnergy_Pcnt] REAL               NOT NULL,
    [NicenessEnergy_Tot]  REAL               NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessEnergy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_NicenessEnergy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_NicenessEnergy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_NicenessEnergy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [FK_NicenessEnergy_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_NicenessEnergy_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_NicenessEnergy_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_NicenessEnergy_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_NicenessEnergy_u
	ON  calc.NicenessEnergy
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.NicenessEnergy
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.NicenessEnergy.[FactorSetId]	= INSERTED.[FactorSetId]
		AND calc.NicenessEnergy.[Refnum]		= INSERTED.[Refnum]
		AND calc.NicenessEnergy.[CalDateKey]	= INSERTED.[CalDateKey]
		AND calc.NicenessEnergy.[StreamId]		= INSERTED.[StreamId];
				
END;
