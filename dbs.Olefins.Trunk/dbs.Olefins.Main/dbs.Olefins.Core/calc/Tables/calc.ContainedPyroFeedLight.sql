﻿CREATE TABLE [calc].[ContainedPyroFeedLight] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [ContainedComponentId] VARCHAR (42)       NOT NULL,
    [Feed_kMT]             REAL               NULL,
    [Recycled_kMT]         REAL               NULL,
    [Total_kMT]            REAL               NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_ContainedPyroFeedLight_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_ContainedPyroFeedLight_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_ContainedPyroFeedLight_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_ContainedPyroFeedLight_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ContainedPyroFeedLight] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ContainedComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ContainedPyroFeedLight_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ContainedPyroFeedLight_Component_LookUp] FOREIGN KEY ([ContainedComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_ContainedPyroFeedLight_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ContainedPyroFeedLight_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ContainedPyroFeedLight_u
	ON  calc.[ContainedPyroFeedLight]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[ContainedPyroFeedLight]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[ContainedPyroFeedLight].FactorSetId			= INSERTED.FactorSetId
		AND calc.[ContainedPyroFeedLight].Refnum					= INSERTED.Refnum
		AND calc.[ContainedPyroFeedLight].CalDateKey				= INSERTED.CalDateKey
		AND calc.[ContainedPyroFeedLight].ContainedComponentId	= INSERTED.ContainedComponentId;
				
END