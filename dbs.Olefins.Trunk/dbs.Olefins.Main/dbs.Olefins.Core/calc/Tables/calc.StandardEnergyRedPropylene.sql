﻿CREATE TABLE [calc].[StandardEnergyRedPropylene] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [StandardEnergyId]       VARCHAR (42)       NOT NULL,
    [Quantity_kMT]           REAL               NOT NULL,
    [StandardEnergy_MBtuDay] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyRedPropylene_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyRedPropylene_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyRedPropylene_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyRedPropylene_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyRedPropylene] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StandardEnergyId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_StandardEnergyRedPropylene_Quantity_kMT] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyRedPropylene_StandardEnergy_MBtuDay] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [FK_StandardEnergyRedPropylene_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_StandardEnergyRedPropylene_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergyRedPropylene_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergyRedPropylene_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_CalcStandardEnergyRedPropylene_u
	ON  calc.StandardEnergyRedPropylene
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.StandardEnergyRedPropylene
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.StandardEnergyRedPropylene.[FactorSetId]		= INSERTED.[FactorSetId]
		AND	calc.StandardEnergyRedPropylene.[Refnum]			= INSERTED.[Refnum]
		AND calc.StandardEnergyRedPropylene.[StandardEnergyId]	= INSERTED.[StandardEnergyId]
		AND calc.StandardEnergyRedPropylene.[CalDateKey]		= INSERTED.[CalDateKey];
				
END;