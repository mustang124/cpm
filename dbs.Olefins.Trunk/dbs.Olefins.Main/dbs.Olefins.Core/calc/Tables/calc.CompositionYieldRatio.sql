﻿CREATE TABLE [calc].[CompositionYieldRatio] (
    [FactorSetId]             VARCHAR (12)       NOT NULL,
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [SimModelId]              VARCHAR (12)       NOT NULL,
    [OpCondId]                VARCHAR (12)       NOT NULL,
    [RecycleId]               INT                NOT NULL,
    [ComponentId]             VARCHAR (42)       NOT NULL,
    [Component_kMT]           REAL               NULL,
    [Component_WtPcnt]        REAL               NULL,
    [Component_Extrap_kMT]    REAL               NULL,
    [Component_Extrap_WtPcnt] REAL               NULL,
    [Component_Supp_kMT]      REAL               NULL,
    [Component_Div_kMT]       REAL               NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_Calc_CompositionYieldRatio_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_Calc_CompositionYieldRatio_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_Calc_CompositionYieldRatio_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_Calc_CompositionYieldRatio_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Calc_CompostionYieldRatio] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC, [RecycleId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CompositionYieldRatio_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CompositionYieldRatio_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_CompositionYieldRatio_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CompositionYieldRatio_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_CompositionYieldRatio_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_calc_CompositionYieldRatio_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_CompositionYieldRatio_Improvement]
    ON [calc].[CompositionYieldRatio]([SimModelId] ASC, [OpCondId] ASC, [Component_kMT] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [RecycleId], [ComponentId]);


GO
CREATE NONCLUSTERED INDEX [IX_CompositionYieldRatio_Improvement_SimModelId]
    ON [calc].[CompositionYieldRatio]([SimModelId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [OpCondId], [RecycleId], [ComponentId], [Component_Extrap_kMT]);


GO
CREATE TRIGGER [calc].[t_calc_CompositionYieldRatio_u]
	ON [calc].[CompositionYieldRatio]
	AFTER UPDATE 
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[CompositionYieldRatio]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[CompositionYieldRatio].[FactorSetId]	= INSERTED.[FactorSetId]
		AND calc.[CompositionYieldRatio].[Refnum]		= INSERTED.[Refnum]
		AND calc.[CompositionYieldRatio].[CalDateKey]	= INSERTED.[CalDateKey]
		AND calc.[CompositionYieldRatio].[SimModelId]	= INSERTED.[SimModelId]
		AND calc.[CompositionYieldRatio].[OpCondId]		= INSERTED.[OpCondId]
		AND calc.[CompositionYieldRatio].[RecycleId]	= INSERTED.[RecycleId];
			
END