﻿CREATE TABLE [calc].[ApcIndexApplications] (
    [FactorSetId]             VARCHAR (12)       NOT NULL,
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [ApcId]                   VARCHAR (42)       NOT NULL,
    [AbsenceCorrectionFactor] REAL               NOT NULL,
    [OnLine_Pcnt]             REAL               NOT NULL,
    [Mpc_Int]                 INT                NOT NULL,
    [Sep_Int]                 INT                NOT NULL,
    [Mpc_Value]               REAL               NOT NULL,
    [Sep_Value]               REAL               NOT NULL,
    [_Abs_Mpc_Value]          AS                 (CONVERT([real],case when [AbsenceCorrectionFactor]<>(0.0) then [Mpc_Value]/[AbsenceCorrectionFactor]  end,(1))) PERSISTED NOT NULL,
    [_Abs_Sep_Value]          AS                 (CONVERT([real],case when [AbsenceCorrectionFactor]<>(0.0) then [Sep_Value]/[AbsenceCorrectionFactor]  end,(1))) PERSISTED NOT NULL,
    [_Apc_Index]              AS                 (CONVERT([real],case when [Mpc_Int]>=(1) then [Mpc_Value] else case when [Sep_Int]>=(1) then [Sep_Value] else (0.0) end end/[AbsenceCorrectionFactor],(1))) PERSISTED NOT NULL,
    [_ApcOnLine_Index]        AS                 (CONVERT([real],((case when [Mpc_Int]>=(1) then [Mpc_Value] else case when [Sep_Int]>=(1) then [Sep_Value] else (0.0) end end/[AbsenceCorrectionFactor])*[OnLine_Pcnt])/(100.0),(1))) PERSISTED NOT NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_ApcIndexApplications_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_ApcIndexApplications_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_ApcIndexApplications_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_ApcIndexApplications_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ApcIndexApplications] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ApcId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ApcIndexApplications_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_calc_ApcIndexApplications_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ApcIndexApplications_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ApcIndexApplications_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_ApcIndexApplications_u
	ON  calc.ApcIndexApplications
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ApcIndexApplications
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ApcIndexApplications.FactorSetId	= INSERTED.FactorSetId
		AND calc.ApcIndexApplications.Refnum		= INSERTED.Refnum
		AND calc.ApcIndexApplications.CalDateKey	= INSERTED.CalDateKey
		AND calc.ApcIndexApplications.ApcId			= INSERTED.ApcId;
				
END;