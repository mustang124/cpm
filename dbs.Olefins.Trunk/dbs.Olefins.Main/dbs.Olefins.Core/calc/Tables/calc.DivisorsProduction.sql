﻿CREATE TABLE [calc].[DivisorsProduction] (
    [FactorSetId]                 VARCHAR (12)       NOT NULL,
    [Refnum]                      VARCHAR (25)       NOT NULL,
    [CalDateKey]                  INT                NOT NULL,
    [ComponentId]                 VARCHAR (42)       NOT NULL,
    [ProductionContained_kMT]        REAL               NOT NULL,
    [ProductionContained_WtPcnt]     REAL               NOT NULL,
    [_ProductionContained_klb]       AS                 (CONVERT([real],[ProductionContained_kMT]*(2.2046),(1))) PERSISTED NOT NULL,
    [ProductionSold_kMT]              REAL               NULL,
    [ProductionSold_WtPcnt]           REAL               NULL,
    [_ProductionSold_klb]             AS                 (CONVERT([real],[ProductionSold_kMT]*(2.2046),(1))) PERSISTED,
    [TaAdj_Production_Ratio]      REAL               NOT NULL,
    [_TaAdj_ProductionContained_kMT] AS                 (CONVERT([real],[TaAdj_Production_Ratio]*[ProductionContained_kMT],(1))) PERSISTED NOT NULL,
    [_TaAdj_ProductionContained_klb] AS                 (CONVERT([real],([TaAdj_Production_Ratio]*[ProductionContained_kMT])*(2.2046),(1))) PERSISTED NOT NULL,
    [_TaAdj_ProductionSold_kMT]       AS                 (CONVERT([real],[TaAdj_Production_Ratio]*[ProductionSold_kMT],(1))) PERSISTED,
    [_TaAdj_ProductionSold_klb]       AS                 (CONVERT([real],([TaAdj_Production_Ratio]*[ProductionSold_kMT])*(2.2046),(1))) PERSISTED,
    [tsModified]                  DATETIMEOFFSET (7) CONSTRAINT [DF_DivisorsProduction_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]              NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]              NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]               NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_DivisorsProduction] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_DivisorsProduction_ProductionSold_kMT] CHECK ([ProductionSold_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_ProductionSold_WtPcnt] CHECK ([ProductionSold_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_ProductionContained_kMT] CHECK ([ProductionContained_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_ProductionContained_WtPcnt] CHECK ([ProductionContained_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_TaAdj_Production_Ratio] CHECK ([TaAdj_Production_Ratio]>=(1.0)),
    CONSTRAINT [FK_calc_DivisorsProduction_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_DivisorsProduction_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_DivisorsProduction_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_DivisorsProduction_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_DivisorsProduction_u
	ON  calc.DivisorsProduction
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.DivisorsProduction
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.DivisorsProduction.FactorSetId		= INSERTED.FactorSetId
		AND calc.DivisorsProduction.Refnum			= INSERTED.Refnum
		AND calc.DivisorsProduction.CalDateKey		= INSERTED.CalDateKey
		AND calc.DivisorsProduction.ComponentId		= INSERTED.ComponentId;
				
END