﻿CREATE VIEW [calc].[MaintPersAvgAggregate]
WITH SCHEMABINDING
AS
SELECT
	mpa.FactorSetId,
	mpa.Refnum,
	mpa.CalDateKey,
	SUM(mpa.MaintPrev_Hrs)	[MaintPrev_Hrs],
	SUM(mpa.MaintCurr_Hrs)	[MaintCurr_Hrs],
	SUM(mpa.MaintPrev_Hrs) / SUM(mpa.MaintCurr_Hrs) * 100.0	[MaintPrev_Pcnt]
FROM calc.MaintPersAverage	mpa
GROUP BY
	mpa.FactorSetId,
	mpa.Refnum,
	mpa.CalDateKey
HAVING
	SUM(mpa.MaintCurr_Hrs) <> 0.0