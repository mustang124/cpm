﻿CREATE VIEW [calc].[CapacityGrowthAggregateChange]
WITH SCHEMABINDING
AS
SELECT
	c.Refnum,
	c.CalDateKey,
	
	c.StreamId,
	c.Prod_kMT,
	p.Prod_kMT				[Previous_Prod_kMT],
	CASE WHEN p.Prod_kMT <> 0.0 THEN (c.Prod_kMT - p.Prod_kMT) / p.Prod_kMT * 100.0 END	[ProdChange_Pcnt],

	c.Capacity_kMT,
	p.Capacity_kMT			[Previous_Capacity_kMT],
	CASE WHEN p.Capacity_kMT <> 0.0 THEN (c.Capacity_kMT - p.Capacity_kMT) / p.Capacity_kMT * 100.0	END [CapacityChange_Pcnt],

	c._Utilization_Pcnt,
	p._Utilization_Pcnt		[Previous_Utilization_Pcnt],
	CASE WHEN p._Utilization_Pcnt <> 0.0 THEN (c._Utilization_Pcnt - p._Utilization_Pcnt) / p._Utilization_Pcnt * 100.0	END [UtilizationChange_Pcnt],

	c.StudyYearDifference
FROM calc.CapacityGrowthAggregate				c
LEFT OUTER JOIN calc.CapacityGrowthAggregate	p
	ON	c.Refnum = p.Refnum
	AND	c.StreamId = p.StreamId
	AND	c.CalDateKey = p.CalDateKey + 10000;