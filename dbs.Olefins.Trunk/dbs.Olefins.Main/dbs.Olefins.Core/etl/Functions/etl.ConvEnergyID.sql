﻿
CREATE FUNCTION etl.ConvEnergyID
(
	@AccountId	VARCHAR(42)
)
RETURNS VARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	VARCHAR(42) =
	CASE @AccountId

		WHEN 'Butanes'			THEN 'PurButane'
		WHEN 'Coal'				THEN 'PurCoalCoke'
		WHEN 'Distillate'		THEN 'PurDistallate'
		WHEN 'Ethane'			THEN 'PurEthane'
		
		WHEN 'ElecPower'		THEN 'PurElec'
		WHEN 'ExpElecPower'		THEN 'ExportElectric'
		
		WHEN 'ExpHPS'			THEN 'ExportSteamHP'
		WHEN 'ExpIPS'			THEN 'ExportSteamIP'
		WHEN 'ExpLPS'			THEN 'ExportSteamLP'
		WHEN 'ExpSHPS'			THEN 'ExportSteamSHP'
		WHEN 'FuelGas'			THEN 'PurFuelNatural'
		WHEN 'HPS'				THEN 'PurSteamHP'
		WHEN 'IPS'				THEN 'PurSteamIP'
		WHEN 'LPS'				THEN 'PurSteamLP'
		WHEN 'Naphtha'			THEN 'PurNaphtha'
		WHEN 'PPCButanes'		THEN 'PPFCButane'
		WHEN 'PPCEthane'		THEN 'PPFCEthane'
		WHEN 'PPCFuelGas'		THEN 'PPFCFuelGas'
		WHEN 'PPCFuelOil'		THEN 'PPFCPyroFuelOil'
		WHEN 'PPCGasOil'		THEN 'PPFCPyroGasOil'
		WHEN 'PPCNaphtha'		THEN 'PPFCPyroNaphtha'
		WHEN 'PPCPropane'		THEN 'PPFCPropane'
		WHEN 'Propane'			THEN 'PurPropane'
		WHEN 'PurchSteam'		THEN 'PurSteam'
		WHEN 'Residual'			THEN 'PurResidual'
		WHEN 'SHPS'				THEN 'PurSteamSHP'
			
		WHEN 'Cogen'			THEN NULL
		WHEN 'Combine'			THEN NULL
		WHEN 'GasTurb'			THEN NULL
		WHEN 'Other'			THEN NULL
		WHEN 'SteamTurb'		THEN NULL
		ELSE @AccountId
		
	END;
		
	RETURN @ResultVar;

END