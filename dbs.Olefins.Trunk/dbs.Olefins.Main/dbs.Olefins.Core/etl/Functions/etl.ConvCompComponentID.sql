﻿CREATE FUNCTION [etl].[ConvCompComponentId]
(
	@CompComponentID	NVARCHAR(5)
)
RETURNS NVARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(42) =

	CASE RTRIM(LTRIM(@CompComponentID))
		WHEN 'AUX'		THEN 'SystemAux'
		WHEN 'COMP'		THEN 'Comp'
		WHEN 'COMPR'	THEN 'Comp'
		WHEN 'CW'		THEN 'InterruptionCooling'
		WHEN 'ELEC'		THEN 'InterruptionElectric'
		WHEN 'EM'		THEN 'MotorElectric'
		WHEN 'FG'		THEN 'InterruptionFuelGas'
		WHEN 'GB'		THEN 'GearBox'
		WHEN 'GT'		THEN 'TurbineGas'
		WHEN 'HX'		THEN 'Exchanger'
		WHEN 'INST'		THEN 'Instr'
		WHEN 'INSTR'	THEN 'Instr'
		WHEN 'LUBE'		THEN 'Lube'
		WHEN 'OTHER'	THEN 'CompOther'
		WHEN 'SCS'		THEN 'SystemSpeed'
		WHEN 'SEAL'		THEN 'Lube'
		WHEN 'ST'		THEN 'TurbineSteam'
		WHEN 'STEAM'	THEN 'TurbineSteam'

	END
		
	RETURN @ResultVar;
	
END;