﻿CREATE TABLE [dim].[Mei_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [MeiId]              VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Mei_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Mei_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Mei_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Mei_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Mei_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Mei_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [MeiId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Mei_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Mei_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Mei_LookUp] ([MeiId]),
    CONSTRAINT [FK_Mei_Bridge_Factor_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Mei_Bridge_LookUp_Mei] FOREIGN KEY ([MeiId]) REFERENCES [dim].[Mei_LookUp] ([MeiId]),
    CONSTRAINT [FK_Mei_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [MeiId]) REFERENCES [dim].[Mei_Parent] ([FactorSetId], [MeiId]),
    CONSTRAINT [FK_Mei_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Mei_Parent] ([FactorSetId], [MeiId])
);


GO

CREATE TRIGGER [dim].[t_Mei_Bridge_u]
ON [dim].[Mei_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Mei_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Mei_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Mei_Bridge].[MeiId]	= INSERTED.[MeiId]
		AND	[dim].[Mei_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;