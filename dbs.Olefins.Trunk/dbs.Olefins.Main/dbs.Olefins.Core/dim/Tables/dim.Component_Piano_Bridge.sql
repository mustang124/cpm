﻿CREATE TABLE [dim].[Component_Piano_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [ComponentId]        VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Component_Piano_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Component_Piano_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Component_Piano_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Component_Piano_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Component_Piano_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Component_Piano_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Component_Piano_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Component_Piano_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Component_Piano_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Component_Piano_Bridge_LookUp_Component] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Component_Piano_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [ComponentId]) REFERENCES [dim].[Component_Parent] ([FactorSetId], [ComponentId]),
    CONSTRAINT [FK_Component_Piano_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Component_Parent] ([FactorSetId], [ComponentId])
);


GO

CREATE TRIGGER [dim].[t_Component_Piano_Bridge_u]
ON [dim].[Component_Piano_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Component_Piano_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Component_Piano_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Component_Piano_Bridge].[ComponentId]	= INSERTED.[ComponentId]
		AND	[dim].[Component_Piano_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;