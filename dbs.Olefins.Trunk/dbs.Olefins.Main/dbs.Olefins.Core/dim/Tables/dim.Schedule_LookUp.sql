﻿CREATE TABLE [dim].[Schedule_LookUp] (
    [ScheduleId]     CHAR (1)           NOT NULL,
    [ScheduleName]   NVARCHAR (84)      NOT NULL,
    [ScheduleDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Schedule_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Schedule_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Schedule_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Schedule_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Schedule_LookUp] PRIMARY KEY CLUSTERED ([ScheduleId] ASC),
    CONSTRAINT [CL_Schedule_LookUp_ScheduleDetail] CHECK ([ScheduleDetail]<>''),
    CONSTRAINT [CL_Schedule_LookUp_ScheduleId] CHECK ([ScheduleId]<>''),
    CONSTRAINT [CL_Schedule_LookUp_ScheduleName] CHECK ([ScheduleName]<>''),
    CONSTRAINT [UK_Schedule_LookUp_ScheduleDetail] UNIQUE NONCLUSTERED ([ScheduleDetail] ASC),
    CONSTRAINT [UK_Schedule_LookUp_ScheduleName] UNIQUE NONCLUSTERED ([ScheduleName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Schedule_LookUp_u]
	ON [dim].[Schedule_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Schedule_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Schedule_LookUp].[ScheduleId]		= INSERTED.[ScheduleId];
		
END;