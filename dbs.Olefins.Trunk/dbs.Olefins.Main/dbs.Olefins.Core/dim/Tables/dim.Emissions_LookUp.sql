﻿CREATE TABLE [dim].[Emissions_LookUp] (
    [EmissionsId]     VARCHAR (42)       NOT NULL,
    [EmissionsName]   NVARCHAR (84)      NOT NULL,
    [EmissionsDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_Emissions_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_Emissions_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_Emissions_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_Emissions_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Emissions_LookUp] PRIMARY KEY CLUSTERED ([EmissionsId] ASC),
    CONSTRAINT [CL_Emissions_LookUp_EmissionsDetail] CHECK ([EmissionsDetail]<>''),
    CONSTRAINT [CL_Emissions_LookUp_EmissionsId] CHECK ([EmissionsId]<>''),
    CONSTRAINT [CL_Emissions_LookUp_EmissionsName] CHECK ([EmissionsName]<>''),
    CONSTRAINT [UK_Emissions_LookUp_EmissionsDetail] UNIQUE NONCLUSTERED ([EmissionsDetail] ASC),
    CONSTRAINT [UK_Emissions_LookUp_EmissionsName] UNIQUE NONCLUSTERED ([EmissionsName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Emissions_LookUp_u]
	ON [dim].[Emissions_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Emissions_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Emissions_LookUp].[EmissionsId]		= INSERTED.[EmissionsId];
		
END;
