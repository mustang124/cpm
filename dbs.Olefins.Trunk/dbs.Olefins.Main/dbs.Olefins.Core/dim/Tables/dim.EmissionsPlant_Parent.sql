﻿CREATE TABLE [dim].[EmissionsPlant_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [EmissionsId]    VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_EmissionsPlant_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_EmissionsPlant_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_EmissionsPlant_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_EmissionsPlant_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_EmissionsPlant_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_EmissionsPlant_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] DESC, [EmissionsId] ASC),
    CONSTRAINT [CR_EmissionsPlant_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_EmissionsPlant_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_EmissionsPlant_Parent_LookUp_Emissions] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsPlant_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsPlant_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[EmissionsPlant_Parent] ([FactorSetId], [EmissionsId])
);


GO

CREATE TRIGGER [dim].[t_EmissionsPlant_Parent_u]
ON [dim].[EmissionsPlant_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[EmissionsPlant_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[EmissionsPlant_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[EmissionsPlant_Parent].[EmissionsId]	= INSERTED.[EmissionsId];

END;
