﻿CREATE TABLE [dim].[ReliabilityOppLoss_Availability_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [OppLossId]          VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ReliabilityOppLoss_Availability_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityOppLoss_Availability_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityOppLoss_Availability_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityOppLoss_Availability_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReliabilityOppLoss_Availability_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityOppLoss_Availability_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] DESC, [OppLossId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ReliabilityOppLoss_Availability_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_Bridge_LookUp_OppLoss] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_Availability_Parent] ([FactorSetId], [OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ReliabilityOppLoss_Availability_Parent] ([FactorSetId], [OppLossId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityOppLoss_Availability_Bridge_u]
ON [dim].[ReliabilityOppLoss_Availability_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityOppLoss_Availability_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityOppLoss_Availability_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityOppLoss_Availability_Bridge].[OppLossId]		= INSERTED.[OppLossId]
		AND	[dim].[ReliabilityOppLoss_Availability_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;