﻿CREATE TABLE [dim].[Gap_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [GapId]              VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Gap_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Gap_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Gap_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Gap_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Gap_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Gap_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] DESC, [GapId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Gap_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Gap_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Gap_LookUp] ([GapId]),
    CONSTRAINT [FK_Gap_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Gap_Bridge_LookUp_Gap] FOREIGN KEY ([GapId]) REFERENCES [dim].[Gap_LookUp] ([GapId]),
    CONSTRAINT [FK_Gap_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [GapId]) REFERENCES [dim].[Gap_Parent] ([FactorSetId], [GapId]),
    CONSTRAINT [FK_Gap_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Gap_Parent] ([FactorSetId], [GapId])
);


GO

CREATE TRIGGER [dim].[t_Gap_Bridge_u]
ON [dim].[Gap_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Gap_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Gap_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Gap_Bridge].[GapId]			= INSERTED.[GapId]
		AND	[dim].[Gap_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
