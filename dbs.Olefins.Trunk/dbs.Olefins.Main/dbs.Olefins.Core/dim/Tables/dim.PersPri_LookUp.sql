﻿CREATE TABLE [dim].[PersPri_LookUp] (
    [PersPriId]      VARCHAR (12)       NOT NULL,
    [PersPriName]    NVARCHAR (84)      NOT NULL,
    [PersPriDetail]  NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PersPri_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PersPri_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PersPri_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PersPri_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_PersPri_LookUp] PRIMARY KEY CLUSTERED ([PersPriId] ASC),
    CONSTRAINT [CL_PersPri_LookUp_PersPriDetail] CHECK ([PersPriDetail]<>''),
    CONSTRAINT [CL_PersPri_LookUp_PersPriId] CHECK ([PersPriId]<>''),
    CONSTRAINT [CL_PersPri_LookUp_PersPriName] CHECK ([PersPriName]<>''),
    CONSTRAINT [UK_PersPri_LookUp_PersPriDetail] UNIQUE NONCLUSTERED ([PersPriDetail] ASC),
    CONSTRAINT [UK_PersPri_LookUp_PersPriName] UNIQUE NONCLUSTERED ([PersPriName] ASC)
);


GO

CREATE TRIGGER [dim].[t_PersPri_LookUp_u]
	ON [dim].[PersPri_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[PersPri_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[PersPri_LookUp].[PersPriId]		= INSERTED.[PersPriId];
		
END;
