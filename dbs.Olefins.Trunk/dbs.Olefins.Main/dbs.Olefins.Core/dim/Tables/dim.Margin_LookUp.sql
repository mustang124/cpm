﻿CREATE TABLE [dim].[Margin_LookUp] (
    [MarginId]       VARCHAR (42)       NOT NULL,
    [MarginName]     NVARCHAR (84)      NOT NULL,
    [MarginDetail]   NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Margin_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Margin_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Margin_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Margin_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Margin_LookUp] PRIMARY KEY CLUSTERED ([MarginId] ASC),
    CONSTRAINT [CL_Margin_LookUp_MarginDetail] CHECK ([MarginDetail]<>''),
    CONSTRAINT [CL_Margin_LookUp_MarginId] CHECK ([MarginId]<>''),
    CONSTRAINT [CL_Margin_LookUp_MarginName] CHECK ([MarginName]<>''),
    CONSTRAINT [UK_Margin_LookUp_MarginDetail] UNIQUE NONCLUSTERED ([MarginDetail] ASC),
    CONSTRAINT [UK_Margin_LookUp_MarginName] UNIQUE NONCLUSTERED ([MarginName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Margin_LookUp_u]
	ON [dim].[Margin_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Margin_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Margin_LookUp].[MarginId]		= INSERTED.[MarginId];
		
END;
