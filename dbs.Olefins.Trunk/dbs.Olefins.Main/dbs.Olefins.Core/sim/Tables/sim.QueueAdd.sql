﻿CREATE TABLE [sim].[QueueAdd]
(
	[QueueId]			INT					NOT	NULL	IDENTITY(1, 1),

	[FactorSetId]		VARCHAR(12)			NOT	NULL,
	[Refnum]			VARCHAR(25)			NOT	NULL,
	[ModelId]			VARCHAR(12)			NOT	NULL,
	[Notes]				VARCHAR(MAX)			NULL,

	[Enqueued]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_QueueAdd_Enqueued]			DEFAULT(SYSDATETIMEOFFSET()),

	[tsModified]		DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_QueueAdd_tsModified]			DEFAULT (SYSDATETIMEOFFSET()),
	[tsModifiedHost]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueAdd_tsModifiedHost]		DEFAULT (HOST_NAME()),
	[tsModifiedUser]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueAdd_tsModifiedUser]		DEFAULT (SUSER_SNAME()),
	[tsModifiedApp]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueAdd_tsModifiedApp]		DEFAULT (APP_NAME()),

	CONSTRAINT [PK_QueueAdd]	PRIMARY KEY([QueueId] ASC),
	CONSTRAINT [UK_QueueAdd]	UNIQUE CLUSTERED([Enqueued] DESC, [FactorSetId] ASC, [Refnum] ASC, [ModelId] ASC)
);