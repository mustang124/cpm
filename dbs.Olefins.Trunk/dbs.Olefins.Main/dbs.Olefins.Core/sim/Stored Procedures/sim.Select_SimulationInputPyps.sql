﻿CREATE PROCEDURE [sim].[Select_SimulationInputPyps]
(
	@FactorSetId	VARCHAR(12),
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SELECT
		[p].[FactorSetId],
		[p].[Refnum],
		[p].[StreamId],
		[p].[StreamDescription],
		[p].[OpCondId],
		[p].[RecycleId],
		[p].[PypsRecycleId],

		[p].[ConversionID2],
		
		[CalcsSeverityIndicator] =
			CASE
				WHEN [p].[FeedConv_WtPcnt]			> 0.1	THEN 3
				WHEN [p].[PropyleneMethane_Ratio]	> 0.1	THEN 4
				WHEN [p].[PropyleneEthylene_Ratio]	> 0.02	THEN 2
				WHEN [p].[EthyleneYield_WtPcnt]		> 0.1	THEN 1
			END,

		  [1]	= [p].[1],
		  [2]	= [p].[2],
		  [3]	= [p].[3],
		  [4]	= [p].[4],
		  [5]	= [p].[5],
		  [6]	= [p].[6],
		  [7]	= [p].[7],
		  [8]	= [p].[8],
		  [9]	= [p].[9],
		 [10]	= CASE WHEN [p].[PypsFeedType] =  0 THEN [p].[10] ELSE 0 END,

		 [11]	= CASE WHEN [p].[PypsFeedType] = 11 THEN 1 ELSE 0 END,
		 [12]	= CASE WHEN [p].[PypsFeedType] = 12 THEN 1 ELSE 0 END,
		 [13]	= CASE WHEN [p].[PypsFeedType] = 13 THEN 1 ELSE 0 END,
		 [14]	= CASE WHEN [p].[PypsFeedType] = 14 THEN 1 ELSE 0 END,

		 [15]	= [p].[15],
		 [16]	= [p].[16],

		 [81]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[Density_SG]		ELSE 0 END,
		 [82]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[D000]				ELSE 0 END,
		 [83]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[D010]				ELSE 0 END,
		 [84]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[D030]				ELSE 0 END,
		 [85]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[D050]				ELSE 0 END,
		 [86]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[D070]				ELSE 0 END,
		 [87]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[D090]				ELSE 0 END,
		 [88]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[D100]				ELSE 0 END,
		 [89]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[H2]				ELSE 0 END,
		 [90]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[I] + [p].[P]		ELSE 0 END,
		 [91]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[N] + [p].[O]		ELSE 0 END,
		 [92]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[A]				ELSE 0 END,
		 [93]	= CASE WHEN [p].[PypsFeedType] = 11 THEN [p].[ParaffinINRatio]	ELSE 0 END,

		 [94]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[Density_SG]		ELSE 0 END,
		 [95]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[D000]				ELSE 0 END,
		 [96]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[D010]				ELSE 0 END,
		 [97]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[D030]				ELSE 0 END,
		 [98]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[D050]				ELSE 0 END,
		 [99]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[D070]				ELSE 0 END,
		[100]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[D090]				ELSE 0 END,
		[101]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[D100]				ELSE 0 END,
		[102]	= CASE WHEN [p].[PypsFeedType] = 12 THEN [p].[H2]				ELSE 0 END,

		[103]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[Density_SG]		ELSE 0 END,
		[104]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[D000]				ELSE 0 END,
		[105]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[D010]				ELSE 0 END,
		[106]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[D030]				ELSE 0 END,
		[107]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[D050]				ELSE 0 END,
		[108]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[D070]				ELSE 0 END,
		[109]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[D090]				ELSE 0 END,
		[110]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[D100]				ELSE 0 END,
		[111]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[H2]				ELSE 0 END,
		[112]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[I] + [p].[P]		ELSE 0 END,
		[113]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[N] + [p].[O]		ELSE 0 END,
		[114]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[A]				ELSE 0 END,
		[115]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[ParaffinINRatio]	ELSE 0 END,

		[116]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[Density_SG]		ELSE 0 END,
		[117]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[D000]				ELSE 0 END,
		[118]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[D010]				ELSE 0 END,
		[119]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[D030]				ELSE 0 END,
		[120]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[D050]				ELSE 0 END,
		[121]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[D070]				ELSE 0 END,
		[122]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[D090]				ELSE 0 END,
		[123]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[D100]				ELSE 0 END,
		[124]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[H2]				ELSE 0 END,

		[125]	= CASE WHEN [p].[PypsFeedType] = 13 THEN [p].[PypsLiquidType]	ELSE 0 END,
		[126]	= CASE WHEN [p].[PypsFeedType] = 14 THEN [p].[PypsLiquidType]	ELSE 0 END,

		[127]	= [p].[PypsRecycleId],
		[128]	= [p].[CoilOutletPressure_kgcm2],

		[129]	= 100.0,	--	FlowRate
		[130]	= CASE WHEN COALESCE([p].[PypsFeedType], 1) = 0  THEN [p].[SteamHydrocarbon_Ratio]	ELSE 0 END,

		[131]	= CASE WHEN COALESCE([p].[PypsFeedType], 1) = 0
			THEN
				CASE
					WHEN [p].[FeedConv_WtPcnt]			> 0.1	THEN   [p].[FeedConv_WtPcnt]
					WHEN [p].[PropyleneMethane_Ratio]	> 0.1	THEN   [p].[PropyleneMethane_Ratio]
					WHEN [p].[PropyleneEthylene_Ratio]	> 0.02	THEN - [p].[PropyleneEthylene_Ratio]
					WHEN [p].[EthyleneYield_WtPcnt]		> 0.1	THEN   [p].[EthyleneYield_WtPcnt]
				END
			ELSE
				0
			END,

		[132]	= CASE WHEN COALESCE([p].[PypsFeedType], 1) = 0
			THEN
				CASE
					WHEN [p].[FeedConv_WtPcnt]			> 0.1	THEN   [p].[ConversionID2]
					ELSE 0
				END
			ELSE
				0
			END,

		[149]	= 0.95,
		[150]	= 3,
		[151]	= 5,
		[152]	= 2,
		[153]	= 0,
		[154]	= 0.5,
		[155]	= 0.9,
		[156]	= 4,
		[157]	= 0,
		[158]	= 1

	FROM [sim].[PypsSimulationInput](@FactorSetId, @Refnum) [p];

	RETURN 0;

END;