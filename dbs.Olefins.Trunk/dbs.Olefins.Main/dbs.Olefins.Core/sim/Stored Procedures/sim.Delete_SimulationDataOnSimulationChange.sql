﻿CREATE PROCEDURE [sim].[Delete_SimulationDataOnSimulationChange]
(
	@Refnum					VARCHAR(25)	= NULL,
	@SimModelId				VARCHAR(12)	= NULL,
	@Tolerance				FLOAT = 0.0001
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		SET @Refnum = RTRIM(LTRIM(@Refnum));

		DECLARE @Multiplier	FLOAT = 0.0;
		DECLARE @RecycleId	TINYINT = 0;
		DECLARE @sRefnum	VARCHAR(9) = RIGHT(@Refnum, LEN(@Refnum) - 2);
		DECLARE @Delete		BIT = 0;

		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					fsisy.FactorSetId,
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
					etl.ConvDateKey(t.StudyYear)		[CalDateKey],
					ecs.SimModelId,
					ecs.OpCondId,
					etl.ConvStreamID(q.FeedProdId)		[StreamId],
					etl.ConvStreamDescription(q.FeedProdId, f.OthLiqFeedDESC, q.MiscProd1, q.MiscProd2, q.MiscFeed)	[StreamDescription],
					ecs.SimEnergy_kCalkg
				FROM stgFact.EnergyConsumptionStream	ecs
				INNER JOIN stgFact.Quantity				q
					ON	q.Refnum			= ecs.Refnum
					AND	q.FeedProdId		= ecs.StreamId
				LEFT OUTER JOIN stgFact.FeedQuality		f
					ON	f.Refnum			= ecs.Refnum
					AND f.FeedProdId		= q.FeedProdId
				INNER JOIN stgFact.TSort				t
					ON	t.Refnum			= ecs.Refnum
					AND t.Refnum			= @sRefnum
				INNER JOIN ante.FactorSetsInStudyYear	fsisy
					ON	fsisy._StudyYear	= t.StudyYear
				WHERE	ecs.SimModelId		= @SimModelId
				) stage
			FULL OUTER JOIN (
				SELECT
					ecs.FactorSetId,
					ecs.Refnum,
					ecs.CalDateKey,
					ecs.SimModelId,
					ecs.OpCondId,
					ecs.StreamId,
					ecs.StreamDescription,
					ecs.SimEnergy_kCalkg
				FROM sim.EnergyConsumptionStream ecs
				WHERE	ecs.Refnum		= @Refnum
					AND	ecs.SimModelId	= @SimModelId
				) fact
				ON	fact.FactorSetId		= stage.FactorSetId
				AND	fact.Refnum				= stage.Refnum
				AND	fact.CalDateKey			= stage.CalDateKey
				AND	fact.SimModelId			= stage.SimModelId
				AND	fact.OpCondId			= stage.OpCondId
				AND	fact.StreamId			= stage.StreamId
				AND	fact.StreamDescription	= stage.StreamDescription
			WHERE [stgFact].[RelativeError](stage.SimEnergy_kCalkg, fact.SimEnergy_kCalkg) > @Tolerance
				)
		BEGIN
			SET @Delete = 1;
		END;

		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					fsisy.FactorSetId,
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
					etl.ConvDateKey(t.StudyYear)		[CalDateKey],
					ecp.SimModelId,
					ecp.OpCondId,
					ecp.SimEnergy_kCalkg
				FROM stgFact.EnergyConsumptionPlant		ecp
				INNER JOIN stgFact.TSort				t
					ON	t.Refnum			= ecp.Refnum
					AND t.Refnum			= @sRefnum
				INNER JOIN ante.FactorSetsInStudyYear	fsisy
					ON	fsisy._StudyYear	= t.StudyYear
				WHERE	ecp.SimModelId		= @SimModelId
				) stage
			FULL OUTER JOIN (
				SELECT
					ecp.FactorSetId,
					ecp.Refnum,
					ecp.CalDateKey,
					ecp.SimModelId,
					ecp.OpCondId,
					ecp.Energy_kCalC2H4
				FROM sim.EnergyConsumptionPlant	 ecp
				WHERE	ecp.Refnum		= @Refnum
					AND	ecp.SimModelId	= @SimModelId
				) fact
				ON	fact.FactorSetId		= stage.FactorSetId
				AND	fact.Refnum				= stage.Refnum
				AND	fact.CalDateKey			= stage.CalDateKey
				AND	fact.SimModelId			= stage.SimModelId
				AND	fact.OpCondId			= stage.OpCondId
			WHERE [stgFact].[RelativeError](stage.SimEnergy_kCalkg, fact.Energy_kCalC2H4) > @Tolerance
				)
		BEGIN
			SET @Delete = 1;
		END;

		SELECT 
			@Multiplier = CASE WHEN MAX(ycs.Component_WtPcnt) <= 1.0 THEN 100.0 ELSE 1.0 END
		FROM stgFact.YieldCompositionStream		ycs
		INNER JOIN stgFact.TSort				t
			ON	t.Refnum = ycs.Refnum
			AND t.Refnum = @sRefnum
		WHERE	ycs.SimModelId = @SimModelId;

		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					fsisy.FactorSetId,
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
					etl.ConvDateKey(t.StudyYear)						[CalDateKey],
					ycs.SimModelId,
					ycs.OpCondId,
					etl.ConvStreamID(q.FeedProdId)						[StreamId],
					etl.ConvStreamDescription(q.FeedProdId, f.OthLiqFeedDESC, q.MiscProd1, q.MiscProd2, q.MiscFeed)	[StreamDescription],
					ycs.ComponentId,
					ycs.Component_WtPcnt * @Multiplier					[Component_WtPcnt]
				FROM stgFact.YieldCompositionStream		ycs
				INNER JOIN stgFact.Quantity				q
					ON q.Refnum				= ycs.Refnum
					AND	q.FeedProdId		= ycs.StreamId
				LEFT OUTER JOIN stgFact.FeedQuality		f
					ON	f.Refnum			= ycs.Refnum
					AND f.FeedProdId		= q.FeedProdId
				INNER JOIN stgFact.TSort				t
					ON	t.Refnum			= ycs.Refnum
					AND t.Refnum			= @sRefnum
				INNER JOIN ante.FactorSetsInStudyYear	fsisy
					ON	fsisy._StudyYear	= t.StudyYear
				WHERE	ycs.SimModelId		= @SimModelId
				) stage
			FULL OUTER JOIN (
				SELECT
					ycs.FactorSetId,
					ycs.Refnum,
					ycs.CalDateKey,
					ycs.SimModelId,
					ycs.OpCondId,
					ycs.StreamId,
					ycs.StreamDescription,
					ycs.ComponentId,
					ycs.Component_WtPcnt
				FROM sim.YieldCompositionStream ycs
				WHERE	ycs.Refnum		= @Refnum
					AND	ycs.SimModelId	= @SimModelId
				) fact
				ON	fact.FactorSetId		= stage.FactorSetId
				AND	fact.Refnum				= stage.Refnum
				AND	fact.CalDateKey			= stage.CalDateKey
				AND	fact.SimModelId			= stage.SimModelId
				AND	fact.OpCondId			= stage.OpCondId
				AND	fact.StreamId			= stage.StreamId
				AND	fact.StreamDescription	= stage.StreamDescription
				AND	fact.ComponentId		= stage.ComponentId
			WHERE [stgFact].[RelativeError](stage.Component_WtPcnt, fact.Component_WtPcnt) > @Tolerance
			)
		BEGIN
			SET @Delete = 1;
		END;

		SELECT 
			@Multiplier = CASE WHEN MAX(ycp.Component_WtPcnt) <= 1.0 THEN 100.0 ELSE 1.0 END
		FROM stgFact.YieldCompositionPlant		ycp
		INNER JOIN stgFact.TSort				t
			ON	t.Refnum = ycp.Refnum
			AND t.Refnum = @sRefnum
		WHERE	ycp.SimModelId = @SimModelId;

		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					fsisy.FactorSetId,
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
					etl.ConvDateKey(t.StudyYear)						[CalDateKey],
					ycp.SimModelId,
					ycp.OpCondId,
					ycp.ComponentId,
					ycp.Component_WtPcnt * @Multiplier					[Component_WtPcnt]
				FROM stgFact.YieldCompositionPlant		ycp
				INNER JOIN stgFact.TSort				t
					ON	t.Refnum			= ycp.Refnum
					AND t.Refnum			= @sRefnum
				INNER JOIN ante.FactorSetsInStudyYear	fsisy
					ON	fsisy._StudyYear	= t.StudyYear
				WHERE	ycp.SimModelId		= @SimModelId
				) stage
			FULL OUTER JOIN (
				SELECT
					ycp.FactorSetId,
					ycp.Refnum,
					ycp.CalDateKey,
					ycp.SimModelId,
					ycp.OpCondId,
					ycp.ComponentId,
					ycp.Component_WtPcnt
				FROM sim.YieldCompositionPlant ycp
				WHERE	ycp.Refnum		= @Refnum
					AND	ycp.SimModelId	= @SimModelId
				) fact
				ON	fact.FactorSetId		= stage.FactorSetId
				AND	fact.Refnum				= stage.Refnum
				AND	fact.CalDateKey			= stage.CalDateKey
				AND	fact.SimModelId			= stage.SimModelId
				AND	fact.OpCondId			= stage.OpCondId
				AND	fact.ComponentId		= stage.ComponentId
			WHERE [stgFact].[RelativeError](stage.Component_WtPcnt, fact.Component_WtPcnt) > @Tolerance
			)
		BEGIN
			SET @Delete = 1;
		END;

		IF (@Delete = 1)
		BEGIN

			PRINT NCHAR(9) + 'DELETE SIMULATION DATA ' + @Refnum + ' (Inconsistend stgFact.* and sim.* data)';

			DELETE FROM sim.EnergyConsumptionPlant
			WHERE	Refnum		= @Refnum
				AND	SimModelId	= @SimModelId;

			DELETE FROM sim.YieldCompositionPlant
			WHERE	Refnum		= @Refnum
				AND	SimModelId	= @SimModelId;

			DELETE FROM sim.EnergyConsumptionStream
			WHERE	Refnum		= @Refnum
				AND	SimModelId	= @SimModelId;

			DELETE FROM sim.YieldCompositionStream
			WHERE	Refnum		= @Refnum
				AND	SimModelId	= @SimModelId;

		END;
				
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;