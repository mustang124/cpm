﻿CREATE FUNCTION [sim].[PypsFeedTypePiano]
(
	@FeedType	TINYINT,
	@HasPiano	TINYINT
)
RETURNS TINYINT
WITH SCHEMABINDING
AS
BEGIN
	
	RETURN ISNULL(@FeedType, 0)
		- CASE WHEN
				ISNULL(@HasPiano, 0) = 1	--	No PIANO composition
			AND	ISNULL(@FeedType, 0) = 14
		THEN 1
		ELSE 0
		END;
		
END;