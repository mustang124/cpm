﻿CREATE FUNCTION [stat].[Fisher_Probability]
(@DegreesOfFreedomNumerator FLOAT (53), @DegreesOfFreedomDenominator FLOAT (53), @FStatistic FLOAT (53))
RETURNS FLOAT (53)
AS
 EXTERNAL NAME [Sa.Meta.Numerics].[UserDefinedFunctions].[Fisher_Probability]

