﻿
CREATE FUNCTION [stat].[DescriptiveGeomHarm]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics
	
	@Population			stat.PopulationRandomVar		READONLY										--	Data points to examine
)
RETURNS @ReturnTable TABLE 
(
	[xCount]			SMALLINT			NOT	NULL	CHECK([xCount]	>= 1),				--	Item Count
	[gMean]				FLOAT				NOT	NULL,										--	Geometric Mean						http://en.wikipedia.org/wiki/Geometric_mean
	[wgMean]			FLOAT					NULL,										--	Weighted Geometric Mean				http://en.wikipedia.org/wiki/Weighted_geometric_mean
	[gStDevP]			FLOAT				NOT	NULL	CHECK([gStDevP]	>= 0.0),			--	Geometirc Standard Deviation		http://en.wikipedia.org/wiki/Geometric_standard_deviation

	[hMean]				FLOAT				NOT	NULL,										--	Harmonic Mean						http://en.wikipedia.org/wiki/Harmonic_mean
	[whMean]			FLOAT					NULL										--	Weighted Harmonic Mean				http://en.wikipedia.org/wiki/Harmonic_mean#Weighted_harmonic_mean
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@xCount			SMALLINT;
	DECLARE @gMean			FLOAT;
	DECLARE @wgMean			FLOAT;
	DECLARE @gStDevP		FLOAT;

	DECLARE @hMean			FLOAT;
	DECLARE @whMean			FLOAT;

	SELECT
		@xCount		= COUNT(1),

		@gMean		= EXP(AVG(LOG(o.x))),
		@hMean		= COUNT(1) / SUM(CASE WHEN o.x <> 0.0 THEN 1.0/o.x END),

		@wgMean		= EXP(SUM(o.w * LOG(o.x)) / SUM(o.w)),
		@whMean		= SUM(o.w) / SUM(CASE WHEN o.x <> 0.0 THEN o.w/o.x END)
	FROM @Population o
	WHERE o.Basis = 1;

	IF (@xCount = 0) RETURN;

	SELECT
		@gStDevP	= EXP(SQRT(SUM(SQUARE(LOG(o.x / @gMean))) / @xCount))
	FROM @Population o
	WHERE o.Basis = 1;

	INSERT INTO @ReturnTable(xCount, gMean, wgMean, gStDevP, hMean, whMean)
	SELECT
		@xCount,
		@gMean,
		@wgMean,
		@gStDevP,
		@hMean,
		@whMean;

	RETURN;

END;