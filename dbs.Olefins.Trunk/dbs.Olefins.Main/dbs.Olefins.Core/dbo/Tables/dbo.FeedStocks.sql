﻿CREATE TABLE [dbo].[FeedStocks] (
    [Refnum]                  VARCHAR (25)  NOT NULL,
    [StreamId]                VARCHAR (42)  NOT NULL,
    [Quantity1Q_kMT]          REAL          NULL,
    [Quantity2Q_kMT]          REAL          NULL,
    [Quantity3Q_kMT]          REAL          NULL,
    [Quantity4Q_kMT]          REAL          NULL,
    [Quantity_kMT]            REAL          NULL,
    [H2]                      REAL          NULL,
    [CH4]                     REAL          NULL,
    [C2H6]                    REAL          NULL,
    [C2H4]                    REAL          NULL,
    [C3H8]                    REAL          NULL,
    [C3H6]                    REAL          NULL,
    [NBUTA]                   REAL          NULL,
    [IBUTA]                   REAL          NULL,
    [IB]                      REAL          NULL,
    [B1]                      REAL          NULL,
    [C4H6]                    REAL          NULL,
    [NC5]                     REAL          NULL,
    [IC5]                     REAL          NULL,
    [NC6]                     REAL          NULL,
    [C6ISO]                   REAL          NULL,
    [C7H16]                   REAL          NULL,
    [C8H18]                   REAL          NULL,
    [CO_CO2]                  REAL          NULL,
    [P]                       REAL          NULL,
    [I]                       REAL          NULL,
    [O]                       REAL          NULL,
    [N]                       REAL          NULL,
    [A]                       REAL          NULL,
    [S]                       REAL          NULL,
    [Total]                   REAL          NULL,
    [SulfurContent_ppm]       REAL          NULL,
    [Density_SG]              REAL          NULL,
    [D000]                    REAL          NULL,
    [D005]                    REAL          NULL,
    [D010]                    REAL          NULL,
    [D030]                    REAL          NULL,
    [D050]                    REAL          NULL,
    [D070]                    REAL          NULL,
    [D090]                    REAL          NULL,
    [D095]                    REAL          NULL,
    [D100]                    REAL          NULL,
    [CoilOutletPressure_Psia] REAL          NULL,
    [CoilOutletTemp_C]        REAL          NULL,
    [CoilInletPressure_Psia]  REAL          NULL,
    [CoilInletTemp_C]         REAL          NULL,
    [RadiantWallTemp_C]       REAL          NULL,
    [SteamHydrocarbon_Ratio]  REAL          NULL,
    [EthyleneYield_WtPcnt]    REAL          NULL,
    [PropyleneEthylene_Ratio] REAL          NULL,
    [PropyleneMethane_Ratio]  REAL          NULL,
    [FeedConv_WtPcnt]         REAL          NULL,
    [DistMethodId]            VARCHAR (25)  NULL,
    [FurnConstruction_Year]   REAL          NULL,
    [ResidenceTime_s]         REAL          NULL,
    [FlowRate_KgHr]           REAL          NULL,
    [Recycle_Bit]             REAL          NULL,
    [PlantReportingCount]     REAL          NULL,
    [tsModified]              SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FeedStocks] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC) WITH (FILLFACTOR = 70)
);

