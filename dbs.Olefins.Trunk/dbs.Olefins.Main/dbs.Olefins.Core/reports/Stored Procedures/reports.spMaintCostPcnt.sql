﻿



CREATE              PROC [reports].[spMaintCostPcnt](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.MaintCostPcnt WHERE GroupId = @GroupId
INSERT into reports.MaintCostPcnt (GroupId
, DataType
, AnnFurnReTube
, PyroFurnMajOth
, PyroFurnMinOth
, PyroFurnTLE
, STFurn
, CompGas
, Pump
, Exchanger
, Vessel
, PipeOnSite
, InstrAna
, PCComputers
, ElecGen
, TowerCool
, BoilFiredSteam
, OtherOnSite
, STOnsite
, Store
, Flare
, Enviro
, OtherOffSite
, STOffSite
, TotDirect
, OverHead
, TotMaint
, TurnAroundServices
)

SELECT GroupId = @GroupId
, DataType = p.DataType
, AnnFurnReTube = SUM(p.AnnFurnReTube) / SUM(p.TotMaint) * 100.0
, PyroFurnMajOth = SUM(p.PyroFurnMajOth) / SUM(p.TotMaint) * 100.0
, PyroFurnMinOth = SUM(p.PyroFurnMinOth) / SUM(p.TotMaint) * 100.0
, PyroFurnTLE = SUM(p.PyroFurnTLE) / SUM(p.TotMaint) * 100.0
, STFurn = SUM(p.STFurn) / SUM(p.TotMaint) * 100.0
, CompGas = SUM(p.CompGas) / SUM(p.TotMaint) * 100.0
, Pump = SUM(p.Pump) / SUM(p.TotMaint) * 100.0
, Exchanger = SUM(p.Exchanger) / SUM(p.TotMaint) * 100.0
, Vessel = SUM(p.Vessel) / SUM(p.TotMaint) * 100.0
, PipeOnSite = SUM(p.PipeOnSite) / SUM(p.TotMaint) * 100.0
, InstrAna = SUM(p.InstrAna) / SUM(p.TotMaint) * 100.0
, PCComputers = SUM(p.PCComputers) / SUM(p.TotMaint) * 100.0
, ElecGen = SUM(p.ElecGen) / SUM(p.TotMaint) * 100.0
, TowerCool = SUM(p.TowerCool) / SUM(p.TotMaint) * 100.0
, BoilFiredSteam = SUM(p.BoilFiredSteam) / SUM(p.TotMaint) * 100.0
, OtherOnSite = SUM(p.OtherOnSite) / SUM(p.TotMaint) * 100.0
, STOnsite = SUM(p.STOnsite) / SUM(p.TotMaint) * 100.0
, Store = SUM(p.Store) / SUM(p.TotMaint) * 100.0
, Flare = SUM(p.Flare) / SUM(p.TotMaint) * 100.0
, Enviro = SUM(p.Enviro) / SUM(p.TotMaint) * 100.0
, OtherOffSite = SUM(p.OtherOffSite) / SUM(p.TotMaint) * 100.0
, STOffSite = SUM(p.STOffSite) / SUM(p.TotMaint) * 100.0
, TotDirect = SUM(p.TotDirect) / SUM(p.TotMaint) * 100.0
, OverHead = SUM(p.OverHead) / SUM(p.TotMaint) * 100.0
, TotMaint = SUM(p.TotMaint) / SUM(p.TotMaint) * 100.0
, TurnAroundServices = SUM(p.TurnAroundServices) / SUM(p.TotMaint) * 100.0
FROM @MyGroup m JOIN dbo.MaintCost p on p.Refnum=m.Refnum
and p.Currency = 'USD' AND p.TotMaint > 0
GROUP BY p.DataType

DECLARE @RoutLaborPcnt real, @RoutMatlPcnt real, @TALaborPcnt real, @TAMatlPcnt real, @MaintLaborPcnt real, @MaintMatlPcnt real

SELECT 
  @RoutLaborPcnt = SUM(CASE WHEN p.DataType = 'RoutLabor' THEN p.TotMaint END) / SUM(CASE WHEN p.DataType = 'RoutTot' THEN p.TotMaint END) * 100.0
, @RoutMatlPcnt = SUM(CASE WHEN p.DataType = 'RoutMatl' THEN p.TotMaint END) / SUM(CASE WHEN p.DataType = 'RoutTot' THEN p.TotMaint END) * 100.0
, @TALaborPcnt = SUM(CASE WHEN p.DataType = 'TALabor' THEN p.TotMaint END) / SUM(CASE WHEN p.DataType = 'TATot' THEN p.TotMaint END) * 100.0
, @TAMatlPcnt = SUM(CASE WHEN p.DataType = 'TAMatl' THEN p.TotMaint END) / SUM(CASE WHEN p.DataType = 'TATot' THEN p.TotMaint END) * 100.0
, @MaintLaborPcnt = SUM(CASE WHEN p.DataType in ('TALabor','RoutLabor') THEN p.TotMaint END) / SUM(CASE WHEN p.DataType in ('TATot','RoutTot') THEN p.TotMaint END) * 100.0
, @MaintMatlPcnt = SUM(CASE WHEN p.DataType in ('TAMatl','RoutMatl') THEN p.TotMaint END) / SUM(CASE WHEN p.DataType in ('TATot','RoutTot') THEN p.TotMaint END) * 100.0
FROM @MyGroup m JOIN dbo.MaintCost p on p.Refnum=m.Refnum
and p.Currency = 'USD' AND p.TotMaint > 0

UPDATE reports.MaintCostPcnt
SET 
  RoutLaborPcnt = @RoutLaborPcnt
, RoutMatlPcnt = @RoutMatlPcnt
, TALaborPcnt = @TALaborPcnt
, TAMatlPcnt = @TAMatlPcnt
, MaintLaborPcnt = @MaintLaborPcnt
, MaintMatlPcnt = @MaintMatlPcnt
WHERE GroupId = @GroupId



--IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileRankVariable = 'EII')
--	EXEC spReportTileMinMax @ReportRefnum 

/*
Select * from reports.GroupPlants
insert into ReportS.GroupPlants SELECT '2011PCH148','2011PCH148'
insert into ReportS.GroupPlants SELECT '2011PCH50B','2011PCH50B'
exec reports.spMaintCostPcnt '11PCH'

exec reports.spMaintCostPcnt '2011PCH148'
exec reports.spMaintCostPcnt '2011PCH003'
exec reports.spMaintCostPcnt '2011PCH028'
exec reports.spMaintCostPcnt '2011PCH115'
exec reports.spMaintCostPcnt '2011PCH50B'
select * from ReportS.MaintCostPcnt
where GroupId = '11PCH'
insert into reports.GroupPlants select Refnum, Refnum from cons.reflist where ListId = '11PCH' and Refnum not in (Select GroupId from reports.groupplants)
select * from cons.reflistlu where listname = '11pch'
select * from cons.reflist where ListId= '11pch'
*/
SET NOCOUNT OFF


















