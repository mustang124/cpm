﻿CREATE PROCEDURE inter.Insert_PricingComposition
(
	@FactorSetId			VARCHAR(42) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	--	Insert Records - Basis	---------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (Light/Prod/Yield: ASIA, EUR, NA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  b.FactorSetId
			, b.RegionId
			, b.CalDateKey
			, b.CurrencyRpt
			, b.StreamId
			, b.ComponentId
			, b.Raw_Amount_Cur
		FROM inter.PricingStagingCompositionRegion b
		WHERE	b.RegionId IN ('ASIA', 'EUR', 'NA')
			AND b.FactorSetId = @FactorSetId;

		------------------------------------------------------------------
		------------------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (Light: LA) (< 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.ComponentId
			, r.Raw_Amount_Cur
		FROM inter.PricingStagingCompositionRegion r
		WHERE	r.RegionId = 'EUR'
			AND	NOT (r.StreamId = 'Light' AND r.ComponentId IN ('C2H4', 'C2H6'))
			AND	NOT (r.StreamId = 'Yield' AND r.ComponentId = 'C2H6')
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey < 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (Light: LA: C2H4, C2H6) (< 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.ComponentId
			, r.Raw_Amount_Cur
		FROM inter.PricingStagingCompositionRegion r
		WHERE	r.RegionId = 'NA'
			AND	((r.StreamId = 'Light' AND r.ComponentId IN ('C2H4', 'C2H6'))
			OR	(r.StreamId = 'Yield' AND r.ComponentId = 'C2H6'))
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey < 20130000;

		--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (LA: NA Basis) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.ComponentId
			, r.Raw_Amount_Cur * 1.5
		FROM inter.PricingStagingCompositionRegion r
		WHERE	r.RegionId = 'NA'
			AND((r.StreamId = 'Light'	AND r.ComponentId IN ('CH4', 'C2H4', 'C2H6', 'C3H8',						  'PricingAdj'))
			OR	(r.StreamId = 'Yield'	AND r.ComponentId IN (				 'C2H6', 'C3H8'))
			OR	(r.StreamId = 'Prod'	AND r.ComponentId IN ('CH4',		 'C2H6', 'C3H8', 'C4H8', 'C3H8', 'C4H10', 'PricingAdj')))
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey > 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (LA: EUR Basis) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.ComponentId
			, r.Raw_Amount_Cur
		FROM inter.PricingStagingCompositionRegion r
		WHERE	r.RegionId = 'EUR'
			AND	(r.StreamId = 'Light'	AND r.ComponentId IN ('NBUTA', 'IBUTA', 'C4H10', 'IC5', 'NC5', 'NC6', 'C6ISO', 'C7H16', 'C8H18'))
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey > 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (LA: 0.8 NA + 0.2 EUR) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  eu.FactorSetId
			, 'LA'				[RegionId]
			, eu.CalDateKey
			, eu.CurrencyRpt
			, eu.StreamId
			, eu.ComponentId
			, 0.8 * na.Raw_Amount_Cur	+ 0.2 * eu.Raw_Amount_Cur
		FROM inter.PricingStagingCompositionRegion			eu
		INNER JOIN inter.PricingStagingCompositionRegion	na
			ON	na.FactorSetId	= eu.FactorSetId
			AND	na.CalDateKey	= eu.CalDateKey
			AND	na.CurrencyRpt	= eu.CurrencyRpt
			AND	na.StreamId		= eu.StreamId
			AND	na.ComponentId	= eu.ComponentId
			AND	na.RegionId		= 'NA'
		WHERE	eu.RegionId		= 'EUR'
			AND((na.StreamId = 'Light'	AND na.ComponentId IN (						   'C3H6', 'C4H6', 'B1', 'IB'))
			OR	(na.StreamId = 'Yield'	AND na.ComponentId IN ('C2H2', 'C2H4', 'C3H4', 'C3H6', 'C4H6', 'C6H6', 'C4H10', 'C5S', 'C6C8NA', 'C7H8', 'C8H8', 'C8H10', 'PyroGasoline', 'PyroGasOil', 'PyroFuelOil'))
			OR	(na.StreamId = 'Prod'	AND na.ComponentId IN ('C2H2',				   'C3H6', 'C4H6', 'C6H6',													  'PyroGasoline', 'PyroGasOil', 'PyroFuelOil')))
			AND eu.FactorSetId = @FactorSetId
			AND	eu.CalDateKey > 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (LA: Hydrogen) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.ComponentId
			, r.Raw_Amount_Cur * 1.5 * 51.2 / 21.5
		FROM inter.PricingStagingCompositionRegion r
		WHERE	r.RegionId = 'NA'
			AND((r.StreamId = 'Light'	AND r.ComponentId IN ('H2'))
			OR	(r.StreamId = 'Yield'	AND r.ComponentId IN ('H2'))
			OR	(r.StreamId = 'Prod'	AND r.ComponentId IN ('H2')))
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey > 20130000;

		------------------------------------------------------------------
		------------------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (Light/Prod/Yield: MEA: Direct)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  b.FactorSetId
			, b.RegionId
			, b.CalDateKey
			, b.CurrencyRpt
			, b.StreamId
			, b.ComponentId
			, b.Raw_Amount_Cur
		FROM inter.PricingStagingCompositionRegion b
		WHERE	b.RegionId = 'MEA'
			AND b.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisCompositionRegion (Light/Prod/Yield: MEA: Asian Conversion)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  r.FactorSetId
			, 'MEA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.ComponentId

			, CASE r.StreamId
				WHEN 'Light'	THEN 
					CASE r.ComponentId
					WHEN 'C3H6'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C3H8'			THEN r.Raw_Amount_Cur - 45.0
					WHEN 'C4H6'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C4H10'		THEN r.Raw_Amount_Cur - 45.0
					WHEN 'NBUTA'		THEN r.Raw_Amount_Cur - 45.0
					WHEN 'IBUTA'		THEN r.Raw_Amount_Cur - 45.0
					WHEN 'IB'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'B1'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'NC5'			THEN r.Raw_Amount_Cur - 25.0
					WHEN 'IC5'			THEN r.Raw_Amount_Cur - 25.0
					WHEN 'NC6'			THEN r.Raw_Amount_Cur - 25.0
					WHEN 'C6ISO'		THEN r.Raw_Amount_Cur - 25.0
					WHEN 'C7H16'		THEN r.Raw_Amount_Cur - 25.0	--	20121203	NC7
					WHEN 'C8H18'		THEN r.Raw_Amount_Cur - 25.0	--	20121203	NC8
					END
				WHEN 'Prod'		THEN
					CASE r.ComponentId
					WHEN 'C2H4'			THEN r.Raw_Amount_Cur - 75.0 * 0.90
					WHEN 'C3H6'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C3H8'			THEN r.Raw_Amount_Cur - 45.0
					WHEN 'C4H6'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C4H8'			THEN r.Raw_Amount_Cur - 45.0
					WHEN 'C4H10'		THEN r.Raw_Amount_Cur - 45.0
					WHEN 'C6H6'			THEN r.Raw_Amount_Cur - 25.0
					WHEN 'PyroGasOil'	THEN r.Raw_Amount_Cur - 25.0
					WHEN 'PyroFuelOil'	THEN r.Raw_Amount_Cur - 25.0
					WHEN 'PyroGasoline'	THEN r.Raw_Amount_Cur - 25.0
					END
				WHEN 'Yield'	THEN
					CASE r.ComponentId
					WHEN 'C2H2'			THEN r.Raw_Amount_Cur - 75.0
					WHEN 'C2H4'			THEN r.Raw_Amount_Cur - 75.0
					WHEN 'C3H4'			THEN r.Raw_Amount_Cur - 75.0
					WHEN 'C3H6'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C3H8'			THEN r.Raw_Amount_Cur - 45.0
					WHEN 'C4H6'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C4H8'			THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C4H10'		THEN r.Raw_Amount_Cur - 70.0
					WHEN 'C6H6'			THEN r.Raw_Amount_Cur - 25.0
					WHEN 'PyroGasOil'	THEN r.Raw_Amount_Cur - 25.0
					WHEN 'PyroFuelOil'	THEN r.Raw_Amount_Cur - 25.0
					WHEN 'PyroGasoline'	THEN r.Raw_Amount_Cur - 25.0

					WHEN 'C5S'			THEN r.Raw_Amount_Cur - 25.0
					WHEN 'C6C8NA'		THEN r.Raw_Amount_Cur - 25.0
					WHEN 'C7H8'			THEN r.Raw_Amount_Cur - 25.0
					WHEN 'C8H10'		THEN r.Raw_Amount_Cur - 25.0
					WHEN 'C8H8'			THEN r.Raw_Amount_Cur - 25.0
					END
				END

		FROM inter.PricingStagingCompositionRegion r
		WHERE	r.RegionId = 'ASIA'
			AND NOT(r.StreamId = 'Light'	AND r.ComponentId IN (SELECT r.ComponentId FROM inter.PricingStagingCompositionRegion r WHERE r.RegionId = 'MEA' AND r.StreamId = 'Light'))
			AND NOT(r.StreamId = 'Prod'		AND r.ComponentId IN (SELECT r.ComponentId FROM inter.PricingStagingCompositionRegion r WHERE r.RegionId = 'MEA' AND r.StreamId = 'Prod' ))
			AND NOT(r.StreamId = 'Yield'	AND r.ComponentId IN (SELECT r.ComponentId FROM inter.PricingStagingCompositionRegion r WHERE r.RegionId = 'MEA' AND r.StreamId = 'Yield'))
			AND r.FactorSetId = @FactorSetId;

	--	Insert Records - Primary Light, Yield	-----------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryCompositionRegion (Light/Prod/Yield)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  r.FactorSetId
			, r.RegionId
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.ComponentId
			, r.Raw_Amount_Cur
		FROM inter.PricingBasisCompositionRegion r
		WHERE	NOT (r.StreamId = 'Light' AND r.ComponentId = 'C4H6')
			AND	NOT (r.StreamId = 'Yield' AND r.ComponentId = 'H2')
			AND	r.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryCompositionRegion (Light: C4H6)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur, MatlBal_Supersede_Cur)
		SELECT
			  [C4H6].FactorSetId
			, [C4H6].RegionId
			, [C4H6].CalDateKey
			, [C4H6].CurrencyRpt
			, [C4H6].StreamId
			, [C4H6].ComponentId
			, [C4H6].Raw_Amount_Cur
			, calc.MinValue(calc.MaxValue(0.9 * [C4H6].Raw_Amount_Cur, [C4H10].Raw_Amount_Cur), [NButa].Raw_Amount_Cur)		[Pricing_Supersede_Cur]
		FROM	   inter.PricingBasisCompositionRegion		[C4H6]
		INNER JOIN inter.PricingBasisCompositionRegion		[C4H10]
			ON	[C4H10].FactorSetId	= [C4H6].FactorSetId
			AND	[C4H10].RegionId	= [C4H6].RegionId
			AND	[C4H10].CalDateKey	= [C4H6].CalDateKey
			AND	[C4H10].CurrencyRpt	= [C4H6].CurrencyRpt
			AND	[C4H10].StreamId	= [C4H6].StreamId
			AND	[C4H10].ComponentId	= 'C4H10'
		INNER JOIN inter.PricingBasisCompositionRegion		[NButa]
			ON	[NButa].FactorSetId	= [C4H6].FactorSetId
			AND	[NButa].RegionId	= [C4H6].RegionId
			AND	[NButa].CalDateKey	= [C4H6].CalDateKey
			AND	[NButa].CurrencyRpt	= [C4H6].CurrencyRpt
			AND	[NButa].StreamId	= [C4H6].StreamId
			AND	[NButa].ComponentId	= 'NButa'
		WHERE	[C4H6].StreamId		= 'Light'
			AND	[C4H6].ComponentId	= 'C4H6'
			AND	[C4H6].FactorSetId	= @FactorSetId;

	--	Insert Records - Primary Production	---------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryCompositionCountry (Light/Prod)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryCompositionCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Proximity_RegionId,
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.CountryId
			, c0.CalDateKey
			, s.CurrencyRpt
			, r.StreamId							[StreamId]
			, r.ComponentId							[ComponentId]
			, prx.OverRide_RegionId					[Proximity_RegionId]

			, r.Raw_Amount_Cur						[Raw_Amount_Cur]
			, rOvr.Raw_Amount_Cur					[Prx_Amount_Cur]

			, NULL									[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur					[Adj_Proximity_Cur]

			, r.MatlBal_Supersede_Cur				[MatlBal_Supersede_Cur]
		FROM ante.PricingMultiplierStreamCountry						s
		INNER JOIN ante.CountryEconRegion								k
			ON	k.FactorSetId		= s.FactorSetId
			AND	k.CountryId			= s.CountryId
		INNER JOIN ante.FactorSetsInStudyYear							fs
			ON	fs.FactorSetId		= s.FactorSetId
			AND	fs.CalDateKey		= s.CalDateKey
		INNER JOIN dim.Calendar_LookUp									c0
			ON	c0.CalYear			= fs._StudyYear

		INNER JOIN inter.PricingPrimaryCompositionRegion				r
			ON	r.FactorSetId		= s.FactorSetId
			AND	r.RegionId			= k.[EconRegionId]
			AND r.CalDateKey		= c0.CalDateKey
			AND	r.CurrencyRpt		= s.CurrencyRpt

		LEFT OUTER JOIN ante.PricingPeersCompositionCountryAgg			prx
			ON	prx.FactorSetId								= fs.FactorSetId
			AND	prx.CountryId								= s.CountryId
			AND prx.CalDateKey								= s.CalDateKey
			AND	prx.CurrencyRpt								= s.CurrencyRpt
			AND ISNULL(prx.StreamId, r.StreamId)			= r.StreamId
			AND ISNULL(prx.ComponentId, r.ComponentId)		= r.ComponentId

		LEFT OUTER JOIN inter.PricingPrimaryCompositionRegion			rOvr
			ON	rOvr.FactorSetId							= s.FactorSetId
			AND	rOvr.RegionId								= prx.OverRide_RegionId
			AND rOvr.CalDateKey								= c0.CalDateKey
			AND	rOvr.CurrencyRpt							= s.CurrencyRpt
			AND	ISNULL(rOvr.StreamId, r.StreamId)			= r.StreamId
			AND ISNULL(rOvr.ComponentId, r.ComponentId)		= r.ComponentId

		WHERE	r.ComponentId <> 'CH4'
			AND NOT(r.StreamId = 'Light'	AND r.ComponentId = 'H2')
			AND NOT(r.StreamId = 'Prod'		AND r.ComponentId = 'C2H6')
			AND r.StreamId <> 'Yield'
			AND	s.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryCompositionCountry (Light: CH4, H2)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryCompositionCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Proximity_RegionId,
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.CountryId
			, r.CalDateKey
			, s.CurrencyRpt
			, 'Light'								[StreamId]
			, m.ComponentId							[ComponentId]
			, prx.OverRide_RegionId					[Proximity_RegionId]

			, m.Raw_Amount_Cur						[Raw_Amount_Cur]
			, mOvr.Raw_Amount_Cur					[Prx_Amount_Cur]

			, NULL									[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur					[Adj_Proximity_Cur]

			, ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.Raw_Amount_Cur) / AVG(ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.Raw_Amount_Cur)) OVER (PARTITION BY r.FactorSetId, r.RegionId, r.StreamId, r.CurrencyRpt) * 2204.6 / 1000000.0 * 21500.0 * s.Amount_Cur [Pricing_Supersede_Cur]
		FROM ante.PricingMultiplierStreamCountry						s		/* Annum */
		INNER JOIN ante.CountryEconRegion								k
			ON	k.FactorSetId		= s.FactorSetId
			AND	k.CountryId			= s.CountryId
		INNER JOIN ante.FactorSetsInStudyYear							fs
			ON	fs.FactorSetId = s.FactorSetId
		INNER JOIN dim.Calendar_LookUp									c0
			ON	c0.CalYear = fs._StudyYear

		INNER JOIN inter.PricingBasisCompositionRegion					m		/* Quarter*/
			ON	m.FactorSetId = s.FactorSetId
			AND	m.RegionId = k.[EconRegionId]
			AND	m.CalDateKey = c0.CalDateKey
			AND	m.CurrencyRpt = s.CurrencyRpt
			AND m.StreamId = 'Light'

		INNER JOIN inter.PricingBasisCompositionRegion					r		/* Quarter*/
			ON	r.FactorSetId = s.FactorSetId
			AND	r.RegionId =  k.[EconRegionId]
			AND	r.CalDateKey = c0.CalDateKey
			AND	r.CurrencyRpt = s.CurrencyRpt
			AND r.StreamId = 'Light'

		LEFT OUTER JOIN ante.PricingPeersCompositionCountryAgg	prx
			ON	prx.FactorSetId								= fs.FactorSetId
			AND	prx.CountryId								= s.CountryId
			AND prx.CalDateKey								= s.CalDateKey
			AND	prx.CurrencyRpt								= s.CurrencyRpt
			AND ISNULL(prx.StreamId, r.StreamId)			= r.StreamId
			AND ISNULL(prx.ComponentId, r.ComponentId)	= r.ComponentId

		LEFT OUTER JOIN inter.PricingBasisCompositionRegion				mOvr		/* Quarter*/
			ON	mOvr.FactorSetId = s.FactorSetId
			AND	mOvr.RegionId = k.[EconRegionId]
			AND	mOvr.CalDateKey = c0.CalDateKey
			AND	mOvr.CurrencyRpt = s.CurrencyRpt
			AND mOvr.StreamId = m.StreamId
			AND mOvr.ComponentId = m.ComponentId

		LEFT OUTER JOIN inter.PricingBasisCompositionRegion				rOvr		/* Quarter*/
			ON	rOvr.FactorSetId = s.FactorSetId
			AND	rOvr.RegionId =  k.[EconRegionId]
			AND	rOvr.CalDateKey = c0.CalDateKey
			AND	rOvr.CurrencyRpt = s.CurrencyRpt
			AND rOvr.StreamId = r.StreamId
			AND rOvr.ComponentId = r.ComponentId

		WHERE	s.StreamId		= 'PricingAdj'
			AND r.ComponentId	= 'PricingAdj'
			AND m.ComponentId	IN ('CH4', 'H2')
			AND	s.FactorSetId	= @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryCompositionCountry (Prod: CH4, C2H6)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryCompositionCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Proximity_RegionId,
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.CountryId
			, r.CalDateKey
			, s.CurrencyRpt
			, 'Prod'
			, m.ComponentId							[ComponentId]
			, prx.OverRide_RegionId					[Proximity_RegionId]

			, m.Raw_Amount_Cur						[Raw_Amount_Cur]
			, mOvr.Raw_Amount_Cur					[Prx_Amount_Cur]

			, NULL									[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur					[Adj_Proximity_Cur]

			, ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.Raw_Amount_Cur) / AVG(ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.Raw_Amount_Cur)) OVER (PARTITION BY r.FactorSetId, r.RegionId, r.StreamId, r.CurrencyRpt) * 2204.6 / 1000000.0 * 21500.0 * s.Amount_Cur [Pricing_Supersede_Cur]
		FROM ante.PricingMultiplierStreamCountry						s		/* Annum */
		INNER JOIN ante.CountryEconRegion								k
			ON	k.FactorSetId		= s.FactorSetId
			AND	k.CountryId			= s.CountryId
		INNER JOIN ante.FactorSetsInStudyYear							fs
			ON	fs.FactorSetId = s.FactorSetId
		INNER JOIN dim.Calendar_LookUp									c0
			ON	c0.CalYear = fs._StudyYear

		INNER JOIN inter.PricingBasisCompositionRegion					r		/* Quarter*/
			ON	r.FactorSetId = s.FactorSetId
			AND	r.CalDateKey = c0.CalDateKey
			AND	r.RegionId =  k.[EconRegionId]
			AND	r.CurrencyRpt = s.CurrencyRpt
			AND r.StreamId = 'Prod'

		INNER JOIN inter.PricingBasisCompositionRegion					m		/* Quarter*/
			ON	m.FactorSetId = s.FactorSetId
			AND	m.CalDateKey = c0.CalDateKey
			AND	m.RegionId =  k.[EconRegionId]
			AND	m.CurrencyRpt = s.CurrencyRpt
			AND m.StreamId = r.StreamId

		LEFT OUTER JOIN ante.PricingPeersCompositionCountryAgg	prx
			ON	prx.FactorSetId								= fs.FactorSetId
			AND	prx.CountryId								= s.CountryId
			AND prx.CalDateKey								= s.CalDateKey
			AND	prx.CurrencyRpt								= s.CurrencyRpt
			AND ISNULL(prx.StreamId, r.StreamId)			= r.StreamId
			AND ISNULL(prx.ComponentId, r.ComponentId)		= r.ComponentId

		LEFT OUTER JOIN inter.PricingBasisCompositionRegion				mOvr		/* Quarter*/
			ON	mOvr.FactorSetId = s.FactorSetId
			AND	mOvr.RegionId = k.[EconRegionId]
			AND	mOvr.CalDateKey = c0.CalDateKey
			AND	mOvr.CurrencyRpt = s.CurrencyRpt
			AND mOvr.StreamId = m.StreamId
			AND mOvr.ComponentId = m.ComponentId

		LEFT OUTER JOIN inter.PricingBasisCompositionRegion				rOvr		/* Quarter*/
			ON	rOvr.FactorSetId = s.FactorSetId
			AND	rOvr.RegionId =  k.[EconRegionId]
			AND	rOvr.CalDateKey = c0.CalDateKey
			AND	rOvr.CurrencyRpt = s.CurrencyRpt
			AND rOvr.StreamId = r.StreamId
			AND rOvr.ComponentId = r.ComponentId

		WHERE	s.StreamId		= 'PricingAdj'
			AND r.ComponentId	= 'PricingAdj'
			AND m.ComponentId	IN ('CH4', 'C2H6')
			AND	s.FactorSetId	= @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryCompositionCountry (Yield: CH4)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryCompositionCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Proximity_RegionId,
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.CountryId
			, r.CalDateKey
			, s.CurrencyRpt
			, 'Yield'
			, m.ComponentId							[ComponentId]
			, prx.OverRide_RegionId					[Proximity_RegionId]

			, m.Raw_Amount_Cur						[Raw_Amount_Cur]
			, mOvr.Raw_Amount_Cur					[Prx_Amount_Cur]

			, NULL									[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur					[Adj_Proximity_Cur]

			, ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.Raw_Amount_Cur) / AVG(ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.Raw_Amount_Cur)) OVER (PARTITION BY r.FactorSetId, r.RegionId, r.StreamId, r.CurrencyRpt) * 2204.6 / 1000000.0 * 21500.0 * s.Amount_Cur [Pricing_Supersede_Cur]
		FROM ante.PricingMultiplierStreamCountry						s		/* Annum */
		INNER JOIN ante.CountryEconRegion								k
			ON	k.FactorSetId		= s.FactorSetId
			AND	k.CountryId			= s.CountryId
		INNER JOIN ante.FactorSetsInStudyYear							fs
			ON	fs.FactorSetId = s.FactorSetId
		INNER JOIN dim.Calendar_LookUp									c0
			ON	c0.CalYear = fs._StudyYear

		INNER JOIN inter.PricingBasisCompositionRegion					r		/* Quarter*/
			ON	r.FactorSetId = s.FactorSetId
			AND	r.CalDateKey = c0.CalDateKey
			AND	r.RegionId =  k.[EconRegionId]
			AND	r.CurrencyRpt = s.CurrencyRpt
			AND r.StreamId = 'Prod'

		INNER JOIN inter.PricingBasisCompositionRegion					m		/* Quarter*/
			ON	m.FactorSetId = s.FactorSetId
			AND	m.CalDateKey = c0.CalDateKey
			AND	m.RegionId =  k.[EconRegionId]
			AND	m.CurrencyRpt = s.CurrencyRpt
			AND m.StreamId = r.StreamId

		LEFT OUTER JOIN ante.PricingPeersCompositionCountryAgg	prx
			ON	prx.FactorSetId								= fs.FactorSetId
			AND	prx.CountryId								= s.CountryId
			AND prx.CalDateKey								= s.CalDateKey
			AND	prx.CurrencyRpt								= s.CurrencyRpt
			AND ISNULL(prx.StreamId, r.StreamId)			= r.StreamId
			AND ISNULL(prx.ComponentId, r.ComponentId)		= r.ComponentId

		LEFT OUTER JOIN inter.PricingBasisCompositionRegion				mOvr		/* Quarter*/
			ON	mOvr.FactorSetId = s.FactorSetId
			AND	mOvr.RegionId = k.[EconRegionId]
			AND	mOvr.CalDateKey = c0.CalDateKey
			AND	mOvr.CurrencyRpt = s.CurrencyRpt
			AND mOvr.StreamId = m.StreamId
			AND mOvr.ComponentId = m.ComponentId

		LEFT OUTER JOIN inter.PricingBasisCompositionRegion				rOvr		/* Quarter*/
			ON	rOvr.FactorSetId = s.FactorSetId
			AND	rOvr.RegionId =  k.[EconRegionId]
			AND	rOvr.CalDateKey = c0.CalDateKey
			AND	rOvr.CurrencyRpt = s.CurrencyRpt
			AND rOvr.StreamId = r.StreamId
			AND rOvr.ComponentId = r.ComponentId

		WHERE	s.StreamId		= 'PricingAdj'
			AND r.ComponentId	= 'PricingAdj'
			AND m.ComponentId	= 'CH4'
			AND	s.FactorSetId	= @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryCompositionCountry (Yield: H2)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryCompositionCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			pcr.FactorSetId,
			cer.CountryId,
			pcr.CalDateKey,
			pcr.CurrencyRpt,
			pcr.StreamId,
			pcr.ComponentId,
			pcr.Raw_Amount_Cur * 0.25 +	pcc._MatlBal_Amount_Cur * 0.75 * 51500.0 / 21500.0
		FROM inter.PricingBasisCompositionRegion			pcr
		INNER JOIN ante.CountryEconRegion					cer
			ON	cer.FactorSetId = pcr.FactorSetId
			AND	cer.EconRegionId = pcr.RegionId
		INNER JOIN inter.PricingPrimaryCompositionCountry	pcc
			ON	pcc.FactorSetId = pcr.FactorSetId
			AND	pcc.CountryId = cer.CountryId
			AND	pcc.CalDateKey = pcr.CalDateKey
			AND	pcc.CurrencyRpt = pcr.CurrencyRpt
			AND	pcc.StreamId = pcr.StreamId
			AND	pcc.ComponentId = 'CH4'

		WHERE	pcr.StreamId = 'Yield'
			AND	pcr.ComponentId = 'H2'
			AND	pcr.FactorSetId = @FactorSetId;

	--	Insert Records	-----------------------------------------------------------------

		DECLARE @MethodId	VARCHAR(42);
		SELECT TOP 1 @MethodId = l.MethodId FROM dim.PricingMethodLu l;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingCompositionRegion';
		PRINT @ProcedureDesc;

		INSERT INTO [inter].PricingCompositionRegion(FactorSetId, PricingMethodId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, MatlBal_Cur)
		SELECT l.FactorSetId, @MethodId, l.RegionId, l.CalDateKey, l.CurrencyRpt, l.StreamId, l.ComponentId, l._MatlBal_Amount_Cur
		FROM inter.PricingPrimaryCompositionRegion l
		WHERE l.FactorSetId = @FactorSetId;
	
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingCompositionCountry';
		PRINT @ProcedureDesc;

		INSERT INTO [inter].PricingCompositionCountry(FactorSetId, PricingMethodId, CountryId, CalDateKey, CurrencyRpt, StreamId, ComponentId, MatlBal_Cur)
		SELECT l.FactorSetId, @MethodId, l.CountryId, l.CalDateKey, l.CurrencyRpt, l.StreamId, l.ComponentId, l._MatlBal_Amount_Cur
		FROM inter.PricingPrimaryCompositionCountry l
		WHERE l.FactorSetId = @FactorSetId;

END