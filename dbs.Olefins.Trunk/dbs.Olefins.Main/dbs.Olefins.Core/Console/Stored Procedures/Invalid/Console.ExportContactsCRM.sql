﻿CREATE PROCEDURE [Console].[ExportContactsCRM]
	
AS
BEGIN

			
		SELECT	'' as Salutation,
	SUBSTRING(NameFull,1,CHARINDEX(' ',NameFull,1)) as [First Name],
	
	SUBSTRING(NameFull,CHARINDEX(' ',NameFull,1),LEN(NameFull)) as [Last Name],
	[NameTitle] as Title,
	Company as [Parent Customer],
	
	[AddressPOBox] as [Address 1],
			[AddressStreet] as [Address 2] ,
			'' as [Address 3],
			'PRIMARY' as [Address Type],
			'' as [Name],
			[AddressCity] as [City],
			[AddressState] as [State],
			[AddressPostalCode] as [Zip],
			[AddressCountry] as [Country]	 
	FROM fact.TSortContact tc join dbo.TSort cl on tc.Refnum = cl.Refnum
	WHERE SUBSTRING(tc.RefNum,1,4) = '2013'
END
