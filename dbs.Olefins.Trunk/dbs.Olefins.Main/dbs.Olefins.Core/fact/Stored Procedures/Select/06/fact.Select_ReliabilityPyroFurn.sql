﻿CREATE PROCEDURE [fact].[Select_ReliabilityPyroFurn]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-4
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[FurnId],
			[FFurnId]			= 'F' + CONVERT(VARCHAR(2), [f].[FurnId]),
		[f].[StreamId],

			[MaintDecoke_Days]	= [d].[MaintDecoke],
			[MaintTLE_Days]		= [d].[MaintTLE],
			[MaintMinor_Days]	= [d].[MaintMinor],
			[MaintMajor_Days]	= [d].[MaintMajor],
			[StandByDT_Days]	= [d].[StandByDT],

		[d].[PeriodDays],
		[d].[PlantOutages],
		[d].[TurnAround],

		[f].[FeedQty_kMT],
		[f].[FeedCap_MTd],
		[f].[FuelTypeId],
		[f].[FuelCons_kMT],
		[f].[StackOxygen_Pcnt],
		[f].[ArchDraft_H20],
		[f].[StackTemp_C],
		[f].[CrossTemp_C],
		[f].[Retubed_Year],
		[f].[Retubed_Pcnt],
		[f].[RetubeInterval_Mnths],
			[RetubeInterval_Years]	= [f].[_RetubeInterval_Years],

		[o].[CurrencyRpt],
			[MaintRetubeMatl_Cur]	= [o].[MaintRetubeMatl],
			[MaintRetubeLabor_Cur]	= [o].[MaintRetubeLabor],
			[MaintMajor_Cur]		= [o].[MaintMajor]

	FROM
		[fact].[ReliabilityPyroFurn]			[f]

	LEFT OUTER JOIN
		(
		SELECT
			[d].[Refnum],
			[d].[CalDateKey],
			[d].[FurnId],
			[d].[OppLossId],
			[d].[DownTime_Days]
		FROM
			[fact].[ReliabilityPyroFurnDownTime]	[d]
		WHERE
			[d].[Refnum]	= @Refnum
		) [t]
		PIVOT
		(
			MAX([t].[DownTime_Days]) FOR [OppLossId] IN
			(
				[MaintDecoke],
				[MaintMajor],
				[MaintMinor],
				[MaintTLE],
				[PeriodDays],
				[PlantOutages],
				[StandByDT],
				[TurnAround]
			)
		) [d]
		ON	[d].[Refnum]		= [f].[Refnum]
		AND	[d].[CalDateKey]	= [f].[CalDateKey]
		AND	[d].[FurnId]		= [f].[FurnId]

	LEFT OUTER JOIN
		(
		SELECT
			[o].[Refnum],
			[o].[CalDateKey],
			[o].[FurnId],
			[o].[AccountId],
			[o].[CurrencyRpt],
			[o].[Amount_Cur]
		FROM
			[fact].[ReliabilityPyroFurnOpEx]	[o]
		WHERE
			[o].[Refnum]	= @Refnum
		) [t]
		PIVOT
		(
			MAX([t].[Amount_Cur]) FOR [AccountId] IN
			(
				[MaintMajor],
				[MaintRetubeLabor],
				[MaintRetubeMatl]
			)
		) [o]
		ON	[o].[Refnum]		= [f].[Refnum]
		AND	[o].[CalDateKey]	= [f].[CalDateKey]
		AND	[o].[FurnId]		= [f].[FurnId]

	WHERE
		[f].[Refnum]	= @Refnum;

END;