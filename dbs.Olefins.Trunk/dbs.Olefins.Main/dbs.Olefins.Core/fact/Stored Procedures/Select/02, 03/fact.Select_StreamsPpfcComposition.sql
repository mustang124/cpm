﻿CREATE PROCEDURE [fact].[Select_StreamsPpfcComposition]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
			[c].[Refnum],
			[c].[StreamId],
				[StreamIdInt]		= CASE WHEN [c].[StreamId] IN ('FeedLiqOther', 'SuppOther', 'ProdOther')
										THEN
											[c].[StreamId] + SUBSTRING([c].[StreamDescription], LEN([c].[StreamDescription]) - 1, 1)
										ELSE
											[c].[StreamId]
										END,
			[c].[StreamDescription],
				[StreamDesc]		= CASE WHEN [c].[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'SuppOther', 'ProdOther')
										THEN
											LEFT([c].[StreamDescription], LEN([c].[StreamDescription]) - CHARINDEX('(', REVERSE([c].[StreamDescription])))
										END,
		[c].[H2],
		[c].[CH4],
		[c].[C2H4],
		[c].[Other]
	FROM
		[fact].[CompositionQuantityPivot]	[c]
	WHERE	[c].[StreamId]	= 'PPFC'
		AND	[c].[Refnum]	= @Refnum

END;