﻿CREATE PROCEDURE [fact].[Select_ROGEntryPoint]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 2C
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[CompGasInlet_Bit],
		[f].[CompGasDischarge_Bit],
		[f].[C2Recovery_Bit],
		[f].[C3Recovery_Bit],
		[CompGasInlet_X]		= CASE WHEN ([f].[CompGasInlet_Bit]		= 1) THEN 'X' END,
		[CompGasDischarge_X]	= CASE WHEN ([f].[CompGasDischarge_Bit]	= 1) THEN 'X' END,
		[C2Recovery_X]			= CASE WHEN ([f].[C2Recovery_Bit]		= 1) THEN 'X' END,
		[C3Recovery_X]			= CASE WHEN ([f].[C3Recovery_Bit]		= 1) THEN 'X' END
	FROM
		[fact].[ROGEntryPoint]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;