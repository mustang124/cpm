﻿CREATE PROCEDURE [fact].[Select_StreamsLiquid]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		--	Table 2A-1, Table 2A-2, Table 2B, Table 2C, Table 3
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[StreamIdInt]		= CASE WHEN [f].[StreamId] IN ('FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										[f].[StreamId] + SUBSTRING([f].[StreamDescription], LEN([f].[StreamDescription]) - 1, 1)
									ELSE
										[f].[StreamId]
									END,
		[f].[StreamDescription],
			[StreamDesc]		= CASE WHEN [f].[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										LEFT([f].[StreamDescription], LEN([f].[StreamDescription]) - CHARINDEX('(', REVERSE([f].[StreamDescription])))
									END,
		[f].[Q1_kMT],
		[f].[Q2_kMT],
		[f].[Q3_kMT],
		[f].[Q4_kMT],

		--	Table 2A-1, Table 2A-2, Table 2B, Table 2C
		[p].[CoilOutletPressure_Psia],
		[p].[CoilOutletTemp_C],
		[p].[CoilInletPressure_Psia],
		[p].[CoilInletTemp_C],
		[p].[RadiantWallTemp_C],
		[p].[SteamHydrocarbon_Ratio],
		[p].[EthyleneYield_WtPcnt],
		[p].[PropyleneEthylene_Ratio],
		[p].[PropyleneMethane_Ratio],
		[p].[FeedConv_WtPcnt],
		[p].[DistMethodId],
		[p].[SulfurContent_ppm],
		[p].[Density_SG],
		[p].[FurnConstruction_Year],
		[p].[ResidenceTime_s],
		[p].[FlowRate_KgHr],
		[p].[Recycle_Bit],

		--	Table 2A-2
		[H2]		= COALESCE([c].[H2], [s].[H2]),
		[S_PPM]		= [c].[S] * 10000.0,

		[c].[P],
		[c].[I],
		[c].[A],
		[c].[N],
		[c].[O],

		--	Table 2A-2
		[d].[D000],
		[d].[D005],
		[d].[D010],
		[d].[D030],
		[d].[D050],
		[d].[D070],
		[d].[D090],
		[d].[D095],
		[d].[D100],
		[d].[C5C6_Fraction],

		[Amount_Cur]	= COALESCE([v].[Amount_Cur], [q].[Amount_Cur])

	FROM
		[fact].[QuantityPivot]					[f]
	INNER JOIN
		[dim].[Stream_Bridge]					[b]
			ON	[b].[DescendantId]		= [f].[StreamId]
			AND	[b].[FactorSetId]		= '2013'
			AND	[b].[StreamId]			= 'Liquid'
	LEFT OUTER JOIN
		[fact].[FeedStockCrackingParameters]	[p]
			ON	[p].[Refnum]			= [f].[Refnum]
			AND	[p].[CalDateKey]		= [f].[CalDateKey]
			AND	[p].[StreamId]			= [f].[StreamId]
			AND	[p].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[fact].[CompositionQuantityPivot]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[StreamId]			= [f].[StreamId]
			AND	[c].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[fact].[FeedStockDistillationPivot]		[d]
			ON	[d].[Refnum]			= [f].[Refnum]
			AND	[d].[StreamId]			= [f].[StreamId]
			AND	[d].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[fact].[QuantityValue]					[v]
			ON	[v].[Refnum]			= [f].[Refnum]
			AND	[v].[CalDateKey]		= [f].[CalDateKey]
			AND	[v].[StreamId]			= [f].[StreamId]
			AND	[v].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[super].[Composition]					[s]
			ON	[s].[Refnum]			= [f].[Refnum]
			AND	[s].[StreamId]			= [f].[StreamId]
	CROSS APPLY(
		SELECT
			[Amount_Cur] = AVG([q].[Amount_Cur])
		FROM
			[super].[QuantityValue]					[q]
		WHERE	[q].[Refnum]			= [f].[Refnum]
			AND	[q].[StreamId]			= [f].[StreamId]
			AND	[q].[StreamDescription]	= [f].[StreamDescription]
		) [q]
	WHERE
		[f].[Refnum]	= @Refnum;

END;