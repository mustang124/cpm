﻿CREATE PROCEDURE [fact].[Select_GenPlantEnvironment]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-4 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[Emissions_MTd],
		[f].[WasteHazMat_MTd],
		[f].[NOx_LbMBtu],
		[f].[WasteWater_MTd]
	FROM
		[fact].[GenPlantEnvironment]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;