﻿CREATE PROCEDURE [fact].[Select_GenPlantFeedFlex]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-3 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[Capability_Pcnt],
		[f].[Demonstrate_Pcnt]
	FROM
		[fact].[GenPlantFeedFlex]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;