﻿CREATE PROCEDURE [fact].[Select_PlantName]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @OsimCountries	TABLE
	(
		[CountryId]		CHAR(3)		NOT	NULL,
		[CountryName]	VARCHAR(20)	NOT	NULL,
		PRIMARY KEY CLUSTERED([CountryId] ASC),
		UNIQUE NONCLUSTERED([CountryName] ASC)
	);

	DECLARE @OsimCurrencies	TABLE
	(
		[CurrencyId]	VARCHAR(4)	NOT	NULL,
		[CurrencyName]	VARCHAR(32)	NOT	NULL,
		PRIMARY KEY CLUSTERED([CurrencyId] ASC)
	);

	INSERT INTO @OsimCountries([CountryId], [CountryName])
	VALUES
		('ARG', 'Argentina'),
		('AUS', 'Australia'),
		('AUT', 'Austria'),
		('BEL', 'Belgium'),
		('BRA', 'Brazil'),
		('CAN', 'Canada'),
		('CHL', 'Chile'),
		('CHN', 'China'),
		('COL', 'Colombia'),
		('CZE', 'Czech Republic'),
		('FIN', 'Finland'),
		('FRA', 'France'),
		('DEU', 'Germany'),
		('HUN', 'Hungary'),
		('IND', 'India'),
		('IDN', 'Indonesia'),
		('ISR', 'Israel'),
		('ITA', 'Italy'),
		('JPN', 'Japan'),
		('KWT', 'Kuwait'),
		('MYS', 'Malaysia'),
		('MEX', 'Mexico'),
		('NLD', 'Netherlands'),
		('NOR', 'Norway'),
		('POL', 'Poland'),
		('PRT', 'Portugal'),
		('QAT', 'Qatar'),
		('ROU', 'Romania'),
		('RUS', 'Russian Federation'),
		('SAU', 'Saudi Arabia'),
		('SGP', 'Singapore'),
		('SVK', 'Slovakia'),
		('ZAF', 'South Africa'),
		('KOR', 'South Korea'),
		('ESP', 'Spain'),
		('SWE', 'Sweden'),
		('TWN', 'Taiwan'),
		('THA', 'Thailand'),
		('TUR', 'Turkey'),
		('ARE', 'United Arab Emirates'),
		('GBR', 'United Kingdom'),
		('USA', 'United States'),
		('VEN', 'Venezuela');

	INSERT INTO @OsimCurrencies([CurrencyId], [CurrencyName])
	VALUES
		('AED', 'United Arab Emirates Dirham'),
		('ARS', 'Argentine Peso'),
		('AUD', 'Australian Dollar'),
		('BRL', 'Brazilian Real'),
		('CAD', 'Canadian Dollar'),
		('CLP', 'Chilean Peso'),
		('CNY', 'Chinese Yuan'),
		('COP', 'Colombian Peso'),
		('CZK', 'Czech Koruna'),
		('EUR', 'Euro'),
		('GBP', 'Pound Sterling'),
		('HUF', 'Hungarian Forint'),
		('IDR', 'Indonesian Rupiah'),
		('ILS', 'Israeli New Shekel'),
		('INR', 'Indian Rupee'),
		('JPY', 'Japanese Yen'),
		('KRW', 'South Korean Won'),
		('KWD', 'Kuwaiti Dinar'),
		('MXN', 'Mexican Peso'),
		('MYR', 'Malaysian Ringgit'),
		('NOK', 'Norwegian Krone'),
		('PLN', 'Polish Zloty'),
		('QAR', 'Qatari Riyal'),
		('RON', 'Romanian Leu'),
		('RUB', 'Russian Ruble'),
		('SAR', 'Saudi Riyal'),
		('SEK', 'Swedish Krona'),
		('SGD', 'Singapore Dollar'),
		('THB', 'Thai Baht'),
		('TRY', 'Turkish Lira'),
		('TWD', 'New Taiwan Dollar'),
		('USD', 'US Dollar'),
		('VEF', 'Venezuelan Bolívar'),
		('ZAR', 'South African Rand'),

		('ATS', 'Euro'),
		('BEF', 'Euro'),
		('DEM', 'Euro'),
		('ESPK', 'Euro'),
		('FIM', 'Euro'),
		('FRF', 'Euro'),
		('HUFK', 'Hungarian Forint'),
		('IDRK', 'Indonesian Rupiah'),
		('ITLK', 'Euro'),
		('JPYK', 'Japanese Yen'),
		('KRWK', 'South Korean Won'),
		('NLG', 'Euro'),
		('PTEK', 'Euro'),
		('SKK', 'Euro'),
		('TRLM', 'Turkish Lira'),
		('UAH', 'Ukrainian Hryvnia');

	SELECT
			[RefnumEntered]			= @Refnum,
			[Refnum]				= COALESCE([f].[Refnum], [t].[Refnum]),
			[SmallRefnum]			= COALESCE([f].[SmallRefnum], [t].[Refnum]),

			[Co]					= COALESCE([f].[Co],	CASE WHEN [t].[Refnum] IS NOT NULL THEN 'Solomon'			END),
			[Loc]					= COALESCE([f].[Loc],	CASE WHEN [t].[Refnum] IS NOT NULL THEN 'Dallas'			END),
			[CoLoc]					= COALESCE([f].[CoLoc],	CASE WHEN [t].[Refnum] IS NOT NULL THEN 'Solomon - Dallas'	END),

		[f].[NameFull],
		[f].[NameTitle],
		[f].[AddressPOBox],
		[f].[AddressStreet],
		[f].[AddressCity],
		[f].[AddressState],
		[f].[AddressCountryId],
			[AddressCountryName]	= [l].[CountryName],

		[f].[AddressPostalCode],
		[f].[NumberVoice],
		[f].[NumberFax],
		[f].[eMail],

		[f].[PricingName],
		[f].[PrincingEMail],
		[f].[DataName],
		[f].[DataEMail],

			[UomId]					= COALESCE([f].[UomId], [t].[UomId]),
		[f].[UomNumber],
			[UomOsim]				= CASE WHEN (([f].[PreviousUom] = 'US') OR ([f].[UomNumber] IS NULL)) THEN 1 ELSE 2 END,
			[StudyYear]				= COALESCE([f].[StudyYear], [t].[_DataYear]),

		[f].[PreviousRefnum],
			[PreviousStudyYear]		= COALESCE([f].[PreviousStudyYear], [t].[_DataYear] - 2),
			[PreviousForexRate]		= COALESCE([f].[PreviousForexRate], 1.0),

			[CurrencyFcn]			= COALESCE([f].[CurrencyFcn], [t].[CurrencyFcn]),
			[CurrencyRpt]			= COALESCE([f].[CurrencyRpt], [t].[CurrencyRpt]),
		[f].[CountryId],
			[CurrencyFcnName]		= [x].[CurrencyName]
	FROM
		(VALUES(@Refnum))			[r]([Refnum])

	LEFT OUTER JOIN
		[fact].[PlantName]			[f]
			ON	[f].[Refnum]		= [r].[Refnum]

	LEFT OUTER JOIN
		[fact].[TSortClient]		[t]
			ON	[t].[Refnum]		= [r].[Refnum]

	LEFT OUTER JOIN
		@OsimCountries				[l]
			ON	[l].[CountryId]		= [f].[AddressCountryId]

	LEFT OUTER JOIN
		@OsimCurrencies				[x]
			ON	[x].[CurrencyId]	= COALESCE([f].[CurrencyFcn], [t].[CurrencyFcn]);

END;