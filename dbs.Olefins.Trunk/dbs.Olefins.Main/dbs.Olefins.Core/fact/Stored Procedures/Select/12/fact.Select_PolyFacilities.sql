﻿CREATE PROCEDURE [fact].[Select_PolyFacilities]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 12-1 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AssetId],
			[TrainId]		= RIGHT([f].[AssetId], 2),
		[f].[PolyName],
		[f].[PolyCity],
		[f].[PolyState],
		[f].[PolyCountryId],
			[PolyCountry]		= [x].[OsimCountryName],
		[f].[Capacity_kMT],
		[f].[Stream_MTd],
		[f].[ReactorTrains_Count],
		[f].[StartUp_Year],
		[f].[PolymerFamilyId],
		[f].[PolymerTechId],
		[f].[PrimeResin_Pcnt]
	FROM
		[fact].[PolyFacilities]	[f]
	LEFT OUTER JOIN
		(VALUES
			('ARG', 'ARGENTINA'),
			('AUS', 'AUSTRALIA'),
			('AUT', 'AUSTRIA'),
			('BEL', 'BELGIUM'),
			('BRA', 'BRAZIL'),
			('CAN', 'CANADA'),
			('CHL', 'CHILE'),
			('COL', 'COLOMBIA'),
			('CZE', 'CZECH REP'),
			('FIN', 'FINLAND'),
			('FRA', 'FRANCE'),
			('DEU', 'GERMANY'),
			('HUN', 'HUNGARY'),
			('IND', 'INDIA'),
			('ISR', 'ISRAEL'),
			('ITA', 'ITALY'),
			('IDN', 'INDONESIA'),
			('JPN', 'JAPAN'),
			('KOR', 'SOUTH KOREA'),
			('KWT', 'KUWAIT'),
			('MYS', 'MALAYSIA'),
			('MEX', 'MEXICO'),
			('NLD', 'NETHERLANDS'),
			('NOR', 'NORWAY'),
			('POL', 'POLAND'),
			('PRT', 'PORTUGAL'),
			('CHN', 'PRC CHINA'),
			('QAT', 'QATAR'),
			('ROU', 'ROMANIA'),
			('RUS', 'RUSSIA'),
			('SAU', 'SAUDI ARABIA'),
			('SGP', 'SINGAPORE'),
			('SVK', 'SLOVAKIA'),
			('ZAF', 'SOUTH AFRICA'),
			('ESP', 'SPAIN'),
			('SWE', 'SWEDEN'),
			('TWN', 'TAIWAN'),
			('THA', 'THAILAND'),
			('TUR', 'TURKEY'),
			('ARE', 'UAE'),
			('GBR', 'UNITED KINGDOM'),
			('USA', 'USA')
			) 	[x]([CountryId], [OsimCountryName])
		ON	[x].[CountryId]	= [f].[PolyCountryId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;