﻿CREATE PROCEDURE [fact].[Select_FeedSelModels]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (4)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[ModelId],
		[f].[Implemented_Bit],
			[Implemented_YN]	= CASE WHEN [f].[Implemented_Bit] = 1 THEN 'Y' ELSE 'N' END,
		[f].[InCompany_Bit],
		[f].[LocationId]
	FROM
		[fact].[FeedSelModels]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;