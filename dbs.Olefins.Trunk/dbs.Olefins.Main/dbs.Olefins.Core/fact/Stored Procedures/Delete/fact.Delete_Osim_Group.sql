﻿CREATE PROCEDURE [fact].[Delete_Osim_Group]
(
	@GroupId	VARCHAR(25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.CapacityGrowth';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[EnergyLHValue]					t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.QuantityValue';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[QuantityValue]					t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.FeedStockDistillation';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[FeedStockDistillation]			t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.FeedStockCrackingParameters';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[FeedStockCrackingParameters]	t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.CompositionQuantity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[CompositionQuantity]			t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.Quantity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[Quantity]						t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.Facilities';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[Facilities]					t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

		SET	@ProcedureDesc = NCHAR(9) + 'DELETE FROM fact.TSortClient';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM [fact].[TSortClient]					t
		INNER JOIN @fpl								fpl
			ON	fpl.[Refnum] = t.[Refnum];

	END TRY
	BEGIN CATCH

		SET @GroupId = 'G:' + @GroupId;
		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;