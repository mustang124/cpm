﻿CREATE PROCEDURE [fact].[Insert_GenPlantInventory]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantInventory(Refnum, CalDateKey, FacilityId, StorageCapacity_kMT, StorageLevel_Pcnt)
		SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvFacilityId(i.TankType)
			, COALESCE(i.KMT, 0.0)
			, CASE WHEN COALESCE(i.YEIL, 0.0) > 1.0 THEN 1.0 ELSE COALESCE(i.YEIL, 0.0) END  * 100.0 [Pcnt]
		FROM stgFact.Inventory i
		INNER JOIN stgFact.TSort t ON t.Refnum = i.Refnum
		WHERE i.TankType <> 'TT'
			AND	(COALESCE(i.YEIL, 0.0) <> 0.0 OR COALESCE(i.KMT, 0.0) <> 0.0)
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;