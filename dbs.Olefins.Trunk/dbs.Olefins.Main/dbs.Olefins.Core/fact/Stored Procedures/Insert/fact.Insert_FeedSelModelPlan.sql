﻿CREATE PROCEDURE [fact].[Insert_FeedSelModelPlan]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSelModelPlan(Refnum, CalDateKey, PlanId
			, Daily_Bit, BiWeekly_Bit, Weekly_Bit, BiMonthly_Bit, Monthly_Bit, LessMonthly_Bit)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, p.PlanId
			, etl.ConvBit(p.Daily)
			, etl.ConvBit(p.BiWeekly)
			, etl.ConvBit(p.Weekly)
			, etl.ConvBit(p.BiMonthly)
			, etl.ConvBit(p.Monthly)
			, etl.ConvBit(p.LessMonthly)
		FROM stgFact.FeedPlan p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;