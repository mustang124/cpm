﻿CREATE PROCEDURE [fact].[Insert_FacilitiesHydroTreat]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesHydroTreat(Refnum, CalDateKey, FacilityId, HTTypeId
				, FeedRate_kBsd, FeedPressure_PSIg, FeedProcessed_kMT, FeedDensity_SG)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'TowerPyroGasHT'										[FacilityId]
			, etl.ConvFacilityHydroTypeID(f.PyrGasHydroType)		[HTTypeId]
			, f.PGasHydroKBSD										[KBSD]
			, f.PGasHydroPSIG										[PSIG]
			, f.PyrGasHydroMT										[kMT]
			, f.SG_NonBnzPGH										[SG]
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t
			ON	t.Refnum = f.Refnum
		INNER JOIN fact.Facilities h
			ON	h.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)
			AND h.CalDateKey = etl.ConvDateKey(t.StudyYear)						
			AND h.FacilityId =  'TowerPyroGasHT'
		WHERE (f.PyrGasHydroType IS NOT NULL
			OR	f.PGasHydroKBSD IS NOT NULL
			OR	f.PGasHydroPSIG IS NOT NULL
			OR	f.PyrGasHydroMT IS NOT NULL
			OR	f.SG_NonBnzPGH IS NOT NULL)
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;