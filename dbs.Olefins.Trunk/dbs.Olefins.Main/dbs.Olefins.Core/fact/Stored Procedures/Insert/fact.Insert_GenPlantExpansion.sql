﻿CREATE PROCEDURE [fact].[Insert_GenPlantExpansion]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantExpansion(Refnum, CalDateKey, AccountId, StreamId, CapacityAdded_kMT)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.AccountId
			, 'Ethylene'	[StreamId]
			, u.kMT
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, e.Onsite
				, e.Exist
			FROM stgFact.CapitalExp e
			INNER JOIN stgFact.TSort t ON t.Refnum = e.Refnum
			WHERE e.CptlCode = 'ADD'
			AND t.Refnum = @sRefnum
			) b
			UNPIVOT (
			kMT FOR AccountId IN (
				  Onsite
				, Exist
				)
			) u
		WHERE u.kMT > 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;