﻿CREATE PROCEDURE [fact].[Insert_Maint_Overhead]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Maint(Refnum, CalDateKey, FacilityId, CurrencyRpt, MaintMaterial_Cur, MaintLabor_Cur, TaMaterial_Cur, TaLabor_Cur)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, 'OverHead'
			, 'USD'
			, m.AllocOHMaintMatl
			, m.AllocOHMaintLabor
			, m.AllocOHTAMatl
			, m.AllocOHTALabor
		FROM stgFact.TotMaintCost m
		INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
		WHERE	(m.AllocOHMaintMatl IS NOT NULL
			OR	m.AllocOHMaintLabor IS NOT NULL
			OR	m.AllocOHTAMatl IS NOT NULL
			OR	m.AllocOHTALabor IS NOT NULL)
			AND	t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;