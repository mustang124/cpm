﻿CREATE PROCEDURE [fact].[Insert_ReliabilityTA]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityTA(Refnum, CalDateKey, TrainId, SchedId
			, Interval_Mnths, DownTime_Hrs, CurrencyRpt, TurnAroundCost_MCur, TurnAround_ManHrs, TurnAround_Date
			, StreamId, Capacity_Pcnt
			, MiniTA_Bit
		)
		SELECT
			  u1.Refnum
			, u1.CalDateKey
			, u1.TrainId
			, LEFT(u1.SchedId, 1)	[SchedId]
			, u1.Interval_Mnths
			, u1.DownTime_Hrs
			, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN 'USD'				END
			, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN u1.TACost			END
			, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN u1.TAManHrs		END
			, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN u1.TaDate			END
			, CASE WHEN u1.Interval_Mnths IS NOT NULL	THEN 'Ethylene'			END
			, CASE WHEN u1.Interval_Mnths IS NOT NULL	THEN u1.PctTotEthylCap	END
			, etl.ConvBit(u1.TAMini)
		FROM (
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)					[Refnum]
				, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
				, a.TrainId
				, a.PlanInterval
				, a.ActInterval
				, a.PlanDT
				, a.ActDT
				, a.TACost
				, a.TAManHrs
				, CONVERT(DATE, CASE WHEN YEAR(a.TaDate) > 1900 THEN a.TaDate END)	[TaDate]
				, a.PctTotEthylCap
				, a.TAMini
			FROM stgFact.TADT a
			INNER JOIN stgFact.TSort t ON t.Refnum = a.Refnum
			WHERE	(a.PlanInterval		> 0.0
				OR	a.PlanDT			> 0.0
				OR	a.ActInterval		> 0.0
				OR	a.ActDT				> 0.0
				OR	a.TACost			> 0.0
				OR	a.TAManHrs			> 0.0
				OR	a.PctTotEthylCap	> 0.0
				)
				AND t.Refnum = @sRefnum
			) p
			UNPIVOT (
			Interval_Mnths FOR SchedId IN (
				  PlanInterval
				, ActInterval
				)
			) u0
			UNPIVOT (
			DownTime_Hrs FOR SchedIDd IN (
				  PlanDT
				, ActDT
				)
			) u1
		WHERE
			LEFT(u1.SchedId, 1) = LEFT(u1.SchedIDd, 1);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;