﻿CREATE TABLE [fact].[MetaEnergy] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [LHValue_MBTU]   REAL               NOT NULL,
    [_LHValue_GJ]    AS                 (CONVERT([real],[LHValue_MBTU]*(1.05499994754791),(1))) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MetaEnergy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MetaEnergy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MetaEnergy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MetaEnergy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MetaEnergy] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_MetaEnergy_LHValue_MBTU] CHECK ([LHValue_MBTU]>=(0.0)),
    CONSTRAINT [FK_MetaEnergy_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_MetaEnergy_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MetaEnergy_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_MetaEnergy_u]
	ON [fact].[MetaEnergy]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[MetaEnergy]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[MetaEnergy].Refnum		= INSERTED.Refnum
		AND [fact].[MetaEnergy].AccountId	= INSERTED.AccountId
		AND [fact].[MetaEnergy].CalDateKey	= INSERTED.CalDateKey;

END;