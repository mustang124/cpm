﻿CREATE TABLE [fact].[FeedSel] (
    [Refnum]                VARCHAR (25)       NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [MinParaffin_Pcnt]      REAL               NULL,
    [MaxVaporPresFRS_RVP]   REAL               NULL,
    [YieldPattern_Count]    INT                NULL,
    [YieldPatternLP_Count]  INT                NULL,
    [YieldGroup_Count]      INT                NULL,
    [YPS_Lummus]            BIT                NULL,
    [YPS_SPYRO]             BIT                NULL,
    [YPS_SelfDev]           BIT                NULL,
    [YPS_Other]             BIT                NULL,
    [YPS_OtherName]         NVARCHAR (256)     NULL,
    [YPS_RPT]               BIT                NULL,
    [LP_Aspen]              BIT                NULL,
    [LP_DeltaBase_No]       TINYINT            NULL,
    [LP_DeltaBase_NotSure]  TINYINT            NULL,
    [LP_DeltaBase_RPT]      BIT                NULL,
    [LP_DeltaBase_Yes]      BIT                NULL,
    [LP_Haverly]            BIT                NULL,
    [LP_Honeywell]          BIT                NULL,
    [LP_MixInt_No]          TINYINT            NULL,
    [LP_MixInt_NotSure]     TINYINT            NULL,
    [LP_MixInt_RPT]         BIT                NULL,
    [LP_MixInt_Yes]         BIT                NULL,
    [LP_Online_No]          TINYINT            NULL,
    [LP_Online_NotSure]     TINYINT            NULL,
    [LP_Online_RPT]         BIT                NULL,
    [LP_Online_Yes]         BIT                NULL,
    [LP_Other]              BIT                NULL,
    [LP_OtherName]          NVARCHAR (256)     NULL,
    [LP_Recur_No]           TINYINT            NULL,
    [LP_Recur_NotSure]      TINYINT            NULL,
    [LP_Recur_RPT]          BIT                NULL,
    [LP_Recur_Yes]          BIT                NULL,
    [LP_RPT]                BIT                NULL,
    [LP_SelfDev]            BIT                NULL,
    [LP_RowCount]           INT                NULL,
    [BeyondCrack_None]      BIT                NULL,
    [BeyondCrack_C4]        BIT                NULL,
    [BeyondCrack_C5]        BIT                NULL,
    [BeyondCrack_Pygas]     BIT                NULL,
    [BeyondCrack_RPT]       BIT                NULL,
    [BeyondCrack_BTX]       BIT                NULL,
    [BeyondCrack_OthChem]   BIT                NULL,
    [BeyondCrack_OthRefine] BIT                NULL,
    [BeyondCrack_Oth]       BIT                NULL,
    [BeyondCrack_OtherName] NVARCHAR (256)     NULL,
    [DecisionPlant_Bit]     BIT                NULL,
    [DecisionCorp_Bit]      BIT                NULL,
    [PlanTime_Days]         REAL               NULL,
    [EffFeedChg_Levels]     REAL               NULL,
    [FeedPurchase_Levels]   REAL               NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSel_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_FeedSel_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_FeedSel_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_FeedSel_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSel] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_FeedSel_BeyondCrack_OtherName] CHECK (len([BeyondCrack_OtherName])>(0)),
    CONSTRAINT [CL_FeedSel_LP_OtherName] CHECK (len([LP_OtherName])>(0)),
    CONSTRAINT [CL_FeedSel_YPS_OtherName] CHECK (len([YPS_OtherName])>(0)),
    CONSTRAINT [CR_FeedSel_EffFeedChg_Levels] CHECK ([EffFeedChg_Levels]>=(0)),
    CONSTRAINT [CR_FeedSel_FeedPurchase_Levels] CHECK ([FeedPurchase_Levels]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_DeltaBase_No] CHECK ([LP_DeltaBase_No]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_DeltaBase_NotSure] CHECK ([LP_DeltaBase_NotSure]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_MixInt_No] CHECK ([LP_MixInt_No]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_MixInt_NotSure] CHECK ([LP_MixInt_NotSure]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_Online_No] CHECK ([LP_Online_No]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_Online_NotSure] CHECK ([LP_Online_NotSure]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_Recur_No] CHECK ([LP_Recur_No]>=(0)),
    CONSTRAINT [CR_FeedSel_LP_Recur_NotSure] CHECK ([LP_Recur_NotSure]>=(0)),
    CONSTRAINT [CR_FeedSel_MaxVaporPresFRS_RVP] CHECK ([MaxVaporPresFRS_RVP]>=(0.0)),
    CONSTRAINT [CR_FeedSel_MinParaffin_Pcnt] CHECK ([MinParaffin_Pcnt]>=(0.0) AND [MinParaffin_Pcnt]<=(100.0)),
    CONSTRAINT [CR_FeedSel_PlanTime_Days] CHECK ([PlanTime_Days]>=(0.0)),
    CONSTRAINT [CR_FeedSel_YieldGroup_Count] CHECK ([YieldGroup_Count]>=(0)),
    CONSTRAINT [CR_FeedSel_YieldPattern_Count] CHECK ([YieldPattern_Count]>=(0)),
    CONSTRAINT [CR_FeedSel_YieldPatternLP_Count] CHECK ([YieldPatternLP_Count]>=(0)),
    CONSTRAINT [FK_FeedSel_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSel_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSel_u]
	ON [fact].[FeedSel]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSel]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSel].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSel].CalDateKey	= INSERTED.CalDateKey;

END;