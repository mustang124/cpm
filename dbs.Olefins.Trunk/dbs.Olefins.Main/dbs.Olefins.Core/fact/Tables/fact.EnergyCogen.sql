﻿CREATE TABLE [fact].[EnergyCogen] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [HasCogen_Bit]   BIT                NULL,
    [Thermal_Pcnt]   REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyCogen_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EnergyCogen_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EnergyCogen_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EnergyCogen_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyCogen] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyCogen_Thermal_Pcnt] CHECK ([Thermal_Pcnt]>=(0.0) AND [Thermal_Pcnt]<=(100.0)),
    CONSTRAINT [FK_EnergyCogen_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyCogen_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergyCogen_u]
	ON [fact].[EnergyCogen]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyCogen]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyCogen].Refnum		= INSERTED.Refnum
		AND [fact].[EnergyCogen].CalDateKey	= INSERTED.CalDateKey;

END;