﻿CREATE VIEW [fact].[TSort]
WITH SCHEMABINDING
AS
SELECT
	[t].[Refnum],
	[t].[SmallRefnum],
	[t].[CalDateKey],

	[t].[Co],
	[t].[Loc],
	[t].[CoLoc],

		[PlantCompanyName]	= RTRIM(LTRIM([x].[PlantCompanyName])),
		[PlantAssetName]	= RTRIM(LTRIM([x].[PlantAssetName])),

		[UomId]				= RTRIM(LTRIM([x].[UomId])),
		[UomNumber]			= CASE WHEN RTRIM(LTRIM([x].[UomId])) = 'US' THEN 1 ELSE 2 END,
		[CurrencyFcn]		= RTRIM(LTRIM([x].[CurrencyFcn])),
		[CurrencyRpt]		= RTRIM(LTRIM([x].[CurrencyRpt])),
		[DataYear]			= [x].[_DataYear],
		[NumDays]			= DATEPART(DY, CONVERT(CHAR(4), [t].[StudyYear]) + '-12-31'),

	[t].[StudyYear],
	[t].[Consultant],
	[t].[FactorSetId],
	[t].[StudyId],

	--	Asset

	[t].[AssetId],
	[t].[AssetName],
	[t].[AssetDetail],
	[t].[AssetNameReport],
	[t].[SubscriberAssetName],
	[t].[SubscriberAssetDetail],
	[t].[AssetPassWord],

	[t].[Region],
	[t].[RegionId],
	[t].[EconRegionId],
	[t].[CountryId],
	[t].[CountryName],
	[t].[StateName],

	[t].[Longitude_Deg],
	[t].[Latitude_Deg],
	[t].[GeogLoc_WGS84],

	[t].[ScenarioId],
	[t].[ScenarioName],
	[t].[ScenarioDetail],

	--	Company

	[t].[CompanyId],
	[t].[CompanyName],
	[t].[CompanyDetail],
	[t].[CompanyNameReport],
	[t].[SubscriberCompanyName],
	[t].[SubscriberCompanyDetail],
	[t].[CompanyPassWord],

	--	Contact
	[c].[NameFull],
	[c].[NameTitle],
	[c].[AddressPOBox],
	[c].[AddressStreet],
	[c].[AddressCity],
	[c].[AddressState],
	[c].[AddressCountryId],
		[AddressCountryName]	= [l].[CountryName],
	[c].[AddressPostalCode],
	[c].[NumberVoice],
	[c].[NumberFax],
	[c].[eMail],

		[PricingName]			= [p].[NameFull],
		[PrincingEMail]			= [p].[eMail],
		[DataName]				= [d].[NameFull],
		[DataEMail]				= [d].[eMail],

	[t].[Comments],
	[t].[ContactCode],
	[t].[RefDirectory]

FROM
	[cons].[TSort]					[t]	WITH (NOEXPAND)

LEFT OUTER JOIN
	[fact].[TSortClient]			[x]
		ON	[x].[Refnum]			= [t].[Refnum]

LEFT OUTER JOIN
	[fact].[TSortContact]			[c]
		ON	[c].[Refnum]			= [t].[Refnum]
		AND	[c].[ContactTypeId]		= 'Coord'

LEFT OUTER JOIN
	[dim].[Country_LookUp]			[l]
		ON	[l].[CountryId]			= [c].[AddressCountryId]

LEFT OUTER JOIN
	[fact].[TSortContact]			[p]
		ON	[p].[Refnum]			= [t].[Refnum]
		AND	[p].[ContactTypeId]		= 'PricingCoord'

LEFT OUTER JOIN
	[fact].[TSortContact]			[d]
		ON	[d].[Refnum]			= [t].[Refnum]
		AND	[d].[ContactTypeId]		= 'DataCoord';
GO