﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal partial class Personnel
			{
				private static RangeReference Plant_FTE = new RangeReference(Tabs.T11, 0, 7, SqlDbType.Float);
				private static RangeReference Corporate_FTE = new RangeReference(Tabs.T11, 0, 8, SqlDbType.Float);

				private static RangeReference BeaProdPlan = new RangeReference(Tabs.T11, 130, 0, SqlDbType.Float);
				private static RangeReference BeaProdPlanExperienced = new RangeReference(Tabs.T11, 131, 0, SqlDbType.Float);
				private static RangeReference BeaTrader = new RangeReference(Tabs.T11, 132, 0, SqlDbType.Float);
				private static RangeReference BeaAnalyst = new RangeReference(Tabs.T11, 133, 0, SqlDbType.Float);
				private static RangeReference BeaLogistics = new RangeReference(Tabs.T11, 134, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_FeedSelPersonnel]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "PersId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Plant_FTE", Personnel.Plant_FTE);
							d.Add("Corporate_FTE", Personnel.Corporate_FTE);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("BeaProdPlan", Personnel.BeaProdPlan);
							d.Add("BeaProdPlanExperienced", Personnel.BeaProdPlanExperienced);
							d.Add("BeaTrader", Personnel.BeaTrader);
							d.Add("BeaAnalyst", Personnel.BeaAnalyst);
							d.Add("BeaLogistics", Personnel.BeaLogistics);

							return d;
						}
					}
				}
			}
		}
	}
}