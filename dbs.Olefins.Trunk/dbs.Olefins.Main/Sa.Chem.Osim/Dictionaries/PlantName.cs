﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal class PlantName
		{
			private static RangeReference Co = new RangeReference(Tabs.T00, 6, 4, SqlDbType.VarChar);
			private static RangeReference Loc = new RangeReference(Tabs.T00, 7, 4, SqlDbType.VarChar);

			private static RangeReference CoLoc = new RangeReference(Tabs.T00, 10, 4, SqlDbType.VarChar);

			private static RangeReference NameFull = new RangeReference(Tabs.T00, 12, 4, SqlDbType.VarChar);
			private static RangeReference NameTitle = new RangeReference(Tabs.T00, 13, 4, SqlDbType.VarChar);
			private static RangeReference AddressPOBox = new RangeReference(Tabs.T00, 14, 4, SqlDbType.VarChar);
			private static RangeReference AddressStreet = new RangeReference(Tabs.T00, 15, 4, SqlDbType.VarChar);
			private static RangeReference AddressCity = new RangeReference(Tabs.T00, 16, 4, SqlDbType.VarChar);
			private static RangeReference AddressState = new RangeReference(Tabs.T00, 17, 4, SqlDbType.VarChar);
			private static RangeReference AddressCountryName = new RangeReference(Tabs.T00, 18, 4, SqlDbType.VarChar);
			private static RangeReference AddressPostalCode = new RangeReference(Tabs.T00, 19, 4, SqlDbType.VarChar);
			private static RangeReference NumberVoice = new RangeReference(Tabs.T00, 20, 4, SqlDbType.VarChar);
			private static RangeReference NumberFax = new RangeReference(Tabs.T00, 21, 4, SqlDbType.VarChar);
			private static RangeReference eMail = new RangeReference(Tabs.T00, 22, 4, SqlDbType.VarChar);
			private static RangeReference PricingName = new RangeReference(Tabs.T00, 23, 4, SqlDbType.VarChar);
			private static RangeReference PrincingEMail = new RangeReference(Tabs.T00, 24, 4, SqlDbType.VarChar);
			private static RangeReference DataName = new RangeReference(Tabs.T00, 25, 4, SqlDbType.VarChar);
			private static RangeReference DataEMail = new RangeReference(Tabs.T00, 26, 4, SqlDbType.VarChar);

			private static RangeReference sRefnum = new RangeReference(Tabs.T00, 29, 4, SqlDbType.VarChar, true);

			private static RangeReference Refnum = new RangeReference(Tabs.T00, 46, 4, SqlDbType.VarChar, true);
			private static RangeReference PreviousRefnum = new RangeReference(Tabs.T00, 47, 4, SqlDbType.VarChar, true);
			private static RangeReference StudyYear = new RangeReference(Tabs.T00, 48, 4, SqlDbType.SmallInt, true);
			private static RangeReference PreviousStudyYear = new RangeReference(Tabs.T00, 49, 4, SqlDbType.SmallInt, true);

			private static RangeReference CurrencyFcnName = new RangeReference(Tabs.T04_Current, 4, 6, SqlDbType.VarChar);
			private static RangeReference PreviousForexRate = new RangeReference(Tabs.T04_History, 5, 6, SqlDbType.Float, true);

			private static RangeReference UomOsim = new RangeReference(Tabs.SysConversion, 2, 2, SqlDbType.SmallInt, true);

			internal class Upload : TransferData.IUpload
			{
				public string StoredProcedure
				{
					get
					{
						return "[stgFact].[Insert_TSort]";
					}
				}

				public Dictionary<string, RangeReference> Parameters
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("@StudyYear", StudyYear);

						d.Add("@Co", Co);
						d.Add("@Loc", Loc);
						d.Add("@CoLoc", CoLoc);

						d.Add("@CoordName", NameFull);
						d.Add("@CoordTitle", NameTitle);
						d.Add("@POBox", AddressPOBox);
						d.Add("@Street", AddressStreet);
						d.Add("@City", AddressCity);
						d.Add("@State", AddressState);
						d.Add("@Country", AddressCountryName);
						d.Add("@Zip", AddressPostalCode);
						d.Add("@Telephone", NumberVoice);
						d.Add("@Fax", NumberFax);
						d.Add("@WWW", eMail);

						d.Add("@PricingContact", PricingName);
						d.Add("@PricingContactEmail", PrincingEMail);
						d.Add("@DCContact", DataName);
						d.Add("@DCContactEmail", DataEMail);

						d.Add("@UOM", UomOsim);

						return d;
					}
				}
			}

			internal class Download : TransferData.IDownload
			{
				public string StoredProcedure
				{
					get
					{
						return "[fact].[Select_PlantName]";
					}
				}

				public string LookUpColumn
				{
					get
					{
						return string.Empty;
					}
				}

				public Dictionary<string, RangeReference> Columns
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("Co", Co);
						d.Add("Loc", Loc);

						d.Add("NameFull", NameFull);
						d.Add("NameTitle", NameTitle);
						d.Add("AddressPOBox", AddressPOBox);
						d.Add("AddressStreet", AddressStreet);
						d.Add("AddressCity", AddressCity);
						d.Add("AddressState", AddressState);
						d.Add("AddressCountryName", AddressCountryName);
						d.Add("AddressPostalCode", AddressPostalCode);
						d.Add("NumberVoice", NumberVoice);
						d.Add("NumberFax", NumberFax);
						d.Add("eMail", eMail);

						d.Add("PricingName", PricingName);
						d.Add("PrincingEMail", PrincingEMail);
						d.Add("DataName", DataName);
						d.Add("DataEMail", DataEMail);

						d.Add("Refnum", Refnum);
						d.Add("PreviousRefnum", PreviousRefnum);
						d.Add("StudyYear", StudyYear);
						d.Add("PreviousStudyYear", PreviousStudyYear);

						d.Add("CurrencyFcnName", CurrencyFcnName);
						d.Add("PreviousForexRate", PreviousForexRate);

						d.Add("UomOsim", UomOsim);

						return d;
					}
				}

				public Dictionary<string, RangeReference> Items
				{
					get
					{
						return new Dictionary<string, RangeReference>();
					}
				}
			}
		}
	}
}