﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Prod
			{
				internal partial class Ppfc
				{
					internal class Composition
					{
						private static RangeReference H2 = new RangeReference(Tabs.T03, 34, 8, SqlDbType.Float, 100.0);
						private static RangeReference CH4 = new RangeReference(Tabs.T03, 35, 8, SqlDbType.Float, 100.0);
						private static RangeReference C2H4 = new RangeReference(Tabs.T03, 36, 8, SqlDbType.Float, 100.0);
						private static RangeReference Other = new RangeReference(Tabs.T03, 37, 8, SqlDbType.Float, 100.0);

						internal class Download : TransferData.IDownload
						{
							public string StoredProcedure
							{
								get
								{
									return "[fact].[Select_StreamsPpfcComposition]";
								}
							}

							public string LookUpColumn
							{
								get
								{
									return string.Empty;
								}
							}

							public Dictionary<string, RangeReference> Columns
							{
								get
								{
									Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

									d.Add("H2", Composition.H2);
									d.Add("CH4", Composition.CH4);
									d.Add("C2H4", Composition.C2H4);
									d.Add("Other", Composition.Other);

									return d;
								}
							}

							public Dictionary<string, RangeReference> Items
							{
								get
								{
									return new Dictionary<string, RangeReference>();
								}
							}
						}
					}
				}
			}
		}
	}
}