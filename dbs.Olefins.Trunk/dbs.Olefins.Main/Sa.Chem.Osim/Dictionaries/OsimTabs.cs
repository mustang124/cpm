﻿namespace Sa.Chem
{
	public partial class Osim
	{
		internal struct Tabs
		{
			// Current must end with a space
			internal const string Current = "Table ";

			// History must end with a space
			internal const string History = "History ";

			// Upload must end with a space
			internal const string Upload = "Upload ";

			internal const string T00 = "PlantName";

			internal const string T01_01 = "1-1";
			internal const string T01_02 = "1-2";
			internal const string T01_03 = "1-3";

			internal const string T02_A1 = "2A-1";
			internal const string T02_A2 = "2A-2";
			internal const string T02_B = "2B";
			internal const string T02_C = "2C";

			internal const string T03 = "3";

			internal const string T04 = "4";
			internal const string T04_Current = Current + T04;
			internal const string T04_History = History + T04;

			internal const string T05_A = "5A";
			internal const string T05_B = "5B";

			internal const string T06_01 = "6-1";
			internal const string T06_02 = "6-2";
			internal const string T06_03 = "6-3";
			internal const string T06_04 = "6-4";
			internal const string T06_05 = "6-5";
			internal const string T06_06 = "6-6";

			internal const string T07 = "7";

			internal const string T08_01 = "8-1";
			internal const string T08_02 = "8-2";
			internal const string T08_03 = "8-3";
			internal const string T08_04 = "8-4";

			internal const string T09_01 = "9-1";
			internal const string T09_02 = "9-2";
			internal const string T09_03 = "9-3";
			internal const string T09_04 = "9-4";
			internal const string T09_05 = "9-5";
			internal const string T09_06 = "9-6";

			internal const string T10_01 = "10-1";
			internal const string T10_02 = "10-2";

			internal const string T11 = "11";

			internal const string T12_01 = "12-1";
			internal const string T12_02 = "12-2";

			internal const string T13 = "13";

			internal const string TChkxTab = "CheckCrossTab";

			internal const string SysConversion = "Conv";
		}
	}
}