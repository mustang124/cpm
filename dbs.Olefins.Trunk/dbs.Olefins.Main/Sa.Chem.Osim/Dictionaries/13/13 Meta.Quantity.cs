﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Metathesis
		{
			internal partial class Quantity
			{
				private static RangeReference MetaOCEthylene = new RangeReference(Tabs.T13, 15, 0, SqlDbType.Float);
				private static RangeReference MetaOCButene1 = new RangeReference(Tabs.T13, 16, 0, SqlDbType.Float);
				private static RangeReference MetaOCButene2 = new RangeReference(Tabs.T13, 17, 0, SqlDbType.Float);
				private static RangeReference MetaOCButanes = new RangeReference(Tabs.T13, 18, 0, SqlDbType.Float);
				private static RangeReference MetaOCOther = new RangeReference(Tabs.T13, 19, 0, SqlDbType.Float);

				private static RangeReference MetaPurchEthylene = new RangeReference(Tabs.T13, 20, 0, SqlDbType.Float);
				private static RangeReference MetaPurchButene1 = new RangeReference(Tabs.T13, 21, 0, SqlDbType.Float);
				private static RangeReference MetaPurchButene2 = new RangeReference(Tabs.T13, 22, 0, SqlDbType.Float);
				private static RangeReference MetaPurchButane = new RangeReference(Tabs.T13, 23, 0, SqlDbType.Float);
				private static RangeReference MetaPurchOther = new RangeReference(Tabs.T13, 24, 0, SqlDbType.Float);

				private static RangeReference MetaProdPropylenePG = new RangeReference(Tabs.T13, 31, 0, SqlDbType.Float);
				private static RangeReference MetaProdUnReactLightPurge = new RangeReference(Tabs.T13, 32, 0, SqlDbType.Float);
				private static RangeReference MetaProdUnReactButane = new RangeReference(Tabs.T13, 33, 0, SqlDbType.Float);
				private static RangeReference MetaProdC5 = new RangeReference(Tabs.T13, 34, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MetaQuantity]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							return Metathesis.Values;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("MetaOCEthylene", Quantity.MetaOCEthylene);
							d.Add("MetaOCButene1", Quantity.MetaOCButene1);
							d.Add("MetaOCButene2", Quantity.MetaOCButene2);
							d.Add("MetaOCButanes", Quantity.MetaOCButanes);
							d.Add("MetaOCOther", Quantity.MetaOCOther);

							d.Add("MetaPurchEthylene", Quantity.MetaPurchEthylene);
							d.Add("MetaPurchButene1", Quantity.MetaPurchButene1);
							d.Add("MetaPurchButene2", Quantity.MetaPurchButene2);
							d.Add("MetaPurchButane", Quantity.MetaPurchButane);
							d.Add("MetaPurchOther", Quantity.MetaPurchOther);

							d.Add("MetaProdPropylenePG", Quantity.MetaProdPropylenePG);
							d.Add("MetaProdUnReactLightPurge", Quantity.MetaProdUnReactLightPurge);
							d.Add("MetaProdUnReactButane", Quantity.MetaProdUnReactButane);
							d.Add("MetaProdC5", Quantity.MetaProdC5);

							return d;
						}
					}
				}
			}
		}
	}
}