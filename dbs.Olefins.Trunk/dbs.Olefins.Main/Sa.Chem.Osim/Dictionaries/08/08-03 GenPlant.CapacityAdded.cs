﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class CapAdded
			{
				private static RangeReference Onsite = new RangeReference(Tabs.T08_03, 39, 8, SqlDbType.Float);
				private static RangeReference Exist = new RangeReference(Tabs.T08_03, 41, 8, SqlDbType.Float);

				private static RangeReference CapacityAdded_kMT = new RangeReference(Tabs.T08_03, 0, 8, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantExpansion]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "AccountID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("CapacityAdded_kMT", CapAdded.CapacityAdded_kMT);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Onsite", CapAdded.Onsite);
							d.Add("Exist", CapAdded.Exist);

							return d;
						}
					}
				}
			}
		}
	}
}