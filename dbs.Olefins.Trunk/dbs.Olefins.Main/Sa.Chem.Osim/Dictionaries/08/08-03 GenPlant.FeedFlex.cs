﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class FeedFlex
			{
				private static RangeReference Ethane = new RangeReference(Tabs.T08_03, 21, 0, SqlDbType.Float, 100.0);
				private static RangeReference Propane = new RangeReference(Tabs.T08_03, 22, 0, SqlDbType.Float, 100.0);
				private static RangeReference Butane = new RangeReference(Tabs.T08_03, 23, 0, SqlDbType.Float, 100.0);
				private static RangeReference LiqLight = new RangeReference(Tabs.T08_03, 24, 0, SqlDbType.Float, 100.0);
				private static RangeReference Diesel = new RangeReference(Tabs.T08_03, 25, 0, SqlDbType.Float, 100.0);
				private static RangeReference GasOilHv = new RangeReference(Tabs.T08_03, 26, 0, SqlDbType.Float, 100.0);

				private static RangeReference Capability_Pcnt = new RangeReference(Tabs.T08_03, 0, 7, SqlDbType.Float, 100.0);
				private static RangeReference Demonstrate_Pcnt = new RangeReference(Tabs.T08_03, 0, 8, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantFeedFlex]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Capability_Pcnt", FeedFlex.Capability_Pcnt);
							d.Add("Demonstrate_Pcnt", FeedFlex.Demonstrate_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Ethane", FeedFlex.Ethane);
							d.Add("Propane", FeedFlex.Propane);
							d.Add("Butane", FeedFlex.Butane);
							d.Add("LiqLight", FeedFlex.LiqLight);
							d.Add("Diesel", FeedFlex.Diesel);
							d.Add("GasOilHv", FeedFlex.GasOilHv);

							return d;
						}
					}
				}
			}
		}
	}
}