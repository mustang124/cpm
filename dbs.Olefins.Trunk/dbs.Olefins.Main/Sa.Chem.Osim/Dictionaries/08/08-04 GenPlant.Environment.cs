﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class Environment
			{
				private static RangeReference Emissions_MTd = new RangeReference(Tabs.T08_04, 19, 5, SqlDbType.Float);
				private static RangeReference WasteHazMat_MTd = new RangeReference(Tabs.T08_04, 24, 5, SqlDbType.Float);
				private static RangeReference NOx_LbMBtu = new RangeReference(Tabs.T08_04, 28, 5, SqlDbType.Float);
				private static RangeReference WasteWater_MTd = new RangeReference(Tabs.T08_04, 32, 5, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantEnvironment]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Emissions_MTd", Environment.Emissions_MTd);
							d.Add("WasteHazMat_MTd", Environment.WasteHazMat_MTd);
							d.Add("NOx_LbMBtu", Environment.NOx_LbMBtu);
							d.Add("WasteWater_MTd", Environment.WasteWater_MTd);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}