﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class Logistics
			{
				private static RangeReference FreshPyroFeed = new RangeReference(Tabs.T08_01, 29, 0);
				private static RangeReference Propylene = new RangeReference(Tabs.T08_01, 32, 0);
				private static RangeReference Ethylene = new RangeReference(Tabs.T08_01, 35, 0);

				private static RangeReference Amount_Cur = new RangeReference(Tabs.T08_01, 0, 8, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantLogistics]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Amount_Cur", Logistics.Amount_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("FreshPyroFeed", Logistics.FreshPyroFeed);
							d.Add("Propylene", Logistics.Propylene);
							d.Add("Ethylene", Logistics.Ethylene);

							return d;
						}
					}
				}
			}
		}
	}
}