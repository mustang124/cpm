﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class Furnace
			{
				private static RangeReference StreamId = new RangeReference(Tabs.T06_04, 6, 0, SqlDbType.VarChar);

				private static RangeReference MaintDecoke_Days = new RangeReference(Tabs.T06_04, 8, 0, SqlDbType.Float);
				private static RangeReference MaintTLE_Days = new RangeReference(Tabs.T06_04, 9, 0, SqlDbType.Float);
				private static RangeReference MaintMinor_Days = new RangeReference(Tabs.T06_04, 10, 0, SqlDbType.Float);
				private static RangeReference MaintMajor_Days = new RangeReference(Tabs.T06_04, 11, 0, SqlDbType.Float);
				private static RangeReference StandByDT_Days = new RangeReference(Tabs.T06_04, 13, 0, SqlDbType.Float);

				private static RangeReference FeedQty_kMT = new RangeReference(Tabs.T06_04, 24, 0, SqlDbType.Float);
				private static RangeReference FeedCap_MTd = new RangeReference(Tabs.T06_04, 25, 0, SqlDbType.Float);

				private static RangeReference FuelCons_kMT = new RangeReference(Tabs.T06_04, 32, 0, SqlDbType.Float);
				private static RangeReference FuelTypeID = new RangeReference(Tabs.T06_04, 33, 0, SqlDbType.Float);
				private static RangeReference StackOxygen_Pcnt = new RangeReference(Tabs.T06_04, 34, 0, SqlDbType.Float, 100.0);
				private static RangeReference ArchDraft_H20 = new RangeReference(Tabs.T06_04, 35, 0, SqlDbType.Float);
				private static RangeReference StackTemp_C = new RangeReference(Tabs.T06_04, 36, 0, SqlDbType.Float);
				private static RangeReference CrossTemp_C = new RangeReference(Tabs.T06_04, 37, 0, SqlDbType.Float);

				private static RangeReference Retubed_Year = new RangeReference(Tabs.T06_04, 39, 0, SqlDbType.Int);
				private static RangeReference Retubed_Pcnt = new RangeReference(Tabs.T06_04, 40, 0, SqlDbType.Float, 100.0);
				private static RangeReference RetubeInterval_Mnths = new RangeReference(Tabs.T06_04, 41, 0, SqlDbType.Float);

				private static RangeReference MaintRetubeMatl_Cur = new RangeReference(Tabs.T06_04, 44, 0, SqlDbType.Float);
				private static RangeReference MaintRetubeLabor_Cur = new RangeReference(Tabs.T06_04, 45, 0, SqlDbType.Float);
				private static RangeReference MaintMajor_Cur = new RangeReference(Tabs.T06_04, 46, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityPyroFurn]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "FFurnID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("StreamId", Furnace.StreamId);

							d.Add("MaintDecoke_Days", Furnace.MaintDecoke_Days);
							d.Add("MaintTLE_Days", Furnace.MaintTLE_Days);
							d.Add("MaintMinor_Days", Furnace.MaintMinor_Days);
							d.Add("MaintMajor_Days", Furnace.MaintMajor_Days);
							d.Add("StandByDT_Days", Furnace.StandByDT_Days);

							d.Add("FeedQty_kMT", Furnace.FeedQty_kMT);
							d.Add("FeedCap_MTd", Furnace.FeedCap_MTd);

							d.Add("FuelTypeID", Furnace.FuelTypeID);
							d.Add("FuelCons_kMT", Furnace.FuelCons_kMT);
							d.Add("StackOxygen_Pcnt", Furnace.StackOxygen_Pcnt);
							d.Add("ArchDraft_H20", Furnace.ArchDraft_H20);
							d.Add("StackTemp_C", Furnace.StackTemp_C);
							d.Add("CrossTemp_C", Furnace.CrossTemp_C);

							d.Add("Retubed_Year", Furnace.Retubed_Year);
							d.Add("Retubed_Pcnt", Furnace.Retubed_Pcnt);
							d.Add("RetubeInterval_Mnths", Furnace.RetubeInterval_Mnths);

							d.Add("MaintRetubeMatl_Cur", Furnace.MaintRetubeMatl_Cur);
							d.Add("MaintRetubeLabor_Cur", Furnace.MaintRetubeLabor_Cur);
							d.Add("MaintMajor_Cur", Furnace.MaintMajor_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							const int offset = 2;

							for (int i = 1; i <= 32; i++)
							{
								d.Add("F" + i.ToString(), new RangeReference(Tabs.T06_04, 0, i + offset));
							}

							return d;
						}
					}
				}
			}
		}
	}
}