﻿CREATE FUNCTION [Ranking].[GetBreaks](@ListId varchar(42) = NULL, @OnlyActiveBreaks Bit = 1)
RETURNS @Breaks TABLE (RankBreak varchar(128) NOT NULL)
AS
BEGIN
	IF @ListId IS NULL OR NOT EXISTS (SELECT * FROM Ranking.RankSpecsLu WHERE ListId = @ListId)
		INSERT @Breaks (RankBreak)
		SELECT RankBreak FROM Ranking.RankBreaks WHERE Active = 'Y' OR @OnlyActiveBreaks = 1
	ELSE
		INSERT @Breaks (RankBreak)
		SELECT DISTINCT RankBreak FROM Ranking.RankSpecsLu WHERE ListId = @ListId
		
	RETURN
END
