﻿namespace Sa.Chem.Simulation.Ux
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.runOneFile = new System.Windows.Forms.Button();
			this.processParallel = new System.Windows.Forms.Button();
			this.runLoop = new System.Windows.Forms.Button();
			this.backGround = new System.Windows.Forms.Button();
			this.listBoxAdd = new System.Windows.Forms.ListBox();
			this.listBoxProcessing = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// runOneFile
			// 
			this.runOneFile.Location = new System.Drawing.Point(12, 108);
			this.runOneFile.Name = "runOneFile";
			this.runOneFile.Size = new System.Drawing.Size(170, 42);
			this.runOneFile.TabIndex = 0;
			this.runOneFile.Text = "Run one file";
			this.runOneFile.UseVisualStyleBackColor = true;
			this.runOneFile.Click += new System.EventHandler(this.runOnefile_Click);
			// 
			// processParallel
			// 
			this.processParallel.Location = new System.Drawing.Point(188, 12);
			this.processParallel.Name = "processParallel";
			this.processParallel.Size = new System.Drawing.Size(170, 42);
			this.processParallel.TabIndex = 2;
			this.processParallel.Text = "Process Parallel";
			this.processParallel.UseVisualStyleBackColor = true;
			this.processParallel.Click += new System.EventHandler(this.processParallel_Click);
			// 
			// runLoop
			// 
			this.runLoop.Location = new System.Drawing.Point(12, 12);
			this.runLoop.Name = "runLoop";
			this.runLoop.Size = new System.Drawing.Size(170, 42);
			this.runLoop.TabIndex = 0;
			this.runLoop.Text = "Run Loop";
			this.runLoop.UseVisualStyleBackColor = true;
			this.runLoop.Click += new System.EventHandler(this.runLoop_Click);
			// 
			// backGround
			// 
			this.backGround.Location = new System.Drawing.Point(13, 61);
			this.backGround.Name = "backGround";
			this.backGround.Size = new System.Drawing.Size(169, 41);
			this.backGround.TabIndex = 3;
			this.backGround.Text = "Back Ground Workers";
			this.backGround.UseVisualStyleBackColor = true;
			this.backGround.Click += new System.EventHandler(this.backGround_Click);
			// 
			// listBoxAdd
			// 
			this.listBoxAdd.FormattingEnabled = true;
			this.listBoxAdd.Location = new System.Drawing.Point(188, 61);
			this.listBoxAdd.Name = "listBoxAdd";
			this.listBoxAdd.Size = new System.Drawing.Size(170, 342);
			this.listBoxAdd.TabIndex = 4;
			// 
			// listBoxProcessing
			// 
			this.listBoxProcessing.FormattingEnabled = true;
			this.listBoxProcessing.Location = new System.Drawing.Point(364, 61);
			this.listBoxProcessing.Name = "listBoxProcessing";
			this.listBoxProcessing.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.listBoxProcessing.Size = new System.Drawing.Size(170, 342);
			this.listBoxProcessing.Sorted = true;
			this.listBoxProcessing.TabIndex = 4;
			this.listBoxProcessing.TabStop = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(365, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 16);
			this.label1.TabIndex = 5;
			this.label1.Text = "label1";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(12, 380);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 6;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(588, 454);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.listBoxProcessing);
			this.Controls.Add(this.listBoxAdd);
			this.Controls.Add(this.backGround);
			this.Controls.Add(this.processParallel);
			this.Controls.Add(this.runLoop);
			this.Controls.Add(this.runOneFile);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button runOneFile;
		private System.Windows.Forms.Button processParallel;
		private System.Windows.Forms.Button runLoop;
		private System.Windows.Forms.Button backGround;
		private System.Windows.Forms.ListBox listBoxAdd;
		private System.Windows.Forms.ListBox listBoxProcessing;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
	}
}

