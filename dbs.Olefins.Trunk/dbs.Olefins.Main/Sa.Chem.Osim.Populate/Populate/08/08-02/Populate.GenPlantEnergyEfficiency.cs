﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void GenPlantEnergyEfficiency(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			const int c = 8;

			Dictionary<string, int> genEnergy = GenPlantEnergyEffRowDictionary();
			Dictionary<string, int> genEnergyPcnt = GenPlantEnergyPcntRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "8-2"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_GenPlantEnergyEfficiency]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								foreach (KeyValuePair<string, int> entry in genEnergy)
								{
									r = entry.Value;
									AddRangeValues(wks, r, c, rdr, entry.Key);
								}

								foreach (KeyValuePair<string, int> entry in genEnergyPcnt)
								{
									r = entry.Value;
									AddRangeValues(wks, r, c, rdr, entry.Key, 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "GenPlantEnergyEfficiency", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_GenPlantEnergyEfficiency]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> GenPlantEnergyEffRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("PyroFurnInletTemp_C", 27);
			d.Add("PyroFurnTLESteam_PSIa", 34);
			d.Add("DriverGT_YN", 38);
			d.Add("PyroFurnFlueGasTemp_C", 39);
			d.Add("TLEOutletTemp_C", 43);

			return d;
		}

		private Dictionary<string, int> GenPlantEnergyPcntRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("PyroFurnPreheat_Pcnt", 29);
			d.Add("PyroFurnPrimaryTLE_Pcnt", 32);
			d.Add("PyroFurnSecondaryTLE_Pcnt", 37);
			d.Add("PyroFurnFlueGasO2_Pcnt", 40);
			d.Add("EnergyDeterioration_Pcnt", 47);
			d.Add("IBSLSteamRequirement_Pcnt", 52);

			return d;
		}
	}
}