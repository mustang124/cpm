﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void Streams(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;
			int x = 0;
			int y = 0;

			string streamId;
			string streamDescription;
			string wksName;
			string componentId;

			Dictionary<string, string> wksStream = StreamWorksheet();
			Dictionary<string, int> colStream = StreamColDictionary();
			Dictionary<string, int> rowStream = StreamRowDictionary();
			Dictionary<string, string> streamComp = ProductionComponentDictionary();
			Dictionary<string, int> otherProdComp = ComponentDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_Streams]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));
								streamDescription = rdr.GetString(rdr.GetOrdinal("StreamDescription"));

								if (wksStream.TryGetValue(streamId, out wksName))
								{
									wks = wkb.Worksheets[tabNamePrefix + wksName];

									switch (wksName)
									{
										#region Light and Recycle

										case T02_A1:
										case T02_B:
											if (colStream.TryGetValue(streamId, out c))
											{
												#region Composition

												AddRangeValues(wks, 6, c, rdr, "CH4", 100.0);
												AddRangeValues(wks, 7, c, rdr, "C2H6", 100.0);
												AddRangeValues(wks, 8, c, rdr, "C2H4", 100.0);
												AddRangeValues(wks, 9, c, rdr, "C3H8", 100.0);
												AddRangeValues(wks, 10, c, rdr, "C3H6", 100.0);

												AddRangeValues(wks, 11, c, rdr, "NBUTA", 100.0);
												AddRangeValues(wks, 12, c, rdr, "IBUTA", 100.0);

												AddRangeValues(wks, 13, c, rdr, "IB", 100.0);
												AddRangeValues(wks, 14, c, rdr, "B1", 100.0);

												AddRangeValues(wks, 15, c, rdr, "C4H6", 100.0);
												AddRangeValues(wks, 16, c, rdr, "NC5", 100.0);
												AddRangeValues(wks, 17, c, rdr, "IC5", 100.0);
												AddRangeValues(wks, 18, c, rdr, "NC6", 100.0);
												AddRangeValues(wks, 19, c, rdr, "C6ISO", 100.0);

												AddRangeValues(wks, 20, c, rdr, "C7H16", 100.0);
												AddRangeValues(wks, 21, c, rdr, "C8H18", 100.0);
												AddRangeValues(wks, 22, c, rdr, "CO_CO2", 100.0);
												AddRangeValues(wks, 23, c, rdr, "H2", 100.0);
												AddRangeValues(wks, 24, c, rdr, "S", 100.0);

												#endregion

												#region Indicators

												AddRangeValues(wks, 27, c, rdr, "CoilOutletPressure_Psia");
												AddRangeValues(wks, 28, c, rdr, "SteamHydrocarbon_Ratio");
												AddRangeValues(wks, 29, c, rdr, "CoilOutletTemp_C");

												AddRangeValues(wks, 31, c, rdr, "EthyleneYield_WtPcnt", 100.0);
												AddRangeValues(wks, 32, c, rdr, "PropyleneEthylene_Ratio");
												AddRangeValues(wks, 33, c, rdr, "FeedConv_WtPcnt", 100.0);
												AddRangeValues(wks, 34, c, rdr, "PropyleneMethane_Ratio");

												#endregion

												#region Quantity

												AddRangeValues(wks, 36, c, rdr, "Q1_kMT");
												AddRangeValues(wks, 37, c, rdr, "Q2_kMT");
												AddRangeValues(wks, 38, c, rdr, "Q3_kMT");
												AddRangeValues(wks, 39, c, rdr, "Q4_kMT");

												#endregion

												AddRangeValues(wks, 43, c, rdr, "FurnConstruction_Year");
												AddRangeValues(wks, 45, c, rdr, "ResidenceTime_s");

												if (streamId == "FeedLtOther")
												{
													AddRangeValues(wks, 4, c, rdr, "StreamDescription");
													AddRangeValues(wks, 46, c, rdr, "Amount_Cur");
												}
											}
											break;

										#endregion

										#region Liquid

										case T02_A2:
											if (colStream.TryGetValue(streamId, out c))
											{
												//if (StreamDescription.Contains("(Other Feed 1)")) c = c + 0;
												if (streamDescription.Contains("(Other Feed 2)")) c = c + 1;
												if (streamDescription.Contains("(Other Feed 3)")) c = c + 2;

												AddRangeValues(wks, 5, c, rdr, "Density_SG");
												AddRangeValues(wks, 6, c, rdr, "S_PPM");

												AddRangeValues(wks, 8, c, rdr, "DistMethodID");

												AddRangeValues(wks, 9, c, rdr, "D000");
												AddRangeValues(wks, 10, c, rdr, "D005");
												AddRangeValues(wks, 11, c, rdr, "D010");
												AddRangeValues(wks, 12, c, rdr, "D030");
												AddRangeValues(wks, 13, c, rdr, "D050");
												AddRangeValues(wks, 14, c, rdr, "D070");
												AddRangeValues(wks, 15, c, rdr, "D090");
												AddRangeValues(wks, 16, c, rdr, "D095");
												AddRangeValues(wks, 17, c, rdr, "D100");

												AddRangeValues(wks, 20, c, rdr, "P", 100.0);
												AddRangeValues(wks, 21, c, rdr, "I", 100.0);
												AddRangeValues(wks, 22, c, rdr, "N", 100.0);
												AddRangeValues(wks, 23, c, rdr, "O", 100.0);
												AddRangeValues(wks, 24, c, rdr, "A", 100.0);

												AddRangeValues(wks, 16, c, rdr, "H2", 100.0);

												#region Indicators

												AddRangeValues(wks, 28, c, rdr, "CoilOutletPressure_Psia");
												AddRangeValues(wks, 29, c, rdr, "SteamHydrocarbon_Ratio");
												AddRangeValues(wks, 30, c, rdr, "CoilOutletTemp_C");

												AddRangeValues(wks, 32, c, rdr, "EthyleneYield_WtPcnt", 100.0);
												AddRangeValues(wks, 33, c, rdr, "PropyleneEthylene_Ratio");
												AddRangeValues(wks, 34, c, rdr, "FeedConv_WtPcnt", 100.0);
												AddRangeValues(wks, 35, c, rdr, "PropyleneMethane_Ratio");

												#endregion

												#region Quantity

												AddRangeValues(wks, 37, c, rdr, "Q1_kMT");
												AddRangeValues(wks, 38, c, rdr, "Q2_kMT");
												AddRangeValues(wks, 39, c, rdr, "Q3_kMT");
												AddRangeValues(wks, 40, c, rdr, "Q4_kMT");

												#endregion

												AddRangeValues(wks, 44, c, rdr, "FurnConstruction_Year");
												AddRangeValues(wks, 46, c, rdr, "ResidenceTime_s");

												if (streamId == "FeedLiqOther")
												{
													AddRangeValues(wks, 4, c, rdr, "StreamDescription");
													AddRangeValues(wks, 47, c, rdr, "Amount_Cur");
												}
											}
											break;

										#endregion

										#region Supplemental

										case T02_C:
										case T03_01:
											if (rowStream.TryGetValue(streamId, out r))
											{
												//if (StreamDescription.Contains("(Other Prod 1)")) r = r + 0;
												if (streamDescription.Contains("(Other Prod 2)")) r = r + 1;

												#region Quantity

												AddRangeValues(wks, r, 3, rdr, "Q1_kMT");
												AddRangeValues(wks, r, 4, rdr, "Q2_kMT");
												AddRangeValues(wks, r, 5, rdr, "Q3_kMT");
												AddRangeValues(wks, r, 6, rdr, "Q4_kMT");

												#endregion

												if (wksName == T02_C) AddRangeValues(wks, r, 8, rdr, "Recovered_WtPcnt", 100.0);

												if (streamComp.TryGetValue(streamId, out componentId))
												{
													AddRangeValues(wks, r, 8, rdr, componentId, 100.0);
												}

												switch (streamId)
												{
													case "Hydrogen":
														AddRangeValues(wks, r, 8, rdr, "H2Mol_Pcnt", 100.0);
														AddRangeValues(wks, 10, 8, rdr, "Amount_Cur");
														break;

													case "Methane":
														AddRangeValues(wks, r, 8, rdr, "CH4Mol_Pcnt", 100.0);
														break;

													case "PPFC":
														AddRangeValues(wks, 34, 8, rdr, "H2", 100.0);
														AddRangeValues(wks, 35, 8, rdr, "CH4", 100.0);
														AddRangeValues(wks, 36, 8, rdr, "C2H4", 100.0);
														AddRangeValues(wks, 37, 8, rdr, "Other", 100.0);
														break;

													case "SuppOther":
														AddRangeValues(wks, r, 2, rdr, "StreamDescription");

														AddRangeValues(wks, 75, 3, rdr, "Amount_Cur");
														
														foreach (KeyValuePair<string, int> entry in otherProdComp)
														{
															y = entry.Value + 58;
															AddRangeValues(wks, y, 3, rdr, entry.Key, 100.0);
														}
														break;

													case "ProdOther":

														AddRangeValues(wks, r, 2, rdr, "StreamDescription");
														AddRangeValues(wks, r, 8, rdr, "Amount_Cur");

														foreach (KeyValuePair<string, int> entry in otherProdComp)
														{
															if (streamDescription.Contains("(Other Prod 1)")) x = 3;
															if (streamDescription.Contains("(Other Prod 2)")) x = 5;

															y = entry.Value + 54;
															AddRangeValues(wks, y, x, rdr, entry.Key, 100.0);
														}
														break;
												}
											}
											break;
									
										#endregion
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "Streams", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_Streams]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, string> StreamWorksheet()
		{
			Dictionary<string, string> d = new Dictionary<string, string>();

			//	Light
			d.Add("Ethane", T02_A1);
			d.Add("EPMix", T02_A1);
			d.Add("Propane", T02_A1);
			d.Add("LPG", T02_A1);
			d.Add("Butane", T02_A1);
			d.Add("FeedLtOther", T02_A1);

			//	Liqud
			d.Add("NaphthaLt", T02_A2);
			d.Add("NaphthaFr", T02_A2);
			d.Add("NaphthaHv", T02_A2);
			d.Add("Raffinate", T02_A2);
			d.Add("HeavyNGL", T02_A2);
			d.Add("Condensate", T02_A2);
			d.Add("Diesel", T02_A2);
			d.Add("GasOilHv", T02_A2);
			d.Add("GasOilHt", T02_A2);
			d.Add("FeedLiqOther", T02_A2);

			//	Recycle
			d.Add("EthRec", T02_B);
			d.Add("ProRec", T02_B);
			d.Add("ButRec", T02_B);

			//	Supplemental
			d.Add("ConcEthylene", T02_C);
			d.Add("ConcPropylene", T02_C);
			d.Add("ConcButadiene", T02_C);
			d.Add("ConcBenzene", T02_C);

			d.Add("ROGHydrogen", T02_C);
			d.Add("ROGMethane", T02_C);
			d.Add("ROGEthane", T02_C);
			d.Add("ROGEthylene", T02_C);
			d.Add("ROGPropane", T02_C);
			d.Add("ROGPropylene", T02_C);
			d.Add("ROGC4Plus", T02_C);
			d.Add("ROGInerts", T02_C);

			d.Add("DilHydrogen", T02_C);
			d.Add("DilMethane", T02_C);
			d.Add("DilEthane", T02_C);
			d.Add("DilEthylene", T02_C);
			d.Add("DilPropane", T02_C);
			d.Add("DilPropylene", T02_C);
			d.Add("DilButane", T02_C);
			d.Add("DilButylene", T02_C);
			d.Add("DilButadiene", T02_C);
			d.Add("DilBenzene", T02_C);
			d.Add("DilMoGas", T02_C);
			d.Add("DilMogas", T02_C);

			d.Add("SuppWashOil", T02_C);
			d.Add("SuppGasOil", T02_C);
			d.Add("SuppOther", T02_C);

			//	Production
			d.Add("Hydrogen", T03_01);
			d.Add("Methane", T03_01);
			d.Add("Acetylene", T03_01);
			d.Add("EthylenePG", T03_01);
			d.Add("EthyleneCG", T03_01);
			d.Add("PropylenePG", T03_01);
			d.Add("PropyleneCG", T03_01);
			d.Add("PropyleneRG", T03_01);
			d.Add("PropaneC3Resid", T03_01);

			d.Add("Butadiene", T03_01);
			d.Add("IsoButylene", T03_01);
			d.Add("Isobutylene", T03_01);
			d.Add("C4Oth", T03_01);

			d.Add("Benzene", T03_01);
			d.Add("PyroGasoline", T03_01);

			d.Add("PyroGasOil", T03_01);
			d.Add("PyroFuelOil", T03_01);
			d.Add("AcidGas", T03_01);

			d.Add("PPFC", T03_01);

			d.Add("ProdOther", T03_01);

			d.Add("LossFlareVent", T03_01);
			d.Add("LossOther", T03_01);
			d.Add("LossMeasure", T03_01);

			return d;
		}

		private Dictionary<string, int> StreamColDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Ethane", 3);
			d.Add("EPMix", 4);
			d.Add("Propane", 5);
			d.Add("LPG", 6);
			d.Add("Butane", 7);
			d.Add("FeedLtOther", 8);

			d.Add("NaphthaLt", 3);
			d.Add("NaphthaFr", 4);
			d.Add("NaphthaHv", 5);
			d.Add("Raffinate", 6);
			d.Add("HeavyNGL", 7);
			d.Add("Condensate", 8);

			d.Add("Diesel", 9);
			d.Add("GasOilHv", 10);
			d.Add("GasOilHt", 11);

			d.Add("FeedLiqOther", 12);

			d.Add("EthRec", 3);
			d.Add("ProRec", 4);
			d.Add("ButRec", 5);

			return d;
		}

		private Dictionary<string, int> StreamRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			//	Supplemental
			d.Add("ConcEthylene", 10);
			d.Add("ConcPropylene", 11);
			d.Add("ConcButadiene", 12);
			d.Add("ConcBenzene", 13);

			d.Add("ROGHydrogen", 15);
			d.Add("ROGMethane", 16);
			d.Add("ROGEthane", 17);
			d.Add("ROGEthylene", 18);
			d.Add("ROGPropane", 19);
			d.Add("ROGPropylene", 20);
			d.Add("ROGC4Plus", 21);
			d.Add("ROGInerts", 22);

			d.Add("DilHydrogen", 25);
			d.Add("DilMethane", 26);
			d.Add("DilEthane", 27);
			d.Add("DilEthylene", 28);
			d.Add("DilPropane", 29);
			d.Add("DilPropylene", 30);
			d.Add("DilButane", 31);
			d.Add("DilButylene", 32);
			d.Add("DilButadiene", 33);
			d.Add("DilBenzene", 34);
			d.Add("DilMoGas", 35);
			d.Add("DilMogas", 35);

			d.Add("SuppWashOil", 36);
			d.Add("SuppGasOil", 37);
			d.Add("SuppOther", 38);

			//	Production
			d.Add("Hydrogen", 9);
			d.Add("Methane", 11);
			d.Add("Acetylene", 12);
			d.Add("EthylenePG", 13);
			d.Add("EthyleneCG", 14);
			d.Add("PropylenePG", 15);
			d.Add("PropyleneCG", 16);
			d.Add("PropyleneRG", 17);
			d.Add("PropaneC3Resid", 18);

			d.Add("Butadiene", 21);
			d.Add("IsoButylene", 22);
			d.Add("Isobutylene", 22);
			d.Add("C4Oth", 23);

			d.Add("Benzene", 26);
			d.Add("PyroGasoline", 27);

			d.Add("PyroGasOil", 29);
			d.Add("PyroFuelOil", 30);
			d.Add("AcidGas", 31);

			d.Add("PPFC", 33);

			d.Add("ProdOther", 41);

			d.Add("LossFlareVent", 44);
			d.Add("LossOther", 45);
			d.Add("LossMeasure", 46);

			return d;
		}

		private Dictionary<string, string> ProductionComponentDictionary()
		{
			Dictionary<string, string> d = new Dictionary<string, string>();

			d.Add("EthylenePG", "C2H4");
			d.Add("EthyleneCG", "C2H4");
			d.Add("PropylenePG", "C3H6");
			d.Add("PropyleneCG", "C3H6");
			d.Add("PropyleneRG", "C3H6");
			d.Add("PropaneC3Resid", "C3H8");

			return d;
		}

		private Dictionary<string, int> ComponentDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("H2", 1);
			d.Add("CH4", 2);
			d.Add("C2H2", 3);
			d.Add("C2H4", 4);
			d.Add("C2H6", 5);
			d.Add("C3H6", 6);
			d.Add("C3H8", 7);
			d.Add("C4H6", 8);
			d.Add("C4H8", 9);
			d.Add("C4H10", 10);
			d.Add("C6H6", 11);
			d.Add("PyroGasoline", 12);
			d.Add("PyroFuelOil", 13);
			d.Add("Inerts", 14);

			return d;
		}
	}
}