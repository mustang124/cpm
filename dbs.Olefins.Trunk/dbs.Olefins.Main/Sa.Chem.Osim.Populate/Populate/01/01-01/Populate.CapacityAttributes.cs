﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		//	0				1			2				3				4				5				6				7
		//Refnum		CalDateKey	Expansion_Pcnt	Expansion_Date	CapAllow_Bit	CapAllow_YN		CapLoss_Pcnt	StartUp_Year
		//2013PCH011	20131231	0				NULL			0				No				2				1979	
	
		public void CapacityAttributes(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 9;
			
			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "1-1"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_CapacityAttributes]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								AddRangeValues(wks, 19, c, rdr, "Expansion_Pcnt", 100.0);
								AddRangeValues(wks, 20, c, rdr, "Expansion_Date");
								AddRangeValues(wks, 23, c, rdr, "CapAllow_YN");
								AddRangeValues(wks, 27, c, rdr, "CapLoss_Pcnt", 100.0);
								AddRangeValues(wks, 29, c, rdr, "StartUp_Year");
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "CapacityAttributes", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_CapacityAttributes]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}