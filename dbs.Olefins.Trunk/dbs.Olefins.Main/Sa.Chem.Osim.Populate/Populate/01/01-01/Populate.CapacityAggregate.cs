﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{

		//	0				1			2				3				4					5					6			7					8
		//FactorSetId	Refnum		CalDateKey		StreamId		Capacity_kMT		Capacity_MTd		Stream_MTd	Record_MTd	CapacityRecord_Pcnt
		//2013			2013PCH011	20131231		Ethylene		-1258.30004882813	-3447.3974609375	-3483		-3625		104.076945162216
		//2013			2013PCH011	20131231		FreshPyroFeed	3100				8493.150390625		NULL		NULL		NULL
		//2013			2013PCH011	20131231		ProdOlefins		-1718.30004882813	-4707.67138671875	-4743		-4825		101.728863588446
		//2013			2013PCH011	20131231		Propylene		-460				-1260.27392578125	-1260		-1200		95.2380952380952
		//2013			2013PCH011	20131231		Recycle			410					1123.28771972656	NULL		NULL		NULL

		public void CapacityAggregate(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			double capacity_kMT;
			double capacity_MTd;
			double? stream_MTd;
			double? record_MTd;
			double? capacityRecord_Pcnt;

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "1-1"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_CapacityAggregate]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;
						cmd.Parameters.Add("@FactorSetId", SqlDbType.VarChar, 12).Value = factorSetId;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								capacity_kMT = double.NaN;
								capacity_MTd = double.NaN;
								stream_MTd = double.NaN;
								record_MTd = double.NaN;
								capacityRecord_Pcnt = double.NaN;

								if (!rdr.IsDBNull(4)) { capacity_kMT = Common.ReturnDouble(rdr, 4, 2); }
								if (!rdr.IsDBNull(5)) { capacity_MTd = Common.ReturnDouble(rdr, 5, 2); }
								if (!rdr.IsDBNull(6)) { stream_MTd = Common.ReturnDouble(rdr, 6, 2); }
								if (!rdr.IsDBNull(7)) { record_MTd = Common.ReturnDouble(rdr, 7, 2); }
								if (!rdr.IsDBNull(8)) { capacityRecord_Pcnt = Common.ReturnDouble(rdr, 8, 2); }

								switch ((string)rdr.GetSqlString(3))
								{
									case "Ethylene":
										r = 10;
										c = 6;
										rng = wks.Cells[r, c];
										rng.Value2 = capacity_kMT;

										c = 8;
										rng = wks.Cells[r, c];
										rng.Value2 = stream_MTd;

										c = 9;
										rng = wks.Cells[r, c];
										rng.Value2 = record_MTd;

										break;

									case "Propylene":
										r = 11;
										c = 6;
										rng = wks.Cells[r, c];
										rng.Value2 = capacity_kMT;
										break;

									case "ProdOlefins":
										r = 12;

										c = 8;
										rng = wks.Cells[r, c];
										rng.Value2 = stream_MTd;

										c = 9;
										rng = wks.Cells[r, c];
										rng.Value2 = record_MTd;

										break;

									case "FreshPyroFeed":
										r = 13;
										c = 6;
										rng = wks.Cells[r, c];
										rng.Value2 = capacity_kMT;
										break;

									case "Recycle":
										r = 14;
										c = 6;
										rng = wks.Cells[r, c];
										rng.Value2 = capacity_kMT;
										break;

									default:
										break;
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "CapacityAggregate", refnum, wkb, wks, rng, r, c, "[fact].[Select_CapacityAggregate]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}