﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityPyroFurnRunLength(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string streamId;

			Dictionary<string, int> feedCol = PyroFurnFeedColDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-3"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityPyroFurn_RunLength]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));

								if (feedCol.TryGetValue(streamId, out c))
								{
									r = 29;
									AddRangeValues(wks, r, c, rdr, "RunLength_Days");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityPyroFurnRunLength", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityPyroFurn_RunLength]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> PyroFurnFeedColDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Ethane", 6);
			d.Add("Propane", 7);
			d.Add("Naphtha", 8);
			d.Add("LiqHeavy", 9);

			return d;
		}
	}
}