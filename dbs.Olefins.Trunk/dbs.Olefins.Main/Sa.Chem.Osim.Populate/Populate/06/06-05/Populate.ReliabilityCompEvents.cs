﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;


		//[r].[Refnum],
		//[r].[CalDateKey],
		//[r].[EventNo],
		//[r].[CompressorID],
		//	[WksCompressorID]		= [f].[Compressor],
		//[r].[ServiceLevelID],
		//[r].[CompComponentID],
		//	[WksCompComponentID]	= [x].[Component],
		//[r].[EventCauseID],
		//	[WksEventCauseID]		= [y].[Cause],
		//[r].[EventOccur_Year],
		//[r].[Duration_Hours],
		//[c].[Comments]


namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityCompEvents(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			Dictionary<string, int> eventCol = EventColDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-5"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityCompEvents]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								r = (int)rdr.GetByte(rdr.GetOrdinal("EventNo")) + 28;

								foreach (KeyValuePair<string, int> entry in eventCol)
								{
									c = entry.Value;
									AddRangeValues(wks, r, c, rdr, entry.Key);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityAttributes", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityAttributes]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> EventColDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("WksCompressorID", 2);
			d.Add("ServiceLevelID", 3);
			d.Add("Duration_Hours", 4);
			d.Add("WksCompComponentID", 5);
			d.Add("WksEventCauseID", 6);
			d.Add("EventOccur_Year", 7);
			d.Add("Comments", 8);

			return d;
		}
	}
}