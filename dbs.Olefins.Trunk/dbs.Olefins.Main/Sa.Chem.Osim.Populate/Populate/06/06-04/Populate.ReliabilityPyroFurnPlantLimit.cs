﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityPyroFurnRunPlantLimit(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			const int r = 48;
			const int c = 3;

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-4"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityPyroFurnPlantLimit]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								AddRangeValues(wks, r, c, rdr, "PlantFurnLimit_Pcnt", 100.0);
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityPyroFurnRunLength", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityPyroFurn_RunLength]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}