﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using Microsoft.CSharp;

using Excel = Microsoft.Office.Interop.Excel;

using Sa.Chem.Osim;

//	http://richnewman.wordpress.com/2007/04/15/a-beginner%E2%80%99s-guide-to-calling-a-net-library-from-excel/

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
public class ChemPopulate
{
	public void OsimPopulate(string CurrentRefnum, string HistoryRefnum, string pathOsimTemplate)
	{
		Excel.Application xla = Common.NewExcelApplication(true);
		Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, pathOsimTemplate);

		OsimPopulate(wkb, HistoryRefnum, false);
		OsimPopulate(wkb, CurrentRefnum, true);

		//wkb.SaveAs(path + "OSIM" + Refnum + " " + Common.GetDateTimeStamp() + ".xlsm");
		//Common.CloseExcel(ref xla, ref wkb);
	}

	public void OsimPopulate(string Refnum, bool IsCurrent, string pathOsimTemplate)
	{
		Excel.Application xla = Common.NewExcelApplication(false);
		Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, pathOsimTemplate);

		OsimPopulate(wkb, Refnum, IsCurrent);

		string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Temp\\";

		wkb.SaveAs(path + "OSIM" + Refnum + " " + Common.GetDateTimeStamp() + ".xlsm");

		Common.CloseExcel(ref xla, ref wkb);
	}

	private void OsimPopulate(Excel.Workbook wkb, string Refnum, bool IsCurrent)
	{
		const string History = "History";
		const string Current = "Table";

		string tabPrefix = ((IsCurrent) ? Current : History) + " ";

		string factorSetId = Common.GetFactorSet(Refnum);
		
		Populate p = new Populate();
		p.InputForm(wkb, tabPrefix, Refnum, factorSetId);
		p = null;
	}
}