<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ClickOnceUpdate
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblDownLoadSize = New System.Windows.Forms.Label()
        Me.btnUpdateRestart = New System.Windows.Forms.Button()
        Me.lblVersionAvailable = New System.Windows.Forms.Label()
        Me.pBarUpdate = New System.Windows.Forms.ProgressBar()
        Me.lblVersionInstalled = New System.Windows.Forms.Label()
        Me.btnCancelClose = New System.Windows.Forms.Button()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.lblUpdate = New System.Windows.Forms.Label()
        Me.txtVersionInstalled = New System.Windows.Forms.TextBox()
        Me.txtVersionAvailable = New System.Windows.Forms.TextBox()
        Me.txtDownloadSize = New System.Windows.Forms.TextBox()
        Me.lblLocation = New System.Windows.Forms.Label()
        Me.txtLocation = New System.Windows.Forms.TextBox()
        Me.lblCopyright = New System.Windows.Forms.Label()
        Me.lblTrademark = New System.Windows.Forms.Label()
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(6, 102)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(42, 13)
        Me.lblStatus.TabIndex = 10
        Me.lblStatus.Text = "Status:"
        '
        'lblDownLoadSize
        '
        Me.lblDownLoadSize.AutoSize = True
        Me.lblDownLoadSize.Location = New System.Drawing.Point(6, 72)
        Me.lblDownLoadSize.Name = "lblDownLoadSize"
        Me.lblDownLoadSize.Size = New System.Drawing.Size(80, 13)
        Me.lblDownLoadSize.TabIndex = 9
        Me.lblDownLoadSize.Text = "Download Size:"
        '
        'btnUpdateRestart
        '
        Me.btnUpdateRestart.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateRestart.BackColor = System.Drawing.Color.Transparent
        Me.btnUpdateRestart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpdateRestart.Location = New System.Drawing.Point(6, 292)
        Me.btnUpdateRestart.Margin = New System.Windows.Forms.Padding(9)
        Me.btnUpdateRestart.Name = "btnUpdateRestart"
        Me.btnUpdateRestart.Size = New System.Drawing.Size(95, 25)
        Me.btnUpdateRestart.TabIndex = 5
        Me.btnUpdateRestart.Text = "Update"
        Me.btnUpdateRestart.UseVisualStyleBackColor = False
        '
        'lblVersionAvailable
        '
        Me.lblVersionAvailable.AutoSize = True
        Me.lblVersionAvailable.Location = New System.Drawing.Point(6, 42)
        Me.lblVersionAvailable.Name = "lblVersionAvailable"
        Me.lblVersionAvailable.Size = New System.Drawing.Size(92, 13)
        Me.lblVersionAvailable.TabIndex = 8
        Me.lblVersionAvailable.Text = "Available Version:"
        '
        'pBarUpdate
        '
        Me.pBarUpdate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pBarUpdate.BackColor = System.Drawing.Color.White
        Me.pBarUpdate.ForeColor = System.Drawing.Color.Maroon
        Me.pBarUpdate.Location = New System.Drawing.Point(6, 252)
        Me.pBarUpdate.Margin = New System.Windows.Forms.Padding(9)
        Me.pBarUpdate.Name = "pBarUpdate"
        Me.pBarUpdate.Size = New System.Drawing.Size(199, 22)
        Me.pBarUpdate.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pBarUpdate.TabIndex = 15
        Me.pBarUpdate.Visible = False
        '
        'lblVersionInstalled
        '
        Me.lblVersionInstalled.AutoSize = True
        Me.lblVersionInstalled.Location = New System.Drawing.Point(6, 12)
        Me.lblVersionInstalled.Name = "lblVersionInstalled"
        Me.lblVersionInstalled.Size = New System.Drawing.Size(90, 13)
        Me.lblVersionInstalled.TabIndex = 7
        Me.lblVersionInstalled.Text = "Installed Version:"
        '
        'btnCancelClose
        '
        Me.btnCancelClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelClose.BackColor = System.Drawing.Color.Transparent
        Me.btnCancelClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelClose.Location = New System.Drawing.Point(110, 292)
        Me.btnCancelClose.Margin = New System.Windows.Forms.Padding(9)
        Me.btnCancelClose.Name = "btnCancelClose"
        Me.btnCancelClose.Size = New System.Drawing.Size(95, 25)
        Me.btnCancelClose.TabIndex = 6
        Me.btnCancelClose.Text = "Close"
        Me.btnCancelClose.UseVisualStyleBackColor = False
        '
        'txtStatus
        '
        Me.txtStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStatus.BackColor = System.Drawing.Color.White
        Me.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtStatus.Location = New System.Drawing.Point(105, 102)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(100, 14)
        Me.txtStatus.TabIndex = 3
        Me.txtStatus.TabStop = False
        '
        'lblUpdate
        '
        Me.lblUpdate.AutoSize = True
        Me.lblUpdate.Location = New System.Drawing.Point(6, 222)
        Me.lblUpdate.Name = "lblUpdate"
        Me.lblUpdate.Size = New System.Drawing.Size(76, 13)
        Me.lblUpdate.TabIndex = 14
        Me.lblUpdate.Text = "Update Status"
        Me.lblUpdate.Visible = False
        '
        'txtVersionInstalled
        '
        Me.txtVersionInstalled.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtVersionInstalled.BackColor = System.Drawing.Color.White
        Me.txtVersionInstalled.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersionInstalled.Location = New System.Drawing.Point(105, 12)
        Me.txtVersionInstalled.Name = "txtVersionInstalled"
        Me.txtVersionInstalled.ReadOnly = True
        Me.txtVersionInstalled.Size = New System.Drawing.Size(100, 14)
        Me.txtVersionInstalled.TabIndex = 0
        Me.txtVersionInstalled.TabStop = False
        '
        'txtVersionAvailable
        '
        Me.txtVersionAvailable.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtVersionAvailable.BackColor = System.Drawing.Color.White
        Me.txtVersionAvailable.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersionAvailable.Location = New System.Drawing.Point(105, 42)
        Me.txtVersionAvailable.Name = "txtVersionAvailable"
        Me.txtVersionAvailable.ReadOnly = True
        Me.txtVersionAvailable.Size = New System.Drawing.Size(100, 14)
        Me.txtVersionAvailable.TabIndex = 1
        Me.txtVersionAvailable.TabStop = False
        '
        'txtDownloadSize
        '
        Me.txtDownloadSize.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDownloadSize.BackColor = System.Drawing.Color.White
        Me.txtDownloadSize.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDownloadSize.Location = New System.Drawing.Point(105, 72)
        Me.txtDownloadSize.Name = "txtDownloadSize"
        Me.txtDownloadSize.ReadOnly = True
        Me.txtDownloadSize.Size = New System.Drawing.Size(100, 14)
        Me.txtDownloadSize.TabIndex = 2
        Me.txtDownloadSize.TabStop = False
        '
        'lblLocation
        '
        Me.lblLocation.AutoSize = True
        Me.lblLocation.Location = New System.Drawing.Point(6, 132)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(51, 13)
        Me.lblLocation.TabIndex = 11
        Me.lblLocation.Text = "Location:"
        '
        'txtLocation
        '
        Me.txtLocation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLocation.BackColor = System.Drawing.Color.White
        Me.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtLocation.Location = New System.Drawing.Point(105, 132)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.ReadOnly = True
        Me.txtLocation.Size = New System.Drawing.Size(100, 14)
        Me.txtLocation.TabIndex = 4
        Me.txtLocation.TabStop = False
        '
        'lblCopyright
        '
        Me.lblCopyright.AutoSize = True
        Me.lblCopyright.Location = New System.Drawing.Point(6, 162)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.Size = New System.Drawing.Size(54, 13)
        Me.lblCopyright.TabIndex = 12
        Me.lblCopyright.Text = "Copyright"
        '
        'lblTrademark
        '
        Me.lblTrademark.AutoSize = True
        Me.lblTrademark.Location = New System.Drawing.Point(6, 192)
        Me.lblTrademark.Name = "lblTrademark"
        Me.lblTrademark.Size = New System.Drawing.Size(58, 13)
        Me.lblTrademark.TabIndex = 13
        Me.lblTrademark.Text = "Trademark"
        '
        'ClickOnceUpdate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.btnUpdateRestart)
        Me.Controls.Add(Me.btnCancelClose)
        Me.Controls.Add(Me.lblTrademark)
        Me.Controls.Add(Me.lblCopyright)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.lblLocation)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.lblDownLoadSize)
        Me.Controls.Add(Me.lblVersionInstalled)
        Me.Controls.Add(Me.txtDownloadSize)
        Me.Controls.Add(Me.lblVersionAvailable)
        Me.Controls.Add(Me.txtVersionAvailable)
        Me.Controls.Add(Me.pBarUpdate)
        Me.Controls.Add(Me.txtVersionInstalled)
        Me.Controls.Add(Me.lblUpdate)
        Me.Controls.Add(Me.txtStatus)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimumSize = New System.Drawing.Size(214, 323)
        Me.Name = "ClickOnceUpdate"
        Me.Size = New System.Drawing.Size(214, 323)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents lblStatus As System.Windows.Forms.Label
    Private WithEvents lblDownLoadSize As System.Windows.Forms.Label
    Private WithEvents btnUpdateRestart As System.Windows.Forms.Button
    Private WithEvents lblVersionAvailable As System.Windows.Forms.Label
    Private WithEvents pBarUpdate As System.Windows.Forms.ProgressBar
    Private WithEvents lblVersionInstalled As System.Windows.Forms.Label
    Private WithEvents btnCancelClose As System.Windows.Forms.Button
    Private WithEvents txtStatus As System.Windows.Forms.TextBox
    Private WithEvents lblUpdate As System.Windows.Forms.Label
    Private WithEvents txtVersionInstalled As System.Windows.Forms.TextBox
    Private WithEvents txtVersionAvailable As System.Windows.Forms.TextBox
    Private WithEvents txtDownloadSize As System.Windows.Forms.TextBox
    Private WithEvents lblLocation As System.Windows.Forms.Label
    Private WithEvents txtLocation As System.Windows.Forms.TextBox
    Private WithEvents lblCopyright As System.Windows.Forms.Label
    Private WithEvents lblTrademark As System.Windows.Forms.Label
    Private WithEvents toolTip As System.Windows.Forms.ToolTip

End Class
