//------------------------------------------------------------------------------
// <copyright file="CSSqlFunction.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    /// <summary>
    /// Implements regular expresssions for SQL Server.
    /// </summary>
    /// <param name="Pattern">Is a literal string.The regular expression pattern to match. <c>Pattern</c> is of the character string data type NVARCHAR(4000).</param>
    /// <param name="SearchString">Is an expression, typically a column that is searched for the specified pattern. <c>SearchString</c> is of the character string data type NVARCHAR(MAX).</param>
    /// <returns>Bit (SqlBoolean, Boolean)</returns>
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlBoolean RegEx(SqlString Pattern, SqlChars SearchString)
    {
        SqlBoolean rtn = SqlBoolean.False;

        if ((Pattern.IsNull && SearchString.IsNull) == false)
        {
            string p = (String)Pattern;
            string e = (String)(SqlString)SearchString;

            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(p);

            if (r.IsMatch(e))
            {
                rtn = SqlBoolean.True;
            }
        }

        return rtn;
    }
}
