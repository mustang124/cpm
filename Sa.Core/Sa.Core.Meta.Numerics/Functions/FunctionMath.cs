﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meta.Numerics.Functions {

    /// <summary>
    /// Contains methods for finding roots and extrema.
    /// </summary>
    public static partial class FunctionMath {

        // logic for finding function points is in other classes

        private static readonly double RelativePrecision = Math.Pow(2.0, -48);

        private static readonly double AbsolutePrecision = Math.Pow(2.0, -192);

    }

}
