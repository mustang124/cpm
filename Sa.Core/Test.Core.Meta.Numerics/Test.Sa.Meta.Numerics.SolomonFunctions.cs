﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Meta.Numerics.Series;

namespace TestSolomon
{
    [TestClass]
    public class Series
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void PartialGeometricSeriesContains()
        {
            List<Object[]> l = new List<Object[]>();

            Int32 n;
            Int32 v;
            bool a;
            bool e;

            l.Add(new Object[3] { 0, 0, 1 });
            l.Add(new Object[3] { 0, 1, 0 });

            l.Add(new Object[3] { 1, 0, 0 });
            l.Add(new Object[3] { 1, 1, 1 });
            l.Add(new Object[3] { 1, 2, 0 });
            l.Add(new Object[3] { 1, 3, 1 });
            l.Add(new Object[3] { 1, 4, 0 });
            l.Add(new Object[3] { 1, 5, 1 });
            l.Add(new Object[3] { 1, 6, 0 });
            l.Add(new Object[3] { 1, 7, 1 });
            l.Add(new Object[3] { 1, 8, 0 });

            l.Add(new Object[3] { 2, 0, 0 });
            l.Add(new Object[3] { 2, 1, 0 });
            l.Add(new Object[3] { 2, 2, 1 });
            l.Add(new Object[3] { 2, 3, 1 });
            l.Add(new Object[3] { 2, 4, 0 });
            l.Add(new Object[3] { 2, 5, 0 });
            l.Add(new Object[3] { 2, 6, 1 });
            l.Add(new Object[3] { 2, 7, 1 });
            l.Add(new Object[3] { 2, 8, 0 });

            l.Add(new Object[3] { 4, 0, 0 });
            l.Add(new Object[3] { 4, 1, 0 });
            l.Add(new Object[3] { 4, 2, 0 });
            l.Add(new Object[3] { 4, 3, 0 });
            l.Add(new Object[3] { 4, 4, 1 });
            l.Add(new Object[3] { 4, 5, 1 });
            l.Add(new Object[3] { 4, 6, 1 });
            l.Add(new Object[3] { 4, 7, 1 });
            l.Add(new Object[3] { 4, 8, 0 });
            
            foreach (Object[] i in l)
            {
                n = Convert.ToInt32(i[0]);
                v = Convert.ToInt32(i[1]);
                e = Convert.ToBoolean(i[2]);

                a = PartialGeometric.Contains(n, v);

                Assert.AreEqual(e, a, "Meta.Numerics.Series.PartialGeometric.Contains failed for n:=" + n.ToString() + " v:=" + v.ToString());
                
            }
        }

        [TestMethod]
        public void PartialGeometricSeriesPrevious()
        {
            List<Object[]> l = new List<Object[]>();

            Int32 n;
            Int32 v;
            Int32 a;
            Int32 e;

            l.Add(new Object[3] { 2, 4, 0 });
            l.Add(new Object[3] { 4, 4, 4 });
            l.Add(new Object[3] { 2, 5, 1 });
            l.Add(new Object[3] { 8, 18, 2 });

            foreach (Object[] i in l)
            {
                n = Convert.ToInt32(i[0]);
                v = Convert.ToInt32(i[1]);
                e = Convert.ToInt32(i[2]);

                a = PartialGeometric.NextNumber(n, v, -1);

                Assert.AreEqual(e, a, "Meta.Numerics.Series.PartialGeometric.Previous failed for n:=" + n.ToString() + " v:=" + v.ToString());
            }
        }
    }
}
