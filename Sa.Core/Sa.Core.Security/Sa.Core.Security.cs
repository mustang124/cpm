﻿using System;
using System.Text;

namespace Sa.Core
{
    #region Abstractions and Interfaces

    /// <summary>
    /// Stream reader interface for ensuring concrete reader classes are properly formed.
    /// Used a parameter type in Sa.Security.HashService.
    /// </summary>
    public interface iStreamReader
    {
        /// <summary>
        /// Reads an object and translates it to a System.IO.Stream.
        /// </summary>
        /// <returns>System.IO.Stream</returns>
        System.IO.Stream Read();
    }

    /// <summary>
    /// Byte writer interface for ensuring concrete writer classes are properly formed.
    /// Used a parameter type in Sa.Security.HashService.
    /// </summary>
    public interface iByteWriter
    {
        /// <summary>
        /// Writes a byte array to an object.
        /// </summary>
        /// <param name="b">byte[]</param>
        void Store(byte[] b);
    }

    /// <summary>
    /// Abstract stream reader class for ensuring concrete reader classes are properly formed.
    /// </summary>
    public abstract class aStreamReader : Sa.Core.iStreamReader
    {

        /// <summary>
        /// Storage space for the string item
        /// </summary>
        private string item;

        /// <summary>
        /// Accesses item through a property rather than a string object.
        /// </summary>
        protected string Item
        {
            set
            {
                item = value;
            }
            get
            {
                return item;
            }
        }
            
        /// <summary>
        /// Read function that creates System.IO.Stream for later use.  This function (method) is
        /// called by the function to where this class is passed.
        /// </summary>
        /// <returns>System.IO.Stream</returns>
        /// <remarks>
        /// <code>
        /// HashService(Sa.Security.iStreamReader iReader)
        /// {
        ///     // ...
        ///     System.IO.Stream s = null;
        ///     s = iReader.Read();
        ///     // ...
        /// }
        /// </code>
        /// </remarks>
        public abstract System.IO.Stream Read();
    }

    /// <summary>
    /// Abstract stream writer class for ensuring concrete writer classes are properly formed.
    /// </summary>
    public abstract class aByteWriter : Sa.Core.iByteWriter
    {
        /// <summary>
        /// Storage space for the byte array item
        /// </summary>
        private byte[] item;

        /// <summary>
        /// Accesses item through a property rather than a string object.
        /// </summary>
        public byte[] Item
        {
            set
            {
                item = value;
            }
            get
            {
                return item;
            }

        }

        /// <summary>
        /// Stores the byte array for use by the Write function.  This function (method) is
        /// called by the function to where this class is passed.
        /// </summary>
        /// <param name="b">byte[]</param>
        /// <remarks>
        /// <code>
        /// HashService(Sa.Security.iByteWriter iWriter)
        /// {
        ///     // ...
        ///     iWriter.Store(b);
        ///     // ...
        /// }
        /// </code>
        /// </remarks>
        public abstract void Store(byte[] b);

        /// <summary>
        /// Write function for the concrete class.
        /// </summary>
        /// <returns>string</returns>
        public abstract string Write();
        }

    #endregion

    #region Concrete Readers and Writers

    /// <summary>
    /// Reads a string to be hashed.
    /// </summary>
    public class ReaderString : Sa.Core.aStreamReader
    {
        /// <summary>
        /// Stores the string for later use.
        /// </summary>
        /// <param name="itemString">string</param>
        public ReaderString(string itemString)
        {
            base.Item = itemString;
        }

        /// <summary>
        /// Converts the string designated by itemString to System.IO.Stream.
        /// </summary>
        /// <returns>System.IO.Stream</returns>
        public override System.IO.Stream Read()
        {
            byte[] b = Encoding.UTF8.GetBytes(base.Item);
            System.IO.MemoryStream s = new System.IO.MemoryStream(b);

            return s;
        }
    }

    /// <summary>
    /// Reads a file to be hashed.
    /// </summary>
    public class ReaderFile : Sa.Core.aStreamReader
    {
        /// <summary>
        /// Stores the file path for later use.
        /// </summary>
        /// <param name="FilePath">string: Path to the file.</param>
        public ReaderFile(string FilePath)
        {
            base.Item = FilePath;
        }

        /// <summary>
        /// Converts the file designated by FilePath to System.IO.Stream.
        /// </summary>
        /// <returns>System.IO.Stream</returns>
        public override System.IO.Stream Read()
        {
            System.IO.Stream s = null;

            if (System.IO.File.Exists(base.Item))
            {
                s = System.IO.File.OpenRead(base.Item);
            }

            return s;
        }
    }

    /// <summary>
    /// Reads a user ID, password, user salt, and password salt to hashed.
    /// </summary>
    public class ReaderUPSalt : Sa.Core.aStreamReader
    {
        /// <summary>
        /// Store the string for later use.
        /// </summary>
        /// <param name="uName">string: User name</param>
        /// <param name="pWord">string: Password</param>
        /// <param name="uSalt">string: User name salt</param>
        /// <param name="pSalt">string: Password salt</param>
        public ReaderUPSalt(string uName, string pWord, string uSalt, string pSalt)
        {
            base.Item = uName + uSalt + pWord + pSalt;
        }

        /// <summary>
        /// Converts the string (uName + uSalt + pWord + pSalt) to System.IO.Stream.
        /// </summary>
        /// <returns>System.IO.Stream</returns>
        public override System.IO.Stream Read()
        {
            byte[] b = Encoding.UTF8.GetBytes(base.Item);
            System.IO.MemoryStream ms = new System.IO.MemoryStream(b);
            return ms;
        }
    }

    /// <summary>
    /// Converts a byte array to a string.
    /// </summary>
    public class WriterByte : Sa.Core.aByteWriter
    {
        /// <summary>
        /// Store the byte array.
        /// </summary>
        /// <param name="b">byte[]</param>
        public override void Store(byte[] b)
        {
            base.Item = b;
        }

        /// <summary>
        /// Outputs the byte array as a string.
        /// </summary>
        /// <returns>string</returns>
        public override string Write()
        {
            string s = null;

            if (base.Item != null)
            {
                s = BitConverter.ToString(base.Item);
                s = s.Replace("-", String.Empty);
            }

            return s;
        }
    }

    #endregion

    /// <summary>
    /// Provides the security components for Solomon
    /// </summary>
    public class Security
    {
        /// <summary>
        /// Enumerates the available hash algorithms
        /// </summary>
        /// <remarks>
        /// Represents the base class from which all implementations of cryptographic hash algorithms must derive.
        /// <para>
        /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.hashalgorithm.aspx">
        /// HashAlgorithm Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.hashalgorithm.aspx </a>
        /// </para>
        /// </remarks>        
        public enum Algorithm : int
        {
            /// <summary>
            /// Computes the SHA512 (512-Bit) hash for the input data.
            /// </summary>
            /// <remarks>
            /// <para>
            /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha512.aspx">
            /// SHA512 Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha512.aspx </a>
            /// </para>
            /// </remarks>
            SHA512 = 512,

            /// <summary>
            /// Computes the SHA384 (384-Bit) hash for the input data.
            /// </summary>
            /// <remarks>
            /// <para>
            /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha384.aspx">
            /// SHA384 Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha384.aspx </a>
            /// </para>
            /// </remarks>
            SHA384 = 384,

            /// <summary>
            /// Computes the SHA256 (256-Bit) hash for the input data.
            /// </summary>
            /// <remarks>
            /// <para>
            /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha256.aspx">
            /// SHA512 Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha256.aspx </a>
            /// </para>
            /// </remarks>
            SHA256 = 256,

            /// <summary>
            /// Computes the SHA1 (160-Bit) hash for the input data.
            /// </summary>
            /// <remarks>
            /// <para>
            /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha1.aspx">
            /// SHA1 Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.sha1.aspx </a>
            /// </para>
            /// </remarks>
			[Obsolete("Questionable for security purposes; use a different Sa.Security.Algorithm", true)]
            SHA1 = 1,

            /// <summary>
            /// Computes the MD160 (160-Bit) hash for the input data.
            /// </summary>
            /// <remarks>
            /// <para>
            /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.ripemd160.aspx">
            /// RIPEMD160 Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.ripemd160.aspx </a>
            /// </para>
            /// </remarks>
            [Obsolete("Questionable for security purposes; use a different Sa.Security.Algorithm", true)]
            RIPEMD160 = 160,

            /// <summary>
            /// Computes the MD5 (128-bit) hash for the input data.
            /// </summary>
            /// <remarks>
            /// <para>
            /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.md5.aspx">
            /// MD5 Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.md5.aspx </a>
            /// </para>
            /// </remarks>
			[Obsolete("Questionable for security purposes; use a different Sa.Security.Algorithm", true)]
            MD5 = 5
        };

        /// <summary>
        /// Utilizes the iStreamReader to retrieve input data; outputs hash data using iByteWriter.  The hash algorithm is selected in Algorithm.
        /// </summary>
        /// <param name="iReader">Sa.Security.iStreamReader</param>
        /// <param name="iWriter">Sa.Security.iByteWriter</param>
        /// <param name="Algorithm">Sa.Security.Algorithm</param>
        /// <remarks>
        /// <code>
        ///    Sa.Security.ReaderString r = new Sa.Security.ReaderString("String");
        ///    Sa.Security.WriterByte w = new Sa.Security.WriterByte();
        ///    Sa.Security.Algorithm h = Sa.Security.Algorithm.SHA256;
        ///    Sa.Security.HashService(r, w, h);
        ///    w.Write();
        /// </code>
        /// </remarks>
        public static void HashService(Sa.Core.iStreamReader iReader, Sa.Core.iByteWriter iWriter, Sa.Core.Security.Algorithm Algorithm)
        {
            System.IO.Stream s = null;
            byte[] b = null;

            System.Security.Cryptography.HashAlgorithm a = Sa.Core.Security.HashAlgorithm(Algorithm);

            s = iReader.Read();

            if (s != null)
            {
                b = a.ComputeHash(s);
                s.Close();
                s.Dispose();
            }

            iWriter.Store(b);
        }

        /// <summary>
        /// Converts the enumerated hash algorithm (Sa.Security.Algorithm) to the appropiate class.
        /// </summary>
        /// <param name="HashType"></param>
        /// <returns>System.Security.Cryptography.HashAlgorithm</returns>
        /// <remarks>
        /// Represents the base class from which all implementations of cryptographic hash algorithms must derive.
        /// <para>
        /// <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.hashalgorithm.aspx">
        /// HashAlgorithm Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.hashalgorithm.aspx </a>
        /// </para>
        /// </remarks> 
        public static System.Security.Cryptography.HashAlgorithm HashAlgorithm(Sa.Core.Security.Algorithm HashType)
        {
            System.Security.Cryptography.HashAlgorithm h = null;

            switch (HashType)
            {
                case Sa.Core.Security.Algorithm.SHA512:
                    h = new System.Security.Cryptography.SHA512Managed();
                    break;

                case Sa.Core.Security.Algorithm.SHA384:
                    h = new System.Security.Cryptography.SHA384Managed();
                    break;

                case Sa.Core.Security.Algorithm.SHA256:
                    h = new System.Security.Cryptography.SHA256Managed();
                    break;

                //case Sa.Core.Security.Algorithm.SHA1:
				//    h = new System.Security.Cryptography.SHA1Managed();
				//    break;

                //case Sa.Core.Security.Algorithm.RIPEMD160:
				//    h = new System.Security.Cryptography.RIPEMD160Managed();
				//    break;

                //case Sa.Core.Security.Algorithm.MD5:
                //    h = new System.Security.Cryptography.MD5CryptoServiceProvider();
                //    break;
            }

            return h;

        }

        /// <summary>
        /// Creates a random salt string bases on a byte length.
        /// </summary>
        /// <param name="ByteLength">Integer from 0 to 255; Default: 32</param>
        /// <returns>string: Twice the ByteLength.</returns>
        public static string GetRandomSalt(Byte ByteLength = 32)
        {
            System.Security.Cryptography.RNGCryptoServiceProvider rng = new System.Security.Cryptography.RNGCryptoServiceProvider();

            byte[] salt = new Byte[ByteLength];
            rng.GetBytes(salt);

            return BytesToHex(salt, "x2");
        }

        /// <summary>
        /// Converts a series of bytes to a string.
        /// </summary>
        /// <param name="toConvert">byte array that will be converted to a string.</param>
        /// <param name="NumericFormat">byte array that will be converted to a string.</param>
        /// <returns>string</returns>
        /// <remarks>
        /// <para>
        /// <a href="http://msdn.microsoft.com/en-us/library/dwhawy9k.aspx">
        /// Standard Numeric Format Strings: http://msdn.microsoft.com/en-us/library/dwhawy9k.aspx </a>
        /// </para>
        /// <para>
        /// <a href="http://msdn.microsoft.com/en-us/library/dwhawy9k.aspx#XFormatString">
        /// The Hexadecimal ("X") Format Specifier: http://msdn.microsoft.com/en-us/library/dwhawy9k.aspx#XFormatString </a>
        /// </para>
        /// </remarks>
        public static string BytesToHex(byte[] toConvert, string NumericFormat = "x2")
        {
            StringBuilder sb = new StringBuilder(toConvert.Length * 2);
            foreach (byte b in toConvert)
            {
                sb.Append(b.ToString(NumericFormat));
            }
            return sb.ToString();
        }
    }
}