﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()>
Public Class eMail_Test

    Private testContextInstance As TestContext

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

    <TestMethod()>
    Public Sub ValidateAddress()

        Dim t As New List(Of String)
        Dim f As New List(Of String)
        Dim b As Boolean = False

        t.Add("Test.Prefix@Server.Suffix.com")
        t.Add("Test.Prefix@Server.Suffix.co.uk")
        t.Add("a.little.unusual@example.com")

        f.Add("")
        f.Add("Abc.example.com")
        f.Add("A@b@c@example.com")
        f.Add("""(),:;<>[\]@example.com")
        f.Add("just""not""right@example.com")
        f.Add("this\ is\""really\""not\\allowed@example.com")

        For Each s As String In t
            b = Sa.Core.eMail.Address.ValidateAddress(s)
            Assert.IsTrue(b, "Sa.Core.eMail.Address.ValidateAddress: " & s)
        Next s

        For Each s As String In f
            b = Sa.Core.eMail.Address.ValidateAddress(s)
            Assert.IsFalse(b, "Sa.Core.eMail.Address.ValidateAddress: " & s)
        Next s

    End Sub

    <TestMethod()>
    Public Sub ValidateAddressAndHost()

        Dim t As New List(Of String)
        Dim f As New List(Of String)
        Dim b As Boolean = False

        t.Add("rogge.heflin@solomononline.com")
        t.Add("Test.Prefix@google.co.uk")

        f.Add("Abc.example.com")
        f.Add("badAddress@ThisBetterNotExist.nothere")

        For Each s As String In t
            b = Sa.Core.eMail.Address.ValidateAddressHost(s)
            Assert.IsTrue(b, "Sa.Core.eMail.Address.ValidateAddress: " & s)
        Next s

        For Each s As String In f
            b = Sa.Core.eMail.Address.ValidateAddressHost(s)
            Assert.IsFalse(b, "Sa.Core.eMail.Address.ValidateAddress: " & s)
        Next s

    End Sub

    <TestMethod()>
    Public Sub ValidateHost()

        Dim t As New List(Of String)
        Dim f As New List(Of String)
        Dim b As Boolean = False

        t.Add("solomononline.com")
        t.Add("google.com")

        f.Add("i.hope.this.is.not.a.host.com.us")
        f.Add("ThisCertaintlyIsNot@Host")

        For Each s As String In t
            b = Sa.Core.eMail.Address.ValidateHost(s)
            Assert.IsTrue(b, "Sa.Core.eMail.Address.ValidateHost: " & s)
        Next s

        For Each s As String In f
            b = Sa.Core.eMail.Address.ValidateHost(s)
            Assert.IsFalse(b, "Sa.Core.eMail.Address.ValidateHost: " & s)
        Next s

    End Sub

End Class
