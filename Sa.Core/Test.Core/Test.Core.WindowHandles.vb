﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()>
Public Class WindowHandles_Test

    Private testContextInstance As TestContext

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

    <TestMethod()>
    Public Sub APICalls()

        Dim t As String = "Test message box"
        Dim c As String = "Test Caption"
        Dim k As String = "#32770"

        Dim ThreadID As IntPtr = Nothing
        Dim ProcessID As IntPtr = Nothing

        Dim i As System.Int32 = 0

        Dim h As IntPtr = Sa.Core.WindowHandles.ReturnWindowHandle(CType(i, IntPtr), k, c)

        ThreadID = Sa.Core.WindowHandles.GetWindowThreadProcessId(h, ProcessID)

        Sa.Core.WindowHandles.IsWindow(h)

        Sa.Core.WindowHandles.SendMessageA(h, Sa.Core.WindowHandles.WM_CLOSE, IntPtr.Zero, IntPtr.Zero)

        Dim tLen As System.Int32 = CType(Sa.Core.WindowHandles.GetWindowTextLengthA(h), System.Int32)
        Dim tStringBuilder As New System.Text.StringBuilder("", tLen + 1)
        Sa.Core.WindowHandles.GetWindowTextA(h, tStringBuilder, tStringBuilder.Capacity)

        Dim cLen As System.Int32 = 256
        Dim cStringBuilder As New System.Text.StringBuilder("", cLen + 1)
        Sa.Core.WindowHandles.GetClassNameA(h, cStringBuilder, cStringBuilder.Capacity)

    End Sub

End Class
