﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()>
Public Class Performance_Test

    Private testContextInstance As TestContext

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

    <TestMethod()>
    Public Sub AvailablePhysicalMemory()

        Dim l As Single = Sa.Core.Performance.AvailablePhysicalMemory
        Dim b As Boolean = CType(l > 0.0, Boolean)
        Assert.IsTrue(b, "Performance.AvailablePhysicalMemory")

    End Sub

    <TestMethod()>
    Public Sub MachineName()

        Dim l As System.Int32 = Len(Sa.Core.Performance.MachineName)
        Dim b As Boolean = CType(l > 0, Boolean)
        Assert.IsTrue(b, "Performance.MachineName")

    End Sub

    <TestMethod()>
    Public Sub OSFullName()

        Dim l As System.Int32 = Len(Sa.Core.Performance.OSFullName)
        Dim b As Boolean = CType(l > 0, Boolean)
        Assert.IsTrue(b, "Performance.OSFullName")

    End Sub

    <TestMethod()>
    Public Sub OSPlatform()

        Dim l As System.Int32 = Len(Sa.Core.Performance.OSPlatform)
        Dim b As Boolean = CType(l > 0, Boolean)
        Assert.IsTrue(b, "Performance.OSPlatform")

    End Sub

    <TestMethod()>
    Public Sub OSVersion()

        Dim l As System.Int32 = Len(Sa.Core.Performance.OSVersion)
        Dim b As Boolean = CType(l > 0, Boolean)
        Assert.IsTrue(b, "Performance.OSVersion")

    End Sub

    <TestMethod()>
    Public Sub PercentAvailableMemory()

        Dim l As Single = Sa.Core.Performance.PercentAvailableMemory
        Dim b As Boolean = CType(l > 0.0, Boolean)
        Assert.IsTrue(b, "Performance.PercentAvailableMemory")

    End Sub

    <TestMethod()>
    Public Sub PortThreads()

        Dim l As Single = Sa.Core.Performance.PortThreads
        Dim b As Boolean = CType(l > 0.0, Boolean)
        Assert.IsTrue(b, "Performance.PortThreads")

    End Sub

    <TestMethod()>
    Public Sub ProcessorCount()

        Dim l As Single = Sa.Core.Performance.ProcessorCount
        Dim b As Boolean = CType(l > 0.0, Boolean)
        Assert.IsTrue(b, "Performance.ProcessorCount")

    End Sub

    <TestMethod()>
    Public Sub ScreenDepth()

        Dim l As System.Int32 = Len(Sa.Core.Performance.ScreenDepth)
        Dim b As Boolean = CType(l > 0, Boolean)
        Assert.IsTrue(b, "Performance.ScreenDepth")

    End Sub

    <TestMethod()>
    Public Sub ScreenResolution()

        Dim l As System.Int32 = Len(Sa.Core.Performance.ScreenResolution)
        Dim b As Boolean = CType(l > 0, Boolean)
        Assert.IsTrue(b, "Performance.ScreenResolution")

    End Sub

    <TestMethod()>
    Public Sub TotalPhysicalMemory()

        Dim l As Single = Sa.Core.Performance.TotalPhysicalMemory
        Dim b As Boolean = CType(l > 0.0, Boolean)
        Assert.IsTrue(b, "Performance.TotalPhysicalMemory")

    End Sub

    <TestMethod()>
    Public Sub WorkerThreads()

        Dim l As Single = Sa.Core.Performance.WorkerThreads
        Dim b As Boolean = CType(l > 0, Boolean)
        Assert.IsTrue(b, "Performance.WorkerThreads")

    End Sub

End Class
