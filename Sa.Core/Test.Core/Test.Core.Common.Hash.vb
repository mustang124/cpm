﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Security.AccessControl
Imports Microsoft.VisualBasic.FileIO

<TestClass()>
Public Class Hash_Test

    <TestMethod()>
    Public Sub HashService()

        Const NequePorro As String = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit."
        Const sTempFile As String = "sNequePorroCS.txt"
        Const tTempFile As String = "tNequePorroCS.txt"

        ' Hash values for the NequePorro string
        Dim HastList As New List(Of Object())
        HastList.Add({Sa.Core.Hash.Algorithm.MD5, "E8E15529CBA3B5E31CAB57B29F5CB1B6"})
        HastList.Add({Sa.Core.Hash.Algorithm.RIPEMD160, "0FC540F51100CD03605DCCB83843A9EA9DDC684D"})
        HastList.Add({Sa.Core.Hash.Algorithm.SHA1, "6CEA79310F48E231502C44101BBAACC6816E0012"})
        HastList.Add({Sa.Core.Hash.Algorithm.SHA256, "EFF5C5B7BA738376E1D6CD57EF3A5DEEBAE9A181DC8D9452FE90C934A80C3EF6"})
        HastList.Add({Sa.Core.Hash.Algorithm.SHA384, "06B8042950FE3E9F649D525F8255F2E696E4C94FAE539130049DA1BCC7506BC534EC27C4213A9D6C9B5421F4FA65FC0F"})
        HastList.Add({Sa.Core.Hash.Algorithm.SHA512, "CF804ECD6EE242FE35304F19DFEE84568F9E8E00F03C1C17C3E605A709425DCB0C0FCDF30598B4A607962E3F60C2A7B541A188805BD55906294E0F2ED14D6B13"})

        System.IO.File.WriteAllText(sTempFile, NequePorro, System.Text.Encoding.ASCII)
        System.IO.File.WriteAllText(tTempFile, NequePorro + "..", System.Text.Encoding.ASCII)

        Dim s As New Sa.Core.Hash.ReaderFile(sTempFile)
        Dim t As New Sa.Core.Hash.ReaderFile(tTempFile)
        Dim x As New Sa.Core.Hash.ReaderString(NequePorro)
        Dim w As New Sa.Core.Hash.WriterByte
        Dim h As Sa.Core.Hash.Algorithm = Nothing

        For Each iHash As Object() In HastList

            h = CType(iHash(0), Sa.Core.Hash.Algorithm)

            Sa.Core.Hash.HashService(x, w, h)
            Assert.AreEqual(w.Write, iHash(1), "Sa.Core.OpsFile.HashService (Hash Equality Failed (String): " + iHash(0).ToString + ")")

            Sa.Core.Hash.HashService(s, w, h)
            Assert.AreEqual(w.Write, iHash(1), "Sa.Core.OpsFile.HashService (Hash Equality Failed (Source File): " + iHash(0).ToString + ")")

            Sa.Core.Hash.HashService(t, w, h)
            Assert.AreNotEqual(w.Write, iHash(1), "Sa.Core.OpsFile.HashService (Hash Inequality Failed (Target File): " + iHash(0).ToString + ")")

        Next iHash

        System.IO.File.Delete(sTempFile)
        System.IO.File.Delete(tTempFile)

        Assert.IsFalse(System.IO.File.Exists(sTempFile), "Sa.Core.OpsFile.CheckSum (D.0)")
        Assert.IsFalse(System.IO.File.Exists(tTempFile), "Sa.Core.OpsFile.CheckSum (D.1)")

    End Sub

    <TestMethod()>
    Public Sub HashAlgorithm()

        Dim h As New List(Of Object())
        h.Add({Sa.Core.Hash.Algorithm.SHA512, New System.Security.Cryptography.SHA512Managed})
        h.Add({Sa.Core.Hash.Algorithm.SHA384, New System.Security.Cryptography.SHA384Managed})
        h.Add({Sa.Core.Hash.Algorithm.SHA256, New System.Security.Cryptography.SHA256Managed})
        h.Add({Sa.Core.Hash.Algorithm.SHA1, New System.Security.Cryptography.SHA1Managed})
        h.Add({Sa.Core.Hash.Algorithm.RIPEMD160, New System.Security.Cryptography.RIPEMD160Managed})
        h.Add({Sa.Core.Hash.Algorithm.MD5, New System.Security.Cryptography.MD5CryptoServiceProvider})

        Dim e As System.Security.Cryptography.HashAlgorithm = Nothing
        Dim a As System.Security.Cryptography.HashAlgorithm = Nothing

        For Each s() As Object In h

            e = DirectCast(s(1), System.Security.Cryptography.HashAlgorithm)
            a = Sa.Core.Hash.HashAlgorithm(DirectCast(s(0), Sa.Core.Hash.Algorithm))

            Assert.AreEqual(e.GetType(), a.GetType(), "Sa.Core.OpsFile.HashAlgorithm (" + s(0).ToString + ")")

        Next s

    End Sub

    <TestMethod()>
    Public Sub PlantHashTest()

        Dim PlantName As String = "Pemex Salamanca"
        Dim ContractBeg As String = "2009.01.01"
        Dim ContractEnd As String = "2010.12.31"
        Dim CTFile As String = "53UcAk7n+gzfJCJ+jFFcPb3CGU2ysCi0/0blL/1h/FU="
        Dim hString As String = PlantName
        Dim x As New Sa.Core.Hash.ReaderString(hString)
        Dim w As New Sa.Core.Hash.WriterByte
        Dim h As Sa.Core.Hash.Algorithm = Sa.Core.Hash.Algorithm.MD5

        Sa.Core.Hash.HashService(x, w, h)

        Dim s As String = w.Write()

    End Sub

    <TestMethod()>
    Public Sub HashAlgorithmCSharp()

        Dim h As New List(Of Object())
        h.Add({Sa.Core.Security.Algorithm.SHA512, New System.Security.Cryptography.SHA512Managed})
        h.Add({Sa.Core.Security.Algorithm.SHA384, New System.Security.Cryptography.SHA384Managed})
        h.Add({Sa.Core.Security.Algorithm.SHA256, New System.Security.Cryptography.SHA256Managed})
        'h.Add({Sa.Core.Security.Algorithm.SHA1, New System.Security.Cryptography.SHA1Managed})
        'h.Add({Sa.Core.Security.Algorithm.RIPEMD160, New System.Security.Cryptography.RIPEMD160Managed})
        'h.Add({Sa.Core.Security.Algorithm.MD5, New System.Security.Cryptography.MD5CryptoServiceProvider})

        Dim e As System.Security.Cryptography.HashAlgorithm = Nothing
        Dim a As System.Security.Cryptography.HashAlgorithm = Nothing

        For Each s() As Object In h

            e = DirectCast(s(1), System.Security.Cryptography.HashAlgorithm)
            a = Sa.Core.Security.HashAlgorithm(CType(s(0), Sa.Core.Security.Algorithm))

            Assert.AreEqual(e.GetType(), a.GetType(), "Sa.Core.OpsFile.HashAlgorithm (" + s(0).ToString + ")")

        Next s

    End Sub

    <TestMethod()>
    Public Sub ReaderSalt()

        'Dim uName As String = ""
        'Dim pWord As String = ""
        'Dim sSalt As String = ""
        'Dim h As Sa.Core.Hash_Accessor.Algorithm = Sa.Core.Hash_Accessor.Algorithm.SHA512

        'Dim x As New Sa.Core.Hash_Accessor.ReaderSalt(uName, pWord, sSalt)
        'Dim w As New Sa.Core.Hash_Accessor.WriterByte

        'Dim csp As New System.Security.Cryptography.RNGCryptoServiceProvider

        Debug.Print(Sa.Core.Hash.GetRandomSalt)

    End Sub

End Class
