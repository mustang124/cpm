﻿
''' <summary>
''' Writes notifications, warnings, and errors to the logs.
''' </summary>
''' <remarks>
''' <para>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </para>
''' </remarks>
Public Module Log

    Public Const LogName As String = "Application"

    ''' <summary>
    ''' Write an entry to a system log
    ''' </summary>
    ''' <param name="EventMessage"></param>
    ''' <param name="LogEntryType"></param>
    ''' <param name="ShowDialog"></param>
    ''' <param name="LogName"></param>
    ''' <param name="EventID"></param>
    ''' <param name="SubCategory"></param>
    ''' <remarks>
    ''' <para>
    ''' Provides interaction with Windows event logs. 
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.diagnostics.eventlog.aspx">
    ''' EventLog Class: http://msdn.microsoft.com/en-us/library/system.diagnostics.eventlog.aspx </a>
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/f6wy11we.aspx">
    ''' EventLog.WriteEntry Method (String, EventLogEntryType, Int32): http://msdn.microsoft.com/en-us/library/f6wy11we.aspx </a>
    ''' </para>
    ''' </remarks>
    Public Sub Write(
        ByVal EventMessage As String,
        ByVal LogEntryType As EventLogEntryType,
        ByVal ShowDialog As Boolean,
        ByVal LogName As String,
        ByVal EventID As System.Int32,
        Optional ByVal SubCategory As Short = 0)

        Dim sMachine As String = Sa.Core.Performance.MachineName
        Dim sSource As String = Sa.Core.AssemblyInfo.ProductAndVersion

        Try

            Dim eLog As New EventLog(LogName, sMachine, sSource)

            eLog.WriteEntry(EventMessage, LogEntryType, EventID, SubCategory)

            eLog.Close()

        Catch ieae As System.ComponentModel.InvalidEnumArgumentException
            WriteError(LogName, ieae)

        Catch ae As ArgumentException
            WriteError(LogName, ae)

        Catch ioe As InvalidOperationException
            WriteError(LogName, ioe)

        Catch w32 As System.ComponentModel.Win32Exception
            WriteError(LogName, w32)

        Finally

            If ShowDialog Then

                Dim eStr As String = Sa.Core.Log.ExceptionToString(LogEntryType, True) + " has occured in this application." + ControlChars.CrLf + ControlChars.CrLf + EventMessage + ControlChars.CrLf + ControlChars.CrLf + "The details are stored in the Event Log: " + LogName + "."

                DisplayEntry(eStr, sSource, LogEntryType)

            End If

        End Try

    End Sub

    ''' <summary>
    ''' Creates a plain text conversion for EventLogEntryType
    ''' </summary>
    ''' <param name="LogType">EventLogEntryType representing the log type for the error.</param>
    ''' <param name="Capital">Boolean representing wether the first letter is capitalized.</param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Friend Function ExceptionToString(ByVal LogType As EventLogEntryType, ByVal Capital As Boolean) As String

        Dim s As String = Nothing

        Select Case LogType
            Case EventLogEntryType.Error
                s = "n Error"
            Case EventLogEntryType.FailureAudit
                s = " failure audit"
            Case EventLogEntryType.Information
                s = "n informational event"
            Case EventLogEntryType.SuccessAudit
                s = " success audit"
            Case EventLogEntryType.Warning
                s = " warning"
            Case Else
                s = "n unknown error type"
        End Select

        If Capital Then s = "A" + s Else s = "a" + s

        Return s

    End Function

    ''' <summary>
    ''' Informs the end user when an item is written to the message log.
    ''' </summary>
    ''' <param name="e">String representing the message text.</param>
    ''' <param name="c">String representing the message box caption.</param>
    ''' <param name="t">EventLogEntryType to modify the message box appearance.</param>
    ''' <remarks></remarks>
    Friend Function DisplayEntry(
        ByVal e As String,
        ByVal c As String,
        ByVal t As EventLogEntryType) As System.Windows.Forms.DialogResult

        Dim p As Windows.Forms.MessageBoxIcon = Nothing
        Dim s As String = Nothing

        Select Case t
            Case EventLogEntryType.Error
                p = Windows.Forms.MessageBoxIcon.Error
                s = " - Error!"
            Case EventLogEntryType.Warning
                p = Windows.Forms.MessageBoxIcon.Warning
                s = " - Warning!"
            Case EventLogEntryType.Information
                p = Windows.Forms.MessageBoxIcon.Information
                s = ""
            Case EventLogEntryType.FailureAudit
                p = Windows.Forms.MessageBoxIcon.Stop
                s = " - Audit Failure!"
            Case EventLogEntryType.SuccessAudit
                p = Windows.Forms.MessageBoxIcon.Asterisk
                s = " - Audit Success!"
            Case Else
                p = Windows.Forms.MessageBoxIcon.None
                s = ""
        End Select

        Return System.Windows.Forms.MessageBox.Show(e, c + s, Windows.Forms.MessageBoxButtons.OK, p, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.ServiceNotification, False)

    End Function

    ''' <summary>
    ''' Informs the end user that an error occured when adding an entry to the log.
    ''' </summary>
    ''' <param name="sLog">String representing the log name.</param>
    ''' <param name="x">Exception that occurs when trying to write to the system log.</param>
    ''' <remarks></remarks>
    Friend Sub WriteError(
        ByVal sLog As String,
        ByVal x As Exception)

        Dim e As String = "An error occured while writing to the Windows Log: " + sLog + "." + ControlChars.CrLf + ControlChars.CrLf + x.Message
        Dim c As String = x.Source + " - System Log Error (" + x.GetType.ToString() + ")!"

        System.Windows.Forms.MessageBox.Show(e, c, Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.ServiceNotification, False)

    End Sub

End Module