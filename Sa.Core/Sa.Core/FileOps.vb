Imports System.Security.AccessControl

''' <summary>
''' Perform file operations such as delete, copy, and checksum.
''' </summary>
''' <remarks>
''' <para>
''' Copyright � 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </para>
''' </remarks>
Public Module FileOps

    ''' <summary>
    ''' Copies a file from one location to another; verifies the target file was created.
    ''' </summary>
    ''' <param name="PathSource">Path to the orignal file.</param>
    ''' <param name="PathTarget">Path to the copied file.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function CopyFile(ByVal PathSource As String, ByVal PathTarget As String) As Boolean

        If PathSource <> PathTarget Then

            If System.IO.File.Exists(PathSource) Then System.IO.File.Copy(PathSource, PathTarget, True)

        End If

        Return System.IO.File.Exists(PathTarget)

    End Function

    ''' <summary>
    ''' Deletes a file; verifies the file has been deleted.
    ''' </summary>
    ''' <param name="PathDelete">Path to file to be deleted.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Loops tyring to delete a file.  Necessary because simulation's do not timely close.</remarks>
    Public Function DeleteFile(ByVal PathDelete As String) As Boolean

        Dim b As Boolean = False

        Const iterations As System.Int32 = 0
        Dim i As System.Int32 = 0

        While System.IO.File.Exists(PathDelete) AndAlso i <= iterations

            System.GC.WaitForPendingFinalizers()
            System.GC.Collect()
            System.GC.WaitForFullGCComplete()

            Try

                System.IO.File.Delete(PathDelete)

            Catch exIO As System.IO.IOException

            Catch exUac As System.UnauthorizedAccessException

            End Try

            i = i + 1

        End While

        b = Not System.IO.File.Exists(PathDelete)

        Return b

    End Function

    ''' <summary>
    ''' Verifies the target file is an exact copy of the original file.
    ''' </summary>
    ''' <param name="PathSource">Path to the orignal file (Assumed to be correct).</param>
    ''' <param name="PathTarget">Path to the copied file (Verfiy against PathSource).</param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Function VerifyFile(ByVal PathSource As String, ByVal PathTarget As String) As Boolean

        Dim bVerifyFile As Boolean = False

        Dim s As New Sa.Core.Hash.ReaderFile(PathSource)
        Dim t As New Sa.Core.Hash.ReaderFile(PathTarget)
        Dim h As Sa.Core.Hash.Algorithm = Sa.Core.Hash.Algorithm.SHA512

        Dim w As New Sa.Core.Hash.WriterByte

        Sa.Core.Hash.HashService(s, w, h)

        Dim pCheckSum As String = w.Write

        Sa.Core.Hash.HashService(t, w, h)

        Dim tCheckSum As String = w.Write

        If pCheckSum <> Nothing AndAlso tCheckSum <> Nothing AndAlso pCheckSum = tCheckSum Then bVerifyFile = True

        Return bVerifyFile

    End Function

    ''' <summary>
    ''' Copies a folder from one location to another; verifies the target folder was created.
    ''' </summary>
    ''' <param name="PathSource">Path to the orignal folder.</param>
    ''' <param name="PathTarget">Path to the copied folder.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CopyFolder(ByVal PathSource As String, ByVal PathTarget As String) As Boolean

        If PathSource <> PathTarget Then Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(PathSource, PathTarget, True)

        Return System.IO.Directory.Exists(PathTarget)

    End Function

    ''' <summary>
    ''' Deletes a folder; verifies the folder has been deleted.
    ''' </summary>
    ''' <param name="PathDelete">Path to the folder to be deleted.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Loops tyring to delete a folder.  Necessary because simulation's do not timely close.</remarks>
    Public Function DeleteFolder(ByVal PathDelete As String) As Boolean

        Const iterations As System.Int32 = 20
        Dim i As System.Int32 = 0

        While System.IO.Directory.Exists(PathDelete) AndAlso i <= iterations

            System.GC.WaitForPendingFinalizers()
            System.GC.Collect()
            System.GC.WaitForFullGCComplete()

            Try

                System.IO.Directory.Delete(PathDelete, True)

            Catch exIO As System.IO.IOException

            Catch exUac As UnauthorizedAccessException

            End Try

            i = i + 1

        End While

        Return Not System.IO.Directory.Exists(PathDelete)

    End Function

    ''' <summary>
    ''' Verifies all the files are in one folder are the same as the files in another folder.
    ''' </summary>
    ''' <param name="PathSource">Path to the original folder (Assumed to be correct).</param>
    ''' <param name="PathTarget">Path to the copied folder (Verfiy against PathSource).</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function VerifyFolder(
        ByVal PathSource As String,
        ByVal PathTarget As String) As Boolean

        Dim bVerifyFolder As Boolean = False

        Dim i As System.Int32 = 0
        Dim j As System.Int32 = 0

        For Each f As String In System.IO.Directory.GetFiles(PathSource, "*", IO.SearchOption.AllDirectories)

            i = i + 1

            If Sa.Core.FileOps.VerifyFile(f, Replace(f, PathSource, PathTarget, 1, 1, CompareMethod.Text)) Then
                j = j + 1
            Else
                Return bVerifyFolder
            End If

        Next f

        If i = j Then bVerifyFolder = True

        Return bVerifyFolder

    End Function

    ''' <summary>
    ''' Verifies the files in a folder are not currupted by comparing checksums of files in the folder to the checksums stored in a text file.  Each file must have a corresponding checksum. 
    ''' </summary>
    ''' <param name="PathCheckSum">Path to a text file containing pipe delimited checksum values</param>
    ''' <param name="PathSource">Path to the source files to be verified</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>The CheckSum text file contains two (2) fields: the relative path and the CheckSum.  When the file's CheckSum is unknown or changes, only the relative path exists.</remarks>
    Public Function VerifySourceFiles(
        ByVal PathCheckSum As String,
        ByVal PathSource As String) As Boolean

        Dim bVerifySourceFiles As Boolean = False

        Dim sCheckSum As String = System.IO.File.ReadAllText(PathCheckSum, System.Text.Encoding.ASCII).Replace(Chr(32), ControlChars.CrLf)
        Dim aCheckSum() As String = sCheckSum.Split(ControlChars.CrLf.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        Const colFile As System.Int32 = 0
        Const colType As System.Int32 = 1
        Const colHash As System.Int32 = 2
        Const colCount As System.Int32 = colHash

        Dim dCheckSum(,) As String = Nothing : ReDim dCheckSum(colCount, 0)
        Dim dRank As System.Int32 = dCheckSum.Rank
        Dim dLength As System.Int32 = 0

        Dim iCheckSum() As String = Nothing

        For i As System.Int32 = 0 To aCheckSum.Length - 1 Step 1

            If dCheckSum(colFile, dLength) <> Nothing Then ReDim Preserve dCheckSum(colCount, dLength + 1)

            dLength = CType(dCheckSum.Length / (dRank + 1), System.Int32) - 1

            iCheckSum = aCheckSum(i).Split({CType("|", Char)}, System.StringSplitOptions.RemoveEmptyEntries)

            If iCheckSum.Length > colFile Then dCheckSum(colFile, dLength) = iCheckSum(colFile)
            If iCheckSum.Length > colType Then dCheckSum(colType, dLength) = iCheckSum(colType)
            If iCheckSum.Length > colHash Then dCheckSum(colHash, dLength) = iCheckSum(colHash)

        Next i

        Dim nCheckSum As System.Int32 = 0                            ' Number of matching CheckSums
        Dim nFiles As System.Int32 = 0                               ' Number of files

        For Each f As String In System.IO.Directory.GetFiles(PathSource, "*", IO.SearchOption.AllDirectories)

            nFiles = nFiles + 1

            For i As System.Int32 = 0 To dLength Step 1

                If Replace(f, PathSource, "", 1, -1, CompareMethod.Text) = dCheckSum(colFile, i) Then

                    Dim r As New Sa.Core.Hash.ReaderFile(f)
                    Dim w As New Sa.Core.Hash.WriterByte
                    Dim h As Sa.Core.Hash.Algorithm = Sa.Core.Hash.Algorithm.SHA512

                    Sa.Core.Hash.HashService(r, w, h)

                    If w.Write = dCheckSum(colHash, i) Or dCheckSum(colHash, i) = Nothing Then
                        nCheckSum = nCheckSum + 1
                    Else
                        Return bVerifySourceFiles
                    End If

                    Exit For

                End If

            Next i

        Next f

        If nCheckSum = nFiles And nCheckSum = dLength + 1 Then bVerifySourceFiles = True

ReturnValue:

        Return bVerifySourceFiles

    End Function

    '<Obsolete("Use for creating a list of files with respective Hash Strings", True)>
    'Public Sub ListFiles(ByVal PathSource As String, ByVal HashType As String)

    '    Dim NameFile As String = Nothing
    '    Dim HashString As String = Nothing

    '    For Each f As String In FileSystem.GetFiles(PathSource, FileIO.SearchOption.SearchAllSubDirectories)

    '        NameFile = Replace(f, PathSource, "", 1, -1, CompareMethod.Text)
    '        HashString = Sa.Core.FileOps.CheckSum(f, Sa.Core.FileOps.fHash(HashType))

    '        Debug.Print(NameFile + "|" + HashType + "|" + HashString)

    '    Next f

    'End Sub

End Module