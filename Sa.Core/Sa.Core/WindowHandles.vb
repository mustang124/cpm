﻿
Imports System.Runtime.InteropServices

''' <summary>
''' Indicates that the attributed method is exposed by an unmanaged dynamic-link library (DLL) as a static entry point.
''' </summary>
''' <remarks>
''' <para>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </para>
''' <para>
''' <a href="http://msdn.microsoft.com/en-us/library/system.runtime.interopservices.dllimportattribute.aspx">
''' DllImportAttribute Class: http://msdn.microsoft.com/en-us/library/system.runtime.interopservices.dllimportattribute.aspx </a>
''' </para>
''' <para>
''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ff468919.aspx">
''' Window Functions (user32.dll): http://msdn.microsoft.com/en-us/library/windows/desktop/ff468919.aspx </a>
''' </para>
''' <para>
''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ff468926.aspx">
''' Window Class Functions (user32.dll): http://msdn.microsoft.com/en-us/library/windows/desktop/ff468926.aspx </a>
''' </para>
''' <para>
''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ff468870.aspx">
''' Message Functions (user32.dll): http://msdn.microsoft.com/en-us/library/windows/desktop/ff468870.aspx </a>
''' </para>
''' </remarks>
Public Module WindowHandles

#Region " Window Notifications Constants "

    'http://msdn.microsoft.com/en-us/library/windows/desktop/ff468922.aspx

    Public Const WM_CLOSE As System.Int32 = &H10

    Public Const WM_ACTIVATEAPP As System.Int32 = &H1C
    Public Const WM_CANCELMODE As System.Int32 = &H1F
    Public Const WM_CHILDACTIVATE As System.Int32 = &H22

    Public Const WM_COMPACTING As System.Int32 = &H41
    Public Const WM_CREATE As System.Int32 = &H1
    Public Const WM_DESTROY As System.Int32 = &H2
    Public Const WM_ENABLE As System.Int32 = &HA
    Public Const WM_ENTERSIZEMOVE As System.Int32 = &H231
    Public Const WM_EXITSIZEMOVE As System.Int32 = &H232
    Public Const WM_GETICON As System.Int32 = &H7F
    Public Const WM_GETMINMAXINFO As System.Int32 = &H24
    Public Const WM_INPUTLANGCHANGE As System.Int32 = &H51
    Public Const WM_INPUTLANGCHANGEREQUEST As System.Int32 = &H50
    Public Const WM_MOVE As System.Int32 = &H3
    Public Const WM_MOVING As System.Int32 = &H216
    Public Const WM_NCACTIVATE As System.Int32 = &H86

    Public Const WM_NCCALCSIZE As System.Int32 = &H83
    Public Enum WM_NCCALCSIZE_Enum As System.Int32
        WM_NCCALCSIZE = &H83
        WVR_ALIGNTOP = &H10
        WVR_ALIGNRIGHT = &H80
        WVR_ALIGNLEFT = &H20
        WVR_ALIGNBOTTOM = &H40
        WVR_HREDRAW = &H100
        WVR_VREDRAW = &H200
        WVR_REDRAW = (WVR_HREDRAW Or WVR_VREDRAW)
        WVR_VALIDRECTS = &H400
    End Enum

    Public Const WM_NCCREATE As System.Int32 = &H81
    Public Const WM_NCDESTROY As System.Int32 = &H82
    Public Const WM_NULL As System.Int32 = &H0
    Public Const WM_QUERYDRAGICON As System.Int32 = &H37
    Public Const WM_QUERYOPEN As System.Int32 = &H13
    Public Const WM_QUIT As System.Int32 = &H12

    Public Const WM_SHOWWINDOW As System.Int32 = &H18
    Public Const SW_OTHERUNZOOM As Int32 = 4
    Public Const SW_OTHERZOOM As Int32 = 2
    Public Const SW_PARENTCLOSING As Int32 = 1
    Public Const SW_PARENTOPENING As Int32 = 3
    Public Enum WM_SHOWWINDOW_Enum As System.Int32
        WM_SHOWWINDOW = &H18
        SW_OTHERUNZOOM = 4
        SW_OTHERZOOM = 2
        SW_PARENTCLOSING = 1
        SW_PARENTOPENING = 3
    End Enum

    Public Const WM_SIZE As System.Int32 = &H5
    Public Enum WM_SIZE_Enum As System.Int32
        WM_SIZE = &H5
        SIZE_MAXHIDE = 4
        SIZE_MAXIMIZED = 2
        SIZE_MAXSHOW = 3
        SIZE_MINIMIZED = 1
        SIZE_RESTORED = 0
    End Enum

    Public Const WM_SIZING As System.Int32 = &H214
    Public Const WMSZ_BOTTOM As Int32 = 6
    Public Const WMSZ_BOTTOMLEFT As Int32 = 7
    Public Const WMSZ_BOTTOMRIGHT As Int32 = 8
    Public Const WMSZ_LEFT As Int32 = 1
    Public Const WMSZ_RIGHT As Int32 = 2
    Public Const WMSZ_TOP As Int32 = 3
    Public Const WMSZ_TOPLEFT As Int32 = 4
    Public Const WMSZ_TOPRIGHT As Int32 = 5
    Public Enum WM_SIZING_Enum As System.Int32
        WM_SIZING = &H214
        WMSZ_BOTTOM = 6
        WMSZ_BOTTOMLEFT = 7
        WMSZ_BOTTOMRIGHT = 8
        WMSZ_LEFT = 1
        WMSZ_RIGHT = 2
        WMSZ_TOP = 3
        WMSZ_TOPLEFT = 4
        WMSZ_TOPRIGHT = 5
    End Enum

    Public Const WM_STYLECHANGED As System.Int32 = &H7D
    Public Enum WM_STYLECHANGED_Enum As System.Int32
        WM_STYLECHANGED = &H7D
        GWL_EXSTYLE = -20
        GWL_STYLE = -16
    End Enum

    Public Const WM_STYLECHANGING As System.Int32 = &H7C
    Public Enum WM_STYLECHANGING_Enum As System.Int32
        WM_STYLECHANGING = &H7C
        GWL_EXSTYLE = -20
        GWL_STYLE = -16
    End Enum

    Public Const WM_THEMECHANGED As System.Int32 = &H31A
    Public Const WM_USERCHANGED As System.Int32 = &H54
    Public Const WM_WINDOWPOSCHANGED As System.Int32 = &H47
    Public Const WM_WINDOWPOSCHANGING As System.Int32 = &H46

#End Region

#Region " user32.dll API Calls "

    ''' <summary>
    ''' Gets the thread Process Identifier (PID) for the in-use application
    ''' </summary>
    ''' <param name="hwnd">A handle to the window or control.</param>
    ''' <param name="lpdwProcessId">A pointer to a variable that receives the process identifier.</param>
    ''' <returns>IntPtr</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633522.aspx">
    ''' GetWindowThreadProcessId function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633522.aspx </a>
    ''' </para>
    ''' </remarks>
    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="GetWindowThreadProcessId")>
    Public Function GetWindowThreadProcessId(ByVal hwnd As IntPtr, ByRef lpdwProcessId As IntPtr) As IntPtr
    End Function

    ''' <summary>
    ''' Sends the specified message to a window or windows. The SendMessage function calls the window procedure for the specified window and does not return until the window procedure has processed the message.
    ''' </summary>
    ''' <param name="hWnd">A handle to the window or control.</param>
    ''' <param name="Msg">The message to be sent. For lists of the system-provided messages, see System-Defined Messages.</param>
    ''' <param name="wParam">Additional message-specific information.</param>
    ''' <param name="lParam">Additional message-specific information.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms644950.aspx">
    ''' SendMessage function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms644950.aspx </a>
    ''' </para>
    ''' </remarks>
    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="SendMessageA")>
    Public Function SendMessageA(ByVal hWnd As IntPtr, ByVal Msg As Int32, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Boolean
    End Function

    ''' <summary>
    ''' Determines whether the specified window handle identifies an existing window.
    ''' </summary>
    ''' <param name="hWnd">A handle to the window or control to be tested.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633528.aspx">
    ''' IsWindow function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633528.aspx</a>
    ''' </para>
    ''' </remarks>
    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="IsWindow")>
    Public Function IsWindow(ByVal hWnd As IntPtr) As Boolean
    End Function

    ''' <summary>
    ''' Enumerates all nonchild windows associated with a thread by passing the handle to each window, in turn, to an application-defined callback function. EnumThreadWindows continues until the last window is enumerated or the callback function returns FALSE.
    ''' </summary>
    ''' <param name="dwThreadId">The identifier of the thread whose windows are to be enumerated.</param>
    ''' <param name="lpfn">A pointer to an application-defined callback function. For more information, see EnumThreadWndProc.</param>
    ''' <param name="lParam">An application-defined value to be passed to the callback function.</param>
    ''' <returns>IntPtr</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633495.aspx">
    ''' EnumThreadWindows function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633495.aspx </a>
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633496.aspx">
    ''' EnumThreadWndProc callback function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633496.aspx </a>
    ''' </para>
    ''' </remarks>
    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="EnumThreadWindows")>
    Private Function EnumThreadWindows(ByVal dwThreadId As IntPtr, ByVal lpfn As EnumThreadDelegate, ByVal lParam As IntPtr) As IntPtr
    End Function

    ''' <summary>
    ''' Retrieves the name of the class to which the specified window belongs.
    ''' </summary>
    ''' <param name="hWnd">A handle to the window and, indirectly, the class to which the window belongs.</param>
    ''' <param name="lpClassName">The class name string.</param>
    ''' <param name="nMaxCount">The maximum number of characters to copy to the buffer, including the null character. If the text exceeds this limit, it is truncated. The buffer must be large enough to include the terminating null character; otherwise, the class name string is truncated to nMaxCount-1 characters.</param>
    ''' <returns>IntPtr</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633582.aspx">
    ''' GetClassName function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633582.aspx </a>
    ''' </para>
    ''' </remarks>
    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="GetClassNameA")>
    Friend Function GetClassNameA(ByVal hWnd As IntPtr, ByVal lpClassName As System.Text.StringBuilder, ByVal nMaxCount As System.Int32) As IntPtr
    End Function

    ''' <summary>
    ''' Copies the text of the specified window's title bar (if it has one) into a buffer.
    ''' </summary>
    ''' <param name="hWnd">A handle to the window or control.</param>
    ''' <param name="lpString">The buffer that will receive the text. If the string is as long or longer than the buffer, the string is truncated and terminated with a null character.</param>
    ''' <param name="nMaxCount">The maximum number of characters to copy to the buffer, including the null character. If the text exceeds this limit, it is truncated. The buffer must be large enough to include the terminating null character; otherwise, the class name string is truncated to nMaxCount-1 characters.</param>
    ''' <returns>IntPtr</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633520.aspx">
    ''' GetWindowText function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633520.aspx </a>
    ''' </para>
    ''' </remarks>
    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="GetWindowTextA")>
    Friend Function GetWindowTextA(ByVal hWnd As IntPtr, ByVal lpString As System.Text.StringBuilder, ByVal nMaxCount As System.Int32) As IntPtr
    End Function

    ''' <summary>
    ''' Retrieves the length, in characters, of the specified window's title bar text.
    ''' </summary>
    ''' <param name="hWnd">A handle to the window or control.</param>
    ''' <returns>IntPtr</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633521.aspx">
    ''' GetWindowTextLength function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633521.aspx </a>
    ''' </para>
    ''' </remarks>
    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="GetWindowTextLengthA")>
    Friend Function GetWindowTextLengthA(ByVal hWnd As IntPtr) As IntPtr
    End Function

    '#Region " Windows Testing "

    '    ''' <summary>
    '    ''' Creates an overlapped, pop-up, or child window.
    '    ''' </summary>
    '    ''' <param name="lpClassName"></param>
    '    ''' <param name="lpWindowName"></param>
    '    ''' <param name="dwStyle"></param>
    '    ''' <param name="x"></param>
    '    ''' <param name="y"></param>
    '    ''' <param name="nWidth"></param>
    '    ''' <param name="nHeight"></param>
    '    ''' <param name="hWndParent"></param>
    '    ''' <param name="hMenu"></param>
    '    ''' <param name="hInstance"></param>
    '    ''' <param name="lpParam"></param>
    '    ''' <returns>IntPtr</returns>
    '    ''' <remarks>
    '    ''' <para>
    '    ''' <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms632679.aspx">
    '    ''' CreateWindowA function: http://msdn.microsoft.com/en-us/library/windows/desktop/ms632679.aspx </a>
    '    ''' </para>
    '    ''' </remarks>
    '    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="CreateWindowA")>
    '    Public Function CreateWindowA(ByVal lpClassName As System.Text.StringBuilder, ByVal lpWindowName As System.Text.StringBuilder,
    '                                  ByVal dwStyle As IntPtr,
    '                                  ByVal x As System.Int32, ByVal y As System.Int32, ByVal nWidth As System.Int32, ByVal nHeight As System.Int32,
    '                                  ByVal hWndParent As IntPtr, ByVal hMenu As IntPtr, ByVal hInstance As IntPtr, ByVal lpParam As IntPtr) As IntPtr
    '    End Function

    '    <DllImport("user32", BestFitMapping:=False, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Ansi, ExactSpelling:=True, PreserveSig:=True, SetLastError:=True, ThrowOnUnmappableChar:=True, EntryPoint:="RegisterClassExA")>
    '    Public Function RegisterClassExA(ByVal lpwcx As IntPtr) As IntPtr
    '    End Function

    '#End Region

#End Region

    ''' <summary>
    ''' A handle to the window or control.
    ''' </summary>
    ''' <remarks>Intermediate IntPtr for transferring the value from EnumThreadWndProc to ReturnWindowHandle</remarks>
    Private h As IntPtr

    ''' <summary>
    ''' The class name for the window or control.
    ''' </summary>
    ''' <remarks></remarks>
    Private cNameDefined As String = Nothing

    ''' <summary>
    ''' The title bar caption for the window or control.
    ''' </summary>
    ''' <remarks></remarks>
    Private tNameDefined As String = Nothing

    ''' <summary>
    ''' Delegate function for EnumThreadWndProc.
    ''' </summary>
    ''' <param name="hWnd">A handle to the window or control.</param>
    ''' <param name="lParam"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Delegate Function EnumThreadDelegate(ByVal hWnd As IntPtr, ByVal lParam As IntPtr) As Boolean

    ''' <summary>
    ''' Returns the first window handle for the specidied ThreadID that matches the class neme (cls) and caption(txt).
    ''' </summary>
    ''' <param name="ThreadId">The identifier of the thread whose windows are to be enumerated.</param>
    ''' <param name="cls">Class name of the window or control.</param>
    ''' <param name="txt">Caption text of the window or control.</param>
    ''' <returns>IntPtr</returns>
    ''' <remarks></remarks>
    Public Function ReturnWindowHandle(ByVal ThreadId As IntPtr, ByVal cls As String, ByVal txt As String) As IntPtr

        Sa.Core.WindowHandles.h = IntPtr.Zero

        Sa.Core.WindowHandles.cNameDefined = cls
        Sa.Core.WindowHandles.tNameDefined = txt

        Sa.Core.WindowHandles.EnumThreadWindows(ThreadId, AddressOf EnumThreadWndProc, IntPtr.Zero)

        Return h

    End Function

    ''' <summary>
    ''' Compares propeties of a window or control.
    ''' </summary>
    ''' <param name="hWnd">A handle to the window or control.</param>
    ''' <param name="lParam"></param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Function EnumThreadWndProc(ByVal hWnd As IntPtr, ByVal lParam As IntPtr) As Boolean

        Dim r As Boolean = True

        Dim tLen As System.Int32 = CType(Sa.Core.WindowHandles.GetWindowTextLengthA(hWnd), System.Int32)
        Dim tStringBuilder As New System.Text.StringBuilder("", tLen + 1)
        Sa.Core.WindowHandles.GetWindowTextA(hWnd, tStringBuilder, tStringBuilder.Capacity)

        Dim cLen As System.Int32 = 256
        Dim cStringBuilder As New System.Text.StringBuilder("", cLen + 1)
        Sa.Core.WindowHandles.GetClassNameA(hWnd, cStringBuilder, cStringBuilder.Capacity)

        Dim cNameFound As String = cStringBuilder.ToString
        Dim tNameFound As String = tStringBuilder.ToString

        If Sa.Core.WindowHandles.cNameDefined = cNameFound AndAlso Sa.Core.WindowHandles.tNameDefined = tNameFound Then
            Sa.Core.WindowHandles.h = hWnd
            r = False
        End If

        Return r

    End Function

End Module
