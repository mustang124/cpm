﻿
''' <summary>
''' Return information about the host computer performance properties.
''' </summary>
''' <remarks>
''' <para>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </para>
''' <para>
''' Programmers should use this library to access host computer information.
''' </para>
''' <para>
''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.aspx">
''' ComputerInfo Class: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.aspx </a>
''' </para>
''' <para>
''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computer.aspx">
''' Computer Class: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computer.aspx </a>
''' </para>
''' <para>
''' <a href="http://msdn.microsoft.com/en-us/library/y5htx827.aspx">
''' Thread Pool Class: http://msdn.microsoft.com/en-us/library/y5htx827.aspx </a>
''' </para>
''' </remarks>
Public Module Performance

    ''' <summary>
    ''' Gets the full operating system name. 
    ''' </summary>
    ''' <value>A String containing the operating-system name.</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' This property returns detailed information about the operating system name if Windows Management Instrumentation (WMI) is installed on the computer. Otherwise, this property returns the same string as the My.Computer.Info.OSPlatform property, which provides less detailed information than WMI can provide.
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.osfullname.aspx">
    ''' ComputerInfo.OSFullName Property: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.osfullname.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property OSFullName As String
        Get

            Dim s As String

            Try
                s = My.Computer.Info.OSFullName
            Catch ex As Security.SecurityException
                s = "Could not retrieve OS Name."
            End Try

            Return s

        End Get
    End Property

    ''' <summary>
    ''' Gets the platform identifier for the operating system of the computer.
    ''' </summary>
    ''' <value>A String containing the platform identifier for the operating system of the computer, chosen from the member names of the PlatformID enumeration.</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.osplatform.aspx">
    ''' ComputerInfo.OSPlatform Property: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.osplatform.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property OSPlatform As String
        Get

            Dim s As String

            Try
                s = My.Computer.Info.OSPlatform
            Catch ex As Exception
                s = "Unknown OS platform"
            End Try

            Return s

        End Get
    End Property

    ''' <summary>
    ''' Gets the version of the computer's operating system.
    ''' </summary>
    ''' <value>A String containing the current version number of the operating system.</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' The My.Computer.Info.OSVersion property formats the version as "major.minor.build.revision". 
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.osversion.aspx">
    ''' ComputerInfo.OSVersion Property: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.osversion.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property OSVersion As String
        Get

            Dim s As String

            Try
                s = My.Computer.Info.OSVersion
            Catch ex As Exception
                s = "Unknown OS version"
            End Try

            Return s

        End Get
    End Property

    ''' <summary>
    ''' Gets the NetBIOS name of the host computer.
    ''' </summary>
    ''' <value>A string containing the name of this computer.</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' The name of this computer is established at system startup when the name is read from the registry. If this computer is a node in a cluster, the name of the node is returned.
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.servercomputer.name.aspx">
    ''' ServerComputer.Name Property: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.servercomputer.name.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property MachineName As String
        Get

            Dim s As String

            Try
                s = My.Computer.Name
            Catch ex As Security.SecurityException
                s = "Could not retrieve machine name."
            End Try

            Return s

        End Get
    End Property

    ''' <summary>
    ''' Gets the screen resolution of the main display.
    ''' </summary>
    ''' <value>A string representing the resolution of the main display.</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.drawing.rectangle.aspx">
    ''' Rectangle Structure: http://msdn.microsoft.com/en-us/library/system.drawing.rectangle.aspx </a>
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.windows.forms.screen.bounds.aspx">
    ''' Screen.Bounds Property: http://msdn.microsoft.com/en-us/library/system.windows.forms.screen.bounds.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property ScreenResolution As String
        Get

            Dim s As String

            Try

                Dim r As System.Drawing.Rectangle
                r = My.Computer.Screen.Bounds

                ' × - Character Code 0215
                s = CStr(r.Width) + " × " + CStr(r.Height)

            Catch ex As Security.SecurityException
                s = "Could not retrieve screen resolution."
            End Try

            Return s

        End Get
    End Property

    ''' <summary>
    ''' Gets the number of bits required to make color.
    ''' </summary>
    ''' <value>String representing the number of bits per pixel.</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.windows.forms.screen.bitsperpixel.aspx">
    ''' Screen.BitsPerPixel Property: http://msdn.microsoft.com/en-us/library/system.windows.forms.screen.bitsperpixel.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property ScreenDepth As String
        Get

            Dim s As String

            Try
                s = My.Computer.Screen.BitsPerPixel.ToString(System.Globalization.NumberFormatInfo.InvariantInfo)
            Catch ex As Security.SecurityException
                s = "Could not retrieve color depth."
            End Try

            Return s

        End Get
    End Property

    ''' <summary>
    ''' Gets the total amount of free physical memory for the computer in GB.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Single</returns>
    ''' <remarks>
    ''' <para>This API is not CLS-compliant.</para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.availablephysicalmemory.aspx">
    ''' ComputerInfo.AvailablePhysicalMemory Property http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.availablephysicalmemory.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property AvailablePhysicalMemory As Single
        Get
            Return CType(My.Computer.Info.AvailablePhysicalMemory / 1073741824.0, Single)
        End Get
    End Property

    ''' <summary>
    ''' Gets the total amount of total physical memory for the computer in GB.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Single</returns>
    ''' <remarks>
    ''' <para>This API is not CLS-compliant.</para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.totalphysicalmemory.aspx">
    ''' ComputerInfo.TotalPhysicalMemory Property: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.totalphysicalmemory.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property TotalPhysicalMemory As Single
        Get
            Return CType(My.Computer.Info.TotalPhysicalMemory / 1073741824.0, Single)
        End Get
    End Property

    ''' <summary>
    ''' Gets the percent of physical memory available for the computer in GB.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Single</returns>
    ''' <remarks>
    ''' <para>This API is not CLS-compliant.</para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.availablephysicalmemory.aspx">
    ''' ComputerInfo.AvailablePhysicalMemory Property: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.availablephysicalmemory.aspx </a>
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.totalphysicalmemory.aspx">
    ''' ComputerInfo.TotalPhysicalMemory Property: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.devices.computerinfo.totalphysicalmemory.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property PercentAvailableMemory As Single
        Get
            Return CType(My.Computer.Info.AvailablePhysicalMemory / My.Computer.Info.TotalPhysicalMemory, Single)
        End Get
    End Property

    ''' <summary>
    ''' Gets the number of processors on the current machine.
    ''' </summary>
    ''' <value>Number of processors (cores).</value>
    ''' <returns>System.Int32 that specifies the number of processors on the current machine. There is no default.</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.environment.processorcount.aspx">
    ''' Environment.ProcessorCount Property http://msdn.microsoft.com/en-us/library/system.environment.processorcount.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property ProcessorCount As System.Int32
        Get
            Return System.Environment.ProcessorCount
        End Get
    End Property

    ''' <summary>
    ''' Retrieves the number of requests to the thread pool that can be active concurrently. All requests above that number remain queued until thread pool threads become available.
    ''' </summary>
    ''' <value></value>
    ''' <returns>System.Int32</returns>
    ''' <remarks>
    ''' <para>Used to return the number of threads available for a process to use.</para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.threading.threadpool.getmaxthreads.aspx">
    ''' ThreadPool.GetMaxThreads Method: http://msdn.microsoft.com/en-us/library/system.threading.threadpool.getmaxthreads.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property WorkerThreads() As System.Int32
        Get

            'Dim wt As System.Int32 ' The maximum number of worker threads in the thread pool.
            'Dim pt As System.Int32 ' The maximum number of asynchronous I/O threads in the thread pool.

            'System.Threading.ThreadPool.GetMaxThreads(wt, pt)

            Return 10 'wt

        End Get
    End Property

    ''' <summary>
    ''' Gets the number of asynchronous I/O threads in the thread pool.
    ''' </summary>
    ''' <value></value>
    ''' <returns>System.Int32</returns>
    ''' <remarks>
    ''' <para>Used to return the number of asynchronous I/O threads available for a process to use.</para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.threading.threadpool.getmaxthreads.aspx">
    ''' ThreadPool.GetMaxThreads Method: http://msdn.microsoft.com/en-us/library/system.threading.threadpool.getmaxthreads.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property PortThreads() As System.Int32
        Get

            Dim wt As System.Int32 ' The maximum number of worker threads in the thread pool.
            Dim pt As System.Int32 ' The maximum number of asynchronous I/O threads in the thread pool.

            System.Threading.ThreadPool.GetMaxThreads(wt, pt)

            Return pt

        End Get
    End Property

End Module
