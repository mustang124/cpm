﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Sa.Core.Core")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("HSB Solomon Associates LLC")> 
<Assembly: AssemblyProduct("Sa.Core.Core")> 
<Assembly: AssemblyCopyright("© 2013 HSB Solomon Associates LLC")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("Test.Core, PublicKey=" +
    "00240000048000001401000006020000002400005253413100080000010001002176a2faa49b2438d58" +
    "05c95ea67802190f85a7c5766dd7737c637737618898a77e8d6f09529c2ba9e3891fc693755a169ab27" +
    "05a3db484d583fb28bedf24e632a9e465b57ccd9300b0944f45a90339c9a49e5cd51ba3d714f4534128" +
    "098eded22c3e4743f75174a7d2e548d92e668766baec64fc5f8f262b4db9323fbcf79cceb362d725a0f" +
    "2b957afa34c17547ca7652087aea3abf130cec6721124f48ad012612c1d00b24e7845da06083d085a51" +
    "88a1b4587c56fba2c24ebddaa57804e39e96aec9ceed17e4d91b632b8f86a127ec17cec498c035a5463" +
    "ef9e3be38badbd3a2ee8b766ad1841b304bd405e35ae746e5c87f836ac68fcbe1e09cfec2640e3")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("08929a22-77ae-4421-ba32-4f706cf71512")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
