﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
    public class Report
    {
        readonly Deliverable deliverable;
        readonly Submission submission;

        StyleFont StyleFontH0 = new StyleFont();
        StyleFont StyleFontH1 = new StyleFont();
        StyleFont StyleFontH2 = new StyleFont();

        StyleFont StyleFontEnt = new StyleFont("Ent");
        StyleFont StyleFontAtt = new StyleFont("Att");

        string pathFile = @".\Report\IssReport.xlsx";

        public Report(Deliverable deliverable, Submission submission)
        {
            this.deliverable = deliverable;
            this.submission = submission;
        }

        public void Create(string reportFilePath)
        {
            DateTime dtBeg = DateTime.Now;

            Excel.Application xla = XL.NewExcelApplication(true);
            Excel.Workbook wkb = XL.OpenWorkbook_ReadOnly(xla, reportFilePath);// this.pathFile);

            using (SqlConnection cn = new SqlConnection(Sa.Common.cnString()))
            {
                cn.Open();

                MakeReport(cn, wkb, 1, this.deliverable, this.submission);

                cn.Close();
            }

            xla.CalculateFullRebuild();

            //string fileName = @"C:\Users\RRH\Desktop\" + this.submission.nameFile + ".xlsx";
            bool deletePriorReportFile=true;
            if (!Boolean.TryParse(ConfigurationManager.AppSettings["DeletePriorReportFile"].ToString(),
                out deletePriorReportFile) || !deletePriorReportFile)
            {
                System.IO.File.Delete(reportFilePath);//fileName);
                wkb.SaveAs(reportFilePath);//fileName);
            }
            else
            {
                wkb.SaveAs(reportFilePath + "1");
            }
            

            wkb.SaveAs(reportFilePath);//fileName);

            DateTime dtEnd = DateTime.Now;
            System.Windows.Forms.MessageBox.Show("DONE" + "\n\r" + dtBeg.ToString() + "\n\r" + dtEnd.ToString());
        }

        void MakeReport(SqlConnection cn, Excel.Workbook wkb, int queueId, Deliverable deliverable, Submission submission)
        {
            using (SqlCommand cmd = new SqlCommand("[rpt].[Select_DeliverablesContent]", cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                //@DeliverableId		INT
                cmd.Parameters.Add("@DeliverableId", SqlDbType.Int).Value = deliverable.Id;

                //using (SqlDataReader rdr = cmd.ExecuteReader())
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    cn.Close();
                    //while (rdr.Read())//gives already an open datareader error in AddData()
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        int ItemNumber = (int)dr["ItemNumber"];
                        int ContentId = (int)dr["ContentId"];

                        this.AddContent(cn, wkb, ContentId, deliverable, submission);
                    }
                }
            }
        }

        void AddContent(SqlConnection cn, Excel.Workbook wkb, int contentId, Deliverable deliverable, Submission submission)
        {
            Excel.Worksheet wks;
            Excel.Range rng;
            int rowOffset = 5;
            int row = rowOffset;
            if (cn.State == ConnectionState.Open)
                cn.Close();
            if (cn.State != ConnectionState.Open)
                cn.Open();

            using (SqlCommand cmd = new SqlCommand("[rpt].[Select_ContentName]", cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ContentId", SqlDbType.Int).Value = contentId;

                string contentName = (string)cmd.ExecuteScalar();

                wks = wkb.Worksheets[contentName];
            }

            using (SqlCommand cmd = new SqlCommand("[rpt].[Select_ContentAnnotations]", cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ContentId", SqlDbType.Int).Value = contentId;

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        row++;

                        int level = rdr.GetInt32(rdr.GetOrdinal("AnnotationsLevelId"));
                        string label = rdr.GetString(rdr.GetOrdinal("AnnotationsTextTag"));

                        rng = wks.Cells[rowOffset + level, 1];

                        switch (level)
                        {
                            case 1:
                                StyleFontH0.styleId = rdr.GetInt32(rdr.GetOrdinal("AnnotationLevelStyleId"));
                                rng.Font.Size = StyleFontH0.Size;
                                rng.Font.Bold = StyleFontH0.Bold;

                                rng.Value = label;
                                break;

                            case 2:
                                StyleFontH1.styleId = rdr.GetInt32(rdr.GetOrdinal("AnnotationLevelStyleId"));
                                rng.Font.Size = StyleFontH1.Size;
                                rng.Font.Bold = StyleFontH1.Bold;

                                rng.Value = label + " - " + submission.nameDate;
                                break;

                            case 3:
                                StyleFontH2.styleId = rdr.GetInt32(rdr.GetOrdinal("AnnotationLevelStyleId"));
                                rng.Font.Size = StyleFontH2.Size;
                                rng.Font.Bold = StyleFontH2.Bold;

                                rng.Value = submission.companyDetail ?? submission.facilityDetail ?? submission.groupDetail;
                                break;
                        }
                    }
                }
            }
            AddSections(cn, contentId, wks, row, deliverable, submission);
        }

        void AddSections(SqlConnection cn, int contentId, Excel.Worksheet wks, int rowOffset, Deliverable deliverable, Submission submission)
        {
            using (SqlCommand cmd = new SqlCommand("[rpt].[Select_ContentSection]", cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ContentId", SqlDbType.Int).Value = contentId;


                //using (SqlDataReader rdr = cmd.ExecuteReader())//gives already an open datareader error in AddData()
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    cn.Close();
                    //while (rdr.Read())//gives already an open datareader error in AddData()
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        /*
						int ContentSectionId = rdr.GetInt32(rdr.GetOrdinal("ContentSectionId"));
						int SectionNumber = rdr.GetInt32(rdr.GetOrdinal("SectionNumber"));
						int DataSourceId = rdr.GetInt32(rdr.GetOrdinal("DataSourceId"));
                         */
                        int ContentSectionId = (int)(dr["ContentSectionId"]);
                        int SectionNumber = (int)(dr["SectionNumber"]);
                        int DataSourceId = (int)(dr["DataSourceId"]);

                        rowOffset = this.AddData(cn, DataSourceId, wks, rowOffset + 2, deliverable, submission);
                    }
                }
            }
        }

        int AddData(SqlConnection cn, int dataSourceId, Excel.Worksheet wks, int rowOffset, Deliverable deliverable, Submission submission)
        {
            string tvfName;
            if (cn.State == ConnectionState.Open)
                cn.Close();
            if (cn.State == ConnectionState.Closed)
                cn.Open();
            using (SqlCommand cmd = new SqlCommand("[rpt].[Select_DataSourceTvf]", cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DataSourceId", SqlDbType.Int).Value = dataSourceId;

                tvfName = (string)cmd.ExecuteScalar();
            }

            Attributes eaAttribtues = new Attributes();
            Entities eaEntities = new Entities();
            EAValues eaValues = new EAValues();

            int EntityId;
            int AttributeId;
            int SortKey;
            string Name;
            cn.Close();
            cn.Open();
            using (SqlCommand cmd = new SqlCommand(tvfName, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;
                cmd.Parameters.Add("@MethodologyId", SqlDbType.Int).Value = deliverable.MethodologyId;
                cmd.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = deliverable.CurrencyId;

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        EntityId = rdr.GetInt32(rdr.GetOrdinal("EntityId"));
                        SortKey = rdr.GetInt32(rdr.GetOrdinal("EntitySortKey"));
                        Name = rdr.GetString(rdr.GetOrdinal("EntityDetail"));

                        Item ent = new Item(EntityId, SortKey, Name);
                        if (!eaEntities.Contains(ent)) eaEntities.Add(ent);

                        ///////////////////

                        AttributeId = rdr.GetInt32(rdr.GetOrdinal("AttributeId"));
                        SortKey = rdr.GetInt32(rdr.GetOrdinal("AttributeSortKey"));
                        Name = rdr.GetString(rdr.GetOrdinal("AttributeDetail"));

                        Item att = new Item(AttributeId, SortKey, Name);
                        if (!eaAttribtues.Contains(att)) eaAttribtues.Add(att);

                        ///////////////////

                        if (!rdr.IsDBNull(rdr.GetOrdinal("Value")))
                        {
                            string v = rdr.GetString(rdr.GetOrdinal("Value"));
                            EAValue val = new EAValue(EntityId, AttributeId, v);
                            if (!eaValues.Contains(val)) eaValues.Add(val);
                        }
                    }
                }

                eaAttribtues.Sort();
                eaEntities.Sort();

                int rInit = rowOffset;
                int cInit = 1;

                Excel.Range rngInit = wks.Cells[rInit, cInit];
                Excel.Range rngAtt;
                Excel.Range rngEnt;
                Excel.Range rngVal;

                int r = rngInit.Row;
                int c = rngInit.Column;
                EAValue eaValue;

                foreach (Item iAtt in eaAttribtues)
                {
                    rngAtt = wks.Cells[rInit, ++c];
                    rngAtt.Font.Size = StyleFontAtt.Size;
                    rngAtt.Font.Bold = StyleFontAtt.Bold;

                    rngAtt.Value = iAtt.Name;

                    r = rngInit.Row;
                    foreach (Item iEnt in eaEntities)
                    {
                        r++;
                        if (iAtt.Equals(eaAttribtues.First()))
                        {
                            rngEnt = wks.Cells[r, cInit];
                            rngEnt.Font.Size = StyleFontEnt.Size;
                            rngEnt.Font.Bold = StyleFontEnt.Bold;

                            rngEnt.Value = iEnt.Name;
                        }

                        eaValue = eaValues.Find(x => x.EntityId == iEnt.Id && x.AttributeId == iAtt.Id);

                        if (eaValue != null)
                        {
                            rngVal = wks.Cells[r, c];
                            rngVal.Value = eaValue.Value;

                            eaValues.Remove(eaValue);
                        }
                    }
                }

                rowOffset = r + 1;
            }
            return rowOffset;
        }
    }

    class EAValue
    {
        public readonly int EntityId;
        public readonly int AttributeId;
        public readonly string Value;

        public EAValue(int entityId, int attributeId, string value)
        {
            this.EntityId = entityId;
            this.AttributeId = attributeId;
            this.Value = value;
        }

        public bool Equals(EAValue other)
        {
            if (other == null) return false;
            return (this.EntityId.Equals(other.EntityId) && this.AttributeId.Equals(other.AttributeId));
        }
    }

    class EAValues : List<EAValue>
    {
    }

    class Item : IComparable<Item>, IEquatable<Item>
    {
        public readonly int Id;
        public readonly int SortKey;
        public readonly string Name;

        public Item(int id, int sortKey, string name)
        {
            this.Id = id;
            this.SortKey = sortKey;
            this.Name = name;
        }

        public int CompareTo(Item compareItem)
        {
            // A null value means that this object is greater. 
            if (compareItem == null)
            {
                return 1;
            }
            else
            {
                return this.SortKey.CompareTo(compareItem.SortKey);
            }
        }

        public bool Equals(Item other)
        {
            if (other == null) return false;
            return (this.Id.Equals(other.Id) && this.SortKey.Equals(other.SortKey));
        }
    }

    class Attributes : List<Item>
    {
    }

    class Entities : List<Item>
    {
    }
}