﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Sa
{
	public class Common
	{
		public static string GetDateTimeStamp()
		{
			return DateTime.Now.ToString("yyyyMMdd.HHmmss");
		}

		public static string cnString()
		{
            string sv = @"10.10.27.45";
            string db = @"NogaHolding";

            string cn = "Server = " + sv + ";";
            cn = cn + "Database = " + db + ";";
            //cn = cn + "Trusted_Connection = True;";
            cn = cn + "MultipleActiveResultSets = True;";
            cn = cn + "Asynchronous Processing = True;";
            cn = cn + "User ID=SBARD;";
            cn = cn + "Password=Stuart2015;";
            cn = cn + "Application Name = " + ProdNameVer() + ";";

            return @"Data Source=(localdb)\ProjectsV12;Initial Catalog=dbs.Iss.NogaHolding;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            //return @"Data Source=(localdb)\ProjectsV12;Initial Catalog=dbs.ProfileFuels12;Integrated Security=True;Pooling=False;Connect Timeout=30";
			return cn;
		}

		public static string dbConnectionString(string dbInst, string dbName, string UserName, string UserPass)
		{
			string cn = "Data Source=" + dbInst + ";";
			cn = cn + "Initial Catalog=" + dbName + ";";
			cn = cn + "User Id=" + UserName + ";";
			cn = cn + "Password=" + UserPass + ";";

			cn = cn + "Application Name = " + ProdNameVer() + ";";

			return cn;
		}

		// HACK: set Version number  = SELECT DATEDIFF(d, '1/1/2000', SYSDATETIME())
		private const string ver = "5507";

		public static string ProdNameVer()
		{
			string rtn = string.Empty;

			try
			{
				Assembly ai = Assembly.GetExecutingAssembly();
				rtn = ai.GetName().Name + " (" + ai.GetName().Version + ")";
			}
			catch
			{
				rtn = "Chem.Upload.Cl 1.0." + ver + ".x";
			}

			return rtn;

		}

		public static string ProdVer()
		{
			string rtn = string.Empty;

			try
			{
				Assembly ai = Assembly.GetExecutingAssembly();
				rtn = Convert.ToString(ai.GetName().Version);
			}
			catch
			{
				rtn = "1.0." + ver + ".x";
			}

			return rtn;
		}
	}
}