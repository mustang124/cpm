﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Iss
{
	class ErrorHandler
	{
		private static string CellAddress(int r, int c)
		{
			string l = String.Empty;
			int m;

			while (c > 0)
			{
				m = (c - 1) % 26;
				l = Convert.ToChar(65 + m).ToString() + l;
				c = ((c - m) / 26);
			}

			return l + r.ToString();
		}

		public static List<string> EtlErrors = new List<string>();

		public static void Insert_UpLoadError(Queue.Upload queue, string FileType, string MethodName, string DataSetIdentifier,
			Excel.Workbook wkb, Excel.Worksheet wks, Excel.Range rng, int row, int col,
			string SqlProcedureName, Exception ex)
		{

			SqlConnection cn = new SqlConnection(Common.cnString());
			cn.Open();

			SqlCommand cmd = new SqlCommand("[audit].[Insert_UploadError]", cn);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@UploadQueueId", SqlDbType.Int).Value = queue.Id;
			cmd.Parameters.Add("@FileType", SqlDbType.NVarChar, 32).Value = FileType;
			cmd.Parameters.Add("@DataSetIdentifier", SqlDbType.NVarChar, 48).Value = DataSetIdentifier;
			cmd.Parameters.Add("@FilePath", SqlDbType.NVarChar, 512).Value = wkb.Path + "\\" + wkb.Name;

			#region Sheet

			string Sheet = string.Empty;

			if (Sheet != null)
			{
				Sheet = wks.Name;
			}
			else
			{
				Sheet = "Worksheet not initialized";
			};

			cmd.Parameters.Add("@Sheet", SqlDbType.VarChar, 128).Value = Sheet;

			#endregion

			#region Range

			string Range = string.Empty;

			if (Range != null)
			{
				Range = rng.AddressLocal;
				if (!string.IsNullOrEmpty(rng.Text)) { cmd.Parameters.Add("@CellText", SqlDbType.NVarChar, 128).Value = rng.Text; }

			}
			else
			{
				Range = "Range not initialized (" + Sheet + "!" + CellAddress(row, col) + ")";
			};

			cmd.Parameters.Add("@Range", SqlDbType.VarChar, 128).Value = Range;

			#endregion

			cmd.Parameters.Add("@ProcedureName", SqlDbType.NVarChar, 128).Value = SqlProcedureName;
			cmd.Parameters.Add("@SystemErrorMessage", SqlDbType.NVarChar, 128).Value = ex.Message;

			cmd.ExecuteNonQuery();

			cn.Close();

			AddToEtlList(MethodName, wks, rng, row, col, ex);
		}

		public static void AddToEtlList(string MethodName, Excel.Worksheet wks, Excel.Range rng, int row, int col, Exception ex)
		{
			string MessageBody = "Module: " + MethodName + "\r\n";
			MessageBody = MessageBody + "Range: " + wks.Name + "!" + CellAddress(row, col) + "\r\n";

			if (rng == null)
			{
				MessageBody = MessageBody + "Address Range failed to initialize: " + wks.Name + "!" + CellAddress(row, col) + " (s= " + wks.Name + ", r= " + row.ToString() + ", c= " + col.ToString() + ")\r\n\r\n";
			}
			else
			{
				MessageBody = MessageBody + "Cell Text: " + Convert.ToString(rng.Text) + "\r\n\r\n";
			}

			EtlErrors.Add(MessageBody + ex.Message);
		}

		public static string AggregateMessage()
		{
			string msg = "";

			foreach (string s in EtlErrors)
			{
				if (msg != "") { msg = msg + "\r\n\r\n"; };
				msg = msg + s;
			}

			return msg;
		}

		//public static void Delete_UploadErrors(string FileType, string Refnum)
		//{
		//	string sRefnum = Common.GetRefnumSmall(Refnum);

		//	SqlConnection cn = new SqlConnection(Common.cnString());
		//	cn.Open();

		//	SqlCommand cmd = new SqlCommand("[stgFact].[Delete_UploadErrors]", cn);
		//	cmd.CommandType = CommandType.StoredProcedure;

		//	cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 20).Value = FileType;
		//	cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = sRefnum;

		//	cmd.ExecuteNonQuery();

		//	cn.Close();
		//}
	}
}
