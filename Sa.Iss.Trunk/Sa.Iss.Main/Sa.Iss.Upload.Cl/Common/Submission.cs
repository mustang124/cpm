﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Iss
{
	public class Queue
	{
		public class Upload : QueueItem
		{
			public DateTimeOffset UploadTime;
		}

		public abstract class QueueItem
		{
			public int Id;
		}
	}

	public class Submission
	{
		public int Id;

		public string name;
		public string nameDate;
		public DateTime periodBeg;
		public DateTime periodEnd;
		public int durationDays;

		public string groupTag;
		public string groupName;
		public string groupDetail;

		public string facilityTag;
		public string facilityName;
		public string facilityDetail;

		public string companyTag;
		public string companyName;
		public string companyDetail;

        public int deliverableId;

		public string nameFile
		{
			get
			{
				return companyTag + "-" + facilityTag + " (" + nameDate + ")";
			}
		}

		public void GetFactSubmission(int id)
		{
			this.GetSubmission(id, "[fact].[Select_SubmissionInfo]");
		}

		private void GetSubmission(int id, string procedure)
		{
			using (SqlConnection cn = new SqlConnection(Sa.Common.cnString()))
			{
				using (SqlCommand cmd = new SqlCommand(procedure, cn))
				{
					cmd.CommandType = CommandType.StoredProcedure;

					//@SubmissionId						INT
					cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = id;

					cn.Open();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						int c;

						while (rdr.Read())
						{
							this.Id = id;

							this.name = rdr.GetString(rdr.GetOrdinal("SubmissionName"));
                            //SubmissionDate col does not exist:  this.nameDate = rdr.GetString(rdr.GetOrdinal("SubmissionDate"));

							this.periodBeg = rdr.GetDateTime(rdr.GetOrdinal("PeriodBeg"));
							this.periodEnd = rdr.GetDateTime(rdr.GetOrdinal("PeriodEnd"));
							this.durationDays = rdr.GetInt32(rdr.GetOrdinal("Duration_Days"));

							this.groupTag = rdr.GetString(rdr.GetOrdinal("GroupTag"));
							this.groupName = rdr.GetString(rdr.GetOrdinal("GroupName"));
							this.groupDetail = rdr.GetString(rdr.GetOrdinal("GroupDetail"));

							c = rdr.GetOrdinal("FacilityTag");
							if (!rdr.IsDBNull(c)) this.facilityTag = rdr.GetString(c);

							c = rdr.GetOrdinal("FacilityName");
							if (!rdr.IsDBNull(c)) this.facilityName = rdr.GetString(c);

							c = rdr.GetOrdinal("FacilityDetail");
							if (!rdr.IsDBNull(c)) this.facilityDetail = rdr.GetString(c);

							c = rdr.GetOrdinal("CompanyTag");
							if (!rdr.IsDBNull(c)) this.companyTag = rdr.GetString(c);

							c = rdr.GetOrdinal("CompanyName");
							if (!rdr.IsDBNull(c)) this.companyName = rdr.GetString(c);

							c = rdr.GetOrdinal("CompanyDetail");
							if (!rdr.IsDBNull(c)) this.companyDetail = rdr.GetString(c);

                            c = rdr.GetOrdinal("DeliverableId");
                            if (!rdr.IsDBNull(c)) this.deliverableId = rdr.GetInt32(c);
						}
					}
					cn.Close();
				}
			}
		}

	}

	public class Deliverable
	{
		public int Id;

		public string Name;
		public int MethodologyId;
		public int CurrencyId;
	}

	public class StyleFont
	{
		public int styleId;
		public string styleTag;

		private int fontId;
		private int size;
		private bool bold;

		public StyleFont()
		{
		}

		public StyleFont(int styleId)
		{
			this.styleId = styleId;
		}

		public StyleFont(string styleTag)
		{
			this.styleTag = styleTag;
		}

		public Int32 Size
		{
			get
			{
				if (this.fontId == 0)
				{
					GetFontStyle();
				}

				return Math.Max(this.size, 1);
			}
			set
			{
				this.size = value;
			}
		}

		public bool Bold
		{
			get
			{
				if (this.fontId == 0)
				{
					GetFontStyle();
				}

				return this.bold;
			}
			set
			{
				this.bold = value;
			}
		}

		private void GetFontStyle()
		{
			using (SqlConnection cn = new SqlConnection(Sa.Common.cnString()))
			{
				using (SqlCommand cmd = new SqlCommand("[rpt].[Select_Style]", cn))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					SqlParameter p;

					//@SubmissionId					INT
					p = cmd.Parameters.Add("@StyleId", SqlDbType.Int);
					if(this.styleId != 0) p.Value = this.styleId;

					//@StyleTag						VARCHAR(12)
					p = cmd.Parameters.Add("@StyleTag", SqlDbType.VarChar, 12);
					if (this.styleTag != string.Empty && this.styleTag != null) p.Value = this.styleTag;

					cn.Open();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						int c;

						while (rdr.Read())
						{
							c = rdr.GetOrdinal("StyleFontId");
							this.fontId = rdr.GetInt32(c);

							c = rdr.GetOrdinal("FontSize");
							if (!rdr.IsDBNull(c)) this.size = rdr.GetInt32(c);

							c = rdr.GetOrdinal("FontBold");
							if (!rdr.IsDBNull(c)) this.bold = rdr.GetBoolean(c);
						}
					}
					cn.Close();
				}
			}
		}
	}
}