﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Iss.Upload.Cl.Calcs
{
    public static class AirportCalcs
    {
        public static Single EDCStandardEnergyCost(Single electricalRequirements, Single electricalTypicalProductionRate,
            Single kWhPerkUSG, Single electricalThermalEquivalent,
            Single dieselRequirement, Single dieselTypicalProductionRate,
            Single litrePerkUSG, Single dieselThermalEquivalent)
            {
                Single electricalRequirementkBtuPerkUSG = electricalRequirements/electricalTypicalProductionRate * electricalThermalEquivalent;
                Single dieselRequirementkBtuPerkUSG = dieselRequirement / dieselTypicalProductionRate * dieselThermalEquivalent;
                Single result =(electricalRequirementkBtuPerkUSG + dieselRequirementkBtuPerkUSG) *5 / 1000;
                return result ;
            }


        public static Single TotalOtherCashOpexDollarPerkUSG( Single vehicleRelatedLeasePurchase,
            Single companyNonPersonnelExpense,
            Single feesLeaseRenewalsPropertyTaxesInsuranceEnvironmental,
            Single otherVolumeRelatedExpenses,Single dayMaxCap,
            Single divisor)
        {
            Single TotalOtherCashOpex = (vehicleRelatedLeasePurchase / divisor) +
                (companyNonPersonnelExpense / divisor) +
                (feesLeaseRenewalsPropertyTaxesInsuranceEnvironmental / divisor) +
                (otherVolumeRelatedExpenses / divisor);
            Single annualFuelDelivery = dayMaxCap * 365;
            return TotalOtherCashOpex / annualFuelDelivery;
        }

        public static Single MaintenanceAdministrationCosts(
            IList<Personnel> maintenanceManagersAndPlanners)
        {
            Single operationsWages = 0;
            foreach (Personnel person in maintenanceManagersAndPlanners)
            {
                operationsWages += person.count * person.hours * person.wage * person.wageMultiplier;
            }
            return operationsWages;
        }

        public static Single MaintenanceAdministrationHours(
        IList<Personnel> maintenanceManagersAndPlanners)
        {
            Single operationsHours = 0;
            foreach (Personnel person in maintenanceManagersAndPlanners)
            {
                operationsHours += person.count * person.hours ;
            }
            return operationsHours;
        }

        public static Single MaintenanceStaffCompanyHours(
        IList<Personnel> maintenanceStaffCompany)
        {
            Single operationsHours = 0;
            foreach (Personnel person in maintenanceStaffCompany)
            {
                operationsHours += person.count * person.hours;
            }
            return operationsHours;
        }


        //changing this to use list of personnel instead
        public static Single MaintenanceEDCStandardAnnualMaintenanceCost
            (IList<Personnel> companyMaintenanceStaff, IList<Personnel> contractMaintenanceServicesStaff,
            Single maintenanceMaterial, Single maintenanceEquipment,
            Single tepHoursInThisYear, Single uSGCWageRate, Single contractorDivisor)
        {
            Single countCompanyMaintenanceStaff = GetWorkerCount(companyMaintenanceStaff);
            Single countContractMaintenanceServices = GetWorkerCount(contractMaintenanceServicesStaff);
            Single maintenanceStaffCompany = (Single)Math.Round((countCompanyMaintenanceStaff * tepHoursInThisYear * uSGCWageRate), 0);
            Single contractMaintenanceServices = (Single)Math.Round((countContractMaintenanceServices * tepHoursInThisYear * uSGCWageRate * contractorDivisor), 0);
            return maintenanceStaffCompany + contractMaintenanceServices + maintenanceMaterial + maintenanceEquipment;
        }
        //changing this to use list of personnel instead
        /*public static Single MaintenanceEDCStandardAnnualMaintenanceCost
            (int countCompanyMaintenanceStaff, int countContractMaintenanceServices,
            Single maintenanceMaterial, Single maintenanceEquipment,
            Single tepHoursInThisYear, Single uSGCWageRate, Single contractorDivisor)
        {
            Single maintenanceStaffCompany = (Single)Math.Round((countCompanyMaintenanceStaff * tepHoursInThisYear * uSGCWageRate), 0);
            Single contractMaintenanceServices = (Single)Math.Round((countContractMaintenanceServices * tepHoursInThisYear * uSGCWageRate * contractorDivisor), 0);
            return maintenanceStaffCompany + contractMaintenanceServices + maintenanceMaterial + maintenanceEquipment;
        } */




        public static Single MaintenanceTotalAnnualizedTankIntegerityEDCCost
         (IList<TankVolumes> tankVolumes, Single conversionM3ToBbl, //Single conversionBblToGallons, 
            Single tankIntegrityCostsPerBbblYear,Single tankRepairToInspectionRatio)
        {
            Single totalTankVolume = 0;
            foreach (TankVolumes tank in tankVolumes)
            {
                totalTankVolume += tank.tankVolumeM3 * conversionM3ToBbl;
            }
            Single totalTankageTankIntegrityCost = totalTankVolume * tankIntegrityCostsPerBbblYear;
            Single tankRepairVersusInspecionCosts = totalTankageTankIntegrityCost * tankRepairToInspectionRatio;
            return totalTankageTankIntegrityCost + tankRepairVersusInspecionCosts;
        }


        public static Single AnnualizedTurnaroundCost
            (Single averageIntervalForInsp, int numberOfTanks, Single hourlyWage,
             IList<Personnel> tAOperationsStaff,
             IList<Personnel> tAMaintenanceStaff,
             IList<Personnel> tAContractors)
        {
            Single turnaroundLaborCostsOperationsStaffCompany =
              (Single)Math.Round( GetPersonnelCost(tAOperationsStaff),0);
            Single turnaroundLaborCostsMaintenanceStaffCompany =
                (Single)Math.Round(GetPersonnelCost(tAMaintenanceStaff),0);
            Single contractorLaborCosts = (Single)Math.Round(GetPersonnelCost(tAContractors),0);
            Single turnaroundLaborCosts =turnaroundLaborCostsOperationsStaffCompany +
                turnaroundLaborCostsMaintenanceStaffCompany + contractorLaborCosts;
            Single turnaroundLaborCostsPerTank =(Single)Math.Round( turnaroundLaborCosts * numberOfTanks,0);
            return turnaroundLaborCostsPerTank / averageIntervalForInsp;
        }

        public static Single AnnualizedTurnaroundHours(Single averageIntervalForInsp,
            IList<Personnel> operationsStaffCompany,
             IList<Personnel> maintenanceStaffCompany,
             IList<Personnel> contractors,
            int numberOfTanks)
        {
            Single operationsStaffCompanyHours =GetWorkHours(operationsStaffCompany);
            Single maintenanceStaffCompanyHours =GetWorkHours(maintenanceStaffCompany);
            Single contractorHours  =GetWorkHours(contractors);
            Single sumOfHours =operationsStaffCompanyHours + maintenanceStaffCompanyHours + contractorHours;
            Single sumOfHoursByNumberOfTanks = sumOfHours * numberOfTanks;
            return sumOfHoursByNumberOfTanks / averageIntervalForInsp;
        }
        
        public static Single EDCAnnuliazedTurnaroundMaintenanceCost
            (Single averageIntervalForInsp, int numberOfTanks, Single hourlyWage,
            IList<Personnel> operationsStaffCompany,
             IList<Personnel> maintenanceStaffCompany,
             IList<Personnel> contractors)
        {
            return AnnualizedTurnaroundHours
                (averageIntervalForInsp, operationsStaffCompany, maintenanceStaffCompany,
              contractors, numberOfTanks);
        }

        public static Single MaintenanceWorkHours(IList<Personnel> MaintenanceManagers,IList<Personnel> MaintenanceStaffCompany,
            IList<Personnel> MaintenanceContractServices, Single annualizedTurnaroundCost, Single tankRepairInspectionRatio )
        { //=F68+F70+F72+T93+T98
            
            Single maintenanceManagersHours = GetWorkHours(MaintenanceManagers);  //F68 4160
            Single maintenanceStaffCompany = GetWorkHours(MaintenanceStaffCompany); //F70 37440

            //F72 MaintenanceContractServices(), //contractors hours 10400  = E72*B69   B69 iss 2080, E72 is 5 (employees???)  so get Count from  "Contract Maintenance Services"
            Single maintenanceContractServices = GetWorkHours(MaintenanceContractServices);

            return maintenanceManagersHours + maintenanceStaffCompany + maintenanceContractServices +
                annualizedTurnaroundCost + (annualizedTurnaroundCost * tankRepairInspectionRatio);

        }

        public static Single EDCStandardMaintenanceCost(Single maintenanceAdministrationCosts,
            Single maintenanceEDCStandardAnnualMaintenanceCost, Single maintenanceTotalAnnualizedTankIntegerityEDCCost)
        {
            return maintenanceAdministrationCosts + maintenanceEDCStandardAnnualMaintenanceCost
                + maintenanceTotalAnnualizedTankIntegerityEDCCost;
        }

        public static Single EDCStandardMaintenanceCostDollerPerkUSG(Single eDCStandardMaintenanceCost,
            Single dAYMAXCAP)
        {
            return eDCStandardMaintenanceCost / (dAYMAXCAP*365);
        }

        public static Single OperationsEDCStandardOperationsCostDollerPerkUSG
            (IList<Personnel> operationsPersonnelCost, IList<Personnel> receiptsPersonnelCost,
                IList<Personnel> issuePersonnelCost, Single dayMaxCap)
        {
            int operationsHours = 0;
            Single operationsWages = 0;
            foreach (Personnel person in operationsPersonnelCost)
            {
                persCostMethod(person, ref operationsHours, ref operationsWages);
            }

            int receiptsHours = 0;
            Single receiptsWages = 0;
            foreach (Personnel person in receiptsPersonnelCost)
            {
                persCostMethod(person, ref receiptsHours, ref receiptsWages);
            }

            int issueHours = 0;
            Single issueWages = 0;
            foreach (Personnel person in issuePersonnelCost)
            {
                persCostMethod(person, ref issueHours, ref issueWages);
            }
            Single eDCStandardMgmtOperatorTechnicianSum =
                operationsWages + receiptsWages + issueWages;
            return eDCStandardMgmtOperatorTechnicianSum / (dayMaxCap * 365);
        }
        
        public static Single OperationsEDCStandardOperationsHoursPerkUSG
            (IList<Personnel> operationsPersonnelCost, IList<Personnel> receiptsPersonnelCost,
                IList<Personnel> issuePersonnelCost, Single dayMaxCap)
        {
            int operationsHours = 0;
            Single operationsWages = 0;
            foreach (Personnel person in operationsPersonnelCost)
            {
                persCostMethod(person, ref operationsHours, ref operationsWages);
            }

            int receiptsHours = 0;
            Single receiptsWages = 0;
            foreach (Personnel person in receiptsPersonnelCost)
            {
                persCostMethod(person, ref receiptsHours, ref receiptsWages);
            }

            int issueHours = 0;
            Single issueWages = 0;
            foreach (Personnel person in issuePersonnelCost)
            {
                persCostMethod(person, ref issueHours, ref issueWages);
            }

            Single eDCStandardMgmtOperatorTechnicianSum =
                operationsWages + receiptsWages + issueWages;
            Single eDCStandardOperationsCostDollerPerkUSG =
                eDCStandardMgmtOperatorTechnicianSum / (dayMaxCap * 365);
            Single operationsWorkHours = operationsHours +
                receiptsHours + issueHours;
            return operationsWorkHours / (dayMaxCap * 365);
        }
        

        public static Single TechAdminEDCStandardOperationsCostDollerPerkUSG
        (IList<Personnel> sitePersonnelCost, IList<Personnel> techQaPersonnelCost,
                IList<Personnel> accountingPersonnelCost, Single dayMaxCap)
        {
            int siteHours = 0;
            Single siteWages = 0;
            foreach (Personnel person in sitePersonnelCost)
            {
                persCostMethod(person, ref siteHours, ref siteWages);
            }

            int techQaHours = 0;
            Single techQaWages = 0;
            foreach (Personnel person in techQaPersonnelCost)
            {
                persCostMethod(person, ref techQaHours, ref techQaWages);
            }

            int accountingQaHours = 0;
            Single accountingQaWages = 0;
            foreach (Personnel person in accountingPersonnelCost)
            {
                persCostMethod(person, ref accountingQaHours, ref accountingQaWages);
            }

            Single eDCStandardTechPersonnel = siteWages + techQaWages + accountingQaWages;
            return eDCStandardTechPersonnel / (dayMaxCap * 365);
        }

        
        public static Single TechAdminEDCStandardOperationsHoursPerkUSG
        (IList<Personnel> sitePersonnelCost, IList<Personnel> techQaPersonnelCost,
                IList<Personnel> accountingPersonnelCost, Single dayMaxCap)
        {
            int siteHours = 0;
            Single siteWages = 0;
            foreach (Personnel person in sitePersonnelCost)
            {
                persCostMethod(person, ref siteHours, ref siteWages);
            }

            int techQaHours = 0;
            Single techQaWages = 0;
            foreach (Personnel person in techQaPersonnelCost)
            {
                persCostMethod(person, ref techQaHours, ref techQaWages);
            }

            int accountingQaHours = 0;
            Single accountingQaWages = 0;
            foreach (Personnel person in accountingPersonnelCost)
            {
                persCostMethod(person, ref accountingQaHours, ref accountingQaWages);
            }
            Single operationsWorkHours = siteHours + techQaHours + accountingQaHours;
            return operationsWorkHours / (dayMaxCap * 365);
        }
        
        public static Single EdcStandardCashOpexPerkUsg(Single eDCStandardEnergyCost,
                 Single eDCStandardMaintenanceCostDollerPerkUSG, Single  operationsEDCStandardOperationsHoursPerkUSG,
                 Single techAdminEDCStandardOperationsCostDollerPerkUSG, Single totalOtherCashOpexDollarPerkUSG)
        {
            return eDCStandardEnergyCost + eDCStandardMaintenanceCostDollerPerkUSG +
                operationsEDCStandardOperationsHoursPerkUSG + techAdminEDCStandardOperationsCostDollerPerkUSG +
            totalOtherCashOpexDollarPerkUSG;
        }

        public static Single EDCFactorForAviationFueling(Single edcStandardCashOpexPerkUsg, Single standardCashOpexPerBblOfCduThruput)
        {
            return (Single)Math.Round((edcStandardCashOpexPerkUsg / standardCashOpexPerBblOfCduThruput), 0);
        }

        public static Single EDCkUsgPerDayAviationFuelingOperation(Single eDCFactorForAviationFueling, Single dayMaxCap)
        {
            return eDCFactorForAviationFueling * dayMaxCap;
        }

        public static Single TotalPEIWorkHoursStandard(IList<Personnel> operationsPersonnel, 
            IList<Personnel> receiptsPersonnel, IList<Personnel> issuePersonnel,
            Single maintenanceWorkHours,
            IList<Personnel> sitePersonnel, IList<Personnel> techQaPersonnel,
                IList<Personnel> accountingPersonnel
            )
        {
            Single workHours=0;
            workHours += WorkHours(operationsPersonnel);
            workHours += WorkHours(receiptsPersonnel);
            workHours += WorkHours(issuePersonnel);
            workHours += maintenanceWorkHours;
            workHours += WorkHours(sitePersonnel);
            workHours += WorkHours(techQaPersonnel);
            workHours += WorkHours(accountingPersonnel);
            return workHours;
        }

        public static Single NeOpexStandard(
            Single eDCStandardCashOpexPerkUSG, //F196
            Single eDCStandardEnergyCostDollarPerkUSG, //  F60
            Single dayMaxCap) //D8
        {
            Single annualFuelDelivery = dayMaxCap * 365/1000;
            return (eDCStandardCashOpexPerkUSG - eDCStandardEnergyCostDollarPerkUSG)
                * annualFuelDelivery /1000;
        }

        private static int WorkHours(IList<Personnel> persons)
        {
            int hours = 0;
            Single wages = 0;
            foreach (Personnel person in persons)
            {
                persCostMethod(person, ref hours, ref wages);
            }
            return hours;            
        }

        private static void persCostMethod(PersonnelCost person, ref int hours, ref Single wages)
        {
            hours += person.hours * person.count;
            wages += person.wage * person.wageMultiplier * person.hours * person.count; 
        }

        private static Single GetPersonnelCost(IList<Personnel> persons)
        {
            Single personnelCost = 0;
            foreach (Personnel person in persons)
            {
                personnelCost += person.wage * person.wageMultiplier * person.hours; // *person.count;
            }
            return personnelCost;
        }

        private static int TotalPEIWorkHoursStandard(int operatorsHours, int maintenanceHours, int techAdminHours)
        {
            return operatorsHours+ maintenanceHours+ techAdminHours;
        }

        private static Single GetWorkHours(IList<Personnel> persons)
        {
            Single workHours = 0;
            foreach (Personnel person in persons)
            {
                workHours += person.hours;
            }
            return workHours;
        }
        private static Single GetWorkerCount(IList<Personnel> persons)
        {
            return persons.Count;
        }
    }

    public class TankVolumes
    {
        public string tankId {get;set;}
        public Single tankVolumeM3{get;set;}
    }

    public class Personnel : PersonnelCost
    {//Is for individual person
        public string position { get; set; }
    }

    public class PersonnelCost
    {
        public int count { get; set; }
        public int hours { get; set; }
        public Single wage { get; set; }
        public Single wageMultiplier { get; set; }
    }


}
