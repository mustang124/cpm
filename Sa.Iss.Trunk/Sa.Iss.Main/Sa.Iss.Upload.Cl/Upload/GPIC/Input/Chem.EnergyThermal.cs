﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Input
		{
			public partial class Upload
			{
				public static void EnergyThermal(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Complex-Level Input"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					const int cName = 8;
					const int cQuant = 12;

					try
					{
						for (int i = 23; i <= 26; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[stg].[Insert_ChemEnergyThermal]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@SubmissionId						INT
								cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

								//@tsModified						DATETIMEOFFSET
								cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

								//@ThermalName						VARCHAR(40),
								c = cName;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@ThermalName", SqlDbType.VarChar, 40).Value = XL.Return.String(rng, 40);

								//@Quantity_GJMonth					FLOAT
								c = cQuant;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@Quantity_GJMonth", SqlDbType.Float).Value = XL.Return.Double(rng);

								try
								{
									cmd.ExecuteNonQuery();
								}
								catch (SqlException)
								{
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "GPIC", "GPIC.ChemEnergyThermal", "0", wkb, wks, rng, r, c, "[stg].[Insert_ChemEnergyThermal]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}