﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Energy(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Energy"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cCons = 2;
						int cConsHvc = 3;

						for (int i = 12; i <= 22; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_Energy]", cn))
							{
								if (XL.RangeHasValue(wks.Cells[r, cName]) &&
									XL.RangeHasValue(wks.Cells[r, cCons]))
								{
									cmd.CommandType = CommandType.StoredProcedure;
									SqlParameter p = new SqlParameter();

									//@SubmissionId						INT,
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified						DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@AccountDetail					VARCHAR(96),
									rng = wks.Cells[r, cName];
									cmd.Parameters.Add("@AccountDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

									//@Consumption_GJ					FLOAT,
									rng = wks.Cells[r, cCons];
									cmd.Parameters.Add("@Consumption_GJ", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@Consumption_GJHvc				FLOAT	= NULL
									rng = wks.Cells[r, cConsHvc];
									p = cmd.Parameters.Add("@Consumption_GJHvc", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "Energy", "Energy", "0", wkb, wks, rng, r, c, "[xls].[Insert_Energy]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}