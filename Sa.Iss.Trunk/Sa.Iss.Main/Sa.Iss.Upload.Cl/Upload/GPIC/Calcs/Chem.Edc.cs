﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Edc(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["EDC"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cCount = 2;
						int cCap = 3;
						int cUom = 4;
						int ckEdc = 5;
						int cukEec = 6;
						int cuCap = 7;

						for (int i = 14; i <= 30; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_EDC]", cn))
							{
								if (XL.RangeHasValue(wks.Cells[r, cName]) &&
									XL.RangeHasValue(wks.Cells[r, ckEdc]) &&
									XL.RangeHasValue(wks.Cells[r, cukEec]))
								{
									cmd.CommandType = CommandType.StoredProcedure;
									SqlParameter p = new SqlParameter();

									//@SubmissionId					INT,
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified					DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@ProcessUnitDetail			VARCHAR(96),
									rng = wks.Cells[r, cName];
									cmd.Parameters.Add("@ProcessUnitDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

									//@Unit_Count				INT				= NULL,
									rng = wks.Cells[r, cCount];
									p = cmd.Parameters.Add("@Unit_Count", SqlDbType.Int);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.UInt16(rng);

									//@Capacity					FLOAT			= NULL,
									rng = wks.Cells[r, cCap];
									p = cmd.Parameters.Add("@Capacity", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									//@Capacity_UomName			NVARCHAR(48)	= NULL,
									rng = wks.Cells[r, cUom];
									p = cmd.Parameters.Add("@Capacity_UomName", SqlDbType.NVarChar, 48);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.String(rng, 48);

									//@kEdc						FLOAT,
									rng = wks.Cells[r, ckEdc];
									cmd.Parameters.Add("@kEdc", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@ukEdc					FLOAT,
									rng = wks.Cells[r, cukEec];
									cmd.Parameters.Add("@ukEdc", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@UtilizedCapacity_Pcnt	FLOAT			= NULL
									rng = wks.Cells[r, cuCap];
									p = cmd.Parameters.Add("@UtilizedCapacity_Pcnt", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "EDC", "EDC", "0", wkb, wks, rng, r, c, "[xls].[Insert_EDC]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}