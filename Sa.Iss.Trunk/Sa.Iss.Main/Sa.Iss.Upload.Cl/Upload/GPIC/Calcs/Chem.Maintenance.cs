﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Maintenance(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Maintenance"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cHours = 3;

						for (int i = 10; i <= 16; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_Maintenance]", cn))
							{
								if (XL.RangeHasValue(wks.Cells[r, cName]) &&
									XL.RangeHasValue(wks.Cells[r, cHours]))
								{
									cmd.CommandType = CommandType.StoredProcedure;

									//@SubmissionId					INT,
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified					DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@AccountDetail				VARCHAR(96),
									rng = wks.Cells[r, cName];
									cmd.Parameters.Add("@AccountDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

									//@Cost_kUsd					FLOAT,
									rng = wks.Cells[r, cHours];
									cmd.Parameters.Add("@Cost_kUsd", SqlDbType.Float).Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "Maintenance", "Maintenance", "0", wkb, wks, rng, r, c, "[xls].[Insert_Maintenance]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}