﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Eii(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["EII"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cCap = 2;
						int cUom = 3;
						int cEii = 4;
						int cPcnt = 5;

						for (int i = 14; i <= 26; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_EII]", cn))
							{
								if (XL.RangeHasValue(wks.Cells[r, cName]) &&
									XL.RangeHasValue(wks.Cells[r, cEii]) &&
									XL.RangeHasValue(wks.Cells[r, cPcnt]))
								{
									cmd.CommandType = CommandType.StoredProcedure;
									SqlParameter p = new SqlParameter();

									//@SubmissionId					INT,
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified					DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@ProcessUnitDetail				VARCHAR(96),
									rng = wks.Cells[r, cName];
									cmd.Parameters.Add("@ProcessUnitDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

									//@UtilizedCapacity					FLOAT			= NULL,
									rng = wks.Cells[r, cCap];
									p = cmd.Parameters.Add("@UtilizedCapacity", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									//@UtilizedCapacity_UomName			NVARCHAR(48)	= NULL,
									rng = wks.Cells[r, cUom];
									p = cmd.Parameters.Add("@UtilizedCapacity_UomName", SqlDbType.NVarChar, 48);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.String(rng, 48);

									//@EiiStandardEnergy_MBtuDay		FLOAT,
									rng = wks.Cells[r, cEii];
									cmd.Parameters.Add("@EiiStandardEnergy_MBtuDay", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@StandardEnergy_Pcnt				FLOAT,
									rng = wks.Cells[r, cPcnt];
									cmd.Parameters.Add("@StandardEnergy_Pcnt", SqlDbType.Float).Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "EII", "EII", "0", wkb, wks, rng, r, c, "[xls].[Insert_EII]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}