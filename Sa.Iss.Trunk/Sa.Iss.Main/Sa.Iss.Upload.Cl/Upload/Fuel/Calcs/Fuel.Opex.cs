﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
    public partial class Fuel
    {
        public partial class Calcs
        {
            public partial class Upload
            {
                public static void OpEx(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
                {
                    Excel.Worksheet wks = wkb.Worksheets["OpEx"];
                    Excel.Range rng = null;

                    int r = 0;
                    int c = 0;

                    try
                    {
                        int cName = 1;
                        int cMbtu = 2;
                        int cUsd = 4;

                        //int cHvc = 3;
                        SqlParameter p = new SqlParameter();

                        for (int i = 12; i <= 30; i++)
                        {
                            r = i;

                            using (SqlCommand cmd = new SqlCommand("[xls].[Insert_OpEx]", cn))
                            {
                                if (XL.RangeHasValue(wks.Cells[r, cName]) &&
                                    XL.RangeHasValue(wks.Cells[r, cMbtu]) &&
                                    XL.RangeHasValue(wks.Cells[r, cUsd]))
                                {
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    //@SubmissionId					INT,
                                    cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

                                    //@tsModified					DATETIMEOFFSET
                                    cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

                                    //@AccountDetail					VARCHAR(96),
                                    rng = wks.Cells[r, cName];
                                    cmd.Parameters.Add("@AccountDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

                                    //@Amount_kUsd						FLOAT,
                                    rng = wks.Cells[r, cMbtu];
                                    cmd.Parameters.Add("@Amount_kUsd", SqlDbType.Float).Value = XL.Return.Double(rng);

                                    //@Amount_CentsUEdc					FLOAT,
                                    rng = wks.Cells[r, cUsd];
                                    cmd.Parameters.Add("@Amount_CentsUEdc", SqlDbType.Float).Value = XL.Return.Double(rng);

                                    //@Amount_kUsdHvc					FLOAT	= NULL
                                    //rng = wks.Cells[r, cHvc];
                                    //p = cmd.Parameters.Add("@Amount_kUsdHvc", SqlDbType.Float);
                                    //if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

                                    try
                                    {
                                        cmd.ExecuteNonQuery();
                                    }
                                    catch (SqlException)
                                    {
                                    }
                                }
                            }
                        }

                        r = 33;

                        using (SqlCommand cmd = new SqlCommand("[xls].[Insert_OpEx]", cn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            //@SubmissionId					INT,
                            cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;
                            //@tsModified					DATETIMEOFFSET
                            cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

                            //@AccountDetail					VARCHAR(96),
                            rng = wks.Cells[r, cName];
                            cmd.Parameters.Add("@AccountDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

                            //@Amount_kUsd						FLOAT,
                            rng = wks.Cells[r, cMbtu];
                            cmd.Parameters.Add("@Amount_kUsd", SqlDbType.Float).Value = XL.Return.Double(rng);

                            //@Amount_CentsUEdc					FLOAT,
                            rng = wks.Cells[r, cUsd];
                            cmd.Parameters.Add("@Amount_CentsUEdc", SqlDbType.Float).Value = XL.Return.Double(rng);

                            //@Amount_kUsdHvc					FLOAT	= NULL
                            //rng = wks.Cells[r, cHvc];
                            //p = cmd.Parameters.Add("@Amount_kUsdHvc", SqlDbType.Float);
                            //if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (SqlException)
                            {
                            }
                        }

                        r = 39;

                        using (SqlCommand cmd = new SqlCommand("[xls].[Insert_OpEx]", cn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            //@SubmissionId					INT,
                            cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;
                            //@tsModified					DATETIMEOFFSET
                            cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

                            //@AccountDetail					VARCHAR(96),
                            rng = wks.Cells[r, cName];
                            cmd.Parameters.Add("@AccountDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

                            //@Amount_kUsd						FLOAT,
                            rng = wks.Cells[r, cMbtu];
                            cmd.Parameters.Add("@Amount_kUsd", SqlDbType.Float).Value = XL.Return.Double(rng);

                            //@Amount_CentsUEdc					FLOAT,
                            rng = wks.Cells[r, cUsd];
                            cmd.Parameters.Add("@Amount_CentsUEdc", SqlDbType.Float).Value = XL.Return.Double(rng);

                            //@Amount_kUsdHvc					FLOAT	= NULL
                            //rng = wks.Cells[r, cHvc];
                            //p = cmd.Parameters.Add("@Amount_kUsdHvc", SqlDbType.Float);
                            //if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (SqlException)
                            {
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.Insert_UpLoadError(queue, "OpEx", "OpEx", "0", wkb, wks, rng, r, c, "[xls].[Insert_OpEx]", ex);
                    }
                    finally
                    {
                        wks = null;
                        rng = null;
                    }
                }
            }
        }
    }
}
