﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
    public partial class Tanks
    {
        public class InputForm : IStgUpload
        {
            private readonly CpaFile studyFile;

            public InputForm(CpaFile studyFile)
            {
                this.studyFile = studyFile;
            }

            public Submission UploadStudyFile(SqlConnection cn)
            {
                Submission submission = new Submission();

                Excel.Application xla = XL.NewExcelApplication(false);
                Excel.Workbook wkb = XL.OpenWorkbook_ReadOnly(xla, this.studyFile.FullyQualifiedPath, this.studyFile.Password);

                Queue.Upload queue = new Queue.Upload();
                queue.Id = 0;
                queue.UploadTime = DateTimeOffset.Now;

                using ( cn)
                {
                    //cn.Open(); OK per Omar to pass in open conx
                    /*
                    Input.Upload.Submission(queue, cn, wkb, out submission);

                    Parallel.Invoke
                    (
                        new ParallelOptions { MaxDegreeOfParallelism = 10 },
                        () => { Input.Upload.EnergyElectricity(queue, cn, wkb, submission); },
                        () => { Input.Upload.EnergyThermal(queue, cn, wkb, submission); },
                        () => { Input.Upload.ProcessUnits(queue, cn, wkb, submission); },
                        () => { Input.Upload.ProcessUnitsQuantity(queue, cn, wkb, submission); },
                        () => { Input.Upload.Losses(queue, cn, wkb, submission); }
                    );
                    */
                    //cn.Close();
                }

                XL.CloseWorkbook(ref wkb);
                XL.CloseApplication(ref xla);

                return submission;
            }

            public Submission UploadStudyFile()
            {
                throw new NotImplementedException();
            }
        }

        public class Calculations : IXlsUpload
        {
            private readonly CpaFile studyFile;

            public Calculations(CpaFile studyFile)
            {
                this.studyFile = studyFile;
            }

            public void UploadStudyFile(Submission submission,SqlConnection cn )
            {
                string pathFileCalc = @".\Calcs\GpicCalcs.xlsm";

                Excel.Application xla = XL.NewExcelApplication(false);
                Excel.Workbook wkbInput = XL.OpenWorkbook_ReadOnly(xla, this.studyFile.FullyQualifiedPath, this.studyFile.Password);
                Excel.Workbook wkbCalc = XL.OpenWorkbook_ReadOnly(xla, pathFileCalc);

                Queue.Upload queue = new Queue.Upload();
                queue.Id = 0;
                queue.UploadTime = DateTimeOffset.Now;

                XL.CopyWorkbookValues(wkbInput, wkbCalc);

                using (cn)
                {
                    cn.Open();
                    /*
                    Parallel.Invoke
                    (
                        new ParallelOptions { MaxDegreeOfParallelism = 10 },
                        () => { Calcs.Upload.ReportingPeriod(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Divisors(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Edc(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.FactorsAndStandards(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Eii(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.OpEx(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Energy(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Personnel(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Maintenance(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Reliability(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Turnaround(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.Yield(queue, cn, wkbCalc, submission); },
                        () => { Calcs.Upload.InputRaw(queue, cn, wkbCalc, submission); }
                    );
                    */
                    cn.Close();
                }

                XL.CloseWorkbook(ref wkbInput);
                XL.CloseWorkbook(ref wkbCalc);
                XL.CloseApplication(ref xla);

                CalcsToReport(submission,cn);
            }

            private static void CalcsToReport(Submission submission,SqlConnection cn)
            {
                using ( cn)
                {
                    cn.Open();

                    using (SqlCommand cmd = new SqlCommand("[rpt].[Inject_Tanks_OutputFacilities]", cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@stgSubmissionId", SqlDbType.Char, 1).Value = submission.Id;

                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (SqlException)
                        {
                        }
                    }

                    cn.Close();
                }
            }

            public void UploadStudyFile(Submission submission)
            {
                throw new NotImplementedException();
            }
        }
    }
}
