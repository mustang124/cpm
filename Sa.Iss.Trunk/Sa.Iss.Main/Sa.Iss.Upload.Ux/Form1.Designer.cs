﻿namespace Sa.Iss.Ux
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCreateReport = new System.Windows.Forms.Button();
			this.btnSummary = new System.Windows.Forms.Button();
			this.lbSubmissions = new System.Windows.Forms.ListBox();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.btnPickUploadfile = new System.Windows.Forms.Button();
			this.lblInputForm = new System.Windows.Forms.Label();
			this.btnUpload = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnCreateReport
			// 
			this.btnCreateReport.Location = new System.Drawing.Point(415, 279);
			this.btnCreateReport.Name = "btnCreateReport";
			this.btnCreateReport.Size = new System.Drawing.Size(109, 39);
			this.btnCreateReport.TabIndex = 0;
			this.btnCreateReport.Text = "Create Report";
			this.btnCreateReport.UseVisualStyleBackColor = true;
			this.btnCreateReport.Click += new System.EventHandler(this.btnCreateReport_Click);
			// 
			// btnSummary
			// 
			this.btnSummary.Location = new System.Drawing.Point(12, 12);
			this.btnSummary.Name = "btnSummary";
			this.btnSummary.Size = new System.Drawing.Size(109, 39);
			this.btnSummary.TabIndex = 2;
			this.btnSummary.Text = "Load Summary";
			this.btnSummary.UseVisualStyleBackColor = true;
			this.btnSummary.Click += new System.EventHandler(this.btnSummary_Click);
			// 
			// lbSubmissions
			// 
			this.lbSubmissions.FormattingEnabled = true;
			this.lbSubmissions.Location = new System.Drawing.Point(160, 267);
			this.lbSubmissions.Name = "lbSubmissions";
			this.lbSubmissions.Size = new System.Drawing.Size(214, 95);
			this.lbSubmissions.TabIndex = 3;
			// 
			// btnRefresh
			// 
			this.btnRefresh.Location = new System.Drawing.Point(46, 267);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(75, 23);
			this.btnRefresh.TabIndex = 4;
			this.btnRefresh.Text = "Refresh List";
			this.btnRefresh.UseVisualStyleBackColor = true;
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// btnPickUploadfile
			// 
			this.btnPickUploadfile.Location = new System.Drawing.Point(12, 102);
			this.btnPickUploadfile.Name = "btnPickUploadfile";
			this.btnPickUploadfile.Size = new System.Drawing.Size(109, 39);
			this.btnPickUploadfile.TabIndex = 6;
			this.btnPickUploadfile.Text = "Select File";
			this.btnPickUploadfile.UseVisualStyleBackColor = true;
			this.btnPickUploadfile.Click += new System.EventHandler(this.btnPickUploadfile_Click);
			// 
			// lblInputForm
			// 
			this.lblInputForm.AutoSize = true;
			this.lblInputForm.Location = new System.Drawing.Point(128, 103);
			this.lblInputForm.Name = "lblInputForm";
			this.lblInputForm.Size = new System.Drawing.Size(35, 13);
			this.lblInputForm.TabIndex = 7;
			this.lblInputForm.Text = "label1";
			// 
			// btnUpload
			// 
			this.btnUpload.Location = new System.Drawing.Point(12, 147);
			this.btnUpload.Name = "btnUpload";
			this.btnUpload.Size = new System.Drawing.Size(339, 39);
			this.btnUpload.TabIndex = 8;
			this.btnUpload.Text = "Upload";
			this.btnUpload.UseVisualStyleBackColor = true;
			this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(550, 383);
			this.Controls.Add(this.btnUpload);
			this.Controls.Add(this.lblInputForm);
			this.Controls.Add(this.btnPickUploadfile);
			this.Controls.Add(this.btnRefresh);
			this.Controls.Add(this.lbSubmissions);
			this.Controls.Add(this.btnSummary);
			this.Controls.Add(this.btnCreateReport);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnCreateReport;
		private System.Windows.Forms.Button btnSummary;
		private System.Windows.Forms.ListBox lbSubmissions;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.Button btnPickUploadfile;
		private System.Windows.Forms.Label lblInputForm;
		private System.Windows.Forms.Button btnUpload;

	}
}

