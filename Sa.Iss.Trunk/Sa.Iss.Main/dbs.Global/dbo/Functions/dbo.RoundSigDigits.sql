﻿
CREATE FUNCTION dbo.RoundSigDigits(@Value float, @SigDigits tinyint)
RETURNS decimal(19,4)
AS
BEGIN
	DECLARE @Result DECIMAL(19,4)
    SELECT @Result = ROUND(@Value, @SigDigits-1-FLOOR(LOG10(@Value)))
    RETURN @Result
END

