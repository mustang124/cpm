﻿CREATE PROCEDURE [audit].[Insert_LogError]
(
	@XActState				INT,
	@ProcedureDesc			NVARCHAR(MAX)	= NULL,
	@DataSetIdentifier		NVARCHAR(48)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	SET @ProcedureDesc = REPLACE(LTRIM(LTRIM(@ProcedureDesc)), CHAR(9), '');

	IF (@ProcedureDesc = '')
	BEGIN

		SET @ProcedureDesc = NULL;

	END;

	DECLARE @ErrorSchema	VARCHAR(18);

	SELECT
		@ErrorSchema = [s].[name]
	FROM
		sys.objects [o]
	INNER JOIN
		sys.schemas s
			ON [o].schema_id = [s].schema_id
	WHERE
		[o].[name] = ERROR_PROCEDURE();

	INSERT INTO [audit].[LogError]
	(
		[ErrorNumber],
		[XActState],
		[DataSetIdentifier],
		[ProcedureSchema],	[ProcedureName],		[ProcedureLine],		[ProcedureDesc],
		[ErrorMessage],		[ErrorSeverity],		[ErrorState],
		[tsModified],		[tsModifiedHost],		[tsModifiedUser],		[tsModifiedApp]
	)
	SELECT
		ERROR_NUMBER(),
		@XActState,
		@DataSetIdentifier,
		@ErrorSchema,		ERROR_PROCEDURE(),	ERROR_LINE(),		@ProcedureDesc,
		ERROR_MESSAGE(),	ERROR_SEVERITY(),	ERROR_STATE(),
		SYSDATETIMEOFFSET(),HOST_NAME(),		SUSER_SNAME(),		APP_NAME();

	DECLARE @ERROR_MESSAGE		NVARCHAR(MAX) = ERROR_MESSAGE();
	DECLARE @ERROR_SEVERITY		NVARCHAR(MAX) = ERROR_SEVERITY();
	DECLARE @ERROR_STATE		NVARCHAR(MAX) = ERROR_STATE();

	DECLARE @Index	INT = CHARINDEX(CHAR(10), @ERROR_MESSAGE);
	DECLARE @Len	INT = LEN(@ERROR_MESSAGE)

	SET @ERROR_MESSAGE = CHAR(13) + CHAR(10) +
		N'Msg ' + CONVERT(NVARCHAR(20), ERROR_NUMBER()) +
		N', Level ' + CONVERT(NVARCHAR(20), ERROR_SEVERITY()) +
		N', State ' + CONVERT(NVARCHAR(20), ERROR_STATE()) +
		N', Procedure ' + SCHEMA_NAME() + '.' + ERROR_PROCEDURE() +
		N', Line ' + CONVERT(NVARCHAR(20), ERROR_LINE()) +
		CHAR(13) + CHAR(10) +
		RIGHT(@ERROR_MESSAGE, @Len - @Index)+
		CHAR(13) + CHAR(10) +
		@ProcedureDesc;

END;