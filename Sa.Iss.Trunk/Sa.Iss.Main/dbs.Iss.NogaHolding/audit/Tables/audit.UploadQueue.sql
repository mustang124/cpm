﻿CREATE TABLE [audit].[UploadQueue]
(
	[UploadQueueIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[UploadQueueName]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_UploadQueue_UploadQueueName]		CHECK([UploadQueueName] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_UploadQueue_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadQueue_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadQueue_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadQueue_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_UploadQueue_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_UploadQueue]			PRIMARY KEY NONCLUSTERED([UploadQueueIdx] ASC),
	CONSTRAINT [UK_UploadQueue]			UNIQUE CLUSTERED([UploadQueueIdx] ASC, [UploadQueueName] ASC)
);
GO

CREATE TRIGGER [audit].[t_UploadQueue_u]
ON	[audit].[UploadQueue]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[audit].[UploadQueue]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[audit].[UploadQueue].[UploadQueueIdx]	= [i].[UploadQueueIdx];

END;