﻿CREATE PROCEDURE [xls].[Insert_OpEx]
(
	@SubmissionId						INT,
	@AccountDetail						NVARCHAR(96),

	@Amount_kUsd						FLOAT,
	@Amount_CentsUEdc					FLOAT,

	@Amount_kUsdHvc						FLOAT	= NULL,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[OpEx]
		(
			[SubmissionId],
			[AccountDetail],
			
			[Amount_kUsd],
			[Amount_CentsUEdc],
			
			[Amount_kUsdHvc],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@AccountDetail,

			@Amount_kUsd,
			@Amount_CentsUEdc,

			@Amount_kUsdHvc,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO