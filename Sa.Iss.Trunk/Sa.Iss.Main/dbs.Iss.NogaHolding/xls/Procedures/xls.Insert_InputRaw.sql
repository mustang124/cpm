﻿CREATE PROCEDURE [xls].[Insert_InputRaw]
(
	@SubmissionId						INT,
	@InputRawDetail						NVARCHAR(96),
	@Quantity_TonnesDay					FLOAT,
	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[InputRaw]
		(
			[SubmissionId],
			[InputRawDetail],
			[Quantity_TonnesDay],
			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@InputRawDetail,
			@Quantity_TonnesDay,
			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO