﻿CREATE PROCEDURE [xls].[Insert_TurnaroundEdcAnn]
(
	@SubmissionId						INT,
	@ProcessUnitDetail					NVARCHAR(96),

	@Downtime_HoursEdc					FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[TurnaroundEdcAnn]
		(
			[SubmissionId],
			[ProcessUnitDetail],

			[Downtime_HoursEdc],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ProcessUnitDetail,

			@Downtime_HoursEdc,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO