﻿CREATE PROCEDURE [xls].[Insert_Yield]
(
	@SubmissionId						INT,
	@ComponentDetail					NVARCHAR(96),

	@Mole_Pcnt							FLOAT,
	@Mass_MolecularWeight				FLOAT,
	@Weight_Pcnt						FLOAT,
	@LowerHeatingValue_kJNm3			FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[Yield]
		(
			[SubmissionId],
			[ComponentDetail],

			[Mole_Pcnt],
			[Mass_MolecularWeight],
			[Weight_Pcnt],
			[LowerHeatingValue_kJNm3],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ComponentDetail,

			@Mole_Pcnt,
			@Mass_MolecularWeight,
			@Weight_Pcnt,
			@LowerHeatingValue_kJNm3,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO