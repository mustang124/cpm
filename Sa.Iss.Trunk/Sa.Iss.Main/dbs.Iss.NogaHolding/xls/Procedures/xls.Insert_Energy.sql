﻿CREATE PROCEDURE [xls].[Insert_Energy]
(
	@SubmissionId						INT,
	@AccountDetail						NVARCHAR(96),

	@Consumption_GJ						FLOAT,
	@Consumption_GJHvc					FLOAT	= NULL,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[Energy]
		(
			[SubmissionId],
			[AccountDetail],
			
			[Consumption_GJ],
			[Consumption_GJHvc],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@AccountDetail,

			@Consumption_GJ,
			@Consumption_GJHvc,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO