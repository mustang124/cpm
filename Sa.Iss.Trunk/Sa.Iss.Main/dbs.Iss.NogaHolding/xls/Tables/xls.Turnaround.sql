﻿CREATE TABLE [xls].[Turnaround]
(
	[TurnaroundIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Turnaround_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Turnaround_ProcessUnitDetail]	CHECK([ProcessUnitDetail] <> ''),
	[ProcessUnitId]						AS CONVERT(INT, [dim].[Return_ProcessUnitId]([ProcessUnitDetail])),

	[Interval_Years]					FLOAT					NULL,	CONSTRAINT [CR_Turnaround_Interval_Years]		CHECK([Interval_Years] >= 0.0),

	[Duration_Type]						VARCHAR(12)			NOT	NULL,	CONSTRAINT [CR_Turnaround_Duration_Type]		CHECK([Duration_Type] IN ('Reported', 'Annualized')),
	[Downtime_Hours]					FLOAT					NULL,	CONSTRAINT [CR_Turnaround_Downtime_Hours]		CHECK([Downtime_Hours] >= 0.0),
	[Cost_kUsd]							FLOAT				NOT	NULL,	CONSTRAINT [CR_Turnaround_Cost_kUsd]			CHECK([Cost_kUsd] >= 0.0),
	[Work_Hours]						FLOAT				NOT	NULL,	CONSTRAINT [CR_Turnaround_Work_Hours]			CHECK([Work_Hours] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Turnaround_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Turnaround_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Turnaround_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Turnaround_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Turnaround_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Turnaround]			PRIMARY KEY NONCLUSTERED([TurnaroundIdx] ASC),
	CONSTRAINT [UK_Turnaround]			UNIQUE CLUSTERED([SubmissionId] ASC, [Duration_Type] ASC, [ProcessUnitDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Turnaround_u]
ON	[xls].[Turnaround]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Turnaround]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Turnaround].[TurnaroundIdx]	= [i].[TurnaroundIdx];

END;