﻿CREATE TABLE [xls].[Eii]
(
	[EiiIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Eii_Submissions]					REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Eii_ProcessUnitDetail]			CHECK([ProcessUnitDetail] <> ''),
	[ProcessUnitId]						AS CONVERT(INT, [dim].[Return_ProcessUnitId]([ProcessUnitDetail])),

	[UtilizedCapacity]					FLOAT					NULL,	CONSTRAINT [CR_Eii_UtilizedCapacity]			CHECK([UtilizedCapacity] >= 0.0),
	[UtilizedCapacity_UomName]			NVARCHAR(48)			NULL,	CONSTRAINT [CL_Eii_UtilizedCapacity_UomName]	CHECK([UtilizedCapacity_UomName] <> ''),
	[UomId]								AS CONVERT(INT, [dim].[Return_UomId]([UtilizedCapacity_UomName])),
	[EiiStandardEnergy_MBtuDay]			FLOAT				NOT	NULL,	CONSTRAINT [CR_Eii_EiiStandardEnergy_MBtuDay]	CHECK([EiiStandardEnergy_MBtuDay] >= 0.0),
	[EiiStandardEnergy_GJDay]			AS CONVERT(FLOAT, [EiiStandardEnergy_MBtuDay] * 1.055)
										PERSISTED			NOT	NULL,
	[StandardEnergy_Pcnt]				FLOAT				NOT	NULL,	CONSTRAINT [CR_Eii_StandardEnergy_Pcnt]			CHECK([StandardEnergy_Pcnt] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Eii_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Eii_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Eii_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Eii_tsModifiedApp]				DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Eii_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Eii]					PRIMARY KEY NONCLUSTERED([EiiIdx] ASC),
	CONSTRAINT [UK_Eii]					UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Eii_u]
ON	[xls].[Eii]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Eii]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Eii].[EiiIdx]	= [i].[EiiIdx];

END;