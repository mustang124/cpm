﻿CREATE TABLE [xls].[Edc]
(
	[EdcIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Edc_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Edc_ProcessUnitDetail]		CHECK([ProcessUnitDetail] <> ''),
	[ProcessUnitId]						AS CONVERT(INT, [dim].[Return_ProcessUnitId]([ProcessUnitDetail])),

	[Unit_Count]						INT						NULL,	CONSTRAINT [CR_Edc_Unit_Count]				CHECK([Unit_Count] >= 0),
	[Capacity]							FLOAT					NULL,	CONSTRAINT [CR_Edc_Capacity]				CHECK([Capacity] >= 0.0),
	[Capacity_UomName]					NVARCHAR(48)			NULL,	CONSTRAINT [CL_Edc_Capacity_UomName]		CHECK([Capacity_UomName] <> ''),
	[UomId]								AS CONVERT(INT, [dim].[Return_UomId]([Capacity_UomName])),
	[kEdc]								FLOAT				NOT	NULL,	CONSTRAINT [CR_Edc_kEdc]					CHECK([kEdc] >= 0.0),
	[ukEdc]								FLOAT				NOT	NULL,	CONSTRAINT [CR_Edc_ukEdc]					CHECK([ukEdc] >= 0.0),
	[UtilizedCapacity_Pcnt]				FLOAT					NULL,	CONSTRAINT [CR_Edc_UtilizedCapacity_Pcnt]	CHECK([UtilizedCapacity_Pcnt] >= 0.0),
	[UtilizedCapacity]					AS CONVERT(FLOAT, [Capacity] * [UtilizedCapacity_Pcnt] / 100.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Edc_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Edc_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Edc_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Edc_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Edc_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Edc]					PRIMARY KEY NONCLUSTERED([EdcIdx] ASC),
	CONSTRAINT [UK_Edc]					UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Edc_u]
ON	[xls].[Edc]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Edc]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Edc].[EdcIdx]	= [i].[EdcIdx];

END;