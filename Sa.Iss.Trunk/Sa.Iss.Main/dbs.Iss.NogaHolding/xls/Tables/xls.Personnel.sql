﻿CREATE TABLE [xls].[Personnel]
(
	[PersonnelIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Personnel_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Personnel_AccountDetail]			CHECK([AccountDetail] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountDetail])),

	[Work_Hours]						FLOAT				NOT	NULL,	CONSTRAINT [CL_Personnel_Work_Hours]			CHECK([Work_Hours] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Personnel_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Personnel_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Personnel_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Personnel_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Personnel_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Personnel]			PRIMARY KEY NONCLUSTERED([PersonnelIdx] ASC),
	CONSTRAINT [UK_Personnel]			UNIQUE CLUSTERED([SubmissionId] ASC, [AccountDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Personnel_u]
ON	[xls].[Personnel]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Personnel]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Personnel].[PersonnelIdx]	= [i].[PersonnelIdx];

END;