﻿CREATE TABLE [xls].[Divisors]
(
	[DivisorsIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Divisors_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[DivisorDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Divisors_DivisorDetail]			CHECK([DivisorDetail] <> ''),
	[DivisorId]							AS CONVERT(INT, [dim].[Return_DivisorId]([DivisorDetail])),

	[Value]								FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Divisors_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Divisors]			PRIMARY KEY NONCLUSTERED([DivisorsIdx] ASC),
	CONSTRAINT [UK_Divisors]			UNIQUE CLUSTERED([SubmissionId] ASC, [DivisorDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Divisors_u]
ON	[xls].[Divisors]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Divisors]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Divisors].[DivisorsIdx]	= [i].[DivisorsIdx];

END;