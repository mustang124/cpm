﻿CREATE TABLE [xls].[TurnaroundEdcAnn]
(
	[TurnaroundIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_TurnaroundEdcAnn_Submissions]		REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_TurnaroundEdcAnn_ProcessUnitDetail]	CHECK([ProcessUnitDetail] <> ''),
	[ProcessUnitId]						AS CONVERT(INT, [dim].[Return_ProcessUnitId]([ProcessUnitDetail])),

	[Downtime_HoursEdc]					FLOAT					NULL,	CONSTRAINT [CR_TurnaroundEdcAnn_Downtime_Hours]		CHECK([Downtime_HoursEdc] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_TurnaroundEdcAnn_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TurnaroundEdcAnn_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TurnaroundEdcAnn_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TurnaroundEdcAnn_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_TurnaroundEdcAnn_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_TurnaroundEdc]		PRIMARY KEY NONCLUSTERED([TurnaroundIdx] ASC),
	CONSTRAINT [UK_TurnaroundEdc]		UNIQUE CLUSTERED([SubmissionId] ASC)
);
GO

CREATE TRIGGER [xls].[t_TurnaroundEdcAnn_u]
ON	[xls].[TurnaroundEdcAnn]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[TurnaroundEdcAnn]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[TurnaroundEdcAnn].[TurnaroundIdx]	= [i].[TurnaroundIdx];

END;