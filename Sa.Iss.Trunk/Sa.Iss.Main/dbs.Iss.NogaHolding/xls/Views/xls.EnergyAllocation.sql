﻿CREATE VIEW [xls].[EnergyAllocation]
WITH SCHEMABINDING
AS
SELECT
	[t].[EnergyIdx],
	[t].[SubmissionId],
	[t].[AccountDetail],
	[t].[AccountId],
	[t].[Consumption_GJ],
	[t].[Consumption_GJHvc],
	[p].[Duration_Days],
	[Consumption_GJDay]		= [t].[Consumption_GJ]		/ [p].[Duration_Days],
	[Consumption_GJHvcDay]	= [t].[Consumption_GJHvc]	/ [p].[Duration_Days]
FROM
	[xls].[ReportingPeriod]	[p]
INNER JOIN
	[xls].[Energy]			[t]
		ON	[t].[SubmissionId]	= [p].[SubmissionId];
GO