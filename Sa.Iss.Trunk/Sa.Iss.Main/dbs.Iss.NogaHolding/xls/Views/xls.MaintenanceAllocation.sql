﻿CREATE VIEW [xls].[MaintenanceAllocation]
WITH SCHEMABINDING
AS
SELECT
	[t].[MaintenanceIdx],
	[t].[SubmissionId],
	[t].[AccountDetail],
	[t].[AccountId],
	[t].[Cost_kUsd],
	[p].[Duration_Days],
	[Cost_kUsdAnnualized]	= [t].[Cost_kUsd] * [p].[Duration_Multiplier]
FROM
	[xls].[ReportingPeriod]	[p]
INNER JOIN
	[xls].[Maintenance]		[t]
		ON	[t].[SubmissionId]	= [p].[SubmissionId];
GO