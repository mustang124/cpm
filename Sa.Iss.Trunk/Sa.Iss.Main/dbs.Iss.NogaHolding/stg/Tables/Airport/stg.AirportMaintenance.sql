﻿CREATE TABLE [stg].[AirportMaintenance]
(
	[MaintenanceIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportMaintenance_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirprotMaintenance_AccountName]			CHECK([AccountName] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountName])),

	[Capital_kUsd]						FLOAT				NOT	NULL,
	[Expense_kUsd]						FLOAT					NULL,

	[Total_kUsd]						AS [Capital_kUsd] + COALESCE([Expense_kUsd], 0.0)
										PERSISTED			NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportMaintenance_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportMaintenance_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportMaintenance_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportMaintenance_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportMaintenance_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportMaintenance]	PRIMARY KEY NONCLUSTERED([MaintenanceIdx] ASC),
	CONSTRAINT [UK_AirportMaintenance]	UNIQUE CLUSTERED([SubmissionId] ASC, [AccountName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportMaintenance_u]
ON	[stg].[AirportMaintenance]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportMaintenance]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportMaintenance].[MaintenanceIdx]	= [i].[MaintenanceIdx];

END;