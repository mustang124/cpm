﻿CREATE TABLE [stg].[AirportOpsReliabilityUnplanned]
(
	[UnplannedIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportOpsReliabilityUnplanned_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[UnplannedName]						VARCHAR(24)				NULL,	CONSTRAINT [CL_AirportOpsMassBalance_UnplannedName]					CHECK([UnplannedName] <> ''),
	[ReliabilityId]						AS CONVERT(INT, [dim].[Return_ReliabilityId]([UnplannedName])),

	[UnplannedEvent_Count]				INT						NULL,	CONSTRAINT [CR_AirportOpsMassBalance_UnplannedEvent_Count]			CHECK([UnplannedEvent_Count] >= 0),
	[UnplannedDuration_Hours]			INT						NULL,	CONSTRAINT [CR_AirportOpsMassBalance_UnplannedDuration_Hours]		CHECK([UnplannedDuration_Hours] >= 0.0),
	[UnplannedNotes]					VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportOpsMassBalance_UnplannedNotes]				CHECK([UnplannedNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityUnplanned_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityUnplanned_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityUnplanned_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityUnplanned_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityUnplanned_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportOpsReliabilityUnplanned]	PRIMARY KEY NONCLUSTERED([UnplannedIdx] ASC),
	CONSTRAINT [UK_AirportOpsReliabilityUnplanned]	UNIQUE CLUSTERED([SubmissionId] ASC, [UnplannedName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportOpsReliabilityUnplanned_u]
ON	[stg].[AirportOpsReliabilityUnplanned]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportOpsReliabilityUnplanned]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportOpsReliabilityUnplanned].[UnplannedIdx]	= [i].[UnplannedIdx];

END;