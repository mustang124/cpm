﻿CREATE TABLE [stg].[AirportFuelDispensed]
(
	[FuelDispensedIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportFuelDispensed_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[FuelDispensedProduct]				VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_AirportFuelDispensed_FuelDispensedProduct]	CHECK([FuelDispensedProduct] <> ''),
	
	[FuelDispensedVolume_m3]			FLOAT				NOT	NULL,	CONSTRAINT [CR_AirportFuelDispensed_FuelDispensedVolume_m3]	CHECK([FuelDispensedVolume_m3] >= 0.0),
	[FuelDispensedNotes]				VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportFuelDispensed_FuelDispensedNotes]		CHECK([FuelDispensedNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportFuelDispensed_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelDispensed_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelDispensed_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelDispensed_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportFuelDispensed_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportFuelDispensed]	PRIMARY KEY NONCLUSTERED([FuelDispensedIdx] ASC),
	CONSTRAINT [UK_AirportFuelDispensed]	UNIQUE CLUSTERED([SubmissionId] ASC, [FuelDispensedProduct] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportFuelDispensed_u]
ON	[stg].[AirportFuelDispensed]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportFuelDispensed]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportFuelDispensed].[FuelDispensedIdx]	= [i].[FuelDispensedIdx];

END;