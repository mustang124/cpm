﻿CREATE TABLE [stg].[AirportOpsMassBalance]
(
	[MassBalanceIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportOpsMassBalance_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[MassBalanceName]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportOpsMassBalance_MassBalanceName]		CHECK([MassBalanceName] <> ''),

	[MassBalanceMass_Tonne]				FLOAT					NULL,	
	[MassBalanceMass_Gallons]			FLOAT					NULL,	
	[MassBalanceMass_Percent]			FLOAT					NULL,
	[MassBalanceNotes]					VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportOpsMassBalance_MassBalanceNotes]		CHECK([MassBalanceNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalance_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalance_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalance_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalance_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalance_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportOpsMassBalance]	PRIMARY KEY NONCLUSTERED([MassBalanceIdx] ASC),
	CONSTRAINT [UK_AirportOpsMassBalance]	UNIQUE CLUSTERED([SubmissionId] ASC, [MassBalanceName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportOpsMassBalance_u]
ON	[stg].[AirportOpsMassBalance]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportOpsMassBalance]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportOpsMassBalance].[MassBalanceIdx]	= [i].[MassBalanceIdx];

END;