﻿CREATE TABLE [stg].[Airport]
(
	[AirportIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Airport_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),

	[AirportServed]						VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_Airport_AirportServed]			CHECK([AirportServed] <> ''),
	[City]								NVARCHAR(96)			NULL,	CONSTRAINT [CL_Airport_City]					CHECK([City] <> ''),
	[Province]							NVARCHAR(96)			NULL,	CONSTRAINT [CL_Airport_Province]				CHECK([Province] <> ''),
	[CountryName]						NVARCHAR(84)			NULL,	CONSTRAINT [CL_Airport_CountryName]				CHECK([CountryName] <> ''),
	[CountryId]							AS CONVERT(INT, [dim].[Return_CountryId]([CountryName])),

	[MapFileName]						VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_Airport_MapFileName]				CHECK([MapFileName] <> ''),
	[OrgFileName]						VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_Airport_OrgFileName]				CHECK([OrgFileName] <> ''),
	[FuelDispensed_m3]					FLOAT				NOT	NULL,	CONSTRAINT [CR_Airport_FuelDispensed_m3]		CHECK([FuelDispensed_m3] >= 0.0),

	[Apron_Count]						SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_Apron_Count]				CHECK([Apron_Count] >= 0),
	[Tank_Count]						SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_Tank_Count]				CHECK([Tank_Count] >= 0),
	[Hydrant_Count]						SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_Hydrant_Count]			CHECK([Hydrant_Count] >= 0),
	[Fueler_Count]						SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_Fueler_Count]			CHECK([Fueler_Count] >= 0),
	[FuelingCarts_Count]				SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_FuelingCarts_Count]		CHECK([FuelingCarts_Count] >= 0),
	[LoadingStations_Count]				SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_LoadingStations_Count]	CHECK([LoadingStations_Count] >= 0),
	[PumpStations_Count]				SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_PumpStations_Count]		CHECK([PumpStations_Count] >= 0),
	[Pump_Count]						SMALLINT			NOT	NULL,	CONSTRAINT [CR_Airport_Pump_Count]				CHECK([Pump_Count] >= 0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Airport_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Airport_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Airport_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Airport_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Airport_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Airport]				PRIMARY KEY NONCLUSTERED([AirportIdx] ASC),
	CONSTRAINT [UK_Airport]				UNIQUE CLUSTERED([SubmissionId] ASC),
);
GO

CREATE TRIGGER [stg].[t_Airport_u]
ON	[stg].[Airport]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[Airport]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[Airport].[AirportIdx]	= [i].[AirportIdx];

END;