﻿CREATE TABLE [stg].[AirportFuelingCarts]
(
	[FuelingCartIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportFuelingCarts_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[FuelingCartInformation]			VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportFuelingCarts_FuelingCartInformation]	CHECK([FuelingCartInformation] <> ''),
	[FuelingCartName]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportFuelingCarts_FuelingCartName]			CHECK([FuelingCartName] <> ''),

	[FuelingCartNotes]					VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportFuelingCarts_FuelingCartNotes]		CHECK([FuelingCartNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportFuelingCarts_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelingCarts_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelingCarts_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelingCarts_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportFuelingCarts_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportFuelingCarts]	PRIMARY KEY NONCLUSTERED([FuelingCartIdx] ASC),
	CONSTRAINT [UK_AirportFuelingCarts]	UNIQUE CLUSTERED([SubmissionId] ASC, [FuelingCartInformation] ASC),
	CONSTRAINT [UX_AirportFuelingCarts]	UNIQUE NONCLUSTERED([SubmissionId] ASC, [FuelingCartName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportFuelingCarts_u]
ON	[stg].[AirportFuelingCarts]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportFuelingCarts]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportFuelingCarts].[FuelingCartIdx]	= [i].[FuelingCartIdx];

END;