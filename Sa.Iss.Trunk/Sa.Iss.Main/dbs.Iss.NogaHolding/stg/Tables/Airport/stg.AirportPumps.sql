﻿CREATE TABLE [stg].[AirportPumps]
(
	[PumpIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportPumps_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[PumpInformation]					VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportPumps_StationsInformation]	CHECK([PumpInformation] <> ''),
	[PumpName]							VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportPumps_StationsName]			CHECK([PumpName] <> ''),

	[PumpCapacity_m3Min]				FLOAT					NULL,	CONSTRAINT [CL_AirportPumps_FuelerCapacity_m3]		CHECK([PumpCapacity_m3Min] >= 0.0),
	[PumpService]						VARCHAR(24)				NULL,	CONSTRAINT [CL_AirportPumps_PumpService]			CHECK([PumpService] <> ''),
	[PumpServiceId]						AS CONVERT(INT, [dim].[Return_PumpServiceId]([PumpService])),

	[PumpNotes]							VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportPumps_PumpNotes]				CHECK([PumpNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportPumps_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportPumps_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportPumps_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportPumps_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportPumps_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportPumps]		PRIMARY KEY NONCLUSTERED([PumpIdx] ASC),
	CONSTRAINT [UK_AirportPumps]		UNIQUE CLUSTERED([SubmissionId] ASC, [PumpInformation] ASC),
	CONSTRAINT [UX_AirportPumps]		UNIQUE NONCLUSTERED([SubmissionId] ASC, [PumpName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportPumps_u]
ON	[stg].[AirportPumps]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportPumps]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportPumps].[PumpIdx]	= [i].[PumpIdx];

END;