﻿CREATE TABLE [stg].[AirportExpenses]
(
	[ExpenseIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportExpenses_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_AirportExpenses_AcountDetail]			CHECK([AccountDetail] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountDetail])),

	[Expense_kUsd]						FLOAT				NOT	NULL,
	[ExpenseNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportExpenses_ExpenseNotes]			CHECK([ExpenseNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportExpenses_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportExpenses_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportExpenses_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportExpenses_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportExpenses_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Expenses]			PRIMARY KEY NONCLUSTERED([ExpenseIdx] ASC),
	CONSTRAINT [UK_Expenses]			UNIQUE CLUSTERED([SubmissionId] ASC, [AccountDetail] ASC),
);
GO

CREATE TRIGGER [stg].[t_AirportExpenses_u]
ON	[stg].[AirportExpenses]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportExpenses]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportExpenses].[ExpenseIdx]	= [i].[ExpenseIdx];

END;