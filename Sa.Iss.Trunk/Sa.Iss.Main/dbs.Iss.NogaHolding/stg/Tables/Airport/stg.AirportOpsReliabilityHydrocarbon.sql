﻿CREATE TABLE [stg].[AirportOpsReliabilityHydrocarbon]
(
	[HydrocarbonIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportOpsReliabilityHydrocarbon_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[HydrocarbonName]					VARCHAR(24)				NULL,	CONSTRAINT [CL_AirportOpsReliabilityHydrocarbon_HydrocarbonName]		CHECK([HydrocarbonName] <> ''),
	[ReliabilityId]						AS CONVERT(INT, [dim].[Return_ReliabilityId]([HydrocarbonName])),

	[HydrocarbonEvent_Count]			INT						NULL,	CONSTRAINT [CR_AirportOpsReliabilityHydrocarbon_HydrocarbonEvent_Count]	CHECK([HydrocarbonEvent_Count] >= 0),
	[HydrocarbonVolume_m3]				INT						NULL,	CONSTRAINT [CR_AirportOpsReliabilityHydrocarbon_HydrocarbonVolume_m3]	CHECK([HydrocarbonVolume_m3] >= 0.0),
	[HydrocarbonNotes]					VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportOpsReliabilityHydrocarbon_HydrocarbonNotes]		CHECK([HydrocarbonNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityHydrocarbon_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityHydrocarbon_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityHydrocarbon_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityHydrocarbon_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportOpsReliabilityHydrocarbon_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportOpsReliabilityHydrocarbon]	PRIMARY KEY NONCLUSTERED([HydrocarbonIdx] ASC),
	CONSTRAINT [UK_AirportOpsReliabilityHydrocarbon]	UNIQUE CLUSTERED([SubmissionId] ASC, [HydrocarbonName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportOpsReliabilityHydrocarbon_u]
ON	[stg].[AirportOpsReliabilityHydrocarbon]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportOpsReliabilityHydrocarbon]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportOpsReliabilityHydrocarbon].[HydrocarbonIdx]	= [i].[HydrocarbonIdx];

END;