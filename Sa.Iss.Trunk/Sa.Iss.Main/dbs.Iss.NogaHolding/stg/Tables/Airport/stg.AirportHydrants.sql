﻿CREATE TABLE [stg].[AirportHydrants]
(
	[HydrantIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportHydrants_Submissions]						REFERENCES [stg].[Submissions]([SubmissionId]),
	[HydrantInformation]				VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportHydrants_HydrantInformation]				CHECK([HydrantInformation] <> ''),
	[HydrantName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportHydrants_HydrantName]						CHECK([HydrantName] <> ''),

	[HydrantProduct]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportHydrants_HydrantProduct]					CHECK([HydrantProduct] <> ''),
	[HydrantPressure_Psi]				FLOAT				NOT	NULL,	CONSTRAINT [CR_AirportHydrants_HydrantPressure_Psi]				CHECK([HydrantPressure_Psi] >= 0.0),
	[HydrantPitValvesServed_Count]		INT					NOT	NULL,	CONSTRAINT [CR_AirportHydrants_HydrantPitValvesServed_Count]	CHECK([HydrantPitValvesServed_Count] >= 1),
	[HydrantDispensers_Count]			INT					NOT	NULL,	CONSTRAINT [CR_AirportHydrants_HydrantDispensers_Count]			CHECK([HydrantDispensers_Count] >= 1),
	[HydrantApronsServed_Count]			INT					NOT	NULL,	CONSTRAINT [CR_AirportHydrants_HydrantApronsServed_Count]		CHECK([HydrantApronsServed_Count] >= 1),
	[HydrantNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportHydrants_HydrantNotes]					CHECK([HydrantNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportHydrants_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportHydrants_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportHydrants_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportHydrants_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportHydrants_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportHydrants]		PRIMARY KEY NONCLUSTERED([HydrantIdx] ASC),
	CONSTRAINT [UK_AirportHydrants]		UNIQUE CLUSTERED([SubmissionId] ASC, [HydrantInformation] ASC),
	CONSTRAINT [UX_AirportHydrants]		UNIQUE NONCLUSTERED([SubmissionId] ASC, [HydrantName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportHydrants_u]
ON	[stg].[AirportHydrants]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportHydrants]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportHydrants].[HydrantIdx]	= [i].[HydrantIdx];

END;