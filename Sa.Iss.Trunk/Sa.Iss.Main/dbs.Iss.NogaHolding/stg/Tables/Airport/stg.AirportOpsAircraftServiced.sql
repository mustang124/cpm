﻿CREATE TABLE [stg].[AirportOpsAircraftServiced]
(
	[AircraftServicedIdx]				INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportOpsAircraftServiced_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[ServicedName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportOpsAircraftServiced_ServicedName]			CHECK([ServicedName] <> ''),
	[AircraftTypeId]					AS CONVERT(INT, [dim].[Return_AircraftTypeId]([ServicedName])),

	[Serviced_Count]					INT					NOT	NULL,	CONSTRAINT [CR_AirportOpsAircraftServiced_Serviced_Count]		CHECK([Serviced_Count] >= 0),
	[ServicedMax_Count]					INT					NOT	NULL,	CONSTRAINT [CR_AirportOpsAircraftServiced_ServicedMax_Count]	CHECK([ServicedMax_Count] >= 0),
	[ServicedNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportOpsAircraftServiced_ServicedNotes]		CHECK([ServicedNotes] <> ''),
			
	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportOpsAircraftServiced_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsAircraftServiced_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsAircraftServiced_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsAircraftServiced_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportOpsAircraftServiced_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportOpsAircraftServiced]	PRIMARY KEY NONCLUSTERED([AircraftServicedIdx] ASC),
	CONSTRAINT [UK_AirportOpsAircraftServiced]	UNIQUE CLUSTERED([SubmissionId] ASC, [ServicedName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportOpsAircraftServiced_u]
ON	[stg].[AirportOpsAircraftServiced]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportOpsAircraftServiced]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportOpsAircraftServiced].[AircraftServicedIdx]	= [i].[AircraftServicedIdx];

END;