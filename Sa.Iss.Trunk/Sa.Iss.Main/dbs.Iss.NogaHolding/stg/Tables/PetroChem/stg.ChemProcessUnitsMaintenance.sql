﻿CREATE TABLE [stg].[ChemProcessUnitsMaintenance]
(
	[MaintenanceIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsMaintenance_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsMaintenance_ChemProcessUnits]		REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),
	
	[Regulatory_Hours]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsMaintenance_Regulatory_Hours]		CHECK([Regulatory_Hours] >= 0.0),
	[Mechanical_Hours]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsMaintenance_Mechanical_Hours]		CHECK([Mechanical_Hours] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenance_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenance_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenance_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenance_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenance_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsMaintenance]	PRIMARY KEY NONCLUSTERED([MaintenanceIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsMaintenance]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsMaintenance_u]
ON	[stg].[ChemProcessUnitsMaintenance]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsMaintenance]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsMaintenance].[MaintenanceIdx]	= [i].[MaintenanceIdx];

END;