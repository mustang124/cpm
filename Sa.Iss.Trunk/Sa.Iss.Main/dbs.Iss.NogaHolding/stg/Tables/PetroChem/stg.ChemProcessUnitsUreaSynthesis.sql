﻿CREATE TABLE [stg].[ChemProcessUnitsUreaSynthesis]
(
	[UreaSynthesisIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsUreaSynthesis_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsUreaSynthesis_ChemProcessUnits]			REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),
	
	[Ammonia_TonneDay]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsUreaSynthesis_Ammonia_TonneDay]			CHECK([Ammonia_TonneDay] >= 0.0),
	[CarbonDioxide_TonneDay]			FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsUreaSynthesis_CarbonDioxide_TonneDay]	CHECK([CarbonDioxide_TonneDay] >= 0.0),
	[ProductType]						VARCHAR(12)			NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsUreaSynthesis_ProductType]				CHECK([ProductType] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsUreaSynthesis_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsUreaSynthesis_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsUreaSynthesis_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsUreaSynthesis_tsModifiedApp]				DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsUreaSynthesis_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsUreaSynthesis]	PRIMARY KEY NONCLUSTERED([UreaSynthesisIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsUreaSynthesis]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsUreaSynthesis_u]
ON	[stg].[ChemProcessUnitsUreaSynthesis]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsUreaSynthesis]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsUreaSynthesis].[UreaSynthesisIdx]	= [i].[UreaSynthesisIdx];

END;