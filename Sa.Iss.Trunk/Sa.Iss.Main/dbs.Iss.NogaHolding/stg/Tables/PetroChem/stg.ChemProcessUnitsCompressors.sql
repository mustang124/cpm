﻿CREATE TABLE [stg].[ChemProcessUnitsCompressors]
(
	[CompressorIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsCompressors_Submissions]					REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsCompressors_ChemProcessUnits]				REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),
	[CompressorsName]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_ChemProcessUnitsCompressors_CompressorsName]				CHECK([CompressorsName] <> ''),
	[CompressorId]						AS CONVERT(INT, [dim].[Return_CompressorId]([CompressorsName])),

	[Power_kW]							FLOAT				NOT	NULl,	CONSTRAINT [CR_ChemProcessUnitsCompressors_Power_kW]						CHECK([Power_kW] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCompressors_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCompressors_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCompressors_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCompressors_tsModifiedApp]				DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCompressors_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsCompressors]	PRIMARY KEY NONCLUSTERED([CompressorIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsCompressors]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC, [CompressorsName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsCompressors_u]
ON	[stg].[ChemProcessUnitsCompressors]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsCompressors]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsCompressors].[CompressorIdx]	= [i].[CompressorIdx];

END;