﻿CREATE TABLE [stg].[ChemEnergyElectricity]
(
	[ElectricityIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemEnergyElectricity_Submissions]					REFERENCES [stg].[Submissions]([SubmissionId]),
	[ElectricityName]					VARCHAR(40)			NOT	NULL,	CONSTRAINT [CL_ChemEnergyElectricity_EnergyElectricityName]			CHECK([ElectricityName] <> ''),
	[EnergyId]							AS CONVERT(INT, [dim].[Return_EnergyId]([ElectricityName])),

	[Quantity_MWhMonth]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemEnergyElectricity_Quantity_MWhMonth]				CHECK([Quantity_MWhMonth] >= 0.0),
	[Efficiency_MJMWh]					FLOAT					NULL,	CONSTRAINT [CR_ChemEnergyElectricity_Efficiency_MJMWh]				CHECK([Efficiency_MJMWh] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemEnergyElectricity_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemEnergyElectricity_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemEnergyElectricity_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemEnergyElectricity_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemEnergyElectricity_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemEnergyElectricity]	PRIMARY KEY NONCLUSTERED([ElectricityIdx] ASC),
	CONSTRAINT [UK_ChemEnergyElectricity]	UNIQUE CLUSTERED([SubmissionId] ASC, [ElectricityName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemEnergyElectricity_u]
ON	[stg].[ChemEnergyElectricity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemEnergyElectricity]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemEnergyElectricity].[ElectricityIdx]	= [i].[ElectricityIdx];

END;