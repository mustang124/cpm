﻿CREATE TABLE [stg].[ChemProcessUnits]
(
	[ProcessUnitIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnits_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitName]					VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_ChemProcessUnits_ProcessUnitName]			CHECK([ProcessUnitName] <> ''),
	[ProcessUnitNumber]					INT						NULL,	CONSTRAINT [CL_ChemProcessUnits_ProcessUnitNumber]			CHECK([ProcessUnitNumber] >= 1),

	[ProcessId]							VARCHAR(12)				NULL,	CONSTRAINT [CL_ChemProcessUnits_ProcessId]					CHECK([ProcessId] <> ''),
	[ProcessType]						VARCHAR(12)				NULL,	CONSTRAINT [CL_ChemProcessUnits_ProcessType]				CHECK([ProcessType] <> ''),

	[Capacity_Units]					VARCHAR(32)				NULL,	CONSTRAINT [CL_ChemProcessUnits_Capacity_Units]				CHECK([Capacity_Units] <> ''),
	[Capacity]							FLOAT					NULL,	CONSTRAINT [CL_ChemProcessUnits_Capacity]					CHECK([Capacity] >= 0.0),
	[CapacityUtilization_Pcnt]			FLOAT					NULL,	CONSTRAINT [CL_ChemProcessUnits_CapacityUtilization_Pcnt]	CHECK([CapacityUtilization_Pcnt] >= 0.0),
	[CapacityUtilization_Unit]			AS CONVERT(FLOAT, [Capacity] * [CapacityUtilization_Pcnt] / 100.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnits_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnits_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnits_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnits_tsModifiedApp]				DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnits_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnits]	PRIMARY KEY NONCLUSTERED([ProcessUnitIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnits]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnits_u]
ON	[stg].[ChemProcessUnits]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnits]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnits].[ProcessUnitIdx]	= [i].[ProcessUnitIdx];

END;