﻿CREATE TABLE [stg].[ChemProductShipments]
(
	[ProductShipmentIdx]				INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProductShipments_Submissions]					REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProductShipmentName]				VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_ChemProductShipments_ProductShipmentName]			CHECK([ProductShipmentName] <> ''),

	[MaterialName]						VARCHAR(12)			NOT	NULL,
	[MaterialId]						AS CONVERT(INT, [dim].[Return_MaterialId]([MaterialName])),
	[TransMethod]						VARCHAR(12)			NOT	NULL,
	[TransportationMethodId]			AS CONVERT(INT, [dim].[Return_TransportationMethodId]([TransMethod])),

	[ProductShipment_TonneMonth]		FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemMaterialQuantity_ProductShipment_TonneMonth]		CHECK([ProductShipment_TonneMonth] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProductShipments_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProductShipments_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProductShipments_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProductShipments_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProductShipments_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProductShipments]	PRIMARY KEY NONCLUSTERED([ProductShipmentIdx] ASC),
	CONSTRAINT [UK_ChemProductShipments]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProductShipmentName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProductShipments_u]
ON	[stg].[ChemProductShipments]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProductShipments]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProductShipments].[ProductShipmentIdx]	= [i].[ProductShipmentIdx];

END;