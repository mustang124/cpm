﻿CREATE TABLE [stg].[ChemProcessUnitsReforming]
(
	[ReformingIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsReforming_Submissions]						REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsReforming_ChemProcessUnits]					REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),

	[SteamToCarbonRatio_MolMol]			FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsReforming_SteamToCarbonRatio_MolMol]			CHECK([SteamToCarbonRatio_MolMol] >= 0.0),
	[ProcGasRecToSteamReform_knm3Day]	FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsReforming_ProcGasRecToSteamReform_knm3Day]	CHECK([ProcGasRecToSteamReform_knm3Day] >= 0.0),
	[SteamReformingHydrogen_MolePcnt]	FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsReforming_SteamReformingHydrogen_MolePcnt]	CHECK([SteamReformingHydrogen_MolePcnt] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsReforming_tsModified]							DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsReforming_tsModifiedHost]						DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsReforming_tsModifiedUser]						DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsReforming_tsModifiedApp]						DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsReforming_tsModifiedGuid]						DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsReforming]	PRIMARY KEY NONCLUSTERED([ReformingIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsReforming]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsReforming_u]
ON	[stg].[ChemProcessUnitsReforming]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsReforming]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsReforming].[ReformingIdx]	= [i].[ReformingIdx];

END;