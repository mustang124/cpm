﻿CREATE TABLE [stg].[ChemPersonnel]
(
	[PersonnelIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemPersonnel_Submissions]							REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_ChemPersonnel_AccountName]							CHECK([AccountName] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountName])),

	[NonTurnaroundLabor_Hours]			FLOAT				NOT	NULL,	CONSTRAINT [CL_ChemPersonnel_NonTurnaroundLabor_Hours]				CHECK([NonTurnaroundLabor_Hours] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemPersonnel_tsModified]							DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemPersonnel_tsModifiedHost]						DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemPersonnel_tsModifiedUser]						DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemPersonnel_tsModifiedApp]							DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemPersonnel_tsModifiedGuid]						DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemPersonnel]		PRIMARY KEY NONCLUSTERED([PersonnelIdx] ASC),
	CONSTRAINT [UK_ChemPersonnel]		UNIQUE CLUSTERED([SubmissionId] ASC, [AccountName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemPersonnel_u]
ON	[stg].[ChemPersonnel]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemPersonnel]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemPersonnel].[PersonnelIdx]	= [i].[PersonnelIdx];

END;