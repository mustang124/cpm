﻿CREATE TABLE [stg].[ChemProcessUnitsComposition]
(
	[CompositionIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsComposition_Submissions]					REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsComposition_ChemProcessUnits]				REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),
--	TODO: StreamName (naming convention)
	[StreamName]						VARCHAR(6)			NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsComposition_StreamName]					CHECK([StreamName] IN ('Feed', 'Syngas')),
	[ComponentName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_ChemProcessUnitsComposition_ProcessCompositionName]		CHECK([ComponentName] <> ''),
	[ComponentId]						AS CONVERT(INT, [dim].[Return_ComponentId]([ComponentName])),

	[Composition_MolePcnt]				FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsComposition_Composition_MolePcnt]			CHECK([Composition_MolePcnt] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsComposition_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsComposition_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsComposition_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsComposition_tsModifiedApp]				DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsComposition_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsComposition]	PRIMARY KEY NONCLUSTERED([CompositionIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsComposition]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC, [StreamName] ASC, [ComponentName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsComposition_u]
ON	[stg].[ChemProcessUnitsComposition]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsComposition]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsComposition].[CompositionIdx]	= [i].[CompositionIdx];

END;