﻿CREATE PROCEDURE [stg].[Insert_ChemEnergyThermal]
(
	@SubmissionId						INT,
	
	@ThermalName						VARCHAR(40),
	@Quantity_GJMonth					FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [stg].[ChemEnergyThermal]
		(
			[SubmissionId],
			[ThermalName],
			[Quantity_GJMonth],
			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ThermalName,
			@Quantity_GJMonth,
			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;