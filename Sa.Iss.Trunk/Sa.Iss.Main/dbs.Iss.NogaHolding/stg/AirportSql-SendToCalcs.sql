﻿
--========================================
--now is where we would read the data to put into the xls calcs doc.
--then pull the output from xls into the xls schema.
--sql for the Reads it is generated below. Putting it into Xls will be the code's job.
--========================================

--AUG data (Raw Data sheet):

--Capital Expenditure (Total): 0


--Maint Costs
--Expense Expenditure
--Company Labor and Contract Services (4087) L23
----table9 C11 = TotalCompLaborMaint
----TotalCompLaborMaint is on BAFCO Maintenance O24
------this is =CoLaborCapitalExp+CoLaborMaintExp
------CoLaborCapitalExp
--------this is BAFCO Maintenance M24, pulls from ='Raw Data'!S20
------CoLaborMaintExp
--------this is BAFCO Maintenance N24, pulls from ='Raw Data'!S23 = 10.831
----------this is the 4087 in Raw Data L23 * 2.65 (Currency conversion) = 10830.55


--table9 opex B20 (Company Labor and Contract Services)   =Maintenance!C11
--Maintenance!C11 =TotalCompLaborMaint
--TotalCompLaborMaint is BAFCO Maintenance sheet, O24  10.831
----this is =CoLaborCapitalExp+CoLaborMaintExp
----CoLaborCapitalExp is same sheet, M 24 (0)
----CoLaborMaintExp is same sheet, N 24 (Raw Data!S23) (10.831)
-----this is the 4087 in Raw Data L23 * 2.65 (Currency conversion) = 10830.55 / 1000
------this is the 2015 sheet L21  

--So backwards, it goes:
/*
2015 sheet
raw data sheet (currency)-this would be the calcs
BAFCO Maintenance sheet (this is analogous to the Input Form)
Table 9 (Opex sheet)
*/

--so I P need to go backwards from the tables etc to get to the #s I need to put into and pull from xls.

--working on the BAFCO EDC sheet in BafcoCalcsToRpt.xlsx

--left off at  Single GetEDCStandardEnergyCost(.txt