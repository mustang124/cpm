﻿CREATE FUNCTION [org].[Return_GroupId]
(
	@Group			NVARCHAR(96)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @GroupId	INT;

	SELECT TOP 1
		@GroupId = [c].[GroupId]
	FROM
		[org].[Groups]	[c]
	WHERE	[c].[GroupTag]		= @Group
		OR	[c].[GroupName]		= @Group
		OR	[c].[GroupDetail]	= @Group;

	RETURN	@GroupId;

END;