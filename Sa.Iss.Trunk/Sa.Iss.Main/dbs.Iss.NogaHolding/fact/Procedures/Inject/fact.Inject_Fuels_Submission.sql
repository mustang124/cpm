﻿CREATE PROCEDURE [fact].[Inject_Fuels_Submission]
(
	@profileSubmissionId		INT
)
AS
BEGIN

	DECLARE @Submissions			TABLE
	(
		[SubmissionId]		INT		NOT	NULL,
		PRIMARY KEY CLUSTERED([SubmissionId] ASC)
	);

	DECLARE @SubmissionId	INT
	DECLARE @FactorSetID			CHAR(8)		= '2012';
	DECLARE @ScenarioId				CHAR(8)		= 'CLIENT';
	DECLARE @CurrencyId				CHAR(4)		= 'USD';
	DECLARE @DataTypeId				CHAR(6)		= 'ADJ';
	DECLARE @UomId					CHAR(3)		= 'MET';

	INSERT INTO [fact].[Submissions]
	(
		[GroupId],
		[DataSpan],
		[DataYear],
		[DataMonth],
		[PeriodBeg],
		[PeriodEnd]
	)
	OUTPUT
		INSERTED.[SubmissionId]
	INTO
		@Submissions
		(
			[SubmissionId]
		)
	SELECT
		[GroupId]		= NULL,
		[DataSpan]		= 'M',
		[DataYear]		= YEAR([p].[PeriodStart]),
		[DataMonth]		= MONTH([p].[PeriodStart]),
		[PeriodBeg]		= [p].[PeriodStart],
		[PeriodEnd]		= [p].[PeriodEnd]
	FROM
		[$(dbProfile)].[dbo].[GenSum]	[p]
	WHERE	[p].[FactorSet]		= @FactorSetId
		AND	[p].[Scenario]		= @ScenarioId
		AND	[p].[Currency]		= @CurrencyId
		AND	[p].[UOM]			= @UomId
		AND	[p].[SubmissionID]	= @profileSubmissionId;

	SET	@SubmissionId = 
	(
		SELECT TOP 1
			[s].[SubmissionId]
		FROM
			@Submissions	[s]
	);
	return @SubmissionId
END