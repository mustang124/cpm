﻿CREATE PROCEDURE [fact].[Select_SubmissionInfo]
(
	@SubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		SELECT TOP 1
			[s].[SubmissionId],
				[SubmissionName]	= DATENAME(MONTH, [s].[PeriodBeg]) + ' ' + CONVERT(CHAR(4), [s].[DataYear]),
				[SubmissionDate]	= DATENAME(MONTH, [s].[PeriodBeg]) + ' ' + CONVERT(CHAR(4), [s].[DataYear]),
			[s].[PeriodBeg],
			[s].[PeriodEnd],
			[s].[Duration_Days],

			[g].[GroupTag],
			[g].[GroupName],
			[g].[GroupDetail],

			[f].[FacilityTag],
			[f].[FacilityName],
			[f].[FacilityDetail],

			[c].[CompanyTag],
			[c].[CompanyName],
			[c].[CompanyDetail]
		FROM
			[fact].[Submissions]			[s]
		INNER JOIN
			[org].[Groups]					[g]
				ON	[g].[GroupId]			= [s].[GroupId]
		INNER JOIN
			[org].[GroupFacilities]			[x]
				ON	[x].[GroupId]			= [g].[GroupId]
				--AND	[x].[GroupId]	IN (SELECT [g].[GroupId] FROM [org].[GroupFacilities] [g] GROUP BY [g].[GroupId] HAVING COUNT(1) = 1)
		INNER JOIN
			[org].[Facilities]				[f]
				ON	[f].[FacilityId]		= [x].[FacilityId]
		INNER JOIN
			[org].[JoinCompaniesFacilities]	[j]
				ON	[j].[FacilityId]		= [f].[FacilityId]
		INNER JOIN
			[org].[Companies]				[c]
				ON	[c].CompanyId			= [j].[CompanyId]
		WHERE
			[s].[SubmissionId]	= @SubmissionId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, N'[fact].[Select_Submissions]';
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;