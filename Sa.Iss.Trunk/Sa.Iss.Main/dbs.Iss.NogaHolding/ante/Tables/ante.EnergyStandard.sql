﻿CREATE TABLE [ante].[EnergyStandard]
(
	[EnergyStandardId]					INT					NOT	NULL	IDENTITY(1, 1),

	[MethodologyId]						INT					NOT	NULL	CONSTRAINT [FK_EnergyStandard_Methodology_LookUp]	REFERENCES [ante].[Methodology]([MethodologyId]),
	[CurrencyId]						INT					NOT	NULL	CONSTRAINT [FK_EnergyStandard_Currency_LookUp]		REFERENCES [dim].[Currency_LookUp]([CurrencyId]),
	[EntityId]							INT					NOT	NULL	CONSTRAINT [FK_EnergyStandard_Entity_LookUp]		REFERENCES [rpt].[Entities]([EntityId]),
	[AttributeId]						INT					NOT	NULL	CONSTRAINT [FK_EnergyStandard_Attribute_LookUp]		REFERENCES [rpt].[Attributes]([AttributeId]),

	[EnergyStandard_GJTonne]			FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_EnergyStandard_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_EnergyStandard_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_EnergyStandard_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_EnergyStandard_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_EnergyStandard_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_EnergyStandard]	PRIMARY KEY NONCLUSTERED([EnergyStandardId] ASC),
	CONSTRAINT [UK_EnergyStandard]	UNIQUE CLUSTERED([MethodologyId] ASC, [CurrencyId] ASC, [EntityId] ASC, [AttributeId] ASC)
);
GO

CREATE TRIGGER [ante].[t_EnergyStandard_u]
ON	[ante].[EnergyStandard]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[ante].[EnergyStandard]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[ante].[EnergyStandard].[EnergyStandardId]	= [i].[EnergyStandardId];

END;
