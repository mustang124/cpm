﻿CREATE TABLE [ante].[BaselineTarget]
(
	[BaselineTargetId]					INT					NOT	NULL	IDENTITY(1, 1),

	[MethodologyId]						INT					NOT	NULL	CONSTRAINT [FK_BaselineTarget_Methodology_LookUp]	REFERENCES [ante].[Methodology]([MethodologyId]),
	[CurrencyId]						INT					NOT	NULL	CONSTRAINT [FK_BaselineTarget_Currency_LookUp]		REFERENCES [dim].[Currency_LookUp]([CurrencyId]),
	[EntityId]							INT					NOT	NULL	CONSTRAINT [FK_BaselineTarget_Entity_LookUp]		REFERENCES [rpt].[Entities]([EntityId]),
	[AttributeId]						INT					NOT	NULL	CONSTRAINT [FK_BaselineTarget_Attribute_LookUp]		REFERENCES [rpt].[Attributes]([AttributeId]),

	[Value_Float]						FLOAT					NULL,
	[Value_Text]						NVARCHAR(256)			NULL,
	[Value_Date]						DATE					NULL,
	[Value]								AS COALESCE(CONVERT(NVARCHAR, [Value_Float]), [Value_Text], LEFT(CONVERT(NVARCHAR, [Value_Date], 102), 10), N'<No Value>')
										PERSISTED			NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_BaselineTarget_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_BaselineTarget_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_BaselineTarget_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_BaselineTarget_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_BaselineTarget_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_BaselineTarget]	PRIMARY KEY NONCLUSTERED([BaselineTargetId] ASC),
	CONSTRAINT [UK_BaselineTarget]	UNIQUE CLUSTERED([MethodologyId] ASC, [CurrencyId] ASC, [EntityId] ASC, [AttributeId] ASC)
);
GO

CREATE TRIGGER [ante].[t_BaselineTarget_u]
ON	[ante].[BaselineTarget]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[ante].[BaselineTarget]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[ante].[BaselineTarget].[BaselineTargetId]	= [i].[BaselineTargetId];

END;