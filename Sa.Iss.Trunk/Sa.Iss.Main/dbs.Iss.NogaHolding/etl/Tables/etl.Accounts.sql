﻿CREATE TABLE [etl].[Accounts]
(
	[AccountIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[XlsAccountDetail]					VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_Accounts_XlsAccountDetail]	CHECK([XlsAccountDetail] <> ''),
	[AccountTag]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Accounts_AccountTag]			CHECK([AccountTag] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountTag])),
	[EntityId]							AS CONVERT(INT, [rpt].[Return_EntityId]([AccountTag])),
	[AttributeId]						AS CONVERT(INT, [rpt].[Return_AttributeId]([AccountTag])),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Accounts_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Accounts_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Accounts_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Accounts_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Accounts_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Accounts]			PRIMARY KEY NONCLUSTERED([AccountIdx] ASC),
	CONSTRAINT [UK_Accounts]			UNIQUE CLUSTERED([XlsAccountDetail] ASC)
);
GO

CREATE TRIGGER [etl].[t_Accounts_u]
ON	[etl].[Accounts]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[etl].[Accounts]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[etl].[Accounts].[AccountIdx]	= [i].[AccountIdx];

END;