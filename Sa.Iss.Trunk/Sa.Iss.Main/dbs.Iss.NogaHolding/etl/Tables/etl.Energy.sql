﻿CREATE TABLE [etl].[Energy]
(
	[EnergyIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[EnergyType]						VARCHAR(3)			NOT	NULL	CONSTRAINT [CL_Energy_EnergyType]			CHECK([EnergyType] <> ''),
	[TransType]							CHAR(3)				NOT	NULL	CONSTRAINT [CL_Energy_TransType]			CHECK([TransType] <> ''),
	
	[AccountTag]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Energy_AccountTag]			CHECK([AccountTag] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountTag])),
	[EntityId]							AS CONVERT(INT, [rpt].[Return_EntityId]([AccountTag])),
	[AttributeId]						AS CONVERT(INT, [rpt].[Return_AttributeId]([AccountTag])),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Energy_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Energy]				PRIMARY KEY NONCLUSTERED([EnergyIdx] ASC),
	CONSTRAINT [UK_Energy]				UNIQUE CLUSTERED([EnergyType] ASC, [TransType] ASC)
);
GO

CREATE TRIGGER [etl].[t_Energy_u]
ON	[etl].[Energy]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[etl].[Energy]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[etl].[Energy].[EnergyIdx]	= [i].[EnergyIdx];

END;