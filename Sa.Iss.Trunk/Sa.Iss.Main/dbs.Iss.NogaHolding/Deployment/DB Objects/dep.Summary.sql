﻿CREATE TABLE [dep].[Summary]
(
	[DeploymentIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	/*	[BusinessUnitName]	*/
	[CompanyName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL_Submissions_CompanyName]		CHECK([CompanyName] <> ''),
	[CompanyId]							AS CONVERT(INT, [org].[Return_CompanyId]([CompanyName])),
	
	/*	[Location]			*/
	[FacilityName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL_Submissions_FacilitName]		CHECK([FacilityName] <> ''),
	[FacilityId]						AS CONVERT(INT, [org].[Return_FacilityId]([FacilityName])),

	[EntityDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Summary_EntityDetail]		CHECK([EntityDetail] <> ''),
	[HistDate]							NVARCHAR(24)		NOT	NULL,	CONSTRAINT [CL_Summary_HistDate]			CHECK([HistDate] <> ''),

	[Value]								FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Summary_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Summary_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Summary_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Summary_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Summary_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Summary]				PRIMARY KEY NONCLUSTERED([DeploymentIdx] ASC),
	CONSTRAINT [UK_Summary]				UNIQUE CLUSTERED([HistDate] ASC, [EntityDetail] ASC)
);
GO

CREATE TRIGGER [dep].[t_Summmary_u]
ON	[dep].[Summary]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dep].[Summary]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dep].[Summary].[DeploymentIdx]	= [i].[DeploymentIdx];

END;