﻿INSERT INTO [ante].[EnergyStandard]
(
	[MethodologyId],
	[CurrencyId],
	[EntityId],
	[AttributeId],
	[EnergyStandard_GJTonne]
)
SELECT
	[ante].[Return_MethodologyId]('2015'),
	[dim].[Return_CurrencyId]('USD'),
	[t].[EntityId],
	[t].[AttributeId],
	[t].[EnergyStandard_GJTonne]
FROM (VALUES
	([rpt].[Return_EntityId]('OSAmmoniaRefrig'),	[rpt].[Return_AttributeId]('EnergyConsGJDay'),		36.7),
	([rpt].[Return_EntityId]('OSMethanolDist'),		[rpt].[Return_AttributeId]('EnergyConsGJDay'),		33.0),
	([rpt].[Return_EntityId]('OSUrea'),				[rpt].[Return_AttributeId]('EnergyConsGJDay'),		 3.2705)
	) [t]([EntityId], [AttributeId], [EnergyStandard_GJTonne])
LEFT OUTER JOIN
	[ante].[EnergyStandard]	[b]
		ON	[b].[EntityId]		= [t].[EntityId]
		AND	[b].[AttributeId]	= [t].[AttributeId]
WHERE
	[b].[EnergyStandardId] IS NULL;