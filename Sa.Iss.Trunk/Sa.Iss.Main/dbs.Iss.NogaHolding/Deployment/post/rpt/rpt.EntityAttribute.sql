﻿DECLARE @EntityAttribute	TABLE
(
	[EntityAttributeId]					INT					NOT	NULL	IDENTITY(1, 1),

	[EntityId]							INT					NOT	NULL,
	[EntityTag]							VARCHAR(48)			NOT	NULL	UNIQUE NONCLUSTERED([EntityTag] ASC),
																		CHECK([EntityTag] <> ''),
	[EntityName]						NVARCHAR(96)		NOT	NULL	UNIQUE NONCLUSTERED([EntityName] ASC),
																		CHECK([EntityName] <> N''),
	[EntityDetail]						NVARCHAR(96)		NOT	NULL	UNIQUE NONCLUSTERED([EntityDetail] ASC),
																		CHECK([EntityDetail] <> N''),

	[AttributeId]						INT					NOT	NULL,
	[AttributeTag]						VARCHAR(48)			NOT	NULL	UNIQUE NONCLUSTERED([AttributeTag] ASC),
																		CHECK([AttributeTag] <> ''),
	[AttributeName]						NVARCHAR(96)		NOT	NULL	UNIQUE NONCLUSTERED([AttributeName] ASC),
																		CHECK([AttributeName] <> N''),
	[AttributeDetail]					NVARCHAR(96)		NOT	NULL	UNIQUE NONCLUSTERED([AttributeDetail] ASC),
																		CHECK([AttributeDetail] <> N''),

	PRIMARY KEY NONCLUSTERED([EntityAttributeId] ASC),
	UNIQUE CLUSTERED([EntityId] ASC, [AttributeId] ASC)
);

INSERT INTO @EntityAttribute
(
	[EntityId],
	[EntityTag],
	[EntityName],
	[EntityDetail],
	[AttributeId],
	[AttributeTag],
	[AttributeName],
	[AttributeDetail]
)
SELECT
	[t].[EntityId],
	[t].[EntityTag],
	[t].[EntityName],
	[t].[EntityDetail],

	[t].[AttributeId],
	[t].[AttributeTag],
	[t].[AttributeName],
	[t].[AttributeDetail]
	FROM (VALUES
	([rpt].[Return_EntityId]('OpEx'), 'OpEx', 'Cash Operating Expense', 'Cash Operating Expense',
		[rpt].[Return_AttributeId]('RptExpCentsUedc'), 'OpEx', 'Cash Operating Expense', 'Cash Operating Expense'),

	([rpt].[Return_EntityId]('OpExLaborMaterials'), 'PersCostIdx', 'Personnel Cost Index', 'Personnel Cost Index',
		[rpt].[Return_AttributeId]('RptExpEdcAnnualized'), 'PersCostIdx', 'Personnel Cost Index', 'Personnel Cost Index'),

	([rpt].[Return_EntityId]('ProcUnits'), 'Util', 'Utilization', 'Utilization',
		[rpt].[Return_AttributeId]('uCap_Pcnt'), 'Util', 'Utilization', 'Utilization'),

	([rpt].[Return_EntityId]('OSAmmoniaRefrig'), 'CapAmmonia', 'Ammonia Capacity (Tonne/Day)', 'Ammonia Capacity (Tonne/Day)',
		[rpt].[Return_AttributeId]('Cap'), 'CapAmmonia', 'Ammonia Capacity (Tonne/Day)', 'Ammonia Capacity (Tonne/Day)'),
	([rpt].[Return_EntityId]('OSMethanolDist'), 'CapMethanol', 'Methanol Capacity (Tonne/Day)', 'Methanol Capacity (Tonne/Day)',
		[rpt].[Return_AttributeId]('Cap'), 'CapMethanol', 'Methanol Capacity (Tonne/Day)', 'Methanol Capacity (Tonne/Day)'),
	([rpt].[Return_EntityId]('OSUrea'), 'CapUrea', 'Urea Capacity (Tonne/Day)', 'Urea Capacity (Tonne/Day)',
		[rpt].[Return_AttributeId]('Cap'), 'CapUrea', 'Urea Capacity (Tonne/Day)', 'Urea Capacity (Tonne/Day)'),

	([rpt].[Return_EntityId]('OSAmmoniaRefrig'), 'ProdAmmonia', 'Ammonia Production (Tonne/Day)', 'Ammonia Production (Tonne/Day)',
		[rpt].[Return_AttributeId]('Prod'), 'ProdAmmonia', 'Ammonia Production (Tonne/Day)', 'Ammonia Production (Tonne/Day)'),
	([rpt].[Return_EntityId]('OSMethanolDist'), 'ProdMethanol', 'Methanol Production (Tonne/Day)', 'Methanol Production (Tonne/Day)',
		[rpt].[Return_AttributeId]('Prod'), 'ProdMethanol', 'Methanol Production (Tonne/Day)', 'Methanol Production (Tonne/Day)'),
	([rpt].[Return_EntityId]('OSUrea'), 'ProdUrea', 'Urea Production (Tonne/Day)', 'Urea Production (Tonne/Day)',
		[rpt].[Return_AttributeId]('Prod'), 'ProdUrea', 'Urea Production (Tonne/Day)', 'Urea Production (Tonne/Day)')

	--([rpt].[Return_EntityId](''), '', '', '',
	--[rpt].[Return_AttributeId](''), '', '', '')

	) [t]([EntityId], [EntityTag], [EntityName], [EntityDetail],
			[AttributeId], [AttributeTag], [AttributeName], [AttributeDetail]);

INSERT INTO [rpt].[EntityAttribute]
(
	[EntityId],
	[AttributeId],
	[EntityTag],
	[EntityName],
	[EntityDetail],
	[AttributeTag],
	[AttributeName],
	[AttributeDetail]
)
SELECT
	[t].[EntityId],
	[t].[AttributeId],
	[t].[EntityTag],
	[t].[EntityName],
	[t].[EntityDetail],
	[t].[AttributeTag],
	[t].[AttributeName],
	[t].[AttributeDetail]
FROM
	@EntityAttribute			[t]
LEFT OUTER JOIN
	[rpt].[EntityAttribute]		[x]
		ON	[x].[EntityId]		= [t].[EntityId]
		AND	[x].[AttributeId]	= [t].[AttributeId]
WHERE	[x].[EntityAttributeId] IS NULL;
