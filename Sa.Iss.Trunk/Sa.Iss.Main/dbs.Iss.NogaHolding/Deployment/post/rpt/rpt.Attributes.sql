﻿DECLARE @Attributes	TABLE
(
	[AttributeTag]						VARCHAR(48)			NOT	NULL	UNIQUE CLUSTERED([AttributeTag] ASC),
																		CHECK([AttributeTag] <> ''),
	[AttributeName]						NVARCHAR(96)		NOT	NULL	UNIQUE NONCLUSTERED([AttributeName] ASC),
																		CHECK([AttributeName] <> N''),
	[AttributeDetail]					NVARCHAR(96)		NOT	NULL	UNIQUE NONCLUSTERED([AttributeDetail] ASC),
																		CHECK([AttributeDetail] <> N''),
	[AttributeParent]					VARCHAR(48)			NOT	NULL
);

INSERT INTO @Attributes
(
	[AttributeTag],
	[AttributeName],
	[AttributeDetail],
	[AttributeParent]
)
SELECT
	[t].[AttributeTag],
	[t].[AttributeName],
	[t].[AttributeDetail],
	[t].[AttributeParent]
FROM (VALUES
	--	Production Divisor
	('Jan', 'Jan', 'Jan', ''), 
	('Feb', 'Feb', 'Feb', ''), 
	('Mar', 'Mar', 'Mar', ''), 
	('Apr', 'Apr', 'Apr', ''), 
	('May', 'May', 'May', ''), 
	('Jun', 'Jun', 'Jun', ''), 
	('Jul', 'Jul', 'Jul', ''), 
	('Aug', 'Aug', 'Aug', ''), 
	('Sep', 'Sep', 'Sep', ''), 
	('Oct', 'Oct', 'Oct', ''), 
	('Nov', 'Nov', 'Nov', ''), 
	('Dec', 'Dec', 'Dec', ''),

	('val', 'Value', 'Value', ''),

	--	EDC
	('Unit', 'Unit count', 'Number of Units', ''),

	('Uom', 'Unit of Measurement', 'Unit of Measurement', ''),
	('kEdc', 'kEDC', 'kEDC', ''),
	('ukEdc', 'Util. kEDC', 'Utilized kEDC', ''),
	('Cap', 'Capacity', 'Total Capacity', ''),
	('Prod', 'Production', 'Production', ''),

	('uCap', 'Util .Capacity', 'Utilized Capacity', ''),
	('uCap_Pcnt', 'Util. Capacity (Percent)', 'Utilized Capacity (Percent)', ''),

	--	Factors&Standards
	('Weighting', 'Weighting Factor', 'Weighting Factor', ''),

	('Eii', 'EII', 'EII', ''),
	('Nei', 'NEI', 'NEI', ''),
	('Pei', 'PEI', 'PEI', ''),
	('Mei', 'MEI', 'MEI', ''),

	('Eii_MBtu', 'EII Standard Energy (MBTU/day)', 'EII Standard Energy (MBTU/day)', ''),
	('Eii_GJ', 'EII Standard Energy (GJ/day)', 'EII Standard Energy (GJ/day)', ''),
	('Pes', 'Personnel Efficiency Standard', 'Personnel Efficiency Standard (k work hours', ''),
	('Mes', 'Maintenance Efficiency Standard', 'Maintenance Efficiency Standard (k USD)', ''),
	('NeCes', 'Non-Energy Cost Efficiency Standard', 'Non-Energy Cost Efficiency Standard (k USD)', ''),

	('MaintIdx', 'Maintenance Index', 'Maintenance Index', ''),
	('PersIdx', 'Personnel Index', 'Personnel Index', ''),
	('FeedFuelIdx', 'Feed and Fuel Index', 'Feed and Fuel Index', ''),

	('AvailMech', 'Mechanical Availability', 'Mechanical Availability', ''),
	('AvailOper', 'Operational Availability', 'Operational Availability', ''),

	--	Eii
	('Eii_Pcnt', 'Energy Standard (Percent)', 'Energy Standard (Percent of Total)', ''),

	--	Yield
	('Mole_Pcnt', 'Mole (Percent)', 'Mole (Percent)', ''),
	('Mass_MW', 'Mass (Molecular Weight)', 'Mass (Molecular Weight)', ''),
	('Mass_Tonnes', 'Mass (Tonne)', 'Mass (Tonne)', ''),
	('Weight_Pcnt', 'Weight (Percent)', 'Weight (Percent)', ''),
	('LHV_kJNm3', N'Lower Heating Value (kJ/NM³)', N'Lower Heating Value (kJ/NM³)', ''),
	('LHV_GJd', N'Lower Heating Value (GJ/Day)', N'Lower Heating Value (GJ/Day)', ''),
	('HVCYield_Pcnt', N'High-Value Chemical Yield (Percent)', N'High-Value Chemical Yield (Percent)', ''),

	('Quantity_TonnesMonth', N'Quantity (Tonne/Month)', N'Quantity (Tonne/Month)', ''),
	('Quantity_TonnesDay', N'Quantity (Tonne/Day)', N'Quantity (Tonne/Day)', ''),
	('Loss_True', N'Loss (Tonne)', N'Loss (Tonne)', ''),
	('Loss_PcntFeed', N'Loss (Percent of Raw Material Input)', N'Loss (Percent of Raw Material Input)', ''),

	--	OpEx
	('RptExp', 'Reported Expenses', 'Reported Expenses', ''),
	('RptExpCentsUedc', 'Reported Expenses USD Cents per UEDC', 'Reported Expenses USD Cents per UEDC', ''),
	('RptExpHvc', 'Reported Expenses per Tonne HVC', 'Reported Expenses per Tonne High-Value Chemicals', ''),

	('RptExpAnnualized', 'Annualized Expenses', 'Annualized Expenses', ''),
	('RptExpEdcAnnualized', 'Annualized Expenses (USD/EDC)', 'Annualized Expenses (USD/EDC)', ''),

	--	Energy
	('EnergyConsGJ', 'Energy Consumption (GJ)', 'Energy Consumption (Gigajoules)', ''),
	('EnergyConsGJTonne', 'Energy Consumption (GJ/Tonne)', 'Energy Consumption (GJ/Tonne)', ''),
	('EnergyConsGJDay', 'Energy Consumption (GJ/Day)', 'Energy Consumption (GJ/Day)', ''),
	('EnergyConsGJHvc', 'Energy Consumption per Tonne HVC (GJ/HVC)', 'Energy Consumption per Tonne High-Value Chemicals (GJ/HVC)', ''),
	('EnergyConsMBTU', 'Energy Consumption (MBTU)', 'Energy Consumption (MBTU)', ''),

	--	Personnel
	('WorkHours', 'Work Hours', 'Work Hours', ''),
	('WorkHoursAnnualized', 'Work Hours Annualized', 'Work Hours Annualized', ''),

	--	Maintenance
	('CostkUSD', 'Cost (k USD)', 'Cost (k USD)', ''),
	('CostkUSDAnnualized', 'Cost Annualized (k USD)', 'Cost Annualized(k USD)', ''),

	--	Reliability
	('RelRegulatory_Hours', 'Regulatory and Process Related (Hrs)', 'Regulatory and Process Related (Hours)', ''),
	('RelMechanical_Hours', 'Mechanical Related (Hrs)', 'Mechanical Related (Hours)', ''),
	('RelTotal_Hours', 'Total (Hrs)', 'Total (Hours)', ''),

	('RelRegulatory_HoursEdc', 'Regulatory and Process Related (Hrs, EDC Weighted)', 'Regulatory and Process Related (Hours, EDC Weighted)', ''),
	('RelMechanical_HoursEdc', 'Mechanical Related (Hrs, EDC Weighted)', 'Mechanical Related (Hours, EDC Weighted)', ''),
	('RelTotal_HoursEdc', 'Total (Hours, EDC Weighted)', 'Total (Hrs, EDC Weighted)', ''),

	--	Turnaround
	('TaIntervalYear', 'Interval (Yrs)', 'Turnaround Interval (Years)', ''),
	('TaDowntimeHours', 'Downtime (Hrs)', 'Turnaround Downtime (Hours)', ''),

	('TaDowntimeDurRpt', 'Reported Downtime (Hrs)', 'Reported Turnaround Downtime (Hours)', ''),
	('TaDowntimeCostRpt', 'Reported Cost (k USD)', 'Reported Cost (k USD)', ''),
	('TaDowntimeWorkRpt', 'Reported Workhours', 'Reported Workhours', ''),

	('TaDowntimeDurAnn', 'Annualized Downtime (Hrs)', 'Annualized Turnaround Downtime (Hours)', ''),
	('TaDowntimeCostAnn', 'Annualized Cost (k USD)', 'Annualized Cost (k USD)', ''),
	('TaDowntimeWorkAnn', 'Annualized Workhours', 'Annualized Workhours', ''),

	('TaDowntimeDurAnnEdc', 'Annual Downtime EDC Weighted (Hrs)', 'Annual Turnaround Downtime EDC Weighted (Hours)', ''),
	('TaDowntimeDurAllocEdc', 'Allocated Downtime EDC Weighted (Hrs)', 'Allocated Turnaround Downtime EDC Weighted (Hours)', ''),
	('TaDowntimeCostAlloc', 'Allocated Cost (k USD)', 'Allocated Cost (k USD)', ''),
	('TaDowntimeWorkAlloc', 'Allocated Workhours', 'Allocated Workhours', ''),

	--	Reporting Period

	('RptPerName', 'Period Name', 'Reporting Period Name', ''),
	('RptPerBeg_Date', 'Period Begin', 'Period Begin', ''),
	('RptPerEnd_Date', 'Period End', 'Period End', ''),

	('RptPerDuration_Days', 'Duration Days', 'Duration Days', ''),
	('RptPerDuration_Hours', 'Duration Hours', 'Duration Hours', ''),
	('RptPerDuration_PcntYear', 'Duration Year (Percent)', 'Duration Year (Percent)', ''),
	('RptPerDuration_Multiplier', 'Duration Year (Multiplier)', 'Duration Year (Multiplier)', ''),
	('RptPerDaysInYear', 'Days in Year', 'Days in Reporting Year', '')

	) [t]([AttributeTag], [AttributeName], [AttributeDetail], [AttributeParent]);

INSERT INTO [rpt].[Attributes]
(
	[AttributeTag],
	[AttributeName],
	[AttributeDetail]
)
SELECT
	[a].[AttributeTag],
	[a].[AttributeName],
	[a].[AttributeDetail]
FROM
	@Attributes			[a]
LEFT OUTER JOIN
	[rpt].[Attributes]	[x]
		ON	[x].[AttributeTag]	= [a].[AttributeTag]
WHERE	[x].[AttributeId] IS NULL
ORDER BY
	[a].[AttributeName] ASC;