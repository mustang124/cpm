﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

:r .\post\dim\dim.Operator.sql
:r .\post\dim\dim.Currency.sql
:r .\post\dim\dim.ProcessUnits.sql

:r .\post\ante\ante.Methodology.sql

:r .\post\etl\etl.Accounts.sql
:r .\post\etl\etl.Divisors.sql
:r .\post\etl\etl.ProcessUnits.sql
:r .\post\etl\etl.Composition.sql
:r .\post\etl\etl.Streams.sql
:r .\post\etl\etl.Energy.sql

:r .\post\rpt\rpt.Attributes.sql
:r .\post\rpt\rpt.Entities.sql
:r .\post\rpt\rpt.EntityAttribute.sql
:r .\post\rpt\rpt.sql

:r .\post\ante\ante.BaselineTarget.sql
:r .\post\ante\ante.KpiNormalization.sql
:r .\post\ante\ante.EnergyStandard.sql

:r .\post\org\org.Companies.sql
