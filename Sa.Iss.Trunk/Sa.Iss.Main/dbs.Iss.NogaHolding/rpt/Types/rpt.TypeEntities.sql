﻿CREATE TYPE [rpt].[TypeEntities] AS TABLE
(
	[EntityId]			INT		NOT	NULL,
	[EntitySortKey]		INT		NOT	NULL,
	PRIMARY KEY CLUSTERED([EntityId])
);