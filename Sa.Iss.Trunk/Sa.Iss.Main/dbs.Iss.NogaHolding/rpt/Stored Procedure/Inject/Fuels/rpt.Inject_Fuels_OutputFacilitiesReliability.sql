﻿CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilitiesReliability]
(
	@fctSubmissionId			INT,
	@profileSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @FactorSetID			CHAR(8)		= '2015';
		DECLARE @CurrencyId				CHAR(4)		= 'USD';

		INSERT INTO [$(DatabaseName)].[rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionID]		= @fctSubmissionId,
				[MethodologyId]		= [$(DatabaseName)].[ante].[Return_MethodologyId](@FactorSetID),
				[CurrencyId]		= [$(DatabaseName)].[dim].[Return_CurrencyId](@CurrencyId),
				[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]([e].[ProcessUnitTag]),
				[AttributeId]		= [$(DatabaseName)].[rpt].[Return_AttributeId]([u].[AttributeTag]),
			[u].[Hours]
		FROM (
			SELECT
				[m].[SubmissionID],
					[ProcessID]				= COALESCE([m].[ProcessID], 'TotProc'),
					[RelRegulatory_Hours]	= SUM(COALESCE([m].[RegDown], 0.0) + COALESCE([m].[OthDown], 0.0)),
					[RelMechanical_Hours]	= SUM(COALESCE([m].[MaintDown], 0.0)),
					[RelTotal_Hours]		= SUM(COALESCE([m].[RegDown], 0.0) + COALESCE([m].[OthDown], 0.0) + COALESCE([m].[MaintDown], 0.0))
			FROM
				[$(dbProfile)].[dbo].[MaintRout]		[m]
			WHERE	[m].[ProcessID]		<> ''
				AND	[m].[SubmissionID]		= @profileSubmissionId
			GROUP BY
				[m].[SubmissionID],
				ROLLUP([m].[ProcessID])
			) [p]
			UNPIVOT (
				[Hours] FOR [AttributeTag] IN (
					[RelRegulatory_Hours],
					[RelMechanical_Hours],
					[RelTotal_Hours]
				)
			) [u]
		LEFT OUTER JOIN
			[$(DatabaseName)].[etl].[ProcessUnits]	[e]
				ON	[e].[XlsProcessUnitDetail] = [u].[ProcessID];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @profileSubmissionId = COALESCE(@profileSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @profileSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;