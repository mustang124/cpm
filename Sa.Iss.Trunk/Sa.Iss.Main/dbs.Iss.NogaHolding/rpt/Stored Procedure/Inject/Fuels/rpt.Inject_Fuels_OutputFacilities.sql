﻿
CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilities]
(
	@profileSubmissionId		INT
)
AS
BEGIN


	SET NOCOUNT ON;

		
		DECLARE @fctSubmissionId	INT;

		EXECUTE @fctSubmissionId	= [fact].[Inject_Fuels_Submission]	@profileSubmissionId;

		EXECUTE [rpt].[Inject_Fuels_OutputFacilitiesOpEx]		@fctSubmissionId, @profileSubmissionId;
		EXECUTE [rpt].[Inject_Fuels_OutputFacilitiesEnergy]		@fctSubmissionId, @profileSubmissionId;
		EXECUTE [rpt].[Inject_Fuels_OutputFacilitiesPersonnel]  @fctSubmissionId, @profileSubmissionId;
		EXECUTE [rpt].[Inject_Fuels_OutputFacilitiesReliability] @fctSubmissionId, @profileSubmissionId;
		EXECUTE [rpt].[Inject_Fuels_OutputFacilitiesTurnaround] @fctSubmissionId, @profileSubmissionId;

END;