﻿CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilitiesSummary]
(
	@fctSubmissionId			INT,
	@profileSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @FactorSetID			CHAR(8)		= '2015';
		DECLARE @CurrencyId				CHAR(4)		= 'USD';

		DECLARE @profileFactorSetID		CHAR(8)		= '2012';
		DECLARE @profileScenarioId		CHAR(8)		= 'CLIENT';
		DECLARE @profileCurrencyId		CHAR(4)		= 'USD';
		DECLARE @profileDataTypeId		CHAR(6)		= 'ADJ';
		DECLARE @profileUomId			CHAR(3)		= 'MET';

		DECLARE @Etl	TABLE
		(
			[EaTag]				VARCHAR(16)	NOT	NULL	CHECK([EaTag] <> ''),
			[EntityTag]			VARCHAR(16)	NOT	NULL	CHECK([EntityTag] <> ''),
			[AttributeTag]		VARCHAR(24)	NOT	NULL	CHECK([AttributeTag] <> ''),
			PrIMARY KEY CLUSTERED([EaTag] ASC),
			UNIQUE NONCLUSTERED([EntityTag] ASC, [AttributeTag] ASC)
		);

		INSERT INTO @Etl
		(
			[EaTag],
			[EntityTag],
			[AttributeTag]
		)
		SELECT
			[t].[EaTag],
			[t].[EntityTag],
			[t].[AttributeTag]
		FROM(VALUES
			--	20	EDC
			('kEdc', 'ProcUnits', 'kEdc'),
			--	21	Utilized EDC
			('ukEdc', 'ProcUnits', 'ukEdc'),
			--	22	Utilization Rate, percent
			('UtilPcnt', 'ProcUnits', 'uCap_Pcnt'),
			--	28	Cash Operating Expense, US Cents/UEDC	 (units??)
			('TotCashOpExUEDC', 'OpEx', 'RptExpCentsUedc'),
			--	29	Non-Energy Cash Operating Expense, $/EDC
			('NEOpExEDC', 'OpExNe', 'RptExpEdcAnnualized'),
			--	30	Non-Energy Efficiency Index (NEI)
			('NEI', 'Nei', 'Nei'),
			--	33	Energy Intensity Index (EII)
			('EII', 'Eii', 'Eii'),
			--	35	Non-Turnaround Maintenance Index, $/EDC
			('RoutIndex', 'MaintRout', 'MaintIdx'),
			--	36	Turnaround Maintenance Index, $/EDC
			('TAIndex', 'MaintTA', 'MaintIdx'),
			--	37	Maintenance Index, $/EDC
			('MaintIndex', 'OpExMaintRoutTA', 'MaintIdx'),
			--	38	Maintenance Efficiency Index (MEI)
			('MEI', 'Mei', 'Mei'),
			--	39	Operational Availability, percent of time
			('OpAvail', 'AvailOper', 'AvailOper'),
			--	40	Mechanical Availability, percent of time
			('MechAvail', 'AvailMech', 'AvailMech'),
			--	42	Personnel Index, hours/100 EDC
			('TotWHrEDC', 'PersIdx', 'PersIdx'),
			--	44	Personnel Efficiency Index (PEI)
			('PEI', 'Pei', 'Pei')
			) [t]([EaTag], [EntityTag], [AttributeTag])

		INSERT INTO [$(DatabaseName)].[rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionID]		= @fctSubmissionId,
				[MethodologyId]		= [$(DatabaseName)].[ante].[Return_MethodologyId](@FactorSetID),
				[CurrencyId]		= [$(DatabaseName)].[dim].[Return_CurrencyId](@CurrencyId),
				[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]([e].[EntityTag]),
				[AttributeId]		= [$(DatabaseName)].[rpt].[Return_AttributeId]([e].[AttributeTag]),
			[u].[Value_Float]
		FROM(
			SELECT
				[p].[SubmissionID],
					[kEdc]				= CONVERT(REAL, [p].[EDC]),
					[ukEdc]				= CONVERT(REAL, [p].[UEDC]),
				[p].[UtilPcnt],
				[p].[TotCashOpExUEDC],
				[p].[NEOpExEDC],
				[p].[NEI],
				[p].[EII],
				[p].[RoutIndex],
				[p].[TAIndex],
				[p].[MaintIndex],
				[p].[MEI],
				[p].[OpAvail],
				[p].[MechAvail],
				[p].[TotWHrEDC],
				[p].[PEI]
			FROM
				[$(dbProfile)].[dbo].[GenSum]	[p]
			WHERE	[p].[FactorSet]		= @profileFactorSetID
				AND	[p].[Scenario]		= @profileScenarioId
				AND	[p].[Currency]		= @profileCurrencyId
				AND	[p].[UOM]			= @profileUomId
				AND	[p].[SubmissionID]	= @profileSubmissionId
			) [p]
			UNPIVOT(
				[Value_Float] FOR [EaTag] IN
				(
					[kEdc],
					[ukEdc],
					[UtilPcnt],
					[TotCashOpExUEDC],
					[NEOpExEDC],
					[NEI],
					[EII],
					[RoutIndex],
					[TAIndex],
					[MaintIndex],
					[MEI],
					[OpAvail],
					[MechAvail],
					[TotWHrEDC],
					[PEI]
				)
			) [u]
		LEFT OUTER JOIN	@Etl	[e]
			ON	[e].[EaTag]	= [u].[EaTag];


	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @profileSubmissionId = COALESCE(@profileSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @profileSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;