﻿CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilitiesPersonnel]
(
	@fctSubmissionId		INT,
	@profileSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY
			INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionID]		= @fctSubmissionId,
				[MethodologyId]		= [$(DatabaseName)].[ante].[Return_MethodologyId]('2015'),
				[CurrencyId]		= [$(DatabaseName)].[dim].[Return_CurrencyId]('USD'),
				[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]([u].[EntityTag]),
				[AttributeId]		= [$(DatabaseName)].[rpt].[Return_AttributeId]('WorkHours'),
			[u].[WorkHours]
		FROM (
			SELECT
				[p].[SubmissionID],
				[GeneralMaintCompanyLabor]	= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[CompWHr]	END),
				[GeneralMaintContractLabor]	= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[ContWHr]	END),
				[AllocGAPers]				= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[GAWHr]	END),
				[GeneralMaint]				= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[TotWHr]	END),
				[MaintTaLabor]				= SUM(CASE WHEN [p].[PersID]		IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[TotWHr]	END),
				[OpExLaborMaterials]		= SUM([p].[TotWHr])
			FROM
				[$(dbProfile)].[dbo].[Pers]	[p]
			WHERE
				[p].[SubmissionID]		= @profileSubmissionId
			GROUP BY
				[p].[SubmissionID]
			) [p]
			UNPIVOT (
				[WorkHours] FOR [EntityTag] IN (
					[GeneralMaintCompanyLabor],
					[GeneralMaintContractLabor],
					[AllocGAPers],
					[GeneralMaint],
					[MaintTaLabor],
					[OpExLaborMaterials]
				)
			) [u]


	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @profileSubmissionId = COALESCE(@profileSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @profileSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;