﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilities_NeiWeighting]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
			[SubmissionId]	= @fctSubmissionId,
			[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
			[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
			[EntityId]		= [rpt].[Return_EntityId]('Nei'),
			[AttributeId]	= [rpt].[Return_AttributeId]('Weighting'),
			[Value_Float]	= [e].[kEdc] * [a].[Amount_kUsdEdcAnnualized] / ([a].[Amount_kUsdAnnualized] / [d].[NeCes_kUsd])
		FROM
			[xls].[Edc]							[e]
		INNER JOIN
			[etl].[ProcessUnits]				[x]
				ON	[x].[XlsProcessUnitDetail]	= [e].[ProcessUnitDetail]
				AND	[x].[ProcessUnitTag]		= 'ProcUnits'
		INNER JOIN
			[xls].[OpExAllocation]				[a]
				ON	[a].[SubmissionId]			= [e].[SubmissionId]
		INNER JOIN
			[etl].[Accounts]					[y]
				ON	[y].[XlsAccountDetail]		= [a].[AccountDetail]
				AND	[y].[AccountTag]			= 'OpExNe'
		INNER JOIN
			[xls].[FactorsAndStandards]			[d]
				ON	[d].[SubmissionId]			= [e].[SubmissionId]
		INNER JOIN
			[etl].[ProcessUnits]				[p]
				ON	[p].[XlsProcessUnitDetail]	= [d].[ProcessUnitDetail]
				AND	[p].[ProcessUnitTag]		= 'ProcUnits'
		WHERE
			[e].[SubmissionId]	= @stgSubmissionId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;