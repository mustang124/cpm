﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesEdc]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Text]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[AttributeId],
			[t].[Value_Text]
		FROM (
			SELECT
				[t].[SubmissionId],
					[MethodologyId]			= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]			= [dim].[Return_CurrencyId]('USD'),
				[r].[EntityId],
				[a].[AttributeId],
					[Value_Text]			= [t].[Capacity_UomName]
			FROM
				[xls].[Edc]							[t]
			INNER JOIN
				[etl].[ProcessUnits]				[e]
					ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitDetail]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
			INNER JOIN
				[rpt].[Attributes]					[a]
					ON	[a].[AttributeTag]			 = 'Uom'
			WHERE	[t].[Capacity_UomName] IS NOT NULL
				AND	[t].[SubmissionId]	= @stgSubmissionId
			) [t]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [t].[MethodologyId]
				AND	[r].[CurrencyId]	= [t].[CurrencyId]
				AND	[r].[AttributeId]	= [t].[AttributeId]
				AND	[r].[EntityId]		= [t].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]		= 1,

					[CurrencyId]		= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]		= 'USD',
				[r].[EntityId],
				[r].[EntityName],

				[Unit]	= CONVERT(FLOAT, [t].[Unit_Count]),
				[Cap]	= [t].[Capacity],
				--[t].[Capacity_UomName],
				[t].[kEdc],
				[t].[ukEdc],
				[uCap_Pcnt]	= [t].[UtilizedCapacity_Pcnt] * 100.0
			FROM
				[xls].[Edc]							[t]
			INNER JOIN
				[etl].[ProcessUnits]				[e]
					ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitDetail]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
			WHERE
				[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value] FOR [AttributeTag] IN ([Unit], [Cap], [kEdc], [ukEdc], [uCap_Pcnt])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;