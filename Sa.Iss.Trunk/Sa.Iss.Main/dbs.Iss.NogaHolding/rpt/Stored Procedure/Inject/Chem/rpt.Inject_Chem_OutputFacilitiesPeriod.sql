﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesPeriod]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Text]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Text]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]				= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]				= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]				= 'USD',
				[r].[EntityId],
				[r].[EntityName],

					[RptPerName]				= CONVERT(VARCHAR, [t].[PeriodName]),
					[RptPerBeg_Date]			= CONVERT(VARCHAR, [t].[PeriodBeg_Date], 106),
					[RptPerDaysInYear]			= CONVERT(VARCHAR, [t].[DaysInYear]),
					[RptPerDuration_Hours]		= CONVERT(VARCHAR, [t].[Duration_Hours]),
					[RptPerDuration_Days]		= CONVERT(VARCHAR, [t].[Duration_Days]),
					[RptPerDuration_PcntYear]	= CONVERT(VARCHAR, [t].[Duration_Fraction]),
					[RptPerDuration_Multiplier]	= CONVERT(VARCHAR, [t].[Duration_Multiplier])

			FROM
				[xls].[ReportingPeriod]				[t]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= 'DivPeriod'
			WHERE
				[t].[SubmissionId]		= @stgSubmissionId
				) [t]
			UNPIVOT (
				[Value_Text] FOR [AttributeTag] IN ([RptPerName], [RptPerBeg_Date], [RptPerDaysInYear], [RptPerDuration_Hours], [RptPerDuration_Days], [RptPerDuration_PcntYear], [RptPerDuration_Multiplier])
				) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;