﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesDivisors]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]		= @fctSubmissionId,
				[MethodologyId]		= [ante].[Return_MethodologyId]('2015'),
				[CurrencyId]		= [dim].[Return_CurrencyId]('USD'),
			[r].[EntityId],
				[AttributeId]		= [rpt].[Return_AttributeId]('val'),
			[t].[Value]
		FROM
			[xls].[Divisors]					[t]
		INNER JOIN
			[etl].[Divisors]					[e]
				ON	[e].[XlsDivisorDetail]		= [t].[DivisorDetail]
		INNER JOIN
			[rpt].[Entities]					[r]
				ON	[r].[EntityTag]				= [e].[DivisorTag]
		WHERE
			[t].[SubmissionId]	 = @stgSubmissionId

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;