﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilities_PersIdx]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
				[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
				[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
				[EntityId]		= [rpt].[Return_EntityId]('PersIdx'),
				[AttributeId]	= [rpt].[Return_AttributeId]('PersIdx'),
			[n].[Work_HoursAnnualized] / [d].[kEdc] / 10.0
		FROM
			[xls].[PersonnelAllocation]			[n]
		INNER JOIN
			[etl].[Accounts]					[a]
				ON	[a].[XlsAccountDetail]		= [n].[AccountDetail]
				AND	[a].[AccountTag]			= 'OpExLaborMaterials'
		INNER JOIN
			[xls].[Edc]							[d]
				ON	[d].[SubmissionId]			= [n].[SubmissionId]
		INNER JOIN
			[etl].[ProcessUnits]				[p]
				ON	[p].[XlsProcessUnitDetail]	= [d].[ProcessUnitDetail]
				AND	[p].[ProcessUnitTag]		= 'ProcUnits'
		WHERE
			[n].[SubmissionId]	= @stgSubmissionId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;