﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilities]
(
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	--BEGIN TRY

		DECLARE @fctSubmissionId	INT;

		EXECUTE @fctSubmissionId	= [fact].[Inject_Chem_Submission]	@stgSubmissionId;

		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesEdc]						@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesFS]						@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesEii]						@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesDivisors]				@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesOpEx]					@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesPeriod]					@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesEnergy]					@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesPersonnel]				@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesMaintenance]				@fctSubmissionId, @stgSubmissionId;

		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesReliability]				@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesProcessUnitProd]			@fctSubmissionId, @stgSubmissionId;

		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesTurnaround]				@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesTurnaroundAlloc]			@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesTurnaroundEdc]			@fctSubmissionId, @stgSubmissionId;

		EXECUTE [rpt].[Inject_Chem_OutputFacilities_Eii]					@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_Nei]					@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_PersIdx]				@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_Pei]					@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_MaintIdx]				@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_Mei]					@fctSubmissionId, @stgSubmissionId;

		EXECUTE [rpt].[Inject_Chem_OutputFacilities_EiiWeighting]			@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_NeiWeighting]			@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_MeiWeighting]			@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilities_PeiWeighting]			@fctSubmissionId, @stgSubmissionId;

		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesReliabilityAvailability]	@fctSubmissionId, @stgSubmissionId;

		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesYield]					@fctSubmissionId, @stgSubmissionId;
		EXECUTE [rpt].[Inject_Chem_OutputFacilitiesFeedFuelIndex]			@fctSubmissionId, @stgSubmissionId;

	--END TRY
	--BEGIN CATCH

	--	DECLARE @XActState	INT = XACT_STATE();
	--	SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
	--	EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
	--	THROW;

	--	RETURN ERROR_NUMBER();

	--END CATCH;

END;