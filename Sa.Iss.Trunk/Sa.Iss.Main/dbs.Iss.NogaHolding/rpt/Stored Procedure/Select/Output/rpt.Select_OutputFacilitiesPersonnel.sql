﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesPersonnel]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('GeneralMaintCompanyLabor'),	10),	--	PersRoutCmpny
			([rpt].[Return_EntityId]('GeneralMaintContractLabor'),	20),	--	PersRoutCntrct
			([rpt].[Return_EntityId]('AllocGA'),					30),	--	PersRoutAllcSGA
			([rpt].[Return_EntityId]('GeneralMaint'),				40),	--	PersRout
			([rpt].[Return_EntityId]('MaintTaLabor'),				50),	--	PersTA
			([rpt].[Return_EntityId]('OpExLaborMaterials'),			60);	--	OpExPers

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('WorkHours'),			10),
			([rpt].[Return_AttributeId]('WorkHoursAnnualized'),	20);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;