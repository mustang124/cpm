﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesEdc]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('OSAmmoniaSyngas'),		20),
			([rpt].[Return_EntityId]('OSAmmoniaRefrig'),		30),
			([rpt].[Return_EntityId]('OSMethanolSyngas'),		40),
			([rpt].[Return_EntityId]('OSMethanolDist'),			50),
			([rpt].[Return_EntityId]('OSUrea'),					60),
			([rpt].[Return_EntityId]('OSPsaH2Pur'),				70),
			([rpt].[Return_EntityId]('OSFlueGasCo2Recov'),		80),
			([rpt].[Return_EntityId]('OSDeSal'),				90),
			([rpt].[Return_EntityId]('ProcUnitsOnSite'),		100),
			([rpt].[Return_EntityId]('UOSLFiredSteamBoilers'),	130),
			([rpt].[Return_EntityId]('UOSLGeneratorFTD'),		140),
			([rpt].[Return_EntityId]('UOSLTankage'),			150),
			([rpt].[Return_EntityId]('ProcUnitsUOSL'),			160),
			([rpt].[Return_EntityId]('ProcUnits'),				170);

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('Unit'),		10),
			([rpt].[Return_AttributeId]('Cap'),			20),
			([rpt].[Return_AttributeId]('Uom'),			30),
			([rpt].[Return_AttributeId]('kEdc'),		40),
			([rpt].[Return_AttributeId]('ukEdc'),		50),
			([rpt].[Return_AttributeId]('uCap_Pcnt'),	60);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[t].[UnitId],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;