﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesOpEx]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('EnergyPurchFuelGas'),			 20),
			([rpt].[Return_EntityId]('EnergyPurchOther'),			 30),
			([rpt].[Return_EntityId]('EnergyPurchElec'),			 40),
			([rpt].[Return_EntityId]('EnergySoldElec'),				 50),
			([rpt].[Return_EntityId]('EnergyPurchSteam'),			 60),
			([rpt].[Return_EntityId]('EnergySoldSteam'),			 70),

			([rpt].[Return_EntityId]('OpExChemCat'),				 80),

			([rpt].[Return_EntityId]('GeneralCompanyLabor'),		 90),	--	PersRoutCmpny
			([rpt].[Return_EntityId]('MaintRoutCompanyLabor'),		100),	--	MaintRoutCompany
			([rpt].[Return_EntityId]('MaintRoutContractLabor'),		110),	--	MaintRoutContract
			([rpt].[Return_EntityId]('MaintRoutMaterial'),			120),	--	MaintRoutMaterial
			([rpt].[Return_EntityId]('MaintRoutEquipment'),			130),	--	MaintRoutEquipment

			([rpt].[Return_EntityId]('GeneralContractLabor'),		140),	--	PersRoutCntrct
			([rpt].[Return_EntityId]('OpExOther'),					150),	--	OpExOther
			([rpt].[Return_EntityId]('AllocGAPers'),				160),	--	PersRoutAllcSGA
			([rpt].[Return_EntityId]('AllocGAOther'),				170),	--	OpExSGA

			([rpt].[Return_EntityId]('OpExRpt'),					180),
			([rpt].[Return_EntityId]('MaintTa'),					190),	--	OpExTa
			([rpt].[Return_EntityId]('OpEx'),						200);

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('RptExp'),		10),
			([rpt].[Return_AttributeId]('RptExpCentsUedc'),	20),
			([rpt].[Return_AttributeId]('RptExpHvc'),	30);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[t].[UnitId],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;