﻿CREATE PROCEDURE [rpt].[Select_OutputFacilities_ChartRadarTrend]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Quartiles TABLE
		(
			[EntityId]			INT				NOT	NULL	IDENTITY,
			[EntityTag]			CHAR(5)			NOT NULL,
			[EntityName]		VARCHAR(16)		NOT	NULL,
			[EntityDetail]		VARCHAR(16)		NOT	NULL,
			[EntitySortKey]		INT				NOT	NULL,
			PRIMARY KEY CLUSTERED([EntityTag] ASC)
		);

		INSERT INTO @Quartiles
		(
			[EntityTag],
			[EntityName],
			[EntityDetail],
			[EntitySortKey]
		)
		VALUES
			('Pcnt4', 'Worst Performers', 'Worst Performers', 1),
			('Pcnt3', 'Q3-Q4 Break', 'Q3-Q4 Break',  2),
			('Pcnt2', 'Q2-Q3 Break', 'Q2-Q3 Break', 3),
			('Pcnt1', 'Q1-Q2 Break', 'Q1-Q2 Break', 4),
			('PcntV', 'Last 12 Mos.', 'Last 12 Mos.', 5);

		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('Eii'),					10),
			([rpt].[Return_AttributeId]('uCap_Pcnt'),			20),
			([rpt].[Return_AttributeId]('AvailOper'),			30),
			([rpt].[Return_AttributeId]('RptExpEdcAnnualized'),	40),
			([rpt].[Return_AttributeId]('Pei'),					50),
			([rpt].[Return_AttributeId]('Mei'),					60),
			([rpt].[Return_AttributeId]('Nei'),					70),
			([rpt].[Return_AttributeId]('RptExpCentsUedc'),		80);

		SELECT
			[u].[SubmissionId],
			[u].[MethodologyId],
			[u].[CurrencyId],

			[q].[EntityId],
			[q].[EntitySortKey],
			[q].[EntityTag],
			[q].[EntityName],
			[q].[EntityDetail],

			[a].[AttributeId],
			[y].[AttributeSortKey],
				[AttributeTag]		= COALESCE([z].[AttributeTag],	[a].[AttributeTag]),
				[AttributeName]		= COALESCE([z].[AttributeName],	[a].[AttributeName]),
				[AttributeDetail]	= COALESCE([z].[AttributeDetail],	[a].[AttributeDetail]),

				[Value]				= CONVERT(VARCHAR, [u].[Value])
		FROM (
			SELECT
					[SubmissionId]	= [s].[BaseSubmissionId],
				[o].[MethodologyId],
				[o].[CurrencyId],
				[o].[EntityId],
				[o].[AttributeId],

				[n].[Pcnt1],
				[n].[Pcnt2],
				[n].[Pcnt3],
				[n].[Pcnt4],
					[PcntV] = [calc].[Minimum]((AVG([o].[Value_Float]) - [n].[Value_Q0]) / ([n].[Value_Q4] - [n].[Value_Q0]) * 100.0, [n].[Pcnt_Max])

			FROM
				[fact].[Select_SubmissionsLast12Mos](@SubmissionId)		[s]
			INNER JOIN
				[rpt].[OutputFacilities]								[o]
					ON	[o].[SubmissionId]		= [s].[SubmissionId]
					AND	[o].[MethodologyId]		= @MethodologyId
					AND	[o].[CurrencyId]		= @CurrencyId
			INNER JOIN
				[ante].[KpiNormalization]		[n]
					ON	[n].[MethodologyId]		= [o].[MethodologyId]
					AND	[n].[CurrencyId]		= [o].[CurrencyId]
					AND	[n].[EntityId]			= [o].[EntityId]
					AND	[n].[AttributeId]		= [o].[AttributeId]
			WHERE	[o].[MethodologyId]			= @MethodologyId
				AND	[o].[CurrencyId]			= @CurrencyId
				AND	[s].[EntityTag]				= 'YTD'
			GROUP BY
				[s].[BaseSubmissionId],
				[o].[MethodologyId],
				[o].[CurrencyId],
				[o].[EntityId],
				[o].[AttributeId],
				[n].[Value_Q0],
				[n].[Value_Q4],
				[n].[Pcnt1],
				[n].[Pcnt2],
				[n].[Pcnt3],
				[n].[Pcnt4],
				[n].[Pcnt_Max]
			) [t]
			UNPIVOT
			(
				[Value] FOR [EntityTag] IN ([Pcnt1], [Pcnt2], [Pcnt3], [Pcnt4], [PcntV])
			) [u]
		INNER JOIN
			@Quartiles					[q]
				ON	[q].[EntityTag]		= [u].[EntityTag]
		INNER JOIN
			[rpt].[Attributes]			[a]
				ON	[a].[AttributeId]	= [u].[AttributeId]
		INNER JOIN
			@Attributes					[y]
				ON	[y].[AttributeId]	= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[EntityAttribute]		[z]
				ON	[z].[EntityId]		= [u].[EntityId]
				AND	[z].[AttributeId]	= [u].[AttributeId];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;