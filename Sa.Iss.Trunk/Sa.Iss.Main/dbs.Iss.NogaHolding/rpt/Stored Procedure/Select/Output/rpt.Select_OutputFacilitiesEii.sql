﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesEii]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('OSAmmoniaSyngas'),		20),
			([rpt].[Return_EntityId]('OSAmmoniaRefrig'),		30),
			([rpt].[Return_EntityId]('OSMethanolSyngas'),		40),
			([rpt].[Return_EntityId]('OSMethanolDist'),			50),
			([rpt].[Return_EntityId]('OSUrea'),					60),
			([rpt].[Return_EntityId]('OSPsaH2Pur'),				70),
			([rpt].[Return_EntityId]('OSFlueGasCo2Recov'),		80),
			([rpt].[Return_EntityId]('OSDeSal'),				90),
			([rpt].[Return_EntityId]('ProcUnitsOnSite'),		100),

			([rpt].[Return_EntityId]('ProcUnitsUOSL'),			160),
			([rpt].[Return_EntityId]('ProcUnits'),				170);

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('uCap'),		10),
			([rpt].[Return_AttributeId]('Uom'),			20),
			([rpt].[Return_AttributeId]('Eii_MBtu'),	30),
			([rpt].[Return_AttributeId]('Eii_GJ'),		40),
			([rpt].[Return_AttributeId]('Eii_Pcnt'),	50);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[t].[UnitId],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;