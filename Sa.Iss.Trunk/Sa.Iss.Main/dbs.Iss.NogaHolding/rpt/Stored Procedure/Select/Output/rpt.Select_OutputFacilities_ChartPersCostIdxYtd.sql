﻿CREATE PROCEDURE [rpt].[Select_OutputFacilities_ChartPersCostIdxYtd]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @EntityId				INT	= [rpt].[Return_EntityId]('OpExLaborMaterials');
		DECLARE @AttributeId			INT	= [rpt].[Return_AttributeId]('RptExpEdcAnnualized');

		DECLARE @DivEntityId			INT	= [rpt].[Return_EntityId]('ProcUnits');
		DECLARE @DivAttributeId			INT	= [rpt].[Return_AttributeId]('ukEdc');

		SELECT
			[s].[SubmissionId],
			[s].[MethodologyId],
			[s].[CurrencyId],
			[s].[EntityId],
			[s].[EntitySortKey],
			[s].[EntityTag],
			[s].[EntityName],
			[s].[EntityDetail],
			[s].[AttributeId],
			[s].[AttributeSortKey],
			[s].[AttributeTag],
			[s].[AttributeName],
			[s].[AttributeDetail],
			[s].[Value]
		FROM
			[rpt].[Select_OutputFacilitiesYtd](@SubmissionId, @MethodologyId, @CurrencyId, @EntityId, @AttributeId, @DivEntityId, @DivAttributeId) [s];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;