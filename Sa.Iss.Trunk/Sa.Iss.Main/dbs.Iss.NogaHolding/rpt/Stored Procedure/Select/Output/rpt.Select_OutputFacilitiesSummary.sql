﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesSummary]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Summary Table
		(
			[SubmissionId]						INT					NOT	NULL,
			[MethodologyId]						INT					NOT	NULL,
			[CurrencyId]						INT					NOT	NULL,

			[EntityId]							INT					NOT	NULL,
			[EntitySortKey]						INT					NOT	NULL,
			[EntityTag]							VARCHAR(48)			NOT	NULL,
			[EntityName]						VARCHAR(96)			NOT	NULL,
			[EntityDetail]						VARCHAR(96)			NOT	NULL,

			[AttributeId]						INT					NOT	NULL,
			[AttributeSortKey]					INT					NOT	NULL,
			[AttributeTag]						VARCHAR(48)			NOT	NULL,
			[AttributeName]						VARCHAR(96)			NOT	NULL,
			[AttributeDetail]					VARCHAR(96)			NOT	NULL,

			[Value]								VARCHAR(128)			NULL,

			PRIMARY KEY CLUSTERED([SubmissionId] ASC, [EntitySortKey] ASC)
		);

		--	11, 12, 13
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('OSAmmoniaRefrig'),		110),
			([rpt].[Return_EntityId]('OSMethanolDist'),			120),
			([rpt].[Return_EntityId]('OSUrea'),					130)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('Cap'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		INNER JOIN
			[rpt].[EntityAttribute]			[t]
				ON	[t].[EntityId]		= [e].[EntityId]
				AND	[t].[AttributeId]	= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	15, 16, 17
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId] * 1000.0,
			[x].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('OSAmmoniaRefrig'),		150),
			([rpt].[Return_EntityId]('OSMethanolDist'),			160),
			([rpt].[Return_EntityId]('OSUrea'),					170)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('Prod'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		INNER JOIN
			[rpt].[EntityAttribute]			[t]
				ON	[t].[EntityId]		= [e].[EntityId]
				AND	[t].[AttributeId]	= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	19, 20
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
				[EntityName]		= [e].[EntityName] + ' (' + [a].[AttributeTag] + ')',
				[EntityDetail]		= [e].[EntityDetail] + ' (' + [a].[AttributeTag] + ')',
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('ProcUnitsOnSite'),		190),
			([rpt].[Return_EntityId]('ProcUnits'),				200)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('kEdc'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	21
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
				[EntityName]		= [e].[EntityName] + ' (' + [a].[AttributeTag] + ')',
				[EntityDetail]		= [e].[EntityDetail] + ' (' + [a].[AttributeTag] + ')',
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('ProcUnits'),				210)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('ukEdc'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	22
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
				[EntityName]		= [e].[EntityName] + ' (' + [a].[AttributeTag] + ')',
				[EntityDetail]		= [e].[EntityDetail] + ' (' + [a].[AttributeTag] + ')',
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('ProcUnits'),				220)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('uCap_Pcnt'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	24
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('Hydrocarbons'),				240)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('HVCYield_Pcnt'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	25
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('LossTotal'),					250)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('Loss_PcntFeed'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	26
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('FeedFuelIdx'),		260)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('FeedFuelIdx'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	28
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('OpEx'),					280)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('RptExpCentsUedc'), 10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	29, 43
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('OpExNe'),					290),
			([rpt].[Return_EntityId]('OpExPers'),				430)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('RptExpEdcAnnualized'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	30
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('Nei'),					300)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('Nei'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	32
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('OpExEnergy'),				320)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('EnergyConsGJHvc'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	33
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('Eii'),					330)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('Eii'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	35, 36, 37
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('MaintRout'),				350),
			([rpt].[Return_EntityId]('MaintTA'),				360),
			([rpt].[Return_EntityId]('OpExMaintRoutTA'),		370)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('MaintIdx'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	38
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('Mei'),					380)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('Mei'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	39
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('AvailOper'),				390)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('AvailOper'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	40
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('AvailMech'),				400)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('AvailMech'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	42
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('PersIdx'),				420)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('PersIdx'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		--	44
		INSERT INTO @Summary
		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
			[e].[EntityId],
			[x].[EntitySortKey],
			[e].[EntityTag],
			[e].[EntityName],
			[e].[EntityDetail],
			[a].[AttributeId],
			[y].[AttributeSortKey],
			[a].[AttributeTag],
			[a].[AttributeName],
			[a].[AttributeDetail],
			[o].[Value]
		FROM
			[rpt].[Entities]				[e]
		INNER JOIN(VALUES
			([rpt].[Return_EntityId]('Pei'),					440)
			)
			[x]([EntityId], [EntitySortKey])
				ON	[x].[EntityId]			= [e].[EntityId]
		CROSS APPLY 
			[rpt].[Attributes]				[a]
		INNER JOIN(VALUES
			([rpt].[Return_AttributeId]('Pei'),	10)
			)[y]([AttributeId], [AttributeSortKey])
				ON	[y].[AttributeId]		= [a].[AttributeId]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]		[o]
				ON	[o].[EntityId]			= [e].[EntityId]
				AND	[o].[AttributeId]		= [a].[AttributeId]
				AND	[o].[SubmissionId]		= @SubmissionId
				AND	[o].[MethodologyId]		= @MethodologyId
				AND	[o].[CurrencyId]		= @CurrencyId;

		SELECT
				[SubmissionId]		= @SubmissionId,
				[MethodologyId]		= @MethodologyId,
				[CurrencyId]		= @CurrencyId,
				[EntityId]			= [s].[EntitySortKey],
			[s].[EntitySortKey],
			[s].[EntityTag],
			[s].[EntityName],
			[s].[EntityDetail],
				[AttributeId]		= 0,
				[AttributeSortKey]	= 10,
				[AttributeTag]		= 'Current Month',
				[AttributeName]		= 'Current Month',
				[AttributeDetail]	= 'Current Month',
			[s].[Value]
		FROM
			@Summary	[s]
		ORDER BY
			[s].EntitySortKey;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;