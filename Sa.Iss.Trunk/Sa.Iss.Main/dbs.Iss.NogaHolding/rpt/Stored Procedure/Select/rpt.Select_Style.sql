﻿CREATE PROCEDURE [rpt].[Select_Style]
(
	@StyleId	INT				= NULL,
	@StyleTag	VARCHAR(12)		= NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		SELECT TOP 1
			[s].[StyleId],
			[f].[StyleFontId],
			[f].[FontSize],
			[f].[FontBold]
		FROM
			[rpt].[Style]			[s]
		INNER JOIN
			[rpt].[StyleFont]		[f]
				ON	[f].[StyleId]	= [s].[StyleId]
		WHERE
				[s].[StyleId]		= @StyleId
			OR	[s].[StyleTag]		= @StyleTag;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		DECLARE @sStyleId	VARCHAR(12) = COALESCE(CONVERT(VARCHAR(12), @StyleId), -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @sStyleId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;