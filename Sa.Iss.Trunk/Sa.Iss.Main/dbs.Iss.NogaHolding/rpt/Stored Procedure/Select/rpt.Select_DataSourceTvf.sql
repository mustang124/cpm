﻿CREATE PROCEDURE [rpt].[Select_DataSourceTvf]
(
	@DataSourceId	INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Tvf	NVARCHAR(256)	= [rpt].[Return_DataSourceTvf](@DataSourceId);

		SELECT [Tvf] = @Tvf;
		RETURN @Tvf;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @Tvf = COALESCE(@Tvf, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @Tvf;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;