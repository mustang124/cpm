﻿CREATE VIEW [rpt].[EntitiesChart]
WITH SCHEMABINDING
AS
SELECT
	[e].[EntityId],
	[e].[EntityTag],
	[e].[EntityName],
	[e].[EntityDetail],
	[t].[SortKey]
FROM
	[rpt].[Entities]	[e]
INNER JOIN (VALUES
	('Tgt', 10),
	('YTD', 20),
	('YearPrev', 30),
	('YearCurr', 40)
	) [t]([EntityTag], [SortKey])
		ON	[t].[EntityTag]	= [e].[EntityTag]
WHERE
	[e].[EntityTag] IN ('Tgt', 'YearCurr', 'YearPrev', 'YTD');