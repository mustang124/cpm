﻿CREATE FUNCTION [rpt].[Select_OutputFacilitiesYtd]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT,
	@EntityId			INT,
	@AttributeId		INT,
	@DivEntityId		INT,
	@DivAttributeId		INT
)
RETURNS @Return TABLE
(
	[SubmissionId]			INT,
	[MethodologyId]			INT,
	[CurrencyId]			INT,

	[EntityId]				INT,
	[EntitySortKey]			INT,
	[EntityTag]				VARCHAR(48),
	[EntityName]			VARCHAR(96),
	[EntityDetail]			VARCHAR(96),

	[AttributeId]			INT,
	[AttributeSortKey]		INT,
	[AttributeTag]			VARCHAR(48),
	[AttributeName]			VARCHAR(96),
	[AttributeDetail]		VARCHAR(96),

	[Value]					VARCHAR(24)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Cumulative TABLE
	(
		[KeyId]		INT,
		[Num]		FLOAT,
		[Den]		FLOAT,
		PRIMARY KEY CLUSTERED([KeyId] ASC)
	);

	INSERT INTO @Cumulative
	(
		[KeyId],
		[Num],
		[Den]
	)
	SELECT
		[s].[AttributeSortKey],
		[r].[Value_Float],
		COALESCE([d].[Value_Float], 1.0)
	FROM
		[fact].[Select_SubmissionsYTD](@SubmissionId)		[s]
	LEFT OUTER JOIN
		[rpt].[OutputFacilities]							[r]
			ON	[r].[SubmissionId]	= [s].[SubmissionId]
			AND	[r].[EntityId]		= @EntityId
			AND	[r].[AttributeId]	= @AttributeId
	LEFT OUTER JOIN
		[rpt].[OutputFacilities]							[d]
			ON	[d].[SubmissionId]	= [s].[SubmissionId]
			AND	[d].[EntityId]		= @DivEntityId
			AND	[d].[AttributeId]	= @DivAttributeId
	WHERE
		[s].[EntityTag]	= 'YTD';

	DECLARE @YtdAverage TABLE
	(
		[KeyId]		INT,
		[Value]		FLOAT,
		PRIMARY KEY CLUSTERED([KeyId] ASC)
	);

	WITH [cte]
	AS (
		SELECT ALL
			[c].[KeyId],
			[Numerator]		= ([c].[Num] * [c].[Den]),
			[Denominator]	= [c].[Den]
		FROM
			@Cumulative	[c]
		WHERE
			[c].[KeyId] = 1
		UNION ALL
		SELECT ALL
			[c].[KeyId],
			[Numerator]		= [cte].[Numerator] + ([c].[Num] * [c].[Den]),
			[Denominator]	= [cte].[Denominator] + [c].[Den]
		FROM
			@Cumulative	[c]
		INNER JOIN
			[cte]
			ON	[cte].[KeyId] = [c].[KeyId] - 1
		)
	INSERT INTO	@YtdAverage
		(
			[KeyId],
			[Value]
		)
	SELECT
		[cte].[KeyId],
		[cte].[Numerator] / [cte].[Denominator]
	FROM
		[cte]
	OPTION(MAXRECURSION 12);

	INSERT INTO @Return
	(
		[SubmissionId],
		[MethodologyId],
		[CurrencyId],

		[EntityId],
		[EntitySortKey],
		[EntityTag],
		[EntityName],
		[EntityDetail],

		[AttributeId],
		[AttributeSortKey],
		[AttributeTag],
		[AttributeName],
		[AttributeDetail],

		[Value]
	)
	SELECT
		[SubmissionId]	= [s].[BaseSubmissionId],
	[r].[MethodologyId],
	[r].[CurrencyId],

	[s].[EntityId],
	[s].[EntitySortKey],
	[s].[EntityTag],
	[s].[EntityName],
	[s].[EntityDetail],

	[s].[AttributeId],
	[s].[AttributeSortKey],
	[s].[AttributeTag],
	[s].[AttributeName],
	[s].[AttributeDetail],

	[Value]	= CONVERT(VARCHAR, CASE [s].[EntityTag]
				WHEN 'YearPrev'	THEN [r].[Value]
				WHEN 'YearCurr'	THEN [r].[Value]
				WHEN 'Tgt'		THEN [b].[Value]
				WHEN 'YTD'		THEN [y].[Value]
				END)

	FROM
		[fact].[Select_SubmissionsYTD](@SubmissionId)		[s]
	LEFT OUTER JOIN
		[rpt].[OutputFacilities]							[r]
			ON	[r].[SubmissionId]	= [s].[SubmissionId]
			AND	[r].[EntityId]		= @EntityId
			AND	[r].[AttributeId]	= @AttributeId
	LEFT OUTER JOIN
		@YtdAverage											[y]
			ON	[y].[KeyId]			= [s].[AttributeSortKey]
	LEFT OUTER JOIN
		[ante].[BaselineTarget]								[b]
			ON	[b].[MethodologyId]	= [r].[MethodologyId]
			AND	[b].[CurrencyId]	= [r].[CurrencyId]
			AND	[b].[EntityId]		= [r].[EntityId]
			AND	[b].[AttributeId]	= [r].[AttributeId];

	RETURN;

END;