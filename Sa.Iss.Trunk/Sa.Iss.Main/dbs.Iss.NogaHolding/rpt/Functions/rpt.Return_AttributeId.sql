﻿CREATE FUNCTION [rpt].[Return_AttributeId]
(
	@AttributeTag	VARCHAR(48)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @AttributeId	INT;

	SELECT TOP 1
		@AttributeId = [a].[AttributeId]
	FROM
		[rpt].[Attributes]	[a]
	WHERE
		[a].[AttributeTag]	= @AttributeTag;

	RETURN @AttributeId;

END;