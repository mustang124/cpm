﻿CREATE FUNCTION [rpt].[Return_StyleId]
(
	@StyleTag	VARCHAR(48)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @StyleId	INT;

	SELECT TOP 1
		@StyleId = [a].[StyleId]
	FROM
		[rpt].[Style]	[a]
	WHERE
		[a].[StyleTag]	= @StyleTag;

	RETURN @StyleId;

END;