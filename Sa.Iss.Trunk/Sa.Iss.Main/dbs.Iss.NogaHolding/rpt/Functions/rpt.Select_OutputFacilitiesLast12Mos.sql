﻿CREATE FUNCTION [rpt].[Select_OutputFacilitiesLast12Mos]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT,
	@EntityId			INT,
	@AttributeId		INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
SELECT
		[SubmissionId]		= [s].[BaseSubmissionId],
	[r].[MethodologyId],
	[r].[CurrencyId],

	[s].[EntityId],
		[EntitySortKey]		= CONVERT(INT, [s].[EntitySortKey]),
	[s].[EntityTag],
	[s].[EntityName],
	[s].[EntityDetail],

	[s].[AttributeId],
		[AttributeSortKey]	= CONVERT(INT, [s].[AttributeSortKey]),
	[s].[AttributeTag],
	[s].[AttributeName],
	[s].[AttributeDetail],

		[Value]	 = CASE [s].[EntityTag]
				WHEN 'Tgt'	THEN [b].[Value]
				WHEN 'YTD'	THEN [r].[Value]
				END

FROM
	[fact].[Select_SubmissionsLast12Mos](@SubmissionId)		[s]
LEFT OUTER JOIN
	[rpt].[OutputFacilities]								[r]
		ON	[r].[SubmissionId]	= [s].[SubmissionId]
		AND	[r].[EntityId]		= @EntityId
		AND	[r].[AttributeId]	= @AttributeId
		AND	[r].[MethodologyId]	= @MethodologyId
		AND	[r].[CurrencyId]	= @CurrencyId
LEFT OUTER JOIN
	[ante].[BaselineTarget]									[b]
		ON	[b].[MethodologyId]	= [r].[MethodologyId]
		AND	[b].[CurrencyId]	= [r].[CurrencyId]
		AND	[b].[EntityId]		= [r].[EntityId]
		AND	[b].[AttributeId]	= [r].[AttributeId];