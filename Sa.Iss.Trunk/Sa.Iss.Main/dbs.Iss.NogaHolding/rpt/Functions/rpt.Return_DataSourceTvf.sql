﻿CREATE FUNCTION [rpt].[Return_DataSourceTvf]
(
	@DataSourceId	INT
)
RETURNS NVARCHAR(256)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Tvf	NVARCHAR(256);

	SELECT TOP 1
		@Tvf = [c].[Tvf]
	FROM
		[rpt].[DataSource]	[c]
	WHERE
		[c].[DataSourceId]	= @DataSourceId;

	RETURN @Tvf;

END;