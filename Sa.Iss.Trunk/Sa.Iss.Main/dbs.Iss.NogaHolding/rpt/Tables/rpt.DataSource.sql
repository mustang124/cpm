﻿CREATE TABLE [rpt].[DataSource]
(
	[DataSourceId]						INT					NOT	NULL	IDENTITY(1, 1),

	[TvfSchema]							NVARCHAR(24)		NOT	NULL,	CONSTRAINT [CL_DataSource_TvfSchema]		CHECK([TvfSchema] <> N''),
	[TvfName]							NVARCHAR(128)		NOT	NULL,	CONSTRAINT [CL_DataSource_TvfName]			CHECK([TvfName] <> N''),
	[Tvf]								AS N'[' + [TvfSchema] + N'].[' + [TvfName] + N']'
										PERSISTED			NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_DataSource_tsModified]		DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DataSource_tsModifiedHost]	DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DataSource_tsModifiedUser]	DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DataSource_tsModifiedApp]	DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_DataSource_tsModifiedGuid]	DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_DataSource]			PRIMARY KEY NONCLUSTERED([DataSourceId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_DataSource_u]
ON	[rpt].[DataSource]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[DataSource]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[DataSource].[DataSourceId]	= [i].[DataSourceId];

END;