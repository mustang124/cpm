﻿CREATE TABLE [rpt].[AnnotationsLevel]
(
	[AnnotationsLevelId]				INT					NOT	NULL	IDENTITY(1, 1),

	[AnnotationsLevelTag]				VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_AnnotationsLevel_AnnotationsLevelTag]	UNIQUE CLUSTERED([AnnotationsLevelTag] ASC),
																		CONSTRAINT [CL_AnnotationsLevel_AnnotationsLevelTag]	CHECK([AnnotationsLevelTag] <> ''),
	[AnnotationsLevelName]				VARCHAR(24)			NOT	NULL	CONSTRAINT [UX_AnnotationsLevel_AnnotationsLevelName]	UNIQUE NONCLUSTERED([AnnotationsLevelName] ASC),
																		CONSTRAINT [CL_AnnotationsLevel_AnnotationsLevelName]	CHECK([AnnotationsLevelName] <> N''),
	[AnnotationsLevelDetail]			VARCHAR(48)			NOT	NULL	CONSTRAINT [UX_AnnotationsLevel_AnnotationsLevelDetail]	UNIQUE NONCLUSTERED([AnnotationsLevelDetail] ASC),
																		CONSTRAINT [CL_AnnotationsLevel_AnnotationsLevelDetail]	CHECK([AnnotationsLevelDetail] <> N''),

	[AnnotationLevelStyleId]			INT						NULL	CONSTRAINT [FK_AnnotationsLevel_Style]					REFERENCES [rpt].[Style]([StyleId]),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AnnotationsLevel_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AnnotationsLevel_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AnnotationsLevel_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AnnotationsLevel_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AnnotationsLevel_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AnnotationsLevel]	PRIMARY KEY NONCLUSTERED([AnnotationsLevelId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_AnnotationsLevel_u]
ON	[rpt].[AnnotationsLevel]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[AnnotationsLevel]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[AnnotationsLevel].[AnnotationsLevelId]	= [i].[AnnotationsLevelId];

END;