﻿CREATE TABLE [rpt].[StyleFont]
(
	[StyleFontId]						INT					NOT	NULL	IDENTITY(1, 1),

	[StyleId]							INT					NOT	NULL	CONSTRAINT [FK_StyleFont_Style]					REFERENCES [rpt].[Style]([StyleId]),

	[FontSize]							INT						NULL,
	[FontBold]							BIT						NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StyleFont_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StyleFont_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StyleFont_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StyleFont_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_StyleFont_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_StyleFont]			PRIMARY KEY NONCLUSTERED([StyleFontId] ASC),
	CONSTRAINT [UK_StyleFont]			UNIQUE CLUSTERED([StyleId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_StyleFont_u]
ON	[rpt].[StyleFont]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[StyleFont]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[StyleFont].[StyleFontId]	= [i].[StyleFontId];

END;