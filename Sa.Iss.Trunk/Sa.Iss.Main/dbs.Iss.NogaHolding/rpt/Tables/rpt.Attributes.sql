﻿CREATE TABLE [rpt].[Attributes]
(
	[AttributeId]						INT					NOT	NULL	IDENTITY(1, 1),

	[AttributeTag]						VARCHAR(48)			NOT	NULL	CONSTRAINT [UK_Attribute_AttributeTag]		UNIQUE CLUSTERED([AttributeTag] ASC),
																		CONSTRAINT [CL_Attribute_AttributeTag]		CHECK([AttributeTag] <> ''),
	[AttributeName]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UX_Attribute_AttributeName]		UNIQUE NONCLUSTERED([AttributeName] ASC),
																		CONSTRAINT [CL_Attribute_AttributeName]		CHECK([AttributeName] <> N''),
	[AttributeDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UX_Attribute_AttributeDetail]	UNIQUE NONCLUSTERED([AttributeDetail] ASC),
																		CONSTRAINT [CL_Attribute_AttributeDetail]	CHECK([AttributeDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Attributes_tsModified]		DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Attributes_tsModifiedHost]	DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Attributes_tsModifiedUser]	DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Attributes_tsModifiedApp]	DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Attributes_tsModifiedGuid]	DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Attributes]			PRIMARY KEY NONCLUSTERED([AttributeId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_Attributes_u]
ON	[rpt].[Attributes]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[Attributes]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[Attributes].[AttributeId]	= [i].[AttributeId];

END;