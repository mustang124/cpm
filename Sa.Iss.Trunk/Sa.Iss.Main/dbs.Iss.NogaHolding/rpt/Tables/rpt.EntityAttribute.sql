﻿CREATE TABLE [rpt].[EntityAttribute]
(
	[EntityAttributeId]					INT					NOT	NULL	IDENTITY(1, 1),

	[EntityId]							INT					NOT	NULL	CONSTRAINT [FK_EntityAttribute_Entity_LookUp]			REFERENCES [rpt].[Entities]([EntityId]),
	[EntityTag]							VARCHAR(48)			NOT	NULL	CONSTRAINT [UK_EntityAttribute_EntityTag]				UNIQUE NONCLUSTERED([EntityTag] ASC),
																		CONSTRAINT [CL_EntityAttribute_EntityTag]				CHECK([EntityTag] <> ''),
	[EntityName]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UX_EntityAttribute_EntityName]				UNIQUE NONCLUSTERED([EntityName] ASC),
																		CONSTRAINT [CL_EntityAttribute_EntityName]				CHECK([EntityName] <> N''),
	[EntityDetail]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UX_EntityAttribute_EntityDetail]			UNIQUE NONCLUSTERED([EntityDetail] ASC),
																		CONSTRAINT [CL_EntityAttribute_EntityDetail]			CHECK([EntityDetail] <> N''),

	[AttributeId]						INT					NOT	NULL	CONSTRAINT [FK_EntityAttribute_EntityAttribute_LookUp]	REFERENCES [rpt].[Attributes]([AttributeId]),
	[AttributeTag]						VARCHAR(48)			NOT	NULL	CONSTRAINT [UK_EntityAttribute_AttributeTag]			UNIQUE NONCLUSTERED([AttributeTag] ASC),
																		CONSTRAINT [CL_EntityAttribute_AttributeTag]			CHECK([AttributeTag] <> ''),
	[AttributeName]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UX_EntityAttribute_AttributeName]			UNIQUE NONCLUSTERED([AttributeName] ASC),
																		CONSTRAINT [CL_EntityAttribute_AttributeName]			CHECK([AttributeName] <> N''),
	[AttributeDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UX_EntityAttribute_AttributeDetail]			UNIQUE NONCLUSTERED([AttributeDetail] ASC),
																		CONSTRAINT [CL_EntityAttribute_AttributeDetail]			CHECK([AttributeDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_EntityAttribute_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_EntityAttribute_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_EntityAttribute_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_EntityAttribute_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_EntityAttribute_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_EntityAttribute]		PRIMARY KEY NONCLUSTERED([EntityAttributeId] ASC),
	CONSTRAINT [UK_EntityAttribute]		UNIQUE CLUSTERED([EntityId] ASC, [AttributeId] ASC),
);
GO

CREATE TRIGGER [rpt].[t_EntityAttribute_u]
ON	[rpt].[EntityAttribute]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[EntityAttribute]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[EntityAttribute].[EntityAttributeId]	= [i].[EntityAttributeId];

END;