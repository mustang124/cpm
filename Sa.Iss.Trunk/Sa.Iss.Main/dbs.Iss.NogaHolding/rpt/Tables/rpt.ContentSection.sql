﻿CREATE TABLE [rpt].[ContentSection]
(
	[ContentSectionId]					INT					NOT	NULL	IDENTITY(1, 1),

	[ContentId]							INT					NOT	NULL	CONSTRAINT [FK_ContentSection_Content]				REFERENCES [rpt].[Content]([ContentId]),
	[SectionNumber]						INT					NOT	NULL,
	[DataSourceId]						INT					NOT	NULL	CONSTRAINT [FK_ContentSection_DataSource]			REFERENCES [rpt].[DataSource]([DataSourceId]),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ContentSection_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContentSection_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContentSection_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContentSection_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ContentSection_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ContentSection]		PRIMARY KEY NONCLUSTERED([ContentSectionId] ASC),
	CONSTRAINT [UK_ContentSection]		UNIQUE CLUSTERED([ContentId] ASC, [SectionNumber] ASC)
);
GO

CREATE TRIGGER [rpt].[t_ContentSection_u]
ON	[rpt].[ContentSection]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[ContentSection]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[ContentSection].[ContentSectionId]	= [i].[ContentSectionId];

END;