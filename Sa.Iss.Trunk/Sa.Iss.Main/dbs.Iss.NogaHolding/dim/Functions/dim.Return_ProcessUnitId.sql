﻿CREATE FUNCTION [dim].[Return_ProcessUnitId]
(
	@ProcessUnit		NVARCHAR(96)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @ProcessUnitId	INT;

	SET @ProcessUnit = RTRIM(LTRIM(@ProcessUnit));

	SELECT TOP 1
		@ProcessUnitId = [c].[ProcessUnitId]
	FROM
		[dim].[ProcessUnit_LookUp]	[c]
	WHERE	[c].[ProcessUnitTag]	= @ProcessUnit
		OR	[c].[ProcessUnitName]	= @ProcessUnit
		OR	[c].[ProcessUnitDetail]	= @ProcessUnit;

	RETURN	@ProcessUnitId;

END;