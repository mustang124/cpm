﻿CREATE FUNCTION [dim].[Return_DeliverySystemId]
(
	@DeliverySystem		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @DeliverySystemId	INT;

	SET @DeliverySystem = RTRIM(LTRIM(@DeliverySystem));

	SELECT TOP 1
		@DeliverySystemId = [c].[DeliverySystemId]
	FROM
		[dim].[DeliverySystem_LookUp]	[c]
	WHERE	[c].[DeliverySystemTag]		= @DeliverySystem
		OR	[c].[DeliverySystemName]	= @DeliverySystem;

	RETURN	@DeliverySystemId;

END;