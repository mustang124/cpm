﻿CREATE FUNCTION [dim].[Return_ComponentId]
(
	@Component		NVARCHAR(48)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @ComponentId	INT;

	SET @Component = RTRIM(LTRIM(@Component));

	SELECT TOP 1
		@ComponentId = [c].[ComponentId]
	FROM
		[dim].[Component_LookUp]	[c]
	WHERE	[c].[ComponentTag]	= @Component
		OR	[c].[ComponentName]	= @Component;

	RETURN	@ComponentId;

END;