﻿CREATE FUNCTION [dim].[Return_ReliabilityId]
(
	@Reliability		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @ReliabilityId	INT;

	SET @Reliability = RTRIM(LTRIM(@Reliability));

	SELECT TOP 1
		@ReliabilityId = [c].[ReliabilityId]
	FROM
		[dim].[Reliability_LookUp]	[c]
	WHERE	[c].[ReliabilityTag]	= @Reliability
		OR	[c].[ReliabilityName]	= @Reliability;

	RETURN	@ReliabilityId;

END;