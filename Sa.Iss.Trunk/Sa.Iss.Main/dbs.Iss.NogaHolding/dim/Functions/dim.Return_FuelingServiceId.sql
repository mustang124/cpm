﻿CREATE FUNCTION [dim].[Return_FuelingServiceId]
(
	@FuelingService		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @FuelingServiceId	INT;

	SET @FuelingService = RTRIM(LTRIM(@FuelingService));

	SELECT TOP 1
		@FuelingServiceId = [c].[FuelingServiceId]
	FROM
		[dim].[FuelingService_LookUp]	[c]
	WHERE	[c].[FuelingServiceTag]		= @FuelingService
		OR	[c].[FuelingServiceName]	= @FuelingService;

	RETURN	@FuelingServiceId;

END;