﻿CREATE FUNCTION [dim].[Return_UomId]
(
	@Uom		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @UomId	INT;

	SET @Uom = RTRIM(LTRIM(@Uom));

	SELECT TOP 1
		@UomId = [c].[UomId]
	FROM
		[dim].[Uom_LookUp]	[c]
	WHERE	[c].[UomTag]	= @Uom
		OR	[c].[UomName]	= @Uom;

	RETURN	@UomId;

END;