﻿CREATE FUNCTION [dim].[Return_ConveyorId]
(
	@Conveyor		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @ConveyorId	INT;

	SET @Conveyor = RTRIM(LTRIM(@Conveyor));

	SELECT TOP 1
		@ConveyorId = [c].[ConveyorId]
	FROM
		[dim].[Conveyor_LookUp]	[c]
	WHERE	[c].[ConveyorTag]	= @Conveyor
		OR	[c].[ConveyorName]	= @Conveyor;

	RETURN	@ConveyorId;

END;