﻿CREATE FUNCTION [dim].[Return_MaterialId]
(
	@Material		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @MaterialId	INT;

	SET @Material = RTRIM(LTRIM(@Material));

	SELECT TOP 1
		@MaterialId = [c].[MaterialId]
	FROM
		[dim].[Material_LookUp]	[c]
	WHERE	[c].[MaterialTag]	= @Material
		OR	[c].[MaterialName]	= @Material;

	RETURN	@MaterialId;

END;