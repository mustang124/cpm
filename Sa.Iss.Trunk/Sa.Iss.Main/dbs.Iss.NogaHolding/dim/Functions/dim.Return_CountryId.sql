﻿CREATE FUNCTION [dim].[Return_CountryId]
(
	@Country		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @CountryId	INT;

	SET @Country = RTRIM(LTRIM(@Country));

	SELECT TOP 1
		@CountryId = [c].[CountryId]
	FROM
		[dim].[Country_LookUp]	[c]
	WHERE	[c].[CountryTag]	= @Country
		OR	[c].[CountryNameEn]	= @Country;

	RETURN	@CountryId;

END;