﻿CREATE FUNCTION [dim].[Return_AircraftTypeId]
(
	@AircraftType		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @AircraftTypeId	INT;

	SET @AircraftType = RTRIM(LTRIM(@AircraftType));

	SELECT TOP 1
		@AircraftTypeId = [c].[AircraftTypeId]
	FROM
		[dim].[AircraftType_LookUp]	[c]
	WHERE	[c].[AircraftTypeTag]	= @AircraftType
		OR	[c].[AircraftTypeName]	= @AircraftType;

	RETURN	@AircraftTypeId;

END;