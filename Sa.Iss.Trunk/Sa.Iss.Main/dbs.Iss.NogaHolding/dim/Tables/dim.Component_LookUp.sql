﻿CREATE TABLE [dim].[Component_LookUp]
(
	[ComponentId]						INT					NOT	NULL	IDENTITY(1, 1),

	[ComponentTag]						VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Component_LookUp_ComponentTag]			UNIQUE CLUSTERED([ComponentTag] ASC),
																		CONSTRAINT [CL_Component_LookUp_ComponentTag]			CHECK([ComponentTag] <> ''),
	[ComponentName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_Component_LookUp_ComponentName]			UNIQUE NONCLUSTERED([ComponentName] ASC),
																		CONSTRAINT [CL_Component_LookUp_ComponentName]			CHECK([ComponentName] <> N''),
	[ComponentDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Component_LookUp_ComponentDetail]		UNIQUE NONCLUSTERED([ComponentDetail] ASC),
																		CONSTRAINT [CL_Component_LookUp_ComponentDetail]		CHECK([ComponentDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Component_LookUp]	PRIMARY KEY NONCLUSTERED([ComponentId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Component_LookUp_u]
ON	[dim].[Component_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Component_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Component_LookUp].[ComponentId]	= [i].[ComponentId];

END;