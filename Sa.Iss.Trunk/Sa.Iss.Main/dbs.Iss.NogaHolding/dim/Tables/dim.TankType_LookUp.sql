﻿CREATE TABLE [dim].[TankType_LookUp]
(
	[TankTypeId]						INT					NOT	NULL	IDENTITY(1, 1),

	[TankTypeTag]						VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_TankType_LookUp_AircraftTag]			UNIQUE CLUSTERED([TankTypeTag] ASC),
																		CONSTRAINT [CL_TankType_LookUp_AircraftTag]			CHECK([TankTypeTag] <> ''),
	[TankTypeName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_TankType_LookUp_AircraftName]		UNIQUE NONCLUSTERED([TankTypeName] ASC),
																		CONSTRAINT [CL_TankType_LookUp_AircraftName]		CHECK([TankTypeName] <> N''),
	[TankTypeDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_TankType_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([TankTypeDetail] ASC),
																		CONSTRAINT [CL_TankType_LookUp_AircraftDetail]		CHECK([TankTypeDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_TankType_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TankType_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TankType_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TankType_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_TankType_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_TankType_LookUp]		PRIMARY KEY NONCLUSTERED([TankTypeId] ASC)
);
GO

CREATE TRIGGER [dim].[t_TankType_LookUp_u]
ON	[dim].[TankType_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[TankType_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[TankType_LookUp].[TankTypeId]	= [i].[TankTypeId];

END;