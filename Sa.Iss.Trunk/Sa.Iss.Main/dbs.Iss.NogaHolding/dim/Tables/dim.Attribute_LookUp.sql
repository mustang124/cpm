﻿CREATE TABLE [dim].[Attribute_LookUp]
(
	[AttributeId]						INT					NOT	NULL	IDENTITY(1, 1),

	[AttributeTag]						VARCHAR(56)			NOT	NULL,	CONSTRAINT [CL_Attribute_LookUp_AttributeTag]		CHECK([AttributeTag] <> ''),
																		CONSTRAINT [UK_Attribute_LookUp_AttributeTag]		UNIQUE CLUSTERED([AttributeTag] ASC),	
	[AttributeName]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Attribute_LookUp_AttributeName]		CHECK([AttributeName] <> N''),
																		CONSTRAINT [UX_Attribute_LookUp_AttributeName]		UNIQUE NONCLUSTERED([AttributeName] ASC),	
	[AttributeDetail]					NVARCHAR(192)		NOT	NULL,	CONSTRAINT [CL_Attribute_LookUp_AttributeDetail]	CHECK([AttributeDetail] <> N''),
																		CONSTRAINT [UX_Attribute_LookUp_AttributeDetail]	UNIQUE NONCLUSTERED([AttributeDetail] ASC),	

	[UomId]								INT						NULL	CONSTRAINT [FK_Attribute_LookUp_Uom_LookUp]			REFERENCES [dim].[Uom_LookUp]([UomId]),
	[DataType]							VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_Attribute_LookUp_DataType]			CHECK([DataType]		<> ''),
	[LengthPrecicion]					INT						NULL,	CONSTRAINT [CR_Attribute_LookUp_LengthPrecicion]	CHECK([LengthPrecicion]	>= 1),
	[Scale]								INT						NULL,	CONSTRAINT [CR_Attribute_LookUp_Scale]				CHECK([Scale]			>= 0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Attribute_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Attribute_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Attribute_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Attribute_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Attribute_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Attribute_LookUp]	PRIMARY KEY NONCLUSTERED([AttributeId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Attribute_LookUp_u]
ON	[dim].[Attribute_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Attribute_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Attribute_LookUp].[AttributeId]	= [i].[AttributeId];

END;