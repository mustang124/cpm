﻿CREATE TABLE [dim].[Country_LookUp]
(
	[CountryId]							INT					NOT	NULL	IDENTITY(1, 1),

	[CountryTag]						CHAR(3)				NOT	NULL	CONSTRAINT [UK_Country_LookUp_CountryTag]			UNIQUE CLUSTERED([CountryTag] ASC),
																		CONSTRAINT [CL_Country_LookUp_CountryTag]			CHECK([CountryTag] <> ''),
	[CountryNameEn]						VARCHAR(96)			NOT	NULL	CONSTRAINT [UK_Country_LookUp_CountryNameEn]		UNIQUE NONCLUSTERED([CountryNameEn] ASC),
																		CONSTRAINT [CL_Country_LookUp_CountryNameEn]		CHECK([CountryNameEn] <> ''),
	[CountryName]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Country_LookUp_CountryName]			UNIQUE NONCLUSTERED([CountryName] ASC),
																		CONSTRAINT [CL_Country_LookUp_CountryName]			CHECK([CountryName] <> N''),
	[CountryDetail]						NVARCHAR(256)		NOT	NULL	CONSTRAINT [UK_Country_LookUp_CountryDetail]		UNIQUE NONCLUSTERED([CountryDetail] ASC),
																		CONSTRAINT [CL_Country_LookUp_CountryDetail]		CHECK([CountryDetail] <> N''),
	[IsoAlpha2]							CHAR(2)				NOT	NULL	CONSTRAINT [UK_Country_LookUp_CountryIsoAlpha2]		UNIQUE NONCLUSTERED([IsoAlpha2] ASC),
																		CONSTRAINT [CL_Country_LookUp_CountryIsoAlpha2]		CHECK([IsoAlpha2] <> ''),
	[IsoNumeric]						CHAR(3)					NULL	CONSTRAINT [UK_Country_LookUp_CountryIsoNumeric]	UNIQUE NONCLUSTERED([IsoNumeric] ASC),
																		CONSTRAINT [CL_Country_LookUp_CountryIsoNumeric]	CHECK([IsoNumeric] <> ''),

	[Active]							INT					NOT	NULL	CONSTRAINT [DF_Country_LookUp_Active]				DEFAULT(0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Country_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Country_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Country_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Country_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Country_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Country_LookUp]		PRIMARY KEY NONCLUSTERED([CountryId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Country_LookUp_u]
ON	[dim].[Country_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Country_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Country_LookUp].[CountryId]	= [i].[CountryId];

END;