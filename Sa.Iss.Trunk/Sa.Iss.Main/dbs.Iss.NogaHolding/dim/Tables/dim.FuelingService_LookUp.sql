﻿CREATE TABLE [dim].[FuelingService_LookUp]
(
	[FuelingServiceId]					INT					NOT	NULL	IDENTITY(1, 1),

	[FuelingServiceTag]					VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_FuelingService_LookUp_AircraftTag]			UNIQUE CLUSTERED([FuelingServiceTag] ASC),
																		CONSTRAINT [CL_FuelingService_LookUp_AircraftTag]			CHECK([FuelingServiceTag] <> ''),
	[FuelingServiceName]				NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_FuelingService_LookUp_AircraftName]			UNIQUE NONCLUSTERED([FuelingServiceName] ASC),
																		CONSTRAINT [CL_FuelingService_LookUp_AircraftName]			CHECK([FuelingServiceName] <> N''),
	[FuelingServiceDetail]				NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_FuelingService_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([FuelingServiceDetail] ASC),
																		CONSTRAINT [CL_FuelingService_LookUp_AircraftDetail]		CHECK([FuelingServiceDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FuelingService_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FuelingService_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FuelingService_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FuelingService_LookUp_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_FuelingService_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_FuelingService_LookUp]		PRIMARY KEY NONCLUSTERED([FuelingServiceId] ASC)
);
GO

CREATE TRIGGER [dim].[t_FuelingService_LookUp_u]
ON	[dim].[FuelingService_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[FuelingService_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[FuelingService_LookUp].[FuelingServiceId]	= [i].[FuelingServiceId];

END;