﻿CREATE TABLE [dim].[Operator_LookUp]
(
	[Operator_LookUpIdx]				INT					NOT	NULL	IDENTITY(1, 1),

	[Operator]							CHAR(1)				NOT	NULL,	CONSTRAINT [CL_Operator_LookUp_Operator]			CHECK([Operator] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Operator_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Operator_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Operator_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Operator_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Operator_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Operator_LookUp]	PRIMARY KEY NONCLUSTERED([Operator_LookUpIdx] ASC),
	CONSTRAINT [UK_Operator_LookUp]	UNIQUE CLUSTERED([Operator] ASC)
);
GO

CREATE TRIGGER [dim].[t_Operator_LookUp_u]
ON	[dim].[Operator_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Operator_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Operator_LookUp].[Operator_LookUpIdx]	= [i].[Operator_LookUpIdx];

END;