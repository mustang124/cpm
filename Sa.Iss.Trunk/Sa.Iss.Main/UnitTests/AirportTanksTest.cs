﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Sa.Iss;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;
using System.Reflection;
using System.Collections.Generic;
using Sa.Iss.Upload.Cl.Calcs;
namespace UnitTests
{
    [TestClass]
    public class AirportTanksTest
    {
        Excel.Application xla;
        Excel.Workbook wkb;
        
        Single hourlyWage = 28.4135f;
        int hoursInAYear = 2080;

        [TestInitialize]
        public void Setup()
        {
            xla = XL.NewExcelApplication(false);
            Assembly testProject = Assembly.GetExecutingAssembly();
            String xlsPath = CutPath(testProject.Location, 3) + @"BAFCOInputData2015-07.xlsx";
            wkb = XL.OpenWorkbook_ReadOnly(xla, xlsPath, "");
        }        
        
        [TestMethod]
        public void TestSubmission()
        {                      
            try
            {
                using (SqlConnection conn  = new SqlConnection(ConfigurationManager.ConnectionStrings["BafcoConnection"].ToString()))
                {
                    conn.Open();
                    SqlTransaction trans=conn.BeginTransaction("Test");
                    Submission submission = new Submission();
                    PrivateType tanks = new PrivateType(typeof(Sa.Iss.AirportTanks.Input.Upload));
                    object[] args = new object[5];
                    args[0] = new Sa.Iss.Queue.Upload();
                    args[1] = conn;
                    args[2] = wkb;
                    args[3] = submission;
                    args[4] = "2015";
                    var result = (Submission)tanks.InvokeStatic("Submission", args);
                    trans.Rollback();            
                    Assert.IsTrue(result.Id > 0);
                }
            }
            catch (Exception ex)
            {
                String msg = ex.Message;
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestCalcs()
        {  
            Single dayMaxCap = 390;
            Single standardCashOpexPerBblOfCduThruput = 0.421f;
            Single tankRepairInspectionRatio  = 4f;
            Single averageIntervalForInsp = 10;
            int numberOfTanks = 14;
 
            Single eDCStandardEnergyCost = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.EDCStandardEnergyCost(4037f,
                360, 11.2f, 9.09f, 262, 360, 0.7f, 9.09f);
            Assert.IsTrue(eDCStandardEnergyCost.ToString().Substring(0, 6) == "0.5427");

            Single totalOtherCashOpexDollarPerkUSG = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.TotalOtherCashOpexDollarPerkUSG(16598,
                196304, 237197, 26374, dayMaxCap, 0.379f);
            Assert.IsTrue(totalOtherCashOpexDollarPerkUSG.ToString().Substring(0, 4) == "8.83");

            Single maintenanceAdministrationCosts = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.MaintenanceAdministrationCosts(
            PopulateMaintenanceManagersAndPlanners());
            Assert.IsTrue(maintenanceAdministrationCosts.ToString().Substring(0, 6) == "138885");

            Single maintenanceAdministrationHours = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.MaintenanceAdministrationHours(
            PopulateMaintenanceManagersAndPlanners());
            Assert.IsTrue(maintenanceAdministrationHours.ToString().Substring(0, 4) == "4160");

            Single maintenanceStaffCompanyHours = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.MaintenanceStaffCompanyHours(
                PopulateMaintenanceStaffCompany());
            Assert.IsTrue(maintenanceStaffCompanyHours.ToString().Substring(0, 5) == "37440");

            Single maintenanceEDCStandardAnnualMaintenanceCost = 
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.MaintenanceEDCStandardAnnualMaintenanceCost(
                PopulateMaintenanceStaffCompany(),
                PopulateContractMaintenanceServices(), 463472, 0, 2080, hourlyWage, 0.66f);
            Assert.IsTrue(maintenanceEDCStandardAnnualMaintenanceCost.ToString().Substring(0, 7) == "1722303");

            Single conversionM3ToBbl = (1 / 0.119240471f);
            IList<Sa.Iss.Upload.Cl.Calcs.TankVolumes> populateTankVolumes = PopulateTankVolumes();
            Single maintenanceTotalAnnualizedTankIntegerityEDCCost = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.MaintenanceTotalAnnualizedTankIntegerityEDCCost(populateTankVolumes,
                conversionM3ToBbl, 0.182f, 1.79f);
            Assert.IsTrue(maintenanceTotalAnnualizedTankIntegerityEDCCost.ToString().Substring(0, 8) == "53502.21");
            
            Single eDCStandardMaintenanceCost = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.EDCStandardMaintenanceCost(
                maintenanceAdministrationCosts, maintenanceEDCStandardAnnualMaintenanceCost,
                maintenanceTotalAnnualizedTankIntegerityEDCCost);
            Assert.IsTrue(eDCStandardMaintenanceCost.ToString().Substring(0, 7) == "1914690");

            Single eDCStandardMaintenanceCostDollerPerkUSG = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.EDCStandardMaintenanceCostDollerPerkUSG(
                eDCStandardMaintenanceCost, dayMaxCap);
            Assert.IsTrue(eDCStandardMaintenanceCostDollerPerkUSG.ToString().Substring(0, 5) == "13.45");

            Single eDCAnnuliazedTurnaroundMaintenanceCost =
            Sa.Iss.Upload.Cl.Calcs.AirportCalcs.EDCAnnuliazedTurnaroundMaintenanceCost(
            averageIntervalForInsp, numberOfTanks, hourlyWage, 
            PopulateTAOperationsStaffCompany(),
            PopulateTAMaintenanceStaffCompany(),
            PopulateTAContractors());
            Assert.IsTrue(eDCAnnuliazedTurnaroundMaintenanceCost.ToString().Substring(0, 5) == "436.8");

            Single annualizedTurnaroundCost =
            Sa.Iss.Upload.Cl.Calcs.AirportCalcs.AnnualizedTurnaroundCost(averageIntervalForInsp,
            numberOfTanks, hourlyWage, 
            PopulateTAOperationsStaffCompany(), 
            PopulateTAMaintenanceStaffCompany(), 
            PopulateTAContractors());
            Assert.IsTrue(annualizedTurnaroundCost.ToString().Substring(0, 7) == "29898.4");
            
            Single annualizedTurnaroundHours = //T93  --tankHoursPerInspection
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.AnnualizedTurnaroundHours(
                averageIntervalForInsp,
                PopulateTAOperationsStaffCompany(),
                PopulateTAMaintenanceStaffCompany(),
                PopulateTAContractors(),numberOfTanks);                
            Assert.IsTrue(annualizedTurnaroundHours.ToString().Substring(0, 5) == "436.8");

            
            Single maintenanceWorkHours =
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.MaintenanceWorkHours
                (PopulateMaintenanceManagersAndPlanners(),
                PopulateMaintenanceStaffCompany(),
                PopulateContractMaintenanceServices(),
                annualizedTurnaroundHours,
                tankRepairInspectionRatio);
            Assert.IsTrue(maintenanceWorkHours.ToString().Substring(0, 5) == "54184");

            Single operationsEDCStandardOperationsCostDollerPerkUSG =
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.OperationsEDCStandardOperationsCostDollerPerkUSG
                (PopulateOperationsPersonnelCost(), PopulateReceiptsPersonnelCost(),
                PopulateIssuePersonnelCost(), dayMaxCap);
            Assert.IsTrue(operationsEDCStandardOperationsCostDollerPerkUSG.ToString().Substring(0, 5) == "23.27");

            Single operationsEDCStandardOperationsHoursPerkUSG =
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.OperationsEDCStandardOperationsHoursPerkUSG
                (PopulateOperationsPersonnelCost(),PopulateReceiptsPersonnelCost(),
                PopulateIssuePersonnelCost(), dayMaxCap);
            Assert.IsTrue(operationsEDCStandardOperationsHoursPerkUSG.ToString().Substring(0, 4) == "0.80");

            Single techAdminEDCStandardOperationsCostDollerPerkUSG =
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.TechAdminEDCStandardOperationsCostDollerPerkUSG
                (PopulateSitePersonnelCost(), PopulateTechQaPersonnelCost(), PopulateAccountingPersonnelCost(),
                dayMaxCap);
            Assert.IsTrue(techAdminEDCStandardOperationsCostDollerPerkUSG.ToString().Substring(0, 4) == "4.96");
            
            Single techAdminEDCStandardOperationsHoursPerkUSG =
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.TechAdminEDCStandardOperationsHoursPerkUSG
                (PopulateSitePersonnelCost(), PopulateTechQaPersonnelCost(), PopulateAccountingPersonnelCost(),
                dayMaxCap);
            Assert.IsTrue(techAdminEDCStandardOperationsHoursPerkUSG.ToString().Substring(0, 4) == "0.16");

            Single edcStandardCashOpexPerkUsg =
                Sa.Iss.Upload.Cl.Calcs.AirportCalcs.EdcStandardCashOpexPerkUsg
                (eDCStandardEnergyCost,
                 eDCStandardMaintenanceCostDollerPerkUSG, operationsEDCStandardOperationsCostDollerPerkUSG,
                 techAdminEDCStandardOperationsCostDollerPerkUSG, totalOtherCashOpexDollarPerkUSG
                 );
            Assert.IsTrue(edcStandardCashOpexPerkUsg.ToString().Substring(0, 6) == "51.056");

            Single eDCFactorForAviationFueling =Sa.Iss.Upload.Cl.Calcs.AirportCalcs.EDCFactorForAviationFueling 
             (edcStandardCashOpexPerkUsg,standardCashOpexPerBblOfCduThruput);
            Assert.IsTrue(eDCFactorForAviationFueling.ToString().Substring(0, 3) == "121");

            Single eDCkUsgPerDayAviationFuelingOperation = Sa.Iss.Upload.Cl.Calcs.AirportCalcs.EDCkUsgPerDayAviationFuelingOperation
                (eDCFactorForAviationFueling, dayMaxCap);
            Assert.IsTrue(eDCkUsgPerDayAviationFuelingOperation.ToString().Substring(0, 5) == "47190");
        
            Single totalPEIWorkHoursStandard= Sa.Iss.Upload.Cl.Calcs.AirportCalcs.TotalPEIWorkHoursStandard
            ( PopulateOperationsPersonnelCost(),
              PopulateReceiptsPersonnelCost(),
              PopulateIssuePersonnelCost(),
              maintenanceWorkHours,
              PopulateSitePersonnelCost(),
              PopulateTechQaPersonnelCost(),
              PopulateAccountingPersonnelCost()
            );
            Assert.IsTrue(totalPEIWorkHoursStandard.ToString().Substring(0, 6) == "191464");

            Single neOpexStandard =
                 Sa.Iss.Upload.Cl.Calcs.AirportCalcs.NeOpexStandard
                 (edcStandardCashOpexPerkUsg, eDCStandardEnergyCost,
                 dayMaxCap);
            Assert.IsTrue(neOpexStandard.ToString().Substring(0, 4) == "7.19");             
        }

        [TestMethod]
        public void TestSummary()
        {
            DataSet dataSet = summaryTestDataset();
            int rowCount = dataSet.Tables[0].Rows.Count;
            Single[] rows = SendRowsToArray(dataSet.Tables[0], 0);
            Array.Sort(rows);
            Single min = rows[0];
            Single max = rows[rows.Length - 1];
            //Single ave = GetAve(rows); 
            //to get ytd, change # of rows in dataset
        }

        public Single GetAve(Single[] rows)
        {
            int count = 0;
            Single sum = 0;
            for (count = 0; count < rows.Length - 1; count++)
            {
                sum += rows[count];
            }
            return sum / (count + 1);
        }

        public Single[] SendRowsToArray(DataTable dataTable, int columnOrdinal = 0)
        {
            int rowsCount = dataTable.Rows.Count;
            Single[] rowsOut = new Single[rowsCount - 1];
            for (int count = 0; count < dataTable.Rows.Count; count++)
            {
                rowsOut[count] = (Single)dataTable.Rows[count][columnOrdinal];
            }
            return rowsOut;
        }

        [TestCleanup]
        public void Shutdown()
        {
            XL.CloseWorkbook(ref wkb);
            XL.CloseApplication(ref xla);
        }

        private DataSet summaryTestDataset()
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();
            DataColumn dataColumn = dataTable.Columns.Add("Column1",typeof(System.Int16));
            for (int count = 1; count < 9; count++)
            {
                DataRow dataRow = dataTable.NewRow();
                /*
                DateTime testDate = new DateTime(DateTime.Now.Year, count, 1);
                String month = testDate.ToString("MMM");
                dataRow[0] = month;
                 */
                dataRow[0] = count;
                dataTable.Rows.Add(dataRow);
            }
            return dataSet;
        }

        private IList<Personnel> PopulateTAContractors()
        {
            IList<Personnel> tAContractors = new List<Personnel>();
            for (int count = 1; count < 4; count++)
            {
                //I'm getting  77.98 from the total 19650 / count (3) / hours (84)
                PopulateOperationsPerson(1, 77.98f, 84, 1f, "TA Contractor", ref tAContractors);
            }
            return tAContractors;
        }

        private IList<Personnel> PopulateTAOperationsStaffCompany()
        {
            IList<Personnel> tAOperationsStaffCompany = new List<Personnel>();
            PopulateOperationsPerson(1, hourlyWage, 44, 1f, "TA Operations Staff Company", ref tAOperationsStaffCompany);
            return tAOperationsStaffCompany;
        }

        private IList<Personnel> PopulateTAMaintenanceStaffCompany()
        {
            IList<Personnel> tAMaintenanceStaffCompany = new List<Personnel>();
            PopulateOperationsPerson(1, hourlyWage, 16, 1f, "TA Maintenance Staff Company", ref tAMaintenanceStaffCompany);
            return tAMaintenanceStaffCompany;
        }

        private IList<Personnel> PopulateContractMaintenanceServices()
        {
            IList<Personnel> maintenanceContractMaintenanceServices = new List<Personnel>();
            for (int count = 1; count < 6; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Contract Maintenance Services", ref maintenanceContractMaintenanceServices);
            }
            return maintenanceContractMaintenanceServices;
        }

        private IList<Personnel> PopulateMaintenanceStaffCompany()
        {
            IList<Personnel> maintenanceStaffCompanyPersonnel = new List<Personnel>();
            for (int count = 1; count < 19; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Maintenance Staff Company", ref maintenanceStaffCompanyPersonnel);
            }
            return maintenanceStaffCompanyPersonnel;
        }
         
        private IList<Personnel> PopulateAccountingPersonnelCost()
        {
            IList<Personnel> accountingPersonnel = new List<Personnel>();
            for (int count = 1; count < 2; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.25f, "Accounting and Financial Manager", ref accountingPersonnel);
            }
            for (int count = 1; count < 6; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 0.95f, "Accountant Clerks", ref accountingPersonnel);
            }
            return accountingPersonnel;
        }

        private IList<Personnel> PopulateTechQaPersonnelCost()
        {
            IList<Personnel> techQaPersonnel = new List<Personnel>();
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.25f, "Technical and Inspection Manager", ref techQaPersonnel);
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.15f, "Inspector", ref techQaPersonnel);
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.2f, "Engineer", ref techQaPersonnel);
            return techQaPersonnel;
        }

        private IList<Personnel> PopulateSitePersonnelCost()
        {
            IList<Personnel> sitePersonnel = new List<Personnel>();
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.5f, "Site Manager", ref sitePersonnel);
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 0.85f, "Site Admin", ref sitePersonnel);
            return sitePersonnel;
        }

        private IList<Personnel> PopulateOperationsPersonnelCost()
        {
            IList<Personnel> operationsPersonnel = new List<Personnel>();
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.25f, "Operations Manager", ref operationsPersonnel);
            for (int count = 1; count < 5; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.15f, "Operations Supervisors", ref operationsPersonnel);
            }
            for (int count = 1; count < 5; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.05f, "Operations Controllers", ref operationsPersonnel);
            }
            return operationsPersonnel;
        }

        private IList<Personnel> PopulateReceiptsPersonnelCost()
        {
            IList<Personnel> receiptsPersonnel = new List<Personnel>();
            for (int count = 1; count < 5; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Operations Staff Company", ref receiptsPersonnel);
            }
            for (int count = 1; count < 3; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Operations Staff Company", ref receiptsPersonnel);
            }
            return receiptsPersonnel;
        }

        private IList<Personnel> PopulateIssuePersonnelCost()
        {
            IList<Personnel> issuePersonnel = new List<Personnel>();
            for (int count = 1; count < 11; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Operations Staff Company", ref issuePersonnel);
            }
            for (int count = 1; count < 11; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Operations Staff Company", ref issuePersonnel);
            }
            for (int count = 1; count < 11; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Operations Staff Company", ref issuePersonnel);
            }
            for (int count = 1; count < 11; count++)
            {
                PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1f, "Operations Staff Company", ref issuePersonnel);
            }
            return issuePersonnel;
        }

        private IList<Personnel> PopulateMaintenanceManagersAndPlanners()
        {
            IList<Personnel> maintenanceManagersAndPlanners = new List<Personnel>();
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.25f, "Maintenance Manager", ref maintenanceManagersAndPlanners);
            PopulateOperationsPerson(1, hourlyWage, hoursInAYear, 1.1f, "Maintenance Manager", ref maintenanceManagersAndPlanners);
            return maintenanceManagersAndPlanners;
        }

        private void PopulateOperationsPerson(int count, Single wage, int hours, Single wageMultiplier, string position,
            ref IList<Personnel> operationsPersonnel)
        {
            Personnel operationsPerson = 
                new Personnel() { count = count, position = position, hours=hours, wage = wage, wageMultiplier = wageMultiplier };
            operationsPersonnel.Add(operationsPerson);
        }

        private IList<Sa.Iss.Upload.Cl.Calcs.TankVolumes> PopulateTankVolumes()
        {
            //TankVolumes[] tankVolumes = new TankVolumes[]();
            IList<Sa.Iss.Upload.Cl.Calcs.TankVolumes> tankVolumes = new List<Sa.Iss.Upload.Cl.Calcs.TankVolumes>();
            PopulateTankVolume("1", 386.115001703449f, ref tankVolumes);
            PopulateTankVolume("2", 386.115001703449f, ref tankVolumes);
            PopulateTankVolume("5", 1817.01177272211f, ref tankVolumes);
            PopulateTankVolume("6", 1817.01177272211f, ref tankVolumes);
            PopulateTankVolume("7", 935.003974713253f, ref tankVolumes);
            PopulateTankVolume("8", 935.003974713253f, ref tankVolumes);
            PopulateTankVolume("9", 935.003974713253f, ref tankVolumes);
            PopulateTankVolume("10", 935.003974713253f, ref tankVolumes);
            PopulateTankVolume("11", 935.003974713253f, ref tankVolumes);
            PopulateTankVolume("12", 2411.32604004997f, ref tankVolumes);
            PopulateTankVolume("19", 54.5103531816633f, ref tankVolumes);
            PopulateTankVolume("20", 54.5103531816633f, ref tankVolumes);
            PopulateTankVolume("21", 62.7815421887421f, ref tankVolumes);
            PopulateTankVolume("22", 62.7815421887421f, ref tankVolumes);
            PopulateTankVolume("Fire Water Tank", 836.582503690805f, ref tankVolumes);
            return tankVolumes;
        }
        private void PopulateTankVolume(string tankId, Single tankVolumeM3,
            ref IList<Sa.Iss.Upload.Cl.Calcs.TankVolumes> tankVolumes)
        {
            Sa.Iss.Upload.Cl.Calcs.TankVolumes tank = new Sa.Iss.Upload.Cl.Calcs.TankVolumes { tankId = tankId, tankVolumeM3 = (float)tankVolumeM3 };
            tankVolumes.Add(tank);
        }
        private String CutPath(String path, int LevelsToCut)
        {
            String returnValue = String.Empty;
            String[] pathParts = path.Split('\\');
            for (int count = 0; count < (pathParts.Length - LevelsToCut); count++)
            {
                returnValue += pathParts[count] + @"\";
            }
            return returnValue;
        }
    }

}
