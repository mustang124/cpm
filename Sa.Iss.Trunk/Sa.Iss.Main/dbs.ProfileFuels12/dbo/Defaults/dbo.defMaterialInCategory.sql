﻿CREATE DEFAULT [dbo].[defMaterialInCategory]
    AS 0;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInRMI]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInOTHRM]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInRChem]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInRLube]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInProd]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInFLube]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInFChem]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInASP]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInCoke]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInSolv]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInMProd]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[LubesOnly]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[MaterialInCategory]';

