﻿








CREATE    VIEW [dbo].[PersCalc] AS
SELECT e.SubmissionID, e.FactorSet, p.PersID, Description = (SELECT Description FROM Pers_LU WHERE Pers_LU.PersID = p.PersID), 
p.SortKey, p.SectionID, 
p.CompEqP, p.CompEqP*100000/PlantEDC AS CompEqPEDC,
p.ContEqP, p.ContEqP*100000/PlantEDC AS ContEqPEDC, 
p.GAEqP, p.GAEqP*100000/PlantEDC AS GAEqPEDC,
p.TotEqP, p.TotEqP*100000/PlantEDC AS TotEqPEDC, 
p.CompWHr, p.CompWHr*100/(PlantEDC*s.FractionOfYear) AS CompWHrEDC, 
p.ContWHr, p.ContWHr*100/(PlantEDC*s.FractionOfYear) AS ContWHrEDC,
p.GAWHr, p.GAWHr*100/(PlantEDC*s.FractionOfYear) AS GAWHrEDC, 
p.TotWHr, p.TotWHr*100/(PlantEDC*s.FractionOfYear) AS TotWHrEDC,
p.TotWHr*100/CASE WHEN e.PersEffDiv > 0 THEN e.PersEffDiv*s.FractionOfYear END AS TotWHrEffIndex,
p.TotWHr*100/CASE WHEN lu.MaintForceGroup IS NOT NULL AND e.MaintPersEffDiv > 0 THEN e.MaintPersEffDiv*s.FractionOfYear END AS MaintPersEffIndex,
p.NumPers, p.STH, p.OVTHours, p.OVTPcnt, p.Contract, p.GA, p.AbsHrs,
e.PlantEDC*s.FractionOfYear/100 AS WHrEDCDivisor,
e.PersEffDiv*s.FractionOfYear/100 AS EffDivisor,
e.MaintPersEffDiv*s.FractionOfYear/100 AS MaintPersEffDivisor
FROM Pers p INNER JOIN FactorTotCalc e ON e.SubmissionID = p.SubmissionID 
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = p.SubmissionID
INNER JOIN Pers_LU lu ON lu.PersID = p.PersID
WHERE e.PlantEDC>0


