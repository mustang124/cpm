﻿



CREATE   VIEW [dbo].[UnitIndicators]
AS
SELECT c.SubmissionID, c.UnitID, c.ProcessID, c.UnitName, mc.Currency, c.UtilPcnt, t.UtilPcnt AS UtilPcnt_Target, UtilOSTA = (SELECT AVG(UtilOSTA) FROM FactorCalc f WHERE f.SubmissionID = c.SubmissionID AND f.UnitID = c.UnitID),
a.MechAvail_Ann AS MechAvail, t.MechAvail AS MechAvail_Target, 
a.OpAvail_Ann AS OpAvail, t.OpAvail AS OpAvail_Target, 
a.OnStream_Ann AS OnStream, t.OnStream AS OnStream_Target,
RoutCostCap = ISNULL(mc.CurrRoutCost, 0)*lu.CostCapMult/CASE WHEN c.Cap > 0 THEN c.Cap END, 
AnnRoutCostCap = ISNULL(mc.AnnRoutCost, 0)*lu.CostCapMult/CASE WHEN c.Cap > 0 THEN c.Cap END, t.RoutCost*dbo.ExchangeRate(t.CurrencyCode, mc.Currency, s.PeriodStart) AS RoutCostCap_Target,
AnnTACostCap = ISNULL(mc.AnnTACost, 0)*lu.CostCapMult/CASE WHEN c.Cap > 0 THEN c.Cap END, t.TACost*dbo.ExchangeRate(t.CurrencyCode, mc.Currency, s.PeriodStart) AS AnnTACostCap_Target,
AnnMaintCostCap = (ISNULL(mc.AnnRoutCost, 0) + ISNULL(mc.AnnTACost, 0))*lu.CostCapMult/CASE WHEN c.Cap > 0 THEN c.Cap END,
UnitEII = (SELECT UnitEII FROM FactorCalc WHERE FactorCalc.SubmissionID = c.SubmissionID AND FactorCalc.UnitID = c.UnitID AND FactorSet = '2008'), 
t.UnitEII AS UnitEII_Target
--, (t.RoutCost + ISNULL(t.TACost, 0))*dbo.ExchangeRate(t.CurrencyCode, mc.Currency, s.PeriodStart) AS AnnMaintCostCap_Target
FROM Config c INNER JOIN MaintCalc a ON a.SubmissionID = c.SubmissionID AND a.UnitID = c.UnitID
INNER JOIN MaintCost mc ON mc.SubmissionID = c.SubmissionID AND mc.UnitID = c.UnitID
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = c.SubmissionID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
LEFT JOIN UnitTargets t ON t.SubmissionID = c.SubmissionID AND t.UnitID = c.UnitID



