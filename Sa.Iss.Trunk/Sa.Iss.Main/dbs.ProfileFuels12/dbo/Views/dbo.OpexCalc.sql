﻿


CREATE  VIEW [dbo].[OpExCalc] 
AS
SELECT	o.SubmissionID, o.Currency, o.Scenario, d.FactorSet, d.Divisor AS DataType, o.OCCSal / d.DivValue AS OCCSal, o.MPSSal / d.DivValue AS MPSSal, 
	o.OCCBen / d.DivValue AS OCCBen, o.MPSBen / d.DivValue AS MPSBen, o.MaintMatl / d.DivValue AS MaintMatl, 
	o.ContMaintLabor / d.DivValue AS ContMaintLabor, o.ContMaintInspect / d.DivValue AS ContMaintInspect, o.ContMaintLaborST / d.DivValue AS ContMaintLaborST, o.ContMaintMatl / d.DivValue AS ContMaintMatl, o.MaintMatlST / d.DivValue AS MaintMatlST, o.OthCont / d.DivValue AS OthCont, 
	o.EquipMaint / d.DivValue AS EquipMaint, o.EquipNonMaint / d.DivValue AS EquipNonMaint, (ISNULL(o.EquipMaint,0)+ISNULL(o.EquipNonMaint,0)) / d.DivValue AS Equip, 
	o.Tax / d.DivValue AS Tax, o.InsurBI / d.DivValue AS InsurBI, o.InsurPC / d.DivValue AS InsurPC, o.InsurOth / d.DivValue AS InsurOth, (ISNULL(o.InsurBI, 0) + ISNULL(o.InsurPC, 0) + ISNULL(InsurOth, 0)) / d.DivValue AS Insur, 
	o.TAAdj / d.DivValue AS TAAdj, o.Envir / d.DivValue AS Envir, 
	o.OthNonVol / d.DivValue AS OthNonVol, o.GAPers / d.DivValue AS GAPers, o.STNonVol / d.DivValue AS STNonVol, o.ChemicalsAntiknock / d.DivValue AS Antiknock,
	o.Chemicals / d.DivValue AS Chemicals, o.Catalysts / d.DivValue AS Catalysts, o.Royalties / d.DivValue AS Royalties, 
	o.PurElec / d.DivValue AS PurElec, o.PurSteam / d.DivValue AS PurSteam, o.PurOth / d.DivValue AS PurOth, o.PurFG / d.DivValue AS PurFG, 
	o.PurLiquid / d.DivValue AS PurLiquid, o.PurSolid / d.DivValue AS PurSolid, o.RefProdFG / d.DivValue AS RefProdFG, 
	o.RefProdOth / d.DivValue AS RefProdOth, o.EmissionsPurch / d.DivValue AS EmissionsPurch, o.EmissionsCredits / d.DivValue AS EmissionsCredits, 
	o.EmissionsTaxes / d.DivValue AS EmissionsTaxes, o.OthVol / d.DivValue AS OthVol, o.STVol / d.DivValue AS STVol, 
	o.TotCashOpEx / d.DivValue AS TotCashOpEx, o.GANonPers / d.DivValue AS GANonPers, o.InvenCarry / d.DivValue AS InvenCarry, 
	o.Depreciation / d.DivValue AS Depreciation, o.Interest / d.DivValue AS Interest, o.STNonCash / d.DivValue AS STNonCash, 
	o.TotRefExp / d.DivValue AS TotRefExp, o.Cogen / d.DivValue AS Cogen, o.OthRevenue / d.DivValue AS OthRevenue, 
	o.ThirdPartyTerminalRM / d.DivValue AS ThirdPartyTerminalRM, o.ThirdPartyTerminalProd / d.DivValue AS ThirdPartyTerminalProd, 
	o.POXO2 / d.DivValue AS POXO2, o.PMAA / d.DivValue AS PMAA, o.ExclFireSafety / d.DivValue AS FireSafetyLoss, 
	o.ExclEnvirFines / d.DivValue AS EnvirFines, o.ExclOth / d.DivValue AS ExclOth, o.TotExpExcl / d.DivValue AS TotExpExcl, o.STSal / d.DivValue AS STSal, 
	o.STBen / d.DivValue AS STBen, o.PersCostExclTA / d.DivValue AS PersCostExclTA, o.PersCost / d.DivValue AS PersCost, 
	o.EnergyCost / d.DivValue AS EnergyCost, o.NEOpEx / d.DivValue AS NEOpEx, d.DivValue AS Divisor
FROM OpExAll o INNER JOIN Divisors d ON d.SubmissionID = o.SubmissionID 
WHERE (o.DataType = 'ADJ') AND (d.DivValue <> 0)



