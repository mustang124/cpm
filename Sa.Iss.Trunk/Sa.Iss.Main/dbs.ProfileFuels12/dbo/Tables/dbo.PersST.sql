﻿CREATE TABLE [dbo].[PersST] (
    [SubmissionID] INT                   NOT NULL,
    [SectionID]    [dbo].[PersSectionID] NOT NULL,
    [NumPers]      REAL                  NULL,
    [STH]          FLOAT (53)            NULL,
    [OVTHours]     FLOAT (53)            NULL,
    [OVTPcnt]      REAL                  NULL,
    [Contract]     FLOAT (53)            NULL,
    [GA]           REAL                  NULL,
    [AbsHrs]       REAL                  NULL,
    [CompEqP]      REAL                  NULL,
    [ContEqP]      REAL                  NULL,
    [GAEqP]        REAL                  NULL,
    [TotEqP]       REAL                  NULL,
    [CompWHr]      REAL                  NULL,
    [ContWHr]      REAL                  NULL,
    [GAWHr]        REAL                  NULL,
    [TotWHr]       REAL                  NULL,
    CONSTRAINT [PK_PersST] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [SectionID] ASC) WITH (FILLFACTOR = 90)
);

