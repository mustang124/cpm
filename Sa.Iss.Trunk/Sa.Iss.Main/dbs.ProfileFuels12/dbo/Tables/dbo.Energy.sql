﻿CREATE TABLE [dbo].[Energy] (
    [SubmissionID]   INT                     NOT NULL,
    [TransCode]      SMALLINT                NOT NULL,
    [EnergyType]     [dbo].[EnergyType]      NOT NULL,
    [TransType]      [dbo].[EnergyTransType] NOT NULL,
    [TransferTo]     CHAR (3)                NULL,
    [SourceMBTU]     FLOAT (53)              NULL,
    [UsageMBTU]      FLOAT (53)              NULL,
    [PriceMBTUUS]    REAL                    NULL,
    [PriceMBTULocal] REAL                    NULL,
    [RptSource]      FLOAT (53)              CONSTRAINT [DF_Energy_RptSource] DEFAULT (0) NOT NULL,
    [RptPriceLocal]  REAL                    CONSTRAINT [DF_Energy_RptPriceLocal] DEFAULT (0) NOT NULL,
    [Hydrogen]       REAL                    NULL,
    [Methane]        REAL                    NULL,
    [Ethane]         REAL                    NULL,
    [Ethylene]       REAL                    NULL,
    [Propane]        REAL                    NULL,
    [Propylene]      REAL                    NULL,
    [Butane]         REAL                    NULL,
    [Isobutane]      REAL                    NULL,
    [Butylenes]      REAL                    NULL,
    [C5Plus]         REAL                    NULL,
    [CO]             REAL                    NULL,
    [CO2]            REAL                    NULL,
    [N2]             REAL                    NULL,
    [Inerts]         REAL                    NULL,
    [Total]          REAL                    NULL,
    [TotalInerts]    REAL                    NULL,
    [HeatContentVol] REAL                    NULL,
    [HeatContentWt]  REAL                    NULL,
    [MolWt]          REAL                    NULL,
    [PsuedoDensity]  REAL                    NULL,
    [MBTU]           FLOAT (53)              NULL,
    [HydrogenWt]     REAL                    NULL,
    [MethaneWt]      REAL                    NULL,
    [EthaneWt]       REAL                    NULL,
    [EthyleneWt]     REAL                    NULL,
    [PropaneWt]      REAL                    NULL,
    [PropyleneWt]    REAL                    NULL,
    [ButaneWt]       REAL                    NULL,
    [IsobutaneWt]    REAL                    NULL,
    [ButylenesWt]    REAL                    NULL,
    [C5PlusWt]       REAL                    NULL,
    [COWt]           REAL                    NULL,
    [CO2Wt]          REAL                    NULL,
    [N2Wt]           REAL                    NULL,
    [InertsWt]       REAL                    NULL,
    [TotalWt]        REAL                    NULL,
    [TotalInertsWt]  REAL                    NULL,
    [FOEBblPerMT]    REAL                    NULL,
    [MSCF]           REAL                    NULL,
    [MLb]            REAL                    NULL,
    [CEF]            REAL                    NULL,
    [H2S]            REAL                    NULL,
    [NH3]            REAL                    NULL,
    [SO2]            REAL                    NULL,
    [OthInerts]      REAL                    NULL,
    [H2SWt]          REAL                    NULL,
    [NH3Wt]          REAL                    NULL,
    [SO2Wt]          REAL                    NULL,
    [OthInertsWt]    REAL                    NULL,
    CONSTRAINT [PK_Energy_1] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [TransCode] ASC) WITH (FILLFACTOR = 90)
);

