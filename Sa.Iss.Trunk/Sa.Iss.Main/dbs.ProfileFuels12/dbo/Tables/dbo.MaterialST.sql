﻿CREATE TABLE [dbo].[MaterialST] (
    [SubmissionID] INT                   NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [GrossBbl]     FLOAT (53)            NULL,
    [GrossMT]      FLOAT (53)            NULL,
    [NetBbl]       FLOAT (53)            NULL,
    [NetMT]        FLOAT (53)            NULL,
    CONSTRAINT [PK_MaterialST] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Category] ASC) WITH (FILLFACTOR = 90)
);

