﻿CREATE TABLE [dbo].[EnergyBackup] (
    [SubmissionID]   INT                     NOT NULL,
    [TransCode]      SMALLINT                NOT NULL,
    [EnergyType]     [dbo].[EnergyType]      NOT NULL,
    [TransType]      [dbo].[EnergyTransType] NOT NULL,
    [TransferTo]     CHAR (3)                NULL,
    [SourceMBTU]     FLOAT (53)              NULL,
    [UsageMBTU]      FLOAT (53)              NULL,
    [PriceMBTUUS]    REAL                    NULL,
    [PriceMBTULocal] REAL                    NULL,
    [RptSource]      FLOAT (53)              NOT NULL,
    [RptPriceLocal]  REAL                    NOT NULL
);

