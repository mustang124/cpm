﻿CREATE TABLE [dbo].[Table2_LU] (
    [ProcessID]             [dbo].[ProcessID] NOT NULL,
    [Property]              VARCHAR (30)      NOT NULL,
    [USDescription]         VARCHAR (60)      NOT NULL,
    [USDecPlaces]           TINYINT           CONSTRAINT [DF_Table2_LU_USDecPlaces] DEFAULT (0) NOT NULL,
    [MetDescription]        VARCHAR (60)      NOT NULL,
    [MetDecPlaces]          TINYINT           CONSTRAINT [DF_Table2_LU_MetDecPlaces] DEFAULT (0) NOT NULL,
    [USUOM]                 VARCHAR (20)      NULL,
    [MetricUOM]             VARCHAR (20)      NULL,
    [CustomGroup]           TINYINT           CONSTRAINT [DF_Table2_LU_CustomGroup] DEFAULT (0) NOT NULL,
    [FormulaSymbol]         VARCHAR (1)       NULL,
    [SortKey]               INT               NULL,
    [USDescriptionRussian]  NVARCHAR (100)    NULL,
    [MetDescriptionRussian] NVARCHAR (100)    NULL,
    CONSTRAINT [PK_Table2_LU] PRIMARY KEY CLUSTERED ([ProcessID] ASC, [Property] ASC) WITH (FILLFACTOR = 90)
);

