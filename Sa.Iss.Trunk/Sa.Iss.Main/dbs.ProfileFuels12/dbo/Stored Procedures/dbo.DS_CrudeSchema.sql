﻿CREATE PROCEDURE [dbo].[DS_CrudeSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(CNum) as CNum, RTRIM(CrudeName) as CrudeName, Gravity, Sulfur, 0.0 AS  Bbl,0.0 AS CostPerBbl 
		FROM Crude 
		WHERE SubmissionID=(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID ORDER BY PeriodStart DESC)
END


