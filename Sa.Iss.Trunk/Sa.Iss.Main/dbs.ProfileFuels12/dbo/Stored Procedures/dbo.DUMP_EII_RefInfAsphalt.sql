﻿CREATE PROCEDURE [dbo].[DUMP_EII_RefInfAsphalt]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@StudyYear nvarchar(20),
	@DataSetID nvarchar(10),
	@Refnum nvarchar(10)
	
	AS
	
	SELECT s.Location,s.PeriodStart,s.PeriodEnd,
                    s.NumDays as DaysInPeriod,@CurrencyCode  AS Currency,@UOM AS UOM , 
                     AspUtilCap, AspStdEnergy, 115 as ASPEIIFormula 
                     FROM FactorTotCalc f , Submissions s WHERE f.FactorSet=@StudyYear
                     AND s.SubmissionID=f.SubmissionID AND f.SubmissionID IN 
                    (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=@DataSetID
                     AND  RefineryID=@Refnum
                     )  Order By PeriodStart DESC

