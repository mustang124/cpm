﻿CREATE    PROC [dbo].[spReportPUScorecardRS] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency= @Currency, UOM = @UOM, 
c.UnitID, c.ProcessID, c.ProcessType, ISNULL(c.Throughput,0) As BPD, ISNULL(EDCNoMult,0) AS EDC, 
ISNULL(UEDCNoMult,0) AS UEDC
FROM Submissions s INNER JOIN ConfigRS c ON c.SubmissionID = s.SubmissionID
INNER JOIN FactorCalc fc ON c.SubmissionID = fc.SubmissionID AND c.UnitID = fc.UnitID
INNER JOIN ProcessID_LU p ON c.ProcessID = p.ProcessID
INNER JOIN DisplayUnits_LU d ON p.DisplayUnits = d.DisplayUnits
WHERE fc.FactorSet=@FactorSet
AND s.RefineryID=@RefineryID AND s.DataSet = @DataSet AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.UseSubmission = 1
ORDER BY s.PeriodStart DESC


