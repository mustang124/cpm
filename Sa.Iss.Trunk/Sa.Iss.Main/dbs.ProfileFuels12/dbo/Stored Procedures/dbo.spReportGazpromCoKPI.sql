﻿CREATE   PROC [dbo].[spReportGazpromCoKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

IF @RefineryID NOT IN ('106FL','150FL','322EUR')
	RETURN 1

/*
106FL = O
150FL = Y
322EUR = M
*/
DECLARE	
	@M_EII real, @M_EII_QTR real, @M_EII_Avg real, 
	@M_RefUtilPcnt real, @M_RefUtilPcnt_QTR real, @M_RefUtilPcnt_Avg real, 
	@M_VEI real, @M_VEI_QTR real, @M_VEI_Avg real, 
	@M_OpAvail real, @M_OpAvail_QTR real, @M_OpAvail_Avg real, 
	@M_RoutIndex real, @M_RoutIndex_QTR real, @M_RoutIndex_Avg real,
	@M_PersIndex real, @M_PersIndex_QTR real, @M_PersIndex_Avg real, 
	@M_NEOpExEDC real, @M_NEOpExEDC_QTR real, @M_NEOpExEDC_Avg real, 
	@M_OpExUEDC real, @M_OpExUEDC_QTR real, @M_OpExUEDC_Avg real, 
	@O_EII real, @O_EII_QTR real, @O_EII_Avg real, 
	@O_RefUtilPcnt real, @O_RefUtilPcnt_QTR real, @O_RefUtilPcnt_Avg real, 
	@O_VEI real, @O_VEI_QTR real, @O_VEI_Avg real, 
	@O_OpAvail real, @O_OpAvail_QTR real, @O_OpAvail_Avg real, 
	@O_RoutIndex real, @O_RoutIndex_QTR real, @O_RoutIndex_Avg real,
	@O_PersIndex real, @O_PersIndex_QTR real, @O_PersIndex_Avg real, 
	@O_NEOpExEDC real, @O_NEOpExEDC_QTR real, @O_NEOpExEDC_Avg real, 
	@O_OpExUEDC real, @O_OpExUEDC_QTR real, @O_OpExUEDC_Avg real, 
	@Y_EII real, @Y_EII_QTR real, @Y_EII_Avg real, 
	@Y_RefUtilPcnt real, @Y_RefUtilPcnt_QTR real, @Y_RefUtilPcnt_Avg real, 
	@Y_VEI real, @Y_VEI_QTR real, @Y_VEI_Avg real, 
	@Y_OpAvail real, @Y_OpAvail_QTR real, @Y_OpAvail_Avg real, 
	@Y_RoutIndex real, @Y_RoutIndex_QTR real, @Y_RoutIndex_Avg real,
	@Y_PersIndex real, @Y_PersIndex_QTR real, @Y_PersIndex_Avg real, 
	@Y_NEOpExEDC real, @Y_NEOpExEDC_QTR real, @Y_NEOpExEDC_Avg real, 
	@Y_OpExUEDC real, @Y_OpExUEDC_QTR real, @Y_OpExUEDC_Avg real 

DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc] '322EUR', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @M_EII OUTPUT, @EII_QTR = @M_EII_QTR OUTPUT, @EII_Avg = @M_EII_Avg OUTPUT, 
	@RefUtilPcnt = @M_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @M_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @M_RefUtilPcnt_Avg OUTPUT, 
	@VEI = @M_VEI OUTPUT, @VEI_QTR = @M_VEI_QTR OUTPUT, @VEI_Avg = @M_VEI_Avg OUTPUT, 
	@OpAvail = @M_OpAvail OUTPUT, @OpAvail_QTR = @M_OpAvail_QTR OUTPUT, @OpAvail_Avg = @M_OpAvail_Avg OUTPUT, 
	@RoutIndex = @M_RoutIndex OUTPUT, @RoutIndex_QTR = @M_RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @M_RoutIndex_Avg OUTPUT,
	@PersIndex = @M_PersIndex OUTPUT, @PersIndex_QTR = @M_PersIndex_QTR OUTPUT, @PersIndex_Avg = @M_PersIndex_Avg OUTPUT, 
	@NEOpExEDC = @M_NEOpExEDC OUTPUT, @NEOpExEDC_QTR = @M_NEOpExEDC_QTR OUTPUT, @NEOpExEDC_Avg = @M_NEOpExEDC_Avg OUTPUT, 
	@OpExUEDC = @M_OpExUEDC OUTPUT, @OpExUEDC_QTR = @M_OpExUEDC_QTR OUTPUT, @OpExUEDC_Avg = @M_OpExUEDC_Avg OUTPUT

IF @spResult > 1	
	RETURN @spResult

EXEC @spResult = [dbo].[spReportGazpromKPICalc] '106FL', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @O_EII OUTPUT, @EII_QTR = @O_EII_QTR OUTPUT, @EII_Avg = @O_EII_Avg OUTPUT, 
	@RefUtilPcnt = @O_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @O_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @O_RefUtilPcnt_Avg OUTPUT, 
	@VEI = @O_VEI OUTPUT, @VEI_QTR = @O_VEI_QTR OUTPUT, @VEI_Avg = @O_VEI_Avg OUTPUT, 
	@OpAvail = @O_OpAvail OUTPUT, @OpAvail_QTR = @O_OpAvail_QTR OUTPUT, @OpAvail_Avg = @O_OpAvail_Avg OUTPUT, 
	@RoutIndex = @O_RoutIndex OUTPUT, @RoutIndex_QTR = @O_RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @O_RoutIndex_Avg OUTPUT,
	@PersIndex = @O_PersIndex OUTPUT, @PersIndex_QTR = @O_PersIndex_QTR OUTPUT, @PersIndex_Avg = @O_PersIndex_Avg OUTPUT, 
	@NEOpExEDC = @O_NEOpExEDC OUTPUT, @NEOpExEDC_QTR = @O_NEOpExEDC_QTR OUTPUT, @NEOpExEDC_Avg = @O_NEOpExEDC_Avg OUTPUT, 
	@OpExUEDC = @O_OpExUEDC OUTPUT, @OpExUEDC_QTR = @O_OpExUEDC_QTR OUTPUT, @OpExUEDC_Avg = @O_OpExUEDC_Avg OUTPUT

IF @spResult > 1	
	RETURN @spResult

EXEC @spResult = [dbo].[spReportGazpromKPICalc] '150FL', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @Y_EII OUTPUT, @EII_QTR = @Y_EII_QTR OUTPUT, @EII_Avg = @Y_EII_Avg OUTPUT, 
	@RefUtilPcnt = @Y_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @Y_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @Y_RefUtilPcnt_Avg OUTPUT, 
	@VEI = @Y_VEI OUTPUT, @VEI_QTR = @Y_VEI_QTR OUTPUT, @VEI_Avg = @Y_VEI_Avg OUTPUT, 
	@OpAvail = @Y_OpAvail OUTPUT, @OpAvail_QTR = @Y_OpAvail_QTR OUTPUT, @OpAvail_Avg = @Y_OpAvail_Avg OUTPUT, 
	@RoutIndex = @Y_RoutIndex OUTPUT, @RoutIndex_QTR = @Y_RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @Y_RoutIndex_Avg OUTPUT,
	@PersIndex = @Y_PersIndex OUTPUT, @PersIndex_QTR = @Y_PersIndex_QTR OUTPUT, @PersIndex_Avg = @Y_PersIndex_Avg OUTPUT, 
	@NEOpExEDC = @Y_NEOpExEDC OUTPUT, @NEOpExEDC_QTR = @Y_NEOpExEDC_QTR OUTPUT, @NEOpExEDC_Avg = @Y_NEOpExEDC_Avg OUTPUT, 
	@OpExUEDC = @Y_OpExUEDC OUTPUT, @OpExUEDC_QTR = @Y_OpExUEDC_QTR OUTPUT, @OpExUEDC_Avg = @Y_OpExUEDC_Avg OUTPUT

IF @spResult > 1	
	RETURN @spResult

SELECT 
	M_EII = @M_EII, M_EII_QTR = @M_EII_QTR, M_EII_Avg = @M_EII_Avg, 
	M_UtilPcnt = @M_RefUtilPcnt, M_UtilPcnt_QTR = @M_RefUtilPcnt_QTR, M_UtilPcnt_Avg = @M_RefUtilPcnt_Avg, 
	M_VEI = @M_VEI, M_VEI_QTR = @M_VEI_QTR, M_VEI_Avg = @M_VEI_Avg, 
	M_OpAvail = @M_OpAvail, M_OpAvail_QTR = @M_OpAvail_QTR, M_OpAvail_Avg = @M_OpAvail_Avg, 
	M_RoutIndex = @M_RoutIndex, M_RoutIndex_QTR = @M_RoutIndex_QTR, M_RoutIndex_Avg = @M_RoutIndex_Avg, 
	M_TotWHrEDC = @M_PersIndex, M_TotWHrEDC_QTR = @M_PersIndex_QTR, M_TotWHrEDC_Avg = @M_PersIndex_Avg, 
	M_NEOpExEDC = @M_NEOpExEDC, M_NEOpExEDC_QTR = @M_NEOpExEDC_QTR, M_NEOpExEDC_Avg = @M_NEOpExEDC_Avg, 
	M_TotCashOpExUEDC = @M_OpExUEDC, M_TotCashOpExUEDC_QTR = @M_OpExUEDC_QTR, M_TotCashOpExUEDC_Avg = @M_OpExUEDC_Avg,

	O_EII = @O_EII, O_EII_QTR = @O_EII_QTR, O_EII_Avg = @O_EII_Avg, 
	O_UtilPcnt = @O_RefUtilPcnt, O_UtilPcnt_QTR = @O_RefUtilPcnt_QTR, O_UtilPcnt_Avg = @O_RefUtilPcnt_Avg, 
	O_VEI = @O_VEI, O_VEI_QTR = @O_VEI_QTR, O_VEI_Avg = @O_VEI_Avg, 
	O_OpAvail = @O_OpAvail, O_OpAvail_QTR = @O_OpAvail_QTR, O_OpAvail_Avg = @O_OpAvail_Avg, 
	O_RoutIndex = @O_RoutIndex, O_RoutIndex_QTR = @O_RoutIndex_QTR, O_RoutIndex_Avg = @O_RoutIndex_Avg, 
	O_TotWHrEDC = @O_PersIndex, O_TotWHrEDC_QTR = @O_PersIndex_QTR, O_TotWHrEDC_Avg = @O_PersIndex_Avg, 
	O_NEOpExEDC = @O_NEOpExEDC, O_NEOpExEDC_QTR = @O_NEOpExEDC_QTR, O_NEOpExEDC_Avg = @O_NEOpExEDC_Avg, 
	O_TotCashOpExUEDC = @O_OpExUEDC, O_TotCashOpExUEDC_QTR = @O_OpExUEDC_QTR, O_TotCashOpExUEDC_Avg = @O_OpExUEDC_Avg,

	Y_EII = @Y_EII, Y_EII_QTR = @Y_EII_QTR, Y_EII_Avg = @Y_EII_Avg, 
	Y_UtilPcnt = @Y_RefUtilPcnt, Y_UtilPcnt_QTR = @Y_RefUtilPcnt_QTR, Y_UtilPcnt_Avg = @Y_RefUtilPcnt_Avg, 
	Y_VEI = @Y_VEI, Y_VEI_QTR = @Y_VEI_QTR, Y_VEI_Avg = @Y_VEI_Avg, 
	Y_OpAvail = @Y_OpAvail, Y_OpAvail_QTR = @Y_OpAvail_QTR, Y_OpAvail_Avg = @Y_OpAvail_Avg, 
	Y_RoutIndex = @Y_RoutIndex, Y_RoutIndex_QTR = @Y_RoutIndex_QTR, Y_RoutIndex_Avg = @Y_RoutIndex_Avg, 
	Y_TotWHrEDC = @Y_PersIndex, Y_TotWHrEDC_QTR = @Y_PersIndex_QTR, Y_TotWHrEDC_Avg = @Y_PersIndex_Avg, 
	Y_NEOpExEDC = @Y_NEOpExEDC, Y_NEOpExEDC_QTR = @Y_NEOpExEDC_QTR, Y_NEOpExEDC_Avg = @Y_NEOpExEDC_Avg, 
	Y_TotCashOpExUEDC = @Y_OpExUEDC, Y_TotCashOpExUEDC_QTR = @Y_OpExUEDC_QTR, Y_TotCashOpExUEDC_Avg = @Y_OpExUEDC_Avg

