﻿CREATE PROC [dbo].[GetInputData]
	
	@RefineryID nvarchar(10)	
	
	AS
	
BEGIN
	EXEC SS_GetInputSettings @RefineryID
	EXEC SS_GetInputConfig @RefineryID
	EXEC SS_GetInputConfigRS @RefineryID
	EXEC SS_GetInputInventory @RefineryID
	EXEC SS_GetInputProcessData @RefineryID
	EXEC SS_GetInputOpEx @RefineryID
	EXEC SS_GetInputPers @RefineryID
	EXEC SS_GetInputAbsence @RefineryID
	EXEC SS_GetInputMaintTA_Process @RefineryID
	EXEC SS_GetInputMaintTA_Other @RefineryID
	EXEC SS_GetInputMaintRout @RefineryID
	EXEC SS_GetInputCrude @RefineryID
	EXEC SS_GetInputYieldRM @RefineryID
	EXEC SS_GetInputYieldRMB @RefineryID
	EXEC SS_GetInputYieldProd @RefineryID
	EXEC SS_GetInputEnergy @RefineryID
	EXEC SS_GetInputElectric @RefineryID
	EXEC SS_GetInputRefTargets @RefineryID
	EXEC SS_GetInputUnitTargets @RefineryID
	EXEC SS_GetInputUserDefined @RefineryID
	EXEC SS_GetInputOpExAdd @RefineryID
	EXEC SS_GetInputEDCStabilizers @RefineryID
	EXEC SS_GetInputOpExAll @RefineryID
End
