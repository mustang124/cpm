﻿CREATE   PROC [dbo].[spReportValeroBURByRef] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2008', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1,
	@KEDC real = NULL OUTPUT, @MechAvail real = NULL OUTPUT, @AdjMaintIndex real = NULL OUTPUT, 
	@UtilPcnt real = NULL OUTPUT, @NEOpExEDC real = NULL OUTPUT, @EII real = NULL OUTPUT, 
	@TotWHrEDC real = NULL OUTPUT, @NetInputKBPD real = NULL OUTPUT, @CrudeKBPD real = NULL OUTPUT, @FCCRateKBPD real = NULL OUTPUT,  
	@NumContPers real = NULL OUTPUT, @RoutMechUnavail real = NULL OUTPUT, @TAMechUnavail real = NULL OUTPUT)

AS
SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, 'Actual')

SELECT	@KEDC = NULL, @MechAvail = NULL, @AdjMaintIndex = NULL, 
	@UtilPcnt = NULL, @NEOpExEDC = NULL, @EII = NULL, 
	@TotWHrEDC = NULL, @NetInputKBPD = NULL, @CrudeKBPD = NULL, @FCCRateKBPD = NULL,  
	@NumContPers = NULL, @RoutMechUnavail = NULL, @TAMechUnavail = NULL
IF @SubmissionID IS NULL
	RETURN 0

SELECT @KEDC = EDC/1000, @MechAvail = MechAvail, @AdjMaintIndex = RoutIndex + TAIndex_Avg, 
	@UtilPcnt = UtilPcnt, @NEOpExEDC = NEOpExEDC, @EII = EII, @TotWHrEDC = TotWHrEDC, 
	@NetInputKBPD = NetInputBPD, @CrudeKBPD = CrudeInputBPD	
FROM GenSum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @NumContPers = SUM(Contract)/@NumDays*365.0/2080.0
FROM Pers p 
WHERE SubmissionID = @SubmissionID AND p.PersID NOT IN ('OCCTAADJ','MPSTAADJ') AND p.PersID IN ('OCCPO','OCCMAS','OCCAS','MPSPO','MPSMA','MPSTS','MPSAS')

SELECT @TAMechUnavail = MechUnavailTA_Ann, @RoutMechUnavail = 100-MechAvail_Act-MechUnavailTA_Act
FROM MaintAvailCalc 
WHERE SubmissionID = @SubmissionID AND FactorSet = '2008'

SELECT @FCCRateKBPD = SUM(UtilCap)/1000
FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'FCC'

SELECT @CrudeKBPD = SUM(UtilCap)/1000
FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'CDU'

