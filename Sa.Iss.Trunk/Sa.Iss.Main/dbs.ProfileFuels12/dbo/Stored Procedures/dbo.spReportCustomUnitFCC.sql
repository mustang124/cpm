﻿CREATE    PROC [dbo].[spReportCustomUnitFCC] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US', 
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

EXEC dbo.spReportCustomUnit @RefineryID, @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM, @ProcessID = 'FCC'
