﻿CREATE   PROC [dbo].[spReportValeroBUR] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

DECLARE @AD_KEDC real, @AD_KEDC_YTD real, @AD_MechAvail real, @AD_MechAvail_YTD real, @AD_AdjMaintIndex real, @AD_MaintIndex_YTD real, 
	@AD_UtilPcnt real, @AD_UtilPcnt_YTD real, @AD_NEOpExEDC real, @AD_NEOpExEDC_YTD real, 
	@AD_EII real, @AD_EII_YTD real, @AD_TotWHrEDC real, @AD_TotWHrEDC_YTD real
DECLARE @BN_KEDC real, @BN_KEDC_YTD real, @BN_MechAvail real, @BN_MechAvail_YTD real, @BN_AdjMaintIndex real, @BN_MaintIndex_YTD real, 
	@BN_UtilPcnt real, @BN_UtilPcnt_YTD real, @BN_NEOpExEDC real, @BN_NEOpExEDC_YTD real, 
	@BN_EII real, @BN_EII_YTD real, @BN_TotWHrEDC real, @BN_TotWHrEDC_YTD real
DECLARE @CC_KEDC real, @CC_KEDC_YTD real, @CC_MechAvail real, @CC_MechAvail_YTD real, @CC_AdjMaintIndex real, @CC_MaintIndex_YTD real, 
	@CC_UtilPcnt real, @CC_UtilPcnt_YTD real, @CC_NEOpExEDC real, @CC_NEOpExEDC_YTD real, 
	@CC_EII real, @CC_EII_YTD real, @CC_TotWHrEDC real, @CC_TotWHrEDC_YTD real
DECLARE @HO_KEDC real, @HO_KEDC_YTD real, @HO_MechAvail real, @HO_MechAvail_YTD real, @HO_AdjMaintIndex real, @HO_MaintIndex_YTD real, 
	@HO_UtilPcnt real, @HO_UtilPcnt_YTD real, @HO_NEOpExEDC real, @HO_NEOpExEDC_YTD real, 
	@HO_EII real, @HO_EII_YTD real, @HO_TotWHrEDC real, @HO_TotWHrEDC_YTD real
DECLARE @MK_KEDC real, @MK_KEDC_YTD real, @MK_MechAvail real, @MK_MechAvail_YTD real, @MK_AdjMaintIndex real, @MK_MaintIndex_YTD real, 
	@MK_UtilPcnt real, @MK_UtilPcnt_YTD real, @MK_NEOpExEDC real, @MK_NEOpExEDC_YTD real, 
	@MK_EII real, @MK_EII_YTD real, @MK_TotWHrEDC real, @MK_TotWHrEDC_YTD real
DECLARE @MP_KEDC real, @MP_KEDC_YTD real, @MP_MechAvail real, @MP_MechAvail_YTD real, @MP_AdjMaintIndex real, @MP_MaintIndex_YTD real, 
	@MP_UtilPcnt real, @MP_UtilPcnt_YTD real, @MP_NEOpExEDC real, @MP_NEOpExEDC_YTD real, 
	@MP_EII real, @MP_EII_YTD real, @MP_TotWHrEDC real, @MP_TotWHrEDC_YTD real
DECLARE @PA_KEDC real, @PA_KEDC_YTD real, @PA_MechAvail real, @PA_MechAvail_YTD real, @PA_AdjMaintIndex real, @PA_MaintIndex_YTD real, 
	@PA_UtilPcnt real, @PA_UtilPcnt_YTD real, @PA_NEOpExEDC real, @PA_NEOpExEDC_YTD real, 
	@PA_EII real, @PA_EII_YTD real, @PA_TotWHrEDC real, @PA_TotWHrEDC_YTD real
DECLARE @QB_KEDC real, @QB_KEDC_YTD real, @QB_MechAvail real, @QB_MechAvail_YTD real, @QB_AdjMaintIndex real, @QB_MaintIndex_YTD real, 
	@QB_UtilPcnt real, @QB_UtilPcnt_YTD real, @QB_NEOpExEDC real, @QB_NEOpExEDC_YTD real, 
	@QB_EII real, @QB_EII_YTD real, @QB_TotWHrEDC real, @QB_TotWHrEDC_YTD real
DECLARE @SC_KEDC real, @SC_KEDC_YTD real, @SC_MechAvail real, @SC_MechAvail_YTD real, @SC_AdjMaintIndex real, @SC_MaintIndex_YTD real, 
	@SC_UtilPcnt real, @SC_UtilPcnt_YTD real, @SC_NEOpExEDC real, @SC_NEOpExEDC_YTD real, 
	@SC_EII real, @SC_EII_YTD real, @SC_TotWHrEDC real, @SC_TotWHrEDC_YTD real
DECLARE @TC_KEDC real, @TC_KEDC_YTD real, @TC_MechAvail real, @TC_MechAvail_YTD real, @TC_AdjMaintIndex real, @TC_MaintIndex_YTD real, 
	@TC_UtilPcnt real, @TC_UtilPcnt_YTD real, @TC_NEOpExEDC real, @TC_NEOpExEDC_YTD real, 
	@TC_EII real, @TC_EII_YTD real, @TC_TotWHrEDC real, @TC_TotWHrEDC_YTD real
DECLARE @TR_KEDC real, @TR_KEDC_YTD real, @TR_MechAvail real, @TR_MechAvail_YTD real, @TR_AdjMaintIndex real, @TR_MaintIndex_YTD real, 
	@TR_UtilPcnt real, @TR_UtilPcnt_YTD real, @TR_NEOpExEDC real, @TR_NEOpExEDC_YTD real, 
	@TR_EII real, @TR_EII_YTD real, @TR_TotWHrEDC real, @TR_TotWHrEDC_YTD real
DECLARE @WM_KEDC real, @WM_KEDC_YTD real, @WM_MechAvail real, @WM_MechAvail_YTD real, @WM_AdjMaintIndex real, @WM_MaintIndex_YTD real, 
	@WM_UtilPcnt real, @WM_UtilPcnt_YTD real, @WM_NEOpExEDC real, @WM_NEOpExEDC_YTD real, 
	@WM_EII real, @WM_EII_YTD real, @WM_TotWHrEDC real, @WM_TotWHrEDC_YTD real
DECLARE @VALERO_KEDC real, @VALERO_KEDC_YTD real, @VALERO_MechAvail real, @VALERO_MechAvail_YTD real, @VALERO_AdjMaintIndex real, @VALERO_MaintIndex_YTD real, 
	@VALERO_UtilPcnt real, @VALERO_UtilPcnt_YTD real, @VALERO_NEOpExEDC real, @VALERO_NEOpExEDC_YTD real, 
	@VALERO_EII real, @VALERO_EII_YTD real, @VALERO_TotWHrEDC real, @VALERO_TotWHrEDC_YTD real

SELECT @AD_KEDC = EDC/1000, @AD_KEDC_YTD = EDC_YTD/1000, 
	@AD_MechAvail = MechAvail, @AD_MechAvail_YTD = MechAvail_YTD,
	@AD_AdjMaintIndex = RoutIndex + TAIndex_Avg, @AD_MaintIndex_YTD = MaintIndex_YTD,
	@AD_UtilPcnt = UtilPcnt, @AD_UtilPcnt_YTD = UtilPcnt_YTD,
	@AD_NEOpExEDC = NEOpExEDC, @AD_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@AD_EII = EII, @AD_EII_YTD = EII_YTD, @AD_TotWHrEDC = TotWHrEDC, @AD_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('75NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @BN_KEDC = EDC/1000, @BN_KEDC_YTD = EDC_YTD/1000, 
	@BN_MechAvail = MechAvail, @BN_MechAvail_YTD = MechAvail_YTD,
	@BN_AdjMaintIndex = RoutIndex + TAIndex_Avg, @BN_MaintIndex_YTD = MaintIndex_YTD,
	@BN_UtilPcnt = UtilPcnt, @BN_UtilPcnt_YTD = UtilPcnt_YTD,
	@BN_NEOpExEDC = NEOpExEDC, @BN_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@BN_EII = EII, @BN_EII_YTD = EII_YTD, @BN_TotWHrEDC = TotWHrEDC, @BN_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('33NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @CC_KEDC = EDC/1000, @CC_KEDC_YTD = EDC_YTD/1000, 
	@CC_MechAvail = MechAvail, @CC_MechAvail_YTD = MechAvail_YTD,
	@CC_AdjMaintIndex = RoutIndex + TAIndex_Avg, @CC_MaintIndex_YTD = MaintIndex_YTD,
	@CC_UtilPcnt = UtilPcnt, @CC_UtilPcnt_YTD = UtilPcnt_YTD,
	@CC_NEOpExEDC = NEOpExEDC, @CC_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@CC_EII = EII, @CC_EII_YTD = EII_YTD, @CC_TotWHrEDC = TotWHrEDC, @CC_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('218NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @HO_KEDC = EDC/1000, @HO_KEDC_YTD = EDC_YTD/1000, 
	@HO_MechAvail = MechAvail, @HO_MechAvail_YTD = MechAvail_YTD,
	@HO_AdjMaintIndex = RoutIndex + TAIndex_Avg, @HO_MaintIndex_YTD = MaintIndex_YTD,
	@HO_UtilPcnt = UtilPcnt, @HO_UtilPcnt_YTD = UtilPcnt_YTD,
	@HO_NEOpExEDC = NEOpExEDC, @HO_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@HO_EII = EII, @HO_EII_YTD = EII_YTD, @HO_TotWHrEDC = TotWHrEDC, @HO_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('98NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @MK_KEDC = EDC/1000, @MK_KEDC_YTD = EDC_YTD/1000, 
	@MK_MechAvail = MechAvail, @MK_MechAvail_YTD = MechAvail_YTD,
	@MK_AdjMaintIndex = RoutIndex + TAIndex_Avg, @MK_MaintIndex_YTD = MaintIndex_YTD,
	@MK_UtilPcnt = UtilPcnt, @MK_UtilPcnt_YTD = UtilPcnt_YTD,
	@MK_NEOpExEDC = NEOpExEDC, @MK_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@MK_EII = EII, @MK_EII_YTD = EII_YTD, @MK_TotWHrEDC = TotWHrEDC, @MK_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('28NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @MP_KEDC = EDC/1000, @MP_KEDC_YTD = EDC_YTD/1000, 
	@MP_MechAvail = MechAvail, @MP_MechAvail_YTD = MechAvail_YTD,
	@MP_AdjMaintIndex = RoutIndex + TAIndex_Avg, @MP_MaintIndex_YTD = MaintIndex_YTD,
	@MP_UtilPcnt = UtilPcnt, @MP_UtilPcnt_YTD = UtilPcnt_YTD,
	@MP_NEOpExEDC = NEOpExEDC, @MP_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@MP_EII = EII, @MP_EII_YTD = EII_YTD, @MP_TotWHrEDC = TotWHrEDC, @MP_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('38NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @PA_KEDC = EDC/1000, @PA_KEDC_YTD = EDC_YTD/1000, 
	@PA_MechAvail = MechAvail, @PA_MechAvail_YTD = MechAvail_YTD,
	@PA_AdjMaintIndex = RoutIndex + TAIndex_Avg, @PA_MaintIndex_YTD = MaintIndex_YTD,
	@PA_UtilPcnt = UtilPcnt, @PA_UtilPcnt_YTD = UtilPcnt_YTD,
	@PA_NEOpExEDC = NEOpExEDC, @PA_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@PA_EII = EII, @PA_EII_YTD = EII_YTD, @PA_TotWHrEDC = TotWHrEDC, @PA_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('19NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @QB_KEDC = EDC/1000, @QB_KEDC_YTD = EDC_YTD/1000, 
	@QB_MechAvail = MechAvail, @QB_MechAvail_YTD = MechAvail_YTD,
	@QB_AdjMaintIndex = RoutIndex + TAIndex_Avg, @QB_MaintIndex_YTD = MaintIndex_YTD,
	@QB_UtilPcnt = UtilPcnt, @QB_UtilPcnt_YTD = UtilPcnt_YTD,
	@QB_NEOpExEDC = NEOpExEDC, @QB_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@QB_EII = EII, @QB_EII_YTD = EII_YTD, @QB_TotWHrEDC = TotWHrEDC, @QB_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('110NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @SC_KEDC = EDC/1000, @SC_KEDC_YTD = EDC_YTD/1000, 
	@SC_MechAvail = MechAvail, @SC_MechAvail_YTD = MechAvail_YTD,
	@SC_AdjMaintIndex = RoutIndex + TAIndex_Avg, @SC_MaintIndex_YTD = MaintIndex_YTD,
	@SC_UtilPcnt = UtilPcnt, @SC_UtilPcnt_YTD = UtilPcnt_YTD,
	@SC_NEOpExEDC = NEOpExEDC, @SC_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@SC_EII = EII, @SC_EII_YTD = EII_YTD, @SC_TotWHrEDC = TotWHrEDC, @SC_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('217NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @TC_KEDC = EDC/1000, @TC_KEDC_YTD = EDC_YTD/1000, 
	@TC_MechAvail = MechAvail, @TC_MechAvail_YTD = MechAvail_YTD,
	@TC_AdjMaintIndex = RoutIndex + TAIndex_Avg, @TC_MaintIndex_YTD = MaintIndex_YTD,
	@TC_UtilPcnt = UtilPcnt, @TC_UtilPcnt_YTD = UtilPcnt_YTD,
	@TC_NEOpExEDC = NEOpExEDC, @TC_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@TC_EII = EII, @TC_EII_YTD = EII_YTD, @TC_TotWHrEDC = TotWHrEDC, @TC_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('72NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @TR_KEDC = EDC/1000, @TR_KEDC_YTD = EDC_YTD/1000, 
	@TR_MechAvail = MechAvail, @TR_MechAvail_YTD = MechAvail_YTD,
	@TR_AdjMaintIndex = RoutIndex + TAIndex_Avg, @TR_MaintIndex_YTD = MaintIndex_YTD,
	@TR_UtilPcnt = UtilPcnt, @TR_UtilPcnt_YTD = UtilPcnt_YTD,
	@TR_NEOpExEDC = NEOpExEDC, @TR_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@TR_EII = EII, @TR_EII_YTD = EII_YTD, @TR_TotWHrEDC = TotWHrEDC, @TR_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('29NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @WM_KEDC = EDC/1000, @WM_KEDC_YTD = EDC_YTD/1000, 
	@WM_MechAvail = MechAvail, @WM_MechAvail_YTD = MechAvail_YTD,
	@WM_AdjMaintIndex = RoutIndex + TAIndex_Avg, @WM_MaintIndex_YTD = MaintIndex_YTD,
	@WM_UtilPcnt = UtilPcnt, @WM_UtilPcnt_YTD = UtilPcnt_YTD,
	@WM_NEOpExEDC = NEOpExEDC, @WM_NEOpExEDC_YTD = NEOpExEDC_YTD,
	@WM_EII = EII, @WM_EII_YTD = EII_YTD, @WM_TotWHrEDC = TotWHrEDC, @WM_TotWHrEDC_YTD = TotWHrEDC_YTD
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('91NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT 	@VALERO_KEDC = SUM(EDC)/1E6, @VALERO_KEDC_YTD = SUM(EDC_YTD)/1E6, 
	@VALERO_MechAvail = SUM(MechAvail*EDC)/SUM(CASE WHEN EDC > 0 THEN EDC END), 
	@VALERO_MechAvail_YTD = SUM(MechAvail_YTD*EDC_YTD)/SUM(CASE WHEN EDC_YTD > 0 THEN EDC_YTD END),
	@VALERO_AdjMaintIndex = SUM((RoutIndex + TAIndex_Avg)*EDC)/SUM(CASE WHEN EDC>0 THEN EDC END), 
	@VALERO_MaintIndex_YTD = SUM(MaintIndex_YTD*EDC_YTD)/SUM(CASE WHEN EDC > 0 THEN EDC_YTD END),
	@VALERO_UtilPcnt = SUM(UtilPcnt*EDC)/SUM(CASE WHEN EDC>0 THEN EDC END),
	@VALERO_UtilPcnt_YTD = SUM(UtilPcnt_YTD*EDC_YTD)/SUM(CASE WHEN EDC > 0 THEN EDC_YTD END),
	@VALERO_NEOpExEDC = SUM(NEOpExEDC*EDC)/SUM(CASE WHEN EDC>0 THEN EDC END),
	@VALERO_NEOpExEDC_YTD = SUM(NEOpExEDC_YTD*EDC_YTD)/SUM(CASE WHEN EDC > 0 THEN EDC_YTD END),
	@VALERO_TotWHrEDC = SUM(TotWHrEDC*EDC)/SUM(CASE WHEN EDC>0 THEN EDC END),
	@VALERO_TotWHrEDC_YTD = SUM(TotWHrEDC_YTD*EDC_YTD)/SUM(CASE WHEN EDC > 0 THEN EDC_YTD END)
FROM GenSum
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA') 
AND DataSet='Actual' AND DATEPART(yyyy, PeriodStart) = @PeriodYear AND DATEPART(mm, PeriodStart) = @PeriodMonth
AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario
AND EXISTS (SELECT * FROM Submissions s WHERE s.SubmissionID = GenSum.SubmissionID AND s.UseSubmission = 1)

SELECT @VALERO_EII = 100*SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays)
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA') 
AND s.DataSet = 'Actual' AND s.PeriodYear = @PeriodYear AND s.PeriodMonth = @PeriodMonth AND s.UseSubmission = 1
AND a.FactorSet = @FactorSet AND a.EnergyUseDay > 0 AND a.TotStdEnergy> 0 AND s.NumDays > 0

SELECT @VALERO_EII_YTD = 100*SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) 
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA') 
AND s.DataSet = 'Actual' AND s.PeriodYear = @PeriodYear AND s.PeriodMonth <= @PeriodMonth AND s.UseSubmission = 1
AND a.FactorSet = @FactorSet AND a.EnergyUseDay > 0 AND a.TotStdEnergy> 0 AND s.NumDays > 0

SELECT  AD_KEDC = @AD_KEDC , AD_KEDC_YTD = @AD_KEDC_YTD, AD_MechAvail = @AD_MechAvail, AD_MechAvail_YTD = @AD_MechAvail_YTD, 
	AD_AdjMaintIndex = @AD_AdjMaintIndex, AD_AdjMaintIndex_YTD = @AD_MaintIndex_YTD, 
	AD_UtilPcnt = @AD_UtilPcnt, AD_UtilPcnt_YTD = @AD_UtilPcnt_YTD, 
	AD_NEOpExEDC = @AD_NEOpExEDC, AD_NEOpExEDC_YTD = @AD_NEOpExEDC_YTD, 
	AD_EII = @AD_EII, AD_EII_YTD = @AD_EII_YTD, AD_TotWHrEDC = @AD_TotWHrEDC, AD_TotWHrEDC_YTD = @AD_TotWHrEDC_YTD,
	BN_KEDC = @BN_KEDC, BN_KEDC_YTD = @BN_KEDC_YTD, BN_MechAvail = @BN_MechAvail, BN_MechAvail_YTD = @BN_MechAvail_YTD, 
	BN_AdjMaintIndex = @BN_AdjMaintIndex, BN_AdjMaintIndex_YTD = @BN_MaintIndex_YTD, 
	BN_UtilPcnt = @BN_UtilPcnt, BN_UtilPcnt_YTD = @BN_UtilPcnt_YTD, 
	BN_NEOpExEDC = @BN_NEOpExEDC, BN_NEOpExEDC_YTD = @BN_NEOpExEDC_YTD, 
	BN_EII = @BN_EII, BN_EII_YTD = @BN_EII_YTD, BN_TotWHrEDC = @BN_TotWHrEDC, BN_TotWHrEDC_YTD = @BN_TotWHrEDC_YTD,
	CC_KEDC = @CC_KEDC, CC_KEDC_YTD = @CC_KEDC_YTD, CC_MechAvail = @CC_MechAvail, CC_MechAvail_YTD = @CC_MechAvail_YTD, 
	CC_AdjMaintIndex = @CC_AdjMaintIndex, CC_AdjMaintIndex_YTD = @CC_MaintIndex_YTD, 
	CC_UtilPcnt = @CC_UtilPcnt, CC_UtilPcnt_YTD = @CC_UtilPcnt_YTD, 
	CC_NEOpExEDC = @CC_NEOpExEDC, CC_NEOpExEDC_YTD = @CC_NEOpExEDC_YTD, 
	CC_EII = @CC_EII, CC_EII_YTD = @CC_EII_YTD, CC_TotWHrEDC = @CC_TotWHrEDC, CC_TotWHrEDC_YTD = @CC_TotWHrEDC_YTD,
	HO_KEDC = @HO_KEDC, HO_KEDC_YTD = @HO_KEDC_YTD, HO_MechAvail = @HO_MechAvail, HO_MechAvail_YTD = @HO_MechAvail_YTD, 
	HO_AdjMaintIndex = @HO_AdjMaintIndex, HO_AdjMaintIndex_YTD = @HO_MaintIndex_YTD, 
	HO_UtilPcnt = @HO_UtilPcnt, HO_UtilPcnt_YTD = @HO_UtilPcnt_YTD, 
	HO_NEOpExEDC = @HO_NEOpExEDC, HO_NEOpExEDC_YTD = @HO_NEOpExEDC_YTD, 
	HO_EII = @HO_EII, HO_EII_YTD = @HO_EII_YTD, HO_TotWHrEDC = @HO_TotWHrEDC, HO_TotWHrEDC_YTD = @HO_TotWHrEDC_YTD,
	MK_KEDC = @MK_KEDC, MK_KEDC_YTD = @MK_KEDC_YTD, MK_MechAvail = @MK_MechAvail, MK_MechAvail_YTD = @MK_MechAvail_YTD, 
	MK_AdjMaintIndex = @MK_AdjMaintIndex, MK_AdjMaintIndex_YTD = @MK_MaintIndex_YTD, 
	MK_UtilPcnt = @MK_UtilPcnt, MK_UtilPcnt_YTD = @MK_UtilPcnt_YTD, 
	MK_NEOpExEDC = @MK_NEOpExEDC, MK_NEOpExEDC_YTD = @MK_NEOpExEDC_YTD, 
	MK_EII = @MK_EII, MK_EII_YTD = @MK_EII_YTD, MK_TotWHrEDC = @MK_TotWHrEDC, MK_TotWHrEDC_YTD = @MK_TotWHrEDC_YTD,
	MP_KEDC = @MP_KEDC, MP_KEDC_YTD = @MP_KEDC_YTD, MP_MechAvail = @MP_MechAvail, MP_MechAvail_YTD = @MP_MechAvail_YTD, 
	MP_AdjMaintIndex = @MP_AdjMaintIndex, MP_AdjMaintIndex_YTD = @MP_MaintIndex_YTD, 
	MP_UtilPcnt = @MP_UtilPcnt, MP_UtilPcnt_YTD = @MP_UtilPcnt_YTD, 
	MP_NEOpExEDC = @MP_NEOpExEDC, MP_NEOpExEDC_YTD = @MP_NEOpExEDC_YTD, 
	MP_EII = @MP_EII, MP_EII_YTD = @MP_EII_YTD, MP_TotWHrEDC = @MP_TotWHrEDC, MP_TotWHrEDC_YTD = @MP_TotWHrEDC_YTD,
	PA_KEDC = @PA_KEDC, PA_KEDC_YTD = @PA_KEDC_YTD, PA_MechAvail = @PA_MechAvail, PA_MechAvail_YTD = @PA_MechAvail_YTD, 
	PA_AdjMaintIndex = @PA_AdjMaintIndex, PA_AdjMaintIndex_YTD = @PA_MaintIndex_YTD, 
	PA_UtilPcnt = @PA_UtilPcnt, PA_UtilPcnt_YTD = @PA_UtilPcnt_YTD, 
	PA_NEOpExEDC = @PA_NEOpExEDC, PA_NEOpExEDC_YTD = @PA_NEOpExEDC_YTD, 
	PA_EII = @PA_EII, PA_EII_YTD = @PA_EII_YTD, PA_TotWHrEDC = @PA_TotWHrEDC, PA_TotWHrEDC_YTD = @PA_TotWHrEDC_YTD,
	QB_KEDC = @QB_KEDC, QB_KEDC_YTD = @QB_KEDC_YTD, QB_MechAvail = @QB_MechAvail, QB_MechAvail_YTD = @QB_MechAvail_YTD, 
	QB_AdjMaintIndex = @QB_AdjMaintIndex, QB_AdjMaintIndex_YTD = @QB_MaintIndex_YTD, 
	QB_UtilPcnt = @QB_UtilPcnt, QB_UtilPcnt_YTD = @QB_UtilPcnt_YTD, 
	QB_NEOpExEDC = @QB_NEOpExEDC, QB_NEOpExEDC_YTD = @QB_NEOpExEDC_YTD, 
	QB_EII = @QB_EII, QB_EII_YTD = @QB_EII_YTD, QB_TotWHrEDC = @QB_TotWHrEDC, QB_TotWHrEDC_YTD = @QB_TotWHrEDC_YTD,
	SC_KEDC = @SC_KEDC, SC_KEDC_YTD = @SC_KEDC_YTD, SC_MechAvail = @SC_MechAvail, SC_MechAvail_YTD = @SC_MechAvail_YTD, 
	SC_AdjMaintIndex = @SC_AdjMaintIndex, SC_AdjMaintIndex_YTD = @SC_MaintIndex_YTD, 
	SC_UtilPcnt = @SC_UtilPcnt, SC_UtilPcnt_YTD = @SC_UtilPcnt_YTD, 
	SC_NEOpExEDC = @SC_NEOpExEDC, SC_NEOpExEDC_YTD = @SC_NEOpExEDC_YTD, 
	SC_EII = @SC_EII, SC_EII_YTD = @SC_EII_YTD, SC_TotWHrEDC = @SC_TotWHrEDC, SC_TotWHrEDC_YTD = @SC_TotWHrEDC_YTD,
	TC_KEDC = @TC_KEDC, TC_KEDC_YTD = @TC_KEDC_YTD, TC_MechAvail = @TC_MechAvail, TC_MechAvail_YTD = @TC_MechAvail_YTD, 
	TC_AdjMaintIndex = @TC_AdjMaintIndex, TC_AdjMaintIndex_YTD = @TC_MaintIndex_YTD, 
	TC_UtilPcnt = @TC_UtilPcnt, TC_UtilPcnt_YTD = @TC_UtilPcnt_YTD, 
	TC_NEOpExEDC = @TC_NEOpExEDC, TC_NEOpExEDC_YTD = @TC_NEOpExEDC_YTD, 
	TC_EII = @TC_EII, TC_EII_YTD = @TC_EII_YTD, TC_TotWHrEDC = @TC_TotWHrEDC, TC_TotWHrEDC_YTD = @TC_TotWHrEDC_YTD,
	TR_KEDC = @TR_KEDC, TR_KEDC_YTD = @TR_KEDC_YTD, TR_MechAvail = @TR_MechAvail, TR_MechAvail_YTD = @TR_MechAvail_YTD, 
	TR_AdjMaintIndex = @TR_AdjMaintIndex, TR_AdjMaintIndex_YTD = @TR_MaintIndex_YTD, 
	TR_UtilPcnt = @TR_UtilPcnt, TR_UtilPcnt_YTD = @TR_UtilPcnt_YTD, 
	TR_NEOpExEDC = @TR_NEOpExEDC, TR_NEOpExEDC_YTD = @TR_NEOpExEDC_YTD, 
	TR_EII = @TR_EII, TR_EII_YTD = @TR_EII_YTD, TR_TotWHrEDC = @TR_TotWHrEDC, TR_TotWHrEDC_YTD = @TR_TotWHrEDC_YTD,
	WM_KEDC = @WM_KEDC, WM_KEDC_YTD = @WM_KEDC_YTD, WM_MechAvail = @WM_MechAvail, WM_MechAvail_YTD = @WM_MechAvail_YTD, 
	WM_AdjMaintIndex = @WM_AdjMaintIndex, WM_AdjMaintIndex_YTD = @WM_MaintIndex_YTD, 
	WM_UtilPcnt = @WM_UtilPcnt, WM_UtilPcnt_YTD = @WM_UtilPcnt_YTD, 
	WM_NEOpExEDC = @WM_NEOpExEDC, WM_NEOpExEDC_YTD = @WM_NEOpExEDC_YTD, 
	WM_EII = @WM_EII, WM_EII_YTD = @WM_EII_YTD, WM_TotWHrEDC = @WM_TotWHrEDC, WM_TotWHrEDC_YTD = @WM_TotWHrEDC_YTD,
	VALERO_KEDC = @VALERO_KEDC, VALERO_KEDC_YTD = @VALERO_KEDC_YTD, VALERO_MechAvail = @VALERO_MechAvail, VALERO_MechAvail_YTD = @VALERO_MechAvail_YTD, 
	VALERO_AdjMaintIndex = @VALERO_AdjMaintIndex, VALERO_AdjMaintIndex_YTD = @VALERO_MaintIndex_YTD, 
	VALERO_UtilPcnt = @VALERO_UtilPcnt, VALERO_UtilPcnt_YTD = @VALERO_UtilPcnt_YTD, 
	VALERO_NEOpExEDC = @VALERO_NEOpExEDC, VALERO_NEOpExEDC_YTD = @VALERO_NEOpExEDC_YTD, 
	VALERO_EII = @VALERO_EII, VALERO_EII_YTD = @VALERO_EII_YTD, VALERO_TotWHrEDC = @VALERO_TotWHrEDC, VALERO_TotWHrEDC_YTD = @VALERO_TotWHrEDC_YTD

