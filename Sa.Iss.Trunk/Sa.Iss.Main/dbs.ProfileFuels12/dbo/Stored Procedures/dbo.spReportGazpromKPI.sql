﻿



CREATE   PROC [dbo].[spReportGazpromKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE	@RefUtilPcnt real, @RefUtilPcnt_QTR real, @RefUtilPcnt_Avg real, 
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_Avg real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_Avg real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_Avg real, 
	
	@OpAvail real, @OpAvail_QTR real, @OpAvail_Avg real, 
	@MechUnavailTA real, @MechUnavailTA_QTR real, @MechUnavailTA_Avg real, 
	@NonTAUnavail real, @NonTAUnavail_QTR real, @NonTAUnavail_Avg real, 

	@EII real, @EII_QTR real, @EII_Avg real, 
	@EnergyUseDay real, @EnergyUseDay_QTR real, @EnergyUseDay_Avg real, 
	@TotStdEnergy real, @TotStdEnergy_QTR real, @TotStdEnergy_Avg real, 

	@VEI real, @VEI_QTR real, @VEI_Avg real, 
	@ReportLossGain real, @ReportLossGain_QTR real, @ReportLossGain_Avg real, 
	@EstGain real, @EstGain_QTR real, @EstGain_Avg real, 

	@Gain real, @Gain_QTR real, @Gain_Avg real, 
	@RawMatl real, @RawMatl_QTR real, @RawMatl_Avg real, 
	@ProdYield real, @ProdYield_QTR real, @ProdYield_Avg real, 

	@PersIndex real, @PersIndex_QTR real, @PersIndex_Avg real, 
	@AnnTAWHr real, @AnnTAWHr_QTR real, @AnnTAWHr_Avg real,
	@NonTAWHr real, @NonTAWHr_QTR real, @NonTAWHr_Avg real,
	@EDC real, @EDC_QTR real, @EDC_Avg real, 
	@UEDC real, @UEDC_QTR real, @UEDC_Avg real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_Avg real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_Avg real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_Avg real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_Avg real, 
	@RoutIndex real, @RoutIndex_QTR real, @RoutIndex_Avg real,
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_Avg real, 
	@RoutCost real, @RoutCost_QTR real, @RoutCost_Avg real, 

	@NEOpExEDC real, @NEOpExEDC_QTR real, @NEOpExEDC_Avg real, 
	@NEOpEx real, @NEOpEx_QTR real, @NEOpEx_Avg real, 

	@OpExUEDC real, @OpExUEDC_QTR real, @OpExUEDC_Avg real, 
	@EnergyCost real, @EnergyCost_QTR real, @EnergyCost_Avg real, 
	@TAAdj real, @TAAdj_QTR real, @TAAdj_Avg real,
	@TotCashOpEx real, @TotCashOpEx_QTR real, @TotCashOpEx_Avg real 
DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc] @RefineryID, @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @EII OUTPUT, @EII_QTR = @EII_QTR OUTPUT, @EII_Avg = @EII_Avg OUTPUT, 
	@EnergyUseDay = @EnergyUseDay OUTPUT, @EnergyUseDay_QTR = @EnergyUseDay_QTR OUTPUT, @EnergyUseDay_Avg = @EnergyUseDay_Avg OUTPUT, 
	@TotStdEnergy = @TotStdEnergy OUTPUT, @TotStdEnergy_QTR = @TotStdEnergy_QTR OUTPUT, @TotStdEnergy_Avg = @TotStdEnergy_Avg OUTPUT, 
	@RefUtilPcnt = @RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @RefUtilPcnt_Avg OUTPUT, 
	@EDC = @EDC OUTPUT, @EDC_QTR = @EDC_QTR OUTPUT, @EDC_Avg = @EDC_Avg OUTPUT, 
	@UEDC = @UEDC OUTPUT, @UEDC_QTR = @UEDC_QTR OUTPUT, @UEDC_Avg = @UEDC_Avg OUTPUT, 
	@VEI = @VEI OUTPUT, @VEI_QTR = @VEI_QTR OUTPUT, @VEI_Avg = @VEI_Avg OUTPUT, 
	@ReportLossGain = @ReportLossGain OUTPUT, @ReportLossGain_QTR = @ReportLossGain_QTR OUTPUT, @ReportLossGain_Avg = @ReportLossGain_Avg OUTPUT, 
	@EstGain = @EstGain OUTPUT, @EstGain_QTR = @EstGain_QTR OUTPUT, @EstGain_Avg = @EstGain_Avg OUTPUT, 
	@OpAvail = @OpAvail OUTPUT, @OpAvail_QTR = @OpAvail_QTR OUTPUT, @OpAvail_Avg = @OpAvail_Avg OUTPUT, 
	@MechUnavailTA = @MechUnavailTA OUTPUT, @MechUnavailTA_QTR = @MechUnavailTA_QTR OUTPUT, @MechUnavailTA_Avg = @MechUnavailTA_Avg OUTPUT, 
	@NonTAUnavail = @NonTAUnavail OUTPUT, @NonTAUnavail_QTR = @NonTAUnavail_QTR OUTPUT, @NonTAUnavail_Avg = @NonTAUnavail_Avg OUTPUT, 
	@RoutIndex = @RoutIndex OUTPUT, @RoutIndex_QTR = @RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @RoutIndex_Avg OUTPUT,
	@RoutCost = @RoutCost OUTPUT, @RoutCost_QTR = @RoutCost_QTR OUTPUT, @RoutCost_Avg = @RoutCost_Avg OUTPUT, 
	@PersIndex = @PersIndex OUTPUT, @PersIndex_QTR = @PersIndex_QTR OUTPUT, @PersIndex_Avg = @PersIndex_Avg OUTPUT, 
	@AnnTAWHr = @AnnTAWHr OUTPUT, @AnnTAWHr_QTR = @AnnTAWHr_QTR OUTPUT, @AnnTAWHr_Avg = @AnnTAWHr_Avg OUTPUT,
	@NonTAWHr = @NonTAWHr OUTPUT, @NonTAWHr_QTR = @NonTAWHr_QTR OUTPUT, @NonTAWHr_Avg = @NonTAWHr_Avg OUTPUT,
	@NEOpExEDC = @NEOpExEDC OUTPUT, @NEOpExEDC_QTR = @NEOpExEDC_QTR OUTPUT, @NEOpExEDC_Avg = @NEOpExEDC_Avg OUTPUT, 
	@NEOpEx = @NEOpEx OUTPUT, @NEOpEx_QTR = @NEOpEx_QTR OUTPUT, @NEOpEx_Avg = @NEOpEx_Avg OUTPUT, 
	@OpExUEDC = @OpExUEDC OUTPUT, @OpExUEDC_QTR = @OpExUEDC_QTR OUTPUT, @OpExUEDC_Avg = @OpExUEDC_Avg OUTPUT, 
	@TAAdj = @TAAdj OUTPUT, @TAAdj_QTR = @TAAdj_QTR OUTPUT, @TAAdj_Avg = @TAAdj_Avg OUTPUT,
	@EnergyCost = @EnergyCost OUTPUT, @EnergyCost_QTR = @EnergyCost_QTR OUTPUT, @EnergyCost_Avg = @EnergyCost_Avg OUTPUT, 
	@TotCashOpEx = @TotCashOpEx OUTPUT, @TotCashOpEx_QTR = @TotCashOpEx_QTR OUTPUT, @TotCashOpEx_Avg = @TotCashOpEx_Avg OUTPUT

IF @spResult > 0	
	RETURN @spResult
ELSE 

SELECT 
	EII = @EII, EII_QTR = @EII_QTR, EII_Avg = @EII_Avg, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_QTR = @EnergyUseDay_QTR, EnergyUseDay_Avg = @EnergyUseDay_Avg, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_QTR = @TotStdEnergy_QTR, TotStdEnergy_Avg = @TotStdEnergy_Avg, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_QTR = @RefUtilPcnt_QTR, UtilPcnt_Avg = @RefUtilPcnt_Avg, 
	EDC = @EDC, EDC_QTR = @EDC_QTR, EDC_Avg = @EDC_Avg, 
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_QTR = @EDC_QTR*@RefUtilPcnt_QTR/100, UtilUEDC_Avg = @EDC_Avg*@RefUtilPcnt_Avg/100, 
	
--	ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, ProcessUtilPcnt_Avg = @ProcessUtilPcnt_Avg,
--	TotProcessEDC = @TotProcessEDC, TotProcessEDC_QTR = @TotProcessEDC_QTR, TotProcessEDC_Avg = @TotProcessEDC_Avg, 
--	TotProcessUEDC = @TotProcessUEDC, TotProcessUEDC_QTR = @TotProcessUEDC_QTR, TotProcessUEDC_Avg = @TotProcessUEDC_Avg, 
	
	VEI = @VEI, VEI_QTR = @VEI_QTR, VEI_Avg = @VEI_Avg, 
	ReportLossGain = @ReportLossGain, ReportLossGain_QTR = @ReportLossGain_QTR, ReportLossGain_Avg = @ReportLossGain_Avg, 
	EstGain = @EstGain, EstGain_QTR = @EstGain_QTR, EstGain_Avg = @EstGain_Avg, 

	OpAvail = @OpAvail, OpAvail_QTR = @OpAvail_QTR, OpAvail_Avg = @OpAvail_Avg, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_QTR = @MechUnavailTA_QTR, MechUnavailTA_Avg = @MechUnavailTA_Avg, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_QTR = @NonTAUnavail_QTR, NonTAUnavail_Avg = @NonTAUnavail_Avg, 

	MaintIndex = @MaintIndex, MaintIndex_QTR = @MaintIndex_QTR, MaintIndex_Avg = @MaintIndex_Avg, 
	AnnTACost = @AnnTACost, AnnTACost_QTR = @AnnTACost_QTR, AnnTACost_Avg = @AnnTACost_Avg, 
	RoutCost = @RoutCost, RoutCost_QTR = @RoutCost_QTR, RoutCost_Avg = @RoutCost_Avg, 
	RoutIndex = @RoutIndex, RoutIndex_QTR = @RoutIndex_QTR, RoutIndex_Avg = @RoutIndex_Avg, 

	TotWHrEDC = @PersIndex, TotWHrEDC_QTR = @PersIndex_QTR, TotWHrEDC_Avg = @PersIndex_Avg, 
	AnnTAWHr = @AnnTAWHr, AnnTAWHr_QTR = @AnnTAWHr_QTR, AnnTAWHr_Avg = @AnnTAWHr_Avg,
	NonTAWHr = @NonTAWHr, NonTAWHr_QTR = @NonTAWHr_QTR, NonTAWHr_Avg = @NonTAWHr_Avg, 

	NEOpExEDC = @NEOpExEDC, NEOpExEDC_QTR = @NEOpExEDC_QTR, NEOpExEDC_Avg = @NEOpExEDC_Avg, 
	NEOpEx = @NEOpEx, NEOpEx_QTR = @NEOpEx_QTR, NEOpEx_Avg = @NEOpEx_Avg, 

	TotCashOpExUEDC = @OpExUEDC, TotCashOpExUEDC_QTR = @OpExUEDC_QTR, TotCashOpExUEDC_Avg = @OpExUEDC_Avg, 
	TAAdj = @TAAdj, TAAdj_QTR = @TAAdj_QTR, TAAdj_Avg = @TAAdj_Avg,
	EnergyCost = @EnergyCost, EnergyCost_QTR = @EnergyCost_QTR, EnergyCost_Avg = @EnergyCost_Avg, 
	TotCashOpEx = @TotCashOpEx, TotCashOpEx_QTR = @TotCashOpEx_QTR, TotCashOpEx_Avg = @TotCashOpEx_Avg, 
	UEDC = @UEDC, UEDC_QTR = @UEDC_QTR, UEDC_Avg = @UEDC_Avg


