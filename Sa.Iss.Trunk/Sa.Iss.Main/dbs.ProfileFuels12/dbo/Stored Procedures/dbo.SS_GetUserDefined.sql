﻿CREATE PROC [dbo].[SS_GetUserDefined]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@DataSet nvarchar(20)='ACTUAL'
	
AS

SELECT u.HeaderText,u.VariableDesc,CAST(u.RptValue AS real) AS RptValue,CAST(u.RptValue_Target AS real) AS RptValue_Target,CAST(u.RptValue_Avg AS real) AS RptValue_Avg,CAST(u.RptValue_YTD AS real) AS RptValue_YTD,CAST( u.DecPlaces AS int) AS DecPlaces
             FROM dbo.UserDefined u  WHERE  
             u.SubmissionID IN 
             (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd)))

