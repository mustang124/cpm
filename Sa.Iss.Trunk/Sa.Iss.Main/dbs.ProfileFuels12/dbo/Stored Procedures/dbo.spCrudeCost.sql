﻿CREATE  PROCEDURE [dbo].[spCrudeCost](@SubmissionID int)
AS
DECLARE @RC int
DECLARE @StudyYear smallint
DECLARE @CrudeID int, @CNum varchar(5), @CrudeName varchar(50), @CrudeAPI real, @CrudeSulfur real, @CrudeDest smallint
DECLARE @BasePrice real, @TransCost real, @GravityAdj real, @SulfurAdj real, @PricePerBbl real, @CrudeOrigin smallint
DELETE FROM CrudeCalc WHERE SubmissionID = @SubmissionID
DELETE FROM CrudeTotPrice WHERE SubmissionID = @SubmissionID
DELETE FROM DataChecks WHERE SubmissionID = @SubmissionID AND DataCheckID = 'CrudeTrans'
DECLARE cCrude CURSOR LOCAL FAST_FORWARD
FOR	SELECT CrudeID, CNum, CrudeName, Gravity, Sulfur FROM Crude
	WHERE SubmissionID = @SubmissionID AND Bbl > 0 AND CNum <> 'C0'
DECLARE cYears CURSOR SCROLL
FOR	SELECT DISTINCT StudyYear FROM CrudePrice
OPEN cYears
OPEN cCrude
FETCH NEXT FROM cCrude INTO @CrudeID, @CNum, @CrudeName, @CrudeAPI, @CrudeSulfur
WHILE @@FETCH_STATUS = 0
BEGIN
	FETCH FIRST FROM cYears INTO @StudyYear
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @CrudeDest = NULL
		SELECT @CrudeDest = CrudeDest
		FROM RefineryFactors f INNER JOIN SubmissionsAll s ON s.RefineryID = f.RefineryID AND DATEPART(yy, f.EffDate) <= @StudyYear AND DATEPART(yy, f.EffUntil) >= @StudyYear
		WHERE SubmissionID = @SubmissionID
		EXEC @RC = dbo.CrudeCost @StudyYear, @CNum, @CrudeAPI, @CrudeSulfur, @CrudeDest, @BasePrice OUTPUT , @TransCost OUTPUT , @GravityAdj OUTPUT , @SulfurAdj OUTPUT , @PricePerBbl OUTPUT , @CrudeOrigin OUTPUT , DEFAULT
		IF @TransCost = 0 AND @CrudeOrigin IS NOT NULL
		BEGIN
			IF NOT EXISTS (SELECT * FROM Transportation WHERE StudyYear = @StudyYear AND CrudeOrigin = @CrudeOrigin AND CrudeDest = @CrudeDest)
			BEGIN
				INSERT DataChecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1)
				VALUES (@SubmissionID, 'CrudeTrans', @CrudeID, @CrudeName, @CNum)
			END
		END
		INSERT dbo.CrudeCalc (SubmissionID, Scenario, CrudeID, CNum, BasePrice, TransCost, GravityAdj, SulfurAdj, PricePerBbl)
		VALUES (@SubmissionID, @StudyYear, @CrudeID, @CNum, @BasePrice, @TransCost, @GravityAdj, @SulfurAdj, @PricePerBbl)
		FETCH NEXT FROM cYears INTO @StudyYear
	END
	FETCH NEXT FROM cCrude INTO @CrudeID, @CNum, @CrudeName, @CrudeAPI, @CrudeSulfur
END
CLOSE cYears
DEALLOCATE cYears
CLOSE cCrude
DEALLOCATE cCrude
IF EXISTS (SELECT * FROM Crude WHERE SubmissionID = @SubmissionID AND CNum = 'C0' AND Bbl > 0)
BEGIN
	INSERT dbo.CrudeCalc (SubmissionID, Scenario, CrudeID, CNum, BasePrice, TransCost, GravityAdj, SulfurAdj, PricePerBbl)
	SELECT c.SubmissionID, cc.Scenario, c.CrudeID, c.CNum, SUM(cc.BasePrice*c2.Bbl)/SUM(c2.Bbl), SUM(cc.TransCost*c2.Bbl)/SUM(c2.Bbl), 0, 0, SUM((ISNULL(cc.BasePrice, 0) + ISNULL(cc.TransCost, 0))*c2.Bbl)/SUM(c2.Bbl)
	FROM Crude c INNER JOIN dbo.CrudeCalc cc ON cc.SubmissionID = c.SubmissionID
	INNER JOIN Crude c2 ON c2.SubmissionID = cc.SubmissionID AND c2.CrudeID = cc.CrudeID
	WHERE c.SubmissionID = @SubmissionID AND c.Bbl > 0 AND c.CNum = 'C0'
	GROUP BY c.SubmissionID, cc.Scenario, c.CrudeID, c.CNum
END
INSERT dbo.CrudeCalc(SubmissionID, Scenario, CrudeID, CNum, PricePerBbl)
SELECT SubmissionID, 'CLIENT', CrudeID, CNum, CostPerBbl
FROM Crude WHERE SubmissionID = @SubmissionID AND Bbl > 0
INSERT dbo.CrudeTotPrice (SubmissionID, Scenario, AvgCost)
SELECT cc.SubmissionID, cc.Scenario, SUM(cc.PricePerBbl*c.Bbl)/SUM(c.Bbl)
FROM CrudeCalc cc INNER JOIN Crude c ON c.SubmissionID = cc.SubmissionID AND cc.CrudeID = c.CrudeID
WHERE cc.SubmissionID = @SubmissionID AND c.Bbl > 0
GROUP BY cc.SubmissionID, cc.Scenario
DELETE FROM DataChecks WHERE SubmissionID = @SubmissionID AND DataCheckID = 'CrudeTrans'
