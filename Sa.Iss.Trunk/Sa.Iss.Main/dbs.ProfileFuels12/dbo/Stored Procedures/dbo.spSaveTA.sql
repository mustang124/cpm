﻿
CREATE PROC [dbo].[spSaveTA](@RefineryID varchar(6), @DataSet varchar(15), @UnitID int, @ProcessID varchar(8), @TAID int
	, @TADate smalldatetime, @TAHrsDown real, @TACostLocal real, @TAExpLocal real, @TACptlLocal real, @TAOvhdLocal real, @TALaborCostLocal real
	, @TAOCCSTH real, @TAOCCOVT real, @TAMPSSTH real, @TAMPSOVTPcnt real
	, @TAContOCC real, @TAContMPS real, @PrevTADate smalldatetime, @TAExceptions smallint, @TACurrency CurrencyCode)
AS
BEGIN
	SET NOCOUNT ON
	IF @TAID IS NULL
	BEGIN
		SELECT @TAID = MAX(TAID) FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID
		IF @TAID IS NULL
			SET @TAID = 1
		ELSE
			SET @TAID = @TAID + 1
	END
	IF @PrevTADate IS NULL
		SELECT @PrevTADate = MAX(TADate) FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TADate < @TADate

	DECLARE @RecalcFrom smalldatetime
	IF EXISTS (SELECT * FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID)
	BEGIN
		SELECT @RecalcFrom = TADate FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
		IF @RecalcFrom > @TADate
			SET @RecalcFrom = @TADate
			
		UPDATE MaintTA 
		SET ProcessID = @ProcessID, TADate = @TADate, TAHrsDown = @TAHrsDown, TACostLocal = @TACostLocal, TAExpLocal = @TAExpLocal, TACptlLocal = @TACptlLocal, TAOvhdLocal = @TAOvhdLocal, @TALaborCostLocal = @TALaborCostLocal
			, TAOCCSTH = @TAOCCSTH, TAOCCOVT = @TAOCCOVT, TAMPSSTH = @TAMPSSTH, TAMPSOVTPcnt = @TAMPSOVTPcnt
			, TAContOCC = @TAContOCC, TAContMPS = @TAContMPS, PrevTADate = @PrevTADate, TAExceptions = @TAExceptions, TACurrency = @TACurrency
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
	END
	ELSE BEGIN
		INSERT MaintTA (RefineryID, DataSet, UnitID, ProcessID, TAID, TADate, TAHrsDown, TACostLocal, TAExpLocal, TACptlLocal,TAOvhdLocal,TALaborCostLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContMPS, PrevTADate, TAExceptions)
		VALUES (@RefineryID, @DataSet, @UnitID, @ProcessID, @TAID, @TADate, @TAHrsDown, @TACostLocal, @TAExpLocal,@TACptlLocal,@TAOvhdLocal,@TALaborCostLocal, @TAOCCSTH, @TAOCCOVT, @TAMPSSTH, @TAMPSOVTPcnt, @TAContOCC, @TAContMPS, @PrevTADate, @TAExceptions)

		SET @RecalcFrom = @TADate
	END

	DELETE FROM MaintTACost WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID

	EXEC spTACalcs @RefineryID, @DataSet, @UnitID, @TAID
	-- Update the Next T/A date in MaintTA and MaintTACost
	EXEC spUpdateNextTADate @RefineryID, @DataSet

	-- Tag the submissions to make updates for turnaround changes
	UPDATE Submissions
	SET CalcsNeeded = 'T'
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet
	AND PeriodEnd >= @RecalcFrom AND CalcsNeeded IS NULL
END		

