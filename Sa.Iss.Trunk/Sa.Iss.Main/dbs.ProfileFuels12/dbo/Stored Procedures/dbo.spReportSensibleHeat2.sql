﻿CREATE     PROC [dbo].[spReportSensibleHeat2] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
DECLARE @TotBbl float, @BPD real
DECLARE @SubID int
IF @PeriodYear IS NULL OR @PeriodMonth IS NULL
	EXEC dbo.spReportSensibleHeat @RefineryID = @RefineryID, @PeriodYear = @PeriodYear, @PeriodMonth = @PeriodMonth, @FactorSet = @FactorSet, @TotBbl = @TotBbl OUTPUT , @BPD = @BPD OUTPUT
ELSE BEGIN
	SELECT @SubID = SubmissionID FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
	EXEC dbo.spReportSensibleHeat @SubmissionID = @SubID, @FactorSet = @FactorSet, @TotBbl = @TotBbl OUTPUT , @BPD = @BPD OUTPUT 
END

