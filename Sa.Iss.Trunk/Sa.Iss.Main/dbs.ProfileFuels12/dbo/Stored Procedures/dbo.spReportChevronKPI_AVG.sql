﻿



CREATE    PROC [dbo].[spReportChevronKPI_Avg] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
IF @RefineryID = '179EUR'
	RETURN 1
	
DECLARE 
	@BU_UtilPcnt_Avg real, @BU_KEDC_Avg real, @BU_KUEDC_Avg real, 
	@BU_MechAvail_Avg real, @BU_KMAEDC_Avg real, @BU_KProcEDC_Avg real,
	@BU_EII_Avg real, @BU_KEnergyUseDay_Avg real, @BU_KTotStdEnergy_Avg real,
	@BU_MaintIndex_Avg real, @BU_TAIndex_Avg real, @BU_RoutIndex_Avg real,   
	@BU_TotEqPEDC_Avg real, 
	@BU_NEOpExUEDC_Avg real, @BU_TotCashOpExUEDC_Avg real,
	@BU_CashMargin_Avg real, @BU_NetInputBPD_Avg real
DECLARE 
	@CT_UtilPcnt_Avg real, @CT_KEDC_Avg real, @CT_KUEDC_Avg real, 
	@CT_MechAvail_Avg real, @CT_KMAEDC_Avg real, @CT_KProcEDC_Avg real,
	@CT_EII_Avg real, @CT_KEnergyUseDay_Avg real, @CT_KTotStdEnergy_Avg real,
	@CT_MaintIndex_Avg real, @CT_TAIndex_Avg real, @CT_RoutIndex_Avg real,   
	@CT_TotEqPEDC_Avg real, 
	@CT_NEOpExUEDC_Avg real, @CT_TotCashOpExUEDC_Avg real,
	@CT_CashMargin_Avg real, @CT_NetInputBPD_Avg real
DECLARE 
	@ES_UtilPcnt_Avg real, @ES_KEDC_Avg real, @ES_KUEDC_Avg real, 
	@ES_MechAvail_Avg real, @ES_KMAEDC_Avg real, @ES_KProcEDC_Avg real,
	@ES_EII_Avg real, @ES_KEnergyUseDay_Avg real, @ES_KTotStdEnergy_Avg real,
	@ES_MaintIndex_Avg real, @ES_TAIndex_Avg real, @ES_RoutIndex_Avg real,   
	@ES_TotEqPEDC_Avg real, 
	@ES_NEOpExUEDC_Avg real, @ES_TotCashOpExUEDC_Avg real,
	@ES_CashMargin_Avg real, @ES_NetInputBPD_Avg real
DECLARE 
	@HI_UtilPcnt_Avg real, @HI_KEDC_Avg real, @HI_KUEDC_Avg real, 
	@HI_MechAvail_Avg real, @HI_KMAEDC_Avg real, @HI_KProcEDC_Avg real,
	@HI_EII_Avg real, @HI_KEnergyUseDay_Avg real, @HI_KTotStdEnergy_Avg real,
	@HI_MaintIndex_Avg real, @HI_TAIndex_Avg real, @HI_RoutIndex_Avg real,   
	@HI_TotEqPEDC_Avg real, 
	@HI_NEOpExUEDC_Avg real, @HI_TotCashOpExUEDC_Avg real,
	@HI_CashMargin_Avg real, @HI_NetInputBPD_Avg real
DECLARE 
	@PA_UtilPcnt_Avg real, @PA_KEDC_Avg real, @PA_KUEDC_Avg real, 
	@PA_MechAvail_Avg real, @PA_KMAEDC_Avg real, @PA_KProcEDC_Avg real,
	@PA_EII_Avg real, @PA_KEnergyUseDay_Avg real, @PA_KTotStdEnergy_Avg real,
	@PA_MaintIndex_Avg real, @PA_TAIndex_Avg real, @PA_RoutIndex_Avg real,   
	@PA_TotEqPEDC_Avg real, 
	@PA_NEOpExUEDC_Avg real, @PA_TotCashOpExUEDC_Avg real,
	@PA_CashMargin_Avg real, @PA_NetInputBPD_Avg real
DECLARE 
	@RI_UtilPcnt_Avg real, @RI_KEDC_Avg real, @RI_KUEDC_Avg real, 
	@RI_MechAvail_Avg real, @RI_KMAEDC_Avg real, @RI_KProcEDC_Avg real,
	@RI_EII_Avg real, @RI_KEnergyUseDay_Avg real, @RI_KTotStdEnergy_Avg real,
	@RI_MaintIndex_Avg real, @RI_TAIndex_Avg real, @RI_RoutIndex_Avg real,   
	@RI_TotEqPEDC_Avg real, 
	@RI_NEOpExUEDC_Avg real, @RI_TotCashOpExUEDC_Avg real,
	@RI_CashMargin_Avg real, @RI_NetInputBPD_Avg real
DECLARE 
	@SL_UtilPcnt_Avg real, @SL_KEDC_Avg real, @SL_KUEDC_Avg real, 
	@SL_MechAvail_Avg real, @SL_KMAEDC_Avg real, @SL_KProcEDC_Avg real,
	@SL_EII_Avg real, @SL_KEnergyUseDay_Avg real, @SL_KTotStdEnergy_Avg real,
	@SL_MaintIndex_Avg real, @SL_TAIndex_Avg real, @SL_RoutIndex_Avg real,   
	@SL_TotEqPEDC_Avg real, 
	@SL_NEOpExUEDC_Avg real, @SL_TotCashOpExUEDC_Avg real,
	@SL_CashMargin_Avg real, @SL_NetInputBPD_Avg real

DECLARE 
	@CHEVRON_UtilPcnt_Avg real, @CHEVRON_KEDC_Avg real, @CHEVRON_KUEDC_Avg real, 
	@CHEVRON_MechAvail_Avg real, @CHEVRON_KMAEDC_Avg real, @CHEVRON_KProcEDC_Avg real,
	@CHEVRON_EII_Avg real, @CHEVRON_KEnergyUseDay_Avg real, @CHEVRON_KTotStdEnergy_Avg real,
	@CHEVRON_MaintIndex_Avg real, @CHEVRON_TAIndex_Avg real, @CHEVRON_RoutIndex_Avg real,   
	@CHEVRON_TotEqPEDC_Avg real, 
	@CHEVRON_NEOpExUEDC_Avg real, @CHEVRON_TotCashOpExUEDC_Avg real,
	@CHEVRON_CashMargin_Avg real, @CHEVRON_NetInputBPD_Avg real

EXEC [dbo].[spReportChevronKPI_Avg_REF] '162NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@BU_UtilPcnt_Avg OUTPUT, @KEDC_Avg=@BU_KEDC_Avg OUTPUT, @KUEDC_Avg=@BU_KUEDC_Avg OUTPUT, 
	@MechAvail_Avg=@BU_MechAvail_Avg OUTPUT, @KMAEDC_Avg=@BU_KMAEDC_Avg OUTPUT, @KProcEDC_Avg=@BU_KProcEDC_Avg OUTPUT,
	@EII_Avg=@BU_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@BU_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@BU_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@BU_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@BU_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@BU_RoutIndex_Avg OUTPUT,   
	@TotEqPEDC_Avg=@BU_TotEqPEDC_Avg OUTPUT, 
	@NEOpExUEDC_Avg=@BU_NEOpExUEDC_Avg OUTPUT, @TotCashOpExUEDC_Avg=@BU_TotCashOpExUEDC_Avg OUTPUT,
	@CashMargin_Avg=@BU_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@BU_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '53PAC', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@CT_UtilPcnt_Avg OUTPUT, @KEDC_Avg=@CT_KEDC_Avg OUTPUT, @KUEDC_Avg=@CT_KUEDC_Avg OUTPUT, 
	@MechAvail_Avg=@CT_MechAvail_Avg OUTPUT, @KMAEDC_Avg=@CT_KMAEDC_Avg OUTPUT, @KProcEDC_Avg=@CT_KProcEDC_Avg OUTPUT,
	@EII_Avg=@CT_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@CT_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@CT_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@CT_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@CT_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@CT_RoutIndex_Avg OUTPUT,   
	@TotEqPEDC_Avg=@CT_TotEqPEDC_Avg OUTPUT, 
	@NEOpExUEDC_Avg=@CT_NEOpExUEDC_Avg OUTPUT, @TotCashOpExUEDC_Avg=@CT_TotCashOpExUEDC_Avg OUTPUT,
	@CashMargin_Avg=@CT_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@CT_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '15NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@ES_UtilPcnt_Avg OUTPUT, @KEDC_Avg=@ES_KEDC_Avg OUTPUT, @KUEDC_Avg=@ES_KUEDC_Avg OUTPUT, 
	@MechAvail_Avg=@ES_MechAvail_Avg OUTPUT, @KMAEDC_Avg=@ES_KMAEDC_Avg OUTPUT, @KProcEDC_Avg=@ES_KProcEDC_Avg OUTPUT,
	@EII_Avg=@ES_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@ES_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@ES_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@ES_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@ES_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@ES_RoutIndex_Avg OUTPUT,   
	@TotEqPEDC_Avg=@ES_TotEqPEDC_Avg OUTPUT, 
	@NEOpExUEDC_Avg=@ES_NEOpExUEDC_Avg OUTPUT, @TotCashOpExUEDC_Avg=@ES_TotCashOpExUEDC_Avg OUTPUT,
	@CashMargin_Avg=@ES_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@ES_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '16NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@HI_UtilPcnt_Avg OUTPUT, @KEDC_Avg=@HI_KEDC_Avg OUTPUT, @KUEDC_Avg=@HI_KUEDC_Avg OUTPUT, 
	@MechAvail_Avg=@HI_MechAvail_Avg OUTPUT, @KMAEDC_Avg=@HI_KMAEDC_Avg OUTPUT, @KProcEDC_Avg=@HI_KProcEDC_Avg OUTPUT,
	@EII_Avg=@HI_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@HI_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@HI_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@HI_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@HI_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@HI_RoutIndex_Avg OUTPUT,   
	@TotEqPEDC_Avg=@HI_TotEqPEDC_Avg OUTPUT, 
	@NEOpExUEDC_Avg=@HI_NEOpExUEDC_Avg OUTPUT, @TotCashOpExUEDC_Avg=@HI_TotCashOpExUEDC_Avg OUTPUT,
	@CashMargin_Avg=@HI_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@HI_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '17NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@PA_UtilPcnt_Avg OUTPUT, @KEDC_Avg=@PA_KEDC_Avg OUTPUT, @KUEDC_Avg=@PA_KUEDC_Avg OUTPUT, 
	@MechAvail_Avg=@PA_MechAvail_Avg OUTPUT, @KMAEDC_Avg=@PA_KMAEDC_Avg OUTPUT, @KProcEDC_Avg=@PA_KProcEDC_Avg OUTPUT,
	@EII_Avg=@PA_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@PA_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@PA_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@PA_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@PA_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@PA_RoutIndex_Avg OUTPUT,   
	@TotEqPEDC_Avg=@PA_TotEqPEDC_Avg OUTPUT, 
	@NEOpExUEDC_Avg=@PA_NEOpExUEDC_Avg OUTPUT, @TotCashOpExUEDC_Avg=@PA_TotCashOpExUEDC_Avg OUTPUT,
	@CashMargin_Avg=@PA_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@PA_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '79FL', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@RI_UtilPcnt_Avg OUTPUT, @KEDC_Avg=@RI_KEDC_Avg OUTPUT, @KUEDC_Avg=@RI_KUEDC_Avg OUTPUT, 
	@MechAvail_Avg=@RI_MechAvail_Avg OUTPUT, @KMAEDC_Avg=@RI_KMAEDC_Avg OUTPUT, @KProcEDC_Avg=@RI_KProcEDC_Avg OUTPUT,
	@EII_Avg=@RI_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@RI_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@RI_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@RI_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@RI_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@RI_RoutIndex_Avg OUTPUT,   
	@TotEqPEDC_Avg=@RI_TotEqPEDC_Avg OUTPUT, 
	@NEOpExUEDC_Avg=@RI_NEOpExUEDC_Avg OUTPUT, @TotCashOpExUEDC_Avg=@RI_TotCashOpExUEDC_Avg OUTPUT,
	@CashMargin_Avg=@RI_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@RI_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '21NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@SL_UtilPcnt_Avg OUTPUT, @KEDC_Avg=@SL_KEDC_Avg OUTPUT, @KUEDC_Avg=@SL_KUEDC_Avg OUTPUT, 
	@MechAvail_Avg=@SL_MechAvail_Avg OUTPUT, @KMAEDC_Avg=@SL_KMAEDC_Avg OUTPUT, @KProcEDC_Avg=@SL_KProcEDC_Avg OUTPUT,
	@EII_Avg=@SL_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@SL_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@SL_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@SL_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@SL_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@SL_RoutIndex_Avg OUTPUT,   
	@TotEqPEDC_Avg=@SL_TotEqPEDC_Avg OUTPUT, 
	@NEOpExUEDC_Avg=@SL_NEOpExUEDC_Avg OUTPUT, @TotCashOpExUEDC_Avg=@SL_TotCashOpExUEDC_Avg OUTPUT,
	@CashMargin_Avg=@SL_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@SL_NetInputBPD_Avg OUTPUT


SELECT	@CHEVRON_KEDC_Avg = ISNULL(@BU_KEDC_Avg,0) + ISNULL(@CT_KEDC_Avg,0) + ISNULL(@ES_KEDC_Avg,0) + ISNULL(@HI_KEDC_Avg,0) 
						  + ISNULL(@PA_KEDC_Avg,0) + ISNULL(@RI_KEDC_Avg,0) + ISNULL(@SL_KEDC_Avg,0),
		@CHEVRON_KUEDC_Avg = ISNULL(@BU_KUEDC_Avg,0) + ISNULL(@CT_KUEDC_Avg,0) + ISNULL(@ES_KUEDC_Avg,0) + ISNULL(@HI_KUEDC_Avg,0) 
						   + ISNULL(@PA_KUEDC_Avg,0) + ISNULL(@RI_KUEDC_Avg,0) + ISNULL(@SL_KUEDC_Avg,0),
		@CHEVRON_KMAEDC_Avg = ISNULL(@BU_KMAEDC_Avg,0) + ISNULL(@CT_KMAEDC_Avg,0) + ISNULL(@ES_KMAEDC_Avg,0) + ISNULL(@HI_KMAEDC_Avg,0) 
							+ ISNULL(@PA_KMAEDC_Avg,0) + ISNULL(@RI_KMAEDC_Avg,0) + ISNULL(@SL_KMAEDC_Avg,0),
		@CHEVRON_KProcEDC_Avg = ISNULL(@BU_KProcEDC_Avg,0) + ISNULL(@CT_KProcEDC_Avg,0) + ISNULL(@ES_KProcEDC_Avg,0) + ISNULL(@HI_KProcEDC_Avg,0) 
							  + ISNULL(@PA_KProcEDC_Avg,0) + ISNULL(@RI_KProcEDC_Avg,0) + ISNULL(@SL_KProcEDC_Avg,0),
		@CHEVRON_NetInputBPD_Avg = ISNULL(@BU_NetInputBPD_Avg,0) + ISNULL(@CT_NetInputBPD_Avg,0) + ISNULL(@ES_NetInputBPD_Avg,0) + ISNULL(@HI_NetInputBPD_Avg,0) 
								 + ISNULL(@PA_NetInputBPD_Avg,0) + ISNULL(@RI_NetInputBPD_Avg,0) + ISNULL(@SL_NetInputBPD_Avg,0),
		@CHEVRON_KEnergyUseDay_Avg = ISNULL(@BU_KEnergyUseDay_Avg,0) + ISNULL(@CT_KEnergyUseDay_Avg,0) + ISNULL(@ES_KEnergyUseDay_Avg,0) + ISNULL(@HI_KEnergyUseDay_Avg,0) 
								   + ISNULL(@PA_KEnergyUseDay_Avg,0) + ISNULL(@RI_KEnergyUseDay_Avg,0) + ISNULL(@SL_KEnergyUseDay_Avg,0),
		@CHEVRON_KTotStdEnergy_Avg = ISNULL(@BU_KTotStdEnergy_Avg,0) + ISNULL(@CT_KTotStdEnergy_Avg,0) + ISNULL(@ES_KTotStdEnergy_Avg,0) + ISNULL(@HI_KTotStdEnergy_Avg,0) 
								   + ISNULL(@PA_KTotStdEnergy_Avg,0) + ISNULL(@RI_KTotStdEnergy_Avg,0) + ISNULL(@SL_KTotStdEnergy_Avg,0)

IF @CHEVRON_KEDC_Avg > 0
	SELECT	@CHEVRON_UtilPcnt_Avg = 100.0 * @CHEVRON_KUEDC_Avg / @CHEVRON_KEDC_Avg,
			@CHEVRON_MaintIndex_Avg = (ISNULL(@BU_KEDC_Avg,0)*ISNULL(@BU_MaintIndex_Avg,0) + ISNULL(@CT_KEDC_Avg,0)*ISNULL(@CT_MaintIndex_Avg,0) + ISNULL(@ES_KEDC_Avg,0)*ISNULL(@ES_MaintIndex_Avg,0) + ISNULL(@HI_KEDC_Avg,0)*ISNULL(@HI_MaintIndex_Avg,0) 
			   						 + ISNULL(@PA_KEDC_Avg,0)*ISNULL(@PA_MaintIndex_Avg,0) + ISNULL(@RI_KEDC_Avg,0)*ISNULL(@RI_MaintIndex_Avg,0) + ISNULL(@SL_KEDC_Avg,0)*ISNULL(@SL_MaintIndex_Avg,0))/@CHEVRON_KEDC_Avg,
			@CHEVRON_TAIndex_Avg = (ISNULL(@BU_KEDC_Avg,0)*ISNULL(@BU_TAIndex_Avg,0) + ISNULL(@CT_KEDC_Avg,0)*ISNULL(@CT_TAIndex_Avg,0) + ISNULL(@ES_KEDC_Avg,0)*ISNULL(@ES_TAIndex_Avg,0) + ISNULL(@HI_KEDC_Avg,0)*ISNULL(@HI_TAIndex_Avg,0) 
								      + ISNULL(@PA_KEDC_Avg,0)*ISNULL(@PA_TAIndex_Avg,0) + ISNULL(@RI_KEDC_Avg,0)*ISNULL(@RI_TAIndex_Avg,0) + ISNULL(@SL_KEDC_Avg,0)*ISNULL(@SL_TAIndex_Avg,0))/@CHEVRON_KEDC_Avg,
			@CHEVRON_RoutIndex_Avg = (ISNULL(@BU_KEDC_Avg,0)*ISNULL(@BU_RoutIndex_Avg,0) + ISNULL(@CT_KEDC_Avg,0)*ISNULL(@CT_RoutIndex_Avg,0) + ISNULL(@ES_KEDC_Avg,0)*ISNULL(@ES_RoutIndex_Avg,0) + ISNULL(@HI_KEDC_Avg,0)*ISNULL(@HI_RoutIndex_Avg,0) 
								    + ISNULL(@PA_KEDC_Avg,0)*ISNULL(@PA_RoutIndex_Avg,0) + ISNULL(@RI_KEDC_Avg,0)*ISNULL(@RI_RoutIndex_Avg,0) + ISNULL(@SL_KEDC_Avg,0)*ISNULL(@SL_RoutIndex_Avg,0))/@CHEVRON_KEDC_Avg,
			@CHEVRON_TotEqPEDC_Avg = (ISNULL(@BU_KEDC_Avg,0)*ISNULL(@BU_TotEqPEDC_Avg,0) + ISNULL(@CT_KEDC_Avg,0)*ISNULL(@CT_TotEqPEDC_Avg,0) + ISNULL(@ES_KEDC_Avg,0)*ISNULL(@ES_TotEqPEDC_Avg,0) + ISNULL(@HI_KEDC_Avg,0)*ISNULL(@HI_TotEqPEDC_Avg,0) 
									+ ISNULL(@PA_KEDC_Avg,0)*ISNULL(@PA_TotEqPEDC_Avg,0) + ISNULL(@RI_KEDC_Avg,0)*ISNULL(@RI_TotEqPEDC_Avg,0) + ISNULL(@SL_KEDC_Avg,0)*ISNULL(@SL_TotEqPEDC_Avg,0))/@CHEVRON_KEDC_Avg

IF @CHEVRON_KUEDC_Avg > 0
	SELECT	@CHEVRON_NEOpExUEDC_Avg = (ISNULL(@BU_KUEDC_Avg,0)*ISNULL(@BU_NEOpExUEDC_Avg,0) + ISNULL(@CT_KUEDC_Avg,0)*ISNULL(@CT_NEOpExUEDC_Avg,0) + ISNULL(@ES_KUEDC_Avg,0)*ISNULL(@ES_NEOpExUEDC_Avg,0) + ISNULL(@HI_KUEDC_Avg,0)*ISNULL(@HI_NEOpExUEDC_Avg,0) 
								     + ISNULL(@PA_KUEDC_Avg,0)*ISNULL(@PA_NEOpExUEDC_Avg,0) + ISNULL(@RI_KUEDC_Avg,0)*ISNULL(@RI_NEOpExUEDC_Avg,0) + ISNULL(@SL_KUEDC_Avg,0)*ISNULL(@SL_NEOpExUEDC_Avg,0))/@CHEVRON_KUEDC_Avg,
			@CHEVRON_TotCashOpExUEDC_Avg = (ISNULL(@BU_KUEDC_Avg,0)*ISNULL(@BU_TotCashOpExUEDC_Avg,0) + ISNULL(@CT_KUEDC_Avg,0)*ISNULL(@CT_TotCashOpExUEDC_Avg,0) + ISNULL(@ES_KUEDC_Avg,0)*ISNULL(@ES_TotCashOpExUEDC_Avg,0) + ISNULL(@HI_KUEDC_Avg,0)*ISNULL(@HI_TotCashOpExUEDC_Avg,0) 
									      + ISNULL(@PA_KUEDC_Avg,0)*ISNULL(@PA_TotCashOpExUEDC_Avg,0) + ISNULL(@RI_KUEDC_Avg,0)*ISNULL(@RI_TotCashOpExUEDC_Avg,0) + ISNULL(@SL_KUEDC_Avg,0)*ISNULL(@SL_TotCashOpExUEDC_Avg,0))/@CHEVRON_KUEDC_Avg

IF @CHEVRON_KProcEDC_Avg > 0
	SELECT	@CHEVRON_MechAvail_Avg = 100.0 * @CHEVRON_KMAEDC_Avg / @CHEVRON_KProcEDC_Avg

IF @CHEVRON_KTotStdEnergy_Avg > 0
	SELECT	@CHEVRON_EII_Avg = 100.0 * @CHEVRON_KEnergyUseDay_Avg / @CHEVRON_KTotStdEnergy_Avg

IF @CHEVRON_NetInputBPD_Avg > 0
	SELECT	@CHEVRON_CashMargin_Avg = (ISNULL(@BU_NetInputBPD_Avg,0)*ISNULL(@BU_CashMargin_Avg,0) + ISNULL(@CT_NetInputBPD_Avg,0)*ISNULL(@CT_CashMargin_Avg,0) + ISNULL(@ES_NetInputBPD_Avg,0)*ISNULL(@ES_CashMargin_Avg,0) + ISNULL(@HI_NetInputBPD_Avg,0)*ISNULL(@HI_CashMargin_Avg,0) 
									 + ISNULL(@PA_NetInputBPD_Avg,0)*ISNULL(@PA_CashMargin_Avg,0) + ISNULL(@RI_NetInputBPD_Avg,0)*ISNULL(@RI_CashMargin_Avg,0) + ISNULL(@SL_NetInputBPD_Avg,0)*ISNULL(@SL_CashMargin_Avg,0))/@CHEVRON_NetInputBPD_Avg

SET NOCOUNT OFF

SELECT  ReportPeriod = CAST(CAST(@PeriodMonth as varchar(2)) + '/1/' + CAST(@PeriodYear as varchar(4)) as smalldatetime),
	BU_UtilPcnt_Avg=@BU_UtilPcnt_Avg, BU_KEDC_Avg=@BU_KEDC_Avg, BU_KUEDC_Avg=@BU_KUEDC_Avg, 
	BU_MechAvail_Avg=@BU_MechAvail_Avg, BU_KMAEDC_Avg=@BU_KMAEDC_Avg, BU_KProcEDC_Avg=@BU_KProcEDC_Avg,
	BU_EII_Avg=@BU_EII_Avg, BU_KEnergyUseDay_Avg=@BU_KEnergyUseDay_Avg, BU_KTotStdEnergy_Avg=@BU_KTotStdEnergy_Avg,
	BU_MaintIndex_Avg=@BU_MaintIndex_Avg, BU_TAIndex_Avg=@BU_TAIndex_Avg, BU_RoutIndex_Avg=@BU_RoutIndex_Avg,   
	BU_TotEqPEDC_Avg=@BU_TotEqPEDC_Avg, 
	BU_NEOpExUEDC_Avg=@BU_NEOpExUEDC_Avg, BU_TotCashOpExUEDC_Avg=@BU_TotCashOpExUEDC_Avg,
	BU_CashMargin_Avg=@BU_CashMargin_Avg, BU_NetInputBPD_Avg=@BU_NetInputBPD_Avg,

	CT_UtilPcnt_Avg=@CT_UtilPcnt_Avg, CT_KEDC_Avg=@CT_KEDC_Avg, CT_KUEDC_Avg=@CT_KUEDC_Avg, 
	CT_MechAvail_Avg=@CT_MechAvail_Avg, CT_KMAEDC_Avg=@CT_KMAEDC_Avg, CT_KProcEDC_Avg=@CT_KProcEDC_Avg,
	CT_EII_Avg=@CT_EII_Avg, CT_KEnergyUseDay_Avg=@CT_KEnergyUseDay_Avg, CT_KTotStdEnergy_Avg=@CT_KTotStdEnergy_Avg,
	CT_MaintIndex_Avg=@CT_MaintIndex_Avg, CT_TAIndex_Avg=@CT_TAIndex_Avg, CT_RoutIndex_Avg=@CT_RoutIndex_Avg,   
	CT_TotEqPEDC_Avg=@CT_TotEqPEDC_Avg, 
	CT_NEOpExUEDC_Avg=@CT_NEOpExUEDC_Avg, CT_TotCashOpExUEDC_Avg=@CT_TotCashOpExUEDC_Avg,
	CT_CashMargin_Avg=@CT_CashMargin_Avg, CT_NetInputBPD_Avg=@CT_NetInputBPD_Avg,

	ES_UtilPcnt_Avg=@ES_UtilPcnt_Avg, ES_KEDC_Avg=@ES_KEDC_Avg, ES_KUEDC_Avg=@ES_KUEDC_Avg, 
	ES_MechAvail_Avg=@ES_MechAvail_Avg, ES_KMAEDC_Avg=@ES_KMAEDC_Avg, ES_KProcEDC_Avg=@ES_KProcEDC_Avg,
	ES_EII_Avg=@ES_EII_Avg, ES_KEnergyUseDay_Avg=@ES_KEnergyUseDay_Avg, ES_KTotStdEnergy_Avg=@ES_KTotStdEnergy_Avg,
	ES_MaintIndex_Avg=@ES_MaintIndex_Avg, ES_TAIndex_Avg=@ES_TAIndex_Avg, ES_RoutIndex_Avg=@ES_RoutIndex_Avg,   
	ES_TotEqPEDC_Avg=@ES_TotEqPEDC_Avg, 
	ES_NEOpExUEDC_Avg=@ES_NEOpExUEDC_Avg, ES_TotCashOpExUEDC_Avg=@ES_TotCashOpExUEDC_Avg,
	ES_CashMargin_Avg=@ES_CashMargin_Avg, ES_NetInputBPD_Avg=@ES_NetInputBPD_Avg,

	HI_UtilPcnt_Avg=@HI_UtilPcnt_Avg, HI_KEDC_Avg=@HI_KEDC_Avg, HI_KUEDC_Avg=@HI_KUEDC_Avg, 
	HI_MechAvail_Avg=@HI_MechAvail_Avg, HI_KMAEDC_Avg=@HI_KMAEDC_Avg, HI_KProcEDC_Avg=@HI_KProcEDC_Avg,
	HI_EII_Avg=@HI_EII_Avg, HI_KEnergyUseDay_Avg=@HI_KEnergyUseDay_Avg, HI_KTotStdEnergy_Avg=@HI_KTotStdEnergy_Avg,
	HI_MaintIndex_Avg=@HI_MaintIndex_Avg, HI_TAIndex_Avg=@HI_TAIndex_Avg, HI_RoutIndex_Avg=@HI_RoutIndex_Avg,   
	HI_TotEqPEDC_Avg=@HI_TotEqPEDC_Avg, 
	HI_NEOpExUEDC_Avg=@HI_NEOpExUEDC_Avg, HI_TotCashOpExUEDC_Avg=@HI_TotCashOpExUEDC_Avg,
	HI_CashMargin_Avg=@HI_CashMargin_Avg, HI_NetInputBPD_Avg=@HI_NetInputBPD_Avg,

	PA_UtilPcnt_Avg=@PA_UtilPcnt_Avg, PA_KEDC_Avg=@PA_KEDC_Avg, PA_KUEDC_Avg=@PA_KUEDC_Avg, 
	PA_MechAvail_Avg=@PA_MechAvail_Avg, PA_KMAEDC_Avg=@PA_KMAEDC_Avg, PA_KProcEDC_Avg=@PA_KProcEDC_Avg,
	PA_EII_Avg=@PA_EII_Avg, PA_KEnergyUseDay_Avg=@PA_KEnergyUseDay_Avg, PA_KTotStdEnergy_Avg=@PA_KTotStdEnergy_Avg,
	PA_MaintIndex_Avg=@PA_MaintIndex_Avg, PA_TAIndex_Avg=@PA_TAIndex_Avg, PA_RoutIndex_Avg=@PA_RoutIndex_Avg,   
	PA_TotEqPEDC_Avg=@PA_TotEqPEDC_Avg, 
	PA_NEOpExUEDC_Avg=@PA_NEOpExUEDC_Avg, PA_TotCashOpExUEDC_Avg=@PA_TotCashOpExUEDC_Avg,
	PA_CashMargin_Avg=@PA_CashMargin_Avg, PA_NetInputBPD_Avg=@PA_NetInputBPD_Avg,

	RI_UtilPcnt_Avg=@RI_UtilPcnt_Avg, RI_KEDC_Avg=@RI_KEDC_Avg, RI_KUEDC_Avg=@RI_KUEDC_Avg, 
	RI_MechAvail_Avg=@RI_MechAvail_Avg, RI_KMAEDC_Avg=@RI_KMAEDC_Avg, RI_KProcEDC_Avg=@RI_KProcEDC_Avg,
	RI_EII_Avg=@RI_EII_Avg, RI_KEnergyUseDay_Avg=@RI_KEnergyUseDay_Avg, RI_KTotStdEnergy_Avg=@RI_KTotStdEnergy_Avg,
	RI_MaintIndex_Avg=@RI_MaintIndex_Avg, RI_TAIndex_Avg=@RI_TAIndex_Avg, RI_RoutIndex_Avg=@RI_RoutIndex_Avg,   
	RI_TotEqPEDC_Avg=@RI_TotEqPEDC_Avg, 
	RI_NEOpExUEDC_Avg=@RI_NEOpExUEDC_Avg, RI_TotCashOpExUEDC_Avg=@RI_TotCashOpExUEDC_Avg,
	RI_CashMargin_Avg=@RI_CashMargin_Avg, RI_NetInputBPD_Avg=@RI_NetInputBPD_Avg,

	SL_UtilPcnt_Avg=@SL_UtilPcnt_Avg, SL_KEDC_Avg=@SL_KEDC_Avg, SL_KUEDC_Avg=@SL_KUEDC_Avg, 
	SL_MechAvail_Avg=@SL_MechAvail_Avg, SL_KMAEDC_Avg=@SL_KMAEDC_Avg, SL_KProcEDC_Avg=@SL_KProcEDC_Avg,
	SL_EII_Avg=@SL_EII_Avg, SL_KEnergyUseDay_Avg=@SL_KEnergyUseDay_Avg, SL_KTotStdEnergy_Avg=@SL_KTotStdEnergy_Avg,
	SL_MaintIndex_Avg=@SL_MaintIndex_Avg, SL_TAIndex_Avg=@SL_TAIndex_Avg, SL_RoutIndex_Avg=@SL_RoutIndex_Avg,   
	SL_TotEqPEDC_Avg=@SL_TotEqPEDC_Avg, 
	SL_NEOpExUEDC_Avg=@SL_NEOpExUEDC_Avg, SL_TotCashOpExUEDC_Avg=@SL_TotCashOpExUEDC_Avg,
	SL_CashMargin_Avg=@SL_CashMargin_Avg, SL_NetInputBPD_Avg=@SL_NetInputBPD_Avg,

	CHEVRON_UtilPcnt_Avg=@CHEVRON_UtilPcnt_Avg, CHEVRON_KEDC_Avg=@CHEVRON_KEDC_Avg, CHEVRON_KUEDC_Avg=@CHEVRON_KUEDC_Avg, 
	CHEVRON_MechAvail_Avg=@CHEVRON_MechAvail_Avg, CHEVRON_KMAEDC_Avg=@CHEVRON_KMAEDC_Avg, CHEVRON_KProcEDC_Avg=@CHEVRON_KProcEDC_Avg,
	CHEVRON_EII_Avg=@CHEVRON_EII_Avg, CHEVRON_KEnergyUseDay_Avg=@CHEVRON_KEnergyUseDay_Avg, CHEVRON_KTotStdEnergy_Avg=@CHEVRON_KTotStdEnergy_Avg,
	CHEVRON_MaintIndex_Avg=@CHEVRON_MaintIndex_Avg, CHEVRON_TAIndex_Avg=@CHEVRON_TAIndex_Avg, CHEVRON_RoutIndex_Avg=@CHEVRON_RoutIndex_Avg,   
	CHEVRON_TotEqPEDC_Avg=@CHEVRON_TotEqPEDC_Avg, 
	CHEVRON_NEOpExUEDC_Avg=@CHEVRON_NEOpExUEDC_Avg, CHEVRON_TotCashOpExUEDC_Avg=@CHEVRON_TotCashOpExUEDC_Avg,
	CHEVRON_CashMargin_Avg=@CHEVRON_CashMargin_Avg, CHEVRON_NetInputBPD_Avg=@CHEVRON_NetInputBPD_Avg


