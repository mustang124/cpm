﻿CREATE PROC [dbo].[spLogActivity]
@Application varchar(20),
@Methodology varchar(20),
@RefineryID varchar(6), 
@CallerIP varchar(20), 
@UserID varchar(20), 
@ComputerName varchar(50), 
@Service varchar(50),
@Method varchar(50), 
@EntityName varchar(50), 
@PeriodStart Varchar(20),
@PeriodEnd varchar(20),
@Notes varchar(max),
@Status varchar(50) 
AS
INSERT INTO dbo.ActivityLog(ActivityTime, [Application], Methodology, RefineryID, CallerIP, UserID,ComputerName,  [Service], Method,EntityName,PeriodStart,PeriodEnd,Notes, Status)
VALUES(GETDATE(),@Application, @Methodology, @RefineryID, @CallerIP, @UserID, @ComputerName,  @Service, @Method, @EntityName, @PeriodStart, @PeriodEnd, @Notes, @Status)


