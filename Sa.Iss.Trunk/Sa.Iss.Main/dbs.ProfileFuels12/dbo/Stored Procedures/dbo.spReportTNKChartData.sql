﻿CREATE   PROC [dbo].[spReportTNKChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'RUB', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	RefUtilPcnt real NULL, 
	ProcessUtilPcnt real NULL, 
	OpAvail real NULL, 
	EII real NULL, 
	ProcessEffIndex real NULL, 
	VEI real NULL, 
	PersIndex real NULL, 
	MaintIndex real NULL, 
	NEOpExEDC real NULL,
	OpExUEDC real NULL
)

--- Everything Already Available in GenSum (missing gain and OpEx on an EDC basis)
INSERT INTO @Data (PeriodStart, PeriodEnd, RefUtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex, MaintIndex, NEOpExEDC, OpExUEDC)
SELECT PeriodStart, PeriodEnd, RefUtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex, MaintIndex, NEOpExEDC, OpExUEDC
FROM dbo.GetCommonProfileLiteChartData(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM, 12)

IF (SELECT COUNT(*) FROM @Data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @Data WHERE PeriodStart = @Period)
			INSERT @Data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

/* Saratov needs PEI instead of VEI */
SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilPcnt = RefUtilPcnt, VolGainKPI = CASE WHEN @RefineryID = '294EUR' THEN ProcessEffIndex ELSE VEI END, OpAvail, PersIndex, MaintIndex, NEOpExEDC, OpExUEDC = OpExUEDC/100
FROM @Data
ORDER BY PeriodStart ASC

