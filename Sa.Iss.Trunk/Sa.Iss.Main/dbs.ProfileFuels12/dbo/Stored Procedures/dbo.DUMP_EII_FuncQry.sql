﻿CREATE PROCEDURE [dbo].[DUMP_EII_FuncQry]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@StudyYear nvarchar(20),
	@DataSetID nvarchar(10),
	@Refnum nvarchar(10)
	
	AS
	
	SELECT s.Location,s.PeriodStart,s.PeriodEnd, 
                  s.NumDays as DaysInPeriod,@CurrencyCode AS Currency,@UOM as UOM ,  
                   p.Description,c.SortKey,c.UnitName, c.UnitID, c.ProcessID, 
                   c.ProcessType, c.UtilCap, fc.StdEnergy, fc.StdGain,  
                  VEIFormulaForReport, EIIFormulaForReport, DisplayTextUS 
                   FROM Config c, FactorCalc fc, Factors f, ProcessID_LU p, DisplayUnits_LU d , Submissions s  
                   WHERE c.UnitID = fc.UnitID AND c.SubmissionID = fc.SubmissionID    
                   AND f.FactorSet=fc.FactorSet AND f.ProcessID = c.ProcessID AND f.ProcessType = c.ProcessType AND f.FactorSet=@StudyYear
                  AND c.ProcessID = p.ProcessID AND c.ProcessID NOT IN ('STEAMGEN','ELECGEN','FCCPOWER','FTCOGEN','BLENDING','TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') and p.DisplayUnits = d.DisplayUnits 
                   AND s.SubmissionID=c.SubmissionID AND c.SubmissionID IN  
                  (SELECT Distinct SubmissionID FROM dbo.Submissions WHERE  DataSet=@DataSetID
                  AND  RefineryID=@Refnum
                   ) ORDER BY PeriodStart DESC, c.SortKey

