﻿CREATE  PROC [dbo].[LoadSubmission](@RefineryID varchar(6), @PeriodYear smallint, @PeriodMonth tinyint, @DataSet varchar(15) = 'ACTUAL', @SubmissionID int OUTPUT, 
	@RptCurrency varchar(5), @RptCurrencyT15 varchar(5), @UOM varchar(5), @Company nvarchar(30), @Location nvarchar(30), 
	@CoordName nvarchar(50), @CoordTitle nvarchar(50), @CoordPhone varchar(40), @CoordEMail varchar(255),
	@BridgeVersion varchar(10) = NULL, @ClientVersion varchar(10) = NULL)
AS
SET NOCOUNT ON

IF @RefineryID IS NULL OR @PeriodYear IS NULL OR @PeriodMonth IS NULL OR @RptCurrency IS NULL OR @UOM IS NULL
	RETURN 1
IF @PeriodMonth NOT BETWEEN 1 AND 12
	RETURN 2
IF @PeriodYear NOT BETWEEN 1997 AND DATEPART(yy, getdate()) + 5
	RETURN 3
IF @DataSet IS NULL
	SET @DataSet = 'ACTUAL'
IF @RptCurrencyT15 IS NULL OR @RptCurrencyT15 = ''
	SELECT @RptCurrencyT15 = @RptCurrency

--IF NOT EXISTS (SELECT * FROM SubmissionsAll WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet)
IF EXISTS (SELECT * FROM SubmissionsAll WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth)
	UPDATE SubmissionsAll SET UseSubmission = 0 WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth

INSERT INTO SubmissionsAll(RefineryID, DataSet, PeriodYear, PeriodMonth, RptCurrency, RptCurrencyT15, UOM, UseSubmission
	, Company, Location, CoordName, CoordTitle, CoordPhone, CoordEMail, BridgeVersion, ClientVersion)
VALUES (@RefineryID, @DataSet, @PeriodYear, @PeriodMonth, @RptCurrency, @RptCurrencyT15, @UOM, 1
	, @Company, @Location, @CoordName, @CoordTitle, @CoordPhone, @CoordEMail, @BridgeVersion, @ClientVersion)

SELECT @SubmissionID = SubmissionID
FROM SubmissionsAll
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND UseSubmission = 1

SET NOCOUNT OFF

SELECT * FROM Submissions WHERE SubmissionID = @SubmissionID

