﻿CREATE PROCEDURE [dbo].[DUMP_GPV_PrimaryProductsLubes]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@PriceCol real,
	@DataSetID nvarchar(10),
	@Refnum nvarchar(10)
	
	AS
	
	SELECT s.Location,s.PeriodStart, s.PeriodEnd,  
        s.NumDays as DaysInPeriod, @CurrencyCode  as Currency,@UOM  as UOM,y.MaterialID,  
        SAIName as MaterialName, ISNULL(SUM(Bbl),0) AS Bbl,   
         CASE WHEN  SUM(y.Bbl)=0 THEN 0   
         ELSE   
         ISNULL(SUM(Bbl * PriceLocal) / SUM(Bbl), 0) 
         END AS PriceLocal,  
         CASE WHEN  SUM(y.Bbl)=0 THEN 0   
         ELSE   
         ISNULL(SUM(Bbl * PriceUS) / SUM(Bbl), 0) 
         END AS PriceUS,   
         ISNULL(SUM(Bbl),0)*ISNULL(SUM( @PriceCol ),0)/1000 As GPV,  
         MIN(m.SortKey) AS SortKey   
         FROM Yield y  
         INNER JOIN Submissions s ON  y.SubmissionID=s.SubmissionID AND s.DataSet=@DataSetID  AND  s.RefineryID=@Refnum
         INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID   
         INNER JOIN TSort t ON t.RefineryID =@Refnum 
        WHERE(AllowInProd = 1 And LubesOnly = 1 And FuelsLubesCombo = 1)  
         GROUP BY s.PeriodStart,s.PeriodEnd,s.Location,s.NumDays,y.MaterialID, SAIName ORDER BY s.PeriodStart DESC,SortKey ASC

