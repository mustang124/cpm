﻿CREATE    PROC [dbo].[spReportValeroBURMonthly] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
DECLARE @AD_KEDC real, @AD_MechAvail real, @AD_AdjMaintIndex real,  
	@AD_UtilPcnt real, @AD_NEOpExEDC real, @AD_EII real, @AD_TotWHrEDC real, 
	@AD_NetInputKBPD real, @AD_CrudeKBPD real, @AD_FCCRateKBPD real,  
	@AD_NumContPers real, @AD_RoutMechUnavail real, @AD_TAMechUnavail real
DECLARE @BN_KEDC real, @BN_MechAvail real, @BN_AdjMaintIndex real,  
	@BN_UtilPcnt real, @BN_NEOpExEDC real, @BN_EII real,  @BN_TotWHrEDC real, 
	@BN_NetInputKBPD real, @BN_CrudeKBPD real, @BN_FCCRateKBPD real, 
	@BN_NumContPers real, @BN_RoutMechUnavail real, @BN_TAMechUnavail real
DECLARE @CC_KEDC real, @CC_MechAvail real, @CC_AdjMaintIndex real,  
	@CC_UtilPcnt real, @CC_NEOpExEDC real, @CC_EII real, @CC_TotWHrEDC real, 
	@CC_NetInputKBPD real, @CC_CrudeKBPD real, @CC_FCCRateKBPD real,  
	@CC_NumContPers real, @CC_RoutMechUnavail real, @CC_TAMechUnavail real
DECLARE @HO_KEDC real, @HO_MechAvail real, @HO_AdjMaintIndex real,  
	@HO_UtilPcnt real, @HO_NEOpExEDC real, @HO_EII real, @HO_TotWHrEDC real, 
	@HO_NetInputKBPD real, @HO_CrudeKBPD real, @HO_FCCRateKBPD real, 
	@HO_NumContPers real, @HO_RoutMechUnavail real, @HO_TAMechUnavail real
DECLARE @MK_KEDC real, @MK_MechAvail real, @MK_AdjMaintIndex real, 
	@MK_UtilPcnt real, @MK_NEOpExEDC real, @MK_EII real, @MK_TotWHrEDC real, 
	@MK_NetInputKBPD real, @MK_CrudeKBPD real, @MK_FCCRateKBPD real,  
	@MK_NumContPers real, @MK_RoutMechUnavail real, @MK_TAMechUnavail real
DECLARE @MP_KEDC real, @MP_MechAvail real, @MP_AdjMaintIndex real, 
	@MP_UtilPcnt real, @MP_NEOpExEDC real, @MP_EII real, @MP_TotWHrEDC real, 
	@MP_NetInputKBPD real, @MP_CrudeKBPD real, @MP_FCCRateKBPD real,  
	@MP_NumContPers real, @MP_RoutMechUnavail real, @MP_TAMechUnavail real
DECLARE @PA_KEDC real, @PA_MechAvail real, @PA_AdjMaintIndex real, 
	@PA_UtilPcnt real, @PA_NEOpExEDC real, @PA_EII real, @PA_TotWHrEDC real, 
	@PA_NetInputKBPD real, @PA_CrudeKBPD real, @PA_FCCRateKBPD real,  
	@PA_NumContPers real, @PA_RoutMechUnavail real, @PA_TAMechUnavail real
DECLARE @QB_KEDC real, @QB_MechAvail real, @QB_AdjMaintIndex real, 
	@QB_UtilPcnt real, @QB_NEOpExEDC real, @QB_EII real, @QB_TotWHrEDC real,
	@QB_NetInputKBPD real, @QB_CrudeKBPD real, @QB_FCCRateKBPD real,  
	@QB_NumContPers real, @QB_RoutMechUnavail real, @QB_TAMechUnavail real
DECLARE @SC_KEDC real, @SC_MechAvail real, @SC_AdjMaintIndex real, 
	@SC_UtilPcnt real, @SC_NEOpExEDC real, @SC_EII real, @SC_TotWHrEDC real, 
	@SC_NetInputKBPD real, @SC_CrudeKBPD real, @SC_FCCRateKBPD real, 
	@SC_NumContPers real, @SC_RoutMechUnavail real, @SC_TAMechUnavail real
DECLARE @TC_KEDC real,  @TC_MechAvail real, @TC_AdjMaintIndex real,  
	@TC_UtilPcnt real,  @TC_NEOpExEDC real, @TC_EII real, @TC_TotWHrEDC real, 
	@TC_NetInputKBPD real, @TC_CrudeKBPD real, @TC_FCCRateKBPD real,  
	@TC_NumContPers real, @TC_RoutMechUnavail real, @TC_TAMechUnavail real
DECLARE @TR_KEDC real, @TR_MechAvail real, @TR_AdjMaintIndex real, 
	@TR_UtilPcnt real, @TR_NEOpExEDC real,	@TR_EII real, @TR_TotWHrEDC real,
	@TR_NetInputKBPD real, @TR_CrudeKBPD real, @TR_FCCRateKBPD real,  
	@TR_NumContPers real, @TR_RoutMechUnavail real, @TR_TAMechUnavail real
DECLARE @WM_KEDC real, @WM_MechAvail real, @WM_AdjMaintIndex real,  
	@WM_UtilPcnt real, @WM_NEOpExEDC real, @WM_EII real, @WM_TotWHrEDC real, 
	@WM_NetInputKBPD real, @WM_CrudeKBPD real, @WM_FCCRateKBPD real,  
	@WM_NumContPers real, @WM_RoutMechUnavail real, @WM_TAMechUnavail real
DECLARE @VALERO_KEDC real, @VALERO_MechAvail real, @VALERO_AdjMaintIndex real, 
	@VALERO_UtilPcnt real, @VALERO_NEOpExEDC real, @VALERO_EII real, @VALERO_TotWHrEDC real,
	@VALERO_NetInputKBPD real, @VALERO_CrudeKBPD real, @VALERO_FCCRateKBPD real, 
	@VALERO_NumContPers real, @VALERO_RoutMechUnavail real, @VALERO_TAMechUnavail real


EXEC [dbo].[spReportValeroBURByRef] '75NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @AD_KEDC OUTPUT, @MechAvail = @AD_MechAvail OUTPUT, @AdjMaintIndex = @AD_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @AD_UtilPcnt OUTPUT, @NEOpExEDC = @AD_NEOpExEDC OUTPUT, @EII = @AD_EII OUTPUT, 
	@TotWHrEDC = @AD_TotWHrEDC OUTPUT, @NetInputKBPD = @AD_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @AD_CrudeKBPD OUTPUT, @FCCRateKBPD = @AD_FCCRateKBPD OUTPUT,  
	@NumContPers = @AD_NumContPers OUTPUT, @RoutMechUnavail = @AD_RoutMechUnavail OUTPUT, @TAMechUnavail = @AD_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '33NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @BN_KEDC OUTPUT, @MechAvail = @BN_MechAvail OUTPUT, @AdjMaintIndex = @BN_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @BN_UtilPcnt OUTPUT, @NEOpExEDC = @BN_NEOpExEDC OUTPUT, @EII = @BN_EII OUTPUT, 
	@TotWHrEDC = @BN_TotWHrEDC OUTPUT, @NetInputKBPD = @BN_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @BN_CrudeKBPD OUTPUT, @FCCRateKBPD = @BN_FCCRateKBPD OUTPUT,  
	@NumContPers = @BN_NumContPers OUTPUT, @RoutMechUnavail = @BN_RoutMechUnavail OUTPUT, @TAMechUnavail = @BN_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '218NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @CC_KEDC OUTPUT, @MechAvail = @CC_MechAvail OUTPUT, @AdjMaintIndex = @CC_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @CC_UtilPcnt OUTPUT, @NEOpExEDC = @CC_NEOpExEDC OUTPUT, @EII = @CC_EII OUTPUT, 
	@TotWHrEDC = @CC_TotWHrEDC OUTPUT, @NetInputKBPD = @CC_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @CC_CrudeKBPD OUTPUT, @FCCRateKBPD = @CC_FCCRateKBPD OUTPUT,  
	@NumContPers = @CC_NumContPers OUTPUT, @RoutMechUnavail = @CC_RoutMechUnavail OUTPUT, @TAMechUnavail = @CC_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '98NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @HO_KEDC OUTPUT, @MechAvail = @HO_MechAvail OUTPUT, @AdjMaintIndex = @HO_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @HO_UtilPcnt OUTPUT, @NEOpExEDC = @HO_NEOpExEDC OUTPUT, @EII = @HO_EII OUTPUT, 
	@TotWHrEDC = @HO_TotWHrEDC OUTPUT, @NetInputKBPD = @HO_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @HO_CrudeKBPD OUTPUT, @FCCRateKBPD = @HO_FCCRateKBPD OUTPUT,  
	@NumContPers = @HO_NumContPers OUTPUT, @RoutMechUnavail = @HO_RoutMechUnavail OUTPUT, @TAMechUnavail = @HO_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '28NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @MK_KEDC OUTPUT, @MechAvail = @MK_MechAvail OUTPUT, @AdjMaintIndex = @MK_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @MK_UtilPcnt OUTPUT, @NEOpExEDC = @MK_NEOpExEDC OUTPUT, @EII = @MK_EII OUTPUT, 
	@TotWHrEDC = @MK_TotWHrEDC OUTPUT, @NetInputKBPD = @MK_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @MK_CrudeKBPD OUTPUT, @FCCRateKBPD = @MK_FCCRateKBPD OUTPUT,  
	@NumContPers = @MK_NumContPers OUTPUT, @RoutMechUnavail = @MK_RoutMechUnavail OUTPUT, @TAMechUnavail = @MK_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '38NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @MP_KEDC OUTPUT, @MechAvail = @MP_MechAvail OUTPUT, @AdjMaintIndex = @MP_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @MP_UtilPcnt OUTPUT, @NEOpExEDC = @MP_NEOpExEDC OUTPUT, @EII = @MP_EII OUTPUT, 
	@TotWHrEDC = @MP_TotWHrEDC OUTPUT, @NetInputKBPD = @MP_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @MP_CrudeKBPD OUTPUT, @FCCRateKBPD = @MP_FCCRateKBPD OUTPUT,  
	@NumContPers = @MP_NumContPers OUTPUT, @RoutMechUnavail = @MP_RoutMechUnavail OUTPUT, @TAMechUnavail = @MP_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '19NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @PA_KEDC OUTPUT, @MechAvail = @PA_MechAvail OUTPUT, @AdjMaintIndex = @PA_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @PA_UtilPcnt OUTPUT, @NEOpExEDC = @PA_NEOpExEDC OUTPUT, @EII = @PA_EII OUTPUT, 
	@TotWHrEDC = @PA_TotWHrEDC OUTPUT, @NetInputKBPD = @PA_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @PA_CrudeKBPD OUTPUT, @FCCRateKBPD = @PA_FCCRateKBPD OUTPUT,  
	@NumContPers = @PA_NumContPers OUTPUT, @RoutMechUnavail = @PA_RoutMechUnavail OUTPUT, @TAMechUnavail = @PA_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '110NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @QB_KEDC OUTPUT, @MechAvail = @QB_MechAvail OUTPUT, @AdjMaintIndex = @QB_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @QB_UtilPcnt OUTPUT, @NEOpExEDC = @QB_NEOpExEDC OUTPUT, @EII = @QB_EII OUTPUT, 
	@TotWHrEDC = @QB_TotWHrEDC OUTPUT, @NetInputKBPD = @QB_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @QB_CrudeKBPD OUTPUT, @FCCRateKBPD = @QB_FCCRateKBPD OUTPUT,  
	@NumContPers = @QB_NumContPers OUTPUT, @RoutMechUnavail = @QB_RoutMechUnavail OUTPUT, @TAMechUnavail = @QB_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '217NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @SC_KEDC OUTPUT, @MechAvail = @SC_MechAvail OUTPUT, @AdjMaintIndex = @SC_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @SC_UtilPcnt OUTPUT, @NEOpExEDC = @SC_NEOpExEDC OUTPUT, @EII = @SC_EII OUTPUT, 
	@TotWHrEDC = @SC_TotWHrEDC OUTPUT, @NetInputKBPD = @SC_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @SC_CrudeKBPD OUTPUT, @FCCRateKBPD = @SC_FCCRateKBPD OUTPUT,  
	@NumContPers = @SC_NumContPers OUTPUT, @RoutMechUnavail = @SC_RoutMechUnavail OUTPUT, @TAMechUnavail = @SC_TAMechUnavail OUTPUT
	
EXEC [dbo].[spReportValeroBURByRef] '72NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @TC_KEDC OUTPUT, @MechAvail = @TC_MechAvail OUTPUT, @AdjMaintIndex = @TC_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @TC_UtilPcnt OUTPUT, @NEOpExEDC = @TC_NEOpExEDC OUTPUT, @EII = @TC_EII OUTPUT, 
	@TotWHrEDC = @TC_TotWHrEDC OUTPUT, @NetInputKBPD = @TC_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @TC_CrudeKBPD OUTPUT, @FCCRateKBPD = @TC_FCCRateKBPD OUTPUT,  
	@NumContPers = @TC_NumContPers OUTPUT, @RoutMechUnavail = @TC_RoutMechUnavail OUTPUT, @TAMechUnavail = @TC_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '29NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @TR_KEDC OUTPUT, @MechAvail = @TR_MechAvail OUTPUT, @AdjMaintIndex = @TR_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @TR_UtilPcnt OUTPUT, @NEOpExEDC = @TR_NEOpExEDC OUTPUT, @EII = @TR_EII OUTPUT, 
	@TotWHrEDC = @TR_TotWHrEDC OUTPUT, @NetInputKBPD = @TR_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @TR_CrudeKBPD OUTPUT, @FCCRateKBPD = @TR_FCCRateKBPD OUTPUT,  
	@NumContPers = @TR_NumContPers OUTPUT, @RoutMechUnavail = @TR_RoutMechUnavail OUTPUT, @TAMechUnavail = @TR_TAMechUnavail OUTPUT

EXEC [dbo].[spReportValeroBURByRef] '91NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC = @WM_KEDC OUTPUT, @MechAvail = @WM_MechAvail OUTPUT, @AdjMaintIndex = @WM_AdjMaintIndex OUTPUT, 
	@UtilPcnt = @WM_UtilPcnt OUTPUT, @NEOpExEDC = @WM_NEOpExEDC OUTPUT, @EII = @WM_EII OUTPUT, 
	@TotWHrEDC = @WM_TotWHrEDC OUTPUT, @NetInputKBPD = @WM_NetInputKBPD OUTPUT, 
	@CrudeKBPD = @WM_CrudeKBPD OUTPUT, @FCCRateKBPD = @WM_FCCRateKBPD OUTPUT,  
	@NumContPers = @WM_NumContPers OUTPUT, @RoutMechUnavail = @WM_RoutMechUnavail OUTPUT, @TAMechUnavail = @WM_TAMechUnavail OUTPUT

SELECT 	@VALERO_KEDC = ISNULL(@AD_KEDC,0) + ISNULL(@BN_KEDC,0) + ISNULL(@CC_KEDC,0) + ISNULL(@HO_KEDC,0) + ISNULL(@MK_KEDC,0) 
			+ ISNULL(@MP_KEDC,0) + ISNULL(@PA_KEDC,0) + ISNULL(@QB_KEDC,0) + ISNULL(@SC_KEDC,0) 
			+ ISNULL(@TC_KEDC,0) + ISNULL(@TR_KEDC,0) + ISNULL(@WM_KEDC,0), 
	@VALERO_NetInputKBPD = ISNULL(@AD_NetInputKBPD,0) + ISNULL(@BN_NetInputKBPD,0) + ISNULL(@CC_NetInputKBPD,0) + ISNULL(@HO_NetInputKBPD,0) + ISNULL(@MK_NetInputKBPD,0) 
			+ ISNULL(@MP_NetInputKBPD,0) + ISNULL(@PA_NetInputKBPD,0) + ISNULL(@QB_NetInputKBPD,0) + ISNULL(@SC_NetInputKBPD,0) 
			+ ISNULL(@TC_NetInputKBPD,0) + ISNULL(@TR_NetInputKBPD,0) + ISNULL(@WM_NetInputKBPD,0),
	@VALERO_CrudeKBPD = ISNULL(@AD_CrudeKBPD,0) + ISNULL(@BN_CrudeKBPD,0) + ISNULL(@CC_CrudeKBPD,0) + ISNULL(@HO_CrudeKBPD,0) + ISNULL(@MK_CrudeKBPD,0) 
			+ ISNULL(@MP_CrudeKBPD,0) + ISNULL(@PA_CrudeKBPD,0) + ISNULL(@QB_CrudeKBPD,0) + ISNULL(@SC_CrudeKBPD,0) 
			+ ISNULL(@TC_CrudeKBPD,0) + ISNULL(@TR_CrudeKBPD,0) + ISNULL(@WM_CrudeKBPD,0),
	@VALERO_FCCRateKBPD = ISNULL(@AD_FCCRateKBPD,0) + ISNULL(@BN_FCCRateKBPD,0) + ISNULL(@CC_FCCRateKBPD,0) + ISNULL(@HO_FCCRateKBPD,0) + ISNULL(@MK_FCCRateKBPD,0) 
			+ ISNULL(@MP_FCCRateKBPD,0) + ISNULL(@PA_FCCRateKBPD,0) + ISNULL(@QB_FCCRateKBPD,0) + ISNULL(@SC_FCCRateKBPD,0) 
			+ ISNULL(@TC_FCCRateKBPD,0) + ISNULL(@TR_FCCRateKBPD,0) + ISNULL(@WM_FCCRateKBPD,0),
	@VALERO_NumContPers = ISNULL(@AD_NumContPers,0) + ISNULL(@BN_NumContPers,0) + ISNULL(@CC_NumContPers,0) + ISNULL(@HO_NumContPers,0) + ISNULL(@MK_NumContPers,0) 
			+ ISNULL(@MP_NumContPers,0) + ISNULL(@PA_NumContPers,0) + ISNULL(@QB_NumContPers,0) + ISNULL(@SC_NumContPers,0) 
			+ ISNULL(@TC_NumContPers,0) + ISNULL(@TR_NumContPers,0) + ISNULL(@WM_NumContPers,0)
IF @VALERO_KEDC > 0
	SELECT @VALERO_MechAvail = (ISNULL(@AD_MechAvail*@AD_KEDC,0) + ISNULL(@BN_MechAvail*@BN_KEDC,0) + ISNULL(@CC_MechAvail*@CC_KEDC,0) 
			+ ISNULL(@HO_MechAvail*@HO_KEDC,0) + ISNULL(@MK_MechAvail*@MK_KEDC,0) + ISNULL(@MP_MechAvail*@MP_KEDC,0) 
			+ ISNULL(@PA_MechAvail*@PA_KEDC,0) + ISNULL(@QB_MechAvail*@QB_KEDC,0) 
			+ ISNULL(@SC_MechAvail*@SC_KEDC,0) + ISNULL(@TC_MechAvail*@TC_KEDC,0) + ISNULL(@TR_MechAvail*@TR_KEDC,0) 
			+ ISNULL(@WM_MechAvail*@WM_KEDC,0))/@VALERO_KEDC, 
	@VALERO_AdjMaintIndex = (ISNULL(@AD_AdjMaintIndex*@AD_KEDC,0) + ISNULL(@BN_AdjMaintIndex*@BN_KEDC,0) + ISNULL(@CC_AdjMaintIndex*@CC_KEDC,0) 
			+ ISNULL(@HO_AdjMaintIndex*@HO_KEDC,0) + ISNULL(@MK_AdjMaintIndex*@MK_KEDC,0) + ISNULL(@MP_AdjMaintIndex*@MP_KEDC,0) 
			+ ISNULL(@PA_AdjMaintIndex*@PA_KEDC,0) + ISNULL(@QB_AdjMaintIndex*@QB_KEDC,0) 
			+ ISNULL(@SC_AdjMaintIndex*@SC_KEDC,0) + ISNULL(@TC_AdjMaintIndex*@TC_KEDC,0) + ISNULL(@TR_AdjMaintIndex*@TR_KEDC,0) 
			+ ISNULL(@WM_AdjMaintIndex*@WM_KEDC,0))/@VALERO_KEDC, 
	@VALERO_UtilPcnt = (ISNULL(@AD_UtilPcnt*@AD_KEDC,0) + ISNULL(@BN_UtilPcnt*@BN_KEDC,0) + ISNULL(@CC_UtilPcnt*@CC_KEDC,0) 
			+ ISNULL(@HO_UtilPcnt*@HO_KEDC,0) + ISNULL(@MK_UtilPcnt*@MK_KEDC,0) + ISNULL(@MP_UtilPcnt*@MP_KEDC,0) 
			+ ISNULL(@PA_UtilPcnt*@PA_KEDC,0) + ISNULL(@QB_UtilPcnt*@QB_KEDC,0) 
			+ ISNULL(@SC_UtilPcnt*@SC_KEDC,0) + ISNULL(@TC_UtilPcnt*@TC_KEDC,0) + ISNULL(@TR_UtilPcnt*@TR_KEDC,0) 
			+ ISNULL(@WM_UtilPcnt*@WM_KEDC,0))/@VALERO_KEDC, 
	@VALERO_NEOpExEDC = (ISNULL(@AD_NEOpExEDC*@AD_KEDC,0) + ISNULL(@BN_NEOpExEDC*@BN_KEDC,0) + ISNULL(@CC_NEOpExEDC*@CC_KEDC,0) 
			+ ISNULL(@HO_NEOpExEDC*@HO_KEDC,0) + ISNULL(@MK_NEOpExEDC*@MK_KEDC,0) + ISNULL(@MP_NEOpExEDC*@MP_KEDC,0) 
			+ ISNULL(@PA_NEOpExEDC*@PA_KEDC,0) + ISNULL(@QB_NEOpExEDC*@QB_KEDC,0) 
			+ ISNULL(@SC_NEOpExEDC*@SC_KEDC,0) + ISNULL(@TC_NEOpExEDC*@TC_KEDC,0) + ISNULL(@TR_NEOpExEDC*@TR_KEDC,0) 
			+ ISNULL(@WM_NEOpExEDC*@WM_KEDC,0))/@VALERO_KEDC, 
	@VALERO_TotWHrEDC = (ISNULL(@AD_TotWHrEDC*@AD_KEDC,0) + ISNULL(@BN_TotWHrEDC*@BN_KEDC,0) + ISNULL(@CC_TotWHrEDC*@CC_KEDC,0) 
			+ ISNULL(@HO_TotWHrEDC*@HO_KEDC,0) + ISNULL(@MK_TotWHrEDC*@MK_KEDC,0) + ISNULL(@MP_TotWHrEDC*@MP_KEDC,0) 
			+ ISNULL(@PA_TotWHrEDC*@PA_KEDC,0) + ISNULL(@QB_TotWHrEDC*@QB_KEDC,0) 
			+ ISNULL(@SC_TotWHrEDC*@SC_KEDC,0) + ISNULL(@TC_TotWHrEDC*@TC_KEDC,0) + ISNULL(@TR_TotWHrEDC*@TR_KEDC,0) 
			+ ISNULL(@WM_TotWHrEDC*@WM_KEDC,0))/@VALERO_KEDC, 
	@VALERO_RoutMechUnavail = (ISNULL(@AD_RoutMechUnavail*@AD_KEDC,0) + ISNULL(@BN_RoutMechUnavail*@BN_KEDC,0) + ISNULL(@CC_RoutMechUnavail*@CC_KEDC,0) 
			+ ISNULL(@HO_RoutMechUnavail*@HO_KEDC,0) + ISNULL(@MK_RoutMechUnavail*@MK_KEDC,0) + ISNULL(@MP_RoutMechUnavail*@MP_KEDC,0) 
			+ ISNULL(@PA_RoutMechUnavail*@PA_KEDC,0) + ISNULL(@QB_RoutMechUnavail*@QB_KEDC,0) 
			+ ISNULL(@SC_RoutMechUnavail*@SC_KEDC,0) + ISNULL(@TC_RoutMechUnavail*@TC_KEDC,0) + ISNULL(@TR_RoutMechUnavail*@TR_KEDC,0) 
			+ ISNULL(@WM_RoutMechUnavail*@WM_KEDC,0))/@VALERO_KEDC, 
	@VALERO_TAMechUnavail = (ISNULL(@AD_TAMechUnavail*@AD_KEDC,0) + ISNULL(@BN_TAMechUnavail*@BN_KEDC,0) + ISNULL(@CC_TAMechUnavail*@CC_KEDC,0) 
			+ ISNULL(@HO_TAMechUnavail*@HO_KEDC,0) + ISNULL(@MK_TAMechUnavail*@MK_KEDC,0) + ISNULL(@MP_TAMechUnavail*@MP_KEDC,0) 
			+ ISNULL(@PA_TAMechUnavail*@PA_KEDC,0) + ISNULL(@QB_TAMechUnavail*@QB_KEDC,0) 
			+ ISNULL(@SC_TAMechUnavail*@SC_KEDC,0) + ISNULL(@TC_TAMechUnavail*@TC_KEDC,0) + ISNULL(@TR_TAMechUnavail*@TR_KEDC,0) 
			+ ISNULL(@WM_TAMechUnavail*@WM_KEDC,0))/@VALERO_KEDC

SELECT @VALERO_EII = 100*SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) 
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA')
AND s.DataSet = 'Actual' AND s.PeriodYear = @PeriodYear AND s.PeriodMonth = @PeriodMonth AND s.UseSubmission = 1
AND a.FactorSet = @FactorSet AND a.EnergyUseDay > 0 AND a.TotStdEnergy> 0 AND s.NumDays > 0

SET NOCOUNT OFF

SELECT  ReportPeriod = CAST(CAST(@PeriodMonth as varchar(2)) + '/1/' + CAST(@PeriodYear as varchar(4)) as smalldatetime),
	AD_KEDC = @AD_KEDC , AD_MechAvail = @AD_MechAvail, AD_AdjMaintIndex = @AD_AdjMaintIndex, 
	AD_UtilPcnt = @AD_UtilPcnt, AD_NEOpExEDC = @AD_NEOpExEDC, AD_EII = @AD_EII, AD_TotWHrEDC = @AD_TotWHrEDC, 
	AD_NetInputKBPD = @AD_NetInputKBPD, AD_CrudeKBPD = @AD_CrudeKBPD, AD_FCCRateKBPD = @AD_FCCRateKBPD,  
	AD_NumContPers = @AD_NumContPers, AD_RoutMechUnavail = @AD_RoutMechUnavail, AD_TAMechUnavail = @AD_TAMechUnavail,
	BN_KEDC = @BN_KEDC , BN_MechAvail = @BN_MechAvail, BN_AdjMaintIndex = @BN_AdjMaintIndex, 
	BN_UtilPcnt = @BN_UtilPcnt, BN_NEOpExEDC = @BN_NEOpExEDC, BN_EII = @BN_EII, BN_TotWHrEDC = @BN_TotWHrEDC, 
	BN_NetInputKBPD = @BN_NetInputKBPD, BN_CrudeKBPD = @BN_CrudeKBPD, BN_FCCRateKBPD = @BN_FCCRateKBPD,  
	BN_NumContPers = @BN_NumContPers, BN_RoutMechUnavail = @BN_RoutMechUnavail, BN_TAMechUnavail = @BN_TAMechUnavail,
	CC_KEDC = @CC_KEDC , CC_MechAvail = @CC_MechAvail, CC_AdjMaintIndex = @CC_AdjMaintIndex, 
	CC_UtilPcnt = @CC_UtilPcnt, CC_NEOpExEDC = @CC_NEOpExEDC, CC_EII = @CC_EII, CC_TotWHrEDC = @CC_TotWHrEDC, 
	CC_NetInputKBPD = @CC_NetInputKBPD, CC_CrudeKBPD = @CC_CrudeKBPD, CC_FCCRateKBPD = @CC_FCCRateKBPD,  
	CC_NumContPers = @CC_NumContPers, CC_RoutMechUnavail = @CC_RoutMechUnavail, CC_TAMechUnavail = @CC_TAMechUnavail,
	HO_KEDC = @HO_KEDC , HO_MechAvail = @HO_MechAvail, HO_AdjMaintIndex = @HO_AdjMaintIndex, 
	HO_UtilPcnt = @HO_UtilPcnt, HO_NEOpExEDC = @HO_NEOpExEDC, HO_EII = @HO_EII, HO_TotWHrEDC = @HO_TotWHrEDC, 
	HO_NetInputKBPD = @HO_NetInputKBPD, HO_CrudeKBPD = @HO_CrudeKBPD, HO_FCCRateKBPD = @HO_FCCRateKBPD,  
	HO_NumContPers = @HO_NumContPers, HO_RoutMechUnavail = @HO_RoutMechUnavail, HO_TAMechUnavail = @HO_TAMechUnavail,
	MK_KEDC = @MK_KEDC , MK_MechAvail = @MK_MechAvail, MK_AdjMaintIndex = @MK_AdjMaintIndex, 
	MK_UtilPcnt = @MK_UtilPcnt, MK_NEOpExEDC = @MK_NEOpExEDC, MK_EII = @MK_EII, MK_TotWHrEDC = @MK_TotWHrEDC, 
	MK_NetInputKBPD = @MK_NetInputKBPD, MK_CrudeKBPD = @MK_CrudeKBPD, MK_FCCRateKBPD = @MK_FCCRateKBPD,  
	MK_NumContPers = @MK_NumContPers, MK_RoutMechUnavail = @MK_RoutMechUnavail, MK_TAMechUnavail = @MK_TAMechUnavail,
	MP_KEDC = @MP_KEDC , MP_MechAvail = @MP_MechAvail, MP_AdjMaintIndex = @MP_AdjMaintIndex, 
	MP_UtilPcnt = @MP_UtilPcnt, MP_NEOpExEDC = @MP_NEOpExEDC, MP_EII = @MP_EII, MP_TotWHrEDC = @MP_TotWHrEDC, 
	MP_NetInputKBPD = @MP_NetInputKBPD, MP_CrudeKBPD = @MP_CrudeKBPD, MP_FCCRateKBPD = @MP_FCCRateKBPD,  
	MP_NumContPers = @MP_NumContPers, MP_RoutMechUnavail = @MP_RoutMechUnavail, MP_TAMechUnavail = @MP_TAMechUnavail,
	PA_KEDC = @PA_KEDC , PA_MechAvail = @PA_MechAvail, PA_AdjMaintIndex = @PA_AdjMaintIndex, 
	PA_UtilPcnt = @PA_UtilPcnt, PA_NEOpExEDC = @PA_NEOpExEDC, PA_EII = @PA_EII, PA_TotWHrEDC = @PA_TotWHrEDC, 
	PA_NetInputKBPD = @PA_NetInputKBPD, PA_CrudeKBPD = @PA_CrudeKBPD, PA_FCCRateKBPD = @PA_FCCRateKBPD,  
	PA_NumContPers = @PA_NumContPers, PA_RoutMechUnavail = @PA_RoutMechUnavail, PA_TAMechUnavail = @PA_TAMechUnavail,
	QB_KEDC = @QB_KEDC , QB_MechAvail = @QB_MechAvail, QB_AdjMaintIndex = @QB_AdjMaintIndex, 
	QB_UtilPcnt = @QB_UtilPcnt, QB_NEOpExEDC = @QB_NEOpExEDC, QB_EII = @QB_EII, QB_TotWHrEDC = @QB_TotWHrEDC, 
	QB_NetInputKBPD = @QB_NetInputKBPD, QB_CrudeKBPD = @QB_CrudeKBPD, QB_FCCRateKBPD = @QB_FCCRateKBPD,  
	QB_NumContPers = @QB_NumContPers, QB_RoutMechUnavail = @QB_RoutMechUnavail, QB_TAMechUnavail = @QB_TAMechUnavail,
	SC_KEDC = @SC_KEDC , SC_MechAvail = @SC_MechAvail, SC_AdjMaintIndex = @SC_AdjMaintIndex, 
	SC_UtilPcnt = @SC_UtilPcnt, SC_NEOpExEDC = @SC_NEOpExEDC, SC_EII = @SC_EII, SC_TotWHrEDC = @SC_TotWHrEDC, 
	SC_NetInputKBPD = @SC_NetInputKBPD, SC_CrudeKBPD = @SC_CrudeKBPD, SC_FCCRateKBPD = @SC_FCCRateKBPD,  
	SC_NumContPers = @SC_NumContPers, SC_RoutMechUnavail = @SC_RoutMechUnavail, SC_TAMechUnavail = @SC_TAMechUnavail,
	TC_KEDC = @TC_KEDC , TC_MechAvail = @TC_MechAvail, TC_AdjMaintIndex = @TC_AdjMaintIndex, 
	TC_UtilPcnt = @TC_UtilPcnt, TC_NEOpExEDC = @TC_NEOpExEDC, TC_EII = @TC_EII, TC_TotWHrEDC = @TC_TotWHrEDC, 
	TC_NetInputKBPD = @TC_NetInputKBPD, TC_CrudeKBPD = @TC_CrudeKBPD, TC_FCCRateKBPD = @TC_FCCRateKBPD,  
	TC_NumContPers = @TC_NumContPers, TC_RoutMechUnavail = @TC_RoutMechUnavail, TC_TAMechUnavail = @TC_TAMechUnavail,
	TR_KEDC = @TR_KEDC , TR_MechAvail = @TR_MechAvail, TR_AdjMaintIndex = @TR_AdjMaintIndex, 
	TR_UtilPcnt = @TR_UtilPcnt, TR_NEOpExEDC = @TR_NEOpExEDC, TR_EII = @TR_EII, TR_TotWHrEDC = @TR_TotWHrEDC, 
	TR_NetInputKBPD = @TR_NetInputKBPD, TR_CrudeKBPD = @TR_CrudeKBPD, TR_FCCRateKBPD = @TR_FCCRateKBPD,  
	TR_NumContPers = @TR_NumContPers, TR_RoutMechUnavail = @TR_RoutMechUnavail, TR_TAMechUnavail = @TR_TAMechUnavail,
	WM_KEDC = @WM_KEDC , WM_MechAvail = @WM_MechAvail, WM_AdjMaintIndex = @WM_AdjMaintIndex, 
	WM_UtilPcnt = @WM_UtilPcnt, WM_NEOpExEDC = @WM_NEOpExEDC, WM_EII = @WM_EII, WM_TotWHrEDC = @WM_TotWHrEDC, 
	WM_NetInputKBPD = @WM_NetInputKBPD, WM_CrudeKBPD = @WM_CrudeKBPD, WM_FCCRateKBPD = @WM_FCCRateKBPD,  
	WM_NumContPers = @WM_NumContPers, WM_RoutMechUnavail = @WM_RoutMechUnavail, WM_TAMechUnavail = @WM_TAMechUnavail,
	VALERO_MEDC = @VALERO_KEDC/1000, VALERO_MechAvail = @VALERO_MechAvail, VALERO_AdjMaintIndex = @VALERO_AdjMaintIndex, 
	VALERO_UtilPcnt = @VALERO_UtilPcnt, VALERO_NEOpExEDC = @VALERO_NEOpExEDC, VALERO_EII = @VALERO_EII, VALERO_TotWHrEDC = @VALERO_TotWHrEDC,
	VALERO_NetInputKBPD = @VALERO_NetInputKBPD, VALERO_CrudeKBPD = @VALERO_CrudeKBPD, VALERO_FCCRateKBPD = @VALERO_FCCRateKBPD, 
	VALERO_NumContPers = @VALERO_NumContPers, VALERO_RoutMechUnavail = @VALERO_RoutMechUnavail, VALERO_TAMechUnavail = @VALERO_TAMechUnavail

