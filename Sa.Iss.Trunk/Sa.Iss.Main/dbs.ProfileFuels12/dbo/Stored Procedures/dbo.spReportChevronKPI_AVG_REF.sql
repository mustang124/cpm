﻿
CREATE   PROC [dbo].[spReportChevronKPI_Avg_REF] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1,
	@UtilPcnt_Avg real = NULL OUTPUT, @KEDC_Avg real = NULL OUTPUT, @KUEDC_Avg real = NULL OUTPUT, 
	@MechAvail_Avg real = NULL OUTPUT, @KMAEDC_Avg real = NULL OUTPUT, @KProcEDC_Avg real = NULL OUTPUT,
	@EII_Avg real = NULL OUTPUT, @KEnergyUseDay_Avg real = NULL OUTPUT, @KTotStdEnergy_Avg real = NULL OUTPUT,
	@MaintIndex_Avg real = NULL OUTPUT, @TAIndex_Avg real = NULL OUTPUT, @RoutIndex_Avg real = NULL OUTPUT,   
	@TotEqPEDC_Avg real = NULL OUTPUT, 
	@NEOpExUEDC_Avg real = NULL OUTPUT, @TotCashOpExUEDC_Avg real = NULL OUTPUT,
	@CashMargin_Avg real = NULL OUTPUT, @NetInputBPD_Avg real = NULL OUTPUT)

AS
SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodEnd smalldatetime
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodEnd = PeriodEnd
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = 'Actual' AND UseSubmission = 1

SELECT	@UtilPcnt_Avg = NULL, @KEDC_Avg = NULL, @KUEDC_Avg = NULL, 
	@MechAvail_Avg = NULL, @KMAEDC_Avg = NULL, @KProcEDC_Avg = NULL,
	@EII_Avg = NULL, @KEnergyUseDay_Avg = NULL, @KTotStdEnergy_Avg = NULL,
	@MaintIndex_Avg = NULL, @TAIndex_Avg = NULL, @RoutIndex_Avg = NULL,   
	@TotEqPEDC_Avg = NULL, 
	@NEOpExUEDC_Avg = NULL, @TotCashOpExUEDC_Avg = NULL,
	@CashMargin_Avg = NULL, @NetInputBPD_Avg = NULL
IF @SubmissionID IS NULL
	RETURN 0

SELECT	@UtilPcnt_Avg = UtilPcnt_Avg, @KEDC_Avg = EDC_Avg/1000, @KUEDC_Avg = UEDC_Avg/1000, 
	@MechAvail_Avg = MechAvail_Avg, 
	@EII_Avg = EII_Avg,
	@MaintIndex_Avg = MaintIndex_Avg, @TAIndex_Avg = TAIndex_Avg, @RoutIndex_Avg = RoutIndex_Avg,   
	@TotEqPEDC_Avg = TotEqPEDC_Avg, 
	@NEOpExUEDC_Avg = NEOpExUEDC_Avg, @TotCashOpExUEDC_Avg = TotCashOpExUEDC_Avg,
	@CashMargin_Avg = CashMargin_Avg, @NetInputBPD_Avg = NetInputBPD_Avg
FROM GenSum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT 
@KProcEDC_Avg = SUM(TotProcessEDC/1000.0*s.NumDays)/SUM(s.NumDays),
@KEnergyUseDay_Avg = SUM(EnergyUseDay/1000.0*s.NumDays)/SUM(s.NumDays),
@KTotStdEnergy_Avg = SUM(TotStdEnergy/1000.0*s.NumDays)/SUM(s.NumDays)
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= DATEADD(mm, -12, @PeriodEnd) AND s.PeriodStart < @PeriodEnd
AND a.FactorSet = @FactorSet AND s.UseSubmission = 1


SELECT @KMAEDC_Avg = @KProcEDC_Avg*@MechAvail_Avg/100.0






