﻿CREATE PROC [dbo].[SS_GetInputCrude]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            RTRIM(CNum) as CNum,RTRIM(CrudeName) AS CrudeName,Gravity,Sulfur,Bbl,CostPerBbl 
            FROM  
            dbo.Crude c 
            ,dbo.Submissions s  
            WHERE   
            c.SubmissionID = s.SubmissionID AND 
            (c.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions WHERE RefineryID = @RefineryID and DataSet = @DataSet and UseSubmission=1 ))

