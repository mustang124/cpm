﻿CREATE PROC [dbo].[SS_GetInputProcessData]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT  
s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            pd.UnitID,pd.Property,pd.RptValue,t.SortKey AS SortKey, RTRIM(cfg.ProcessID) AS ProcessID, RTRIM(cfg.UnitName) AS UnitName 
            FROM dbo.ProcessData pd, Config cfg, Table2_LU  t    
            ,dbo.Submissions s  
            WHERE    
            pd.SubmissionID = s.SubmissionID AND 
            cfg.UnitID=pd.UnitID AND  
            cfg.SubmissionID = pd.SubmissionID AND t.ProcessID=cfg.ProcessID AND t.Property=pd.Property AND (pd.SubmissionID IN  
            (SELECT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1))

