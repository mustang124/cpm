﻿CREATE PROCEDURE [dbo].[CrudeGravityAdj] 
	(@StudyYear StudyYear, @APIScale tinyint, 
	@CrudeAPI real, @PostedAPI real, @APIAdj smallmoney OUTPUT, @Details bit = 0)
AS
DECLARE @tblAPI table(APIScale tinyint, APILevel tinyint, 
	MinAPI real, MaxAPI real, APIAdj real)
INSERT INTO @tblAPI
SELECT APIScale, APILevel, MinAPI, MaxAPI, 
APIAdj = CASE 
    /*Gravity>=MinAPI, Gravity<=MaxAPI */
	WHEN (@CrudeAPI>=S.MinAPI And @CrudeAPI<=S.MaxAPI) THEN
		/* AND PostedAPI>MaxAPI*/
		CASE WHEN (@PostedAPI>S.MaxAPI) THEN
			(@CrudeAPI-S.MaxAPI)*S.Adj
	    	/* AND PostedAPI<=MinAPI */
		WHEN (@PostedAPI<=S.MinAPI) THEN
			(@CrudeAPI-S.MinAPI)*S.Adj
		/* AND PostedAPI>MinAPI, PostedAPI<=MaxAPI */
		WHEN (@PostedAPI>S.MinAPI AND @PostedAPI<=S.MaxAPI) THEN
			(@CrudeAPI-@PostedAPI)*S.Adj
		END
    /* Gravity<MinAPI */
	WHEN (@CrudeAPI<S.MinAPI) THEN
		/* AND PostedAPI>=MinAPI, PostedAPI<=MaxAPI */
		CASE WHEN (@PostedAPI>=S.MinAPI AND @PostedAPI<=S.MaxAPI) THEN
			(S.MinAPI-@PostedAPI)*S.Adj
		/* PostedAPI>MaxAPI */
		WHEN (@PostedAPI>S.MaxAPI) THEN
			(S.MinAPI-S.MaxAPI)*S.Adj
		END
    /* Gravity>MaxAPI */
	WHEN (@CrudeAPI>S.MaxAPI) THEN
		/* AND PostedAPI>MinAPI, PostedAPI<=MaxAPI */
		CASE WHEN (@PostedAPI>S.MinAPI AND @PostedAPI<=MaxAPI) THEN
			(S.MaxAPI-@PostedAPI)*S.Adj
		/* AND PostedAPI<=MinAPI */
		WHEN (@PostedAPI<=S.MinAPI) THEN
			(S.MaxAPI-S.MinAPI)*S.Adj
		END
	END
FROM APIScale S
WHERE S.StudyYear = @StudyYear AND S.APIScale = @APIScale 
SELECT @APIAdj = SUM(APIAdj)
FROM @tblAPI
IF @Details = 1
BEGIN
	DECLARE @strPrint varchar(255)
	SELECT @strPrint = 'In ' + CONVERT(varchar(4), @StudyYear) 
			+ ', a Crude with an API of ' +
			+ CONVERT(varchar(5), @CrudeAPI)
			+ ' priced based on API scale ' + 
			+ CONVERT(varchar(3), @APIScale)
			+ ' with a Posted API of ' 
			+ CONVERT(varchar(5), @PostedAPI)
			+ ' would get an adjustment of '
			+ CONVERT(varchar(5), @APIAdj) + '.'
	PRINT @strPrint
	PRINT ''
	SELECT * FROM @tblAPI
END
