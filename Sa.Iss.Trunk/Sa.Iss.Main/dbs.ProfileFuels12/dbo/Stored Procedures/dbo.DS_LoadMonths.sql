﻿CREATE PROCEDURE [dbo].[DS_LoadMonths]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS
BEGIN
	SELECT PeriodYear, PeriodMonth 
	FROM dbo.Submissions 
	WHERE RefineryID = @RefineryID  and DataSet = @DataSet and UseSubmission=1
    ORDER BY PeriodStart DESC
END

