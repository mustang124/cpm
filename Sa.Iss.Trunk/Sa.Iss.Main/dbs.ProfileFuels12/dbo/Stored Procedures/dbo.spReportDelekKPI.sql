﻿CREATE   PROC [dbo].[spReportDelekKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @StartYTD smalldatetime, @Start12Mo smalldatetime, @Start24Mo smalldatetime
SELECT @StartYTD = p.StartYTD, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)

SELECT 
	EII = m.EII, EII_YTD = avgYTD.EII, EII_Avg = avg12mo.EII, 
	EnergyUseDay = m.EnergyUseDay, EnergyUseDay_YTD = avgYTD.EnergyUseDay, EnergyUseDay_Avg = avg12mo.EnergyUseDay, 
	TotStdEnergy = m.TotStdEnergy, TotStdEnergy_YTD = avgYTD.TotStdEnergy, TotStdEnergy_Avg = avg12mo.TotStdEnergy, 

	UtilPcnt = m.RefUtilPcnt, UtilPcnt_YTD = avgYTD.RefUtilPcnt, UtilPcnt_Avg = avg12mo.RefUtilPcnt, 
	EDC = m.EDC/1000, EDC_YTD = avgYTD.EDC/1000, EDC_Avg = avg12mo.EDC/1000, 
	UtilUEDC = m.UtilUEDC/1000, UtilUEDC_YTD = avgYTD.UtilUEDC/1000, UtilUEDC_Avg = avg12mo.UtilUEDC/1000, 
	
	ProcessEffIndex = m.ProcessEffIndex, ProcessEffIndex_YTD = avgYTD.ProcessEffIndex, ProcessEffIndex_Avg = avg12mo.ProcessEffIndex, 
	VEI = m.VEI, VEI_YTD = avgYTD.VEI, VEI_Avg = avg12mo.VEI, 
	NetInputBPD = m.NetInputBPD, NetInputBPD_YTD = avgYTD.NetInputBPD, NetInputBPD_Avg = avg12mo.NetInputBPD,
	ReportLossGain = m.ReportLossGain, ReportLossGain_YTD = avgYTD.ReportLossGain, ReportLossGain_Avg = avg12mo.ReportLossGain, 
	EstGain = m.EstGain, EstGain_YTD = avgYTD.EstGain, EstGain_Avg = avg12mo.EstGain, 

	OpAvail = m.OpAvail, OpAvail_YTD = avgYTD.OpAvail, OpAvail_Avg = avg24mo.OpAvail, 
	MechUnavailTA = m.MechUnavailTA, MechUnavailTA_YTD = avgYTD.MechUnavailTA, MechUnavailTA_Avg = avg24mo.MechUnavailTA, 
	NonTAUnavail = m.MechUnavailRout + m.RegUnavail, NonTAUnavail_YTD = avgYTD.MechUnavailRout + avgYTD.RegUnavail, NonTAUnavail_Avg = avg24mo.MechUnavailRout + avg24mo.RegUnavail, 

	TotWHrEDC = m.PersIndex, TotWHrEDC_YTD = avgYTD.PersIndex, TotWHrEDC_Avg = avg12mo.PersIndex, 
	AnnTAWHr = m.MaintTAWHr_k, AnnTAWHr_YTD = avgYTD.MaintTAWHr_k, AnnTAWHr_Avg = avg12mo.MaintTAWHr_k,
	NonTAWHr = m.TotNonTAWHr_k, NonTAWHr_YTD = avgYTD.TotNonTAWHr_k, NonTAWHr_Avg = avg12mo.TotNonTAWHr_k, 

	MaintIndex = m.MaintIndex, MaintIndex_YTD = avgYTD.MaintIndex, MaintIndex_Avg = avg24mo.MaintIndex, 
	TAAdj = m.AnnTACost, TAAdj_YTD = avgYTD.AnnTACost, TAAdj_Avg = mi24.TAEffIndex*avg24mo.MaintEffDiv/100/1000,
	RoutCost = m.RoutCost, RoutCost_YTD = avgYTD.RoutCost, RoutCost_Avg = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1000, 

	NEOpExEDC = m.NEOpExEDC, NEOpExEDC_YTD = avgYTD.NEOpExEDC, NEOpExEDC_Avg = avg12mo.NEOpExEDC, 
	NEOpEx = m.NEOpEx, NEOpEx_YTD = avgYTD.NEOpEx, NEOpEx_Avg = avg12mo.NEOpEx, 

	TotCashOpExUEDC = m.OpExUEDC, TotCashOpExUEDC_YTD = avgYTD.OpExUEDC, TotCashOpExUEDC_Avg = avg12mo.OpExUEDC, 
	EnergyCost = m.EnergyCost, EnergyCost_YTD = avgYTD.EnergyCost, EnergyCost_Avg = avg12mo.EnergyCost, 
	TotCashOpEx = m.TotCashOpEx, TotCashOpEx_YTD = avgYTD.TotCashOpEx, TotCashOpEx_Avg = avg12mo.TotCashOpEx, 
	UEDC = m.UEDC/1000, UEDC_YTD = avgYTD.UEDC/1000, UEDC_Avg = avg12mo.UEDC/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM) avgYTD ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1
