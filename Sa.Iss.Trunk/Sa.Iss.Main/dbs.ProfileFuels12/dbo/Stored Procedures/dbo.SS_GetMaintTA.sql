﻿CREATE PROC [dbo].[SS_GetMaintTA]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@DataSet nvarchar(20)='ACTUAL'
	
AS

SELECT mt.TAID,mt.UnitID,RTRIM(cfg.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown,
            mt.TACostLocal,mt.TAExpLocal, mt.TACptlLocal, mt.TAOvhdLocal,mt.TALaborCostLocal, mt.TAOCCSTH,
            mt.TAOCCOVT,mt.TAMPSSTH,mt.TAMPSOVTPcnt,
            cfg.SortKey,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate,
            mt.TAExceptions,RTRIM(cfg.UnitName) AS UnitName 
            FROM dbo.MaintTA mt, Config cfg WHERE  mt.RefineryID=@RefineryID
             AND mt.UnitID = cfg.UnitID AND cfg.SubmissionID IN 
             (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1
           AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd)))

