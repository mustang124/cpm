﻿CREATE PROC [dbo].[SS_GetInputSettings]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

	SELECT 
             s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
             RTRIM(s.Company) AS Company,RTRIM(s.Location) AS Location, 
             RTRIM(CoordName) AS CoordName,RTRIM(CoordTitle) AS CoordTitle, 
             RTRIM(CoordPhone) AS CoordPhone,RTRIM(CoordEMail) AS CoordEMail, 
             RTRIM(RptCurrency) AS RptCurrency,RTRIM(RptCurrencyT15) AS RptCurrencyT15,(CASE RTRIM(UOM) 
             WHEN 'US' THEN 'US Units' WHEN 'MET' THEN 'Metric' END) AS UOM, 
             ' ' AS BridgeLocation,FuelsLubesCombo, 
             PeriodMonth,PeriodYear, 
             RTRIM(DataSet) AS DataSet, BridgeVersion,ClientVersion 
             FROM dbo.Submissions s,TSort t 
             WHERE s.RefineryID=@RefineryID AND s.RefineryID=t.RefineryID and s.DataSet = @DataSet and UseSubmission=1

