﻿CREATE PROCEDURE [dbo].[DS_UserDefinedSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(u.HeaderText) AS HeaderText,RTRIM(u.VariableDesc) AS VariableDesc, 0.0 AS RptValue, 0.0 AS RptValue_Target, 0.0 AS RptValue_Avg, 0.0 AS RptValue_YTD, 0 AS DecPlaces FROM UserDefined u 
        WHERE u.SubmissionID =(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID ORDER BY PeriodStart DESC)
END

