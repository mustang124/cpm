﻿CREATE      PROC [dbo].[spInitialCalcs](@SubmissionID int)
AS

SET NOCOUNT ON

INSERT INTO CurrenciesToCalc (RefineryID, Currency)
SELECT RefineryID, RptCurrency 
FROM SubmissionsAll s
WHERE NOT EXISTS (SELECT * FROM CurrenciesToCalc c WHERE c.RefineryID = s.RefineryID AND c.Currency = s.RptCurrency)
AND SubmissionID = @SubmissionID

INSERT INTO CurrenciesToCalc (RefineryID, Currency)
SELECT RefineryID, RptCurrencyT15
FROM SubmissionsAll s
WHERE NOT EXISTS (SELECT * FROM CurrenciesToCalc c WHERE c.RefineryID = s.RefineryID AND c.Currency = s.RptCurrencyT15)
AND SubmissionID = @SubmissionID

UPDATE Config
SET SortKey = ProcessID_LU.SortKey
FROM Config INNER JOIN ProcessID_LU ON ProcessID_LU.ProcessID = Config.ProcessID
WHERE Config.SubmissionID = @SubmissionID

UPDATE Config
SET BlockOp = NULL
WHERE SubmissionID = @SubmissionID AND BlockOp = ''

EXEC spSetUOM @SubmissionID
EXEC spLoadFactorData @SubmissionID
EXEC spEDCStabilizers @SubmissionID


