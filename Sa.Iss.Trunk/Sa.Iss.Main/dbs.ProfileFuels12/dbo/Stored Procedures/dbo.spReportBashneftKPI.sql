﻿
CREATE   PROC [dbo].[spReportBashneftKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
BEGIN
	--RAISERROR (N'Data has not been uploaded for this month.', -- Message text.
 --          10, -- Severity,
 --          1 --State
 --          );
	RETURN 1
END
ELSE IF @CalcsNeeded IS NOT NULL
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE	
	@EII real, @EII_Avg real, @EII_QTR real, 
	@EnergyUseDay real, @EnergyUseDay_Avg real, @EnergyUseDay_QTR real, 
	@FuelUseDay real, @FuelUseDay_Avg real, @FuelUseDay_QTR real, 
	@TotStdEnergy real, @TotStdEnergy_Avg real, @TotStdEnergy_QTR real, 

	@ProcessUtilPcnt real, @ProcessUtilPcnt_Avg real, @ProcessUtilPcnt_QTR real, 
	@TotProcessEDC real, @TotProcessEDC_Avg real, @TotProcessEDC_QTR real, 
	@TotProcessUEDC real, @TotProcessUEDC_Avg real, @TotProcessUEDC_QTR real, 
	@OffsitesEDC real, @OffsitesEDC_Avg real, @OffsitesEDC_QTR real, 
	@Complexity real, @Complexity_Avg real, @Complexity_QTR real, 

	@VEI real, @VEI_Avg real, @VEI_QTR real, 
	@ReportLossGain real, @ReportLossGain_Avg real, @ReportLossGain_QTR real, 
	@EstGain real, @EstGain_Avg real, @EstGain_QTR real, 
	
	@OpAvail real, @OpAvail_Avg real, @OpAvail_QTR real, 
	@MechAvail real, @MechAvail_Avg real, @MechAvail_QTR real, 
	@MechUnavailTA real, @MechUnavailTA_Avg real, @MechUnavailTA_QTR real, 
	@NonTAMechUnavail real, @NonTAMechUnavail_Avg real, @NonTAMechUnavail_QTR real, 
	@NonTAUnavail real, @NonTAUnavail_Avg real, @NonTAUnavail_QTR real, 
	@RegUnavail real, @RegUnavail_Avg real, @RegUnavail_QTR real, 
	@TADD real, @TADD_Avg real, @TADD_QTR real,
	@NTAMDD real, @NTAMDD_Avg real, @NTAMDD_QTR real,
	@RPDD real, @RPDD_Avg real, @RPDD_QTR real,
	@NumDays_Avg real, @NumDays_QTR real,
	
	@MaintEffIndex real, @MaintEffIndex_Avg real, @MaintEffIndex_QTR real, 
	@TAAdj real, @TAAdj_Avg real, @TAAdj_QTR real, @TAEffIndex_Avg real,
	@AnnTACost real, @AnnTACost_Avg real, @AnnTACost_QTR real, 
	@RoutCost real, @RoutCost_Avg real, @RoutCost_QTR real, 
	@MaintEffDiv real, @MaintEffDiv_Avg real, @MaintEffDiv_QTR real, 
	@EDC real, @EDC_Avg real, @EDC_QTR real, 

	@nmPersEffIndex real, @nmPersEffIndex_Avg real, @nmPersEffIndex_QTR real,
	@NonMaintWHr real, @NonMaintWHr_Avg real, @NonMaintWHr_QTR real,
	@OCCNonMaintWHr real, @OCCNonMaintWHr_Avg real, @OCCNonMaintWHr_QTR real,
	@MPSNonMaintWHr real, @MPSNonMaintWHr_Avg real, @MPSNonMaintWHr_QTR real,
	@nmPersEffDiv real, @nmPersEffDiv_Avg real, @nmPersEffDiv_QTR real,

	@NEOpExEDC real, @NEOpExEDC_Avg real, @NEOpExEDC_QTR real, 
	@NEOpEx real, @NEOpEx_Avg real, @NEOpEx_QTR real, 

	@OpExUEDC real, @OpExUEDC_Avg real, @OpExUEDC_QTR real,
	@EnergyCost real, @EnergyCost_Avg real, @EnergyCost_QTR real, 
	@TotCashOpEx real, @TotCashOpEx_Avg real, @TotCashOpEx_QTR real,
	@UEDC real, @UEDC_Avg real, @UEDC_QTR real
DECLARE @ExchRate real, @ExchRate_Avg real, @ExchRate_QTR real

SELECT @EII = NULL, @EII_Avg = NULL, @EII_QTR = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_Avg = NULL, @EnergyUseDay_QTR = NULL, 
	@FuelUseDay = NULL, @FuelUseDay_Avg = NULL, @FuelUseDay_QTR = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_Avg = NULL, @TotStdEnergy_QTR = NULL, 
	@ProcessUtilPcnt = NULL, @ProcessUtilPcnt_Avg = NULL, @ProcessUtilPcnt_QTR = NULL, 
	@TotProcessEDC = NULL, @TotProcessEDC_Avg = NULL, @TotProcessEDC_QTR = NULL, 
	@TotProcessUEDC = NULL, @TotProcessUEDC_Avg = NULL, @TotProcessUEDC_QTR = NULL, 
	@Complexity = NULL, @Complexity_Avg = NULL, @Complexity_QTR = NULL, 
	@EDC = NULL, @EDC_Avg = NULL, @EDC_QTR = NULL, 
	@UEDC = NULL, @UEDC_Avg = NULL, @UEDC_QTR = NULL, 
	@VEI = NULL, @VEI_Avg = NULL, @VEI_QTR = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_Avg = NULL, @ReportLossGain_QTR = NULL, 
	@EstGain = NULL, @EstGain_Avg = NULL, @EstGain_QTR = NULL, 
	@OpAvail = NULL, @OpAvail_Avg = NULL, @OpAvail_QTR = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_Avg = NULL, @MechUnavailTA_QTR = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_Avg = NULL, @NonTAUnavail_QTR = NULL, 
	@RoutCost = NULL, @RoutCost_Avg = NULL, @RoutCost_QTR = NULL, 
	@nmPersEffIndex = NULL, @nmPersEffIndex_Avg = NULL, @nmPersEffIndex_QTR = NULL, 
	@NonMaintWHr = NULL, @NonMaintWHr_Avg = NULL, @NonMaintWHr_QTR = NULL,
	@OCCNonMaintWHr = NULL, @OCCNonMaintWHr_Avg = NULL, @OCCNonMaintWHr_QTR = NULL,
	@MPSNonMaintWHr = NULL, @MPSNonMaintWHr_Avg = NULL, @MPSNonMaintWHr_QTR = NULL,
	@nmPersEffDiv = NULL, @nmPersEffDiv_Avg = NULL, @nmPersEffDiv_QTR = NULL,
	@NEOpExEDC = NULL, @NEOpExEDC_Avg = NULL, @NEOpExEDC_QTR = NULL, 
	@NEOpEx = NULL, @NEOpEx_Avg = NULL, @NEOpEx_QTR = NULL, 
	@OpExUEDC = NULL, @OpExUEDC_Avg = NULL, @OpExUEDC_QTR = NULL, 
	@TAAdj = NULL, @TAAdj_Avg = NULL, @TAAdj_QTR = NULL, @TAEffIndex_Avg = NULL,
	@EnergyCost = NULL, @EnergyCost_Avg = NULL, @EnergyCost_QTR = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_Avg = NULL, @TotCashOpEx_QTR = NULL


--- Everything Already Available in GenSum
SELECT	@ProcessUtilPcnt = ProcessUtilPcnt, @ProcessUtilPcnt_Avg = ProcessUtilPcnt_Avg,
	@OpAvail = OpAvail, @OpAvail_Avg = OpAvail_Avg,
	@MechAvail = MechAvail, @MechAvail_Avg = MechAvail_Avg,
	@EII = EII, @EII_Avg = EII_Avg, @VEI = VEI, @VEI_Avg = VEI_Avg, 
	@nmPersEffIndex = NonMaintPEI, @nmPersEffIndex_Avg = NonMaintPEI_Avg, 
	@EDC = EDC/1000, @EDC_Avg = EDC_Avg/1000, @UEDC = UEDC/1000, @UEDC_Avg = UEDC_Avg/1000,
	@NEOpExEDC = NEOpExEDC, @NEOpExEDC_Avg = NEOpExEDC_Avg
FROM GenSum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_QTR = MechUnavailTA_Ann, @MechUnavailTA_Avg = MechUnavailTA_Ann,
	@NonTAUnavail = 100 - OpAvail_Ann - MechUnavailTA_Ann, 	@NonTAMechUnavail = 100 - MechAvail_Ann - MechUnavailTA_Ann, @RegUnavail = MechAvail_Ann - OpAvail_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @NonTAUnavail_QTR = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NonTAMechUnavail_QTR = SUM((100 - MechAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @RegUnavail_QTR = SUM((MechAvail_Ann - OpAvail_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NumDays_QTR = SUM(s.NumDays)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail_Avg = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NonTAMechUnavail_Avg = SUM((100 - MechAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @RegUnavail_Avg = SUM((MechAvail_Ann - OpAvail_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NumDays_Avg = SUM(s.NumDays)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

IF @NonTAUnavail < 0.05
	SELECT @NonTAUnavail = 0, @NonTAMechUnavail = 0, @RegUnavail = 0
IF @NonTAUnavail_QTR < 0.05
	SELECT @NonTAUnavail_QTR = 0, @NonTAMechUnavail_QTR = 0, @RegUnavail_QTR = 0
IF @NonTAUnavail_Avg < 0.05
	SELECT @NonTAUnavail_Avg = 0, @NonTAMechUnavail_Avg = 0, @RegUnavail_Avg = 0
	
SELECT @OpAvail = 100 - @NonTAUnavail - @MechUnavailTA, 
		@OpAvail_QTR = 100 - @NonTAUnavail_QTR - @MechUnavailTA, 
		@OpAvail_Avg = 100 - @NonTAUnavail_Avg - @MechUnavailTA

SELECT @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays,
	@nmPersEffDiv = f.NonMaintPersEffDiv*s.FractionOfYear,
	@MaintEffDiv = f.MaintEffDiv*s.FractionOfYear,
	@TotProcessEDC = TotProcessEDC/1000, @TotProcessUEDC = TotProcessUEDC/1000,
	@Complexity = Complexity
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

SELECT 	@Complexity_Avg = Complexity, @TotProcessEDC_Avg = AvgProcessEDC/1000, @TotProcessUEDC_Avg = AvgProcessUEDC/1000,
		@EnergyUseDay_Avg = EnergyUseDay, @TotStdEnergy_Avg = AvgStdEnergy, 
		@ReportLossGain_Avg = LossGainBpD, @EstGain_Avg = AvgStdGainBpD, 
		@nmPersEffDiv_Avg = NonMaintPersEffDiv, @MaintEffDiv_Avg = MaintEffDiv
FROM dbo.CalcAverageFactors(@RefineryID, @DataSet, @FactorSet, @Start12Mo, @PeriodEnd)

SELECT 	@EII_QTR = EII, @VEI_QTR = VEI, @EDC_QTR = AvgEDC/1000, @UEDC_QTR = AvgUEDC/1000, @Complexity_QTR = Complexity,
		@ProcessUtilPcnt_QTR = ProcessUtilPcnt, @TotProcessEDC_QTR = AvgProcessEDC/1000, @TotProcessUEDC_QTR = AvgProcessUEDC/1000,
		@EnergyUseDay_QTR = EnergyUseDay, @TotStdEnergy_QTR = AvgStdEnergy, @ReportLossGain_QTR = LossGainBpD, @EstGain_QTR = AvgStdGainBpD, 
		@nmPersEffDiv_QTR = NonMaintPersEffDiv, @MaintEffDiv_QTR = MaintEffDiv
FROM dbo.CalcAverageFactors(@RefineryID, @DataSet, @FactorSet, @Start3Mo, @PeriodEnd)
/*
SELECT @MaintEffDiv_QTR = SUM(MaintEffDiv*s.FractionOfYear),
	@nmPersEffDiv_QTR = SUM(NonMaintPersEffDiv*s.FractionOfYear)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet
SELECT 	@MaintEffDiv_Avg = SUM(MaintEffDiv*s.FractionOfYear)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
INNER JOIN MaintIndex i On i.SubmissionID = s.SubmissionID AND i.Currency = @Currency AND i.FactorSet = f.FactorSet
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet
*/
SELECT @FuelUseDay_Avg = SUM(PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU)/SUM(s.NumDays*1.0)
	, @FuelUseDay = SUM(CASE WHEN s.SubmissionID = @SubmissionID THEN PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU ELSE 0 END)/SUM(CASE WHEN s.SubmissionID = @SubmissionID THEN s.NumDays*1.0 END)
	, @FuelUseDay_QTR = SUM(CASE WHEN s.PeriodStart >= @Start3Mo THEN PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU ELSE 0 END)/SUM(CASE WHEN s.PeriodStart >= @Start3Mo THEN s.NumDays*1.0 END)
FROM EnergyTot e INNER JOIN Submissions s ON s.SubmissionID = e.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1


SELECT	@MaintEffIndex = RoutEffIndex + TAEffIndex_Avg, @TAEffIndex_Avg = TAEffIndex_Avg,
		@MaintEffIndex_Avg = MaintEffIndex_Avg
FROM MaintIndex
WHERE SubmissionID = @SubmissionID AND Currency = @Currency AND FactorSet = @FactorSet AND FactorSet = @FactorSet

SELECT	@MaintEffIndex_QTR = SUM(i.RoutEffIndex*f.MaintEffDiv)/SUM(f.MaintEffDiv) + @TAEffIndex_Avg
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
INNER JOIN MaintIndex i On i.SubmissionID = s.SubmissionID AND i.Currency = @Currency AND i.FactorSet = f.FactorSet
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @nmPersEffIndex_QTR = NonMaintPEI
FROM dbo.CalcAveragePersKPIs(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet)

SELECT @NonMaintWHr = NonMaintWHr FROM PersTot WHERE SubmissionID = @SubmissionID

SELECT @NonMaintWHr_Avg = NonMaintWHr
FROM dbo.SumPersTot(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)

SELECT @NonMaintWHr_QTR = NonMaintWHr
FROM dbo.SumPersTot(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd)
/*
SELECT @NonMaintWHr_QTR = SUM(NonMaintWHr)
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
*/
SELECT @OCCNonMaintWHr_Avg = SUM(CASE WHEN SectionID IN ('OO','OA') THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@MPSNonMaintWHr_Avg = SUM(CASE WHEN SectionID IN ('MO','MT','MA') THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@OCCNonMaintWHr = SUM(CASE WHEN SectionID IN ('OO','OA') AND p.SubmissionID = @SubmissionID THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@MPSNonMaintWHr = SUM(CASE WHEN SectionID IN ('MO','MT','MA') AND p.SubmissionID = @SubmissionID THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@OCCNonMaintWHr_QTR = SUM(CASE WHEN SectionID IN ('OO','OA') AND s.PeriodStart >= @Start3Mo THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@MPSNonMaintWHr_QTR = SUM(CASE WHEN SectionID IN ('MO','MT','MA') AND s.PeriodStart >= @Start3Mo THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END)
	--@NonMaintWHr_Avg = SUM(CASE WHEN SectionID IN ('OO','OA','MO','MT','MA') THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END)
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.PersID IN (SELECT PersID FROM Pers_LU WHERE IncludeForInput = 'Y')

SELECT @AnnTACost = AllocAnnTACost, @RoutCost = CurrRoutCost
FROM dbo.SumMaintCost(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd, @Currency)

SELECT @AnnTACost_Avg = AllocAnnTACost, @RoutCost_Avg = CurrRoutCost
FROM dbo.SumMaintCost(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd, @Currency)

SELECT @AnnTACost_QTR = AllocAnnTACost, @RoutCost_QTR = CurrRoutCost
FROM dbo.SumMaintCost(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @Currency)

SELECT @NEOpEx = NEOpEx, @EnergyCost = EnergyCost, @TotCashOpEx = TotCashOpEx, @TAAdj = TAAdj
FROM dbo.SumOpEx(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd, @Currency)

SELECT @OpExUEDC = TotCashOpEx
FROM OpExCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpEx_Avg = NEOpEx, @EnergyCost_Avg = EnergyCost, @TotCashOpEx_Avg = TotCashOpEx, @TAAdj_Avg = TAAdj
FROM dbo.SumOpEx(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, @Currency)

SELECT @OpExUEDC_Avg = TotCashOpExUEDC, @NEOpExEDC_Avg = NEOpExEDC
FROM dbo.CalcAverageOpEx(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, @FactorSet, @Scenario, @Currency)

SELECT @NEOpEx_QTR = NEOpEx, @EnergyCost_QTR = EnergyCost, @TotCashOpEx_QTR = TotCashOpEx, @TAAdj_QTR = TAAdj
FROM dbo.SumOpEx(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @Currency)

SELECT @OpExUEDC_QTR = TotCashOpExUEDC, @NEOpExEDC_QTR = NEOpExEDC
FROM dbo.CalcAverageOpEx(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, @Scenario, @Currency)

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_Avg = @EnergyUseDay_Avg * 1.055, @EnergyUseDay_QTR = @EnergyUseDay_QTR * 1.055, 
			@FuelUseDay = @FuelUseDay * 1.055, @FuelUseDay_Avg = @FuelUseDay_Avg * 1.055, @FuelUseDay_QTR = @FuelUseDay_QTR * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_Avg = @TotStdEnergy_Avg * 1.055, @TotStdEnergy_QTR = @TotStdEnergy_QTR * 1.055

SELECT @OffsitesEDC = @EDC - @TotProcessEDC, @OffsitesEDC_QTR = @EDC_QTR - @TotProcessEDC_QTR, @OffsitesEDC_Avg = @EDC_Avg - @TotProcessEDC_Avg

SELECT @ExchRate = dbo.AvgExchangeRate('RUB', @PeriodStart, @PeriodEnd)
	, @ExchRate_QTR = dbo.AvgExchangeRate('RUB', MIN(CASE WHEN PeriodStart >= @Start3Mo THEN PeriodStart ELSE @PeriodStart END), @PeriodEnd)
	, @ExchRate_Avg = dbo.AvgExchangeRate('RUB', MIN(PeriodStart), @PeriodEnd)
FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodStart >= @Start12Mo AND PeriodStart < @PeriodEnd AND UseSubmission = 1

SELECT 
	EII = @EII, EII_Avg = @EII_Avg, EII_QTR = @EII_QTR, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_Avg = @EnergyUseDay_Avg, EnergyUseDay_QTR = @EnergyUseDay_QTR, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_Avg = @TotStdEnergy_Avg, TotStdEnergy_QTR = @TotStdEnergy_QTR, 
	FuelUseDay = @FuelUseDay, FuelUseDay_Avg = @FuelUseDay_Avg, FuelUseDay_QTR = @FuelUseDay_QTR, 

	ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_Avg = @ProcessUtilPcnt_Avg, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, 
	ProcessEDC = @TotProcessEDC, ProcessEDC_Avg = @TotProcessEDC_Avg, ProcessEDC_QTR = @TotProcessEDC_QTR, 
	ProcessUEDC = @TotProcessUEDC, ProcessUEDC_Avg = @TotProcessUEDC_Avg, ProcessUEDC_QTR = @TotProcessUEDC_QTR, 
	OffsitesEDC = @OffsitesEDC, OffsitesEDC_Avg = @OffsitesEDC_Avg, OffsitesEDC_QTR = @OffsitesEDC_QTR, 
	Complexity = @Complexity, Complexity_Avg = @Complexity_Avg, Complexity_QTR = @Complexity_QTR, 
	
	VEI = @VEI, VEI_Avg = @VEI_Avg, VEI_QTR = @VEI_QTR, 
	ReportLossGain = @ReportLossGain, ReportLossGain_Avg = @ReportLossGain_Avg, ReportLossGain_QTR = @ReportLossGain_QTR, 
	EstGain = @EstGain, EstGain_Avg = @EstGain_Avg, EstGain_QTR = @EstGain_QTR, 

	OpAvail = @OpAvail, OpAvail_Avg = @OpAvail_Avg, OpAvail_QTR = @OpAvail_QTR, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_Avg = @MechUnavailTA_Avg, MechUnavailTA_QTR = @MechUnavailTA_QTR, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_Avg = @NonTAUnavail_Avg, NonTAUnavail_QTR = @NonTAUnavail_QTR, 
	TADD = @MechUnavailTA/100*@NumDays, TADD_Avg = @MechUnavailTA/100*@NumDays_Avg, TADD_QTR = @MechUnavailTA/100*@NumDays_QTR,
	NTAMDD = @NonTAMechUnavail/100*@NumDays, NTAMDD_Avg = @NonTAMechUnavail_Avg/100*@NumDays_Avg, NTAMDD_QTR = @NonTAMechUnavail_QTR/100*@NumDays_QTR,
	RPDD = @RegUnavail/100*@NumDays, RPDD_Avg = @RegUnavail_Avg/100*@NumDays_Avg, RPDD_QTR = @RegUnavail_QTR/100*@NumDays_QTR,
	NumDays = @NumDays, NumDays_Avg = @NumDays_Avg, NumDays_QTR = @NumDays_QTR,

	nmPersEffIndex = @nmPersEffIndex, nmPersEffIndex_Avg = @nmPersEffIndex_Avg, nmPersEffIndex_QTR = @nmPersEffIndex_QTR, 
	NonMaintWHr = @NonMaintWHr/1000, NonMaintWHr_Avg = @NonMaintWHr_Avg/1000, NonMaintWHr_QTR = @NonMaintWHr_QTR/1000,
	OCCNonMaintWHr = @OCCNonMaintWHr/1000, OCCNonMaintWHr_Avg = @OCCNonMaintWHr_Avg/1000, OCCNonMaintWHr_QTR = @OCCNonMaintWHr_QTR/1000,
	MPSNonMaintWHr = @MPSNonMaintWHr/1000, MPSNonMaintWHr_Avg = @MPSNonMaintWHr_Avg/1000, MPSNonMaintWHr_QTR = @MPSNonMaintWHr_QTR/1000,
	nmPersEffDiv = @nmPersEffDiv/1000, nmPersEffDiv_Avg = @nmPersEffDiv_Avg/1000, nmPersEffDiv_QTR = @nmPersEffDiv_QTR/1000,

	MaintEffIndex = @MaintEffIndex, MaintEffIndex_Avg = @MaintEffIndex_Avg, MaintEffIndex_QTR = @MaintEffIndex_QTR, 
	AnnTACost = @TAAdj/1000, AnnTACost_Avg = @TAAdj_Avg/1000, AnnTACost_QTR = @TAAdj_QTR/1000,
	RoutCost = @RoutCost/1000, RoutCost_Avg = @RoutCost_Avg/1000, RoutCost_QTR = @RoutCost_QTR/1000, 
	MaintEffDiv = @MaintEffDiv/1000000, MaintEffDiv_QTR = @MaintEffDiv_QTR/1000000, MaintEffDiv_Avg = @MaintEffDiv_Avg/1000000,

	NEOpExEDC = @NEOpExEDC, NEOpExEDC_Avg = @NEOpExEDC_Avg, NEOpExEDC_QTR = @NEOpExEDC_QTR, 
	NEOpEx = @NEOpEx/1000, NEOpEx_Avg = @NEOpEx_Avg/1000, NEOpEx_QTR = @NEOpEx_QTR/1000, 
	EDC = @EDC, EDC_Avg = @EDC_Avg, EDC_QTR = @EDC_QTR,

	TotCashOpExUEDC = @OpExUEDC, TotCashOpExUEDC_Avg = @OpExUEDC_Avg, TotCashOpExUEDC_QTR = @OpExUEDC_QTR, 
	TAAdj = @TAAdj/1000, TAAdj_Avg = @TAAdj_Avg/1000, TAAdj_QTR = @TAAdj_QTR/1000,
	EnergyCost = @EnergyCost/1000, EnergyCost_Avg = @EnergyCost_Avg/1000, EnergyCost_QTR = @EnergyCost_QTR/1000, 
	TotCashOpEx = @TotCashOpEx/1000, TotCashOpEx_Avg = @TotCashOpEx_Avg/1000, TotCashOpEx_QTR = @TotCashOpEx_QTR/1000, 
	UEDC = @UEDC, UEDC_Avg = @UEDC_Avg, UEDC_QTR = @UEDC_QTR,
	
	ExchRate = @ExchRate, ExchRate_Avg = @ExchRate_Avg, ExchRate_QTR = @ExchRate_QTR







