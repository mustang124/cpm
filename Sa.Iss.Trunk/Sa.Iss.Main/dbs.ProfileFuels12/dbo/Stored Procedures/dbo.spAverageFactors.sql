﻿CREATE      PROC [dbo].[spAverageFactors](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @EII real OUTPUT, @VEI real OUTPUT, @UtilPcnt real OUTPUT, @UtilOSTA real OUTPUT, @EDC float OUTPUT, @UEDC float OUTPUT,
	@ProcessUtilPcnt real OUTPUT, @TotProcessEDC float = NULL OUTPUT, @TotProcessUEDC float = NULL OUTPUT)
AS
SELECT @EII = 100*CASE WHEN SUM(a.TotStdEnergy*s.NumDays) > 0 THEN SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) ELSE NULL END,
@VEI = 100*SUM(CASE WHEN VEI IS NOT NULL THEN ReportLossGain END)/SUM(CASE WHEN VEI IS NOT NULL THEN EstGain END),
@UtilPcnt = CASE WHEN SUM(EDC) > 0 THEN SUM(UtilPcnt*EDC*s.NumDays)/SUM(EDC*s.NumDays) ELSE NULL END,
@ProcessUtilPcnt = 100*CASE WHEN SUM(TotProcessEDC) > 0 THEN SUM(TotProcessUEDC*s.NumDays)/SUM(TotProcessEDC*s.NumDays) ELSE NULL END,
@UtilOSTA = CASE WHEN SUM(TotProcessEDC) > 0 THEN SUM(UtilOSTA*TotProcessEDC*s.NumDays)/SUM(TotProcessEDC*s.NumDays) ELSE NULL END,
@EDC = SUM(EDC*s.NumDays)/SUM(s.NumDays), @UEDC = SUM(UEDC*s.NumDays)/SUM(s.NumDays),
@TotProcessEDC = SUM(TotProcessEDC*s.NumDays)/SUM(s.NumDays), @TotProcessUEDC = SUM(TotProcessUEDC*s.NumDays)/SUM(s.NumDays)
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND a.FactorSet = @FactorSet

