﻿CREATE  PROC [dbo].[spFullCalcs](@SubmissionID int)
AS
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'Started'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spInitialCalcs Started'
	EXEC spInitialCalcs @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spInitialCalcs Completed'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spInventory Started'
	EXEC spInventory @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spInventory Completed'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spCrudeCalcs Started'
	EXEC spCrudeCalcs @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spCrudeCalcs Completed'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spEnergyCalc Started'
	EXEC spEnergyCalc @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spEnergyCalc Completed'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spEnergyTotCost Started'
	EXEC spEnergyTotCost @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spEnergyTotCost Completed'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spMaterialCalc Started'
	EXEC spMaterialCalc @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spMaterialCalc Completed'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spMaterialCost Started'
	EXEC spMaterialCost @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spMaterialCost Completed'
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spUnitCalcs Started'
	EXEC spUnitCalcs @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'spUnitCalcs Completed'
	EXEC spCalcIndicators @SubmissionID
	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spFullIndicators', @MessageText = 'Completed'
