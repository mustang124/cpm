﻿CREATE   PROC [dbo].[spReportValeroBUR_YTD] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @AD_KEDC_YTD real, @AD_MechAvail_YTD real, @AD_AdjMaintIndex_YTD real, 
	@AD_UtilPcnt_YTD real, @AD_NEOpExEDC_YTD real, @AD_EII_YTD real, @AD_TotWHrEDC_YTD real,
	@AD_NetInputKBPD_YTD real, @AD_CrudeKBPD_YTD real, @AD_FCCRateKBPD_YTD real, 
	@AD_NumContPers_YTD real, @AD_RoutMechUnavail_YTD real, @AD_TAMechUnavail_YTD real
DECLARE @BN_KEDC_YTD real, @BN_MechAvail_YTD real, @BN_AdjMaintIndex_YTD real, 
	@BN_UtilPcnt_YTD real, @BN_NEOpExEDC_YTD real, @BN_EII_YTD real, @BN_TotWHrEDC_YTD real,
	@BN_NetInputKBPD_YTD real, @BN_CrudeKBPD_YTD real, @BN_FCCRateKBPD_YTD real, 
	@BN_NumContPers_YTD real, @BN_RoutMechUnavail_YTD real, @BN_TAMechUnavail_YTD real
DECLARE @CC_KEDC_YTD real, @CC_MechAvail_YTD real, @CC_AdjMaintIndex_YTD real, 
	@CC_UtilPcnt_YTD real, @CC_NEOpExEDC_YTD real, @CC_EII_YTD real, @CC_TotWHrEDC_YTD real,
	@CC_NetInputKBPD_YTD real, @CC_CrudeKBPD_YTD real, @CC_FCCRateKBPD_YTD real, 
	@CC_NumContPers_YTD real, @CC_RoutMechUnavail_YTD real, @CC_TAMechUnavail_YTD real
DECLARE @HO_KEDC_YTD real, @HO_MechAvail_YTD real, @HO_AdjMaintIndex_YTD real, 
	@HO_UtilPcnt_YTD real, @HO_NEOpExEDC_YTD real, @HO_EII_YTD real, @HO_TotWHrEDC_YTD real,
	@HO_NetInputKBPD_YTD real, @HO_CrudeKBPD_YTD real, @HO_FCCRateKBPD_YTD real, 
	@HO_NumContPers_YTD real, @HO_RoutMechUnavail_YTD real, @HO_TAMechUnavail_YTD real
DECLARE @MK_KEDC_YTD real, @MK_MechAvail_YTD real, @MK_AdjMaintIndex_YTD real, 
	@MK_UtilPcnt_YTD real, @MK_NEOpExEDC_YTD real, @MK_EII_YTD real, @MK_TotWHrEDC_YTD real,
	@MK_NetInputKBPD_YTD real, @MK_CrudeKBPD_YTD real, @MK_FCCRateKBPD_YTD real, 
	@MK_NumContPers_YTD real, @MK_RoutMechUnavail_YTD real, @MK_TAMechUnavail_YTD real
DECLARE @MP_KEDC_YTD real, @MP_MechAvail_YTD real, @MP_AdjMaintIndex_YTD real, 
	@MP_UtilPcnt_YTD real, @MP_NEOpExEDC_YTD real, @MP_EII_YTD real, @MP_TotWHrEDC_YTD real,
	@MP_NetInputKBPD_YTD real, @MP_CrudeKBPD_YTD real, @MP_FCCRateKBPD_YTD real, 
	@MP_NumContPers_YTD real, @MP_RoutMechUnavail_YTD real, @MP_TAMechUnavail_YTD real
DECLARE @PA_KEDC_YTD real, @PA_MechAvail_YTD real, @PA_AdjMaintIndex_YTD real, 
	@PA_UtilPcnt_YTD real, @PA_NEOpExEDC_YTD real, @PA_EII_YTD real, @PA_TotWHrEDC_YTD real,
	@PA_NetInputKBPD_YTD real, @PA_CrudeKBPD_YTD real, @PA_FCCRateKBPD_YTD real, 
	@PA_NumContPers_YTD real, @PA_RoutMechUnavail_YTD real, @PA_TAMechUnavail_YTD real
DECLARE @QB_KEDC_YTD real, @QB_MechAvail_YTD real, @QB_AdjMaintIndex_YTD real, 
	@QB_UtilPcnt_YTD real, @QB_NEOpExEDC_YTD real, @QB_EII_YTD real, @QB_TotWHrEDC_YTD real,
	@QB_NetInputKBPD_YTD real, @QB_CrudeKBPD_YTD real, @QB_FCCRateKBPD_YTD real, 
	@QB_NumContPers_YTD real, @QB_RoutMechUnavail_YTD real, @QB_TAMechUnavail_YTD real
DECLARE @SC_KEDC_YTD real, @SC_MechAvail_YTD real, @SC_AdjMaintIndex_YTD real, 
	@SC_UtilPcnt_YTD real, @SC_NEOpExEDC_YTD real, @SC_EII_YTD real, @SC_TotWHrEDC_YTD real,
	@SC_NetInputKBPD_YTD real, @SC_CrudeKBPD_YTD real, @SC_FCCRateKBPD_YTD real, 
	@SC_NumContPers_YTD real, @SC_RoutMechUnavail_YTD real, @SC_TAMechUnavail_YTD real
DECLARE @TC_KEDC_YTD real, @TC_MechAvail_YTD real, @TC_AdjMaintIndex_YTD real, 
	@TC_UtilPcnt_YTD real, @TC_NEOpExEDC_YTD real, @TC_EII_YTD real, @TC_TotWHrEDC_YTD real,
	@TC_NetInputKBPD_YTD real, @TC_CrudeKBPD_YTD real, @TC_FCCRateKBPD_YTD real, 
	@TC_NumContPers_YTD real, @TC_RoutMechUnavail_YTD real, @TC_TAMechUnavail_YTD real
DECLARE @TR_KEDC_YTD real, @TR_MechAvail_YTD real, @TR_AdjMaintIndex_YTD real, 
	@TR_UtilPcnt_YTD real, @TR_NEOpExEDC_YTD real, @TR_EII_YTD real, @TR_TotWHrEDC_YTD real,
	@TR_NetInputKBPD_YTD real, @TR_CrudeKBPD_YTD real, @TR_FCCRateKBPD_YTD real, 
	@TR_NumContPers_YTD real, @TR_RoutMechUnavail_YTD real, @TR_TAMechUnavail_YTD real
DECLARE @WM_KEDC_YTD real, @WM_MechAvail_YTD real, @WM_AdjMaintIndex_YTD real, 
	@WM_UtilPcnt_YTD real, @WM_NEOpExEDC_YTD real, @WM_EII_YTD real, @WM_TotWHrEDC_YTD real,
	@WM_NetInputKBPD_YTD real, @WM_CrudeKBPD_YTD real, @WM_FCCRateKBPD_YTD real, 
	@WM_NumContPers_YTD real, @WM_RoutMechUnavail_YTD real, @WM_TAMechUnavail_YTD real
DECLARE @VALERO_KEDC_YTD real, @VALERO_MechAvail_YTD real, @VALERO_AdjMaintIndex_YTD real, 
	@VALERO_UtilPcnt_YTD real, @VALERO_NEOpExEDC_YTD real, @VALERO_EII_YTD real, @VALERO_TotWHrEDC_YTD real,
	@VALERO_NetInputKBPD_YTD real, @VALERO_CrudeKBPD_YTD real, @VALERO_FCCRateKBPD_YTD real, 
	@VALERO_NumContPers_YTD real, @VALERO_RoutMechUnavail_YTD real, @VALERO_TAMechUnavail_YTD real

EXEC [dbo].[spReportValeroBURYTDByRef] '75NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @AD_KEDC_YTD OUTPUT, @MechAvail_YTD = @AD_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @AD_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @AD_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @AD_NEOpExEDC_YTD OUTPUT, @EII_YTD = @AD_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @AD_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @AD_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @AD_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @AD_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @AD_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @AD_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @AD_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '33NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @BN_KEDC_YTD OUTPUT, @MechAvail_YTD = @BN_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @BN_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @BN_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @BN_NEOpExEDC_YTD OUTPUT, @EII_YTD = @BN_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @BN_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @BN_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @BN_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @BN_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @BN_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @BN_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @BN_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '218NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @CC_KEDC_YTD OUTPUT, @MechAvail_YTD = @CC_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @CC_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @CC_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @CC_NEOpExEDC_YTD OUTPUT, @EII_YTD = @CC_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @CC_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @CC_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @CC_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @CC_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @CC_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @CC_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @CC_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '98NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @HO_KEDC_YTD OUTPUT, @MechAvail_YTD = @HO_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @HO_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @HO_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @HO_NEOpExEDC_YTD OUTPUT, @EII_YTD = @HO_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @HO_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @HO_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @HO_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @HO_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @HO_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @BN_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @HO_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '28NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @MK_KEDC_YTD OUTPUT, @MechAvail_YTD = @MK_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @MK_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @MK_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @MK_NEOpExEDC_YTD OUTPUT, @EII_YTD = @MK_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @MK_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @MK_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @MK_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @MK_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @MK_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @MK_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @MK_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '38NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @MP_KEDC_YTD OUTPUT, @MechAvail_YTD = @MP_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @MP_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @MP_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @MP_NEOpExEDC_YTD OUTPUT, @EII_YTD = @MP_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @MP_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @MP_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @MP_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @MP_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @MP_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @MP_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @MP_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '19NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @PA_KEDC_YTD OUTPUT, @MechAvail_YTD = @PA_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @PA_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @PA_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @PA_NEOpExEDC_YTD OUTPUT, @EII_YTD = @PA_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @PA_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @PA_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @PA_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @PA_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @PA_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @PA_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @PA_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '110NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @QB_KEDC_YTD OUTPUT, @MechAvail_YTD = @QB_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @QB_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @QB_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @QB_NEOpExEDC_YTD OUTPUT, @EII_YTD = @QB_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @QB_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @QB_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @QB_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @QB_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @QB_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @QB_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @QB_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '217NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @SC_KEDC_YTD OUTPUT, @MechAvail_YTD = @SC_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @SC_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @SC_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @SC_NEOpExEDC_YTD OUTPUT, @EII_YTD = @SC_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @SC_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @SC_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @SC_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @SC_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @SC_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @SC_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @SC_TAMechUnavail_YTD OUTPUT
	
EXEC [dbo].[spReportValeroBURYTDByRef] '72NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @TC_KEDC_YTD OUTPUT, @MechAvail_YTD = @TC_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @TC_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @TC_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @TC_NEOpExEDC_YTD OUTPUT, @EII_YTD = @TC_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @TC_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @TC_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @TC_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @TC_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @TC_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @TC_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @TC_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '29NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @TR_KEDC_YTD OUTPUT, @MechAvail_YTD = @TR_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @TR_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @TR_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @TR_NEOpExEDC_YTD OUTPUT, @EII_YTD = @TR_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @TR_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @TR_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @TR_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @TR_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @TR_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @TR_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @TR_TAMechUnavail_YTD OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '91NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEDC_YTD = @WM_KEDC_YTD OUTPUT, @MechAvail_YTD = @WM_MechAvail_YTD OUTPUT, @AdjMaintIndex_YTD = @WM_AdjMaintIndex_YTD OUTPUT, 
	@UtilPcnt_YTD = @WM_UtilPcnt_YTD OUTPUT, @NEOpExEDC_YTD = @WM_NEOpExEDC_YTD OUTPUT, @EII_YTD = @WM_EII_YTD OUTPUT, 
	@TotWHrEDC_YTD = @WM_TotWHrEDC_YTD OUTPUT, @NetInputKBPD_YTD = @WM_NetInputKBPD_YTD OUTPUT, 
	@CrudeKBPD_YTD = @WM_CrudeKBPD_YTD OUTPUT, @FCCRateKBPD_YTD = @WM_FCCRateKBPD_YTD OUTPUT,  
	@NumContPers_YTD = @WM_NumContPers_YTD OUTPUT, @RoutMechUnavail_YTD = @WM_RoutMechUnavail_YTD OUTPUT, @TAMechUnavail_YTD = @WM_TAMechUnavail_YTD OUTPUT

SELECT 	@VALERO_KEDC_YTD = ISNULL(@AD_KEDC_YTD,0) + ISNULL(@BN_KEDC_YTD,0) + ISNULL(@CC_KEDC_YTD,0) + ISNULL(@HO_KEDC_YTD,0) + ISNULL(@MK_KEDC_YTD,0) 
			+ ISNULL(@MP_KEDC_YTD,0) + ISNULL(@PA_KEDC_YTD,0) + ISNULL(@QB_KEDC_YTD,0) + ISNULL(@SC_KEDC_YTD,0) 
			+ ISNULL(@TC_KEDC_YTD,0) + ISNULL(@TR_KEDC_YTD,0) + ISNULL(@WM_KEDC_YTD,0), 
	@VALERO_NetInputKBPD_YTD = ISNULL(@AD_NetInputKBPD_YTD,0) + ISNULL(@BN_NetInputKBPD_YTD,0) + ISNULL(@CC_NetInputKBPD_YTD,0) + ISNULL(@HO_NetInputKBPD_YTD,0) + ISNULL(@MK_NetInputKBPD_YTD,0) 
			+ ISNULL(@MP_NetInputKBPD_YTD,0) + ISNULL(@PA_NetInputKBPD_YTD,0) + ISNULL(@QB_NetInputKBPD_YTD,0) + ISNULL(@SC_NetInputKBPD_YTD,0) 
			+ ISNULL(@TC_NetInputKBPD_YTD,0) + ISNULL(@TR_NetInputKBPD_YTD,0) + ISNULL(@WM_NetInputKBPD_YTD,0),
	@VALERO_CrudeKBPD_YTD = ISNULL(@AD_CrudeKBPD_YTD,0) + ISNULL(@BN_CrudeKBPD_YTD,0) + ISNULL(@CC_CrudeKBPD_YTD,0) + ISNULL(@HO_CrudeKBPD_YTD,0) + ISNULL(@MK_CrudeKBPD_YTD,0) 
			+ ISNULL(@MP_CrudeKBPD_YTD,0) + ISNULL(@PA_CrudeKBPD_YTD,0) + ISNULL(@QB_CrudeKBPD_YTD,0) + ISNULL(@SC_CrudeKBPD_YTD,0) 
			+ ISNULL(@TC_CrudeKBPD_YTD,0) + ISNULL(@TR_CrudeKBPD_YTD,0) + ISNULL(@WM_CrudeKBPD_YTD,0),
	@VALERO_FCCRateKBPD_YTD = ISNULL(@AD_FCCRateKBPD_YTD,0) + ISNULL(@BN_FCCRateKBPD_YTD,0) + ISNULL(@CC_FCCRateKBPD_YTD,0) + ISNULL(@HO_FCCRateKBPD_YTD,0) + ISNULL(@MK_FCCRateKBPD_YTD,0) 
			+ ISNULL(@MP_FCCRateKBPD_YTD,0) + ISNULL(@PA_FCCRateKBPD_YTD,0) + ISNULL(@QB_FCCRateKBPD_YTD,0) + ISNULL(@SC_FCCRateKBPD_YTD,0) 
			+ ISNULL(@TC_FCCRateKBPD_YTD,0) + ISNULL(@TR_FCCRateKBPD_YTD,0) + ISNULL(@WM_FCCRateKBPD_YTD,0),
	@VALERO_NumContPers_YTD = ISNULL(@AD_NumContPers_YTD,0) + ISNULL(@BN_NumContPers_YTD,0) + ISNULL(@CC_NumContPers_YTD,0) + ISNULL(@HO_NumContPers_YTD,0) + ISNULL(@MK_NumContPers_YTD,0) 
			+ ISNULL(@MP_NumContPers_YTD,0) + ISNULL(@PA_NumContPers_YTD,0) + ISNULL(@QB_NumContPers_YTD,0) + ISNULL(@SC_NumContPers_YTD,0) 
			+ ISNULL(@TC_NumContPers_YTD,0) + ISNULL(@TR_NumContPers_YTD,0) + ISNULL(@WM_NumContPers_YTD,0)
IF @VALERO_KEDC_YTD > 0
	SELECT @VALERO_MechAvail_YTD = (ISNULL(@AD_MechAvail_YTD*@AD_KEDC_YTD,0) + ISNULL(@BN_MechAvail_YTD*@BN_KEDC_YTD,0) + ISNULL(@CC_MechAvail_YTD*@CC_KEDC_YTD,0) 
			+ ISNULL(@HO_MechAvail_YTD*@HO_KEDC_YTD,0) + ISNULL(@MK_MechAvail_YTD*@MK_KEDC_YTD,0) + ISNULL(@MP_MechAvail_YTD*@MP_KEDC_YTD,0) 
			+ ISNULL(@PA_MechAvail_YTD*@PA_KEDC_YTD,0) + ISNULL(@QB_MechAvail_YTD*@QB_KEDC_YTD,0) 
			+ ISNULL(@SC_MechAvail_YTD*@SC_KEDC_YTD,0) + ISNULL(@TC_MechAvail_YTD*@TC_KEDC_YTD,0) + ISNULL(@TR_MechAvail_YTD*@TR_KEDC_YTD,0) 
			+ ISNULL(@WM_MechAvail_YTD*@WM_KEDC_YTD,0))/@VALERO_KEDC_YTD, 
	@VALERO_AdjMaintIndex_YTD = (ISNULL(@AD_AdjMaintIndex_YTD*@AD_KEDC_YTD,0) + ISNULL(@BN_AdjMaintIndex_YTD*@BN_KEDC_YTD,0) + ISNULL(@CC_AdjMaintIndex_YTD*@CC_KEDC_YTD,0) 
			+ ISNULL(@HO_AdjMaintIndex_YTD*@HO_KEDC_YTD,0) + ISNULL(@MK_AdjMaintIndex_YTD*@MK_KEDC_YTD,0) + ISNULL(@MP_AdjMaintIndex_YTD*@MP_KEDC_YTD,0) 
			+ ISNULL(@PA_AdjMaintIndex_YTD*@PA_KEDC_YTD,0) + ISNULL(@QB_AdjMaintIndex_YTD*@QB_KEDC_YTD,0) 
			+ ISNULL(@SC_AdjMaintIndex_YTD*@SC_KEDC_YTD,0) + ISNULL(@TC_AdjMaintIndex_YTD*@TC_KEDC_YTD,0) + ISNULL(@TR_AdjMaintIndex_YTD*@TR_KEDC_YTD,0) 
			+ ISNULL(@WM_AdjMaintIndex_YTD*@WM_KEDC_YTD,0))/@VALERO_KEDC_YTD, 
	@VALERO_UtilPcnt_YTD = (ISNULL(@AD_UtilPcnt_YTD*@AD_KEDC_YTD,0) + ISNULL(@BN_UtilPcnt_YTD*@BN_KEDC_YTD,0) + ISNULL(@CC_UtilPcnt_YTD*@CC_KEDC_YTD,0) 
			+ ISNULL(@HO_UtilPcnt_YTD*@HO_KEDC_YTD,0) + ISNULL(@MK_UtilPcnt_YTD*@MK_KEDC_YTD,0) + ISNULL(@MP_UtilPcnt_YTD*@MP_KEDC_YTD,0) 
			+ ISNULL(@PA_UtilPcnt_YTD*@PA_KEDC_YTD,0) + ISNULL(@QB_UtilPcnt_YTD*@QB_KEDC_YTD,0) 
			+ ISNULL(@SC_UtilPcnt_YTD*@SC_KEDC_YTD,0) + ISNULL(@TC_UtilPcnt_YTD*@TC_KEDC_YTD,0) + ISNULL(@TR_UtilPcnt_YTD*@TR_KEDC_YTD,0) 
			+ ISNULL(@WM_UtilPcnt_YTD*@WM_KEDC_YTD,0))/@VALERO_KEDC_YTD, 
	@VALERO_NEOpExEDC_YTD = (ISNULL(@AD_NEOpExEDC_YTD*@AD_KEDC_YTD,0) + ISNULL(@BN_NEOpExEDC_YTD*@BN_KEDC_YTD,0) + ISNULL(@CC_NEOpExEDC_YTD*@CC_KEDC_YTD,0) 
			+ ISNULL(@HO_NEOpExEDC_YTD*@HO_KEDC_YTD,0) + ISNULL(@MK_NEOpExEDC_YTD*@MK_KEDC_YTD,0) + ISNULL(@MP_NEOpExEDC_YTD*@MP_KEDC_YTD,0) 
			+ ISNULL(@PA_NEOpExEDC_YTD*@PA_KEDC_YTD,0) + ISNULL(@QB_NEOpExEDC_YTD*@QB_KEDC_YTD,0) 
			+ ISNULL(@SC_NEOpExEDC_YTD*@SC_KEDC_YTD,0) + ISNULL(@TC_NEOpExEDC_YTD*@TC_KEDC_YTD,0) + ISNULL(@TR_NEOpExEDC_YTD*@TR_KEDC_YTD,0) 
			+ ISNULL(@WM_NEOpExEDC_YTD*@WM_KEDC_YTD,0))/@VALERO_KEDC_YTD, 
	@VALERO_TotWHrEDC_YTD = (ISNULL(@AD_TotWHrEDC_YTD*@AD_KEDC_YTD,0) + ISNULL(@BN_TotWHrEDC_YTD*@BN_KEDC_YTD,0) + ISNULL(@CC_TotWHrEDC_YTD*@CC_KEDC_YTD,0) 
			+ ISNULL(@HO_TotWHrEDC_YTD*@HO_KEDC_YTD,0) + ISNULL(@MK_TotWHrEDC_YTD*@MK_KEDC_YTD,0) + ISNULL(@MP_TotWHrEDC_YTD*@MP_KEDC_YTD,0) 
			+ ISNULL(@PA_TotWHrEDC_YTD*@PA_KEDC_YTD,0) + ISNULL(@QB_TotWHrEDC_YTD*@QB_KEDC_YTD,0) 
			+ ISNULL(@SC_TotWHrEDC_YTD*@SC_KEDC_YTD,0) + ISNULL(@TC_TotWHrEDC_YTD*@TC_KEDC_YTD,0) + ISNULL(@TR_TotWHrEDC_YTD*@TR_KEDC_YTD,0) 
			+ ISNULL(@WM_TotWHrEDC_YTD*@WM_KEDC_YTD,0))/@VALERO_KEDC_YTD, 
	@VALERO_RoutMechUnavail_YTD = (ISNULL(@AD_RoutMechUnavail_YTD*@AD_KEDC_YTD,0) + ISNULL(@BN_RoutMechUnavail_YTD*@BN_KEDC_YTD,0) + ISNULL(@CC_RoutMechUnavail_YTD*@CC_KEDC_YTD,0) 
			+ ISNULL(@HO_RoutMechUnavail_YTD*@HO_KEDC_YTD,0) + ISNULL(@MK_RoutMechUnavail_YTD*@MK_KEDC_YTD,0) + ISNULL(@MP_RoutMechUnavail_YTD*@MP_KEDC_YTD,0) 
			+ ISNULL(@PA_RoutMechUnavail_YTD*@PA_KEDC_YTD,0) + ISNULL(@QB_RoutMechUnavail_YTD*@QB_KEDC_YTD,0) 
			+ ISNULL(@SC_RoutMechUnavail_YTD*@SC_KEDC_YTD,0) + ISNULL(@TC_RoutMechUnavail_YTD*@TC_KEDC_YTD,0) + ISNULL(@TR_RoutMechUnavail_YTD*@TR_KEDC_YTD,0) 
			+ ISNULL(@WM_RoutMechUnavail_YTD*@WM_KEDC_YTD,0))/@VALERO_KEDC_YTD, 
	@VALERO_TAMechUnavail_YTD = (ISNULL(@AD_TAMechUnavail_YTD*@AD_KEDC_YTD,0) + ISNULL(@BN_TAMechUnavail_YTD*@BN_KEDC_YTD,0) + ISNULL(@CC_TAMechUnavail_YTD*@CC_KEDC_YTD,0) 
			+ ISNULL(@HO_TAMechUnavail_YTD*@HO_KEDC_YTD,0) + ISNULL(@MK_TAMechUnavail_YTD*@MK_KEDC_YTD,0) + ISNULL(@MP_TAMechUnavail_YTD*@MP_KEDC_YTD,0) 
			+ ISNULL(@PA_TAMechUnavail_YTD*@PA_KEDC_YTD,0) + ISNULL(@QB_TAMechUnavail_YTD*@QB_KEDC_YTD,0) 
			+ ISNULL(@SC_TAMechUnavail_YTD*@SC_KEDC_YTD,0) + ISNULL(@TC_TAMechUnavail_YTD*@TC_KEDC_YTD,0) + ISNULL(@TR_TAMechUnavail_YTD*@TR_KEDC_YTD,0) 
			+ ISNULL(@WM_TAMechUnavail_YTD*@WM_KEDC_YTD,0))/@VALERO_KEDC_YTD

SELECT @VALERO_EII_YTD = 100*SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) 
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA')
AND s.DataSet = 'Actual' AND s.PeriodYear = @PeriodYear AND s.PeriodMonth <= @PeriodMonth AND s.UseSubmission = 1
AND a.FactorSet = @FactorSet AND a.EnergyUseDay > 0 AND a.TotStdEnergy> 0 AND s.NumDays > 0

SET NOCOUNT OFF
SELECT ReportPeriod = CAST(CAST(@PeriodMonth as varchar(2)) + '/1/' + CAST(@PeriodYear as varchar(4)) as smalldatetime),
	AD_KEDC_YTD = @AD_KEDC_YTD, AD_MechAvail_YTD = @AD_MechAvail_YTD, AD_AdjMaintIndex_YTD = @AD_AdjMaintIndex_YTD, 
	AD_UtilPcnt_YTD = @AD_UtilPcnt_YTD, AD_NEOpExEDC_YTD = @AD_NEOpExEDC_YTD, AD_EII_YTD = @AD_EII_YTD, AD_TotWHrEDC_YTD = @AD_TotWHrEDC_YTD,
	AD_NetInputKBPD_YTD = @AD_NetInputKBPD_YTD, AD_CrudeKBPD_YTD = @AD_CrudeKBPD_YTD, AD_FCCRateKBPD_YTD = @AD_FCCRateKBPD_YTD, 
	AD_NumContPers_YTD = @AD_NumContPers_YTD, AD_RoutMechUnavail_YTD = @AD_RoutMechUnavail_YTD, AD_TAMechUnavail_YTD = @AD_TAMechUnavail_YTD,
	BN_KEDC_YTD = @BN_KEDC_YTD, BN_MechAvail_YTD = @BN_MechAvail_YTD, BN_AdjMaintIndex_YTD = @BN_AdjMaintIndex_YTD, 
	BN_UtilPcnt_YTD = @BN_UtilPcnt_YTD, BN_NEOpExEDC_YTD = @BN_NEOpExEDC_YTD, BN_EII_YTD = @BN_EII_YTD, BN_TotWHrEDC_YTD = @BN_TotWHrEDC_YTD,
	BN_NetInputKBPD_YTD = @BN_NetInputKBPD_YTD, BN_CrudeKBPD_YTD = @BN_CrudeKBPD_YTD, BN_FCCRateKBPD_YTD = @BN_FCCRateKBPD_YTD, 
	BN_NumContPers_YTD = @BN_NumContPers_YTD, BN_RoutMechUnavail_YTD = @BN_RoutMechUnavail_YTD, BN_TAMechUnavail_YTD = @BN_TAMechUnavail_YTD,
	CC_KEDC_YTD = @CC_KEDC_YTD, CC_MechAvail_YTD = @CC_MechAvail_YTD, CC_AdjMaintIndex_YTD = @CC_AdjMaintIndex_YTD, 
	CC_UtilPcnt_YTD = @CC_UtilPcnt_YTD, CC_NEOpExEDC_YTD = @CC_NEOpExEDC_YTD, CC_EII_YTD = @CC_EII_YTD, CC_TotWHrEDC_YTD = @CC_TotWHrEDC_YTD,
	CC_NetInputKBPD_YTD = @CC_NetInputKBPD_YTD, CC_CrudeKBPD_YTD = @CC_CrudeKBPD_YTD, CC_FCCRateKBPD_YTD = @CC_FCCRateKBPD_YTD, 
	CC_NumContPers_YTD = @CC_NumContPers_YTD, CC_RoutMechUnavail_YTD = @CC_RoutMechUnavail_YTD, CC_TAMechUnavail_YTD = @CC_TAMechUnavail_YTD,
	HO_KEDC_YTD = @HO_KEDC_YTD, HO_MechAvail_YTD = @HO_MechAvail_YTD, HO_AdjMaintIndex_YTD = @HO_AdjMaintIndex_YTD, 
	HO_UtilPcnt_YTD = @HO_UtilPcnt_YTD, HO_NEOpExEDC_YTD = @HO_NEOpExEDC_YTD, HO_EII_YTD = @HO_EII_YTD, HO_TotWHrEDC_YTD = @HO_TotWHrEDC_YTD,
	HO_NetInputKBPD_YTD = @HO_NetInputKBPD_YTD, HO_CrudeKBPD_YTD = @HO_CrudeKBPD_YTD, HO_FCCRateKBPD_YTD = @HO_FCCRateKBPD_YTD, 
	HO_NumContPers_YTD = @HO_NumContPers_YTD, HO_RoutMechUnavail_YTD = @HO_RoutMechUnavail_YTD, HO_TAMechUnavail_YTD = @HO_TAMechUnavail_YTD,
	MK_KEDC_YTD = @MK_KEDC_YTD, MK_MechAvail_YTD = @MK_MechAvail_YTD, MK_AdjMaintIndex_YTD = @MK_AdjMaintIndex_YTD, 
	MK_UtilPcnt_YTD = @MK_UtilPcnt_YTD, MK_NEOpExEDC_YTD = @MK_NEOpExEDC_YTD, MK_EII_YTD = @MK_EII_YTD, MK_TotWHrEDC_YTD = @MK_TotWHrEDC_YTD,
	MK_NetInputKBPD_YTD = @MK_NetInputKBPD_YTD, MK_CrudeKBPD_YTD = @MK_CrudeKBPD_YTD, MK_FCCRateKBPD_YTD = @MK_FCCRateKBPD_YTD, 
	MK_NumContPers_YTD = @MK_NumContPers_YTD, MK_RoutMechUnavail_YTD = @MK_RoutMechUnavail_YTD, MK_TAMechUnavail_YTD = @MK_TAMechUnavail_YTD,
	MP_KEDC_YTD = @MP_KEDC_YTD, MP_MechAvail_YTD = @MP_MechAvail_YTD, MP_AdjMaintIndex_YTD = @MP_AdjMaintIndex_YTD, 
	MP_UtilPcnt_YTD = @MP_UtilPcnt_YTD, MP_NEOpExEDC_YTD = @MP_NEOpExEDC_YTD, MP_EII_YTD = @MP_EII_YTD, MP_TotWHrEDC_YTD = @MP_TotWHrEDC_YTD,
	MP_NetInputKBPD_YTD = @MP_NetInputKBPD_YTD, MP_CrudeKBPD_YTD = @MP_CrudeKBPD_YTD, MP_FCCRateKBPD_YTD = @MP_FCCRateKBPD_YTD, 
	MP_NumContPers_YTD = @MP_NumContPers_YTD, MP_RoutMechUnavail_YTD = @MP_RoutMechUnavail_YTD, MP_TAMechUnavail_YTD = @MP_TAMechUnavail_YTD,
	PA_KEDC_YTD = @PA_KEDC_YTD, PA_MechAvail_YTD = @PA_MechAvail_YTD, PA_AdjMaintIndex_YTD = @PA_AdjMaintIndex_YTD, 
	PA_UtilPcnt_YTD = @PA_UtilPcnt_YTD, PA_NEOpExEDC_YTD = @PA_NEOpExEDC_YTD, PA_EII_YTD = @PA_EII_YTD, PA_TotWHrEDC_YTD = @PA_TotWHrEDC_YTD,
	PA_NetInputKBPD_YTD = @PA_NetInputKBPD_YTD, PA_CrudeKBPD_YTD = @PA_CrudeKBPD_YTD, PA_FCCRateKBPD_YTD = @PA_FCCRateKBPD_YTD, 
	PA_NumContPers_YTD = @PA_NumContPers_YTD, PA_RoutMechUnavail_YTD = @PA_RoutMechUnavail_YTD, PA_TAMechUnavail_YTD = @PA_TAMechUnavail_YTD,
	QB_KEDC_YTD = @QB_KEDC_YTD, QB_MechAvail_YTD = @QB_MechAvail_YTD, QB_AdjMaintIndex_YTD = @QB_AdjMaintIndex_YTD, 
	QB_UtilPcnt_YTD = @QB_UtilPcnt_YTD, QB_NEOpExEDC_YTD = @QB_NEOpExEDC_YTD, QB_EII_YTD = @QB_EII_YTD, QB_TotWHrEDC_YTD = @QB_TotWHrEDC_YTD,
	QB_NetInputKBPD_YTD = @QB_NetInputKBPD_YTD, QB_CrudeKBPD_YTD = @QB_CrudeKBPD_YTD, QB_FCCRateKBPD_YTD = @QB_FCCRateKBPD_YTD, 
	QB_NumContPers_YTD = @QB_NumContPers_YTD, QB_RoutMechUnavail_YTD = @QB_RoutMechUnavail_YTD, QB_TAMechUnavail_YTD = @QB_TAMechUnavail_YTD,
	SC_KEDC_YTD = @SC_KEDC_YTD, SC_MechAvail_YTD = @SC_MechAvail_YTD, SC_AdjMaintIndex_YTD = @SC_AdjMaintIndex_YTD, 
	SC_UtilPcnt_YTD = @SC_UtilPcnt_YTD, SC_NEOpExEDC_YTD = @SC_NEOpExEDC_YTD, SC_EII_YTD = @SC_EII_YTD, SC_TotWHrEDC_YTD = @SC_TotWHrEDC_YTD,
	SC_NetInputKBPD_YTD = @SC_NetInputKBPD_YTD, SC_CrudeKBPD_YTD = @SC_CrudeKBPD_YTD, SC_FCCRateKBPD_YTD = @SC_FCCRateKBPD_YTD, 
	SC_NumContPers_YTD = @SC_NumContPers_YTD, SC_RoutMechUnavail_YTD = @SC_RoutMechUnavail_YTD, SC_TAMechUnavail_YTD = @SC_TAMechUnavail_YTD,
	TC_KEDC_YTD = @TC_KEDC_YTD, TC_MechAvail_YTD = @TC_MechAvail_YTD, TC_AdjMaintIndex_YTD = @TC_AdjMaintIndex_YTD, 
	TC_UtilPcnt_YTD = @TC_UtilPcnt_YTD, TC_NEOpExEDC_YTD = @TC_NEOpExEDC_YTD, TC_EII_YTD = @TC_EII_YTD, TC_TotWHrEDC_YTD = @TC_TotWHrEDC_YTD,
	TC_NetInputKBPD_YTD = @TC_NetInputKBPD_YTD, TC_CrudeKBPD_YTD = @TC_CrudeKBPD_YTD, TC_FCCRateKBPD_YTD = @TC_FCCRateKBPD_YTD, 
	TC_NumContPers_YTD = @TC_NumContPers_YTD, TC_RoutMechUnavail_YTD = @TC_RoutMechUnavail_YTD, TC_TAMechUnavail_YTD = @TC_TAMechUnavail_YTD,
	TR_KEDC_YTD = @TR_KEDC_YTD, TR_MechAvail_YTD = @TR_MechAvail_YTD, TR_AdjMaintIndex_YTD = @TR_AdjMaintIndex_YTD, 
	TR_UtilPcnt_YTD = @TR_UtilPcnt_YTD, TR_NEOpExEDC_YTD = @TR_NEOpExEDC_YTD, TR_EII_YTD = @TR_EII_YTD, TR_TotWHrEDC_YTD = @TR_TotWHrEDC_YTD,
	TR_NetInputKBPD_YTD = @TR_NetInputKBPD_YTD, TR_CrudeKBPD_YTD = @TR_CrudeKBPD_YTD, TR_FCCRateKBPD_YTD = @TR_FCCRateKBPD_YTD, 
	TR_NumContPers_YTD = @TR_NumContPers_YTD, TR_RoutMechUnavail_YTD = @TR_RoutMechUnavail_YTD, TR_TAMechUnavail_YTD = @TR_TAMechUnavail_YTD,
	WM_KEDC_YTD = @WM_KEDC_YTD, WM_MechAvail_YTD = @WM_MechAvail_YTD, WM_AdjMaintIndex_YTD = @WM_AdjMaintIndex_YTD, 
	WM_UtilPcnt_YTD = @WM_UtilPcnt_YTD, WM_NEOpExEDC_YTD = @WM_NEOpExEDC_YTD, WM_EII_YTD = @WM_EII_YTD, WM_TotWHrEDC_YTD = @WM_TotWHrEDC_YTD,
	WM_NetInputKBPD_YTD = @WM_NetInputKBPD_YTD, WM_CrudeKBPD_YTD = @WM_CrudeKBPD_YTD, WM_FCCRateKBPD_YTD = @WM_FCCRateKBPD_YTD, 
	WM_NumContPers_YTD = @WM_NumContPers_YTD, WM_RoutMechUnavail_YTD = @WM_RoutMechUnavail_YTD, WM_TAMechUnavail_YTD = @WM_TAMechUnavail_YTD,
	VALERO_MEDC_YTD = @VALERO_KEDC_YTD/1000, VALERO_MechAvail_YTD = @VALERO_MechAvail_YTD, VALERO_AdjMaintIndex_YTD = @VALERO_AdjMaintIndex_YTD, 
	VALERO_UtilPcnt_YTD = @VALERO_UtilPcnt_YTD, VALERO_NEOpExEDC_YTD = @VALERO_NEOpExEDC_YTD, VALERO_EII_YTD = @VALERO_EII_YTD, VALERO_TotWHrEDC_YTD = @VALERO_TotWHrEDC_YTD,
	VALERO_NetInputKBPD_YTD = @VALERO_NetInputKBPD_YTD, VALERO_CrudeKBPD_YTD = @VALERO_CrudeKBPD_YTD, VALERO_FCCRateKBPD_YTD = @VALERO_FCCRateKBPD_YTD, 
	VALERO_NumContPers_YTD = @VALERO_NumContPers_YTD, VALERO_RoutMechUnavail_YTD = @VALERO_RoutMechUnavail_YTD, VALERO_TAMechUnavail_YTD = @VALERO_TAMechUnavail_YTD

