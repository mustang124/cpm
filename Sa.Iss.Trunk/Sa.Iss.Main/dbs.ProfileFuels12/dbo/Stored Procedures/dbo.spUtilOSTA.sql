﻿CREATE   PROC [dbo].[spUtilOSTA] (@SubmissionID int)
AS
SET NOCOUNT ON

DECLARE @NumDays real
SELECT @NumDays = NumDays FROM SubmissionsAll WHERE SubmissionID = @SubmissionID

UPDATE FactorCalc
SET UtilOSTA = UtilPcnt
WHERE SubmissionID = @SubmissionID
AND ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE InUtilOSTA = 'Y')

UPDATE FactorCalc
SET UtilOSTA = CASE WHEN (@NumDays * c.InServicePcnt/100 <= ta.HrsDownInPeriod/24.0) THEN 100 ELSE
	FactorCalc.UtilPcnt*((c.InServicePcnt/100 * @NumDays)/
	(@NumDays * c.InServicePcnt/100 - ta.HrsDownInPeriod/24.0)) END
FROM FactorCalc INNER JOIN Config c ON c.SubmissionID = FactorCalc.SubmissionID AND c.UnitID = FactorCalc.UnitID
INNER JOIN TAForSubmission ta ON ta.SubmissionID = c.SubmissionID AND ta.UnitID = c.UnitID
WHERE FactorCalc.SubmissionID = @SubmissionID AND ta.HrsDownInPeriod > 0
AND FactorCalc.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE InUtilOSTA = 'Y')

UPDATE FactorCalc
SET UtilOSTA = CASE WHEN UtilOSTA > 100 THEN CASE WHEN UtilPcnt > 100 THEN UtilPcnt ELSE 100 END ELSE UtilOSTA END,
UEDCOSTA = EDCNoMult*(CASE WHEN UtilOSTA > 100 THEN CASE WHEN UtilPcnt > 100 THEN UtilPcnt ELSE 100 END ELSE UtilOSTA END)/100
WHERE SubmissionID = @SubmissionID

SELECT f.FactorSet, ProcessID = g.ProcessGrouping, SUM(f.UEDCOSTA) AS UEDCOSTA, [$(dbsGlobal)].dbo.WtAvg(f.UtilOSTA, f.EDCNoMult*(100.0-mc.MechUnavailTA_Act)/100.0) AS UtilOSTA
INTO #ProcessOSTA
FROM FactorCalc f INNER JOIN RefProcessGroupings g ON g.SubmissionID = f.SubmissionID AND g.UnitID = f.UnitID AND g.FactorSet = f.FactorSet
INNER JOIN MaintCalc mc ON mc.SubmissionID = f.SubmissionID AND mc.UnitID = f.UnitID
WHERE f.SubmissionID = @SubmissionID AND f.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE InUtilOSTA = 'Y')
GROUP BY f.FactorSet, g.ProcessGrouping

UPDATE FactorProcessCalc
SET UEDCOSTA = FactorProcessCalc.MultiFactor * #ProcessOSTA.UEDCOSTA,
UtilOSTA = #ProcessOSTA.UtilOSTA
FROM FactorProcessCalc INNER JOIN #ProcessOSTA 
	ON #ProcessOSTA.FactorSet = FactorProcessCalc.FactorSet 
	AND #ProcessOSTA.ProcessID = FactorProcessCalc.ProcessID
WHERE FactorProcessCalc.SubmissionID = @SubmissionID

DROP TABLE #ProcessOSTA

SELECT FactorSet, UEDCOSTA = UEDCOSTA, 
UtilOSTA = UtilOSTA
INTO #RefOSTA
FROM FactorProcessCalc f 
WHERE f.SubmissionID = @SubmissionID AND f.ProcessID = 'TotProc'

UPDATE FactorTotCalc
SET UEDCOSTA = #RefOSTA.UEDCOSTA, UtilOSTA = #RefOSTA.UtilOSTA
FROM FactorTotCalc INNER JOIN #RefOSTA ON #RefOSTA.FactorSet = FactorTotCalc.FactorSet
WHERE FactorTotCalc.SubmissionID = @SubmissionID

DROP TABLE #RefOSTA

