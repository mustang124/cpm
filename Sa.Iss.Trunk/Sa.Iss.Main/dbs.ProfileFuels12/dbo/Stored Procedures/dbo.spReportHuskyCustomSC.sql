﻿CREATE PROC [dbo].[spReportHuskyCustomSC] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE	
	@EII real, @EII_YTD real, @EII_Avg real, 
	@EnergyUseDay real, @EnergyUseDay_YTD real, @EnergyUseDay_Avg real, 
	@TotStdEnergy real, @TotStdEnergy_YTD real, @TotStdEnergy_Avg real, 

	@RefUtilPcnt real, @RefUtilPcnt_YTD real, @RefUtilPcnt_Avg real, 
	@EDC real, @EDC_YTD real, @EDC_Avg real, 
/*	
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_Avg real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_Avg real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_Avg real, 
*/	
	@MechAvail real, @MechAvail_YTD real, @MechAvail_Avg real, 
	@MechUnavailTA real, @MechUnavailTA_YTD real, @MechUnavailTA_Avg real, 
	@NonTAUnavail real, @NonTAUnavail_YTD real, @NonTAUnavail_Avg real, 

	@PersIndex real, @PersIndex_YTD real, @PersIndex_Avg real, 
	@CompWHr real, @CompWHr_YTD real, @CompWHr_Avg real,
	@ContWHr real, @ContWHr_YTD real, @ContWHr_Avg real,

	@MaintIndex real, @MaintIndex_YTD real, @MaintIndex_Avg real, 
	@TAAdj real, @TAAdj_YTD real, @TAAdj_Avg real,
	@AnnTACost real, @AnnTACost_YTD real, @AnnTACost_Avg real, 
	@RoutCost real, @RoutCost_YTD real, @RoutCost_Avg real, 

	@NEOpExUEDC real, @NEOpExUEDC_YTD real, @NEOpExUEDC_Avg real, 
	@NEOpEx real, @NEOpEx_YTD real, @NEOpEx_Avg real, 

	@OpExUEDC real, @OpExUEDC_YTD real, @OpExUEDC_Avg real,
	@EnergyCost real, @EnergyCost_YTD real, @EnergyCost_Avg real, 
	@TotCashOpEx real, @TotCashOpEx_YTD real, @TotCashOpEx_Avg real,
	@UEDC real, @UEDC_YTD real, @UEDC_Avg real
DECLARE @EII_Target real, @RefUtilPcnt_Target real, @MechAvail_Target real, @MaintIndex_Target real, @PersIndex_Target real, @NEOpExUEDC_Target real, @OpExUEDC_Target real
	
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)

SELECT @EII = NULL, @EII_YTD = NULL, @EII_Avg = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_YTD = NULL, @EnergyUseDay_Avg = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_YTD = NULL, @TotStdEnergy_Avg = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_YTD = NULL, @RefUtilPcnt_Avg = NULL, 
	@EDC = NULL, @EDC_YTD = NULL, @EDC_Avg = NULL, 
	@UEDC = NULL, @UEDC_YTD = NULL, @UEDC_Avg = NULL, 
	@MechAvail = NULL, @MechAvail_YTD = NULL, @MechAvail_Avg = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_YTD = NULL, @MechUnavailTA_Avg = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_YTD = NULL, @NonTAUnavail_Avg = NULL, 
	@RoutCost = NULL, @RoutCost_YTD = NULL, @RoutCost_Avg = NULL, 
	@PersIndex = NULL, @PersIndex_YTD = NULL, @PersIndex_Avg = NULL, 
	@CompWHr = NULL, @CompWHr_YTD = NULL, @CompWHr_Avg = NULL,
	@ContWHr = NULL, @ContWHr_YTD = NULL, @ContWHr_Avg = NULL,
	@NEOpExUEDC = NULL, @NEOpExUEDC_YTD = NULL, @NEOpExUEDC_Avg = NULL, 
	@NEOpEx = NULL, @NEOpEx_YTD = NULL, @NEOpEx_Avg = NULL, 
	@OpExUEDC = NULL, @OpExUEDC_YTD = NULL, @OpExUEDC_Avg = NULL, 
	@TAAdj = NULL, @TAAdj_YTD = NULL, @TAAdj_Avg = NULL,
	@EnergyCost = NULL, @EnergyCost_YTD = NULL, @EnergyCost_Avg = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_YTD = NULL, @TotCashOpEx_Avg = NULL,
	@EII_Target = NULL, @RefUtilPcnt_Target = NULL, @MechAvail_Target = NULL, @MaintIndex_Target = NULL, @PersIndex_Target = NULL, @NEOpExUEDC_Target = NULL, @OpExUEDC_Target = NULL
	
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
DECLARE @NumDays24Mo real, @NumDays12Mo real
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)

--- Everything Already Available in GenSum
SELECT	@RefUtilPcnt = UtilPcnt, @RefUtilPcnt_YTD = UtilPcnt_YTD, @RefUtilPcnt_Avg = UtilPcnt_Avg,
	@MechAvail = MechAvail, @MechAvail_YTD = MechAvail_YTD, @MechAvail_Avg = MechAvail_Avg,
	@EII = EII, @EII_YTD = EII_YTD, @EII_Avg = EII_Avg,
	@PersIndex = TotWHrEDC, @PersIndex_YTD = TotWHrEDC_YTD, @PersIndex_Avg = TotWHrEDC_Avg,
	@EDC = EDC/1000, @EDC_YTD = EDC_YTD/1000, @EDC_Avg = EDC_Avg/1000,
	@UEDC = UEDC/1000, @UEDC_YTD = UEDC_YTD/1000, @UEDC_Avg = UEDC_Avg/1000,
	@MaintIndex = RoutIndex + TAIndex_Avg, @MaintIndex_YTD = MaintIndex_YTD, @MaintIndex_Avg = MaintIndex_Avg,
	@NEOpExUEDC = NEOpExUEDC, @NEOpExUEDC_YTD = NEOpExUEDC_YTD, @NEOpExUEDC_Avg = NEOpExUEDC_Avg,
	@EII_Target = EII_Target, @RefUtilPcnt_Target = UtilPcnt_Target, @MechAvail_Target = MechAvail_Target, @MaintIndex_Target = MaintIndex_Target, 
	@PersIndex_Target = TotWHrEDC_Target, @NEOpExUEDC_Target = NEOpExUEDC_Target, 
	@OpExUEDC = TotCashOpExUEDC, @OpExUEDC_YTD = TotCashOpExUEDC_YTD, @OpExUEDC_Avg = TotCashOpExUEDC_Avg, @OpExUEDC_Target = TotCashOpExUEDC_Target
FROM GenSum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_YTD = MechUnavailTA_Ann, @MechUnavailTA_Avg = MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet
/*
SELECT @MechUnavailTA_YTD=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet
*/
SELECT @MechUnavailTA_Avg=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail = 100 - @MechAvail - @MechUnavailTA, @NonTAUnavail_YTD = 100 - @MechAvail_YTD - @MechUnavailTA_YTD, @NonTAUnavail_Avg = 100 - @MechAvail_Avg - @MechUnavailTA_Avg
IF @NonTAUnavail < 0.05
	SET @NonTAUnavail = 0
IF @NonTAUnavail_YTD < 0.05
	SET @NonTAUnavail_YTD = 0
IF @NonTAUnavail_Avg < 0.05
	SET @NonTAUnavail_Avg = 0
	
SELECT @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet
/*
SELECT @EnergyUseDay_YTD = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_YTD = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet
*/
SELECT @EnergyUseDay_Avg = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_Avg = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@NumDays12Mo = SUM(s.NumDays)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @CompWHr = CompWHr+GAWHr, @ContWHr = ContWHr
FROM PersST WHERE SubmissionID = @SubmissionID AND SectionID = 'TP'
/*
SELECT @CompWHr_YTD = SUM(CompWHr+GAWHr), @ContWHr_YTD = SUM(ContWHr)
FROM PersST p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.SectionID = 'TP'
*/
SELECT @CompWHr_Avg = SUM(CompWHr+GAWHr), @ContWHr_Avg = SUM(ContWHr)
FROM PersST p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.SectionID = 'TP'

SELECT @AnnTACost = AllocAnnTACost, @RoutCost = CurrRoutCost
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency
/*
SELECT @AnnTACost_YTD = SUM(AllocAnnTACost), @RoutCost_YTD = SUM(CurrRoutCost)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency
*/
SELECT @RoutCost_Avg = SUM(CurrRoutCost), @NumDays24Mo = SUM(s.NumDays)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency

SELECT @NEOpEx = NEOpEx*Divisor, @EnergyCost = EnergyCost*Divisor, @TotCashOpEx = TotCashOpEx*Divisor, @TAAdj = TAAdj*Divisor
FROM OpExCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

/*
SELECT @NEOpEx_YTD = SUM(NEOpEx*Divisor), @EnergyCost_YTD = SUM(EnergyCost*Divisor)
	, @TotCashOpEx_YTD = SUM(TotCashOpEx*Divisor), @TAAdj_YTD = SUM(TAAdj*Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpExUEDC_YTD = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'
*/
SELECT @NEOpEx_Avg = SUM(NEOpEx*Divisor), @EnergyCost_Avg = SUM(EnergyCost*Divisor), @TAAdj_Avg = SUM(TAAdj*Divisor)
	, @TotCashOpEx_Avg = SUM(TotCashOpEx*Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'
/*
SELECT @OpExUEDC_Avg = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'
*/
IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_YTD = @EnergyUseDay_YTD * 1.055, @EnergyUseDay_Avg = @EnergyUseDay_Avg * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_YTD = @TotStdEnergy_YTD * 1.055, @TotStdEnergy_Avg = @TotStdEnergy_Avg * 1.055

SELECT 
	@AnnTACost = @AnnTACost/365.25, 
	@RoutCost = @RoutCost/@NumDays, @RoutCost_Avg = @RoutCost_Avg/@NumDays24Mo, 
	@CompWHr = @CompWHr/@NumDays, @CompWHr_Avg = @CompWHr_Avg/@NumDays12Mo, 
	@ContWHr = @ContWHr/@NumDays, @ContWHr_Avg = @ContWHr_Avg/@NumDays12Mo, 
	@NEOpEx = @NEOpEx/@NumDays, @NEOpEx_Avg = @NEOpEx_Avg/@NumDays12Mo, 
	@EnergyCost = @EnergyCost/@NumDays, @EnergyCost_Avg = @EnergyCost_Avg/@NumDays12Mo, 
	@TotCashOpEx = @TotCashOpEx/@NumDays, @TotCashOpEx_Avg = @TotCashOpEx_Avg/@NumDays12Mo
SELECT @AnnTACost_Avg = @AnnTACost

DECLARE @EII_Study real, @EII_StudyP1 real, @EnergyUseDay_Study real, @EnergyUseDay_StudyP1 real, @TotStdEnergy_Study real, @TotStdEnergy_StudyP1 real
DECLARE @RefUtilPcnt_Study real, @RefUtilPcnt_StudyP1 real, @EDC_Study real, @EDC_StudyP1 real, @UtilUEDC_Study real, @UtilUEDC_StudyP1 real
DECLARE @MechAvail_Study real, @MechAvail_StudyP1 real, @MechUnavailTA_Study real, @MechUnavailTA_StudyP1 real, @NonTAUnavail_Study real, @NonTAUnavail_StudyP1 real
DECLARE @MaintIndex_Study real, @MaintIndex_StudyP1 real, @TAAdj_Study real, @TAAdj_StudyP1 real, @RoutCost_Study real, @RoutCost_StudyP1 real
DECLARE @PersIndex_Study real, @PersIndex_StudyP1 real, @CompWHr_Study real, @CompWHr_StudyP1 real, @ContWHr_Study real, @ContWHr_StudyP1 real
DECLARE @NEOpExUEDC_Study real, @NEOpExUEDC_StudyP1 real, @NEOpEx_Study real, @NEOpEx_StudyP1 real, @UEDC_Study real, @UEDC_StudyP1 real
DECLARE @OpExUEDC_Study real, @OpExUEDC_StudyP1 real, @EnergyCost_Study real, @EnergyCost_StudyP1 real, @TotCashOpEx_Study real, @TotCashOpEx_StudyP1 real

-- Get study values
SELECT @EII_Study = StudyValue, @EII_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EII'
SELECT @EnergyUseDay_Study = StudyValue, @EnergyUseDay_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EnergyUseDay'
SELECT @TotStdEnergy_Study = StudyValue, @TotStdEnergy_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotStdEnergy'
SELECT @RefUtilPcnt_Study = StudyValue, @RefUtilPcnt_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'RefUtilPcnt'
SELECT @EDC_Study = StudyValue, @EDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EDC'
SELECT @UtilUEDC_Study = StudyValue, @UtilUEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'UEDC_Util'
SELECT @MechAvail_Study = StudyValue, @MechAvail_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MechAvail'
SELECT @MechUnavailTA_Study = StudyValue, @MechUnavailTA_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MechUnavailTA'
SELECT @NonTAUnavail_Study = 100 - @MechAvail_Study - @MechUnavailTA_Study, @NonTAUnavail_StudyP1 = 100 - @MechAvail_StudyP1 - @MechUnavailTA_StudyP1
SELECT @MaintIndex_Study = StudyValue, @MaintIndex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MaintIndex'
SELECT @TAAdj_Study = StudyValue, @TAAdj_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'AnnTACostK$/Day'
SELECT @RoutCost_Study = StudyValue, @RoutCost_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'AnnRoutCostK$/Day'
SELECT @PersIndex_Study = StudyValue, @PersIndex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'PersIndex'
SELECT @CompWHr_Study = StudyValue, @CompWHr_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'CompWHr/Day'
SELECT @ContWHr_Study = StudyValue, @ContWHr_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'ContWHr/Day'
SELECT @NEOpExUEDC_Study = StudyValue, @NEOpExUEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'NEOpExUEDC'
SELECT @NEOpEx_Study = StudyValue, @NEOpEx_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'NEOpExK$/Day'
SELECT @UEDC_Study = StudyValue, @UEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'UEDC_OpEx'
SELECT @OpExUEDC_Study = StudyValue, @OpExUEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotCashOpExUEDC'
SELECT @EnergyCost_Study = StudyValue, @EnergyCost_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EnergyCostK$/Day'
SELECT @TotCashOpEx_Study = StudyValue, @TotCashOpEx_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotCashOpExK$/Day'

SELECT 
	EII = @EII, EII_Avg = @EII_Avg, EII_Study = @EII_Study, EII_StudyP1 = @EII_StudyP1, EII_Target = @EII_Target,
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_Avg = @EnergyUseDay_Avg, EnergyUseDay_Study = @EnergyUseDay_Study, EnergyUseDay_StudyP1 = @EnergyUseDay_StudyP1, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_Avg = @TotStdEnergy_Avg, TotStdEnergy_Study = @TotStdEnergy_Study, TotStdEnergy_StudyP1 = @TotStdEnergy_StudyP1, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_Avg = @RefUtilPcnt_Avg, UtilPcnt_Study = @RefUtilPcnt_Study, UtilPcnt_StudyP1 = @RefUtilPcnt_StudyP1, UtilPcnt_Target = @RefUtilPcnt_Target,
	EDC = @EDC, EDC_Avg = @EDC_Avg, EDC_Study = @EDC_Study, EDC_StudyP1 = @EDC_StudyP1,
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_Avg = @EDC_Avg*@RefUtilPcnt_Avg/100, UtilUEDC_Study = @UtilUEDC_Study, UtilUEDC_StudyP1 = @UtilUEDC_StudyP1,
	
	MechAvail = @MechAvail, MechAvail_Avg = @MechAvail_Avg, MechAvail_Study = @MechAvail_Study, MechAvail_StudyP1 = @MechAvail_StudyP1, MechAvail_Target = @MechAvail_Target,
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_Avg = @MechUnavailTA_Avg, MechUnavailTA_Study = @MechUnavailTA_Study, MechUnavailTA_StudyP1 = @MechUnavailTA_StudyP1,
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_Avg = @NonTAUnavail_Avg, NonTAUnavail_Study = @NonTAUnavail_Study, NonTAUnavail_StudyP1 = @NonTAUnavail_StudyP1,

	MaintIndex = @MaintIndex, MaintIndex_Avg = @MaintIndex_Avg, MaintIndex_Study = @MaintIndex_Study, MaintIndex_StudyP1 = @MaintIndex_StudyP1, MaintIndex_Target = @MaintIndex_Target,
	TAAdj = @AnnTACost, TAAdj_Avg = @AnnTACost_Avg, TAAdj_Study = @TAAdj_Study, TAAdj_StudyP1 = @TAAdj_StudyP1,
	RoutCost = @RoutCost, RoutCost_Avg = @RoutCost_Avg, RoutCost_Study = @RoutCost_Study, RoutCost_StudyP1 = @RoutCost_StudyP1,

	TotWHrEDC = @PersIndex, TotWHrEDC_Avg = @PersIndex_Avg, TotWHrEDC_Study = @PersIndex_Study, TotWHrEDC_StudyP1 = @PersIndex_StudyP1, TotWHrEDC_Target = @PersIndex_Target,
	CompWHr = @CompWHr, CompWHr_Avg = @CompWHr_Avg, CompWHr_Study = @CompWHr_Study, CompWHr_StudyP1 = @CompWHr_StudyP1,
	ContWHr = @ContWHr, ContWHr_Avg = @ContWHr_Avg, ContWHr_Study = @ContWHr_Study, ContWHr_StudyP1 = @ContWHr_StudyP1,

	NEOpExUEDC = @NEOpExUEDC, NEOpExUEDC_Avg = @NEOpExUEDC_Avg, NEOpExUEDC_Study = @NEOpExUEDC_Study, NEOpExUEDC_StudyP1 = @NEOpExUEDC_StudyP1, NEOpExUEDC_Target = @NEOpExUEDC_Target,
	NEOpEx = @NEOpEx, NEOpEx_Avg = @NEOpEx_Avg, NEOpEx_Study = @NEOpEx_Study, NEOpEx_StudyP1 = @NEOpEx_StudyP1,
	UEDC = @UEDC,  UEDC_Avg = @UEDC_Avg, UEDC_Study = @UEDC_Study, UEDC_StudyP1 = @UEDC_StudyP1,

	TotCashOpExUEDC = @OpExUEDC, TotCashOpExUEDC_Avg = @OpExUEDC_Avg, TotCashOpExUEDC_Study = @OpExUEDC_Study, TotCashOpExUEDC_StudyP1 = @OpExUEDC_StudyP1, TotCashOpExUEDC_Target = @OpExUEDC_Target,
	EnergyCost = @EnergyCost, EnergyCost_Avg = @EnergyCost_Avg, EnergyCost_Study = @EnergyCost_Study, EnergyCost_StudyP1 = @EnergyCost_StudyP1,
	TotCashOpEx = @TotCashOpEx, TotCashOpEx_Avg = @TotCashOpEx_Avg, TotCashOpEx_Study = @TotCashOpEx_Study, TotCashOpEx_StudyP1 = @TotCashOpEx_StudyP1

