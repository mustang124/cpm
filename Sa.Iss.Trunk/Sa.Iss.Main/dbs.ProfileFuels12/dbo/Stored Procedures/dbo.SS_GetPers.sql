﻿CREATE PROC [dbo].[SS_GetPers]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@DataSet nvarchar(20)='ACTUAL'
	
AS

	SELECT RTRIM(PersID) AS PersID,NumPers,AbsHrs,STH,OVTHours,OVTPcnt,Contract,GA,MaintPcnt
             FROM dbo.Pers WHERE  (SubmissionID IN 
             (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1
            AND PersID NOT IN ('OCCTAADJ','MPSTAADJ','OCCTAEXCL','MPSTAEXCL') AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd))))

