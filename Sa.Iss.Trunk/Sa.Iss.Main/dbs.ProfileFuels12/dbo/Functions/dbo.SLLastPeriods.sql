﻿CREATE FUNCTION [dbo].[SLLastPeriods](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE AS
RETURN (
	WITH ls AS (
		SELECT s.RefineryID, s.DataSet, MAX(s.PeriodStart) AS LastPeriod
		FROM @SubmissionList sl INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = sl.SubmissionID
		GROUP BY s.RefineryID, s.DataSet
	)
	SELECT ls.RefineryID, ls.DataSet, s.* 
	FROM ls CROSS APPLY dbo.GetSubmission(ls.RefineryID, DATEPART(YEAR, ls.LastPeriod), DATEPART(MONTH, ls.LastPeriod), ls.DataSet) s
)
