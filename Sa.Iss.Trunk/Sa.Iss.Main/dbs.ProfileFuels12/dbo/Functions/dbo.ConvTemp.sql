﻿create function [dbo].[ConvTemp](@ValToConv float, @FromUnits varchar(255), @ToUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @TempF float, @Result float
	--if  units are the same then return the value
	IF @FromUnits = @ToUnits 
		SET @Result = @ValToConv
	ELSE BEGIN
		-- First, convert temperature to Farenheit
		IF @FromUnits = 'TF' 
			SET @TempF = @ValToConv
		ELSE IF @FromUnits = 'TC' 
			SELECT @TempF = 1.8*@ValToConv+32
		ELSE IF @FromUnits = 'TK' 
			SELECT @TempF = 1.8*(@ValToConv-273)+32
		ELSE IF @FromUnits = 'TR' 
			SELECT @TempF = @ValToConv - 460
		ELSE 
			SELECT @TempF = NULL
		-- Second, convert from F to specified units
		IF @ToUnits = 'TF' 
			SET @Result = @TempF
		ELSE IF @ToUnits = 'TC'
			SELECT @Result = (@TempF-32)/1.8
		ELSE IF @ToUnits = 'TK' 
			SELECT @Result = (@TempF-32)/1.8+273
		ELSE IF @ToUnits = 'TR' 
			SELECT @Result = @TempF + 460
		ELSE
			SET @Result = NULL
	END
	RETURN @Result
END
