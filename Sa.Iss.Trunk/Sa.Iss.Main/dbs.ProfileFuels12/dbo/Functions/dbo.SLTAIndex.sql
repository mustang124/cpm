﻿
CREATE FUNCTION [dbo].[SLTAIndex](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet varchar(8), @Currency char(4))
RETURNS TABLE
AS
RETURN (
	WITH data AS (
	SELECT s.RefineryID, s.DataSet, s.PeriodStart, mi.FactorSet, mi.Currency
		, TAIndex = mi.TAIndex_Avg, TAMatlIndex = mi.TAMatlIndex_Avg, TAEffIndex = mi.TAEffIndex_Avg
		, ftc.EDC, ftc.MaintEffDiv
	FROM MaintIndex mi INNER JOIN FactorTotCalc ftc ON ftc.SubmissionID = mi.SubmissionID AND ftc.FactorSet = mi.FactorSet
	INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = mi.SubmissionID
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = mi.SubmissionID
	WHERE mi.FactorSet = ISNULL(@FactorSet, mi.FactorSet) AND mi.Currency = ISNULL(@Currency, mi.Currency))
	SELECT FactorSet, Currency, TAIndex = [$(dbsGlobal)].dbo.WtAvg(TAIndex, EDC), TAMatlIndex = [$(dbsGlobal)].dbo.WtAvg(TAMatlIndex, EDC), TAEffIndex = [$(dbsGlobal)].dbo.WtAvg(TAEffIndex, MaintEffDiv)
	FROM data INNER JOIN (SELECT RefineryID, MAX(PeriodStart) MaxPeriodStart FROM data GROUP BY RefineryID) lp ON lp.RefineryID = data.RefineryID AND lp.MaxPeriodStart = data.PeriodStart
	GROUP BY FactorSet, Currency
)
