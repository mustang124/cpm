﻿
CREATE   FUNCTION [dbo].[SLAverageEnergyCost](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS @AvgEnergyCost TABLE (Currency varchar(4), TotCostMBTU real NULL, PurCostMBTU real NULL, ProdCostMBTU real NULL, TotCostGJ real NULL, PurCostGJ real NULL, ProdCostGJ real NULL)
AS
BEGIN
	-- Use client Energy prices for all pricing scenarios
	INSERT @AvgEnergyCost (Currency, TotCostMBTU, PurCostMBTU, ProdCostMBTU)
	SELECT c.Currency, TotCostMBTU = CASE WHEN m.TotEnergyConsMBTU > 0 THEN c.TotCostK*1000/m.TotEnergyConsMBTU END
		, PurCostMBTU = CASE WHEN m.PurTotMBTU > 0 THEN c.PurTotCostK*1000/m.PurTotMBTU END
		, ProdCostMBTU = CASE WHEN m.ProdTotMBTU > 0 THEN c.ProdTotCostK*1000/m.ProdTotMBTU END
	FROM dbo.SLSumEnergyCons(@SubmissionList) m, dbo.SLSumEnergyCost(@SubmissionList) c
	
	UPDATE @AvgEnergyCost SET TotCostGJ = TotCostMBTU/1.055, PurCostGJ = PurCostMBTU/1.055, ProdCostGJ = ProdCostMBTU/1.055

RETURN

END

