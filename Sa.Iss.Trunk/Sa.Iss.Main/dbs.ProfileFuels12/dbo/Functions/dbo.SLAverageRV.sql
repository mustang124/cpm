﻿
CREATE FUNCTION [dbo].[SLAverageRV](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet, @Currency dbo.CurrencyCode)
RETURNS TABLE
AS
RETURN (
	WITH MonthlyTotals AS (
	SELECT a.FactorSet, s.PeriodYear, s.PeriodMonth, RefCount = COUNT(*), NumDays = AVG(s.NumDays), FractionOfYear = AVG(s.FractionOfYear)
		, SumRV = SUM(a.RV)
	FROM FactorTotCalc a INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = a.SubmissionID
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	WHERE (a.FactorSet = @FactorSet OR @FactorSet IS NULL)
	GROUP BY a.FactorSet, s.PeriodYear, s.PeriodMonth)
	SELECT FactorSet, AvgRV = [$(dbsGlobal)].dbo.WtAvg(SumRV*dbo.ExchangeRate('USD',@Currency,dbo.BuildDate(PeriodYear, PeriodMonth, 1)), NumDays)
	FROM MonthlyTotals WHERE RefCount = (SELECT MAX(RefCount) FROM MonthlyTotals)
	GROUP BY FactorSet
	)


