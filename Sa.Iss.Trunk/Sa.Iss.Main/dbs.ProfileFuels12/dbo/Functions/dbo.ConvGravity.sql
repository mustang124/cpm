﻿CREATE  function [dbo].[ConvGravity](@ValToConv float, @FromUnits varchar(255), @ToUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @Density float, @Result float
	IF @ToUnits = @FromUnits
		SET @Result = @ValToConv
	ELSE BEGIN
		-- First, convert the density to specific gravity
		IF @FromUnits = 'SG' 
			SELECT @Density = @ValToConv*1000
		ELSE IF @FromUnits = 'API'
		BEGIN
			SELECT @Density = 1000*(141.5/(@ValToConv+131.5))
		END
		ELSE IF @FromUnits = 'KGM3' 
			SELECT @Density = @ValToConv
		ELSE 
			SET @Density = NULL

		-- Second, convert from Specific Gravity to the desired units
		IF @ToUnits = 'SG' 
			SELECT @Result = @Density/1000
		ELSE IF @ToUnits = 'API'
			SELECT @Result = (1000*141.5/@Density)-131.5
		ELSE IF @ToUnits = 'KGM3' 
			SELECT @Result = @Density
		ELSE
			SET @Result = NULL
	END
	RETURN @Result
END
