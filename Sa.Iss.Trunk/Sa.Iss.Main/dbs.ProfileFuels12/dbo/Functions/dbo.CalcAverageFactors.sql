﻿CREATE FUNCTION [dbo].[CalcAverageFactors](@RefineryID varchar(6), @DataSet varchar(15), @FactorSet dbo.FactorSet, @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT a.FactorSet
		, AvgEDC = [$(dbsGlobal)].dbo.WtAvg(a.EDC, s.NumDays)
		, AvgUEDC = [$(dbsGlobal)].dbo.WtAvg(a.UEDC, s.NumDays)
		, SumUEDC = SUM(1.0*a.UEDC*s.NumDays)
		, UtilPcnt = [$(dbsGlobal)].dbo.WtAvg(a.UtilPcnt, EDC*s.NumDays)
		, AvgProcessEDC = [$(dbsGlobal)].dbo.WtAvg(a.TotProcessEDC, s.NumDays)
		, AvgProcessUEDC = [$(dbsGlobal)].dbo.WtAvg(a.TotProcessUEDC, s.NumDays)
		, ProcessUtilPcnt = [$(dbsGlobal)].dbo.WtAvg(a.TotProcessUtilPcnt, TotProcessEDC*s.NumDays)
		, UtilOSTA = [$(dbsGlobal)].dbo.WtAvg(a.UtilOSTA, TotProcessEDC*s.NumDays)
		, EnergyUseDay = [$(dbsGlobal)].dbo.WtAvg(a.EnergyUseDay, s.NumDays)
		, AvgStdEnergy = [$(dbsGlobal)].dbo.WtAvg(a.TotStdEnergy, s.NumDays)
		, EII = [$(dbsGlobal)].dbo.WtAvg(a.EII, a.TotStdEnergy*s.NumDays)
		, LossGainBpD = SUM(a.ReportLossGain)/SUM(s.NumDays*1.0)
		, AvgStdGainBpD = SUM(a.EstGain)/SUM(s.NumDays*1.0)
		, VEI = [$(dbsGlobal)].dbo.WtAvgNN(a.VEI, EstGain)
		, Complexity = [$(dbsGlobal)].dbo.WtAvg(a.Complexity, a.EDC*s.NumDays)
		, NonMaintPersEffDiv = SUM(a.NonMaintPersEffDiv*s.FractionOfYear)
		, MaintPersEffDiv = SUM(a.MaintPersEffDiv*s.FractionOfYear)
		, PersEffDiv = SUM(a.PersEffDiv*s.FractionOfYear)
		, MaintEffDiv = SUM(a.MaintEffDiv*s.FractionOfYear)
		, AvgAnnNonMaintPersEffDiv = [$(dbsGlobal)].dbo.WtAvg(a.NonMaintPersEffDiv,s.NumDays)
		, AvgAnnMaintPersEffDiv = [$(dbsGlobal)].dbo.WtAvg(a.MaintPersEffDiv,s.NumDays)
		, AvgAnnPersEffDiv = [$(dbsGlobal)].dbo.WtAvg(a.PersEffDiv,s.NumDays)
		, AvgAnnMaintEffDiv = [$(dbsGlobal)].dbo.WtAvg(a.MaintEffDiv,s.NumDays)
	FROM FactorTotCalc a INNER JOIN dbo.Submissions s ON s.SubmissionID = a.SubmissionID   
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	AND (a.FactorSet = @FactorSet OR @FactorSet IS NULL)
	GROUP BY a.FactorSet
	)


