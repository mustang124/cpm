﻿
CREATE FUNCTION [dbo].[SLAverageOpExByDataType](@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @DataType varchar(8))
RETURNS @OpEx TABLE (FactorSet varchar(8) NOT NULL, Currency dbo.CurrencyCode NOT NULL, Scenario dbo.Scenario NOT NULL, DataType varchar(8) NOT NULL
	, OCCSal real NULL, MPSSal real NULL, OCCBen real NULL, MPSBen real NULL
	, MaintMatl real NULL, ContMaintMatl real NULL, MaintMatlST real NULL
	, ContMaintLabor real NULL, ContMaintInspect real NULL, ContMaintLaborST real NULL, OthCont real NULL
	, EquipMaint real NULL, EquipNonMaint real NULL, Equip real NULL
	, Tax real NULL, InsurBI real NULL, InsurPC real NULL, InsurOth real NULL, Insur real NULL
	, TAAdj real NULL, Envir real NULL, OthNonVol real NULL, GAPers real NULL, STNonVol real NULL
	, Antiknock real NULL, Chemicals real NULL, Catalysts real NULL, Royalties real NULL
	, PurElec real NULL, PurSteam real NULL, PurOth real NULL, PurFG real NULL, PurLiquid real NULL, PurSolid real NULL, RefProdFG real NULL, RefProdOth real NULL
	, EmissionsPurch real NULL, EmissionsCredits real NULL, EmissionsTaxes real NULL, OthVol real NULL, STVol real NULL
	, TotCashOpEx real NULL
	, GANonPers real NULL, InvenCarry real NULL, Depreciation real NULL, Interest real NULL, STNonCash real NULL, TotRefExp real NULL
	, Cogen real NULL, OthRevenue real NULL
	, ThirdPartyTerminalRM real NULL, ThirdPartyTerminalProd real NULL, POXO2 real NULL, PMAA real NULL
	, FireSafetyLoss real NULL, EnvirFines real NULL, ExclOth real NULL, TotExpExcl real NULL
	, STSal real NULL, STBen real NULL, PersCostExclTA real NULL, PersCost real NULL
	, EnergyCost real NULL, NEOpEx real NULL, Divisor real NULL)
AS BEGIN
	INSERT @OpEx(FactorSet, Currency, Scenario, DataType
		, OCCSal, MPSSal, OCCBen, MPSBen
		, MaintMatl, ContMaintMatl, MaintMatlST
		, ContMaintLabor, ContMaintInspect, ContMaintLaborST, OthCont
		, EquipMaint, EquipNonMaint, Equip
		, Tax, InsurBI, InsurPC, InsurOth, Insur
		, TAAdj
		, Envir, OthNonVol, GAPers
		, STNonVol
		, Antiknock, Chemicals, Catalysts, Royalties
		, PurElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth
		, EmissionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol
		, TotCashOpEx
		, GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp
		, Cogen, OthRevenue
		, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA
		, FireSafetyLoss, EnvirFines, ExclOth, TotExpExcl
		, STSal, STBen, PersCostExclTA
		, PersCost, EnergyCost, NEOpEx, Divisor)
	SELECT x.FactorSet, x.Currency, x.Scenario, x.DataType
		, x.OCCSal, x.MPSSal, x.OCCBen, x.MPSBen
		, x.MaintMatl, x.ContMaintMatl, x.MaintMatlST
		, x.ContMaintLabor, x.ContMaintInspect, x.ContMaintLaborST, x.OthCont
		, x.EquipMaint, x.EquipNonMaint, x.Equip
		, x.Tax, x.InsurBI, x.InsurPC, x.InsurOth, x.Insur
		, TAAdj = ta.PeriodTAAdj/x.Divisor
		, x.Envir, x.OthNonVol, x.GAPers
		, STNonVol = x.STNonVol - x.TAAdj + ta.PeriodTAAdj/x.Divisor
		, x.Antiknock, x.Chemicals, x.Catalysts, x.Royalties
		, x.PurElec, x.PurSteam, x.PurOth, x.PurFG, x.PurLiquid, x.PurSolid, x.RefProdFG, x.RefProdOth
		, x.EmissionsPurch, x.EmissionsCredits, x.EmissionsTaxes, x.OthVol, x.STVol
		, TotCashOpEx = x.TotCashOpEx - x.TAAdj + ta.PeriodTAAdj/x.Divisor
		, x.GANonPers, x.InvenCarry, x.Depreciation, x.Interest, x.STNonCash, x.TotRefExp
		, x.Cogen, x.OthRevenue
		, x.ThirdPartyTerminalRM, x.ThirdPartyTerminalProd, x.POXO2, x.PMAA
		, x.FireSafetyLoss, x.EnvirFines, x.ExclOth, x.TotExpExcl
		, x.STSal, x.STBen, x.PersCostExclTA
		, PersCost = x.PersCostExclTA + 0.55*(ta.PeriodTAAdj/x.Divisor)
		, x.EnergyCost
		, NEOpEx = x.NEOpEx - x.TAAdj + ta.PeriodTAAdj/x.Divisor
		, x.Divisor
	FROM (
	SELECT o.FactorSet, o.Currency, Scenario = ISNULL(@Scenario, o.Scenario), o.DataType
		, OCCSal = [$(dbsGlobal)].dbo.WtAvg(OCCSal, Divisor)
		, MPSSal = [$(dbsGlobal)].dbo.WtAvg(MPSSal, Divisor)
		, OCCBen = [$(dbsGlobal)].dbo.WtAvg(OCCBen, Divisor)
		, MPSBen = [$(dbsGlobal)].dbo.WtAvg(MPSBen, Divisor)
		, MaintMatl = [$(dbsGlobal)].dbo.WtAvg(MaintMatl, Divisor)
		, ContMaintLabor = [$(dbsGlobal)].dbo.WtAvg(ContMaintLabor, Divisor)
		, ContMaintInspect = [$(dbsGlobal)].dbo.WtAvg(ContMaintInspect, Divisor)
		, ContMaintLaborST = [$(dbsGlobal)].dbo.WtAvg(ContMaintLaborST, Divisor)
		, ContMaintMatl = [$(dbsGlobal)].dbo.WtAvg(ContMaintMatl, Divisor)
		, MaintMatlST = [$(dbsGlobal)].dbo.WtAvg(MaintMatlST, Divisor)
		, OthCont = [$(dbsGlobal)].dbo.WtAvg(OthCont, Divisor)
		, EquipMaint = [$(dbsGlobal)].dbo.WtAvg(EquipMaint, Divisor)
		, EquipNonMaint = [$(dbsGlobal)].dbo.WtAvg(EquipNonMaint, Divisor)
		, Equip = [$(dbsGlobal)].dbo.WtAvg(Equip, Divisor)
		, Tax = [$(dbsGlobal)].dbo.WtAvg(Tax, Divisor)
		, InsurBI = [$(dbsGlobal)].dbo.WtAvg(InsurBI, Divisor)
		, InsurPC = [$(dbsGlobal)].dbo.WtAvg(InsurPC, Divisor)
		, InsurOth = [$(dbsGlobal)].dbo.WtAvg(InsurOth, Divisor)
		, Insur = [$(dbsGlobal)].dbo.WtAvg(Insur, Divisor)
		, TAAdj = [$(dbsGlobal)].dbo.WtAvg(TAAdj, Divisor)
		, Envir = [$(dbsGlobal)].dbo.WtAvg(Envir, Divisor)
		, OthNonVol = [$(dbsGlobal)].dbo.WtAvg(OthNonVol, Divisor)
		, GAPers = [$(dbsGlobal)].dbo.WtAvg(GAPers, Divisor)
		, STNonVol = [$(dbsGlobal)].dbo.WtAvg(STNonVol, Divisor)
		, Antiknock = [$(dbsGlobal)].dbo.WtAvg(Antiknock, Divisor)
		, Chemicals = [$(dbsGlobal)].dbo.WtAvg(Chemicals, Divisor)
		, Catalysts = [$(dbsGlobal)].dbo.WtAvg(Catalysts, Divisor)
		, Royalties = [$(dbsGlobal)].dbo.WtAvg(Royalties, Divisor)
		, PurElec = [$(dbsGlobal)].dbo.WtAvg(PurElec, Divisor)
		, PurSteam = [$(dbsGlobal)].dbo.WtAvg(PurSteam, Divisor)
		, PurOth = [$(dbsGlobal)].dbo.WtAvg(PurOth, Divisor)
		, PurFG = [$(dbsGlobal)].dbo.WtAvg(PurFG, Divisor)
		, PurLiquid = [$(dbsGlobal)].dbo.WtAvg(PurLiquid, Divisor)
		, PurSolid = [$(dbsGlobal)].dbo.WtAvg(PurSolid, Divisor)
		, RefProdFG = [$(dbsGlobal)].dbo.WtAvg(RefProdFG, Divisor)
		, RefProdOth = [$(dbsGlobal)].dbo.WtAvg(RefProdOth, Divisor)
		, EmissionsPurch = [$(dbsGlobal)].dbo.WtAvg(EmissionsPurch, Divisor)
		, EmissionsCredits = [$(dbsGlobal)].dbo.WtAvg(EmissionsCredits, Divisor)
		, EmissionsTaxes = [$(dbsGlobal)].dbo.WtAvg(EmissionsTaxes, Divisor)
		, OthVol = [$(dbsGlobal)].dbo.WtAvg(OthVol, Divisor)
		, STVol = [$(dbsGlobal)].dbo.WtAvg(STVol, Divisor)
		, TotCashOpEx = [$(dbsGlobal)].dbo.WtAvg(TotCashOpEx, Divisor)
		, GANonPers = [$(dbsGlobal)].dbo.WtAvg(GANonPers, Divisor)
		, InvenCarry = [$(dbsGlobal)].dbo.WtAvg(InvenCarry, Divisor)
		, Depreciation = [$(dbsGlobal)].dbo.WtAvg(Depreciation, Divisor)
		, Interest = [$(dbsGlobal)].dbo.WtAvg(Interest, Divisor)
		, STNonCash = [$(dbsGlobal)].dbo.WtAvg(STNonCash, Divisor)
		, TotRefExp = [$(dbsGlobal)].dbo.WtAvg(TotRefExp, Divisor)
		, Cogen = [$(dbsGlobal)].dbo.WtAvg(Cogen, Divisor)
		, OthRevenue = [$(dbsGlobal)].dbo.WtAvg(OthRevenue, Divisor)
		, ThirdPartyTerminalRM = [$(dbsGlobal)].dbo.WtAvg(ThirdPartyTerminalRM, Divisor)
		, ThirdPartyTerminalProd = [$(dbsGlobal)].dbo.WtAvg(ThirdPartyTerminalProd, Divisor)
		, POXO2 = [$(dbsGlobal)].dbo.WtAvg(POXO2, Divisor)
		, PMAA = [$(dbsGlobal)].dbo.WtAvg(PMAA, Divisor)
		, FireSafetyLoss = [$(dbsGlobal)].dbo.WtAvg(FireSafetyLoss, Divisor)
		, EnvirFines = [$(dbsGlobal)].dbo.WtAvg(EnvirFines, Divisor)
		, ExclOth = [$(dbsGlobal)].dbo.WtAvg(ExclOth, Divisor)
		, TotExpExcl = [$(dbsGlobal)].dbo.WtAvg(TotExpExcl, Divisor)
		, STSal = [$(dbsGlobal)].dbo.WtAvg(STSal, Divisor)
		, STBen = [$(dbsGlobal)].dbo.WtAvg(STBen, Divisor)
		, PersCostExclTA = [$(dbsGlobal)].dbo.WtAvg(PersCostExclTA, Divisor)
		, PersCost = [$(dbsGlobal)].dbo.WtAvg(PersCost, Divisor)
		, EnergyCost = [$(dbsGlobal)].dbo.WtAvg(EnergyCost, Divisor)
		, NEOpEx = [$(dbsGlobal)].dbo.WtAvg(NEOpEx, Divisor)
		, Divisor = SUM(Divisor)
	FROM OpExCalc o INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = o.SubmissionID
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	WHERE o.Scenario = 'CLIENT' AND o.FactorSet = ISNULL(@FactorSet, o.FactorSet) AND o.Currency = ISNULL(@Currency, o.Currency) AND o.DataType = ISNULL(@DataType, o.DataType)
	GROUP BY o.FactorSet, o.Currency, o.Scenario, o.DataType) x
	INNER JOIN dbo.SLTAAdj(@SubmissionList, @FactorSet, @Currency) ta ON ta.FactorSet = x.FactorSet AND ta.Currency = x.Currency
	
	RETURN
END

