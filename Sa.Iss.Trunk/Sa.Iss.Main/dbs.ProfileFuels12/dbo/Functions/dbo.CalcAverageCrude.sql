﻿CREATE FUNCTION [dbo].[CalcAverageCrude](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT TotBbl = SUM(TotBbl), TotMT = SUM(TotMT)
		, Density = [$(dbsGlobal)].dbo.WtAvg(AvgDensity, TotMT)
		, Gravity = dbo.KGM3toAPI([$(dbsGlobal)].dbo.WtAvg(AvgDensity, TotMT))
		, Sulfur = [$(dbsGlobal)].dbo.WtAvgNZ(AvgSulfur,TotMT)
	FROM CrudeTot c INNER JOIN dbo.Submissions s ON s.SubmissionID = c.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.UseSubmission = 1
	AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
)


