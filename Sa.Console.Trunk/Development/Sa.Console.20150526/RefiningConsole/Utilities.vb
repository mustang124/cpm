﻿Imports System.IO
Imports System.Collections.Generic
Imports System.Security.Cryptography
Imports System.Text
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports Word = Microsoft.Office.Interop.Word
Imports System.Runtime.InteropServices
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.Core
Imports Ionic.Zip
Imports System.Threading
Imports System.Configuration


Public Class Utilities
    'Dim err As ErrorLog()


    Public Shared Function GetLongRefnum(shortRefNum As String) As String
        Select Case ConsoleVertical
            Case VerticalType.OLEFINS Or VerticalType.SEEC
                Return "20" + shortRefNum
            Case VerticalType.REFINING
                Return shortRefNum
            Case VerticalType.NGPP
                Return shortRefNum
            Case VerticalType.LNG
                Return shortRefNum
        End Select


    End Function

    Public Shared Function FileNameWithoutExtension(fileNameWithoutFolder As String) As String
        Dim nameParts() As String = fileNameWithoutFolder.Split(".")
        Dim result As String = String.Empty
        For count As Integer = 0 To nameParts.Length - 2
            result += nameParts(count) + "."
        Next
        result = result.Remove(result.Length - 1)
        Return result
    End Function

    Public Shared Function GetFileExtension(fileName As String) As String
        Dim parts As String() = fileName.Split(".")
        Return parts(parts.Length - 1)
    End Function

    Public Shared Function ConvertToDoc(TempPath As String, ByVal in_file As String) As String
        Const FORMAT_DOC As Integer = 0
        Dim out_file As String = Utilities.ExtractFileName(in_file)
        out_file = TempPath & out_file.Substring(0, out_file.LastIndexOf(".")) & ".doc"
        Dim word_server As Object ' Word.Application

        word_server = CreateObject("Word.Application")

        ' Open the input file.
        word_server.Documents.Open( _
            FileName:=in_file, _
            ReadOnly:=False, _
            AddToRecentFiles:=False)

        ' Save the output file.
        word_server.ActiveDocument.SaveAs( _
            FileName:=out_file, _
            FileFormat:=FORMAT_DOC, _
            LockComments:=False, _
            AddToRecentFiles:=True)

        ' Exit the server without prompting.
        word_server.ActiveDocument.Close(False)
        word_server = Nothing
        Return out_file
    End Function
    Public Shared Function AddDateStamp(f As String) As String
        Dim rtn As String
        Dim fn As String
        Dim ex As String

        fn = f.Substring(0, f.IndexOf("."))
        ex = f.Substring(f.IndexOf(".") + 1, f.Length - f.IndexOf(".") - 1)

        rtn = fn & " " & Today.ToString("dd-MMM-yy") & "." & ex
        Return rtn
    End Function
    Public Shared Sub SendEmail(strTo As String, strCC As String, subj As String, body As String, attachments As List(Of String), mode As Boolean, Optional bcc As String = "")
        Dim outlook As Outlook.Application
        Dim outlookprocess As Process

        If strTo.Length < 3 Then Exit Sub
        outlookprocess = Utilities.CheckProcess("C:\Program Files (x86)\Microsoft Office\Office14\", "Outlook")
        'outlookprocess = Utilities.CheckProcess("C:\Program Files (x86)\Microsoft Office\root\Office16", "Outlook")
        Try

            If outlookprocess IsNot Nothing Then
                outlook = DirectCast(Marshal.GetActiveObject("Outlook.Application"), Outlook.Application)
            Else
                outlook = New Outlook.Application
            End If

            Dim AddDays As String = ""

            Dim msg As Outlook.MailItem

            msg = outlook.CreateItem(OlItemType.olMailItem)

            msg.To = strTo
            If msg.To <> strCC And strCC <> "" Then
                msg.CC = strCC
            End If
            If bcc.Length > 0 Then
                msg.BCC = bcc
            End If

            msg.Subject = subj
            Dim strSignatureFile As String = CStr(Environ("USERPROFILE")) & "\AppData\Roaming\Microsoft\Signatures\SolomonBlue.txt"
            'Read the specific signature file
            Dim objFileSystem = CreateObject("Scripting.FileSystemObject")
            Dim objTextStream = objFileSystem.OpenTextFile(strSignatureFile)
            Dim strSignature = objTextStream.ReadAll
            msg.Body = body & strSignature
            If attachments IsNot Nothing Then
                For Each att In attachments
                    Try
                        msg.Attachments.Add(att)
                    Catch exAdd As System.Exception
                        'sometimes an attachment gets added to a different email and deleted, but not removed from the attachments collection
                        Dim exAddMsg As String = exAdd.Message
                    End Try
                Next
            End If
            If mode Then
                msg.Display(False)
            Else
                msg.Send()
                outlook = Nothing
            End If

            msg = Nothing
        Catch ex As System.Exception
            'MsgBox(ex.Message)
            MessageBox.Show("Error Launching OUTLOOK.  Must launch Outlook manually then try and resend.", "Error")
        End Try

    End Sub
    Public Shared Function EncryptedPassword(aString As String, salt As String) As String

        Dim EncryptionIV As String = "SACTW0S1G2L3C4D5B6R7M8F9"
        Dim PartialKey As String = "SOALCDMSCCHOOL1996DUSTYRHODES"

        ' Note that the key and IV must be the same for the encrypt and decrypt calls.
        Dim results As String
        Dim tdesEngine As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        Dim key As String = salt + PartialKey
        Dim keyBytes As Byte() = Encoding.ASCII.GetBytes(key.Substring(0, 24))
        Dim ivBytes As Byte() = Encoding.ASCII.GetBytes(EncryptionIV)

        Dim transform As ICryptoTransform = tdesEngine.CreateEncryptor(keyBytes, ivBytes)
        Dim mesg As Byte() = Encoding.ASCII.GetBytes(aString)
        Dim ecn As Byte() = transform.TransformFinalBlock(mesg, 0, mesg.Length)

        results = Convert.ToBase64String(ecn)

        Return results
    End Function
    Public Shared Function CleanFileName(strFileName As String) As String
        Dim strBadChar As String
        Dim strRepChar As String
        Dim I As Integer
        Dim J As Integer
        Dim blnChanged As Boolean
        blnChanged = False

        strBadChar = "\/:*?""<>|%"
        ' A
        strBadChar = strBadChar & Chr(192)
        strBadChar = strBadChar & Chr(193)
        strBadChar = strBadChar & Chr(194)
        strBadChar = strBadChar & Chr(195)
        strBadChar = strBadChar & Chr(196)
        strBadChar = strBadChar & Chr(197)
        strBadChar = strBadChar & Chr(198)
        ' E
        strBadChar = strBadChar & Chr(200)
        strBadChar = strBadChar & Chr(201)
        strBadChar = strBadChar & Chr(202)
        strBadChar = strBadChar & Chr(203)
        ' I
        strBadChar = strBadChar & Chr(204)
        strBadChar = strBadChar & Chr(205)
        strBadChar = strBadChar & Chr(206)
        strBadChar = strBadChar & Chr(207)
        ' O
        strBadChar = strBadChar & Chr(210)
        strBadChar = strBadChar & Chr(211)
        strBadChar = strBadChar & Chr(212)
        strBadChar = strBadChar & Chr(213)
        strBadChar = strBadChar & Chr(214)
        ' U
        strBadChar = strBadChar & Chr(217)
        strBadChar = strBadChar & Chr(218)
        strBadChar = strBadChar & Chr(219)
        strBadChar = strBadChar & Chr(220)

        ' a
        strBadChar = strBadChar & Chr(224)
        strBadChar = strBadChar & Chr(225)
        strBadChar = strBadChar & Chr(226)
        strBadChar = strBadChar & Chr(227)
        strBadChar = strBadChar & Chr(228)
        strBadChar = strBadChar & Chr(229)
        ' e
        strBadChar = strBadChar & Chr(232)
        strBadChar = strBadChar & Chr(233)
        strBadChar = strBadChar & Chr(234)
        strBadChar = strBadChar & Chr(235)
        ' i
        strBadChar = strBadChar & Chr(236)
        strBadChar = strBadChar & Chr(237)
        strBadChar = strBadChar & Chr(238)
        strBadChar = strBadChar & Chr(239)
        ' o
        strBadChar = strBadChar & Chr(242)
        strBadChar = strBadChar & Chr(243)
        strBadChar = strBadChar & Chr(244)
        strBadChar = strBadChar & Chr(245)
        strBadChar = strBadChar & Chr(246)
        ' u
        strBadChar = strBadChar & Chr(249)
        strBadChar = strBadChar & Chr(250)
        strBadChar = strBadChar & Chr(251)
        strBadChar = strBadChar & Chr(252)

        strRepChar = "  -      AAAAAAAEEEEIIIIOOOOOUUUUaaaaaaeeeeiiiiooooouuuu"
        For I = 1 To Len(strBadChar)
            For J = 1 To Len(strFileName)
                If Mid(strFileName, J, 1) = Mid(strBadChar, I, 1) Then
                    Mid(strFileName, J, 1) = Mid(strRepChar, I, 1)
                    blnChanged = True
                End If
            Next J
        Next I
        CleanFileName = Trim(strFileName)


    End Function


    Public Shared Function DecryptPassword(aString As String, salt As String)

        Dim EncryptionIV As String = "SACTW0S1G2L3C4D5B6R7M8F9"
        Dim PartialKey As String = "SOALCDMSCCHOOL1996DUSTYRHODES"
        Dim results As String
        aString = aString.Replace(" ", "+")
        Dim tdesEngine As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        Dim key As String = salt + PartialKey
        Dim keyBytes As Byte() = Encoding.ASCII.GetBytes(key.Substring(0, 24))
        Dim ivBytes As Byte() = Encoding.ASCII.GetBytes(EncryptionIV)

        Dim transform As ICryptoTransform = tdesEngine.CreateEncryptor(keyBytes, ivBytes)
        Dim mesg As Byte() = Convert.FromBase64String(aString)

        Dim ecn As Byte() = transform.TransformFinalBlock(mesg, 0, mesg.Length)

        results = Encoding.ASCII.GetString(ecn)
        Return results

    End Function


    Public Shared Function ExtractFileName(fn As String) As String
        Dim ret As String
        ret = fn.Substring(fn.LastIndexOf("\") + 1, fn.Length - fn.LastIndexOf("\") - 1)
        Return ret
    End Function
    Public Shared Function Pad(str As String, space As Integer) As String
        Dim len As Integer = str.Length
        If len > space Then Return str.Substring(0, space)
        For i = len To space
            str += " "
        Next
        Return str
    End Function
    Public Shared Function GetExcelProcess() As Excel.Application
        Dim process As Excel.Application
        Try
            process = CType(GetObject(, "Excel.Application"), Excel.Application)
        Catch ex As System.Exception
            process = New Excel.Application
        End Try

        Return process
    End Function
    Public Shared Function GetWordProcess() As Word.Application
        Dim process As Word.Application
        Try
            process = CType(GetObject(, "Word.Application"), Word.Application)
        Catch ex As System.Exception
            process = New Word.Application
        End Try
        Return process
    End Function

    Public Shared Function PadWith(str As String, chr As String, replications As Integer) As String
        Dim len As Integer = str.Length
        If len > replications Then Return str.Substring(0, replications)
        For i = len To replications
            str = chr + str
        Next
        Return str
    End Function
    Public Shared Function GetFileFromLink(link As String) As String
        Dim ret As String = Nothing
        '
        ' Initialize
        Dim wshLink, strPath, wshShell
        strPath = link
        '
        ' Create Windows Scripting Host Object
        wshShell = CreateObject("WScript.Shell")
        '
        ' Get Shortcut
        wshLink = wshShell.CreateShortcut(strPath)
        '
        ' Display Target Path
        ret = wshLink.TargetPath
        '
        ' Housekeeping
        wshLink = Nothing
        wshShell = Nothing
        '
        Return ret
    End Function



    Public Shared Function CheckUser(_Username As String) As Boolean
        Try
            Dim configValues As String = ConfigurationManager.AppSettings("ContactFormAdminUsers").ToString()
            Dim adminUsers() As String = configValues.Split("|")
            For count As Integer = 0 To adminUsers.Length - 1
                If adminUsers(count).ToUpper.Trim() = _Username.ToUpper().Trim() Then
                    Return True
                End If
            Next
        Catch
        End Try

        'If _Username.ToUpper() = "DBB" _
        '            Or _Username = "EJE" _
        '            Or _Username = "BLT" _
        '            Or _Username = "JKZ" _
        '            Or _Username = "SWP" _
        '            Or _Username = "JDW" _
        '            Or _Username = "JAB" _
        '            Or _Username = "SAM" Then

        '    Return True
        'Else
        Return False
        'End If

    End Function
    Public Shared Sub UnZipFiles(zfile As String, tempdir As String, pw As String)
        Dim UnpackDirectory As String = "Extracted Files"
        Using zip1 As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(zfile)
            Dim e As Ionic.Zip.ZipEntry
            ' here, we extract every entry, but we could extract conditionally,
            ' based on entry name, size, date, checkbox status, etc.   
            For Each e In zip1
                Try
                    e.ExtractWithPassword(tempdir & UnpackDirectory, ExtractExistingFileAction.OverwriteSilently, pw)
                Catch
                    Try
                        e.Extract(tempdir & UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                    Catch ex2 As System.Exception
                        Dim msg As String = "Error in UnZipFiles. "
                        If pw.Length < 1 Then
                            msg += "Possibly because the password is blank. "
                        End If
                        msg += "Details: " + ex2.Message
                        MsgBox(msg)
                    End Try
                End Try

            Next
        End Using
    End Sub

    Public Shared Function FixQuotes(strString As String) As String
        ' If there is a single quote in the subject or body of the email,
        ' you must make it two single quotes together. If you don't,
        ' you will end up with an empty email!!!
        Dim I As Integer
        For I = 1 To Len(strString) - 1
            ' Is this character a single quote?
            If Mid(strString, I, 1) = Chr(39) Then
                ' Is the next character a single quote?
                If Mid(strString, I + 1, 1) = Chr(39) Then
                    ' Yes, just index past it.
                    I = I + 1
                Else
                    ' No, insert another single quote right after this one.
                    strString = Left(strString, I) & Chr(39) & Right(strString, Len(strString) - I)
                    ' And index past the single quote we just inserted.
                    I = I + 1
                End If
            End If
        Next I
        ' If the last character of the string is a quote,
        ' then put one extra quote on the end.
        If Right(strString, 1) = Chr(39) Then
            strString = strString & Chr(39)
        End If
        ' Return the repaired string 
        FixQuotes = strString.Replace("/", "-")
        FixQuotes = FixQuotes.Replace(",", " ")
    End Function
    Public Shared Function isNumeric(val As String) As Boolean

        Dim result As Integer
        Return Integer.TryParse(val, result)

    End Function
    Public Shared Function Zipfile(filesPath As String, password As String, outPathName As String) As String

        Dim fsOut As FileStream = System.IO.File.Create(outPathName)
        Dim zipStream As New ICSharpCode.SharpZipLib.Zip.ZipOutputStream(fsOut)

        zipStream.SetLevel(3)       '0-9, 9 being the highest level of compression
        zipStream.Password = password   ' optional. Null is the same as not setting.

        ' This setting will strip the leading part of the folder path in the entries, to
        ' make the entries relative to the starting folder.
        ' To include the full path for each entry up to the drive root, assign folderOffset = 0.
        Dim folderOffset As Integer = filesPath.Length + (If(filesPath.EndsWith("\"), 0, 1))

        CompressFolder(filesPath, zipStream, folderOffset)

        zipStream.IsStreamOwner = True
        ' Makes the Close also Close the underlying stream
        zipStream.Close()
        Return outPathName

    End Function
    ' Recurses down the folder structure
    '
    Public Shared Sub CompressFolder(path As String, zipStream As ICSharpCode.SharpZipLib.Zip.ZipOutputStream, folderOffset As Integer)

        Dim files As String() = Directory.GetFiles(path)

        For Each filename As String In files

            Dim fi As New FileInfo(filename)

            Dim entryName As String = filename.Substring(folderOffset)  ' Makes the name in zip based on the folder
            entryName = ICSharpCode.SharpZipLib.Zip.ZipEntry.CleanName(entryName)       ' Removes drive from name and fixes slash direction
            Dim newEntry As New ICSharpCode.SharpZipLib.Zip.ZipEntry(entryName)
            newEntry.DateTime = fi.LastWriteTime            ' Note the zip format stores 2 second granularity

            ' Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
            '   newEntry.AESKeySize = 256;

            ' To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
            ' you need to do one of the following: Specify UseZip64.Off, or set the Size.
            ' If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
            ' but the zip will be in Zip64 format which not all utilities can understand.
            '   zipStream.UseZip64 = UseZip64.Off;
            newEntry.Size = fi.Length

            zipStream.PutNextEntry(newEntry)

            ' Zip the file in buffered chunks
            ' the "using" will close the stream even if an exception occurs
            Dim buffer As Byte() = New Byte(4095) {}
            Using streamReader As FileStream = File.OpenRead(filename)
                StreamUtils.Copy(streamReader, zipStream, buffer)
            End Using
            zipStream.CloseEntry()
        Next
        'Dim folders As String() = Directory.GetDirectories(path)
        'For Each folder As String In folders
        '    CompressFolder(folder, zipStream, folderOffset)
        'Next
    End Sub
    Public Shared Function XOREncryption(CodeKey As String, DataIn As String) As String
        Dim lonDataPtr As Long
        Dim strDataOut As String = ""
        Dim intXOrValue1 As Integer, intXOrValue2 As Integer

        If CodeKey <> "" Then
            For lonDataPtr = 1 To Len(DataIn)
                'The first value to be XOr-ed comes from
                '     the data to be encrypted
                intXOrValue1 = Asc(Mid$(DataIn, lonDataPtr, 1))
                'The second value comes from the code ke
                '     y
                intXOrValue2 = Asc(Mid$(CodeKey, ((lonDataPtr Mod Len(CodeKey)) + 1), 1))
                If (intXOrValue1 Xor intXOrValue2) < 33 Then
                    strDataOut += Chr((intXOrValue1 Xor intXOrValue2) + 33)
                ElseIf (intXOrValue1 Xor intXOrValue2) > 126 Then
                    strDataOut = strDataOut + Chr((intXOrValue1 Xor intXOrValue2) - 33)
                Else
                    strDataOut = strDataOut + Chr(intXOrValue1 Xor intXOrValue2)
                End If
            Next lonDataPtr
        End If
        XOREncryption = strDataOut
    End Function
    Public Shared Sub DeleteFiles(dir As String)

        Dim di As New System.IO.DirectoryInfo(dir)
        Dim fi() As FileInfo = di.GetFiles()
        Try

            For Each f In fi
                'If Not f.FullName.Contains("IDR Instructions") Then f.Delete()
            Next
        Catch ex As System.Exception
            Dim dr As New DialogResult()
            dr = MessageBox.Show(ex.Message & vbCrLf & "You have a document open in memory that needs to be deleted." & vbCrLf & "The document is stuck open in memory and needs to be shut down." & vbCrLf & "<SAVE>  any Word or Excel documents you have open and then click OK when ready", "Error Deleting Temp Files", MessageBoxButtons.OKCancel)
            If dr = DialogResult.OK Then
                KillProcesses("WinWord")
                KillProcesses("Excel")
            End If
        End Try
    End Sub
    Public Shared Sub KillProcesses(p As String)
        Dim pProcess() As Process = System.Diagnostics.Process.GetProcessesByName(p)

        For Each ep As Process In pProcess
            Try
                ep.Kill()
            Catch
            End Try
        Next

    End Sub
    Public Shared Function FileTooBig(fn As String) As Boolean
        Dim file As New FileInfo(fn)
        Dim sizeInBytes As Long = file.Length
        If sizeInBytes > 1048576 And fn.ToUpper.Contains("RETURN") Then
            Return True
        End If
        Return False
    End Function
    Public Shared Function DropQuotes(strString As String) As String
        ' If there is a single quote in the supplied string, drop it.
        Dim I As Integer
        For I = 1 To Len(strString)
            ' Is this character a single quote?
            If Mid(strString, I, 1) = Chr(39) Then
                ' Drop the single quote
                strString = Left(strString, I - 1) & Right(strString, Len(strString) - I)
                ' Back up one character in case they have 2 together
                I = I - 1
            End If
        Next I
        ' If the last character of the string is a quote,
        ' then drop it.
        Do While Right(strString, 1) = Chr(39)
            strString = Left(strString, Len(strString) - 1)
        Loop
        ' Return the repaired string
        DropQuotes = strString
    End Function
    Public Shared Sub WordTemplateReplace(strfilePath As String, fields As SA.Console.Common.WordTemplate, strfilenamePath As String, mode As Integer)

        'THIS DOES NOT USE MFILES, IS LEGACY

        'Olefins calls codebase local to itsself

        Dim fpath As String = String.Empty
        Dim first, second As Integer
        Dim oWord As Object
        Dim oDoc As Object
        Dim range As Object
        Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
        For count = 0 To fields.Field.Count - 1
            DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
        Next
        If mode = 1 Then MessageBox.Show(DoubleCheck, "Template Field Check")

        'strfilePath
        'If Not File.Exists(strfilenamePath) Then - SB changed 2016-11-04
        If Not File.Exists(strfilePath) Then
            MsgBox("Unable to find the IDR Template file at " + strfilePath + ". ") ' strfilenamePath + ". ")
            Exit Sub
        End If

        'copy to K: with new name
        'If Not File.Exists(strfilenamePath) AndAlso File.Exists(strfilenamePath) Then  - SB changed 2016-11-04
        If Not File.Exists(strfilenamePath) AndAlso File.Exists(strfilePath) Then
            File.Copy(strfilePath, strfilenamePath)
        Else
            If mode = 1 Then
                'If File.Exists(strfilenamePath) Then
                Dim warning As String = "CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document." & Environment.NewLine & Environment.NewLine
                'strfilenamePath = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilenamePath)
                'Else
                first = strfilenamePath.LastIndexOf("\") + 1
                second = strfilenamePath.Length - first
                fpath = strfilenamePath.Substring(0, first)
                warning = warning & "Here is the suggested name for this New Vetted Fax document." & Chr(10) &
                "Click OK to accept it, or change, then click OK."
                strfilenamePath = InputBox(warning, "SaveAs", strfilenamePath.Substring(first, second))
                'End If
                ' If they hit cancel, just stop.
                If strfilenamePath = "" Then
                    Exit Sub
                Else
                    strfilenamePath = fpath & strfilenamePath
                    Try
                        File.Copy(strfilePath, strfilenamePath, True)
                    Catch exOVerwrite As System.Exception
                        MsgBox("Unable to overwrite file " + fpath & strfilenamePath)
                    End Try
                End If
            End If
        End If
        oWord = Utilities.GetWordProcess() ' CreateObject("Word.Application")
        If mode <> 1 Then
            oWord.Visible = False
        Else
            oWord.Visible = True
        End If

        oDoc = oWord.Documents.Open(strfilenamePath)  'strfile

        range = oDoc.Content
        Try
            For count = 0 To fields.Field.Count - 1
                With oWord.Selection.Find
                    .Text = fields.Field(count)
                    .Replacement.Text = fields.RField(count)
                    .Forward = True
                    .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                    .Format = False
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
            Next

            If Not mode = 1 Then
                oDoc.Close()
                oWord.Visible = False
                If Not oWord Is Nothing Then
                    oWord = Nothing
                End If
            End If


            'If ConsoleVertical = VerticalType.REFINING Then
            '    'moved parts of legacy code to calling method
            '    oDoc.SaveAs(strfilenamePath, ADDTORECENTFILES:=True)
            '    If Not mode = 1 Then
            '        oDoc.Close()
            '        If Not oWord Is Nothing Then
            '            Marshal.FinalReleaseComObject(oWord)
            '            oWord = Nothing
            '        End If
            '    End If
            'Else  'legacy
            '    If mode = 1 Then
            '        If File.Exists(strfilenamePath) Then
            '            strfilenamePath = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilenamePath)
            '        Else
            '            first = strfilenamePath.LastIndexOf("\") + 1
            '            second = strfilenamePath.Length - first
            '            fpath = strfilenamePath.Substring(0, first)
            '            If mode = 1 Then
            '                strfilenamePath = InputBox("Here is the suggested name for this New Vetted Fax document." & Chr(10) & _
            '                "Click OK to accept it, or change, then click OK.", "SaveAs", strfilenamePath.Substring(first, second))
            '            Else
            '                strfilenamePath = strfilenamePath.Substring(first, second)
            '            End If
            '        End If
            '        ' If they hit cancel, just stop.
            '        If strfilenamePath = "" Then Exit Sub
            '        '
            '        oDoc.SaveAs(fpath & strfilenamePath, ADDTORECENTFILES:=True)
            '        'MessageBox.Show("Name is now " & oDoc.Name)
            '    Else
            '        oDoc.SaveAs(strfilenamePath, ADDTORECENTFILES:=True)
            '        oDoc.Close()
            '        If Not oWord Is Nothing Then
            '            Marshal.FinalReleaseComObject(oWord)
            '            oWord = Nothing
            '        End If

            '    End If
            'End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Shared Function WordTemplateReplace2(strfilePath As String, fields As SA.Console.Common.WordTemplate,
                                                strfilenamePath As String, mode As Integer, ByRef finalPath As String,
                                                ByRef wordApp As Microsoft.Office.Interop.Word.Application) As Boolean

        'Olefins calls codebase local to itsself

        Dim fpath As String = String.Empty
        Dim first, second As Integer
        Dim oWord As Object
        Dim oDoc As Object
        Dim range As Object
        Try
            Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
            For count = 0 To fields.Field.Count - 1
                DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
            Next
            If mode = 1 Then MessageBox.Show(DoubleCheck, "Template Field Check")

            'strfilePath
            'If Not File.Exists(strfilenamePath) Then - SB changed 2016-11-04
            If Not File.Exists(strfilePath) Then
                MsgBox("Unable to find the IDR Template file at " + strfilePath + ". ") ' strfilenamePath + ". ")
                Exit Function
            End If

            'copy to K: with new name
            If Not File.Exists(strfilenamePath) AndAlso File.Exists(strfilePath) Then
                File.Copy(strfilePath, strfilenamePath)
            Else
                If mode = 1 Then
                    'If File.Exists(strfilenamePath) Then
                    Dim warning As String = "CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document." & Environment.NewLine & Environment.NewLine
                    'strfilenamePath = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilenamePath)
                    'Else
                    first = strfilenamePath.LastIndexOf("\") + 1
                    second = strfilenamePath.Length - first
                    fpath = strfilenamePath.Substring(0, first)
                    warning = warning & "Here is the suggested name for this New Vetted Fax document." & Chr(10) &
                    "Click OK to accept it, or change, then click OK."
                    strfilenamePath = InputBox(warning, "SaveAs", strfilenamePath.Substring(first, second))
                    'End If
                    ' If they hit cancel, just stop.
                    If strfilenamePath = "" Then
                        Exit Function
                    Else
                        strfilenamePath = fpath & strfilenamePath
                        Try
                            File.Copy(strfilePath, strfilenamePath, True)
                        Catch exOVerwrite As System.Exception
                            MsgBox("Unable to overwrite file " + fpath & strfilenamePath)
                        End Try
                    End If
                End If
            End If
            oWord = Utilities.GetWordProcess() ' CreateObject("Word.Application")
            If mode <> 1 Then
                oWord.Visible = False
            Else
                oWord.Visible = True
            End If

            oDoc = oWord.Documents.Open(strfilenamePath)  'strfile
            finalPath = strfilenamePath
            range = oDoc.Content
            Try
                For count = 0 To fields.Field.Count - 1
                    With oWord.Selection.Find
                        .Text = fields.Field(count)
                        .Replacement.Text = fields.RField(count)
                        .Forward = True
                        .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                        .Format = False
                        .MatchCase = False
                        .MatchWholeWord = False
                        .MatchWildcards = False
                        .MatchSoundsLike = False
                        .MatchAllWordForms = False
                    End With
                    oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
                Next

                If Not mode = 1 Then
                    oDoc.Close()
                    oWord.Visible = False
                    If Not oWord Is Nothing Then
                        oWord = Nothing
                    End If
                Else
                    wordApp = oWord
                End If
            Catch ex As System.Exception
                MessageBox.Show(ex.Message)
            End Try

            Return True
        Catch exOuter As System.Exception
            MsgBox("Error in WordTemplateReplace2: " & exOuter.Message)
            Return False
        End Try
    End Function
    Public Shared Function WordDocIsEncrypted(file As String) As Boolean
        Dim wordApp As Word.Application
        wordApp = Utilities.GetWordProcess()
        Dim doc As Word.Document = Nothing
        Try
            'NOTE: If doc not password protected, doc will open even if you pass in a password.
            doc = wordApp.Documents.Open(file, PasswordDocument:="12F38DE7") '"12F38DE7-673B-42D2-8B73-E05A01A38631")
            Return False
        Catch ex As System.Exception
            Dim err As String = ex.Message
            If err.Contains("The password is incorrect. Word cannot open the document.") Then
                Return True
            Else
                Return False
            End If
        Finally
            Try
                doc.Close()
            Catch
            End Try
        End Try
    End Function

    Public Shared Function ExcelWorkbookIsEncrypted(file As String) As Boolean
        T("ExcelWorkbookIsEncrypted")
        Dim xl As Excel.Application
        Dim w As Excel.Workbook = Nothing
        Dim xlInitialEventsSetting As Boolean = False
        Dim xlInitialCalculationsSetting As Integer = 0
        Dim workbookWasOpened As Boolean = False
        Try
            Try
                T("ExcelWorkbookIsEncrypted-EnteringTry2")
                xl = GetExcelProcess()
                T("ExcelWorkbookIsEncrypted-GetExcelProcess")
                xl.EnableEvents = False ' stop autorun macros
                T("ExcelWorkbookIsEncrypted-EnableEventsFalse")
                w = xl.Workbooks.Open(file, False, , , "")
                workbookWasOpened = True
                xlInitialEventsSetting = xl.EnableEvents
                xlInitialCalculationsSetting = xl.Calculation
                If Not xl.Calculation = Excel.XlCalculation.xlCalculationManual Then
                    T("ExcelWorkbookIsEncrypted-Calcs not Manual")
                    xl.Calculation = Excel.XlCalculation.xlCalculationManual
                    T("ExcelWorkbookIsEncrypted-setting Calcs to Manual")
                End If
                T("ExcelWorkbookIsEncrypted-Open(" & file & ")")
            Catch ex As System.Exception
                T("ExcelWorkbookIsEncrypted-ex(inner): " & ex.Message)
                Dim err As String = ex.Message
                If err.Contains("The password you supplied is not correct. Verify that the CAPS LOCK key is off and be sure to use the correct capitalization.") Then
                    T("ExcelWorkbookIsEncrypted-Returning True")
                    Return True
                Else
                    T("ExcelWorkbookIsEncrypted-Throwing ex")
                    Dim exMain As New System.Exception("Error in ExcelWorkbookIsEncrypted(): " + err)
                    Throw exMain
                End If
            End Try
        Catch exOuter As System.Exception
            T("ExcelWorkbookIsEncrypted-ex(outer): " & exOuter.Message)
            Throw exOuter
        Finally
            Try
                xl.EnableEvents = xlInitialEventsSetting
                If workbookWasOpened Then xl.Calculation = xlInitialCalculationsSetting
                w.Close(False)
            Catch
            End Try

        End Try
        T("ExcelWorkbookIsEncrypted-leaving method")
    End Function

    Public Shared Function ExcelPassword(file As String, password As String, Optional mode As Integer = 0) As Boolean
        T("ExcelPassword")
        Dim xl As Excel.Application
        Dim w As Excel.Workbook
        Dim xlInitialEventsSetting As Boolean = False
        Dim xlInitialCalculationsSetting As Integer = 0
        Dim workbookWasOpened As Boolean = False
        Try
            T("ExcelPassword-entering Try")
            xl = GetExcelProcess()
            T("ExcelPassword-GetExcelProcess")
            xl.EnableEvents = False 'stop autorun macros
            T("ExcelPassword-EnableEventsFalse")
            If Not file.ToUpper.Contains("MACRO") Then
                T("ExcelPassword-file (" & file & ") not contains MACRO")
                If mode = 0 Then
                    T("ExcelPassword-mode = 0")
                    w = xl.Workbooks.Open(file)
                    workbookWasOpened = True
                    xlInitialEventsSetting = xl.EnableEvents
                    xlInitialCalculationsSetting = xl.Calculation

                    If Not xl.Calculation = Excel.XlCalculation.xlCalculationManual Then
                        T("ExcelPassword-calcs not manual")
                        xl.Calculation = Excel.XlCalculation.xlCalculationManual
                        T("ExcelPassword-set calcs to manual")
                    End If
                    T("ExcelPassword-opened file " & file & ")")
                    w.Password = password
                    T("ExcelPassword-set w.Password")
                Else
                    T("ExcelPassword-mode not 0")
                    'Actually Word does not save any changes if it just has to do with password. One has to physically make some changes to the document for the Save doc to work. So just protected it again and then unprotected it and then save it.
                    'If (My.Computer.Name = "WKLTDAL0058" Or My.Computer.Name = "") And file.Contains("CPP.xls") Then
                    '    'the odd issue wher it errs on w.Protect...
                    '    'do nothing here.
                    '    System.IO.File.Copy(file, file.Replace("CPP", "CPPZ"))
                    '    System.IO.File.Delete(file)
                    '    w = xl.Workbooks.Open(file.Replace("CPP", "CPPZ"), False, , , password, )
                    '    w.Password = ""
                    '    w.SaveAs(file)
                    '    w.Close(False)
                    '    w = xl.Workbooks.Open(file, False)
                    'Else
                    w = xl.Workbooks.Open(file, False, , , password)
                    workbookWasOpened = True
                    xlInitialEventsSetting = xl.EnableEvents
                    xlInitialCalculationsSetting = xl.Calculation
                    If Not xl.Calculation = Excel.XlCalculation.xlCalculationManual Then
                        T("ExcelPassword-calcs not manual")
                        xl.Calculation = Excel.XlCalculation.xlCalculationManual
                        T("ExcelPassword-set calcs to manual")
                    End If


                    T("ExcelPassword-file (" & file & ") opened")
                    Try 'Ngpp Aux Sable problem-errors on w.Protect
                        w.Protect(password)
                        T("ExcelPassword-w.Protect called")
                        w.Unprotect(password)
                        T("ExcelPassword-w.Unprotect called")
                    Catch exAuxSable As System.Exception
                        T("ExcelPassword-exAuxSable: " & exAuxSable.Message)
                        'don't do the above, just set pw to blank and try to save.
                        MsgBox("Console will now try to decrypt again. If no further 'password' errors show, then decryption was successful.")
                    End Try
                    'End If
                    w.Password = ""
                    T("ExcelPassword-w.Password set to blank")
                End If

                'w.Save()
                T("ExcelPassword-w Saved")
                xl.EnableEvents = xlInitialEventsSetting
                If workbookWasOpened Then xl.Calculation = xlInitialCalculationsSetting

                w.Close(True)
                T("ExcelPassword-w Closed")
            End If
            If ConsoleVertical = VerticalType.OLEFINS Or ConsoleVertical = VerticalType.NGPP Then
                T("ExcelPassword-ConsoleVertical is Olefins or Ngpp")
                Try
                    xl.EnableEvents = xlInitialEventsSetting
                    If workbookWasOpened Then xl.Calculation = xlInitialCalculationsSetting
                    w.Close(True)
                    T("ExcelPassword-w closed")
                Catch exClose As System.Exception
                    T("ExcelPassword-Discarded Exception exClose is: " & exClose.Message)
                End Try
                'Don't close out other open apps  xl.Quit()
            End If
            T("ExcelPassword-returning True")
            Return True
        Catch ex As System.Exception
            T("ExcelPassword-entering catch for ex: " & ex.Message)
            If ConsoleVertical = VerticalType.OLEFINS Or ConsoleVertical = VerticalType.NGPP Or ConsoleVertical = VerticalType.REFINING Then
                T("ExcelPassword-ex-ConsoleVertical match")
                Dim err As String = "Error in Utilities.ExcelPassword(): Unable to decrypt " + file + " using password '" + password + "'"
                If password.Length > 15 Then
                    T("ExcelPassword-ex-Password > 15 char")
                    err = err & Environment.NewLine & "The problem might be that the password is more than 15 letters long."
                End If
                MsgBox(err)
                Try
                    xl.EnableEvents = xlInitialEventsSetting
                    If workbookWasOpened Then xl.Calculation = xlInitialCalculationsSetting
                    w.Close(False)
                    T("ExcelPassword-ex-w closed")
                Catch exClose As System.Exception
                    T("ExcelPassword-ex-Discarded Exception exClose is: " & exClose.Message)
                End Try
                'Don't close out other open apps  xl.Quit()
                T("ExcelPassword-ex-returning False 1")
                Return False
            End If
            'Debug.Print(ex.Message)
            T("ExcelPassword-ex-returning False 2")
            Return False
        Finally

        End Try
        T("ExcelPassword-leaving method")
    End Function

    Public Function RemovePassword(File As String, password As String, companypassword As String) As Boolean
        T("RemovePassword")
        Dim success As Boolean = True
        Dim fileName As String = Utilities.ExtractFileName(File)
        T("RemovePassword-File: " & File)
        Dim ext As String = GetExtension(File)
        ext = ext.ToUpper()
        T("RemovePassword-ext: " & ext)
        If ext.StartsWith("XLS") Then
            If fileName.ToUpper.Contains("RETURN") Then
                success = Utilities.ExcelPassword(File, password, 1)
            Else
                success = Utilities.ExcelPassword(File, companypassword, 1)
            End If
        ElseIf ext.StartsWith("DOC") Then
            success = Utilities.WordPassword(File, companypassword, 1)
        End If

        'Select Case GetExtension(File).ToUpper()
        '    Case "XLS", "XLSX"
        '        If fileName.ToUpper.Contains("RETURN") Then
        '            success = Utilities.ExcelPassword(File, password, 1)
        '        Else
        '            success = Utilities.ExcelPassword(File, companypassword, 1)
        '        End If
        '    Case "DOC", "DOCX"
        '        success = Utilities.WordPassword(File, companypassword, 1)
        'End Select

        T("RemovePassword-success = " & success.ToString())
        Return success
    End Function

    Private Function GetExtension(strFile As String)
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, strFile.Length - (strFile.LastIndexOf(".") + 1))
        Catch
            ext = ""
        End Try
        Return ext
    End Function

    Public Shared Function WordPassword(mfile As String, password As String, Optional mode As Integer = 0, Optional user As String = "") As Boolean

        Dim Word As Word.Application
        Word = Utilities.GetWordProcess()

        Dim doc As Word.Document = Nothing
        Dim NewPath As String = mfile.Substring(0, mfile.LastIndexOf("\") + 1)
        Try

            If mode = 0 Then
                doc = Word.Documents.Open(mfile)
                doc.Password = password.Trim
                doc.SaveAs(mfile, Password:=password.Trim)
                doc.Close()
            Else
                If user = "BHH" Then MessageBox.Show("NewPath = " & NewPath & "  File = " & mfile & "  Password=" & password)
                If Console.ConsoleVertical = VerticalType.OLEFINS Then
                    doc = Word.Documents.Open(mfile, , , , password)
                    'Actually Word does not save any changes if it just has to do with password. One has to physically make some changes to the document for the Save doc to work. So just protected it again and then unprotected it and then save it.
                    doc.Protect(Microsoft.Office.Interop.Word.WdProtectionType.wdAllowOnlyComments)
                    doc.Unprotect("")
                    doc.Password = ""
                    doc.Save()
                    doc.Close()
                    'Don't close out other open apps  Word.Quit()
                    Return True
                Else
                    doc = Word.Documents.Open(mfile, PasswordDocument:=password)
                End If

                doc.Activate()
                doc.SaveAs(NewPath & "TEMP.DOC", Password:="")

                doc.Close()
                doc = Word.Documents.Open(NewPath & "TEMP.DOC")
                File.Delete(mfile)
                doc.SaveAs(mfile, Password:="")
                doc.Close()
                File.Delete(NewPath & "TEMP.DOC")
            End If
            Return True
        Catch ex As System.Exception
            Dim err As String = "Error in Utilities.WordPassword(): Unable to decrypt " + mfile + " using password '" + password + "'"
            If password.Length > 15 Then
                err = err & Environment.NewLine & "The problem might be that the password is more than 15 letters long."
            End If
            MsgBox(err)
            Try
                doc.Close()
            Catch exClose As System.Exception
            End Try
            Return False
        End Try

    End Function
    Public Shared Function ValFaxDateDue(txtDaysToAdd) As Date

        ' Take today's date and add the number of working days they put on the screen.
        ' Return the result.
        Dim I As Integer

        ValFaxDateDue = Now
        I = 0
        Do While I < Val(txtDaysToAdd)
            ValFaxDateDue = DateAdd(DateInterval.Day, 1, ValFaxDateDue)
            If Weekday(ValFaxDateDue) > 1 And Weekday(ValFaxDateDue) < 7 Then
                I = I + 1
            End If
        Loop
        Return ValFaxDateDue
    End Function
    Public Shared Sub GetLinkPath(ByRef link As String)
        If link.Substring(link.Length - 3, 3).ToUpper = "LNK" Then
            link = Utilities.GetFileFromLink(link)
        End If

    End Sub
    Public Shared Function GetConsultantName(ByRef dataObject As Internal.Console.DataObject.DataObject, initials As String) As String
        Dim ds As DataSet
        Dim cons As String = initials
        Dim params As New List(Of String)
        'Check for Interim Contact Info
        params = New List(Of String)
        params.Add("Initials/" + initials)
        Dim db As SA.Internal.Console.DataObject.DataObject
        If Not IsNothing(dataObject) Then
            db = dataObject
        Else
            'TODO: CHANGE (rrh)
            'leaving it as I found it:
            db = New SA.Internal.Console.DataObject.DataObject(Internal.Console.DataObject.DataObject.StudyTypes.REFINING, "rrh", "rrh#4279")
        End If

        ds = db.ExecuteStoredProc("Console." & "GetConsultantName", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0)

            End If
        End If
        Return cons

    End Function

    Public Shared Function CheckProcess(path As String, name As String) As Process
        For Each mProcess In Process.GetProcesses()
            If mProcess.ProcessName.ToUpper.Contains(name.ToUpper) Then
                Return mProcess
            End If
        Next
        MessageBox.Show("Outlook must be started to use Secure Send features.  Console will start the Outlook for you.", "Warning")

        Try
            Process.Start(path & name & ".exe")
        Catch ex As System.Exception
            MsgBox("Unable to start Outlook at " & path & name & ". Please start Outlook manually and try your email again.")
        End Try
        Return Nothing
    End Function

    Public Shared Function checkVerifySend() As Boolean
        Dim count As Integer
        Dim i As Integer
        Dim verifysendStatus As Boolean
        Dim outlook As Outlook.Application
        Dim outlookprocess As Process
        Try
            If File.Exists(ConfigurationManager.AppSettings("OutlookFolder1").ToString() & "outlook.exe") Then
                outlookprocess = Utilities.CheckProcess(ConfigurationManager.AppSettings("OutlookFolder1").ToString(), "Outlook")
            ElseIf File.Exists(ConfigurationManager.AppSettings("OutlookFolder2").ToString() & "outlook.exe") Then
                outlookprocess = Utilities.CheckProcess(ConfigurationManager.AppSettings("OutlookFolder2").ToString(), "Outlook")
            Else
                outlookprocess = Nothing
            End If

            If Not outlookprocess Is Nothing Then
                outlook = DirectCast(Marshal.GetActiveObject("Outlook.Application"), Outlook.Application)
            Else
                outlook = New Outlook.Application
                End If

            If outlook Is Nothing Then
                MsgBox("Please make sure your Outlook is started")
                Exit Function
            Else
                count = outlook.COMAddIns.Count
            End If

            For i = 1 To count
                If outlook.COMAddIns.Item(i).Description = "VerifySend" Then
                    verifysendStatus = outlook.COMAddIns.Item(i).Connect
                    Thread.Sleep(200)
                    Exit For
                End If
            Next i
            Return verifysendStatus
        Catch ex As System.Exception
            MsgBox("Error on checking VerifySend Status: " + ex.Message)
        End Try

    End Function
    Public Shared Function GetDataSetID(strRefNum As String, CurrentSortMode As Integer) As String
        If strRefNum = "" Then Return ""
        If CurrentSortMode = 0 Then
            Return strRefNum.Substring(0, strRefNum.IndexOf("-") - 1)
        Else
            Return strRefNum.Substring(strRefNum.LastIndexOf("-") + 1, strRefNum.Length - strRefNum.LastIndexOf("-") - 1)
        End If

    End Function
    Public Shared Function GetSite(strRefNum As String, CurrentSortMode As Integer) As String
        If strRefNum = "" Then Return ""
        If CurrentSortMode = 0 Then
            Return strRefNum.Substring(strRefNum.LastIndexOf("-") + 1, strRefNum.Length - strRefNum.LastIndexOf("-") - 1).Trim
        Else
            Return strRefNum.Substring(0, strRefNum.IndexOf("-") - 1).Trim
        End If
    End Function
    Public Shared Function GetRefNumPart(refnum As String, mode As Integer) As String

        Dim FirstPart As String
        Dim SecondPart As String
        Dim ThirdPart As String
        Dim Buffer As String
        Dim Looper As Integer
        Dim StopPoint As Integer
        Dim Pos As Integer

        Try
            If refnum <> "" Then
                Pos = 1

                While isNumeric(refnum.Substring(Pos, 1))
                    Pos = Pos + 1
                End While
                StopPoint = Pos

                FirstPart = refnum.Substring(0, StopPoint)
                Looper = 0

                While Not isNumeric(refnum.Substring(Pos, 1))
                    Pos = Pos + 1
                    Looper = Looper + 1
                End While

                SecondPart = refnum.Substring(StopPoint, Looper)

                If SecondPart = "LIV" Or SecondPart = "LEV" Then
                    SecondPart = "LUB"
                End If

                Looper = 0
                StopPoint = Pos

                While Pos <= refnum.Length
                    Looper = Looper + 1
                    Pos = Pos + 1
                End While

                ThirdPart = refnum.Substring(StopPoint, Looper - 1)

                Buffer = "INVALID MODE"
                If mode = 0 Then
                    Buffer = FirstPart + SecondPart + ThirdPart
                End If

                If mode = 1 Then
                    Buffer = FirstPart
                End If

                If mode = 2 Then
                    Buffer = SecondPart
                End If

                If mode = 3 Then
                    Buffer = ThirdPart
                End If

                Return Buffer

            Else
                Return ""
            End If
        Catch ex As System.Exception

            Throw ex
        End Try

    End Function

    Public Shared Function GetStudyType(RefNum As String) As String

        Select Case GetRefNumPart(RefNum, 2)
            Case "NSA", "EUR", "PAC"
                Return "REFINING"
            Case "LUB"
                Return "LUBES"
            Case "PA"
                Return "PIPELINE"
            Case "TA"
                Return "TERMINAL"
            Case "PN", "PNP"
                Return "POWER"
            Case "EBSM"
                Return "STYREME"
            Case "PCH", "OLE"
                Return "OLEFINS"
            Case Else
                Return ""
        End Select
    End Function

    Public Function GetBestError(ex As System.Exception) As String
        If Not IsNothing(ex.InnerException) AndAlso Not IsNothing(ex.InnerException.Message) Then
            Return ex.InnerException.Message
        Else
            Return ex.Message
        End If
    End Function

    Public Shared Function GetSiteNumberFromRefnum(refnum As String, vertical As Console.VerticalType) As String
        Dim siteNumber As String = String.Empty
        If vertical = VerticalType.REFINING Then
            'the 777 from 777EUR16A
            Dim count As Integer = 0
            While isNumeric(refnum.Substring(count, 1))
                count += 1
            End While
            siteNumber = refnum.Substring(0, count)
        End If
        Return siteNumber
    End Function

    Public Shared Function GetRegionFromRefnum(refnum As String, vertical As Console.VerticalType) As String
        Dim region As String = String.Empty
        If vertical = VerticalType.REFINING Then
            'the LUB from 777EUR16A
            Dim count As Integer = 0
            Dim start As Integer = 0
            While isNumeric(refnum.Substring(count, 1))
                count += 1
                start += 1
            End While
            Dim len As Integer = 0
            While Not isNumeric(refnum.Substring(count, 1))
                count += 1
                len += 1
            End While
            region = refnum.Substring(start, len)
        End If
        Return region
    End Function
    Public Shared Function GetYearFromRefnum(refnum As String, vertical As Console.VerticalType) As String
        Dim year As String = String.Empty
        If vertical = VerticalType.REFINING Then
            'the 16 from 777EUR16A
            Dim count As Integer = 0
            Dim start As Integer = 0
            While isNumeric(refnum.Substring(count, 1))
                count += 1
                start += 1
            End While
            While Not isNumeric(refnum.Substring(count, 1))
                count += 1
                start += 1
            End While
            Dim len As Integer = 0
            While isNumeric(refnum.Substring(count, 1))
                count += 1
                len += 1
            End While
            year = refnum.Substring(start, len)
        End If
        Return year
    End Function

    Public Shared Function GetWindowHandle() As Long
        Return CLng(My.Application.OpenForms(0).Handle)
    End Function


    'Public Shared Function CleanFilename(fileName As String, replaceIllegalCharacters As Boolean) As String
    '    'Per Dave, don't replace illegal chars; just strip them out.
    '    Dim result As String = String.Empty
    '    If Not IsNothing(fileName) AndAlso fileName.Length > 0 Then
    '        For i As Integer = 0 To fileName.Length - 1
    '            Dim ltr As Char = fileName.Substring(i)
    '            Dim code As Integer = Asc(ltr)
    '            If code <> Asc("\") And code <> Asc("/") _
    '                And code <> Asc(":") And code <> Asc("*") _
    '                    And code <> Asc("?") _
    '                    And code <> Asc(">") _
    '                    And code <> Asc("<") _
    '                    And code <> Asc(32) _
    '                    And code <> Asc("|") Then
    '                result = result & ltr
    '            End If
    '        Next
    '        If result.StartsWith(" ") Or result.StartsWith(".") Then
    '            result = result.Substring(1, result.Length - 1) ' remove first char
    '        End If
    '    End If
    '    Return result
    'End Function

End Class
