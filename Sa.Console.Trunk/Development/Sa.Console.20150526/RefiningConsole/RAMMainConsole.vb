﻿
Imports System.Data.SqlClient
Imports System.Configuration
Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.Text
Imports System.IO.Compression
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports SA.Internal.Console.DataObject




Public Class RAMMainConsole

#Region "PRIVATE VARIABLES"
    Dim CurrentSortMode As Integer = 0
    Dim SavedFile As String = Nothing
    Dim CompleteIssue As String = "N"
    Dim CurrentTab As Integer = 0
    Dim ValidationIssuesIsDirty As Boolean = False
    Dim Time As Integer = 0
    Dim LastTime As Integer = 0
    Dim CompanyContact As Contacts
    Dim OlefinsContact As Contacts
    Dim RefineryContact As Contacts
    Dim InterimContact As Contacts
    Dim PlantContact As Contacts
    Dim PowerContact As Contacts
    Dim OpsContact As Contacts
    Dim TechContact As Contacts
    Dim PricingContact As Contacts
    Dim DataContact As Contacts
    Dim CompanyPassword As String = ""

    Dim TabDirty(9) As Boolean
    Dim ds As DataSet
    'Dim db As New DataAccess()
    Dim db As DataObject


    Dim sDir(20) As String
    Dim StudyRegion As String = ""
    Dim Study As String = ""

    Dim SANumber As String

    Dim CurrentConsultant As String
    Dim CurrentRefNum As String
    Dim ValidationFile As String
    Dim ValidatePath As String
    Dim JustLooking As Boolean = False
    Dim CurrentCompany As String
    Dim Company As String
    Dim CompCorrPath As String
    Dim GeneralPath As String
    Dim ProjectPath As String
    Dim TempPath As String
    Dim OldTempPath As String = ""
    Dim TemplatePath As String
    Dim DirResult As String

    Dim StudyDrive As String
    Dim TempDrive As String = "C:\USERS\"
    Dim CurrentPath As String
    Dim RefNum As String
    Dim bJustLooking As Boolean = True
    Dim RefNumPart As String
    Dim LubRefNum As String
    Dim StudySave(20) As String
    Dim RefNumSave(20) As String
    Dim FirstTime As Boolean = True
    Dim KeyEntered As Boolean = False

    Private mDBConnection As String
    Private mStudyYear As String
    Private mPassword As String
    Private mRefNum As String
    Private mSpawn As Boolean
    Private mStudyType As String
    Private mUserName As String
    Private mReturnPassword As String

    'Private Err As ErrorLog
#End Region

#Region "PUBLIC PROPERTIES"
    Private mVPN As Boolean
    Public Property VPN() As Boolean
        Get
            Return mVPN
        End Get
        Set(ByVal value As Boolean)
            mVPN = value
        End Set
    End Property

    Public Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
        Set(ByVal value As String)
            mDBConnection = value
        End Set
    End Property
    Private mCurrentStudyYear As String
    Public Property CurrentStudyYear() As String
        Get
            Return mCurrentStudyYear
        End Get
        Set(ByVal value As String)
            mCurrentStudyYear = value
        End Set
    End Property
    Public Property StudyYear() As String
        Get
            Return "20" & mStudyYear
        End Get
        Set(ByVal value As String)
            mStudyYear = value
        End Set
    End Property

    Public Property UserName() As String

        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set

    End Property
    Public Property ReturnPassword() As String
        Get
            Return mReturnPassword
        End Get
        Set(ByVal value As String)
            mReturnPassword = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property


    Public Property StudyType() As String
        Get
            Return mStudyType
        End Get
        Set(ByVal value As String)
            mStudyType = value
        End Set
    End Property

    Public Property ReferenceNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property

    Public Property Spawn() As Boolean
        Get
            Return mSpawn
        End Get
        Set(ByVal value As Boolean)
            mSpawn = value
        End Set
    End Property
#End Region

#Region "INITIALIZATION"
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub Initialize()
        Try

            Dim p As Process = Utilities.CheckProcess("C:\Program Files (x86)\Microsoft Office\Office14\", "Outlook")
            Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim version As System.Version = assembly.GetName().Version
            txtVersion.Text = String.Format("Version {0}", version)

            'Populate Consultant Tab with login

            SetTabDirty(True)


            'Populate the Form Title
            Me.Text = "Console." & StudyType & " Validation Console"



            'Fill Study Combo Box
            cboStudy.Items.Add("RAM14")
            cboStudy.Items.Add("RAM13")
            cboStudy.Items.Add("RAM12")
            cboStudy.Items.Add("RAM11")
            cboStudy.Items.Add("RAM08")


            StudyDrive = "K:\STUDY\RAM\"
            db = New DataObject(DataObject.StudyTypes.RAM, UserName, Password)

            cboStudy.SelectedIndex = 0

            'Setup Database connection for ErrorLog
            'db.DBConnection = mDBConnection
            'Err = New ErrorLog(db) 'StudyType, UserName, Password)
            'Assigned Combo Box



            'Fill Directory Drop Down

            cboDir.Items.Add("Corr. Polishing")
            cboDir2.Items.Add("Corr. Polishing")
            cboDir.Items.Add("Corr. Validation")
            cboDir2.Items.Add("Corr. Validation")

            cboDir.Items.Add("Company Correspondence")
            cboDir.Items.Add("Client Attachments")

            cboDir2.Items.Add("Company Correspondence")
            cboDir2.Items.Add("Client Attachments")

            cboDir.Items.Add("Site Data")
            cboDir2.Items.Add("Site Data")



            cboDir.SelectedIndex = 0
            cboDir2.SelectedIndex = 2




            'Choose first Checklist item in drop down



            'Set up Tabs and buttons per Study Type

            lblCompany.Text = "Site"


            'Check for existance of Settings file, if missing, copy default one

            GetSettingsFile()

            'If not spawned off an existing instance of the program, read the settings file and set the drop downs accordingly


            GetCompanyContactInfo()

            BuildSummaryTab()

        Catch ex As System.Exception
            ''db.WriteLog("Initialize", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region



    Private Sub SetSummaryFileButtons()





        If File.Exists("K:\STUDY\RAM\" & StudyYear & "\Company\Correspondence\RAM" & StudyYear.Substring(2, 2) & " Validation Status.xls") Then
            btnValidationStatus.Enabled = True
        Else
            btnValidationStatus.Enabled = False
        End If

        If File.Exists("K:\STUDY\RAM\" & StudyYear & "\Company\Correspondence\RAM" & StudyYear.Substring(2, 2) & " Datacheck Comments.xls") Then
            btnValidationComments.Enabled = True
        Else
            btnValidationComments.Enabled = False
        End If




    End Sub

#Region "BUILD TABS"

    Private Sub BuildSummaryTab()
        Dim Unlocked As Integer = 0
        RefNum = cboRefNum.Text

        lblQualityScore.Text = ""
        lblNoUnits.Text = ""
        lblPercentComplete.Text = ""
        lblNumNeedingApproval.Text = ""
        lblQualityIssues.Text = ""
        lblDataCollectionTime.Text = ""
        lblUnlockedForUser.Text = ""

        SetSummaryFileButtons()



        Dim row As DataRow = Nothing

        Dim params = New List(Of String)
        params.Add("RefNum/" + Utilities.GetDataSetID(cboRefNum.Text, CurrentSortMode))
        ds = db.ExecuteStoredProc("Console." & "GetUnitCount", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            row = ds.Tables(0).Rows(0)
            lblNoUnits.Text = row(0).ToString
        End If

        lblPercentComplete.ForeColor = Color.DarkGreen
        lblQualityScore.ForeColor = Color.DarkGreen

        ds = db.ExecuteStoredProc("Console." & "GetSiteDataQuality", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then

            row = ds.Tables(0).Rows(0)

            If Convert.ToDecimal(row("PcntComplete").ToString) < 98.0 Then lblPercentComplete.ForeColor = Color.Goldenrod
            If Convert.ToDecimal(row("PcntComplete").ToString) < 90.0 Then lblPercentComplete.ForeColor = Color.DarkGoldenrod
            If Convert.ToDecimal(row("PcntComplete").ToString) < 80.0 Then lblPercentComplete.ForeColor = Color.DarkRed


            If Convert.ToDecimal(row("Grade").ToString) < 96.0 Then lblQualityScore.ForeColor = Color.Goldenrod
            If Convert.ToDecimal(row("Grade").ToString) < 90.0 Then lblQualityScore.ForeColor = Color.DarkGoldenrod
            If Convert.ToDecimal(row("Grade").ToString) < 86.0 Then lblQualityScore.ForeColor = Color.DarkRed


            lblQualityScore.Text = row("Grade").ToString & "%"
            lblPercentComplete.Text = row("PcntComplete").ToString & "%"
            lblQualityIssues.Text = row("ChecksNC").ToString
            lblNumNeedingApproval.Text = row("ChecksNA").ToString
        End If
        ds = db.ExecuteStoredProc("Console." & "GetValidationHours", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            lblDataCollectionTime.Text = ds.Tables(0).Rows(0)(0).ToString
        End If

        ds = db.ExecuteStoredProc("Console." & "GetLockedStatus", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Unlocked = Convert.ToInt32(ds.Tables(0).Rows(0)(0).ToString)
        End If


        If Unlocked > 0 Then
            lblUnlockedForUser.Text = "Locked"
        Else
            lblUnlockedForUser.Text = "Unlocked"
        End If


    End Sub





    Private Sub BuildNotesTab()

        txtContinuingIssues.Text = ""
        txtConsultingOpps.Text = ""
        txtValidationIssues.Text = ""
        Dim row As DataRow
        Dim params As List(Of String)


        params = New List(Of String)
        params.Add("SiteDatasetID/" + Utilities.GetDataSetID(RefNum, CurrentSortMode))
        ds = db.ExecuteStoredProc("Console." & "GetNotes", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            row = ds.Tables(0).Rows(0)
            txtContinuingIssues.Text = row("ContinuingIssues").ToString
            txtConsultingOpps.Text = row("ConsultingOpportunities").ToString
            txtValidationIssues.Text = row("ValidationNotes").ToString
        End If

    End Sub

    Private Sub BuildDirectoriesTab()
        SetPaths()

        PopulateCorrespondenceTreeView(tvwCorrespondence)

        PopulateCorrespondenceTreeView(tvwCorrespondence2)
        PopulatePolishCorrespondenceTreeView(tvwPolishCorr)
        PopulatePolishCorrespondenceTreeView(tvwPolishCorr2)

        PopulateCompanyTreeView(tvwCompCorr)

        PopulateCompanyTreeView(tvwCompCorr2)

        PopulateSiteDataTreeView(tvwSiteData)

        PopulateSiteDataTreeView(tvwSiteData2)

        PopulateCATreeView(tvwClientAttachments)

        PopulateCATreeView(tvwClientAttachments2)
    End Sub

#End Region

#Region "UTILITIES"
    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function

    Private Sub SetTabDirty(status As Boolean)
        For i = 0 To 3
            TabDirty(i) = status
        Next
    End Sub
    Private Sub LoadTab(tab As Integer)
        Select Case tab


            Case 0 'Refining/Plant and Company Tab
                dgContacts.DataSource = Nothing
                dgContacts.Refresh()
                GetCompanyContactInfo()
                FillContacts()
                BuildSummaryTab()

            Case 1
                BuildNotesTab()

            Case 2


                BuildDirectoriesTab()



        End Select
    End Sub

    Private Function CheckSAMaster(RefNum) As Boolean

        Dim params = New List(Of String)

        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console.CheckSANumber", params)
        If ds.Tables.Count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Function RemovePassword(File As String, password As String, companypassword As String) As Boolean
        Dim success As Boolean = True
        Try
            If File.Substring(File.Length - 3, 3).ToUpper = "XLS" Then
                success = Utilities.ExcelPassword(File, companypassword, 1)
            End If
            If File.Substring(File.Length - 3, 3).ToUpper = "DOC" Or File.Substring(File.Length - 3, 3).ToUpper = "OCX" Then
                success = Utilities.WordPassword(File, companypassword, 1)
            End If
        Catch
        End Try
        Return success
    End Function
    Private Function FixFilename(ext As String, strFile As String)
        Dim strNewFilename As String
        Dim NewExt As String = GetExtension(strFile)
        strNewFilename = strFile.Replace(NewExt, ext)
        Return strNewFilename
    End Function
    Private Function GetExtension(strFile As String)
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, 3)
        Catch
            ext = ""
        End Try
        Return ext
    End Function

    Public Function PlugLetterIfNeeded(strFileName As String) As String
        Dim strWorkFileName As String
        Dim strDirResult As String
        Dim strCorrPath As String
        Dim strABC(10) As String
        Dim I As Integer
        Dim intInsertPoint As Integer

        strABC(0) = ""
        strABC(1) = "A"
        strABC(2) = "B"
        strABC(3) = "C"
        strABC(4) = "D"
        strABC(5) = "E"
        strABC(6) = "F"
        strABC(7) = "G"
        strABC(8) = "H"
        strABC(9) = "I"

        strCorrPath = CorrPath
        If strFileName.Substring(0, 6).ToUpper = "RETURN" Then
            intInsertPoint = 7
        Else
            intInsertPoint = 3
        End If

        For I = 0 To 9
            strWorkFileName = strFileName.Substring(0, intInsertPoint) & strABC(I) & Mid(strFileName, intInsertPoint + 1, 99)
            strDirResult = Dir(CorrPath & strWorkFileName)
            If strDirResult = "" Then
                Return strABC(I)
                Exit Function
            End If
        Next I

        MsgBox("More than 9 iterations of same file?", vbOKOnly, "Check with Joe Waters (JDW)?")

        PlugLetterIfNeeded = strFileName

    End Function
    Function CleanFileName(strFileName As String) As String
        Dim strBadChar As String
        Dim strRepChar As String
        Dim I As Integer
        Dim J As Integer
        Dim blnChanged As Boolean
        blnChanged = False

        strBadChar = "\/:*?""<>|%"
        ' A
        strBadChar = strBadChar & Chr(192)
        strBadChar = strBadChar & Chr(193)
        strBadChar = strBadChar & Chr(194)
        strBadChar = strBadChar & Chr(195)
        strBadChar = strBadChar & Chr(196)
        strBadChar = strBadChar & Chr(197)
        strBadChar = strBadChar & Chr(198)
        ' E
        strBadChar = strBadChar & Chr(200)
        strBadChar = strBadChar & Chr(201)
        strBadChar = strBadChar & Chr(202)
        strBadChar = strBadChar & Chr(203)
        ' I
        strBadChar = strBadChar & Chr(204)
        strBadChar = strBadChar & Chr(205)
        strBadChar = strBadChar & Chr(206)
        strBadChar = strBadChar & Chr(207)
        ' O
        strBadChar = strBadChar & Chr(210)
        strBadChar = strBadChar & Chr(211)
        strBadChar = strBadChar & Chr(212)
        strBadChar = strBadChar & Chr(213)
        strBadChar = strBadChar & Chr(214)
        ' U
        strBadChar = strBadChar & Chr(217)
        strBadChar = strBadChar & Chr(218)
        strBadChar = strBadChar & Chr(219)
        strBadChar = strBadChar & Chr(220)

        ' a
        strBadChar = strBadChar & Chr(224)
        strBadChar = strBadChar & Chr(225)
        strBadChar = strBadChar & Chr(226)
        strBadChar = strBadChar & Chr(227)
        strBadChar = strBadChar & Chr(228)
        strBadChar = strBadChar & Chr(229)
        ' e
        strBadChar = strBadChar & Chr(232)
        strBadChar = strBadChar & Chr(233)
        strBadChar = strBadChar & Chr(234)
        strBadChar = strBadChar & Chr(235)
        ' i
        strBadChar = strBadChar & Chr(236)
        strBadChar = strBadChar & Chr(237)
        strBadChar = strBadChar & Chr(238)
        strBadChar = strBadChar & Chr(239)
        ' o
        strBadChar = strBadChar & Chr(242)
        strBadChar = strBadChar & Chr(243)
        strBadChar = strBadChar & Chr(244)
        strBadChar = strBadChar & Chr(245)
        strBadChar = strBadChar & Chr(246)
        ' u
        strBadChar = strBadChar & Chr(249)
        strBadChar = strBadChar & Chr(250)
        strBadChar = strBadChar & Chr(251)
        strBadChar = strBadChar & Chr(252)

        strRepChar = "  -      AAAAAAAEEEEIIIIOOOOOUUUUaaaaaaeeeeiiiiooooouuuu"
        For I = 1 To Len(strBadChar)
            For J = 1 To Len(strFileName)
                If Mid(strFileName, J, 1) = Mid(strBadChar, I, 1) Then
                    Mid(strFileName, J, 1) = Mid(strRepChar, I, 1)
                    blnChanged = True
                End If
            Next J
        Next I
        CleanFileName = Trim(strFileName)


    End Function
    Private Sub GetSettingsFile()

        Try
            If Directory.Exists(TempDrive & mUserName & "\Console") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console")
            If Directory.Exists(TempDrive & mUserName & "\Console\Temp") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp")
            If Directory.Exists(TempDrive & mUserName & "\Console\Temp\ConsoleSettings") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp\ConsoleSettings")
            TempPath = TempDrive & mUserName & "\Console\Temp\"

        Catch ex As System.Exception
            'db.WriteLog("GetSettingsFile", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ReadLastStudy()
        Try
            Dim objReader As New IO.StreamReader(TempPath & "ConsoleSettings\" & StudyType & "settings.txt")
            Select Case StudyType.ToUpper
                Case "REFINING"
                    For i = 0 To 12
                        StudySave(i) = objReader.ReadLine()
                        RefNumSave(i) = objReader.ReadLine()
                    Next
                Case "OLEFINS"
                    For i = 0 To 2
                        StudySave(i) = objReader.ReadLine()
                        RefNumSave(i) = objReader.ReadLine()
                    Next
            End Select

            Dim lastStudy As String = objReader.ReadLine()
            cboStudy.SelectedIndex = cboStudy.FindString(lastStudy)
            Dim rn As String = objReader.ReadLine()
            cboRefNum.Text = rn
            RefNum = rn
            objReader.Close()
            GetRefNumRecord(rn)
        Catch ex As System.Exception
            'db.WriteLog("ReadLastStudy", ex)
        End Try

    End Sub

    Private Sub SaveLastStudy()
        Try
            If cboRefNum.Text <> "" Then
                If Directory.Exists(TempPath & "ConsoleSettings\") = False Then GetSettingsFile()
                Dim objWriter As New System.IO.StreamWriter(TempPath & "ConsoleSettings\" & StudyType & "Settings.txt", False)

                Select Case StudyType.ToUpper
                    Case "REFINING"
                        For i = 0 To 11
                            objWriter.WriteLine(StudySave(i))
                            objWriter.WriteLine(RefNumSave(i))
                        Next
                    Case "OLEFINS"
                        For i = 0 To 2
                            objWriter.WriteLine(StudySave(i))
                            objWriter.WriteLine(RefNumSave(i))
                        Next
                End Select

                objWriter.WriteLine(cboStudy.Text)
                objWriter.WriteLine(cboRefNum.Text)
                objWriter.Close()
            End If
        Catch ex As System.Exception
            'db.WriteLog("SaveLastStudy", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ClearFields()





    End Sub

    Public Sub WordTemplateReplace(strfile As String, fields As SA.Console.Common.WordTemplate, strfilename As String)

        Dim oWord As Object
        Dim oDoc As Object
        Dim range As Object
        Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
        For count = 0 To fields.Field.Count - 1
            DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
        Next
        MessageBox.Show(DoubleCheck, "Template Field Check")

        oWord = CreateObject("Word.Application")
        oWord.Visible = True
        oDoc = oWord.Documents.Open(strfile)
        range = oDoc.Content
        Try
            For count = 0 To fields.Field.Count - 1
                With oWord.Selection.Find
                    .Text = fields.Field(count)
                    .Replacement.Text = fields.RField(count)
                    .Forward = True
                    .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                    .Format = False
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
            Next

            If File.Exists(strfilename) Then
                strfilename = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilename)
            Else
                'Stop
                strfilename = InputBox("Here is the suggested name for this New Validation Fax document." & Chr(10) & "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename)
            End If
            ' If they hit cancel, just stop.
            If strfilename = "" Then Exit Sub
            '
            oDoc.SaveAs(strfilename, ADDTORECENTFILES:=True)
            MessageBox.Show("Name is now " & oDoc.Name)
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Function NextVF()

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 9
            strDirResult = Dir(CorrPath & "\vf" & CStr(I) & "*.*")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextVR()

        Dim strDirResult As String
        Dim I As Integer
        For I = 0 To 9
            strDirResult = Dir(CorrPath & "\vr" & CStr(I) & "*.*")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextReturnFile()
        Dim strDirResult As String
        Dim I As Integer
        For I = 0 To 9
            strDirResult = Dir(CorrPath & "*F_Return" & CStr(I) & "*.xls")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing

    End Function


    Function CacheShellIcon(ByVal argPath As String) As String
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            mKey = "folder"
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function




    Function DatePulledFromVAFile(ByVal strDirAndFileName As String) As Date

        Dim strWork As String = ""
        ' Read the second line of the VA file. Get the date from there.
        Dim objReader As New System.IO.StreamReader(strDirAndFileName, True)

        ' Read and toss the first line
        strWork = objReader.ReadLine()
        ' Read the date-time line
        strWork = objReader.ReadLine()

        DatePulledFromVAFile = strWork

    End Function
    Sub ResetGradeTab(intSectionORVersion As Integer)

        'Dim intVersion As Integer
        'Dim params As New List(Of String)
        'Dim rdr As SqlDataReader


        'If intSectionORVersion = 1 Then
        '    ' Load the data by Section.
        '    ' First determined which version we need to get.
        '    params.Add("RefNum/" & ParseRefNum(RefNum, 0))

        '    rdr = db.ExecuteReader("Console.GetSectionGrade", params)
        '    'strSQL = "SELECT Version FROM Val.SectionGrade WHERE Refnum = '" & gRefineryID & "' ORDER BY Version DESC"
        '    If rdr.Read() Then
        '        intVersion = rdr.GetInt16(0)
        '    End If
        '    rdr.Close()
        '    params.Clear()
        '    params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        '    params.Add("Version/" & intVersion)

        '    ds = db.ExecuteStoredProc("Console.GetGradesBySection", params)

        '    dgGrade.DataSource = ds.Tables(0)

        'Else
        '    ' Load the data by Version.

        '    params.Clear()
        '    params.Add("RefNum/" & RefNum)
        '    params.Add("Version/" & intVersion)

        '    ds = db.ExecuteStoredProc("Console.GetGradesByVersion", params)

        '    dgGrade.DataSource = ds.Tables(0)

        'End If
        'TimeHistory_Load()
        'TimeSummary_Load()

    End Sub


    Private Sub GetDirectories(ByVal subDirs() As DirectoryInfo, ByVal nodeToAddTo As TreeNode)

        Dim aNode As TreeNode
        Dim subSubDirs() As DirectoryInfo
        Dim subDir As DirectoryInfo
        For Each subDir In subDirs
            aNode = New TreeNode(subDir.Name, 0, 0)
            aNode.Tag = subDir
            aNode.ImageKey = "folder"
            subSubDirs = subDir.GetDirectories()
            If subSubDirs.Length <> 0 Then
                GetDirectories(subSubDirs, aNode)
            End If
            nodeToAddTo.Nodes.Add(aNode)
        Next subDir

    End Sub
    'Validation

    'Set all directory paths
    Private Sub SetPaths()
        Dim Site As String

        Site = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)

        Dim mRefNum As String = cboRefNum.Text
        StudyRegion = cboStudy.Text.Substring(0, 3)

        Dim params As List(Of String)
        GeneralPath = StudyDrive & StudyYear
        ProjectPath = StudyDrive & StudyYear & "\Project\"

        If Not Directory.Exists(TempDrive & mUserName & "\Console\Temp\") Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp\")
        TempPath = TempDrive & mUserName & "\Console\Temp\"



        params = New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("RefNum/" + Utilities.GetDataSetID(cboRefNum.Text, CurrentSortMode))
        Try
            ds = db.ExecuteStoredProc("Console." & "GetCompanyID", params)
            Dim row As DataRow = Nothing 'Represents a row of data in Systems.data.datatable
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Company = ds.Tables(0).Rows(0)("CompanyID").ToString.Trim
                End If
            End If

            SiteDataPath = StudyDrive & StudyYear & "\Company\" & Company & "\" & Site & "\"
            PolishCorrPath = StudyDrive & StudyYear & "\Correspondence\" & Company & "\" & Site & "\Polishing\"
            CorrPath = StudyDrive & StudyYear & "\Correspondence\" & Company & "\" & Site & "\Validation\"
            CompanyPath = StudyDrive & StudyYear & "\Company\" & Company & "\"
            CompCorrPath = CompanyPath

            If Not Directory.Exists(CorrPath) Then
                Directory.CreateDirectory(CorrPath)
            End If

            If Not Directory.Exists(CompanyPath) Then
                Directory.CreateDirectory(CompanyPath)
            End If

            TemplatePath = StudyDrive & StudyYear & "\software\validation\template\"
            ClientAttachmentsPath = StudyDrive & StudyYear & "\software\validation\Client Attachments\"


        Catch ex As System.Exception
            'db.WriteLog("SetPaths: " & RefNum, ex)

        End Try

    End Sub
    'Method to remove specific tab from tabcontrol
    Private Sub RemoveTab(tabname As String)
        Dim tab As TabPage
        tab = ConsoleTabs.TabPages(tabname)
        ConsoleTabs.TabPages.Remove(tab)
    End Sub



    'Method to save last refnum visited per Study for settings file
    Private Sub SaveRefNum(strStudy As String, strRefNum As String)
        Select Case StudyType
            Case "REFINING"
                For i = 0 To 11
                    If StudySave(i) = strStudy Then
                        RefNumSave(i) = strRefNum
                    End If
                Next
            Case "OLEFINS"
                For i = 0 To 2
                    If StudySave(i) = strStudy Then
                        RefNumSave(i) = strRefNum
                    End If
                Next

        End Select
    End Sub
#End Region

#Region "DATABASE LOOKUPS"
    Private Sub UpdateContinuingIssues()
        Dim params As New List(Of String)


        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Issue/" & Utilities.FixQuotes(txtContinuingIssues.Text))

        ds = db.ExecuteStoredProc("Console." & "UpdateContinuingIssues", params)
        BuildNotesTab()
    End Sub
    Private Sub UpdateConsultingOpportunities()
        Dim params As New List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Issue/" & Utilities.FixQuotes(txtConsultingOpps.Text))

        ds = db.ExecuteStoredProc("Console." & "UpdateConsultingOpportunity", params)
        BuildNotesTab()
    End Sub


    Private Function IsValidRefNum(refnum As String) As String
        Dim ret As String = ""
        Dim params = New List(Of String)

        params.Add("@RefNum/" + Utilities.GetDataSetID(cboRefNum.Text, CurrentSortMode))
        params.Add("@StudyYear/" + StudyYear.Substring(2, 2))

        Dim c As DataSet = db.ExecuteStoredProc("Console." & "IsValidRefNum", params)
        If c.Tables.Count > 0 Then
            If c.Tables(0).Rows.Count > 0 Then
                ret = c.Tables(0).Rows(0)(0).ToString
            End If
        End If
        Return ret

    End Function
    Private Function FindString(lb As ComboBox, str As String) As Integer
        Dim count As Integer = 0
        For Each item As String In lb.Items
            If item.Contains(str) Then
                Return count
            End If
            count += 1
        Next
    End Function





    Public Sub TimeSummary_Load()

        'Dim params As New List(Of String)

        'params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        'ds = db.ExecuteStoredProc("Console.GetTimeSummary", params)
        'dgSummary.DataSource = ds.Tables(0)


    End Sub

    Public Sub TimeHistory_Load()

        'Dim params As New List(Of String)

        'params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        'ds = db.ExecuteStoredProc("Console.GetTimeHistory", params)
        'dgHistory.DataSource = ds.Tables(0)

    End Sub
    Private Function FillRefNums(mode As Integer) As Integer
        RemoveHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged
        Dim params As List(Of String)
        cboRefNum.Text = ""
        cboRefNum.Items.Clear()
        params = New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("Mode/" + mode.ToString)
        Try
            ds = db.ExecuteStoredProc("Console." & "GetRefNums", params)
            If ds.Tables.Count > 0 Then
                Dim row As DataRow 'Represents a row of data in Systems.data.datatable
                For Each row In ds.Tables(0).Rows
                    cboRefNum.Items.Add(row(0).ToString.Trim & " - " & row(1).ToString.Trim)
                Next
                AddHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged
                Return ds.Tables(0).Rows.Count
            Else
                Return 0
            End If
        Catch ex As System.Exception
            'db.WriteLog("FillRefNums: " & RefNum, ex)
            MessageBox.Show("FillRefNums: " & StudyYear & " - " & mode.ToString & "  " & ex.Message)
        End Try
    End Function
    'Method to populate refnums combo box
    Private Sub GetRefNums(Optional strRefNum As String = "")
        Dim NoRefNums As Integer = 0
        Dim selectItem As String = cboStudy.SelectedItem

        Try


            Study = StudyType

            If Not strRefNum Is Nothing Then

                'Study RefNum Box
                NoRefNums = FillRefNums(CurrentSortMode)

                If NoRefNums = 0 Then
                    MessageBox.Show("There are no records for " & StudyYear & " study.", "Not Found")

                    cboStudy.Text = "RAM13"
                    StudyYear = "2013"


                End If



                If Not strRefNum Is Nothing And strRefNum.Length > 5 Then

                    cboRefNum.SelectedIndex = cboRefNum.FindString(strRefNum)
                Else
                    If ReferenceNum Is Nothing Then
                        'RefNum = cboRefNum.Items(0).ToString
                        cboRefNum.SelectedIndex = 0
                    Else
                        RefNum = ReferenceNum.Trim
                        cboRefNum.SelectedIndex = cboRefNum.FindString(ReferenceNum)
                    End If
                End If
            End If

        Catch ex As System.Exception
            'db.WriteLog("GetRefNums: " & RefNum, ex)
            MessageBox.Show("GetRefNums:  " + ex.Message)
        End Try
    End Sub

    'Method to retrieve and populate consultants drop down box


    'Function to retrieve Company name by RefNum
    Private Function GetCompanyName(strRefnum As String) As String
        Dim CompanyName As String
        ''Get Company Name
        If strRefnum = "" Then Return ""
        Dim FirstDash As Integer = strRefnum.IndexOf("-") + 2
        Dim SecondDash As Integer = strRefnum.IndexOf("-", FirstDash + 1) - 2
        Dim DashSpan = SecondDash - FirstDash + 1

        CompanyName = strRefnum.Substring(FirstDash, DashSpan)

        Return CompanyName

    End Function



    'Method to retrieve Company Contact Information
    Private Sub GetCompanyContactInfo()
        CompanyContact = New Contacts()

        Dim params As New List(Of String)
        params.Add("RefNum/" + Utilities.GetDataSetID(cboRefNum.Text, CurrentSortMode))
        ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                dgContacts.DataSource = ds.Tables(0)
                dgContacts.Columns("Password").Visible = False
            End If

        End If
    End Sub

    'Method to retrieve Refinery Contact information

    'Method to retrieve all infomation per Ref Num
    Public Sub GetARefNumRecord(mRefNum As String)

        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum


        cboRefNum.Text = CurrentRefNum
        'Main 

        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub
    'Method to retrieve all infomation per Ref Num
    Public Sub GetRefNumRecord(mRefNum As String)

        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum


        Company = GetCompanyName(mRefNum)
        If Company.Length > 0 Then CurrentCompany = Company

        'cboRefNum.Text = CurrentRefNum
        cboRefNum.Text = CurrentRefNum
        'Main 

        GetCompanyContactInfo()
        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub

    'Retrieve all Refnums per Consultant, Study Type and Study Year for Consultant Tab


    'Routine to populate the Consultant Treeview with Refnums and outstanding issues



#End Region

#Region "UI METHODS"
    Private Sub btnResort_Click(sender As System.Object, e As System.EventArgs) Handles btnResort.Click
        If btnResort.Text = "By Site Name" Then
            CurrentSortMode = 1
            btnResort.Text = "By DataSet ID"
        Else
            CurrentSortMode = 0
            btnResort.Text = "By Site Name"
        End If
        GetRefNums()
    End Sub
    Private Sub txtContinuingIssues_LostFocus(sender As Object, e As System.EventArgs)
        UpdateContinuingIssues()
    End Sub


    Private Sub txtNewConsultingOpportunity_LostFocus(sender As Object, e As System.EventArgs)
        UpdateConsultingOpportunities()
    End Sub

    Private Sub ConsoleTabs_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ConsoleTabs.SelectedIndexChanged
        CurrentTab = ConsoleTabs.SelectedIndex
        LoadTab(CurrentTab)
    End Sub


    Private Sub txtValidationIssues_LostFocus(sender As Object, e As System.EventArgs) Handles txtValidationIssues.LostFocus, txtConsultingOpps.LostFocus, txtContinuingIssues.LostFocus
        Dim params = New List(Of String)

        params.Add("SiteDataSetID/" + Utilities.GetDataSetID(cboRefNum.Text, CurrentSortMode))
        params.Add("ValidationNotes/" + txtValidationIssues.Text.Replace("/", "-"))
        params.Add("ContinuingIssues/" + txtContinuingIssues.Text.Replace("/", "-"))
        params.Add("ConsultingOpportunities/" + txtConsultingOpps.Text.Replace("/", "-"))
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
        If c = 1 Then

            ValidationIssuesIsDirty = False
        Else
            MessageBox.Show("Validation Notes did not update")
        End If

    End Sub


    Private Sub txtValidationIssues_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtValidationIssues.TextChanged
        ValidationIssuesIsDirty = True
    End Sub


    Private Sub tvwCorrespondence_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tvwCorrespondence.KeyDown, tvwClientAttachments.KeyDown, tvwCompCorr.KeyDown, tvwCorrespondence2.KeyDown, tvwClientAttachments2.KeyDown, tvwCompCorr2.KeyDown
        If e.KeyCode = Keys.Space Then
            Process.Start("C:\")
        End If
    End Sub
    Private Sub btnOpenCorrFolder_Click(sender As System.Object, e As System.EventArgs)
        Process.Start(CorrPath)
    End Sub


    Private Sub btnUpdatePresenterNotes_Click(sender As System.Object, e As System.EventArgs)
        Dim params = New List(Of String)

        params.Add("RefNum/" + Utilities.GetDataSetID(cboRefNum.Text, CurrentSortMode))
        params.Add("Notes/" + txtValidationIssues.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
        If c = 1 Then
            MessageBox.Show("Validation Notes updated successfully")
        Else
            MessageBox.Show("Validation Notes did not update")
        End If
    End Sub


    Private Sub btnSecureSend_Click_1(sender As System.Object, e As System.EventArgs) Handles btnSecureSend.Click
        Dim ss As New SecureSend(db, StudyType, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, tvwPolishCorr, tvwPolishCorr2)

        ss.Company = Company
        ss.RefNum = Utilities.GetDataSetID(RefNum, CurrentSortMode)
        'ss.LubeRefNum = LubRefNum
        ss.Study = StudyType
        ss.TemplatePath = TemplatePath
        ss.CorrPath = CorrPath
        ss.PolishCorrPath = PolishCorrPath
        ss.PolishCorrPath2 = PolishCorrPath2
        ss.DrawingPath = DrawingPath
        ss.ClientAttachmentsPath = ClientAttachmentsPath
        ss.Loc = Utilities.GetSite(RefNum, CurrentSortMode)
        ss.User = UserName

        ss.CompCorrPath = CompCorrPath
        ss.CorrPath2 = CorrPath
        ss.DrawingPath2 = DrawingPath
        ss.ClientAttachmentsPath2 = ClientAttachmentsPath

        ss.CompCorrPath2 = CompCorrPath
        ss.CompanyPassword = CompanyPassword
        ss.TempPath = TempPath


        ss.Show()
    End Sub




    Private Sub cboRefNum_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cboRefNum.KeyDown
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True

            cboRefNum.SelectedIndex = FindString(cboRefNum, cboRefNum.Text)

            If cboRefNum.SelectedIndex <> -1 Then
                RefNumChanged(cboRefNum.Text)
            Else
                cboRefNum.SelectedIndex = cboRefNum.FindString(CurrentRefNum)
            End If
        End If

    End Sub

    Private Sub btnPA_Click(sender As System.Object, e As System.EventArgs)
        Dim btn As System.Windows.Forms.Button = CType(sender, System.Windows.Forms.Button)
        If Dir(btn.Tag) <> "" Then Process.Start(btn.Tag)
    End Sub

    Private Sub btnFilesSave_Click(sender As System.Object, e As System.EventArgs)



    End Sub






    Private Sub cboDir_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir.SelectedIndexChanged
        Select Case cboDir.Text

            Case "Corr. Validation"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = True
                tvwCompCorr.Visible = False
                tvwSiteData.Visible = False
                tvwPolishCorr.Visible = False

            Case "Client Attachments"
                tvwClientAttachments.Visible = True
                tvwCorrespondence.Visible = False
                tvwCompCorr.Visible = False
                tvwSiteData.Visible = False
                tvwPolishCorr.Visible = False

            Case "Site Data"
                tvwSiteData.Visible = True
                tvwCorrespondence.Visible = False
                tvwCompCorr.Visible = False
                tvwClientAttachments.Visible = False
                tvwPolishCorr.Visible = False

            Case "Company Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False
                tvwCompCorr.Visible = True
                tvwSiteData.Visible = False
                tvwPolishCorr.Visible = False

            Case "Corr. Polishing"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False
                tvwCompCorr.Visible = False
                tvwSiteData.Visible = False
                tvwPolishCorr.Visible = True
        End Select
    End Sub
    Private Sub cboDir2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir2.SelectedIndexChanged
        Select Case cboDir2.Text

            Case "Corr. Validation"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = True
                tvwCompCorr2.Visible = False
                tvwSiteData2.Visible = False

            Case "Client Attachments"
                tvwClientAttachments2.Visible = True
                tvwCorrespondence2.Visible = False
                tvwCompCorr2.Visible = False
                tvwSiteData2.Visible = False

            Case "Site Data"
                tvwSiteData2.Visible = True
                tvwCorrespondence2.Visible = False
                tvwCompCorr2.Visible = False
                tvwClientAttachments2.Visible = False

            Case "Company Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False
                tvwCompCorr2.Visible = True
                tvwSiteData2.Visible = False

            Case "Corr. Polishing"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False
                tvwCompCorr2.Visible = False
                tvwSiteData2.Visible = False
                tvwPolishCorr2.Visible = True
        End Select
    End Sub






    Private Sub btnOpenWord_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As New Word.Application
        Dim docPN As New Word.Document
        Try
            If Dir(CorrPath & "PN_" & Company & ".doc") = "" Then
                If MsgBox("Do you want to create PN_" & Company & " .doc and stop updating the Validation Notes / Presenter's Notes field through Console?", vbYesNoCancel + vbDefaultButton2, "Click Yes to create the file.") = vbYes Then
                    'MsgBox "Create the file, lock the field, open the file with word."

                    Dim strWork As String = "~ " & Now & " The Presenters Notes document has been built so this field is locked. Please use the Presenters Notes button to open the file. " & Chr(13) & Chr(10) & "----------" & Chr(13) & Chr(10)

                    ' Add the tilde to the Issues Notes field
                    ' First, see if a record exists for this refnum
                    Dim row As DataRow
                    Dim params As New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

                    If ds.Tables.Count > 0 Then
                        row = ds.Tables(0).Rows(0)

                        txtValidationIssues.Text = row("ValidationNotes").ToString


                    Else

                        params = New List(Of String)
                        params.Add("RefNum/" + RefNum)
                        params.Add("Notes/" + txtValidationIssues.Text)

                        Dim Count As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)

                    End If

                    Dim objWriter As New System.IO.StreamWriter(TempPath & "\PNotes.txt", True)
                    objWriter.WriteLine(RefNum)
                    objWriter.WriteLine(txtValidationIssues.Text)
                    objWriter.Close()

                    ' Create the document.
                    File.Copy(TemplatePath & "PN_Template.doc", TempPath & "PN_Template.doc", True)
                    msword = New Word.Application
                    'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                    docPN = msword.Documents.Open(TempPath & "PN_Template.doc")

                    docPN.SaveAs(CorrPath & "PN_" & Company & ".doc")

                    ' Put the CoLoc and the Notes into the document.
                    msword.Run("SubsAuto")
                End If
            Else

                msword = New Word.Application
                'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                docPN = msword.Documents.Open(CorrPath & "PN_" & Company & ".doc")
                msword.Visible = True
            End If

        Catch Ex As System.Exception
            'db.WriteLog("btnOpenWord", Ex)
        End Try


    End Sub



    Private Sub MainConsole_DragDrop(ByVal sender As Object, _
                        ByVal e As System.Windows.Forms.DragEventArgs) _
                        Handles MyBase.DragDrop, ConsoleTabs.DragDrop, tvwClientAttachments.DragDrop, tvwClientAttachments2.DragDrop, tvwCompCorr.DragDrop, tvwCompCorr2.DragDrop, tvwCorrespondence.DragDrop, tvwCorrespondence2.DragDrop,
                        tvwDrawings.DragDrop, tvwDrawings2.DragDrop, tvwPolishCorr.DragDrop, tvwPolishCorr2.DragDrop
        'DEBUG NOTE: If this isn't firing in the IDE, try closing VS and reopening but NOT "As Administrator"
        '  For further info, see https://stackoverflow.com/questions/68598/how-do-i-drag-and-drop-files-into-an-application

        Dim strFile As String = String.Empty
        Dim ext As String
        Dim SavedFiles As New List(Of String)
        Dim strFileName As String
        Dim strDocFileName As String
        Dim Solomon As Boolean = False
        Dim mailItem As Outlook._MailItem = Nothing
        If cboRefNum.Text <> "" Then
            lblStatus.ForeColor = Color.DarkGreen
            lblStatus.Text = "Saving..."

            Dim filenames() As String = Directory.GetFiles(TempPath, "*.*")
            For Each sFile In filenames
                Try
                    File.Delete(sFile)
                Catch ex As System.Exception
                    Debug.Print(ex.Message)
                End Try
            Next




            If Not e.Data.GetDataPresent(DataFormats.Text) = False Then
                'check if this is an outlook message. The outlook messages, all contain a FileContents attribute. If not, exit.
                Dim formats() As String = e.Data.GetFormats()

                If formats.Contains("FileContents") = False Then Exit Sub

                'they are dragging the attachment
                If (e.Data.GetDataPresent("Object Descriptor")) Then
                    Dim app As New Microsoft.Office.Interop.Outlook.Application() ' // get current selected items
                    Dim selection As Microsoft.Office.Interop.Outlook.Selection
                    Dim myText As String = ""
                    selection = app.ActiveExplorer.Selection
                    If selection IsNot Nothing Then


                        For i As Integer = 0 To selection.Count - 1

                            mailItem = TryCast(selection.Item(i + 1), Outlook._MailItem)

                            If mailItem IsNot Nothing Then

                                'If mailItem.Recipients.Count > 0 Then Solomon = mailItem.Recipients(1).Address.Contains("SOLOMON")
                                myText = ""
                                myText = e.Data.GetData("Text")  'header text
                                myText += vbCrLf + vbCrLf
                                myText += mailItem.Body  'Plain Text Body Message

                                If mailItem.SenderEmailAddress.ToUpper.Contains("SOLOMON") Then Solomon = True

                                'now save the attachments with the same file name and then 1,2,3 next to it
                                For Each att As Attachment In mailItem.Attachments
                                    If att.FileName.Substring(0, 5).ToUpper <> "IMAGE" Then
                                        Try
                                            ext = att.FileName.Substring(att.FileName.LastIndexOf(".") + 1, att.FileName.Length - att.FileName.LastIndexOf(".") - 1)
                                        Catch
                                            ext = ""
                                        End Try

                                        If ext.ToUpper = "ZIP" Then
                                            If Dir(TempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(TempPath & "Extracted Files\")
                                            Dim zfilenames() As String = Directory.GetFiles(TempPath & "Extracted Files", "*.*")
                                            For Each sFile In zfilenames
                                                Try
                                                    File.Delete(sFile)
                                                Catch ex As System.Exception
                                                    'db.WriteLog("DragDrop", ex)
                                                    Debug.Print(ex.Message)
                                                End Try
                                            Next
                                            If Solomon Then
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & att.FileName)
                                            Else
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (RECV) - " & att.FileName)
                                            End If

                                            Utilities.UnZipFiles(TempPath & att.FileName, TempPath, CompanyPassword)
                                            OldTempPath = TempPath
                                            TempPath = TempPath & "Extracted Files\"

                                        Else
                                            If Solomon Then
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & att.FileName)
                                            Else
                                                att.SaveAsFile(TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (RECV) - " & att.FileName)
                                            End If
                                        End If
                                    End If
                                Next


                            End If




                        Next


                        strFileName = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)

                        ' More often, use this technique to name the text file.
                        If mailItem.Subject Is Nothing Or mailItem.Subject = "" Then
                            strFileName = Utilities.GetSite(cboRefNum.Text, CurrentSortMode)
                            strFileName = strFileName & ".txt"
                        Else
                            strFileName = strFileName & " - " & mailItem.Subject.Replace(":", " ") & ".txt"
                        End If

                        If My.Computer.Keyboard.ShiftKeyDown Then
                            If Solomon Then
                                strFileName = CompCorrPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & strFileName
                            Else
                                strFileName = CompCorrPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (RECV) - " & strFileName
                            End If
                            MessageBox.Show("Saved to Company Correspondence Directory")
                        Else
                            If Solomon Then
                                strFileName = TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & strFileName
                            Else
                                strFileName = TempPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (RECV) - " & strFileName
                            End If
                        End If
                        Dim strw As New StreamWriter(strFileName)

                        strw.WriteLine(myText)
                        strw.Close()
                        strw.Dispose()



                        strDocFileName = Utilities.ConvertToDoc(TempPath, strFileName)
                        File.Delete(strFileName)

                    End If

                End If
            End If
        End If

        SaveFiles()

    End Sub

    Private Sub SaveFiles()
        Dim CopyFile As String
        Dim Site As String
        Dim ext As String
        Dim success As Boolean = True
        Try


            If CompanyPassword.Length = 0 Then
                MessageBox.Show("Must have a Company Password to Encrypt files", "Save Error")
                Exit Sub
            End If
            For Each f In Directory.GetFiles(TempPath)
                ext = GetExtension(f)
                CopyFile = Utilities.ExtractFileName(f)

                CopyFile = FixFilename(ext, CopyFile)


                If ext.Substring(0, 3).ToUpper = "DOC" Or ext.Substring(0, 3).ToUpper = "XLS" Then

                    success = RemovePassword(TempPath & CopyFile, "", CompanyPassword)

                End If

                If Not success Then MessageBox.Show(CopyFile & " is password protected.", "Warning")

                File.Copy(TempPath & CopyFile, CorrPath & CopyFile, True)
                lblStatus.Text = "Saving " & CopyFile & "...."
                File.Delete(TempPath & CopyFile)

            Next
            lblStatus.Text = "Ready"
        Catch ex As System.Exception
            db.WriteLog("btnSaveFiles", ex)
            lblStatus.ForeColor = Color.DarkRed
            lblStatus.Text = "Error saving files"
        End Try
    End Sub





    Private Sub tvwCorrespondence_BeforeExpand(sender As System.Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvwCorrespondence.BeforeExpand, tvwClientAttachments.BeforeExpand, tvwCompCorr.BeforeExpand, tvwCorrespondence2.BeforeExpand, tvwClientAttachments2.BeforeExpand, tvwCompCorr2.BeforeExpand, tvwPolishCorr.BeforeExpand, tvwPolishCorr2.BeforeExpand
        e.Node.Nodes.Clear()
        ' get the directory representing this node
        Dim mNodeDirectory As IO.DirectoryInfo
        mNodeDirectory = New IO.DirectoryInfo(e.Node.Tag.ToString)

        ' add each subdirectory from the file system to the expanding node as a child node
        For Each mDirectory As IO.DirectoryInfo In mNodeDirectory.GetDirectories
            ' declare a child TreeNode for the next subdirectory
            Dim mDirectoryNode As New TreeNode
            ' store the full path to this directory in the child TreeNode's Tag property
            mDirectoryNode.Tag = mDirectory.FullName
            ' set the child TreeNodes's display text
            mDirectoryNode.Text = mDirectory.Name
            ' add a dummy TreeNode to this child TreeNode to make it expandable
            mDirectoryNode.Nodes.Add("*DUMMY*")
            mDirectoryNode.ImageKey = CacheShellIcon(mDirectoryNode.Tag)
            mDirectoryNode.SelectedImageKey = mDirectoryNode.ImageKey & "-open"
            ' add this child TreeNode to the expanding TreeNode
            e.Node.Nodes.Add(mDirectoryNode)
        Next

        ' add each file from the file system that is a child of the argNode that was passed in
        For Each mFile As IO.FileInfo In mNodeDirectory.GetFiles
            ' declare a TreeNode for this file
            Dim mFileNode As New TreeNode
            ' store the full path to this file in the file TreeNode's Tag property
            mFileNode.Tag = mFile.FullName
            ' set the file TreeNodes's display text
            mFileNode.Text = mFile.Name
            mFileNode.ImageKey = CacheShellIcon(mFileNode.Tag)
            mFileNode.SelectedImageKey = mFileNode.ImageKey
            mFileNode.SelectedImageKey = mFileNode.ImageKey & "-open"
            ' add this file TreeNode to the TreeNode that is being populated
            e.Node.Nodes.Add(mFileNode)
        Next

    End Sub

    Private Sub tvwCorrespondence_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvwCorrespondence.NodeMouseDoubleClick, tvwClientAttachments.NodeMouseDoubleClick, tvwCompCorr.NodeMouseDoubleClick, tvwCorrespondence2.NodeMouseDoubleClick, tvwClientAttachments2.NodeMouseDoubleClick, tvwCompCorr2.NodeMouseDoubleClick, tvwPolishCorr.NodeMouseDoubleClick, tvwPolishCorr2.NodeMouseDoubleClick
        ' try to open the file
        Try
            Process.Start(e.Node.Tag)
        Catch ex As System.Exception
            'db.WriteLog("tvwCorrespondence_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try
    End Sub
    Private Sub MainConsole_DragEnter(ByVal sender As Object, _
                           ByVal e As System.Windows.Forms.DragEventArgs) _
                           Handles MyBase.DragEnter, ConsoleTabs.DragEnter, tvwCorrespondence.DragEnter, tvwCompCorr.DragEnter, tvwCompCorr2.DragEnter, tvwClientAttachments.DragEnter, tvwCorrespondence2.DragEnter, tvwClientAttachments2.DragEnter, tvwPolishCorr.DragEnter, tvwPolishCorr2.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub


    Private Sub btnHelp_Click(sender As System.Object, e As System.EventArgs) Handles btnHelp.Click


        Dim strHelpFilePath As String

        strHelpFilePath = "K:\STUDY\SAI Software\Console2012\"


        Process.Start(strHelpFilePath & "RAM Study Console.docx")

    End Sub



    Private Sub btnLog_Click(sender As System.Object, e As System.EventArgs)
        ' These are the "file" buttons on the right of the
        ' Validation Summary tab.
        ' Basically, here we just open the path/file stored in the tag
        ' property of the button.
        ' This property was set by SetFileButtons earlier when the Refnum
        ' was set/changed.
        Dim wb As Object
        Dim xl As New Excel.Application


        Try

            xl.Visible = True
            wb = xl.Workbooks.Open("K:\Study\" & "Console." & StudyType & "\" & StudyYear & "\" & cboStudy.SelectedItem.ToString.Substring(0, 3) & "\Refinery\" & RefNum & "\" & RefNum & "_Log.xls")
            xl.Visible = True

            ' Next line is important.
            ' If you don't do this, if the user closes Excel then clicks a file button
            ' again, you will get an Excel window with just the 'frame'. The inside
            ' of the 'window' will be empty!
            xl = Nothing

        Catch ex As System.Exception

            MsgBox("Unable to open file. If you have Excel open, and are editing a cell, get out of that mode and retry. ", vbOKOnly, "Unable to open file")
            'db.WriteLog("btnLOG_Click", ex)
        End Try
    End Sub


    Private Sub MainConsole_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Initialize()
    End Sub

    Private Sub MainConsole_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not Spawn Then SaveLastStudy()
        End
    End Sub



    Public Sub SetTree(tvw As TreeView, mRootPath As String)
        Try
            tvw.Nodes.Clear()
            Dim mRootNode As New TreeNode
            mRootNode.Text = mRootPath
            mRootNode.Tag = mRootPath
            mRootNode.ImageKey = CacheShellIcon(mRootPath)
            mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
            mRootNode.Nodes.Add("*")
            tvw.Nodes.Add(mRootNode)
            tvw.ExpandAll()
            tvw.Refresh()
        Catch ex As System.Exception
            'db.WriteLog("SetTree", ex)
        End Try
    End Sub

    Private Sub btnUpdateNotes_Click(sender As System.Object, e As System.EventArgs)


        Dim bIssues As Boolean = False

        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

        If ds.Tables.Count = 0 Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "InsertValidationNotes", params)

        Else
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "UpdateValidationNotes", params)
        End If

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)


        ds = db.ExecuteStoredProc("Console." & "UpdateComments", params)


        BuildSummaryTab()



    End Sub



    Private Sub btnPrintIssues_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As New Word.Application
        Dim docIssues As New Word.Document
        Dim objWriter As New System.IO.StreamWriter(TempPath & "\Issues.txt", True)
        objWriter.WriteLine(RefNum)
        objWriter.WriteLine("Printed by: " & mUserName & " At: " & Now())
        objWriter.WriteLine("")
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.WriteLine("Validation Issues/Presenter's Notes:")
        objWriter.WriteLine(txtValidationIssues.Text)
        objWriter.WriteLine("")
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.WriteLine("Consulting Opportunities:")
        objWriter.WriteLine(txtConsultingOpps.Text)
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.Close()

        ' Set up an object that will be Word.

        docIssues = msword.Documents.Open(TempPath & "Issues.txt")
        docIssues.PrintOut()

        ' Make Word visible
        'MSWord.Visible = True

        docIssues.Close()

        ' Cut the connection.
        msword = Nothing

        MsgBox("The text in the 'Validation Issues / Presenter's Notes' box has been sent to your default printer.")

    End Sub

    Private Sub PopulateSiteDataTreeView(tvw As TreeView)

        If Directory.Exists(SiteDataPath) Then
            SetTree(tvw, SiteDataPath)
        End If

    End Sub

    Private Sub PopulateCompanyTreeView(tvw As TreeView)

        If Directory.Exists(CompanyPath) Then
            SetTree(tvw, CompanyPath)
        End If

    End Sub


    Private Sub PopulateCorrespondenceTreeView(tvw As TreeView)

        If Directory.Exists(CorrPath) Then
            SetTree(tvw, CorrPath)
        End If

    End Sub
    Private Sub PopulatePolishCorrespondenceTreeView(tvw As TreeView)

        If Directory.Exists(PolishCorrPath) Then
            SetTree(tvw, PolishCorrPath)
        End If

    End Sub


    Private Sub PopulateCATreeView(tvw As TreeView)

        If Directory.Exists(ClientAttachmentsPath) Then
            SetTree(tvw, ClientAttachmentsPath)
        End If

    End Sub


    Private Sub ChangedStudy(Optional mRefNum As String = "")
        StudyYear = cboStudy.Text.Substring(cboStudy.Text.Length - 2, 2)
        If StudyYear.Substring(2, 2) <> CurrentStudyYear Then
            cboStudy.BackColor = Color.Yellow
            lblWarning.Visible = True
        Else
            cboStudy.BackColor = Color.White
            lblWarning.Visible = False
        End If
        GetRefNums(cboStudy.Text)

    End Sub
    'If Study Changes, then re-populate RefNums
    Private Sub cboStudy_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboStudy.SelectedIndexChanged
        ChangedStudy()
    End Sub

    'Method to populate the labels for Contacts
    Private Sub FillContacts()

        ClearFields()


    End Sub
    <Obsolete("Been incorrectly implemented, change for 2013 Olefins and 2014 Fuels", False)>
    Private Function GetReturnPassword(RefNum As String, salt As String) As String
        Return Utilities.XOREncryption(Trim(UCase(RefNum)), salt)
    End Function
    Public Sub RefNumChanged(Optional mRefNum As String = "")

        SetTabDirty(True)
        SetPaths()
        cboRefNum.Text = mRefNum
        GetRefNumRecord(mRefNum)
        SaveRefNum(cboStudy.Text, mRefNum)
        CompanyPassword = GetCompanyPassword()

        If mRefNum.Length > 0 Then RefNum = mRefNum

    End Sub
    Private Function GetCompanyPassword() As String
        Dim pw As String = ""

        Dim params = New List(Of String)
        params.Add("refNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetCompanyPassword", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then

            pw = ds.Tables(0).Rows(0)("CompanyPassword").ToString

        End If

        Return pw
    End Function
    Private Sub cboRefNum_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboRefNum.SelectedIndexChanged
        RefNumChanged(cboRefNum.Text)
    End Sub

    Private Sub btnValidationStatus_Click(sender As System.Object, e As System.EventArgs) Handles btnValidationStatus.Click
        Dim xl As Excel.Application
        Dim w As Excel.Workbook

        If File.Exists("K:\STUDY\RAM\" & StudyYear & "\Company\Correspondence\RAM" & StudyYear.Substring(2, 2) & " Validation Status.xls") Then
            btnValidationStatus.Enabled = True
            xl = Utilities.GetExcelProcess()
            xl.Visible = True
            w = xl.Workbooks.Open("K:\STUDY\RAM\" & StudyYear & "\Company\Correspondence\RAM" & StudyYear.Substring(2, 2) & " Validation Status.xls")
        Else
            btnValidationStatus.Enabled = False
        End If
    End Sub

    Private Sub btnValidationComments_Click(sender As System.Object, e As System.EventArgs) Handles btnValidationComments.Click
        Dim xl As Excel.Application
        Dim w As Excel.Workbook
        If File.Exists("K:\STUDY\RAM\" & StudyYear & "\Correspondence\RAM" & StudyYear.Substring(2, 2) & " Datacheck Comments.xls") Then
            btnValidationComments.Enabled = True
            xl = Utilities.GetExcelProcess()
            xl.Visible = True
            w = xl.Workbooks.Open("K:\STUDY\RAM\" & StudyYear & "\Correspondence\RAM" & StudyYear.Substring(2, 2) & " Datacheck Comments.xls")
        Else
            btnValidationComments.Enabled = False
        End If
    End Sub





    Private Sub btnDirRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnDirRefresh.Click
        BuildDirectoriesTab()
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Dim t As New TimeSpan(0, 0, Time)
        Dim l As New TimeSpan(0, 0, LastTime)
        Dim hours As String
        Dim minutes As String
        Dim seconds As String
        Dim lhours As String
        Dim lminutes As String
        Dim lseconds As String
        hours = t.Hours.ToString()
        minutes = t.Minutes.ToString
        seconds = t.Seconds.ToString
        If hours.Length = 1 Then hours = "0" & hours
        If minutes.Length = 1 Then minutes = "0" & minutes
        If seconds.Length = 1 Then seconds = "0" & seconds
        lhours = l.Hours.ToString()
        lminutes = l.Minutes.ToString
        lseconds = l.Seconds.ToString
        If lhours.Length = 1 Then lhours = "0" & lhours
        If lminutes.Length = 1 Then lminutes = "0" & lminutes
        If lseconds.Length = 1 Then lseconds = "0" & lseconds
        Time += 1
        ConsoleTimer.Text = "Timer : " & hours & ":" & minutes & ":" & seconds & "   Last Time: " & lhours & ":" & lminutes & ":" & lseconds
    End Sub





    Private Sub ConsoleTimer_DoubleClick(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.DoubleClick
        LastTime = Time
        Time = 0
    End Sub

    Private Sub ConsoleTimer_Click_1(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.Click
        If Timer1.Enabled Then
            ConsoleTimer.ForeColor = Color.DarkRed
            Timer1.Enabled = False
        Else
            ConsoleTimer.ForeColor = Color.Green
            Timer1.Enabled = True
        End If
    End Sub


    Private Sub btnKill_Click(sender As System.Object, e As System.EventArgs) Handles btnKill.Click
        Dim dr As New DialogResult()
        dr = MessageBox.Show("This will kill all OPEN Excel and Word Documents.  Please <SAVE> any Word or Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel)
        If dr = DialogResult.OK Then
            Utilities.KillProcesses("WinWord")
            Utilities.KillProcesses("Excel")
        End If
    End Sub
#End Region

    Private Sub chkLockPN_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkLockPN.CheckedChanged
        If chkLockPN.Checked = False Then
            txtValidationIssues.ReadOnly = False
            txtContinuingIssues.ReadOnly = False
            txtConsultingOpps.ReadOnly = False
        Else
            txtValidationIssues.ReadOnly = True
            txtContinuingIssues.ReadOnly = True
            txtConsultingOpps.ReadOnly = True
        End If
    End Sub


    Private Sub btnPresentationSchedule_Click(sender As System.Object, e As System.EventArgs) Handles btnPresentationSchedule.Click
        If File.Exists("K:\STUDY\RAM\" & StudyYear & "\Presentations\presentation schedule " & StudyYear & " RAM Study.xls") Then
            Process.Start("K:\STUDY\RAM\" & StudyYear & "\Presentations\presentation schedule " & StudyYear & " RAM Study.xls")
        Else
            MessageBox.Show("Missing File:  " & "K:\STUDY\RAM\" & StudyYear & "\Presentations\presentation schedule " & StudyYear & " RAM Study.xls", "Error")
        End If
    End Sub

    Private Sub btnPresentationPlan_Click(sender As System.Object, e As System.EventArgs) Handles btnPresentationPlan.Click
        If File.Exists("K:\STUDY\RAM\" & StudyYear & "\Presentations\Presentation Plan.xls") Then
            Process.Start("K:\STUDY\RAM\" & StudyYear & "\Presentations\Presentation Plan.xls")
        Else
            MessageBox.Show("Missing File:  " & "K:\STUDY\RAM\" & StudyYear & "\Presentations\Presentation Plan.xls", "Error")
        End If

    End Sub
End Class
