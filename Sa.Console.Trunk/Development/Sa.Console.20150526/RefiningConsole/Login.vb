﻿Imports System.Data.SqlClient
Imports SA.Internal.Console.DataObject
Imports System.Reflection
Imports System
Imports Solomon.Common.DocumentManagement.MFiles
Imports Solomon.Common.DocumentManagement.MFiles.WebAccess
Imports System.Configuration

Public Class Login
    Dim db As DataObject
    Dim RefNum As String
    Dim UserName As String
    Dim Password As String
    Dim StudyType As String
    Dim StudyYear As String
    Dim Entered As Boolean = False
    Dim Console As Object
    Dim VPN As Boolean = False
    Dim ScreenName As String = ""

    'Private _mfiles As Solomon.Common.DocumentManagement.MFiles.MFilesManager
    Private _url As String = ConfigurationManager.AppSettings("MfilesUrl")
    Private _vaultGuid As String = ConfigurationManager.AppSettings("VaultGuid")
    'Private _mfilesId As String = My.User.Name ' DevConfigHelper.ReadConfig("MFilesId")
    'Private _mfilesPw As String = String.Empty 'Console.MfilesPassword ' Login.MfilesPassword ' DevConfigHelper.ReadConfig("MFilesPw")
    Private _profileConsoleTempPath As String = ProfileConsolePath + "temp\"
    Private _settingPath As String = _profileConsoleTempPath + "ConsoleSettings\"

    Private Sub LoginClick()
        If cboVertical.Text.Trim().Length < 1 Then
            MsgBox("Please select a Vertical and login again")
            Exit Sub
        End If
        Try
            lblError.Visible = False
            Dim tpw As String = txtLogin.Text.ToUpper

            'If txtMfilesPassword.Text.Trim().Length < 1 Then
            '    txtMfilesPassword.Text = DevConfigHelper.ReadConfig("MFilesPw")
            'End If

            'If Main.ConsoleVertical = VerticalType.REFINING Then ' Or Main.ConsoleVertical = VerticalType.OLEFINS Then
            '    If txtMfilesPassword.Text.Trim().Length < 1 Then
            '        MsgBox("Please enter your Outlook password. This is needed in order to access MFiles.")
            '        Exit Sub
            '    End If
            'End If
            Select Case Main.ConsoleVertical
                'Case "REFININGDEV"
                '    db = New DataObject(DataObject.StudyTypes.REFININGDEV, txtLogin.Text, txtPassword.Text)
                '    Console = New MainConsole()
                Case VerticalType.REFINING
                    db = New DataObject(DataObject.StudyTypes.REFINING, txtLogin.Text, txtPassword.Text)
                    Console = New MainConsole()
                    ConsoleVertical = VerticalType.REFINING
                Case VerticalType.OLEFINS
                    db = New DataObject(DataObject.StudyTypes.OLEFINS, txtLogin.Text, txtPassword.Text)
                    Console = New OlefinsMainConsole()
                    ConsoleVertical = VerticalType.OLEFINS
                    Console.MonitoringInterval = 0.5
                    'Main.MfilesPassword = txtMfilesPassword.Text.Trim()
                Case VerticalType.CQM
                    db = New DataObject(DataObject.StudyTypes.OLEFINS, txtLogin.Text, txtPassword.Text)
                    Console = New OlefinsMainConsole()
                    ConsoleVertical = VerticalType.CQM
                    Console.MonitoringInterval = 4
                Case VerticalType.SEEC
                    db = New DataObject(DataObject.StudyTypes.OLEFINS, txtLogin.Text, txtPassword.Text)
                    Console = New OlefinsMainConsole()
                    ConsoleVertical = VerticalType.SEEC
                    Console.MonitoringInterval = 1
                Case VerticalType.POWER
                    db = New DataObject(DataObject.StudyTypes.POWER, txtLogin.Text, txtPassword.Text)
                    Console = New PowerConsole()
                    ConsoleVertical = VerticalType.POWER
                Case VerticalType.RAM
                    'CHANGE
                    'db = New DataObject(DataObject.StudyTypes.RAM, "mgv", "solomon2012")
                    If CheckRAMPassword(txtLogin.Text, txtPassword.Text) Then
                        UserName = ScreenName
                        Console = New RAMMainConsole()
                    Else
                        lblError.Text = "Invalid Login"
                        lblError.Visible = True
                        Exit Sub
                    End If
                    ConsoleVertical = VerticalType.RAM
                Case VerticalType.PIPELINES
                    db = New DataObject(DataObject.StudyTypes.PIPELINES, txtLogin.Text, txtPassword.Text)
                    Console = New TPMainConsole()
                    ConsoleVertical = VerticalType.PIPELINES
                Case VerticalType.NGPP
                    db = New DataObject(DataObject.StudyTypes.NGPP, txtLogin.Text, txtPassword.Text)
                    Console = New NgppConsole(db)
                    ConsoleVertical = VerticalType.NGPP
                Case VerticalType.LNG
                    db = New DataObject(DataObject.StudyTypes.LNG, txtLogin.Text, txtPassword.Text)
                    Console = New LNGConsole(db)
                    ConsoleVertical = VerticalType.LNG
                Case Else
                    MessageBox.Show("Unable to find the vertical you chose. Please contact support.")
                    Exit Sub
            End Select

            If IsNothing(db) Then
                MsgBox("Console was unable to get data from the database")
                lblError.Text = "Database Error"
                lblError.Visible = True
                Exit Sub
            ElseIf db.DBError IsNot Nothing Then
                MsgBox(db.DBError.ToString())
                lblError.Text = "Database Error"
                lblError.Visible = True
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show("Please make sure you have access to the database: " + ex.InnerException.ToString, "Console")
            lblError.Text = ex.Message
            lblError.Visible = True
            Exit Sub
        End Try
        'If Main.ConsoleVertical = VerticalType.REFINING Then 'Main.ConsoleVertical = VerticalType.OLEFINS
        '    Main.MfilesPassword = txtMfilesPassword.Text.Trim()
        '    'test login here
        '    Try
        '        Debug.WriteLine("######Calling New MFilesManager " + DateTime.Now.ToString())
        '        _mfiles = New Solomon.Common.DocumentManagement.MFiles.MFilesManager(_url, _vaultGuid, _mfilesId, txtMfilesPassword.Text.Trim())
        '        Debug.WriteLine("######Returned from calling New MFilesManager " + DateTime.Now.ToString())
        '        _mfiles.Logout() 'unsuccessful  login does not err, but logout after it will err.
        '    Catch MfilesLoginex As Exception
        '        Dim msg As String = MfilesLoginex.Message
        '        If msg.Contains("Login to vault failed") Then
        '            Dim tryAgainMsg As String = msg + ". Please retype your Outlook password and try again."
        '            MsgBox(tryAgainMsg)
        '            Exit Sub
        '        End If
        '    End Try
        'End If
        Try

            If Main.ConsoleVertical <> VerticalType.LNG And Main.ConsoleVertical <> VerticalType.NGPP And Main.ConsoleVertical <> VerticalType.REFINING And ConsoleVertical <> VerticalType.POWER Then 'StudyType does not exist for NGPP
                Console.StudyType = Main.ConsoleVertical.ToString() ' StudyType
            End If
            If Not IsNothing(RefNum) AndAlso RefNum <> "" Then
                Console.ReferenceNum = RefNum
                Console.Spawn = True
            End If
            If ConsoleVertical <> VerticalType.POWER Then
                Console.Password = txtPassword.Text
            End If
            Console.UserName = IIf(UserName Is Nothing, txtLogin.Text, UserName)
            If ConsoleVertical = VerticalType.OLEFINS Or ConsoleVertical = VerticalType.CQM Or Main.ConsoleVertical = VerticalType.REFINING Or ConsoleVertical = VerticalType.POWER Then
                Console.UserWindowsProfileName = UserName ' = Environment.UserName + "." + Environment.UserDomainName
            End If
            Console.CurrentStudyYear = My.Settings.Item("CurrentStudyYear")
            Console.VPN = VPN

            Console.Show()
            Me.Hide()
            Me.WindowState = FormWindowState.Minimized
            If txtLogin.Text.Trim().ToUpper() <> "DBB" And txtLogin.Text.Trim().ToUpper() <> "JDW" And txtLogin.Text.Trim().ToUpper() <> "EJB" Then Me.Close()
        Catch ex As System.Exception
            MessageBox.Show("Errors on initializing the form:" + ex.Message, "Console")
            lblError.Text = ex.Message
            lblError.Visible = True
        End Try

    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        Entered = True
        Me.Cursor = Cursors.WaitCursor
        Dim writer As StreamWriter
        Try
            writer = New StreamWriter(_settingPath + "ConsoleLastVertical.txt")
            writer.WriteLine(cboVertical.Text)
        Catch
        Finally
            Try
                writer.Close()
            Catch
            End Try
        End Try
        LoginClick()
        Me.Cursor = Cursors.Default
    End Sub

    Private Function CheckRAMPassword(RAMUserName As String, RAMPassword As String) As Boolean
        
        Dim strPassword As String = ""
        Dim CompanyID As String = "000SAI"
        Dim pw As String = ""

        Dim params As New List(Of String)
        params.add("UserID/" & RAMUserName)


        Dim ds As DataSet = db.ExecuteStoredProc("Console.GetRAMPassword", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                strPassword = ds.Tables(0).Rows(0)("Password")
                ScreenName = ds.Tables(0).Rows(0)("ScreenName")
                CompanyID = ds.Tables(0).Rows(0)("CompanyID")
            End If
            Dim salt As String = "dog" + ScreenName.ToLower + "butt" & CompanyID.Trim & "steelers"
            pw = Utilities.EncryptedPassword(txtPassword.Text, salt)
        End If

        If pw = strPassword Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub Login_Activated(sender As Object, e As EventArgs) Handles Me.Activated
    End Sub

    Private Sub Login_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then
            LoginClick()
        End If
    End Sub

    Private Sub Login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        For Each enumValue In System.Enum.GetValues(GetType(Main.VerticalType))
            cboVertical.Items.Add(enumValue.ToString())
        Next

        Dim lastVertical As String = String.Empty

        Try
            Using reader = New StreamReader(_settingPath + "ConsoleLastVertical.txt")
                lastVertical = reader.ReadLine()
            End Using
        Catch
        End Try
        For counter As Integer = 0 To cboVertical.Items.Count - 1
            If cboVertical.Items(counter).ToString() = lastVertical Then
                cboVertical.SelectedIndex = counter
                Exit For
            End If
        Next

        Try
            Me.Height = 223 '180
            If CBool(ConfigurationManager.AppSettings("DbTesting").ToString()) Then
                Me.Text = " **Testing **"
                'Me.Height = 260
            End If
        Catch
        End Try

        VPN = False
        'Dim s() As String = System.Environment.GetCommandLineArgs()
        'If s.Length > 1 Then
        '    If s(1).Length > 2 Then
        '        StudyType = s(1)
        '        StudyYear = s(2)
        '        If s.Length > 3 Then VPN = s(3)
        '    End If
        'Else
        '    StudyType = "REFINING"
        '    StudyYear = "14"
        'End If

        'Dim rd As RadioButton = Controls.Find(StudyType, False)(0)
        'rd.Checked = True
        Me.Show()
        If UserName = "" Then
            If StudyType = "RAM" Then
                txtLogin.Text = Environment.UserName & "@sa"
            Else
                txtLogin.Text = Environment.UserName
            End If
        End If

        If txtLogin.Text <> "" Then txtPassword.Focus()



        Me.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) / 2
        Me.Top = (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) / 2


        'txtMfilesPassword.Select()

        'Me.Height = 252
    End Sub

    'Private Sub txtPassword_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress, txtLogin.KeyPress
    '    If e.KeyChar = Chr(13) Then LoginClick()
    'End Sub

    Private Sub cboVertical_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVertical.SelectedIndexChanged
        'lblMfilesPassword.Visible = False
        'txtMfilesPassword.Enabled = False
        'txtMfilesPassword.Visible = False

        For count As Integer = 0 To System.Enum.GetValues(GetType(VerticalType)).Length - 1
            If cboVertical.Text.Trim().ToUpper() = System.Enum.GetName(GetType(VerticalType), count) Then
                Main.ConsoleVertical = count
                Exit For
            End If
        Next

        Select Case Main.ConsoleVertical
            'Case VerticalType.REFINING ', VerticalType.OLEFINS
            '    lblMfilesPassword.Visible = True
            '    txtMfilesPassword.Enabled = True
            '    txtMfilesPassword.Visible = True
            Case VerticalType.RAM
                txtLogin.Enabled = True
                txtPassword.Enabled = True
            Case VerticalType.POWER
            Case VerticalType.PIPELINES ' "PL && T"
            Case VerticalType.NGPP
            Case Else
        End Select
        Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
        Dim version As System.Version = assembly.GetName().Version
        Me.Text = "Console Login" + " " + version.ToString() ' & " 20" & StudyYear
        'Me.Text = "Console " + StudyType '& " 20" & StudyYear
    End Sub


End Class

