﻿Imports Solomon.Common.DocumentManagement.MFiles
Imports System

Public Class DocsMFilesUploadFacade
    Private _mfilesAccess As DocsMFilesAccess
    Private _mfilesManager As MFilesManager
    Private _benchmarkingParticipantDocs As List(Of BenchmarkingParticipantDoc)
    Private _refiningGeneralDocs As List(Of RefiningGeneralDoc)

    Public Sub New(mfilesAccess As DocsMFilesAccess)
        _mfilesAccess = mfilesAccess
    End Sub

    Public Sub New(mfilesManager As MFilesManager)
        _mfilesManager = mfilesManager
    End Sub

#Region "Bench"
    Public Sub AddBenchmarkingParticipantDocForUpload(deliverableType As String, filePath As String,
        Optional fileNameWithoutExtension As String = Nothing, Optional docDate As DateTime? = Nothing)

        If Not IsNothing(_mfilesAccess) Then
            If IsNothing(_benchmarkingParticipantDocs) Then
                _benchmarkingParticipantDocs = New List(Of BenchmarkingParticipantDoc)
            End If
            Dim doc As New BenchmarkingParticipantDoc
            Dim delType As Integer
            Dim deliverableTypes As Dictionary(Of Integer, String) = _mfilesAccess.GetBenchmarkingDeliverableTypes()
            For Each item In deliverableTypes
                If item.Value.ToUpper() = deliverableType.ToUpper() Then
                    delType = item.Key
                    Exit For
                End If
            Next
            doc.DeliverableType = delType
            doc.BenchmarkingParticipant = _mfilesAccess.BenchmarkingParticipantId
            Dim fileName As String = String.Empty
            If Not IsNothing(fileNameWithoutExtension) AndAlso fileNameWithoutExtension.Length > 0 Then
                fileName = fileNameWithoutExtension
            Else
                fileName = GetFileNameWithoutExtension(ExtractFileName(filePath))
            End If
            Dim ext As String = GetFileExtension(fileName)
            doc.DocNameWithoutExtension = fileName
            doc.FilePath = filePath
            If Not IsNothing(docDate) Then
                doc.DocDate = docDate
            Else
                doc.DocDate = DateTime.Today
            End If
            _benchmarkingParticipantDocs.Add(doc)
        End If
    End Sub
    Public Function UploadBenchmarkingParticipantDocs() As String
        'return errors on error else return blank
        Try
            If Not IsNothing(_mfilesAccess) AndAlso Not IsNothing(_benchmarkingParticipantDocs) Then
                For Each doc As BenchmarkingParticipantDoc In _benchmarkingParticipantDocs
                    Dim dateToUse As Date = Date.Today
                    If Not IsNothing(doc.DocDate) Then
                        dateToUse = CDate(doc.DocDate)
                    End If
                    _mfilesAccess.UploadNewDocToMFiles(doc.FilePath, doc.DeliverableType, doc.BenchmarkingParticipant, dateToUse)














                    'Refresh local mfiles cache of docs in calling form?

















                Next
            End If
            Return String.Empty
        Catch ex As Exception
            Return "Error in UploadBenchmarkingParticipantDocs(): " & ex.Message
        End Try
    End Function
#End Region

#Region "General"
    Public Sub AddRefiningGeneralDocForUpload(generalRefinery As String, filePath As String, refNum As String,
            Optional fileNameWithoutExtension As String = Nothing, Optional docDate As DateTime? = Nothing)
        If Not IsNothing(_mfilesAccess) Then
            If IsNothing(_refiningGeneralDocs) Then
                _refiningGeneralDocs = New List(Of RefiningGeneralDoc)
            End If
            Dim doc As New RefiningGeneralDoc

            '!!!!
            'doc.DocClass = "Benchmarking General" '!!??!!

            doc.GeneralRefinery = generalRefinery
            doc.RefNum = refNum
            Dim fileName As String = String.Empty
            If Not IsNothing(fileNameWithoutExtension) AndAlso fileNameWithoutExtension.Length > 0 Then
                fileName = fileNameWithoutExtension
            Else
                fileName = GetFileNameWithoutExtension(ExtractFileName(filePath))
            End If
            Dim ext As String = GetFileExtension(fileName)
            doc.DocNameWithoutExtension = fileName
            doc.FilePath = filePath
            If Not IsNothing(docDate) Then
                doc.DocDate = docDate
            Else
                doc.DocDate = DateTime.Today
            End If
            _refiningGeneralDocs.Add(doc)
        End If
    End Sub
    Public Function UploadRefiningGeneralDocs() As String
        'return errors on error else return blank
        Try
            If Not IsNothing(_mfilesAccess) AndAlso Not IsNothing(_refiningGeneralDocs) Then
                Dim refNumWithoutYear = String.Empty
                For Each doc As RefiningGeneralDoc In _refiningGeneralDocs
                    refNumWithoutYear = String.Empty
                    If _mfilesAccess.Vertical = VerticalType.REFINING Then
                        Dim count As Integer = 0
                        Dim ltr As String = String.Empty
                        Do While Not IsNumeric(ltr)
                            count = count + 1
                            ltr = doc.RefNum.Substring(count)
                        Loop
                        'now add 3 to Count to get Year position
                        refNumWithoutYear = doc.RefNum.Substring(0, count + 3)
                    End If

                    Dim subFolder As Integer = 0 ' default level, no subfolder.
                    Dim subFolders As Dictionary(Of Integer, String) = _mfilesAccess.GetGeneralRefiningSubfolders()
                    For Each item In subFolders
                        If item.Value.ToUpper() = doc.Subfolder.ToUpper() Then
                            subFolder = item.Key
                            Exit For
                        End If
                    Next

                    Dim dateToUse As Date = Date.Today
                    If Not IsNothing(doc.DocDate) Then
                        dateToUse = CDate(doc.DocDate)
                    End If
                    _mfilesAccess.UploadNewGeneralRefineryItemToMFiles(doc.FilePath, refNumWithoutYear, subFolder, dateToUse)





                    'refresh local mfiles cache of docs in calling form?





                Next
            End If
            Return String.Empty
        Catch ex As Exception
            Return "Error in UploadRefiningGeneralDocs(): " & ex.Message
        End Try
    End Function
#End Region


    Private Function ExtractFileName(filePath As String) As String
        Return filePath.Substring(filePath.LastIndexOf("\") + 1, filePath.Length - filePath.LastIndexOf("\") - 1)
    End Function
    Private Function GetFileExtension(fileName As String) As String
        Dim parts As String() = fileName.Split(".")
        Return parts(parts.Length - 1)
    End Function
    Private Function GetFileNameWithoutExtension(fileNameWithoutFolder As String) As String
        Dim nameParts() As String = fileNameWithoutFolder.Split(".")
        Dim result As String = String.Empty
        For count As Integer = 0 To nameParts.Length - 2
            result += nameParts(count) + "."
        Next
        result = result.Remove(result.Length - 1)
        Return result
    End Function


    Private Class BenchmarkingParticipantDoc
        Public DocClass As Integer = 52  'private static int _benchmarkingParticipantClassId = 52;
        Public DeliverableType As String = String.Empty
        Public DocNameWithoutExtension As String = String.Empty
        Public BenchmarkingParticipant As String = String.Empty
        Public DocDate As Date? = Nothing
        Public FilePath As String = String.Empty
    End Class

    Private Class RefiningGeneralDoc
        'Public DocClass As String = String.Empty
        Public GeneralRefinery As String = String.Empty
        Public RefNum As String = String.Empty 'need the part wihout year
        Public Subfolder As String = String.Empty
        Public DocNameWithoutExtension As String  'Archive, Drawings
        Public DocDate As Date? = Nothing
        Public FilePath As String = String.Empty
    End Class
End Class
