﻿Imports System
Imports System.Configuration
Imports System.Text.RegularExpressions
Imports SA.Console.Common

Public Class DocsFileSystemAccess

    Dim _vertical As VerticalType 'Olefins, Refining, etc.
    Dim _errorLogType As String
    Private _profilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + "\"
    Private _profileConsolePath As String = _profilePath + "Console\"
    Private _profileConsoleTempPath As String = _profileConsolePath + "Temp\"
    Private _templatePath As String = String.Empty '"K:\STUDY\Olefins\2015\Correspondence\!Validation\Templates\"
    Private _dropdownChoice As String = String.Empty
    Private _useMFiles As Boolean = False
    Private _mfilesRefnum As String = String.Empty
    Private _mfilesCompanyBenchmarkName = String.Empty
    Private _refreshMFilesCollectionsRefnum As String = String.Empty
    Private _refreshCompanyBenchmarkName As String = String.Empty
    'Private _mfiles As DocsMFilesAccess = Nothing

    Public ReadOnly Property Vertical() As VerticalType
        Get
            Return _vertical
        End Get
        'Set(ByVal value As String)
        '    _vertical = value
        'End Set
    End Property

    Public Property ErrorLogType() As String
        Get
            Return _errorLogType
        End Get
        Set(value As String)
            _errorLogType = value
        End Set
    End Property

    Public Sub New(Vertical As VerticalType, UseMFiles As Boolean)
        _vertical = Vertical
        _useMFiles = UseMFiles
        '_mfiles = Nothing
        Select Case Vertical
            Case VerticalType.OLEFINS
                _templatePath = String.Empty ' System.Configuration.ConfigurationManager.AppSettings("OlefinsTemplatePath").ToString()
            Case VerticalType.REFINING
                Try
                    _templatePath = String.Empty ' System.Configuration.ConfigurationManager.AppSettings("RefiningVFTemplatePath").ToString()
                Catch ex As System.Exception
                    Throw New System.Exception("Unable to find app setting for _templatePath in DocsFileSystemAccess.vb")
                End Try
        End Select
    End Sub

#Region "Correspondence Tab"
    Private Sub PopulateVFFiles(ByVal CorrPath As String, ByVal CorrPathV As String,
                                ByVal SelectedItemString As String, ByRef VFFiles As List(Of Object))
        'called by lstVRFilesItems() below
        Dim strDirResult As String
        strDirResult = Dir(CorrPath + CorrPathV)
        Do While strDirResult <> ""
            If SelectedItemString = Mid(strDirResult, 3, 1) Then
                Dim found As Boolean = False
                For Each item As String In VFFiles
                    If item.Contains(strDirResult) Then
                        found = True
                        Exit For
                    End If
                Next
                If Not found Then
                    VFFiles.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                End If
            End If
            strDirResult = Dir()
        Loop
    End Sub

    Private Function PopulateVFFiles(ByVal CorrPath As String, ByVal CorrPathV As String,
                            ByVal SelectedItemString As String, ByVal consoleFilesInfo As ConsoleFilesInfo) As ConsoleFilesInfo
        Dim strDirResult As String
        strDirResult = Dir(CorrPath + CorrPathV)
        Do While strDirResult <> ""
            If SelectedItemString = Mid(strDirResult, 3, 1) Then
                Dim found As Boolean = False
                For Each item In consoleFilesInfo.Files
                    If item.FileName.ToUpper() = strDirResult.ToUpper() Then
                        found = True
                        Exit For
                    End If
                Next
                If Not found Then
                    Dim cf As New ConsoleFile(False)
                    cf.LastModifiedUtcDate = FileDateTime(CorrPath & strDirResult)
                    cf.FileName = strDirResult
                    consoleFilesInfo.Files.Add(cf)
                End If
            End If
            strDirResult = Dir()
        Loop
        Return consoleFilesInfo
    End Function

    Public Function lstVRFilesItems(ByVal CorrPath As String, ByVal SelectedItemString As String, _
                                    shortRefNum As String) As List(Of Object)
        If Me.Vertical = VerticalType.OLEFINS Or (Me.Vertical = VerticalType.REFINING And _useMFiles) Then
            Dim longRefNum As String = shortRefNum
            If Me.Vertical = VerticalType.OLEFINS Then
                longRefNum = Utilities.GetLongRefnum(shortRefNum)
            End If
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
            Return mfiles.lstVRFilesItems(String.Empty, SelectedItemString)
        Else
            'Use the VFFiles to populate lstVRFiles
            Dim vFFiles As New List(Of Object)
            PopulateVFFiles(CorrPath, "VF*", SelectedItemString, vFFiles)
            PopulateVFFiles(CorrPath, "VR*", SelectedItemString, vFFiles)
            '------------------
            ' Added 1/30/2006 by FRS
            ' Include any VA files in lstVRFiles so the user can see whether
            ' the VF has been ack'ed.
            PopulateVFFiles(CorrPath, "VA*", SelectedItemString, vFFiles)
            PopulateVFFiles(CorrPath, "*Return*", SelectedItemString, vFFiles)
            Return vFFiles
        End If
    End Function

    Public Function GetVFilesForCorrespondenceTab(ByVal CorrPath As String, ByVal SelectedItemString As String, _
                                    shortRefNum As String) As ConsoleFilesInfo
        If Me.Vertical = VerticalType.REFINING Then
            If _useMFiles Then
                Dim longRefNum As String = shortRefNum
                If Me.Vertical = VerticalType.OLEFINS Then
                    longRefNum = Utilities.GetLongRefnum(shortRefNum)
                End If
                Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
                Dim consoleFilesInfo As ConsoleFilesInfo = mfiles.GetDocByNameAndRefnum("VF" & SelectedItemString, shortRefNum, True)
                Dim temp As ConsoleFilesInfo = mfiles.GetDocByNameAndRefnum("VR" & SelectedItemString, shortRefNum, True)
                If Not IsNothing(temp) Then
                    For Each fil As ConsoleFile In temp.Files
                        If IsNothing(consoleFilesInfo) Then consoleFilesInfo = New ConsoleFilesInfo(_useMFiles, _vertical)
                        consoleFilesInfo.Files.Add(fil)
                    Next
                End If

                temp = mfiles.GetDocByNameAndRefnum("VA" & SelectedItemString, shortRefNum, True)
                If Not IsNothing(temp) Then
                    For Each fil As ConsoleFile In temp.Files
                        If IsNothing(consoleFilesInfo) Then consoleFilesInfo = New ConsoleFilesInfo(_useMFiles, _vertical)
                        consoleFilesInfo.Files.Add(fil)
                    Next
                End If

                temp = mfiles.GetDocByNameAndRefnum("F_Return" & SelectedItemString, shortRefNum, False, True)
                If Not IsNothing(temp) Then
                    For Each fil As ConsoleFile In temp.Files
                        If IsNothing(consoleFilesInfo) Then consoleFilesInfo = New ConsoleFilesInfo(_useMFiles, _vertical)
                        consoleFilesInfo.Files.Add(fil)
                    Next
                End If

                temp = mfiles.GetDocByNameAndRefnum("L_Return" & SelectedItemString, shortRefNum, False, True)
                If Not IsNothing(temp) Then
                    For Each fil As ConsoleFile In temp.Files
                        If IsNothing(consoleFilesInfo) Then consoleFilesInfo = New ConsoleFilesInfo(_useMFiles, _vertical)
                        consoleFilesInfo.Files.Add(fil)
                    Next
                End If
                Return consoleFilesInfo
                'Return mfiles.lstVRFilesItems(String.Empty, SelectedItemString)
            Else 'not use MFiles
                Dim cfi As New ConsoleFilesInfo(False, _vertical)
                cfi = PopulateVFFiles(CorrPath, "VF*", SelectedItemString, cfi)
                cfi = PopulateVFFiles(CorrPath, "VR*", SelectedItemString, cfi)
                cfi = PopulateVFFiles(CorrPath, "VA*", SelectedItemString, cfi)
                cfi = PopulateVFFiles(CorrPath, "*Return*", SelectedItemString, cfi)
                Return cfi
            End If
        End If
    End Function

    Public Function lstVFilesItems(ByVal path As String, ByVal VFileTypeAndVNumber As String, _
                               Optional shortRefNum As String = "") As List(Of Object)
        Dim vFFiles As New List(Of Object)
        Dim strDirResult As String
        If Not path.EndsWith("\") Then path = path + "\"
        strDirResult = Dir(path + VFileTypeAndVNumber + "*")
        Do While strDirResult <> ""
            vFFiles.Add(File.GetLastWriteTime(path + strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult)
            'vFFiles.Add(Format(path & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult)
            strDirResult = Dir()
        Loop
        Return vFFiles
    End Function

    Public Sub btnReceiptAck_Click(lstVFNumbersText As String, mUserName As String, shortRefNum As String)
        Dim strDirResult As String
        Dim strDateTimeToPrint As String
        If IsNothing(lstVFNumbersText) Then lstVFNumbersText = String.Empty

        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, Utilities.GetLongRefnum(shortRefNum))
        If Me.Vertical = VerticalType.OLEFINS Then
            'reoving the '".txt" part because not part of name in mfiles
            If mfiles.DocExists("VA" + lstVFNumbersText, shortRefNum, True) Then
                MsgBox("Receipt of VF" & lstVFNumbersText & " has already been noted.")
                Exit Sub
            End If
        Else
            strDirResult = Dir(CorrPath & "VA" & lstVFNumbersText & ".txt")
            If strDirResult <> "" Then
                MsgBox("Receipt of VA" & lstVFNumbersText & " has already been noted.")
                Exit Sub
            End If
        End If

        strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        If strDateTimeToPrint <> "" Then
            If Me.Vertical = VerticalType.OLEFINS Then
                Dim path As String = _profileConsolePath & "VA" & lstVFNumbersText & ".txt"
                Dim objWriter As New System.IO.StreamWriter(path, True)

                objWriter.WriteLine(mUserName)
                objWriter.WriteLine(strDateTimeToPrint)
                objWriter.Close()

                mfiles.UploadNewDocToMFiles(path, 4, -1)
            Else
                Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & lstVFNumbersText & ".txt", True)

                objWriter.WriteLine(mUserName)
                objWriter.WriteLine(strDateTimeToPrint)
                objWriter.Close()
            End If
            MsgBox("VA" & lstVFNumbersText & ".txt has been built.")
        Else
            MsgBox("Receipt Acknowledgement was canceled.")
        End If
    End Sub

    Public Function ValFax_Build(ByRef strValFaxTemplate As String,
        ByRef UserName As String,
        TemplatePath As String,
        ByRef btnValFaxText As String,
        TempDrive As String,
        ByRef mUserName As String,
        RefNum As String,
        ByRef db As SA.Internal.Console.DataObject.DataObject,
        StudyDrive As String,
        StudyYear As String,
        StudyRegion As String,
        Study As String,
        TempPath As String,
        ByRef CurrentRefNum As String,
        CorrPathParameter As String,
        ByRef TemplateValues As Object,
        ByRef VFFileName As String,
        shortRefNum As String,
        longRefNum As String) As Boolean
        'ByRef TemplateValues As OlefinsMainConsole.WordTemplate,




        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.



        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""

        Try
            'refining will call its own local ValFax_Build for legacy studies
            If (Me.Vertical = VerticalType.OLEFINS And _useMFiles) Or (Me.Vertical = VerticalType.REFINING And _useMFiles) Then
                Select Case Me.Vertical
                    Case VerticalType.OLEFINS
                        TemplateValues = TryCast(TemplateValues, SA.Console.Common.WordTemplate)
                    Case VerticalType.REFINING
                        TemplateValues = TryCast(TemplateValues, SA.Console.Common.WordTemplate)
                End Select
                Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
                'see if any VFs. Then popup the deadline box
                Dim vfExists As Boolean = mfiles.DocExists("V", Utilities.GetLongRefnum(shortRefNum), True)
                If Not vfExists Then
                    intNumDays = 10
                Else
                    intNumDays = 5
                End If
                Dim DeadlineDays As String = InputBox("How many business days do you want to give them to respond?", "", intNumDays)
                If _templatePath.Length > 0 Then 'NGPP, not Olefins or Refining
                    TemplatePath = _templatePath
                End If
                Return mfiles.ValFax_Build(strValFaxTemplate, UserName, TemplatePath, btnValFaxText, mUserName,
                                    RefNum, db, StudyDrive, StudyYear, StudyRegion, Study, TempPath, CurrentRefNum,
                                    CorrPathParameter, DeadlineDays, TemplateValues, VFFileName)
            ElseIf Me.Vertical = VerticalType.NGPP Then
                TemplateValues = TryCast(TemplateValues, SA.Console.Common.WordTemplate)
                ValFax_Build_NGPP(strValFaxTemplate, UserName, TemplatePath, btnValFaxText, TempDrive, mUserName, RefNum, db, StudyDrive,
                                  StudyYear, StudyRegion, Study, TempPath, CurrentRefNum, CorrPathParameter, TemplateValues, VFFileName,
                                  shortRefNum, longRefNum)
                Return True
            ElseIf Me.Vertical = VerticalType.LNG Then
                TemplateValues = TryCast(TemplateValues, SA.Console.Common.WordTemplate)
                ValFax_Build_LNG(strValFaxTemplate, UserName, TemplatePath, btnValFaxText, TempDrive, mUserName, RefNum, db, StudyDrive,
                                  StudyYear, StudyRegion, Study, TempPath, CurrentRefNum, CorrPathParameter, TemplateValues, VFFileName,
                                  shortRefNum, longRefNum)
                Return True
            Else 'not mfiles
                If Dir(CorrPath & "vf*") = "" Then
                    intNumDays = 10
                Else
                    intNumDays = 5
                End If
                Dim DeadlineDays As String = InputBox("How many business days do you want to give them to respond?", "", intNumDays)
                If File.Exists(_profileConsoleTempPath + "ValFax.txt") Then File.Delete(_profileConsoleTempPath + "\ValFax.txt")
                If strValFaxTemplate = "" Then
                    ' Find out which template they want to use.
                    strProgress = "cboTemplates.Clear"
                    frmTemplates.cboTemplates.Items.Clear()

                    strDirResult = Dir(TemplatePath & "*.doc*")
                    If strDirResult = "" Then
                        MsgBox("No templates found for this study. Please contact Jon Bowen (JAB).", vbOKOnly)
                        Return False
                    End If
                    strProgress = "Loading cboTemplates"
                    Do While strDirResult <> ""

                        frmTemplates.cboTemplates.Items.Add(strDirResult)
                        'Debug.Print strDirResult, frmTemplates.cboTemplates.ListCount

                        strDirResult = Dir()
                    Loop

                    If frmTemplates.cboTemplates.Items.Count < 1 Then
                        MsgBox("No templates found for this study. Please contact Jon Bowen (JAB).", vbOKOnly)
                        Return False
                    End If

                    ' Default to first one.
                    frmTemplates.cboTemplates.SelectedIndex = 0

                    ' If there is only one, don't make them choose,
                    ' just go.
                    If frmTemplates.cboTemplates.Items.Count > 1 Then
                        'MsgBox "Show form modal"
                        ' We use this form for other purposes as well (like selecting which SpecFrac file to open).
                        ' So, reload the caption and label items to the origian "ValFax" values.
                        frmTemplates.Text = "Select a Cover Template"

                        ' Ready to show them the form.
                        frmTemplates.ShowDialog()

                        If frmTemplates.OKClick = False Then
                            Return False
                        End If

                        ' Load strValFaxTemplate from what they selected on
                        ' frmTemplates. (strTemplateFile is a global variable.)
                        strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem
                        'MsgBox "After show form modal"
                    Else
                        strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem
                    End If

                End If

                ' At this point we have the name of the template to use.
                'MsgBox "Prepare new doc"
                btnValFaxText = "Preparing New Document"
                strProgress = "Preparing New Document"
                ' Create the file to pass the data in.
                'Open gstrTempFolder & "ValFax.txt" For Output As #1
                'If Dir(TempDrive & _UserName & "\Console\", vbDirectory) = "" Then
                '    MkDir(TempDrive & _UserName & "\Console\")
                'End If

                Dim objWriterPath As String = String.Empty
                If _vertical = VerticalType.OLEFINS Then
                    objWriterPath = _profileConsoleTempPath
                Else
                    objWriterPath = TempDrive & mUserName & "\Console\"
                End If


                'If Not Directory.Exists(TempDrive & mUserName & "\Console\") Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\")
                If Not Directory.Exists(objWriterPath) Then Directory.CreateDirectory(objWriterPath)
                'Dim objWriter As New System.IO.StreamWriter(TempDrive & mUserName & "\Console\ValFax.txt", True)
                Dim objWriter As New System.IO.StreamWriter(objWriterPath + "ValFax.txt", True)

                ' Todays date
                strProgress = "Printing Long Date"
                objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
                TemplateValues.Field.Add("_TodaysDate")
                TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))
                TemplateValues.Field.Add("_VFNumber")
                TemplateValues.RField.Add(NextV("VF", shortRefNum, longRefNum).ToString())
                strProgress = "Getting business days to respond."
                ' Due date
                ' Need a way to make sure we get valid input here.

                'SB all this moved to calling app:
                'If Dir(CorrPathParameter & "vf*") = "" Then
                '    intNumDays = 10
                'Else
                '    intNumDays = 5
                'End If
                'strDeadline = InputBox("How many business days do you want to give them to respond?", "", intNumDays)

                If DeadlineDays = "" Then
                    objWriter.Close()
                    Return False
                End If

                datDeadline = Utilities.ValFaxDateDue(DeadlineDays)
                objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
                TemplateValues.Field.Add("_DueDate")
                TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
                strProgress = "Getting Company name"
                ' Company Name
                If _vertical <> VerticalType.POWER Then
                    params = New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)
                Else
                    params = New List(Of String)
                    params.Add("@SiteId/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetTSortDataBySiteId", params)
                End If

                If ds.Tables.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    objWriter.WriteLine(row("Company"))
                    TemplateValues.Field.Add("_Company")
                    TemplateValues.RField.Add(row("Company").ToString)

                    strProgress = "Getting Location"
                    ' Refinery Name
                    objWriter.WriteLine(row("Location"))
                    TemplateValues.Field.Add("_Plant")
                    TemplateValues.RField.Add(row("Location").ToString)
                    Coloc = row("Coloc").ToString

                End If

                strProgress = "Getting Contact info"


                ' Contact Name
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
                If ds.Tables.Count = 0 Then

                    If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                        btnValFaxText = "New Validation Fax"
                        Return False
                    Else
                        strFaxName = "NAME UNAVAILABLE"
                        strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                    End If
                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        strFaxName = row("NameFull").ToString
                        strFaxEmail = row("Email").ToString
                    End If
                End If

                objWriter.WriteLine(strFaxName)

                TemplateValues.Field.Add("_Contact")
                TemplateValues.RField.Add(strFaxName)
                ' Contact Email Address
                objWriter.WriteLine(strFaxEmail)
                TemplateValues.Field.Add("_EMail")
                TemplateValues.RField.Add(strFaxEmail)
                strProgress = "Getting consultant info"
                ' Consultant Name
                params = New List(Of String)
                If _vertical = VerticalType.OLEFINS Then
                    params.Add("Initials/" + UserName)
                Else
                    params.Add("Initials/" + mUserName)
                End If
                ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)

                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)

                        If row("EmployeeName") Is Nothing Or IsDBNull(row("EmployeeName")) Then
                            strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                        Else
                            strName = row("EmployeeName").ToString
                        End If
                    Else

                        strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                    End If
                End If
                If strName = "" Then
                    objWriter.Close()
                    Return False
                End If
                objWriter.WriteLine(strName)
                TemplateValues.Field.Add("_ConsultantName")
                TemplateValues.RField.Add(strName)
                ' Consultant Initials
                objWriter.WriteLine(mUserName)
                TemplateValues.Field.Add("_Initials")

                TemplateValues.RField.Add(mUserName)
                strProgress = "Path to store it in"
                ' Path to store it in
                ' If it is Trans Pricing or FLCOMP, put it, without VF#, into Refinery Corr folder.
                If strValFaxTemplate.Substring(0, 17) = "Trans Pricing.doc" Or strValFaxTemplate.Substring(strValFaxTemplate.Length - 10, 10) = "FLCOMP.doc" Then
                    Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\" & Coloc & "\"
                    ' If it has "Company Wide" in the name, store it in the Company Corr folder
                ElseIf InStr(1, strValFaxTemplate, "Company Wide") > 0 Then
                    Filename = StudyDrive & StudyYear & "\Company Correspondence\" & Coloc & "\"
                    ' Otherwise -- MOST CASES HERE -- store it in the Refinery Corr folder with VF# on the front.
                Else
                    ' Request from DEJ 3/2011 -- if it a Lubes refinery, put "Lube" at end of doc name.
                    Filename = StudyDrive & StudyYear & "\Correspondence\" & RefNum & "\VF" & NextV("VF", shortRefNum, longRefNum).ToString() & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                End If

                objWriter.WriteLine(Filename)
                VFFileName = Utilities.ExtractFileName(Filename)
                ' --------- Add new items here 4/20/2007 ---------

                strProgress = "Getting Company Contact info"

                ' Company Contact Name
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                params.Add("ContactType/COORD")
                params.Add("StudyYear/" & StudyYear)
                ds = db.GetCompanyContacts(params)

                If ds.Tables.Count = 0 Then

                    If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then


                        btnValFaxText = "New Validation Fax"
                        Return False
                    Else
                        strFaxName = "NAME UNAVAILABLE"
                        strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                    End If
                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                        strFaxEmail = row("Email").ToString
                    End If
                End If

                objWriter.WriteLine(strFaxName)
                TemplateValues.Field.Add("_CoCoContact")
                TemplateValues.RField.Add(strFaxName)

                ' Company Contact Email Address
                objWriter.WriteLine(strFaxEmail)
                TemplateValues.Field.Add("_CoCoEMail")
                TemplateValues.RField.Add(strFaxEmail)
                objWriter.Close()
                ' --------- End of new items 4/20/2007 ---------



                ' Make sure the template file is available to copy.
                'On Error GoTo File1NotAvailable
                'intAttr = GetAttr(strTemplatePath & strValFaxTemplate)
                ' Next 2 lines commented 12/1/04 to try new approach
                ' due to problem with compression of network files.
                'SetAttr strTemplatePath & strValFaxTemplate, intAttr
                'On Error GoTo 0

                strProgress = "Copy doc to hard drive"
                ' Bring the doc down to the hard drive.

                If File.Exists(_profileConsoleTempPath & strValFaxTemplate) Then
                    Try
                        Kill(_profileConsoleTempPath & strValFaxTemplate)
                    Catch ex As Exception
                        MsgBox("Console was unable to delete '" & _profileConsoleTempPath & strValFaxTemplate & "'. Please try to delete it yourself and run again.")
                        Return False
                    End Try
                End If

                If File.Exists(TemplatePath & strValFaxTemplate) Then
                    TemplatePath = TemplatePath
                End If

                ' Copy the Template to my work area.
                File.Copy(TemplatePath & strValFaxTemplate, _profileConsoleTempPath & strValFaxTemplate, True)
                'varShellReturn = Shell("XCopy """ & strTemplatePath & strValFaxTemplate & """ """ & gstrTempFolder & """ /Y")
                'Stop
                timStart = Now()
                'MsgBox Now()
                Do While File.Exists(_profileConsoleTempPath & strValFaxTemplate) = False
                    If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                        MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                        'Exit Do
                    End If
                Loop
            End If  'end of not mfiles or NGPP
            Return True
        Catch ex As System.Exception
            ErrorLogType = "Validation: " & CurrentRefNum.ToString()
            Throw ex
            '!!need to do this in calling method:  MessageBox.Show(ex.Message)
        Finally
        End Try
    End Function


    Private Sub ReturnChoice(sender As System.Object)
        _dropdownChoice = sender.ToString()
    End Sub

    Public Sub ValFax_Build_NGPP(ByRef strValFaxTemplate As String,
        ByRef UserName As String,
        TemplatePath As String,
        ByRef btnValFaxText As String,
        TempDrive As String,
        ByRef mUserName As String,
        RefNum As String,
        ByRef db As SA.Internal.Console.DataObject.DataObject,
        StudyDrive As String,
        StudyYear As String,
        StudyRegion As String,
        Study As String,
        TempPath As String,
        ByRef CurrentRefNum As String,
        CorrPathParameter As String,
        ByVal TemplateValues As SA.Console.Common.WordTemplate,
        ByRef VFFileName As String,
        shortRefNumForMfiles As String,
        longRefNumForMfiles As String) 'Implements DocsAccessInterfaces.ValFax_Build
        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.
        Dim strDeadline As String
        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""

        Try
            'Windows couldn't find "K:" so changed it to \\Dallas2\data\data
            Dim networkTemplatePath As String = "\\dallas2\Data\Data\STUDY\NGPP\" & StudyYear & "\Software\IDR\Template\" 'String.Empty


            Dim choice As DropdownForm
            Try
                'networkTemplatePath = ConfigurationManager.AppSettings("NgppVFTemplatePath").ToString()
                Dim choices As New List(Of String)
                Dim folder As New DirectoryInfo(networkTemplatePath)
                Dim files As FileInfo() = folder.GetFiles("*.docx")
                For count As Integer = 0 To files.Length - 1
                    choices.Add(files(count).Name)
                Next
                choice = New DropdownForm("Choose your VF template", choices)
                choice.choice = New DropdownForm.ReturnChoice(AddressOf ReturnChoice)
                _dropdownChoice = String.Empty
                choice.ShowDialog()
                If _dropdownChoice.Trim().Length < 1 Then
                    MsgBox("No template chosen. Quitting VF.")
                    Return
                End If
                strValFaxTemplate = _dropdownChoice 'networkTemplatePath + _dropdownChoice 'ConfigurationManager.AppSettings("NgppVFTemplateFile").ToString()
                If Not File.Exists(networkTemplatePath + strValFaxTemplate) Then
                    MsgBox("Can't find VF Template at " + networkTemplatePath + strValFaxTemplate)
                    Return
                End If
            Catch idrException As Exception
                MsgBox("VF Template Error: " + idrException.Message)
                Exit Sub
            Finally
                Try
                    choice.Close()
                Catch
                End Try
                choice = Nothing
            End Try

            strProgress = "Preparing New Document"
            ' Create the file to pass the data in.
            Dim objWriter As New System.IO.StreamWriter(_profileConsoleTempPath & "ValFax.txt", True)
            ' Todays date
            strProgress = "Printing Long Date"
            objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
            TemplateValues = New SA.Console.Common.WordTemplate
            TemplateValues.Field.Add("_TodaysDate")
            TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))
            strProgress = "Getting business days to respond."
            ' Due date
            ' Need a way to make sure we get valid input here.
            If Dir(CorrPath & "vf*") = "" Then
                intNumDays = 10
            Else
                intNumDays = 5
            End If
            strDeadline = InputBox("How many business days do you want to give them to respond?", "Deadline", intNumDays)
            datDeadline = Utilities.ValFaxDateDue(strDeadline)
            objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
            TemplateValues.Field.Add("_DueDate")
            TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
            strProgress = "Getting Company name"
            ' Company Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)
            If ds.Tables.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                objWriter.WriteLine(row("CompanyName"))
                TemplateValues.Field.Add("_Company")
                TemplateValues.RField.Add(row("CompanyName").ToString)
                strProgress = "Getting Location"
                ' Refinery Name
                objWriter.WriteLine(row("FacilityName"))
                TemplateValues.Field.Add("_Plant")
                TemplateValues.RField.Add(row("FacilityName").ToString)
                Coloc = row("Coloc").ToString
            End If
            strProgress = "Getting Contact info"

            'params = New List(Of String)
            'params.Add("RefNum/" + RefNum)
            'ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)

            Dim sql As String = "SELECT CoordName,CoordEmail,CoordPhone,Coord2Name,Coord2Email,Coord2Phone " &
                " from raw.ContactInfo I inner join dim.Companies C on I.CompanyName = C.CompanyName " &
                " inner join dbo.TSort T on C.CompanyId = T.CompanyId " &
                " where t.DataSetID = I.DataSetId and T.RefNum = '" & RefNum & "'"
            db.SQL = sql
            ds = db.Execute()
            db.SQL = String.Empty
            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim lfn As String = ""
                    For Each row In ds.Tables(0).Rows
                        If lfn <> row("CoordName").ToString Then
                            strFaxName += row("CoordName").ToString + IIf(ds.Tables(0).Rows.Count > 1, ", ", "")
                            lfn = row("CoordName").ToString
                            strFaxEmail += row("CoordEmail").ToString + IIf(ds.Tables(0).Rows.Count > 1, "; ", "")
                        Else
                            strFaxName = strFaxName.Replace(",", "")
                            strFaxEmail = strFaxEmail.Replace(";", "")
                        End If
                    Next
                End If
            End If
            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_Contact")
            TemplateValues.RField.Add(strFaxName)
            ' Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_EMail")
            TemplateValues.RField.Add(strFaxEmail)
            strFaxEmail = ""
            strFaxName = ""
            strProgress = "Getting consultant info"
            ' Consultant Name
            params = New List(Of String)
            params.Add("Initials/" + mUserName)
            params.Add("StudyYear/" + StudyYear)
            params.Add("StudyType/" + Study)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    If row("ConsultantName") Is Nothing Or IsDBNull(row("ConsultantName")) Then
                        strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                    Else
                        strName = row("ConsultantName").ToString
                    End If
                Else
                    strName = InputBox("I cannot find you in the employee table, please give me your full name: ", "Employee data base")
                End If
            End If
            objWriter.WriteLine(strName)
            TemplateValues.Field.Add("_ConsultantName")
            TemplateValues.RField.Add(strName)
            ' Consultant Initials
            objWriter.WriteLine(mUserName)
            TemplateValues.Field.Add("_Initials")
            TemplateValues.RField.Add(mUserName)
            strProgress = "Path to store it in"
            'Filename = StudyDrive & StudyYear & "\Correspondence\" & RefNum.Trim() & "\VF" & NextVF() & " " & Coloc & ".docx"

            Filename = CorrPath & "VF" & NextV("VF", CurrentRefNum, CurrentRefNum) & " " & Coloc & ".docx"

            objWriter.WriteLine(Filename)
            strProgress = "Getting Company Contact info"
            ' Company Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("ContactType/Coord")
            'NGPP doesnt use this param:  params.Add("StudyYear/" + StudyYear)
            ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)
            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row In ds.Tables(0).Rows
                        strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                        strFaxEmail = row("Email").ToString
                    Next
                End If
            End If
            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_CoCoContact")
            TemplateValues.RField.Add(strFaxName)
            ' Company Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_CoCoEMail")
            TemplateValues.RField.Add(strFaxEmail)
            objWriter.Close()
            strProgress = "Copy doc to hard drive"
            ' Bring the doc down to the hard drive.


            'MsgBox("Expeting TempPath to be _profile")


            If File.Exists(TempPath & strValFaxTemplate) Then
                Kill(TempPath & strValFaxTemplate)
            End If
            ' Copy the Template to my work area.
            'File.Copy(localTemplatePath & strValFaxTemplate, _profileConsoleTempPath & strValFaxTemplate, True)
            File.Copy(networkTemplatePath & strValFaxTemplate, TempPath & strValFaxTemplate, True)
            timStart = Now()
            Do While File.Exists(TempPath & strValFaxTemplate) = False
                If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                    MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                End If
            Loop
            Utilities.WordTemplateReplace(TempPath & strValFaxTemplate, TemplateValues, Filename, 1)
            File.Delete(_profileConsoleTempPath & "ValFax.txt")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub ValFax_Build_LNG(ByRef strValFaxTemplate As String,
        ByRef UserName As String,
        TemplatePath As String,
        ByRef btnValFaxText As String,
        TempDrive As String,
        ByRef mUserName As String,
        RefNum As String,
        ByRef db As SA.Internal.Console.DataObject.DataObject,
        StudyDrive As String,
        StudyYear As String,
        StudyRegion As String,
        Study As String,
        TempPath As String,
        ByRef CurrentRefNum As String,
        CorrPathParameter As String,
        ByVal TemplateValues As SA.Console.Common.WordTemplate,
        ByRef VFFileName As String,
        shortRefNumForMfiles As String,
        longRefNumForMfiles As String) 'Implements DocsAccessInterfaces.ValFax_Build
        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.
        Dim strDeadline As String
        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""

        Try
            'Windows couldn't find "K:" so I changed it to \\Dallas2\data\data
            Dim networkTemplatePath As String = "\\dallas2\Data\Data\STUDY\LNG\" & StudyYear & "\Software\IDR\Template\" 'String.Empty


            Dim choice As DropdownForm
            Try
                'networkTemplatePath = ConfigurationManager.AppSettings("NgppVFTemplatePath").ToString()
                Dim choices As New List(Of String)
                Dim folder As New DirectoryInfo(networkTemplatePath)
                Dim files As FileInfo() = folder.GetFiles("*.docx")
                For count As Integer = 0 To files.Length - 1
                    choices.Add(files(count).Name)
                Next
                choice = New DropdownForm("Choose your VF template", choices)
                choice.choice = New DropdownForm.ReturnChoice(AddressOf ReturnChoice)
                _dropdownChoice = String.Empty
                choice.ShowDialog()
                If _dropdownChoice.Trim().Length < 1 Then
                    MsgBox("No template chosen. Quitting VF.")
                    Return
                End If
                strValFaxTemplate = _dropdownChoice 'networkTemplatePath + _dropdownChoice 'ConfigurationManager.AppSettings("NgppVFTemplateFile").ToString()
                If Not File.Exists(networkTemplatePath + strValFaxTemplate) Then
                    MsgBox("Can't find VF Template at " + networkTemplatePath + strValFaxTemplate)
                    Return
                End If
            Catch idrException As Exception
                MsgBox("VF Template Error: " + idrException.Message)
                Exit Sub
            Finally
                Try
                    choice.Close()
                Catch
                End Try
                choice = Nothing
            End Try

            strProgress = "Preparing New Document"
            ' Create the file to pass the data in.
            Dim objWriter As New System.IO.StreamWriter(_profileConsoleTempPath & "ValFax.txt", True)
            ' Todays date
            strProgress = "Printing Long Date"
            objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
            TemplateValues = New SA.Console.Common.WordTemplate
            TemplateValues.Field.Add("_TodaysDate")
            TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))
            strProgress = "Getting business days to respond."
            ' Due date
            ' Need a way to make sure we get valid input here.
            If Dir(CorrPath & "vf*") = "" Then
                intNumDays = 10
            Else
                intNumDays = 5
            End If
            strDeadline = InputBox("How many business days do you want to give them to respond?", "Deadline", intNumDays)
            datDeadline = Utilities.ValFaxDateDue(strDeadline)
            objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
            TemplateValues.Field.Add("_DueDate")
            TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
            strProgress = "Getting Company name"
            ' Company Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)
            If ds.Tables.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                objWriter.WriteLine(row("CompanyName"))
                TemplateValues.Field.Add("_Company")
                TemplateValues.RField.Add(row("CompanyName").ToString)
                strProgress = "Getting Location"
                ' Refinery Name
                objWriter.WriteLine(row("FacilityName"))
                TemplateValues.Field.Add("_Plant")
                TemplateValues.RField.Add(row("FacilityName").ToString)
                Coloc = row("Coloc").ToString
            End If
            strProgress = "Getting Contact info"

            'params = New List(Of String)
            'params.Add("RefNum/" + RefNum)
            'ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)

            Dim sql As String = "SELECT CoordName,CoordEmail,CoordPhone,Coord2Name,Coord2Email,Coord2Phone " &
                " from raw.ContactInfo I inner join dim.Companies C on I.CompanyName = C.CompanyName " &
                " inner join dbo.TSort T on C.CompanyId = T.CompanyId " &
                " where t.DataSetID = I.DataSetId and T.RefNum = '" & RefNum & "'"
            db.SQL = sql
            ds = db.Execute()
            db.SQL = String.Empty
            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim lfn As String = ""
                    For Each row In ds.Tables(0).Rows
                        If lfn <> row("CoordName").ToString Then
                            strFaxName += row("CoordName").ToString + IIf(ds.Tables(0).Rows.Count > 1, ", ", "")
                            lfn = row("CoordName").ToString
                            strFaxEmail += row("CoordEmail").ToString + IIf(ds.Tables(0).Rows.Count > 1, "; ", "")
                        Else
                            strFaxName = strFaxName.Replace(",", "")
                            strFaxEmail = strFaxEmail.Replace(";", "")
                        End If
                    Next
                End If
            End If
            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_Contact")
            TemplateValues.RField.Add(strFaxName)
            ' Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_EMail")
            TemplateValues.RField.Add(strFaxEmail)
            strFaxEmail = ""
            strFaxName = ""
            strProgress = "Getting consultant info"
            ' Consultant Name
            params = New List(Of String)
            params.Add("Initials/" + mUserName)
            params.Add("StudyYear/" + StudyYear)
            params.Add("StudyType/" + Study)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    If row("ConsultantName") Is Nothing Or IsDBNull(row("ConsultantName")) Then
                        strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                    Else
                        strName = row("ConsultantName").ToString
                    End If
                Else
                    strName = InputBox("I cannot find you in the employee table, please give me your full name: ", "Employee data base")
                End If
            End If
            objWriter.WriteLine(strName)
            TemplateValues.Field.Add("_ConsultantName")
            TemplateValues.RField.Add(strName)
            ' Consultant Initials
            objWriter.WriteLine(mUserName)
            TemplateValues.Field.Add("_Initials")
            TemplateValues.RField.Add(mUserName)
            strProgress = "Path to store it in"
            'Filename = StudyDrive & StudyYear & "\Correspondence\" & RefNum.Trim() & "\VF" & NextVF() & " " & Coloc & ".docx"

            Filename = CorrPath & "VF" & NextV("VF", CurrentRefNum, CurrentRefNum) & " " & Coloc & ".docx"

            objWriter.WriteLine(Filename)
            strProgress = "Getting Company Contact info"
            ' Company Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("ContactType/Coord")
            'NGPP doesnt use this param:  params.Add("StudyYear/" + StudyYear)
            ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)
            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row In ds.Tables(0).Rows
                        strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                        strFaxEmail = row("Email").ToString
                    Next
                End If
            End If
            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_CoCoContact")
            TemplateValues.RField.Add(strFaxName)
            ' Company Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_CoCoEMail")
            TemplateValues.RField.Add(strFaxEmail)
            objWriter.Close()
            strProgress = "Copy doc to hard drive"
            ' Bring the doc down to the hard drive.


            'MsgBox("Expeting TempPath to be _profile")


            If File.Exists(TempPath & strValFaxTemplate) Then
                Kill(TempPath & strValFaxTemplate)
            End If
            ' Copy the Template to my work area.
            'File.Copy(localTemplatePath & strValFaxTemplate, _profileConsoleTempPath & strValFaxTemplate, True)
            File.Copy(networkTemplatePath & strValFaxTemplate, TempPath & strValFaxTemplate, True)
            timStart = Now()
            Do While File.Exists(TempPath & strValFaxTemplate) = False
                If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                    MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                End If
            Loop
            Utilities.WordTemplateReplace(TempPath & strValFaxTemplate, TemplateValues, Filename, 1)
            File.Delete(_profileConsoleTempPath & "ValFax.txt")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function BuildVRFile(mUserName As String, strTextForFile As String, CorrPathVR As String,
                                 lstVFNumbersText As String, shortRefNum As String) As String

        If Me.Vertical = VerticalType.OLEFINS Then
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, Utilities.GetLongRefnum(shortRefNum))
            Return mfiles.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText)
        Else
            Dim strDirResult As String
            Dim fullPath As String = CorrPathVR + "VR" + lstVFNumbersText + ".txt"
            strDirResult = Dir(fullPath)
            If strDirResult <> "" Then
                Return "There already is a VR" & lstVFNumbersText & " file."
            End If
            If strTextForFile <> "" Then

                'Code as I found it creates "VA" yet method was for "VR". Go figure.
                Dim objWriter As New System.IO.StreamWriter(CorrPathVR & "VA" & lstVFNumbersText & ".txt", True)

                objWriter.WriteLine(mUserName)
                objWriter.WriteLine(strTextForFile)
                objWriter.Close()
                Return "VR" & lstVFNumbersText & ".txt has been built."
            Else
                Return "VR file build was cancelled."
            End If
        End If
    End Function

    Public Function UploadDocToMfiles(fullPath As String, docClass As Integer, shortRefNum As String,
                                      deliverableType As Integer, Optional benchMarkingParticipant As Integer = -1,
                                       Optional DocumentDate As DateTime? = Nothing) As String
        Try
            Dim longRefnum As String = shortRefNum
            If Vertical = VerticalType.OLEFINS Then
                longRefnum = Utilities.GetLongRefnum(shortRefNum)
            End If

            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefnum)
            If deliverableType = 12 Then
                Return mfiles.UploadNewIdr(fullPath)
            Else
                Return mfiles.UploadNewDocToMFiles(fullPath, deliverableType, benchMarkingParticipant, DocumentDate)
            End If
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function UploadCompanyDocToMfiles(fullPath As String, docClass As Integer, benchMarkingParticipantName As String,
                                  deliverableType As Integer, Optional benchMarkingParticipant As Integer = -1,
                                   Optional DocumentDate As DateTime? = Nothing) As String
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, benchMarkingParticipantName)
            If deliverableType = 12 Then
                Return mfiles.UploadNewIdr(fullPath)
            Else
                Return mfiles.UploadNewDocToMFiles(fullPath, deliverableType, benchMarkingParticipant, DocumentDate)
            End If
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Sub btnBuildVRFile_Click(lstVFNumbersText As String, mUserName As String, shortRefNum As String)
        Dim strTextForFile As String

        If IsNothing(lstVFNumbersText) Then lstVFNumbersText = String.Empty

        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, Utilities.GetLongRefnum(shortRefNum))
        'reoving the '".txt" part because not part of name in mfiles
        If mfiles.DocExists("VR" + lstVFNumbersText, shortRefNum, True) Then
            MsgBox("There already is a VR" & lstVFNumbersText & " file.")
            Exit Sub
        End If
        strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        Dim result As String = BuildVRFile(mUserName, strTextForFile, CorrPath, lstVFNumbersText, shortRefNum)
        MsgBox(result)

    End Sub

    Private Function Get2DigitYearFromLongRefnum(vert As Console.VerticalType, localRefNum As String) As String
        If localRefNum.Trim().Length < 1 Then Return String.Empty
        localRefNum = localRefNum.Trim()
        Dim returnValue As String = String.Empty
        If vert = VerticalType.REFINING Then
            'will be last 2 char, but might have "A" suffix.
            Dim numbersOnly As String = System.Text.RegularExpressions.Regex.Replace(localRefNum, "[^0-9.]", "")
            Return Right(numbersOnly, 2)

            'If Not IsNumeric(localRefNum.Substring(localRefNum.Length - 1, 1)) Then
            '    localRefNum = localRefNum.Substring(0, localRefNum.Length - 1)
            'End If
            'returnValue = localRefNum.Substring(localRefNum.Length - 2, 2)
        ElseIf vert = VerticalType.OLEFINS Then
            'MsgBox("NOT TESTED YET")
            returnValue = localRefNum.Substring(2, 2)
        End If

        Return returnValue
    End Function

    Public Function GetDataFor_lstVFNumbers(filter As String, path As String,
        numsFoundAlready As List(Of String), shortRefNumber As String, longRefNum As String,
        Optional containsNotStartsWith As Boolean = False) As List(Of String)

        '###################can use this in DocsMFilesAcces.vb:  GetDataFor_lstVFNumbers()

        If IsNothing(numsFoundAlready) Then
            numsFoundAlready = New List(Of String)
        End If

        'arg Path will be the Full paht from BuildCorrPath

        If Me.Vertical = VerticalType.OLEFINS AndAlso Int32.Parse(longRefNum.Substring(0, 4)) >= 2015 Then
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            numsFoundAlready = mfiles.GetDataFor_lstVFNumbers(filter, path, numsFoundAlready)
        ElseIf Me.Vertical = VerticalType.REFINING AndAlso _useMFiles Then
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            numsFoundAlready = mfiles.GetDataFor_lstVFNumbers(filter, path, numsFoundAlready, containsNotStartsWith)
        Else 'NGPP, Not MFiles
            Dim dirResult() As String
            dirResult = Directory.GetFiles(path, filter)
            Dim i As Integer
            For i = 0 To dirResult.Length - 1
                ' Get the next V file.
                Dim blnAlready As Boolean = False
                Dim fileinfo As New FileInfo(dirResult(i))
                ' Is it already in the list?
                Dim refNum = getFileSequenceNumber(Replace(fileinfo.Name, fileinfo.Extension, ""), filter)
                If Not (refNum Is Nothing) Then
                    If Not (numsFoundAlready.Contains(refNum)) Then
                        numsFoundAlready.Add(refNum)
                    End If
                End If
                'For Each refNum As String In numsFoundAlready
                '    If refNum = Mid(fileinfo.Name, 3, 1) Then
                '        blnAlready = True
                '        Exit For
                '    End If
                'Next
                ''No, put it there
                'If Not blnAlready Then
                '    numsFoundAlready.Add(Mid(fileinfo.Name, 3, 1)) ' & Chr(9) & FileDateTime(strCorrPath & strDirResult)
                'End If
            Next
        End If
        Return numsFoundAlready
    End Function
    Function getFileSequenceNumber(strFileName As String, fileNamePattern As String)
        Dim i As Integer
        Dim temp As String()
        fileNamePattern = Replace(fileNamePattern.ToUpper, "*", "")
        strFileName = strFileName.ToUpper
        temp = strFileName.Split(" ")
        For i = 0 To temp.Length - 1
            If temp(i).Contains(Replace(fileNamePattern, "*", "").ToUpper) Then
                temp(i) = (Replace(temp(i), fileNamePattern, ""))
                If temp(i).Contains(".") Then
                    If temp(i).IndexOf(".") > 0 Then
                        temp(i) = temp(i).Substring(0, temp(i).IndexOf("."))
                    End If
                End If
                If temp(i).Contains("-") Then
                    If temp(i).IndexOf("-") > 0 Then
                        temp(i) = temp(i).Substring(0, temp(i).IndexOf("-"))
                    End If
                End If
                For Each c As Char In temp(i)
                    If Not Regex.IsMatch(c, "^[0-9 ]+$") Then
                        temp(i) = Replace(temp(i), c, "")
                    End If
                Next
                If temp(i) Is Nothing Then
                    Return Nothing
                Else
                    If Regex.IsMatch(temp(i), "^[0-9 ]+$") Then
                        If temp(i) <> 0 Then
                            Return temp(i).TrimStart("0")
                        Else
                            Return 0
                        End If
                    End If
                End If
            End If
        Next
        Return Nothing
    End Function
    Public Sub lstVFFiles_DoubleClick(fileName As String, pathToFile As String, shortRefNum As String, longRefNum As String)
        If Vertical = VerticalType.OLEFINS Or _useMFiles Then
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
            mfiles.lstVFFiles_DoubleClick(fileName.Trim(), _profileConsolePath)
        Else
            Process.Start(pathToFile + fileName)
        End If
    End Sub

    Public Function DownloadFile(folderPath As String, docId As Integer, shortRefNum As String, longRefNum As String)
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
            Return mfiles.DownloadFile(folderPath, docId)
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Private Function NextFile(findFile As String, findExt As String)
        Try
            Dim strDirResult As String
            Dim I As Integer
            For I = 1 To 20
                strDirResult = Dir(findFile & CStr(I) & "*." & findExt)
                If strDirResult = "" Then
                    Return I
                    Exit For
                End If
            Next I
            Return Nothing
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function NextV(filter As String, shortRefNum As String, longRefNum As String) As Integer
        If _useMFiles Then
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
            Return mfiles.NextV(filter, longRefNum)
        Else
            Dim strDirResult As String
            Dim I As Integer
            For I = 1 To 20
                strDirResult = Dir(CorrPath & filter & CStr(I) & "*.doc")
                If strDirResult = "" Then
                    strDirResult = Dir(CorrPath & filter & CStr(I) & "*.docx")
                    If strDirResult = "" Then
                        Return I
                        Exit For
                    End If
                End If
            Next I
            Return Nothing
        End If
    End Function

    Public Function NextV(filter As String, correspondencePath As String, siteIdOrShortRefNum As String, longRefNum As String) As Integer
        If _useMFiles Then
            Throw New NotImplementedException("NextV for mfiles not implemented")
            'Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
            'Return mfiles.NextV(filter, longRefNum)
        Else
            Dim strDirResult As String
            Dim I As Integer
            For I = 1 To 20
                strDirResult = Dir(correspondencePath & filter & CStr(I) & "*.doc")
                If strDirResult = "" Then
                    strDirResult = Dir(correspondencePath & filter & CStr(I) & "*.docx")
                    If strDirResult = "" Then
                        Return I
                        Exit For
                    End If
                End If
            Next I
            Return Nothing
        End If
    End Function



#End Region

    Public Function GetDocInfoByNameAndRefnum(fileName As String, shortRefNum As String, _
                                              longRefNum As String, startsWith As Boolean) As ConsoleFilesInfo
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, longRefNum)
        Return mfiles.GetDocByNameAndRefnum(fileName, longRefNum, False)
    End Function

    Public Function GetDocInfoByNameAndBenchmarkingParticipantName(fileName As String, _
                                          benchmarkingParticipantName As String, startsWith As Boolean) As ConsoleFilesInfo
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, benchmarkingParticipantName)
        Return mfiles.GetDocByNameAndRefnum(fileName, benchmarkingParticipantName, False)
    End Function

    Public Function CheckOutToMe(docId As Integer, shortRefNumber As String, longRefNum As String) As Boolean
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            mfiles.CheckOutToMe(docId)
            Return True
        Catch ex As System.Exception
            Return False
        End Try
    End Function

    Public Function CheckIn(docId As Integer, shortRefNumber As String, longRefNum As String) As Boolean
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            mfiles.CheckIn(docId)
            Return True
        Catch ex As System.Exception
            Return False
        End Try
    End Function

    Public Function GetCheckedOutStatus(docId As Integer, shortRefNumber As String, longRefNum As String) As MFilesCheckOutStatus
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            Return mfiles.GetCheckedOutStatus(docId)
        Catch ex As System.Exception
            Return Nothing
        End Try
    End Function

    Public Function OpenFileFromMfiles(fileNameWithoutExtension As String, shortRefNumber As String, longRefNum As String) As String
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            Return mfiles.OpenFileFromMfiles(fileNameWithoutExtension)
        Catch ex As System.Exception
            Return "Error in docsFileSystemAccess.OpenFileFromMfiles: " + ex.Message
        End Try
    End Function

    Public Function OpenCompanyFileFromMfiles(fileNameWithoutExtension As String, benchmarkingParticipantName As String) As String
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, benchmarkingParticipantName)
            Return mfiles.OpenFileFromMfiles(fileNameWithoutExtension)
        Catch ex As System.Exception
            Return "Error in docsFileSystemAccess.OpenFileFromMfiles: " + ex.Message
        End Try
    End Function

    Public Function OpenFileFromMfiles(docId As Integer, shortRefNumber As String, longRefNum As String) As String
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            Return mfiles.OpenFileFromMfiles(docId)
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function GetDocNamesAndIdsByBenchmarkingParticipant( _
                shortRefNumber As String, longRefNum As String) As SA.Console.Common.ConsoleFilesInfo
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
        Return mfiles.GetDocNamesAndIdsByBenchmarkingParticipant("") 'the arg for this call isn't used.
    End Function

    Public Function GetDocNamesAndIdsByBenchmarkingParticipant( _
            benchmarkingParticipantName As String) As SA.Console.Common.ConsoleFilesInfo
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, benchmarkingParticipantName)
        Return mfiles.GetDocNamesAndIdsByBenchmarkingParticipant("") 'the arg for this call isn't used.
    End Function

    Public Function GetGeneralRefineryDocs(shortRefNumber As String, longRefNum As String,
            generalRefineryTextId As String, ByRef consoleFiles As SA.Console.Common.ConsoleFilesInfo,
            drawingArchive As Integer, Optional nameContains As String = "") As String
        'For refining, generalRefineryId will be like 102EUR
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
        Return mfiles.GetGeneralRefineryDocs(generalRefineryTextId, consoleFiles, drawingArchive, nameContains)
    End Function

    Public Function UploadNewGeneralRefineryItemToMFiles(shortRefNumber As String, longRefNum As String,
            fullPath As String, refNumWithoutYear As String,
            drawingArchive As Integer, Optional documentDate As DateTime? = Nothing) As Boolean
        '//if drawingArchive = 0, then put at GeneralRefinery level. If 1, put at 
        '//  acrhive subfolder. If 2, put at Drawings subfolder) As Boolean
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
        Return mfiles.UploadNewGeneralRefineryItemToMFiles(fullPath, refNumWithoutYear, drawingArchive, documentDate)
    End Function
    'Public Function GetConsultantTabInfo(shortRefNumber As String, longRefNum As String, refNumIds As List(Of Integer), _
    '        Optional ByVal docNameStartsWithFilter As String = "", Optional ByVal docNameContainsFilter As String = "") As ConsoleFilesInfo
    '    Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
    '    Return mfiles.GetConsultantTabInfo(refNumIds, docNameStartsWithFilter, docNameContainsFilter)
    'End Function

    Public Function DocExistsInMFiles(fileName As String, shortRefNumber As String, _
                              longRefNum As String, startsWith As Boolean)
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
        Return mfiles.DocExists(fileName, "", startsWith) '2nd arg isn't used anymore
    End Function

    Public Function DocExistsInMFiles(fileName As String, benchmarkingParticipantName As String, _
                          startsWith As Boolean)
        Dim mfiles As New DocsMFilesAccess(Me.Vertical, benchmarkingParticipantName)
        Return mfiles.DocExists(fileName, "", startsWith) '2nd arg isn't used anymore
    End Function

    Public Function NextReturnFile(shortRefNumber As String, _
                              longRefNum As String) As Integer
        If Vertical = VerticalType.OLEFINS Then
            If _useMFiles Then
                Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
                Return mfiles.NextReturnFile(longRefNum)
            Else
                Dim strDirResult As String
                Dim shortFilter As String = "_Return"
                Dim I As Integer
                For I = 0 To 20
                    strDirResult = Dir(CorrPath & "*_Return" & CStr(I) & "*.xls")
                    If strDirResult = "" Then
                        strDirResult = Dir(CorrPath & "*" & shortFilter & CStr(I) & "*.xls*")
                        If strDirResult = "" Then
                            Return I
                            Exit For
                        End If
                    End If
                Next I
                Return Nothing
            End If
        ElseIf Vertical = VerticalType.NGPP Then
            Dim strDirResult As String
            Dim I As Integer
            For I = 0 To 20
                strDirResult = Dir(CorrPath & "*G_Return" & CStr(I) & "*.xls")
                If strDirResult = "" Then
                    strDirResult = Dir(CorrPath & "*G_Return" & CStr(I) & "*.xlsx")
                    If strDirResult = "" Then
                        Return I
                        Exit For
                    End If
                End If
            Next I
            Return Nothing
        ElseIf Vertical = VerticalType.REFINING Then
            If _useMFiles Then
                Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
                Return mfiles.NextReturnFile(longRefNum)
            Else
                Dim strDirResult As String
                Dim shortFilter As String = "F_Return"
                If shortRefNumber.Contains("LUB") Then
                    shortFilter = "L_Return"
                End If
                Dim I As Integer
                For I = 0 To 20
                    strDirResult = Dir(CorrPath & "*_Return" & CStr(I) & "*.xls")
                    If strDirResult = "" Then
                        strDirResult = Dir(CorrPath & "*" & shortFilter & CStr(I) & "*.xls*")
                        If strDirResult = "" Then
                            Return I
                            Exit For
                        End If
                    End If
                Next I
                Return Nothing
            End If
        ElseIf Vertical = VerticalType.POWER Then
            If _useMFiles Then
                Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
                Return mfiles.NextReturnFile(longRefNum)
            Else
                Dim strDirResult As String
                Dim shortFilter As String = "_Return"
                Dim I As Integer
                For I = 0 To 20
                    strDirResult = Dir(CorrPath & "*_Return" & CStr(I) & "*.xls")
                    If strDirResult = "" Then
                        strDirResult = Dir(CorrPath & "*" & shortFilter & CStr(I) & "*.xls*")
                        If strDirResult = "" Then
                            Return I
                            Exit For
                        End If
                    End If
                Next I
                Return Nothing
            End If
        End If
    End Function

    Public Function FileExists(path As String) As Boolean
        Try
            Return File.Exists(path)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function FileSearchDirCommand(Optional path As String = "") As String
        'expecting wildcard char in path
        Return Dir(path)
    End Function

    Public Function FileDateTime(path As String) As String
        Return Microsoft.VisualBasic.FileSystem.FileDateTime(path)
    End Function

    Public Sub FileMove(pathFrom As String, pathTo As String)
        Try
            File.Move(pathFrom, pathTo)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function FileDelete(path As String) As Boolean
        Try
            File.Delete(path)
        Catch ex As Exception
            'MsgBox(ex.Message)
            Return False
        End Try
        Return True
    End Function

    Public Sub WriteStream(path As String, append As Boolean, text As System.Text.StringBuilder)
        Dim stream As New StreamWriter(path, append)
        stream.Write(text.ToString())
        stream.Close()
    End Sub

    Public Function ReadStream(path As String) As String()
        Dim output As String()
        Dim count As Integer = 0
        Dim reader As StreamReader
        Try
            reader = New StreamReader(path)
            While Not reader.EndOfStream
                ReDim Preserve output(count)
                output(count) = reader.ReadLine()
            End While
        Catch ex As Exception
            Dim blank() As String
            Return blank
        Finally
            Try
                reader.Close()
            Catch
            End Try
        End Try
        Return output
    End Function

    Public Function OpenXlWorkbook(path As String, ByRef xl As Excel.Application, _
                                   Optional readAccess As Boolean = False) As Excel.Workbook
        xl = Utilities.GetExcelProcess()
        Dim w As Excel.Workbook = xl.Workbooks.Open(path, , readAccess)
        Return w
    End Function

    Public Function DirectoryExists(path As String) As Boolean
        'path = path.Remove(path.Length - 1, 1)
        Return Directory.Exists(path)
    End Function

    Public Sub CreateDirectory(path As String)
        Try
            Directory.CreateDirectory(path)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function FileCopy(pathFrom As String, pathTo As String, Optional overWrite As Boolean = True)
        Try
            File.Copy(pathFrom, pathTo, overWrite)
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Public Function GetFiles(path As String, Optional filter As String = "") As String()
        If filter = String.Empty Then filter = "*"
        Return Directory.GetFiles(path, filter)
    End Function

    'Public Function RefreshMFilesCollections(refnum As String, companyBenchmarkName As String,
    '    ByRef benchmarkingParticipantRefineryFiles As SA.Console.Common.ConsoleFilesInfo,
    '    ByRef benchmarkingParticipantCompanyFiles As SA.Console.Common.ConsoleFilesInfo,
    '    ByRef refiningGeneralFiles As SA.Console.Common.ConsoleFilesInfo,
    '    forceRefresh As Integer) As Boolean

    '    'forceRefresh: 0 = Plant, 1 = company, 2 = refining general

    '    If Not _useMFiles Then
    '        Return True
    '    End If

    '    If forceRefresh > -1 Then
    '        If Not IsNothing(_mfiles) Then
    '            Select Case forceRefresh
    '                Case 0
    '                    _mfiles.BenchmarkingParticipantRefineryFiles = Nothing 'reset

    '                    benchmarkingParticipantRefineryFiles = _mfiles.BenchmarkingParticipantRefineryFiles
    '                Case 1
    '                    _mfiles.BenchmarkingParticipantCompanyFiles = Nothing 'reset
    '                    Try
    '                        Dim tempMfiles As New DocsMFilesAccess(ConsoleVertical, companyBenchmarkName)
    '                        tempMfiles.BenchmarkingParticipantCompanyFiles = Nothing
    '                        benchmarkingParticipantCompanyFiles = tempMfiles.BenchmarkingParticipantCompanyFiles
    '                    Catch ex As Exception
    '                        If Not ex.Message.Contains("Unable to find Benchmarking Participant for") Then
    '                            MsgBox(ex.Message)
    '                            Return False
    '                        End If
    '                        benchmarkingParticipantCompanyFiles = Nothing
    '                    End Try
    '                Case 2
    '                    _mfiles.RefiningGeneralFiles = Nothing 'reset
    '                    refiningGeneralFiles = _mfiles.RefiningGeneralFiles
    '                Case Else

    '            End Select
    '            Return True
    '        End If
    '    End If

    '    Try
    '        Dim changed As Boolean = False
    '        If refnum.Trim().ToUpper() = _mfilesRefnum AndAlso companyBenchmarkName.Trim().ToUpper() = _mfilesCompanyBenchmarkName.Trim().ToUpper() Then
    '            changed = False
    '        Else
    '            changed = True
    '        End If

    '        If changed Then
    '            _mfilesRefnum = refnum
    '            _mfilesCompanyBenchmarkName = companyBenchmarkName
    '            _mfiles = New DocsMFilesAccess(_vertical, refnum, refnum)
    '            benchmarkingParticipantRefineryFiles = _mfiles.BenchmarkingParticipantRefineryFiles
    '            refiningGeneralFiles = _mfiles.RefiningGeneralFiles
    '            Try
    '                Dim tempMfiles As New DocsMFilesAccess(ConsoleVertical, companyBenchmarkName)
    '                tempMfiles.BenchmarkingParticipantCompanyFiles = Nothing
    '                benchmarkingParticipantCompanyFiles = tempMfiles.BenchmarkingParticipantCompanyFiles
    '            Catch ex As Exception
    '                If ex.Message.Contains("Unable to find Benchmarking Participant for") Then
    '                    benchmarkingParticipantCompanyFiles = Nothing
    '                End If
    '            End Try
    '        Else
    '            'don't change the byref vars
    '        End If
    '    Catch ex As Exception
    '        MsgBox("Error in RefreshMFilesCollections(): " & ex.Message)
    '        Return False
    '    End Try
    'End Function


    'Public Function GetPathToFile(folderPath As String, fileName As String, Optional LongRefNum As String = "") As String
    '    If _vertical = VerticalType.NGPP Then
    '        Return folderPath + fileName
    '    Else
    '        'download from mfiles and return path to that.
    '        Return String.Empty
    '    End If
    'End Function
End Class



