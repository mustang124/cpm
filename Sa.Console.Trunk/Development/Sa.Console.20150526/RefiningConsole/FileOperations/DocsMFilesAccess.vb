﻿Imports Solomon.Common.DocumentManagement.MFiles
Imports Solomon.Common.DocumentManagement.MFiles.WebAccess
Imports System.Configuration
'Imports System.Web
Imports System
Imports System.Collections.Generic
Imports Solomon.Common.DocumentManagement.MFilesComUtilities
Imports SA.Console.Common

Public Class DocsMFilesAccess

    Private _mfiles As Solomon.Common.DocumentManagement.MFiles.MFilesManager
    Private _url As String = ConfigurationManager.AppSettings("MfilesUrl")
    Private _vaultGuid As String = ConfigurationManager.AppSettings("VaultGuid")
    Private _mfilesId As String = My.User.Name ' DevConfigHelper.ReadConfig("MFilesId")
    Private _mfilesPw As String = String.Empty 'Console.MfilesPassword ' Login.MfilesPassword ' DevConfigHelper.ReadConfig("MFilesPw")
    Private Const _delim As Char = Chr(14)

    Private _vertical As VerticalType 'Olefins, Refining, etc.
    Private _errorLogType As String
    Private _shortRefNumber As String = String.Empty
    Private _longRefNumber As String = String.Empty
    Private _benchmarkingParticipantId As Integer = -1
    Private _mFilesUserId As Integer = -1

    'MFiles constants
    Private _generalRefineryPropertyDefinitionId As Integer = 1327

    Private _docClassesListId As Integer = 1
    Private _benchmarkingDeliverableTypesListId As Integer = 157
    Private _generalRefineryValueListId As Integer = 198
    Private _generalRefinerySubfoldersListId As Integer = 199 ' a/k/a Benchmarking General Type
    Private _benchmarkingGeneralTypesListId As Integer = _generalRefinerySubfoldersListId 'I'm just adding this so it can be found by the MFiles name
    Private _benchmarkingParticipantListId As Integer = 1144
    Private _projectObjectId As Integer = 162

    Private _benchmarkingParticipantRefineryFiles As ConsoleFilesInfo

    Public Property BenchmarkingParticipantRefineryFiles As ConsoleFilesInfo
        Get
            If IsNothing(_benchmarkingParticipantRefineryFiles) Then
                _benchmarkingParticipantRefineryFiles = New ConsoleFilesInfo(True, _vertical)
                If _vertical = VerticalType.POWER Then
                    _benchmarkingParticipantRefineryFiles.BenchmarkingParticipantId = _benchmarkingParticipantId
                End If
                '_benchmarkingParticipantRefineryFiles.Files = New List(Of ConsoleFile)
                '_benchmarkingParticipantRefineryFiles.BenchmarkingParticipant = _benchmarkingParticipantId
                Dim results As Results(Of ObjectVersion) _
                      = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
                If Not IsNothing(results) AndAlso Not IsNothing(results.Items) AndAlso results.Items.Count > 0 Then
                    For Each item As ObjectVersion In results.Items
                        Dim fileInfo As New ConsoleFile(True)
                        fileInfo.Id = item.ObjVer.ID
                        fileInfo.DocumentDate = item.CreatedUtc
                        fileInfo.FileName = item.Title  'item.Name????
                        fileInfo.FileExtension = item.Files(0).Extension
                        fileInfo.CheckedOutTo = item.CheckedOutTo
                        fileInfo.CheckedOutToUserName = item.CheckedOutToUserName
                        Try  'these all seem to have a problem at this time
                            fileInfo.CreatedUtcDate = item.CreatedUtcDate
                        Catch CreatedUtcDateEx As System.Exception
                            'ignore for now
                        End Try
                        Try
                            fileInfo.LastModifiedUtcDate = item.LastModifiedUtc
                        Catch
                        End Try
                        _benchmarkingParticipantRefineryFiles.Files.Add(fileInfo)
                    Next
                End If
            End If
            Return _benchmarkingParticipantRefineryFiles
        End Get
        Set(value As ConsoleFilesInfo)
            _benchmarkingParticipantRefineryFiles = value
        End Set
    End Property

    ''what is the difference between corr and co files??? BenchPart id is diff, even though class for both is BP

    Private _benchmarkingParticipantCompanyFiles As ConsoleFilesInfo
    Public Property BenchmarkingParticipantCompanyFiles As ConsoleFilesInfo
        Get
            If IsNothing(_benchmarkingParticipantCompanyFiles) Then
                _benchmarkingParticipantCompanyFiles = New ConsoleFilesInfo(True, _vertical)
                '_benchmarkingParticipantCompanyFiles.Files = New List(Of ConsoleFile)
                Try
                    _benchmarkingParticipantCompanyFiles = GetDocNamesAndIdsByBenchmarkingParticipant(_longRefNumber)
                    Return _benchmarkingParticipantCompanyFiles
                Catch ex As Exception
                    Throw ex
                End Try
            End If
            Return _benchmarkingParticipantCompanyFiles
        End Get
        Set(value As ConsoleFilesInfo)
            _benchmarkingParticipantCompanyFiles = value
        End Set
    End Property

    Private _refiningGeneralFiles As ConsoleFilesInfo
    Public Property RefiningGeneralFiles As ConsoleFilesInfo
        Get
            If IsNothing(_refiningGeneralFiles) Then
                _refiningGeneralFiles = New ConsoleFilesInfo(True, _vertical)
                Try
                    If _vertical = VerticalType.REFINING Then
                        Dim refnumWithoutYear As String = Utilities.GetSiteNumberFromRefnum(_longRefNumber, _vertical)
                        refnumWithoutYear = refnumWithoutYear & Utilities.GetRegionFromRefnum(_longRefNumber, _vertical)
                        Dim result As String = GetGeneralRefineryDocs(refnumWithoutYear, _refiningGeneralFiles, 0)
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End If
            Return _refiningGeneralFiles
        End Get
        Set(value As ConsoleFilesInfo)
            _refiningGeneralFiles = value
        End Set
    End Property

    Public ReadOnly Property Vertical() As VerticalType
        Get
            Return _vertical
        End Get
        'Set(ByVal value As String)
        '    _vertical = value
        'End Set
    End Property

    Public Property ErrorLogType() As String
        Get
            Return _errorLogType
        End Get
        Set(value As String)
            _errorLogType = value
        End Set
    End Property

    Public Property BenchmarkingParticipantId() As Integer
        Get
            Return _benchmarkingParticipantId
        End Get
        Set(value As Integer)
            _benchmarkingParticipantId = value
        End Set
    End Property

    Public Sub New(Vertical As VerticalType, documentTitle As String)
        'For finding Company benchmarking participant. Use other New for finding Plant/Refinery benchmarking participant
        Try
            _vertical = Vertical
            Select Case Vertical
                Case VerticalType.OLEFINS, VerticalType.REFINING, VerticalType.POWER
                    _mfilesPw = Main.MfilesPassword
                    If _mfilesPw.Length < 1 Then
                        _mfilesPw = DevConfigHelper.ReadConfig("MFilesPw")
                    End If
                Case Else

            End Select
            'login here
            _mfiles = New Solomon.Common.DocumentManagement.MFiles.MFilesManager(_url, _vaultGuid, _mfilesId, _mfilesPw)

            _benchmarkingParticipantId = _mfiles.GetBenchmarkingParticipantIdByUniqueNameOrTitle(documentTitle)
            If _benchmarkingParticipantId < 0 Then
                'not found: 
                Throw New System.Exception("Unable to find Benchmarking Participant for '" + documentTitle + "' in MFiles.")
            End If

            _mFilesUserId = _mfiles.GetUserId()

        Catch ex As System.Exception
            If ex.Message.Contains("Login to vault failed") Then
                Dim loginErrMsg As String = "Login to MFiles failed: ID " + _mfilesId + ", password " + _mfilesPw + ", URL " + _url + ", Vault GUID " + _vaultGuid
                MsgBox(loginErrMsg)
                Throw New System.Exception(loginErrMsg)
            Else
                Throw ex
            End If
        End Try

    End Sub

    Public Sub New(Vertical As VerticalType, ShortRefNumber As String, LongRefNumber As String)
        'For Plant/Refinery benchmarking participant. Use other New for finding Company benchmarking participant.
        Try
            _vertical = Vertical
            _shortRefNumber = ShortRefNumber
            _longRefNumber = LongRefNumber
            Select Case Vertical
                Case VerticalType.OLEFINS, VerticalType.REFINING, VerticalType.POWER
                    _mfilesPw = Main.MfilesPassword
                    If _mfilesPw.Length < 1 Then
                        _mfilesPw = DevConfigHelper.ReadConfig("MFilesPw")
                    End If
                Case Else

            End Select
            'login here
            _mfiles = New Solomon.Common.DocumentManagement.MFiles.MFilesManager(_url, _vaultGuid, _mfilesId, _mfilesPw)




            'Dim encodedRefnum As String = _longRefNumber.Replace("&", "&amp;")
            Dim encodedRefnum As String = _longRefNumber.Replace("&", "%26")
            'Dim encodedRefnum As String = System.Web.HttpUtility.HtmlEncode(_longRefNumber)





            _benchmarkingParticipantId = _mfiles.GetBenchmarkingParticipantId(encodedRefnum)
            If _benchmarkingParticipantId < 0 Then
                'not found: 
                Throw New System.Exception("Unable to find Benchmarking Participant for " + _longRefNumber + " in MFiles.")
            End If

            _mFilesUserId = _mfiles.GetUserId()

        Catch ex As System.Exception
            If ex.Message.Contains("Login to vault failed") Then
                Dim loginErrMsg As String = "Login to MFiles failed: ID " + _mfilesId + ", password " + _mfilesPw + ", URL " + _url + ", Vault GUID " + _vaultGuid
                MsgBox(loginErrMsg)
                Throw New System.Exception(loginErrMsg)
            Else
                MsgBox(ex.Message)
            End If
        End Try
    End Sub

    Public Function GetVF1FileInfo() As ConsoleFilesInfo
        'sample VF1 info:
        'doc ID 1911 Version 1
        'Class   Benchmarking Participant
        'Deliverable Type  Input Data Review (IDR)
        'Document Name VF1 DOW - FREEPORT 8
        'Benchmarking Participant  2013PCH099 - Dow - FREEPORT 8
        'Document date 3/27/2014

        Dim mfilesFileInfo As New ConsoleFilesInfo(True, _vertical)
        'Dim refNumId As Integer = _mfiles.GetIdOfBenchmarkingRefnum(_longRefNumber)
        'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(_longRefNumber)

        '//olomon.Common.DocumentManagement.MFiles.WebAccess.Results(Of ObjectVersion)


        Dim results As Results(Of ObjectVersion) _
            = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        For Each item As ObjectVersion In results.Items
            If item.Files.Count > 0 Then
                For count As Integer = 0 To item.Files.Count - 1
                    If item.Files(0).Name.StartsWith("VF1 ") Then
                        Dim consoleFile As New ConsoleFile(True)
                        consoleFile = PopulateMfilesConsoleFile(item)
                        'consoleFile.FileName = item.Files(count).Name
                        'Dim fileId As Integer = item.ObjVer.ID
                        'Dim propertyValues As List(Of PropertyValue) = _mfiles.GetObjectProperties(fileId)
                        'consoleFile.Id = item.ObjVer.ID
                        'Try
                        '    'these all seem to have a problem at this time
                        '    consoleFile.CreatedUtcDate = item.CreatedUtcDate
                        'Catch CreatedUtcDateEx As System.Exception
                        '    'ignore for now
                        'End Try

                        ''now get file's specific info
                        'consoleFile.ClassType = GetValueOfPropertyValue(propertyValues, 100).DisplayValue '/100  52
                        'consoleFile.DeliverableType = GetValueOfPropertyValue(propertyValues, 1211).DisplayValue
                        'consoleFile.DocumentDate = GetValueOfPropertyValue(propertyValues, 1179).DisplayValue
                        mfilesFileInfo.Files.Add(consoleFile)
                    End If
                Next
            End If
        Next
        Return mfilesFileInfo
    End Function



    Private Function GetValueOfPropertyValue(propertyValues As List(Of PropertyValue), _
                                             propertyDefNumber As Integer) As TypedValue
        'MFiles has a set of properties, each is of type "PropertyValue"
        'So we have to get the value of the PropertyValue type that we choose.
        Dim result As TypedValue = Nothing
        For Each propertyValue As PropertyValue In propertyValues
            If propertyValue.PropertyDef = propertyDefNumber Then 'title
                result = propertyValue.TypedValue
                Exit For
            End If
        Next
        Return result
    End Function

    Public Function BuildVRFile(mUserName As String, strTextForFile As String, CorrPathVR As String, _
                                lstVFNumbersText As String) As String
        'reuse CorrPathVR as the full path to the new VR file that has already been created.
        Try
            'Dim BenchPartId As Integer = _mfiles.GetBenchmarkingParticipantId(_longRefNumber)
            If Not UploadNewDocToMFiles(CorrPathVR, 4, _benchmarkingParticipantId) Then
                Return "VR" & lstVFNumbersText & ".txt failed to be built!"
            End If
            Return "VR" & lstVFNumbersText & ".txt has been built."
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Private Sub PopulateVFFiles(ByVal CorrPath As String, ByVal CorrPathV As String,
                            ByVal SelectedItemString As String, ByRef VFFiles As List(Of Object),
                            Optional ByVal containsNotStartsWith As Boolean = False)
        'orig filesystem code
        'Dim strDirResult As String
        'strDirResult = Dir(CorrPath + CorrPathV)
        'Do While strDirResult <> ""
        '    If SelectedItemString = Mid(strDirResult, 3, 1) Then
        '        VFFiles.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
        '    End If
        '    strDirResult = Dir()
        'Loop

        'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(_longRefNumber)
        Dim results As Results(Of ObjectVersion) _
            = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        For Each item As ObjectVersion In results.Items
            If item.Files.Count > 0 Then
                For count As Integer = 0 To item.Files.Count - 1
                    Dim filter = CorrPathV.Replace("*", "")
                    Dim match = False
                    If Not containsNotStartsWith Then
                        If (item.Files(count).Name.StartsWith(filter)) AndAlso _
                          (SelectedItemString = Mid(item.Files(count).Name, 3, 1)) Then  'i.e., the "1" in "VF1"
                            match = True
                        End If
                    Else
                        If (item.Files(count).Name.Contains(filter)) Then
                            match = True
                        End If
                    End If
                    'If (item.Files(count).Name.StartsWith(filter)) AndAlso _
                    '    (SelectedItemString = Mid(item.Files(count).Name, 3, 1)) Then  'i.e., the "1" in "VF1"
                    If match Then
                        Dim consoleFile As New ConsoleFile(True)
                        consoleFile.FileName = item.Files(count).Name
                        consoleFile.FileExtension = item.Files(count).Extension
                        Dim fileId As Integer = item.ObjVer.ID
                        Dim propertyValues As List(Of PropertyValue) = _mfiles.GetObjectProperties(fileId)
                        consoleFile.Id = item.ObjVer.ID
                        Try
                            'these all seem to have a problem at this time
                            consoleFile.CreatedUtcDate = item.CreatedUtcDate
                        Catch CreatedUtcDateEx As System.Exception
                            'ignore for now
                        End Try

                        Try
                            consoleFile.LastModifiedUtcDate = item.LastModifiedUtc
                        Catch
                        End Try

                        'now get file's specific info
                        'consoleFile.ClassType = GetValueOfPropertyValue(propertyValues, 100).DisplayValue '/100  52
                        'consoleFile.DeliverableType = GetValueOfPropertyValue(propertyValues, 1211).DisplayValue
                        'consoleFile.DocumentDate = GetValueOfPropertyValue(propertyValues, 1179).DisplayValue

                        PopulateVF(consoleFile.ClassType, propertyValues, 100)
                        PopulateVF(consoleFile.DeliverableType, propertyValues, 1211)
                        PopulateVF(consoleFile.DocumentDate, propertyValues, 1179, "DATE")
                        PopulateVF(consoleFile.LastModifiedUtcDate, propertyValues, 21)

                        'VFFiles.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                        Dim formatDocumentDate As String = String.Empty
                        If item.CheckedOutTo > 0 Then
                            formatDocumentDate = "(Checked out to " + item.CheckedOutToUserName + "):  "
                        End If

                        'If consoleFile.DocumentDate.HasValue Then
                        If Not IsNothing(consoleFile.LastModifiedUtcDate) Then
                            formatDocumentDate += consoleFile.LastModifiedUtcDate.ToString("dd-MMM-yy") '.DocumentDate
                        End If
                        VFFiles.Add(Format(formatDocumentDate & " | " & consoleFile.FileName & "." & consoleFile.FileExtension))
                    End If
                Next
            End If
        Next

    End Sub

    Private Sub PopulateVF(ByRef prop As Object, propertyValues As List(Of PropertyValue), _
        propertyDefNumber As Integer, Optional dataType As String = "")
        Try
            If dataType.ToUpper() = "DATE" Then
                prop = CDate(GetValueOfPropertyValue(propertyValues, propertyDefNumber).DisplayValue)
            ElseIf dataType.ToUpper() = "INTEGER" Then
                prop = CInt(GetValueOfPropertyValue(propertyValues, propertyDefNumber).DisplayValue)
            Else
                prop = GetValueOfPropertyValue(propertyValues, propertyDefNumber).DisplayValue
            End If
        Catch ex As System.Exception
            'just skip this prop
        End Try
    End Sub

    Public Function DownloadFile(folderPath As String, docId As Integer) As Boolean
        Try
            Return _mfiles.DownloadFile(folderPath, docId)
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Sub lstVFFiles_DoubleClick(fileName As String, pathToFile As String)
        'If Not folderPath.EndsWith("\") Then
        '    folderPath = CorrPath + "\"
        'End If
        'need to download file and open it.
        'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(_longRefNumber)

        Dim results As Results(Of ObjectVersion) _
            = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        '##############maybe need this one insetead:
        'Dim results As Results(Of ObjectVersion) _
        '    = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", refNumId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        For Each item As ObjectVersion In results.Items
            If item.Files.Count > 0 Then
                For count As Integer = 0 To item.Files.Count - 1
                    If (item.Files(count).Name + "." + item.Files(count).Extension = fileName) Then
                        Dim id As Integer = item.ObjVer.ID
                        Dim result As String = OpenFileFromMfiles(id)
                        If result.Length > 0 Then Throw New System.Exception(result)
                        '_mfiles.DownloadFileToThisFolder(id, pathToFile)
                        'Process.Start(pathToFile + item.Files(count).Name) '+ "." + item.Files(count).Extension)
                    End If
                Next
            End If
        Next
    End Sub

    Public Function GetDataFor_lstVFNumbers(filter As String, path As String, numsFoundAlready As List(Of String),
                Optional containsNotStartsWith As Boolean = False) As List(Of String)
        If IsNothing(numsFoundAlready) Then
            numsFoundAlready = New List(Of String)
        End If

        'get files in correspondence for this refnum
        filter = filter.Replace("*", "")
        'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(_longRefNumber)
        Dim results As Results(Of ObjectVersion) _
            = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        For Each item As ObjectVersion In results.Items
            If item.Files.Count > 0 Then
                For count As Integer = 0 To item.Files.Count - 1
                    If Not containsNotStartsWith Then
                        If (item.Files(count).Name.StartsWith(filter)) Then 'AndAlso _
                            '(SelectedItemString = Mid(item.Files(count).Name, 3, 1)) Then  'i.e., the "1" in "VF1"
                            Dim found As Boolean = False
                            Dim thisNumber As String = item.Files(count).Name.Substring(2, 1)
                            For Each num As String In numsFoundAlready
                                If num = thisNumber Then
                                    found = True
                                    Exit For
                                End If
                            Next
                            If Not found Then
                                numsFoundAlready.Add(thisNumber)
                            End If
                        End If
                    Else
                        If (item.Files(count).Name.Contains(filter)) Then
                            Dim found As Boolean = False
                            Dim fileName As String = item.Files(count).Name
                            Dim index As Integer = fileName.IndexOf(filter)

                            If Not fileName.EndsWith(filter) Then ' doesn't end with a number so don't count it
                                Dim thisNumber As String = fileName.Substring(index + filter.Length, 1)
                                For Each num As String In numsFoundAlready
                                    If num = thisNumber Then
                                        found = True
                                        Exit For
                                    End If
                                Next
                                If Not found Then
                                    numsFoundAlready.Add(thisNumber)
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        Next
        Return numsFoundAlready
    End Function



    Public Function lstVRFilesItems(CorrPath As String, SelectedItemString As String) As List(Of Object)
        ''Use the VFFiles to populate lstVRFiles
        Dim vFFiles As New List(Of Object)
        PopulateVFFiles(CorrPath, "VF*", SelectedItemString, vFFiles)
        PopulateVFFiles(CorrPath, "VR*", SelectedItemString, vFFiles)
        ''------------------
        '' Added 1/30/2006 by FRS
        '' Include any VA files in lstVRFiles so the user can see whether
        '' the VF has been ack'ed.
        PopulateVFFiles(CorrPath, "VA*", SelectedItemString, vFFiles)
        If _vertical = VerticalType.REFINING Then
            PopulateVFFiles(CorrPath, "F_Return" + SelectedItemString, SelectedItemString, vFFiles, True)
            PopulateVFFiles(CorrPath, "L_Return" + SelectedItemString, SelectedItemString, vFFiles, True)
        Else
            PopulateVFFiles(CorrPath, "*Return*", SelectedItemString, vFFiles)
        End If

        Return vFFiles
    End Function

    Public Function ValFax_Build(ByRef strValFaxTemplate As String, ByRef UserName As String, TemplatePath As String, _
                            ByRef btnValFaxText As String, ByRef userInitials As String, _
                            RefNum As String, ByRef db As Internal.Console.DataObject.DataObject, _
                            StudyDrive As String, StudyYear As String, StudyRegion As String, Study As String, _
                            TempPath As String, ByRef CurrentRefNum As String, CorrPathParameter As String, _
                            DeadlineDays As String, ByRef TemplateValues As Object, _
                            ByRef VFFileName As String) As Boolean


        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.



        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        'Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""
        Dim objWriter As System.IO.StreamWriter
        Try
            If Me.Vertical = VerticalType.OLEFINS Then
                TemplateValues = TryCast(TemplateValues, SA.Console.Common.WordTemplate)
            ElseIf Me.Vertical = VerticalType.REFINING Then
                TemplateValues = TryCast(TemplateValues, SA.Console.Common.WordTemplate)
            End If

            If Not Directory.Exists(Main.ProfileConsolePath) Then Directory.CreateDirectory(ProfileConsolePath)
            If Not Directory.Exists(TempPath) Then Directory.CreateDirectory(TempPath)

            If File.Exists(TempPath + "ValFax.txt") Then File.Delete(TempPath + "ValFax.txt")
            If strValFaxTemplate = "" Then
                ' Find out which template they want to use.
                strProgress = "cboTemplates.Clear"
                frmTemplates.cboTemplates.Items.Clear()

                strDirResult = Dir(TemplatePath & "*.doc*")
                If strDirResult = "" Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)

                    Exit Function
                End If
                strProgress = "Loading cboTemplates"
                Do While strDirResult <> ""
                    If Me.Vertical = VerticalType.REFINING Then
                        If InStr(1, strDirResult, "Cover") > 0 And strDirResult.Contains(Utilities.GetRefNumPart(RefNum, 2)) Then
                            frmTemplates.cboTemplates.Items.Add(strDirResult)
                        End If
                    ElseIf Me.Vertical = VerticalType.OLEFINS Then
                        frmTemplates.cboTemplates.Items.Add(strDirResult)
                    End If
                    strDirResult = Dir()
                Loop

                If frmTemplates.cboTemplates.Items.Count < 1 Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)
                    Return False
                End If

                ' Default to first one.
                frmTemplates.cboTemplates.SelectedIndex = 0

                ' If there is only one, don't make them choose, just go.
                If frmTemplates.cboTemplates.Items.Count > 1 Then
                    ' We use this form for other purposes as well (like selecting which SpecFrac file to open).
                    ' So, reload the caption and label items to the origian "ValFax" values.
                    frmTemplates.Text = "Select a Cover Template"

                    ' Ready to show them the form.
                    'this shows a form with just a dropdown for user to choose from. Dropdown loaded from *.doc files in Correspondence/Valdiation/Templates folder
                    frmTemplates.ShowDialog()
                    If frmTemplates.OKClick = False Then
                        Exit Function
                    End If

                    ' Load strValFaxTemplate from what they selected on
                    ' frmTemplates. (strTemplateFile is a global variable.)
                    strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem
                    'MsgBox "After show form modal"
                Else
                    strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem
                End If
            End If

            ' At this point we have the name of the template to use.
            btnValFaxText = "Preparing New Document"
            strProgress = "Preparing New Document"
            ' Create the file to pass the data in.
            'Open gstrTempFolder & "ValFax.txt" For Output As #1
            'If Dir(TempDrive & _UserName & "\Console\", vbDirectory) = "" Then
            '    MkDir(TempDrive & _UserName & "\Console\")
            'End If

            objWriter = New System.IO.StreamWriter(TempPath + "ValFax.txt", True)

            ' Todays date
            strProgress = "Printing Long Date"
            objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
            If _vertical = VerticalType.REFINING Then
                TemplateValues.Field.Add("_TodaysDate")
            Else
                TemplateValues.Field.Add("_TodaysDate")
            End If

            TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))

            Dim nextVFnumber As String = String.Empty
            If strValFaxTemplate.Contains("LUB16 Cover FLCOMP.doc") Then
                nextVFnumber = NextV("VFFL", _longRefNumber).ToString()
            Else
                nextVFnumber = NextV("VF", _longRefNumber).ToString()
            End If

            If Me.Vertical = VerticalType.OLEFINS Then
                TemplateValues.Field.Add("_VFNumber")
                TemplateValues.RField.Add(nextVFnumber)
            End If

            strProgress = "Getting business days to respond."
            ' Due date
            ' Need a way to make sure we get valid input here.

            'SB all this moved to calling app:
            'If Dir(CorrPathParameter & "vf*") = "" Then
            '    intNumDays = 10
            'Else
            '    intNumDays = 5
            'End If
            'strDeadline = InputBox("How many business days do you want to give them to respond?", "", intNumDays)

            If DeadlineDays = "" Then
                objWriter.Close()
                MsgBox("Business Days were not given.")
                Return False
            End If

            datDeadline = Utilities.ValFaxDateDue(DeadlineDays)
            objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
            If _vertical = VerticalType.REFINING Then
                TemplateValues.Field.Add("DueDate")
            Else
                TemplateValues.Field.Add("_DueDate")
            End If

            TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
            strProgress = "Getting Company name"
            ' Company Name
            If _vertical <> VerticalType.POWER Then
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)
            Else
                params = New List(Of String)
                params.Add("@SiteId/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetTSortDataBySiteId", params)
            End If
            If ds.Tables.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                objWriter.WriteLine(row("Company"))
                TemplateValues.Field.Add("_Company")
                TemplateValues.RField.Add(row("Company").ToString)

                strProgress = "Getting Location"
                ' Refinery Name
                objWriter.WriteLine(row("Location"))

                If Me.Vertical = VerticalType.OLEFINS Then
                    TemplateValues.Field.Add("_Plant")
                ElseIf Me.Vertical = VerticalType.REFINING Then
                    TemplateValues.Field.Add("_Refinery")
                Else
                    TemplateValues.Field.Add("_Refinery")
                End If

                TemplateValues.RField.Add(row("Location").ToString)

                Coloc = row("Coloc").ToString

            End If

            strProgress = "Getting Contact info"


            ' Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            If Me.Vertical = VerticalType.REFINING AndAlso strValFaxTemplate = "LUB14 Cover FLCOMP.doc" Then
                ds = db.ExecuteStoredProc("Console." & "GetFuelLubeContactInfo", params)
            ElseIf Me.Vertical = VerticalType.OLEFINS OrElse Me.Vertical = VerticalType.REFINING Then
                ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
            End If
            If ds.Tables.Count = 0 Then

                If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    btnValFaxText = "New IDR Fax"
                    Exit Function
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    If Me.Vertical = VerticalType.REFINING Then
                        Dim lfn As String = ""
                        For Each row In ds.Tables(0).Rows
                            If lfn <> row("RefName").ToString Then
                                strFaxName += row("RefName").ToString + IIf(ds.Tables(0).Rows.Count > 1, ", ", "")
                                lfn = row("RefName").ToString
                                strFaxEmail += row("RefEmail").ToString + IIf(ds.Tables(0).Rows.Count > 1, "; ", "")
                            Else
                                strFaxName = strFaxName.Replace(",", "")
                                strFaxEmail = strFaxEmail.Replace(";", "")
                            End If
                        Next
                    ElseIf Me.Vertical = VerticalType.OLEFINS Then
                        row = ds.Tables(0).Rows(0)
                        strFaxName = row("NameFull").ToString
                        strFaxEmail = row("Email").ToString
                    End If
                End If
            End If

            objWriter.WriteLine(strFaxName)

            TemplateValues.Field.Add("_Contact")
            TemplateValues.RField.Add(strFaxName)
            ' Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_EMail")
            TemplateValues.RField.Add(strFaxEmail)
            strFaxEmail = ""
            strFaxName = ""
            strProgress = "Getting consultant info"
            ' Consultant Name
            params = New List(Of String)
            params.Add("Initials/" + userInitials)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    If Me.Vertical = VerticalType.REFINING Then
                        row = ds.Tables(0).Rows(0)
                        If row("ConsultantName") Is Nothing Or IsDBNull(row("ConsultantName")) Then
                            strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                        Else
                            strName = row("ConsultantName").ToString
                        End If
                    ElseIf Me.Vertical = VerticalType.OLEFINS Then
                        If row("EmployeeName") Is Nothing Or IsDBNull(row("EmployeeName")) Then
                            strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                        Else
                            strName = row("EmployeeName").ToString
                        End If
                    End If
                Else
                    strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                End If
            End If

            If strName = "" Then
                objWriter.Close()
                MsgBox("Name was not found or given.")
                Return False
            End If
            objWriter.WriteLine(strName)
            TemplateValues.Field.Add("_ConsultantName")
            TemplateValues.RField.Add(strName)
            ' Consultant Initials
            objWriter.WriteLine(userInitials)
            TemplateValues.Field.Add("_Initials")

            TemplateValues.RField.Add(userInitials)
            strProgress = "Path to store it in"
            ' Path to store it in
            ' If it is Trans Pricing or FLCOMP, put it, without VF#, into Refinery Corr folder.
            If Me.Vertical = VerticalType.REFINING Then
                'This has to be a check in MFiles, not the Network
                'StudyYear needs to be 4 digits
                If strValFaxTemplate.Contains("Trans Pricing.doc") Then
                    'Filename = StudyDrive & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\" & Coloc & "\"
                    Filename = "VF" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                    ' If it has "Company Wide" in the name, store it in the Company Corr folder
                ElseIf InStr(1, strValFaxTemplate, "Company Wide") > 0 Then
                    'Filename = StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Coloc & "\"
                    Filename = "VF" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                    ' Otherwise -- MOST CASES HERE -- store it in the Refinery Corr folder with VF# on the front.
                Else
                    If strValFaxTemplate <> "LUB16 Cover FLCOMP.doc" Then
                        ' Request from DEJ 3/2011 -- if it a Lubes refinery, put "Lube" at end of doc name.
                        'Filename = StudyDrive & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\VF" & NextV("VF", _longRefNumber) & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                        Filename = "VF" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                    Else
                        'Filename = StudyDrive & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\VFFL" & NextV("VFFL", _longRefNumber) & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                        Filename = "VFFL" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                    End If
                End If
            ElseIf Me.Vertical = VerticalType.OLEFINS Then
                'StudyYear needs to be 4 digits
                Filename = "VF" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc" 'StudyDrive & StudyYear & "\Correspondence\" & RefNum & "\VF" & nextVFnumber & " " & Coloc & ".doc"
            End If

            objWriter.WriteLine(Filename)
            If Me.Vertical = VerticalType.OLEFINS Then
                VFFileName = "VF" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
            ElseIf Me.Vertical = VerticalType.REFINING Then
                If (strValFaxTemplate.Contains("LUB16 Cover FLCOMP.doc")) Then
                    VFFileName = "VFFL" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                Else
                    VFFileName = "VF" & nextVFnumber & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                End If
            End If
            ' --------- Add new items here 4/20/2007 ---------

            strProgress = "Getting Company Contact info"

            ' Company Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("ContactType/COORD")
            params.Add("StudyYear/" & StudyYear)
            ds = db.GetCompanyContacts(params) 'ds = ExecuteStoredProc("CONSOLE.[GetCompanyContactInfo]", params)

            If ds.Tables.Count = 0 Then

                If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    btnValFaxText = "New IDR Fax"
                    Return False
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                    strFaxEmail = row("Email").ToString
                End If
            End If

            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_CoCoContact")
            TemplateValues.RField.Add(strFaxName)

            ' Company Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_CoCoEMail")
            TemplateValues.RField.Add(strFaxEmail)
            objWriter.Close()

            strProgress = "Copy doc to hard drive"
            ' Bring the doc down to the hard drive.

            If File.Exists(TempPath & strValFaxTemplate) Then
                Try
                    File.Delete(TempPath & strValFaxTemplate)
                Catch
                    'for some reason, user can't always delete this file.
                    MsgBox("Console needs to delete an old file at '" & TempPath & strValFaxTemplate & "', but was unable to do so. Please try to delete this file yourself and then retry.")
                    Try
                        File.Delete(TempPath + "ValFax.txt")
                    Catch
                    End Try
                    Return False
                End Try
            End If

            ''I don't care that this WAS in original code. I'M commenting it OUT !!!!
            'If File.Exists(TemplatePath & strValFaxTemplate) Then
            '    TemplatePath = TemplatePath
            'End If

            ' Copy the Template to my work area.
            File.Copy(TemplatePath & strValFaxTemplate, TempPath & strValFaxTemplate, True)
            timStart = Now()
            Do While File.Exists(TempPath & strValFaxTemplate) = False
                If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                    MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                End If
            Loop

            'WordTemplateReplaceNew(TempPath & strValFaxTemplate, TemplateValues, Filename)

            'UploadToMFiles(TempPath & Filename, 52, 12, _benchmarkingParticipantId)
            Return True
        Catch ex As System.Exception
            ErrorLogType = "Validation: " & CurrentRefNum.ToString()
            Throw ex
        Finally
            Try
                objWriter.Close()
            Catch
            End Try
        End Try

    End Function

    'Public Sub WordTemplateReplaceNew(strfile As String, fields As OlefinsMainConsole.WordTemplate, strfilename As String)


    '    Dim oWord As Object
    '    Dim oDoc As Object
    '    Dim range As Object
    '    Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
    '    For count = 0 To fields.Field.Count - 1
    '        DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
    '    Next
    '    MessageBox.Show(DoubleCheck, "Template Field Check")

    '    oWord = CreateObject("Word.Application")
    '    oWord.Visible = True
    '    oDoc = oWord.Documents.Open(strfile)
    '    range = oDoc.Content
    '    Try
    '        For count = 0 To fields.Field.Count - 1
    '            With oWord.Selection.Find
    '                .Text = fields.Field(count)
    '                .Replacement.Text = fields.RField(count)
    '                .Forward = True
    '                .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
    '                .Format = False
    '                .MatchCase = False
    '                .MatchWholeWord = False
    '                .MatchWildcards = False
    '                .MatchSoundsLike = False
    '                .MatchAllWordForms = False
    '            End With
    '            oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
    '        Next

    '        If File.Exists(strfilename) Then
    '            strfilename = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilename)
    '        Else
    '            'Stop
    '            strfilename = InputBox("Here is the suggested name for this New IDR Fax document." & Chr(10) & "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename)
    '        End If
    '        ' If they hit cancel, just stop.
    '        If strfilename = "" Then

    '            Exit Sub
    '        End If


    '        'Tries to save to Documents folder.
    '        'oDoc.SaveAs(strfilename, ADDTORECENTFILES:=True)
    '        '!!! Need to save toMFiles instead
    '        'Moved to DocsMFilesAccess.vb.ValFax_Build()


    '        MessageBox.Show("Name is now " & oDoc.Name)
    '    Catch ex As System.Exception
    '        MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Public Function UploadNewIdr(fullPath As String) As Boolean
        Try
            Dim pathParts() As String = fullPath.Split("\")
            Dim nameParts() As String = pathParts(pathParts.Length - 1).Split(".")
            Dim mfilesName As String = String.Empty
            For count As Integer = 0 To nameParts.Length - 2
                mfilesName += mfilesName + nameParts(count) + "."
            Next
            mfilesName = mfilesName.Remove(mfilesName.Length - 1)
            'Dim partId As Integer = _mfiles.GetBenchmarkingParticipantId(_longRefNumber)
            Dim project As Integer = GetMFilesProject(mfilesName)
            If project < 0 Then
                Return False
            End If
            _mfiles.UploadNewIDR(fullPath, _longRefNumber, DateTime.Today, project, mfilesName)
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function UploadNewDocToMFiles(FullPath As String, DeliverableType As Integer,
                               BenchmarkingParticipant As Integer, Optional DocumentDate As DateTime? = Nothing) _
                           As Boolean
        'THIS IS FOR BENCHMARKING PARTICIPANT DOC CLASS ONLY
        If BenchmarkingParticipant < 0 Then
            BenchmarkingParticipant = _benchmarkingParticipantId
        End If
        Try
            'deliverabletype = PropertyDef = 1211
            '52 is doc class Benchmarking Participant
            Dim docClass As Integer = 52
            Dim fileName As String = Utilities.ExtractFileName(FullPath)
            Dim project As Integer = GetMFilesProject(fileName)
            If project < 0 Then
                Return False
            End If
            _mfiles.UploadNewDoc(FullPath, docClass, DeliverableType, BenchmarkingParticipant, DocumentDate, project)
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function
    Public Function UploadNewGeneralRefineryItemToMFiles(fullPath As String, refNumWithoutYear As String,
            drawingArchive As Integer, Optional documentDate As DateTime? = Nothing) As Boolean
        '//if drawingArchive = 0, then put at GeneralRefinery level. If 1, put at 
        '//  acrhive subfolder. If 2, put at Drawings subfolder
        Try
            Dim nameOnly = Utilities.ExtractFileName(fullPath)
            nameOnly = Utilities.FileNameWithoutExtension(nameOnly)
            Dim project As Integer = GetMFilesProject(Utilities.ExtractFileName(fullPath))
            If project < 0 Then
                Return False
            End If
            _mfiles.UploadNewGeneralRefineryItem(fullPath, nameOnly, refNumWithoutYear, documentDate, drawingArchive, project)
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function
    Public Function UploadNewVersionOfDocToMFiles(fullPath As String, docId As Integer) As Boolean
        Try
            _mfiles.UploadNewVersionOfDoc(fullPath, docId)
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function GetDocNamesByBenchmarkingParticipant(longRefNum As String) As List(Of String)
        Dim result As New List(Of String)
        Dim list As List(Of ObjectVersion) = GetDocsByBenchmarkingParticipant(longRefNum)
        For Each item As ObjectVersion In list
            For Each mfileFile As ObjectFile In item.Files
                result.Add(mfileFile.Name + "." + mfileFile.Extension)
            Next
        Next
        Return result
    End Function

    Public Function GetDocNamesAndIdsByBenchmarkingParticipant(longRefNum As String) As SA.Console.Common.ConsoleFilesInfo
        Dim result As New List(Of String)
        Dim list As List(Of ObjectVersion) = GetDocsByBenchmarkingParticipant(longRefNum)
        Dim files As New SA.Console.Common.ConsoleFilesInfo(True, _vertical)
        For Each item As ObjectVersion In list
            Dim consoleFile As SA.Console.Common.ConsoleFile = PopulateMfilesConsoleFile(item)
            If Not IsNothing(consoleFile) Then files.Files.Add(consoleFile)
        Next
        Return files
    End Function

    Private Function PopulateMfilesConsoleFile(obj As ObjectVersion) As ConsoleFile
        Try
            Dim consoleFile As New ConsoleFile(True)
            consoleFile.CheckedOutTo = obj.CheckedOutTo
            consoleFile.CheckedOutToUserName = obj.CheckedOutToUserName
            consoleFile.FileName = obj.Files(0).Name ' obj.Title
            consoleFile.FileExtension = obj.Files(0).Extension
            consoleFile.Id = obj.ObjVer.ID
            Try
                'these all seem to have a problem at this time
                consoleFile.CreatedUtcDate = obj.CreatedUtcDate
            Catch CreatedUtcDateEx As System.Exception
                'ignore for now
            End Try
            Dim propertyValues As List(Of PropertyValue) = _mfiles.GetObjectProperties(consoleFile.Id)
            consoleFile.ClassType = GetValueOfPropertyValue(propertyValues, 100).DisplayValue

            Try
                consoleFile.DeliverableType = GetValueOfPropertyValue(propertyValues, 1211).DisplayValue
            Catch
            End Try

            Try
                consoleFile.DocumentDate = GetValueOfPropertyValue(propertyValues, 1179).DisplayValue
            Catch
            End Try

            Try
                consoleFile.LastModifiedUtcDate = GetValueOfPropertyValue(propertyValues, 21).DisplayValue
            Catch
            End Try

            Return consoleFile
        Catch ex As System.Exception
            Return Nothing
        End Try
    End Function

    Private Function GetDocsByBenchmarkingParticipant(longRefNum As String) As List(Of ObjectVersion)
        'NOTE: longRefNum arg is no longer used.
        Dim list As New List(Of ObjectVersion)
        Dim results As Results(Of ObjectVersion) _
            = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        For Each item As ObjectVersion In results.Items
            If item.Files.Count > 0 Then
                For count As Integer = 0 To item.Files.Count - 1
                    list.Add(item)
                Next
            End If
        Next
        Return list
    End Function

    Public Function GetConsultantTabInfo(refNumIds As List(Of Integer), Optional ByVal docNameStartsWithFilter As String = "", _
                                         Optional ByVal docNameContainsFilter As String = "") As ConsoleFilesInfo
        '!!! ' tried something other than ObjectVersion but no success.
        Throw New System.Exception("This won't work; MFiles returns ObjectVersion but that doesn't contain BenchmarkingParticipant.")
        If (Not IsNothing(docNameStartsWithFilter) AndAlso docNameStartsWithFilter.Length > 0) _
            And _
            (Not IsNothing(docNameContainsFilter) AndAlso docNameContainsFilter.Length > 0) Then
            Throw New System.Exception("Cannot send both startsWithFilter and containsFilter in GetConsultantTabInfo method")
        End If
        Dim args As New List(Of String)
        For Each refnumId As Integer In refNumIds
            args = AddNewBenchmarkingParticipantArg(args, _benchmarkingParticipantListId, _delim, "=", refnumId.ToString())
        Next

        If (Not IsNothing(docNameStartsWithFilter) And docNameStartsWithFilter.Length > 0) Then
            args = AddNewBenchmarkingParticipantArg(args, 0, _delim, "^=", docNameStartsWithFilter)
        ElseIf (Not IsNothing(docNameContainsFilter) And docNameContainsFilter.Length > 0) Then
            args = AddNewBenchmarkingParticipantArg(args, 0, _delim, "*=", docNameContainsFilter)
        End If
        Dim results As Results(Of ObjectVersion) =
            _mfiles.GetObjectsByMultiplePropertyValues(_delim, args)
        Dim files As New ConsoleFilesInfo(True, _vertical)
        For Each doc As ObjectVersion In results.Items
            Dim file As New ConsoleFile(True)
            file = PopulateMfilesConsoleFile(doc)
            files.Files.Add(file)
        Next
        Return files
    End Function

    Private Function AddNewBenchmarkingParticipantArg(list As List(Of String), propertyDefId As Integer, _
            delim As Char, searchOperator As String, propertyValue As String) As List(Of String)
        If IsNothing(list) Then
            list = New List(Of String)
        End If
        list.Add(propertyDefId.ToString() + delim + searchOperator + delim + propertyValue)
        Return list
    End Function
    Public Function GetDocByNameAndRefnum(fileName As String, longRefNum As String, startsWith As Boolean,
        Optional contains As Boolean = False) As ConsoleFilesInfo
        'NOTE: longRefNum arg is no longer used.
        Dim foundItem As ObjectVersion = Nothing
        'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(longRefNum)
        Dim results As Results(Of ObjectVersion) _
            = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        For Each item As ObjectVersion In results.Items
            If item.Files.Count > 0 Then
                For count As Integer = 0 To item.Files.Count - 1
                    If startsWith Then
                        If (item.Files(count).Name.ToUpper().StartsWith(fileName.ToUpper())) Then
                            foundItem = item
                            Exit For
                        End If
                    ElseIf contains Then
                        If (item.Files(count).Name.ToUpper().Contains(fileName.ToUpper())) Then
                            foundItem = item
                        End If
                    Else
                        If (item.Files(count).Name = fileName) Then
                            foundItem = item
                        End If
                    End If
                Next
            End If
        Next
        If IsNothing(foundItem) Then
            Return Nothing
        Else
            Dim fileInfo As New ConsoleFile(True)
            fileInfo.Id = foundItem.ObjVer.ID
            fileInfo.DocumentDate = foundItem.CreatedUtc
            fileInfo.FileName = foundItem.Title
            fileInfo.FileExtension = foundItem.Files(0).Extension
            Dim filesInfo As New ConsoleFilesInfo(True, _vertical)
            filesInfo.BenchmarkingParticipantId = _benchmarkingParticipantId
            filesInfo.Files.Add(fileInfo)
            Return filesInfo
        End If
        Return Nothing
    End Function

    'Public Function GetGeneralRefineryDocs(generalRefineryTextId As String,
    '                                       ByRef consoleFiles As ConsoleFilesInfo) As String
    '    'For refining, generalRefineryId will be like 102EUR
    '    Try
    '        If _vertical = VerticalType.REFINING Then
    '            Dim foundItem As ObjectVersion = Nothing
    '            Dim generalIdPropertyValue As Integer = -1
    '            'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(longRefNum)
    '            Dim valueListItemResults As Results(Of ValueListItem) _
    '                = _mfiles.GetSomeValueListItems(_generalRefineryValueListId)
    '            For Each itm As ValueListItem In valueListItemResults.Items
    '                If itm.DisplayID = generalRefineryTextId Then
    '                    generalIdPropertyValue = itm.ID
    '                    Exit For
    '                End If
    '            Next
    '            If generalIdPropertyValue < 0 Then
    '                Return "GeneralRefinery '" + generalRefineryTextId + "' was not found in M-Files"
    '            End If
    '            Dim searchOperator As String = "=" '; // where Name Contains property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
    '            Dim resultsGeneralRefineryDocs As Results(Of ObjectVersion) =
    '                    _mfiles.GetObjectsByPropertyValue(_generalRefineryPropertyDefinitionId,
    '                     searchOperator, generalIdPropertyValue.ToString())

    '            'If consoleFiles.Files.Count() < 1 Then   just return empty list
    '            consoleFiles = New ConsoleFilesInfo(True)
    '            consoleFiles.BenchmarkingParticipant = _benchmarkingParticipantId
    '            For Each item As ObjectVersion In resultsGeneralRefineryDocs.Items
    '                Dim fileInfo As New ConsoleFile(True)
    '                fileInfo.Id = item.ObjVer.ID
    '                fileInfo.DocumentDate = item.CreatedUtc
    '                fileInfo.FileName = item.Title
    '                fileInfo.FileExtension = item.Files(0).Extension
    '                consoleFiles.Files.Add(fileInfo)
    '            Next
    '            'If consoleFiles.Files.Count() < 1 Then   just return empty list
    '        End If
    '        Return String.Empty 'no errors
    '    Catch ex As Exception
    '        Return "Error in GetGeneralRefineryDocs: " + ex.Message
    '    End Try
    'End Function
    Public Function GetGeneralRefineryDocs(generalRefineryTextId As String,
            ByRef consoleFiles As SA.Console.Common.ConsoleFilesInfo, drawingArchive As Integer,
            Optional nameContains As String = "") As String
        'For refining, generalRefineryId will be like 102EUR
        Try
            If _vertical = VerticalType.REFINING Then
                Dim foundItem As ObjectVersion = Nothing
                Dim generalIdPropertyValue As Integer = -1
                'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(longRefNum)
                Dim valueListItemResults As Results(Of ValueListItem) _
                    = _mfiles.GetSomeValueListItems(_generalRefineryValueListId)
                For Each itm As ValueListItem In valueListItemResults.Items
                    If itm.DisplayID = generalRefineryTextId Then
                        generalIdPropertyValue = itm.ID
                        Exit For
                    End If
                Next
                If generalIdPropertyValue < 0 Then
                    Return "GeneralRefinery '" + generalRefineryTextId + "' was not found in M-Files"
                End If

                Dim delim As String = "^"
                Dim args As New List(Of String)
                '//if drawingArchive = 0, then everythihg a GeneralRefinery level and below (drawings, etc). If 1, put at 
                '//  acrhive subfolder. If 2, put at Drawings subfolder
                args = AddNewBenchmarkingParticipantArg(args, 1327, delim, "=", generalIdPropertyValue.ToString())
                If nameContains.Length > 0 Then
                    'Dim searchOperator As String = "=" '; // where Name Contains property value. Refer to http://www.m-files.com/mfws/syntax.html#sect:search-encoding for more operators.
                    args = AddNewBenchmarkingParticipantArg(args, 0, delim, "*=", nameContains)
                End If
                'for drawings, 2
                'dont ask for 1331 if drawingArchive is 0!
                If drawingArchive <> 0 Then
                    args = AddNewBenchmarkingParticipantArg(args, 1331, delim, "=", drawingArchive.ToString())
                End If
                Dim resultsGeneralRefineryDocs As Results(Of ObjectVersion) =
                    _mfiles.GetObjectsByMultiplePropertyValues(delim, args)
                'Dim files As New ConsoleFilesInfo(True)
                'For Each doc As ObjectVersion In results.Items
                '    Dim file As New ConsoleFile(True)
                '    file = PopulateMfilesConsoleFile(doc)
                '    files.Files.Add(file)
                'Next


                'If consoleFiles.Files.Count() < 1 Then   just return empty list
                consoleFiles = New ConsoleFilesInfo(True, _vertical)
                consoleFiles.BenchmarkingParticipantId = _benchmarkingParticipantId
                For Each item As ObjectVersion In resultsGeneralRefineryDocs.Items
                    Dim fileInfo As New ConsoleFile(True)
                    fileInfo.Id = item.ObjVer.ID
                    fileInfo.DocumentDate = item.CreatedUtc
                    fileInfo.FileName = item.Title
                    fileInfo.FileExtension = item.Files(0).Extension
                    consoleFiles.Files.Add(fileInfo)
                Next

                'If consoleFiles.Files.Count() < 1 Then   just return empty list
            End If
            Return String.Empty 'no errors
        Catch ex As Exception
            Return "Error in GetGeneralRefineryDocs: " + ex.Message
        End Try
    End Function

    Public Function DocExists(fileName As String, longRefNum As String, startsWith As Boolean) As Boolean
        'NOTE: longRefNum arg is no longer used.
        If Not IsNothing(GetDocByNameAndRefnum(fileName, longRefNum, startsWith)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function NextV(filter As String, longRefNum As String)
        'expect filter such as vf  vr  etc.
        Dim count As Integer
        If ConsoleVertical = VerticalType.REFINING Then
            For count = 1 To 9
                If Not DocExists(filter + count.ToString(), longRefNum, True) Then
                    Return count
                End If
            Next
        Else
            For count = 1 To 9
                If Not DocExists(filter & count.ToString(), longRefNum, True) Then
                    Return count
                End If
            Next
        End If




        Return Nothing
    End Function

    Public Function NextReturnFile(longRefNum As String) As Integer
        'Dim refNumId As Integer = _mfiles.GetBenchmarkingParticipantId(longRefNum)
        Dim returnCount As Integer = 0
        Dim results As Results(Of ObjectVersion) _
            = _mfiles.GetObjectsByPropertyValue(_benchmarkingParticipantListId, "=", _benchmarkingParticipantId) 'get docs where value of Benchmarking Participant prop starts with long ref num
        For Each item As ObjectVersion In results.Items
            'Debug.Print(item.Title)
            If item.Files.Count > 0 Then
                For count As Integer = 9 To 0 Step -1
                    For itemCount As Integer = 0 To item.Files.Count - 1
                        Dim name As String = item.Files(itemCount).Name
                        Dim ext As String = item.Files(itemCount).Extension
                        'Based on what I'm seeing in MFiles, the prior logic
                        '    If (name.Contains("F_Return") And (name.EndsWith(".xls"))) Then
                        'needs to be replaced with this:
                        If _vertical = VerticalType.OLEFINS Then
                            If (name.StartsWith("VR" + count.ToString()) And name.Contains("Return") And (ext.Contains("xls"))) Then
                                Return count + 1
                            End If
                        ElseIf _vertical = VerticalType.REFINING Then
                            Dim strDirResult As String
                            Dim shortFilter As String = "F_Return"
                            If longRefNum.Contains("LUB") Then
                                shortFilter = "L_Return"
                            End If
                            If (name.Contains(shortFilter + count.ToString()) And ext.Contains("xls")) Then
                                If count + 1 > returnCount Then
                                    returnCount = count + 1
                                Else
                                    'do nothing, we don't want this number
                                End If
                            End If
                        End If
                    Next
                Next
            End If
        Next
        Return returnCount
    End Function

    Public Function CheckOutToMe(docId As Integer) As ObjectVersion
        Try
            Return _mfiles.SetCheckedoutStatus(docId, MFCheckOutStatus.CheckedOutToMe)
        Catch ex As System.Exception
            Return Nothing
        End Try
    End Function

    Public Function CheckIn(docId As Integer) As ObjectVersion
        Try
            Return _mfiles.SetCheckedoutStatus(docId, MFCheckOutStatus.CheckedIn)
        Catch ex As System.Exception
            Return Nothing
        End Try
    End Function

    Public Function GetCheckedOutStatus(docId As Integer) As MFilesCheckOutStatus
        Try
            Dim result As MFCheckOutStatus = _mfiles.GetCheckedOutStatus(docId)
            Select Case result
                Case MFCheckOutStatus.CheckedIn
                    Return MFilesCheckOutStatus.CheckedIn
                Case MFCheckOutStatus.CheckedOut
                    Return MFilesCheckOutStatus.CheckedOut
                Case MFCheckOutStatus.CheckedOutToMe
                    Return MFilesCheckOutStatus.CheckedOutToMe
                Case Else
                    Return Nothing
            End Select
        Catch ex As System.Exception
            Return Nothing
        End Try
    End Function

    Public Function DeleteFile(fileId As Integer) As Boolean
        Try
            _mfiles.DeleteFile(fileId)
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function OpenFileFromMfiles(fileNameWithoutExtension As String) As String
        Dim result As ConsoleFilesInfo = GetDocByNameAndRefnum(fileNameWithoutExtension, _longRefNumber, False)
        If Not IsNothing(result) Then 'AndAlso Not IsNothing(result.Files) AndAlso Not IsNothing(result.Files(0)) Then
            Dim vaultGuid As String = ConfigurationManager.AppSettings("VaultGuid").ToString()
            vaultGuid = vaultGuid.Replace("{", "")
            vaultGuid = vaultGuid.Replace("}", "")
            Dim url As String = "m-files://edit/" + vaultGuid + "/0-" + result.Files(0).Id.ToString() ' result.DisplayID
            Try
                Process.Start(url)
                Return String.Empty
            Catch ex As System.Exception
                Return "Unexpected error: Can't find the file " + fileNameWithoutExtension + " in MFiles. The url " + url + " failed."
            End Try
        Else
            Return "Unexpected error: Can't find the file " + fileNameWithoutExtension + " in MFiles."
        End If
    End Function

    Public Function OpenFileFromMfiles(docId As Integer) As String
        'Dim result As ObjectVersion = GetDocByNameAndRefnum(fileNameWithoutExtension, _longRefNumber, False)
        If docId > 0 Then 'AndAlso Not IsNothing(result.Files) AndAlso Not IsNothing(result.Files(0)) Then
            Dim vaultGuid As String = ConfigurationManager.AppSettings("VaultGuid").ToString()
            vaultGuid = vaultGuid.Replace("{", "")
            vaultGuid = vaultGuid.Replace("}", "")
            Dim url As String = "m-files://edit/" + vaultGuid + "/0-" + docId.ToString()
            Try
                Process.Start(url)
                Return String.Empty
            Catch ex As System.Exception
                Return "Unexpected error: Can't find the file with ID " + docId.ToString() + " in MFiles. The url " + url + " failed."
            End Try
        Else
            Return "Unexpected error: Can't find the file with ID " + docId.ToString() + " in MFiles."
        End If
    End Function

    Public Function GetUrlToOpenMfilesClient(objectTypeId As Integer, Optional benchmarkingId As Integer = -1) As String

        Dim url As String = "m-files://show/" + _vaultGuid + "/" + objectTypeId.ToString() + "-"
        url = url.Replace("{", "")
        url = url.Replace("}", "")
        If benchmarkingId = -1 Then 'Refinery corresp
            url += _benchmarkingParticipantId.ToString()
        Else 'company corresp
            url += benchmarkingId.ToString()
        End If
        'ENSURE NO { } 
        Return url
    End Function

    Private Function GetValueListItems(listId As Integer) As Results(Of ValueListItem)
        Return _mfiles.GetSomeValueListItems(listId)
    End Function

    Public Function GetGeneralRefineryId(generalRefineryTextId As String) As Integer
        Dim valueListItemsResults As Results(Of ValueListItem) = GetValueListItems(_generalRefineryValueListId)
        Dim generalRefineryId As Integer = -1
        For Each itm As ValueListItem In valueListItemsResults.Items
            If itm.DisplayID = generalRefineryTextId Then
                Return itm.ID
            End If
        Next
        Return -1
    End Function

    Public Function GetBenchmarkingDeliverableTypes() As Dictionary(Of Integer, String)
        Dim dic As Dictionary(Of Integer, String)
        If _vertical = VerticalType.REFINING Then
            Dim results As Results(Of ValueListItem) = GetValueListItems(_benchmarkingDeliverableTypesListId)
            For Each item As ValueListItem In results.Items
                If IsNothing(dic) Then
                    dic = New Dictionary(Of Integer, String)
                End If
                dic.Add(item.ID, item.Name)
            Next
        End If
        Return dic
    End Function

    Public Function GetDocClasses() As Dictionary(Of Integer, String)
        Dim dic As Dictionary(Of Integer, String)
        If _vertical = VerticalType.REFINING Then
            Dim results As Results(Of ValueListItem) = GetValueListItems(_docClassesListId)
            For Each item As ValueListItem In results.Items
                If IsNothing(dic) Then
                    dic = New Dictionary(Of Integer, String)
                End If
                dic.Add(item.ID, item.Name)
            Next
        End If
        Return dic
    End Function

    Public Function GetGeneralRefiningSubfolders() As Dictionary(Of Integer, String)
        Dim dic As Dictionary(Of Integer, String)
        If _vertical = VerticalType.REFINING Then
            Dim results As Results(Of ValueListItem) = GetValueListItems(_generalRefinerySubfoldersListId)
            For Each item As ValueListItem In results.Items
                If IsNothing(dic) Then
                    dic = New Dictionary(Of Integer, String)
                End If
                dic.Add(item.ID, item.Name)
            Next
        End If
        Return dic
    End Function

    Private Function GetMFilesProject(fileName As String) As Integer
        Dim suggestion As String = String.Empty
        Select Case ConsoleVertical
            Case VerticalType.REFINING
                suggestion = ConfigurationManager.AppSettings("RefiningMfileProjectNumberDefault")
            Case VerticalType.OLEFINS
                suggestion = ConfigurationManager.AppSettings("OlefinsMfileProjectNumberDefault")
            Case VerticalType.POWER
                '<add key="PowerMfileProjectNumberDefault" value="6PM006S"/>
                suggestion = ConfigurationManager.AppSettings("PowerMfileProjectNumberDefault")
            Case Else
                Return -1
        End Select
        Dim projectText As String = InputBox("Please enter the Project Number that applies to file " & fileName, "Console: MFiles Project Number needed", suggestion)
        Dim project As Integer = ConvertMFilesProjectTextToId(projectText)
        Return project
    End Function

    Private Function ConvertMFilesProjectTextToId(projectText As String) As Integer
        Dim valueListItemResults As Results(Of ValueListItem) _
                    = _mfiles.GetSomeValueListItems(_projectObjectId, Nothing, Nothing, Nothing, Nothing, projectText, Nothing, Nothing)
        For Each itm As ValueListItem In valueListItemResults.Items
            If itm.Name.StartsWith(projectText) Then
                Return itm.ID
            End If
        Next
        Throw New Exception("There was a problem finding the ID for MFiles Project " & projectText & ". Please notify Console Support.")
    End Function

    Protected Overrides Sub Finalize()
        ' Destructor
        Try
            _mfiles.Logout()
        Catch logoutError As System.Exception
            'this fails all the time with the current third party tool.
        End Try
    End Sub

End Class

Public Enum MFilesCheckOutStatus
    CheckedIn = 0
    CheckedOut = 1
    CheckedOutToMe = 2
End Enum

