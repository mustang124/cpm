﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.Data.SqlClient
Imports DownloadManagerLibrary
Imports System.Runtime.InteropServices
Imports System.Collections.Generic
Imports SA.Internal.Console.DataObject
Imports iwantedue.Windows.Forms
Imports System.Configuration
Imports PowerService
Imports System.Globalization
Imports System.Threading

Public Class SecureSend

    Public SendIDR As Boolean
    Dim ds As DataSet
    Dim db As DataObject
    Dim DLMLink As String
    Dim FileList As String
    Dim ValidationLinks() As String
    Dim chkFiles(10) As System.Windows.Forms.CheckBox
    Dim fdlg As FileDialog = Nothing
    Dim Emails As New List(Of String)
    Dim EmailsAndNames As New Dictionary(Of String, String)()
    Dim RefineryEmail As String
    Dim AltRefineryEmail As String
    Dim CompanyEmail As String
    Public CompanyContactName As String
    Dim AltCompanyEmail As String
    Dim EmailAddress As String
    Dim PricingEmail As String
    Dim PlantEmail As String
    Dim DCEmail As String
    Dim Coloc As String = ""
    Dim CorrTree As TreeView
    Dim CompanyTree As TreeView
    Dim DrawingTree As TreeView
    Dim CATree As TreeView
    Dim CompCorrTree As TreeView
    Dim PolishCorrTree As TreeView
    Dim PolishCorrTree2 As TreeView
    Dim CorrTree2 As TreeView
    Dim CompanyTree2 As TreeView
    Dim DrawingTree2 As TreeView
    Dim CATree2 As TreeView
    Dim CompCorrTree2 As TreeView
    Dim CTTree As TreeView
    Dim CTTree2 As TreeView
    'Private Err As ErrorLog
    Private mPassword As String
    Dim lstFoundNode As List(Of String)
    Public PCCEmail As String
    Public StudyType As String
    Public IDRFile As String
    Dim strFaxName As String
    Dim strFaxEmail As String
    Dim subj As String = ""
    Dim ConsultantName As String
    Dim FirstName As String = ""
    Dim EmailToPersonName As String = ""

    Private _refineryCorresp2Tree As TreeView '-mfilesTree
    Private Const _delim As Char = Chr(14)
    Private _mfilesAttachmentList As List(Of MfilesAttachment)
    Private mFilesFiles As List(Of String)
    Private _dueDate As Date = Nothing
    Private _powerSvc As PowerManager
    Private curCulture As CultureInfo = Thread.CurrentThread.CurrentCulture
    'Create TextInfo object.
    Private _tInfo As TextInfo = curCulture.TextInfo()

    Private _shortRefNum As String = String.Empty
    Public Property ShortRefNum() As String
        Get
            Return _shortRefNum
        End Get
        Set(value As String)
            _shortRefNum = value
        End Set
    End Property

    Private _longRefNum As String = String.Empty
    Public Property LongRefNum() As String
        Get
            Return _longRefNum
        End Get
        Set(value As String)
            _longRefNum = value
        End Set
    End Property

    Private mCompany As String
    Public Property Company() As String
        Get
            Return mCompany
        End Get
        Set(ByVal value As String)
            mCompany = value
        End Set
    End Property

    Private mCompanyPassword As String
    Public Property CompanyPassword() As String
        Get
            Return mCompanyPassword
        End Get
        Set(ByVal value As String)
            mCompanyPassword = value
        End Set
    End Property
    Private mLoc As String
    Public Property Loc() As String
        Get
            Return mLoc
        End Get
        Set(ByVal value As String)
            mLoc = value
        End Set
    End Property

    Private mUser As String
    Public Property User() As String
        Get
            Return mUser
        End Get
        Set(ByVal value As String)
            mUser = value
        End Set
    End Property

    Private mRefNum As String
    Public Property RefNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property
    'Private mLubeRefNum As String
    'Public Property LubeRefNum() As String
    '    Get
    '        Return mLubeRefNum
    '    End Get
    '    Set(ByVal value As String)
    '        mLubeRefNum = value
    '    End Set
    'End Property

    Private mStudy As String
    Public Property Study() As String
        Get
            Return mStudy
        End Get
        Set(ByVal value As String)
            mStudy = value
        End Set
    End Property
    Private mPolishCorrPath As String
    Public Property PolishCorrPath() As String
        Get
            Return mPolishCorrPath
        End Get
        Set(ByVal value As String)
            mPolishCorrPath = value
        End Set
    End Property

    Private mPolishCorrPath2 As String
    Public Property PolishCorrPath2() As String
        Get
            Return mPolishCorrPath2
        End Get
        Set(ByVal value As String)
            mPolishCorrPath2 = value
        End Set
    End Property

    Private mCorrPath As String
    Public Property CorrPath() As String
        Get
            Return mCorrPath
        End Get
        Set(ByVal value As String)
            mCorrPath = value
        End Set
    End Property

    Private mCompanyPath As String
    Public Property CompanyPath() As String
        Get
            Return mCompanyPath
        End Get
        Set(ByVal value As String)
            mCompanyPath = value
        End Set
    End Property

    Private mDrawingPath As String
    Public Property DrawingPath() As String
        Get
            Return mDrawingPath
        End Get
        Set(ByVal value As String)
            mDrawingPath = value
        End Set
    End Property
    Private mCorrPath2 As String
    Public Property CorrPath2() As String
        Get
            Return mCorrPath2
        End Get
        Set(ByVal value As String)
            mCorrPath2 = value
        End Set
    End Property

    Private mCompanyPath2 As String
    Public Property CompanyPath2() As String
        Get
            Return mCompanyPath2
        End Get
        Set(ByVal value As String)
            mCompanyPath2 = value
        End Set
    End Property

    Private mDrawingPath2 As String
    Public Property DrawingPath2() As String
        Get
            Return mDrawingPath2
        End Get
        Set(ByVal value As String)
            mDrawingPath2 = value
        End Set
    End Property
    Private mTempPath As String
    Public Property TempPath() As String
        Get
            Return mTempPath
        End Get
        Set(ByVal value As String)
            mTempPath = value
        End Set
    End Property

    Private mTemplatePath As String
    Public Property TemplatePath() As String
        Get
            Return mTemplatePath
        End Get
        Set(ByVal value As String)
            mTemplatePath = value
        End Set
    End Property

    Private mClientAttachmentsPath As String
    Public Property ClientAttachmentsPath() As String
        Get
            Return mClientAttachmentsPath
        End Get
        Set(ByVal value As String)
            mClientAttachmentsPath = value
        End Set
    End Property
    Private mClientAttachmentsPath2 As String
    Public Property ClientAttachmentsPath2() As String
        Get
            Return mClientAttachmentsPath2
        End Get
        Set(ByVal value As String)
            mClientAttachmentsPath2 = value
        End Set
    End Property
    Private mClientToolsPath As String
    Public Property ClientToolsPath() As String
        Get
            Return mClientToolsPath
        End Get
        Set(ByVal value As String)
            mClientToolsPath = value
        End Set
    End Property

    Private mCompCorrPath As String
    Public Property CompCorrPath() As String
        Get
            Return mCompCorrPath
        End Get
        Set(ByVal value As String)
            mCompCorrPath = value
        End Set
    End Property
    Private mCompCorrPath2 As String
    Public Property CompCorrPath2() As String
        Get
            Return mCompCorrPath2
        End Get
        Set(ByVal value As String)
            mCompCorrPath2 = value
        End Set
    End Property

    Private mSendMethod As String
    Public Property SendMethod() As String
        Get
            Return mSendMethod
        End Get
        Set(ByVal value As String)
            mSendMethod = value
        End Set
    End Property
    Private mSendTo As String
    Public Property SendTo() As String
        Get
            Return mSendTo
        End Get
        Set(ByVal value As String)
            mSendTo = value
        End Set
    End Property
    Private mSendCC As String
    Public Property SendCC() As String
        Get
            Return mSendCC
        End Get
        Set(ByVal value As String)
            mSendCC = value
        End Set
    End Property
    Private _vertical As VerticalType
    Public Property Vertical() As VerticalType
        Get
            Return _vertical
        End Get
        Set(ByVal value As VerticalType)
            _vertical = value
        End Set
    End Property
    Private _studyYear As String
    Public Property StudyYear() As String '4-digit
        Get
            Return _studyYear
        End Get
        Set(ByVal value As String)
            _studyYear = value
        End Set
    End Property
    'Private _useMfiles As Boolean
    'Public Property UseMFiles() As Boolean
    '    Get
    '        Return _useMfiles
    '    End Get
    '    Set(value As Boolean)
    '        _useMfiles = value
    '    End Set
    'End Property
    Private _fLMacrosPath As String
    Public Property FLMacrosPath As String
        Get
            Return _fLMacrosPath
        End Get
        Set(value As String)
            _fLMacrosPath = value
        End Set
    End Property
    Private _powerSiteId As String
    Public Property PowerSiteId As String
        Get
            Return _powerSiteId
        End Get
        Set(value As String)
            _powerSiteId = value
        End Set
    End Property


    Public Sub Clear()
        Loc = ""
        User = ""
        RefNum = ""
        Study = ""
        CorrPath = ""
        CompanyPath = ""
        DrawingPath = ""
        CorrPath2 = ""
        CompanyPath2 = ""
        DrawingPath2 = ""
        TempPath = ""
        TemplatePath = ""
        ClientAttachmentsPath = ""
        ClientAttachmentsPath2 = ""
        ClientToolsPath = ""
        CompCorrPath = ""
        CompCorrPath2 = ""
        SendMethod = ""
        PolishCorrPath = ""
        PolishCorrPath2 = ""
        ShortRefNum = ""
        LongRefNum = ""
        Vertical = Nothing
    End Sub

    Public Sub New(ByRef dataObject As DataObject, StudyType As String, tvCorr As TreeView, tvDraw As TreeView, tvCA As TreeView, tvCompCorr As TreeView, tvCT As TreeView,
                   tvCorr2 As TreeView, tvDraw2 As TreeView, tvCA2 As TreeView, tvCompCorr2 As TreeView, tvCT2 As TreeView,
                   Optional tvPolishCorr As TreeView = Nothing, Optional tvPolishCorr2 As TreeView = Nothing)
        ' This call is required by the designer.
        InitializeComponent()
        db = dataObject
        CompCorrTree2 = tvCompCorr2
        CorrTree = tvCorr
        DrawingTree = tvDraw
        CATree = tvCA
        CompCorrTree = tvCompCorr
        CorrTree2 = tvCorr
        DrawingTree2 = tvDraw2
        CATree2 = tvCA2
        CompCorrTree2 = tvCompCorr2
        PolishCorrTree = tvPolishCorr
        PolishCorrTree2 = tvPolishCorr2
        CTTree = tvCT
        CTTree2 = tvCT2

        '_mfilesTree = tvCorr2
        _refineryCorresp2Tree = tvCorr2
        'Err = New ErrorLog(StudyType, username, password)
        Coloc = mLoc
        _vertical = ConsoleVertical

        If _vertical = VerticalType.POWER Then
            _powerSvc = New PowerManager(db)
        End If
    End Sub


    Private Function GetCompanyPassword(mRefNum As String) As String
        Dim pw As String = ""

        Dim params = New List(Of String)
        params.Add("RefNum/" + mRefNum)
        ds = db.ExecuteStoredProc("Console." & "GetCompanyPassword", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If _vertical = VerticalType.NGPP Or _vertical = VerticalType.LNG Then
                pw = ds.Tables(0).Rows(0)("CoPassword").ToString.Trim()
            Else
                pw = ds.Tables(0).Rows(0)("CompanyPassword").ToString.Trim()
            End If
        End If

        Return pw
    End Function
    Private Sub SecureSend_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim Interim As Boolean = False
        Dim CompCorrFiles As List(Of String)
        Dim PolishCorrFiles2 As List(Of String) = Nothing
        Dim PolishCorrFiles As List(Of String) = Nothing
        Dim CorrFiles As List(Of String)
        Dim DrawingFiles As List(Of String)
        Dim CAFiles As List(Of String)
        Dim CTFiles As List(Of String)
        Dim CTFiles2 As List(Of String)
        Dim CompCorrFiles2 As New List(Of String)
        Dim CorrFiles2 As List(Of String)
        Dim DrawingFiles2 As New List(Of String)
        Dim CAFiles2 As New List(Of String)
        Dim refineryCorresp2Files As New List(Of String)
        Dim file As String

        Try
            If _vertical <> VerticalType.NGPP And _vertical <> VerticalType.LNG Then
                Utilities.DeleteFiles(TempPath)
            End If

            lstFilesToSend.Items.Clear()
            If IDRFile.Length > 1 Then lstFilesToSend.Items.Add(IDRFile)
            CompCorrFiles = TraverseTree(CompCorrTree, mCompCorrPath)
            CompCorrFiles2 = TraverseTree(CompCorrTree2, mCompCorrPath)   'right side of the tree view
            If Not PolishCorrTree Is Nothing Then
                PolishCorrFiles = TraverseTree(PolishCorrTree, mPolishCorrPath)
                PolishCorrFiles2 = TraverseTree(PolishCorrTree2, mPolishCorrPath2)
            End If
            CorrFiles = TraverseTree(CorrTree, mCorrPath)
            CorrFiles2 = TraverseTree(CorrTree2, mCorrPath) 'right side of the tree view
            DrawingFiles = TraverseTree(DrawingTree, mDrawingPath)
            DrawingFiles2 = TraverseTreeWithMFilesOption(DrawingTree2, mDrawingPath)   'right side of the tree view
            CAFiles = TraverseTree(CATree, mClientAttachmentsPath)
            CAFiles2 = TraverseTree(CATree2, mClientAttachmentsPath)  'right side of the tree view
            CTFiles = TraverseTree(CTTree, mClientToolsPath)
            CTFiles2 = TraverseTree(CTTree2, mClientToolsPath)
            refineryCorresp2Files = TraverseTree(_refineryCorresp2Tree, mCorrPath)
            'If _useMfiles Then
            '    '_RefineryCorresp2Tree
            '    'this never gets used outside of MFIles:
            '    If _refineryCorresp2Tree.Tag = "USE_MFILES" Then ' Or Me.Vertical = VerticalType.OLEFINS Then
            '        TraverseTreeWithMFilesOption(_refineryCorresp2Tree, "")
            '    End If
            '    If Not IsNothing(CompCorrTree2) Then
            '        CompCorrFiles2 = TraverseTreeWithMFilesOption(CompCorrTree2, mCompCorrPath)
            '    End If
            '    DrawingFiles2 = TraverseTreeWithMFilesOption(DrawingTree2, mDrawingPath)
            '    CAFiles2 = TraverseTreeWithMFilesOption(CATree2, mClientAttachmentsPath)
            'End If


            For Each f In CompCorrFiles

                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    FileCopy(f, file)
                    lstFilesToSend.Items.Add(file)
                End If

            Next

            For Each f In CorrFiles
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In refineryCorresp2Files
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            If Not PolishCorrTree Is Nothing Then
                For Each f In PolishCorrFiles
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next

                For Each f In PolishCorrFiles2
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next
            End If

            For Each f In DrawingFiles
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In CAFiles
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In CTFiles
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            If Not CompCorrFiles2 Is Nothing Then
                For Each f In CompCorrFiles2
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next
            End If

            If Not CorrFiles2 Is Nothing Then
                For Each f In CorrFiles2
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next
            End If

            If Not DrawingFiles2 Is Nothing Then
                For Each f In DrawingFiles2
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next
            End If

            If Not CAFiles2 Is Nothing Then
                For Each f In CAFiles2
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next
            End If

            If Not CTFiles2 Is Nothing Then
                For Each f In CTFiles2
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next
            End If

            'mFilesFiles
            'If UseMFiles Then
            '    Dim mfiles As Object
            '    If Not Vertical = VerticalType.POWER Then
            '        mfiles = New DocsMFilesAccess(Vertical, ShortRefNum, LongRefNum)
            '    Else
            '        mfiles = New DocsMFilesAccess(Vertical, PowerSiteId, PowerSiteId)
            '    End If
            '    If Not mFilesFiles Is Nothing Then
            '        Dim attachmentOrder As Integer = 0
            '        For Each f In mFilesFiles
            '            'Utilities.GetLinkPath(f)
            '            Dim nameAndId() As String = f.Split(_delim)
            '            'filePath = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
            '            Dim localFilePath As String = TempPath + nameAndId(0)
            '            If lstFilesToSend.FindString(localFilePath) = -1 Then
            '                If lstFilesToSend.FindString(localFilePath) = -1 Then
            '                    'download file to TempPath instead of:  FileCopy(f, fileName)
            '                    If mfiles.DownloadFile(TempPath, Integer.Parse(nameAndId(1))) Then
            '                        If _mfilesAttachmentList Is Nothing Then
            '                            _mfilesAttachmentList = New List(Of MfilesAttachment)
            '                        End If
            '                        Dim attachment As New MfilesAttachment()
            '                        attachmentOrder += 1
            '                        attachment.ListOrder = attachmentOrder
            '                        attachment.FileName = nameAndId(0)
            '                        attachment.FolderPath = TempPath
            '                        attachment.FileId = Integer.Parse(nameAndId(1))
            '                        _mfilesAttachmentList.Add(attachment)
            '                        lstFilesToSend.Items.Add("MFiles: " + nameAndId(0))
            '                    End If
            '                End If
            '            End If
            '        Next
            '    End If
            'End If

            If Not IsNothing(FLMacrosPath) AndAlso FLMacrosPath.Length > 0 Then
                lstFilesToSend.Items.Add(FLMacrosPath)
            End If

            If Study = "RAM" Then
                txtPassword.Enabled = False
                Label5.Visible = False
            Else
                txtPassword.Enabled = True
                Label5.Visible = True
            End If

            txtResponseDays.Text = "10"
            Me.cboSendMethod.Items.Clear()
            Me.cboSendMethod.Items.Add("DLM - Download Manager")
            Me.cboSendMethod.Items.Add("PPZ - Encrypted Zip File")
            Me.cboSendMethod.Items.Add("PPF - Encrypt Each File")

            cboSendMethod.SelectedText = "PPF - Encrypt Each File"
            mSendMethod = "PPF - Encrypt Each File"
            Dim params = New List(Of String)

            If _vertical = VerticalType.POWER Then
                'get refunums, send 1st to GetCompanyContactInfo.
                Dim refnums As List(Of String) = _powerSvc.GetRefNums(_powerSiteId, StudyYear)
                If refnums.Count > 0 Then
                    Dim coContact As SA.Console.Common.Contact = _powerSvc.GetCompanyContactInfo(_powerSiteId, StudyYear, "COORD")
                    txtEmail.Text = coContact.Email
                    'txtCC.Text = coContact.
                End If
                txtPassword.Text = _powerSvc.GetCompanyPassword(_powerSiteId, StudyYear)
            Else
                params.Clear()
                params.Add("RefNum/" + mRefNum)
                Dim nRow As DataRow
                Dim AddtoEmails As Boolean
                ds = db.ExecuteStoredProc("Console.GetContactEmails", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then Emails.Add(ds.Tables(0).Rows(0)("EmailType").ToString.ToUpper + " - " + ds.Tables(0).Rows(0)("Email").ToString.ToUpper)
                    If Vertical = VerticalType.OLEFINS Or Vertical = VerticalType.CQM Then
                        Emails.Clear()
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            nRow = ds.Tables(0).Rows(i)
                            AddtoEmails = True
                            'Dim role As String = mail.Split(" - ")(0).Trim()
                            Dim emailAddress As String = String.Empty
                            If Not IsDBNull(nRow("Email")) Then
                                emailAddress = nRow("Email").ToString()
                            End If
                            If emailAddress.Length > 0 Then

                                If Not nRow("EmailType").ToString().ToUpper().StartsWith("COORD") Then ')(nRow("Email").ToString.ToUpper) Then AddtoEmails = False
                                    AddtoEmails = False
                                Else
                                    For Each mail In Emails
                                        If mail.ToUpper().Trim() = emailAddress.ToUpper.Trim() Then
                                            AddtoEmails = False
                                        End If
                                    Next
                                End If
                                'If AddtoEmails Then Emails.Add(nRow("EmailType").ToString.ToUpper + " - " + nRow("Email").ToString.ToUpper)
                                If AddtoEmails Then Emails.Add(emailAddress.Trim().ToUpper())
                            End If
                        Next
                        For i As Integer = 0 To Emails.Count - 1
                            CompanyEmail = CompanyEmail & Emails(i) & ";"
                        Next

                    Else
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            nRow = ds.Tables(0).Rows(i)
                            AddtoEmails = True
                            For Each mail In Emails
                                If mail.Contains(nRow("Email").ToString.ToUpper) Then AddtoEmails = False
                            Next
                            If AddtoEmails Then Emails.Add(nRow("EmailType").ToString.ToUpper + " - " + nRow("Email").ToString.ToUpper)
                            'add all to the dictionary
                            If EmailsAndNames.Count > 0 Then
                                If Not EmailsAndNames.ContainsKey(_tInfo.ToTitleCase(nRow("Email").ToString.ToLower)) Then
                                    EmailsAndNames.Add(_tInfo.ToTitleCase(nRow("Email").ToString.ToLower), _tInfo.ToTitleCase(nRow("Name").ToString.ToLower))
                                End If
                            Else
                                EmailsAndNames.Add(_tInfo.ToTitleCase(nRow("Email").ToString.ToLower), _tInfo.ToTitleCase(nRow("Name").ToString.ToLower))
                            End If
                        Next

                    End If

                    If Study = "RAM" Then
                        txtPassword.Text = GetCompanyPassword(Company)
                    Else
                        txtPassword.Text = GetCompanyPassword(RefNum)
                    End If

                    If Emails.Count > 0 Then
                        For Each em In Emails
                            If _vertical = VerticalType.REFINING Then
                                If (em.ToUpper.StartsWith("REFINERY") And em.Contains("@")) Then
                                    RefineryEmail += StripEmail(em) & ";"
                                End If
                                If (em.ToUpper.StartsWith("ALT REFINERY") And em.Contains("@")) Then
                                    AltRefineryEmail += StripEmail(em) & ";"
                                End If
                                If (em.ToUpper.StartsWith("COMPANY") And em.Contains("@")) Then
                                    CompanyEmail += StripEmail(em) & ";"
                                End If

                            Else
                                If em.ToUpper.Contains("ALT PIPELINE") And em.Contains("@") Then
                                    AltRefineryEmail += StripEmail(em) & ";"
                                Else
                                    If em.ToUpper.Contains("PIPELINE") And em.Contains("@") Then
                                        RefineryEmail += StripEmail(em) & ";"
                                    End If
                                End If

                                If em.ToUpper.Contains("ALT REFINERY") And em.Contains("@") Then
                                    AltRefineryEmail += StripEmail(em) & ";"
                                Else
                                    If (em.ToUpper.Contains("REFINERY") Or em.ToUpper.Contains("SITE")) And em.Contains("@") Then
                                        RefineryEmail += StripEmail(em) & ";"
                                    End If
                                End If

                                If em.ToUpper.Contains("ALT COMPANY") And em.Contains("@") Then
                                    AltCompanyEmail += StripEmail(em) & ";"
                                Else

                                    If em.ToUpper.Contains("COMPANY") And em.Contains("@") And Not Interim Then
                                        CompanyEmail += StripEmail(em) & ";"
                                    End If
                                End If

                                If em.ToUpper.Contains("PLANT") And em.Contains("@") Then
                                    PlantEmail += StripEmail(em) & ";"
                                End If
                                If em.ToUpper.Contains("PRICING") And em.Contains("@") Then
                                    PricingEmail += StripEmail(em) & ";"
                                End If
                                If em.ToUpper.Contains("DC") And em.Contains("@") Then
                                    DCEmail += StripEmail(em) & ";"
                                End If
                            End If
                        Next


                        If Study.ToUpper = "NGPP" Or Study.ToUpper = "LNG" Or _vertical = VerticalType.REFINING Or Study.ToUpper = "RAM" Or Study.ToUpper = "PIPELINES" Or Study.ToUpper = "TERMINALS" Then


                            If RefineryEmail Is Nothing Then
                                txtEmail.Text = IIf(CompanyEmail = Nothing, "", CompanyEmail)
                                txtCC.Text = IIf(AltCompanyEmail = Nothing, "", AltCompanyEmail) & IIf(AltRefineryEmail = Nothing, "", AltRefineryEmail)
                            Else
                                txtEmail.Text = IIf(RefineryEmail = Nothing, "", RefineryEmail)
                                txtCC.Text = IIf(CompanyEmail = Nothing, "", CompanyEmail) & IIf(AltRefineryEmail = Nothing, "", AltRefineryEmail)
                            End If
                            EmailToPersonName = _tInfo.ToTitleCase(EmailsAndNames.Item(Replace(_tInfo.ToTitleCase(txtEmail.Text.ToLower), ";", "")))
                            If RefineryEmail = "" And AltRefineryEmail = "" And CompanyEmail = "" Then
                                MessageBox.Show("No contact email found. Please add one manually.")
                                'Me.Close()
                            End If

                        ElseIf _vertical = VerticalType.OLEFINS Or _vertical = VerticalType.CQM Then
                            Dim emailTo As String = PlantEmail
                            If Not HasLength(emailTo) Then
                                emailTo = AltRefineryEmail
                            End If

                            If Not HasLength(emailTo) AndAlso Not HasLength(CompanyEmail) Then
                                MessageBox.Show("No contact email found. Please add one manually.")
                                'Me.Close()
                            Else
                                If HasLength(emailTo) Then
                                    txtEmail.Text = emailTo
                                    txtCC.Text = PricingEmail + DCEmail + CompanyEmail
                                ElseIf HasLength(CompanyEmail) Then
                                    txtEmail.Text = CompanyEmail
                                    txtCC.Text = PricingEmail + DCEmail
                                End If
                            End If
                        Else
                            txtEmail.Text = PlantEmail
                            txtCC.Text = PricingEmail + DCEmail + CompanyEmail
                        End If
                    End If
                End If
            End If

            GetTemplates(mSendMethod)
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
            db.WriteLog("SecureSend Load", ex)
            CloseSS()
        End Try

    End Sub

    Private Function HasLength(emailType As String) As Boolean
        If IsNothing(emailType) Then
            Return False
        End If
        If emailType.Length < 1 Then
            Return False
        End If
        Return True
    End Function

    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function
    Private Function GetCompanyName(str As String) As String
        Dim CompanyName As String
        ''Get Company Name
        If str = "" Then Return ""
        Dim FirstDash As Integer = str.IndexOf("-")
        Dim SecondDash As Integer = str.IndexOf("-", FirstDash + 1)

        CompanyName = str.Substring(0, FirstDash - 1)

        Return CompanyName

    End Function


    Private Sub GetTemplates(sendMethod As String)
        Try
            cboTemplate.Items.Clear()

            If sendMethod = "DLM - Download Manager" Then
                Dim di As New IO.DirectoryInfo(TemplatePath)
                Dim diar1 As IO.FileInfo() = di.GetFiles("*DLM.txt")
                Dim dra As IO.FileInfo

                'list the names of all files in the specified directory
                For Each dra In diar1
                    cboTemplate.Items.Add(dra.ToString)
                Next

            Else
                Dim di As New IO.DirectoryInfo(TemplatePath)
                If Vertical = VerticalType.NGPP Or Vertical = VerticalType.LNG Then
                    Dim path As String = "\\dallas2\Data\Data\Study\" & Study & "\" & StudyYear & "\Software\IDR\Template\"
                    di = New IO.DirectoryInfo(path)
                Else
                    di = New IO.DirectoryInfo(TemplatePath)
                End If
                If File.Exists(di.FullName + "valfax.txt") Then File.Delete(di.FullName + "valfax.txt")

                Dim diar1 As IO.FileInfo() = di.GetFiles("*.txt")
                Dim dra As IO.FileInfo

                'list the names of all files in the specified directory
                For Each dra In diar1
                    If Not dra.ToString.Contains("DLM") And Not dra.ToString.Contains("Notification of Transmittal") Then
                        cboTemplate.Items.Add(dra.ToString)
                    End If
                Next

            End If
            If _vertical = VerticalType.REFINING Then
                If Utilities.GetRefNumPart(RefNum, 2) = "LUB" Then

                    cboTemplate.SelectedItem = "Lube Email Transmittal.txt"
                Else

                    cboTemplate.SelectedItem = "Fuels Email Transmittal.txt"
                End If
            End If

        Catch ex As System.Exception
            db.WriteLog("SecureSend-GetTemplates", ex)
        End Try
    End Sub
    Private Function getColoc() As String
        Dim params = New List(Of String)

        If Vertical = VerticalType.OLEFINS Or Vertical = VerticalType.CQM Or Vertical = VerticalType.SEEC Then
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console.GetTSortData", params)
        ElseIf Vertical = VerticalType.REFINING Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console.GetTSortData", params)
        ElseIf Vertical = VerticalType.POWER Then
            params = New List(Of String)
            ' Consultant Name
            params.Add("SiteId/" + PowerSiteId)
            ds = db.ExecuteStoredProc("Console.GetTSortDataBySiteId", params)
        Else
            'SB: there doesn't seem to be 2 args but this code was here when I first looked at it so I'm leaving it alone
            ds = db.ExecuteStoredProc("Console.GetTSortData", params)
        End If

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("Coloc").ToString().Trim
            End If
        End If
        Return String.Empty
    End Function

    Private Function FillTemplate(template As String) As String

        Dim fileContents As String = Nothing
        Dim DoubleCheck As String = ""

        Dim SiteName As String = ""


        Dim consultantEmail As String = String.Empty

        Try
            Dim params = New List(Of String)
            ' Consultant Name
            params.Add("Initials/" + mUser)
            If _vertical = VerticalType.NGPP Or _vertical = VerticalType.LNG Then
                params.Add("StudyYear/" + StudyYear)
                params.Add("StudyType/NGPP")
                ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
                params.Clear() 'clear for reuse
                ElseIf _vertical = VerticalType.POWER Then
                    params = New List(Of String)
                    ' Consultant Name
                    params.Add("SiteId/" + PowerSiteId)
                    ds = db.ExecuteStoredProc("Console." & "GetConsultantBySiteId", params)
                Else
                    ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
            End If


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Vertical = VerticalType.OLEFINS Or Vertical = VerticalType.CQM Then
                        ConsultantName = ds.Tables(0).Rows(0).Item("EmployeeName").ToString().Trim()
                        consultantEmail = User & "@solomononline.com"
                    ElseIf Vertical = VerticalType.POWER Then
                        ConsultantName = ds.Tables(0).Rows(0).Item("ConsultantName").ToString().Trim()
                        Dim sql As String = "SELECT Consultant  FROM dbo.StudySites where SiteID ='" & PowerSiteId & "'"
                        db.SQL = sql
                        Try
                            ds = db.Execute
                        Catch
                        End Try
                        db.SQL = String.Empty
                        consultantEmail = ds.Tables(0).Rows(0).Item("Consultant").ToString().Trim()
                    Else
                        ConsultantName = ds.Tables(0).Rows(0).Item("ConsultantName").ToString().Trim()
                        consultantEmail = ds.Tables(0).Rows(0).Item("email").ToString()
                    End If
                End If
            End If

            params.Add("RefNum/" + RefNum)

            If mStudy <> "RAM" Then
                If Vertical = VerticalType.OLEFINS Or Vertical = VerticalType.CQM Then
                    Dim getTSortDataParams = New List(Of String)
                    getTSortDataParams.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console.GetTSortData", getTSortDataParams)
                ElseIf Vertical = VerticalType.REFINING Or Vertical = VerticalType.NGPP Or Vertical = VerticalType.LNG Then
                    params = New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console.GetTSortData", params)
                ElseIf Vertical = VerticalType.POWER Then
                    params = New List(Of String)
                    ' Consultant Name
                    params.Add("SiteId/" + PowerSiteId)
                    ds = db.ExecuteStoredProc("Console.GetTSortDataBySiteId", params)
                Else
                    'SB: there doesn't seem to be 2 args but this code was here when I first looked at it so I'm leaving it alone
                    ds = db.ExecuteStoredProc("Console.GetTSortData", params)
                End If

                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Coloc = ds.Tables(0).Rows(0)("Coloc").ToString().Trim
                    End If
                End If
            Else
                ds = db.ExecuteStoredProc("Console.GetCompanyContactInfo", params)
                If ds.Tables.Count > 0 Then
                    For Each Row In ds.Tables(0).Rows
                        If Not FirstName.Contains(Row("First Name").ToString().Trim & " " & Row("Last Name").ToString().Trim) Then FirstName += Row("First Name").ToString().Trim & " " & Row("Last Name").ToString().Trim + ", "
                    Next
                End If

                ds = db.ExecuteStoredProc("Console.GetSiteInfo", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        SiteName = ds.Tables(0).Rows(0)("SiteName").ToString().Trim
                    End If
                End If
            End If

            If template <> "" Then
                If File.Exists(TemplatePath & template) Then
                    fileContents = My.Computer.FileSystem.ReadAllText(TemplatePath & template)
                    fileContents = fileContents.Replace("~Site~", SiteName)
                    fileContents = fileContents.Replace("~RefName~", getColoc())

                    If _vertical = VerticalType.POWER Or _vertical = VerticalType.REFINING Or
                    _vertical = VerticalType.OLEFINS Or _vertical = VerticalType.CQM Then
                        fileContents = fileContents.Replace("~DueDate~", _dueDate.ToString("dd-MMM-yy"))
                    Else
                        fileContents = fileContents.Replace("~DueDate~", Today.ToString("dd-MMM-yy"))
                    End If

                    If consultantEmail Is Nothing Or consultantEmail = "" Then
                        fileContents = fileContents.Replace("~UserID~", Trim(User))
                    Else
                        fileContents = fileContents.Replace("~UserID~", Trim(consultantEmail))  'was var ConsultantName
                    End If

                    fileContents = fileContents.Replace("~FirstName~", Trim(FirstName))

                    'power only:
                    fileContents = fileContents.Replace("~Greeting~", "Dear " & Trim(EmailToPersonName) & ",")


                    If _vertical = VerticalType.OLEFINS Or _vertical = VerticalType.CQM Then
                        'change this to use “~PlantContact~” = 
                        'select CoordName field from stgFact.TSort where Refnum = '" + shortRefNum + '="'"
                        fileContents = fileContents.Replace("~PlantContact~", Trim(CompanyContactName))
                    End If
                End If
            End If
        Catch ex As System.Exception
            db.WriteLog("SecureSend-FillTemplate", ex)
        End Try
        Return fileContents
    End Function
    Private Sub CopyToTempDir()

        Dim f As String
        Dim start As Integer
        Utilities.DeleteFiles(TempPath)
        Try
            For i = 0 To lstFilesToSend.Items.Count - 1
                start = lstFilesToSend.Items(i).ToString.LastIndexOf("\") + 1
                f = lstFilesToSend.Items(i).ToString.Substring(start, lstFilesToSend.Items(i).ToString.Length - start)
                File.Copy(lstFilesToSend.Items(i), mTempPath & f)
            Next
        Catch
        End Try

    End Sub
    Private Function GetExtension(strFile As String)
        'this should be changed from hardcoded 3; we have docs with 4-char extensions
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, 3)
        Catch
            ext = ""
        End Try
        Return ext
    End Function
    Private Sub SendEmail()

        Dim Zip As Boolean = False
        Dim success As Boolean = True
        Dim answer As DialogResult
        Dim mfile As String
        Dim fileExt As String
        Dim i As Integer = 0

        Dim ext As String
        If txtEmail.Text = "" Then
            MessageBox.Show("Must Enter an Email Address")
            Exit Sub
        End If
        'check the network files for .doc, .xls
        For i = 0 To lstFilesToSend.Items.Count - 1
            If Not lstFilesToSend.Items(i).ToString().StartsWith("MFiles:") Then
                'If _vertical = VerticalType.REFINING Then
                Dim docExt As String = GetExtension(lstFilesToSend.Items(i).ToString()).ToUpper()
                If Not docExt.ToUpper().StartsWith("DOC") And Not docExt.ToUpper().StartsWith("XLS") Then
                    Zip = True
                    Exit For
                End If
                '    End If
                'Else
                '    If GetExtension(lstFilesToSend.Items(i).ToString()).ToUpper() <> "DOC" And GetExtension(lstFilesToSend.Items(i).ToString()).ToUpper() <> "XLS" Then
                '        Zip = True
                '        Exit For
                '    End If
            End If
        Next
        'check the Mfiles for .doc, .xls
        Dim mfilesAttachmentListCount As Integer = 1
        For i = 0 To lstFilesToSend.Items.Count - 1
            If lstFilesToSend.Items(i).ToString().StartsWith("MFiles:") Then
                For Each attachment As MfilesAttachment In _mfilesAttachmentList
                    If attachment.ListOrder = mfilesAttachmentListCount Then
                        mfilesAttachmentListCount += 1
                        If _vertical = VerticalType.REFINING Then
                            Dim docExt As String = GetExtension(lstFilesToSend.Items(i).ToString()).ToUpper()
                            If Not docExt.ToUpper().StartsWith("DOC") And Not docExt.ToUpper().StartsWith("XLS") Then
                                'If GetExtension(attachment.FileName.ToUpper()) <> "DOC" And GetExtension(attachment.FileName.ToUpper()) <> "XLS" Then
                                Zip = True
                                Exit For
                            End If
                        Else
                            If GetExtension(attachment.FileName.ToUpper()) <> "DOC" And GetExtension(attachment.FileName.ToUpper()) <> "XLS" Then
                                Zip = True
                                Exit For
                            End If
                        End If

                    End If
                Next
            End If
        Next

        'If _vertical = VerticalType.REFINING Then
        If Zip And (cboSendMethod.Text.Substring(0, 3) = "PPF") Then
            Dim rtn As DialogResult = MessageBox.Show("It is Solomon's policy to not send unencrypted file via email.  You are attempting to send a file that cannot be encrypted.  Do you wish to send an encrypted ZIP file instead?", "Wait", MessageBoxButtons.YesNo)
            If rtn = System.Windows.Forms.DialogResult.Yes Then
                cboSendMethod.SelectedIndex = 1
            Else
                Exit Sub
            End If
        End If
        'Else
        'If Zip Then
        '    Dim rtn As DialogResult = MessageBox.Show("It is Solomon's policy to not send unencrypted file via email.  You are attempting to send a file that cannot be encrypted.  Do you wish to send an encrypted ZIP file instead?", "Wait", MessageBoxButtons.YesNo)
        '    If rtn = System.Windows.Forms.DialogResult.Yes Then cboSendMethod.SelectedIndex = 1
        'End If
        'End If

        If cboSendMethod.Text.Trim().Length < 1 Then
            MsgBox("Please enter a Send Method")
            Exit Sub
        End If
        If cboTemplate.Text = "" Then
            MsgBox("Please choose a template")
            Exit Sub
        Else

            Select Case cboSendMethod.Text.Substring(0, 3)

                Case "PPF"
                    subj = My.Computer.FileSystem.OpenTextFileReader(TemplatePath & cboTemplate.Text).ReadLine
                    Try
                        Dim mfileAttachmentCount As Integer = 1
                        For i = 0 To lstFilesToSend.Items.Count - 1
                            Dim fileName As String = lstFilesToSend.Items(i).ToString
                            If Not fileName.StartsWith("MFiles:") Then
                                If Not CompanyPasswordRoutine(fileName, Nothing) Then
                                    Exit Sub
                                End If
                            Else
                                For Each attachment As MfilesAttachment In _mfilesAttachmentList
                                    If attachment.ListOrder = mfileAttachmentCount Then
                                        mfileAttachmentCount += 1
                                        If Not CompanyPasswordRoutine("", attachment) Then
                                            Exit Sub
                                        End If
                                        Exit For
                                    End If
                                Next
                            End If

                            'If 1 = 2 Then
                            '    'replacing the below with CompanyPasswordRoutine()
                            '    mfile = lstFilesToSend.Items(i).ToString
                            '    ext = mfile.Substring(mfile.LastIndexOf(".") + 1, mfile.Length - mfile.LastIndexOf(".") - 1)
                            '    fileExt = ext.ToUpper.Substring(0, 3)

                            '    If txtPassword.Text.Trim.Length = 0 And (fileExt = "XLS" Or fileExt = "DOC") Then
                            '        MessageBox.Show("Company password is missing.  Must include a password for Excel or Word documents with PPF Send Method")
                            '        txtPassword.ReadOnly = False
                            '        txtPassword.Enabled = True
                            '        txtPassword.Focus()
                            '        Exit Sub
                            '    Else
                            '        txtPassword.ReadOnly = True

                            '        Select Case fileExt
                            '            Case "XLS"
                            '                success = Utilities.ExcelPassword(mfile, txtPassword.Text.Trim)
                            '            Case "DOC", "OCX"
                            '                If Not mfile.ToUpper.Contains("INSTRUCTIONS") Then
                            '                    success = Utilities.WordPassword(mfile, txtPassword.Text.Trim)
                            '                Else
                            '                    success = True
                            '                End If

                            '        End Select

                            '        If Not success Then
                            '            MessageBox.Show("An Error has occurred in the packaging of the file: " & mfile & ".  Please make sure all files exist and are of the correct type.")
                            '            Exit Sub
                            '        End If
                            '    End If
                            'End If
                        Next

                        ''Attach all the files in the TempDir
                        Dim attachments As New List(Of String)
                        Dim mfileAttacCount As Integer = 1
                        For i = 0 To lstFilesToSend.Items.Count - 1
                            If Not lstFilesToSend.Items(i).ToString().StartsWith("MFiles:") Then
                                attachments.Add(lstFilesToSend.Items(i).ToString)
                            Else
                                For Each attachment As MfilesAttachment In _mfilesAttachmentList
                                    If attachment.ListOrder = mfileAttacCount Then
                                        mfileAttacCount += 1
                                        attachments.Add(attachment.FolderPath + attachment.FileName)
                                        Exit For
                                    End If
                                Next
                            End If
                        Next
                        If subj.Length = 0 Then subj = Study & " Study Correspondence for " & getColoc()
                        'If lstFilesToSend.Items.Count > 0 And _vertical <> VerticalType.LNG Then SendOtherEmail("Notification of Transmittal - " & subj, "", True, "Notification of Transmittal.txt")
                        If lstFilesToSend.Items.Count > 0 Then SendOtherEmail("Notification of Transmittal - " & subj, "", True, "Notification of Transmittal.txt")
                        SendOtherEmail(subj, "", True, cboTemplate.Text, attachments)

                    Catch ex As System.Exception
                        db.WriteLog("SecureSend Load", ex)

                        MessageBox.Show("SecureSend Load error: " + ex.Message, "Send Email")
                    End Try

                Case "DLM"
                    Dim body As String
                    'Attach all the files in the TempDir
                    Dim zipfile As String = ZipFiles()
                    body = "Files link: " & DLM_Link(zipfile)
                    If _vertical = VerticalType.REFINING And LongRefNum.Contains("LUB") Then
                        If subj.Length = 0 Then subj = Study & " Study Correspondence for " & getColoc()
                    Else
                        If subj.Length = 0 Then subj = Study & " Study Correspondence for " & RefNum
                    End If


                    SendDLMEmail(subj, body, True, cboTemplate.Text)

                Case "PPZ"
                    Dim zipfile As List(Of String) = New List(Of String)
                    Dim nZip As String = ZipFiles()
                    If nZip.Length = 0 Then Exit Select
                    zipfile.Add(nZip)

                    If _vertical = VerticalType.POWER Then
                        If subj.Length = 0 Then subj = Study & " Study Correspondence for " & mLoc
                    Else
                        If subj.Length = 0 Then subj = Study & " Study Correspondence for " & getColoc()
                    End If
                    If lstFilesToSend.Items.Count > 0 Then
                        'SendOtherEmail("Notification of Transmittal - " & subj, "", True, "Notification of Transmittal.txt")
                        SendOtherEmail("Notification of Transmittal - " & subj, "", True, cboTemplate.Text)
                    End If
                    SendOtherEmail(subj, "", True, cboTemplate.Text, zipfile)

            End Select

            Dim folder As New DirectoryInfo(TempPath)
            Dim files As FileInfo() = folder.GetFiles()
            For fileCount As Integer = 0 To files.Length - 1
                Try
                    files(fileCount).Delete()
                Catch
                End Try
            Next
            If Directory.Exists(TempPath & "ZIP") Then
                folder = New DirectoryInfo(TempPath & "ZIP")
                files = folder.GetFiles()
                For fileCount As Integer = 0 To files.Length - 1
                    Try
                        files(fileCount).Delete()
                    Catch
                    End Try
                Next
            End If
            CloseSS()
        End If

    End Sub
    Private Sub CloseSS()
        Clear()
        Me.Close()
    End Sub

    Private Function CompanyPasswordRoutine(Optional lstFilesToSend As String = "", Optional mfilesAttachment As MfilesAttachment = Nothing) As Boolean
        Try
            Dim success As Boolean = True 'this is how the code was before I moved it here = defaults to true
            If lstFilesToSend = "" And Not IsNothing(mfilesAttachment) Then
                lstFilesToSend = mfilesAttachment.FolderPath + mfilesAttachment.FileName
            End If
            Dim ext As String = lstFilesToSend.Substring(lstFilesToSend.LastIndexOf(".") + 1, lstFilesToSend.Length - lstFilesToSend.LastIndexOf(".") - 1)
            Dim fileExt As String = ext.ToUpper.Substring(0, 3)

            If txtPassword.Text.Trim.Length = 0 And (fileExt = "XLS" Or fileExt = "DOC") Then
                MessageBox.Show("Company password is missing.  Must include a password for Excel or Word documents with PPF Send Method")
                txtPassword.ReadOnly = False
                txtPassword.Enabled = True
                txtPassword.Focus()
                Return False
            Else
                txtPassword.ReadOnly = True
                Select Case fileExt
                    Case "XLS"
                        success = Utilities.ExcelPassword(lstFilesToSend, txtPassword.Text.Trim)
                    Case "DOC", "OCX"
                        If Not lstFilesToSend.ToUpper.Contains("INSTRUCTIONS") Then
                            success = Utilities.WordPassword(lstFilesToSend, txtPassword.Text.Trim)
                        Else
                            success = True
                        End If
                End Select

                If Not success Then
                    MessageBox.Show("An Error has occurred in the packaging of the file: " & lstFilesToSend & ".  Please make sure all files exist and are of the correct type.")
                    Return False
                End If
            End If
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function


    Private Function BuildIDREmail(mfile As String) As String
        Dim TemplateValues As New SA.Console.Common.WordTemplate()
        Dim IDRConsultantName As String


        TemplateValues.Field.Add("_TodaysDate")
        TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))


        Dim Row As DataRow
        Dim params As List(Of String)
        ' Company Name
        If _vertical <> VerticalType.POWER Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)
        Else
            params = New List(Of String)
            params.Add("@SiteId/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortDataBySiteId", params)
        End If
        If ds.Tables.Count > 0 Then
            Row = ds.Tables(0).Rows(0)

            TemplateValues.Field.Add("_Company")
            TemplateValues.RField.Add(Row("Company").ToString)


            TemplateValues.Field.Add("_Refinery")
            TemplateValues.RField.Add(Row("Location").ToString)
            Coloc = Row("Coloc").ToString

        End If




        ' Contact Name
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
        If ds.Tables.Count = 0 Then
            strFaxName = "NAME UNAVAILABLE"
            strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
        Else
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)
                strFaxName = Row("CoordName").ToString
                strFaxEmail = Row("CoordEmail").ToString
            End If
        End If



        TemplateValues.Field.Add("_Contact")
        TemplateValues.RField.Add(strFaxName)
        ' Contact Email Address

        TemplateValues.Field.Add("_EMail")
        TemplateValues.RField.Add(strFaxEmail)

        ' Consultant Nant
        params = New List(Of String)
        If _vertical <> VerticalType.POWER Then
            params.Add("Initials/" + mUser)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
        Else
            params.Add("SiteId/" + PowerSiteId)
            ds = db.ExecuteStoredProc("Console." & "GetConsultantBySiteId", params)
        End If

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)

                If Row("ConsultantName") Is Nothing Or IsDBNull(Row("ConsultantName")) Then
                    IDRConsultantName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                Else
                    IDRConsultantName = Row("ConsultantName").ToString
                End If
            Else

                IDRConsultantName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
            End If
        End If


        TemplateValues.Field.Add("_ConsultantName")
        TemplateValues.RField.Add(IDRConsultantName)
        ' Consultant Initials

        TemplateValues.Field.Add("_Initials")

        TemplateValues.RField.Add(mUser)
        Utilities.WordTemplateReplace(TemplatePath & mfile, TemplateValues, TempPath & mfile, 0)
        Return TempPath & mfile
    End Function

    Private Sub SendDLMEmail(subject As String, nbody As String, display As Boolean, Optional template As String = Nothing, Optional attaches As List(Of String) = Nothing)

        If _vertical <> VerticalType.REFINING Then Exit Sub ' not tested on other verticals

        Dim body As System.Text.StringBuilder = New System.Text.StringBuilder()
        Dim consultantName As String, consultantFormalEmail As String
        consultantName = GetConsultantName(User, consultantFormalEmail)
        Try
            If _vertical = VerticalType.REFINING And LongRefNum.Contains("LUB") Then
                'body.Append("Dear " & CompanyContactName & "," & Environment.NewLine & Environment.NewLine)
                body.Append("Dear " & EmailToPersonName & "," & Environment.NewLine & Environment.NewLine)
                body.Append("Input Data Review Questions Transmittal Notification - ~RefName~" & Environment.NewLine & Environment.NewLine)
                body.Append("Questions on the Input Data for the " & StudyYear & " Lube Refinery Study" & Environment.NewLine & Environment.NewLine)
                body.Append("On behalf of HSB Solomon Associates LLC (Solomon), we would like to thank you for your input data submission to the Lube Refinery Performance Analysis (Lube Study) for operating year " & StudyYear & ". Please extend our thanks to your team for their time and effort to assemble and report the refinery information." & Environment.NewLine & Environment.NewLine)
                body.Append("We reviewed your input data and have noted various items and questions we would like to review with you. Please ensure that your answers arrive in our office by ~DueDate~." & Environment.NewLine & Environment.NewLine)
                body.Append("Please note the items attached for your attention and action. The Word file contains our questions about your original input data submission in each of the Input Spreadsheet Tables. Another attached file is the Excel file that contain the example second-level DataChecks as some extra independent guidance that we provide to clarify our questions so that you can view specifically what we reviewed." & Environment.NewLine & Environment.NewLine)
                body.Append("The individual files have been secured using the " & StudyYear & " study password provided by your company to us. If you do not have the password, please contact your company coordinator for the password. Please review all these attachments carefully before revising the data and submitting revisions via generation of another Return.xls file." & Environment.NewLine & Environment.NewLine)
                body.Append("ACTION: For tracking purposes, please email me at " & consultantFormalEmail & "@SolomonOnline.com indicating that you received this document." & Environment.NewLine & Environment.NewLine)
                body.Append("If we do not receive confirmation within twenty-four hours, we will contact you by email, phone, and/or fax to make sure that you have received it. If you plan to be out of the office for an extended period, please inform us so that we may send these communications to an appropriate back up." & Environment.NewLine & Environment.NewLine)
                body.Append("In order for us to adhere to the project schedule, it is important that you respond in a timely fashion to these questions. Thank you for your acknowledgment of the receipt of this transmittal and for your on-time response to these questions. If you have any questions regarding the material covered in the documents, please contact me." & Environment.NewLine & Environment.NewLine)
            ElseIf (Not _vertical = VerticalType.OLEFINS) And (Not _vertical = VerticalType.CQM) And (Not _vertical = VerticalType.NGPP) And (Not _vertical = VerticalType.LNG) Then
                body.Append("Dear " & EmailToPersonName & "," & vbCrLf & vbCrLf)
            End If
            body.Append("DOWNLOAD INSTRUCTIONS" & Environment.NewLine & Environment.NewLine)
            body.Append("NOTE:  This file can only be downloaded ONCE for security purposes.  You will need to distribute the appropriate files to your refineries after downloading the information." & Environment.NewLine & Environment.NewLine)
            body.Append("(1) Click your company's unique link below or type it into the address line of your web browser:" & Environment.NewLine & Environment.NewLine)
            body.Append(nbody & Environment.NewLine & Environment.NewLine) '
            body.Append("(2) A blank screen will appear with a box for your password.  Type your company password and click 'Enter'.  Please remember the password is case-sensitive." & Environment.NewLine & Environment.NewLine)
            body.Append("(3) A File Download dialog box will appear.  Click 'Save' and navigate to your desired directory to download your encrypted results file.  After a successful download, your file will be removed from our server." & Environment.NewLine & Environment.NewLine)
            body.Append("(4) You will need to use your company password to unzip your .zip file after it has been downloaded to your local system.  Use the same case-sensitive company password you used to access the download site." & Environment.NewLine & Environment.NewLine)
            body.Append("(5) Please confirm by reply e-mail your successful download and receipt of this file." & Environment.NewLine & Environment.NewLine)
            body.Append("Regards, " & Environment.NewLine & Environment.NewLine)
            body.Append(consultantName)

            'If nbody.Contains("Files link:") Then
            '    body += Environment.NewLine + nbody
            'End If

            subject = subject.Replace("~RefName~", mLoc)
            subject = subject.Replace("~DueDate~", _dueDate.ToString("dd-MMM-yyyy"))
            subject = subject.Replace("~UserID~", User)

            Dim finalBody As String = body.ToString()
            finalBody = finalBody.Replace("~RefName~", mLoc)
            finalBody = finalBody.Replace("~DueDate~", _dueDate.ToString("dd-MMM-yyyy"))
            finalBody = finalBody.Replace("~UserID~", User)

            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    attaches(i) = attaches(i).ToString
                Next
            End If
            If (Not subject.Contains("Notification of Transmittal")) AndAlso (_vertical <> VerticalType.NGPP) AndAlso (_vertical <> VerticalType.LNG) Then
                Utilities.SendEmail(txtEmail.Text, txtCC.Text + IIf(chkPCC.Checked, txtPCC.Text, ""), subject, finalBody, attaches, display)
            Else
                Utilities.SendEmail(txtEmail.Text, "", subject, finalBody, attaches, display)
            End If

            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    File.Delete(attaches(i).ToString)
                Next
            End If
            'If chkSaveCopy.Checked And Not subject.Contains("Notification of Transmittal") Then
            '    Dim strw As New StreamWriter(CorrPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & subject & ".txt")
            '    strw.WriteLine(body)
            '    strw.Close()
            '    strw.Dispose()
            'End If

        Catch ex As System.Exception
            db.WriteLog("SecureSend-SendDLMEmail", ex)
            MessageBox.Show(ex.Message, "SendDLMEmail")
            CloseSS()
        End Try
    End Sub

    Private Function GetConsultantName(userId As String, ByRef consultantFormalEmail As String) As String
        Try
            Dim csltName As String = String.Empty
            Dim params = New List(Of String)
            ' Consultant Name
            params.Add("Initials/" + userId)
            If _vertical = VerticalType.NGPP Or _vertical = VerticalType.LNG Then
                params.Add("StudyYear/" + StudyYear)
                params.Add("StudyType/NGPP")
                ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
                params.Clear() 'clear for reuse
            ElseIf _vertical = VerticalType.POWER Then
                params = New List(Of String)
                ' Consultant Name
                params.Add("SiteId/" + PowerSiteId)
                ds = db.ExecuteStoredProc("Console." & "GetConsultantBySiteId", params)
            Else
                ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
            End If


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Vertical = VerticalType.OLEFINS Or _vertical = VerticalType.CQM Then
                        csltName = ds.Tables(0).Rows(0).Item("EmployeeName").ToString()
                    Else
                        csltName = ds.Tables(0).Rows(0).Item("ConsultantName").ToString()
                        'consultantEmail = ds.Tables(0).Rows(0).Item("email").ToString()
                    End If
                    consultantFormalEmail = ds.Tables(0).Rows(0).Item("Email").ToString()
                End If
            End If
            Return csltName
        Catch ex As System.Exception

        End Try
    End Function

    'Private Function GetConsultantNameDeprecated9(ByRef userId As String) As String
    '    Try
    '        Dim csltName As String = String.Empty
    '        Dim params = New List(Of String)
    '        ' Consultant Name
    '        params.Add("Initials/" + userId)
    '        If _vertical = VerticalType.NGPP Then
    '            params.Add("StudyYear/" + StudyYear)
    '            params.Add("StudyType/NGPP")
    '            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
    '            params.Clear() 'clear for reuse
    '        Else
    '            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
    '        End If


    '        If ds.Tables.Count > 0 Then
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                If Vertical = VerticalType.OLEFINS or  _vertical = VerticalType.CQM Then
    '                    csltName = ds.Tables(0).Rows(0).Item("EmployeeName").ToString()
    '                Else
    '                    csltName = ds.Tables(0).Rows(0).Item("ConsultantName").ToString()
    '                    'consultantEmail = ds.Tables(0).Rows(0).Item("email").ToString()
    '                End If
    '            End If
    '        End If
    '        Return csltName
    '    Catch ex As System.Exception

    '    End Try
    'End Function

    Private Sub SendOtherEmail(subject As String, nbody As String, display As Boolean, Optional template As String = Nothing, Optional attaches As List(Of String) = Nothing)
        Dim body As String = nbody

        Try
            If (Not _vertical = VerticalType.OLEFINS) And (Not _vertical = VerticalType.CQM) And (Not _vertical = VerticalType.NGPP) _
                 And (Not _vertical = VerticalType.POWER) And (Not _vertical = VerticalType.LNG) Then
                body = "Dear " & EmailToPersonName & "," & vbCrLf & vbCrLf
            End If

            If template.Length > 0 Then
                If _vertical = VerticalType.POWER Then
                    body += FillTemplate(template)
                Else
                    body += FillTemplate(template).Replace(subj, "")
                End If
            End If
            body += vbCrLf + vbCrLf + ConsultantName

            'If nbody.Contains("Files link:") Then
            '    body += Environment.NewLine + nbody
            'End If

            subject = subject.Replace("~RefName~", getColoc())
            subject = subject.Replace("~DueDate~", _dueDate.ToString("dd-MMM-yyyy"))
            subject = subject.Replace("~UserID~", User)

            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    attaches(i) = attaches(i).ToString
                Next
            End If

            If (Not subject.Contains("Notification of Transmittal")) AndAlso (_vertical <> VerticalType.NGPP) AndAlso (_vertical <> VerticalType.LNG) Then
                Utilities.SendEmail(txtEmail.Text, txtCC.Text + IIf(chkPCC.Checked, txtPCC.Text, ""), subject, body, attaches, display)
            Else
                Utilities.SendEmail(txtEmail.Text, "", subject, body, attaches, display)
            End If

            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    File.Delete(attaches(i).ToString)
                Next
            End If
            'If chkSaveCopy.Checked And Not subject.Contains("Notification of Transmittal") Then
            '    Dim strw As New StreamWriter(CorrPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & subject & ".txt")
            '    strw.WriteLine(body)
            '    strw.Close()
            '    strw.Dispose()
            'End If

        Catch ex As System.Exception
            db.WriteLog("SecureSend-SendOtherEmail", ex)
            MessageBox.Show(ex.Message, "Send Other Email")
            CloseSS()
        End Try
    End Sub


    Public Function DLM_Link(file As String) As String
        ' Download Manager
        Dim servClt As New ServiceClient
        ' CALL STATEMENT
        servClt.TransferProtocol = ServiceClient.TRANSFER_PROTOCOL.SOAP
        Return servClt.GetDownloadLink(txtEmail.Text, txtPassword.Text, file, 1, mRefNum, 0, mUser, mRefNum)

    End Function
    Public Sub RecurseTree(ByVal Node As TreeNodeCollection, path As String)

        For Each tn As TreeNode In Node
            If tn.Checked And tn.GetNodeCount(False) = 0 Then
                lstFoundNode.Add(tn.Tag)
            End If

            RecurseTree(tn.Nodes, path)

        Next
    End Sub
    Private Function TraverseTree(ByVal treeView As TreeView, path As String) As List(Of String)
        'This takes the filename in the node, and finds it on the network, and passes back the path to it on the network.
        lstFoundNode = New List(Of String)
        If Not treeView Is Nothing Then
            Dim nodeStack As TreeNodeCollection = treeView.Nodes
            RecurseTree(nodeStack, path)
            Return lstFoundNode
        End If
        Return Nothing
    End Function
    Private Function AddToMfilesIdsList(masterList As List(Of String), newIds As List(Of String)) As List(Of String)
        If IsNothing(newIds) OrElse newIds.Count < 1 Then
            Return masterList
        End If
        For Each id1 As String In newIds
            Dim alreadyAdded As Boolean = False
            For Each id2 As String In masterList
                If id2 = id1 Then
                    alreadyAdded = True
                    Exit For
                End If
            Next
            If Not alreadyAdded Then
                masterList.Add(id1)
            End If
        Next
        Return masterList
    End Function
    Private Function MFilesTreeViewIds(tvw As TreeView) As List(Of String)
        Dim mfilesIds As New List(Of String)
        ' Dim tb As TabPage = ConsoleTabs.TabPages("tabSS")
        ' Dim coll As System.Windows.Forms.TabPage.TabPageControlCollection = tb.Controls
        'For Each ctrl As Control In coll
        '    If ctrl.GetType().ToString() = "System.Windows.Forms.TreeView" Then
        '        Dim tvw As TreeView = TryCast(ctrl, TreeView)
        For Each n As TreeNode In tvw.Nodes
            If n.Checked Then
                mfilesIds.Add(n.Tag)
            End If
        Next
        Return mfilesIds
        '    End If
        'Next
    End Function
    Private Function TraverseTreeWithMFilesOption(ByVal treeView As TreeView, path As String) As List(Of String)
        If (Not treeView.Tag = "USE_MFILES") And (Me.Vertical <> VerticalType.OLEFINS) Then
            'This takes the filename in the node, and finds it on the network, and passes back the path to it on the network.
            lstFoundNode = New List(Of String)
            Dim nodeStack As TreeNodeCollection = treeView.Nodes
            RecurseTree(nodeStack, path)

            Return lstFoundNode
        Else
            Dim results As New List(Of String)
            For Each treeNode As TreeNode In treeView.Nodes
                Debug.WriteLine(treeNode.Text)
                If treeNode.Checked Then
                    results.Add(treeNode.Text + _delim + treeNode.Tag)
                End If
            Next
            '            Return results
            '"add to lsit of IDS after ensuring no duplicates
            If IsNothing(mFilesFiles) Then
                mFilesFiles = New List(Of String)
            End If
            mFilesFiles = AddToMfilesIdsList(mFilesFiles, results)
            Return New List(Of String) 'so no Network files in this list
        End If
    End Function
    Private Function TraverseMfiles(treeView As TreeView) As List(Of String)
        Dim results As New List(Of String)
        For Each treeNode As TreeNode In treeView.Nodes
            If treeNode.Checked Then
                results.Add(treeNode.Text + _delim + treeNode.Tag)
            End If
        Next
        Return results
    End Function


    Private Sub btnSend_Click(sender As System.Object, e As System.EventArgs) Handles btnSend.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            If txtResponseDays.Text.Length > 0 Then
                _dueDate = Utilities.ValFaxDateDue(txtResponseDays.Text)
            End If
            If txtPassword.Text.Length > 0 Then Clipboard.SetText(txtPassword.Text)
            Dim p As Process
            If File.Exists(ConfigurationManager.AppSettings("OutlookFolder1").ToString() & "outlook.exe") Then
                p = Utilities.CheckProcess(ConfigurationManager.AppSettings("OutlookFolder1").ToString(), "Outlook")
            ElseIf File.Exists(ConfigurationManager.AppSettings("OutlookFolder2").ToString() & "outlook.exe") Then
                p = Utilities.CheckProcess(ConfigurationManager.AppSettings("OutlookFolder2").ToString(), "Outlook")
            Else
                p = Nothing
            End If

            If Utilities.checkVerifySend Then
                SendEmail()
            Else
                MsgBox("Verify Send in your Outlook is not connected, please send emails after Verify Send is connected.", vbOKOnly, "Verify Send Status")
                Me.Close()
                Exit Sub
            End If
        Catch ex As System.Exception
            db.WriteLog("SecureSend-btnSend_Click", ex)
            MessageBox.Show(ex.Message, "btnSend_Click")
            CloseSS()
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Function ZipFiles() As String

        If Not Directory.Exists(TempPath & "ZIP\") Then
            Directory.CreateDirectory(TempPath & "ZIP\")
        End If
        Dim FN As String = Directory.GetFiles(TempPath)(0).ToString ' gets first file found in C:\Users\sfb.DC1\Console\temp\SecureSend folder
        Dim nameOnly As String = Utilities.ExtractFileName(FN)
        Dim ext As String = Utilities.GetFileExtension(nameOnly)
        nameOnly = nameOnly.Remove(nameOnly.Length - ext.Length)
        nameOnly += "ZIP"
        'FN = FN.Substring(FN.LastIndexOf("\") + 1, FN.Length - FN.LastIndexOf("\") - 1)
        'FN = FN.Replace(GetExtension(FN), "ZIP")

        'sets name of zip file to be name of first file found in SecureSend folder

        Dim zipFile As String = mTempPath & "ZIP\" & nameOnly
        Dim notWordExcelDocMessage As String = String.Empty ' "Because you have a file that is not a WORD or EXCEL document, you must send your email with an encrypted zip file. "
        Dim filesToZip As String() = Directory.GetFiles(TempPath)
        Dim profilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString()
        Dim strOutlookTempFolder As String = profilePath & "\AppData\Local\Microsoft\Windows\Temporary Internet Files\Content.Outlook"
        For i As Integer = 0 To filesToZip.Length - 1
            Dim fileNameOnly As String = Utilities.ExtractFileName(filesToZip(i))
            Dim fileExt As String = Utilities.GetFileExtension(fileNameOnly)
            DeleteThisFileInTheseSubfolders(strOutlookTempFolder, fileNameOnly)
            Select Case fileExt.ToUpper()
                Case "XLS", "XLSX", "DOC", "DOCX", "XLSM", "XLSB"
                    'notWordExcelDocMessage = String.Empty
                Case Else
                    notWordExcelDocMessage = "Because you have a file that is not a WORD or EXCEL document, you must send your email with an encrypted zip file. "
            End Select
        Next
        Dim newFile = InputBox(notWordExcelDocMessage & vbCrLf & "Enter Zip filename: ", "Enter Filename", nameOnly)
        If newFile.Length = 0 Then
            Return newFile
        End If
        If Not newFile.ToUpper().EndsWith(".ZIP") Then newFile = newFile & ".zip"
        newFile = TempPath & "ZIP\" & newFile
        Utilities.Zipfile(TempPath, txtPassword.Text, newFile)
        Return newFile

    End Function


    Private Sub lstFilesToSend_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles lstFilesToSend.DragDrop
        Dim dataObject As OutlookDataObject = New OutlookDataObject(e.Data)


        'get the names and data streams of the files dropped
        Dim filenames() As String = CType(dataObject.GetData("FileGroupDescriptorW"), String())
        Dim ans As String
        ans = InputBox(CorrPath & " : ", "Save File to:", filenames(0).Trim)
        lstFilesToSend.Items.Add(CorrPath & "\" & filenames(0).Trim)

        Dim filestreams() As MemoryStream = CType(dataObject.GetData("FileContents"), MemoryStream())

        For fileIndex = 0 To filenames.Length - 1

            'use the fileindex to get the name and data stream
            Dim filename As String = filenames(fileIndex)
            Dim filestream As MemoryStream = filestreams(fileIndex)

            'save the file stream using its name to the application path
            Dim outputStream As FileStream = File.Create(CorrPath & "\" & ans)
            filestream.WriteTo(outputStream)
            outputStream.Close()

        Next

    End Sub

    Private Sub lstFilesToSend_DragEnter(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles lstFilesToSend.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub lstFilesToSend_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles lstFilesToSend.KeyDown
        If e.KeyCode = Keys.Delete Then
            If MsgBox("Are you sure you want to delete this file?", vbYesNo, "Delete File") <> MsgBoxResult.Yes Then
                Exit Sub
            End If
            'Commenting this section out; intention MUST be to remove from email attachments, NOT to delete entire file !!!
            'If lstFilesToSend.SelectedItem.ToString().StartsWith("MFiles:") Then
            '    For Each attachment As MfilesAttachment In _mfilesAttachmentList
            '        If attachment.FileName = lstFilesToSend.SelectedItem.ToString() Then
            '            Dim mfiles As New DocsMFilesAccess(Me.Vertical, RefNum, Utilities.GetLongRefnum(RefNum))
            '            mfiles.DeleteFile(attachment.FileId)
            '        End If
            '    Next
            'Else
            '    File.Delete(lstFilesToSend.SelectedItem)
            'End If
            lstFilesToSend.Items.Remove(lstFilesToSend.SelectedItem)
        End If
    End Sub


    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub btnAddFile_Click(sender As System.Object, e As System.EventArgs)

        fileDialog.Title = "Please Select a File"
        fileDialog.InitialDirectory = "\\dallas2\Data\Data\STUDY\" & mStudy & "\"
        fileDialog.ShowDialog()

    End Sub

    Private Sub fdlg_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles fileDialog.FileOk

        Dim file As String = fileDialog.FileName
        Dim start As Integer = file.LastIndexOf("\") + 1
        Dim len As Integer = file.Length
        Dim fileLength As Integer = len - start
        lstFilesToSend.Items.Add(file)
        FileCopy(file, TempPath & file.Substring(start, fileLength))

    End Sub

    Private Sub chkPCC_CheckedChanged(sender As Object, e As EventArgs) Handles chkPCC.CheckedChanged
        If chkPCC.Checked Then
            txtPCC.Enabled = True
            txtPCC.Text = PCCEmail
        Else
            txtPCC.Enabled = False
            txtPCC.Text = ""
        End If

    End Sub

    Private Function DeleteThisFileInTheseSubfolders(path As String, fileNameWithExtension As String) As Boolean
        'This resolves an issue with an attachment being renamed from "abc.def" to abc(1).def" because a file
        'with same name exists in Outlook 2010's cache folder

        Try
            Dim dirs As DirectoryInfo() = New DirectoryInfo(path).GetDirectories()
            For i As Integer = 0 To dirs.Length - 1
                Dim files As FileInfo() = dirs(i).GetFiles(fileNameWithExtension)
                For j = 0 To files.Length - 1
                    If files(j).Name = fileNameWithExtension Then
                        files(j).Delete()
                        Return True
                    End If
                Next
            Next
            Return False
        Catch
            Return False
        End Try
    End Function


    Private Class MfilesAttachment
        Private _listOrder As Integer
        Public Property ListOrder As Integer
            Get
                Return _listOrder
            End Get
            Set(value As Integer)
                _listOrder = value
            End Set
        End Property
        Private _fileName As String = String.Empty
        Public Property FileName As String
            Get
                Return _fileName
            End Get
            Set(value As String)
                _fileName = value
            End Set
        End Property
        Private _folderPath As String = String.Empty
        Public Property FolderPath As String
            Get
                Return _folderPath
            End Get
            Set(value As String)
                _folderPath = value
            End Set
        End Property
        Private _fileId As Integer
        Public Property FileId As Integer
            Get
                Return _fileId
            End Get
            Set(value As Integer)
                _fileId = value
            End Set
        End Property
    End Class
End Class
