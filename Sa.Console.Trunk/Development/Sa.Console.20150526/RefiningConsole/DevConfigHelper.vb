﻿Imports System.Reflection
Imports System.IO
Imports System.Configuration

Module DevConfigHelper
    Public Function ReadConfig(key As String) As String
        Dim result As String = String.Empty
        Dim folder As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString()
        Try
            If File.Exists(folder & "\DevConfigHelper.txt") Then
                Dim streamReader As New StreamReader(folder & "\DevConfigHelper.txt")
                Dim valuesList As New List(Of String)
                While Not streamReader.EndOfStream
                    valuesList.Add(streamReader.ReadLine())
                End While
                streamReader.Close()
                For Each item As String In valuesList
                    Dim keyValuePair As String() = item.Split(Chr(14))
                    If keyValuePair(0).ToUpper() = key.ToUpper() Then
                        result = keyValuePair(1)
                        Exit For
                    End If
                Next
            End If

        Catch ex1 As System.Exception
            Dim msg As String = ex1.Message
            'do the next try below
        End Try

        Try
            If result.Length < 1 Then
                result = ConfigurationManager.AppSettings(key).ToString()
            End If
        Catch ex2 As System.Exception
            Return String.Empty
        End Try
        Return result
    End Function
End Module
