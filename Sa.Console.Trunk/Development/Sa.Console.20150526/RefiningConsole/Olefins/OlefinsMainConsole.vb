﻿Imports System
Imports System.Data.SqlClient
Imports System.Configuration
Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.Text
Imports System.IO.Compression
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports SA.Internal.Console.DataObject
Imports iwantedue.Windows.Forms
Imports SA.Console.Common


Public Class OlefinsMainConsole


#Region "PRIVATE VARIABLES"
    Dim SavedFile As String = Nothing
    Dim CheckListStatus As String = "Y"
    Dim CompleteIssue As String = "N"
    Dim CurrentTab As Integer = 3
    Dim ValidationIssuesIsDirty As Boolean = False
    Dim Time As Integer = 0
    Dim LastTime As Integer = 0
    Dim CompanyContact As Contacts
    Dim AltCompanyContact As Contacts
    Dim OlefinsContact As Contacts
    Dim RefineryContact As Contacts
    Dim InterimContact As Contacts
    Dim PlantContact As Contacts
    Dim PowerContact As Contacts
    Dim OpsContact As Contacts
    Dim TechContact As Contacts
    Dim PricingContact As Contacts
    Dim DataContact As Contacts
    Dim CompanyPassword As String = ""

    Dim TabDirty(9) As Boolean
    Dim ds As DataSet
    'Dim db As New DataAccess()
    Dim db As DataObject


    Dim sDir(10) As String
    Dim StudyRegion As String = ""
    Dim Study As String = ""

    Dim SANumber As String

    Dim CurrentConsultant As String
    Dim CurrentRefNum As String
    Dim ValidationFile As String
    Dim ValidatePath As String
    Dim JustLooking As Boolean = False
    Dim CurrentCompany As String
    Dim Company As String
    Dim CompCorrPath As String
    Dim GeneralPath As String
    Dim ProjectPath As String
    Dim TempPath As String
    Dim OldTempPath As String = ""
    Dim TemplatePath As String
    Dim DirResult As String
    Dim PipelinePath As String
    Dim StudyDrive As String

    'changing this so can run on VM
    'Dim TempDrive As String = "C:\USERS\"
    'Private ProfilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + "\"
    'Private ProfileConsolePath As String = ProfilePath + "Console\"
    Private _profileConsoleTempPath As String = ProfileConsolePath + "temp\"
    Private _profileConsoleExtractedPath As String = _profileConsoleTempPath + "Extracted Files\"
    Dim CurrentPath As String
    Dim RefNum As String
    Dim bJustLooking As Boolean = True
    Dim RefNumPart As String
    Dim ShortRefNum As String
    Dim FirstTime As Boolean = True
    Dim KeyEntered As Boolean = False

	Private mDBConnection As String
    Private mStudyYear As String
    Private mPassword As String
    Private mRefNum As String
    Private mSpawn As Boolean
    Private mStudyType As String
    Private mUserName As String
    Private mReturnPassword As String

    Private _userWindowsProfileName As String = String.Empty
    'Private _lastChosenRefNum As String = String.Empty
    'Private _pathToPriorDatachecksFile As String = String.Empty '"K:\STUDY\Olefins\2013\Programs\Validate\Olefins DataChecks.xls"
    'Private _pathToCurrentDatachecksFile As String = String.Empty '"K:\STUDY\Olefins\2015\Programs\Validate\Olefins DataChecks.xls"
    Private Const _delim As Char = Chr(14)
    'Private Err As ErrorLog
    Private _mfilesFailed As Boolean = False
    Private _useMfilesCompanyFolders As Boolean = False 'Olefins doesn't use for now
    Private _useMFiles As Boolean = False
    Private _plantFolder As String = String.Empty
    Private _secureSendTemplateFolder As String = String.Empty
    Private _dgContinuingIssuesHeight As Integer = 159
    Private _studiesAndRefnums As New List(Of String)
    Private _lastChosenRefNum As String = String.Empty
    'Monitoring



#End Region

#Region "PUBLIC PROPERTIES"
    Private _monitoringInterval As Single 'used to distinguise Studies in Olepins, and as a parameter to build the settings file in local drive.
    Public Property MonitoringInterval As Single
        Get
            Return _monitoringInterval
        End Get
        Set(value As Single)
            _monitoringInterval = value
        End Set
    End Property

    Private mVPN As Boolean
    Public Property VPN() As Boolean
        Get
            Return mVPN
        End Get
        Set(ByVal value As Boolean)
            mVPN = value
        End Set
    End Property
    Public Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
        Set(ByVal value As String)
            mDBConnection = value
        End Set
    End Property
    Private mCurrentStudyYear As String
    Public Property CurrentStudyYear() As String
        Get
            Return mCurrentStudyYear
        End Get
        Set(ByVal value As String)
            mCurrentStudyYear = value
        End Set
    End Property
    Public Property StudyYear() As String
        Get
            Return mStudyYear
        End Get
        Set(ByVal value As String)
            mStudyYear = value
        End Set
    End Property

    Public Property UserName() As String

        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set

    End Property

    Public Property UserWindowsProfileName() As String

        Get
            If (IsNothing(_userWindowsProfileName)) OrElse (_userWindowsProfileName.Length < 1) Then
                ' = My.User.Name + ".DC1"
                Dim fullName As String = My.User.Name
                If Not fullName.Contains("\") Then
                    _userWindowsProfileName = fullName
                Else
                    Dim temp As String() = My.User.Name.Split("\")

                    _userWindowsProfileName = temp(1) + "." + temp(0)
                End If
            End If

            Return _userWindowsProfileName
        End Get
        Set(ByVal value As String)
            _userWindowsProfileName = value
        End Set

    End Property

    Public Property ReturnPassword() As String
        Get
            Return mReturnPassword
        End Get
        Set(ByVal value As String)
            mReturnPassword = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property


    Public Property StudyType() As String
        Get
            Return mStudyType
        End Get
        Set(ByVal value As String)
            mStudyType = value
        End Set
    End Property

    Public Property ReferenceNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property

    Public Property Spawn() As Boolean
        Get
            Return mSpawn
        End Get
        Set(ByVal value As Boolean)
            mSpawn = value
        End Set
    End Property
#End Region

#Region "INITIALIZATION"
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
    End Sub

    Private Sub Initialize()
        Try
            _useMFiles = False
           
            'Populate Consultant Tab with login
            tabConsultant.Text = UserName
            SetTabDirty(True)

            RemoveTab("tabTimeGrade")

            'Populate the Form Title
            Me.Text = StudyType & " Console"
            ' Me.Icon = My.Resources.refinery

            db = New DataObject(DataObject.StudyTypes.OLEFINS, UserName, Password)
            Dim sql As String = String.Empty
            If _monitoringInterval = 4 Then   'CQM
                StudyDrive = "K:\STUDY\OlefinsCQM\"
                sql = "select distinct 'CQM' + right(studyyear, 2) as Study from dbo.TSort where StudyId='CQM' order by Study desc"
                'K:\STUDY\Olefins\2017\Correspondence
                'K:\STUDY\OlefinsCQM\2018\Correspondence

                'Label46.Visible = False
                'txtValidationIssues.Visible = False
                'lblNotesStatus.Visible = False
                'btnLastPresenterNoteSaved.Visible = False
                'btnCreatePN.Visible = False
            ElseIf _monitoringInterval = 1 Then  'SEEC
                StudyDrive = "\\dallas2\Data\Data\STUDY\SEEC\"
                sql = "select CONCAT('SEEC', RIGHT(STR(m.StudyYear),2)) from (select distinct StudyYear from dbo.TSort where StudyID = 'SEEC') m  order by m.StudyYear desc"
            ElseIf _monitoringInterval = 0.5 Then '2 year = study [Olepins]
                StudyDrive = "K:\STUDY\Olefins\"
                sql = "select CONCAT('PCH', RIGHT(STR(m.StudyYear),2)) from (select distinct StudyYear from dbo.TSort where StudyID = 'PCH') m  order by m.StudyYear desc"
            End If
            If Not FillCboStudy(sql) Then
                Return
            End If

            CurrentStudyYear = cboStudy.Items(0).ToString().Substring(cboStudy.Items(0).ToString().Length - 2, 2)

            Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim version As System.Version = assembly.GetName().Version
            txtVersion.Text = String.Format("Version {0}", version) & " - 20" & CurrentStudyYear

            LoadCboConsultants()


            cboDir.Items.Add("Plant Correspondence")
            cboDir2.Items.Add("Plant Correspondence")

            cboDir.SelectedIndex = 0
            cboDir2.SelectedIndex = 0
            cbConsultants.Text = UserName

            'Check for existance of Settings file, if missing, copy default one

            GetSettingsFile()

            'If not spawned off an existing instance of the program, read the settings file and set the drop downs accordingly
            If Not Spawn Then
                ReadLastStudy()
            Else
                If ReferenceNum <> "" Then
                    cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(ReferenceNum, 2) & StudyYear)
                Else
                    cboStudy.SelectedIndex = 0
                End If
            End If
            'Fill Directory Drop Down

            GetCompanyContactInfo("COORD")
            GetCompanyContactInfo("ALT")
            cboCheckListView.SelectedIndex = 0


            'Try
            '    _pathToPriorDatachecksFile = System.Configuration.ConfigurationManager.AppSettings("OlefinsPathToPriorDatachecksFile").ToString()
            '    _pathToCurrentDatachecksFile = System.Configuration.ConfigurationManager.AppSettings("OlefinsPathToCurrentDatachecksFile").ToString()
            'Catch exDataCheckConfig As System.Exception
            '    db.WriteLog("OlefinsMainConsole.Initialize(): Can't find app setting for either OlefinsPathToPriorDatachecksFile  or  OlefinsPathToCurrentDatachecksFile", exDataCheckConfig)
            'End Try

            'don't need this since starting with non-MFiles year
            'If _mfilesFailed Then
            '    MFilesFailedRoutine()
            'Else
            '    UnhideMFilesTabs()
            'End If


        Catch ex As System.Exception
            db.WriteLog("Initialize", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub

#End Region

    Private Function FillCboStudy(sql As String) As Boolean
        Dim ds As DataSet
        Try
            db.SQL = sql
            ds = db.Execute()
            For Each row As DataRow In ds.Tables(0).Rows
                If _lastChosenRefNum.Length < 1 Then
                    _lastChosenRefNum = row(0).ToString() 'most recent year
                End If
                cboStudy.Items.Add(row(0).ToString())
            Next
            Return True
        Catch ex As Exception
            MsgBox("Error getting first item of data from Olefins database (FillCboStudy()): " & ex.Message)
            Return False
        Finally
            db.SQL = String.Empty
        End Try
    End Function

#Region "BUILD TABS"

    Private Sub MFilesFailedRoutine()
        ClearMFilesTabs()
        HideMFilesTabs()

        SetTabDirty(True)

        TabDirty(5) = False
        TabDirty(7) = False
        TabDirty(8) = False

        LoadTab(0) 'consultatnt tab
        LoadTab(1) ''plnat tab
        LoadTab(2) ''company tab
        LoadTab(3) ''summary tab
        LoadTab(4) ''checklist
        ''Time/grade
        LoadTab(6) 'issues
    End Sub

    Private Sub ClearMFilesTabs()
        'clear and hide : corresp, securesend, dragdrop. Reload all other tabs
        lstVFNumbers.Items.Clear()
        listViewCorrespondenceVF.Items.Clear()
        listViewCorrespondenceVR.Items.Clear()
        listViewCorrespondenceVReturn.Items.Clear()
        tvwCorrespondence2.Nodes.Clear()
        dgFiles.Rows.Clear()
    End Sub



    Private Sub HideMFilesTabs()
        'these don't work:
        ' tabSS.Hide()
        'tabCorr.Hide()
        'tabDD.Hide()

        'these get tabs out of order:
        'tabSS.Parent = Nothing
        'tabCorr.Parent = Nothing '"ConsoleTabs"
        'tabDD.Parent = Nothing

        '//((Control)this.tabPage).Enabled = false;
        TryCast(tabSS, Control).Enabled = False
        TryCast(tabCorr, Control).Enabled = False
        TryCast(tabDD, Control).Enabled = False

    End Sub
    Private Sub UnhideMFilesTabs()
        TryCast(tabSS, Control).Enabled = True
        TryCast(tabCorr, Control).Enabled = True
        TryCast(tabDD, Control).Enabled = True
    End Sub


    Private Sub BuildSummaryTab()
        'txtValidationIssues.ReadOnly = False
        lblValidationStatus.Text = "Status"
        RefNum = cboRefNum.Text
        SetSummaryFileButtons()

        lblLastCalcDate.Text = ""
        lblLastFileSave.Text = ""
        lblLastUpload.Text = ""
        lblRedFlags.Text = ""
        lblBlueFlags.Text = ""
        lblItemsRemaining.Text = ""
        txtValidationIssues.Text = ""



        lblCalcUpload.Visible = True
        lblOSIMUpload.Visible = True
        lblCU.Visible = True
        lblLU.Visible = True
        lblOU.Visible = True
        lblCalcUpload.Text = ""
        lblOSIMUpload.Text = ""
        lblOSIMUpload.Text = ""
        lblLFS.Text = "OSIM Modification:"
        lblLU.Text = "PYPS File Date:"
        lblLCD.Text = "SPSL File Date:"
        lblCU.Text = "Calc Upload:"
        lblOU.Text = "OSIM Upload:"




        ' See if their data file is in holding.
        ' If so, get the modification date/time from it.
        ' I changed 'VALIDATION' to 'VALIDATE'
        Try


            If FileExistsForXlsXlsm(PipelinePath & ParseRefNum(RefNum, 0) & ".xls") = False Then
                lblValidationStatus.ForeColor = Color.DarkRed
                lblValidationStatus.Text = "Not Received"

            Else

                If VChecked(RefNum, "V2.0") Then
                    lblInHolding.Text = ""
                    lblLastFileSave.Text = FileDateTimeForFileXlsXlsm(PipelinePath & ParseRefNum(RefNum, 0) & ".xls")
                    lblValidationStatus.ForeColor = Color.DarkGreen
                    lblValidationStatus.Text = "Complete"
                Else

                    If FileExistsForXlsXlsm(PipelinePath & ParseRefNum(RefNum, 0) & ".xls") = False Then
                        lblValidationStatus.ForeColor = Color.DarkRed
                        lblValidationStatus.Text = "Not Received"

                    Else

                        If FileExistsForXlsXlsm(ValidatePath & "Holding\" & ParseRefNum(RefNum, 0) & ".xls") Then
                            lblValidationStatus.ForeColor = Color.DarkGoldenrod
                            lblValidationStatus.Text = "In Holding"

                        End If
                    End If

                    If VChecked(RefNum, "V1.2") Then
                        lblInHolding.Text = ""
                        lblLastFileSave.Text = FileDateTimeForFileXlsXlsm(PipelinePath & ParseRefNum(RefNum, 0) & ".xls")
                        lblValidationStatus.ForeColor = Color.DarkBlue
                        lblValidationStatus.Text = "Validating"

                    Else

                        If VChecked(RefNum, "V1.5") Then
                            lblInHolding.Text = ""
                            lblLastFileSave.Text = FileDateTimeForFileXlsXlsm(PipelinePath & ParseRefNum(RefNum, 0) & ".xls")
                            lblValidationStatus.ForeColor = Color.MediumBlue
                            lblValidationStatus.Text = "1st IDR Complete"
                        Else
                            If VChecked(RefNum, "V1.6") Then
                                lblInHolding.Text = ""
                                lblLastFileSave.Text = FileDateTimeForFileXlsXlsm(PipelinePath & ParseRefNum(RefNum, 0) & ".xls")
                                lblValidationStatus.ForeColor = Color.LightBlue
                                lblValidationStatus.Text = "Polishing Complete"

                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As System.Exception
            db.WriteLog("BuildSummaryTab", ex)
        End Try

        Dim mRefNum As String = RefNum

        Dim row As DataRow
        Dim params = New List(Of String)

        Dim PYPSFile As String = "PYPS" + "20" + RefNum + ".xls*"
        Dim SPSLFile As String = "SPSL" + "20" + RefNum + ".xls*"
        Dim CalcFile As String = "CALC" + "20" + RefNum + ".xls"
        Dim OSIMFile As String = "OSIM" + "20" + RefNum + ".xls"

        params.Clear()
        db.SQL = "SELECT OSIMUpload, Calcs from dbo.PlantStatus where refnum = '20" + RefNum + "'"
        Dim dataReader As SqlDataReader = db.ExecuteReader()
        If dataReader.HasRows Then
            While dataReader.Read
                If Not IsDBNull(dataReader.Item("OSIMUpload")) Then
                    lblOSIMUpload.Text = dataReader.Item("OSIMUpload").ToString.Trim
                Else
                    lblOSIMUpload.Text = String.Empty
                End If
                If Not IsDBNull(dataReader.Item("Calcs")) Then
                    lblCalcUpload.Text = dataReader.Item("Calcs").ToString.Trim
                    Exit While
                Else
                    lblCalcUpload.Text = String.Empty
                End If
                Exit While
            End While
        End If
        dataReader.Close()


        'MOVE TO MFILES CHECK - no, cancel that . . .
        If File.Exists(CorrPath + OSIMFile) Then
            Dim f As New FileInfo(CorrPath + OSIMFile)
            lblLastFileSave.Text = f.LastWriteTime.ToString
        ElseIf File.Exists(CorrPath + OSIMFile + "m") Then
            Dim f As New FileInfo(CorrPath + OSIMFile + "m")
            lblLastFileSave.Text = f.LastWriteTime.ToString
        End If
        'Dim docsFileSystemAccess As New DocsFileSystemAccess(VerticalType.OLEFINS)
        'Dim files As ConsoleFilesInfo = _
        '    docsFileSystemAccess.GetDocInfoByNameAndRefnum(OSIMFile, _
        '    GetRefNumCboText(), Get20PlusRefNumCboText(), True)
        'If Not IsNothing(files) AndAlso Not IsNothing(files.Files) AndAlso Not IsNothing(files.Files(0)) Then
        '    If Not IsNothing(files.Files(0).DocumentDate) Then
        '        lblLastFileSave.Text = files.Files(0).DocumentDate.ToString()
        '    End If
        'End If

        Dim pypsDate As DateTime? = CheckForLastModifiedDate(CorrPath, PYPSFile)
        If Not IsNothing(pypsDate) Then
            lblLastUpload.Text = pypsDate.ToString()
        End If

        Dim spslDate As DateTime? = CheckForLastModifiedDate(CorrPath, SPSLFile)
        If Not IsNothing(spslDate) Then
            lblLastCalcDate.Text = spslDate.ToString()
        End If

        'If File.Exists(CorrPath + CalcFile) Then
        '    Dim f As New FileInfo(CorrPath + CalcFile)
        '    lblCalcUpload.Text = f.CreationTime.ToString
        'End If

        RefNum = mRefNum

        'Add Parameters and call GetConsultants
        ' Load the Uncompleted Checklist Item count label
        params.Clear()
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Mode/N")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                lblItemsRemaining.Text = row(0).ToString & " Incompleted Item"
                If Convert.ToInt32(row(0)) <> 1 Then
                    lblItemsRemaining.Text += "s Remaining"
                Else
                    lblItemsRemaining.Text += " Remaining"
                End If
            End If
        Else
            lblItemsRemaining.Text = "0 Items Remaining"
        End If



        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("NoteType/Validation")
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                txtValidationIssues.Text = row("Note").ToString
            Else
                txtValidationIssues.Text = ""
            End If
        End If


        If txtValidationIssues.Text.Length > 0 Then
            If txtValidationIssues.Text.Substring(0, 1) = "~" Then
                txtValidationIssues.ReadOnly = True
            Else
                txtValidationIssues.ReadOnly = False
            End If
        End If



        'TODO:  This should be ROLE BASED when we get ACTIVE DIRECTORY

        ' Only the polishers get to push the button to lock the notes and create the Word file.
        ' Not sure if CRT and JDW are supposed to be on this list, but not a big deal.
        ' Added LS 9/9/09 per DEJ so she can go thru and extract the ones that have not been done.
        mUserName = mUserName.ToUpper



    End Sub

    Private Function FileDateTimeForFileXlsXlsm(path As String) As String
        If File.Exists(path) Then
            Return FileDateTime(path).ToString()
        ElseIf File.Exists(path + "m") Then
            Return FileDateTime(path + "m").ToString()
        Else
            Return String.Empty
        End If
    End Function

    Private Function FileExistsForXlsXlsm(path As String) As Boolean
        If File.Exists(path) Then
            Return True
        ElseIf File.Exists(path + "m") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function CheckForCreatedDate(folderPath As String, fileName As String) As DateTime?
        Dim result As Boolean = False
        Dim fileFound As String = Dir(folderPath + fileName)
        If fileFound <> String.Empty Then
            Dim fileInfo As New FileInfo(folderPath + fileFound)
            Return fileInfo.CreationTime
        End If
        Return Nothing
    End Function

    Private Function CheckForLastModifiedDate(folderPath As String, fileName As String) As DateTime?
        Dim result As Boolean = False
        Dim fileFound As String = Dir(folderPath + fileName)
        If fileFound <> String.Empty Then
            Dim fileInfo As New FileInfo(folderPath + fileFound)
            Return fileInfo.LastWriteTime
        End If
        Return Nothing
    End Function

    Private Sub BuildCheckListTab(yesNoAllText As String) ' Y for completed, N for not completed, A for all

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""
        Dim yesNoAllLetter As String = String.Empty

        Select Case yesNoAllText
            Case "Complete"
                yesNoAllLetter = "Y"

            Case "Incomplete"
                yesNoAllLetter = "N"

            Case Else
                yesNoAllLetter = "A"

        End Select

        UpdateCheckList(yesNoAllLetter)


    End Sub

    Private Sub BuildCorrespondenceTab()

        Dim I As Integer
        Dim blnAlready As Boolean
        CorrPath = BuildCorrPath(cboRefNum.Text)
        lstVFNumbers.Items.Clear()

        If Not _useMFiles Then
            DirResult = Dir(CorrPath & "VF*")
            Do While DirResult <> ""

                ' Get the next VF file.
                I = 0
                blnAlready = False
                ' Is it already in the list?
                Do While I < lstVFNumbers.Items.Count
                    If lstVFNumbers.Items(I) = Mid(DirResult, 3, 1) Then
                        blnAlready = True
                        Exit Do
                    End If
                    I = I + 1
                Loop
                ' No, put it there
                If Not blnAlready Then
                    lstVFNumbers.Items.Add(Mid(DirResult, 3, 1)) ' & Chr(9) & FileDateTime(strCorrPath & strDirResult)
                End If
                ' Look for next file
                DirResult = Dir()
            Loop

            ' Now look for any orphan VR files
            ' Same logic as above except VR.
            DirResult = Dir(CorrPath & "VR*")
            Do While DirResult <> ""

                I = 0
                blnAlready = False
                Do While I < lstVFNumbers.Items.Count
                    If lstVFNumbers.Items(I) = Mid(DirResult, 3, 1) Then
                        blnAlready = True
                        Exit Do
                    End If
                    I = I + 1
                Loop
                If Not blnAlready Then
                    lstVFNumbers.Items.Add(Mid(DirResult, 3, 1)) ' & Chr(9) & FileDateTime(strCorrPath & strDirResult)
                End If
                DirResult = Dir()
            Loop
        Else
            Try
                Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
                Dim vNums As List(Of String) = docsAccess.GetDataFor_lstVFNumbers("VF*", CorrPath, Nothing, GetRefNumCboText(), Get20PlusRefNumCboText())
                ' Now look for any orphan VR files
                vNums = docsAccess.GetDataFor_lstVFNumbers("VR*", CorrPath, vNums, GetRefNumCboText(), Get20PlusRefNumCboText())
                For Each item As String In vNums
                    lstVFNumbers.Items.Add(item)
                Next
            Catch ex As Exception
                If ex.Message.Contains("Unable to find Benchmarking Participant") Then
                    _mfilesFailed = True
                End If
                MsgBox("Error in BuildCorrespondenceTab, " + ex.Message)
            End Try
            If _mfilesFailed Then
                MFilesFailedRoutine()
            Else
                UnhideMFilesTabs()
            End If
        End If

        lstVFNumbers.SelectedIndex = lstVFNumbers.Items.Count - 1
        ' Using this routine to actually load the Corr tab and also
        ' to find highest VF file number for fraCons_Handle.
        ' Only do the SetCorr routine if we are doing the Corr tab.

        SetCorr()

    End Sub

    Private Sub BuildIssuesTab()
        Dim row As DataRow
        Dim params As List(Of String)

        ClearContinuingIssues()
        BuildContinuingIssues()


        'Throw New Exception("BuildIssuesTab - under construction")
        'txtContinuingIssues.Text = ""
        'params = New List(Of String)
        'params.Add("RefNum/" + RefNum)
        'params.Add("NoteType/Continuing")
        'ds = db.ExecuteStoredProc("Console." & "GetNote", params)
        'If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
        '    row = ds.Tables(0).Rows(0)
        '    txtContinuingIssues.Text = row("Note").ToString
        'End If


        txtConsultingOpps.Text = ""
        lblIssueStatus.Text = "Notes Save Status:"

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("NoteType/Consulting")
        ds = db.ExecuteStoredProc("Console." & "GetNote", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            row = ds.Tables(0).Rows(0)
            txtConsultingOpps.Text = row("Note").ToString
        End If

    End Sub

    Private Sub BuildDirectoriesTab() 'this works the SecureSend tab
        SetPaths()
        'There are 4 treeviews
        '-2 are corresp
        '-2 are company corrresp
        PopulateCompCorrTreeView(tvwCompCorr)
        PopulateCorrespondenceTreeView(tvwCorrespondence)

        'PopulateCompCorrTreeView(tvwCompCorr2)
        'If Int32.Parse(StudyYear) < 2015 Then
        '    PopulateCompCorrTreeView(tvwCompCorr2)
        'Else
        '    PopulateTreeViewWithMfilesFiles(tvwCompCorr2) ', True)
        'End If

        'If Int32.Parse(StudyYear) <> 2015 Then
        Label13.Text = "" ' "Directory"
        PopulateCorrespondenceTreeView(tvwCorrespondence2)

        'tvwCompCorr2.Visible = False
        tvwCorrespondence2.Visible = False
        cboDir2.Visible = False
        'Else
        '    Label13.Text = "M-Files"
        '    PopulateTreeViewWithMfilesFiles(tvwCorrespondence2)

        '    ' tvwCompCorr2.Visible = True
        '    tvwCorrespondence2.Visible = True
        '    cboDir2.Visible = True
        'End If



    End Sub

#End Region

#Region "UTILITIES"
    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function
    Private Sub SetTabDirty(status As Boolean)
        For i = 0 To 9 ' 8
            TabDirty(i) = status
        Next
    End Sub
    Private Sub LoadTab(tab As Integer)
        Select Case tab
            Case 0 'Consultant Tab
                If TabDirty(0) Then
                    'SetConsultant(UserName)
                    TabDirty(0) = False
                End If

            Case 1, 2 'Refining/Plant and Company Tab
                If TabDirty(1) Or TabDirty(2) Then
                    GetCompanyContactInfo("COORD")
                    GetCompanyContactInfo("ALT")
                    GetPlantContacts()
                    'SB commented out. Is failing - no such stored proc for Olefins DB: GetInterimContactInfo()
                    FillContacts()
                    TabDirty(1) = False
                    TabDirty(2) = False
                End If

            Case 3 'Summary Tab
                If TabDirty(3) Then
                    BuildSummaryTab()
                    TabDirty(3) = False
                End If
                lblNotesStatus.Text = "Notes Save Status:"

            Case 4  'CheckList Tab
                If TabDirty(4) Then
                    BuildCheckListTab("Incomplete")
                    TabDirty(4) = False
                End If

            Case 5  'Correspondence Tab
                If TabDirty(5) Then
                    BuildCorrespondenceTab()
                    TabDirty(5) = False
                End If

            Case 6  'Grade/Test or Issues Tab

                If TabDirty(7) Then
                    BuildIssuesTab()
                    TabDirty(7) = False
                End If


            Case 7  'SecureSend Tab

                If TabDirty(8) Then
                    BuildDirectoriesTab()
                    TabDirty(8) = False
                End If

            Case 8 'Drag and Drop tab
                If TabDirty(9) Then
                    ClearDragAndDrop()
                End If


        End Select
    End Sub
    Private Function BuildNewSARecord() As String
        Dim params = New List(Of String)
        params.Add("RefNum/" + RefNum)

        If Not CheckSAMaster(RefNum) Then
            ds = db.ExecuteStoredProc("Console.InsertSARecord", params)
        Else
            MsgBox(CurrentCompany & " already has a record in SAMaster for " & Study & StudyYear)
            Return ""
        End If
        Return ds.Tables(0).Rows(0).Item(0)
    End Function
    Private Function CheckSAMaster(RefNum) As Boolean

        Dim params = New List(Of String)

        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console.CheckSANumber", params)
        If ds.Tables.Count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    'Private Function RemovePassword(File As String, password As String, companypassword As String) As Boolean
    '    Dim success As Boolean = True
    '    If File.Substring(File.Length - 3, 3).ToUpper = "XLS" Then
    '        If File.ToUpper.Contains("RETURN") Then
    '            success = Utilities.ExcelPassword(File, password, 1)
    '        Else
    '            success = Utilities.ExcelPassword(File, companypassword, 1)
    '        End If
    '    End If
    '    If File.Substring(File.Length - 3, 3).ToUpper = "DOC" Or File.Substring(File.Length - 3, 3).ToUpper = "OCX" Then
    '        success = Utilities.WordPassword(File, companypassword, 1)
    '    End If
    '    Return success
    'End Function
    Private Function FixFilename(ext As String, strFile As String)
        Dim strNewFilename As String
        Dim NewExt As String = GetExtension(strFile)
        strNewFilename = strFile.Replace(NewExt, ext)
        Return strNewFilename
    End Function
    Private Function GetExtension(strFile As String)
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, strFile.Length - (strFile.LastIndexOf(".") + 1))
        Catch
            ext = ""
        End Try
        Return ext
    End Function
    Function FindModifier(strNextNum As String) As String
        ' Given a number to use in VRx, see if we need to add a letter to it.
        Dim strWork1 As String
        Dim strWork2 As String

        strWork1 = ""
        strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & cboCompany.Text & " Email Text.txt")

        If strWork2 > strWork1 Then
            strWork1 = strWork2
        End If

        For I = 0 To dgFiles.Rows.Count - 1
            If dgFiles.Rows(I).Cells(0).ToString.Substring(dgFiles.Rows(I).Cells(0).ToString.Length - 4, 4).ToUpper = ".XLS" And dgFiles.Rows(I).Cells(0).ToString.ToUpper.Contains("RETURN") Then
                strWork2 = PlugLetterIfNeeded("Return" & strNextNum & ".xls")
            Else
                strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & dgFiles.Rows(I).Cells(0).ToString)
            End If
            If strWork2 > strWork1 Then
                strWork1 = strWork2
            End If
        Next I
        Return strWork1
    End Function

    Public Function PlugLetterIfNeeded(strFileName As String) As String
        Dim strWorkFileName As String
        Dim strDirResult As String
        Dim strCorrPath As String
        Dim strABC(10) As String
        Dim I As Integer
        Dim intInsertPoint As Integer

        strABC(0) = ""
        strABC(1) = "A"
        strABC(2) = "B"
        strABC(3) = "C"
        strABC(4) = "D"
        strABC(5) = "E"
        strABC(6) = "F"
        strABC(7) = "G"
        strABC(8) = "H"
        strABC(9) = "I"

        strCorrPath = CorrPath
        If strFileName.Substring(0, 6).ToUpper = "RETURN" Then
            intInsertPoint = 7
        Else
            intInsertPoint = 3
        End If

        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
        ', GetRefNumCboText(), Get20PlusRefNumCboText())

        For count As Integer = 0 To 9
            strWorkFileName = strFileName.Substring(0, intInsertPoint) & strABC(I) & Mid(strFileName, intInsertPoint + 1, 99)
            If _useMFiles Then
                If Not docsAccess.DocExistsInMFiles(strWorkFileName, GetRefNumCboText(), Get20PlusRefNumCboText(), True) Then
                    Return strABC(I)
                End If
            Else
                If Not File.Exists(CorrPath & strWorkFileName) Then
                    Return strABC(I)
                End If
            End If
        Next

        'For I = 0 To 9
        '    strWorkFileName = strFileName.Substring(0, intInsertPoint) & strABC(I) & Mid(strFileName, intInsertPoint + 1, 99)
        '    strDirResult = Dir(CorrPath & strWorkFileName)
        '    If strDirResult = "" Then
        '        Return strABC(I)
        '        Exit Function
        '    End If
        'Next I

        MsgBox("More than 9 iterations of same file?", vbOKOnly, "Check with Joe Waters (JDW)?")

        PlugLetterIfNeeded = strFileName

    End Function
    Function CleanFileName(strFileName As String) As String
        Dim strBadChar As String
        Dim strRepChar As String
        Dim I As Integer
        Dim J As Integer
        Dim blnChanged As Boolean
        blnChanged = False

        strBadChar = "\/:*?""<>|%"
        ' A
        strBadChar = strBadChar & Chr(192)
        strBadChar = strBadChar & Chr(193)
        strBadChar = strBadChar & Chr(194)
        strBadChar = strBadChar & Chr(195)
        strBadChar = strBadChar & Chr(196)
        strBadChar = strBadChar & Chr(197)
        strBadChar = strBadChar & Chr(198)
        ' E
        strBadChar = strBadChar & Chr(200)
        strBadChar = strBadChar & Chr(201)
        strBadChar = strBadChar & Chr(202)
        strBadChar = strBadChar & Chr(203)
        ' I
        strBadChar = strBadChar & Chr(204)
        strBadChar = strBadChar & Chr(205)
        strBadChar = strBadChar & Chr(206)
        strBadChar = strBadChar & Chr(207)
        ' O
        strBadChar = strBadChar & Chr(210)
        strBadChar = strBadChar & Chr(211)
        strBadChar = strBadChar & Chr(212)
        strBadChar = strBadChar & Chr(213)
        strBadChar = strBadChar & Chr(214)
        ' U
        strBadChar = strBadChar & Chr(217)
        strBadChar = strBadChar & Chr(218)
        strBadChar = strBadChar & Chr(219)
        strBadChar = strBadChar & Chr(220)

        ' a
        strBadChar = strBadChar & Chr(224)
        strBadChar = strBadChar & Chr(225)
        strBadChar = strBadChar & Chr(226)
        strBadChar = strBadChar & Chr(227)
        strBadChar = strBadChar & Chr(228)
        strBadChar = strBadChar & Chr(229)
        ' e
        strBadChar = strBadChar & Chr(232)
        strBadChar = strBadChar & Chr(233)
        strBadChar = strBadChar & Chr(234)
        strBadChar = strBadChar & Chr(235)
        ' i
        strBadChar = strBadChar & Chr(236)
        strBadChar = strBadChar & Chr(237)
        strBadChar = strBadChar & Chr(238)
        strBadChar = strBadChar & Chr(239)
        ' o
        strBadChar = strBadChar & Chr(242)
        strBadChar = strBadChar & Chr(243)
        strBadChar = strBadChar & Chr(244)
        strBadChar = strBadChar & Chr(245)
        strBadChar = strBadChar & Chr(246)
        ' u
        strBadChar = strBadChar & Chr(249)
        strBadChar = strBadChar & Chr(250)
        strBadChar = strBadChar & Chr(251)
        strBadChar = strBadChar & Chr(252)

        strRepChar = "  -      AAAAAAAEEEEIIIIOOOOOUUUUaaaaaaeeeeiiiiooooouuuu"
        For I = 1 To Len(strBadChar)
            For J = 1 To Len(strFileName)
                If Mid(strFileName, J, 1) = Mid(strBadChar, I, 1) Then
                    Mid(strFileName, J, 1) = Mid(strRepChar, I, 1)
                    blnChanged = True
                End If
            Next J
        Next I
        CleanFileName = Trim(strFileName)
    End Function
    Private Sub GetSettingsFile()
        Dim yr As String
        Try
            If Directory.Exists(_profileConsoleTempPath) = False Then Directory.CreateDirectory(_profileConsoleTempPath)
            If Directory.Exists(_profileConsoleTempPath & "ConsoleSettings") = False Then Directory.CreateDirectory(_profileConsoleTempPath & "ConsoleSettings")

            Dim textInterval As String = String.Empty
            If _monitoringInterval <> 0.5 Then
                textInterval = _monitoringInterval.ToString()
            End If

            If Not File.Exists(_profileConsoleTempPath & "ConsoleSettings\" & StudyType & textInterval & "Settings.txt") Then
                Dim studies As New List(Of String)

                For i As Integer = cboStudy.Items.Count - 1 To 0
                    studies.Add(cboStudy.Items(i))
                Next
                Using objWriter As New System.IO.StreamWriter(_profileConsoleTempPath & "ConsoleSettings\" & StudyType & textInterval & "Settings.txt", False)
                    For Each studyItem As String In studies
                        objWriter.WriteLine(studyItem & _delim)
                        _studiesAndRefnums.Add(studyItem & _delim)
                    Next
                    'For i = 9 To 17 Step 2
                    '    yr = i.ToString
                    '    If yr.Length = 1 Then yr = "0" & yr

                    '    objWriter.WriteLine("PCH" & yr)
                    '    objWriter.WriteLine(yr & "PCH003")
                    'Next
                End Using
            End If

        Catch ex As System.Exception
            db.WriteLog("GetSettingsFile", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ReadLastStudy()
        Dim textInterval As String = String.Empty
        If _monitoringInterval <> 0.5 Then
            textInterval = _monitoringInterval.ToString()
        End If
        Dim path As String = _profileConsoleTempPath & "ConsoleSettings\" & StudyType & textInterval & "settings.txt"
        If Not File.Exists(path) Then
            'MsgBox("Error in ReadLastStudy: Can't find file " + path)
            Exit Sub
        End If
        Try
            Dim countOfLinesWithOldFormat As Integer = 0
            Using objReader As New IO.StreamReader(path)
                While objReader.EndOfStream = False
                    Dim line As String = objReader.ReadLine()
                    If line.Length > 0 Then
                        If Not line.Contains(_delim) Then
                            countOfLinesWithOldFormat = countOfLinesWithOldFormat + 1
                        End If
                    End If
                End While
            End Using
            If countOfLinesWithOldFormat > 0 And _monitoringInterval = 0.5 Then
                'delete old file, exit. On save, will write new file with new format
                Using objWriter As New IO.StreamWriter(path, False)
                    Dim entry As String = "PCH17" & _delim
                    objWriter.WriteLine(entry)
                    _studiesAndRefnums.Add(entry)
                    entry = "PCH15" & _delim
                    objWriter.WriteLine(entry)
                    _studiesAndRefnums.Add(entry)

                    entry = "PCH13" & _delim
                    objWriter.WriteLine(entry)
                    _studiesAndRefnums.Add(entry)
                    entry = "PCH11" & _delim
                    objWriter.WriteLine(entry)
                    _studiesAndRefnums.Add(entry)

                    entry = "PCH09" & _delim
                    objWriter.WriteLine(entry)
                    _studiesAndRefnums.Add(entry)
                End Using
                Exit Sub
            End If

            Dim priorLoginStudies As New List(Of String)
            'get studies shown in last login
            Using objReader As New IO.StreamReader(path)
                While objReader.EndOfStream = False
                    priorLoginStudies.Add(objReader.ReadLine())
                End While
            End Using
            'add new studies to end of old
            For Each studyItem As String In cboStudy.Items
                Dim found As Boolean = False
                For Each priorStudy As String In priorLoginStudies
                    Dim tempArray() As String = priorStudy.Split(_delim)
                    If studyItem.ToUpper().Trim() = tempArray(0).ToUpper().Trim() Then
                        found = True
                        Exit For
                    End If
                Next
                If Not found Then
                    priorLoginStudies.Add(studyItem & _delim)
                End If
            Next
            'write old and new studies to file
            Using objWriter As New IO.StreamWriter(path, False)
                For Each priorStudy As String In priorLoginStudies
                    objWriter.WriteLine(priorStudy)
                    _studiesAndRefnums.Add(priorStudy)
                Next
            End Using

            Dim tempArray2() As String = _studiesAndRefnums(0).Split(_delim)

            Dim lastStudy As String = tempArray2(0) ' objReader.ReadLine()
            RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            cboStudy.SelectedIndex = cboStudy.FindString(lastStudy)
            AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged

            ChangedStudy()

            If tempArray2(1).Trim().Length > 0 Then
                cboRefNum.Text = tempArray2(1)
                RefNum = tempArray2(1)
                GetRefNumRecord(tempArray2(1))
            End If
        Catch ex As System.Exception
            Dim util As New Utilities
            'MsgBox("Error in ReadLastStudy: (path) " + path + "; Error: " + util.GetBestError(ex))
            db.WriteLog("ReadLastStudy", ex)
        End Try

    End Sub

    Private Sub UpdateStudiesAndRefnumsList(newStudyAndRefnum As String)
        'by now, won't be any studies getting added. just changed refnums
        Try
            'new list to be built
            Dim newList As New List(Of String)

            'split arg into array
            Dim argumentArray() As String = newStudyAndRefnum.Split(_delim)
            Dim count As Integer = 0
            'loop thru _studiesAndRefnums,
            For count = 0 To _studiesAndRefnums.Count - 1
                Dim thisStudyAndRefnum() As String = _studiesAndRefnums(count).Split(_delim)
                'if match same year in both, then
                If thisStudyAndRefnum(0) = argumentArray(0) AndAlso argumentArray(1).Length > 0 Then
                    '  add full arg to new list
                    newList.Add(newStudyAndRefnum)
                Else
                    '  if not match, then put old entry from _studiesAndRefnums into new list
                    newList.Add(_studiesAndRefnums(count))
                End If
            Next
            _studiesAndRefnums = newList

        Catch ex As Exception
            Dim breakPoint As String = String.Empty
        End Try
    End Sub
    Private Sub UpdateStudiesAndRefnumsListOld(newStudyAndRefnum As String)
        'by now, won't be any studies getting added. just changed refnums
        Try
            Dim newList As New List(Of String)
            'For Each studyAndRefnum As String In _studiesAndRefnums
            '    newList.Add(studyAndRefnum)
            'Next
            Dim count As Integer = 0
            Dim argumentArray() As String = newStudyAndRefnum.Split(_delim)
            For Each studyAndRefnum As String In _studiesAndRefnums
                Dim thisStudyAndRefnum() As String = studyAndRefnum.Split(_delim)
                If thisStudyAndRefnum(0) = argumentArray(0) Then
                    newList.Add(argumentArray(0) & _delim & argumentArray(1))
                Else
                    newList.Add(studyAndRefnum)
                End If
                count = count + 1
            Next
            _studiesAndRefnums = newList
        Catch ex As Exception
            Dim breakPoint As String = String.Empty
        End Try
    End Sub
    Private Function GetRefnumWhenChangeStudy(study As String) As String
        Try
            For Each studyAndRefnum In _studiesAndRefnums
                Dim tempArray() As String = studyAndRefnum.Split(_delim)
                If study = tempArray(0) Then
                    Return tempArray(1)
                End If
            Next
            Return String.Empty
        Catch ex As Exception

        End Try
    End Function

    Private Sub SaveLastStudy()
        Try
            If cboRefNum.Text <> "" Then
                If Directory.Exists(_profileConsoleTempPath & "ConsoleSettings\") = False Then
                    GetSettingsFile()
                End If
                Dim textInterval As String = String.Empty
                If _monitoringInterval <> 0.5 Then
                    textInterval = _monitoringInterval.ToString()
                End If
                Using objWriter As New System.IO.StreamWriter(_profileConsoleTempPath & "ConsoleSettings\" & StudyType & textInterval & "Settings.txt", False)
                    For Each studyAndRefnum As String In _studiesAndRefnums
                        objWriter.WriteLine(studyAndRefnum)
                    Next
                End Using
            End If
        Catch ex As System.Exception
            db.WriteLog("SaveLastStudy", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ClearFields()

        'lblName.Text = ""
        'lblEmail.Text = ""
        'lblPhone.Text = ""

        'lblAltName.Text = ""
        'lblAltEmail.Text = ""
        'lblAltPhone.Text = ""




        'txtCompanyPassword.Text = ""


        'txtCorrNotes.Text = ""

        'txtConsultingOpps.Text = ""
        'txtValidationIssues.Text = ""
        'txtCompletedBy.Text = ""
        'txtCompletedOn.Text = ""
        'txtDescription.Text = ""
        'txtIssueName.Text = ""

        'lblBlueFlags.Text = ""
        'lblRedFlags.Text = ""

        'lblItemCount.Text = ""
        'lblItemsRemaining.Text = ""
        'txtReturnPassword.Text = ""
        'lblPricingName.Text = ""
        'lblPricingEmail.Text = ""
        'lblDataCoordinator.Text = ""
        'lblDataEmail.Text = ""

    End Sub
    Private Sub ValFax_Build(strValFaxTemplate As String)

        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.



        Dim strName As String = ""
        Dim row As DataRow = Nothing
        Dim TemplateValues As New SA.Console.Common.WordTemplate()
        'Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""
        Dim intNumDays As Integer

        Dim ErrorLogType As String = String.Empty
        Dim docsFileSystemAccess As New DocsFileSystemAccess(ConsoleVertical, _useMFiles)

        Dim tempFiles As FileInfo() = New DirectoryInfo(_profileConsoleTempPath).GetFiles()
        'Delete all files in temp
        For i = 0 To tempFiles.Length - 1
            Try
                File.Delete(tempFiles(i).FullName)
            Catch
            End Try

        Next

        Try
            Dim VFFileName As String = String.Empty
            If docsFileSystemAccess.ValFax_Build(strValFaxTemplate, UserName, _secureSendTemplateFolder,
            btnValFax.Text, ProfileConsolePath, cboConsultant.Text, RefNum, db, StudyDrive, StudyYear,
            StudyRegion, Study, ProfileConsolePath, CurrentRefNum, CorrPath, TemplateValues, VFFileName,
            GetRefNumCboText(), Get20PlusRefNumCboText()) Then

                'could this to be called by DocsMFilesAccess.vb.Valfax_build()
                WordTemplateReplace(_profileConsoleTempPath & strValFaxTemplate, TemplateValues, VFFileName)
                If Not _useMFiles Then
                    Process.Start(CorrPath & VFFileName)
                End If
            Else
                Throw New Exception("VF file generation failed")
            End If
        Catch ex As System.Exception
            db.WriteLog(docsFileSystemAccess.ErrorLogType, ex)
            MessageBox.Show(ex.Message)
        Finally
            btnValFax.Text = "New IDR"
        End Try
        'MSWord = Nothing
    End Sub
    Public Sub WordTemplateReplace(strfile As String, fields As SA.Console.Common.WordTemplate, strfilename As String, Optional PresentationNote As Boolean = False)

        Dim oWord As Object
        Dim oDoc As Object
        Dim range As Object
        Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
        For count = 0 To fields.Field.Count - 1
            DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
        Next
        MessageBox.Show(DoubleCheck, "Template Field Check")

        'If File.Exists(strfilename) Then ' do this check from MFiles not directory.
        Dim docsFIleSystemAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)

        If Not _useMFiles Then
            If File.Exists(CorrPath & strfilename) Then
                strfilename = InputBox("CAUTION: The suggested file name already exists on the K drive. Blank out this box to avoid overwriting an existing document.", "CAUTION", strfilename)
            Else
                strfilename = InputBox("Here is the suggested name for this new file: " & Chr(10) & "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename)
            End If
        Else
            If Not IsNothing(docsFIleSystemAccess.GetDocInfoByNameAndRefnum(FileNameWithoutExtension(strfilename), GetRefNumCboText(), Get20PlusRefNumCboText(), False)) Then
                strfilename = InputBox("CAUTION: The suggested file name already exists in MFiles. Blank out this box to avoid creating a new version of an existing document.", "CAUTION", strfilename)
            Else
                strfilename = InputBox("Here is the suggested name for this new file: " & Chr(10) & "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename)
            End If
        End If

        ' If they hit cancel, just quit.
        If strfilename = "" Then
            Exit Sub
        End If

        oWord = Utilities.GetWordProcess ' CreateObject("Word.Application")
        If ConfigurationManager.AppSettings("OlefinsWordTemplateReplace-ShowWordDoc").ToString().ToUpper = "TRUE" Then
            oWord.Visible = True
        Else
            oWord.Visible = False
        End If
        oDoc = oWord.Documents.Open(strfile)
        range = oDoc.Content
        Try
            For count = 0 To fields.Field.Count - 1
                With oWord.Selection.Find
                    .Text = fields.Field(count)
                    .Replacement.Text = fields.RField(count)
                    .Forward = True
                    .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                    .Format = False
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
            Next

            Dim deliverable As Integer
            If strfilename.StartsWith("VF") Then  'all will be IDR
                deliverable = 12
            ElseIf Not IsNothing(PresentationNote) AndAlso PresentationNote = True Then
                deliverable = 17  'Presenters Notes
            Else
                deliverable = 4
            End If

            oDoc.SaveAs(_profileConsoleTempPath + strfilename, ADDTORECENTFILES:=True)
            oDoc.Close()
            'Don't close out other open apps  oWord.Quit()
            'oWord = Nothing
            'now upload
            'Dim benchmarkingParticipant As String = Nothing
            'If ConsoleVertical = VerticalType.OLEFINS Then
            '    benchmarkingParticipant = Get20PlusRefNumCboText()
            'End If
            If Not _useMFiles Then
                File.Copy(_profileConsoleTempPath & strfilename, CorrPath & strfilename)
                'oWord.Documents.Open(CorrPath & strfilename) doesn't work here, we are closng the word obj below
            Else
                If docsFIleSystemAccess.UploadDocToMfiles(_profileConsoleTempPath + strfilename, 52, GetRefNumCboText(), deliverable, -1, DateTime.Today) Then
                    'then reopen from MFIles and check out, delete local file at end of this method
                    Dim mFilesFilename As String = FileNameWithoutExtension(strfilename)
                    docsFIleSystemAccess.OpenFileFromMfiles(mFilesFilename, GetRefNumCboText(), Get20PlusRefNumCboText())
                End If
            End If

            Try
                File.Delete(_profileConsoleTempPath + strfilename)
            Catch
                'not critical that it get deleted at this second
            End Try
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        Finally
            'btnValFaxText = "New IDR"
            Try
                'Don't close out other open apps  oWord.Quit()
            Catch ex As Exception
            End Try
            oWord = Nothing
        End Try
    End Sub

    Private Function FileNameWithoutExtension(fileNameWithoutFolder As String)
        Dim nameParts() As String = fileNameWithoutFolder.Split(".")
        Dim result As String = String.Empty
        For count As Integer = 0 To nameParts.Length - 2
            result += nameParts(count) + "."
        Next
        result = result.Remove(result.Length - 1)
        Return result
    End Function

    'Private Function FileNameExtensionOnly(fileNameWithoutFolder As String)
    '    Dim nameParts() As String = fileNameWithoutFolder.Split(".")
    '    Return nameParts(nameParts.Length - 1)
    'End Function


    Function NextVF() As Integer
        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
        Dim result = docsAccess.NextV("VF", GetRefNumCboText(), Get20PlusRefNumCboText())
        If IsNothing(result) Then
            Return 1
        Else
            Return Integer.Parse(result)
        End If
        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 1 To 9
        '    strDirResult = Dir(CorrPath & "\vf" & CStr(I) & "*.*")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        'Return 1
    End Function
    Function NextVR()
        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
        Return docsAccess.NextV("VR", GetRefNumCboText(), Get20PlusRefNumCboText())

        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 0 To 9
        '    strDirResult = Dir(CorrPath & "\vr" & CStr(I) & "*.*")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        'Return Nothing
    End Function
    Function NextReturnFile()
        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
        ', GetRefNumCboText(), Get20PlusRefNumCboText())
        Dim count As Integer? = docsAccess.NextReturnFile(GetRefNumCboText(), Get20PlusRefNumCboText())
        If IsNothing(count) Then
            Return String.Empty
        Else
            Return count.ToString()
        End If


        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 0 To 9
        '    strDirResult = Dir(CorrPath & "*F_Return" & CStr(I) & "*.xls")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        'Return Nothing

    End Function

    Function CacheShellIconByExtension(ByVal fileExtension As String) As String
        ' check if an icon for this key has already been added to the collection
        Try
            If ImageList1.Images.ContainsKey(fileExtension) = False Then
                ImageList1.Images.Add(fileExtension, GetShellIconAsImage(fileExtension))
                If fileExtension = "folder" Then ImageList1.Images.Add(fileExtension & "-open", GetShellOpenIconAsImage(fileExtension))
            End If
        Catch ex As Exception
            fileExtension = String.Empty
        End Try
        Return fileExtension
    End Function

    Function CacheShellIcon(ByVal argPath As String) As String
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            mKey = "folder"
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function



    Sub SetCorr()

        Dim strDirResult As String

        listViewCorrespondenceVF.Items.Clear()

        listViewCorrespondenceVR.Items.Clear()

        listViewCorrespondenceVReturn.Items.Clear()

        'If CInt(StudyYear) = 2015 Then
        '    'SB new code
        '    Dim docsFileSystemAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
        '    Dim items As New List(Of Object)

        '    Try
        '        'need a VF file in correspondence folder for this to get populated
        '        items = docsFileSystemAccess.lstVRFilesItems(CorrPath, lstVFNumbers.SelectedItem.ToString(), GetRefNumCboText())
        '    Catch exlstVRFilesItems As System.Exception

        '    End Try

        '    'If listViewCorrespondenceVF.Columns.Count < 1 Then listViewCorrespondenceVF.Columns.Add("Fax Files (VF*.*)")
        '    'listViewCorrespondenceVF.Items.Clear()
        '    'listViewCorrespondenceVF.View = Windows.Forms.View.Details
        '    'listViewCorrespondenceVF.FullRowSelect = True
        '    'listViewCorrespondenceVF.Columns(0).Width = listViewCorrespondenceVF.Width
        '    'For Each obj As Object In items
        '    '    Dim objArr() As String = Split(obj, " | ")
        '    '    If lstVFNumbers.SelectedItem = Mid(objArr(1), 3, 1) And objArr(1).StartsWith("VF") Then
        '    '        Dim listViewItem As New ListViewItem(obj.ToString())
        '    '        If obj.ToString().Contains("(Checked out to") Then
        '    '            listViewItem.BackColor = Color.Red
        '    '        End If
        '    '        listViewCorrespondenceVF.Items.Add(listViewItem)
        '    '    End If
        '    'Next
        '    PrepCorrListview(listViewCorrespondenceVF, "VF", items, lstVFNumbers.SelectedItem, False)

        '    'For Each obj As Object In items
        '    '    Dim objArr() As String = Split(obj, " | ")
        '    '    If lstVFNumbers.SelectedItem = Mid(objArr(1), 3, 1) And objArr(1).StartsWith("VR") Then
        '    '        lstVRFiles.Items.Add(obj.ToString())
        '    '    End If
        '    'Next

        '    PrepCorrListview(listViewCorrespondenceVR, "VR", items, lstVFNumbers.SelectedItem, False)
        '    PrepCorrListview(listViewCorrespondenceVR, "VA", items, lstVFNumbers.SelectedItem, True)
        '    'For Each obj As Object In items
        '    '    Dim objArr() As String = Split(obj, " | ")
        '    '    If lstVFNumbers.SelectedItem = Mid(objArr(1), 3, 1) And objArr(1).StartsWith("VA") Then
        '    '        lstVRFiles.Items.Add(obj.ToString())
        '    '    End If
        '    'Next

        '    'listViewCorrespondenceVReturn
        '    PrepCorrListview(listViewCorrespondenceVReturn, "Return", items, lstVFNumbers.SelectedItem, False)

        '    'For Each obj As Object In items
        '    '    Dim objArr() As String = Split(obj, " | ")
        '    '    Dim filename As String = objArr(1)
        '    '    If (lstVFNumbers.SelectedItem = Mid(filename, Len(filename) - 4, 1) _
        '    '           Or InStr(1, filename, "Return" & lstVFNumbers.SelectedItem) > 0) Then
        '    '        lstReturnFiles.Items.Add(obj.ToString())
        '    '    End If
        '    'Next




        'Else
        strDirResult = Dir(CorrPath & "VF*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem = Mid(strDirResult, 3, 1) Then
                'lstVFFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                listViewCorrespondenceVF.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
            End If

            strDirResult = Dir()
        Loop
        strDirResult = Dir(CorrPath & "VR*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem = Mid(strDirResult, 3, 1) Then
                'lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                listViewCorrespondenceVR.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
            End If
            strDirResult = Dir()
        Loop
        '------------------
        ' Added 1/30/2006 by FRS
        ' Include any VA files in lstVRFiles so the user can see whether
        ' the VF has been ack'ed.

        strDirResult = Dir(CorrPath & "VA*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem = Mid(strDirResult, 3, 1) Then
                'lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                listViewCorrespondenceVR.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
            End If
            strDirResult = Dir()
        Loop
        '------------------
        strDirResult = Dir(CorrPath & "*Return*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem > "" _
                And (lstVFNumbers.SelectedItem = Mid(strDirResult, Len(strDirResult) - 4, 1) _
                    Or InStr(1, strDirResult, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                'lstReturnFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                listViewCorrespondenceVReturn.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
            End If
            strDirResult = Dir()
        Loop
        'End If
    End Sub
    Function DatePulledFromVAFile(ByVal strDirAndFileName As String) As Date

        Dim strWork As String = ""
        ' Read the second line of the VA file. Get the date from there.
        Dim objReader As New System.IO.StreamReader(strDirAndFileName, True)

        ' Read and toss the first line
        strWork = objReader.ReadLine()
        ' Read the date-time line
        strWork = objReader.ReadLine()

        DatePulledFromVAFile = strWork

    End Function
    Sub ResetGradeTab(intSectionORVersion As Integer)

        Dim intVersion As Integer
        Dim params As New List(Of String)
        Dim rdr As SqlDataReader


        If intSectionORVersion = 1 Then
            ' Load the data by Section.
            ' First determined which version we need to get.
            params.Add("RefNum/" & ParseRefNum(RefNum, 0))

            rdr = db.ExecuteReader("Console.GetSectionGrade", params)
            'strSQL = "SELECT Version FROM Val.SectionGrade WHERE Refnum = '" & gRefineryID & "' ORDER BY Version DESC"
            If rdr.Read() Then
                intVersion = rdr.GetInt16(0)
            End If
            rdr.Close()
            params.Clear()
            params.Add("RefNum/" & ParseRefNum(RefNum, 0))
            params.Add("Version/" & intVersion)

            ds = db.ExecuteStoredProc("Console.GetGradesBySection", params)

            dgGrade.DataSource = ds.Tables(0)

        Else
            ' Load the data by Version.

            params.Clear()
            params.Add("RefNum/" & RefNum)
            params.Add("Version/" & intVersion)

            ds = db.ExecuteStoredProc("Console.GetGradesByVersion", params)

            dgGrade.DataSource = ds.Tables(0)

        End If
        TimeHistory_Load()
        TimeSummary_Load()

    End Sub

    Private Function VChecked(mRefNum As String, mode As String) As Boolean
        Dim ret As Boolean = False
        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("IssueID/" + mode)
        Dim ds As DataSet = db.ExecuteStoredProc("Console." & "IssueCheckV", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0).ToString = "Y" Then Return True
            End If
        End If
        Return False
    End Function
    Private Sub GetDirectories(ByVal subDirs() As DirectoryInfo, ByVal nodeToAddTo As TreeNode)

        Dim aNode As TreeNode
        Dim subSubDirs() As DirectoryInfo
        Dim subDir As DirectoryInfo
        For Each subDir In subDirs
            aNode = New TreeNode(subDir.Name, 0, 0)
            aNode.Tag = subDir
            aNode.ImageKey = "folder"
            subSubDirs = subDir.GetDirectories()
            If subSubDirs.Length <> 0 Then
                GetDirectories(subSubDirs, aNode)
            End If
            nodeToAddTo.Nodes.Add(aNode)
        Next subDir

    End Sub

    'Private Function TryGetExistingExcelApplication() As Excel.Application

    '    Try

    '        Dim o As Object = Marshal.GetActiveObject("Excel.Application")
    '        Return CType(o, Excel.Application)

    '    Catch ex As COMException


    '        Return Nothing
    '    End Try

    'End Function


    'Validation
    Private Sub Validation(Optional choice As String = "")

        Cursor = Cursors.WaitCursor
        Dim xl As Excel.Application
        xl = Utilities.GetExcelProcess() ' TryGetExistingExcelApplication()
        If xl Is Nothing Then
            xl = Utilities.GetExcelProcess() ' New Excel.Application
        End If

        xl.Workbooks.Open(ValidationFile, False, True, , , , , , , , , , , , )

        Try


            xl.Visible = True
            xl.Run("ValidatePlant", choice, False)
            Cursor = Cursors.Default
        Catch ex As System.Exception
            db.WriteLog("Validation: " & CurrentRefNum.ToString, ex)
            MessageBox.Show("Trouble launching " & ValidationFile & ".  Make sure your Macro File is not already opened" & vbCrLf & ex.Message)
        End Try

    End Sub
    'Set all directory paths
    Private Sub SetPaths()
        Dim mRefNum As String = cboRefNum.Text
        StudyRegion = cboStudy.Text.Substring(0, 3)

        GeneralPath = StudyDrive & "General\" & StudyRegion & "\" & ParseRefNum(mRefNum, 1) & StudyRegion & "\"
        ProjectPath = StudyDrive & StudyYear & "\Project\"

        'If Not Directory.Exists(TempDrive & UserWindowsProfileName & "\Console\Temp\") Then Directory.CreateDirectory(TempDrive & UserWindowsProfileName & "\Console\Temp\")
        'TempPath = TempDrive & UserWindowsProfileName & "\Console\Temp\"
        If Not Directory.Exists(_profileConsoleTempPath) Then Directory.CreateDirectory(_profileConsoleTempPath)
        TempPath = _profileConsoleTempPath

        PipelinePath = StudyDrive & StudyYear & "\Plants\" & mRefNum & "\"
        CorrPath = StudyDrive & StudyYear & "\Correspondence\" & mRefNum & "\"
        CompCorrPath = StudyDrive & StudyYear & "\Plants\" & mRefNum & "\"
        CompanyPath = StudyDrive & StudyYear & "\Company\" & CurrentCompany & "\"
        ValidatePath = StudyDrive & StudyYear & "\Programs\Validate\"
        ClientAttachmentsPath = ValidatePath & "Client Attachments\"
        TemplatePath = StudyDrive & StudyYear & "\Correspondence\!Validation\Templates\"

    End Sub
    'Method to remove specific tab from tabcontrol
    Private Sub RemoveTab(tabname As String)
        Dim tab As TabPage
        tab = ConsoleTabs.TabPages(tabname)
        ConsoleTabs.TabPages.Remove(tab)
    End Sub

    Private Function ParseRefNum(mRefNum As String, mode As Integer) As String
        Dim parsed As String = Utilities.GetRefNumPart(mRefNum, mode)
        Return parsed
    End Function

    'Method to save last refnum visited per Study for settings file
    Private Sub SaveRefNum(strStudy As String, strRefNum As String)
        UpdateStudiesAndRefnumsList(strStudy & _delim & strRefNum)
    End Sub
#End Region

#Region "DATABASE LOOKUPS"
    Private Sub UpdateContinuingIssues()

        'Dim params As New List(Of String)
        'params = New List(Of String)
        'params.Add("RefNum/" + RefNum)
        'params.Add("Issue/" & Utilities.FixQuotes(txtContinuingIssues.Text))
        'ds = db.ExecuteStoredProc("Console." & "UpdateContinuingIssues", params)
        
        Throw New Exception("UpdateContinuingIssues - under construction")
        BuildIssuesTab()

    End Sub
    Private Sub UpdateConsultingOpportunities()
        Dim params As New List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Issue/" & Utilities.FixQuotes(txtConsultingOpps.Text))

        ds = db.ExecuteStoredProc("Console." & "UpdateConsultingOpportunities", params)
        BuildIssuesTab()
    End Sub


    Private Function IsValidRefNum(refnum As String) As String
        Dim ret As String = ""
        Dim params = New List(Of String)

        params.Add("@RefNum/" + cboRefNum.Text)
        params.Add("@StudyYear/" + StudyYear.Substring(2, 2))

        Dim c As DataSet = db.ExecuteStoredProc("Console." & "IsValidRefNum", params)
        If c.Tables.Count > 0 Then
            If c.Tables(0).Rows.Count > 0 Then
                ret = c.Tables(0).Rows(0)(0).ToString
            End If
        End If
        Return ret

    End Function
    Private Function FindString(lb As ComboBox, str As String) As Integer
        Dim count As Integer = 0
        For Each item As String In lb.Items
            If item.Contains(str) Then
                Return count
            End If
            count += 1
        Next
    End Function


    Private Sub UpdateConsultant()
        Dim params = New List(Of String)

        params.Add("RefNum/" + ParseRefNum(RefNum, 0))
        params.Add("Consultant/" + cboConsultant.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)
        If c = 0 Then
            MessageBox.Show("Consultant did not save.")
        End If

    End Sub
    Private Sub UpdateIssue(status As String)
        Dim params As List(Of String)
        If Me.tvIssues.SelectedNode IsNot Nothing Then
            Dim node As String = tvIssues.SelectedNode.Text.ToString.Trim
            'params = New List(Of String)
            'params.Add("RefNum/" + RefNum)
            'params.Add("IssueTitle/" & node.Substring(0, node.IndexOf("-") - 1).Trim)
            'params.Add("Status/" & status)
            'params.Add("UpdatedBy/" & UserName)

            'Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateIssue", params)

            Try
                db.SQL = "UPDATE Val.Checklist SET Completed = '" & status & "', SetTime = '" & DateTime.Now &
                    "',SetBy = '" & UserName & "' Where Refnum = '20" & RefNum &
                    "' AND IssueID = '" & node.Substring(0, node.IndexOf("-") - 1).Trim & "'"
                db.Execute()



            Catch ex As Exception
            Finally
                db.SQL = String.Empty
            End Try
        End If
        If status = "Y" Then status = "N" Else status = "Y"
        UpdateCheckList(status)

    End Sub

    Public Sub TimeSummary_Load()

        Dim params As New List(Of String)

        params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        ds = db.ExecuteStoredProc("Console.GetTimeSummary", params)
        dgSummary.DataSource = ds.Tables(0)


    End Sub

    Public Sub TimeHistory_Load()

        Dim params As New List(Of String)

        params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        ds = db.ExecuteStoredProc("Console.GetTimeHistory", params)
        dgHistory.DataSource = ds.Tables(0)

    End Sub
    Private Function FillRefNums() As Integer
        Dim params As List(Of String)
        params = New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("Study/" & cboStudy.SelectedItem.ToString.Substring(0, cboStudy.SelectedItem.ToString.Length - 2))

        ds = db.ExecuteStoredProc("Console." & "GetRefNums_1", params)
        Dim row As DataRow 'Represents a row of data in Systems.data.datatable
        For Each row In ds.Tables(0).Rows
            If row("CoLoc").ToString.Trim.Length > 0 Then
                cboRefNum.Items.Add(row("RefNum").ToString.Trim)
                cboCompany.Items.Add(row("CoLoc").ToString.Trim)
            End If
        Next
        Return ds.Tables(0).Rows.Count
    End Function
    'Method to populate refnums combo box
    Private Sub GetRefNums(Optional strRefNum As String = "")
        Dim NoRefNums As Integer = 0
        Dim selectItem As String = cboStudy.SelectedItem


        StudyYear = selectItem.Substring(selectItem.Length - 2, 2)
        If StudyYear <> CurrentStudyYear Then
            cboStudy.BackColor = Color.Yellow
            lblWarning.Visible = True
        Else
            cboStudy.BackColor = Color.White
            lblWarning.Visible = False
        End If

        Study = selectItem.Substring(0, selectItem.Length - 2)
        If (StudyYear > 50) Then
            StudyYear = "19" + StudyYear
        Else
            StudyYear = "20" + StudyYear
        End If

        PopulateSecureSendTemplateFolder() 'needs Study Year, which gets reset just above here.

        If Not strRefNum Is Nothing Then
            cboRefNum.Items.Clear()
            cboCompany.Items.Clear()

            'Study RefNum Box
            'If _monitoringInterval = 4 Then
            '    'someone moved the data so have to do it outside of proc
            '    Dim dsLocal As DataSet = GetDataset("SELECT substring(Refnum,3,len(refnum)-2) as refnum, CoLoc FROM dbo.TSort WHERE StudyYear = " & StudyYear &
            '                                        " AND StudyID = '" & cboStudy.SelectedItem.ToString.Substring(0, cboStudy.SelectedItem.ToString.Length - 2) &
            '                                        "' And DerivedFrom Is NULL ORDER BY RefNum ASC")
            '    If DataSetHasData(dsLocal) Then
            '        Dim row As DataRow 'Represents a row of data in Systems.data.datatable
            '        For Each row In dsLocal.Tables(0).Rows
            '            If row("CoLoc").ToString.Trim.Length > 0 Then
            '                cboRefNum.Items.Add(row("RefNum").ToString.Trim)
            '                cboCompany.Items.Add(row("CoLoc").ToString.Trim)
            '            End If
            '        Next
            '        NoRefNums = dsLocal.Tables(0).Rows.Count
            '    End If
            '    ''NoRefNums = FillRefNums()
            'ElseIf _monitoringInterval = 0.5 Then
            NoRefNums = FillRefNums()
            'ElseIf _monitoringInterval = 1 Then
            'End If



            If NoRefNums = 0 Then
                MessageBox.Show("There are no records for " & StudyYear & " study.", "Not Found")
                RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
                cboStudy.Text = "PCH17"
                StudyYear = "2017"
                cboStudy.BackColor = Color.White
                lblWarning.Visible = False
                NoRefNums = FillRefNums()
                AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            End If



            If Not strRefNum Is Nothing And strRefNum.Length > 5 Then

                cboRefNum.SelectedIndex = cboRefNum.FindString(strRefNum)
            Else
                If ReferenceNum Is Nothing Then
                    'RefNum = cboRefNum.Items(0).ToString
                    cboRefNum.SelectedIndex = 0
                Else
                    RefNum = ReferenceNum.Trim
                    cboRefNum.SelectedIndex = cboRefNum.FindString(ReferenceNum)
                End If
            End If
        End If
    End Sub


    'Method to retrieve and populate consultants drop down box
    Private Sub GetConsultants()
        Try
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("StudyYear/" + StudyYear)
            params.Add("Study/OLE")
            params.Add("RefNum/" + cboRefNum.Text)
            ds = db.ExecuteStoredProc("Console." & "GetConsultants", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    CurrentConsultant = ds.Tables(0).Rows(0).Item("Consultant").ToString.ToUpper.Trim
                End If
            End If

            cboConsultant.Text = CurrentConsultant


        Catch ex As Exception
            Dim errMsg As String = ex.Message
        End Try

    End Sub

    'Function to retrieve Company name by RefNum
    Private Function GetCompanyName(str As String) As String

        'Get Company Name
        Dim params As List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetCompanyName", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then Return ds.Tables(0).Rows(0)("CoLoc").ToString.Trim
        End If

        ClearFields()
        Return ""

    End Function

    'If exists, this will retrieve Interim Contact info
    Private Sub GetInterimContactInfo()
        InterimContact = New Contacts()
        Dim params As New List(Of String)

        'Check for Interim Contact Info
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetInterimContactInfo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                InterimContact.FirstName = ds.Tables(0).Rows(0).Item("FirstName").ToString
                InterimContact.LastName = ds.Tables(0).Rows(0).Item("LastName").ToString
                InterimContact.Email = ds.Tables(0).Rows(0).Item("Email").ToString
                InterimContact.Phone = ds.Tables(0).Rows(0).Item("Phone").ToString
            End If
        Else
            InterimContact.Clear()
        End If

    End Sub
    ''Method to retrieve Company Contact Information
    'Private Sub GetAltCompanyContactInfo()

    '    AltCompanyContact = New Contacts()
    '    Dim row As DataRow
    '    Dim params As New List(Of String)
    '    params.Add("RefNum/" + cboRefNum.Text)
    '    params.Add("ContactType/ALT")
    '    params.Add("@StudyYear/" & StudyYear)
    '    ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)

    '    If ds.Tables.Count > 0 Then
    '        If ds.Tables(0).Rows.Count > 0 Then

    '            row = ds.Tables(0).Rows(0)

    '            cboConsultant.SelectedItem = CurrentConsultant
    '            AltCompanyContact.ContactType = "ALT"
    '            AltCompanyContact.FirstName = row("FirstName").ToString
    '            AltCompanyContact.LastName = row("LastName").ToString
    '            AltCompanyContact.Email = row("Email").ToString
    '            AltCompanyContact.Phone = row("Phone").ToString
    '            AltCompanyContact.AltFirstName = row("AltFirstName").ToString
    '            AltCompanyContact.AltLastName = row("AltLastName").ToString
    '            AltCompanyContact.AltEmail = row("AltEmail").ToString
    '            'AltCompanyContact.AltPhone = row("PhoneSecondary").ToString
    '            AltCompanyContact.Fax = row("Fax").ToString
    '            AltCompanyContact.MailAdd1 = row("MailAddr1").ToString
    '            AltCompanyContact.MailAdd2 = row("MailAddr2").ToString
    '            AltCompanyContact.MailAdd3 = row("MailAddr3").ToString
    '            AltCompanyContact.MailCity = row("MailCity").ToString
    '            AltCompanyContact.MailState = row("MailState").ToString
    '            AltCompanyContact.MailZip = row("MailZip").ToString
    '            AltCompanyContact.MailCountry = row("MailCountry").ToString
    '            AltCompanyContact.StreetAdd1 = row("StrAddr1").ToString
    '            AltCompanyContact.StreetAdd2 = row("StrAddr2").ToString
    '            AltCompanyContact.StreetAdd3 = row("StrAdd3").ToString
    '            AltCompanyContact.StreetCity = row("StrCity").ToString
    '            AltCompanyContact.StreetState = row("StrState").ToString
    '            AltCompanyContact.StreetZip = row("StrZip").ToString
    '            AltCompanyContact.StreetCountry = row("StrCountry").ToString
    '            AltCompanyContact.Title = row("JobTitle").ToString
    '            AltCompanyContact.Password = row("CompanyPassword").ToString.Trim
    '            CompanyPassword = AltCompanyContact.Password
    '            AltCompanyContact.SendMethod = row("CompanySendMethod").ToString

    '            txtCorrNotes.Text = row("Comment").ToString.Trim



    '        Else
    '            AltCompanyContact.Clear()
    '        End If
    '    End If

    'End Sub
    'Method to retrieve Company Contact Information
    Private Sub GetCompanyContactInfo(studytype As String)



        Dim row As DataRow
        Dim params As New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("ContactType/" & studytype)
        params.Add("StudyYear/" & StudyYear)
        ds = db.GetCompanyContacts(params)

        If studytype = "COORD" Then
            CompanyContact = New Contacts()
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    cboConsultant.SelectedItem = CurrentConsultant


                    CompanyContact.FirstName = row("FirstName").ToString
                    CompanyContact.LastName = row("LastName").ToString
                    CompanyContact.Email = row("Email").ToString
                    CompanyContact.Phone = row("Phone").ToString
                    CompanyContact.Fax = row("Fax").ToString
                    CompanyContact.MailAdd1 = row("MailAddr1").ToString
                    CompanyContact.MailAdd2 = row("MailAddr2").ToString
                    CompanyContact.MailAdd3 = row("MailAddr3").ToString
                    CompanyContact.MailCity = row("MailCity").ToString
                    CompanyContact.MailState = row("MailState").ToString
                    CompanyContact.MailZip = row("MailZip").ToString
                    CompanyContact.MailCountry = row("MailCountry").ToString
                    CompanyContact.StreetAdd1 = row("StrAddr1").ToString
                    CompanyContact.StreetAdd2 = row("StrAddr2").ToString
                    CompanyContact.StreetAdd3 = row("StrAdd3").ToString
                    CompanyContact.StreetCity = row("StrCity").ToString
                    CompanyContact.StreetState = row("StrState").ToString
                    CompanyContact.StreetZip = row("StrZip").ToString
                    CompanyContact.StreetCountry = row("StrCountry").ToString
                    CompanyContact.Title = row("JobTitle").ToString

                    CompanyContact.Password = GetCompanyPassword(cboRefNum.Text) ' row("CompanyPassword").ToString.Trim
                    CompanyPassword = CompanyContact.Password

                    CompanyContact.SendMethod = row("CompanySendMethod").ToString

                    txtCorrNotes.Text = row("Comment").ToString.Trim
                Else
                    CompanyContact.Clear()
                End If




            Else
                CompanyContact.Clear()
            End If
        Else
            AltCompanyContact = New Contacts()
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    AltCompanyContact.FirstName = row("FirstName").ToString
                    AltCompanyContact.LastName = row("LastName").ToString
                    AltCompanyContact.Email = row("Email").ToString
                    AltCompanyContact.Fax = row("Fax").ToString
                    AltCompanyContact.MailAdd1 = row("MailAddr1").ToString
                    AltCompanyContact.MailAdd2 = row("MailAddr2").ToString
                    AltCompanyContact.MailAdd3 = row("MailAddr3").ToString
                    AltCompanyContact.MailCity = row("MailCity").ToString
                    AltCompanyContact.MailState = row("MailState").ToString
                    AltCompanyContact.MailZip = row("MailZip").ToString
                    AltCompanyContact.MailCountry = row("MailCountry").ToString
                    AltCompanyContact.StreetAdd1 = row("StrAddr1").ToString
                    AltCompanyContact.StreetAdd2 = row("StrAddr2").ToString
                    AltCompanyContact.StreetAdd3 = row("StrAdd3").ToString
                    AltCompanyContact.StreetCity = row("StrCity").ToString
                    AltCompanyContact.StreetState = row("StrState").ToString
                    AltCompanyContact.StreetZip = row("StrZip").ToString
                    AltCompanyContact.StreetCountry = row("StrCountry").ToString
                    AltCompanyContact.Title = row("JobTitle").ToString
                    AltCompanyContact.Password = GetCompanyPassword(cboRefNum.Text) ' row("CompanyPassword").ToString.Trim

                    AltCompanyContact.SendMethod = row("CompanySendMethod").ToString


                Else
                    AltCompanyContact.Clear()
                End If

            Else
                AltCompanyContact.Clear()
            End If

        End If


    End Sub

    'Method to retrieve Refinery Contact information
    Private Sub GetPlantContacts()

        Dim row As DataRow
        PlantContact = New Contacts()

        Dim params As New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.GetContacts(params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    Select Case row("ContactTypeID").ToString.ToUpper

                        Case "COORD"
                            PlantContact.FullName = row("NameFull").ToString
                            PlantContact.Phone = row("NumberVoice").ToString
                            PlantContact.Email = row("eMail").ToString
                            PlantContact.Fax = row("numberfax").ToString
                            PlantContact.MailAdd1 = row("addressStreet").ToString
                            PlantContact.MailCity = row("addressCity").ToString
                            PlantContact.MailState = row("addressState").ToString
                            PlantContact.Title = row("NameTitle").ToString
                            PlantContact.StreetZip = row("AddressPostalCode").ToString

                        Case "PRICINGCOORD"
                            PlantContact.PricingName = row("NameFull").ToString
                            PlantContact.PricingEmail = row("eMail").ToString

                        Case "DATACOORD"
                            PlantContact.DCName = row("NameFull").ToString
                            PlantContact.DCEmail = row("eMail").ToString

                    End Select
                Next

            Else
                PlantContact.Clear()
            End If
        End If

        'Change per Mike Brown 2/27/18
        Dim sql As String = "SELECT * FROM STGFACT.TSORT   WHERE REFNUM = '" & cboRefNum.Text & "'"
                Try
            db.SQL = sql
            Dim ds As DataSet = db.Execute()
            If Not ds Is Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count = 1 Then
                If Not IsDBNull(ds.Tables(0).Rows(0)("CoordName")) Then
                    PlantContact.FullName = ds.Tables(0).Rows(0)("CoordName").ToString()
                    PlantCoordName.Text = ds.Tables(0).Rows(0)("CoordName").ToString()
                End If
                If Not IsDBNull(ds.Tables(0).Rows(0)("PricingContact")) Then
                    PlantContact.PricingName = ds.Tables(0).Rows(0)("PricingContact").ToString()
                    PlantCoordName.Text = ds.Tables(0).Rows(0)("PricingContact").ToString()
                End If
                If Not IsDBNull(ds.Tables(0).Rows(0)("DCContact")) Then
                    PlantContact.DCName = ds.Tables(0).Rows(0)("DCContact").ToString()
                    PlantCoordName.Text = ds.Tables(0).Rows(0)("DCContact").ToString()
                End If
                If Not IsDBNull(ds.Tables(0).Rows(0)("Telephone")) Then
                    PlantContact.Phone = ds.Tables(0).Rows(0)("Telephone").ToString()
                End If
            End If

        Catch

        Finally
            db.SQL = String.Empty
        End Try
        




    End Sub
    'Method to retrieve all infomation per Ref Num
    Public Sub GetARefNumRecord(mRefNum As String)

        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum

        'If Refnum is lubes, change variable according to need
        Company = GetCompanyName(mRefNum)
        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)

        StudyRegion = ParseRefNum(mRefNum, 2)
        cboRefNum.Text = CurrentRefNum
        'Main 
        GetConsultants()
        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub
    'Method to retrieve all infomation per Ref Num
    Public Sub GetRefNumRecord(mRefNum As String)
        If IsNothing(mRefNum) Then Exit Sub
        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum

        'If Refnum is lubes, change variable according to need


        StudyRegion = ParseRefNum(mRefNum, 2)
        'Somone moved the data to cons from dbo so now I have to do it this way.
        If _monitoringInterval <> 0.5 Then
            Dim sql As String = "SELECT CoLoc FROM dbo.TSort WHERE Refnum = '20" & mRefNum & "'"
            db.SQL = sql
            ds = db.Execute
            If Not ds Is Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Company = ds.Tables(0).Rows(0)(0).ToString()
            End If
        Else
            Company = GetCompanyName(mRefNum)
        End If

        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        cboCompany.Text = Company
        'cboRefNum.Text = CurrentRefNum
        cboRefNum.Text = CurrentRefNum
        'Main 
        GetConsultants()
        GetCompanyContactInfo("COORD")
        GetCompanyContactInfo("ALT")
        FillContacts()
        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub

    'Retrieve all Refnums per Consultant, Study Type and Study Year for Consultant Tab
    Private Sub SetConsultant(consultant As String)
        tvConsultant.Visible = False
        Dim params As New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("Study/OLE")
        params.Add("Consultant/" & consultant)
        Dim ds As DataSet = db.ExecuteStoredProc("Console." & "GetRefNumsByConsultant", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                tvConsultant.Visible = True
                tvConsultant.Nodes.Clear()
                FillConsultantTree(ds.Tables(0))
            End If
        End If
        cbConsultants.SelectedIndex = cbConsultants.FindStringExact(consultant)
    End Sub

    'Routine to populate the Consultant Treeview with Refnums and outstanding issues
    Private Sub FillConsultantTree(dt As System.Data.DataTable)
        Dim root As TreeNode
        Dim node As TreeNode
        Dim dr As DataRow
        Dim row As DataRow
        Dim count As Integer = 0
        Dim params As List(Of String)
        Dim rn As String
        tvConsultant.Nodes.Clear()
        root = tvConsultant.Nodes.Add(Utilities.Pad(cbConsultants.Text + " " + StudyYear, 60) & Utilities.Pad("VF#", 7) & Utilities.Pad("Days", 7) & "File Date")

        For Each dr In dt.Rows
            rn = dr("RefNum").ToString.Trim
            If rn.Length < 8 Then rn = Space(8 - rn.Length) + rn
            node = root.Nodes.Add(Utilities.Pad(rn & " - " & dr("Coloc"), 59) & _
                                  R3Data(dr("RefNum").ToString.Trim))


            params = New List(Of String)
            params.Add("RefNum/" + dr("RefNum").ToString.Trim)
            params.Add("Completed/N")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables.Count > 0 Then
                For I = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(I)
                        node.Nodes.Add(Utilities.Pad(row("IssueTitle").ToString.Trim, 60))

                    End If
                Next
            End If
            count += 1
        Next
        root.Expand()
    End Sub
    Function R3Data(refnum As String) As String
        Dim strTempCorrPath As String = Nothing
        Dim lstVFNumbers As New List(Of String)
        Dim dteFileDateLatest As Date
        Dim dteFileDate As Date
        Dim strDirResult As String
        Dim blnAlready As Boolean
        Dim I As Integer
        Dim R3 As String = Nothing

        If ConsoleVertical = VerticalType.OLEFINS Then
            'If CInt(StudyYear) = 2015 Then
            '    Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
            '    'Get all V*s
            '    Dim consoleFilesInfo As ConsoleFilesInfo = docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant(refnum, Utilities.GetLongRefnum(refnum))
            '    Dim thisVFConsoleFile As ConsoleFile
            '    For count As Integer = 9 To 1 Step -1
            '        Dim foundVF As Boolean = False
            '        Dim foundVR As Boolean = False
            '        thisVFConsoleFile = Nothing
            '        'check for VFs
            '        For Each consoleFile As ConsoleFile In consoleFilesInfo.Files
            '            If consoleFile.FileName.StartsWith("VF" + count.ToString()) Then
            '                foundVF = True
            '                thisVFConsoleFile = consoleFile
            '                Exit For
            '            End If
            '        Next
            '        'now check for VRs
            '        For Each consoleFile As ConsoleFile In consoleFilesInfo.Files
            '            If consoleFile.FileName.StartsWith("VR" + count.ToString()) Then
            '                foundVR = True
            '                Exit For
            '            End If
            '        Next
            '        ' - if VFcount but no VRcount then
            '        If (foundVF) And (Not foundVR) Then
            '            ' - - get its # and document date
            '            Dim docDate As Date? = thisVFConsoleFile.LastModifiedUtcDate
            '            ' - - calc time since doc date
            '            If Not IsNothing(docDate) Then
            '                ' - - change Modified Date from UTC to CDT and account for DaylightSavingsTime
            '                Dim dateLocal As Date = CDate(docDate).ToLocalTime()
            '                ' - - build R3
            '                'R3 = intHighVF & "      " & DateDiff(DateInterval.Day, dteFileDateLatest, Now) & "       " & Format(dteFileDateLatest, "dd-MMM-yy")
            '                R3 = count.ToString() + "      " + DateDiff(DateInterval.Day, CDate(dateLocal), Now).ToString() + "       " + Format(dateLocal, "dd-MMM-yy")
            '            End If
            '            ' - - return
            '            Exit For
            '        End If
            '        If foundVF Then  'found both
            '            R3 = "        "
            '            Exit For
            '        End If
            '    Next
            '    Return R3
            'End If
        End If

        ' lstVFNumbers is a non-visible list control that I use to
        ' hold and sort the VF numbers.
        strTempCorrPath = BuildCorrPath(refnum)
        strDirResult = Dir(strTempCorrPath & "VF*")
        Do While strDirResult <> ""
            I = 0
            blnAlready = False
            Do While I < lstVFNumbers.Count
                If lstVFNumbers(I) = Mid(strDirResult, 3, 1) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            If Not blnAlready Then
                If IsNumeric(Mid(strDirResult, 3, 1)) Then
                    lstVFNumbers.Add(Mid(strDirResult, 3, 1))
                End If
            End If
            dteFileDate = FileDateTime(strTempCorrPath & strDirResult)
            If dteFileDate > dteFileDateLatest Then
                dteFileDateLatest = dteFileDate
            End If
            strDirResult = Dir()
        Loop

        Dim intHighVF As Integer
        If lstVFNumbers.Count > 0 Then
            If lstVFNumbers(lstVFNumbers.Count - 1) <> "" Then
                intHighVF = lstVFNumbers(lstVFNumbers.Count - 1)
                strDirResult = Dir(strTempCorrPath & "VR" & CStr(intHighVF) & "*")
                If strDirResult = "" Then
                    R3 = intHighVF & "      " & DateDiff(DateInterval.Day, dteFileDateLatest, Now) & "       " & Format(dteFileDateLatest, "dd-MMM-yy")
                Else
                    R3 = "        "
                End If
            End If
        Else
            R3 = "        "
        End If

        Return R3
    End Function
    Private Function BuildCorrPath(refnum As String) As String
        Dim path As String = Nothing


        If Directory.Exists(StudyDrive & StudyYear & "\Correspondence\" & refnum & "\") Then
            path = StudyDrive & StudyYear & "\Correspondence\" & refnum & "\"
        End If


        Return path
    End Function
#End Region

#Region "UI METHODS"

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ConsoleTabs.SelectedIndexChanged
        CurrentTab = ConsoleTabs.SelectedIndex
        LoadTab(CurrentTab)
    End Sub

    Private Sub txtCorrNotes_LostFocus(sender As Object, e As System.EventArgs) Handles txtCorrNotes.LostFocus
        Dim params As New List(Of String)
        If txtCorrNotes.Text.Length > 0 Then
            params = New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            params.Add("Comments/" + txtCorrNotes.Text)
            ' Be sure it does not already exist.
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateComments", params)
        End If
    End Sub
    Private Function SaveNotes(Notes As String, NoteType As String) As Integer
        'IF you are saving a note where the text was already saved once before, it won't resave.
        'The stored procedure does a check for notes in the Val.Notes table, and if it finds the same note there already (also with same consultant, refnum, and note type), then it does not update the table.
        Dim params = New List(Of String)
        params.Add("User/" & UserName)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("NoteType/" & NoteType)
        params.Add("Note/" + Notes.Replace("/", "-"))
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateNote", params)
        Return c
    End Function

    Private Sub txtValidationIssues_LostFocus(sender As Object, e As EventArgs) Handles txtValidationIssues.LostFocus
        'If txtValidationIssues.Text contains slash (/), then
        'command object treats it as delimiter and truncates slash and anything behind it.

        'If txtValidationIssues.Text.Contains("/") Then
        '    If MsgBox("Warning: you have a slash character in the 'IDR Issues/Presenter's Notes' box. This will cause anything after the slash to be lost!" & vbCrLf & "Do you want to change it?", vbYesNo) <> vbNo Then
        '        Exit Sub
        '    End If
        'End If
        If txtValidationIssues.ReadOnly Then Exit Sub

        Dim params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Note/" + txtValidationIssues.Text.Replace("/", "-"))
        params.Add("NoteType/Validation")
        params.Add("UpdatedBy/" + mUserName)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)

        'SaveNotes(txtValidationIssues.Text, "Validation")
        If NotesAreSaved(cboRefNum.Text, "Validation", txtValidationIssues.Text) Then
            lblNotesStatus.Text = "Notes Are Updated"
        Else
            lblNotesStatus.Text = "Notes Are NOT Updated"
        End If
    End Sub

    'Private Sub txtValidationIssues_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtValidationIssues.TextChanged
    '    ValidationIssuesIsDirty = True
    '    txtValidationSlashCheck(txtValidationIssues.Text)
    'End Sub

    'Private Sub txtValidationSlashCheck(theText As String)
    '    'If Me.tabSummary.
    '    If txtValidationIssues.Text.Contains("/") Then
    '        RemoveHandler txtValidationIssues.LostFocus, AddressOf txtValidationIssues_LostFocus
    '        MsgBox("Warning: you have a slash character in the 'IDR Issues/Presenter's Notes' box. This will cause anything after the slash to be lost!")
    '        AddHandler txtValidationIssues.LostFocus, AddressOf txtValidationIssues_LostFocus
    '    End If
    'End Sub

    Private Sub tvwClientAttachments2_DragDrop(sender As Object, e As System.Windows.Forms.DragEventArgs) Handles tvwClientAttachments2.DragDrop
        TreeviewDragDrop(sender, e, ClientAttachmentsPath)
    End Sub

    Private Sub tvIssues_AfterCheck(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterCheck

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""

        Try
            tvIssues.SelectedNode = e.Node

            If e.Node.Checked Then
                UpdateIssue("Y")
            Else

                UpdateIssue("N")
            End If

        Catch ex As System.Exception
            db.WriteLog("tvIssues_AfterCheck:" & RefNum, ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub tvIssues_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterSelect
        Dim IssueID As String = Nothing
        If Not tvIssues.SelectedNode Is Nothing Then


            IssueID = tvIssues.SelectedNode.Text
            IssueID = IssueID.Substring(0, IssueID.IndexOf("-")).Trim
            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueID/" & IssueID)
            'ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)

            Try
                db.SQL = "Select * from Val.CheckList Where Refnum = '20" & RefNum & "' and ((IssueID IS NULL) OR (IssueID = '" & IssueID & "'))"
                ds = db.Execute()
            Catch ex As Exception

            Finally
                db.SQL = String.Empty
            End Try

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString
                    txtPostedBy.Text = row("PostedBy").ToString
                    txtPostedOn.Text = row("PostedTime").ToString
                    txtCompletedBy.Text = row("SetBy").ToString
                    txtCompletedOn.Text = row("SetTime").ToString
                    txtDescription.Text = row("IssueText").ToString

                End If
            End If
        End If

    End Sub

    Private Sub btnPricing_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Pricing", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()
    End Sub

    Private Sub btnDataCoordinator_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "DC", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()
    End Sub


    Dim WithEvents ContactForm As ContactFormPopup

    'Show Contact Popup form
    Private Sub btnShow_Click(sender As System.Object, e As System.EventArgs) Handles btnShow.Click

        ContactForm = New ContactFormPopup(UserName, "COORD", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()

    End Sub

    Private Sub btnShowAltCo_Click(sender As System.Object, e As System.EventArgs) Handles btnShowAltCo.Click
        ContactForm = New ContactFormPopup(UserName, "ALT", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()

    End Sub
    Private Sub btnPlantCoordinator_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Plant", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()
    End Sub

    Private Sub btnShowIntCo_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Interim", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()

    End Sub


    Private Sub btnShowRefCo_Click(sender As System.Object, e As System.EventArgs)

        ContactForm = New ContactFormPopup(UserName, "REFINERY", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()

    End Sub

    Private Sub btnShowRefAltCo_Click(sender As System.Object, e As System.EventArgs)

        ContactForm = New ContactFormPopup(UserName, "ALTREF", RefNum, GetCompanyName(RefNum), StudyType, StudyYear, db)
        ContactForm.Show()

    End Sub
    Private Sub ContactForm_UpdateContact() Handles ContactForm.UpdateContact
        GetCompanyContactInfo("COORD")
        FillContacts()
    End Sub
    Private Sub ContactForm_UpdateAltContact() Handles ContactForm.UpdateAltContact
        GetCompanyContactInfo("ALT")
        FillContacts()
    End Sub
    Private Sub cbConsultants_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbConsultants.SelectedIndexChanged
        SetConsultant(cbConsultants.Text)
    End Sub

    Private Sub tvConsultant_DoubleClick(sender As Object, e As System.EventArgs) Handles tvConsultant.DoubleClick
        Dim refnum As String = ""
        Dim tab As String = cbConsultants.Text

        If Not tvConsultant.SelectedNode Is Nothing Then

            If tvConsultant.SelectedNode.Text.Substring(0, 3) <> cbConsultants.Text Then
                If Not tvConsultant.SelectedNode.Parent Is Nothing Then
                    If tvConsultant.SelectedNode.Parent.Text.Substring(0, 3) = cbConsultants.Text Then
                        refnum = tvConsultant.SelectedNode.Text
                        tab = "tabCompany"
                    Else
                        refnum = tvConsultant.SelectedNode.Parent.Text
                        tab = "tabCheckList"

                    End If
                    refnum = refnum.Substring(0, refnum.IndexOf("-")).Trim

                    cboStudy.Text = ParseRefNum(refnum, 2) & ParseRefNum(refnum, 3)
                    cboRefNum.Text = refnum
                    ConsoleTabs.SelectTab(tab)

                End If
            End If
        End If
    End Sub

    Private Sub btnCopyRetPassword_Click(sender As System.Object, e As System.EventArgs)
        Clipboard.SetText(txtReturnPassword.Text)
    End Sub

    Private Sub btnCopyCompanyPassword_Click(sender As System.Object, e As System.EventArgs) Handles btnCopyCompanyPassword.Click
        Clipboard.SetText(txtCompanyPassword.Text)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Clipboard.SetText(txtReturnPassword.Text)
    End Sub

    Private Sub tvwClientAttachments_DragDrop(sender As Object, e As System.Windows.Forms.DragEventArgs) Handles tvwClientAttachments.DragDrop
        TreeviewDragDrop(sender, e, ClientAttachmentsPath)
    End Sub

    Private Sub tvwCorrespondence2_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles tvwCorrespondence2.AfterCheck
        'For Each tn As TreeNode In tvwCorrespondence2.Nodes
        '    If tn.BackColor = Color.Red Then
        '        tn.Checked = False
        '    End If
        'Next
    End Sub

    Private Sub tvwCorrespondence_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tvwCorrespondence.KeyDown, tvwDrawings.KeyDown, tvwClientAttachments.KeyDown, tvwCompCorr.KeyDown, tvwCorrespondence2.KeyDown, tvwDrawings2.KeyDown, tvwClientAttachments2.KeyDown, tvwCompCorr2.KeyDown
        If e.KeyCode = Keys.Space Then
            Process.Start("C:\")
        End If
    End Sub

    Private Sub cboCompany_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        RemoveHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged
        Dim params = New List(Of String)
        Try
            params.Add("CoLoc/" + cboCompany.Text)
            params.Add("Study/OLE")
            params.Add("StudyYear/" + StudyYear)
            ds = db.ExecuteStoredProc("Console." & "GetRefNumsByCompany", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    cboRefNum.SelectedIndex = cboRefNum.FindString(ds.Tables(0).Rows(0)("RefNum").ToString.Trim)
                    'If _useMFiles Then
                    '    Try
                    '        'check for MFiles error
                    '        _mfilesFailed = False
                    '        Dim mfiles As New DocsMFilesAccess(ConsoleVertical, cboRefNum.Text, "20" + cboRefNum.Text)
                    '    Catch ex As Exception
                    '        If ex.Message.Contains("Unable to find Benchmarking Participant") Then
                    '            _mfilesFailed = True
                    '        End If
                    '        MsgBox("Error in cboRefNum_SelectedIndexChanged, " + ex.Message)
                    '    End Try
                    'End If

                    GetARefNumRecord(cboRefNum.Text)
                End If
            End If

            ' do these for populating ss.CompanyContactName in SecureSend
            GetCompanyContactInfo("COORD")
            GetPlantContacts()
            FillContacts() ' 
            UpdateStudiesAndRefnumsList(cboStudy.Text & _delim & cboRefNum.Text)
            If _useMFiles And _mfilesFailed Then
                MFilesFailedRoutine()
            Else
                UnhideMFilesTabs()
            End If
            AddHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged
        Catch ex As System.Exception
            db.WriteLog("cboCompany_SelectedIndexChanged", ex)
            MessageBox.Show("Error in cboCompany_SelectedIndexChanged: " + ex.Message)
        End Try
    End Sub


    Private Sub btnOpenCorrFolder_Click(sender As System.Object, e As System.EventArgs) Handles btnOpenCorrFolder.Click
        If Not _useMFiles Then
            Process.Start(CorrPath)
        Else
            If _mfilesFailed Then
                MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you.")
                Exit Sub
            End If
            Try
                Dim docsMFilesAccess As New DocsMFilesAccess(VerticalType.OLEFINS, GetRefNumCboText(), Get20PlusRefNumCboText())
                Dim uri As New System.Uri(docsMFilesAccess.GetUrlToOpenMfilesClient(163))
                WebBrowser1.Url = uri
                WebBrowser1.Refresh()
            Catch ex As Exception
                Dim err As String = ex.Message
            End Try
        End If
    End Sub

    Private Sub btnUpdatePresenterNotes_Click(sender As System.Object, e As System.EventArgs)
        Dim params = New List(Of String)

        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Notes/" + txtValidationIssues.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
        If c = 1 Then
            MessageBox.Show("Notes updated successfully")
        Else
            MessageBox.Show("Notes did not update")
        End If
    End Sub


    Private Sub btnUpdateIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateIssue.Click
        txtIssueID.Text = txtIssueID.Text.Trim()
        Dim params As List(Of String)
        ' Be sure they gave us an IssueID and an IssueTitle
        If txtIssueID.Text = "" Or txtIssueName.Text = "" Then
            MsgBox("Please enter both an ID and a Title")
            Exit Sub
        End If

        ' Drop any single quotes in ID, Title, and Text.
        txtIssueID.Text = Utilities.DropQuotes(txtIssueID.Text)
        txtIssueName.Text = Utilities.DropQuotes(txtIssueName.Text)
        txtDescription.Text = Utilities.DropQuotes(txtDescription.Text)

        'find issue ID if they didn't change that. Otherwise, add as new issue.

        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("IssueID/" + txtIssueID.Text)
        ' Be sure it does not already exist.
        'ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)

        Try
            db.SQL = "Select * from Val.CheckList Where Refnum = '20" & RefNum & "' and ((IssueID IS NULL) OR (IssueID = '" & txtIssueID.Text & "'))"
            ds = db.Execute()
        Catch ex As Exception

        Finally
            db.SQL = String.Empty
        End Try


        Dim issueExists As Boolean = False
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                issueExists = True
                'MsgBox("That IssueID already exists for this Refnum.", vbOKOnly)
                'Exit Sub
            End If
        End If


        If Not issueExists Then
            ' Everything looks OK, add it here.
            'Study Combo Box
            params = New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            params.Add("IssueID/" + txtIssueID.Text)
            params.Add("IssueTitle/" + txtIssueName.Text)
            params.Add("IssueText/" + txtDescription.Text)
            If ConsoleVertical = VerticalType.OLEFINS Then
                params.Add("UserName/" + UserName.Substring(0, 3))
            Else
                params.Add("UserName/" + UserName)
            End If

            ds = db.ExecuteStoredProc("Console." & "AddIssue", params)
        Else
            Dim sql As String = "UPDATE Val.Checklist set IssueTitle = '" & txtIssueName.Text & "', IssueText = '" & txtDescription.Text & "', tsModified = '" & DateTime.Now.ToString & "', tsModifiedUser = '" & UserName & "' where IssueID = '" & txtIssueID.Text & "'"
            Try
                db.SQL = sql
                db.Execute()
            Catch ex As Exception
                MsgBox("Error in btnUpdateIssue_Click:" & ex.Message)
            Finally
                db.SQL = String.Empty
            End Try
        End If
        BuildCheckListTab("Incomplete")
        btnAddIssue.Enabled = True
        'txtDescription.ReadOnly = True
    End Sub

    Private Sub btnCancelIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelIssue.Click
        btnAddIssue.Enabled = True
    End Sub


    Private Sub btnSecureSend_Click_1(sender As System.Object, e As System.EventArgs) Handles btnSecureSend.Click
        'tvwCorrespondence2 will hold the MFiles info
        'Dim ss As New SecureSend(db, StudyType, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, _
        '                         tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, Nothing, Nothing)

        Dim ss As New SecureSend(db, StudyType, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, _
                                tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, Nothing, Nothing, Nothing)

        ss.RefNum = RefNum
        'ss.LubeRefNum = LubRefNum
        ss.Study = StudyType
        ss.TemplatePath = TemplatePath
        ss.CorrPath = CorrPath
        ss.DrawingPath = DrawingPath
        ss.ClientAttachmentsPath = ClientAttachmentsPath
        ss.Loc = Company
        ss.User = UserName

        ss.CompCorrPath = CompCorrPath
        ss.CorrPath2 = CorrPath
        ss.DrawingPath2 = DrawingPath
        ss.ClientAttachmentsPath2 = ClientAttachmentsPath

        ss.CompCorrPath2 = CompCorrPath

        ss.TempPath = TempPath

        ss.IDRFile = String.Empty

        ss.ShortRefNum = GetRefNumCboText()
        ss.LongRefNum = Get20PlusRefNumCboText()
        ss.Vertical = ConsoleVertical
        ss.CompanyContactName = PlantCoordName.Text ' change per Jon Bowen 3/20/18 lblName.Text
        ss.Show()
    End Sub


    Private Sub cboConsultant_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles cboConsultant.KeyDown
        Dim params As List(Of String)


        If e.KeyCode = Keys.Enter Then
            cboConsultant.Text = cboConsultant.Text.ToUpper
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Consultant/" & cboConsultant.Text)
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)
            If c <> 1 Then
                MessageBox.Show("Consultant Not Updated.", "Missing TSORT Record")
                cboConsultant.Text = ""
            Else
                cboConsultant.SelectedIndex = cboConsultant.FindString(cboConsultant.Text)
                LoadCboConsultants() 'reload showing new consultanat
            End If
            ConsoleTabs.SelectTab("tabConsultant")
            cbConsultants.Text = cboConsultant.Text
        End If

    End Sub


    Private Sub cboRefNum_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cboRefNum.KeyDown
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True

            cboRefNum.SelectedIndex = FindString(cboRefNum, cboRefNum.Text)

            If cboRefNum.SelectedIndex <> -1 Then
                RefNumChanged(cboRefNum.Text)
            Else
                cboRefNum.SelectedIndex = cboRefNum.FindString(CurrentRefNum)
            End If
        End If
        If _useMFiles And _mfilesFailed Then
            MFilesFailedRoutine()
        Else
            UnhideMFilesTabs()
        End If
    End Sub

    Private Sub btnPA_Click(sender As System.Object, e As System.EventArgs) Handles btnDrawings.Click, btnPA.Click, btnCT.Click, btnHYC.Click, btnPT.Click, btnRAM.Click, btnSC.Click, btnUnitReview.Click, btnSpecFrac.Click, btnVI.Click
        Dim btn As System.Windows.Forms.Button = CType(sender, System.Windows.Forms.Button)
        If Dir(btn.Tag) <> "" Then Process.Start(btn.Tag)
    End Sub

    Private Sub btnSection_Click(sender As System.Object, e As System.EventArgs) Handles btnSection.Click
        If btnSection.Text = "Section" Then
            btnSection.Text = "Revision"
            ResetGradeTab(2)
        Else
            btnSection.Text = "Section"
            ResetGradeTab(1)
        End If
    End Sub
    Private Sub btnFilesSave_Click(sender As System.Object, e As System.EventArgs) Handles btnFilesSave.Click

        'need to check names and see if clash with MFiles docs.

        'get list of to-be filenames 
        Dim consoleCorrespFiles As New List(Of ConsoleFile)
        'Dim consoleCompanyCorrespFiles As New List(Of ConsoleFile)
        'Dim companyBenchmarkRefnum As String = String.Empty
        Dim correspondenceBenchmarkingId As Integer = -1
        'Dim companyCorrBenchmarkingId As Integer = -1
        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
        'Dim nameClash As Boolean = False
        Dim clashingNames As New List(Of String)

        'companyBenchmarkRefnum = GetMfilesCompany()

        'Dim tempConsoleFileInfo = docsAccess.GetDocInfoByNameAndRefnum(companyBenchmarkRefnum, companyBenchmarkRefnum, companyBenchmarkRefnum, False)
        'Dim mfiles As New DocsMFilesAccess(ConsoleVertical, companyBenchmarkRefnum, companyBenchmarkRefnum)
        'If Not IsNothing(mfiles) Then
        '    companyCorrBenchmarkingId = mfiles.BenchmarkingParticipantId
        'End If


        For i = 0 To dgFiles.Rows.Count - 1
            If dgFiles.Rows(i).Cells(2).Value <> "Do Not Save" Then
                Dim gridFileName As String = dgFiles.Rows(i).Cells(1).Value
                If dgFiles.Rows(i).Cells(2).Value = "Correspondence" Then
                    Dim consoleFileInfo As ConsoleFilesInfo
                    If Not _useMFiles Then
                        Try
                            Dim f As New FileInfo(CorrPath & gridFileName)
                            If Not IsNothing(f) AndAlso f.Exists Then
                                consoleFileInfo = New ConsoleFilesInfo(False, Common.VerticalType.OLEFINS)
                                consoleFileInfo.Files = New List(Of ConsoleFile)
                                Dim consoleFile As New ConsoleFile(False)
                                consoleFile.FileName = gridFileName
                                consoleFile.FileExtension = GetExtension(gridFileName)
                                consoleFileInfo.Files.Add(consoleFile)
                            Else
                                consoleFileInfo = Nothing
                            End If
                        Catch ex As Exception
                            consoleFileInfo = Nothing
                        End Try
                    Else
                        consoleFileInfo = docsAccess.GetDocInfoByNameAndRefnum( _
                    Utilities.FileNameWithoutExtension(gridFileName), GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()), False)
                    End If
                    
                    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                        If consoleFileInfo.Files.Count > 0 Then
                            correspondenceBenchmarkingId = consoleFileInfo.BenchmarkingParticipantId
                            For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                                consoleCorrespFiles.Add(consoleFile)
                                If consoleFile.FileExtension.ToUpper = GetExtension(gridFileName).ToUpper() Then '.Substring(gridFileName.Length - 3, 3).ToUpper() Then
                                    clashingNames.Add(consoleFile.FileName)
                                End If
                            Next
                        End If
                        '//consoleFiles.Add(consoleFileInfo.Files(0)) 'prospectiveFileName + _delim + FileInfo.Files(0).Id.ToString)
                    Else
                        Dim newFile As New ConsoleFile(True)
                        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                        newFile.FileExtension = GetExtension(gridFileName)
                        consoleCorrespFiles.Add(newFile)
                    End If
                    'ElseIf dgFiles.Rows(i).Cells(2).Value = "Company" Then
                    '    Dim consoleFileInfo As ConsoleFilesInfo = Nothing
                    '    If companyBenchmarkRefnum.Length > 0 Then
                    '        consoleFileInfo = docsAccess.GetDocInfoByNameAndRefnum( _
                    '        Utilities.FileNameWithoutExtension(gridFileName), companyBenchmarkRefnum, companyBenchmarkRefnum, False)
                    '        If Not IsNothing(consoleFileInfo) Then
                    '            companyCorrBenchmarkingId = consoleFileInfo.BenchmarkingParticipantId
                    '        End If
                    '    Else
                    '        consoleFileInfo = New ConsoleFilesInfo(True)
                    '    End If
                    '    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                    '        For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                    '            If consoleFile.FileExtension.ToUpper = GetExtension(gridFileName).ToUpper() Then
                    '                nameClash = True
                    '                consoleCompanyCorrespFiles.Add(consoleFile)
                    '            End If
                    '        Next
                    '    Else
                    '        Dim newFile As New ConsoleFile(True)
                    '        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                    '        newFile.FileExtension = GetExtension(gridFileName)
                    '        consoleCompanyCorrespFiles.Add(newFile)
                    '    End If
                End If
            End If
        Next
        If clashingNames.Count > 0 Then
            Dim message As String = "You are about to save files with these names: " + Environment.NewLine
            If Not _useMFiles Then
                For Each fileName As String In clashingNames
                    message += "       " + fileName + "," + Environment.NewLine
                Next
                message += " but files with these names already exist on the K Drive." + Environment.NewLine + Environment.NewLine
                message += "Click 'Yes' to overwrite these files, making your files the latest version." + Environment.NewLine
                message += "Click 'No' to go back and make changes to the file names."
            Else
                For Each consoleFile As ConsoleFile In consoleCorrespFiles
                    If consoleFile.Id > 0 Then
                        message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                    End If
                Next
                'For Each consoleFile As ConsoleFile In consoleCompanyCorrespFiles
                '    If consoleFile.Id > 0 Then
                '        message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                '    End If
                'Next
                message += " but files with these names already exist in MFiles." + Environment.NewLine + Environment.NewLine
                message += "Click 'Yes' to overwrite the MFiles, making your files the latest version." + Environment.NewLine
                message += "Click 'No' to go back and make changes to the file names"
            End If


            If MsgBox(message, MsgBoxStyle.YesNo, "Console - Name Clash") <> vbYes Then Exit Sub
        End If
        If Not _useMFiles Then
            DragNDropSaveFiles(consoleCorrespFiles)
        Else
            DragNDropSaveMFiles(consoleCorrespFiles, correspondenceBenchmarkingId, Nothing, Nothing, Nothing)
        End If
    End Sub

    Private Sub DragNDropSaveFiles(consoleFiles As List(Of ConsoleFile))
        Dim CopyFile As String
        Dim Dest As String
        Dim DestFile As String
        Dim ext As String
        Dim success As Boolean = False
        'Dim profileConsolePathTemp As String = ProfileConsolePath + "temp\"


        Try
            'Dim docsMFilesAccessCorrespondence As New DocsMFilesAccess(ConsoleVertical, GetRefNumCboText(), Get20PlusRefNumCboText())

            For i = 0 To dgFiles.Rows.Count - 1
                Dest = dgFiles.Rows(i).Cells(2).Value
                Dim fileName As String = dgFiles.Rows(i).Cells(0).Value
                If Dest <> "Do Not Save" Then
                    txtDragDropStatus.Text = "Saving " & fileName & "...."
                    If Dest = "Correspondence" Then
                        If Not ProcessDragDropFile(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleFiles) Then
                            MsgBox("Error occurred in DragNDropSaveFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                    End If
                Else
                    Try
                        If File.Exists(_profileConsoleTempPath + fileName) Then
                            File.Delete(_profileConsoleTempPath + fileName)
                        ElseIf File.Exists(_profileConsoleExtractedPath + fileName) Then
                            File.Delete(_profileConsoleExtractedPath + fileName)
                        End If
                    Catch
                    End Try
                End If  'end of  If Dest <> "Do Not Save" Then
            Next

            'save email
            'Might not need the the new "save email" method, email shows up in grid with attachments.
            'ProcessDragDropEmail()

            dgFiles.Rows.Clear()
            txtMessageFilename.Text = ""
            txtDragDropStatus.Text = "Attachments Saved"
            DeleteExtractedFolder()
            Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath, "*.*")
            For Each sFile In zfilenames
                Try
                    File.Delete(sFile)
                Catch 'it's not that important. Will get cleared out during next Drag-N-Drop
                End Try
            Next
        Catch ex As System.Exception
            db.WriteLog("DragNDropSaveFiles", ex)
            txtDragDropStatus.Text = "Error saving files: " & ex.Message
        End Try
    End Sub
    Private Sub DragNDropSaveMFiles(consoleFiles As List(Of ConsoleFile), correspondenceBenchmarkingId As Integer,
                                    ByRef companyCorrespondenceFiles As List(Of ConsoleFile), companyCorrBenchmarkingId As Integer,
                                    companyCorrespondenceRefnum As String)
        Dim CopyFile As String
        Dim Dest As String
        Dim DestFile As String
        Dim ext As String
        Dim success As Boolean = False
        'Dim profileConsolePathTemp As String = ProfileConsolePath + "temp\"


        Try
            'If CompanyPassword.Length = 0 Then
            '    MessageBox.Show("Must have a Company Password to Encrypt files", "Save Error")
            '    Exit Sub
            'End If
            Dim docsMFilesAccessCorrespondence As New DocsMFilesAccess(ConsoleVertical, GetRefNumCboText(), Get20PlusRefNumCboText())
            'Dim docsMFilesAccessCompany As DocsMFilesAccess = New DocsMFilesAccess(ConsoleVertical, companyCorrespondenceRefnum, companyCorrespondenceRefnum)

            For i = 0 To dgFiles.Rows.Count - 1
                Dest = dgFiles.Rows(i).Cells(2).Value
                Dim fileName As String = dgFiles.Rows(i).Cells(0).Value
                If Dest <> "Do Not Save" Then
                    txtDragDropStatus.Text = "Saving " & fileName & "...."
                    If Dest = "Correspondence" Then
                        If Not ProcessDragDropMFile(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleFiles, correspondenceBenchmarkingId, docsMFilesAccessCorrespondence) Then
                            MsgBox("Error occurred in DragNDropSaveFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                        'ElseIf Dest = "Company" Then
                        '    If Not ProcessDragDropMFile(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), companyCorrespondenceFiles, companyCorrBenchmarkingId, docsMFilesAccessCompany) Then
                        '        MsgBox("Error occurred in DragNDropSaveFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        '    End If
                    End If
                Else
                    Try
                        If File.Exists(_profileConsoleTempPath + fileName) Then
                            File.Delete(_profileConsoleTempPath + fileName)
                        ElseIf File.Exists(_profileConsoleExtractedPath + fileName) Then
                            File.Delete(_profileConsoleExtractedPath + fileName)
                        End If
                    Catch
                    End Try
                End If  'end of  If Dest <> "Do Not Save" Then
            Next

            'save email
            'mailItem.SaveAs(_profileConsoleTempPath + cboRefNum.Text + ".msg")
            'If Not chkEmailIsCompanyCorr.Checked Then
            '    'save email to REfinery corr
            '    If Not ProcessDragDropMFileEmail(docsMFilesAccessCorrespondence, correspondenceBenchmarkingId) Then
            '        MsgBox("Error occured; email not saved.")
            '    End If
            'Else
            '    'save email to Company  corresp
            '    If Not ProcessDragDropMFileEmail(docsMFilesAccessCompany, companyCorrBenchmarkingId) Then
            '        MsgBox("Error occured; email not saved.")
            '    End If
            'End If

            dgFiles.Rows.Clear()
            txtMessageFilename.Text = ""
            txtDragDropStatus.Text = "Attachments Saved"
            DeleteExtractedFolder()
            Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath, "*.*")
            For Each sFile In zfilenames
                Try
                    File.Delete(sFile)
                Catch 'it's not that important. Will get cleared out during next Drag-N-Drop
                End Try
            Next
        Catch ex As System.Exception
            db.WriteLog("DragNDropSaveFiles", ex)
            txtDragDropStatus.Text = "Error saving files: " & ex.Message
        End Try
    End Sub
    Private Function ProcessDragDropMFile(fileName As String, saveAsFileName As String, _
                                          ByRef filesList As List(Of ConsoleFile), benchmark As Integer, _
                                          ByRef mFilesAccess As DocsMFilesAccess) As Boolean
        Try
            Dim ext As String = String.Empty
            Try
                ext = GetExtension(fileName)
            Catch  'ext = ""
            End Try

            '##### NOTE: FixFilename below will change file extension!!!
            Dim localPath As String = String.Empty
            If File.Exists(_profileConsoleTempPath + fileName) Then
                localPath = _profileConsoleTempPath
            ElseIf File.Exists(_profileConsoleTempPath + fileName) Then
                localPath = _profileConsoleTempPath
            End If
            Dim CopyFile As String = FixFilename(ext, fileName.Trim()) ' will change file extension!!!
            Dim DestFile As String = FixFilename(ext, saveAsFileName)  'will change file extension!!!
            'If Not CopyFile.EndsWith(".msg") Then 'handle it below
            Dim nameClash As Boolean = False
            Dim consoleFileId As Integer = 0
            Dim found As Boolean = False

            For Each consoleFile As ConsoleFile In filesList 'ths could be Corresp or Company
                '1 try to match up Existing Filename with consoleFile
                '2 if not find, then is handled below this For
                'If CopyFile.ToUpper() = consoleFile.FileName.ToUpper() + _
                If DestFile.ToUpper() = consoleFile.FileName.ToUpper() + _
                    "." + consoleFile.FileExtension.ToUpper() Then
                    found = True
                    If consoleFile.Id > 0 Then
                        consoleFileId = consoleFile.Id
                    Else
                        'new file to be uploadd to MFiles. No other action needed.
                    End If
                    Exit For
                End If
            Next

            If Not found Then
                'User must have changed to name in 2nd column so rename and save as that.
                If File.Exists(localPath + fileName) Then
                    File.Move(localPath + fileName, localPath + DestFile.Trim())
                    CopyFile = DestFile.Trim()
                End If
            End If
            If (File.Exists(localPath + fileName)) And (Not File.Exists(localPath + DestFile.Trim())) Then
                'User must have changed to name in 2nd column so rename and save as that.
                File.Move(localPath + fileName, localPath + DestFile.Trim())
                CopyFile = DestFile.Trim()
            End If


            If nameClash Then
                mFilesAccess.UploadNewVersionOfDocToMFiles(localPath & CopyFile, consoleFileId)
            Else 'is new doc, benchmark could be for corresp or for Company.
                mFilesAccess.UploadNewDocToMFiles(localPath & CopyFile, 12, benchmark, Nothing)
            End If
            Try
                File.Delete(localPath + CopyFile)
            Catch exDelete As Exception
                Dim discard As String = String.Empty
            End Try
            Return True
        Catch ex As Exception
            db.WriteLog("Error in ProcessDragDropMFile().", ex)
            Return False
        End Try
    End Function
    Private Function ProcessDragDropFile(fileName As String, saveAsFileName As String, _
                                          ByRef filesList As List(Of ConsoleFile)) As Boolean
        Try
            Dim ext As String = String.Empty
            Try
                ext = GetExtension(fileName)
            Catch  'ext = ""
            End Try

            '##### NOTE: FixFilename below will change file extension!!!
            Dim localPath As String = String.Empty
            If File.Exists(_profileConsoleTempPath + fileName) Then
                localPath = _profileConsoleTempPath
            ElseIf File.Exists(_profileConsoleTempPath + fileName) Then
                localPath = _profileConsoleTempPath
            End If
            Dim CopyFile As String = FixFilename(ext, fileName.Trim()) ' will change file extension!!!
            Dim DestFile As String = FixFilename(ext, saveAsFileName)  'will change file extension!!!
            'If Not CopyFile.EndsWith(".msg") Then 'handle it below
            Dim nameClash As Boolean = False
            Dim consoleFileId As Integer = 0
            Dim found As Boolean = False

            For Each consoleFile As ConsoleFile In filesList 'ths could be Corresp or Company
                '1 try to match up Existing Filename with consoleFile
                '2 if not find, then is handled below this For
                If DestFile.ToUpper() = consoleFile.FileName.ToUpper() Then
                    found = True
                    nameClash = True ' will save as new version of existing file
                    consoleFileId = consoleFile.Id
                    Exit For
                End If
            Next

            If Not found Then
                'User must have changed to name in 2nd column so rename and save as that.
                If File.Exists(localPath + fileName) Then
                    File.Move(localPath + fileName, localPath + DestFile.Trim())
                    CopyFile = DestFile.Trim()
                End If
            End If
            If (File.Exists(localPath + fileName)) And (Not File.Exists(localPath + DestFile.Trim())) Then
                'User must have changed to name in 2nd column so rename and save as that.
                File.Move(localPath + fileName, localPath + DestFile.Trim())
                CopyFile = DestFile.Trim()
            End If

            If nameClash Then
                File.Delete(CorrPath & CopyFile)
            End If
            File.Copy(localPath & CopyFile, CorrPath & CopyFile)
            Try
                File.Delete(localPath + CopyFile)
            Catch exDelete As Exception
                Dim discard As String = String.Empty
            End Try
            Return True
        Catch ex As Exception
            db.WriteLog("Error in ProcessDragDropMFile().", ex)
            Return False
        End Try
    End Function
    Private Function ProcessDragDropEmail() As Boolean
        Try
            Dim emailCorrespFileId As Integer = -1
            Dim msgFileName As String = txtMessageFilename.Text
            If Not msgFileName.ToUpper().EndsWith(".MSG") Then
                msgFileName = msgFileName & ".msg"
            End If
            '
            File.Move(_profileConsoleTempPath + cboRefNum.Text + ".msg", _profileConsoleTempPath & msgFileName)
            If File.Exists(CorrPath & msgFileName) Then 'name clash
                File.Delete(CorrPath & msgFileName)
            End If
            File.Copy(_profileConsoleTempPath & msgFileName, CorrPath & msgFileName)

            Return True
        Catch ex As Exception
            db.WriteLog("Error in ProcessDragDropEmail().", ex)
            Return False
        End Try
    End Function
    Private Function ProcessDragDropMFileEmail(ByRef mfilesAccess As DocsMFilesAccess, benchmark As Integer) As Boolean
        Try
            Dim emailCorrespFileId As Integer = -1
            Dim consoleEmailCorresp As ConsoleFilesInfo = mfilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant(Utilities.GetLongRefnum(cboRefNum.Text))
            For Each consoleMFile As ConsoleFile In consoleEmailCorresp.Files
                If consoleMFile.FileName.ToUpper().Trim() = Utilities.FileNameWithoutExtension(txtMessageFilename.Text.ToUpper.Trim()) Then
                    emailCorrespFileId = consoleMFile.Id
                    Exit For
                End If
            Next
            File.Move(_profileConsoleTempPath + cboRefNum.Text + ".msg", _profileConsoleTempPath + txtMessageFilename.Text)
            If emailCorrespFileId > -1 Then 'name clash
                mfilesAccess.UploadNewVersionOfDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, emailCorrespFileId)
            Else
                mfilesAccess.UploadNewDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, 12, benchmark, Nothing)
            End If
            Return True
        Catch ex As Exception
            db.WriteLog("Error in ProcessDragDropMFileEmail().", ex)
            Return False
        End Try
    End Function
    Private Sub tvwCompCorr_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCompCorr.DragDrop

        TreeviewDragDrop(sender, e, CompCorrPath)

    End Sub
    Private Sub tvwCompCorr2_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCompCorr2.DragDrop

        TreeviewDragDrop(sender, e, CompCorrPath)

    End Sub
    Private Sub tvwCorrespondence2_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCorrespondence2.DragDrop

        TreeviewDragDrop(sender, e, CorrPath)

    End Sub

    Private Sub tvwDrawings2_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwDrawings2.DragDrop

        TreeviewDragDrop(sender, e, DrawingPath)

    End Sub


    Private Sub tvwCorrespondence_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwCorrespondence.DragDrop

        TreeviewDragDrop(sender, e, CorrPath)

    End Sub


    Private Sub tvwCompany_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs)
        TreeviewDragDrop(sender, e, CompanyPath)
    End Sub
    Private Sub TreeviewDragDrop(sender As Object, e As System.Windows.Forms.DragEventArgs, sDir As String)
        Dim dataObject As OutlookDataObject = New OutlookDataObject(e.Data)

        If cboRefNum.Text <> "" Then
            'get the names and data streams of the files dropped
            Dim filenames() As String = CType(dataObject.GetData("FileGroupDescriptorW"), String())
            Dim ans As String
            ans = InputBox(ValidatePath & " : ", "Save File to:", filenames(0).Trim)
            If ans <> "" Then
                Dim filestreams() As MemoryStream = CType(dataObject.GetData("FileContents"), MemoryStream())

                For fileIndex = 0 To filenames.Length - 1

                    'use the fileindex to get the name and data stream
                    Dim filename As String = filenames(fileIndex)
                    Dim filestream As MemoryStream = filestreams(fileIndex)

                    'save the file stream using its name to the application path
                    Dim outputStream As FileStream = File.Create(sDir & "\" & ans)
                    filestream.WriteTo(outputStream)
                    outputStream.Close()
                    SetTree(tvwCompCorr, CompCorrPath)
                Next
            End If
        End If
    End Sub


    Private Sub btnSecureSend_Click(sender As System.Object, e As System.EventArgs)

    End Sub


    Private Sub cboDir_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir.SelectedIndexChanged
        Select Case cboDir.Text

            Case "Plant Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = True

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Client Attachments"
                tvwClientAttachments.Visible = True
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Drawings"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = True
                tvwCompCorr.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = True
        End Select
    End Sub
    Private Sub cboDir2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir2.SelectedIndexChanged
        'Exit Sub 'changing to use MFiles instead.
        Select Case cboDir2.Text

            Case "Plant Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = True

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Client Attachments"
                tvwClientAttachments2.Visible = True
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Drawings"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = True
                tvwCompCorr2.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = True
        End Select
    End Sub

    Private Sub tvwDrawings_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles tvwDrawings.DragDrop
        TreeviewDragDrop(sender, e, DrawingPath)
    End Sub


    Private Sub btnFLComp_Click(sender As System.Object, e As System.EventArgs)
        File.Copy(ValidatePath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls", CorrPath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls")
        Process.Start(CorrPath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls")
    End Sub

    Private Sub btnTP_Click(sender As System.Object, e As System.EventArgs)
        File.Copy(ValidatePath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls", CorrPath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls")
        Process.Start(CorrPath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls")
    End Sub


    Private Sub btnOpenWord_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As Word.Application
        msword = Utilities.GetWordProcess()
        Dim docPN As New Word.Document
        Try
            If Dir(CorrPath & "PN_" & Company & ".doc") = "" Then
                If MsgBox("Do you want to create PN_" & Company & " .doc and stop updating the IDR Notes / Presenter's Notes field through Console?", vbYesNoCancel + vbDefaultButton2, "Click Yes to create the file.") = vbYes Then
                    'MsgBox "Create the file, lock the field, open the file with word."

                    Dim strWork As String = "~ " & Now & " The Presenters Notes document has been built so this field is locked. Please use the Presenters Notes button to open the file. " & Chr(13) & Chr(10) & "----------" & Chr(13) & Chr(10)

                    ' Add the tilde to the Issues Notes field
                    ' First, see if a record exists for this refnum
                    Dim row As DataRow
                    Dim params As New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

                    If ds.Tables.Count > 0 Then
                        row = ds.Tables(0).Rows(0)

                        txtValidationIssues.Text = row("ValidationNotes").ToString


                    Else

                        params = New List(Of String)
                        params.Add("RefNum/" + RefNum)
                        params.Add("Notes/" + txtValidationIssues.Text)

                        Dim Count As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)

                    End If

                    Dim objWriter As New System.IO.StreamWriter(TempPath & "\PNotes.txt", True)
                    objWriter.WriteLine(RefNum)
                    objWriter.WriteLine(txtValidationIssues.Text)
                    objWriter.Close()

                    ' Create the document.
                    File.Copy(TemplatePath & "PN_Template.doc", TempPath & "PN_Template.doc", True)
                    msword = Utilities.GetWordProcess() 'New Word.Application
                    'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                    docPN = msword.Documents.Open(TempPath & "Olefins Study 2013 PN_Template.doc")

                    docPN.SaveAs(CorrPath & "PN_" & Company & ".doc")

                    ' Put the CoLoc and the Notes into the document.
                    msword.Run("SubsAuto")
                End If
            Else

                msword = Utilities.GetWordProcess() ' New Word.Application
                'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                docPN = msword.Documents.Open(CorrPath & "PN_" & Company & ".doc")
                msword.Visible = True
            End If

        Catch Ex As System.Exception
            db.WriteLog("btnOpenWord", Ex)
        End Try


    End Sub


    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        BuildCorrespondenceTab()
    End Sub


    Private Sub OlefinsMainConsole_DragDrop(ByVal sender As Object, _
                        ByVal e As System.Windows.Forms.DragEventArgs) _
                        Handles MyBase.DragDrop, ConsoleTabs.DragDrop
        'DEBUG NOTE: If this isn't firing in the IDE, try closing VS and reopening but NOT "As Administrator"
        '  For further info, see https://stackoverflow.com/questions/68598/how-do-i-drag-and-drop-files-into-an-application

        'If CompanyPassword.Length = 0 Then
        '    MessageBox.Show("Must have a Company Password to Encrypt files", "Save Error")
        '    Exit Sub
        'End If
        'move code out of event
        Dim companyPw As String = GetCompanyPassword(cboRefNum.Text)
        If _useMFiles And _mfilesFailed Then Exit Sub
        OlefinsMainConsoleDragDropRoutine(sender, e, companyPw)
    End Sub

    Private Sub OlefinsMainConsoleDragDropRoutine(ByVal sender As Object, _
                    ByVal e As System.Windows.Forms.DragEventArgs, companyPasword As String)
        Dim strFile As String = String.Empty
        Dim ext As String
        Dim zext As String
        Dim strNextNum = NextVF().ToString
        Dim strNextRetNum = NextReturnFile().ToString
        Dim SavedFiles As New List(Of String)
        Dim strFileName As String
        Dim ReturnFile As Boolean = False

        If cboRefNum.Text <> "" Then
            Dim strNumModifier = FindModifier(strNextNum)
            Try
                Dim filenames() As String = Directory.GetFiles(TempPath, "*.*")
                For Each sFile In filenames
                    Try
                        File.Delete(sFile)
                    Catch ex As System.Exception
                        db.WriteLog("DragDrop", ex)
                        Debug.Print(ex.Message)
                    End Try
                Next

                'Extrcted Files subfolder gets cleared below.

                Me.ConsoleTabs.SelectedIndex = 8

                If dgFiles.Rows.Count = 0 Then
                    If Not e.Data.GetDataPresent(DataFormats.Text) = False Then
                        'check if this is an outlook message. The outlook messages, all contain a FileContents attribute. If not, exit.
                        Dim formats() As String = e.Data.GetFormats()
                        If formats.Contains("FileContents") = False Then Exit Sub

                        'they are dragging the attachment
                        If (e.Data.GetDataPresent("Object Descriptor")) Then
                            Dim app As New Microsoft.Office.Interop.Outlook.Application() ' // get current selected items
                            Dim selection As Microsoft.Office.Interop.Outlook.Selection
                            Dim myText As String = ""
                            selection = app.ActiveExplorer.Selection
                            If selection IsNot Nothing Then

                                Dim mailItem As Outlook._MailItem
                                For i As Integer = 0 To selection.Count - 1

                                    mailItem = TryCast(selection.Item(i + 1), Outlook._MailItem)
                                    'Dim docsMFilesAccess As New DocsMFilesAccess(VerticalType.OLEFINS, GetRefNumCboText(), Get20PlusRefNumCboText())
                                    If mailItem IsNot Nothing Then
                                        Try
                                            'If mailItem.Recipients.Count > 0 Then Solomon = mailItem.Recipients(1).Address.Contains("SOLOMON")
                                            myText = ""
                                            myText = e.Data.GetData("Text")  'header text
                                            myText += vbCrLf + vbCrLf
                                            myText += mailItem.Body  'Plain Text Body Message

                                            'now save the attachments with the same file name and then 1,2,3 next to it
                                            For Each att As Attachment In mailItem.Attachments
                                                If att.FileName.Substring(0, 5).ToUpper <> "IMAGE" Then
                                                    Try
                                                        ext = att.FileName.Substring(att.FileName.LastIndexOf(".") + 1, att.FileName.Length - att.FileName.LastIndexOf(".") - 1)
                                                    Catch
                                                        ext = ""
                                                    End Try

                                                    If ext.ToUpper = "ZIP" Then
                                                        If Dir(TempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(TempPath & "Extracted Files\")
                                                        Dim zfilenames() As String = Directory.GetFiles(TempPath & "Extracted Files", "*.*")
                                                        For Each sFile In zfilenames
                                                            Try
                                                                File.Delete(sFile)
                                                            Catch ex As System.Exception
                                                                db.WriteLog("DragDrop", ex)
                                                                Debug.Print(ex.Message)
                                                            End Try
                                                        Next
                                                        att.SaveAsFile(TempPath & att.FileName)
                                                        Utilities.UnZipFiles(TempPath & att.FileName, TempPath, companyPasword)
                                                        OldTempPath = TempPath
                                                        TempPath = TempPath & "Extracted Files\"
                                                        For Each file In Directory.GetFiles(TempPath)
                                                            file = Utilities.ExtractFileName(file)
                                                            Dim zr = dgFiles.Rows.Add()

                                                            Dim zcbcell = New DataGridViewComboBoxCell
                                                            zcbcell.Items.Add("Do Not Save")
                                                            zcbcell.Items.Add("Correspondence")
                                                            'zcbcell.Items.Add("Company")
                                                            'zcbcell.Items.Add("Drawings")
                                                            'zcbcell.Items.Add("General")
                                                            zcbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                                                            dgFiles.Rows(zr).Cells(2) = zcbcell
                                                            zext = file.Substring(file.LastIndexOf(".") + 1, 3)
                                                            Dim docExtFound As Boolean = False
                                                            Try
                                                                If ConfigurationManager.AppSettings("OlefinsZipDragAndDropFileTypeCorrespondence").ToString().Contains(zext.ToUpper) Then
                                                                    zcbcell.Value = "Correspondence"
                                                                    docExtFound = True
                                                                End If
                                                            Catch Correspex As Exception
                                                                MsgBox("Error reading from config file (OlefinsZipDragAndDropFileTypeCorrespondence)")
                                                                Exit Sub
                                                            End Try

                                                            Try
                                                                If ConfigurationManager.AppSettings("OlefinsZipDragAndDropFileTypeDrawings").ToString().Contains(zext.ToUpper) Then
                                                                    zcbcell.Value = "Drawings"
                                                                    docExtFound = True
                                                                End If
                                                            Catch Correspex As Exception
                                                                MsgBox("Error reading from config file (OlefinsZipDragAndDropFileTypeDrawings)")
                                                                Exit Sub
                                                            End Try
                                                            If Not docExtFound Then
                                                                zcbcell.Value = "General"
                                                            End If
                                                            'Select Case (zext.ToUpper)
                                                            '    Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX"
                                                            '        zcbcell.Value = "Correspondence"
                                                            '    Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG", "VSO"
                                                            'zcbcell.Value = "Drawings"
                                                            '    Case Else
                                                            '        zcbcell.Value = "General"
                                                            'End Select

                                                            dgFiles.Rows(zr).Cells(0).Value = file
                                                            If file.Substring(0, 2).ToUpper() = "VF" Then
                                                                strFile = "VR" & NextVR() & "-" & file
                                                                dgFiles.Rows(zr).Cells(1).Value = "VR" & NextVR() & "-" & file
                                                            Else
                                                                strFile = file.Substring(0, file.Length - 4) & strNextRetNum & "." & zext
                                                                If file.ToUpper.Contains("RETURN") Then
                                                                    dgFiles.Rows(zr).Cells(1).Value = file.Substring(0, file.Length - 4) & strNextRetNum & "." & zext
                                                                Else
                                                                    dgFiles.Rows(zr).Cells(1).Value = file.Substring(0, file.Length - 4) & "." & zext
                                                                End If

                                                            End If
                                                            att.SaveAsFile(strFile)
                                                            'committing the file happens in btnFilesSave_Click()
                                                        Next
                                                    Else

                                                        Dim r = dgFiles.Rows.Add()
                                                        dgFiles.Rows(r).Cells(0).Value = att.FileName
                                                        Dim cbcell = New DataGridViewComboBoxCell
                                                        cbcell.Items.Add("Do Not Save")
                                                        cbcell.Items.Add("Correspondence")
                                                        'cbcell.Items.Add("Company")
                                                        'cbcell.Items.Add("Drawings")
                                                        'cbcell.Items.Add("General")
                                                        cbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                                                        dgFiles.Rows(r).Cells(2) = cbcell
                                                        Dim docExtFound As Boolean = False
                                                        Try
                                                            If ConfigurationManager.AppSettings("OlefinsDragAndDropFileTypeCorrespondence").ToString().Contains(ext.ToUpper) Then
                                                                cbcell.Value = "Correspondence"
                                                                docExtFound = True
                                                            End If
                                                        Catch Correspex As Exception
                                                            MsgBox("Error reading from config file (OlefinsDragAndDropFileTypeCorrespondence)")
                                                            Exit Sub
                                                        End Try
                                                        Try 'ext.ToUpper
                                                            If ConfigurationManager.AppSettings("OlefinsDragAndDropFileTypeDrawings").ToString().Contains(ext.ToUpper) Then
                                                                cbcell.Value = "Drawings"
                                                                docExtFound = True
                                                            End If
                                                        Catch Correspex As Exception
                                                            MsgBox("Error reading from config file (OlefinsDragAndDropFileTypeDrawings)")
                                                            Exit Sub
                                                        End Try
                                                        If Not docExtFound Then
                                                            cbcell.Value = "General"
                                                        End If
                                                        'Select Case (ext.ToUpper.Substring(0, 3))
                                                        '    Case "DOC", "XLS", "PDF", "XL", "TXT", "MSG"
                                                        '        cbcell.Value = "Correspondence"
                                                        '    Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF"
                                                        '        cbcell.Value = "Drawings"
                                                        '    Case Else
                                                        '        cbcell.Value = "General"
                                                        'End Select

                                                        dgFiles.Rows(r).Cells(0).Value = att.FileName
                                                        If att.FileName.Substring(0, 2).ToUpper() = "VF" Then
                                                            ReturnFile = True
                                                            strFile = "VR" & NextVR() & "-" & att.FileName
                                                            dgFiles.Rows(r).Cells(1).Value = "VR" & NextVR() & "-" & att.FileName
                                                        Else
                                                            strFile = att.FileName.Substring(0, att.FileName.Length - ext.Length - 1) & "." & ext
                                                            If strFile.ToUpper.Contains("RETURN") Then
                                                                ReturnFile = True
                                                                dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, att.FileName.Length - ext.Length - 1) & strNextRetNum & "." & ext
                                                            Else
                                                                dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, att.FileName.Length - ext.Length - 1) & "." & ext
                                                            End If
                                                        End If

                                                        'Per Penny, we aren't saving anything to MFiles in an encrypted state
                                                        'moving this below to decrypt before saving:  att.SaveAsFile(TempPath & dgFiles.Rows(r).Cells(0).Value)
                                                        'NEW: unprotect here
                                                        att.SaveAsFile(TempPath & dgFiles.Rows(r).Cells(0).Value)
                                                        Dim utilities As New Utilities
                                                        If Not utilities.RemovePassword(TempPath & dgFiles.Rows(r).Cells(0).Value, GetReturnPassword(RefNum, "olefins"), companyPasword) Then
                                                            ClearDragAndDrop()
                                                            Exit Sub
                                                        End If
                                                        'moving the below up because it seems we have to write the file before unlocking it.
                                                        'att.SaveAsFile(TempPath & dgFiles.Rows(r).Cells(0).Value)
                                                        'committing the file happens in btnFilesSave_Click()
                                                    End If
                                                End If
                                            Next
                                            Dim strEmailFile As String
                                            dgFiles.EditMode = DataGridViewEditMode.EditOnEnter
                                            'save the mail message

                                            'Per Dave, don't remove the attachments
                                            'For attachmentCount As Integer = 1 To mailItem.Attachments.Count
                                            '    mailItem.Attachments.Remove(attachmentCount)
                                            'Next

                                            'strEmailFile = TempPath & "email.msg"
                                            'Dim strw As New StreamWriter(strEmailFile, False)
                                            'strw.WriteLine(myText)
                                            'strw.Close()
                                            'strw.Dispose()
                                            If strNextNum = "" Then
                                                strFileName = cboCompany.Text
                                            Else
                                                strFileName = "VR" & NextVR() & "-" & cboCompany.Text

                                                ' More often, use this technique to name the text file.
                                                If mailItem.Subject Is Nothing Or mailItem.Subject = "" Then
                                                    strFileName = cboCompany.Text
                                                    If ReturnFile Then
                                                        strFileName = "VR" & NextVR() & strFileName
                                                    End If
                                                    txtMessageFilename.Text = strFileName & ".msg" ' ".txt"
                                                Else
                                                    'txtMessageFilename.Text = strFileName & " - " & mailItem.Subject.Replace(":", " ") & ".msg" ' ".txt"
                                                    txtMessageFilename.Text = mailItem.Subject.Replace(":", " ") & ".msg" ' ".txt"
                                                End If

                                            End If

                                            Dim emailFileName As String = txtMessageFilename.Text ' strFileName & ".msg"

                                            mailItem.SaveAs(TempPath & emailFileName)
                                            'committing the file happens in btnFilesSave_Click()

                                            Dim yr = dgFiles.Rows.Add()

                                            Dim ycbcell = New DataGridViewComboBoxCell
                                            ycbcell.Items.Add("Do Not Save")
                                            ycbcell.Items.Add("Correspondence")
                                            'ycbcell.Items.Add("Company")
                                            'ycbcell.Items.Add("Drawings")
                                            'ycbcell.Items.Add("General")
                                            ycbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                                            ycbcell.Value = "Correspondence"
                                            dgFiles.Rows(yr).Cells(2) = ycbcell
                                            Dim yext As String = "msg"

                                            Dim nextRow As Integer = dgFiles.Rows.Count - 1
                                            dgFiles.Rows(nextRow).Cells(0).Value = emailFileName
                                            If emailFileName.Substring(0, 2).ToUpper() = "VF" Then
                                                ReturnFile = True
                                                strFile = "VR" & NextVR() & "-" & emailFileName
                                                dgFiles.Rows(nextRow).Cells(1).Value = "VR" & NextVR() & "-" & emailFileName
                                            Else
                                                strFile = emailFileName.Substring(0, emailFileName.Length - yext.Length - 1) & "." & yext
                                                If strFile.ToUpper.Contains("RETURN") Then
                                                    ReturnFile = True
                                                    dgFiles.Rows(nextRow).Cells(1).Value = strFile.Substring(0, emailFileName.Length - yext.Length - 1) & strNextRetNum & "." & yext
                                                Else
                                                    dgFiles.Rows(nextRow).Cells(1).Value = strFile.Substring(0, emailFileName.Length - yext.Length - 1) & "." & yext
                                                End If
                                            End If
                                        Catch ex As System.Exception
                                            db.WriteLog("DragDrop", ex)
                                        Finally
                                            Marshal.ReleaseComObject(mailItem)
                                        End Try
                                    End If
                                Next
                            End If
                        End If
                    End If
                End If
            Catch ex As System.Exception
                db.WriteLog("DragDrop", ex)
                MessageBox.Show(ex.Message)
            End Try
            If txtMessageFilename.Text.Length > 0 Then btnFilesSave.Enabled = True
        End If
    End Sub

    Private Sub DeleteExtractedFolder()
        If Directory.Exists(_profileConsoleTempPath) = False Then Directory.CreateDirectory(_profileConsoleTempPath)
        Dim extractedFilesFolderPath As String = _profileConsoleTempPath + "Extracted Files\"

        System.Threading.Thread.Sleep(2000)
        If Directory.Exists(extractedFilesFolderPath) Then
            If Dir(extractedFilesFolderPath) <> "" Then
                Dim zfilenames() As String = Directory.GetFiles(extractedFilesFolderPath, "*.*")
                For Each sFile In zfilenames
                    Try
                        File.Delete(sFile)
                    Catch
                        'it's not that important. Will get cleared out during next Drag-N-Drop
                    End Try
                Next
                Try
                    Directory.Delete(extractedFilesFolderPath)
                Catch
                    'it's not that important. Will get cleared out during next Drag-N-Drop
                End Try
            End If
        End If
    End Sub

    Private Sub dgFiles_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFiles.CellEnter
        If e.ColumnIndex = 1 Then
            SendKeys.Send("{F4}")
        End If
        SavedFile = dgFiles.CurrentCell.Value
    End Sub

    Private Sub btnGVClear_Click(sender As System.Object, e As System.EventArgs) Handles btnGVClear.Click
        ClearDragAndDrop()
    End Sub

    Private Sub ClearDragAndDrop()
        dgFiles.RowCount = 1
        dgFiles.Rows.Clear()
        For Each foundFile As String In Directory.GetFiles(TempPath)
            Try
                File.Delete(foundFile)
            Catch
            End Try
        Next
        txtMessageFilename.Text = ""
        btnFilesSave.Enabled = False
    End Sub

    Private Sub tvwCorrespondence_BeforeExpand(sender As System.Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvwCorrespondence.BeforeExpand, tvwDrawings.BeforeExpand, tvwClientAttachments.BeforeExpand, tvwCompCorr.BeforeExpand, tvwCorrespondence2.BeforeExpand, tvwDrawings2.BeforeExpand, tvwClientAttachments2.BeforeExpand, tvwCompCorr2.BeforeExpand
        e.Node.Nodes.Clear()
        ' get the directory representing this node
        Dim mNodeDirectory As IO.DirectoryInfo
        mNodeDirectory = New IO.DirectoryInfo(e.Node.Tag.ToString)
        If IO.Directory.Exists(mNodeDirectory.ToString()) Then
            ' add each subdirectory from the file system to the expanding node as a child node
            For Each mDirectory As IO.DirectoryInfo In mNodeDirectory.GetDirectories
                ' declare a child TreeNode for the next subdirectory
                Dim mDirectoryNode As New TreeNode
                ' store the full path to this directory in the child TreeNode's Tag property
                mDirectoryNode.Tag = mDirectory.FullName
                ' set the child TreeNodes's display text
                mDirectoryNode.Text = mDirectory.Name
                ' add a dummy TreeNode to this child TreeNode to make it expandable
                mDirectoryNode.Nodes.Add("*DUMMY*")
                mDirectoryNode.ImageKey = CacheShellIcon(mDirectoryNode.Tag)
                mDirectoryNode.SelectedImageKey = mDirectoryNode.ImageKey & "-open"
                ' add this child TreeNode to the expanding TreeNode
                e.Node.Nodes.Add(mDirectoryNode)
            Next

            ' add each file from the file system that is a child of the argNode that was passed in
            For Each mFile As IO.FileInfo In mNodeDirectory.GetFiles
                ' declare a TreeNode for this file
                Dim mFileNode As New TreeNode
                ' store the full path to this file in the file TreeNode's Tag property
                mFileNode.Tag = mFile.FullName
                ' set the file TreeNodes's display text
                mFileNode.Text = mFile.Name
                mFileNode.ImageKey = CacheShellIcon(mFileNode.Tag)
                mFileNode.SelectedImageKey = mFileNode.ImageKey
                mFileNode.SelectedImageKey = mFileNode.ImageKey & "-open"
                ' add this file TreeNode to the TreeNode that is being populated
                e.Node.Nodes.Add(mFileNode)
            Next
        Else
            MsgBox("Cannot find path " & mNodeDirectory.ToString())
        End If


    End Sub

    Private Sub tvwCorrespondence_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvwCorrespondence.NodeMouseDoubleClick, tvwDrawings.NodeMouseDoubleClick, tvwClientAttachments.NodeMouseDoubleClick, tvwCompCorr.NodeMouseDoubleClick, tvwCorrespondence2.NodeMouseDoubleClick, tvwDrawings2.NodeMouseDoubleClick, tvwClientAttachments2.NodeMouseDoubleClick, tvwCompCorr2.NodeMouseDoubleClick
        ' try to open the file
        Try
            Dim tagText As String = CStr(e.Node.Tag)
            If Not tagText.Contains("\") Then
                'Is MFiles
                Dim docId As Integer = 0
                If Integer.TryParse(e.Node.Tag, docId) Then
                    Dim docAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
                    Dim result As String = docAccess.OpenFileFromMfiles(docId, GetRefNumCboText(), Get20PlusRefNumCboText())
                    If result.Length > 0 Then
                        MsgBox(result)
                    End If
                Else
                    MsgBox("Unable to find the file to open (" + e.Node.Tag.ToString() + ")")
                End If
            Else
                'not MFiles
                Process.Start(e.Node.Tag)
            End If
        Catch ex As System.Exception
            db.WriteLog("tvwCorrespondence_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try
    End Sub
    Private Sub MainConsole_DragEnter(ByVal sender As Object, _
                           ByVal e As System.Windows.Forms.DragEventArgs) _
                           Handles MyBase.DragEnter, ConsoleTabs.DragEnter, tvwCorrespondence.DragEnter, tvwDrawings.DragEnter, tvwCompCorr.DragEnter, tvwCompCorr2.DragEnter, tvwClientAttachments.DragEnter, tvwCorrespondence2.DragEnter, tvwDrawings2.DragEnter, tvwClientAttachments2.DragEnter

        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub btnAddIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnAddIssue.Click

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""
        txtIssueID.Focus()
        btnAddIssue.Enabled = False
        'btnUpdateIssue.Enabled = True

        'IssueForm.DBConn = mDBConnection
        'IssueForm.RefNum = RefNum
        'IssueForm.StudyType = StudyType
        'IssueForm.UserName = mUserName
        'IssueForm.Show()
        txtDescription.ReadOnly = False

    End Sub


    Private Sub lstVFNumbers_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstVFNumbers.Click

        SetCorr()

    End Sub

    Private Sub btnHelp_Click(sender As System.Object, e As System.EventArgs) Handles btnHelp.Click
        Dim strHelpFilePath As String

        strHelpFilePath = "K:\STUDY\SAI Software\Console2012\"


        Process.Start(strHelpFilePath & "Console 2012.docx")

    End Sub

    Private Sub btnOpenValFax_Click(sender As System.Object, e As System.EventArgs) Handles btnOpenValFax.Click

        ' This routine will look for a personal ValFax for this user.
        ' If it does not exist, give a message to the user.
        ' If it does exist, open it in Word
        Dim docMyValFax As Word.Document

        Dim strTemplateFile As String
        Dim strDirResult As String

        'On Error GoTo MyValFaxError

        ' If they have a "WithSubstitutions" template,
        ' Call ValFax_Build and do not do anything else in this routine.
        strTemplateFile = Dir(TemplatePath & "MyValFax\WithSubstitutions\" & mUserName & ".doc")
        If strTemplateFile > "" Then
            ValFax_Build(strTemplateFile)
            Exit Sub
        End If

        ' No WithSubtitutions template, look for a vanilla template.
        strDirResult = Dir(TemplatePath & "MyValFax\" & mUserName & ".doc")
        If strDirResult = "" Then
            MsgBox("No MyValFax was found for " & mUserName & ". Please create the Word document " & TemplatePath & "MyValFax\" & mUserName & ".doc. See Joe Waters if you have questions.", vbOKOnly)
            'gblnTemplateOK = False
            Exit Sub
        End If

        ' Found a vanilla template, process it.
        strTemplateFile = strDirResult
        Dim MSWord As Word.Application
        MSWord = Utilities.GetWordProcess() ' 
        ' Make Word visible
        MSWord.Visible = True
        ' Open the document
        ' This was .Add which added a new document based on a .dot
        ' Changed it to open followed by .Run of AutoNew.


        ' Open the document
        docMyValFax = MSWord.Documents.Open(TemplatePath & "MyValFax\" & strTemplateFile)

        ' Cut the connection.
        MSWord = Nothing


    End Sub

    Private Sub btnLog_Click(sender As System.Object, e As System.EventArgs)
        ' These are the "file" buttons on the right of the
        ' Validation Summary tab.
        ' Basically, here we just open the path/file stored in the tag
        ' property of the button.
        ' This property was set by SetFileButtons earlier when the Refnum
        ' was set/changed.
        Dim wb As Object
        Dim xl As Excel.Application
        xl = Utilities.GetExcelProcess() ' 


        Try

            xl.Visible = True
            wb = xl.Workbooks.Open("K:\Study\" & "Console." & StudyType & "\" & StudyYear & "\" & cboStudy.SelectedItem.ToString.Substring(0, 3) & "\Refinery\" & RefNum & "\" & RefNum & "_Log.xls")
            xl.Visible = True

            ' Next line is important.
            ' If you don't do this, if the user closes Excel then clicks a file button
            ' again, you will get an Excel window with just the 'frame'. The inside
            ' of the 'window' will be empty!
            xl = Nothing

        Catch ex As System.Exception

            MsgBox("Unable to open file. If you have Excel open, and are editing a cell, get out of that mode and retry. ", vbOKOnly, "Unable to open file")
            db.WriteLog("btnLOG_Click", ex)
        End Try
    End Sub


    Private Sub MainConsole_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Initialize()
        ConsoleTabs.SelectTab("tabSummary")
        lblSummaryFocus.Select() 'do this so focus doesnt' land in textbox, which fires stored proc when textbox loses focus.
    End Sub

    Private Sub MainConsole_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not Spawn Then SaveLastStudy()
        End
    End Sub
    Private Sub BuildGradeGrid()

        Dim params As List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Consultant/" & cboConsultant.SelectedItem)
        ds = db.ExecuteStoredProc("Console." & "GetValidatorData", params)
        If ds.Tables.Count > 0 Then dgGrade.DataSource = ds.Tables(0)

    End Sub

    'Private Sub lstVFFiles_DoubleClick(sender As System.Object, e As System.EventArgs)
    '    Dim file As String
    '    Dim FindTab As String = lstVFFiles.SelectedItem.ToString.IndexOf("|")
    '    file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
    '    If CInt(StudyYear) >= 2015 Then
    '        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS) ', ShortRefNum, "20" + ShortRefNum)
    '        docsAccess.lstVFFiles_DoubleClick(file.Trim(), CorrPath, GetRefNumCboText(), Get20PlusRefNumCboText())
    '    Else
    '        Process.Start(CorrPath & file.Trim)
    '    End If
    'End Sub

    'Private Sub lstVRFiles_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)

    '    Dim file As String
    '    Dim FindTab As String = lstVRFiles.SelectedItem.ToString.IndexOf("|")
    '    file = lstVRFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVRFiles.SelectedItem.ToString.Length - FindTab - 1).Trim()
    '    If CInt(StudyYear) >= 2015 Then
    '        Dim docAccess As New DocsFileSystemAccess(VerticalType.OLEFINS)
    '        Dim result As String = docAccess.OpenFileFromMfiles(FileNameWithoutExtension(file), GetRefNumCboText(), Get20PlusRefNumCboText())
    '        If result.Length > 0 Then
    '            MsgBox(result)
    '        End If
    '    Else
    '        Process.Start(CorrPath & file.Trim)
    '    End If
    'End Sub
    'Private Sub lstReturnFiles_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)

    '    Dim file As String
    '    Dim FindTab As String = lstReturnFiles.SelectedItem.ToString.IndexOf("|")
    '    file = lstReturnFiles.SelectedItem.ToString.Substring(FindTab + 1, lstReturnFiles.SelectedItem.ToString.Length - FindTab - 1).Trim()
    '    If CInt(StudyYear) >= 2015 Then
    '        Dim docAccess As New DocsFileSystemAccess(VerticalType.OLEFINS)
    '        Dim result As String = docAccess.OpenFileFromMfiles(FileNameWithoutExtension(file), GetRefNumCboText(), Get20PlusRefNumCboText())
    '        If result.Length > 0 Then
    '            MsgBox(result)
    '        End If
    '    Else
    '        Process.Start(CorrPath & file.Trim)
    '    End If
    'End Sub

    Private Sub btnReceiptAck_Click(sender As System.Object, e As System.EventArgs) Handles btnReceiptAck.Click

        Dim docsFileSystemAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
        docsFileSystemAccess.btnReceiptAck_Click(Me.lstVFNumbers.Text, mUserName, GetRefNumCboText())

        'All below legacy code moved to  docsFileSystemAccess.btnReceiptAck_Click()
        'Dim strDirResult As String
        'Dim strDateTimeToPrint As String

        'strDirResult = Dir(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt")
        'If strDirResult <> "" Then
        '    MsgBox("Receipt of VF" & Me.lstVFNumbers.Text & " has already been noted.")
        '    Exit Sub
        'End If

        'strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        'If strDateTimeToPrint <> "" Then

        '    Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)

        '    objWriter.WriteLine(mUserName)
        '    objWriter.WriteLine(strDateTimeToPrint)
        '    objWriter.Close()

        '    MsgBox("VA" & Me.lstVFNumbers.Text & ".txt has been built.")
        'Else
        '    MsgBox("Receipt Acknowledgement was canceled.")
        'End If

    End Sub

    Private Sub btnBuildVRFile_Click(sender As System.Object, e As System.EventArgs) Handles btnBuildVRFile.Click
        'changed button visible to False, none of the consultants were using it and didn't know what it was supposed to do.


        'Dim docsFileSystemAccess As New DocsFileSystemAccess(VerticalType.OLEFINS)
        'docsFileSystemAccess.btnBuildVRFile_Click(Me.lstVFNumbers.Text, mUserName, RefNum)



        'all legacy code below moved to DocsFileSystemAccess
        'Dim strDirResult As String
        'Dim strTextForFile As String

        'strDirResult = Dir(CorrPath & "VR" & Me.lstVFNumbers.Text & ".txt")
        'If strDirResult <> "" Then
        '    MsgBox("There already is a VR" & Me.lstVFNumbers.Text & " file.")
        '    Exit Sub
        'End If

        'strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        'Dim docsFileSystemAccess As New DocsFileSystemAccess("Olefins")
        'Dim result As String = docsFileSystemAccess.BuildVRFile(mUserName, strTextForFile, CorrPath,
        '                         Me.lstVFNumbers.Text)
        'MsgBox(result)

    End Sub

    Private Sub btnValFax_Click(sender As System.Object, e As System.EventArgs) Handles btnValFax.Click

        ValFax_Build("")
        BuildCorrespondenceTab()
    End Sub

    Private Sub ValCheckList_ItemCheck(sender As Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles ValCheckList.ItemCheck

        Try
            If e.CurrentValue = CheckState.Checked Then
                e.NewValue = CheckState.Unchecked
                UpdateIssue("N")
            Else
                e.NewValue = CheckState.Checked
                UpdateIssue("Y")
            End If

        Catch ex As System.Exception
            db.WriteLog("ValCheckList_ItemCheck", ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ValCheckList_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ValCheckList.SelectedIndexChanged

        If Not ValCheckList.SelectedItem Is Nothing Then

            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueTitle/" & Me.ValCheckList.SelectedItem.ToString.Trim)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString
                    txtCompletedBy.Text = row("PostedBy").ToString
                    txtCompletedOn.Text = row("PostedTime").ToString
                    txtPostedBy.Text = row("SetBy").ToString
                    txtPostedOn.Text = row("SetTime").ToString
                    txtDescription.Text = row("IssueText").ToString

                End If


            End If

        End If



    End Sub


    Public Sub SetTree(tvw As TreeView, mRootPath As String)
        Try
            tvw.Nodes.Clear()
            Dim mRootNode As New TreeNode
            mRootNode.Text = mRootPath
            mRootNode.Tag = mRootPath
            mRootNode.ImageKey = CacheShellIcon(mRootPath)
            mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
            mRootNode.Nodes.Add("*")
            tvw.Nodes.Add(mRootNode)
            tvwExpand(tvw, 1)
            tvw.Refresh()
        Catch ex As System.Exception
            db.WriteLog("SetTree", ex)
        End Try
    End Sub
    Public Sub tvwExpand(ByVal instance As TreeView, ByVal depth As Integer)
        instance.CollapseAll()
        recursiveExpand(instance.Nodes, depth, 0)
    End Sub

    Private Sub recursiveExpand(ByVal t As TreeNodeCollection, ByVal depth As Integer, ByVal startAt As Integer)
        For x As Integer = startAt To depth - 1
            For Each node As TreeNode In t.Cast(Of TreeNode).Where(Function(n) n.Level = x)
                node.Expand()
                recursiveExpand(node.Nodes, depth, x + 1)
            Next
        Next
    End Sub
    Private Sub btnUpdateNotes_Click(sender As System.Object, e As System.EventArgs)


        Dim bIssues As Boolean = False

        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

        If ds.Tables.Count = 0 Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "InsertValidationNotes", params)

        Else
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "UpdateValidationNotes", params)
        End If

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)


        ds = db.ExecuteStoredProc("Console." & "UpdateComments", params)


        BuildSummaryTab()



    End Sub



    Private Sub btnPrintIssues_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As Word.Application
        msword = Utilities.GetWordProcess() ' 
        Dim docIssues As New Word.Document
        Dim objWriter As New System.IO.StreamWriter(TempPath & "\Issues.txt", True)
        objWriter.WriteLine(RefNum)
        objWriter.WriteLine("Printed by: " & mUserName & " At: " & Now())
        objWriter.WriteLine("")
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.WriteLine("Validation Issues/Presenter's Notes:")
        objWriter.WriteLine(txtValidationIssues.Text)
        objWriter.WriteLine("")
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.WriteLine("Consulting Opportunities:")
        objWriter.WriteLine(txtConsultingOpps.Text)
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.Close()

        ' Set up an object that will be Word.

        docIssues = msword.Documents.Open(TempPath & "Issues.txt")
        docIssues.PrintOut()

        ' Make Word visible
        'MSWord.Visible = True

        docIssues.Close()

        ' Cut the connection.
        msword = Nothing

        MsgBox("The text in the 'IDR Issues / Presenter's Notes' box has been sent to your default printer.")

    End Sub

    Private Sub cboCheckListView_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCheckListView.SelectedIndexChanged
        BuildCheckListTab(cboCheckListView.SelectedItem)
    End Sub

    Private Sub UpdateCheckList(status As String)

        Dim I As Integer
        Dim row As DataRow
        Dim params As List(Of String)
        Dim intCompleteCount As Integer, intNotCompleteCount As Integer
        Dim node As TreeNode
        RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        RemoveHandler cboCheckListView.SelectedIndexChanged, AddressOf cboCheckListView_SelectedIndexChanged
        ValCheckList.Items.Clear()
        tvIssues.Nodes.Clear()

        ' Load the Uncompleted Checklist Item count label
        'params = New List(Of String)
        'params.Add("RefNum/" & cboRefNum.Text)
        'params.Add("Mode/Y")
        'ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)

        Dim mode As String = "Y"
        Try
            db.SQL = "SELECT COUNT(IssueID) as Issues FROM Val.Checklist WHERE Refnum = '20" & cboRefNum.Text &
                "' AND completed = '" & mode & "' or '" & mode & "'='A'"
            ds = db.Execute()
        Catch ex As Exception

        Finally
            db.SQL = String.Empty
        End Try

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        'params = New List(Of String)
        'params.Add("RefNum/" & cboRefNum.Text)
        'params.Add("Mode/N")
        'ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)
        mode = "N"
        Try
            db.SQL = "SELECT COUNT(IssueID) as Issues FROM Val.Checklist WHERE Refnum = '20" & cboRefNum.Text &
                "' AND completed = '" & mode & "' or '" & mode & "'='A'"
            ds = db.Execute()
        Catch ex As Exception

        Finally
            db.SQL = String.Empty
        End Try

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intNotCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        lblItemCount.Text = intCompleteCount & " Completed    " & intNotCompleteCount & " Not Complete."

        Dim chosenView As String = cboCheckListView.Text

        If chosenView = "All" Or chosenView = "Complete" Then '   status = "Y" Or status = "A" Then
            params = New List(Of String)
            params.Add("RefNum/" & cboRefNum.Text)
            params.Add("Completed/Y")
            Select Case _monitoringInterval
                Case 0.5 'study
                    ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
                Case 1, 4 'qrtly
                    Dim sql As String = ""
                    db.SQL = "SELECT * FROM Val.Checklist WHERE Completed = 'Y' and Refnum = '20" & cboRefNum.Text & "' order by IssueID ASC"
                    ds = db.Execute()
            End Select

            If ds.Tables.Count > 0 Then
                For I = 0 To ds.Tables(0).Rows.Count - 1
                    row = ds.Tables(0).Rows(I)
                    node = tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))
                    node.Checked = True
                Next
            End If
            'cboCheckListView.SelectedIndex = 1
        End If

        If chosenView = "All" Or chosenView = "Incomplete" Then '   If status = "N" Or status = "A" Then
            params = New List(Of String)
            If cboRefNum.SelectedItem IsNot Nothing Then
                params.Add("RefNum/" + cboRefNum.SelectedItem.ToString)
            End If
            params.Add("Completed/N")
                Select Case _monitoringInterval
                    Case 0.5 'study
                        ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
                Case 1, 4 'qrtly
                    Dim sql As String = ""
                        db.SQL = "SELECT * FROM Val.Checklist WHERE Completed = 'N' and  Refnum = '20" & cboRefNum.Text & "' order by IssueID ASC"
                        ds = db.Execute()
                End Select

                If ds.Tables.Count > 0 Then
                    For I = 0 To ds.Tables(0).Rows.Count - 1
                        row = ds.Tables(0).Rows(I)
                        tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))
                    Next
                End If
            End If
            AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        AddHandler cboCheckListView.SelectedIndexChanged, AddressOf cboCheckListView_SelectedIndexChanged
    End Sub
    Private Sub SetSummaryFileButtons()
        RefNum = cboRefNum.Text
        If RefNum <> "" Then
            Dim foundFile As String = String.Empty

            'If IsNothing(DrawingPath) Then
            '    DrawingPath = GetDrawingPath()
            'End If
            'If Directory.Exists(DrawingPath) Then
            '    btnDrawings.Enabled = True
            '    btnDrawings.Tag = DrawingPath
            'Else
            '    btnDrawings.Enabled = False
            'End If

            'foundFile = FileExistsVariousExtensions(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_UnitReview.xls")
            'If foundFile.Length > 0 Then  'If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_UnitReview.xls") Then
            '    btnUnitReview.Tag = foundFile 'GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_UnitReview.xls"
            '    btnUnitReview.Enabled = True
            'Else
            '    btnUnitReview.Enabled = False
            'End If

            'foundFile = FileExistsVariousExtensions(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_SF.xls")
            'If foundFile.Length > 0 Then  'If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_SF.xls") Then
            '    btnSpecFrac.Enabled = True
            '    btnSpecFrac.Tag = foundFile 'GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_SF.xls"
            'Else
            '    btnSpecFrac.Enabled = False
            'End If

            'foundFile = FileExistsVariousExtensions(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_HYC.xls")
            'If foundFile.Length > 0 Then  'If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_HYC.xls") Then
            '    btnHYC.Enabled = True
            '    btnHYC.Tag = foundFile 'GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_HYC.xls"
            'Else
            '    btnHYC.Enabled = False
            'End If

            foundFile = FileExistsVariousExtensions(_plantFolder & "20" & RefNum & "_CT.xls")
            If foundFile.Length > 0 Then  'If File.Exists(CorrPath & "20" & RefNum & "_CT.xls") Then
                btnCT.Enabled = True
                btnCT.Tag = foundFile 'CorrPath & "20" & RefNum & "_CT.xls"
            Else
                btnCT.Enabled = False
            End If

            foundFile = FileExistsVariousExtensions(_plantFolder & "20" & RefNum & "_PA.xls")
            If foundFile.Length > 0 Then  'If File.Exists(CorrPath & "20" & RefNum & "_PA.xls") Then
                btnPA.Enabled = True
                btnPA.Tag = foundFile 'CorrPath & "20" & RefNum & "_PA.xls"
            Else
                btnPA.Enabled = False
            End If

            'If File.Exists(CorrPath & "20" & RefNum & "_PT.xls") Then
            foundFile = FileExistsVariousExtensions(CorrPath & "20" & RefNum & "_PT.xls")
            If foundFile.Length > 0 Then
                btnPT.Enabled = True
                btnPT.Tag = foundFile ' CorrPath & "20" & RefNum & "_PT.xls"
            Else
                btnPT.Enabled = False
            End If

            'foundFile = FileExistsVariousExtensions(CorrPath & "20" & RefNum & "_RAM.xls")
            'If foundFile.Length > 0 Then  'If File.Exists(CorrPath & "20" & RefNum & "_RAM.xls") Then
            '    btnRAM.Enabled = True
            '    btnRAM.Tag = foundFile 'CorrPath & "20" & RefNum & "_RAM.xls"
            'Else
            '    btnRAM.Enabled = False
            'End If

            'If File.Exists(CorrPath & "20" & RefNum & "_SC.xls") Then
            foundFile = FileExistsVariousExtensions(CorrPath & "20" & RefNum & "_SC.xls")
            If foundFile.Length > 0 Then
                btnSC.Enabled = True
                btnSC.Tag = foundFile ' CorrPath & "20" & RefNum & "_SC.xls"
            Else
                btnSC.Enabled = False
            End If

            foundFile = FileExistsVariousExtensions(_plantFolder & "20" & RefNum & "_VI.xls")
            If foundFile.Length > 0 Then  'If File.Exists(CorrPath & "20" & RefNum & "_VI.xls") Then
                btnVI.Enabled = True
                btnVI.Tag = foundFile 'CorrPath & "20" & RefNum & "_VI.xls"
            Else
                btnVI.Enabled = False
            End If

        End If

    End Sub

    Private Function FileExistsVariousExtensions(pathToEitherFile As String, Optional extensionsToCheck As String = Nothing) As String
        'expecting extensionsToCheck to be "xls" + delim + "XLSM" etc.  Delim is Chr(14)
        Dim folderPath As String = String.Empty
        Dim pathParts As String() = pathToEitherFile.Split("\")
        For count As Integer = 0 To pathParts.Length - 2
            folderPath += pathParts(count) + "\"
        Next
        Dim temp As String = pathParts(pathParts.Length - 1)
        Dim fileNameNoExtension As String = String.Empty
        Dim nameParts As String() = temp.Split(".")
        For count As Integer = 0 To nameParts.Length - 2
            fileNameNoExtension += nameParts(count) + "."
        Next

        If IsNothing(extensionsToCheck) OrElse extensionsToCheck.Length < 1 Then
            Dim ext As String = nameParts(nameParts.Length - 1)
            Dim foundFile As String = Dir(folderPath + fileNameNoExtension + ext + "*")
            If foundFile <> "" Then
                Return folderPath + foundFile
            Else
                Return String.Empty
            End If
        End If

        ' leave training dot:  fileNameNoExtension.Remove(fileNameNoExtension.Length - 1, 1)
        Dim exts As String() = extensionsToCheck.Split(_delim)
        For count As Integer = 0 To exts.Length - 1
            If File.Exists(folderPath + fileNameNoExtension + exts(count)) Then
                Return folderPath + fileNameNoExtension + exts(count)
            End If
        Next
        Return String.Empty 'not found
    End Function

    Private Sub PopulateCompCorrTreeView(tvw As TreeView)
        If Directory.Exists(CompCorrPath) Then
            SetTree(tvw, CompCorrPath)
        Else
            If StudyDrive.Substring(StudyDrive.Length - 1, 1) = "\" Then
                CompCorrPath = StudyDrive & StudyYear & "\Company\"
            Else
                CompCorrPath = StudyDrive & "\" & StudyYear & "\Company\"
            End If
            SetTree(tvw, CompCorrPath)
        End If

    End Sub

    Private Function GetMfilesCompany9() As String
        Dim result As String = String.Empty
        Dim sql As String = "SELECT CompanyID FROM DBO.TSORT WHERE Refnum = '" + Utilities.GetLongRefnum(cboRefNum.Text.Trim()) + "'"
        db.SQL = sql
        Try
            Dim ds As DataSet = db.Execute()
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0)(0).ToString().Length > 0 Then
                'EUR16Co-EXAMPLE
                result = StudyYear + StudyRegion + "Co-" + ds.Tables(0).Rows(0)(0).ToString().Trim()
            End If
        Catch ex As Exception
            'reset to no data so we don't try to populate list or err while doing it
            result = String.Empty
        End Try
        db.SQL = String.Empty
        Return result
    End Function

    Private Sub PopulateTreeViewWithMfilesFiles(tvw As TreeView) ', Optional companyCorrespondence As Boolean = False)
        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles) ', GetRefNumCboText(), Get20PlusRefNumCboText())
        Dim consoleFilesInfo As ConsoleFilesInfo
        Try
            consoleFilesInfo = docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant( _
                 GetRefNumCboText(), Get20PlusRefNumCboText())
        Catch ex As Exception
            If ex.Message.Contains("Unable to find Benchmarking Participant") Then
                _mfilesFailed = True
            End If
            MsgBox("Error in PopulateTreeViewWithMfilesFiles, " + ex.Message)
        End Try
        If _mfilesFailed Then
            MFilesFailedRoutine()
        Else
            UnhideMFilesTabs()
        End If

        tvw.Nodes.Clear()
        tvw.ImageList = ImageList1
        If Not _mfilesFailed Then
            For Each fil As ConsoleFile In consoleFilesInfo.Files
                tvw.Nodes.Add(PopulateTreeViewNodeWithMfilesFiles(fil))
                tvw.Refresh()
            Next
        End If
    End Sub

    Private Function PopulateTreeViewNodeWithMfilesFiles(fil As ConsoleFile) As TreeNode
        Dim info As String = String.Empty
        Dim checkedOut As Boolean = False
        If fil.CheckedOutTo > 0 Then
            info = "(" + fil.CheckedOutToUserName + ") "
            checkedOut = True
        End If
        info += fil.FileName + "." + fil.FileExtension
        Dim tn As New TreeNode(info)
        If checkedOut Then
            tn.BackColor = Color.Red
        End If
        tn.Tag = fil.Id.ToString()
        Try
            tn.ImageIndex = 0
            'tn.ImageKey = "MFiles"
        Catch exImage As System.Exception
        End Try
        Return tn
    End Function

    Private Sub PopulateCorrespondenceTreeView(tvw As TreeView)

        If Directory.Exists(CorrPath) Then
            SetTree(tvw, CorrPath)
        Else
            If cboRefNum.Text.ToUpper.Contains("SEEC") Then
                StudyDrive = "K:\STUDY\SEEC\"
                CorrPath = StudyDrive & StudyYear & "\Correspondence\" & cboRefNum.Text
            Else
                CorrPath = StudyDrive & StudyYear & "\Correspondence\" & cboRefNum.Text
            End If

            SetTree(tvw, CorrPath)
            End If

    End Sub


    Private Sub PopulateDrawingTreeView(tvw As TreeView)

        'If Not Directory.Exists(DrawingPath) Then
        '    DrawingPath = GetDrawingPath() ' & "Console." & StudyType & "\" & StudyRegion & "\General\"
        'End If
        'SetTree(tvw, DrawingPath)
    End Sub
    Private Sub PopulateCATreeView(tvw As TreeView)

        If Directory.Exists(ClientAttachmentsPath) Then
            SetTree(tvw, ClientAttachmentsPath)
        End If

    End Sub

    Private Sub UploadFile(fn As String)

        Dim upload As New ChemUpLoad
        Cursor = Cursors.WaitCursor
        upload.UpLoadChemFile(CorrPath & fn)
        Cursor = Cursors.Default
        If upload.EtlErrors.Count > 0 Then
            For Each e As String In upload.EtlErrors
                MessageBox.Show("Error: " & e, "Upload Error")
            Next
        Else
            MessageBox.Show("Upload of " & fn & " Successful")
        End If




    End Sub

    'Private Sub btnValidate_Click(sender As System.Object, e As System.EventArgs)
    '    Dim choice As String = InputBox("Enter Refnum: ", "", cboRefNum.Text)
    '    LaunchValidation(choice, _pathToLastDatachecksFile)
    'End Sub

    Private Sub LaunchDataChecks(choice As String, pathToDatachecksFile As String)
        Try


            If cboConsultant.Text = "" Then
                MessageBox.Show("You have not assigned a consultant.")
            Else

                'Launch Validation
                ValidationFile = pathToDatachecksFile ' "K:\STUDY\Olefins\2013\Programs\Validate\Olefins DataChecks.xls"
                'ValidationFile = "FL" & StudyYear.Substring(2, 2) & "Macros.xls"

                Validation(choice)

                'If pathToDatachecksFile = _pathToPriorDatachecksFile Then
                '    btnValidate.Enabled = True
                '    btnValidate.Text = "2013 DataChecks"
                'ElseIf pathToDatachecksFile = _pathToCurrentDatachecksFile Then
                '    btnValidateUsingCurrentYearDatachecks.Enabled = True
                '    btnValidateUsingCurrentYearDatachecks.Text = "2015 DataChecks"
                'End If

            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "LaunchDataChecks")
            db.WriteLog("LaunchDataChecks", ex)
        End Try

    End Sub
    Private Sub ChangedStudy()
        'here, get refnum from _studiesAndRefnums and call GetRefNums on it
        Dim refNumToUse As String = GetRefnumWhenChangeStudy(cboStudy.Text)
        GetRefNums(refNumToUse)
    End Sub

    'If Study Changes, then re-populate RefNums
    Private Sub cboStudy_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboStudy.SelectedIndexChanged
        Try
            Dim year As String = String.Empty
            Dim lastChosenRefNum As String = String.Empty
            If cboStudy.Text.Trim().Length > 2 Then
                year = cboStudy.Text.Substring(cboStudy.Text.Length - 2, 2)
            End If
            _useMFiles = False
            _mfilesFailed = False
            If cboRefNum.Text.Length > 2 Then
                UpdateStudiesAndRefnumsList(_lastChosenRefNum & _delim & lastChosenRefNum)
                'For i As Integer = 0 To 20
                '    If StudySave(i).Contains(year) Then
                '        lastChosenRefNum = RefNumSave(i)
                '        Exit For
                '    End If
                'Next
            End If


            ChangedStudy()


            ''this logic is done in ChangedStudy() above
            'For count As Integer = 0 To cboRefNum.Items.Count - 1
            '    'Dim a As String = cboRefNum.Items(count).ToString().ToUpper()
            '    'Dim b = cboRefNum.Text.Length
            '    'Dim c = a.Substring(2, b - 3)
            '    Dim localRefNum As String = lastChosenRefNum.ToString().ToUpper()
            '    Dim CompanyNumber As String = localRefNum.Substring(2, localRefNum.Length - 2)

            '    'If year + cboRefNum.Items(count).ToString().ToUpper().Substring(2, cboRefNum.Text.Length - 3) = year + lastChosenRefNum.ToUpper() Then
            '    If year + CompanyNumber = lastChosenRefNum.ToUpper() Then
            '        lastChosenRefNum = year + lastChosenRefNum
            '        cboRefNum.Text = lastChosenRefNum
            '        Exit For
            '    End If
            'Next

            _lastChosenRefNum = cboStudy.Text

        Catch ex As Exception
            MsgBox("Error in cboStudy_SelectedIndexChanged, " + ex.Message)
        End Try
    End Sub

    'Method to populate the labels for Contacts
    Private Sub FillContacts()

        ClearFields()


        If Not PlantContact Is Nothing Then
            PlantCoordName.Text = PlantContact.FullName
            PlantCoordEmail.Text = PlantContact.Email
            PlantCoordPhone.Text = PlantContact.Phone
            lblPricingName.Text = PlantContact.PricingName
            lblPricingEmail.Text = PlantContact.PricingEmail
            lblDataCoordinator.Text = PlantContact.DCName
            lblDataEmail.Text = PlantContact.DCEmail
        End If


        lblName.Text = CompanyContact.FirstName & " " & CompanyContact.LastName
        lblEmail.Text = CompanyContact.Email
        lblPhone.Text = CompanyContact.Phone
        txtCompanyPassword.Text = GetCompanyPassword(RefNum)



        lblAltName.Text = AltCompanyContact.FirstName & " " & AltCompanyContact.LastName
        lblAltEmail.Text = AltCompanyContact.Email
        lblAltPhone.Text = AltCompanyContact.Phone



        txtReturnPassword.Text = GetReturnPassword(Trim(UCase(RefNum)), "olefins")
        ReturnPassword = txtReturnPassword.Text

    End Sub
    <Obsolete("Been incorrectly implemented, change for 2013 Olefins and 2014 Fuels", False)>
    Private Function GetReturnPassword(RefNum As String, salt As String) As String
        Return Utilities.XOREncryption(Trim(UCase(RefNum)), salt)
    End Function
    Private Function GetCompanyPassword(refNumber As String) As String
        Dim pw As String = ""

        ''Legacy, didn't give correct pw:
        'Dim params = New List(Of String)
        'params.Add("RefNum/" + mRefNum)
        'ds = db.ExecuteStoredProc("Console." & "GetCompanyPassword", params)
        'If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
        '    pw = ds.Tables(0).Rows(0)("CompanyPassword").ToString
        'End If

        ''new query to use a variant of:
        'select 
        'Password_plaintext 
        'FROM dbo.TSort t
        '		inner JOIN dim.Company_LookUp k
        '			ON	k.CompanyID		= etl.ConvCompanyID(t.CompanyID, t.PlantCompanyName, t.Co)
        '			/*
        '		inneR JOIN stgFact.CoContactInfo c
        '			ON	c.ContactCode	= t.ContactCode
        '			AND	c.StudyYear		= t.StudyYear
        '			AND	c.[Password]	IS NOT NULL
        '			*/
        '		inner JOIN cons.SubscriptionsCompanies sc
        '			ON	sc.StudyID		= @StudyId
        '			AND	sc.CompanyID	= k.CompanyID
        '			and sc._studyYear = t.Studyyear

        'where t.AssetID = '998' --  in('997','998','999')
        'and t.studyyear = 2015;

        'Dim studyType As String = String.Empty
        'Dim assetId As String = String.Empty

        'If Int32.Parse(StudyYear) <= 2015 Then
        '    'format 15PCH998   yr,PCH,assetid
        '    studyType = refNumber.Substring(2, 3)
        '    assetId = refNumber.Substring(5, 3)
        'End If
        'If refNumber.StartsWith("20") Then
        '    refNumber = refNumber.Substring(2, refNumber.Length)
        'End If

        Dim sql As String = "select C.Password  from dbo.CoContactInfo C"
        sql += "  right outer join dbo.tsort t on t.contactcode=C.contactcode"
        sql += " and t.studyyear=C.studyyear"
        sql += " where t.smallRefNum = '" + refNumber + "'"
        sql += " and C.ContactType = 'COORD'"
        sql += " and C.studyYear = '" + StudyYear + "'"

        db.SQL = sql
        Dim reader As SqlDataReader = db.ExecuteReader()
        If Not IsNothing(reader) Then
            If reader.HasRows Then
                reader.Read()
                pw = reader.Item("Password").ToString.Trim
            End If
        End If
        Try
            reader.Close()
        Catch
        End Try
        db.SQL = String.Empty
        Return pw
    End Function
    Public Sub RefNumChanged(Optional mRefNum As String = "")
        CompanyPassword = GetCompanyPassword(mRefNum)
        PopulatePlantFolder(mRefNum)

        RemoveHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
        SetTabDirty(True)
        BuildDirectoriesTab()
        BuildIssuesTab()
        cboRefNum.Text = mRefNum
        GetRefNumRecord(mRefNum)
        SaveRefNum(cboStudy.Text, mRefNum)

        AddHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
        If mRefNum.Length > 0 Then RefNum = mRefNum
        'TabControl1.SelectedIndex = 0

    End Sub
    Private Sub cboRefNum_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboRefNum.SelectedIndexChanged
        Try
            If _monitoringInterval = 4 Then
                StudyDrive = "K:\STUDY\OlefinsCQM\"
            ElseIf _monitoringInterval = 0.5 Then '2 year = study
                StudyDrive = "K:\STUDY\Olefins\"
            ElseIf _monitoringInterval = 1 Then
                StudyDrive = "K:\STUDY\SEEC\"
            End If
            _mfilesFailed = False
            UpdateStudiesAndRefnumsList(cboStudy.Text & _delim & cboRefNum.Text)
            RefNumChanged(cboRefNum.Text)
        Catch ex As Exception
            If ex.Message.Contains("Unable to find Benchmarking Participant") Then
                _mfilesFailed = True
            End If
            MsgBox("Error in cboRefNum_SelectedIndexChanged, " + ex.Message)
        End Try
        ' do these for populating ss.CompanyContactName in SecureSend
        GetCompanyContactInfo("COORD")
        GetPlantContacts()
        FillContacts() ' 
        If _useMFiles And _mfilesFailed Then
            MFilesFailedRoutine()
        Else
            UnhideMFilesTabs()
        End If
        'check verify send status
        VerifySendButtonColor()

    End Sub

    Private Sub cboConsultant_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboConsultant.SelectedIndexChanged
        Dim params As List(Of String)
        cboConsultant.Text = cboConsultant.Text.ToUpper
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Consultant/" & cboConsultant.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)
        If c <> 1 Then
            MessageBox.Show("Consultant Not Updated.", "Missing TSORT Record")
            cboConsultant.Text = ""
        Else
            cboConsultant.SelectedIndex = cboConsultant.FindString(cboConsultant.Text)
        End If
        'ConsoleTabs.SelectTab("tabConsultant")
        'cbConsultants.Text = cboConsultant.Text
    End Sub


    Private Sub btnDirRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnDirRefresh.Click
        BuildDirectoriesTab()
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Dim t As New TimeSpan(0, 0, Time)
        Dim l As New TimeSpan(0, 0, LastTime)
        Dim hours As String
        Dim minutes As String
        Dim seconds As String
        Dim lhours As String
        Dim lminutes As String
        Dim lseconds As String
        hours = t.Hours.ToString()
        minutes = t.Minutes.ToString
        seconds = t.Seconds.ToString
        If hours.Length = 1 Then hours = "0" & hours
        If minutes.Length = 1 Then minutes = "0" & minutes
        If seconds.Length = 1 Then seconds = "0" & seconds
        lhours = l.Hours.ToString()
        lminutes = l.Minutes.ToString
        lseconds = l.Seconds.ToString
        If lhours.Length = 1 Then lhours = "0" & lhours
        If lminutes.Length = 1 Then lminutes = "0" & lminutes
        If lseconds.Length = 1 Then lseconds = "0" & lseconds
        Time += 1
        ConsoleTimer.Text = "Timer : " & hours & ":" & minutes & ":" & seconds & "   Last Time: " & lhours & ":" & lminutes & ":" & lseconds
    End Sub


    Private Sub btnRefreshConsultantTab_Click(sender As System.Object, e As System.EventArgs) Handles btnRefreshConsultantTab.Click
        SetConsultant(cbConsultants.Text)
    End Sub


    Private Sub ConsoleTimer_DoubleClick(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.DoubleClick
        LastTime = Time
        Time = 0
    End Sub

    Private Sub ConsoleTimer_Click_1(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.Click
        If Timer1.Enabled Then
            ConsoleTimer.ForeColor = Color.DarkRed
            Timer1.Enabled = False
        Else
            ConsoleTimer.ForeColor = Color.Green
            Timer1.Enabled = True
        End If
    End Sub
#End Region


    Private Sub btnKill_Click(sender As System.Object, e As System.EventArgs) Handles btnKill.Click
        Dim dr As New DialogResult()
        dr = MessageBox.Show("This will kill all OPEN Excel and Word Documents.  Please <SAVE> any Word or Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel)
        If dr = DialogResult.OK Then
            Utilities.KillProcesses("WinWord")
            Utilities.KillProcesses("Excel")
        End If
    End Sub
    Public Sub UpdatePN()
        Dim params = New List(Of String)

        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Note/" + txtValidationIssues.Text)
        params.Add("NoteType/Validation")
        params.Add("UpdatedBy/" + UserName)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
        If c = 1 Then
            txtDragDropStatus.Text = "Notes updated successfully"
        Else
            MessageBox.Show("Notes did not update")
        End If
    End Sub

    Private Sub btnUnlockPN_Click(sender As System.Object, e As System.EventArgs) Handles btnUnlockPN.Click
        'If txtValidationIssues.Text.Substring(0, 1) = "~" Then
        '    txtValidationIssues.ReadOnly = False
        '    txtValidationIssues.Text = txtValidationIssues.Text.Substring(1, txtValidationIssues.Text.Length - 1)
        '    txtValidationIssues.Refresh()
        '    btnCreatePN.Text = "Create PN File"
        '    UpdatePN()
        '    txtDragDropStatus.Text = "PN File unlocked..."


        '    If File.Exists(CorrPath & "PN_" & Company & ".doc") Then File.Delete(CorrPath & "PN_" & Company & ".doc")
        '    BuildSummaryTab()
        'End If
    End Sub

    Private Sub btnCreatePN_Click(sender As System.Object, e As System.EventArgs) Handles btnCreatePN.Click
        Try

            Dim myText As String = "Presenter Notes for" & vbCrLf & Company & vbCrLf & vbCrLf

            'pull from MFiles if exists there. Else create.
            'Dim docFileAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles)
            'Dim docs As ConsoleFilesInfo = docFileAccess.GetDocNamesAndIdsByBenchmarkingParticipant(GetRefNumCboText(), Get20PlusRefNumCboText())
            'For Each item As ConsoleFile In docs.Files
            '    If item.DeliverableType = "Presenters Notes" Then 'deliverable = 17  'Presenters Notes" Then
            '        docFileAccess.OpenFileFromMfiles(item.Id, GetRefNumCboText(), Get20PlusRefNumCboText())
            '        Exit Sub
            '    End If
            'Next
            'If File.Exists(CorrPath & "PN_" & Company & ".doc") Then
            '    Process.Start(CorrPath & "PN_" & Company & ".doc")
            'Else
            Dim pnTemplatePath As String = String.Empty
            Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will push the notes to MFiles.", "Wait", MessageBoxButtons.YesNo)
            If dr = System.Windows.Forms.DialogResult.Yes Then
                pnTemplatePath = GetPNTemplatePath()
                Dim params = New List(Of String)
                Dim row As DataRow

                txtDragDropStatus.Text = "Creating Presenter's Word file..."
                params.Add("RefNum/" + RefNum)
                params.Add("NoteType/Validation")
                ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        myText += row("Note").ToString
                    Else
                        myText += ""
                    End If
                End If
                Dim fields As New SA.Console.Common.WordTemplate
                fields.Field.Add("~CoLoc~")
                fields.Field.Add("~Notes~")
                fields.RField.Add(Company)
                fields.RField.Add(myText)
                Dim docName As String = "PresentationNotes.docx"
                If File.Exists(pnTemplatePath) Then
                    File.Delete(ProfileConsolePath & docName) ' & "PN_" & Company & ".doc")
                    File.Copy(pnTemplatePath, ProfileConsolePath & docName) ' "PN_" & Company & ".doc")
                    'This will create the doc and push it into MFiles
                    'WordTemplateReplace(_profileConsoleTempPath & "PN_" & Company & ".doc", fields, CorrPath & "PN_" & Company & ".doc")
                    WordTemplateReplace(ProfileConsolePath & docName, fields, docName, True)
                    'Else
                    'Dim strw As New StreamWriter(TempPath & "PN_" & Company & ".txt")

                    'strw.WriteLine(myText)
                    'strw.Close()
                    'strw.Dispose()
                    'Dim doc As String = Utilities.ConvertToDoc(TempPath, TempPath & "PN_" & Company & ".txt")
                    'File.Move(TempPath & "PN_" & Company & ".doc", CorrPath & "PN_" & Company & ".doc")
                    'File.Delete(TempPath & "PN_" & Company & ".txt")
                    'txtDragDropStatus.Text = "Presenter's Word file created and locked..."
                    'txtValidationIssues.Text = "~" & txtValidationIssues.Text
                Else
                    MsgBox("Template file is not existed in K:\STUDY\Olefins\2018\Correspondence\!Validation\Templates\Archive\Olefins Study 2018 PN_Template.docx")
                    Exit Sub
                End If
                txtValidationIssues.Text = "~" + txtValidationIssues.Text
                UpdatePN()
                BuildSummaryTab()

                'WordTemplateReplace will do this:  Process.Start(CorrPath & "PN_" & Company & ".doc")
                btnCreatePN.Text = "Open PN File"
            End If

        Catch ex As System.Exception
            txtDragDropStatus.Text = "Error: " & ex.Message

        End Try
        ' End If


    End Sub



    Private Sub btnUploadPYM_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        UploadFile("PYPS" + "20" + RefNum + ".xls")
    End Sub

    Private Sub btnUploadSPSL_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        UploadFile("SPSL" + "20" + RefNum + ".xls")
    End Sub

    Private Sub btnUploadOSIM_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        UploadFile("OSIM" + "20" + RefNum + ".xls")
    End Sub


    Private Sub btnValidate_Click_1(sender As System.Object, e As System.EventArgs) Handles btnValidate.Click
        Try
            'Dim choice As String = InputBox("Enter Refnum: ", "Validation", cboRefNum.Text)
            Dim choice As String = InputBox("Enter Refnum: ", "DataChecks", cboRefNum.Text)



            Dim path As String = "K:\STUDY\Olefins\" & StudyYear & "\Programs\Validate\Olefins DataChecks."
            If File.Exists(path & "xlsm") Then
                LaunchDataChecks(choice, path & "xlsm")
            ElseIf File.Exists(path & "xlsx") Then
                LaunchDataChecks(choice, path & "xlsx")
            ElseIf File.Exists(path & "xls") Then
                LaunchDataChecks(choice, path & "xls")
            Else
                MsgBox("Can't find DataChecks file at " & path)
                Exit Sub
            End If

            'If Int32.Parse(StudyYear.Substring(2, 2)) < 15 Then
            '    path = _pathToPriorDatachecksFile
            'Else
            '    path = _pathToCurrentDatachecksFile
            'End If


            'LaunchDataChecks(choice, path)
        Catch ex As System.Exception
            MsgBox("Error in running DataChecks Excel file: " + ex.Message)
        End Try
    End Sub



    Private Sub btnUpdateNotes_Click_1(sender As System.Object, e As System.EventArgs) Handles btnUpdateNotes.Click
        If SaveNotes(txtValidationIssues.Text, "Validation") = 0 Then
            lblNotesStatus.Text = "Notes Not Updated"
        Else
            lblNotesStatus.Text = "Notes Updated"
        End If


    End Sub

    Private Sub txtContinuingIssues_LostFocus(sender As Object, e As EventArgs)
        'Throw New Exception("txtContinuingIssues_LostFocus - under construction")
        'SaveNotes(txtContinuingIssues.Text, "Continuing")
        'If NotesAreSaved(cboRefNum.Text, "Continuing", txtContinuingIssues.Text) Then
        '    'If SaveNotes(txtContinuingIssues.Text, "Continuing") > 1 Then
        '    lblIssueStatus.Text = "Continuing Issue Updated"
        'Else
        '    lblIssueStatus.Text = "Continuing Issue Not Updated"
        'End If
    End Sub

    Private Sub txtConsultingOpps_LostFocus(sender As Object, e As EventArgs) Handles txtConsultingOpps.LostFocus
        'If SaveNotes(txtConsultingOpps.Text, "Consulting") Then
        SaveNotes(txtConsultingOpps.Text, "Consulting")
        If NotesAreSaved(cboRefNum.Text, "Consulting", txtConsultingOpps.Text) Then
            lblIssueStatus.Text = "Consulting Issue Updated"
        Else
            lblIssueStatus.Text = "Consulting Issue Not Updated"
        End If
    End Sub

    Public Function GetRefNumCboText() As String
        If IsNothing(cboRefNum.Text) OrElse cboRefNum.Text.Length < 1 OrElse cboRefNum.Text.Trim().Length < 1 Then
            Return String.Empty
        Else
            Return cboRefNum.Text.Trim()
        End If
    End Function

    Public Function Get20PlusRefNumCboText() As String
        Dim cboText As String = GetRefNumCboText()
        If cboText.Length < 1 Then
            Return String.Empty
        Else
            If cboText.StartsWith("20") Then
                Return cboText
            Else
                Return Utilities.GetLongRefnum(cboText)
            End If
        End If
    End Function

    Private Sub tvwCorrespondence2_MouseUp(sender As Object, e As MouseEventArgs) Handles tvwCorrespondence2.MouseUp
        For Each tn As TreeNode In tvwCorrespondence2.Nodes
            If tn.BackColor = Color.Red Then
                tn.Checked = False
            End If
        Next
    End Sub

    Private Function TrimStringToXLength(theText As String, maxLen As Integer) As String
        If IsNothing(theText) Then Return String.Empty
        If theText.Length <= maxLen Then
            Return theText
        Else
            Return theText.Substring(0, maxLen)
        End If
    End Function

    Private Sub listViewCorrespondenceVF_DoubleClick(sender As Object, e As EventArgs) Handles listViewCorrespondenceVF.DoubleClick
        listViewCorrespondence_Shell(CType(sender, ListView), "F")
        'Dim file As String
        ''file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
        'Dim selections As ListView.SelectedListViewItemCollection = _
        '    listViewCorrespondenceVF.SelectedItems
        'If selections.Count <> 1 Then
        '    MsgBox("Please select only 1 VF")
        '    Return
        'End If
        'For Each item As ListViewItem In selections
        '    Dim FindTab As String = item.Text.ToString.IndexOf("|")
        '    file = item.Text.Substring( _
        '        FindTab + 1, item.Text.ToString.Length - FindTab - 1)
        '    If CInt(StudyYear) >= 2015 Then
        '        Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS) ', ShortRefNum, "20" + ShortRefNum)
        '        docsAccess.lstVFFiles_DoubleClick(file.Trim(), CorrPath, GetRefNumCboText(), Get20PlusRefNumCboText())
        '    Else
        '        Process.Start(CorrPath & file.Trim)
        '    End If
        '    Return 'don't do more than 1
        'Next
    End Sub

    Private Sub listViewCorrespondence_Shell(listView As ListView, v As Char)
        Dim file As String
        'file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
        Dim selections As ListView.SelectedListViewItemCollection = _
            listView.SelectedItems
        If selections.Count <> 1 Then
            'MsgBox("Please select only 1 V" + v.ToString().ToUpper())
            Return
        End If
        For Each item As ListViewItem In selections
            Dim FindTab As String = item.Text.ToString.IndexOf("|")
            file = item.Text.Substring( _
                FindTab + 1, item.Text.ToString.Length - FindTab - 1)
            'If CInt(StudyYear) >= 2015 Then
            '    Dim docsAccess As New DocsFileSystemAccess(VerticalType.OLEFINS, _useMFiles) ', ShortRefNum, "20" + ShortRefNum)
            '    docsAccess.lstVFFiles_DoubleClick(file.Trim(), CorrPath, GetRefNumCboText(), Get20PlusRefNumCboText())
            'Else
            Process.Start(CorrPath & file.Trim)
            'End If
            Return 'don't do more than 1
        Next
    End Sub

    Private Sub PrepCorrListview(listView As ListView, startsWith As String, items As List(Of Object), _
                                 vfNumber As String, append As Boolean)
        Dim heading As String = String.Empty
        Select Case startsWith.ToUpper()
            Case "VF"
                heading = "IDR Fax Files (VF*.*)"
            Case "VR"
                heading = "IDR Reply Files (VR*.*)"
            Case Else
                heading = "Return Files"
        End Select
        If Not append Then
            If listView.Columns.Count < 1 Then listView.Columns.Add(heading)
            listView.Items.Clear()
            listView.View = Windows.Forms.View.Details
            listView.FullRowSelect = True
            listView.Columns(0).Width = listView.Width - 4 - SystemInformation.VerticalScrollBarWidth '4 - SystemInformation.VerticalScrollBarWidth
        End If
        For Each item As Object In items
            Dim itemText As String = item.ToString()
            Dim checkedOut As Boolean = itemText.Contains("(Checked out to")
            itemText = itemText.Replace("Checked out to ", "")
            Dim itemParts() As String = Split(itemText, " | ")
            Dim fileName As String = itemParts(1)
            If heading <> "Return Files" Then
                If vfNumber = Mid(fileName, 3, 1) And fileName.StartsWith(startsWith) Then
                    Dim listViewItem As New ListViewItem(itemText)
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                End If
            Else 'return files box
                'If (lstVFNumbers.SelectedItem = Mid(filename, Len(filename) - 4, 1) _
                ''If (lstVFNumbers.SelectedItem = Mid(objArr(1), Len(objArr(1)) - 4, 1) _
                '-Or InStr(1, filename, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                If vfNumber = Mid(fileName, Len(fileName) - 4, 1) Or _
                    fileName.Contains("Return" + vfNumber) Then
                    Dim listViewItem As New ListViewItem(itemText)
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                End If
            End If
        Next
    End Sub

    Private Sub listViewCorrespondenceVR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listViewCorrespondenceVR.SelectedIndexChanged
        listViewCorrespondence_Shell(CType(sender, ListView), "R")
    End Sub

    Private Sub listViewCorrespondenceVReturn_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listViewCorrespondenceVReturn.SelectedIndexChanged
        listViewCorrespondence_Shell(CType(sender, ListView), "")
    End Sub

    Private Sub listViewCorrespondenceVF_DragDrop(sender As Object, e As DragEventArgs) Handles listViewCorrespondenceVF.DragDrop

    End Sub

    Private Function NotesAreSaved(argRefnum As String, noteType As String, noteToFind As String) As Boolean
        If noteToFind = GetLastNoteSaved(argRefnum, noteType) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetLastNoteSaved(argRefnum As String, noteType As String) As String
        Dim result As String = String.Empty
        Dim params As New List(Of String)
        params.Add("RefNum/" + argRefnum)
        params.Add("NoteType/" + noteType)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)
        If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
            result = ds.Tables(0).Rows(0)("Note").ToString()
        End If
        Return result
    End Function

    Private Function WasThisValidationNoteSavedBefore(argRefnum As String, noteType As String, note As String) As Boolean
        Dim params As New List(Of String)
        params.Add("RefNum/" + argRefnum)
        params.Add("NoteType/" + noteType)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)
        If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                If row("Note").ToString().Trim() = note.Trim() Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    Private Sub btnLastConsultingIssueSaved_Click(sender As Object, e As EventArgs) Handles btnLastConsultingIssueSaved.Click
        MsgBox("The last Consulting note saved in the database was: " + GetLastNoteSaved(cboRefNum.Text, "Consulting"))
    End Sub

    Private Sub btnLastContinuingIssueSaved_Click(sender As Object, e As EventArgs) Handles btnLastContinuingIssueSaved.Click
        MsgBox("The last Continuing note saved in the database was: " + GetLastNoteSaved(cboRefNum.Text, "Continuing"))
    End Sub

    Private Sub btnLastPresenterNoteSaved_Click(sender As Object, e As EventArgs) Handles btnLastPresenterNoteSaved.Click
        Dim lastNoteSaved As String = GetLastNoteSaved(cboRefNum.Text, "Validation")
        Dim messageToUser As String = "The last IDR Issue/Presenters note saved in the database was: " + lastNoteSaved
        'If lastNoteSaved.Trim() <> txtValidationIssues.Text.Trim() Then
        '    If WasThisValidationNoteSavedBefore(cboRefNum.Text, "Validation", txtValidationIssues.Text) Then
        '        messageToUser += Environment.NewLine + "But your existing note was already saved before this."
        '    End If
        'End If
        MsgBox(messageToUser)
    End Sub

    Private Sub btnRefreshChecklist_Click(sender As Object, e As EventArgs) Handles btnRefreshChecklist.Click
        BuildCheckListTab(cboCheckListView.SelectedItem)
    End Sub

    Private Sub btnPolishing_Click(sender As Object, e As EventArgs) Handles btnPolishing.Click
        OpenPolishReport()
    End Sub

    Private Sub GeneratePolishRpt()
        Dim xl As Excel.Application
        xl = Utilities.GetExcelProcess()
        Me.Cursor = Cursors.WaitCursor
        Dim polishTemplatePath As String = String.Empty
        Try
            polishTemplatePath = "K:\STUDY\Olefins\" & StudyYear & "\Correspondence\!Validation\Polishing\Polishing with ODC and Outliers.xlsm"
            Dim w As Excel.Workbook
            w = xl.Workbooks.Open(polishTemplatePath, , False)
            xl.Visible = True
            xl.Run("Console_OpenPolishRpt_RefNum", "20" + cboRefNum.Text)
            w.Close()
        Catch ex As Exception
            MsgBox("Cannot find the Polishing Excel file.")
        Finally
            Me.Cursor = Cursors.Default
            'Macro does this: f File.Exists(CorrPath & "20" + cboRefNum.Text & "_Polish.xls") Then Process.Start(CorrPath & CurrentRefNum & "_Polish.xls")
        End Try
    End Sub

    Private Sub OpenPolishReport()
        Dim rtn As DialogResult = System.Windows.Forms.DialogResult.OK
        If Not File.Exists(PolishReportPath()) Then
            rtn = MessageBox.Show("Generate Polish Report?", "Polish Report", MessageBoxButtons.OKCancel)
            If rtn = System.Windows.Forms.DialogResult.OK Then
                GeneratePolishRpt()
            End If
        Else
            Dim xl As Excel.Application
            xl = Utilities.GetExcelProcess()
            Dim w As Excel.Workbook
            Me.Cursor = Cursors.WaitCursor
            Try
                w = xl.Workbooks.Open(PolishReportPath(), , False)
                xl.Visible = True
            Catch
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End If
    End Sub

    Private Function PolishReportPath() As String
        Return CorrPath & "20" & cboRefNum.Text & "_Polish.xls"
    End Function

    Private Sub btnBug_Click(sender As Object, e As EventArgs) Handles btnBug.Click
        Dim bug As New frmBug(Main.ConsoleVertical, cboRefNum.Text, UserName, db.ConnectionString)
        bug.Show()
    End Sub

    Private Sub LoadCboConsultants()
        Select Case _monitoringInterval
            Case 4
                ds = db.ExecuteStoredProc("Console." & "GetAllConsultants_CQM")
            Case Else
                ds = db.ExecuteStoredProc("Console." & "GetAllConsultants")
        End Select

        If ds.Tables.Count > 0 Then
            cboConsultant.Items.Clear()
            For Each Row In ds.Tables(0).Rows
                If Row("Consultant").ToString.Trim.Length > 0 Then
                    cboConsultant.Items.Add(Row("Consultant").ToString.ToUpper.Trim)
                    cbConsultants.Items.Add(Row("Consultant").ToString.ToUpper.Trim)
                End If
            Next
        End If
    End Sub
    'Private Function GetDrawingPath() As String
    '    Return StudyDrive & "Console." & StudyType & "\" & StudyRegion & "\General\"
    'End Function
    Private Sub PopulatePlantFolder(localRefNum As String)
        Select Case _monitoringInterval
            Case 0.5
                _plantFolder = "K:\STUDY\Olefins\" & StudyYear & "\Plants\" & localRefNum & "\"
            Case 4
                _plantFolder = "K:\STUDY\OlefinsCQM\" & StudyYear & "\Plants\" & localRefNum & "\"
            Case 1
                _plantFolder = "K:\STUDY\SEEC\" & StudyYear & "\Plants\" & localRefNum & "\"
        End Select
    End Sub
    Private Sub PopulateSecureSendTemplateFolder()
        _secureSendTemplateFolder = "K:\STUDY\Olefins\" & StudyYear & "\Correspondence\!Validation\Templates\"
    End Sub
    Private Function GetPNTemplatePath()
        Return "K:\STUDY\Olefins\" & StudyYear & "\Correspondence\!Validation\Templates\Archive\Olefins Study " & StudyYear & " PN_Template.docx"
    End Function



    Private CI As String
    Private CIText As String
    Private CIIssueDate As String
    Private CIConsultant As String
    Private CIDeleted As String
    Private CIDeletedDate As String
    Private CIDeletedBy As String
    Private _continuingIssueSqlType As Integer = 0  '1=insert, 2=upate

    Private Sub dgContinuingIssues_Click(sender As Object, e As EventArgs) Handles dgContinuingIssues.Click
        Dim row As DataGridViewRow
        dgContinuingIssues.Height = _dgContinuingIssuesHeight ' 240
        txtCI.Text = ""
        If dgContinuingIssues.SelectedRows.Count > 0 Then
            row = dgContinuingIssues.SelectedRows(0)
            CI = row.Cells(0).Value.ToString
            txtCI.Text = row.Cells(2).Value.ToString
            CIText = txtCI.Text
            CIConsultant = row.Cells(3).Value.ToString
            CIIssueDate = row.Cells(4).Value.ToString
            CIDeletedBy = cboConsultant.Text
            CIDeletedDate = DateTime.Now
            CIDeleted = False
            btnCINewIssue.Enabled = False
        End If
        txtCI.ReadOnly = True
        btnCISaveIssue.Enabled = False
        btnCIUpdateIssue.Enabled = True
        btnCINewIssue.Enabled = True
        _continuingIssueSqlType = 0
    End Sub

    Private Sub btnCINewIssue_Click(sender As Object, e As EventArgs) Handles btnCINewIssue.Click
        txtCI.Text = ""
        txtCI.ReadOnly = False
        CI = -1
        btnCISaveIssue.Enabled = True
        _continuingIssueSqlType = 1 'Private _continuingIssueSqlType As Integer = 0  '1=insert, 2=upate
        btnCIUpdateIssue.Enabled = False
        btnCINewIssue.Enabled = False
    End Sub

    Private Sub btnCIUpdateIssue_Click(sender As Object, e As EventArgs) Handles btnCIUpdateIssue.Click
        _continuingIssueSqlType = 2         'Private _continuingIssueSqlType As Integer = 0  '1=insert, 2=upate
        txtCI.ReadOnly = False
        btnCIUpdateIssue.Enabled = False
        btnCISaveIssue.Enabled = True
        btnCINewIssue.Enabled = False
    End Sub

    Private Sub btnCISaveIssue_Click(sender As Object, e As EventArgs) Handles btnCISaveIssue.Click
        Dim testText As String = Utilities.FixQuotes(txtCI.Text.Replace("/", "-"))
        'Private _continuingIssueSqlType As Integer = 0  '1=insert, 2=upate
        Me.Cursor = Cursors.WaitCursor
        If _continuingIssueSqlType = 1 Then
            Dim sql As String = "INSERT INTO Console.ContinuingIssues (RefNum, Consultant,Note,InsertedDate,InsertedBy,ModifiedDate,ModifiedBy) VALUES ("  ',Deleted,DeletedDate,DeletedBy
            'INSERT ,    Consultant,    Note,                                       InsertedDate,    InsertedBy,    ModifiedDate,    ModifiedBy,    Deleted,    DeletedDate,    DeletedBy )
            sql += "'" & GetRefNumCboText() & "','" & UserName & "','" & txtCI.Text & "','" & DateTime.Now.ToString() & "','" & UserName & "','" & DateTime.Now.ToString() & "','" & UserName & "');"
            db.SQL = sql
            Try
                ds = db.Execute()
                'db.SQL = "SELECT * FROM Console.ContinuingIssues where RefNum = '" & GetRefNumCboText() & "' ORDER BY ModifiedDate DESC;"
                'ds = db.Execute()
                txtCI.Text = String.Empty
            Catch ex As Exception
                MsgBox("Continuing Issue not saved: " & ex.Message)
            Finally
                db.SQL = ""
            End Try
        ElseIf _continuingIssueSqlType = 2 Then
            Dim sql As String = "UPDATE Console.ContinuingIssues Set Note = '" & txtCI.Text & _
                "', ModifiedDate = '" & DateTime.Now & "', ModifiedBy = '" & UserName & "' WHERE IssueID = " & CI & ";"
            db.SQL = sql
            Try
                ds = db.Execute()
                ' db.SQL = "SELECT * FROM Console.ContinuingIssues where IssueID = " & CI
                ' ds = db.Execute()
                txtCI.Text = String.Empty
            Catch ex As Exception
                MsgBox("Continuing Issue not saved: " & ex.Message)
            Finally
                db.SQL = ""
            End Try
            txtCI.BackColor = Color.White
            BuildContinuingIssues()
        Else
            'it is zero, do nothing
        End If

        _continuingIssueSqlType = 0
        'System.Threading.Thread.Sleep(1000)
        Me.Cursor = Cursors.Default
        BuildContinuingIssues()
        txtCI.ReadOnly = True
        btnCIUpdateIssue.Enabled = True
        btnCISaveIssue.Enabled = False
        btnCINewIssue.Enabled = True
    End Sub
    Private Sub btnCISaveIssue_Click_OLD()

        'Dim rowToUse As Integer = 0
        'If dgContinuingIssues.Rows.Count < 1 Then
        '    If Not SaveCI() Then
        '        MsgBox("Continuing Issue not saved")
        '        Exit Sub
        '    End If
        'Else
        '    If _newIssueClicked Then
        '        If Not SaveCI() Then
        '            MsgBox("Continuing Issue not saved")
        '            Exit Sub
        '        End If
        '    Else
        '        rowToUse = dgContinuingIssues.SelectedCells(0).RowIndex
        '        Dim savedText As String = dgContinuingIssues.Rows(rowToUse).Cells(3).Value.ToString()
        '        dgContinuingIssues.Height = _dgContinuingIssuesHeight ' 385

        '        'no need to save if both blank.
        '        If savedText.Trim().Length < 1 AndAlso testText.Trim().Length < 1 Then
        '            Return
        '        End If

        '        If dgContinuingIssues.Rows(rowToUse).Cells(3).Value.ToString().ToUpper() <> testText.ToUpper() Then
        '            If Not SaveCI() Then
        '                MsgBox("Continuing Issue not saved")
        '                Exit Sub
        '            End If
        '        End If
        '    End If
        'End If
        'ClearContinuingIssues()
        'BuildContinuingIssues()
        'btnCINewIssue.Enabled = True
        'btnCIUpdateIssue.Enabled = True
        'txtCI.Text = String.Empty
        'txtCI.BackColor = Color.Black
        'btnCISaveIssue.Enabled = False
    End Sub

    Private Sub btnCICancel_Click(sender As Object, e As EventArgs) Handles btnCICancel.Click
        txtCI.Text = ""
        'btnCISaveIssue.Enabled = False
    End Sub

    Private Function SaveCI(Optional CalledByLostFocus As Boolean = False) As Boolean
        Dim consultant As String = cboConsultant.Text
        Dim whatToSave As String = Utilities.FixQuotes(txtCI.Text.Trim().Replace("/", "-"))
        If whatToSave.Trim().Length < 1 Then Return True
        Dim sql As String = ""
        If CalledByLostFocus Then
            sql = "select Note from [Console].[ContinuingIssues] where IssueID = " & CI
            db.SQL = sql
            ds = db.Execute()
            db.SQL = ""
            If Not ds Is Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0)("Note").ToString() = CIText Then
                Return True
            End If
        End If

        Dim thisRefNum As String = FormatRefNum(GetRefNumCboText())
        _continuingIssueSqlType = 2   '1=insert, 2=upate
        If _continuingIssueSqlType = 2 Then
            sql = "UPDATE Console.ContinuingIssues Set Note = '" & whatToSave & _
                "', Consultant = '" & consultant & _
                "', ModifiedDate = '" & DateTime.Now & "', ModifiedBy = '" & consultant & "' WHERE IssueID = " & CI & ";"
            db.SQL = sql
            Try
                ds = db.Execute()
                db.SQL = "SELECT * FROM Console.ContinuingIssues where IssueID = " & CI & " and Deleted ='N'"
                ds = db.Execute()
            Catch ex As Exception
                MsgBox("Continuing Issue not saved: " & ex.Message)
                Return False
            Finally
                db.SQL = ""
            End Try
        ElseIf _continuingIssueSqlType = 1 Then
            'save new issue
            sql = "INSERT INTO Console.ContinuingIssues (RefNum, Consultant,Note,InsertedDate,InsertedBy,ModifiedDate,ModifiedBy) VALUES ("  ',Deleted,DeletedDate,DeletedBy
            'INSERT ,    Consultant,    Note,                                       InsertedDate,    InsertedBy,    ModifiedDate,    ModifiedBy,    Deleted,    DeletedDate,    DeletedBy )
            sql += "'" & GetRefNumCboText() & "','" & consultant & "','" & whatToSave & "','" & DateTime.Now.ToString() & "','" & consultant & "','" & DateTime.Now.ToString() & "','" & consultant & "');"
            db.SQL = sql
            Try
                ds = db.Execute()
                db.SQL = "SELECT * FROM Console.ContinuingIssues where RefNum = '" & GetRefNumCboText() & "' and DELETED = 'N' ORDER BY ModifiedDate DESC;"
                ds = db.Execute()
            Catch ex As Exception
                MsgBox("Continuing Issue not saved: " & ex.Message)
                Return False
            Finally
                db.SQL = ""
            End Try
        Else
            MsgBox("Error in SaveCI: value is not 1 or 2")
            Return False
        End If

        If Not ds Is Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0)("Note").ToString() = CIText Then
            Return True
        Else
            Dim descriptiveError As String = String.Empty
            If ds Is Nothing OrElse ds.Tables.Count = 0 OrElse ds.Tables(0).Rows.Count = 0 Then
                descriptiveError = " No data was saved to the database."
            ElseIf ds.Tables(0).Rows(0)("Note").ToString() <> whatToSave Then
                descriptiveError = " Note was found in the database, but it is not an exact match with what you were trying to save."
            End If
            If CalledByLostFocus Then
                Try
                    Dim notesPath As String = ProfilePath & "dgContinuingIssuesLostFocus_" & DateTime.Now.ToString("MM-dd-yyyy HH-mm-ss") & ".txt"
                    Using sw As New StreamWriter(notesPath)
                        sw.Write(CIText)
                    End Using
                    MsgBox("Continuing Issue was not saved. Please see Notepad for the text that should have been saved." & descriptiveError)
                    Process.Start(notesPath)
                Catch ex As Exception
                    Dim breakpoint As String = ex.Message
                End Try
            Else
                Return descriptiveError.Length < 1
            End If
        End If
        Return False
    End Function
    Private Sub BuildContinuingIssues()
        Dim thisRefNum As String = GetRefNumCboText()
        Dim params = New List(Of String)
        dgContinuingIssues.Rows.Clear()
        dgContinuingIssues.Height = _dgContinuingIssuesHeight ' 385
        If thisRefNum.Length < 1 Then Exit Sub
        thisRefNum = thisRefNum.Substring(5, Len(thisRefNum) - 5)

        'MessageBox.Show("NEed to import Console.ContinuingIssues table from refining into Olefins. THen add records from Val.Notes")
        'Dim sql As String = "select IssueID, RefNum, Consultant, Note, ModifiedBy,ModifiedDate from Console.ContinuingIssues  where refnum Like '%PCH" & thisRefNum & "' ORDER BY IssueID desc ;"

        Dim localStudyType As String = String.Empty
        Select Case _monitoringInterval
            Case 0.5 'study
                localStudyType = "PCH"
            Case 4 'qrtly
                localStudyType = "CQM"
            Case 1 'SEEC
                localStudyType = "SEEC"
        End Select


        Dim sql As String = "select IssueID, RefNum, Note, ModifiedBy,ModifiedDate from Console.ContinuingIssues  where refnum Like '%" & localStudyType & thisRefNum & "' AND DELETED  = 'N' ORDER BY IssueID desc ;"
        Try
            db.SQL = sql
            ds = db.Execute()
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr In ds.Tables(0).Rows
                    If dr("note").ToString().Trim().Length > 0 Then
                        'dgContinuingIssues.Rows.Add({dr("IssueID").ToString, dr("RefNum").ToString, dr("consultant").ToString, dr("note").ToString, dr("ModifiedBy").ToString, dr("ModifiedDate").ToString})
                        dgContinuingIssues.Rows.Add({dr("IssueID").ToString, dr("RefNum").ToString, dr("note").ToString, dr("ModifiedBy").ToString, dr("ModifiedDate").ToString})
                    End If
                Next
            End If
        Catch ex As Exception
            db.SQL = String.Empty
        End Try
    End Sub
    Private Sub ClearContinuingIssues()
        dgContinuingIssues.Rows.Clear()
    End Sub
    Private Sub txtCI_TextChanged(sender As Object, e As EventArgs) Handles txtCI.TextChanged
        If txtCI.Text.Length > 1 Then
            btnCISaveIssue.Enabled = True
        End If
    End Sub

    Private Function FormatRefNum(refnum As String) As String
        'refnum = refnum.Replace("LIV", "LUB").Replace("LEV", "LUB")
        If refnum.Substring(refnum.Length - 1, 1) = "A" Then
            Return refnum.Substring(0, refnum.Length - 3)
        Else
            Return refnum.Substring(0, refnum.Length - 2)
        End If
    End Function

    Private Sub btnCIDeleteIssue_Click(sender As Object, e As EventArgs) Handles btnCIDeleteIssue.Click
        If MsgBox("Are you sure you want to delete issue " & CI.ToString() & "?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim sql As String = "UPdate [Console].[ContinuingIssues] SET DELETED = 'Y', DeletedDate = '" & DateTime.Now.ToString() & "', DeletedBy = '" & UserName & "' where IssueID = " & CI
            Try
                db.SQL = sql
                db.Execute()
            Catch ex As Exception
                MsgBox("Error in btnCIDeleteIssue_Click:" & ex.Message)
            Finally
                db.SQL = String.Empty
            End Try
        End If
        BuildContinuingIssues()
    End Sub

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        'MessageBox.Show(lblName.Text)
    End Sub

    Private Sub dgFiles_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgFiles.DataError
        'System.ArgumentException: DataGridViewComboBoxCell value is not valid
        e.Cancel = True
    End Sub

    Private Function GetDataset(sql As String) As DataSet
        Dim dsLocal As DataSet
        Try
            db.SQL = sql
            dsLocal = db.Execute()
            Return dsLocal
        Catch ex As Exception
            MsgBox("Error in GetDataset: " & ex.Message)
            Return Nothing
        Finally
            db.SQL = String.Empty
        End Try
    End Function

    Private Function DataSetHasData(dsLocal As DataSet) As Boolean
        If Not IsNothing(dsLocal) AndAlso dsLocal.Tables.Count = 1 AndAlso dsLocal.Tables(0).Rows.Count > 0 Then
            Return True
        End If
        Return False
    End Function

    Private Sub btnVerifySend_Click(sender As Object, e As EventArgs) Handles btnVerifySend.Click
        Try
            'check verify send status
            btnVerifySend.Text = "Checking"
            btnVerifySend.BackColor = Color.Blue
            VerifySendButtonColor()
            Thread.Sleep(1000)
        Catch ex As Exception
            MsgBox("Checking VerifySend Error: " + ex.Message)
            Exit Sub
        End Try
        btnVerifySend.Text = "VerifySend"
    End Sub
    Private Sub VerifySendButtonColor()
        Dim verifysendStatus As Boolean
        verifysendStatus = Utilities.checkVerifySend
        If verifysendStatus Then
            btnVerifySend.BackColor = Color.Green
        Else
            btnVerifySend.BackColor = Color.Red
        End If
    End Sub

    Private Sub OlefinsMainConsole_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        'check verify send status
        VerifySendButtonColor()

    End Sub
End Class

