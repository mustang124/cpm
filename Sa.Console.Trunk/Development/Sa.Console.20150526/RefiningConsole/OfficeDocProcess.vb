﻿
Public Class OfficeDocProcess
    Implements IDisposable
    Private _officeAppType As Integer 'xl is 0, word is 1
    Private _existingDoc As Boolean = False
    Private _processReleased As Boolean = False
    Protected disposed As Boolean = False
    'Private _wordProcess As Word.Application
    'Private _excelProcess As Excel.Application
    Public ExcelProcess As Microsoft.Office.Interop.Excel.Application ' = _excelProcess
    Public WordProcess As Microsoft.Office.Interop.Word.Application ' = _wordProcess

    Public Sub New(officeAppType As Integer)
        _officeAppType = officeAppType
        If _officeAppType = 0 Then
            GetExcelProcess()
        ElseIf _officeAppType = 1 Then
            GetWordProcess()
        End If
    End Sub

    Private Function GetWordProcess() As Microsoft.Office.Interop.Word.Application
        Try
            WordProcess = CType(GetObject(, "Word.Application"), Word.Application)
            If WordProcess.Documents.Count > 0 Then _existingDoc = True
        Catch ex As System.Exception
            WordProcess = New Word.Application
        End Try
        Return WordProcess
    End Function

    Private Function GetExcelProcess() As Microsoft.Office.Interop.Excel.Application
        Try
            ExcelProcess = CType(GetObject(, "Excel.Application"), Excel.Application)
            If ExcelProcess.Workbooks.Count > 0 Then _existingDoc = True
        Catch ex As System.Exception
            ExcelProcess = New Excel.Application
        End Try
        Return ExcelProcess
    End Function

    Public Sub ReleaseProcess()
        If Not _processReleased Then
            If _officeAppType = 0 Then 'xls
                If _existingDoc Then
                    'activate prior doc to release file on hard drive for deletion
                    ExcelProcess.Workbooks(1).Activate()
                Else
                    ExcelProcess.Quit()
                End If
            ElseIf _officeAppType = 1 Then 'word
                If _existingDoc Then
                    'activate prior doc to release file on hard drive for deletion
                    WordProcess.Documents(1).Activate()
                Else
                    WordProcess.Quit()
                End If
            End If
            _processReleased = True
        End If
    End Sub

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                ' Insert code to free managed resources. 
            End If
            ' Insert code to free unmanaged resources. 
            ReleaseProcess()
        End If
        Me.disposed = True
    End Sub
#Region " IDisposable Support "
    ' Do not change or add Overridable to these methods. 
    ' Put cleanup code in Dispose(ByVal disposing As Boolean). 
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub
#End Region
End Class
