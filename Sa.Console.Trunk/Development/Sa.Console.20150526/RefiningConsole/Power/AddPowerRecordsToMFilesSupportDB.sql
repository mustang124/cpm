USE [PowerWork]
--START OF DELETES TO RESET DATA-Was only needed for Site YYYCC. These DELETES will not be needed for future reuse.
DELETE from  MFilesSupport.dbo.StudyFacilities where refnum = 'YYYNA1C16';
DELETE from  MFilesSupport.dbo.StudyCompanies where companyid = 'YNATURKRAFT';
DELETE from  MFilesSupport.dbo.FacilityCompany  where FacilityId = 'YYYCC';
DELETE from  MFilesSupport.dbo.Companies where companyid = 'YNATURKRAFT';
--END OF DELETES TO RESET DATA--Was only needed for Site YYYCC. These DELETES will not be needed for future reuse.

--BEGIN INSERTS, use the following as template for future reuse.
/* MIGHT NEED FOR FUTURE:
INSERT MFilesSupport.dbo.Companies(CompanyID, CompanyName)
SELECT DISTINCT CompanyID, CompanyID FROM StudySites
       where CompanyID = 'NATURKRAFT';
*/


INSERT MFilesSupport.dbo.StudyCompanies(CompanyID, StudyYear, StudyID) 
SELECT DISTINCT CompanyID, StudyYear, 'Power' FROM StudySites 
WHERE SiteID NOT LIKE '%P' 
AND StudyYear IN (SELECT StudyYear FROM MFilesSupport.dbo.Studies WHERE StudyID = 'Power') and CompanyID = 'NATURKRAFT' and StudyYear=2016;

/* MIGHT NEED FOR FUTURE:
INSERT MFilesSupport.dbo.Facilities(FacilityType, FacilityID, Location)
SELECT DISTINCT 'Power', SUBSTRING(SiteID, 1, LEN(SiteID)-2), (SELECT TOP 1 SiteName FROM StudySites n WHERE SUBSTRING(n.SiteID, 1, LEN(n.SiteID)-2) = SUBSTRING(ss.SiteID, 1, LEN(ss.SiteID)-2) AND SiteID LIKE '%[0-9]' ORDER BY StudyYear DESC)
FROM StudySites ss
WHERE SiteID LIKE '%[0-9]'
       and siteid = 'YYYCC16';
*/

INSERT MFilesSupport.dbo.FacilityCompany(FacilityID, CompanyID) 
SELECT DISTINCT SUBSTRING(SiteID, 1, LEN(SiteID)-2), (SELECT TOP 1 CompanyID FROM StudySites n WHERE SUBSTRING(n.SiteID, 1, LEN(n.SiteID)-2) = SUBSTRING(ss.SiteID, 1, LEN(ss.SiteID)-2) AND SiteID LIKE '%[0-9]' ORDER BY StudyYear DESC) 
FROM StudySites ss 
WHERE SiteID NOT LIKE '%P' 
AND EXISTS (SELECT * FROM MFilesSupport.dbo.Facilities f WHERE f.FacilityID = SUBSTRING(SiteID, 1, LEN(SiteID)-2))
and siteid = 'YYYCC16';

INSERT MFilesSupport.dbo.StudyFacilities(FacilityID, Refnum, StudyID, StudyYear) 
SELECT DISTINCT SUBSTRING(SiteID, 1, LEN(SiteID)-2), SiteID, 'Power', StudyYear FROM StudySites  
WHERE SiteID LIKE '%[0-9]' 
AND EXISTS (SELECT * FROM MFilesSupport.dbo.Studies s WHERE s.StudyID = 'Power' AND s.StudyYear = StudySites.StudyYear)       
and siteid = 'YYYCC16';

