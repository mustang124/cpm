USE [PowerWork]
GO
--DEV/TEST ONLY: cREATE SCHEMA Console
--DEV/TEST ONLY: GO
--DEV/TEST ONLY: GRANT ALTER ON [Console] to public
--DEV/TEST ONLY: GO

/*
CREATE TABLE [Console].[ContinuingIssues](
	[IssueID] [int] IDENTITY(1,1) NOT NULL,
	[RefNum] [varchar](12) NOT NULL,
	[EntryDate] [datetime] NOT NULL CONSTRAINT [DF_ContinuingIssues_NoteDate]  DEFAULT (getdate()),
	[Consultant] [varchar](3) NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Deleted] [bit] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [varchar](3) NULL,
 CONSTRAINT [PK_ContinuingIssues] PRIMARY KEY NONCLUSTERED 
(
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Console].[ContinuingIssues] TO PUBLIC
GO

CREATE PROCEDURE [Console].[AddOrUpdateContinuingIssues]
	@ID int,
	@RefNum nvarchar(20),
	@Issue nvarchar(max),
	@EditedBy nvarchar(3)
AS
BEGIN
	IF EXISTS(SELECT IssueID FROM [Console].[ContinuingIssues] where IssueID=@ID)
		BEGIN
			UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@EditedBy where IssueID=@ID
		END
		
		INSERT INTO [Console].[ContinuingIssues] values(@RefNum,getdate(),@EditedBy,@Issue,0,null,@EditedBy)

		SELECT @@IDENTITY
END
GO
GRANT ALTER, EXECUTE ON [Console].[AddOrUpdateContinuingIssues] TO PUBLIC
GO

CREATE PROCEDURE [Console].[DeleteContinuingIssue]
	@IssueID int,
	@DeletedBy nvarchar(3)
AS
BEGIN
	UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@DeletedBy where IssueID=@IssueID
END
GO
GRANT ALTER, EXECUTE ON [Console].[DeleteContinuingIssue] TO PUBLIC
GO

INSERT INTO [Console].[ContinuingIssues]
(--[IssueID]
[RefNum],[EntryDate],[Consultant],[Note],[Deleted],[DeletedDate],[DeletedBy] )
Values
('YYYNA1C16',GETDATE(),'SFB','TEST NEW CI',0,null,null)
GO


GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Val].[Checklist] TO PUBLIC
GO

GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON  [PowerWork].[dbo].[CoContactInfo] TO PUBLIC
GO

CREATE TABLE [Console].[SANumbers]
(
		[ID] [int] IDENTITY NOT NULL,
		[SANumber] [int] NOT NULL,
       [SiteID] [nvarchar](9) NULL,
       [RefNum] [nvarchar](12) NULL,
	   CONSTRAINT [PK_[SANumbers] PRIMARY KEY 
(
       [SANumber]
))
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Console].[SANumbers] TO PUBLIC
GO

GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON dbo.[StudySites] TO PUBLIC
GO

INSERT INTO [dbo].[CoContactInfo]
([SANumber],[ContactType],[FirstName],[LastName],[JobTitle],[PersonalTitle]
,[Phone],[PhoneSecondary],[Fax],[Email],[StrAddr1],[StrAddr2],[StrAdd3],[StrCity]
,[StrState],[StrZip],[StrCountry],[MailAddr1],[MailAddr2],[MailAddr3],[MailCity],[MailState]
,[MailZip],[MailCountry],[AltFirstName],[AltLastName],[AltEmail],[CCAlt],[SendMethod],[Comment])
     VALUES
(1,'ALT','TOM','EDISON','ALTERNATE CONTACT',
NULL,'2345678901',NULL,'','T.EDISON@DCCURRENT.COM','','','','MENLO PARK','NJ','23456','USA',
'','','','MENLO PARK','NJ','23456','USA',NULL,NULL,NULL,'N',NULL,NULL);

INSERT INTO [dbo].[CoContactInfo]
([SANumber],[ContactType],[FirstName],[LastName],[JobTitle],[PersonalTitle]
,[Phone],[PhoneSecondary],[Fax],[Email],[StrAddr1],[StrAddr2],[StrAdd3],[StrCity]
,[StrState],[StrZip],[StrCountry],[MailAddr1],[MailAddr2],[MailAddr3],[MailCity],[MailState]
,[MailZip],[MailCountry],[AltFirstName],[AltLastName],[AltEmail],[CCAlt],[SendMethod],[Comment])
     VALUES
(1,'COORD','Ben','Franklin','Company Coordinator',
NULL,'1234567890',NULL,'','b.frank@goflyakite.com','123 4th st','#500','','Philadelphia','PA','12345','USA',
'123 4th st','#500','','Philadelphia','PA','12345','USA',NULL,NULL,NULL,'N',NULL,NULL);
GO


DROP PROCEDURE [Console].[GetCompanyPassword]
GO
CREATE PROCEDURE [Console].[GetCompanyPassword]
(
	@SiteID char(10), 
	@StudyYear int  
)
AS
	select l.Password as CompanyPassword 
	FROM TSort t 
	inner join Company_LU l on t.CompanyID = l.CompanyID
	WHERE t.SiteID=@SiteID
	AND t.StudyYear = @StudyYear 
GO
GRANT ALTER, EXECUTE ON [Console].[GetCompanyPassword] TO PUBLIC
GO

DROP PROCEDURE  [Console].[GetCompanyContactInfo]
GO
CREATE PROCEDURE  [Console].[GetCompanyContactInfo]
	@SiteId nvarchar(10),
	@StudyYear int,
	@ContactType char(5)
AS
BEGIN
	IF @SiteId is not null 
	BEGIN
		--IF SUBSTRING(@RefNum,LEN(@RefNum),1)='A'
		--BEGIN
		--	IF NOT EXISTS(SELECT RefineryID from TSort WHERE RefineryID = @RefNum)
		--	BEGIN
		--		SET @RefNum = SUBSTRING(@RefNum,1,LEN(@RefNum)-1)
		--	END
		--END
	SELECT 	
		c.FirstName + ' ' + c.LastName as CoordName,
		c.[SANumber],
       [ContactType] ,
       c.[FirstName] ,
       c.[LastName] ,
       [JobTitle] ,
       [PersonalTitle] ,
       [Phone] ,
       [PhoneSecondary] ,
       [Fax] ,
       c.[Email] ,
       [StrAddr1],
       [StrAddr2] ,
       [StrAdd3] ,
       [StrCity] ,
       [StrState] ,
       [StrZip] ,
       [StrCountry] ,
       [MailAddr1] ,
       [MailAddr2] ,
       [MailAddr3] ,
       [MailCity] ,
       [MailState] ,
       [MailZip] ,
       [MailCountry],
       [AltFirstName],
       [AltLastName],
       [AltEmail] ,
       [CCAlt] ,
       [SendMethod] ,
       [Comment] ,
		t.Country,
		l.Password as CompanyPassword
		--t.location + ', ' + t.[state] as Location
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	join PowerWork.dbo.Company_LU l on t.CompanyID = l.CompanyID
	WHERE t.SiteId = @SiteId 
	--t.RefineryID = dbo.FormatRefNum(@RefNum, 0)
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 
END
END
GO
GRANT ALTER, EXECUTE ON Console.[GetCompanyContactInfo] TO PUBLIC
GO

INSERT INTO  [Console].[SANumbers] (
		[SANumber],       [SiteID] ,       [RefNum] ) 
	   Values (1,'YYYCC16','YYYNA1C16')
GO
INSERT INTO  [Console].[SANumbers] (
		[SANumber],       [SiteID] ,       [RefNum] ) 
	   Values (2,'PTT1CC16','PTT1CC16')
GO

CREATE PROCEDURE [Console].[UpdateCoContact] 
	@SiteId nvarchar(12),
	@StudyYear int,
	@FirstName varchar(25),
	@LastName varchar(25),
	@JobTitle varchar(75),
	@Address1 varchar(75),
	@Address2 varchar(50),
	@Address3 nvarchar(50),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zip nvarchar(30),
	@Country nvarchar(30),
	@Phone nvarchar(40),
	@Email nvarchar(255),
	@StAddress1 as nvarchar(75),
	@StAddress2 as nvarchar(50),
	@StAddress3 as nvarchar(50),
	@StAddress4 as nvarchar(30),
	@StAddress5 as nvarchar(30),
	@StAddress6 as nvarchar(30),
	@StAddress7 as nvarchar(30),
	@Fax as nvarchar(40),
	@ContactType as nvarchar(10),
	@Password as nvarchar(50)
AS
BEGIN
IF EXISTS(SELECT *
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID = @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType )
UPDATE C 
SET
		c.FirstName=@FirstName ,
		c.LastName=@LastName ,
		c.JobTitle = @JobTitle,
		c.Email =@Email,
		c.Phone = @Phone,
		c.Fax = @Fax ,
		c.MailAddr1 = @Address1 ,
		c.MailAddr2 = @Address2 ,
		c.MailAddr3 = @Address3 ,
		c.MailCity = @City  ,
		c.MailState = @State ,
		c.MailZip = @Zip ,
		c.MailCountry = @Country ,
		c.StrAddr1 = @StAddress1,
		c.StrAddr2 = @StAddress2,
		c.StrAdd3 = @StAddress3,
		c.StrCity = @StAddress4,
		c.StrState = @StAddress5,
		c.StrZip = @StAddress6,
		c.StrCountry = @StAddress7,
		c.contactType = @ContactType		
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID= @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 	
ELSE	
	DECLARE @SANumber int
	SET @SANumber = (SELECT SANumber from [Console].[SANumbers] s 
		join TSort t on s.SiteID=t.SiteID 
		WHERE t.SiteID = @SiteId 
		AND t.StudyYear = @StudyYear )

		INSERT INTO CoContactInfo (
			[SANumber],[ContactType] ,[FirstName] ,[LastName] ,[JobTitle] ,[Phone] ,[Fax] ,
			[Email],[StrAddr1] ,[StrAddr2] ,[StrAdd3] ,[StrCity] ,[StrState] ,[StrZip] ,
			[StrCountry] ,[MailAddr1] ,[MailAddr2] ,[MailAddr3] ,[MailCity] ,[MailState] ,
			[MailZip] ,[MailCountry] ,[CCAlt]
		) values(					
			@SANumber,
			@ContactType,
			@FirstName ,
			@LastName ,
			@JobTitle,
			@Phone,
			@Fax,
			@Email,
			@StAddress1,
			@StAddress2,
			@StAddress3,
			@StAddress4,
			@StAddress5,
			@StAddress6,
			@STAddress7,
			@Address1 ,
			@Address2 ,
			@Address3 ,
			@City  ,
			@State ,
			@Zip ,
			@Country,
			'N')
END
GO
GRANT ALTER, EXECUTE ON [Console].[UpdateCoContact]  TO PUBLIC
GO

CREATE PROCEDURE Console.ChangeCoord
(
	 @SiteID nvarchar(15),
	 @PlantName char(50),
	 @CoordName char(40),
	 @CoordTitle char(50),
	 @CoordAddr1 char(50),
	 @CoordAddr2 char(50),
	 @CoordCity char(30),
	 @CoordState char(20),
	 @CoordZip char(15),
	 @CoordPhone char(30),
	 @CoordFax char(20),
	 @CoordEmail char(40)
)
AS
	IF( (select COUNT(SiteID) FROM [dbo].[ClientInfo] where SiteID=@SiteID)=0)
		INSERT INTO [dbo].[ClientInfo]( SiteID,PlantName,CoordName,CoordTitle,CoordAddr1,
		CoordAddr2,	CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail) VALUES
		(@SiteID, @PlantName, @CoordName, @CoordTitle, @CoordAddr1, @CoordAddr2,
		@CoordCity, @CoordState, @CoordZip, @CoordPhone, @CoordFax, @CoordEmail)
	ELSE
		UPDATE[dbo].[ClientInfo] SET 
		CoordName = @CoordName ,
		CoordTitle = @CoordTitle, 
		CoordAddr1 = @CoordAddr1 ,
		CoordAddr2 = @CoordAddr2 ,
		CoordCity = @CoordCity ,
		CoordState = @CoordState, 
		CoordZip = @CoordZip ,
		CoordPhone = @CoordPhone ,
		CoordFax = @CoordFax ,
		CoordEMail = @CoordEMail 	
		where SiteID=@SiteID
GO	
GRANT ALTER, EXECUTE ON Console.ChangeCoord TO PUBLIC
GO

CREATE PROCEDURE Console.GetCoord
(
	 @SiteID nvarchar(15)  --='007CC07   '; 	-- @SiteID nvarchar(15) ='PTTGC';
)
AS
	SELECT CoordName,CoordTitle,CoordAddr1,CoordAddr2,	CoordCity,
	CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail
	from [dbo].[ClientInfo] where SiteID=@SiteID
GO	
GRANT ALTER, EXECUTE ON Console.GetCoord TO PUBLIC
GO

CREATE PROCEDURE Console.GetConsultantBySiteId
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.ConsultantName from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId 
GO
GRANT ALTER, EXECUTE ON Console.GetConsultantBySiteId TO PUBLIC
GO

CREATE PROCEDURE Console.GetConsultantEmail
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.Email from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId 
GO
GRANT ALTER, EXECUTE ON Console.GetConsultantEmail TO PUBLIC
GO

CREATE PROCEDURE Console.GetTSortDataBySiteId
(
	@SiteId nvarchar(12)
)
AS
select 
Refnum,SiteID,CompanyID,CompanyName,UnitName,UnitLabel,UnitID,CoLoc,StudyYear,
EvntYear,Continent,Country,Region,State,PricingHub,Regulated,CalcCommUnavail,
EGCRegion,EGCTechnology,EGCManualTech,CTGs,CTG_NDC,NDC,Coal_NDC,Oil_NDC,
Gas_NDC,PrecBagYN,ScrubbersYN,NumUnitsAtSite,FuelType,Metric,HHV,CurrencyID,
FormerCurrencyID,SingleShaft,NumSTs,DryCoolTower
FROM dbo.TSort 
where SiteID=@SiteId
GO
GRANT ALTER, EXECUTE ON Console.GetTSortDataBySiteId TO PUBLIC
GO

GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [dbo].[ValidationNotes] TO PUBLIC
GO

CREATE PROCEDURE [Console].[GetConsultingOpportunities]
	@RefNum  nvarchar(12)
AS
BEGIN
	SELECT ConsultingOpportunities From ValidationNotes Where Refnum = @RefNum
END
GO
GRANT ALTER, EXECUTE ON Console.GetConsultingOpportunities TO PUBLIC
GO

CREATE PROCEDURE [Console].[InsertConsultingOpportunities]

	@RefNum nvarchar(12),
	@ConsultingOpportunities text

AS
BEGIN
	INSERT INTO dbo.ValidationNotes (Refnum, ConsultingOpportunities) VALUES (@RefNum, @ConsultingOpportunities)
END
GO
GRANT ALTER, EXECUTE ON Console.InsertConsultingOpportunities TO PUBLIC
GO

CREATE PROCEDURE [Console].[UpdateConsultingOpportunities]
	@RefNum nvarchar(12),
	@ConsultingOpportunities text
AS
BEGIN
	UPDATE dbo.ValidationNotes SET ConsultingOpportunities = @ConsultingOpportunities
	WHERE Refnum=@RefNum 
END
GO
GRANT ALTER, EXECUTE ON Console.UpdateConsultingOpportunities TO PUBLIC
GO

DROP  PROCEDURE Console.GetValidationNotes
GO
CREATE PROCEDURE [Console].[GetValidationNotes]
	@RefNum nvarchar(12)
AS
BEGIN
	SELECT ValidationNotes From Val.Notes Where RefNum = @RefNum
END
GO
GRANT ALTER, EXECUTE ON Console.GetValidationNotes TO PUBLIC
GO


--PROC IN PROD DOESN"T WORK
DROP PROCEDURE [Console].[UpdateCoContact] 
GO
CREATE PROCEDURE [Console].[UpdateCoContact] 
	@SiteId nvarchar(12),
	@StudyYear int,
	@FirstName varchar(25),
	@LastName varchar(25),
	@JobTitle varchar(75),
	@Address1 varchar(75),
	@Address2 varchar(50),
	@Address3 nvarchar(50),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zip nvarchar(30),
	@Country nvarchar(30),
	@Phone nvarchar(40),
	@Email nvarchar(255),
	@StAddress1 as nvarchar(75),
	@StAddress2 as nvarchar(50),
	@StAddress3 as nvarchar(50),
	@StAddress4 as nvarchar(30),
	@StAddress5 as nvarchar(30),
	@StAddress6 as nvarchar(30),
	@StAddress7 as nvarchar(30),
	@Fax as nvarchar(40),
	@ContactType as nvarchar(10),
	@Password as nvarchar(50)
AS
BEGIN
UPDATE PowerGlobal.dbo.Company_LU set Password = @Password where CompanyID = 
  (select CompanyID from dbo.TSort where SiteID = @SiteId);
IF EXISTS(
SELECT *
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID = @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 
)
  BEGIN
   UPDATE C SET c.FirstName=@FirstName ,
		c.LastName=@LastName ,
		c.JobTitle = @JobTitle,
		c.Email =@Email,
		c.Phone = @Phone,
		c.Fax = @Fax ,
		c.MailAddr1 = @Address1 ,
		c.MailAddr2 = @Address2 ,
		c.MailAddr3 = @Address3 ,
		c.MailCity = @City  ,
		c.MailState = @State ,
		c.MailZip = @Zip ,
		c.MailCountry = @Country ,
		c.StrAddr1 = @StAddress1,
		c.StrAddr2 = @StAddress2,
		c.StrAdd3 = @StAddress3,
		c.StrCity = @StAddress4,
		c.StrState = @StAddress5,
		c.StrZip = @StAddress6,
		c.StrCountry = @StAddress7,
		c.contactType = @ContactType		
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID= @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 	
  END
ELSE
  BEGIN
	DECLARE @SANumber int
	SET @SANumber = (SELECT SANumber from [Console].[SANumbers] s 
		join TSort t on s.SiteID=t.SiteID 
		WHERE t.SiteID = @SiteId 
		AND t.StudyYear = @StudyYear )
		INSERT INTO CoContactInfo (
			[SANumber],[ContactType] ,[FirstName] ,[LastName] ,[JobTitle] ,[Phone] ,[Fax] ,
			[Email],[StrAddr1] ,[StrAddr2] ,[StrAdd3] ,[StrCity] ,[StrState] ,[StrZip] ,
			[StrCountry] ,[MailAddr1] ,[MailAddr2] ,[MailAddr3] ,[MailCity] ,[MailState] ,
			[MailZip] ,[MailCountry] ,[CCAlt]
		) values(					
			@SANumber,
			@ContactType,
			@FirstName ,
			@LastName ,
			@JobTitle,
			@Phone,
			@Fax,
			@Email,
			@StAddress1,
			@StAddress2,
			@StAddress3,
			@StAddress4,
			@StAddress5,
			@StAddress6,
			@STAddress7,
			@Address1 ,
			@Address2 ,
			@Address3 ,
			@City  ,
			@State ,
			@Zip ,
			@Country,
			'N')
  END		
END
GO
GRANT ALTER, EXECUTE ON Console.UpdateCoContact TO PUBLIC
GO

CREATE PROCEDURE [dbo].[UpdatePassword]
(
	@RefNum nvarchar(12),
	@Password char(20) 
)
AS
BEGIN
	IF EXISTS(select * from dbo.Company_LU where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum))
	BEGIN
		UPDATE dbo.Company_LU 
		SET [Password] = @Password
		where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum)
	END
END
GO
GRANT ALTER, EXECUTE ON dbo.[UpdatePassword] TO PUBLIC
GO

--NOT USED in prod.
--change val checklist from refnum to siteid:
ALTER TABLE  [Val].[Checklist] DROP  PK_Checklist
GO
DROP TABLE [Val].[Checklist]
GO
CREATE TABLE [Val].[Checklist](
	[SiteId] [char](10) NOT NULL,
	[IssueID] [char](8) NOT NULL,
	[IssueTitle] [char](50) NOT NULL,
	[IssueText] [text] NULL,
	[PostedBy] [char](3) NOT NULL,
	[PostedTime] [datetime] NOT NULL,
	[Completed] [char](1) NOT NULL,
	[SetBy] [char](3) NULL,
	[SetTime] [datetime] NULL,
 CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED 
(
	[SiteId] ASC,
	[IssueID] ASC
))
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Val].[Checklist] TO PUBLIC
GO

CREATE TABLE Console.ConsultantInfo
(
	Id INTEGER IDENTITY,
	Initials nvarchar(20) NOT NULL,
	FullName nvarchar(50) NOT NULL,
	EmailAddress nvarchar(100),
	PhoneNumber nvarchar(20),
 CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC,
		[Initials] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON Console.ConsultantInfo TO PUBLIC
GO
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('SFB','Stuart Bard',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('MLB','Michael Brown',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('AJC','Tony Carrino',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('ELP','Ed Platt',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('CLR','Cliff Roberts',NULL,NULL);


CREATE PROCEDURE  [Console].[GetMFilesCompanyBenchmarkName]
	@FacilityId nvarchar(10),
	@SiteId nvarchar(15)
AS
BEGIN
    DECLARE @Result nvarchar(200) = (SELECT CONCAT(RTRIM(c.Refnum), ' - ', RTRIM(c.CoLoc)) 
	As CompanyBenchmarkName FROM PowerWork.dbo.TSort t  LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID = @FacilityId  LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.facilityid =  @FacilityId   LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID)  ON c.Study = 'Power' AND c.StudyYear = t.StudyYear AND fc.FacilityID =  @FacilityId   WHERE t.Siteid = @SiteId);
	SET @Result=RTRIM(@Result);
	SET @Result=LTRIM(@Result);
	IF LEN(@Result) = 1
		SET @Result = NULL;
	SELECT  @Result;
END
GO
GRANT ALTER, EXECUTE on [Console].[GetMFilesCompanyBenchmarkName] to PUBLIC
GO

GRant alter, select, insert, delete, update on Console.ConsoleLog to Public
go
GRant alter, execute on Console.[WriteLog] to Public
GO

CREATE TABLE [Console].[Employees](
	[Initials] [nvarchar](10) NULL,
	[ConsultantName] [nvarchar](80) NULL,
	[Active] [bit] NULL,
	[Email] [nvarchar](80) NULL
) 
GO
GRant ALTER, SELECT, INSERT, UPDATE, DELETE on Console.Employees to Public
GO

INSERT INTO Console.Employees values('AJC','Tony Carrino',1,'ajc@solomononline.com');
--INSERT INTO Console.Employees values('MLB','Mike Brown',1,'mlb@solomononline.com');
INSERT INTO Console.Employees values('EWP','Ed Platt',1,'ewp@solomononline.com');


CREATE PROCEDURE dbo.UpdateClientInfo
(
	@SiteID char(10),
	@CorpName char(50),
	@AffName char(50),
	@PlantName char(50),
	@City char(30),
	@State char(20),
	@CoordName char(40),
	@CoordTitle char(50),
	@CoordAddr1 char(50),
	@CoordAddr2 char(50),
	@CoordCity char(30),
	@CoordState char(20),
	@CoordZip char(15),
	@CoordPhone char(30),
	@CoordFax char(20),
	@CoordEMail char(40),
	@RptUOM varchar(3),
	@RptHV varchar(3),
	@RptSteamMethod varchar(3)
	
) AS
	IF EXISTS(SELECT TOP 1 * from dbo.ClientInfo where SiteID=@SiteID) 
		BEGIN
			UPDATE dbo.ClientInfo 
			SET 
			SiteID =  RTRIM(@SiteID),
			CorpName =  RTRIM(@CorpName),
			AffName =  RTRIM(@AffName),
			PlantName =  RTRIM(@PlantName),
			City =  RTRIM(@City),
			State =  RTRIM(@State),
			CoordName =  RTRIM(@CoordName),
			CoordTitle =  RTRIM(@CoordTitle),
			CoordAddr1 =  RTRIM(@CoordAddr1),
			CoordAddr2 =  RTRIM(@CoordAddr2),
			CoordCity =  RTRIM(@CoordCity),
			CoordState =  RTRIM(@CoordState),
			CoordZip =  RTRIM(@CoordZip),
			CoordPhone =  RTRIM(@CoordPhone),
			CoordFax =  RTRIM(@CoordFax),
			CoordEMail =  RTRIM(@CoordEMail),
			RptUOM =  RTRIM(@RptUOM),
			RptHV =  RTRIM(@RptHV),
			RptSteamMethod =  RTRIM(@RptSteamMethod)
			where SiteID=@SiteID
		END
	ELSE
	BEGIN
		INSERT INTO dbo.ClientInfo (SiteID,CorpName,AffName,PlantName,City,State,CoordName,CoordTitle,CoordAddr1,CoordAddr2,CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail,RptUOM,RptHV,RptSteamMethod)
		VALUES
		(RTRIM(@SiteID),
		RTRIM(@CorpName),
		RTRIM(@AffName),
		RTRIM(@PlantName),
		RTRIM(@City),
		RTRIM(@State),
		RTRIM(@CoordName),
		RTRIM(@CoordTitle),
		RTRIM(@CoordAddr1),
		RTRIM(@CoordAddr2),
		RTRIM(@CoordCity),
		RTRIM(@CoordState),
		RTRIM(@CoordZip),
		RTRIM(@CoordPhone),
		RTRIM(@CoordFax),
		RTRIM(@CoordEMail),
		RTRIM(@RptUOM),
		RTRIM(@RptHV),
		RTRIM(@RptSteamMethod)
		)
	END

GO
GRANT ALTER, EXECUTE ON dbo.UpdateClientInfo TO PUBLIC
GO

--===============================================================================================================
--MFIlesSupport 
INSERT INTO [MFilesSupport].[dbo].Facilities VALUES
('YYYCC','Karsto',	'Power');
INSERT INTO [MFilesSupport].[dbo].FacilityCompany VALUES
('YYYCC',	'NATURKRAFT');
INSERT INTO [MFilesSupport].[dbo].StudyCompanies VALUES
('Power'	,2016,'NATURKRAFT');
INSERT INTO [MFilesSupport].[dbo].StudyFacilities VALUES
('Power',2016,'YYYCC16','YYYCC','');
GO

--BEGIN INSERTS, use the following as template for future reuse.
--INSERT MFilesSupport.dbo.StudyCompanies(CompanyID, StudyYear, StudyID) 
--SELECT DISTINCT CompanyID, StudyYear, 'Power' FROM StudySites 
--WHERE SiteID NOT LIKE '%P' 
--AND StudyYear IN (SELECT StudyYear FROM MFilesSupport.dbo.Studies WHERE StudyID = 'Power') and CompanyID = 'NATURKRAFT' and StudyYear=2016;

--INSERT MFilesSupport.dbo.Companies(CompanyID, CompanyName)
--SELECT DISTINCT CompanyID, CompanyID FROM StudySites
--       where CompanyID = 'NATURKRAFT';
  	
--INSERT MFilesSupport.dbo.Facilities(FacilityType, FacilityID, Location)
--SELECT DISTINCT 'Power', SUBSTRING(SiteID, 1, LEN(SiteID)-2), (SELECT TOP 1 SiteName FROM StudySites n WHERE SUBSTRING(n.SiteID, 1, LEN(n.SiteID)-2) = SUBSTRING(ss.SiteID, 1, LEN(ss.SiteID)-2) AND SiteID LIKE '%[0-9]' ORDER BY StudyYear DESC)
--FROM StudySites ss
--WHERE SiteID LIKE '%[0-9]'
--       and siteid = 'YYYCC16';

--INSERT MFilesSupport.dbo.FacilityCompany(FacilityID, CompanyID) 
--SELECT DISTINCT SUBSTRING(SiteID, 1, LEN(SiteID)-2), (SELECT TOP 1 CompanyID FROM StudySites n WHERE SUBSTRING(n.SiteID, 1, LEN(n.SiteID)-2) = SUBSTRING(ss.SiteID, 1, LEN(ss.SiteID)-2) AND SiteID LIKE '%[0-9]' ORDER BY StudyYear DESC) 
--FROM StudySites ss 
--WHERE SiteID NOT LIKE '%P' 
--AND EXISTS (SELECT * FROM MFilesSupport.dbo.Facilities f WHERE f.FacilityID = SUBSTRING(SiteID, 1, LEN(SiteID)-2))
--and siteid = 'YYYCC16';

--INSERT MFilesSupport.dbo.StudyFacilities(FacilityID, Refnum, StudyID, StudyYear) 
--SELECT DISTINCT SUBSTRING(SiteID, 1, LEN(SiteID)-2), SiteID, 'Power', StudyYear FROM StudySites  
--WHERE SiteID LIKE '%[0-9]' 
--AND EXISTS (SELECT * FROM MFilesSupport.dbo.Studies s WHERE s.StudyID = 'Power' AND s.StudyYear = StudySites.StudyYear)       
--and siteid = 'YYYCC16';
