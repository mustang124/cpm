﻿USE [PowerWork]
GO


--DEV/TEST ONLY: cREATE SCHEMA Console
--DEV/TEST ONLY: GO

--DEV/TEST ONLY: GRANT ALTER ON [Console] to public
--DEV/TEST ONLY: GO


CREATE TABLE [Console].[ContinuingIssues](
	[IssueID] [int] IDENTITY(1,1) NOT NULL,
	[RefNum] [varchar](12) NOT NULL,
	[EntryDate] [datetime] NOT NULL CONSTRAINT [DF_ContinuingIssues_NoteDate]  DEFAULT (getdate()),
	[Consultant] [varchar](3) NULL,
	[Note] [nvarchar](max) NOT NULL,
	[Deleted] [bit] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [varchar](3) NULL,
 CONSTRAINT [PK_ContinuingIssues] PRIMARY KEY NONCLUSTERED 
(
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Console].[ContinuingIssues] TO PUBLIC
GO

CREATE PROCEDURE [Console].[AddOrUpdateContinuingIssues]
	@ID int,
	@RefNum nvarchar(20),
	@Issue nvarchar(max),
	@EditedBy nvarchar(3)
AS
BEGIN
	IF EXISTS(SELECT IssueID FROM [Console].[ContinuingIssues] where IssueID=@ID)
		BEGIN
			UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@EditedBy where IssueID=@ID
		END
		
		INSERT INTO [Console].[ContinuingIssues] values(@RefNum,getdate(),@EditedBy,@Issue,0,null,@EditedBy)

		SELECT @@IDENTITY
END
GO

GRANT ALTER, EXECUTE ON [Console].[AddOrUpdateContinuingIssues] TO PUBLIC
GO

CREATE PROCEDURE [Console].[DeleteContinuingIssue]
	@IssueID int,
	@DeletedBy nvarchar(3)
AS
BEGIN
	UPDATE [Console].[ContinuingIssues] SET Deleted = 1, DeletedBy=@DeletedBy where IssueID=@IssueID
END
GO
GRANT ALTER, EXECUTE ON [Console].[DeleteContinuingIssue] TO PUBLIC
GO

--DEV/TEST ONLY:  ALTER TABLE [Console].[ContinuingIssues] ALTER COLUMN [RefNum]  [varchar](12) NOT NULL
--DEV/TEST ONLY: GO

INSERT INTO [Console].[ContinuingIssues]
(--[IssueID]
[RefNum],[EntryDate],[Consultant],[Note],[Deleted],[DeletedDate],[DeletedBy] )
Values
('YYYNA1C16',GETDATE(),'SFB','TEST NEW CI',0,null,null)
GO

--DEV/TEST ONLY: 
/*CREATE TABLE [Val].[Checklist](
	[Refnum] [char](12) NOT NULL,
	[IssueID] [char](8) NOT NULL,
	[IssueTitle] [char](50) NOT NULL,
	[IssueText] [text] NULL,
	[PostedBy] [char](3) NOT NULL,
	[PostedTime] [datetime] NOT NULL,
	[Completed] [char](1) NOT NULL,
	[SetBy] [char](3) NULL,
	[SetTime] [datetime] NULL,
 CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC,
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Val].[Checklist] TO PUBLIC
GO


--DEV/TEST ONLY:  CREATE TABLE [dbo].[CoContactInfo](
       [SANumber] [int] NOT NULL,
       [ContactType] [char](5) NOT NULL,
       [FirstName] [varchar](25) NULL,
       [LastName] [varchar](25) NULL,
       [JobTitle] [varchar](75) NULL,
       [PersonalTitle] [varchar](5) NULL,
       [Phone] [varchar](40) NULL,
       [PhoneSecondary] [varchar](40) NULL,
       [Fax] [varchar](40) NULL,
       [Email] [varchar](255) NULL,
       [StrAddr1] [varchar](75) NULL,
       [StrAddr2] [varchar](50) NULL,
       [StrAdd3] [varchar](50) NULL,
       [StrCity] [varchar](30) NULL,
       [StrState] [varchar](30) NULL,
       [StrZip] [varchar](30) NULL,
       [StrCountry] [varchar](30) NULL,
       [MailAddr1] [varchar](75) NULL,
       [MailAddr2] [varchar](50) NULL,
       [MailAddr3] [varchar](50) NULL,
       [MailCity] [varchar](30) NULL,
       [MailState] [varchar](30) NULL,
       [MailZip] [varchar](30) NULL,
       [MailCountry] [varchar](30) NULL,
       [AltFirstName] [varchar](20) NULL,
       [AltLastName] [varchar](25) NULL,
       [AltEmail] [varchar](255) NULL,
       [CCAlt] [varchar](1) NULL, --Y or N
       [SendMethod] [char](3) NULL,
       [Comment] [varchar](255) NULL,
CONSTRAINT [PK_CoContactInfo] PRIMARY KEY CLUSTERED 
(
       [SANumber] ASC,
       [ContactType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70)
)
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON  [PowerWork].[dbo].[CoContactInfo] TO PUBLIC
GO

--DEV/TEST ONLY:  CREATE TABLE [Console].[SANumbers]
(
		[ID] [int] IDENTITY NOT NULL,
		[SANumber] [int] NOT NULL,
       [SiteID] [nvarchar](9) NULL,
       [RefNum] [nvarchar](12) NULL,
	   CONSTRAINT [PK_[SANumbers] PRIMARY KEY 
(
       [SANumber]
))
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Console].[SANumbers] TO PUBLIC
GO

CREATE TABLE [dbo].[StudySites](
	[SiteID] nvarchar(12) NOT NULL,
	[CompanyID] [char](15) NOT NULL,
	[SiteName] [varchar](50) NOT NULL,
	[SiteLabel] [char](5) NULL,
	[SiteNo] [char](5) NOT NULL,
	[StudyYear] [int] NOT NULL,
	[SiteDirectory] [varchar](100) NULL,
	[Consultant] [char](4) NULL,
 CONSTRAINT [PK_StudySites] PRIMARY KEY CLUSTERED 
(
	[SiteID] ASC
)
)
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [dbo].[StudySites] TO PUBLIC
GO
INSERT INTO  [PowerWork].[dbo].[StudySites] VALUES	(						
'YYYCC16',	'NATURKRAFT',	'Karsto',	'KAR',	'NAT',	'2016',	'\\DALLAS2\DATA\Data\Study\Power\2016P\Plant\YYYCC16',	'');
GO


*/
INSERT INTO [dbo].[CoContactInfo]
([SANumber],[ContactType],[FirstName],[LastName],[JobTitle],[PersonalTitle]
,[Phone],[PhoneSecondary],[Fax],[Email],[StrAddr1],[StrAddr2],[StrAdd3],[StrCity]
,[StrState],[StrZip],[StrCountry],[MailAddr1],[MailAddr2],[MailAddr3],[MailCity],[MailState]
,[MailZip],[MailCountry],[AltFirstName],[AltLastName],[AltEmail],[CCAlt],[SendMethod],[Comment])
     VALUES
(1,'ALT','TOM','EDISON','ALTERNATE CONTACT',
NULL,'2345678901',NULL,'','T.EDISON@DCCURRENT.COM','','','','MENLO PARK','NJ','23456','USA',
'','','','MENLO PARK','NJ','23456','USA',NULL,NULL,NULL,'N',NULL,NULL);

INSERT INTO [dbo].[CoContactInfo]
([SANumber],[ContactType],[FirstName],[LastName],[JobTitle],[PersonalTitle]
,[Phone],[PhoneSecondary],[Fax],[Email],[StrAddr1],[StrAddr2],[StrAdd3],[StrCity]
,[StrState],[StrZip],[StrCountry],[MailAddr1],[MailAddr2],[MailAddr3],[MailCity],[MailState]
,[MailZip],[MailCountry],[AltFirstName],[AltLastName],[AltEmail],[CCAlt],[SendMethod],[Comment])
     VALUES
(1,'COORD','Ben','Franklin','Company Coordinator',
NULL,'1234567890',NULL,'','b.frank@goflyakite.com','123 4th st','#500','','Philadelphia','PA','12345','USA',
'123 4th st','#500','','Philadelphia','PA','12345','USA',NULL,NULL,NULL,'N',NULL,NULL);
GO

CREATE PROCEDURE [Console].[GetCompanyPassword]
(
	@SiteID char(10), 
	@StudyYear int  
)
AS
	select l.Password as CompanyPassword 
	FROM TSort t 
	inner join Company_LU l on t.CompanyID = l.CompanyID
	WHERE t.SiteID=@SiteID
	AND t.StudyYear = @StudyYear 
GO
GRANT ALTER, EXECUTE ON [Console].[GetCompanyPassword] TO PUBLIC
GO



DROP PROCEDURE  [Console].[GetCompanyContactInfo]
GO
CREATE PROCEDURE  [Console].[GetCompanyContactInfo]
	@SiteId nvarchar(10),
	@StudyYear int,
	@ContactType char(5)
AS
BEGIN
	IF @SiteId is not null 
	BEGIN
		--IF SUBSTRING(@RefNum,LEN(@RefNum),1)='A'
		--BEGIN
		--	IF NOT EXISTS(SELECT RefineryID from TSort WHERE RefineryID = @RefNum)
		--	BEGIN
		--		SET @RefNum = SUBSTRING(@RefNum,1,LEN(@RefNum)-1)
		--	END
		--END
	SELECT 	
		c.FirstName + ' ' + c.LastName as CoordName,
		c.[SANumber],
       [ContactType] ,
       c.[FirstName] ,
       c.[LastName] ,
       [JobTitle] ,
       [PersonalTitle] ,
       [Phone] ,
       [PhoneSecondary] ,
       [Fax] ,
       c.[Email] ,
       [StrAddr1],
       [StrAddr2] ,
       [StrAdd3] ,
       [StrCity] ,
       [StrState] ,
       [StrZip] ,
       [StrCountry] ,
       [MailAddr1] ,
       [MailAddr2] ,
       [MailAddr3] ,
       [MailCity] ,
       [MailState] ,
       [MailZip] ,
       [MailCountry],
       [AltFirstName],
       [AltLastName],
       [AltEmail] ,
       [CCAlt] ,
       [SendMethod] ,
       [Comment] ,
		t.Country,
		l.Password as CompanyPassword
		--t.location + ', ' + t.[state] as Location
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	join PowerWork.dbo.Company_LU l on t.CompanyID = l.CompanyID
	WHERE t.SiteId = @SiteId 
	--t.RefineryID = dbo.FormatRefNum(@RefNum, 0)
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 
END
END
GO
GRANT ALTER, EXECUTE ON Console.[GetCompanyContactInfo] TO PUBLIC
GO

INSERT INTO  [Console].[SANumbers] (
		[SANumber],       [SiteID] ,       [RefNum] ) 
	   Values (1,'YYYCC16','YYYNA1C16')
GO
INSERT INTO  [Console].[SANumbers] (
		[SANumber],       [SiteID] ,       [RefNum] ) 
	   Values (2,'PTT1CC16','PTT1CC16')
GO



DROP PROCEDURE [Console].[UpdateCoContact] 
GO
CREATE PROCEDURE [Console].[UpdateCoContact] 
	@SiteId nvarchar(12),
	@StudyYear int,
	@FirstName varchar(25),
	@LastName varchar(25),
	@JobTitle varchar(75),
	@Address1 varchar(75),
	@Address2 varchar(50),
	@Address3 nvarchar(50),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zip nvarchar(30),
	@Country nvarchar(30),
	@Phone nvarchar(40),
	@Email nvarchar(255),
	@StAddress1 as nvarchar(75),
	@StAddress2 as nvarchar(50),
	@StAddress3 as nvarchar(50),
	@StAddress4 as nvarchar(30),
	@StAddress5 as nvarchar(30),
	@StAddress6 as nvarchar(30),
	@StAddress7 as nvarchar(30),
	@Fax as nvarchar(40),
	@ContactType as nvarchar(10),
	@Password as nvarchar(50)
AS
BEGIN
IF EXISTS(SELECT *
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID = @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType )
UPDATE C 
SET
		c.FirstName=@FirstName ,
		c.LastName=@LastName ,
		c.JobTitle = @JobTitle,
		c.Email =@Email,
		c.Phone = @Phone,
		c.Fax = @Fax ,
		c.MailAddr1 = @Address1 ,
		c.MailAddr2 = @Address2 ,
		c.MailAddr3 = @Address3 ,
		c.MailCity = @City  ,
		c.MailState = @State ,
		c.MailZip = @Zip ,
		c.MailCountry = @Country ,
		c.StrAddr1 = @StAddress1,
		c.StrAddr2 = @StAddress2,
		c.StrAdd3 = @StAddress3,
		c.StrCity = @StAddress4,
		c.StrState = @StAddress5,
		c.StrZip = @StAddress6,
		c.StrCountry = @StAddress7,
		c.contactType = @ContactType		
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID= @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 	
ELSE	
	DECLARE @SANumber int
	SET @SANumber = (SELECT SANumber from [Console].[SANumbers] s 
		join TSort t on s.SiteID=t.SiteID 
		WHERE t.SiteID = @SiteId 
		AND t.StudyYear = @StudyYear )

		INSERT INTO CoContactInfo (
			[SANumber],[ContactType] ,[FirstName] ,[LastName] ,[JobTitle] ,[Phone] ,[Fax] ,
			[Email],[StrAddr1] ,[StrAddr2] ,[StrAdd3] ,[StrCity] ,[StrState] ,[StrZip] ,
			[StrCountry] ,[MailAddr1] ,[MailAddr2] ,[MailAddr3] ,[MailCity] ,[MailState] ,
			[MailZip] ,[MailCountry] ,[CCAlt]
		) values(					
			@SANumber,
			@ContactType,
			@FirstName ,
			@LastName ,
			@JobTitle,
			@Phone,
			@Fax,
			@Email,
			@StAddress1,
			@StAddress2,
			@StAddress3,
			@StAddress4,
			@StAddress5,
			@StAddress6,
			@STAddress7,
			@Address1 ,
			@Address2 ,
			@Address3 ,
			@City  ,
			@State ,
			@Zip ,
			@Country,
			'N')
END
GO
GRANT ALTER, EXECUTE ON [Console].[UpdateCoContact]  TO PUBLIC
GO
/*
already done above
drop PROCEDURE Console.GetCompanyPassword
GO
CREATE PROCEDURE Console.GetCompanyPassword
(
	@SiteId nvarchar(12), -- = 'PTT1CC16'
	@StudyYear int  --= 2016;
)
AS
select l.Password as CompanyPassword 
FROM TSort t 
inner join Company_LU l on t.CompanyID = l.CompanyID
WHERE t.SiteID = @SiteId 
AND t.StudyYear = @StudyYear 
GO
GRANT ALTER, EXECUTE ON Console.GetCompanyPassword TO PUBLIC
GO
*/
CREATE PROCEDURE Console.ChangeCoord
(
	 @SiteID nvarchar(15),
	 @PlantName char(50),
	 @CoordName char(40),
	 @CoordTitle char(50),
	 @CoordAddr1 char(50),
	 @CoordAddr2 char(50),
	 @CoordCity char(30),
	 @CoordState char(20),
	 @CoordZip char(15),
	 @CoordPhone char(30),
	 @CoordFax char(20),
	 @CoordEmail char(40)
)
AS
	IF( (select COUNT(SiteID) FROM [dbo].[ClientInfo] where SiteID=@SiteID)=0)
		INSERT INTO [dbo].[ClientInfo]( SiteID,PlantName,CoordName,CoordTitle,CoordAddr1,
		CoordAddr2,	CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail) VALUES
		(@SiteID, @PlantName, @CoordName, @CoordTitle, @CoordAddr1, @CoordAddr2,
		@CoordCity, @CoordState, @CoordZip, @CoordPhone, @CoordFax, @CoordEmail)
	ELSE
		UPDATE[dbo].[ClientInfo] SET 
		CoordName = @CoordName ,
		CoordTitle = @CoordTitle, 
		CoordAddr1 = @CoordAddr1 ,
		CoordAddr2 = @CoordAddr2 ,
		CoordCity = @CoordCity ,
		CoordState = @CoordState, 
		CoordZip = @CoordZip ,
		CoordPhone = @CoordPhone ,
		CoordFax = @CoordFax ,
		CoordEMail = @CoordEMail 	
		where SiteID=@SiteID
GO	
GRANT ALTER, EXECUTE ON Console.ChangeCoord TO PUBLIC
GO

CREATE PROCEDURE Console.GetCoord
(
	 @SiteID nvarchar(15)  --='007CC07   '; 	-- @SiteID nvarchar(15) ='PTTGC';
)
AS
	SELECT CoordName,CoordTitle,CoordAddr1,CoordAddr2,	CoordCity,
	CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail
	from [dbo].[ClientInfo] where SiteID=@SiteID
GO	
GRANT ALTER, EXECUTE ON Console.GetCoord TO PUBLIC
GO

--DEV/TEST ONLY: 
/*
CREATE TABLE [dbo].[TSort](
	[Refnum] [nvarchar](12) NOT NULL,
	[SiteID] [nvarchar](10) NOT NULL,
	[CompanyID] [char](15) NOT NULL,
	[CompanyName] [char](50) NULL,
	[UnitName] [varchar](50) NOT NULL,
	[UnitLabel] [char](5) NULL,
	[UnitID] [char](8) NOT NULL,
	[CoLoc] [varchar](100) NULL,
	[StudyYear] [int] NOT NULL,
	[EvntYear] [int] NULL,
	[Continent] [varchar](20) NULL,
	[Country] [varchar](50) NULL,
	[Region] [char](15) NULL,
	[State] [char](20) NULL,
	[PricingHub] [char](15) NULL,
	[Regulated] [nvarchar](1) NULL,
	[CalcCommUnavail] [nvarchar](1) NOT NULL DEFAULT ('Y'),
	[EGCRegion] [char](15) NULL,
	[EGCTechnology] [varchar](15) NULL,
	[EGCManualTech] [bit] NULL,
	[CTGs] [tinyint] NULL,
	[CTG_NDC] [real] NULL,
	[NDC] [real] NULL,
	[Coal_NDC] [real] NULL,
	[Oil_NDC] [real] NULL,
	[Gas_NDC] [real] NULL,
	[PrecBagYN] [tinyint] NULL,
	[ScrubbersYN] [tinyint] NULL,
	[NumUnitsAtSite] [real] NULL,
	[FuelType] [varchar](8) NULL,
	[Metric] [bit] NULL,
	[HHV] [bit] NULL,
	[CurrencyID] [int] NULL,
	[FormerCurrencyID] [int] NULL,
	[SingleShaft] [real] NULL,
	[NumSTs] [real] NULL,
	[DryCoolTower] [real] NULL,
 CONSTRAINT [PK_TSort] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON dbo.TSort TO PUBLIC
GO
INSERT INTO dbo.TSort (
Refnum,	SiteID,	CompanyID,	CompanyName,	UnitName,	UnitLabel,	UnitID,	CoLoc,	StudyYear,	EvntYear,	Continent,	Country,	Region,	State,	PricingHub,	Regulated,	CalcCommUnavail,	EGCRegion,	EGCTechnology,	EGCManualTech,	CTGs,	CTG_NDC,	NDC,	Coal_NDC,	Oil_NDC,	Gas_NDC,	PrecBagYN,	ScrubbersYN,	NumUnitsAtSite,	FuelType,	Metric,	HHV,	CurrencyID,	FormerCurrencyID,	SingleShaft,	NumSTs,	DryCoolTower) VALUES (
'YYYNA1C16   ',	'YYYCC16   ',	'YNATURKRAFT    ',	'Naturkraft                                        ',	'KARSTO CCGT',	'KA   ',	'SKAR    ',	'YNATURKRAFT – KARSTO CCGT (KA)',	2016,	2010,	'Europe',	'Norway',	'W Europe       ',	NULL, 	'Nordpool NO5   ',	'N',	'N',	'W Europe       ',	'CCGT-E',	0,	1,	287,	420,	NULL, 	NULL, 	NULL, 	0,	0,	1,	'Gas',	1,	0,	41,	NULL, 	NULL, 	NULL, 	NULL);
*/

CREATE PROCEDURE Console.GetConsultantBySiteId
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.ConsultantName from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId 
GO
GRANT ALTER, EXECUTE ON Console.GetConsultantBySiteId TO PUBLIC
GO

CREATE PROCEDURE Console.GetConsultantEmail
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select e.Email from Console.Employees e 
inner join dbo.StudySites s on s.Consultant = e.Initials
where SiteID = @SiteId 
GO
GRANT ALTER, EXECUTE ON Console.GetConsultantEmail TO PUBLIC
GO

CREATE PROCEDURE Console.GetTSortDataBySiteId
(
	@SiteId nvarchar(12)
	--, @StudyYear int  --= 2016;
)
AS
select 
Refnum,SiteID,CompanyID,CompanyName,UnitName,UnitLabel,UnitID,CoLoc,StudyYear,
EvntYear,Continent,Country,Region,State,PricingHub,Regulated,CalcCommUnavail,
EGCRegion,EGCTechnology,EGCManualTech,CTGs,CTG_NDC,NDC,Coal_NDC,Oil_NDC,
Gas_NDC,PrecBagYN,ScrubbersYN,NumUnitsAtSite,FuelType,Metric,HHV,CurrencyID,
FormerCurrencyID,SingleShaft,NumSTs,DryCoolTower
FROM dbo.TSort 
where SiteID=@SiteId
GO
GRANT ALTER, EXECUTE ON Console.GetTSortDataBySiteId TO PUBLIC
GO

CREATE TABLE [dbo].[ValidationNotes](
	[Refnum] [nvarchar](12) NOT NULL,
	[ValidationNotes] [text] NULL,
	[ConsultingOpportunities] [text] NULL,
 CONSTRAINT [PK_ValidationNotes] PRIMARY KEY CLUSTERED 
(
	[Refnum] ASC
)
)
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [dbo].[ValidationNotes] TO PUBLIC
GO


CREATE PROCEDURE [Console].[GetConsultingOpportunities]
	@RefNum  nvarchar(12)
AS
BEGIN
	SELECT ConsultingOpportunities From Val.Notes Where Refnum = @RefNum
END
GO
GRANT ALTER, EXECUTE ON Console.GetConsultingOpportunities TO PUBLIC
GO

CREATE PROCEDURE [Console].[InsertConsultingOpportunities]

	@RefNum nvarchar(12),
	@ConsultingOpportunities text

AS
BEGIN
	INSERT INTO dbo.ValidationNotes (Refnum, ConsultingOpportunities) VALUES (@RefNum, @ConsultingOpportunities)
END
GO
GRANT ALTER, EXECUTE ON Console.InsertConsultingOpportunities TO PUBLIC
GO

CREATE PROCEDURE [Console].[UpdateConsultingOpportunities]
	@RefNum nvarchar(12),
	@ConsultingOpportunities text
AS
BEGIN
	UPDATE dbo.ValidationNotes SET ConsultingOpportunities = @ConsultingOpportunities
	WHERE Refnum=@RefNum 
END
GO
GRANT ALTER, EXECUTE ON Console.UpdateConsultingOpportunities TO PUBLIC
GO


DROP  PROCEDURE Console.GetValidationNotes
GO
CREATE PROCEDURE [Console].[GetValidationNotes]
	@RefNum nvarchar(12)
AS
BEGIN
	SELECT ValidationNotes From Val.Notes Where RefNum = @RefNum
END
GO
GRANT ALTER, EXECUTE ON Console.GetValidationNotes TO PUBLIC
GO

drop PROCEDURE [Console].[InsertValidationNotes]
GO
CREATE PROCEDURE [Console].[InsertValidationNotes]

	@RefNum nvarchar(12),
	@Notes text

AS
BEGIN
	INSERT INTO dbo.ValidationNotes (RefNum, ValidationNotes) VALUES (@RefNum, @Notes)
END
GO
GRANT ALTER, EXECUTE ON Console.InsertValidationNotes TO PUBLIC
GO

DROP PROCEDURE [Console].[UpdateValidationNotes]
GO
CREATE PROCEDURE [Console].[UpdateValidationNotes]
	@RefNum nvarchar(12),
	@Notes text
AS
BEGIN
	UPDATE dbo.ValidationNotes SET ValidationNotes = @Notes
	WHERE RefNum=@RefNum 
END
GO
GRANT ALTER, EXECUTE ON Console.UpdateValidationNotes TO PUBLIC
GO

--PROC IN PROD DOESN"T WORK
DROP PROCEDURE [Console].[UpdateCoContact] 
GO
CREATE PROCEDURE [Console].[UpdateCoContact] 
	@SiteId nvarchar(12),
	@StudyYear int,
	@FirstName varchar(25),
	@LastName varchar(25),
	@JobTitle varchar(75),
	@Address1 varchar(75),
	@Address2 varchar(50),
	@Address3 nvarchar(50),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zip nvarchar(30),
	@Country nvarchar(30),
	@Phone nvarchar(40),
	@Email nvarchar(255),
	@StAddress1 as nvarchar(75),
	@StAddress2 as nvarchar(50),
	@StAddress3 as nvarchar(50),
	@StAddress4 as nvarchar(30),
	@StAddress5 as nvarchar(30),
	@StAddress6 as nvarchar(30),
	@StAddress7 as nvarchar(30),
	@Fax as nvarchar(40),
	@ContactType as nvarchar(10),
	@Password as nvarchar(50)
AS
BEGIN
UPDATE PowerGlobal.dbo.Company_LU set Password = @Password where CompanyID = 
  (select CompanyID from dbo.TSort where SiteID = @SiteId);
IF EXISTS(
SELECT *
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID = @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 
)
  BEGIN
   UPDATE C SET c.FirstName=@FirstName ,
		c.LastName=@LastName ,
		c.JobTitle = @JobTitle,
		c.Email =@Email,
		c.Phone = @Phone,
		c.Fax = @Fax ,
		c.MailAddr1 = @Address1 ,
		c.MailAddr2 = @Address2 ,
		c.MailAddr3 = @Address3 ,
		c.MailCity = @City  ,
		c.MailState = @State ,
		c.MailZip = @Zip ,
		c.MailCountry = @Country ,
		c.StrAddr1 = @StAddress1,
		c.StrAddr2 = @StAddress2,
		c.StrAdd3 = @StAddress3,
		c.StrCity = @StAddress4,
		c.StrState = @StAddress5,
		c.StrZip = @StAddress6,
		c.StrCountry = @StAddress7,
		c.contactType = @ContactType		
	FROM [dbo].[CoContactInfo] c 
	join [Console].[SANumbers] s on c.SANumber=s.SANumber
	join TSort t on s.SiteID=t.SiteID 
	WHERE t.SiteID= @SiteId
	AND t.StudyYear = @StudyYear 
	AND UPPER(c.ContactType)= @ContactType 	
  END
ELSE
  BEGIN
	DECLARE @SANumber int
	SET @SANumber = (SELECT SANumber from [Console].[SANumbers] s 
		join TSort t on s.SiteID=t.SiteID 
		WHERE t.SiteID = @SiteId 
		AND t.StudyYear = @StudyYear )
		INSERT INTO CoContactInfo (
			[SANumber],[ContactType] ,[FirstName] ,[LastName] ,[JobTitle] ,[Phone] ,[Fax] ,
			[Email],[StrAddr1] ,[StrAddr2] ,[StrAdd3] ,[StrCity] ,[StrState] ,[StrZip] ,
			[StrCountry] ,[MailAddr1] ,[MailAddr2] ,[MailAddr3] ,[MailCity] ,[MailState] ,
			[MailZip] ,[MailCountry] ,[CCAlt]
		) values(					
			@SANumber,
			@ContactType,
			@FirstName ,
			@LastName ,
			@JobTitle,
			@Phone,
			@Fax,
			@Email,
			@StAddress1,
			@StAddress2,
			@StAddress3,
			@StAddress4,
			@StAddress5,
			@StAddress6,
			@STAddress7,
			@Address1 ,
			@Address2 ,
			@Address3 ,
			@City  ,
			@State ,
			@Zip ,
			@Country,
			'N')
  END		
END
GO
GRANT ALTER, EXECUTE ON Console.UpdateCoContact TO PUBLIC
GO
--DEV/TEST ONLY: 
/*
CREATE TABLE [PowerGlobal].[dbo].[Company_LU](
	[CompanyID] [char](15) NOT NULL,
	[CompanyName] [char](50) NOT NULL,
	[LastName] [char](40) NULL,
	[FirstName] [char](40) NULL,
	[Email] [char](50) NULL,
	[Password] [char](20) NULL,
	[Regulated] [char](1) NULL,
 CONSTRAINT [PK_CompanyLU_2__17] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
))
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [PowerGlobal].[dbo].[Company_LU] TO PUBLIC
GO
INSERT INTO [PowerGlobal].[dbo].[Company_LU] (							
CompanyID,	CompanyName,	LastName,	FirstName,	Email,	Password,	Regulated) VALUES (	
'NATURKRAFT     ',	'NATURKRAFT                                        ',	'Mosaker                                 ',	'Ivar                                    ',	'i.mosaker@naturkraft.no                           ',	NULL, 	NULL);	
GO
INSERT INTO PowerGlobal.dbo.Company_LU(CompanyID,CompanyName) VALUES
('YNATURKRAFT','YNATURKRAFT');
GO
*/

CREATE VIEW [dbo].[Company_LU] AS 
SELECT * FROM PowerGlobal.dbo.Company_LU
GO

CREATE PROCEDURE [dbo].[UpdatePassword]
(
	@RefNum nvarchar(12),
	@Password char(20) 
)
AS
BEGIN
	IF EXISTS(select * from dbo.Company_LU where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum))
	BEGIN
		UPDATE dbo.Company_LU 
		SET [Password] = @Password
		where CompanyID = (select CompanyID from dbo.TSort where Refnum=@RefNum)
	END
END
GO
GRANT ALTER, EXECUTE ON dbo.[UpdatePassword] TO PUBLIC
GO

--change val checklist from refnum to siteid:
ALTER TABLE  [Val].[Checklist] DROP  PK_Checklist
GO
DROP TABLE [Val].[Checklist]
GO
CREATE TABLE [Val].[Checklist](
	[SiteId] [char](10) NOT NULL,
	[IssueID] [char](8) NOT NULL,
	[IssueTitle] [char](50) NOT NULL,
	[IssueText] [text] NULL,
	[PostedBy] [char](3) NOT NULL,
	[PostedTime] [datetime] NOT NULL,
	[Completed] [char](1) NOT NULL,
	[SetBy] [char](3) NULL,
	[SetTime] [datetime] NULL,
 CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED 
(
	[SiteId] ASC,
	[IssueID] ASC
))
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON [Val].[Checklist] TO PUBLIC
GO




CREATE TABLE Console.ConsultantInfo
(
	Id INTEGER IDENTITY,
	Initials nvarchar(20) NOT NULL,
	FullName nvarchar(50) NOT NULL,
	EmailAddress nvarchar(100),
	PhoneNumber nvarchar(20),
 CONSTRAINT [PK_Checklist] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC,
		[Initials] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON Console.ConsultantInfo TO PUBLIC
GO
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('SFB','Stuart Bard',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('MLB','Michael Brown',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('AJC','Tony Carrino',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('ELP','Ed Platt',NULL,NULL);
INSERT INTO Console.ConsultantInfo(Initials,FullName,EmailAddress,PhoneNumber) VALUES
('CLR','Cliff Roberts',NULL,NULL);



--DEV/TEST ONLY: 
---------  MFilesSupport  -------------------------
--NOTE: the value for the 2nd field in Facilities if found in PowerWork.dbo.StudySites
/* - don't need these for prod, already exist:
CREATE TABLE [dbo].[StudyCompanies](
	[StudyID] [varchar](5) NOT NULL,
	[StudyYear] [smallint] NOT NULL,
	[CompanyID] [varchar](15) NOT NULL,
 CONSTRAINT [PK_StudyCompanies] PRIMARY KEY CLUSTERED 
(
	[StudyID] ASC,
	[StudyYear] ASC,
	[CompanyID] ASC
)
)
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON dbo.StudyCompanies TO PUBLIC
GO
CREATE TABLE [dbo].[Facilities](
	[FacilityID] [varchar](15) NOT NULL,
	[Location] [varchar](100) NOT NULL,
	[FacilityType] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Facilities] PRIMARY KEY CLUSTERED 
(
	[FacilityID] ASC
)
)
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON dbo.[Facilities] TO PUBLIC
GO
CREATE TABLE [dbo].[FacilityCompany](
	[FacilityID] [varchar](15) NOT NULL,
	[CompanyID] [varchar](15) NOT NULL,
 CONSTRAINT [PK_FacilityCompany] PRIMARY KEY CLUSTERED 
(
	[FacilityID] ASC
)
)
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON dbo.[FacilityCompany] TO PUBLIC
GO
CREATE TABLE [dbo].[StudyFacilities](
	[StudyID] [varchar](5) NOT NULL,
	[StudyYear] [smallint] NOT NULL,
	[Refnum] [varchar](25) NOT NULL,
	[FacilityID] [varchar](15) NOT NULL,
	[CaseName] [varchar](30) NOT NULL CONSTRAINT [DF_StudyFacilities_CaseName]  DEFAULT (''),
 CONSTRAINT [PK_StudyFacilities] PRIMARY KEY CLUSTERED 
(
	[StudyID] ASC,
	[StudyYear] ASC,
	[Refnum] ASC
)
)
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON dbo.[StudyFacilities] TO PUBLIC
GO
CREATE TABLE [dbo].[Companies](
	[CompanyID] [varchar](15) NOT NULL,
	[CompanyName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)
)
GO
GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON dbo.[Companies] TO PUBLIC
GO
*/

--DEV/TEST ONLY: 
/*  
INSERT INTO MFilesSupport.[dbo].[Companies] VALUES ('NATURKRAFT','NATURKRAFT');
INSERT INTO [MFilesSupport].[dbo].Facilities VALUES
('YYYCC','Karsto',	'Power');
INSERT INTO [MFilesSupport].[dbo].FacilityCompany VALUES
('YYYCC',	'NATURKRAFT');
INSERT INTO [MFilesSupport].[dbo].StudyCompanies VALUES
('Power'	,2016,'NATURKRAFT');
INSERT INTO [MFilesSupport].[dbo].StudyFacilities VALUES
('Power',2016,'YYYCC16','YYYCC','');
GO	
*/

--BEGIN INSERTS, use the following as template for future reuse.
/* MIGHT NEED FOR FUTURE:
INSERT MFilesSupport.dbo.Companies(CompanyID, CompanyName)
SELECT DISTINCT CompanyID, CompanyID FROM StudySites
       where CompanyID = 'NATURKRAFT';
*/

INSERT MFilesSupport.dbo.StudyCompanies(CompanyID, StudyYear, StudyID) 
SELECT DISTINCT CompanyID, StudyYear, 'Power' FROM StudySites 
WHERE SiteID NOT LIKE '%P' 
AND StudyYear IN (SELECT StudyYear FROM MFilesSupport.dbo.Studies WHERE StudyID = 'Power') and CompanyID = 'NATURKRAFT' and StudyYear=2016;

/* MIGHT NEED FOR FUTURE:
INSERT MFilesSupport.dbo.Facilities(FacilityType, FacilityID, Location)
SELECT DISTINCT 'Power', SUBSTRING(SiteID, 1, LEN(SiteID)-2), (SELECT TOP 1 SiteName FROM StudySites n WHERE SUBSTRING(n.SiteID, 1, LEN(n.SiteID)-2) = SUBSTRING(ss.SiteID, 1, LEN(ss.SiteID)-2) AND SiteID LIKE '%[0-9]' ORDER BY StudyYear DESC)
FROM StudySites ss
WHERE SiteID LIKE '%[0-9]'
       and siteid = 'YYYCC16';
*/

INSERT MFilesSupport.dbo.FacilityCompany(FacilityID, CompanyID) 
SELECT DISTINCT SUBSTRING(SiteID, 1, LEN(SiteID)-2), (SELECT TOP 1 CompanyID FROM StudySites n WHERE SUBSTRING(n.SiteID, 1, LEN(n.SiteID)-2) = SUBSTRING(ss.SiteID, 1, LEN(ss.SiteID)-2) AND SiteID LIKE '%[0-9]' ORDER BY StudyYear DESC) 
FROM StudySites ss 
WHERE SiteID NOT LIKE '%P' 
AND EXISTS (SELECT * FROM MFilesSupport.dbo.Facilities f WHERE f.FacilityID = SUBSTRING(SiteID, 1, LEN(SiteID)-2))
and siteid = 'YYYCC16';

INSERT MFilesSupport.dbo.StudyFacilities(FacilityID, Refnum, StudyID, StudyYear) 
SELECT DISTINCT SUBSTRING(SiteID, 1, LEN(SiteID)-2), SiteID, 'Power', StudyYear FROM StudySites  
WHERE SiteID LIKE '%[0-9]' 
AND EXISTS (SELECT * FROM MFilesSupport.dbo.Studies s WHERE s.StudyID = 'Power' AND s.StudyYear = StudySites.StudyYear)       
and siteid = 'YYYCC16';
GO


CREATE PROCEDURE  [Console].[GetMFilesCompanyBenchmarkName]
	@FacilityId nvarchar(10),
	@SiteId nvarchar(15)
AS
BEGIN
    DECLARE @Result nvarchar(200) = (SELECT CONCAT(RTRIM(c.Refnum), ' - ', RTRIM(c.CoLoc)) 
	As CompanyBenchmarkName FROM PowerWork.dbo.TSort t  LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID = @FacilityId  LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.facilityid =  @FacilityId   LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID)  ON c.Study = 'Power' AND c.StudyYear = t.StudyYear AND fc.FacilityID =  @FacilityId   WHERE t.Siteid = @SiteId);
	SET @Result=RTRIM(@Result);
	SET @Result=LTRIM(@Result);
	IF LEN(@Result) = 1
		SET @Result = NULL;
	SELECT  @Result;
END
GO
GRANT ALTER, EXECUTE on [Console].[GetMFilesCompanyBenchmarkName] to PUBLIC
GO


CREATE TABLE [Console].[ConsoleLog](
	[UserName] [nvarchar](20) NULL,
	[LogType] [nvarchar](20) NULL,
	[Message] [nvarchar](2000) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]

GRant alter, select, insert, delete, update on Console.ConsoleLog to Public
go

CREATE PROC [Console].[WriteLog]
	@UserName nvarchar(20),
	@LogType nvarchar(20),
	@Message nvarchar(2000)	
AS	
	INSERT INTO Console.ConsoleLog
	VALUES(@UserName,@LogType,@Message,GETDATE())
GO
GRant alter, execute on Console.[WriteLog] to Public

CREATE TABLE [Console].[Employees](
	[Initials] [nvarchar](10) NULL,
	[ConsultantName] [nvarchar](80) NULL,
	[Active] [bit] NULL,
	[Email] [nvarchar](80) NULL
) 
GO
GRant ALTER, SELECT, INSERT, UPDATE, DELETE on Console.Employees to Public
GO

INSERT INTO Console.Employees values('AJC','Tony Carrino',1,'ajc@solomononline.com');
--INSERT INTO Console.Employees values('MLB','Mike Brown',1,'mlb@solomononline.com');
INSERT INTO Console.Employees values('EWP','Ed Platt',1,'ewp@solomononline.com');
GO

CREATE PROCEDURE dbo.UpdateClientInfo
(
	@SiteID char(10),
	@CorpName char(50),
	@AffName char(50),
	@PlantName char(50),
	@City char(30),
	@State char(20),
	@CoordName char(40),
	@CoordTitle char(50),
	@CoordAddr1 char(50),
	@CoordAddr2 char(50),
	@CoordCity char(30),
	@CoordState char(20),
	@CoordZip char(15),
	@CoordPhone char(30),
	@CoordFax char(20),
	@CoordEMail char(40),
	@RptUOM varchar(3),
	@RptHV varchar(3),
	@RptSteamMethod varchar(3)
	
) AS
	IF EXISTS(SELECT TOP 1 * from dbo.ClientInfo where SiteID=@SiteID) 
		BEGIN
			UPDATE dbo.ClientInfo 
			SET 
			SiteID =  RTRIM(@SiteID),
			CorpName =  RTRIM(@CorpName),
			AffName =  RTRIM(@AffName),
			PlantName =  RTRIM(@PlantName),
			City =  RTRIM(@City),
			State =  RTRIM(@State),
			CoordName =  RTRIM(@CoordName),
			CoordTitle =  RTRIM(@CoordTitle),
			CoordAddr1 =  RTRIM(@CoordAddr1),
			CoordAddr2 =  RTRIM(@CoordAddr2),
			CoordCity =  RTRIM(@CoordCity),
			CoordState =  RTRIM(@CoordState),
			CoordZip =  RTRIM(@CoordZip),
			CoordPhone =  RTRIM(@CoordPhone),
			CoordFax =  RTRIM(@CoordFax),
			CoordEMail =  RTRIM(@CoordEMail),
			RptUOM =  RTRIM(@RptUOM),
			RptHV =  RTRIM(@RptHV),
			RptSteamMethod =  RTRIM(@RptSteamMethod)
			where SiteID=@SiteID
		END
	ELSE
	BEGIN
		INSERT INTO dbo.ClientInfo (SiteID,CorpName,AffName,PlantName,City,State,CoordName,CoordTitle,CoordAddr1,CoordAddr2,CoordCity,CoordState,CoordZip,CoordPhone,CoordFax,CoordEMail,RptUOM,RptHV,RptSteamMethod)
		VALUES
		(RTRIM(@SiteID),
		RTRIM(@CorpName),
		RTRIM(@AffName),
		RTRIM(@PlantName),
		RTRIM(@City),
		RTRIM(@State),
		RTRIM(@CoordName),
		RTRIM(@CoordTitle),
		RTRIM(@CoordAddr1),
		RTRIM(@CoordAddr2),
		RTRIM(@CoordCity),
		RTRIM(@CoordState),
		RTRIM(@CoordZip),
		RTRIM(@CoordPhone),
		RTRIM(@CoordFax),
		RTRIM(@CoordEMail),
		RTRIM(@RptUOM),
		RTRIM(@RptHV),
		RTRIM(@RptSteamMethod)
		)
	END

GO
GRANT ALTER, EXECUTE ON dbo.UpdateClientInfo TO PUBLIC
GO

CREATE PROCEDURE Console.GetChecklist
(
	@SiteId char(10)
)
AS
SELECT SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime FROM VAL.CHECKLIST WHERE RTRIM(SiteId) = @SiteId
GO
GRANT ALTER, EXECUTE ON Console.GetChecklist to PUBLIC
GO

CREATE PROCEDURE [Console].[GetContinuingIssue]
(
	@RefNum varchar(12),
	@IssueID int
)
AS
	SELECT IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby 
	From [Console].[ContinuingIssues] 
	Where RTRIM(RefNum) = @RefNum and IssueID = @IssueID and deleted = 0 order by EntryDate;
GO
GRANT ALTER, EXECUTE ON Console.GetContinuingIssue to PUBLIC
GO

CREATE PROCEDURE Console.GetContinuingIssueByConsultant
(
	@RefNum varchar(12),
	@Consultant varchar(3)
)
AS
	SELECT IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby 
	From [Console].[ContinuingIssues] 
	Where RTRIM(RefNum) = @RefNum and Consultant = @Consultant and deleted = 0 order by EntryDate;
GO
GRANT ALTER, EXECUTE ON Console.GetContinuingIssueByConsultant to PUBLIC
GO

CREATE PROCEDURE Console.GetSiteIds
(
	@Year int
)
AS
	SELECT SiteID from [dbo].[StudySites] where StudyYear = @Year and RTRIM(SiteID) NOT LIKE '%P';
GO
GRANT ALTER, EXECUTE ON Console.GetSiteIds to PUBLIC
GO

CREATE PROCEDURE Console.GetCompanynamesWithSiteIds
(
	@Year int
)
AS
	SELECT CONCAT(RTRIM(SiteName)  , CHAR(10), CHAR(45),RTRIM(SiteID) ) As Site from [dbo].[StudySites] 
	where StudyYear = @Year and RTRIM(SiteID) NOT LIKE '%P' order by Site asc;
GO
GRANT ALTER, EXECUTE ON Console.GetCompanynamesWithSiteIds to PUBLIC
GO

CREATE PROCEDURE Console.GetRefNumsBySiteId
(
	@SiteId char(10),
	@Year int
)
AS
	SELECT Refnum from [dbo].[TSort] 
	where SiteID = @SiteId AND StudyYear = @Year AND RTRIM(Refnum) NOT LIKE '%P' order by refnum asc;
GO
GRANT ALTER, EXECUTE ON Console.GetRefNumsBySiteId to PUBLIC
GO

CREATE PROCEDURE Console.GetUnitnamesWithRefNums
(
	@SiteId char(10),
	@Year int
)
AS
	SELECT CONCAT(RTRIM(UnitName)  , CHAR(10), CHAR(45),RTRIM(Refnum) ) As UnitnameRefnum from [dbo].[TSort] 
	where SiteID = @SiteId  AND RTRIM(SiteID) NOT LIKE '%P' AND StudyYear = @Year AND RTRIM(Refnum) NOT LIKE '%P' 
	order by UnitnameRefnum asc;
GO
GRANT ALTER, EXECUTE ON Console.GetUnitnamesWithRefNums to PUBLIC
GO

CREATE PROCEDURE Console.GetConsultantInitials
(
	@SiteId char(10)
)
AS
	Select Consultant from dbo.StudySites where SiteId = @SiteId;
GO
GRANT ALTER, EXECUTE ON Console.GetConsultantInitials to PUBLIC
GO

CREATE PROCEDURE Console.GetConsultantName
(
	@ConsultantInitials nvarchar(10)
)
AS
	SELECT ConsultantName from Console.Employees  where Initials = @ConsultantInitials;
GO
GRANT ALTER, EXECUTE ON Console.GetConsultantName to PUBLIC
GO

CREATE PROCEDURE Console.GetAllConsultantsByYear
(
	@Year int
)
AS
	SELECT DISTINCT Consultant from [dbo].[StudySites] 
	where StudyYear =@Year
	and SiteID not like '%P'
	and Consultant <> ''
	AND Consultant IS NOT NULL
GO
GRANT ALTER, EXECUTE ON Console.GetAllConsultantsByYear to PUBLIC
GO

CREATE PROCEDURE Console.ChangeConsultant
(
	@SiteId char(10),
	@Year int,
	@Consultant char(4)
)
AS
	UPDATE dbo.StudySites set Consultant = @Consultant 
	where StudyYear =@Year and SiteID=@SiteId
	AND EXISTS(SELECT * from Console.Employees E where E.Initials = RTRIM(@Consultant))
GO
GRANT ALTER, EXECUTE ON Console.ChangeConsultant to PUBLIC
GO



--GRANT ALTER, SELECT, INSERT, UPDATE, DELETE ON ??? TO PUBLIC
--GRANT ALTER, EXECUTE ON ??? TO PUBLIC




