﻿
-- for 2016 study

INSERT INTO [NGPP].[Console].[Employees]
([Initials]
      ,[ConsultantName]
      ,[StudyYear]
      ,[StudyType]
      ,[Active]
      ,[Email])
	  VALUES (
	  'RWH','Richard Harper',2016,'NGPP',1,'Richard.Harper');

update [NGPP].[dim].[Companies] set coPassword = 'Foothills' where CompanyID = 'SHELL';

INSERT INTO [NGPP].[dbo].[CoContactInfo]
(
[StudyYear],[CompanyID],[ContactType],[FirstName],[LastName],[Email],[Phone],
[StrAddr1],[StrState],[StrZip],[StrCountry])
 VALUES (
2016,'SHELL','COORD','Abhishek','Banerjee','abhishek.banerjee@shell.com','403-384-8860',
'400 4th Ave SW', 'Calgary', 'Alberta',  'T2P 2H5')
;

UPDATE  [NGPP].[dbo].[CoContactInfo] set 
[FirstName]= 'Jay',
[LastName]='Schofield',
[Email]='jay.v.schofield@tsocorp.com',
[Phone]='307-922-5745',
[StrAddr1]='1955 Blairtown Road',
[StrCity] = 'Rock Springs',
[StrState]= 'WY',
[StrZip]= '82901',
[StrCountry] = 'USA'
WHERE [StudyYear] = 2016 AND  [CompanyID] = 'Tesoro' and [ContactType] = 'COORD';

