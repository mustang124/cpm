﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
'Imports System.ComponentModel
Imports System.Threading
'Imports System.IO
'Imports System.Text
'Imports System.IO.Compression
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports SA.Internal.Console.DataObject
Imports System.Text.RegularExpressions
Imports System
Imports System.Xml
Imports SA.Console.Common
Imports System.Globalization


Public Class MainConsole



#Region "PRIVATE VARIABLES"

    Dim FirstTime As Boolean = True
    Dim CI As String
    Dim CILastEditedDate As String
    Dim CILastEditedBy As String
    Dim CIIssueDate As String
    Dim CIConsultant As String
    Dim CIDeleted As String
    Dim CIDeletedDate As String
    Dim CIDeletedBy As String
    Dim SavedFile As String = Nothing
    Dim CompleteIssue As String = "N"
    Dim CurrentTab As String
    Dim ValidationIssuesIsDirty As Boolean = False
    Dim Time As Integer = 0
    Dim LastTime As Integer = 0
    Dim CompanyContact As Contacts
    Dim AltCompanyContact As Contacts
    Dim OlefinsContact As Contacts
    Dim RefineryContact As Contacts
    Dim InterimContact As Contacts
    Dim PlantContact As Contacts
    Dim PowerContact As Contacts
    Dim OpsContact As Contacts
    Dim TechContact As Contacts
    Dim PricingContact As Contacts
    Dim DataContact As Contacts
    Dim CompanyPassword As String = ""
    Dim SelectedRow As Integer
    Dim TabDirty(9) As Boolean
    Dim ds As DataSet
    Dim db As DataObject


    Dim sDir(100) As String
    Dim StudyRegion As String = ""
    Dim Study As String = ""

    Dim SANumber As String

    Dim CurrentConsultant As String
    Dim CurrentRefNum As String

    Dim IDRPath As String
    Dim JustLooking As Boolean = False
    Dim CurrentCompany As String
    Dim Company As String
    Dim Coloc As String
    Dim CompCorrPath As String
    Dim GeneralPath As String
    Dim ProjectPath As String
    Dim TempPath As String
    Dim OldTempPath As String = ""
    Dim TemplatePath As String
    Dim DirResult() As String
    Dim PolishPath As String

    Dim StudyDrive As String

    Dim CurrentPath As String
    Dim RefNum As String
    Dim bJustLooking As Boolean = True
    Dim RefNumPart As String
    Dim LubRefNum As String
    Dim StudySave(16) As String
    Dim RefNumSave(16) As String

    Dim KeyEntered As Boolean = False

    'Private mDBConnection As String
    'Private mStudyYear As String
    Private mPassword As String
    Private mRefNum As String
    Private mSpawn As Boolean
    'Private mStudyType As String
    Private mUserName As String
    Private mReturnPassword As String
    Private VFFiles As New List(Of String)
    Private VRFiles As New List(Of String)
    Private VAFiles As New List(Of String)
    Private RetFiles As New List(Of String)

    Dim VF, VR, VA, Ret, No As Integer


    'Private Err As ErrorLog
    Private _studyType As String = "REFINING" 'maybe should be EUR or NSA etc instead????
    Private _lastStudyAndRefnum As String()
    Private _yearRefnumsAndPlants As IYearRefnumsAndPlants
    Private _profileConsoleTempPath As String = ProfileConsolePath + "temp\"
    Private _userWindowsProfileName As String = String.Empty
    'Private _lastChosenRefNum As String = String.Empty
    Private _pathToPriorDatachecksFile As String = String.Empty
    Private _pathToCurrentDatachecksFile As String = String.Empty
    Private Const _delim As Char = Chr(14)
    Private _docsAccess As DocsFileSystemAccess
    'Private _useMFiles As Boolean = False
    'Private _templatePath As String = String.Empty
    Private _settingPath As String = _profileConsoleTempPath + "ConsoleSettings\"
    '
    'Private _mfilesFailed As Boolean = False
    Private _settings As Dictionary(Of String, String) = New Dictionary(Of String, String)
    Private _xmlAppSettingsPath As String = _settingPath + "RefineryAppSettings.xml"
    Private _formIsLoaded As Boolean = False
    Private _profileConsoleExtractedPath As String = _profileConsoleTempPath + "Extracted Files\"

    'Private _mfilesBenchmarkingParticipantVPath As String = String.Empty
    'Private _mfilesBenchmarkingCompanyVPath As String = String.Empty
    'Private _mfilesRefiningGeneralVPath As String = String.Empty
    Private _dragDropMode As Integer = 0 '1 for dragdrop is processing files, 2 for dragdrop is processing an email
    Private _benchmarkingParticipantRefineryFiles As ConsoleFilesInfo
    Private _benchmarkingParticipantCompanyFiles As ConsoleFilesInfo
    Private _refiningGeneralFiles As ConsoleFilesInfo
    'Private _mfilesDesktopUIPath As String = String.Empty
    Private _profileConsoleReferencePath As String = ProfileConsolePath + "Reference\"
    Private _profileConsoleReferenceRefiningPath As String = _profileConsoleReferencePath + "Refining\"

    'Private _selectedChecklistNode As TreeNode = Nothing
    Private _checklistDataTable As DataTable = New DataTable()
    Private _checklistManager As RefiningChecklistManager = Nothing
    Private _checklistNodeChecked As Boolean = False
    Private _vertical As SA.Console.Common.VerticalType = Common.VerticalType.REFINING
    Private curCulture As CultureInfo = Thread.CurrentThread.CurrentCulture
    'Create TextInfo object.
    Private _tInfo As TextInfo = curCulture.TextInfo()


#End Region

#Region "PUBLIC PROPERTIES"

    Private mVPN As Boolean

    Public Property VPN() As Boolean
        Get
            Return mVPN
        End Get
        Set(ByVal value As Boolean)
            mVPN = value
        End Set
    End Property

    'Public Property DBConnection() As String
    '    Get
    '        Return mDBConnection
    '    End Get
    '    Set(ByVal value As String)
    '        mDBConnection = value
    '    End Set
    'End Property

    Private mCurrentStudyYear As String

    Public Property CurrentStudyYear() As String
        Get
            Return mCurrentStudyYear
        End Get
        Set(ByVal value As String)
            mCurrentStudyYear = value
        End Set
    End Property

    'Public Property StudyYear() As String
    '    Get
    '        Return mStudyYear
    '    End Get
    '    Set(ByVal value As String)
    '        mStudyYear = value
    '    End Set
    'End Property

    Public Property UserName() As String

        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set

    End Property

    Public Property UserWindowsProfileName() As String
        Get
            If (IsNothing(_userWindowsProfileName)) OrElse (_userWindowsProfileName.Length < 1) Then
                ' = My.User.Name + ".DC1"
                Dim fullName As String = My.User.Name
                If Not fullName.Contains("\") Then
                    _userWindowsProfileName = fullName
                Else
                    Dim temp As String() = My.User.Name.Split("\")

                    _userWindowsProfileName = temp(1) + "." + temp(0)
                End If
            End If

            Return _userWindowsProfileName
        End Get
        Set(ByVal value As String)
            _userWindowsProfileName = value
        End Set
    End Property

    Public Property ReturnPassword() As String
        Get
            Return mReturnPassword
        End Get
        Set(ByVal value As String)
            mReturnPassword = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property

    'Public Property StudyType() As String
    '    Get
    '        Return mStudyType
    '    End Get
    '    Set(ByVal value As String)
    '        mStudyType = value
    '    End Set
    'End Property

    Public Property ReferenceNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property

    Public Property Spawn() As Boolean
        Get
            Return mSpawn
        End Get
        Set(ByVal value As Boolean)
            mSpawn = value
        End Set
    End Property

#End Region

#Region "INITIALIZATION"

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub MainConsole_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Initialize()
        _formIsLoaded = True
    End Sub

    Private Sub Initialize()
        If File.Exists("C:\Dc1Sfb.txt") Then File.Delete("C:\Dc1Sfb.txt")
        Try
            Dim badConsoleFile As String = ProfileConsolePath.Remove(ProfileConsolePath.Length - 1) 'a CONSOLE file left by the batch file, need folder instead.
            If File.Exists(badConsoleFile) Then File.Delete(badConsoleFile)

            If Not CreateConsoleFolder(ProfileConsolePath) Then End
            If Not CreateConsoleFolder(_profileConsoleTempPath) Then End
            If Not CreateConsoleFolder(_profileConsoleTempPath & "SecureSend") Then End
            If Not CreateConsoleFolder(_profileConsoleTempPath & "ConsoleSettings") Then End
            If Not CreateConsoleFolder(_profileConsoleTempPath & "Extracted Files") Then End
            If Not CreateConsoleFolder(_profileConsoleTempPath & "ZIP") Then End

            If Not CreateConsoleFolder(_profileConsoleReferencePath) Then End
            If Not CreateConsoleFolder(_profileConsoleReferenceRefiningPath) Then End


            If Tracing Then
                Using sr As New StreamWriter(TracingFile, False) 'clear out old
                    sr.WriteLine(DateTime.Now.ToString())
                End Using

                Try
                    Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
                    Dim version As System.Version = assembly.GetName().Version
                    T("Version: " & String.Format("Version {0}", version))
                Catch
                End Try

            End If
            T("Initialize")

            '//CleanFilesFromFolder(_profileConsoleTempPath)??
            If Directory.Exists(_profileConsoleTempPath) Then
                Dim siblingFiles() As String
                Try
                    Dim filesList = ConfigurationManager.AppSettings("RefiningValidationFileSiblings").ToString()
                    siblingFiles = filesList.Split("^")
                Catch
                    siblingFiles = "".Split("^")
                End Try

                Dim dir As New DirectoryInfo(_profileConsoleTempPath)
                Dim files() As FileInfo = dir.GetFiles()
                For i As Integer = 0 To files.Count - 1
                    Try
                        Dim deleteFlag As Boolean = True
                        If files(i).Name = "FL16Macros_IDR.xlsm" Then
                            deleteFlag = False
                        End If
                        For j As Integer = 0 To siblingFiles.Length - 1
                            If files(i).Name.Trim().ToUpper() = Utilities.ExtractFileName(siblingFiles(j).Trim().ToUpper()) Then
                                deleteFlag = False
                                Exit For
                            End If
                        Next
                        If deleteFlag Then
                            Kill(files(i).FullName)
                        End If
                    Catch
                    End Try
                Next
            End If
            CleanFilesFromFolder(_profileConsoleTempPath + "SecureSend")
            CleanFilesFromFolder(_profileConsoleTempPath + "Extracted Files")
            CleanFilesFromFolder(_profileConsoleTempPath + "ZIP")
            'CleanFilesFromFolder(ProfileConsolePath)

            If ConsoleVertical = VerticalType.REFINING Then ' StudyType = "REFINING" Then
                db = New DataObject(DataObject.StudyTypes.REFINING, UserName, Password)
            End If

            'Hide Query and QA tabs per Joe 2/2/17
            If Not CheckRole("QA", UserName) Then
                RemoveTab("tabQA")
            End If
            If Not CheckRole("DB", UserName) Then RemoveTab("tabClippy")
            'If CheckRole("ADMIN", UserName) Or CheckRole("Admin", UserName) Then
            'btnBug.Visible = True
            'End If
            'now check for last place they left off: study year, refinery.

            'Check for existance of Settings file, if missing, copy default one
            GetSettingsFile() 'gets list of studies and refnums used before

            Dim studyRegionAndYear As String = ReadLastStudy() ' gets the last study (i.e. year) that the use was on when last used Console
            Dim loadedSettings As Boolean = False
            T("Initialize-calling New YearRefnumsAndPlants")
            _yearRefnumsAndPlants = New YearRefnumsAndPlants(ConsoleVertical, db) 'this populates the GetStudyYears() return for 
            '   _yearRefnumsAndPlants; i.e., EUR16 PAC16 LUB16 etc.
            For Each item As String In _yearRefnumsAndPlants.StudyYears
                cboStudy.Items.Add(item)
            Next

            'Try
            '    _mfilesDesktopUIPath = ConfigurationManager.AppSettings("MfilesUIPath")
            'Catch
            'End Try


            'If not spawned off an existing instance of the program, read the settings file and set the drop downs accordingly
            If Not Spawn Then
                T("Initialize-NotSpawn")
                'check sizes for mismatch. Fix mismatch if exists.
                If _yearRefnumsAndPlants.StudyYears.Length <> _lastStudyAndRefnum.Length Then
                    T("Initialize-_yearRefnumsAndPlants.StudyYears.Length <> _lastStudyAndRefnum.Length")
                    'if mismatch, create new array (sort one of the others)
                    Dim lastStudyYearsCount As Integer = 0
                    Dim tempArray() As String = _yearRefnumsAndPlants.StudyYears.Clone()
                    T("Initialize-tempArray() As String")
                    'sort one since in in diff order
                    Array.Sort(tempArray)
                    'populate new array
                    For count As Integer = 0 To tempArray.Length - 1
                        If count < _lastStudyAndRefnum.Length AndAlso tempArray(count) = _lastStudyAndRefnum(count).Substring(0, 4) Then
                            tempArray(count) = _lastStudyAndRefnum(count)
                        Else
                            tempArray(count) = tempArray(count) + "^"
                        End If
                    Next
                    _lastStudyAndRefnum = tempArray
                    T("Initialize-_lastStudyAndRefnum = tempArray")
                End If

                'we will be loading from txt file, so find last refnum we used and pass in to YearRefnumPlantsInfo
                'This finds last refnum
                Dim refNumToStartWith As String = String.Empty
                T("Initialize-Dim refNumToStartWith As String = String.Empty")
                For count As Integer = 0 To _lastStudyAndRefnum.Count - 1
                    Dim parts As String() = _lastStudyAndRefnum(count).Split(_delim)
                    If parts(0) = studyRegionAndYear Then
                        refNumToStartWith = parts(1)
                        Exit For
                    End If
                Next



                'if refNumToStartWith blank, then just load cbos and leave text blank.

                'otherwise, load cbos and select refNumToStartWith and corresponding company
                Dim yearRefnumPlantsInfo As YearRefnumPlantsInfo = ChangeStudy(studyRegionAndYear, refNumToStartWith) ' will change cboStudy and refresh _yearRefnumsAndPlants
                T("Initialize-Dim yearRefnumPlantsInfo As YearRefnumPlantsInfo = ChangeStudy")
                If Not IsNothing(yearRefnumPlantsInfo) Then
                    T("Initialize-Not IsNothing(yearRefnumPlantsInfo)")
                    If refNumToStartWith.Length > 0 Then
                        If ChangedRefnumOrCompany(refNumToStartWith, "") Then
                            T("Initialize-ChangedRefnumOrCompany")
                            'won't get here if MFiles failed...
                            loadedSettings = True
                            'done in ChangedRefnumOrCompany above ClearAllControls()
                            'done in ChangedRefnumOrCompany above If Not PopulateForm() Then Exit Sub
                            'done in ChangedRefnumOrCompany above If Not PopulateTabs() Then Exit Sub
                        End If
                        ''CboStudyStartupRoutine(yearRefnumPlantsInfo)
                        'yearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangeRefnum(refNumToStartWith)
                        'If yearRefnumPlantsInfo.SelectedRefNum.Length > 0 Then
                        '    'ChangeColocRefnum(yearRefnumPlantsInfo)
                        '    For count As Integer = 0 To cboRefNum.Items.Count - 1
                        '        Dim thisItem As String = cboRefNum.Items(count).ToString().Trim()
                        '        If thisItem = yearRefnumPlantsInfo.SelectedRefNum Then
                        '            'trying to fire event
                        '            cboRefNum.SelectedItem = cboRefNum.Items(count)
                        '            Exit For
                        '        End If
                        '    Next
                        '    'CboRefNumSelectionChangeCommittedRoutine()
                        '    loadedSettings = True
                        'End If
                    Else
                        'just populate refnum and compay cbos
                        If cboRefNum.Items.Count < 1 Then
                            For Each refn As String In yearRefnumPlantsInfo.RefNums
                                cboRefNum.Items.Add(refn)
                            Next
                        End If
                        If cboRefNum.Items.Count > 1 Then
                            cboRefNum.SelectedIndex = 0
                        End If
                        If cboCompany.Items.Count < 1 Then
                            For Each plnt As String In yearRefnumPlantsInfo.Plants
                                cboCompany.Items.Add(plnt)
                            Next
                        End If
                        If cboCompany.Items.Count > 1 Then
                            cboCompany.SelectedIndex = 0
                        End If
                    End If
                End If
            Else
                T("Initialize-Sapwn")
                'Spawn: Don't know if this is in use anymore so did not test it.
                If ReferenceNum <> "" Then
                    Dim whatToFind As String = ParseRefNum(ReferenceNum, 2)
                    Dim twoDigitStudyYear As String = ReferenceNum.Substring(3, ReferenceNum.Length - 3)
                    twoDigitStudyYear = System.Text.RegularExpressions.Regex.Replace(twoDigitStudyYear, "[^0-9.]", "")
                    whatToFind = whatToFind + twoDigitStudyYear
                    cboStudy.SelectedIndex = cboStudy.FindString(whatToFind)
                Else
                    cboStudy.SelectedIndex = 0
                End If
            End If
            lbl_BlueFlags.Text = ""
            lbl_RedFlags.Text = ""
            lbl_ItemRemaining.Text = ""
        Catch ex As System.Exception
            db.WriteLog("Initialize", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub

#End Region

    Private Function ChangeStudy(newStudy As String, Optional refNumToStartWith As String = "") As YearRefnumPlantsInfo
        T("ChangeStudy")
        If newStudy.Length < 1 Then Return Nothing
        '_mfilesFailed = False
        Dim yearRefnumPlantsInfo As YearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangeYear(newStudy, db, refNumToStartWith)
        T("ChangeStudy-Dim yearRefnumPlantsInfo As YearRefnumPlantsInfo")
        For i As Integer = 0 To cboStudy.Items.Count - 1
            If cboStudy.Items(i).ToString().Trim() = newStudy.Trim() Then
                cboStudy.SelectedIndex = i
                Exit For
            End If
        Next
        Study = yearRefnumPlantsInfo.SelectedStudyYear.Substring(0, 3)
        T("ChangeStudy-Study = yearRefnumPlantsInfo.SelectedStudyYear.Substring")
        'moved this to ChangedRenumOrCompay
        'UpdateLastStudyRefnum(_yearRefnumsAndPlants.GetStudyYear(), yearRefnumPlantsInfo.SelectedRefNum)
        Dim studyChangedTo As String = yearRefnumPlantsInfo.SelectedStudyYear
        T("ChangeStudy-Dim studyChangedTo As String")
        Dim year As String = "20" + System.Text.RegularExpressions.Regex.Replace(yearRefnumPlantsInfo.SelectedStudyYear, "[^0-9.]", "") ' get only the numbers from cboStudy
        '_useMFiles = (Int32.Parse(year) = 2016)

        Return yearRefnumPlantsInfo
    End Function
    Private Function ChangedRefnumOrCompany(ByVal newRefNum As String, ByVal newCompany As String) As Boolean
        T("ChangedRefnumOrCompany")
        Try
            '_mfilesFailed = False
            RefNum = newRefNum
            Dim yearRefnumPlantsInfo As YearRefnumPlantsInfo
            If newRefNum.Length < 1 AndAlso newCompany.Length < 1 Then Exit Function
            If newRefNum.Length < 1 AndAlso newCompany.Length > 0 Then
                'only have company passed in
                yearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangePlant(newCompany)
                T("ChangedRefnumOrCompany-yearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangePlant(newCompany)")
                newRefNum = yearRefnumPlantsInfo.SelectedRefNum
            Else
                yearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangeRefnum(newRefNum)
                T("ChangedRefnumOrCompany-yearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangeRefnum(newRefNum)")
            End If
            UpdateLastStudyRefnum(_yearRefnumsAndPlants.GetStudyYear(), yearRefnumPlantsInfo.SelectedRefNum)
            If yearRefnumPlantsInfo.SelectedRefNum.Length > 0 Then
                T("ChangedRefnumOrCompany-yearRefnumPlantsInfo.SelectedRefNum.Length > 0")
                If cboRefNum.Items.Count < 1 Then
                    For Each refn As String In yearRefnumPlantsInfo.RefNums
                        cboRefNum.Items.Add(refn)
                    Next
                End If
                If cboCompany.Items.Count < 1 Then
                    For Each plnt As String In yearRefnumPlantsInfo.Plants
                        cboCompany.Items.Add(plnt)
                    Next
                End If
                For count As Integer = 0 To cboRefNum.Items.Count - 1
                    If cboRefNum.Items(count).ToString().Trim() = yearRefnumPlantsInfo.SelectedRefNum.Trim() Then
                        cboRefNum.SelectedIndex = count
                        Exit For
                    End If
                Next
                For count As Integer = 0 To cboCompany.Items.Count - 1
                    If cboCompany.Items(count).ToString().Trim() = yearRefnumPlantsInfo.SelectedPlant.Trim() Then
                        cboCompany.SelectedIndex = count
                        Exit For
                    End If
                Next
                SaveCboSettings()
                _docsAccess = New DocsFileSystemAccess(ConsoleVertical, False)
                '_docsAccess = New DocsFileSystemAccess(ConsoleVertical, _useMFiles)
                'If _useMFiles And Not _mfilesFailed Then
                '    T("ChangedRefnumOrCompany-_useMFiles And Not _mfilesFailed")
                '    Try
                '        _docsAccess.NextV("VF", newRefNum, newRefNum)
                '    Catch exMFiles As Exception
                '        If exMFiles.Message.Contains("Unable to find Benchmarking Participant") Then
                '            _mfilesFailed = True
                '        End If
                '        MsgBox(exMFiles.Message + Environment.NewLine + Environment.NewLine + "Please notify your MFiles Administrator to set up this Benchmarking Participant in MFiles.")
                '    End Try
                'End If

                ClearAllControls()
                'If (_useMFiles AndAlso Not _mfilesFailed) OrElse Not _useMFiles Then
                PopulateForm()
                cboCheckListView.Text = "All"  'to get checklist to populate after clearing out controls
                PopulateTabs()

                'End If
                Return True
            End If
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function

#Region "ClearForm"
    Private Function ClearAllControls() As Boolean
        T("ClearAllControls")
        'MsgBox("clear top of form, status, each control on each tab. EXCEPT cboStudy and cboRenum and cboCompany")
        '_mfilesBenchmarkingParticipantVPath = String.Empty
        '_mfilesBenchmarkingCompanyVPath = String.Empty
        '_mfilesRefiningGeneralVPath = String.Empty
        ClearForm()
        ClearContactsTab()
        ClearCorrespondenceTab()
        ClearChecklistTab()
        ClearNotesTab()
        ClearContinuingIssuesTab()
        ClearSecureSendTab()
        ClearTimeGradeTab()
        ClearDragDropTab()
        ClearQueryTab()
        ClearQATab()
        ClearPaths()
        btnRefresh.Text = String.Empty
        ClearSummaryFileButtonTags()
        Try
            For Each frm As Form In My.Application.OpenForms
                If frm.Name = "SecureSend" Then
                    frm.Close()
                End If
            Next
        Catch
        End Try
        Return True
    End Function
    Private Sub ClearForm()
        T("ClearForm")
        'clears all except the tabs
        btnFuelLube.Visible = False
        WebBrowser1.Visible = False
        btnMain.Text = String.Empty
        cboConsultant.Items.Clear()
        cboConsultant.Text = String.Empty
        btnCompanyPW.Text = String.Empty
        btnReturnPW.Text = String.Empty
        lblValidationStatus.Text = String.Empty
        lblLastFileSave.Text = String.Empty
        lblLastUpload.Text = String.Empty
        lblLastCalcDate.Text = String.Empty
        btnFLValidation.Visible = False
        btnCreateMissingPNFiles.Visible = False
        btnSendPrelimPricing.Visible = False
        lblStatus.Text = String.Empty
    End Sub
    Private Sub ClearPaths()
        T("ClearPaths")
        StudyRegion = String.Empty
        Coloc = String.Empty
        Company = String.Empty
        mRefNum = String.Empty
        RefineryPath = String.Empty
        GeneralPath = String.Empty
        ProjectPath = String.Empty
        CorrPath = String.Empty
        DataFolderPath = String.Empty
        CompanyPath = String.Empty
        CompCorrPath = String.Empty
        TempPath = String.Empty
        IDRPath = String.Empty
        DrawingPath = String.Empty
        ClientAttachmentsPath = String.Empty
        ClientToolsPath = String.Empty
        TemplatePath = String.Empty
        PolishPath = String.Empty
    End Sub
    Private Sub ClearContactsTab()
        T("ClearForm")
        lblPricingContact.Text = String.Empty
        lblPricingContactEmail.Text = String.Empty
        lblPricingPhone.Text = String.Empty

        lblName.Text = String.Empty
        lblEmail.Text = String.Empty
        lblPhone.Text = String.Empty
        lblAltName.Text = String.Empty
        lblAltEmail.Text = String.Empty
        lblAltPhone.Text = String.Empty
        lblInterimName.Text = String.Empty
        lblInterimEmail.Text = String.Empty
        lblInterimPhone.Text = String.Empty
        lblRefCoName.Text = String.Empty
        lblRefCoEmail.Text = String.Empty
        lblRefCoPhone.Text = String.Empty
        lblRefAltName.Text = String.Empty
        lblRefAltEmail.Text = String.Empty
        lblRefAltPhone.Text = String.Empty
        lblRefMgrName.Text = String.Empty
        lblRefMgrEmail.Text = String.Empty
        'lblRefMgrPhone.Text = String.Empty
        lblOpsMgrName.Text = String.Empty
        lblOpsMgrEmail.Text = String.Empty
        'lblOpsMgrPhone.Text = String.Empty
        lblMaintMgrName.Text = String.Empty
        lblMaintMgrEmail.Text = String.Empty
        'lblMaintMgrPhone.Text = String.Empty
        lblTechMgrName.Text = String.Empty
        lblTechMgrEmail.Text = String.Empty
        'lblTechMgrPhone.Text = String.Empty
        lblPCCName.Text = String.Empty
        lblPCCEmail.Text = String.Empty
        'lblPCCPhone.Text = String.Empty



        'ClearLabelsPrefixedWith("lbl")
        'ClearLabelsPrefixedWith("lblAlt")
        'ClearLabelsPrefixedWith("lblInterim")
        'ClearLabelsPrefixedWith("lblRefCo")
        'ClearLabelsPrefixedWith("lblRefAlt")
        'ClearLabelsPrefixedWith("lblRefMgr")
        'ClearLabelsPrefixedWith("lblOpsMgr")
        'ClearLabelsPrefixedWith("lblMaintMgr")
        'ClearLabelsPrefixedWith("lblTechMgr")
        'ClearLabelsPrefixedWith("lblPCC")
    End Sub
    Private Sub ClearCorrespondenceTab()
        T("ClearCorrespondenceTab")
        lstVFNumbers.Items.Clear()
        listViewCorrespondenceVF.Items.Clear()
        listViewCorrespondenceVR.Items.Clear()
        listViewCorrespondenceVReturn.Items.Clear()
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()
    End Sub
    Private Sub ClearChecklistTab()
        T("ClearChecklistTab")
        tvIssues.Nodes.Clear()
        txtIssueID.Text = String.Empty
        txtIssueName.Text = String.Empty
        txtDescription.Text = String.Empty
    End Sub
    Private Sub ClearNotesTab()
        T("ClearNotesTab")
        txtNotes.Text = String.Empty
    End Sub
    Private Sub ClearContinuingIssuesTab()
        T("ClearContinuingIssuesTab")
        dgContinuingIssues.Rows.Clear()
    End Sub
    Private Sub ClearSecureSendTab()
        T("ClearSecureSendTab")
        tvwCompCorr.Nodes.Clear()
        tvwCompCorr2.Nodes.Clear()
        tvwDrawings.Nodes.Clear()
        tvwDrawings2.Nodes.Clear()
        tvwCorrespondence.Nodes.Clear()
        tvwCorrespondence2.Nodes.Clear()
        tvwClientAttachments.Nodes.Clear()
        tvwClientAttachments2.Nodes.Clear()
        tvwClientTools.Nodes.Clear()
        tvwClientTools2.Nodes.Clear()
    End Sub
    Private Sub ClearTimeGradeTab()
        T("ClearTimeGradeTab")
        Try
            dgGrade.Rows.Clear()
        Catch
        End Try
        Try
            dgSummary.Rows.Clear()
        Catch
        End Try
        Try
            dgHistory.Rows.Clear()
        Catch
        End Try
    End Sub
    'Private Sub ClearDragDropTab()
    '    T("ClearDragDropTab")
    '    txtMessageFilename.Text = String.Empty
    '    optCompanyCorr.Checked = False
    '    dgFiles.Rows.Clear()
    'End Sub
    Private Sub ClearQueryTab()
        T("ClearQueryTab")
        txtClippySearch.Text = String.Empty
        dgClippyResults.Rows.Clear()
    End Sub
    Private Sub ClearQATab()
        T("ClearQATab")
        QASearch.Text = String.Empty
        tv.Nodes.Clear()
    End Sub
#End Region


    Private Function PopulateForm() As Boolean
        T("PopulateForm")
        Try
            If cboRefNum.SelectedItem.ToString().Trim().Length < 1 Then Return False
            'MsgBox("top of form, status, not cbostudy7/refnum/company")
            'Try
            '    Dim configValues As String = ConfigurationManager.AppSettings("TfsTaskAdminUsers").ToString()
            '    Dim adminUsers() As String = configValues.Split("|")
            '    For count As Integer = 0 To adminUsers.Length - 1
            '        If adminUsers(count).ToUpper.Trim() = UserName.ToUpper().Trim() Then
            '            btnBug.Visible = True
            '        End If
            '    Next
            'Catch
            'End Try
            Try
                _pathToPriorDatachecksFile = System.Configuration.ConfigurationManager.AppSettings("RefiningPathToPriorDatachecksFile").ToString()
                _pathToCurrentDatachecksFile = System.Configuration.ConfigurationManager.AppSettings("RefiningPathToCurrentDatachecksFile").ToString()
            Catch exDataCheckConfig As System.Exception
                db.WriteLog("MainConsole.Initialize(): Can't find app setting for either RefiningPathToPriorDatachecksFile  or  RefiningPathToCurrentDatachecksFile", exDataCheckConfig)
            End Try
            Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim version As System.Version = assembly.GetName().Version
            txtVersion.Text = String.Format("Version {0}", version)
            Try
                StudyDrive = ConfigurationManager.AppSettings("RefiningStudyFolderPath").ToString()  '\\dallas2\Data\Data\Study\Refining
            Catch
                StudyDrive = "\\dallas2\Data\Data\STUDY\" + _studyType + "\"
            End Try
            If Not Directory.Exists(StudyDrive) Then
                Throw New Exception("Folder: " & StudyDrive & " is Not exist")
            End If

            'this seems to be not as good as the GetConsultants() method . . . 
            '"Curr" dropdown
            '         ds = db.ExecuteStoredProc("Console." & "GetAllConsultants")
            '            If ds.Tables.Count > 0 Then
            '                For Each Row In ds.Tables(0).Rows
            '                    cboConsultant.Items.Add(Row("Consultant").ToString.ToUpper.Trim)
            '                Next
            '            End If


            'might need this later:
            'If Not CheckRole("QA", UserName) Then
            '    RemoveTab("tabQA")
            'End If
            'FL Review button
            If CheckRole("FL", UserName) Then btnFLValidation.Visible = True
            btnMain.Text = GetMainConsultant(GetRefNumCboText()) '"Main" butotn
            SetPaths()

            'lblStatus is the status label at bottom of form
            lblStatus.ForeColor = Color.DarkGreen
            lblStatus.Text = My.Settings.News
            'CboStudyChangedRoutine()
            CheckForCurrentStudyYear()
            GetConsultants()

            ' company and return pws
            btnCompanyPW.Text = GetCompanyPassword(GetRefNumCboText())
            btnReturnPW.Text = GetReturnPassword(Trim(UCase(GetRefNumCboText())), "fuelslubes")

            BuildSummarySection() ' Populate Top Right  -review status filesaved,lastupload,lastcalc,  red blue flags and items

            'hide/show, Missing PNs, Prelim Pricing
            If CheckRole("MissingPNs", UserName) Then
                btnCreateMissingPNFiles.Visible = True
            Else
                btnCreateMissingPNFiles.Visible = False
            End If
            CheckPreliminaryPricing()
            CheckFuelsLubes()
            'ConsoleTabs.SelectedIndex = 1
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function

    Private Function PopulateTabs() As Boolean
        T("PopulateTabs")
        If Not PopulateDragDropTab() Then Return False
        If Not PopulateTimeGradeTab() Then Return False
        If Not PopulateSecureSendTab() Then Return False
        If Not PopulateContinuingIssuesTab() Then Return False
        If Not PopulateNotesTab() Then Return False
        If Not PopulateChecklistTab() Then Return False
        If Not PopulateCorrespondenceTab() Then Return False
        If Not PopulateContactsTab() Then Return False

        ' ConsoleTabs.SelectTab(tabContacts)
        Return True
    End Function
    Private Function PopulateContactsTab() As Boolean
        T("PopulateContactsTab")
        Try
            BuildContactTab()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateCorrespondenceTab() As Boolean
        T("PopulateCorrespondenceTab")
        Try
            BuildCorrespondenceTab()
            Return True
        Catch ex As Exception
            MsgBox("Populate Correspondence Tab Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateChecklistTab() As Boolean
        T("PopulateChecklistTab")
        Try
            BuildCheckListTab("A")
            ' Y for completed, N for not completed, A for all
            cboCheckListView.SelectedIndex = 0
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateNotesTab() As Boolean
        T("PopulateNotesTab")
        Try
            BuildNotesTab()
            'If Not _useMFiles Then
            Dim pnDocPath As String = CorrPath & "PN_" & cboCompany.Text & ".doc"
            If File.Exists(pnDocPath) OrElse File.Exists(pnDocPath + "x") Then
                btnCreatePN.Text = "Open PN File"
                txtNotes.ReadOnly = True
                btnCreatePN.Text = "Open PN File"
            Else
                txtNotes.ReadOnly = False
                btnCreatePN.Text = "Create Final PN File"
            End If
            'Else
            '    Try
            '        If Convert.ToBoolean(_docsAccess.DocExistsInMFiles("PN_", GetRefNumCboText(), GetRefNumCboText(), True)) = True Then
            '            txtNotes.ReadOnly = True
            '            btnUnlockPN.Visible = False
            '            btnCreatePN.Text = "Open PN File"
            '        Else
            '            txtNotes.ReadOnly = False
            '            If Not _useMFiles Then
            '                btnUnlockPN.Visible = True
            '            End If
            '            btnCreatePN.Text = "Create Final PN File"
            '        End If


            '    Catch ex As Exception
            '        MsgBox(ex.Message)
            '    End Try
            'End If
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateContinuingIssuesTab() As Boolean
        T("PopulateContinuingIssuesTab")
        Try
            BuildContinuingIssuesTab()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateSecureSendTab() As Boolean
        T("PopulateSecureSendTab")
        Try
            BuildSecureSendTab()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateTimeGradeTab() As Boolean
        T("PopulateTimeGradeTab")
        Try
            ShowTimeGrade()
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function PopulateDragDropTab() As Boolean
        T("PopulateDragDropTab")
        Try
            'this really needs to be empty until someone drags and drops.
            'So there is nothing to do to prep this tab
            Return True
        Catch ex As Exception
            MsgBox("Console Error: " + ex.Message)
            Return False
        End Try
    End Function
    Private Function CreateConsoleFolder(path As String) As Boolean
        T("CreateConsoleFolder")
        Try
            If Not Directory.Exists(path) Then
                Directory.CreateDirectory(path)
            End If
            Return True
        Catch exSecureSendPath As Exception
            MsgBox("Console tried to create a folder at " + path + ", but was unable to. Please create this folder manually and then try running Console again.")
            Return False
        End Try
    End Function

    'old code below this line
    '==========================================================================


    Private Sub ChangeColocRefnum(results As YearRefnumPlantsInfo)
        T("ChangeColocRefnum")
        ChangeCboSelectedItem(cboStudy, results.SelectedStudyYear)
        ChangeCboSelectedItem(cboCompany, results.SelectedPlant)
        ChangeCboSelectedItem(cboRefNum, results.SelectedRefNum)
        RefNum = results.SelectedRefNum
    End Sub

    Private Sub ChangeCboSelectedItem(cbo As ComboBox, selectedItemString As String)
        For i As Integer = 0 To cbo.Items.Count - 1
            If cbo.Items(i).ToString() = selectedItemString Then
                cbo.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub



    Private Sub UpdateLastStudyRefnum(oldStudyYear As String, newRefNum As String)
        T("UpdateLastStudyRefnum")
        Try
            For count As Integer = 0 To _lastStudyAndRefnum.Length - 1
                Dim parts As String() = _lastStudyAndRefnum(count).Split(_delim)
                If parts(0).Trim() = oldStudyYear.Trim() Then
                    _lastStudyAndRefnum(count) = oldStudyYear.Trim() + _delim + newRefNum.Trim()
                    Exit Sub
                End If
            Next
        Catch
        End Try
    End Sub



#Region "BUILD TABS"

    Private Sub MFilesFailedRoutine()
        ClearMFilesTabs()
        HideMFilesTabs()

        SetTabDirty(True)

        TabDirty(1) = False
        TabDirty(5) = False
        TabDirty(7) = False

        BuildContactTab()
        BuildCheckListTab("")
        BuildNotesTab()
        BuildContinuingIssuesTab()
        'unsure: ShowTimeGrade()
        'not much guidance here on whether to show Query tab or not
        'not much guidance here on whether to show Q&A tab or not
    End Sub

    Private Sub ClearMFilesTabs()
        'clear and hide : corresp, securesend, dragdrop. Reload all other tabs
        lstVFNumbers.Items.Clear()
        listViewCorrespondenceVF.Items.Clear()
        listViewCorrespondenceVR.Items.Clear()
        listViewCorrespondenceVReturn.Items.Clear()
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()

        tvwCorrespondence2.Nodes.Clear()
        tvwCompCorr2.Nodes.Clear()
        tvwDrawings2.Nodes.Clear()
        tvwClientAttachments2.Nodes.Clear()
        dgFiles.Rows.Clear()
    End Sub
    Private Sub HideMFilesTabs()
        'these don't work:
        ' tabSS.Hide()        'tabCorr.Hide()        'tabDD.Hide()
        'these get tabs out of order:
        'tabSS.Parent = Nothing        'tabCorr.Parent = Nothing '"ConsoleTabs"        'tabDD.Parent = Nothing
        TryCast(tabSS, Control).Enabled = False
        TryCast(tabCorr, Control).Enabled = False
        TryCast(tabDD, Control).Enabled = False
    End Sub
    Private Sub UnhideMFilesTabs()
        TryCast(tabSS, Control).Enabled = True
        TryCast(tabCorr, Control).Enabled = True
        TryCast(tabDD, Control).Enabled = True
        lstVFFiles.Visible = False
        lstVRFiles.Visible = False
        lstReturnFiles.Visible = False
    End Sub


    Private Sub BuildSummarySection()
        T("BuildSummaryTab")
        Try
            Dim results = _docsAccess.FileSearchDirCommand(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xls*")
            If results.Length < 1 Then
                lblValidationStatus.ForeColor = Color.DarkRed
                lblValidationStatus.Text = "Not Received"
            Else
                If _docsAccess.FileSearchDirCommand(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xls").Length > 0 Then
                    lblLastFileSave.Text = _docsAccess.FileDateTime(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xls")
                End If
                If _docsAccess.FileSearchDirCommand(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm").Length > 0 Then
                    lblLastFileSave.Text = _docsAccess.FileDateTime(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm")
                End If
                lblValidationStatus.ForeColor = Color.DarkGreen
                lblValidationStatus.Text = "In Progress"
            End If
            'use .txt instead of .xls* for "In Holding" by Sung 5/9/2019
            results = _docsAccess.FileSearchDirCommand(IDRPath & "Holding\" & ParseRefNum(GetRefNumCboText(), 0) & ".txt")
            If results.Length > 0 Then
                lblValidationStatus.Text = "In Holding"
                If _docsAccess.FileSearchDirCommand(IDRPath & "Holding\" & ParseRefNum(GetRefNumCboText(), 0) & ".xls").Length > 0 Then
                    lblLastFileSave.Text = _docsAccess.FileDateTime(IDRPath & "Holding\" & ParseRefNum(GetRefNumCboText(), 0) & ".txt")
                End If
                If _docsAccess.FileSearchDirCommand(IDRPath & "Holding\" & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm").Length > 0 Then
                    lblLastFileSave.Text = _docsAccess.FileDateTime(IDRPath & "Holding\" & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm")
                End If

                lblValidationStatus.ForeColor = Color.DarkGoldenrod
                lblValidationStatus.Text = "In Holding"

            End If

            If VChecked(GetRefNumCboText(), "V2") Then
                If File.Exists(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xls") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xls")
                End If
                If File.Exists(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm")
                End If

                lblValidationStatus.ForeColor = Color.Black
                lblValidationStatus.Text = "Complete"
            End If

            If VChecked(GetRefNumCboText(), "V1") And Not VChecked(GetRefNumCboText(), "V2") Then
                If File.Exists(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xls") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xls")
                End If
                If File.Exists(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(GetRefNumCboText(), 0) & ".xlsm")
                End If

                lblValidationStatus.ForeColor = Color.DarkGoldenrod
                lblValidationStatus.Text = "Polishing"

            End If

            Dim mRefNum As String = RefNum

            Dim dt As String
            Dim row As DataRow
            Dim params = New List(Of String)

            params.Add("RefNum/" + GetRefNumCboText())
            params.Add("MessageType/53")
            ds = db.ExecuteStoredProc("Console." & "GetMessageLog", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    dt = row(0).ToString
                    lblLastUpload.Text = dt.ToString
                End If
            End If

            ' Get the last time it was Calced from MessageLog
            'Add Parameters and call GetConsultants
            params = New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            params.Add("MessageType/44")
            ds = db.ExecuteStoredProc("Console." & "GetMessageLog", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    dt = row(0).ToString
                    lblLastCalcDate.Text = dt.ToString
                End If

            End If
            ' Get the last time it was Calced from MessageLog
            'Add Parameters and call GetConsultants
            params = New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())

            ' Load the Red and Blue Flags count labels
            ds = db.ExecuteStoredProc("Console." & "GetFlags", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)

                    If row("DCRedFlags").ToString = "" Then
                        lbl_RedFlags.Text = "0 Red Flags"
                    Else
                        lbl_RedFlags.Text = row("DCRedFlags").ToString & " Red Flag"
                        If Convert.ToInt32(row("DCRedFlags")) <> 1 Then
                            lbl_RedFlags.Text += "s"
                        End If
                    End If

                    If row("DCBlueFlags").ToString = "" Then
                        lbl_BlueFlags.Text = "0 Blue Flags"
                    Else
                        lbl_BlueFlags.Text = row("DCBlueFlags").ToString & " Blue Flag"
                        If Convert.ToInt32(row("DCBlueFlags")) <> 1 Then
                            lbl_BlueFlags.Text += "s"
                        End If
                    End If
                Else
                    lbl_BlueFlags.Text = ""
                    lbl_RedFlags.Text = ""
                End If
            End If

            RefNum = mRefNum

            ' Load the Uncompleted Checklist Item count label
            params = New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            params.Add("Mode/N")
            ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    If row.Item(0) = 1 Or row.Item(0) = 0 Then
                        lbl_ItemRemaining.Text = row(0).ToString & " Item"
                    ElseIf row.Item(0) > 1 Then
                        lbl_ItemRemaining.Text = row(0).ToString & " Items"
                    End If
                Else
                    lbl_ItemRemaining.Text = ""
                End If
            Else
                lbl_ItemRemaining.Text = ""
            End If

        Catch ex As System.Exception
            'MessageBox.Show(ex.Message)
            db.WriteLog("BuildSummaryTab", ex)
        End Try

        'TODO:  This should be ROLE BASED when we get ACTIVE DIRECTORY

        ' Only the polishers get to push the button to lock the notes and create the Word file.
        ' Not sure if CRT and JDW are supposed to be on this list, but not a big deal.
        ' Added LS 9/9/09 per DEJ so she can go thru and extract the ones that have not been done.
        mUserName = mUserName.ToUpper
        'If lblLastCalcDate.Text.Length > 1 Then btnPolishRpt.Enabled = True
    End Sub

    Private Sub BuildNotesTab()
        T("BuildNotesTab")
        Dim thisRefNum As String = GetRefNumCboText()
        If ParseRefNum(thisRefNum, 2) = "LUB" Then thisRefNum = thisRefNum.Replace("LUB", "LIV")
        Dim params = New List(Of String)
        txtNotes.Text = ""
        params = New List(Of String)
        params.Add("RefNum/" + thisRefNum)

        'btnCreatePN.Text = "Create Final PN File"
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If ds.Tables(0).Rows.Count = 1 Then
                        txtNotes.Text = row("Note").ToString
                    Else
                        txtNotes.Text = txtNotes.Text & vbCrLf & row("Note").ToString
                    End If

                Next
            Else
                txtNotes.Text = ""
            End If

        End If

    End Sub

    Private Sub PresenterNotesReport()

        Dim ps = New List(Of String)
        Dim MissingPNs = New List(Of String)
        Dim InHolding = New List(Of String)
        Dim Polishing = New List(Of String)
        Dim NotReceived = New List(Of String)
        Dim Validating = New List(Of String)
        Dim InProgress = New List(Of String)
        Dim Successful = New List(Of String)
        Dim _path As String = ""
        Dim NewRefNum As String = ""
        Dim NewColoc As String = ""

        Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word files?   This will lock the fields and will have to be unlocked manually.", "Wait", MessageBoxButtons.YesNo)
        If dr = System.Windows.Forms.DialogResult.Yes Then
            Dim NewStudy As String = cboStudy.Text.Substring(0, 3)

            ps.Add("@Study/" & GetStudyRegionFromCboStudy())
            ps.Add("@StudyYear/" & Get4DigitYearFromCboStudy().ToString())
            Dim reader As SqlDataReader = db.ExecuteReader("Console.GetMissingPNs", ps)
            While reader.Read()

                NewRefNum = reader.GetString(0).Trim
                NewColoc = reader.GetString(1).Trim

                Dim _status As String = GetStatus(NewRefNum)

                _path = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\" & StudyRegion & "\Correspondence\" & NewRefNum & "\"

                If File.Exists(_path & "PN_" & NewColoc & ".doc") = False And _status = "Complete" Then
                    MissingPNs.Add(NewRefNum & "," & NewColoc)
                Else
                    Select Case _status
                        Case "Not Received"
                            NotReceived.Add(NewRefNum & " - " & NewColoc)
                        Case "Polishing"
                            Polishing.Add(NewRefNum & " - " & NewColoc)
                        Case "In Holding"
                            InHolding.Add(NewRefNum & " - " & NewColoc)
                        Case "Validating"
                            Validating.Add(NewRefNum & " - " & NewColoc)
                        Case "In Progress"
                            InProgress.Add(NewRefNum & " - " & NewColoc)
                    End Select
                End If
            End While

            Dim pbForm As PNProgressBar = New PNProgressBar()
            pbForm.Show()
            pbForm.InitializePB(1, MissingPNs.Count, 1)

            For Each RN As String In MissingPNs
                Dim NRN As String = RN.Split(",")(0).ToString()
                Dim NCL As String = RN.Split(",")(1).ToString()

                Try
                    Dim myText As String = "Presenter Notes for" & vbCrLf & NCL & vbCrLf & vbCrLf
                    Dim params = New List(Of String)
                    Dim row As DataRow

                    lblStatus.Text = "Creating Presenter's Word file..."
                    params.Add("RefNum/" + NRN)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            row = ds.Tables(0).Rows(0)
                            myText += row("ValidationNotes").ToString
                        Else
                            myText += ""
                        End If
                    End If
                    Dim strw As New StreamWriter(_profileConsoleTempPath & "PN_" & NCL & ".txt")

                    strw.WriteLine(myText)
                    strw.Close()
                    strw.Dispose()
                    Dim doc As String = Utilities.ConvertToDoc(_profileConsoleTempPath, _profileConsoleTempPath & "PN_" & NCL & ".txt")
                    Dim NewPath = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\" & GetStudyRegionFromCboStudy() & "\Correspondence\" & NRN & "\"
                    File.Move(_profileConsoleTempPath & "PN_" & NCL & ".doc", NewPath & "PN_" & NCL & ".doc")
                    File.Delete(_profileConsoleTempPath & "PN_" & NCL & ".txt")

                    params.Add("RefNum/" + NRN)
                    params.Add("Notes/" + txtNotes.Text)
                    Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)

                    Dim _status As String = UpdatePN(NRN)
                    If _status = "IDR Notes updated successfully" Then
                        pbForm.PerformStepPB("PN_" & NCL & " - " & NRN)
                        Successful.Add(NRN & " - " & NCL)
                    Else
                        lblStatus.Text = _status
                    End If

                Catch ex As System.Exception
                    lblStatus.Text = "Error: " & ex.Message
                End Try

            Next

            pbForm.Close()
            pbForm.Dispose()
            While Not pbForm.IsDisposed
                Thread.Sleep(100)
            End While

            Dim _stream As New StreamWriter(_profileConsoleTempPath & " Presentation Document Log.txt", False)
            _stream.WriteLine(Study & "Presentation Document Log")
            _stream.WriteLine("---------------------------------------------")
            _stream.WriteLine()
            _stream.WriteLine("Presentation Documents Created: " & Successful.Count)
            _stream.WriteLine()
            _stream.WriteLine("Refnums In Holding: " & InHolding.Count)
            For i = 0 To InHolding.Count - 1
                _stream.WriteLine(InHolding(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums Not Received: " & NotReceived.Count)
            For i = 0 To NotReceived.Count - 1
                _stream.WriteLine(NotReceived(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums In Progress: " & InProgress.Count)
            For i = 0 To InProgress.Count - 1
                _stream.WriteLine(InProgress(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums Polishing: " & Polishing.Count)
            For i = 0 To Polishing.Count - 1
                _stream.WriteLine(Polishing(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums Validating: " & Validating.Count)
            For i = 0 To Validating.Count - 1
                _stream.WriteLine(Validating(i))
            Next

            _stream.Close()

            Process.Start(_profileConsoleTempPath & "Presentation Document Log.txt")
        End If

    End Sub

    Private Sub BuildCheckListTab(strYesNoAll As String) ' Y for completed, N for not completed, A for all
        T("BuildCheckListTab")
        'CODE AS OF 2017-04-20:
        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtDescription.Text = ""

        Dim yesNoAllLetter As String = String.Empty

        Select Case strYesNoAll
            Case "Complete"
                yesNoAllLetter = "Y"
            Case "Incomplete"
                yesNoAllLetter = "N"
            Case Else
                yesNoAllLetter = "A"
        End Select
        UpdateCheckList(yesNoAllLetter)

        'New code:
        RebuildChecklist(db, GetRefNumCboText(), yesNoAllLetter)


    End Sub

    Private Sub BuildCorrespondenceTab()
        T("BuildCorrespondenceTab")
        'If _useMFiles Then
        '    If _mfilesFailed Then Exit Sub
        '    'RefreshMFilesCollections(0)
        'End If
        'MsgBox("Commenting out GetConsultants() here; was commented out in NGPP so P duplicated.")
        'GetConsultants()

        SetSummaryFileButtons() ' this hanldes the buttons on the right
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()
        lstVFNumbers.Items.Clear()

        listViewCorrespondenceVF.Clear()
        listViewCorrespondenceVR.Clear()
        listViewCorrespondenceVReturn.Clear()

        'If Not _useMFiles Then
        CorrPath = BuildCorrPathFromRefnum(GetRefNumCboText(), Get4DigitYearFromCboStudy(), GetStudyRegionFromCboStudy())
        If IsNothing(CorrPath) OrElse CorrPath.Length < 1 Then
            CorrPath = BuildCorrPathFromCompany(cboCompany.SelectedItem.ToString(), Get4DigitYearFromCboStudy(), GetStudyRegionFromCboStudy())
        End If
        T("CorrPath is nothing and not using M-Files")
        Try
            'If (Not _useMFiles) And IsNothing(CorrPath) Then Exit Sub
            If IsNothing(CorrPath) Then Exit Sub
        Catch ex As Exception
            MsgBox("Err at CorrPath is nothing and not using M-Files, exiting. Error: " & ex.Message)
            Exit Sub
        End Try
        'End If
        T("BuildCorrespondenceTab - Going to check GetRefNumCboText().Length")
        If GetRefNumCboText().Length > 0 Then
            Dim vNums As New List(Of String)
            'If Not _useMFiles Then
            If Not IsNothing(CorrPath) AndAlso CorrPath.Length > 0 Then
                T("BuildCorrespondenceTab - GetRefNumCboText len > 0")
                vNums = _docsAccess.GetDataFor_lstVFNumbers("*VF*", CorrPath, vNums, GetRefNumCboText(), GetRefNumCboText())
                T("BuildCorrespondenceTab - vNums VF")
                ' Now look for any orphan VR files
                vNums = _docsAccess.GetDataFor_lstVFNumbers("*VR*", CorrPath, vNums, GetRefNumCboText(), GetRefNumCboText())
                T("BuildCorrespondenceTab - vNums VR")
                vNums = _docsAccess.GetDataFor_lstVFNumbers("*VA*", CorrPath, vNums, GetRefNumCboText(), GetRefNumCboText())
                T("BuildCorrespondenceTab - vNums VA")
                vNums = _docsAccess.GetDataFor_lstVFNumbers("*F_Return*", CorrPath, vNums, GetRefNumCboText(), GetRefNumCboText(), True)
                T("BuildCorrespondenceTab - vNums F_Return")
                vNums = _docsAccess.GetDataFor_lstVFNumbers("*L_Return*", CorrPath, vNums, GetRefNumCboText(), GetRefNumCboText(), True)
                T("BuildCorrespondenceTab - vNums L_Return")
            End If
            'Else
            '    T("BuildCorrespondenceTab - GetRefNumCboText len > 0")
            '    vNums = GetDataFor_lstVFNumbersMFiles("VF*", vNums)
            '    T("BuildCorrespondenceTab - vNums VF")
            '    ' Now look for any orphan VR files
            '    vNums = GetDataFor_lstVFNumbersMFiles("VR*", vNums)
            '    T("BuildCorrespondenceTab - vNums VR")
            '    vNums = GetDataFor_lstVFNumbersMFiles("VA*", vNums)
            '    T("BuildCorrespondenceTab - vNums VA")
            '    vNums = GetDataFor_lstVFNumbersMFiles("F_Return*", vNums, True)
            '    T("BuildCorrespondenceTab - vNums F_Return")
            '    vNums = GetDataFor_lstVFNumbersMFiles("L_Return*", vNums, True)
            '    T("BuildCorrespondenceTab - vNums L_Return")
            'End If
            vNums.Sort()

            For Each item As Int16 In vNums
                lstVFNumbers.Items.Add(item)
            Next
        End If
        T("BuildCorrespondenceTab - going to check lstVFNumbers.SelectedIndex")
        lstVFNumbers.SelectedIndex = lstVFNumbers.Items.Count - 1
        If lstVFNumbers.SelectedIndex > -1 Then
            SetCorr() ' populates the 3 listviews in the middle
            'Else
            '    If _useMFiles Then
            '        listViewCorrespondenceVF.Visible = True
            '        listViewCorrespondenceVR.Visible = True
            '        listViewCorrespondenceVReturn.Visible = True
            '    End If
        End If
        T("Exiting BuildCorrespondenceTab")
    End Sub

    'Private Sub BuildCorrespondenceTab2()
    '    T("BuildCorrespondenceTab2")
    '    If _useMFiles And _mfilesFailed Then Exit Sub

    '    'MsgBox("Commenting out GetConsultants() here; was commented out in NGPP so P duplicated.")
    '    'GetConsultants()

    '    SetSummaryFileButtons() ' this hanldes the buttons on the right
    '    lstVFFiles.Items.Clear()
    '    lstVRFiles.Items.Clear()
    '    lstReturnFiles.Items.Clear()
    '    lstVFNumbers.Items.Clear()

    '    listViewCorrespondenceVF.Clear()
    '    listViewCorrespondenceVR.Clear()
    '    listViewCorrespondenceVReturn.Clear()

    '    If Not _useMFiles Then
    '        CorrPath = BuildCorrPath(GetRefNumCboText(), Get4DigitYearFromCboStudy(), GetStudyRegionFromCboStudy())
    '        If (Not _useMFiles) And CorrPath = Nothing Then Exit Sub
    '    End If

    '    If GetRefNumCboText().Length > 0 Then
    '        Dim allBenchParticFiles As ConsoleFilesInfo = _docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant(GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
    '        Dim found As New List(Of String)
    '        Dim num As String = String.Empty
    '        For Each fil As ConsoleFile In allBenchParticFiles.Files
    '            LoadVFNums(fil, found, "VF", True, False)
    '            LoadVFNums(fil, found, "VR", True, False)
    '            LoadVFNums(fil, found, "VA", True, False)
    '            LoadVFNums(fil, found, "F_Return", False, True)
    '            LoadVFNums(fil, found, "L_Return", False, True)
    '        Next
    '        Dim arr As String() = found.ToArray()
    '        Array.Sort(arr)
    '        For i As Integer = 0 To arr.Length - 1
    '            lstVFNumbers.Items.Add(arr(i))
    '        Next
    '    End If
    '    lstVFNumbers.SelectedIndex = lstVFNumbers.Items.Count - 1
    '    Dim consoleFilesInfo As ConsoleFilesInfo = Nothing
    '    If Directory.Exists("C:\Users\sfb.DC1") Then MsgBox("Would like to use allBenchParticFiles to replace where click left box!!! Also repopulate during refreshes")
    '    If Directory.Exists("C:\Users\sfb.DC1") Then MsgBox("Also in GetVFilesForCorrespondenceTab, use the allBenchParticFiles ")
    '    consoleFilesInfo = _docsAccess.GetVFilesForCorrespondenceTab(CorrPath, lstVFNumbers.Text, GetRefNumCboText())
    '    If lstVFNumbers.SelectedIndex > -1 Then
    '        SetCorr(consoleFilesInfo) ' populates the 3 listviews in the middle
    '    Else
    '        If _useMFiles Then
    '            listViewCorrespondenceVF.Visible = True
    '            listViewCorrespondenceVR.Visible = True
    '            listViewCorrespondenceVReturn.Visible = True
    '        End If
    '    End If

    'End Sub

    Private Function LoadVFNums(consoleFile As ConsoleFile, ByRef foundNums As List(Of String), filter As String,
                                startsWith As Boolean, contains As Boolean) As Boolean
        Try
            Dim num As String = String.Empty
            Try
                If startsWith Then
                    num = consoleFile.FileName.Substring(2, 1)
                ElseIf contains Then
                    num = consoleFile.FileName.Substring(consoleFile.FileName.IndexOf(filter) + filter.Length, 1)
                End If
            Catch ex As Exception
                num = String.Empty
            End Try
            If startsWith Then
                If consoleFile.FileName.StartsWith(filter) AndAlso IsNumeric(num) Then
                    Dim already As Boolean = False
                    For Each number As String In foundNums
                        If number = num Then
                            already = True
                            Exit For
                        End If
                    Next
                    If Not already Then foundNums.Add(num)
                End If
            ElseIf contains Then
                If consoleFile.FileName.Contains(filter) AndAlso IsNumeric(num) Then
                    Dim already As Boolean = False
                    For Each number As String In foundNums
                        If number = num Then
                            already = True
                            Exit For
                        End If
                    Next
                    If Not already Then foundNums.Add(num)
                End If
            End If
            Return True
        Catch ex As Exception
            MsgBox("Error in LoadVFNums: " & ex.Message)
            Return False
        End Try


    End Function

    Private Function GetRefNumCboText() As String
        If IsNothing(cboRefNum.SelectedItem) Then
            Return String.Empty
        End If
        Return cboRefNum.SelectedItem.ToString()
    End Function
    'Private Function Get20PlusRefNumCboText() As String
    '    Return Utilities.GetLongRefnum(GetRefNumCboText)
    '    Return RefNum ' Refining doesn't use the "20" + RefNum
    'End Function
    Sub SetCorr()
        T("SetCorr")
        Dim strDirResult() As String
        Dim choice As String = String.Empty
        If lstVFNumbers.Items.Count > 0 Then
            choice = lstVFNumbers.SelectedItem.ToString()
        End If
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()

        'If _useMFiles Then
        '    lstVFFiles.Visible = False
        '    lstVRFiles.Visible = False
        '    lstReturnFiles.Visible = False

        '    listViewCorrespondenceVF.Visible = True
        '    listViewCorrespondenceVR.Visible = True
        '    listViewCorrespondenceVReturn.Visible = True

        '    Dim items As New List(Of ConsoleFile)
        '    Try
        '        'need a VF file in correspondence folder for this to get populated


        '        'at some point, call _docsAccess.GetFilesForCorrespondenceTab() instead, so can 
        '        'add mfiles id to ListViewItem's .Tag

        '        'PopulateVFFilesMFiles("", "", lstVFNumbers.SelectedItem.ToString(), items)
        '        PopulateVFFilesMFiles("VF", lstVFNumbers.SelectedItem.ToString(), items, False)
        '        PopulateVFFilesMFiles("VFFL", lstVFNumbers.SelectedItem.ToString(), items, False)
        '        PopulateVFFilesMFiles("VR", lstVFNumbers.SelectedItem.ToString(), items, False)
        '        PopulateVFFilesMFiles("VRFL", lstVFNumbers.SelectedItem.ToString(), items, False)
        '        PopulateVFFilesMFiles("VA", lstVFNumbers.SelectedItem.ToString(), items, False)
        '        PopulateVFFilesMFiles("VAFL", lstVFNumbers.SelectedItem.ToString(), items, False)
        '        PopulateVFFilesMFiles("Return", lstVFNumbers.SelectedItem.ToString(), items, True)

        '        'items = _docsAccess.lstVRFilesItems(CorrPath, lstVFNumbers.SelectedItem.ToString(), GetRefNumCboText())
        '    Catch exlstVRFilesItems As System.Exception

        '    End Try
        '    PrepCorrListview2(listViewCorrespondenceVF, "VF", items, lstVFNumbers.SelectedItem, False)
        '    PrepCorrListview2(listViewCorrespondenceVR, "VR", items, lstVFNumbers.SelectedItem, False)
        '    PrepCorrListview2(listViewCorrespondenceVR, "VA", items, lstVFNumbers.SelectedItem, True)
        '    PrepCorrListview2(listViewCorrespondenceVReturn, "Return", items, lstVFNumbers.SelectedItem, False)
        'Else

        lstVFFiles.Visible = True
        lstVRFiles.Visible = True
        lstReturnFiles.Visible = True

        listViewCorrespondenceVF.Visible = False
        listViewCorrespondenceVR.Visible = False
        listViewCorrespondenceVReturn.Visible = False

        strDirResult = Directory.GetFiles(CorrPath, "*VF" & choice & "*")

        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            lstVFFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
        Next

        strDirResult = Directory.GetFiles(CorrPath, "*VFFL" & choice & "*")

        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            lstVFFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VR" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VRFL" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VA" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VAFL" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
        Next

        strDirResult = Directory.GetFiles(CorrPath, "*Return" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            lstReturnFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
        Next
        'End If

    End Sub

    Sub SetCorr(consoleFilesInfo As ConsoleFilesInfo)
        T("SetCorr")
        Dim strDirResult() As String
        Dim choice As String = String.Empty
        If lstVFNumbers.Items.Count > 0 Then
            choice = lstVFNumbers.SelectedItem.ToString()
        End If
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()

        'If _useMFiles Then
        '    lstVFFiles.Visible = False
        '    lstVRFiles.Visible = False
        '    lstReturnFiles.Visible = False

        '    listViewCorrespondenceVF.Visible = True
        '    listViewCorrespondenceVR.Visible = True
        '    listViewCorrespondenceVReturn.Visible = True

        '    'prior to SteCorr, call _docsAccess.GetFilesForCorrespondenceTab() instead, so can 
        '    'add mfiles id to ListViewItem's .Tag

        '    If Not PrepCorrListview(listViewCorrespondenceVF, "VF", consoleFilesInfo, lstVFNumbers.SelectedItem, False) Then Exit Sub
        '    If Not PrepCorrListview(listViewCorrespondenceVR, "VR", consoleFilesInfo, lstVFNumbers.SelectedItem, False) Then Exit Sub
        '    If Not PrepCorrListview(listViewCorrespondenceVR, "VA", consoleFilesInfo, lstVFNumbers.SelectedItem, True) Then Exit Sub
        '    If Not PrepCorrListview(listViewCorrespondenceVReturn, "Return", consoleFilesInfo, lstVFNumbers.SelectedItem, False) Then Exit Sub
        'Else

        lstVFFiles.Visible = True
        lstVRFiles.Visible = True
        lstReturnFiles.Visible = True

        listViewCorrespondenceVF.Visible = False
        listViewCorrespondenceVR.Visible = False
        listViewCorrespondenceVReturn.Visible = False

        strDirResult = Directory.GetFiles(CorrPath, "VF" & choice & "*")

        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = IIf(f.Contains("VFFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                lstVFFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            End If
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VR" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = IIf(f.Contains("VRFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            End If
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VA" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = IIf(f.Contains("VAFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            End If
        Next

        strDirResult = Directory.GetFiles(CorrPath, "*Return" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = "" Then
                lstReturnFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            Else
                If (lstVFNumbers.SelectedItem = Mid(f, Len(f) - 4, 1) _
                            Or InStr(1, f, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                    lstReturnFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            End If
        Next
        'End If

    End Sub


    Private Sub BuildDirectoriesTab()
        T("BuildDirectoriesTab")
        cboDir.Items.Clear()
        'If Not _useMFiles Then
        cboDir.Items.Add("Refinery Correspondence")
        cboDir.Items.Add("Company Correspondence")
        cboDir.Items.Add("Drawings")
        'End If
        cboDir.Items.Add("Client Attachments")
        cboDir.Items.Add("Client Tools")
        If cboDir2.Items.Count < 1 Then
            cboDir2.Items.Add("Refinery Correspondence")
            cboDir2.Items.Add("Company Correspondence")
            'If Not _useMFiles Then
            cboDir2.Items.Add("Client Attachments")  'these are not in MFiles het
            'End If
            cboDir2.Items.Add("Drawings")
            cboDir2.Items.Add("Client Tools")
        End If

        Try
            chkAddFLMacros.Enabled = False
            If File.Exists(GetFLMacrosPath()) Then
                chkAddFLMacros.Enabled = True
            End If
        Catch
        End Try


        tvwCompCorr.Nodes.Clear()
        tvwCorrespondence.Nodes.Clear()
        tvwDrawings.Nodes.Clear()
        tvwClientAttachments.Nodes.Clear()
        tvwCorrespondence2.Nodes.Clear()
        tvwCompCorr2.Nodes.Clear()
        tvwDrawings2.Nodes.Clear()
        tvwClientAttachments2.Nodes.Clear()
        PopulateTreeView(tvwCompCorr, CompCorrPath)
        PopulateTreeView(tvwCorrespondence, CorrPath)
        PopulateTreeView(tvwCompCorr2, CompCorrPath)
        PopulateTreeView(tvwCorrespondence2, CorrPath)
        PopulateTreeView(tvwDrawings, DrawingPath)
        PopulateTreeView(tvwClientAttachments, ClientAttachmentsPath)
        PopulateTreeView(tvwDrawings2, DrawingPath)
        PopulateTreeView(tvwClientAttachments2, ClientAttachmentsPath)
        PopulateTreeView(tvwClientTools, ClientToolsPath)
        PopulateTreeView(tvwClientTools2, ClientToolsPath)
        Try
            cboDir.SelectedIndex = 0
            cboDir2.SelectedIndex = 0
        Catch
        End Try
        'If _useMFiles Then
        '    lblSecureSendDirectory2.Text = "M-Files:"
        'Else
        lblSecureSendDirectory2.Text = "Directory:"
        'End If
    End Sub

    Private Function SortConsoleFilesByDate(consoleFilesInfo As ConsoleFilesInfo) As ConsoleFilesInfo
        If Not IsNothing(consoleFilesInfo) AndAlso Not IsNothing(consoleFilesInfo.Files) AndAlso consoleFilesInfo.Files.Count > 0 Then
            Try
                Dim temp As New ConsoleFilesInfo(False, _vertical)
                'If Not IsNothing(consoleFilesInfo.BenchmarkingParticipantId) Then
                '   temp.BenchmarkingParticipantId = consoleFilesInfo.BenchmarkingParticipantId
                'End If
                'temp.RefNum = populated by BenchmarkingParticipant
                Dim dates As New List(Of DateTime)
                For Each fil As ConsoleFile In consoleFilesInfo.Files
                    dates.Add(fil.DocumentDate)
                Next
                Dim foundIds As String = String.Empty
                dates.Sort()
                For i As Integer = dates.Count - 1 To 0 Step -1
                    Dim toFind As DateTime = dates(i)
                    For j As Integer = 0 To consoleFilesInfo.Files.Count - 1
                        If consoleFilesInfo.Files(j).DocumentDate = toFind And Not foundIds.Contains(consoleFilesInfo.Files(j).Id.ToString()) Then
                            temp.Files.Add(consoleFilesInfo.Files(j))
                            foundIds += consoleFilesInfo.Files(j).Id.ToString() + "|"
                            Exit For
                        End If
                    Next
                Next
                Return temp
            Catch ex As Exception
                db.WriteLog("SortConsoleFilesByDate", ex)
                Return consoleFilesInfo
            End Try
        Else
            Return consoleFilesInfo 'dont sort
        End If
    End Function

    Private Sub PopulateTreeView(tvw As TreeView, path As String)
        T("PopulateTreeView")
        'Left treeviews-not using MFiles:
        If tvw.Name <> "tvwCorrespondence2" AndAlso tvw.Name <> "tvwCompCorr2" AndAlso tvw.Name <> "tvwDrawings2" AndAlso tvw.Name <> "tvwClientAttachments2" AndAlso tvw.Name <> "tvwClientTools2" Then
            If Directory.Exists(path) Then
                SetTree(tvw, path)
                'If _useMFiles And (tvw.Name.Contains("tvwCorrespondence") Or tvw.Name.Contains("tvwCompCorr")) Then
                '    tvw.Enabled = False
                'Else
                tvw.Enabled = True
                'End If
            End If
        Else ' right treeviews. These MIGHT use MFiles depending on year
            'If _useMFiles Then
            '    Dim consoleFilesInfo As ConsoleFilesInfo = Nothing
            '    'orig: but now only use for tvwCorrespondence2
            '    'NEED to do diff lookup for tvwCompCorr2
            '    If tvw.Name = "tvwCorrespondence2" AndAlso GetRefNumCboText().Length > 0 Then
            '        consoleFilesInfo = _benchmarkingParticipantRefineryFiles
            '        '_docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant( _
            '        '                         GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
            '        consoleFilesInfo = SortConsoleFilesByDate(consoleFilesInfo)
            '    ElseIf tvw.Name = "tvwCompCorr2" And cboCompany.Text.Length > 0 Then
            '        Try
            '            'EUR16Co-EXAMPLE
            '            'Dim benchmarkName As String = GetCompanyBenchmarkName()
            '            consoleFilesInfo = _benchmarkingParticipantCompanyFiles ' _docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant( _
            '            'benchmarkName, benchmarkName)
            '            consoleFilesInfo = SortConsoleFilesByDate(consoleFilesInfo)
            '        Catch ex As Exception
            '            'reset to no data so we don't try to populate list or err while trying
            '            consoleFilesInfo = Nothing
            '        End Try
            '        db.SQL = String.Empty
            '    ElseIf tvw.Name = "tvwDrawings2" AndAlso GetRefNumCboText().Length > 0 Then
            '        consoleFilesInfo = _refiningGeneralFiles
            '        'Dim studyIndex As Integer = GetRefNumCboText().IndexOf(cboStudy.Text().Substring(0, 3))
            '        'Dim refnumWithoutYear = GetRefNumCboText().Substring(0, studyIndex + 3)
            '        'Dim result As String = _docsAccess.GetGeneralRefineryDocs(GetRefNumCboText(),
            '        '        Utilities.GetLongRefnum(GetRefNumCboText()), refnumWithoutYear, consoleFilesInfo, 2)
            '        'If result.Length > 0 Then
            '        '    MsgBox(result)
            '        'End If
            '    End If
            '    'This doesn't change the image from a FOlder to anything else
            '    'If IsNothing(tvw.ImageList) Then
            '    '    Dim i As New System.Windows.Forms.ImageList
            '    '    Dim r As New Icon("C:\tfs\CPA\Sa.Console.Trunk\Development\Sa.Console.20150526\RefiningConsole\Images\Refinery.ico")
            '    '    i.Images.Add("MFiles", r)
            '    '    tvw.ImageList = i
            '    'End If  
            '    If Not IsNothing(consoleFilesInfo) AndAlso Not IsNothing(consoleFilesInfo.Files) Then
            '        tvw.Nodes.Clear()
            '        tvw.ImageList = ImageList1
            '        For Each fil As ConsoleFile In consoleFilesInfo.Files
            '            tvw.Nodes.Add(PopulateTreeViewNodeWithMfilesFiles(fil))
            '            tvw.Refresh()
            '        Next
            '    End If
            'Else 'not using MFiles
            If Directory.Exists(path) Then
                SetTree(tvw, path)
            End If
        End If
        'End If

    End Sub
    Private Function PopulateTreeViewNodeWithMfilesFiles(fil As ConsoleFile) As TreeNode
        Dim info As String = String.Empty
        Dim checkedOut As Boolean = False
        If fil.CheckedOutTo > 0 Then
            info = "(" + fil.CheckedOutToUserName + ") "
            checkedOut = True
        End If
        info += fil.FileName + "." + fil.FileExtension
        Dim tn As New TreeNode(info)
        If checkedOut Then
            tn.BackColor = Color.Red
        End If
        tn.Tag = fil.Id.ToString()
        Try
            tn.ImageIndex = 1 ' 0 'I added the blank File.bmp to the imagelist, and it's ordinal is 1
        Catch exImage As System.Exception
        End Try
        Return tn
    End Function
#End Region

#Region "UTILITIES"

    Private Function IsInHolding() As Boolean
        Try
            Dim results = _docsAccess.FileSearchDirCommand(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xls*")
            If results.Length < 1 Then
                Return False
            Else
                Return True
            End If
            'If File.Exists(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xls") Then
            '    lblValidationStatus.Text = "In Holding"
            '    Return True
            'End If
            'Return False

        Catch ex As System.Exception
            db.WriteLog("IsInHolding", ex)
        End Try


    End Function

    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function

    Private Sub SetTabDirty(status As Boolean)
        For i = 0 To 8
            TabDirty(i) = status
        Next
    End Sub

    Public Sub BuildSupportTab()
        T("BuildSupportTab")
        SetSummaryFileButtons()
        BuildGradeGrid()
        ResetGradeTab(1)
    End Sub

    'No such stored proc:  "Console.InsertSARecord"
    'Private Function BuildNewSARecord() As String
    '    T("BuildNewSARecord")
    '    Dim params = New List(Of String)
    '    params.Add("RefNum/" + RefNum)

    '    If Not CheckSAMaster(RefNum) Then
    '        ds = db.ExecuteStoredProc("Console.InsertSARecord", params)
    '    Else
    '        MsgBox(CurrentCompany & " already has a record in SAMaster for " & Study & StudyYear)
    '        Return ""
    '    End If
    '    Return ds.Tables(0).Rows(0).Item(0)
    'End Function

    Private Function CheckSAMaster(RefNum) As Boolean
        T("CheckSAMaster")
        Dim params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console.CheckSANumber", params)
        If ds.Tables.Count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function RefiningRemovePassword(File As String, dFile As String, password As String, companypassword As String) As Boolean
        Dim success As Boolean = True
        Dim ext As String = GetExtension(File)
        If ext.Length > 2 AndAlso ext.ToUpper.Substring(0, 3) = "XLS" Then
            If File.ToUpper.Contains("RETURN") Or dFile.ToUpper.Contains("RETURN") Then
                success = Utilities.ExcelPassword(File, password, 1)
            Else
                success = Utilities.ExcelPassword(File, companypassword, 1)
            End If
        End If
        If File.Substring(File.Length - 3, 3).ToUpper = "DOC" Or File.Substring(File.Length - 3, 3).ToUpper = "OCX" Then
            success = Utilities.WordPassword(File, companypassword, 1)
        End If
        Return success
    End Function

    Private Function FixFilename(ext As String, strFile As String)
        T("FixFilename")
        Dim strNewFilename As String
        Dim NewExt As String = GetExtension(strFile)
        strNewFilename = strFile.Replace(NewExt, ext)
        Return strNewFilename
    End Function

    Private Function GetExtension(strFile As String) As String
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, strFile.Length - strFile.LastIndexOf(".") - 1)
        Catch
            ext = ""
        End Try
        Return ext
    End Function

    'Function FindModifier(strNextNum As String) As String
    '    ' Given a number to use in VRx, see if we need to add a letter to it.
    '    Dim strWork1 As String
    '    Dim strWork2 As String

    '    strWork1 = ""
    '    strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & cboCompany.Text & " Email Text.txt")

    '    If strWork2 > strWork1 Then
    '        strWork1 = strWork2
    '    End If

    '    For I = 0 To dgFiles.Rows.Count - 1
    '        If dgFiles.Rows(I).Cells(0).ToString.Substring(dgFiles.Rows(I).Cells(0).ToString.Length - 4, 4).ToUpper = ".XLS" And dgFiles.Rows(I).Cells(0).ToString.ToUpper.Contains("RETURN") Then
    '            strWork2 = PlugLetterIfNeeded("Return" & strNextNum & ".xls")
    '        Else
    '            strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & dgFiles.Rows(I).Cells(0).ToString)
    '        End If
    '        If strWork2 > strWork1 Then
    '            strWork1 = strWork2
    '        End If
    '    Next I
    '    Return strWork1
    'End Function

    Public Function PlugLetterIfNeeded(strFileName As String) As String
        Dim strWorkFileName As String
        Dim strDirResult As String
        Dim strCorrPath As String
        Dim strABC(10) As String
        Dim I As Integer
        Dim intInsertPoint As Integer

        strABC(0) = ""
        strABC(1) = "A"
        strABC(2) = "B"
        strABC(3) = "C"
        strABC(4) = "D"
        strABC(5) = "E"
        strABC(6) = "F"
        strABC(7) = "G"
        strABC(8) = "H"
        strABC(9) = "I"

        strCorrPath = CorrPath
        If strFileName.Substring(0, 6).ToUpper = "RETURN" Then
            intInsertPoint = 7
        Else
            intInsertPoint = 3
        End If

        For I = 0 To 9
            strWorkFileName = strFileName.Substring(0, intInsertPoint) & strABC(I) & Mid(strFileName, intInsertPoint + 1, 99)
            strDirResult = Dir(CorrPath & strWorkFileName)
            If strDirResult = "" Then
                Return strABC(I)
                Exit Function
            End If
        Next I

        MsgBox("More than 9 iterations of same file?", vbOKOnly, "Check with Joe Waters (JDW)?")

        PlugLetterIfNeeded = strFileName

    End Function



#Region "Xml App Settings"
    Private Function GetRefineryAppSettingsXmlSetting(key As String) As String
        Try
            If _settings.Keys.Count < 1 Then
                If Not ReadRefineryAppXmlSettings(_xmlAppSettingsPath) Then
                    WriteRefineryAppSettingsXmlFreshEntries(_xmlAppSettingsPath)
                End If
            End If
            ReadRefineryAppXmlSettings(_xmlAppSettingsPath)
            Return _settings(key)
            Return String.Empty
        Catch ex As Exception
            db.WriteLog("GetRefineryAppSettingsXmlSetting", ex)
            Return String.Empty
        End Try
    End Function
    Private Function SaveRefineryAppSettingsXmlSettings() As Boolean
        Try
            WriteRefineryAppXmlSettings(_xmlAppSettingsPath)
            Return True
        Catch ex As Exception
            db.WriteLog("SaveRefineryAppSettingsXmlSettings", ex)
            Return False
        End Try
    End Function
    Private Sub SaveRefineryAppSettingsXmlSetting(key As String, value As String)
        _settings(key) = value
        SaveRefineryAppSettingsXmlSettings()
    End Sub
    Private Sub WriteRefineryAppSettingsXmlFreshEntries(filePath As String)
        Dim streamWriter As StreamWriter = Nothing
        Try
            streamWriter = New StreamWriter(filePath)
            Dim settings As XmlWriterSettings = New XmlWriterSettings()
            settings.Indent = True
            settings.IndentChars = (ControlChars.Tab)
            Using writer As XmlWriter = XmlWriter.Create(streamWriter, settings)
                writer.WriteStartElement("RefineryAppSettings")
                writer.WriteEndElement()
                writer.Flush()
            End Using
        Catch ex As Exception
            db.WriteLog("WriteRefineryAppSettingsXmlFreshEntries", ex)
        Finally
            streamWriter.Close()
        End Try
    End Sub

    Private Sub WriteRefineryAppXmlSettings(filePath As String)
        Dim streamWriter As StreamWriter = Nothing
        Try
            streamWriter = New StreamWriter(filePath)
            Dim settings As XmlWriterSettings = New XmlWriterSettings()
            settings.Indent = True
            settings.IndentChars = (ControlChars.Tab)
            Using writer As XmlWriter = XmlWriter.Create(streamWriter, settings)
                writer.WriteStartElement("RefineryAppSettings")
                For Each pair As KeyValuePair(Of String, String) In _settings
                    writer.WriteElementString(pair.Key, pair.Value)
                Next
                writer.WriteEndElement()
                writer.Flush()
            End Using
        Catch ex As Exception
            db.WriteLog("WriteRefineryAppXmlSettings", ex)
        Finally
            streamWriter.Close()
        End Try
    End Sub

    Private Function ReadRefineryAppXmlSettings(filePath As String) As Boolean
        Try
            _settings.Clear()
            Dim element As String = String.Empty
            Dim value As String = String.Empty
            If Not File.Exists(filePath) Then
                Return False
            End If
            Using reader As XmlReader = XmlReader.Create(filePath)
                While reader.Read()
                    Select Case reader.NodeType
                        Case XmlNodeType.Element
                            If element.Length > 0 AndAlso element <> "RefineryAppSettings" Then
                                _settings.Add(element, value)
                            End If
                            element = reader.Name
                            value = String.Empty
                        Case XmlNodeType.Text
                            value = reader.Value
                            _settings.Add(element, reader.Value)
                            element = String.Empty
                        Case XmlNodeType.XmlDeclaration
                        Case XmlNodeType.ProcessingInstruction
                        Case XmlNodeType.Comment
                        Case XmlNodeType.EndElement
                    End Select
                End While
            End Using
            If element.Length > 0 Then
                _settings.Add(element, value)
            End If
            Return True
        Catch ex As Exception
            db.WriteLog("ReadRefineryAppXmlSettings", ex)
            Return False
        End Try
    End Function
#End Region


    Private Sub GetSettingsFile()
        T("GetSettingsFile")
        Try
            Dim settingsFilePath As String = _profileConsoleTempPath & "ConsoleSettings\" & _studyType & "Settings.txt"
            If Not File.Exists(settingsFilePath) Then
                CreateDefaultSettingsFile(settingsFilePath)
            End If

            'Dim emptyFile As Boolean = False
            'Using blankCheck As New StreamReader(settingsFilePath)
            '    If blankCheck.ReadToEnd().Trim().Length < 5 Then
            '        emptyFile = True
            '    End If
            'End Using
            'If emptyFile Then
            '    CreateDefaultSettingsFile(settingsFilePath)
            'End If
            'releaseFile = File.Exists("C:\NOFILE.txt")
            Using reader As New System.IO.StreamReader(settingsFilePath, False)
                Dim count As Integer = 0
                While Not reader.EndOfStream
                    Dim line As String = reader.ReadLine()
                    line = line.Replace("^", _delim)
                    ReDim Preserve _lastStudyAndRefnum(count)
                    _lastStudyAndRefnum(count) = line
                    count += 1
                End While
            End Using
            System.Threading.Thread.Sleep(1000)
            Using writer As New System.IO.StreamWriter(settingsFilePath, False)
                For count As Integer = 0 To _lastStudyAndRefnum.Count - 1
                    writer.WriteLine(_lastStudyAndRefnum(count))
                Next
            End Using

            'releaseFile = File.Exists("C:\NOFILE.txt")
        Catch ex As System.Exception
            db.WriteLog("GetSettingsFile 2 ", ex) '("GetSettingsFile 2 ", ex)
            'cboStudy.SelectedIndex = 1
        End Try
    End Sub

    Private Sub CreateDefaultSettingsFile(path As String)
        Try
            'create default file
            Dim studies As String = "SELECT DISTINCT CONCAT(Study,SUBSTRING(CONVERT(VARCHAR(4), StudyYear), 3, 2)) AS StudyAndYear,studyyear,"
            studies = studies + " SortOrder =  "
            studies = studies + "  CASE "
            studies = studies + " 	WHEN study = 'NSA' THEN 1"
            studies = studies + " 	WHEN study = 'EUR' THEN 2"
            studies = studies + " 	WHEN study = 'PAC' THEN 3"
            studies = studies + " 	WHEN study = 'LUB' THEN 4"
            studies = studies + "  End"
            studies = studies + " FROM [RefiningWork].[dbo].[TSort]"
            studies = studies + " WHERE RIGHT(StudyYear, 2) BETWEEN 10 AND 16"
            studies = studies + " AND Study IN ('NSA','EUR','PAC','LUB')"
            studies = studies + " ORDER BY StudyYear DESC,SOrtOrder ASC"
            db.SQL = studies
            Dim ds As DataSet = db.Execute()
            db.SQL = String.Empty
            If ds.Tables.Count > 0 Then
                Using writer As New System.IO.StreamWriter(path, False)
                    For Each Row In ds.Tables(0).Rows
                        If Not IsNothing(Row("StudyAndYear")) Then
                            writer.WriteLine(Row("StudyAndYear").ToString() + _delim)
                        End If
                    Next
                End Using
            End If
        Catch exDefault As Exception
            db.WriteLog("CreateDefaultSettingsFile() ", exDefault) '("GetSettingsFile 1 ", exDefault)
        End Try
    End Sub

    Private Function ReadLastStudy() As String
        T("ReadLastStudy")
        Dim lastStudy As String = String.Empty
        Try
            Dim path As String = _profileConsoleTempPath & "ConsoleSettings\" & _studyType & "LastStudy.txt"
            If Not File.Exists(path) Then
                For Each item As String In _lastStudyAndRefnum
                    Dim val = item.Split(_delim)
                    lastStudy = val(0) ' will make it the latest year
                Next
                Using writer As New StreamWriter(path, False)
                    writer.WriteLine(lastStudy)
                End Using
            Else
                Dim objReader As New IO.StreamReader(path)
                lastStudy = objReader.ReadLine()
                objReader.Close()
            End If
        Catch ex As System.Exception
            db.WriteLog("ReadLastStudy: ", ex)
        End Try
        Return lastStudy
    End Function

    Private Sub SaveLastStudy()
        Try
            If cboStudy.Text <> "" Then
                Dim path As String = _profileConsoleTempPath & "ConsoleSettings\" & _studyType & "LastStudy.txt"
                'If Directory.Exists(_profileConsoleTempPath) = False Then GetSettingsFile()
                Dim objWriter As New System.IO.StreamWriter(path, False)
                objWriter.WriteLine(cboStudy.Text)
                objWriter.Close()
            End If
        Catch ex As System.Exception
            db.WriteLog("SaveLastStudy", ex) '("SaveLastStudy", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub SaveCboSettings()
        T("SaveCboSettings")
        Try
            Dim path As String = _profileConsoleTempPath & "ConsoleSettings\" & _studyType & "Settings.txt"
            'If Directory.Exists(_profileConsoleTempPath) = False Then GetSettingsFile()
            Dim objWriter As New System.IO.StreamWriter(path, False)
            For count As Integer = 0 To _lastStudyAndRefnum.Length - 1
                objWriter.WriteLine(_lastStudyAndRefnum(count))
            Next
            objWriter.Close()
        Catch ex As System.Exception
            db.WriteLog("SaveCboSettings", ex) '("SaveCboSettings", ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ClearFields()
        T("ClearFields")

        lblMaintMgrName.Text = ""
        lblMaintMgrEmail.Text = ""
        lblOpsMgrName.Text = ""
        lblOpsMgrEmail.Text = ""
        lblTechMgrName.Text = ""
        lblTechMgrEmail.Text = ""
        lblRefMgrName.Text = ""
        lblRefMgrEmail.Text = ""
        lblName.Text = ""
        lblEmail.Text = ""
        lblPhone.Text = ""
        lblAltName.Text = ""
        lblAltEmail.Text = ""
        lblAltPhone.Text = ""
        lblRefCoName.Text = ""
        lblRefCoEmail.Text = ""
        lblRefCoPhone.Text = ""
        lblRefAltName.Text = ""
        lblRefAltEmail.Text = ""
        lblRefAltPhone.Text = ""
        lblMaintMgrName.Text = ""
        lblMaintMgrEmail.Text = ""
        lblOpsMgrName.Text = ""
        lblOpsMgrEmail.Text = ""
        lblTechMgrName.Text = ""
        lblTechMgrEmail.Text = ""
        lblRefMgrName.Text = ""
        lblRefMgrEmail.Text = ""
        btnCompanyPW.Text = ""
        lblInterimName.Text = ""
        lblInterimEmail.Text = ""
        lblInterimPhone.Text = ""

        txtDescription.Text = ""
        txtIssueName.Text = ""

        lblItemCount.Text = ""


    End Sub

    Private Sub ValFax_Build_Mfiles(strValFaxTemplate As String)
        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.
        Dim strName As String = ""
        Dim row As DataRow = Nothing
        Dim TemplateValues As New SA.Console.Common.WordTemplate()
        'Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""
        'Dim intNumDays As Integer

        Dim ErrorLogType As String = String.Empty
        'Dim docsFileSystemAccess As New DocsFileSystemAccess(ConsoleVertical)
        Try
            Dim VFFileName As String = String.Empty
            Dim result As Boolean = _docsAccess.ValFax_Build(strValFaxTemplate, UserName, TemplatePath,
            btnValFax.Text, _profileConsoleTempPath, cboConsultant.Text, RefNum, db, StudyDrive, Get4DigitYearFromCboStudy().ToString(),
            StudyRegion, Study, _profileConsoleTempPath, CurrentRefNum, CorrPath, TemplateValues, VFFileName,
            GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
            If Not result Then Exit Sub
            'could this be called by DocsMFilesAccess.vb.Valfax_build()
            WordTemplateReplace(_profileConsoleTempPath & strValFaxTemplate, TemplateValues, VFFileName)
        Catch ex As System.Exception
            db.WriteLog(_docsAccess.ErrorLogType, ex)
            MessageBox.Show(ex.Message)
        Finally
            btnValFax.Text = "New IDR"
        End Try
        'MSWord = Nothing
    End Sub

    Public Sub WordTemplateReplace(strfile As String, fields As SA.Console.Common.WordTemplate, strfilename As String, Optional PresentationNote As Boolean = False)

        Dim oWord As Word.Application
        Dim oDoc As Object
        Dim range As Object
        Dim DoubleCheck As String = "These are the template fields: " & vbCrLf

        Dim tempFolder As String = strfile.Replace(Utilities.ExtractFileName(strfile), "")

        For count = 0 To fields.Field.Count - 1
            DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
        Next
        MessageBox.Show(DoubleCheck, "Template Field Check")

        'If File.Exists(strfilename) Then ' do this check from MFiles not directory.

        'If _useMFiles Then 'don't need full path
        '    strfilename = Utilities.ExtractFileName(strfilename)
        '    'strfilename = Utilities.FileNameWithoutExtension(strfilename)
        'End If
        If Not IsNothing(_docsAccess.GetDocInfoByNameAndRefnum(Utilities.FileNameWithoutExtension(strfilename), GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()), False)) Then
            strfilename = InputBox("CAUTION: The suggested file name already exists in MFiles. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilename)
        Else
            strfilename = InputBox("Here is the suggested name for this new file: " & Chr(10) & "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename)
        End If
        ' If they hit cancel, just quit.
        If strfilename = "" Then
            Exit Sub
        End If

        oWord = Utilities.GetWordProcess() ' CreateObject("Word.Application")

        'If Word already open, don't kill the Word Process when done.
        'Find out if Word already open here:
        Dim existingDoc As Boolean = oWord.Documents.Count > 0

        If ConfigurationManager.AppSettings("RefiningWordTemplateReplace-ShowWordDoc").ToString().ToUpper = "TRUE" Then
            oWord.Visible = True
        Else
            oWord.Visible = False
        End If
        oDoc = oWord.Documents.Open(strfile)
        range = oDoc.Content
        Try
            For count = 0 To fields.Field.Count - 1
                With oWord.Selection.Find
                    .Text = fields.Field(count)
                    .Replacement.Text = fields.RField(count)
                    .Forward = True
                    .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                    .Format = False
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
            Next

            Dim deliverable As Integer
            If strfilename.StartsWith("VF") Then  'all will be IDR
                deliverable = 12
            ElseIf Not IsNothing(PresentationNote) AndAlso PresentationNote = True Then
                deliverable = 17  'Presenters Notes
            Else
                deliverable = 4
            End If

            oDoc.SaveAs(tempFolder + strfilename, ADDTORECENTFILES:=True)
            oDoc.Close()
            'Don't close out other open apps  oWord.Quit()
            'oWord = Nothing
            'now upload
            'Dim benchmarkingParticipant As String = Nothing
            'If ConsoleVertical = VerticalType.OLEFINS Then
            '    benchmarkingParticipant = Utilities.GetLongRefnum(GetRefNumCboText())
            'End If
            If _docsAccess.UploadDocToMfiles(tempFolder + strfilename, 52, GetRefNumCboText(), deliverable, -1, DateTime.Today) Then
                'then reopen from MFIles and check out, delete local file at end of this method
                Dim mFilesFilename As String = Utilities.FileNameWithoutExtension(strfilename)
                _docsAccess.OpenFileFromMfiles(mFilesFilename, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                'Dim filesInfo As ConsoleFilesInfo = docsFIleSystemAccess.GetDocInfoByNameAndRefnum(mFilesFilename, _
                '    GetRefNumCboText(), "20" + GetRefNumCboText(), False)
                'If Not IsNothing(filesInfo) AndAlso Not IsNothing(filesInfo.Files) AndAlso Not IsNothing(filesInfo.Files(0)) Then
                '    Dim vaultGuid As String = ConfigurationManager.AppSettings("VaultGuid").ToString()
                '    vaultGuid = vaultGuid.Replace("{", "")
                '    vaultGuid = vaultGuid.Replace("}", "")
                '    Dim url As String = "m-files://edit/" + vaultGuid + "/0-" + filesInfo.Files(0).Id.ToString()
                '    Try
                '        Process.Start(url)
                '    Catch ex As Exception
                '        MsgBox("Unexpected error: Can't find the new file " + strfilename + " in MFiles. The url " + url + " failed.")
                '    End Try
                'Else
                '    MsgBox("Unexpected error: Can't find the new file " + strfilename + " in MFiles. " + mFilesFilename + " was not found.")
                'End If
            End If
            'RefreshMFilesCollections(0)
            Try

                'leave it open for consultants to check

                'If existingDoc Then
                '    'activate prior doc to release file on hard drive for deletion
                '    oWord.Documents(1).Activate()
                'Else
                '    oWord.Quit()
                'End If
                Dim files() = Directory.GetFiles(tempFolder)
                For i As Integer = 0 To files.Length - 1
                    Try
                        File.Delete(files(i))
                    Catch
                    End Try
                Next
            Catch
                'not critical that it get deleted at this second
            End Try
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        Finally
            'btnValFaxText = "New IDR"
            'oWord = Nothing
        End Try
    End Sub


    Private Sub ValFax_Build_Legacy(strValFaxTemplate As String)

        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.


        Dim strDeadline As String
        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult() As FileInfo
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim TemplateValues As New SA.Console.Common.WordTemplate()
        Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""
        Dim frmTemp = New frmTemplates()



        Try
            RefNum = RefNum.Trim
            If File.Exists(_profileConsoleTempPath & "ValFax.txt") Then File.Delete(_profileConsoleTempPath & "ValFax.txt")

            If strValFaxTemplate = "" Then
                ' Find out which template they want to use.
                strProgress = "cboTemplates.Clear"
                frmTemp.cboTemplates.Items.Clear()
                Dim myDir As New DirectoryInfo(TemplatePath)
                strDirResult = myDir.GetFiles("*.doc*")
                If strDirResult.Length = 0 Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)
                    Exit Sub
                End If
                strProgress = "Loading cboTemplates"
                Dim i As Int16 = 0
                For i = 0 To strDirResult.Length - 1
                    If InStr(1, strDirResult(i).ToString, "Cover") > 0 And strDirResult(i).ToString.Contains(ParseRefNum(RefNum, 2)) Then
                        frmTemp.cboTemplates.Items.Add(strDirResult(i).ToString)
                    End If
                Next

                If frmTemp.cboTemplates.Items.Count < 1 Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)

                    Exit Sub
                End If

                ' Default to first one.
                frmTemp.cboTemplates.SelectedIndex = 0

                ' If there is only one, don't make them choose,
                ' just go.
                If frmTemp.cboTemplates.Items.Count > 1 Then
                    'MsgBox "Show form modal"
                    ' We use this form for other purposes as well (like selecting which SpecFrac file to open).
                    ' So, reload the caption and label items to the origian "ValFax" values.
                    frmTemp.Text = "Select a Cover Template"

                    ' Ready to show them the form.
                    frmTemp.ShowDialog()
                    If Not frmTemp.OKClick Then Exit Sub

                    ' Load strValFaxTemplate from what they selected on
                    ' frmTemplates. (strTemplateFile is a global variable.)
                    strValFaxTemplate = frmTemp.cboTemplates.SelectedItem
                    'MsgBox "After show form modal"
                Else
                    strValFaxTemplate = frmTemp.cboTemplates.SelectedItem

                End If

            End If

            ' At this point we have the name of the template to use.
            'MsgBox "Prepare new doc"
            'btnValFax.Text = "Preparing New Document"
            strProgress = "Preparing New Document"
            ' Create the file to pass the data in.
            'Open gstrTempFolder & "ValFax.txt" For Output As #1

            Dim objWriter As New System.IO.StreamWriter(_profileConsoleTempPath & "ValFax.txt", True)


            ' Todays date
            strProgress = "Printing Long Date"
            objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
            TemplateValues.Field.Add("_TodaysDate")
            TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))



            strProgress = "Getting business days to respond."
            ' Due date
            ' Need a way to make sure we get valid input here.
            If Dir(CorrPath & "vf*") = "" Then
                intNumDays = 10
            Else
                intNumDays = 5
            End If
            strDeadline = InputBox("How many business days do you want to give them to respond?", "Deadline", intNumDays)
            datDeadline = Utilities.ValFaxDateDue(strDeadline)
            objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
            TemplateValues.Field.Add("_DueDate")
            TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
            strProgress = "Getting Company name"
            ' Company Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)

            If ds.Tables.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                objWriter.WriteLine(row("Company"))
                TemplateValues.Field.Add("_Company")
                TemplateValues.RField.Add(row("Company").ToString)

                strProgress = "Getting Location"
                ' Refinery Name
                objWriter.WriteLine(row("Location"))
                TemplateValues.Field.Add("_Refinery")
                TemplateValues.RField.Add(row("Location").ToString)
                Coloc = row("Coloc").ToString

            End If

            strProgress = "Getting Contact info"


            ' Contact Name
            If strValFaxTemplate = "LUB14 Cover FLCOMP.doc" Then
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetFuelLubeContactInfo", params)
            Else
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
            End If
            If ds.Tables.Count = 0 Then

                If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then

                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim lfn As String = ""
                    For Each row In ds.Tables(0).Rows
                        If lfn <> row("RefName").ToString Then
                            strFaxName += _tInfo.ToTitleCase(row("RefName").ToString + IIf(ds.Tables(0).Rows.Count > 1, ", ", ""))
                            lfn = _tInfo.ToTitleCase(row("RefName").ToString)
                            strFaxEmail += row("RefEmail").ToString + IIf(ds.Tables(0).Rows.Count > 1, "; ", "")
                        Else
                            strFaxName = _tInfo.ToTitleCase(strFaxName.Replace(",", ""))
                            strFaxEmail = strFaxEmail.Replace(";", "")
                        End If
                    Next
                End If
            End If

            objWriter.WriteLine(strFaxName)

            TemplateValues.Field.Add("_Contact")
            TemplateValues.RField.Add(_tInfo.ToTitleCase(strFaxName.ToLower))
            ' Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_EMail")
            TemplateValues.RField.Add(strFaxEmail)
            strFaxEmail = ""
            strFaxName = ""
            strProgress = "Getting consultant info"
            ' Consultant Name
            params = New List(Of String)
            params.Add("Initials/" + mUserName)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)

                    If row("ConsultantName") Is Nothing Or IsDBNull(row("ConsultantName")) Then
                        strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                    Else
                        strName = row("ConsultantName").ToString
                    End If
                Else
                    strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                End If
            End If
            strName = _tInfo.ToTitleCase(strName)

            objWriter.WriteLine(strName)
            TemplateValues.Field.Add("_ConsultantName")
            TemplateValues.RField.Add(strName)
            ' Consultant Initials
            objWriter.WriteLine(mUserName)
            TemplateValues.Field.Add("_Initials")

            TemplateValues.RField.Add(mUserName)
            strProgress = "Path to store it in"
            ' Path to store it in
            ' If it is Trans Pricing or FLCOMP, put it, without VF#, into Refinery Corr folder.
            If strValFaxTemplate.Substring(0, 17) = "Trans Pricing.doc" Then
                Filename = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\" & StudyRegion & "\Correspondence\" & RefNum & "\" & Coloc & "\"
                ' If it has "Company Wide" in the name, store it in the Company Corr folder
            ElseIf InStr(1, strValFaxTemplate, "Company Wide") > 0 Then
                Filename = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\" & StudyRegion & "\Company Correspondence\" & Coloc & "\"
                ' Otherwise -- MOST CASES HERE -- store it in the Refinery Corr folder with VF# on the front.
            Else
                If strValFaxTemplate <> "LUB14 Cover FLCOMP.doc" Then
                    ' Request from DEJ 3/2011 -- if it a Lubes refinery, put "Lube" at end of doc name.
                    Filename = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\" & StudyRegion & "\Correspondence\" & RefNum & "\VF" & NextVF() & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                Else
                    Filename = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\" & StudyRegion & "\Correspondence\" & RefNum & "\VFFL" & NextVFFL() & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"

                End If

            End If

            objWriter.WriteLine(Filename)


            ' --------- Add new items here 4/20/2007 ---------

            strProgress = "Getting Company Contact info"

            ' Company Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("ContactType/Coord")
            params.Add("StudyYear/" + Get4DigitYearFromCboStudy().ToString()) ' StudyYear) Get
            ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)

            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row In ds.Tables(0).Rows
                        strFaxName = _tInfo.ToTitleCase(row("FirstName").ToString & " " & row("LastName").ToString)
                        strFaxEmail = row("Email").ToString
                    Next
                End If
            End If

            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_CoCoContact")
            TemplateValues.RField.Add(strFaxName)

            ' Company Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_CoCoEMail")
            TemplateValues.RField.Add(strFaxEmail)
            objWriter.Close()
            ' --------- End of new items 4/20/2007 ---------



            ' Make sure the template file is available to copy.
            'On Error GoTo File1NotAvailable
            'intAttr = GetAttr(strTemplatePath & strValFaxTemplate)
            ' Next 2 lines commented 12/1/04 to try new approach
            ' due to problem with compression of network files.
            'SetAttr strTemplatePath & strValFaxTemplate, intAttr
            'On Error GoTo 0

            strProgress = "Copy doc to hard drive"
            ' Bring the doc down to the hard drive.

            If File.Exists(_profileConsoleTempPath & strValFaxTemplate) Then

                Kill(_profileConsoleTempPath & strValFaxTemplate)

            End If

            If File.Exists(TemplatePath & strValFaxTemplate) Then
                TemplatePath = TemplatePath
            End If

            ' Copy the Template to my work area.
            File.Copy(TemplatePath & strValFaxTemplate, _profileConsoleTempPath & strValFaxTemplate, True)
            'varShellReturn = Shell("XCopy """ & strTemplatePath & strValFaxTemplate & """ """ & gstrTempFolder & """ /Y")
            'Stop
            timStart = Now()
            'MsgBox Now()
            Do While File.Exists(_profileConsoleTempPath & strValFaxTemplate) = False

                If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                    MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                    'Exit Do
                End If
            Loop

            'moved from WordTemplateReplace so inputbox won't popup behind Word doc.
            'this will always have mode = 1
            'If mode = 1 Then

            Dim fpath As String = String.Empty
            Dim first, second As Integer
            If File.Exists(Filename) Then
                Filename = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", Filename)
            Else
                first = Filename.LastIndexOf("\") + 1
                second = Filename.Length - first
                fpath = Filename.Substring(0, first)
                'If mode = 1 Then
                Filename = InputBox("Here is the suggested name for this New Vetted Fax document." & Chr(10) &
                "Click OK to accept it, or change, then click OK.", "SaveAs", Filename.Substring(first, second))
            End If
            ' If they hit cancel, just stop.
            If Filename = "" Then Exit Sub
            If fpath.Length > 0 Then
                Filename = fpath & Filename
            End If
            'end move

            Utilities.WordTemplateReplace(_profileConsoleTempPath & strValFaxTemplate, TemplateValues, Filename, 1)

        Catch ex As System.Exception
            MSWord = Nothing
            db.WriteLog("ValFax_Build_Legacy: " & CurrentRefNum.ToString, ex)
            MessageBox.Show(ex.Message)
        End Try
        MSWord = Nothing
    End Sub

    Function NextFile(findFile As String, findExt As String)

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 20
            strDirResult = Dir(findFile & CStr(I) & "*." & findExt)
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function

    Function NextVF()
        'If _useMFiles Then
        '    Return _docsAccess.NextV("VF", GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
        'Else
        'Find all the files starting with VF
        Dim fileNamePattern As String = "vf*"
        'VF file always start with 1
        Dim VFSequence = getNextFileSequenceNumber(fileNamePattern)
        If VFSequence Is Nothing Or VFSequence = 0 Then
            Return 1
        Else
            Return VFSequence
        End If

        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 1 To 20
        '    strDirResult = Dir(CorrPath & "\vf" & CStr(I) & "*.doc")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        ''End If
        'Return Nothing
    End Function

    Function NextVFFL()
        'If _useMFiles Then
        '    Return _docsAccess.NextV("VFFL", GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
        'Else
        Dim fileNamePattern As String = "vffl*"
        Dim VFFLSequence = getNextFileSequenceNumber(fileNamePattern)
        If VFFLSequence Is Nothing Or VFFLSequence = 0 Then
            Return 1
        Else
            Return VFFLSequence
        End If

        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 1 To 20
        '    strDirResult = Dir(CorrPath & "\vffl" & CStr(I) & "*.doc")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        ''End If
        'Return Nothing
    End Function

    Function NextVR()
        Dim fileNamePattern As String = "vr*"
        Dim VRSequence = getNextFileSequenceNumber(fileNamePattern)
        'VR file always start with 1
        If VRSequence Is Nothing Or VRSequence = 0 Then
            Return 1
        Else
            Return VRSequence
        End If

        'If _useMFiles Then
        '    Return _docsAccess.NextV("VR", GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
        'Else
        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 1 To 20
        '    strDirResult = Dir(CorrPath & "\vr" & CStr(I) & "*.doc")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        ''End If
        'Return Nothing
    End Function

    Function NextVRFL()
        Dim fileNamePattern As String = "vrfl*"
        Dim VRFLSequence = getNextFileSequenceNumber(fileNamePattern)
        'VR file always start with 1
        If VRFLSequence Is Nothing Or VRFLSequence = 0 Then
            Return 1
        Else
            Return VRFLSequence
        End If


        'If _useMFiles Then
        '    Dim count As Integer? = _docsAccess.NextReturnFile(GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
        '    If IsNothing(count) Then
        '        Return String.Empty
        '    Else
        '        Return count.ToString()
        '    End If
        'Else
        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 1 To 20
        '    strDirResult = Dir(CorrPath & "\vrfl" & CStr(I) & "*.doc")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        ''End If
        'Return Nothing
    End Function

    Function NextReturnFile(fuelsNotLubes As Boolean, Optional locationName As String = "")
        Dim shortFilter As String = "F_RETURN"
        If Not fuelsNotLubes Then
            shortFilter = "L_RETURN"
        End If
        'If _useMFiles Then
        '    'locationName only used for 2016 and higher
        '    Dim filter As String = locationName.ToUpper() + " " + shortFilter
        '    Return _docsAccess.NextReturnFile(GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
        'Else
        'Dim strDirResult As String
        'Dim I As Integer
        'For I = 0 To 20
        '    strDirResult = Dir(CorrPath & "*" & shortFilter & CStr(I) & "*.xls")
        '    If strDirResult = "" Then
        '        Return I
        '        Exit For
        '    End If
        'Next I
        'End If
        Dim fileNamePattern As String = "*" & shortFilter & "*"

        Return getNextFileSequenceNumber(fileNamePattern)
    End Function
    Function getNextFileSequenceNumber(fileNamePattern As String)
        Dim strFileName As String = ""
        Dim di As New IO.DirectoryInfo(CorrPath)
        Dim aryFi As IO.FileInfo() = di.GetFiles(fileNamePattern)
        Dim fi As IO.FileInfo
        Dim FileSequence As New List(Of Integer)
        Dim i As Integer
        fileNamePattern = Replace(fileNamePattern.ToUpper, "*", "")
        If aryFi.Length > 0 Then
            For Each fi In aryFi
                Dim temp As String()
                strFileName = fi.Name.ToUpper
                temp = strFileName.Split(" ")
                For i = 0 To temp.Length - 1
                    If temp(i).Contains(Replace(fileNamePattern, "*", "").ToUpper) Then
                        temp(i) = (Replace(temp(i), fileNamePattern, ""))
                        If temp(i).Contains(".") Then
                            If temp(i).IndexOf(".") > 0 Then
                                temp(i) = temp(i).Substring(0, temp(i).IndexOf("."))
                            End If
                        End If
                        If temp(i).Contains("-") Then
                            If temp(i).IndexOf("-") > 0 Then
                                temp(i) = temp(i).Substring(0, temp(i).IndexOf("-"))
                            End If
                        End If
                        If Regex.IsMatch(temp(i), "^[0-9 ]+$") Then
                            FileSequence.Add(temp(i))
                        End If
                    End If
                Next
            Next
            If FileSequence.Count > 0 Then
                FileSequence.Sort()
                Return (FileSequence(FileSequence.Count - 1) + 1)
            End If
            Return 0
        End If
        Return Nothing
    End Function
    Function getNextPFileSequenceNumber(fileNamePattern As String)
        Dim strFileName As String = ""
        Dim di As New IO.DirectoryInfo(CorrPath)
        Dim aryFi As IO.FileInfo() = di.GetFiles(fileNamePattern)
        Dim fi As IO.FileInfo
        Dim FileSequence As New List(Of Integer)
        fileNamePattern = Replace(fileNamePattern.ToUpper, "*", "")
        If aryFi.Length > 0 Then
            For Each fi In aryFi
                Dim tempNum As String
                strFileName = fi.Name.ToUpper
                tempNum = strFileName.Substring(strFileName.IndexOf(fileNamePattern) + fileNamePattern.Length, (strFileName.IndexOf(".") - strFileName.IndexOf(fileNamePattern) - fileNamePattern.Length))
                If tempNum = "" Then
                    tempNum = "0"
                End If
                If Regex.IsMatch(Trim(tempNum), "^[0-9 ]+$") Then
                    FileSequence.Add(tempNum)
                End If
            Next
            If FileSequence.Count > 0 Then
                FileSequence.Sort()
                Return (FileSequence(FileSequence.Count - 1) + 1)
            End If
        End If
        Return 0
    End Function
    'Function getFileSequenceNumber(fileName As String, fileNamePattern As String)
    '    Dim i As Integer
    '    fileNamePattern = fileNamePattern.ToUpper
    '    Dim temp As String()
    '    temp = fileName.ToUpper.Split(" ")
    '    For i = 0 To temp.Length - 1
    '        If temp(i).Contains(fileNamePattern.ToUpper) Then
    '            temp(i) = (Replace(temp(i), fileNamePattern, ""))
    '            If temp(i).Contains(".") Then
    '                If temp(i).IndexOf(".") > 0 Then
    '                    temp(i) = temp(i).Substring(0, temp(i).IndexOf("."))
    '                End If
    '            End If
    '            If temp(i).Contains("-") Then
    '                If temp(i).IndexOf("-") > 0 Then
    '                    temp(i) = temp(i).Substring(0, temp(i).IndexOf("-"))
    '                End If
    '            End If
    '            If temp(i).Contains("_") Then
    '                If temp(i).IndexOf("_") > 0 Then
    '                    temp(i) = temp(i).Substring(temp(i).IndexOf("_") + 1, temp(i).Length - temp(i).IndexOf("_") - 1)
    '                End If
    '            End If
    '            If Regex.IsMatch(temp(i), "^[0-9 ]+$") Then
    '                Return temp(i)
    '            End If
    '        End If
    '    Next
    '    Return Nothing
    'End Function
    Function CacheShellIcon(ByVal argPath As String) As String
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            Return mKey
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function

    Function GetStatus(refnum As String)

        refnum = refnum.Trim
        Dim status As String = ""
        Dim path = StudyDrive & Get4DigitYearFromCboStudy.ToString() & "\" & GetStudyRegionFromCboStudy() & "\refinery\" & refnum & "\"
        If Dir(path & ParseRefNum(refnum, 0) & ".xls*") = "" Then
            status = "Not Received"
        Else
            status = "In Progress"
        End If

        If Dir(IDRPath & "Holding\" & ParseRefNum(refnum, 0) & ".xls*") <> "" Then
            status = "In Holding"
        End If

        If VChecked(refnum, "V1") Then
            status = "Polishing"
        End If


        If VChecked(refnum, "V2") Then
            status = "Complete"
        End If

        Return status

    End Function

    Private Sub BuildVR()
        Dim strDirResult As String
        Dim strTextForFile As String

        strDirResult = Dir(CorrPath & "VR" & SelectedRow & ".txt")
        If strDirResult <> "" Then
            MsgBox("There already is a VR" & Me.SelectedRow & " file.")
            Exit Sub
        End If

        strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        If strTextForFile <> "" Then
            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VR" & SelectedRow & ".txt", True)
            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strTextForFile)
            objWriter.Close()
            MsgBox("VR" & SelectedRow & ".txt has been built.")
        Else
            MsgBox("VR file build was cancelled.")
        End If
        'RefreshMFilesCollections(0)
    End Sub

    Private Sub btnBuildVRFile_Click_1(sender As Object, e As EventArgs)
        BuildVR()
    End Sub

    Function DatePulledFromVAFile(ByVal strDirAndFileName As String) As Date

        Dim strWork As String = ""
        ' Read the second line of the VA file. Get the date from there.
        Dim objReader As New System.IO.StreamReader(strDirAndFileName, True)

        ' Read and toss the first line
        strWork = objReader.ReadLine()
        ' Read the date-time line
        strWork = objReader.ReadLine()

        DatePulledFromVAFile = strWork

    End Function

    Sub ResetGradeTab(intSectionORVersion As Integer)
        T("ResetGradeTab")
        Dim intVersion As Integer
        Dim params As New List(Of String)
        Dim rdr As SqlDataReader


        If intSectionORVersion = 1 Then
            ' Load the data by Section.
            ' First determined which version we need to get.
            params.Add("RefNum/" & ParseRefNum(RefNum, 0))

            rdr = db.ExecuteReader("Console.GetSectionGrade", params)
            If Not rdr.IsClosed Then
                If rdr.Read() Then
                    intVersion = rdr.GetInt16(0)
                End If
                rdr.Close()
            End If

            params.Clear()
            params.Add("RefNum/" & ParseRefNum(RefNum, 0))
            params.Add("Version/" & intVersion)

            ds = db.ExecuteStoredProc("Console.GetGradesBySection", params)

            dgGrade.DataSource = ds.Tables(0)

        Else
            ' Load the data by Version.
            Dim sqlForVersion As String = "select max(version) as version from val.Versions where refnum = '" & ParseRefNum(RefNum, 0) & "'"
            db.SQL = sqlForVersion
            Dim dsVersion As DataSet = db.Execute()
            db.SQL = String.Empty
            If Not IsDBNull(dsVersion.Tables(0).Rows(0)("version")) And Not IsNothing(dsVersion) AndAlso dsVersion.Tables.Count = 1 AndAlso dsVersion.Tables(0).Rows.Count > 0 Then
                intVersion = dsVersion.Tables(0).Rows(0)("version")
            End If

            params.Clear()
            params.Add("RefNum/" & RefNum)
            params.Add("Version/" & intVersion)

            ds = db.ExecuteStoredProc("Console.GetGradesByVersion", params)

            dgGrade.DataSource = ds.Tables(0)

        End If
        TimeHistory_Load()
        TimeSummary_Load()

    End Sub

    Private Function VChecked(mRefNum As String, mode As String) As Boolean
        Dim ret As Boolean = False
        Dim params As New List(Of String)
        params.Add("RefNum/" + mRefNum)
        params.Add("IssueID/" + mode)
        Dim ds As DataSet = db.ExecuteStoredProc("Console." & "IssueCheckV", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0).ToString = "Y" Then Return True
            End If
        End If
        Return False
    End Function

    Private Sub GetDirectories(ByVal subDirs() As DirectoryInfo, ByVal nodeToAddTo As TreeNode)

        Dim aNode As TreeNode
        Dim subSubDirs() As DirectoryInfo
        Dim subDir As DirectoryInfo
        For Each subDir In subDirs
            aNode = New TreeNode(subDir.Name, 0, 0)
            aNode.Tag = subDir
            aNode.ImageKey = "folder"
            subSubDirs = subDir.GetDirectories()
            If subSubDirs.Length <> 0 Then
                GetDirectories(subSubDirs, aNode)
            End If
            nodeToAddTo.Nodes.Add(aNode)
        Next subDir

    End Sub

    Private Sub GeneratePolishRpt(twoDigitYear As String)
        Dim w As Excel.Workbook
        If Int32.Parse(twoDigitYear) < 16 Then
            Dim xl As Excel.Application
            xl = Utilities.GetExcelProcess()
            w = xl.Workbooks.Open(PolishPath & "Database Version Of Outliers.xlsm", , True)
            xl.Visible = True
            Try
                xl.Run("Console_OpenPolishRpt_RefNum", CurrentRefNum)
                w.Close()
            Catch
            Finally
                If File.Exists(CorrPath & CurrentRefNum & "_Polish.xls") Then Process.Start(CorrPath & CurrentRefNum & "_Polish.xls")
            End Try
        Else
            'Don't use this because need to keep spreadsheet open after firing macro
            'Dim officeDocProcess As New OfficeDocProcess(0)
            Try
                Dim xl As Excel.Application
                xl = Utilities.GetExcelProcess()
                w = xl.Workbooks.Open(ConfigurationManager.AppSettings("RefiningPathToPolishSpreadsheet").ToString(), , True)
                xl.Visible = True
                xl.Run(ConfigurationManager.AppSettings("RefiningPolishMacroName").ToString(), GetRefNumCboText())
            Catch exXls As Exception
                If exXls.Message.Contains("HRESULT") Then
                    'swallow exception per Sung
                    'MsgBox("Excel Macro encountered a problem. Please notify Sung." & Environment.NewLine & exXls.Message)
                End If
            Finally
                Try
                    'w.Close()  Sung says to leave it open
                Catch
                End Try
            End Try
        End If
    End Sub

    Private Sub OpenPolishReport(twoDigitYear As String)
        Dim rtn As DialogResult = System.Windows.Forms.DialogResult.OK
        'If Int32.Parse(twoDigitYear) < 16 Then
        If Not File.Exists(CorrPath & Trim(cboRefNum.SelectedItem.ToString) & "_Polish.xls") Then
            rtn = MessageBox.Show("Run Polish Macro?", "Polish", MessageBoxButtons.OKCancel)
            If rtn = System.Windows.Forms.DialogResult.OK Then
                GeneratePolishRpt(twoDigitYear)
            End If
        Else
            GeneratePolishRpt(twoDigitYear)
        End If

        'Else
        '    rtn = MessageBox.Show("Run Polish Macro?", "Polish", MessageBoxButtons.OKCancel)
        '    If rtn = System.Windows.Forms.DialogResult.OK Then
        '        GeneratePolishRpt(twoDigitYear)
        '    End If
        'End If
    End Sub

    'Validation
    Private Sub IDR(JustLooking As Boolean, validationFile As String)
        'Dim AlreadyOpen As Boolean = False
        'Launch Excel Validation
        ' Create new Application.
        Dim xl As Excel.Application
        xl = Utilities.GetExcelProcess()
        Try
            'For Each wb In xl.Workbooks
            '    If wb.Name = validationFile Then
            '        AlreadyOpen = True
            '        Exit For
            '    End If
            'Next wb

            Dim w As Excel.Workbook
            ' Open Excel spreadsheet.
            'If Not AlreadyOpen Then
            If File.Exists(IDRPath & validationFile) Then
                Dim networkInfo As FileInfo = New FileInfo(IDRPath & validationFile)
                If IO.File.Exists(_profileConsoleReferenceRefiningPath & validationFile) Then
                    Dim localInfo As FileInfo = New FileInfo(_profileConsoleReferenceRefiningPath & validationFile)
                    If networkInfo.LastWriteTime > localInfo.LastWriteTime Then
                        File.Delete(_profileConsoleReferenceRefiningPath & validationFile)
                        'Not sure this ever gets populated
                        If (VPN) Then
                            Dim b As DialogResult = MessageBox.Show("Since you are On VPN, we are going to pause to give the file enough time to load", "Wait", MessageBoxButtons.OK)
                            System.Threading.Thread.Sleep(3000)
                        End If
                        FileCopy(IDRPath & validationFile, _profileConsoleReferenceRefiningPath & validationFile)
                    End If
                Else
                    FileCopy(IDRPath & validationFile, _profileConsoleReferenceRefiningPath & validationFile)
                End If
            Else
                MsgBox(validationFile & " Is missing from " & IDRPath)
                Exit Sub
            End If
            w = xl.Workbooks.Open(_profileConsoleReferenceRefiningPath & validationFile)
            xl.Visible = True
            w.RunAutoMacros(1)
            'End If
            If xl.Workbooks(validationFile).Windows(validationFile).Visible = False Then xl.Workbooks(validationFile).Windows(validationFile).Visible = True
            If bJustLooking Then
                'Run Validation Macro
                xl.Run("JustLookin", GetRefNumCboText())
            Else
                xl.Run("DelayLoadRefinery", GetRefNumCboText())
            End If
            'make the excel as the active application
            AppActivate(xl.Caption)
        Catch ex As System.Exception
            db.WriteLog("Validation:  " & GetRefNumCboText().ToString, ex)
            MessageBox.Show("Trouble launching " & validationFile & ".  Make sure your Macro File is not already opened. " & vbCrLf & ex.Message)
        End Try

    End Sub

    Private Function Get4DigitYearFromCboStudy() As Integer
        Dim twodigitYear As String = System.Text.RegularExpressions.Regex.Replace(cboStudy.Text, "[^0-9.]", "")
        Dim result As Integer = 0
        If Int32.TryParse(twodigitYear, result) Then
            Return 2000 + result
        Else
            Return 0
        End If
    End Function

    Private Function GetStudyRegionFromCboStudy() As String
        Dim studyRegion As String = cboStudy.Text.Substring(0, 3)
        Return studyRegion
    End Function
    Private Function Get2DigitYearFromCboStudy() As Integer
        Dim twoDigitYear As String = System.Text.RegularExpressions.Regex.Replace(cboStudy.Text, "[^0-9.]", "")
        Dim result As Integer = 0
        If Int32.TryParse(twoDigitYear, result) Then
            Return result
        Else
            Return 0
        End If
    End Function


    'Set all directory paths
    Private Sub SetPaths()
        T("SetPaths")
        Try
            If cboStudy.Text <> "" Then
                StudyRegion = cboStudy.Text.Substring(0, 3)
            Else
                Exit Sub
            End If
            If cboCompany.Text <> "" Then
                Coloc = GetCompanyName(GetRefNumCboText())
                If Not IsNothing(Coloc) AndAlso Coloc.Contains("-") Then
                    Company = Coloc.Substring(0, Coloc.IndexOf("-") - 1)
                End If
            End If
            If GetRefNumCboText() <> "" Then
                mRefNum = GetRefNumCboText()
            End If
            Dim new4digitYear As Integer = Get4DigitYearFromCboStudy()

            RefineryPath = StudyDrive & new4digitYear.ToString() & "\" & StudyRegion & "\Refinery\" & ParseRefNum(mRefNum, 0) & "\"
            GeneralPath = StudyDrive & "General\" & StudyRegion & "\" & ParseRefNum(mRefNum, 1) & StudyRegion & "\"
            ProjectPath = StudyDrive & new4digitYear.ToString() & "\Project\"

            CorrPath = BuildCorrPathFromRefnum(GetRefNumCboText(), new4digitYear.ToString(), StudyRegion)
            T("returned from CorrPath")
            DataFolderPath = BuildDataFolderPath(GetRefNumCboText(), new4digitYear.ToString(), StudyRegion)
            ClientToolsPath = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\Software\Input Diskette\ReadyToSend\Fuels\WkSheets\"
            T("returned from DataFolderPath")
            '            If Not IsNothing(CorrPath) AndAlso CorrPath.Length > 1 Then
            '            DataFolderPath = CorrPath.Replace("Correspondence", "Refinery")
            'Else
            'DataFolderPath = Nothing
            'End If
            Try
                If Company.Length > 0 Then
                    CompanyPath = StudyDrive & new4digitYear.ToString() & "\" & StudyRegion + "\" & "Company\" & Company & "\"
                    If Not Directory.Exists(CompanyPath) Then
                        'If Not Directory.Exists(CompanyPath) AndAlso Not _useMFiles Then
                        'MessageBox.Show("Company Directory has not been created yet. Please contact Console Support.")
                    End If
                    CompCorrPath = StudyDrive & new4digitYear.ToString() & "\" & StudyRegion & "\Company Correspondence\" & Company & "\"
                Else
                    CompCorrPath = String.Empty
                End If
            Catch exCompany As Exception
                db.WriteLog("SetPaths (1)", exCompany)
            End Try

            If new4digitYear >= 2015 Then
                TempPath = _profileConsoleTempPath
                IDRPath = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\Software\IDR\"
                DrawingPath = GeneralPath & "Drawings\"
                ClientAttachmentsPath = IDRPath & "Client Attachments\"
                TemplatePath = IDRPath & "Template\"
                TempPath = _profileConsoleTempPath
                PolishPath = IDRPath & "Polish\"
            ElseIf new4digitYear < 2014 Then
                IDRPath = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\Software\Validate\"
                DrawingPath = GeneralPath & "Drawings\"
                ClientAttachmentsPath = IDRPath & "Client Attachments\"
                TemplatePath = IDRPath & "Template\"
                TempPath = _profileConsoleTempPath ' "c:\users\" & UserName & "\Console\Temp\"
                PolishPath = IDRPath & "Polish\"
            Else ' = 2014
                T("SetPaths-2014")
                IDRPath = StudyDrive & Get4DigitYearFromCboStudy().ToString() & "\Software\IDR\"
                T("SetPaths-IDRPath = " & IDRPath)
                DrawingPath = GeneralPath & "Drawings\"
                T("SetPaths-Drawigspath=" & DrawingPath)
                ClientAttachmentsPath = IDRPath & "Client Attachments\"
                T("SetPaths-CLinetAttachmentsPath=" & ClientAttachmentsPath)
                TemplatePath = IDRPath & "Template\"
                T("SetPaths-TemplatePath=" & TemplatePath)
                TempPath = _profileConsoleTempPath ' Path.GetTempPath() & StudyType & "\"
                T("SetPaths-TEmpPath=" & TempPath)
                PolishPath = IDRPath & "Polish\"
                T("SetPaths-PolishPath=" & PolishPath)
            End If
            'BuildDirectoriesTab()
        Catch setPathsEx As System.Exception
            db.WriteLog("SetPaths", setPathsEx)
        End Try

    End Sub
    Private Sub SetPathsLegacy()
        'Try
        '    Dim mRefNum As String
        '    StudyRegion = cboStudy.Text.Substring(0, 3)
        '    Coloc = GetCompanyName(GetRefNumCboText())
        '    Company = Company.Substring(0, Company.IndexOf("-") - 1)
        '    mRefNum = GetRefNumCboText()
        '    GeneralPath = StudyDrive & "General\" & StudyRegion & "\" & ParseRefNum(mRefNum, 1) & StudyRegion & "\"
        '    ProjectPath = StudyDrive & StudyYear & "\Project\"

        '    RefineryPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Refinery\" & ParseRefNum(mRefNum, 0) & "\"
        '    CorrPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Correspondence\" & mRefNum & "\"
        '    If Directory.Exists(StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\") Then
        '        CompCorrPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\"
        '    Else
        '        Directory.CreateDirectory(StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\")
        '        CompCorrPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\"
        '    End If
        '    CompanyPath = StudyDrive & StudyYear & "\" & StudyRegion + "\" & "Company\" & Company & "\"
        '    If StudyYear <> "2014" Then
        '        IDRPath = StudyDrive & StudyYear & "\Software\Validate\"
        '        DrawingPath = GeneralPath & "Drawings\"
        '        ClientAttachmentsPath = IDRPath & "Client Attachments\"
        '        TemplatePath = IDRPath & "Template\"
        '        If Not Directory.Exists("c:\users\" & UserName & "\Console") Then Directory.CreateDirectory("c:\users\" & UserName & "\Console")
        '        If Not Directory.Exists("c:\users\" & UserName & "\Console\temp") Then Directory.CreateDirectory("c:\users\" & UserName & "\Console\temp")
        '        TempPath = "c:\users\" & UserName & "\Console\Temp\"

        '        PolishPath = IDRPath & "Polish\"
        '    Else
        '        IDRPath = StudyDrive & StudyYear & "\Software\IDR\"
        '        DrawingPath = GeneralPath & "Drawings\"
        '        ClientAttachmentsPath = IDRPath & "Client Attachments\"
        '        TemplatePath = IDRPath & "Template\"
        '        TempPath = Path.GetTempPath() & StudyType & "\"

        '        PolishPath = IDRPath & "Polish\"
        '    End If
        '    BuildDirectoriesTab()
        'Catch ex As System.Exception
        '    Err.WriteLog("SetPaths", ex)
        'End Try

    End Sub
    'Method to remove specific tab from tabcontrol
    Private Sub RemoveTab(tabname As String)
        Dim tab As TabPage
        tab = ConsoleTabs.TabPages(tabname)
        ConsoleTabs.TabPages.Remove(tab)
    End Sub

    Private Function ParseRefNum(mRefNum As String, mode As Integer) As String
        Dim parsed As String = Utilities.GetRefNumPart(mRefNum, mode)
        Return parsed
    End Function

    'Method to save last refnum visited per Study for settings file
    Private Sub SaveRefNum(strStudy As String, strRefNum As String)

        For I = 0 To 8
            If StudySave(I) = strStudy Then
                RefNumSave(I) = strRefNum
            End If
        Next

    End Sub

#End Region

#Region "DATABASE LOOKUPS"

    Private Sub btnClippySearch_Click(sender As System.Object, e As System.EventArgs) Handles btnClippySearch.Click
        Dim da As SqlDataAdapter
        Dim conn As New SqlConnection(db.ConnectionString)
        Dim cmd As SqlCommand

        If chkSQL.Checked Then

            If txtClippySearch.Text.ToUpper.Contains("INSERT") Or txtClippySearch.Text.ToUpper.Contains("DELETE") Or txtClippySearch.Text.ToUpper.Contains("UPDATE") Or txtClippySearch.Text.ToUpper.Contains("DROP") Then
                lblError.Text = "CAN ONLY SELECT DATA - READ ONLY"
                Exit Sub
            End If
            cmd = New SqlCommand
            da = New SqlDataAdapter
            ds = New DataSet
            lblError.Text = ""
            Try
                Try
                    conn.Open()
                Catch ex As System.Exception
                    lblError.Text = ex.Message
                    Exit Sub
                End Try
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text
                txtClippySearch.Text = txtClippySearch.Text.ToUpper.Replace("SELECT *", "SELECT TOP 1000 *")
                cmd.CommandText = txtClippySearch.Text
                cmd.CommandTimeout = 15

                da.SelectCommand = cmd
                Me.Cursor = Cursors.WaitCursor
                da.Fill(ds)
                dgClippyResults.DataSource = ds.Tables(0)
            Catch ex As System.Exception
                MessageBox.Show(ex.Message, "SQL ERROR")
            End Try

            conn.Close()

        Else
            'DO CLIPPY SEARCH
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Function IsValidRefNum(refnum As String) As String
        Dim ret As String = ""
        Dim params = New List(Of String)

        params.Add("@RefNum/" + GetRefNumCboText())
        params.Add("@StudyYear/" + Get4DigitYearFromCboStudy().ToString())

        Dim c As DataSet = db.ExecuteStoredProc("Console." & "IsValidRefNum", params)
        If c.Tables.Count > 0 Then
            If c.Tables(0).Rows.Count > 0 Then
                ret = c.Tables(0).Rows(0)(0).ToString
            End If
        End If
        Return ret

    End Function

    Private Function FindString(lb As ComboBox, str As String) As Integer
        Dim count As Integer = 0
        For Each item As String In lb.Items
            If item.Contains(str) Then
                Return count
            End If
            count += 1
        Next
    End Function

    Private Sub UpdateConsultant(newConsultant As String)

        Dim rslt As DialogResult = MessageBox.Show("Are you sure you want to change the current consultant?", "Warning", MessageBoxButtons.YesNo)
        If rslt = System.Windows.Forms.DialogResult.Yes Then
            Dim params = New List(Of String)

            params.Add("RefNum/" + ParseRefNum(RefNum, 0))
            params.Add("Consultant/" + newConsultant.ToUpper()) ' cboConsultant.Text.Trim().ToUpper())
            Try
                db.ExecuteNonQuery("Console." & "UpdateConsultant", params)
            Catch ex As Exception
                MessageBox.Show("Consultant did not updated with error: " + ex.Message)
            End Try
        Else
            RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
            cboConsultant.Text = CurrentConsultant
            AddHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
        End If
    End Sub

    Private Function CheckRole(role As String, initials As String) As Boolean
        T("CheckRole")
        Dim params As List(Of String)
        params = New List(Of String)
        Dim roles As DataSet
        params.Add("UserID/" + initials)
        roles = db.ExecuteStoredProc("Console.GetRoles", params)
        For Each dr In roles.Tables(0).Rows
            Dim dbRole As String = dr("Role").ToString()
            If dbRole = role Then Return True
        Next
        Return False

    End Function

    Private Sub UpdateIssue(status As String)
        Dim Success As Boolean = False
        Dim CheckV2 As Boolean = False
        Dim params As List(Of String)
        params = New List(Of String)
        If Me.tvIssues.SelectedNode IsNot Nothing Then
            If tvIssues.SelectedNode.Text.ToUpper.Contains("V2") Then
                CheckV2 = True

                If CheckRole("CheckV2", UserName) Then
                    Success = True

                End If

            End If
            params.Clear()
            If Success Or Not CheckV2 Then
                Dim node As String = tvIssues.SelectedNode.Text.ToString.Trim
                params.Add("RefNum/" + RefNum.Trim())
                params.Add("IssueID/" & node.Substring(0, node.IndexOf("-") - 1).Trim)
                params.Add("IssueTitle/" & Utilities.DropQuotes(txtIssueName.Text.Trim()))
                params.Add("IssueText/" & Utilities.DropQuotes(txtDescription.Text.Trim()))
                params.Add("Status/" & status)
                params.Add("UpdatedBy/" & UserName)
                Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateIssue2", params)
            Else
                RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
                tvIssues.SelectedNode.Checked = Not tvIssues.SelectedNode.Checked
                AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
                MessageBox.Show("You do not have approval rights for this Issue, see the Project Manager")
            End If
        End If
        UpdateCheckList()

    End Sub

    Public Function GetMainConsultant(rnum As String) As String
        Dim params As New List(Of String)

        params.Add("RefNum/" & ParseRefNum(rnum, 0))
        ds = db.ExecuteStoredProc("Console.GetMainConsultant", params)

        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0).ToString().Trim() & " - " & ds.Tables(0).Rows(0)(1).ToString() & "m"
        End If
        Return ""
    End Function

    Public Sub TimeSummary_Load()

        Dim params As New List(Of String)
        params.Add("RefNum/" & ParseRefNum(GetRefNumCboText(), 0))
        ds = db.ExecuteStoredProc("Console.GetTimeSummary", params)
        dgSummary.DataSource = ds.Tables(0)

    End Sub

    Public Sub TimeHistory_Load()

        Dim params As New List(Of String)
        params.Add("RefNum/" & ParseRefNum(GetRefNumCboText(), 0))
        ds = db.ExecuteStoredProc("Console.GetTimeHistory", params)
        dgHistory.DataSource = ds.Tables(0)

    End Sub

    Private Function FillRefNums() As Integer
        T("FillRefNums")
        Dim rns As List(Of String)
        rns = GetRefNumsByConsultant()

        For Each r In rns
            cboRefNum.Items.Add(r)
        Next

        cboRefNum.Items.Add("--------------------")

        Dim params As List(Of String)
        params = New List(Of String)
        params.Add("StudyYear/" + Get4DigitYearFromCboStudy.ToString())
        params.Add("Study/" + GetStudyRegionFromCboStudy())

        ds = db.ExecuteStoredProc("Console." & "GetRefNums", params)
        Dim row As DataRow 'Represents a row of data in Systems.data.datatable
        For Each row In ds.Tables(0).Rows
            cboRefNum.Items.Add(row("RefNum").ToString.Trim)
            cboCompany.Items.Add(row("CoLoc").ToString.Trim)
        Next
        Return ds.Tables(0).Rows.Count

    End Function

    'Method to retrieve and populate consultants drop down box
    Private Sub GetConsultants()
        T("GetConsultants")
        RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
        Dim year As String = Get2DigitYearFromCboStudy().ToString()
        If Not year.StartsWith("20") Then year = "20" + year
        Dim sql As String = "SELECT DISTINCT CONSULTANT FROM DBO.TSORT WHERE STUDYYear = " + year + " ORDER BY CONSULTANT ASC"
        db.SQL = sql
        Dim consultants As DataSet = db.Execute()
        db.SQL = String.Empty
        cboConsultant.Items.Clear()

        If Not IsNothing(consultants) AndAlso consultants.Tables.Count = 1 AndAlso consultants.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In consultants.Tables(0).Rows
                cboConsultant.Items.Add(row(0).ToString())
            Next
            'cboConsultant.SelectedIndex
        End If

        Dim params As List(Of String)
        Try
            params = New List(Of String)

            params.Add("StudyYear/" + year)
            params.Add("Study/" + Study)
            params.Add("RefNum/" + GetRefNumCboText())
            ds = db.ExecuteStoredProc("Console." & "GetConsultants", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    CurrentConsultant = ds.Tables(0).Rows(0).Item("Consultant").ToString.ToUpper.Trim
                End If
            End If
            'RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
            cboConsultant.Text = CurrentConsultant
            'AddHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
        Catch ex As System.Exception
            db.WriteLog("GetConsultants", ex)
        Finally
            AddHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
        End Try

    End Sub

    'Function to retrieve Company name by RefNum
    Private Function GetCompanyName(str As String) As String
        T("GetCompanyName")
        'Get Company Name
        Dim params As List(Of String)
        Try
            params = New List(Of String)
            params.Add("RefNum/" + str)
            ds = db.ExecuteStoredProc("Console." & "GetCompanyName", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then Return ds.Tables(0).Rows(0)("CoLoc").ToString.Trim
            End If

            ClearFields()
            Return String.Empty
        Catch ex As System.Exception
            db.WriteLog("GetCompanyName", ex)
            Return String.Empty
        End Try

    End Function

    'If exists, this will retrieve Interim Contact info
    Private Sub GetInterimContactInfo()
        T("GetInterimContactInfo")
        InterimContact = New Contacts()
        Dim params As New List(Of String)

        'Check for Interim Contact Info
        params = New List(Of String)
        params.Add("RefNum/" + GetRefNumCboText())
        ds = db.ExecuteStoredProc("Console." & "GetInterimContactInfo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                InterimContact.FirstName = ds.Tables(0).Rows(0).Item("FirstName").ToString
                InterimContact.LastName = ds.Tables(0).Rows(0).Item("LastName").ToString
                InterimContact.Email = ds.Tables(0).Rows(0).Item("Email").ToString
                InterimContact.Phone = ds.Tables(0).Rows(0).Item("Phone").ToString
            End If
        Else
            InterimContact.Clear()
        End If

    End Sub

    Public Sub GetPrimaryClientCoordinator()
        lblPCCEmail.Text = ""
        lblPCCName.Text = ""
        Dim params As New List(Of String)
        Try
            'Check for Interim Contact Info
            params = New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            ds = db.ExecuteStoredProc("Console." & "GetPrimaryCoordinator", params)
            Dim PCC As String
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    PCC = ds.Tables(0).Rows(0).Item("PCC").ToString
                    lblPCCName.Text = _tInfo.ToTitleCase(Utilities.GetConsultantName(db, PCC))
                    lblPCCEmail.Text = PCC & IIf(PCC = "", "", "@solomononline.com")

                End If
            End If
        Catch ex As System.Exception
            db.WriteLog("GetPrimaryClientCoordinator", ex)
        End Try
    End Sub

    Private Sub GetPricingContactInfo()
        lblPricingContact.Text = ""
        lblPricingContactEmail.Text = ""
        lblPricingPhone.Text = ""
        Dim params As New List(Of String)
        Try
            'Check for Interim Contact Info
            params = New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            ds = db.ExecuteStoredProc("Console." & "GetPricingContact", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblPricingContact.Text = ds.Tables(0).Rows(0).Item("PricingName").ToString
                    lblPricingContactEmail.Text = ds.Tables(0).Rows(0).Item("PricingEmail").ToString
                    lblPricingPhone.Text = ds.Tables(0).Rows(0).Item("PricingPhone").ToString
                End If
            End If
        Catch ex As System.Exception
            db.WriteLog("GetPricingContactInfo", ex)
        End Try
    End Sub

    'Method to retrieve Company Contact Information
    Private Sub GetCompanyContactInfo()
        T("GetCompanyContactInfo")
        Try
            CompanyContact = New Contacts()
            Dim row As DataRow
            Dim params As New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            params.Add("ContactType/COORD")
            params.Add("StudyYear/") 'this arg isn't used in the proc so OK if it's blank
            ds = db.GetCompanyContacts(params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    'cboConsultant.SelectedItem = CurrentConsultant

                    CompanyContact.FirstName = row("FirstName").ToString
                    CompanyContact.LastName = row("LastName").ToString
                    CompanyContact.Email = row("Email").ToString
                    CompanyContact.Phone = row("Phone").ToString
                    CompanyContact.AltFirstName = row("AltFirstName").ToString
                    CompanyContact.AltLastName = row("AltLastName").ToString
                    CompanyContact.AltEmail = row("AltEmail").ToString
                    CompanyContact.Fax = row("Fax").ToString
                    CompanyContact.MailAdd1 = row("MailAddr1").ToString
                    CompanyContact.MailAdd2 = row("MailAddr2").ToString
                    CompanyContact.MailAdd3 = row("MailAddr3").ToString
                    CompanyContact.MailCity = row("MailCity").ToString
                    CompanyContact.MailState = row("MailState").ToString
                    CompanyContact.MailZip = row("MailZip").ToString
                    CompanyContact.MailCountry = row("MailCountry").ToString
                    CompanyContact.StreetAdd1 = row("StrAddr1").ToString
                    CompanyContact.StreetAdd2 = row("StrAddr2").ToString
                    CompanyContact.StreetAdd3 = row("StrAdd3").ToString
                    CompanyContact.StreetCity = row("StrCity").ToString
                    CompanyContact.StreetState = row("StrState").ToString
                    CompanyContact.StreetZip = row("StrZip").ToString
                    CompanyContact.StreetCountry = row("StrCountry").ToString
                    CompanyContact.Title = row("JobTitle").ToString
                    CompanyContact.Password = row("CompanyPassword").ToString.Trim
                    CompanyPassword = CompanyContact.Password
                    CompanyContact.SendMethod = row("CompanySendMethod").ToString
                    'txtSendFormat.Text = CompanyContact.SendMethod
                    'txtCorrNotes.Text = row("Comment").ToString
                    SANumber = row("SANumber").ToString

                End If

            Else
                CompanyContact.Clear()
            End If
            'If CompanyPassword.Length = 0 Then
            '    CompanyContact.Password = GetCompanyPassword(GetRefNumCboText()).Trim

            'End If
        Catch ex As System.Exception
            db.WriteLog("GetCompanyContactInfo", ex)
        End Try

    End Sub

    Private Function GetCompanyPassword(RefNum As String) As String
        Dim params As New List(Of String)
        Dim pw As String = String.Empty
        params.Add("RefNum/" + RefNum)
        ds = db.GetCompanyPassword(params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                pw = ds.Tables(0).Rows(0)("companypassword")
            End If
        End If
        Return pw.Trim()
    End Function

    Private Sub GetAltCompanyContactInfo()
        T("GetAltCompanyContactInfo")
        Try
            AltCompanyContact = New Contacts()
            Dim row As DataRow
            Dim params As New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            params.Add("ContactType/ALT")
            params.Add("StudyYear/" + Get4DigitYearFromCboStudy().ToString()) ' NOTE: the proc calls for this param but never uses it.
            ds = db.GetCompanyContacts(params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    cboConsultant.SelectedItem = CurrentConsultant

                    AltCompanyContact.FirstName = row("FirstName").ToString
                    AltCompanyContact.LastName = row("LastName").ToString
                    AltCompanyContact.Email = row("Email").ToString
                    AltCompanyContact.Phone = row("Phone").ToString
                    AltCompanyContact.AltFirstName = row("AltFirstName").ToString
                    AltCompanyContact.AltLastName = row("AltLastName").ToString
                    AltCompanyContact.AltEmail = row("AltEmail").ToString
                    AltCompanyContact.Fax = row("Fax").ToString
                    AltCompanyContact.MailAdd1 = row("MailAddr1").ToString
                    AltCompanyContact.MailAdd2 = row("MailAddr2").ToString
                    AltCompanyContact.MailAdd3 = row("MailAddr3").ToString
                    AltCompanyContact.MailCity = row("MailCity").ToString
                    AltCompanyContact.MailState = row("MailState").ToString
                    AltCompanyContact.MailZip = row("MailZip").ToString
                    AltCompanyContact.MailCountry = row("MailCountry").ToString
                    AltCompanyContact.StreetAdd1 = row("StrAddr1").ToString
                    AltCompanyContact.StreetAdd2 = row("StrAddr2").ToString
                    AltCompanyContact.StreetAdd3 = row("StrAdd3").ToString
                    AltCompanyContact.StreetCity = row("StrCity").ToString
                    AltCompanyContact.StreetState = row("StrState").ToString
                    AltCompanyContact.StreetZip = row("StrZip").ToString
                    AltCompanyContact.StreetCountry = row("StrCountry").ToString
                    AltCompanyContact.Title = row("JobTitle").ToString
                    AltCompanyContact.Password = row("CompanyPassword").ToString.Trim
                    CompanyPassword = AltCompanyContact.Password
                    AltCompanyContact.SendMethod = row("CompanySendMethod").ToString
                    SANumber = row("SANumber").ToString


                Else
                    AltCompanyContact.Clear()
                End If
            End If
        Catch ex As System.Exception
            db.WriteLog("GetAltCompanyContactInfo", ex)
        End Try
    End Sub

    'Method to retrieve Refinery Contact information
    Private Sub GetContacts()
        T("GetContacts")
        Dim row As DataRow
        Dim params As New List(Of String)
        Try
            params.Add("RefNum/" + GetRefNumCboText())
            ds = db.GetContacts(params)

            RefineryContact = New Contacts()
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    RefineryContact.FullName = row("CoordName").ToString
                    RefineryContact.AltFullName = row("RefName2").ToString
                    RefineryContact.Title = row("RefTitle").ToString
                    RefineryContact.Email = row("CoordEmail").ToString
                    RefineryContact.AltEmail = row("RefEmail2").ToString
                    RefineryContact.Phone = row("CoordPhone").ToString
                    RefineryContact.AltPhone = row("RefPhone2").ToString
                    RefineryContact.MaintMgrName = row("MaintMgrName").ToString
                    RefineryContact.MaintEmail = row("MaintMgrEmail").ToString
                    RefineryContact.OpsMgrName = row("OpsMgrName").ToString
                    RefineryContact.OpsEmail = row("OpsMgrEmail").ToString
                    RefineryContact.TechMgrName = row("TechMgrName").ToString
                    RefineryContact.TechEmail = row("TechMgrEmail").ToString
                    RefineryContact.RefMgrName = row("RefMgrName").ToString
                    RefineryContact.RefEmail = row("RefMgrEmail").ToString
                    RefineryContact.RefinerySPD = row("location").ToString
                    RefineryContact.RefineryCountry = row("country").ToString
                Else
                    RefineryContact.Clear()
                End If
            End If

            GetPrimaryClientCoordinator()
        Catch ex As System.Exception
            db.WriteLog("GetContacts", ex)
        End Try
    End Sub

    'Method to retrieve all infomation per Ref Num
    Public Sub GetRefNumRecord(mRefNum As String)
        T("GetRefNumRecord")
        Dim Level As String = mRefNum
        Try
            CompanyPassword = ""
            ConsoleTabs.Enabled = True
            If mRefNum = "" Then
                cboStudy.SelectedIndex = 1
            End If


            CurrentRefNum = mRefNum
            RefNum = mRefNum

            'If Refnum is lubes, change variable according to need
            If CurrentRefNum.Contains("LIV") Then
                LubRefNum = CurrentRefNum
                CurrentRefNum = CurrentRefNum.Replace("LIV", "LUB")
            End If
            If CurrentRefNum.Contains("LEV") Then
                LubRefNum = CurrentRefNum
                CurrentRefNum = CurrentRefNum.Replace("LEV", "LUB")
            End If
            Level = "1"
            StudyRegion = ParseRefNum(mRefNum, 2)
            Level = "2"
            Company = GetCompanyName(mRefNum)
            Level = "3"
            If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
            cboCompany.Text = Company

            cboRefNum.Text = CurrentRefNum
            Throw New Exception("Replace cboRefNum.Text = CurrentRefNum   or test it")

            'Main 
            Level = "4"
            IsInHolding()
            btnMain.Text = GetMainConsultant(GetRefNumCboText())
            'SetTabDirty(True)
            BuildCorrespondenceTab()
            BuildTab(CurrentTab)
            BuildTab("Contacts")
        Catch ex As System.Exception
            db.WriteLog("GetRefNumRecord: " & Level, ex)
        End Try

    End Sub

    'Retrieve all Refnums per Consultant, Study Type and Study Year for Consultant Tab


    'Routine to populate the Consultant Treeview with Refnums and outstanding issues

    Function R3Data(refnum As String) As String
        T("R3Data")
        Dim strTempCorrPath As String = Nothing
        Dim lstVFNumbers As New List(Of String)
        Dim dteFileDateLatest As Date
        Dim dteFileDate As Date
        Dim strDirResult As String
        Dim blnAlready As Boolean
        Dim I As Integer
        Dim R3 As String = Nothing

        ' lstVFNumbers is a non-visible list control that I use to
        ' hold and sort the VF numbers.
        strTempCorrPath = BuildCorrPathFromRefnum(refnum, GetStudyRegionFromCboStudy(), GetStudyRegionFromCboStudy())
        strDirResult = Dir(strTempCorrPath & "VF*")
        Do While strDirResult <> ""
            I = 0
            blnAlready = False
            Do While I < lstVFNumbers.Count
                If lstVFNumbers(I) = Mid(strDirResult, 3, 1) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            If Not blnAlready Then
                If IsNumeric(Mid(strDirResult, 3, 1)) Then
                    lstVFNumbers.Add(Mid(strDirResult, 3, 1))
                End If
            End If
            dteFileDate = FileDateTime(strTempCorrPath & strDirResult)
            If dteFileDate > dteFileDateLatest Then
                dteFileDateLatest = dteFileDate
            End If
            strDirResult = Dir()
        Loop

        Dim intHighVF As Integer
        If lstVFNumbers.Count > 0 Then
            If lstVFNumbers(lstVFNumbers.Count - 1) <> "" Then
                intHighVF = lstVFNumbers(lstVFNumbers.Count - 1)
                strDirResult = Dir(strTempCorrPath & "VR" & CStr(intHighVF) & "*")
                If strDirResult = "" Then
                    R3 = intHighVF & "      " & DateDiff(DateInterval.Day, dteFileDateLatest, Now) & "       " & Format(dteFileDateLatest, "dd-MMM-yy")
                Else
                    R3 = "        "
                End If
            End If
        Else
            R3 = "        "
        End If

        Return R3
    End Function

    Private Function BuildCorrPathFromRefnum(thisRefnum As String, fourDigitYear As Integer, thisStudyRegion As String) As String
        T("BuildCorrPath")
        'NOTE: this is for Plant Correspondence.
        thisRefnum = thisRefnum.Trim()
        T("BuildCorrPath-1")
        Dim path As String = Nothing
        T("BuildCorrPath-2")
        Dim proposedPath As String = StudyDrive & fourDigitYear.ToString() & "\" & thisStudyRegion & "\Correspondence\" & thisRefnum & "\"
        T("BuildCorrPath-3")
        T("proposedPath=" & proposedPath)
        If Directory.Exists(proposedPath) Then
            T("-BuildCorrPath-exists")
            path = proposedPath
            T("-BuildCorrPath path=proposedPath")
        End If
        T("BuildCorrPath-return")
        Return path
        T("exiting BuildCorrPath")
    End Function
    Private Function BuildCorrPathFromCompany(coloc As String, fourDigitYear As Integer, thisStudyRegion As String) As String
        T("BuildCorrPath-1")
        Dim path As String = Nothing
        Dim sql As String = "select refnum from dbo.TSort where coloc = '" & coloc.Trim() & "' and studyYear = " & fourDigitYear & " and study = '" & thisStudyRegion & "' "
        db.SQL = sql
        Dim ds As DataSet = db.Execute()
        db.SQL = String.Empty
        If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim thisRefnum = ds.Tables(0).Rows(0)("Refnum").ToString().Trim()
            T("BuildCorrPath-2")
            Dim proposedPath As String = StudyDrive & fourDigitYear.ToString() & "\" & thisStudyRegion & "\Correspondence\" & thisRefnum & "\"
            T("BuildCorrPath-3")
            T("proposedPath=" & proposedPath)
            If Directory.Exists(proposedPath) Then
                T("-BuildCorrPath-exists")
                path = proposedPath
                T("-BuildCorrPath path=proposedPath")
            End If
        End If
        T("BuildCorrPath-return")
        Return path
        T("exiting BuildCorrPath")
    End Function

    Private Function BuildDataFolderPath(thisRefnum As String, fourDigitYear As Integer, thisStudyRegion As String) As String
        T("BuildDataFolderPath")
        'NOTE: this is for Plant Correspondence.
        thisRefnum = thisRefnum.Trim()
        Dim path As String = Nothing
        Dim proposedPath As String = StudyDrive & fourDigitYear.ToString() & "\" & thisStudyRegion & "\Refinery\" & thisRefnum & "\"
        If Directory.Exists(proposedPath) Then
            T("BuildDataFolderPath - path exists")
            T("-proposedPath=" & proposedPath)
            path = proposedPath
        End If
        Return path
    End Function

#End Region

#Region "Company/Refnum Navigation"

    Private Sub cboCompany_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Exit Sub
    End Sub
    Private Sub cboCompany_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboCompany.SelectionChangeCommitted
        Dim plantChangedTo As String = cboCompany.SelectedItem
        'CompanyCboChangedRoutine(plantChangedTo)
        Me.Cursor = Cursors.WaitCursor
        ChangedRefnumOrCompany("", plantChangedTo)
        Me.Cursor = Cursors.Default
        'refreshForm()
    End Sub
    Private Sub CompanyCboChangedRoutine(newCompany As String)
        T("CompanyCboChangedRoutine")
        ClearPasswordsAndConsultant()
        'ConsoleTabs.TabPages.Remove(tabDD)
        'ConsoleTabs.TabPages.Remove(tabSS)
        Dim results As YearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangePlant(newCompany)
        ChangeColocRefnum(results)

        ConsoleTabs.Enabled = True
        Company = GetCompanyName(newCompany)
        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        StudyRegion = ParseRefNum(results.SelectedRefNum, 2)
        SetPaths()
        Dim year As String = Get4DigitYearFromCboStudy().ToString()
        PopulateConsultantDropdown(GetRefNumCboText(), StudyRegion, year)
        Try
            '_mfilesFailed = False
            BuildCorrespondenceTab() 'calls  SetSummaryFileButtons()
        Catch ex As Exception
            'If ex.Message.Contains("Unable to find Benchmarking Participant") Then
            '    _mfilesFailed = True
            'End If
            'MsgBox(ex.Message + Environment.NewLine + Environment.NewLine + "Please notify your MFiles Administrator to set up this Benchmarking Participant in MFiles.")
        End Try
        'If _mfilesFailed Then
        '    MFilesFailedRoutine()
        'Else
        '    UnhideMFilesTabs()
        'End If

        BuildContactTab()
        BuildNotesTab()
        BuildCheckListTab("")
        cboCheckListView.SelectedIndex = 0
        CheckPreliminaryPricing()
        CheckFuelsLubes()
        ChangedRefNumRoutine(GetRefNumCboText())  'SB NEW
    End Sub

    Private Sub cboCompany_KeyDown(sender As Object, e As KeyEventArgs) Handles cboCompany.KeyDown
        Dim company As String = CurrentCompany

        If e.KeyCode = Keys.Enter Then
            Dim plantChangedTo As String = cboCompany.SelectedItem
            Me.Cursor = Cursors.WaitCursor
            'CompanyCboChangedRoutine(plantChangedTo)
            'RefreshMFilesCollections(0)
            'RefreshMFilesCollections(1)
            'RefreshMFilesCollections(2)
            ChangedRefnumOrCompany("", plantChangedTo)
            'KeyEntered = True
            'For Each i In cboCompany.Items
            '    If (i.StartsWith(cboCompany.Text.ToUpper)) Then
            '        cboCompany.SelectedItem = i
            '        CompanyCboChangedRoutine(cboCompany.Text)
            '        Exit For
            '    End If
            'Next
        End If

        'legacy:
        'Dim rmRefNum As String = CurrentRefNum

        'If e.KeyCode = Keys.Enter Then
        '    KeyEntered = True
        '    For Each i In cboCompany.Items
        '        If (i.StartsWith(cboCompany.Text.ToUpper)) Then
        '            cboCompany.SelectedItem = i
        '            Exit For
        '        End If
        '    Next
        'End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cboRefNum_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboRefNum.SelectionChangeCommitted
        '        CboRefNumSelectionChangeCommittedRoutine()
        T("cboRefNum_SelectionChangeCommitted")
        Me.Cursor = Cursors.WaitCursor
        RefNum = GetRefNumCboText()
        ChangedRefnumOrCompany(GetRefNumCboText(), "")
        'check verify send status
        VerifySendButtonColor()

        Me.Cursor = Cursors.Default

    End Sub
    'Check verify send status
    Private Sub VerifySendButtonColor()
        Dim verifysendStatus As Boolean
        verifysendStatus = Utilities.checkVerifySend
        If verifysendStatus Then
            btnVerifySend.BackColor = Color.Green
        Else
            btnVerifySend.BackColor = Color.Red
        End If
    End Sub
    Private Sub CboRefNumSelectionChangeCommittedRoutine()
        T("CboRefNumSelectionChangeCommittedRoutine")
        ClearAllControls()
        Dim newRefNum As String = cboRefNum.SelectedItem
        If newRefNum.Length > 0 Then
            UpdateLastStudyRefnum(cboStudy.Text, newRefNum)
        End If
        Dim refNumChangedTo As String = cboRefNum.SelectedItem
        Dim results As YearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangeRefnum(refNumChangedTo)
        ChangeColocRefnum(results)

        'legacy method
        'RefNumChanged(RefNum)
        'try this instead, has less duplkcation
        ChangedRefNumRoutine(newRefNum)
    End Sub
    Private Sub ChangedRefNumRoutine(newRefNum As String)
        T("ChangedRefNumRoutine")
        ClearPasswordsAndConsultant()
        mRefNum = newRefNum
        SetTabDirty(True)
        'GetRefNumRecord(mRefNum) is this:
        CompanyPassword = ""
        ConsoleTabs.Enabled = True
        CurrentRefNum = newRefNum
        RefNum = newRefNum
        StudyRegion = ParseRefNum(newRefNum, 2)
        IsInHolding() 'If File.Exists(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xls") Then lblValidationStatus.Text = "In Holding"
        'btnMain.Text = GetMainConsultant(newRefNum, cboStudy.Text, _studyType)
        'cboConsultant.Text = GetMainConsultant(newRefNum, cboStudy.Text, _studyType)
        Dim year As String = Get2DigitYearFromCboStudy().ToString()
        PopulateConsultantDropdown(GetRefNumCboText(), StudyRegion, "20" + year)

        SetPaths() 'SetPaths calls BuildDirectoriesTab() too
        'BuildTab(CurrentTab) instead of calling this, call indivicually all the tabs I'm showing
        Try
            '_mfilesFailed = False
            BuildCorrespondenceTab() 'calls  SetSummaryFileButtons()
        Catch ex As Exception
            'If ex.Message.Contains("Unable to find Benchmarking Participant") Then
            '    _mfilesFailed = True
            'End If
            'MsgBox(ex.Message + Environment.NewLine + Environment.NewLine + "Please notify your MFiles Administrator to set up this Benchmarking Participant in MFiles.")
        End Try
        'If _mfilesFailed Then
        '    MFilesFailedRoutine()
        'Else
        '    UnhideMFilesTabs()
        'End If

        BuildTabs()
        BuildContactTab()
        BuildNotesTab()
        BuildContinuingIssuesTab()
        BuildCheckListTab("")
        cboCheckListView.SelectedIndex = 0
        CheckPreliminaryPricing()
        Company = GetCompanyName(GetRefNumCboText())
        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)

        btnValidate.Enabled = True
        BuildSummarySection()
        'If Not _useMFiles Then
        Dim pnDocPath As String = CorrPath & "PN_" & cboCompany.Text & ".doc"
        If File.Exists(pnDocPath) OrElse File.Exists(pnDocPath + "x") Then
            btnCreatePN.Text = "Open PN File"
            txtNotes.ReadOnly = True
            btnCreatePN.Text = "Open PN File"
        Else
            txtNotes.ReadOnly = False
            btnCreatePN.Text = "Create Final PN File"
        End If
        'Else
        '    Try
        '        If Convert.ToBoolean(_docsAccess.DocExistsInMFiles("PN_", GetRefNumCboText(), GetRefNumCboText(), True)) = True Then
        '            txtNotes.ReadOnly = True
        '            btnUnlockPN.Visible = False
        '            btnCreatePN.Text = "Open PN File"
        '        Else
        '            txtNotes.ReadOnly = False
        '            If Not _useMFiles Then btnUnlockPN.Visible = True
        '            btnCreatePN.Text = "Create Final PN File"
        '        End If


        'Catch ex As Exception
        '        MsgBox(ex.Message)
        '    End Try
        'End If
        CheckFuelsLubes()
    End Sub
    Private Sub ClearPasswordsAndConsultant()
        T("ClearPasswordsAndConsultant")
        cboConsultant.Items.Clear()
        cboConsultant.Text = String.Empty
        btnReturnPW.Text = String.Empty
        btnCompanyPW.Text = String.Empty
    End Sub
    Private Sub PopulateConsultantDropdown(thisRefnum As String, thisStudy As String, studyYear As String)
        T("PopulateConsultantDropdown")
        cboConsultant.Items.Clear()
        cboConsultant.Text = String.Empty
        'db.SQL = "select distinct consultant, refnum from tsort where StudyYear = '" + StudyYear + "' and study = ( Select top 1 (select distinct study from tsort)) order by consultant asc"
        db.SQL = "select consultant, refnum from tsort where StudyYear = '" + studyYear + "' and study = '" + thisStudy + "' and consultant is not null order by consultant asc"
        ds = db.Execute()
        If ds.Tables.Count > 0 Then
            Dim list As String = String.Empty
            For Each row As DataRow In ds.Tables(0).Rows
                If Not IsNothing(row("Consultant")) Then
                    Dim cons As String = row("Consultant").ToString().ToUpper().Trim()
                    If Not list.Contains(cons) Then
                        cboConsultant.Items.Add(cons)
                    End If

                    If thisRefnum.Trim().Contains("LUB") Then
                        Dim test As String = thisRefnum.Replace("LUB", "LEV")
                        If test.Trim() = row("RefNum").ToString.Trim() Then
                            cboConsultant.Text = cons
                        Else
                            test = thisRefnum.Replace("LUB", "LIV")
                            If test.Trim() = row("RefNum").ToString.Trim() Then
                                cboConsultant.Text = cons
                            End If
                        End If
                    Else
                        If row("RefNum").ToString.Trim() = thisRefnum.Trim() Then
                            cboConsultant.Text = cons
                        End If
                    End If
                    list += cons + "|"
                End If
            Next
        End If
    End Sub
    Private Sub cboRefNum_KeyDown_1(sender As Object, e As KeyEventArgs) Handles cboRefNum.KeyDown
        'Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            Me.Cursor = Cursors.WaitCursor
            Dim refNumChangedTo As String = cboRefNum.SelectedItem
            'RefreshMFilesCollections(0)
            'RefreshMFilesCollections(1)
            'RefreshMFilesCollections(2)
            ChangedRefnumOrCompany(refNumChangedTo, "")
            'CboRefNumSelectionChangeCommittedRoutine()

            'KeyEntered = True
            ''legacy method
            ''RefNumChanged(rmRefNum)
            ''try this instead, has less duplkcation
            'ChangedRefNumRoutine(rmRefNum)
            'cboRefNum.SelectedIndex = cboRefNum.FindString(GetRefNumCboText().ToUpper)
            'If cboRefNum.SelectedIndex = -1 Then
            '    cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(rmRefNum, 2) & ParseRefNum(rmRefNum, 3))
            '    'ChangedStudy(rmRefNum)
            'End If

        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnFuelLube_Click(sender As System.Object, e As System.EventArgs) Handles btnFuelLube.Click
        Dim StudyType As String = ParseRefNum(btnFuelLube.Text, 2).Trim
        Dim StudyYear As String = ParseRefNum(btnFuelLube.Text, 3).Trim.Substring(0, 2)
        ReferenceNum = btnFuelLube.Text

        RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
        cboStudy.SelectedIndex = cboStudy.FindString(StudyType & StudyYear)
        AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged

        'ChangedStudy(ReferenceNum)
        CboStudyChangedRoutine(True)
        GetRefNums(GetRefNumCboText())
        CheckFuelsLubes()
        If cboConsultant.Text.Trim().Length < 1 Then
            PopulateConsultantDropdown(GetRefNumCboText(), cboStudy.Text.Substring(0, 3), Get4DigitYearFromCboStudy.ToString())
        End If
    End Sub

#End Region

#Region "Post Navigation"

    Private Sub PostNavigation(ByVal Refnum As String, ByVal RefnumInit As String)
        T("PostNavigation")
        GetARefNumRecord(GetRefNumCboText())
        Company = cboCompany.Text
        BuildCorrespondenceTab()
        BuildTab("Contacts")
        CheckPreliminaryPricing()

        'ChangedStudy(ReferenceNum)
        GetRefNums(GetRefNumCboText())
        CheckFuelsLubes()

    End Sub

    'Method to retrieve all infomation per Ref Num
    Public Sub GetARefNumRecord(mRefNum As String)
        T("GetARefNumRecord")
        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum

        'If Refnum is lubes, change variable according to need
        If CurrentRefNum.Contains("LIV") Then
            LubRefNum = CurrentRefNum
            CurrentRefNum = CurrentRefNum.Replace("LIV", "LUB")
        End If
        If CurrentRefNum.Contains("LEV") Then
            LubRefNum = CurrentRefNum
            CurrentRefNum = CurrentRefNum.Replace("LEV", "LUB")
        End If
        Company = GetCompanyName(mRefNum)
        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        cboCompany.Text = Company
        StudyRegion = ParseRefNum(mRefNum, 2)
        cboRefNum.Text = CurrentRefNum
        Throw New Exception("Test or replace cboRefNum.Text = CurrentRefNum")
        SetPaths()
        btnMain.Text = GetMainConsultant(GetRefNumCboText())

    End Sub

    Public Sub RefNumChanged(Optional mRefNum As String = "")
        T("RefNumChanged")
        mRefNum = GetRefNumCboText()
        Dim pattern As String = "^\d{1,3}[A-Za-z]{3}\d{2}[A-Za-z]*$"

        Dim match As Match = Regex.Match(mRefNum, pattern, RegexOptions.IgnoreCase)
        If (mRefNum <> String.Empty And match.Success) Then
            'RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(mRefNum, 2) & ParseRefNum(mRefNum, 3).Substring(0, 2))
            'AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            'If Lubes, make button visible Fuel Lubes and Transmit Pricing
            If ParseRefNum(mRefNum, 2) = "LUB" Then
                'btnFLComp.Visible = True
                'btnTP.Visible = True
            End If

            RemoveHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged

            SetTabDirty(True)
            SetPaths()
            'cboRefNum.Text = mRefNum
            GetRefNumRecord(mRefNum)
            SaveRefNum(cboStudy.Text, mRefNum)
            'BuildTabs()
            AddHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
            If mRefNum.Length > 0 Then RefNum = mRefNum
            'If Not _useMFiles Then
            If File.Exists(CorrPath & "PN_" & cboCompany.Text & ".doc") Then
                btnCreatePN.Text = "Open PN File"
                txtNotes.ReadOnly = True
            Else
                txtNotes.ReadOnly = False
            End If
            'Else
            '    If Convert.ToBoolean(_docsAccess.DocExistsInMFiles("PN_", GetRefNumCboText(), GetRefNumCboText(), True)) = True Then
            '        txtNotes.ReadOnly = True
            '        btnUnlockPN.Visible = False
            '        btnCreatePN.Text = "Open PN File"
            '    Else
            '        txtNotes.ReadOnly = False
            '        If Not _useMFiles Then btnUnlockPN.Visible = True
            '    End If
            'End If
        End If
    End Sub

    'Private Sub ChangedStudy(Optional mRefNum As String = "")
    '    Dim year As Integer = 0
    '    If Int32.TryParse("20" + cboStudy.Text.Substring(3, 2), year) Then
    '        _useMFiles = year > 2015
    '    End If
    '    _docsAccess = New DocsFileSystemAccess(VerticalType.REFINING, _useMFiles)
    '    If mRefNum = "" Then
    '        For I = 0 To 16
    '            If StudySave(I) = cboStudy.Text Then
    '                GetRefNums(RefNumSave(I))
    '                Exit Sub
    '            End If
    '        Next
    '    Else
    '        GetRefNums(mRefNum)
    '    End If
    'End Sub

    'Method to populate refnums combo box
    Private Sub GetRefNums(Optional strRefNum As String = "")
        T("GetRefNums")
        Dim NoRefNums As Integer = 0
        Dim selectItem As String = cboStudy.SelectedItem
        CheckForCurrentStudyYear()
        If Not strRefNum Is Nothing Then
            cboRefNum.Items.Clear()
            cboCompany.Items.Clear()

            'Study RefNum Box
            NoRefNums = FillRefNums()

            If NoRefNums = 0 Then
                MessageBox.Show("There are no records for " & Get4DigitYearFromCboStudy().ToString() & " study.", "Not Found")
                RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
                cboStudy.Text = "NSA14"
                'StudyYear = "2014"
                NoRefNums = FillRefNums()
                AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            End If

            If Not strRefNum Is Nothing And strRefNum.Length > 5 Then
                cboRefNum.SelectedIndex = cboRefNum.FindString(strRefNum)
            Else
                If ReferenceNum Is Nothing Then
                    'RefNum = cboRefNum.Items(0).ToString
                    cboRefNum.SelectedIndex = 0
                Else
                    RefNum = ReferenceNum.Trim
                    cboRefNum.SelectedIndex = cboRefNum.FindString(ReferenceNum)
                End If
            End If
        End If
        CheckFuelsLubes()
    End Sub

    'Method to see if there is a combo refinery and show and populate the button
    Private Sub CheckFuelsLubes()
        btnFuelLube.Visible = False
        lblCombo.Visible = False

        Dim params As New List(Of String)
        Dim RNum As String = ParseRefNum(GetRefNumCboText(), 0).Trim
        params.Add("RefNum/" + RNum)
        ds = db.ExecuteStoredProc("Console.CheckForCombo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                btnFuelLube.Text = ds.Tables(0).Rows(0)(0).ToString.Trim
                btnFuelLube.Visible = True
                lblCombo.Visible = True

            Else
                btnFuelLube.Visible = False
                lblCombo.Visible = False
            End If
        Else
            btnFuelLube.Visible = False
            lblCombo.Visible = False

        End If

    End Sub

#End Region

#Region "UI METHODS"

    Private Sub btnSendPrelimPricing_Click(sender As System.Object, e As System.EventArgs) Handles btnSendPrelimPricing.Click
        Dim params As New List(Of String)
        Dim emails As New List(Of String)
        Dim nRow As DataRow
        Dim AddtoEmails As Boolean

        Dim PricingEmail As String = ""
        Dim RefineryEmail As String = ""

        params.Clear()
        params.Add("RefNum/" + GetRefNumCboText())

        ds = db.ExecuteStoredProc("Console.GetPricingContact", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                PricingEmail = ds.Tables(0).Rows(0)("PricingEmail").ToString
            End If
        End If

        ds = db.ExecuteStoredProc("Console.GetContactEmails", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then emails.Add(ds.Tables(0).Rows(0)("Email").ToString.ToUpper)
            For I = 1 To ds.Tables(0).Rows.Count - 1
                nRow = ds.Tables(0).Rows(I)
                AddtoEmails = True
                For Each mail In emails
                    If mail.Contains(nRow("Email").ToString.ToUpper) Then AddtoEmails = False
                Next
                If AddtoEmails Then emails.Add(nRow("Email").ToString.ToUpper)
            Next

            For Each em In emails
                RefineryEmail += em & ";"
            Next
        End If

        If PricingEmail <> "" Then
            SendPreliminaryPricing(PricingEmail, RefineryEmail)
        Else
            SendPreliminaryPricing(RefineryEmail, "")
        End If

    End Sub

    Private Function SendPreliminaryPricing(strTo As String, strCC As String) As Boolean

        Dim attachments As New List(Of String)
        Dim subj As String = ""
        Dim AddDays As String = ""
        Dim body As String = ""

        subj = "Preliminary Pricing for " & cboCompany.Text & " (" & RefNum & ")"
        Try
            If File.Exists(IDRPath & "PrelimPricing\" & Study & "\EmailText.txt") Then
                AddDays = InputBox("Number of working days to allow for response?", "How much time?", "10")
                subj = My.Computer.FileSystem.OpenTextFileReader(IDRPath & "PrelimPricing\" & Study & "\EmailText.txt").ReadLine
                body = My.Computer.FileSystem.ReadAllText(IDRPath & "\PrelimPricing\" & Study & "\EmailText.txt")
                body = body.Replace("~DueDate~", Utilities.ValFaxDateDue(Convert.ToInt32(AddDays)).ToString("dd-MMM-yy"))
                body = body.Replace("~Refinery~", Coloc)
                subj = subj.Replace("~Refinery~", Coloc)
            Else
                MessageBox.Show(IDRPath & "\PrelimPricing\" & Study & "\EmailText.txt is missing")
                body = ""
            End If

            Dim prelimPriceLetterPath As String = IDRPath & "PrelimPricing\" & Study & "\" & Get4DigitYearFromCboStudy().ToString() & " Preliminary Price Letter.PDF"
            If File.Exists(prelimPriceLetterPath) Then
                attachments.Add(prelimPriceLetterPath)
            Else
                MessageBox.Show(prelimPriceLetterPath & " is missing")
            End If

            Dim prelimPricingPPFolder As String = IDRPath & "PrelimPricing\" & Study & "\" '& GetRefNumCboText() & "_PP.xls*"

            Dim files As String()
            If Get4DigitYearFromCboStudy() < 2016 Then
                files = Directory.GetFiles(prelimPricingPPFolder, GetRefNumCboText() & "_PP.xls*")
            Else
                'new pattern is filename from db where cboCompany gets its data from
                Dim fileName As String = String.Empty
                Dim study As String = cboStudy.SelectedItem.ToString()
                Dim params As New List(Of String)
                Dim yearOnly As Integer = 0
                Dim yearText As String = String.Empty
                yearOnly = Int32.Parse(study.Substring(3, 2))
                yearText = yearOnly.ToString()
                If yearText.Length = 1 Then yearText = yearText.PadLeft(2, "0")
                params.Add("StudyYear/" + "20" + yearText)
                params.Add("Study/" + study.Substring(0, 3))
                If study.Length > 0 Then
                    Dim ds As DataSet = db.ExecuteStoredProc("Console." & "GetRefNums", params)
                    If IsNothing(ds) OrElse ds.Tables.Count < 1 OrElse ds.Tables(0).Rows.Count < 1 Then
                        Throw New System.Exception("No data available for finding refnums and colocs for year " + study)
                    Else
                        For Each row As DataRow In ds.Tables(0).Rows
                            If row("RefNum").ToString().Trim() = GetRefNumCboText() Then
                                fileName = row("CoLoc").ToString().Trim()
                                Exit For
                            End If
                        Next
                    End If
                    files = Directory.GetFiles(prelimPricingPPFolder, fileName & "_PP.xls*")
                End If
            End If



            If files.Length > 0 Then
                'If File.Exists(IDRPath & "\PrelimPricing\" & Study & "\" & cboCompany.Text & "_PP.xls") Then
                attachments.Add(files(0)) 'IDRPath & "\PrelimPricing\" & Study & "\" & cboCompany.Text & "_PP.xls")
            Else
                MessageBox.Show("_PP.xls*  is missing")
            End If

            If Study = "LUB" Then
                Dim prelimLubePricingPath As String = IDRPath & "PrelimPricing\" & Study & "\" & Get4DigitYearFromCboStudy().ToString() & " Lube Study Product Pricing Basis.PDF"
                If File.Exists(prelimLubePricingPath) Then
                    attachments.Add(prelimLubePricingPath)
                Else
                    MessageBox.Show(prelimLubePricingPath & " is missing")
                End If


                'new for Jamie 2017-04-24: put all attachments in encrypted zip

                Dim useZip As Boolean = False
                For Each f As String In attachments
                    Dim name As String = Utilities.ExtractFileName(f)
                    If name.ToLower().EndsWith(".xls") Or name.ToLower().EndsWith(".xlsx") Or name.ToLower().EndsWith(".doc") Or name.ToLower().EndsWith(".docx") _
                        Or name.ToLower().EndsWith(".pdf") Then
                        'leave as false
                    Else
                        useZip = True
                    End If
                Next

                'new for Jamie 2017-05-11: option to ### NOT ### put all attachments in encrypted zip
                If Not useZip Then
                    Dim result As MsgBoxResult = MsgBox("Click Yes to password protect the non-PDF attachments, click No to put them into an encrypted Zip file.", MsgBoxStyle.YesNo, "Console Prelim Pricing Email")
                    If result = vbYes Then
                        'leave it true
                    Else
                        useZip = True
                    End If
                End If

                If useZip Then
                    For Each f As String In attachments
                        Dim nameOnly As String = Utilities.ExtractFileName(f)
                        File.Copy(f, _profileConsoleTempPath & "SecureSend\" & nameOnly)
                    Next
                    Dim zipfile As List(Of String) = New List(Of String)
                    Dim nZip As String = ZipFiles()
                    If nZip.Length = 0 Then Exit Function
                    zipfile.Add(nZip)
                    SendOtherEmail(strTo, strCC, subj, body, True, Nothing, zipfile)
                Else
                    Dim localAttachments As New List(Of String)
                    For Each f As String In attachments
                        Dim nameOnly As String = Utilities.ExtractFileName(f)
                        Dim newPath As String = _profileConsoleTempPath & "SecureSend\" & nameOnly
                        'File.Copy(f, newPath,)
                        File.Copy(f, newPath, True)
                        If f.ToLower().EndsWith(".xls") Or f.ToLower().EndsWith(".xlsx") Then
                            If Not Utilities.ExcelPassword(newPath, btnCompanyPW.Text.Trim(), 0) Then
                                MsgBox("Unable to password-protect " & newPath & ". Please try again using the encrypted Zip file.")
                                Return False
                            End If
                        ElseIf f.ToLower().EndsWith(".doc") Or f.ToLower().EndsWith(".docx") Then
                            If Not Utilities.WordPassword(newPath, btnCompanyPW.Text.Trim(), 0) Then
                                MsgBox("Unable to password-protect " & newPath & ". Please try again using the encrypted Zip file.")
                                Return False
                            End If
                        ElseIf f.ToLower().EndsWith(".pdf") Then
                            'do nothing
                        Else
                            MsgBox("Unable to password-protect " & newPath & ". Please try again using the encrypted Zip file.")
                            Return False
                        End If
                        localAttachments.Add(newPath)
                    Next
                    SendOtherEmail(strTo, strCC, subj, body, True, Nothing, localAttachments)
                End If
                CleanFilesFromFolder(_profileConsoleTempPath & "SecureSend")
                CleanFilesFromFolder(_profileConsoleTempPath & "SecureSend\Zip")

                Return True
                'end new code


            End If
            Utilities.SendEmail(strTo, strCC, subj, body, attachments, True)
            Return True
        Catch ex As System.Exception
            db.WriteLog("SendPreliminaryPricing", ex)
            MessageBox.Show(ex.Message, "SendPreliminaryPricing")
            Return False
        End Try

    End Function
    Private Sub SendOtherEmail(emailTo As String, emailCC As String, subject As String, nbody As String, display As Boolean, Optional template As String = Nothing, Optional attaches As List(Of String) = Nothing)
        Dim body As String = nbody

        Try
            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    attaches(i) = attaches(i).ToString
                Next
            End If
            'Utilities.SendEmail(txtEmail.Text, txtCC.Text + IIf(chkPCC.Checked, txtPCC.Text, ""), subject, body, attaches, display)
            Utilities.SendEmail(emailTo, emailCC, subject, body, attaches, display)

            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    File.Delete(attaches(i).ToString)
                Next
            End If

        Catch ex As System.Exception
            db.WriteLog("MainConsole-SendOtherEmail", ex)
            MessageBox.Show(ex.Message, "Send Other Email")
        End Try
    End Sub
    Private Function ZipFiles() As String

        If Not Directory.Exists(_profileConsoleTempPath & "ZIP\") Then
            Directory.CreateDirectory(_profileConsoleTempPath & "ZIP\")
        End If
        ''Dim zipFileName As String = InputBox("Please enter the name of the zip file to create")
        ''If zipFileName.Trim().Length < 1 Then
        ''    MsgBox("No zip file named, quitting.")
        ''    Return String.Empty
        ''End If
        ''zipFileName = zipFileName.Replace(".zip", "") & ".zip"
        'Dim FN As String = Directory.GetFiles(_profileConsoleTempPath)(0).ToString
        'Dim nameOnly As String = Utilities.ExtractFileName(FN)
        'Dim ext As String = Utilities.GetFileExtension(nameOnly)
        'nameOnly = nameOnly.Remove(nameOnly.Length - ext.Length)
        'nameOnly += "ZIP"
        ''FN = FN.Substring(FN.LastIndexOf("\") + 1, FN.Length - FN.LastIndexOf("\") - 1)
        ''FN = FN.Replace(GetExtension(FN), "ZIP")


        'Dim zipFile As String = _profileConsoleTempPath & "ZIP\" & nameOnly
        'Dim notWordExcelDocMessage As String = String.Empty '"Because you have a file that is not a WORD or EXCEL document, you must send your email with an encrypted zip file. "
        'Select Case ext.ToUpper()
        '    Case "XLS", "XLSX", "DOC", "DOCX"
        '        notWordExcelDocMessage = String.Empty
        '    Case Else
        'End Select





        'Dim newFile = InputBox(notWordExcelDocMessage & vbCrLf & "Enter Zip filename: ", "Enter Filename", nameOnly)
        Dim newFile = InputBox("Enter Zip filename: ", "Enter Filename")
        If newFile.Length = 0 Then
            Return newFile
        End If
        newFile = _profileConsoleTempPath & "SecureSend\" & "ZIP\" & newFile & ".zip"
        Utilities.Zipfile(_profileConsoleTempPath & "SecureSend", GetCompanyPassword(GetRefNumCboText()), newFile)
        Return newFile

    End Function

    Private Sub tvIssues_AfterCheck(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterCheck

        Try
            _checklistNodeChecked = True
            Dim issueId As String = ParseIssueId(e.Node.Text).Trim()

            Dim results As RefiningChecklistResults =
            _checklistManager.GetResults(cboCheckListView.Text, "V", issueId, e.Node.Checked, Nothing)
            'RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
            RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            e.Node.Checked = results.IsChecked
            AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
            txtIssueID.Text = results.BoxesContent(0)
            txtIssueName.Text = results.BoxesContent(1)
            txtDescription.Text = results.BoxesContent(2)
            RebuildChecklist(db, GetRefNumCboText(), cboCheckListView.Text)
            ShadeChecklist(results)
        Catch ex As Exception
            MsgBox("Error in tvIssues_AfterCheck(): " & ex.Message)
        Finally
            ' AddHandler tvIssues.AfterSelect, AddressOf tvIssues_AfterSelect
        End Try

    End Sub

    Private Sub tvIssues_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterSelect
        If _checklistNodeChecked Then
            _checklistNodeChecked = False
            If txtIssueID.Text.Trim().Length > 0 Then
                txtIssueID.ReadOnly = True
            End If
            Exit Sub
        End If
        Dim issueId As String = ParseIssueId(tvIssues.SelectedNode.Text).Trim()

        Dim v As String = "VIEW"
        'If radChecklistAddNew.Checked Then v = "ADD"

        radChecklistAddNew.Checked = False
        radChecklistEditIssue.Checked = True

        Dim results As RefiningChecklistResults =
        _checklistManager.GetResults(cboCheckListView.Text, v, issueId, Nothing, Nothing)
        txtIssueID.Text = results.BoxesContent(0)
        txtIssueName.Text = results.BoxesContent(1)
        txtDescription.Text = results.BoxesContent(2)
        ShadeChecklist(results)
        If txtIssueID.Text.Trim().Length > 0 Then
            txtIssueID.ReadOnly = True
        End If
    End Sub

    Private Sub RebuildChecklist(db As DataObject, refnum As String, AIC As String)
        tvIssues.Nodes.Clear() '
        Dim shortUserName = mUserName.Split(".")
        _checklistManager = New RefiningChecklistManager(db, refnum, shortUserName(0))
        db.SQL = "SELECT * FROM VAL.CHECKLIST WHERE REFNUM = '" & GetRefNumCboText() & "'"
        Dim ds As DataSet = db.Execute()

        If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 Then
            _checklistDataTable = ds.Tables(0)

            Select Case AIC.Substring(0, 1).ToUpper()
                Case "A"
                    For Each row As DataRow In _checklistDataTable.Rows
                        If row("Completed") = "Y" Then
                            Dim item As String = row("IssueId") & " - " & row("IssueTitle")
                            Dim n As TreeNode = New TreeNode(item)
                            n.Checked = True
                            tvIssues.Nodes.Add(n)
                        End If
                    Next
                    For Each row As DataRow In _checklistDataTable.Rows

                        If row("Completed") <> "Y" Then
                            Dim item2 As String = row("IssueId") & " - " & row("IssueTitle")
                            Dim n2 As TreeNode = New TreeNode(item2)
                            n2.Checked = False
                            tvIssues.Nodes.Add(n2)
                        End If


                        '==========
                        'Dim item As String = row("IssueId") & " - " & row("IssueTitle")
                        'Dim n As TreeNode = New TreeNode(item)
                        'If row("Completed") = "Y" Then n.Checked = True Else n.Checked = False
                        'If (AIC.StartsWith("C") AndAlso n.Checked) Or (AIC.StartsWith("I") AndAlso Not n.Checked) Then
                        '    tvIssues.Nodes.Add(n)
                        'End If
                    Next
                Case Else
                    For Each row As DataRow In _checklistDataTable.Rows
                        Dim item As String = row("IssueId") & " - " & row("IssueTitle")
                        Dim n As TreeNode = New TreeNode(item)
                        If row("Completed") = "Y" Then n.Checked = True Else n.Checked = False
                        If (AIC.StartsWith("C") AndAlso n.Checked) Or (AIC.StartsWith("I") AndAlso Not n.Checked) Then
                            tvIssues.Nodes.Add(n)
                        End If
                    Next
            End Select
        End If


        'cboCheckListView.SelectedIndex = 0
        radChecklistAddNew.Checked = False
        radChecklistEditIssue.Checked = True
        Try

        Catch ex As Exception
            MsgBox("Error in RebuildChecklist(): " & ex.Message())
        Finally
            db.SQL = String.Empty
        End Try
    End Sub
    Private Function ParseIssueId(nodetext As String) As String
        Dim idx As Integer = nodetext.IndexOf(" -")
        'Return nodetext.Substring(0, idx - 1)
        Dim result As String = nodetext.Substring(0, idx).Trim()
        Return result
    End Function
    Private Sub ShadeChecklist(results As RefiningChecklistResults)
        If Not IsNothing(results.ShadedSelectedNodeText) AndAlso results.ShadedSelectedNodeText.Length > 1 Then
            For Each n As TreeNode In tvIssues.Nodes
                'If n.Text.Trim().ToUpper() = results.ShadedSelectedNodeText.Trim().ToUpper() Then
                'If n.Text.Substring(0, 2) = Results.ShadedSelectedNodeText.Substring(0, 2) Then
                If ParseIssueId(n.Text).Trim() = ParseIssueId(results.ShadedSelectedNodeText) Then
                    n.BackColor = Color.LightGray
                    tvIssues.SelectedNode = n
                Else
                    n.BackColor = Color.Empty
                End If
            Next
        End If
    End Sub

    Private Function ReadIssue(issueId As String) As Boolean
        Try
            Dim row As DataRow
            Dim params As New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueID/" & issueId)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    txtIssueID.Text = Utilities.DropQuotes(row("IssueID").ToString.Trim())
                    txtIssueName.Text = Utilities.DropQuotes(row("IssueTitle").ToString.Trim())
                    txtDescription.Text = Utilities.DropQuotes(row("IssueText").ToString.Trim())
                End If
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function


    Private Sub btnPricing_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Pricing", GetRefNumCboText(), GetCompanyName(GetRefNumCboText()), String.Empty, Get4DigitYearFromCboStudy.ToString(), db)
        ContactForm.Show()
    End Sub

    Private Sub btnDataCoordinator_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "DC", GetRefNumCboText(), GetCompanyName(GetRefNumCboText()), String.Empty, Get4DigitYearFromCboStudy.ToString(), db)
        ContactForm.Show()
    End Sub

    Private Sub btnDensity_Click(sender As System.Object, e As System.EventArgs)
        If Dir("\\dallas2\Data\Data\Study\Refining\General\DensityCalc.xls") <> "" Then
            Process.Start("\\dallas2\Data\Data\Study\Refining\General\DensityCalc.xls")
        End If
    End Sub

    Dim WithEvents ContactForm As ContactFormPopup

    'Show Contact Popup form
    Private Sub btnShow_Click(sender As System.Object, e As System.EventArgs) Handles btnShow.Click
        ContactForm = New ContactFormPopup(UserName, "COORD", GetRefNumCboText(), GetCompanyName(GetRefNumCboText()), String.Empty, Get4DigitYearFromCboStudy.ToString(), db)
        ContactForm.Show()
        ClearContactsTab()
        PopulateContactsTab()
    End Sub

    Private Sub btnShowAltCo_Click(sender As System.Object, e As System.EventArgs) Handles btnShowAltCo.Click
        ContactForm = New ContactFormPopup(UserName, "ALT", GetRefNumCboText(), GetCompanyName(GetRefNumCboText()), String.Empty, Get4DigitYearFromCboStudy.ToString(), db)
        ContactForm.Show()
        ClearContactsTab()
        PopulateContactsTab()
    End Sub

    Private Sub btnShowIntCo_Click(sender As System.Object, e As System.EventArgs) Handles btnShowIntCo.Click
        ContactForm = New ContactFormPopup(UserName, "INTERIM", GetRefNumCboText(), GetCompanyName(GetRefNumCboText()), String.Empty, Get4DigitYearFromCboStudy.ToString(), db)
        ContactForm.Show()
        ClearContactsTab()
        PopulateContactsTab()
    End Sub

    Private Sub btnShowRefCo_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "REFINERY", GetRefNumCboText(), GetCompanyName(GetRefNumCboText()), String.Empty, Get4DigitYearFromCboStudy.ToString(), db)
        ContactForm.Show()
    End Sub

    Private Sub btnShowRefAltCo_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "ALTREF", GetRefNumCboText(), GetCompanyName(GetRefNumCboText()), String.Empty, Get4DigitYearFromCboStudy.ToString(), db)
        ContactForm.Show()
    End Sub

    Private Sub ContactForm_UpdateContact() Handles ContactForm.UpdateContact
        GetCompanyContactInfo()
        GetContacts()
        GetInterimContactInfo()
        FillContacts()
    End Sub

    Private Sub ContactForm_UpdateAltContact() Handles ContactForm.UpdateAltContact
        GetAltCompanyContactInfo()
        FillContacts()
    End Sub

    Private Function GetRefNumsByConsultant() As List(Of String)
        T("GetRefNumsByConsultant")
        Dim rns As List(Of String) = New List(Of String)
        Dim params = New List(Of String)
        params.Add("@Consultant/" + UserName)
        params.Add("@Study/" + Study)
        params.Add("@StudyYear/" + Get4DigitYearFromCboStudy().ToString())
        ds = db.ExecuteStoredProc("Console." & "GetRefNumsByConsultant", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables.Count > 0 Then
                For Each r In ds.Tables(0).Rows
                    rns.Add(r(0))
                Next
            End If
        End If
        Return rns

    End Function

    Private Sub tvwCorrespondence_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tvwCorrespondence.KeyDown, tvwDrawings.KeyDown, tvwClientAttachments.KeyDown, tvwCompCorr.KeyDown, tvwCorrespondence2.KeyDown, tvwDrawings2.KeyDown, tvwClientAttachments2.KeyDown, tvwCompCorr2.KeyDown
        If e.KeyCode = Keys.Space Then
            Process.Start("C:\")
        End If
    End Sub

    Private Sub CheckPreliminaryPricing()
        Try
            Dim folder As String = IDRPath & "PrelimPricing\" & Study
            If Not Directory.Exists(folder) Then Return
            Dim files As String() = Directory.GetFiles(folder, GetRefNumCboText() & "_PP.xls*")
            If files.Length > 0 Then
                'If File.Exists(IDRPath & "\PrelimPricing\" & Study & "\" & GetRefNumCboText() & "_PP.xls") Then
                If CheckRole("PrelimPricing", UserName) Then
                    btnSendPrelimPricing.Visible = True
                Else
                    btnSendPrelimPricing.Visible = False
                End If
            Else
                btnSendPrelimPricing.Visible = False
            End If
        Catch ex As System.Exception
            db.WriteLog("CheckPreliminaryPricing", ex)
        End Try

    End Sub

    Private Sub btnOpenCorrFolder_Click(sender As System.Object, e As System.EventArgs)
        Process.Start(CorrPath)
    End Sub

    Public Function UpdatePN(RN As String) As String
        Dim params = New List(Of String)

        params.Add("RefNum/" + RN)
        params.Add("Issue/" + txtNotes.Text)
        params.Add("Consultant/" + UserName)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)
        If c > 0 Then
            Return "Review Notes updated successfully"
        Else
            Return RN & " did not create"
            MessageBox.Show("Review Notes did not update")
        End If
    End Function

    Private Sub btnUpdatePresenterNotes_Click(sender As System.Object, e As System.EventArgs)
        If txtNotes.Text <> "" Then
            lblStatus.Text = UpdatePN(GetRefNumCboText())
        End If
    End Sub

    Private Sub btnUpdateIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateIssue.Click
        Dim v As String = "VIEW"
        If radChecklistAddNew.Checked Then v = "ADD"
        Dim ckd As Boolean = False
        For Each n As TreeNode In tvIssues.Nodes
            'If n.Text.Substring(0,2).ToUpper.Trim() = "" 
        Next
        Dim issueId As String = txtIssueID.Text.Trim()
        Dim changes As New List(Of String)

        If txtIssueID.Text.Trim() = "" Then
            MsgBox("Issue ID is required")
            Exit Sub
        End If

        If txtIssueName.Text.Trim() = "" Then
            MsgBox("Issue name is required")
            Exit Sub
        End If
        If txtDescription.Text.Trim() = "" Then
            MsgBox("Description is required")
            Exit Sub
        End If
        changes.Add(txtIssueID.Text.Trim())
        changes.Add(txtIssueName.Text.Trim())
        changes.Add(txtDescription.Text.Trim())
        Dim results As RefiningChecklistResults
        results = _checklistManager.GetResults(cboCheckListView.Text, v, issueId, Nothing, changes)
        If Not results Is Nothing Then
            txtIssueID.Text = results.BoxesContent(0)
            txtIssueName.Text = results.BoxesContent(1)
            txtDescription.Text = results.BoxesContent(2)

            If v = "ADD" Then
                If txtIssueID.Text.Trim().Length < 1 Then
                    MsgBox("Issue ID must not be blank")
                    Exit Sub
                End If
                Dim n2 As New TreeNode(results.BoxesContent(0) & " - " & results.BoxesContent(1))
                n2.Checked = False
                tvIssues.Nodes.Add(n2)
                tvIssues.Refresh()
                'the following Try was replaced by a sql server service
                'Try
                '    'desc in body
                '    'refnum then issue name in subject line
                '    Dim emailSubject As String = "Refnum " & GetRefNumCboText() & " has a new checklist item: " & txtIssueName.Text.Trim()
                '    Dim emailBody As String = "Please take note that a new item has been added to your checklist by " & UserName & ":" & Environment.NewLine
                '    emailBody = emailBody & txtDescription.Text
                '    Utilities.SendEmail(cboConsultant.Text & "@Solomononline.com", String.Empty, emailSubject, emailBody, Nothing, False)
                'Catch emailEx As Exception
                'End Try
            End If
            RebuildChecklist(db, GetRefNumCboText(), cboCheckListView.Text)
            ShadeChecklist(results)

            radChecklistEditIssue.Checked = True
        End If
    End Sub
    Private Sub ChecklistSaveNewIssue()
        Dim params As List(Of String)
        ' Be sure they gave us an IssueID and an IssueTitle
        If txtIssueID.Text = "" Or txtIssueName.Text = "" Then
            MsgBox("Please enter both an ID and a Title")
            Exit Sub
        End If

        ' Drop any single quotes in ID, Title, and Text.
        txtIssueID.Text = Utilities.DropQuotes(txtIssueID.Text)
        txtIssueName.Text = Utilities.DropQuotes(txtIssueName.Text)
        txtDescription.Text = Utilities.DropQuotes(txtDescription.Text)

        params = New List(Of String)
        params.Add("RefNum/" + GetRefNumCboText())
        params.Add("IssueID/" + txtIssueID.Text)
        ' Be sure it does not already exist.
        ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                MsgBox("That IssueID already exists for this Refnum.", vbOKOnly)
                Exit Sub
            End If

        End If

        ' Everything looks OK, add it here.
        'Study Combo Box
        params = New List(Of String)
        params.Add("RefNum/" + GetRefNumCboText())
        params.Add("IssueID/" + txtIssueID.Text)
        params.Add("IssueTitle/" + Utilities.DropQuotes(txtIssueName.Text))
        params.Add("IssueText/" + Utilities.DropQuotes(txtDescription.Text))
        params.Add("UserName/" + UserName)
        ds = db.ExecuteStoredProc("Console." & "AddIssue", params)
        BuildCheckListTab("Incomplete")
        btnAddIssue.Enabled = True
        'txtDescription.ReadOnly = True  chg per Jon Stroup
    End Sub

    Private Sub btnCancelIssue_Click(sender As System.Object, e As System.EventArgs)
        btnAddIssue.Enabled = True
    End Sub

    Private Function BuildIDREmail(mfile As String) As String
        Dim TemplateValues As New SA.Console.Common.WordTemplate()

        Dim Coloc As String

        TemplateValues.Field.Add("_TodaysDate")
        TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))


        Dim Row As DataRow
        Dim params As List(Of String)
        ' Company Name
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)

        If ds.Tables.Count > 0 Then
            Row = ds.Tables(0).Rows(0)

            TemplateValues.Field.Add("_Company")
            TemplateValues.RField.Add(Row("Company").ToString)


            TemplateValues.Field.Add("_Refinery")
            TemplateValues.RField.Add(Row("Location").ToString)
            Coloc = Row("Coloc").ToString

        End If

        Dim strFaxName As String
        Dim strFaxEmail As String


        ' Contact Name
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
        If ds.Tables.Count = 0 Then
            strFaxName = "NAME UNAVAILABLE"
            strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
        Else
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)
                strFaxName = Row("CoordName").ToString
                strFaxEmail = Row("CoordEmail").ToString
            End If
        End If

        TemplateValues.Field.Add("_Contact")
        TemplateValues.RField.Add(_tInfo.ToTitleCase(strFaxName))
        ' Contact Email Address

        TemplateValues.Field.Add("_EMail")
        TemplateValues.RField.Add(strFaxEmail)

        ' Consultant Nant
        params = New List(Of String)
        params.Add("Initials/" + UserName)
        ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
        Dim strName As String

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)

                If Row("ConsultantName") Is Nothing Or IsDBNull(Row("ConsultantName")) Then
                    strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                Else
                    strName = Row("ConsultantName").ToString
                End If
            Else
                strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
            End If
        End If

        TemplateValues.Field.Add("_ConsultantName")
        TemplateValues.RField.Add(strName)
        ' Consultant Initials

        TemplateValues.Field.Add("_Initials")

        TemplateValues.RField.Add(UserName)

        Utilities.WordTemplateReplace(mfile, TemplateValues, _profileConsoleTempPath & "SecureSend\" & Utilities.ExtractFileName(mfile), 0)
        Return _profileConsoleTempPath & "SecureSend\" & Utilities.ExtractFileName(mfile)
    End Function


    Private Sub btnSecureSend_Click_1(sender As System.Object, e As System.EventArgs) Handles btnSecureSend.Click
        'If _useMFiles Then
        '    tvwCorrespondence2.Tag = "USE_MFILES"
        '    tvwCompCorr2.Tag = "USE_MFILES"
        '    tvwDrawings2.Tag = "USE_MFILES"
        '    tvwClientAttachments2.Tag = "USE_MFILES"
        'Else
        tvwCorrespondence2.Tag = ""
        tvwCompCorr2.Tag = ""
        tvwDrawings2.Tag = ""
        tvwClientAttachments2.Tag = ""
        'End If
        Dim ss As New SecureSend(db, String.Empty, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, tvwClientTools,
                                 tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, tvwClientTools2, Nothing, Nothing)
        ss.IDRFile = ""
        If chkIDR.Checked Then
            Dim localTemplatePath As String = String.Empty
            Dim NewIDR As String = String.Empty
            'If _useMFiles Then
            '    localTemplatePath = TemplatePath & cboStudy.Text.Trim() & " IDR Instructions.docx"
            'Else
            localTemplatePath = TemplatePath & cboStudy.Text.Trim() & " IDR Instructions.docx"
            'End If
            If Not File.Exists(localTemplatePath) Then
                localTemplatePath = localTemplatePath.Replace(".docx", ".doc")
            End If
            NewIDR = BuildIDREmail(localTemplatePath)
            If System.IO.File.Exists(NewIDR) Then
                'seems to be done already-File.Copy(localTemplatePath, _profileConsoleTempPath + "SecureSend\" + Utilities.ExtractFileName(localTemplatePath), True)
                'NewIDR = BuildIDREmail(StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc")
                ss.IDRFile = NewIDR ' _profileConsoleTempPath & "SecureSend\" & GetStudyRegionFromCboStudy() & Get2DigitYearFromCboStudy().ToString() & " IDR Instructions.doc"
            Else
                MessageBox.Show(NewIDR + " is missing.", "Missing")
                Exit Sub
            End If
        End If
        If ConsoleVertical = VerticalType.REFINING Then
            ss.CompanyContactName = lblRefCoName.Text
        Else
            ss.CompanyContactName = lblRefCoEmail.Text
        End If

        ss.RefNum = RefNum
        'ss.LubeRefNum = LubRefNum
        ss.Study = String.Empty
        ss.TemplatePath = TemplatePath
        ss.CorrPath = CorrPath
        ss.DrawingPath = DrawingPath
        ss.ClientAttachmentsPath = ClientAttachmentsPath
        ss.ClientToolsPath = ClientToolsPath
        If ConsoleVertical = VerticalType.REFINING And GetRefNumCboText().Contains("LUB") Then
            ss.Loc = cboCompany.Text
        Else
            ss.Loc = Company
        End If
        ss.User = UserName
        ss.PCCEmail = lblPCCEmail.Text
        ss.StudyType = cboStudy.Text
        ss.CompCorrPath = CompCorrPath
        ss.CorrPath2 = CorrPath
        ss.DrawingPath2 = DrawingPath
        ss.ClientAttachmentsPath2 = ClientAttachmentsPath
        ss.SendIDR = chkIDR.Checked
        ss.CompCorrPath2 = CompCorrPath
        ss.TempPath = _profileConsoleTempPath + "SecureSend\" '   TempPath
        ss.CompanyPassword = CompanyPassword
        ss.ShortRefNum = GetRefNumCboText()
        ss.LongRefNum = Utilities.GetLongRefnum(GetRefNumCboText())
        ss.StudyYear = Get4DigitYearFromCboStudy.ToString()
        'ss.UseMFiles = _useMFiles
        ss.Vertical = ConsoleVertical
        ss.CompanyContactName = lblName.Text
        If chkAddFLMacros.Checked Then
            Dim filePath As String = GetFLMacrosPath()
            ss.FLMacrosPath = ss.TempPath & Utilities.ExtractFileName(filePath)
            If _docsAccess.FileExists(ss.TempPath & Utilities.ExtractFileName(filePath)) Then
                _docsAccess.FileDelete(ss.TempPath & Utilities.ExtractFileName(filePath))
            End If
            File.Copy(filePath, ss.TempPath & Utilities.ExtractFileName(filePath))
        Else
            ss.FLMacrosPath = String.Empty
        End If
        ss.Show()

    End Sub
    'Private Sub btnSecureSend_Click_1_ORIG(sender As System.Object, e As System.EventArgs) Handles btnSecureSend.Click
    '    If Not _useMFiles Then
    '        If Not Directory.Exists(TempPath & "SecureSend\") Then Directory.CreateDirectory(TempPath & "SecureSend\")
    '    End If
    '    Dim ss As New SecureSend(db, StudyType, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, Nothing, Nothing)


    '    ss.IDRFile = ""
    '    If chkIDR.Checked Then
    '        Dim templatePath As String = String.Empty
    '        If _useMFiles Then
    '            templatePath = _templatePath + StudyRegion + StudyYear.Substring(2, 2) + " IDR Instructions.doc"
    '        Else
    '            templatePath = templatePath & StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc"
    '        End If

    '        If System.IO.File.Exists(templatePath) Then
    '            File.Copy(templatePath, _profileConsoleTempPath + "SecureSend\" + Utilities.ExtractFileName(templatePath), True)
    '            Dim NewIDR As String = BuildIDREmail(StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc")
    '            'FileCopy(TemplatePath & StudyType & " IDR Instructions.doc", TempPath & NewIDR)
    '            ss.IDRFile = TempPath & "SecureSend\" & StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc"
    '        Else
    '            MessageBox.Show(templatePath & StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc is missing.", "Missing")
    '        End If

    '    End If



    '    ss.CompanyContactName = lblRefCoName.Text
    '    ss.RefNum = RefNum
    '    ss.LubeRefNum = LubRefNum
    '    ss.Study = StudyType
    '    ss.TemplatePath = TemplatePath
    '    ss.CorrPath = CorrPath
    '    ss.DrawingPath = DrawingPath
    '    ss.ClientAttachmentsPath = ClientAttachmentsPath
    '    ss.Loc = Company
    '    ss.User = UserName
    '    ss.PCCEmail = lblPCCEmail.Text
    '    ss.StudyType = cboStudy.Text
    '    ss.CompCorrPath = CompCorrPath
    '    ss.CorrPath2 = CorrPath
    '    ss.DrawingPath2 = DrawingPath
    '    ss.ClientAttachmentsPath2 = ClientAttachmentsPath
    '    ss.SendIDR = chkIDR.Checked
    '    ss.CompCorrPath2 = CompCorrPath

    '    ss.TempPath = TempPath & "SecureSend\"

    '    ss.CompanyPassword = CompanyPassword

    '    ss.Show()
    'End Sub

    Private Sub cboRefNum_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs)
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True

            cboRefNum.SelectedIndex = FindString(cboRefNum, GetRefNumCboText())

            'If cboRefNum.SelectedIndex <> -1 Then
            '    RefNumChanged(GetRefNumCboText())
            'Else
            '    cboRefNum.SelectedIndex = cboRefNum.FindString(CurrentRefNum)
            'End If
        End If

    End Sub

    Private Sub btnSection_Click(sender As System.Object, e As System.EventArgs) Handles btnSection.Click
        btnSectionClick()
    End Sub
    Private Sub btnSectionClick()
        If btnSection.Text = "Section" Then
            btnSection.Text = "Revision"
            ResetGradeTab(1)
        Else
            btnSection.Text = "Section"
            ResetGradeTab(2)
        End If
    End Sub
    Private Sub btnFilesSaveMFiles()
        'need to check names and see if clash with MFiles docs.

        'get list of to-be filenames 
        Dim consoleCorrespFiles As New List(Of ConsoleFile)
        Dim consoleCompanyCorrespFiles As New List(Of ConsoleFile)
        Dim consoleGeneralDrawingsFiles As New List(Of ConsoleFile)
        Dim consoleGeneralFiles As New List(Of ConsoleFile)

        Dim companyBenchmarkRefnum As String = String.Empty
        Dim correspondenceBenchmarkingId As Integer = -1
        Dim companyCorrBenchmarkingId As Integer = -1
        Dim nameClash As Boolean = False

        'EUR16Co-EXAMPLE
        companyBenchmarkRefnum = GetCompanyBenchmarkName()
        'Dim tempConsoleFileInfo = _docsAccess.GetDocInfoByNameAndRefnum(companyBenchmarkRefnum, companyBenchmarkRefnum, companyBenchmarkRefnum, False)

        'HttpContext.Current.Server.HtmlEncode(longRefNum);
        'Dim encoded As String = companyBenchmarkRefnum.Replace("&", "&amp;")

        'm encodedRefnum As String = System.Web.HttpContext.Current.Server.HtmlEncode(encoded)

        'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
        Dim tempConsoleFileInfo = _docsAccess.GetDocInfoByNameAndRefnum(companyBenchmarkRefnum, companyBenchmarkRefnum, companyBenchmarkRefnum, False)
        Dim mfiles As New DocsMFilesAccess(ConsoleVertical, companyBenchmarkRefnum, companyBenchmarkRefnum)
        If Not IsNothing(mfiles) Then
            companyCorrBenchmarkingId = mfiles.BenchmarkingParticipantId
        End If

        For i = 0 To dgFiles.Rows.Count - 1
            If dgFiles.Rows(i).Cells(2).Value <> "Do Not Save" Then
                Dim gridFileName As String = dgFiles.Rows(i).Cells(1).Value
                If dgFiles.Rows(i).Cells(2).Value = "Correspondence" Then
                    'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
                    Dim consoleFileInfo As ConsoleFilesInfo = _docsAccess.GetDocInfoByNameAndRefnum(
                    Utilities.FileNameWithoutExtension(gridFileName), GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()), False)
                    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                        If Not IsNothing(consoleFileInfo) Then
                            correspondenceBenchmarkingId = consoleFileInfo.BenchmarkingParticipantId
                            For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                                If consoleFile.FileExtension.ToUpper = GetExtension(gridFileName).ToUpper() Then '.Substring(gridFileName.Length - 3, 3).ToUpper() Then
                                    nameClash = True
                                    consoleCorrespFiles.Add(consoleFile)
                                End If
                            Next
                        End If
                        '//consoleFiles.Add(consoleFileInfo.Files(0)) 'prospectiveFileName + _delim + FileInfo.Files(0).Id.ToString)
                    Else
                        Dim newFile As New ConsoleFile(True)
                        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                        newFile.FileExtension = GetExtension(gridFileName)
                        consoleCorrespFiles.Add(newFile)
                    End If
                ElseIf dgFiles.Rows(i).Cells(2).Value = "Company Correspondence" Then
                    Dim consoleFileInfo As ConsoleFilesInfo = Nothing
                    If companyBenchmarkRefnum.Length > 0 Then
                        'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
                        consoleFileInfo = _docsAccess.GetDocInfoByNameAndRefnum(
                        Utilities.FileNameWithoutExtension(gridFileName), companyBenchmarkRefnum, companyBenchmarkRefnum, False)
                        If Not IsNothing(consoleFileInfo) Then
                            companyCorrBenchmarkingId = consoleFileInfo.BenchmarkingParticipantId
                        End If
                    Else
                        consoleFileInfo = New ConsoleFilesInfo(True, _vertical)
                    End If
                    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                        For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                            If consoleFile.FileExtension.ToUpper = GetExtension(gridFileName).ToUpper() Then
                                nameClash = True
                                consoleCompanyCorrespFiles.Add(consoleFile)
                            End If
                        Next
                    Else
                        Dim newFile As New ConsoleFile(True)
                        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                        newFile.FileExtension = GetExtension(gridFileName)
                        consoleCompanyCorrespFiles.Add(newFile)
                    End If
                ElseIf dgFiles.Rows(i).Cells(2).Value = "Drawings" Then
                    Dim consoleFileInfo As ConsoleFilesInfo = Nothing
                    Dim studyIndex As Integer = GetRefNumCboText().IndexOf(cboStudy.Text().Substring(0, 3))
                    Dim refnumWithoutYear = GetRefNumCboText().Substring(0, studyIndex + 3)
                    Dim strFileName = Utilities.FileNameWithoutExtension(dgFiles.Rows(i).Cells(1).Value)
                    'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
                    Dim result As String = _docsAccess.GetGeneralRefineryDocs(GetRefNumCboText(),
                            Utilities.GetLongRefnum(GetRefNumCboText()), refnumWithoutYear, consoleFileInfo, 2, strFileName)
                    If result.Length > 0 Then
                        MsgBox(result)
                    End If
                    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                        For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                            If consoleFile.FileName.ToUpper() + "." + consoleFile.FileExtension.ToUpper = gridFileName.ToUpper() Then
                                nameClash = True
                                consoleGeneralDrawingsFiles.Add(consoleFile)
                            End If
                        Next
                    Else
                        Dim newFile As New ConsoleFile(True)
                        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                        newFile.FileExtension = GetExtension(gridFileName)
                        consoleGeneralDrawingsFiles.Add(newFile)
                    End If
                ElseIf dgFiles.Rows(i).Cells(2).Value = "General" Then
                    Dim consoleFileInfo As ConsoleFilesInfo = Nothing
                    Dim studyIndex As Integer = GetRefNumCboText().IndexOf(cboStudy.Text().Substring(0, 3))
                    Dim refnumWithoutYear = GetRefNumCboText().Substring(0, studyIndex + 3)
                    Dim strFileName = Utilities.FileNameWithoutExtension(dgFiles.Rows(i).Cells(1).Value)
                    'DON'T USE the instance-level mfiles cached-docs objects. We want to get a fresh list of mfiles docs for the name clash check
                    Dim result As String = _docsAccess.GetGeneralRefineryDocs(GetRefNumCboText(),
                            Utilities.GetLongRefnum(GetRefNumCboText()), refnumWithoutYear, consoleFileInfo, 0, strFileName)
                    If result.Length > 0 Then
                        MsgBox(result)
                    End If
                    If Not IsNothing(consoleFileInfo) AndAlso Not IsNothing(consoleFileInfo.Files) Then
                        For Each consoleFile As ConsoleFile In consoleFileInfo.Files
                            If consoleFile.FileName.ToUpper() + "." + consoleFile.FileExtension.ToUpper = gridFileName.ToUpper() Then
                                nameClash = True
                                consoleGeneralFiles.Add(consoleFile)
                            End If
                        Next
                    Else
                        Dim newFile As New ConsoleFile(True)
                        newFile.FileName = Utilities.FileNameWithoutExtension(gridFileName)
                        newFile.FileExtension = GetExtension(gridFileName)
                        consoleGeneralFiles.Add(newFile)
                    End If

                End If
            End If
        Next
        If nameClash Then
            Dim message As String = "You are about to save files with these names: " + Environment.NewLine
            For Each consoleFile As ConsoleFile In consoleCorrespFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            For Each consoleFile As ConsoleFile In consoleCompanyCorrespFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            For Each consoleFile As ConsoleFile In consoleGeneralDrawingsFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            For Each consoleFile As ConsoleFile In consoleGeneralFiles
                If consoleFile.Id > 0 Then
                    message += "       " + consoleFile.FileName + "." + consoleFile.FileExtension + "," + Environment.NewLine
                End If
            Next
            message += " but files with these names already exist in MFiles." + Environment.NewLine + Environment.NewLine
            message += "Click 'Yes' to overwrite the MFiles, making your file the latest version." + Environment.NewLine
            message += "Click 'No' to go back and make changes to the file names"
            If MsgBox(message, MsgBoxStyle.YesNo, "Console - MFiles") <> vbYes Then Exit Sub
        End If
        'separate code from UI
        DragNDropSaveMFiles(consoleCorrespFiles, correspondenceBenchmarkingId, consoleCompanyCorrespFiles, companyCorrBenchmarkingId, companyBenchmarkRefnum, consoleGeneralDrawingsFiles, consoleGeneralFiles)
    End Sub

    Private Sub btnFilesSaveLegacy()
        Dim CopyFile As String
        Dim Dest As String
        Dim DestFile As String
        Dim ext As String
        Dim success As Boolean = True
        Try
            CompanyPassword = btnCompanyPW.Text
            For I = 0 To dgFiles.Rows.Count - 1

                Dest = dgFiles.Rows(I).Cells(2).Value
                If Dest <> "Do Not Save" Then
                    lblStatus.Text = "Saving " & dgFiles.Rows(I).Cells(0).Value & "...."
                    Try
                        ext = GetExtension(dgFiles.Rows(I).Cells(0).Value)
                    Catch
                        ext = ""
                    End Try

                    CopyFile = _profileConsoleTempPath & FixFilename(ext, dgFiles.Rows(I).Cells(0).Value)

                    DestFile = FixFilename(ext, dgFiles.Rows(I).Cells(1).Value)

                    Select Case (Dest)
                        Case "Correspondence"
                            sDir(I) = CorrPath
                        Case "Drawings"
                            sDir(I) = DrawingPath
                            DestFile = Utilities.AddDateStamp(DestFile)
                        Case "Company Correspondence"
                            sDir(I) = CompCorrPath
                        Case Else
                            sDir(I) = GeneralPath
                    End Select

                    If (ext.ToUpper = "DOC" Or ext.ToUpper = "DOCX" Or ext.ToUpper.Substring(0, 3) = "XLS") And ext.ToUpper <> "XLSM" And ext.ToUpper <> "XLSB" And ext.ToUpper <> "XLST" Then
                        If CompanyPassword.Length = 0 Then
                            MessageBox.Show("Must have a Company Password to Encrypt files", "Save Error")
                            Exit Sub
                        End If

                        success = RefiningRemovePassword(CopyFile, DestFile, GetReturnPassword(RefNum, "fuelslubes"), CompanyPassword)
                        If success Then
                            File.Copy(CopyFile, sDir(I) & DestFile, True)
                            File.Delete(CopyFile)
                        Else
                            MessageBox.Show("Appears the password is incorrect for " & CopyFile & ".  Make sure this is the correct company: " & Company)
                            Exit Sub
                        End If
                    Else
                        File.Copy(CopyFile, sDir(I) & DestFile, True)
                        File.Delete(CopyFile)
                    End If
                End If
            Next

            If optCompanyCorr.Checked Then
                File.Copy(_profileConsoleTempPath & "Email.msg", CompCorrPath & Utilities.CleanFileName(txtMessageFilename.Text), True)
            Else
                File.Copy(_profileConsoleTempPath & "Email.msg", CorrPath & Utilities.CleanFileName(txtMessageFilename.Text), True)
            End If

            File.Delete(_profileConsoleTempPath & "Email.msg")

            dgFiles.Rows.Clear()

            'Dim d As DialogResult = MessageBox.Show("Email and attachments saved.  Would you like to open the correspondence directory?", "Open Folder", MessageBoxButtons.YesNo)
            'If d = System.Windows.Forms.DialogResult.Yes Then Process.Start(CorrPath)
            txtMessageFilename.Text = ""
            btnFilesSave.Enabled = False
            If OldTempPath.Length > 0 Then TempPath = OldTempPath
            lblStatus.Text = "Attachments Saved"
            'ConsoleTabs.TabPages.Remove(tabDD)
            ConsoleTabs.SelectedIndex = 1
        Catch ex As System.Exception
            db.WriteLog("btnSaveFiles", ex)
            lblStatus.Text = "Error saving files: " & ex.Message
        End Try
    End Sub

    Private Sub btnFilesSave_Click(sender As System.Object, e As System.EventArgs) Handles btnFilesSave.Click
        'If _useMFiles Then
        '    If txtMessageFilename.Enabled = True Then 'came from email, not files dropped onto form
        '        If txtMessageFilename.Text.Trim().Length < 1 Then
        '            txtMessageFilename.Text = cboRefNum.Text.Trim() & ".msg"
        '        End If
        '        Dim ext As String = Utilities.GetFileExtension(txtMessageFilename.Text)
        '        If ext.ToUpper() <> "MSG" Then
        '            txtMessageFilename.Text = Utilities.FileNameWithoutExtension(txtMessageFilename.Text) + ".msg"
        '        End If
        '    End If
        '    btnFilesSaveMFiles()
        '    _dragDropMode = 0
        'Else
        btnFilesSaveLegacy()
        'BuildCorrespondenceTab()
        'End If

    End Sub

    Private Sub DragNDropSaveMFiles(ByRef consoleFiles As List(Of ConsoleFile), correspondenceBenchmarkingId As Integer,
                                    ByRef companyCorrespondenceFiles As List(Of ConsoleFile), companyCorrBenchmarkingId As Integer,
                                    companyCorrespondenceRefnum As String,
                                    ByRef consoleGeneralDrawingsFiles As List(Of ConsoleFile),
                                    ByRef consoleGeneralFiles As List(Of ConsoleFile))
        Dim Dest As String
        Dim success As Boolean = False

        Try
            Dim docsMFilesAccessCorrespondence As DocsMFilesAccess = Nothing
            docsMFilesAccessCorrespondence = New DocsMFilesAccess(ConsoleVertical, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
            Dim docsMFilesAccessCompany As DocsMFilesAccess = Nothing
            docsMFilesAccessCompany = New DocsMFilesAccess(ConsoleVertical, companyCorrespondenceRefnum, companyCorrespondenceRefnum)

            For i = 0 To dgFiles.Rows.Count - 1
                Dest = dgFiles.Rows(i).Cells(2).Value
                Dim fileName As String = dgFiles.Rows(i).Cells(0).Value
                If Dest <> "Do Not Save" Then
                    'txtDragDropStatus.Text = "Saving " & fileName & "...."
                    If Dest = "Correspondence" Then
                        If Not ProcessDragDropMFile(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleFiles, correspondenceBenchmarkingId, docsMFilesAccessCorrespondence) Then
                            MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                    ElseIf Dest = "Company Correspondence" Then
                        If Not ProcessDragDropMFile(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), companyCorrespondenceFiles, companyCorrBenchmarkingId, docsMFilesAccessCompany) Then
                            MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                    ElseIf Dest = "Drawings" Then
                        If Not ProcessDragDropMFileGeneralItem(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleGeneralDrawingsFiles, 2) Then
                            MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                    ElseIf Dest = "General" Then
                        If Not ProcessDragDropMFileGeneralItem(fileName, dgFiles.Rows(i).Cells(1).Value.Trim(), consoleGeneralFiles, 0) Then
                            MsgBox("Error occurred in DragNDropSaveMFiles," + dgFiles.Rows(i).Cells(1).Value.Trim() + " was not saved.")
                        End If
                    End If
                Else
                    Try
                        If File.Exists(_profileConsoleTempPath + fileName) Then
                            File.Delete(_profileConsoleTempPath + fileName)
                        ElseIf File.Exists(_profileConsoleExtractedPath + fileName) Then
                            File.Delete(_profileConsoleExtractedPath + fileName)
                        End If
                    Catch
                    End Try
                End If  'end of  If Dest <> "Do Not Save" Then
            Next

            'save email
            'mailItem.SaveAs(_profileConsoleTempPath + GetRefNumCboText() + ".msg")
            If txtMessageFilename.Enabled Then
                If Not optCompanyCorr.Checked Then
                    'save email to REfinery corr
                    If Not ProcessDragDropMFileEmail(docsMFilesAccessCorrespondence, correspondenceBenchmarkingId) Then
                        MsgBox("Error occured; email not saved.")
                    End If
                Else
                    'save email to Company  corresp
                    If Not ProcessDragDropMFileEmail(docsMFilesAccessCompany, companyCorrBenchmarkingId) Then
                        MsgBox("Error occured; email not saved.")
                    End If
                End If
            End If
            'start move to new method---------------------------
            'Dim consoleMFiles As ConsoleFilesInfo = DocsMFilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant(Utilities.GetLongRefnum(cboRefNum.Text))
            'Dim emailFileId As Integer = -1
            'For Each consoleMFile As ConsoleFile In consoleMFiles.Files
            '    If consoleMFile.FileName.ToUpper().Trim() = Utilities.FileNameWithoutExtension(txtMessageFilename.Text.ToUpper.Trim()) Then
            '        emailFileId = consoleMFile.Id
            '        Exit For
            '    End If
            'Next
            'File.Move(_profileConsoleTempPath + cboRefNum.Text + ".msg", _profileConsoleTempPath + txtMessageFilename.Text)
            'If emailFileId > -1 Then 'name clash
            '    DocsMFilesAccess.UploadNewVersionOfDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, emailFileId)
            'Else
            '    DocsMFilesAccess.UploadNewDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, 12, -1, Nothing)
            'End If
            ''end move to new method--------------------------------

            dgFiles.Rows.Clear()
            txtMessageFilename.Text = ""
            'txtDragDropStatus.Text = "Attachments Saved"
            DeleteExtractedFolder()

            Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath, "*.*")
            For Each sFile In zfilenames
                Try
                    File.Delete(sFile)
                Catch 'it's not that important. Will get cleared out during next Drag-N-Drop
                End Try
            Next
        Catch ex As System.Exception
            db.WriteLog("btnSaveFiles", ex)
            'txtDragDropStatus.Text = "Error saving files: " & ex.Message
        Finally
            'RefreshMFilesCollections(0)
            'RefreshMFilesCollections(1)
            'RefreshMFilesCollections(2)
        End Try
    End Sub

    Private Function ProcessDragDropMFileGeneralItem(fileName As String, saveAsFileName As String,
        ByRef filesList As List(Of ConsoleFile), drawingArchive As Integer) As Boolean
        '//if drawingArchive = 0, then put at GeneralRefinery level. If 1, put at 
        '//  acrhive subfolder. If 2, put at Drawings subfolder
        Try
            Dim ext As String = String.Empty
            Dim mFilesAccess As New DocsMFilesAccess(ConsoleVertical, GetRefNumCboText(), GetRefNumCboText())
            Try
                ext = GetExtension(fileName)
            Catch  'ext = ""
            End Try

            '##### NOTE: FixFilename below will change file extension!!!
            Dim localPath As String = String.Empty
            If File.Exists(_profileConsoleTempPath + fileName) Then
                localPath = _profileConsoleTempPath
            ElseIf File.Exists(_profileConsoleExtractedPath + fileName) Then
                localPath = _profileConsoleExtractedPath
            End If
            Dim CopyFile As String = FixFilename(ext, fileName.Trim()) ' will change file extension!!!
            Dim DestFile As String = FixFilename(ext, saveAsFileName)  'will change file extension!!!
            'If Not CopyFile.EndsWith(".msg") Then 'handle it below
            Dim nameClash As Boolean = False
            Dim consoleFileId As Integer = 0
            Dim found As Boolean = False

            For Each consoleFile As ConsoleFile In filesList 'ths could be Corresp or Company
                '1 try to match up Existing Filename with consoleFile
                '2 if not find, then is handled below this For
                'If CopyFile.ToUpper() = consoleFile.FileName.ToUpper() + _
                If DestFile.ToUpper() = consoleFile.FileName.ToUpper() +
                    "." + consoleFile.FileExtension.ToUpper() Then
                    found = True
                    If consoleFile.Id > 0 Then
                        nameClash = True ' will save as new version of existing file
                        consoleFileId = consoleFile.Id
                    Else
                        'new file to be uploadd to MFiles. No other action needed.
                    End If
                    Exit For
                End If
            Next

            If Not found Then
                'User must have changed the name in 2nd column so rename and save as that.
                If File.Exists(localPath + fileName) Then
                    File.Move(localPath + fileName, localPath + DestFile.Trim())
                    CopyFile = DestFile.Trim()
                End If
            End If
            If (File.Exists(localPath + fileName)) And (Not File.Exists(localPath + DestFile.Trim())) Then
                'User must have changed to name in 2nd column so rename and save as that.
                File.Move(localPath + fileName, localPath + DestFile.Trim())
                CopyFile = DestFile.Trim()
            End If

            If nameClash Then
                mFilesAccess.UploadNewVersionOfDocToMFiles(localPath & CopyFile, consoleFileId)
            Else 'is new doc
                'need 35EUR
                Dim regionStart As Integer = cboRefNum.Text.IndexOf(cboStudy.Text.Substring(0, 3))
                Dim startingNumber As String = cboRefNum.Text.Substring(0, regionStart)
                Dim refNumWithoutYear As String = startingNumber & cboStudy.Text.Replace(Get2DigitYearFromCboStudy, "")
                mFilesAccess.UploadNewGeneralRefineryItemToMFiles(localPath & CopyFile, refNumWithoutYear, drawingArchive)
            End If
            Try
                File.Delete(localPath + CopyFile)
            Catch exDelete As Exception
                Dim discard As String = String.Empty
            End Try
            Return True
        Catch ex As Exception
            db.WriteLog("Error in ProcessDragDropMFileGeneralItem().", ex)
            Return False
        End Try
    End Function

    Private Function ProcessDragDropMFile(fileName As String, saveAsFileName As String,
                                      ByRef filesList As List(Of ConsoleFile), benchmark As Integer,
                                      ByRef mFilesAccess As DocsMFilesAccess) As Boolean
        Try
            Dim ext As String = String.Empty
            Try
                ext = GetExtension(fileName)
            Catch  'ext = ""
            End Try

            '##### NOTE: FixFilename below will change file extension!!!
            Dim localPath As String = String.Empty
            If File.Exists(_profileConsoleTempPath + fileName) Then
                localPath = _profileConsoleTempPath
            ElseIf File.Exists(_profileConsoleExtractedPath + fileName) Then
                localPath = _profileConsoleExtractedPath
            End If
            Dim CopyFile As String = FixFilename(ext, fileName.Trim()) ' will change file extension!!!
            Dim DestFile As String = FixFilename(ext, saveAsFileName)  'will change file extension!!!
            'If Not CopyFile.EndsWith(".msg") Then 'handle it below
            Dim nameClash As Boolean = False
            Dim consoleFileId As Integer = 0
            Dim found As Boolean = False

            For Each consoleFile As ConsoleFile In filesList 'ths could be Corresp or Company
                '1 try to match up Existing Filename with consoleFile
                '2 if not find, then is handled below this For
                'If CopyFile.ToUpper() = consoleFile.FileName.ToUpper() + _
                If DestFile.ToUpper() = consoleFile.FileName.ToUpper() +
                    "." + consoleFile.FileExtension.ToUpper() Then
                    found = True
                    If consoleFile.Id > 0 Then
                        nameClash = True ' will save as new version of existing file
                        consoleFileId = consoleFile.Id
                    Else
                        'new file to be uploadd to MFiles. No other action needed.
                    End If
                    Exit For
                End If
            Next

            If Not found Then
                'User must have changed to name in 2nd column so rename and save as that.
                If File.Exists(localPath + fileName) Then
                    File.Move(localPath + fileName, localPath + DestFile.Trim())
                    CopyFile = DestFile.Trim()
                End If
            End If
            If (File.Exists(localPath + fileName)) And (Not File.Exists(localPath + DestFile.Trim())) Then
                'User must have changed to name in 2nd column so rename and save as that.
                File.Move(localPath + fileName, localPath + DestFile.Trim())
                CopyFile = DestFile.Trim()
            End If


            If nameClash Then
                mFilesAccess.UploadNewVersionOfDocToMFiles(localPath & CopyFile, consoleFileId)
            Else 'is new doc, benchmark could be for corresp or for Company.
                mFilesAccess.UploadNewDocToMFiles(localPath & CopyFile, 12, benchmark, Nothing)
            End If
            Try
                File.Delete(localPath + CopyFile)
            Catch exDelete As Exception
                Dim discard As String = String.Empty
            End Try
            Return True
        Catch ex As Exception
            db.WriteLog("Error in ProcessDragDropMFile().", ex)
            Return False
        End Try
    End Function


    Private Function ProcessDragDropMFileEmail(ByRef mfilesAccess As DocsMFilesAccess, benchmark As Integer) As Boolean
        Try
            Dim emailCorrespFileId As Integer = -1
            Dim consoleEmailCorresp As ConsoleFilesInfo = mfilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant(Utilities.GetLongRefnum(GetRefNumCboText()))
            For Each consoleMFile As ConsoleFile In consoleEmailCorresp.Files
                If consoleMFile.FileName.ToUpper().Trim() = Utilities.FileNameWithoutExtension(txtMessageFilename.Text.ToUpper.Trim()) Then
                    emailCorrespFileId = consoleMFile.Id
                    Exit For
                End If
            Next
            File.Move(_profileConsoleTempPath + GetRefNumCboText() + ".msg", _profileConsoleTempPath + txtMessageFilename.Text)
            If emailCorrespFileId > -1 Then 'name clash
                mfilesAccess.UploadNewVersionOfDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, emailCorrespFileId)
            Else
                mfilesAccess.UploadNewDocToMFiles(_profileConsoleTempPath + txtMessageFilename.Text, 12, benchmark, Nothing)
            End If
            Return True
        Catch ex As Exception
            MsgBox("Error in ProcessDragDropMFileEmail(): " + ex.Message)
            db.WriteLog("Error in ProcessDragDropMFileEmail().", ex)
            Return False
        End Try
    End Function



    Private Sub cboDir_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir.SelectedIndexChanged
        T("cboDir_SelectedIndexChanged")
        Select Case cboDir.Text

            Case "Refinery Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = True

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False
                tvwClientTools.Visible = False

            Case "Client Attachments"
                tvwClientAttachments.Visible = True
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False
                tvwClientTools.Visible = False

            Case "Drawings"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = True
                tvwCompCorr.Visible = False
                tvwClientTools.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = True
                tvwClientTools.Visible = False
            Case "Client Tools"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False
                tvwClientTools.Visible = True
        End Select
    End Sub

    Private Sub cboDir2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir2.SelectedIndexChanged
        T("cboDir2_SelectedIndexChanged")
        Select Case cboDir2.Text

            Case "Refinery Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = True

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False
                tvwClientTools2.Visible = False
            Case "Client Attachments"
                tvwClientAttachments2.Visible = True
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False
                tvwClientTools2.Visible = False
            Case "Drawings"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = True
                tvwCompCorr2.Visible = False
                tvwClientTools2.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = True
                tvwClientTools2.Visible = False
            Case "Client Tools"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False
                tvwClientTools2.Visible = True
        End Select
    End Sub

    Private Sub btnFLComp_Click(sender As System.Object, e As System.EventArgs)
        File.Copy(IDRPath & "Compare Fuels and Lubes " & Get2DigitYearFromCboStudy().ToString() & ".xls", CorrPath & "Compare Fuels and Lubes " & Get2DigitYearFromCboStudy().ToString() & ".xls")
        Process.Start(CorrPath & "Compare Fuels and Lubes " & Get2DigitYearFromCboStudy().ToString() & ".xls")
    End Sub

    Private Sub btnTP_Click(sender As System.Object, e As System.EventArgs)
        '  "*Transfer Pricing*" does not exist in 2012 or 2014...
        File.Copy(IDRPath & "Transfer Pricing " & Get2DigitYearFromCboStudy().ToString() & ".xls", CorrPath & "Transfer Pricing " & Get2DigitYearFromCboStudy().ToString() & ".xls")
        Process.Start(CorrPath & "Transfer Pricing " & Get2DigitYearFromCboStudy().ToString() & ".xls")
    End Sub

    Private Sub btnOpenWord_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As New Word.Application
        Dim docPN As New Word.Document
        Try
            If Dir(CorrPath & "PN_" & cboCompany.Text & ".doc") = "" Then
                If MsgBox("Do you want to create PN_" & cboCompany.Text & " .doc and stop updating the IDR Notes / Presenter's Notes field through Console?", vbYesNoCancel + vbDefaultButton2, "Click Yes to create the file.") = vbYes Then
                    'MsgBox "Create the file, lock the field, open the file with word."

                    Dim strWork As String = "~ " & Now & " The Presenters Notes document has been built so this field is locked. Please use the Presenters Notes button to open the file. " & Chr(13) & Chr(10) & "----------" & Chr(13) & Chr(10)

                    ' Add the tilde to the Issues Notes field
                    ' First, see if a record exists for this refnum
                    Dim row As DataRow
                    Dim params As New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)
                    If ds.Tables.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        txtNotes.Text = row("ValidationNotes").ToString
                    Else
                        params = New List(Of String)
                        params.Add("RefNum/" + RefNum)
                        params.Add("Notes/" + txtNotes.Text)
                        Dim Count As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)
                    End If

                    Dim objWriter As New System.IO.StreamWriter(_profileConsoleTempPath & "PNotes.txt", True)
                    objWriter.WriteLine(RefNum)
                    objWriter.WriteLine(txtNotes.Text)
                    objWriter.Close()

                    ' Create the document.
                    File.Copy(TemplatePath & "PN_Template.doc", _profileConsoleTempPath & "PN_Template.doc", True)
                    msword = New Word.Application
                    'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                    docPN = msword.Documents.Open(_profileConsoleTempPath & "PN_Template.doc")

                    docPN.SaveAs(CorrPath & "PN_" & cboCompany.Text & ".doc")

                    ' Put the CoLoc and the Notes into the document.
                    msword.Run("SubsAuto")
                End If
            Else

                msword = New Word.Application
                'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                docPN = msword.Documents.Open(CorrPath & "PN_" & cboCompany.Text & ".doc")
                msword.Visible = True
            End If

        Catch Ex As System.Exception
            db.WriteLog("btnOpenWord", Ex)
        End Try


    End Sub

    Private Function HandleDragDropZipAttachmentFiles(ByRef localstrFile As String,
           ByRef localOldTempPath As String, ByRef localTempPath As String,
           ByRef localstrNextRetNum As String) As Boolean
        Try
            For Each localmfile In Directory.GetFiles(localOldTempPath)
                localmfile = Utilities.ExtractFileName(localmfile)

                localstrFile = localmfile

                Dim zr = dgFiles.Rows.Add()
                Dim zcbcell = New DataGridViewComboBoxCell
                zcbcell.Items.Add("Do Not Save")
                zcbcell.Items.Add("Correspondence")
                zcbcell.Items.Add("Company Correspondence")
                zcbcell.Items.Add("Drawings")
                zcbcell.Items.Add("General")
                zcbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                dgFiles.Rows(zr).Cells(2) = zcbcell
                Dim zext As String = GetExtension(localmfile)
                Select Case (zext.ToUpper)
                    Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM"
                        zcbcell.Value = "Correspondence"
                    Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG", "VSO"
                        zcbcell.Value = "Drawings"
                    Case Else
                        zcbcell.Value = "General"
                End Select

                File.Copy(localOldTempPath & "\" & localstrFile, localTempPath & localstrFile)
                dgFiles.Rows(zr).Cells(0).Value = localmfile
                If localmfile.Contains("VFFL") Then
                    localstrFile = localmfile
                    dgFiles.Rows(zr).Cells(1).Value = "VRFL" & NextFile(CorrPath & "VRFL", "doc") & "-" & localmfile.Substring(6, Len(localmfile) - 6)
                Else
                    If localmfile.Substring(0, 2).ToUpper() = "VF" Then
                        localstrFile = localmfile
                        dgFiles.Rows(zr).Cells(1).Value = "VR" & NextVR() & "-" & localmfile.Substring(4, Len(localmfile) - 4)
                    Else
                        localstrFile = localmfile.Substring(0, localmfile.Length - 4) & "." & zext
                    End If
                End If

                If localmfile.ToUpper.Contains("RETURN") Then
                    dgFiles.Rows(zr).Cells(1).Value = localmfile.Substring(0, localmfile.Length - 4) & localstrNextRetNum & "." & zext
                Else
                    dgFiles.Rows(zr).Cells(1).Value = localmfile.Substring(0, localmfile.Length - 4) & "." & zext
                End If
            Next

            Return True
        Catch ex As Exception

        End Try
    End Function

    Private Sub MainConsole_DragDrop(ByVal sender As Object,
                        ByVal e As System.Windows.Forms.DragEventArgs) _
                        Handles MyBase.DragDrop, ConsoleTabs.DragDrop
        'DEBUG NOTE: If this isn't firing in the IDE, try closing VS and reopening but NOT "As Administrator"
        '  For further info, see https://stackoverflow.com/questions/68598/how-do-i-drag-and-drop-files-into-an-application

        'If _useMFiles And _mfilesFailed Then Exit Sub
        If Not ConsoleTabs.TabPages.Contains(tabDD) Then ConsoleTabs.TabPages.Add(tabDD)
        ConsoleTabs.SelectTab(tabDD)

        txtMessageFilename.Text = String.Empty
        txtMessageFilename.Enabled = True
        optCompanyCorr.Enabled = True

        Dim strFile As String = String.Empty
        Dim ext As String
        'Dim zext As String
        Dim SavedFiles As New List(Of String)
        Dim strFileName As String
        Dim ReturnFile As Boolean = False
        Dim OrigFile As Boolean = False
        Dim NextLetter As String = ""
        Dim strNextNum = NextVF()
        Dim location As String = String.Empty
        Dim failedFiles As New List(Of String)
        'If _useMFiles Then
        '    Dim params = New List(Of String)
        '    params.Add("RefNum/" + GetRefNumCboText())
        '    Dim ds As DataSet = db.ExecuteStoredProc("Console." & "GetTSortData", params)
        '    If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 Then
        '        'If ds.Tables(0).Rows.Count = 1 Then  can have > 1 record in TSort if LUB
        '        location = ds.Tables(0).Rows(0)("Location").ToString
        '    End If
        'End If
        Dim fuelsNotLubes = Not cboStudy.Text.Contains("LUB")
        Dim strNextRetNum = NextReturnFile(fuelsNotLubes, location)
        Dim companyPw As String = GetCompanyPassword(GetRefNumCboText())
        If GetRefNumCboText() <> "" Then
            'Dim strNumModifier = FindModifier(strNextNum)
            Dim mailItem As Outlook._MailItem
            Try
                Me.ConsoleTabs.SelectedIndex = 9

                'RRH removed due to skipping zip files; it alsu used a double negative
                'If e.Data.GetDataPresent(DataFormats.Text) Then

                'check if this is an outlook message. The outlook messages, all contain a FileContents attribute. If not, exit.
                'Dim formats() As String = e.Data.GetFormats()
                'If formats.Contains("FileContents") = False Then Exit Sub 

                'check for Drag-Drop Files instead of Email
                Dim filePaths As List(Of String) = Nothing
                Select Case DragDropForFilesCheck(e.Data, filePaths)
                    Case 0 'error
                        Exit Sub
                    Case 1 'yes, drag drop for files,

                        If Not _dragDropMode = 1 Then
                            ClearDragDropTab()
                        End If
                        _dragDropMode = 1
                        DragDropNonEmailToMFiles(filePaths)
                        Return
                    Case 2 'no, drag drop for email,
                        'Fall through and continue below
                        'unless already have an email in the grid
                        If _dragDropMode = 2 Then
                            ClearDragDropTab()
                            Return
                        End If
                        _dragDropMode = 2
                End Select

                ClearDragDropTab()
                Dim currentFileName As String = String.Empty

                If dgFiles.Rows.Count = 0 Then

                    'they are dragging the attachment
                    If (e.Data.GetDataPresent("Object Descriptor")) Then
                        Dim app As New Microsoft.Office.Interop.Outlook.Application() ' // get current selected items
                        Dim selection As Microsoft.Office.Interop.Outlook.Selection
                        Dim myText As String = ""
                        selection = app.ActiveExplorer.Selection
                        If selection IsNot Nothing Then

                            For i As Integer = 0 To selection.Count - 1

                                mailItem = TryCast(selection.Item(i + 1), Outlook._MailItem)
                                'Dim docsMFilesAccess As New DocsMFilesAccess(ConsoleVertical, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                                If mailItem IsNot Nothing Then
                                    Try

                                        myText = "" '"DATE SENT: " & mailItem.SentOn.ToString() & vbCrLf & vbCrLf

                                        myText += e.Data.GetData("Text")  'header text
                                        myText += vbCrLf + vbCrLf
                                        myText += mailItem.Body  'Plain Text Body Message

                                        'now save the attachments with the same file name and then 1,2,3 next to it
                                        For Each att As Attachment In mailItem.Attachments
                                            Dim thisIsARealAttachment As Boolean = True
                                            Try
                                                currentFileName = att.FileName
                                            Catch ex As Exception
                                                'something embedded in email, not a true attachment.
                                                thisIsARealAttachment = False
                                            End Try

                                            If thisIsARealAttachment Then
                                                currentFileName = Utilities.CleanFileName(currentFileName)
                                                btnFilesSave.Enabled = True
                                                If currentFileName.Substring(0, 5).ToUpper <> "IMAGE" Then
                                                    Try
                                                        ext = currentFileName.Substring(currentFileName.LastIndexOf(".") + 1, currentFileName.Length - currentFileName.LastIndexOf(".") - 1)
                                                    Catch
                                                        ext = ""
                                                    End Try

                                                    If ext.ToUpper = "ZIP" Then
                                                        If Dir(_profileConsoleTempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(_profileConsoleTempPath & "\Extracted Files\")
                                                        Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath & "Extracted Files", "*.*")
                                                        For Each sFile In zfilenames
                                                            Try
                                                                File.Delete(sFile)
                                                            Catch ex As System.Exception
                                                                db.WriteLog("DragDrop", ex)
                                                                Debug.Print(ex.Message)
                                                            End Try
                                                        Next

                                                        att.SaveAsFile(_profileConsoleTempPath & "Extracted Files\" & currentFileName)
                                                        Utilities.UnZipFiles(_profileConsoleTempPath & "Extracted Files\" & currentFileName, _profileConsoleTempPath, CompanyPassword)
                                                        File.Delete(_profileConsoleTempPath & "Extracted Files\" & currentFileName)
                                                        OldTempPath = _profileConsoleTempPath & "Extracted Files\"

                                                        Dim extractedFilesDirectory As New DirectoryInfo(OldTempPath)
                                                        'For Each zipfolder In Directory.GetDirectories(OldTempPath)
                                                        Dim zipfolders As DirectoryInfo() = extractedFilesDirectory.GetDirectories()
                                                        For folderCount As Integer = 0 To zipfolders.Length - 1
                                                            If Not HandleDragDropZipAttachmentFiles(strFile,
                                                            zipfolders(folderCount).FullName, TempPath, strNextRetNum) Then
                                                                failedFiles.Add(strFile)
                                                                dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                                                'Exit Sub
                                                            End If
                                                        Next

                                                        If Not HandleDragDropZipAttachmentFiles(strFile,
                                                            OldTempPath, TempPath, strNextRetNum) Then
                                                            failedFiles.Add(strFile)
                                                            dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                                            'Exit Sub
                                                        End If

                                                    Else
                                                        Dim mfile As String
                                                        Dim r = dgFiles.Rows.Add()
                                                        dgFiles.Rows(r).Cells(0).Value = currentFileName

                                                        mfile = currentFileName
                                                        Dim cbcell = New DataGridViewComboBoxCell

                                                        cbcell.Items.Add("Do Not Save")
                                                        cbcell.Items.Add("Correspondence") ' changing causes dataerror
                                                        cbcell.Items.Add("Company Correspondence")
                                                        cbcell.Items.Add("Drawings")
                                                        cbcell.Items.Add("General")
                                                        cbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton

                                                        dgFiles.Rows(r).Cells(2) = cbcell

                                                        Select Case (ext.ToUpper.Substring(0, 3))
                                                            Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM"
                                                                cbcell.Value = "Correspondence"
                                                            Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG"
                                                                cbcell.Value = "Drawings"
                                                            Case Else
                                                                cbcell.Value = "General"
                                                        End Select

                                                        If File.Exists(_profileConsoleTempPath & mfile) Then
                                                            Try
                                                                File.Delete(_profileConsoleTempPath & mfile)
                                                            Catch ex As Exception
                                                                'MsgBox "Console tried to clear out this file, but failed. Please 
                                                                Try
                                                                    File.Move(_profileConsoleTempPath & mfile, _profileConsoleTempPath & "DELETE" + DateTime.Now.ToString())
                                                                Catch
                                                                End Try
                                                            End Try
                                                        End If
                                                        att.SaveAsFile(_profileConsoleTempPath & mfile)
                                                        If mfile.Contains("VFFL") Then
                                                            strFile = "VRFL" & NextVRFL() & "-" & mfile.Substring(6, Len(mfile) - 6)
                                                            dgFiles.Rows(r).Cells(1).Value = strFile
                                                        Else
                                                            If mfile.Substring(0, 4).ToUpper().Contains("VF") Then
                                                                ReturnFile = True
                                                                strFile = "VR" & NextVR() & "-" & mfile.Substring(4, Len(mfile) - 4)
                                                                dgFiles.Rows(r).Cells(1).Value = strFile
                                                            Else
                                                                strFile = mfile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                                                                'If Not _useMFiles Then
                                                                If strFile.ToUpper.Contains("RETURN") Then
                                                                    ReturnFile = True
                                                                    If strNextRetNum Is Nothing Then
                                                                        strNextRetNum = "0"
                                                                    End If
                                                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & strNextRetNum & "." & ext
                                                                ElseIf strFile.ToUpper.Contains("_CPP") Or strFile.ToUpper.Contains("_PPP") Then
                                                                    Dim strReplaceWith As String
                                                                    Dim strReplace As String
                                                                    If strFile.ToUpper.Contains("_CPP") Then
                                                                        strReplace = "CPP"
                                                                    Else
                                                                        strReplace = "PPP"
                                                                    End If
                                                                    strReplaceWith = getPfileName(_profileConsoleTempPath & mfile)
                                                                    If strReplaceWith <> String.Empty Then
                                                                        dgFiles.Rows(r).Cells(1).Value = strFile.Replace(strReplace, strReplaceWith)
                                                                    Else
                                                                        dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & getNextPFileSequenceNumber("*_" & strReplace & "*") & "." & ext
                                                                    End If
                                                                Else
                                                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                                                                End If
                                                                '        Else
                                                                '            If (strFile.ToUpper.Contains("F_RETURN") OrElse strFile.ToUpper.Contains("L_RETURN")) AndAlso strFile.ToUpper.Contains(".XLSX") Then
                                                                '                ReturnFile = True
                                                                '                If Not strFile.ToUpper().Contains(Replace(location.ToUpper(), ".", "")) Then
                                                                '                    MsgBox("Possible Refinery Mismatch - the Return File does not have the correct Location (" + location + ") in its name. Please contact Sung for resolution of this problem")
                                                                '                    ClearDragDropTab()
                                                                '                    Exit Sub
                                                                '                End If
                                                                '                dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & strNextRetNum & "." & ext
                                                                '            Else
                                                                '                dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                                                                '            End If
                                                                '        End If
                                                                '    End If
                                                            End If
                                                            mfile = dgFiles.Rows(r).Cells(1).Value
                                                            'Per Penny, we aren't saving anything to MFiles in an encrypted state
                                                            'moving this below to decrypt before saving:  att.SaveAsFile(TempPath & dgFiles.Rows(r).Cells(0).Value)
                                                            'NEW: unprotect here
                                                            Dim fileName As String = dgFiles.Rows(r).Cells(0).Value
                                                            Dim filePath As String = TempPath & fileName
                                                            att.SaveAsFile(filePath)
                                                            If Utilities.GetFileExtension(fileName).ToUpper().StartsWith("XLS") And Utilities.GetFileExtension(fileName).ToUpper() <> "XLSM" _
                                                                And Utilities.GetFileExtension(fileName).ToUpper() <> "XLSB" And Utilities.GetFileExtension(fileName).ToUpper() <> "XLST" Then
                                                                If Utilities.ExcelWorkbookIsEncrypted(filePath) Then
                                                                    Dim u As New Utilities()
                                                                    If Not u.RemovePassword(filePath, GetReturnPassword(GetRefNumCboText(), "fuelslubes"), companyPw) Then
                                                                        MsgBox("Unable to remove password on " & fileName)
                                                                        'ClearDragDropTab()
                                                                        failedFiles.Add(currentFileName)
                                                                        dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                                                        'Exit Sub
                                                                    End If
                                                                End If
                                                            ElseIf Utilities.GetFileExtension(fileName).ToUpper().StartsWith("DOC") Then
                                                                If Utilities.WordDocIsEncrypted(filePath) Then
                                                                    Dim u As New Utilities()
                                                                    If Not u.RemovePassword(filePath, GetReturnPassword(GetRefNumCboText(), "fuelslubes"), companyPw) Then
                                                                        MsgBox("Unable to remove password on " & fileName)
                                                                        'ClearDragDropTab()
                                                                        failedFiles.Add(currentFileName)
                                                                        dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                                                        'Exit Sub
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Next
                                        dgFiles.EditMode = DataGridViewEditMode.EditOnEnter
                                        'save the mail message

                                        'strEmailFile = _profileConsoleTempPath & "email.txt"
                                        'Dim strw As New StreamWriter(strEmailFile, False)
                                        'strw.WriteLine(myText)
                                        'strw.Close()
                                        'strw.Dispose()

                                        Dim subject As String = String.Empty

                                        If IsNothing(mailItem.Subject) OrElse mailItem.Subject.Length < 1 Then
                                            subject = GetRefNumCboText() & " " & DateTime.Today().ToShortDateString().Replace("/", "-")
                                        Else
                                            subject = mailItem.Subject
                                            subject = Utilities.CleanFileName(subject)
                                        End If

                                        strFileName = cboCompany.Text
                                        Dim emailExt As String = ".msg"
                                        'If _useMFiles Then emailExt = ".msg"
                                        If Not strNextNum Is Nothing And strNextNum = 0 Then
                                            strFileName = cboCompany.Text
                                        Else
                                            ' More often, use this technique to name the text file.
                                            If strFileName.Contains("VFFL") Then
                                                txtMessageFilename.Text = Utilities.CleanFileName("VRFL" & NextVRFL() & " " & subject.Trim.Replace(":", " ") & emailExt) '".txt"
                                            Else
                                                If strFileName.Contains("VF") Then
                                                    txtMessageFilename.Text = Utilities.CleanFileName("VR" & NextVR() & " " & subject.Trim.Replace(":", " ") & emailExt) '".txt"
                                                Else
                                                    If OrigFile Then
                                                        txtMessageFilename.Text = Utilities.CleanFileName(subject.Trim.Replace(":", " ") & "-" & NextLetter & emailExt) '".txt"
                                                    Else
                                                        txtMessageFilename.Text = Utilities.CleanFileName(subject.Trim.Replace(":", " ") & emailExt) '".txt"
                                                    End If
                                                End If
                                            End If
                                        End If
                                        currentFileName = "Outlook Email (.msg)"
                                        'If _useMFiles Then
                                        '    mailItem.SaveAs(_profileConsoleTempPath + GetRefNumCboText() & emailExt)
                                        'Else
                                        mailItem.SaveAs(_profileConsoleTempPath + "EMail" & emailExt)
                                        'End If

                                    Catch ex As System.Exception
                                        failedFiles.Add(currentFileName)
                                        If currentFileName = "Outlook Email (.msg)" Then
                                            txtMessageFilename.Text = String.Empty
                                        Else
                                            dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                                        End If
                                        MsgBox("DragDrop error on file " & currentFileName & ": " & ex.Message)
                                        db.WriteLog("DragDrop", ex)
                                    End Try
                                End If
                            Next
                        End If
                    End If
                    'End If ' If e.Data.GetDataPresent(DataFormats.Text) Then
                Else
                    ClearDragDropTab()
                End If
            Catch ex As System.Exception
                db.WriteLog("DragDrop", ex)
                MessageBox.Show(ex.Message)
            Finally
                mailItem = Nothing
                Try
                    Marshal.ReleaseComObject(mailItem)
                Catch

                End Try
            End Try
            If txtMessageFilename.Text.Length > 0 Then btnFilesSave.Enabled = True
            If failedFiles.Count > 0 Then
                Dim msg As String = "These attachments had problems, and were not included in Drag-and-Drop: " & Environment.NewLine
                For Each item As String In failedFiles
                    msg = msg & "    " & item & ", " & Environment.NewLine
                Next
                msg = msg.Remove(msg.Length - 4, 4) 'trailing comma
                msg = msg & Environment.NewLine
                msg = msg & "To start over, please press the Clear button and try Drag-and-Drop again."
                MsgBox(msg)
            End If
        End If
    End Sub

    Private Function getPfileName(theFile As String)
        Dim xl As Excel.Application
        Dim w As Excel.Workbook

        xl = Utilities.GetExcelProcess()
        w = xl.Workbooks.Open(theFile,, True)

        Dim _BuiltInProperties As Object = w.BuiltinDocumentProperties

        If Not _BuiltInProperties Is Nothing Then
            Return _BuiltInProperties("Category").Value
        Else
            Return String.Empty
        End If

        If Not w Is Nothing Then
            w.Close()
        End If

        If Not xl Is Nothing Then
            xl.Quit()
        End If
    End Function
    Private Function DragDropForFilesCheck(dataObject As System.Windows.Forms.DataObject,
                                           ByRef filePaths As List(Of String)) As Integer
        '0 is error
        '1 is yes, drag drop for files,
        '2 is no, drag drop for email,
        Try
            Dim isEmail As Boolean = False
            Dim draggedFileName As String = String.Empty
            '//get fileName of file saved on disk
            Dim fileNames = dataObject.GetFileDropList()
            If fileNames.Count = 1 Then
                'If Not _useMFiles Then
                '    MsgBox("Drag-Drop for files requires a refnum that is using M-Files")
                '    Return 0
                'End If
                If Not Directory.Exists(fileNames(0)) Then
                    MsgBox("Cannot find folder " & fileNames(0))
                    Return 0
                Else
                    draggedFileName = fileNames(0)
                End If
            ElseIf fileNames.Count < 1 Then
                If dataObject.GetDataPresent("FileGroupDescriptor") Then
                    isEmail = True
                Else
                    Return 0
                End If
                'Else
                '    If Not _useMFiles Then
                '        MsgBox("Drag-Drop for files requires a refnum that is using M-Files")
                '        Return 0
                '    End If
            End If

            'now 
            If Not isEmail Then
                filePaths = New List(Of String)
                If fileNames.Count > 1 Then
                    For Each f In fileNames
                        filePaths.Add(f)
                    Next
                Else
                    filePaths.Add(fileNames(0))
                End If
                'DragDropNonEmailToMFiles(filePaths)
                Return 1
            Else
                Return 2
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function


    Private Sub DeleteAllButRefiningValidationFileSiblings()
        Dim siblingFiles() As String
        Try
            Dim filesList = ConfigurationManager.AppSettings("RefiningValidationFileSiblings").ToString()
            siblingFiles = filesList.Split("^")
        Catch
            siblingFiles = "".Split("^")
        End Try
        Dim dirInfo As New DirectoryInfo(_profileConsoleTempPath)
        Dim files() As FileInfo = dirInfo.GetFiles()
        For i As Integer = 0 To files.Count - 1
            Try
                Dim deleteFlag As Boolean = True
                If files(i).Name = "FL16Macros_IDR.xlsm" Then
                    deleteFlag = False
                End If
                For j As Integer = 0 To siblingFiles.Length - 1
                    If files(i).Name.Trim().ToUpper() = Utilities.ExtractFileName(siblingFiles(j).Trim().ToUpper()) Then
                        deleteFlag = False
                        Exit For
                    End If
                Next
                If deleteFlag Then
                    Kill(files(i).FullName)
                End If
            Catch exDelete As System.Exception
                db.WriteLog("RefiningValidationFileSiblings", exDelete)
                Debug.Print(exDelete.Message)
            End Try
        Next
        If Directory.Exists(_profileConsoleTempPath & "Extracted Files") Then
            dirInfo = New DirectoryInfo(_profileConsoleTempPath & "Extracted Files")
            Dim subfolders As DirectoryInfo() = dirInfo.GetDirectories()
            For i As Integer = 0 To subfolders.Count - 1
                Try
                    subfolders(i).Delete(True)
                Catch
                End Try
            Next
            files = dirInfo.GetFiles()
            For i As Integer = 0 To files.Count - 1
                Try
                    Kill(files(i).FullName)
                Catch
                End Try
            Next
        End If
    End Sub
    Private Function DragDropNonEmailToMFiles(filesPaths As List(Of String)) As Boolean
        Dim currentFileName As String = String.Empty
        Dim failedFiles As New List(Of String)
        Dim rowHasBeenAdded As Boolean = True
        Dim result As Boolean = False
        For Each path As String In filesPaths
            Try
                Dim zext As String = String.Empty
                Dim ext As String = String.Empty
                rowHasBeenAdded = False
                currentFileName = Utilities.ExtractFileName(path)

                Try
                    ext = Utilities.GetFileExtension(path)
                Catch
                    ext = ""
                End Try
                Dim invalidMsg As String = String.Empty
                Select Case ext.ToUpper()
                    Case "LNK"
                        invalidMsg = "We are not currently saving .lnk files to M-Files"
                    Case "URL"
                        invalidMsg = "We are not currently saving .url files to M-Files"
                    Case "INI"
                        invalidMsg = "We are not currently saving .ini files to M-Files"
                    Case "EXE"
                        invalidMsg = "We are not currently saving .exe files to M-Files"
                    Case "DLL"
                        invalidMsg = "We are not currently saving .dll files to M-Files"
                    Case "ZIP"
                        invalidMsg = "We are not currently saving .zip files to M-Files. Please extract the files from the zip and drag them into Console."
                    Case "ZIPX"
                        invalidMsg = "We are not currently saving .zipx files to M-Files"
                    Case Else

                End Select
                If invalidMsg.Length > 0 Then
                    MsgBox(invalidMsg)
                    failedFiles.Add(currentFileName)
                    result = False
                    'ClearDragDropTab()
                    'Return False
                Else
                    'If ext.ToUpper = "ZIP" Then
                    '    If Dir(_profileConsoleTempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(_profileConsoleTempPath & "\Extracted Files\")
                    '    Dim zfilenames() As String = Directory.GetFiles(_profileConsoleTempPath & "Extracted Files", "*.*")
                    '    For Each sFile In zfilenames
                    '        Try
                    '            File.Delete(sFile)
                    '        Catch ex As System.Exception
                    '            db.WriteLog("DragDrop", ex)
                    '            Debug.Print(ex.Message)
                    '        End Try
                    '    Next
                    '    att.SaveAsFile(_profileConsoleTempPath & "Extracted Files\" & att.FileName)
                    '    Utilities.UnZipFiles(_profileConsoleTempPath & "Extracted Files\" & att.FileName, _profileConsoleTempPath, CompanyPassword)
                    '    File.Delete(_profileConsoleTempPath & "Extracted Files\" & att.FileName)
                    '    OldTempPath = _profileConsoleTempPath & "Extracted Files\"
                    'End If
                    'For Each mfile In Directory.GetFiles(OldTempPath)
                    Dim fileName As String = Utilities.ExtractFileName(path)
                    Dim proposedName As String = fileName
                    'strFile = mfile
                    Dim zr = dgFiles.Rows.Add()
                    rowHasBeenAdded = True
                    Dim zcbcell = New DataGridViewComboBoxCell
                    zcbcell.Items.Add("Do Not Save")
                    zcbcell.Items.Add("Correspondence")
                    zcbcell.Items.Add("Company Correspondence")
                    zcbcell.Items.Add("Drawings")
                    zcbcell.Items.Add("General")
                    zcbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                    dgFiles.Rows(zr).Cells(2) = zcbcell
                    zext = Utilities.GetFileExtension(fileName) ' GetExtension(mfile)
                    Select Case (zext.ToUpper)
                        Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM", "XLSB"
                            zcbcell.Value = "Correspondence"
                        Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG", "VSO"
                            zcbcell.Value = "Drawings"
                        Case Else
                            zcbcell.Value = "General"
                    End Select
                    File.Copy(path, _profileConsoleTempPath & fileName, True)
                    dgFiles.Rows(zr).Cells(0).Value = fileName
                    dgFiles.Rows(zr).Cells(1).Value = proposedName
                    If fileName.Contains("VFFL") Then
                        'strFile = mfile
                        proposedName = "VRFL" + _docsAccess.NextReturnFile(GetRefNumCboText(), GetRefNumCboText()).ToString() + fileName.Substring(5, fileName.Length - 5)
                        dgFiles.Rows(zr).Cells(1).Value = proposedName
                    Else
                        If fileName.StartsWith("VF") Then
                            'strFile = mfile
                            proposedName = "VR" + _docsAccess.NextReturnFile(GetRefNumCboText(), GetRefNumCboText()).ToString() + fileName.Substring(3, fileName.Length - 3)
                            dgFiles.Rows(zr).Cells(1).Value = proposedName '"VR" & NextVR() & "-" & mfile.Substring(4, Len(mfile) - 4)
                        ElseIf fileName.ToUpper.Contains("RETURN") Then
                            Dim replace As Integer = fileName.ToUpper().IndexOf("RETURN") + 6
                            proposedName = fileName.Substring(0, replace)
                            proposedName += _docsAccess.NextReturnFile(GetRefNumCboText(), GetRefNumCboText()).ToString
                            proposedName += fileName.Substring(replace, fileName.Length - replace)
                            dgFiles.Rows(zr).Cells(1).Value = proposedName 'mfile.Substring(0, mfile.Length - 4) & strNextRetNum & "." & zext
                        Else
                            'same as fileName
                            'strFile = mfile.Substring(0, mfile.Length - 4) & "." & zext
                        End If
                    End If

                    'If mfile.ToUpper.Contains("RETURN") Then
                    '    dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & strNextRetNum & "." & zext
                    'Else
                    '    dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & "." & zext
                    'End If
                    ''Next

                End If
            Catch ex As Exception
                result = False
                MsgBox("Error in DragDropNonEmail(): " & ex.Message)
                failedFiles.Add(currentFileName)
                If rowHasBeenAdded Then
                    dgFiles.Rows.RemoveAt(dgFiles.Rows.Count - 1)
                End If
                'ClearDragDropTab()
            End Try
        Next
        txtMessageFilename.Text = String.Empty
        txtMessageFilename.Enabled = False
        optCompanyCorr.Checked = False
        optCompanyCorr.Enabled = False
        If dgFiles.Rows.Count > 0 Then
            btnFilesSave.Enabled = True
        Else
            btnFilesSave.Enabled = False
        End If


        If failedFiles.Count > 0 Then
            Dim msg As String = "These attachments had problems, and were not included in Drag-and-Drop: " & Environment.NewLine
            For Each item As String In failedFiles
                msg = msg & "    " & item & ", " & Environment.NewLine
            Next
            msg = msg.Remove(msg.Length - 4, 4) 'trailing comma
            msg = msg & Environment.NewLine
            msg = msg & "To start over, please press the Clear button and try Drag-and-Drop again."
            MsgBox(msg)
        End If
        Return result
    End Function


    Private Function GetFileName(fn As String) As String
        Return fn.Substring(0, fn.Length - 4)
    End Function

    Private Function GVNextFile(findFile As String, findExt As String) As String
        Dim rtn As Integer = 1
        Dim result As Integer
        For Each r As DataGridViewRow In dgFiles.Rows

            If r.Cells(0).Value.ToString Like findFile & "*." & findExt Then
                If Not r.Cells(1).Value Is Nothing Then
                    Dim tf As String = GetFileName(r.Cells(1).Value).ToString.Substring(GetFileName(r.Cells(1).Value).ToString.Length - 1, 1)
                    If Int32.TryParse(tf, result) Then rtn = rtn + 1

                End If
            End If
        Next
        Return rtn.ToString
    End Function

    Private Function IncrementFileNumber(strFile As String, ext As String) As String

        Dim NewFile As String = strFile & "-A." & ext

        Dim Letters As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        For Each letter In Letters
            If File.Exists(CorrPath & strFile & "-" & letter & ext) Then
                NewFile = strFile & "-" & Chr(Asc(letter) + 1)
            End If
        Next

        Return NewFile

    End Function

    Private Sub dgFiles_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFiles.CellEnter
        T("dgFiles_CellEnter")
        If e.ColumnIndex = 1 Then
            SendKeys.Send("{F4}")
        End If
        SavedFile = dgFiles.CurrentCell.Value
    End Sub

    Private Sub btnGVClear_Click(sender As System.Object, e As System.EventArgs) Handles btnGVClear.Click
        ClearDragDropTab()
    End Sub

    'Private Sub ClearDragDrop()
    '    dgFiles.RowCount = 1
    '    dgFiles.Rows.Clear()
    '    For Each foundFile As String In Directory.GetFiles(TempPath)
    '        Try
    '            File.Delete(foundFile)
    '        Catch
    '        End Try
    '    Next
    '    txtMessageFilename.Text = ""
    '    btnFilesSave.Enabled = False
    'End Sub

    Private Sub tvwCorrespondence_BeforeExpand(sender As System.Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvwCorrespondence.BeforeExpand, tvwDrawings.BeforeExpand, tvwClientAttachments.BeforeExpand, tvwClientTools.BeforeExpand, tvwCompCorr.BeforeExpand, tvwCorrespondence2.BeforeExpand, tvwDrawings2.BeforeExpand, tvwClientAttachments2.BeforeExpand, tvwClientTools2.BeforeExpand, tvwCompCorr2.BeforeExpand
        e.Node.Nodes.Clear()
        ' get the directory representing this node
        Dim mNodeDirectory As IO.DirectoryInfo
        mNodeDirectory = New IO.DirectoryInfo(e.Node.Tag.ToString)

        ' add each subdirectory from the file system to the expanding node as a child node
        For Each mDirectory As IO.DirectoryInfo In mNodeDirectory.GetDirectories
            ' declare a child TreeNode for the next subdirectory
            Dim mDirectoryNode As New TreeNode
            ' store the full path to this directory in the child TreeNode's Tag property
            mDirectoryNode.Tag = mDirectory.FullName
            ' set the child TreeNodes's display text
            mDirectoryNode.Text = mDirectory.Name
            ' add a dummy TreeNode to this child TreeNode to make it expandable
            mDirectoryNode.Nodes.Add("*DUMMY*")
            mDirectoryNode.ImageKey = CacheShellIcon(mDirectoryNode.Tag)
            mDirectoryNode.SelectedImageKey = mDirectoryNode.ImageKey & "-open"
            ' add this child TreeNode to the expanding TreeNode
            e.Node.Nodes.Add(mDirectoryNode)
        Next

        ' add each file from the file system that is a child of the argNode that was passed in
        For Each mFile As IO.FileInfo In mNodeDirectory.GetFiles
            ' declare a TreeNode for this file
            Dim mFileNode As New TreeNode
            ' store the full path to this file in the file TreeNode's Tag property
            mFileNode.Tag = mFile.FullName
            ' set the file TreeNodes's display text
            mFileNode.Text = mFile.Name
            mFileNode.ImageKey = CacheShellIcon(mFileNode.Tag)
            mFileNode.SelectedImageKey = mFileNode.ImageKey
            mFileNode.SelectedImageKey = mFileNode.ImageKey & "-open"
            ' add this file TreeNode to the TreeNode that is being populated
            e.Node.Nodes.Add(mFileNode)
        Next

    End Sub

    Private Sub tvwCorrespondence_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvwCorrespondence.NodeMouseDoubleClick, tvwDrawings.NodeMouseDoubleClick, tvwClientAttachments.NodeMouseDoubleClick, tvwCompCorr.NodeMouseDoubleClick, tvwCorrespondence2.NodeMouseDoubleClick, tvwDrawings2.NodeMouseDoubleClick, tvwClientAttachments2.NodeMouseDoubleClick, tvwCompCorr2.NodeMouseDoubleClick
        ' try to open the file
        'If Not _useMFiles Then
        Try
            Process.Start(e.Node.Tag)
        Catch ex As System.Exception
            db.WriteLog("tvwCorrespondence_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try
        'Else
        '    Try
        '        Dim errMsg As String = _docsAccess.OpenFileFromMfiles(Int32.Parse(sender.SelectedNode.Tag), GetRefNumCboText(), GetRefNumCboText())
        '        If errMsg.Length > 0 Then MsgBox(errMsg)
        '    Catch
        '    End Try
        'End If
    End Sub

    Private Sub MainConsole_DragEnter(ByVal sender As Object,
                           ByVal e As System.Windows.Forms.DragEventArgs) _
                           Handles MyBase.DragEnter, ConsoleTabs.DragEnter, tvwCorrespondence.DragEnter, tvwDrawings.DragEnter, tvwCompCorr.DragEnter, tvwCompCorr2.DragEnter, tvwClientAttachments.DragEnter, tvwClientTools.DragEnter, tvwCorrespondence2.DragEnter, tvwDrawings2.DragEnter, tvwClientAttachments2.DragEnter, tvwClientTools2.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnAddIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnAddIssue.Click
        ChecklistAddNewIssue()
    End Sub

    Private Sub ChecklistAddNewIssue()
        txtIssueID.Text = ""
        txtIssueName.Text = ""

        txtDescription.Text = ""
        txtIssueID.Focus()
        btnAddIssue.Enabled = False

        'IssueForm.DBConn = mDBConnection
        'IssueForm.RefNum = RefNum
        'IssueForm.StudyType = StudyType
        'IssueForm.UserName = mUserName
        'IssueForm.Show()
        txtDescription.ReadOnly = False
    End Sub


    Private Sub lstVFNumbers_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstVFNumbers.Click
        T("lstVFNumbers_SelectedIndexChanged")
        Me.Cursor = Cursors.WaitCursor
        SetCorr()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnHelp_Click(sender As System.Object, e As System.EventArgs) Handles btnHelp.Click



        Dim strHelpFilePath As String

        strHelpFilePath = My.Settings.Item("HelpPath")

        If File.Exists(strHelpFilePath) Then
            Process.Start(strHelpFilePath)
        Else
            MessageBox.Show("Help file missing", "Missing")
        End If
    End Sub

    Private Sub btnOpenValFax_Click(sender As System.Object, e As System.EventArgs)

        ' This routine will look for a personal ValFax for this user.
        ' If it does not exist, give a message to the user.
        ' If it does exist, open it in Word
        Dim docMyValFax As Word.Document

        Dim strTemplateFile As String
        Dim strDirResult As String

        'On Error GoTo MyValFaxError

        ' If they have a "WithSubstitutions" template,
        ' Call ValFax_Build and do not do anything else in this routine.
        strTemplateFile = Dir(TemplatePath & "MyValFax\WithSubstitutions\" & mUserName & ".doc")
        If strTemplateFile > "" Then
            'If _useMFiles Then
            '    ValFax_Build_Mfiles(strTemplateFile)
            'Else
            ValFax_Build_Legacy(strTemplateFile)
            'End If

            Exit Sub
        End If

        ' No WithSubtitutions template, look for a vanilla template.
        strDirResult = Dir(TemplatePath & "MyValFax\" & mUserName & ".doc")
        If strDirResult = "" Then
            MsgBox("No MyValFax was found for " & mUserName & ". Please create the Word document " & TemplatePath & "MyValFax\" & mUserName & ".doc. See Joe Waters if you have questions.", vbOKOnly)
            'gblnTemplateOK = False
            Exit Sub
        End If

        ' Found a vanilla template, process it.
        strTemplateFile = strDirResult
        Dim MSWord As New Word.Application

        ' Make Word visible
        MSWord.Visible = True
        ' Open the document
        ' This was .Add which added a new document based on a .dot
        ' Changed it to open followed by .Run of AutoNew.


        ' Open the document
        docMyValFax = MSWord.Documents.Open(TemplatePath & "MyValFax\" & strTemplateFile)

        ' Cut the connection.
        MSWord = Nothing


    End Sub

    Private Sub btnLog_Click(sender As System.Object, e As System.EventArgs)
        ' These are the "file" buttons on the right of the
        ' Validation Summary tab.
        ' Basically, here we just open the path/file stored in the tag
        ' property of the button.
        ' This property was set by SetFileButtons earlier when the Refnum
        ' was set/changed.
        Dim wb As Object
        Dim xl As Excel.Application


        Try
            xl = Utilities.GetExcelProcess()
            xl.Visible = True
            wb = xl.Workbooks.Open("\\dallas2\Data\Data\Study\" & ConsoleVertical.ToString() & "\" & Get4DigitYearFromCboStudy.ToString() & "\" & GetStudyRegionFromCboStudy.ToString() & "\Refinery\" & GetRefNumCboText() & "\" & GetRefNumCboText() & "_Log.xls")
            xl.Visible = True

            ' Next line is important.
            ' If you don't do this, if the user closes Excel then clicks a file button
            ' again, you will get an Excel window with just the 'frame'. The inside
            ' of the 'window' will be empty!
            xl = Nothing

        Catch ex As System.Exception
            MsgBox("Unable to open file. If you have Excel open, and are editing a cell, get out of that mode and retry. ", vbOKOnly, "Unable to open file")
            db.WriteLog("btnLOG_Click", ex)
        End Try
    End Sub

    Private Sub MainConsole_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            If Not Spawn Then
                SaveLastStudy()
                SaveCboSettings()
            End If

            'Dim ProfileConsole2014Path As String = ProfilePath + "Console2014\"
            'If Directory.Exists(ProfileConsole2014Path) Then
            '    'Delete all files from the Directory
            '    For Each filepath As String In Directory.GetFiles(ProfileConsole2014Path)
            '        File.Delete(filepath)
            '    Next
            'End If

            'If My.Computer.FileSystem.DirectoryExists(ProfileConsole2014Path) Then
            '    My.Computer.FileSystem.DeleteDirectory(ProfileConsole2014Path, FileIO.DeleteDirectoryOption.DeleteAllContents)
            'End If

        Catch
        End Try
        End
        'could get an "Error creating window handle" error here...
    End Sub

    Private Sub BuildGradeGrid()
        T("BuildGradeGrid")
        Dim params As New List(Of String)
        'params = New List(Of String)
        'params.Add("RefNum/" + GetRefNumCboText())
        'params.Add("Consultant/" & cboConsultant.SelectedItem)
        'ds = db.ExecuteStoredProc("Console." & "GetValidatorData", params)
        'If ds.Tables.Count > 0 Then dgGrade.DataSource = ds.Tables(0)

        btnSectionClick()
    End Sub

    Private Sub lstVFFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstVFFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstVFFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub lstVRFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstVRFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstVRFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVRFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVRFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub lstReturnFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstReturnFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstReturnFiles.SelectedItem.ToString.IndexOf("|")
        file = lstReturnFiles.SelectedItem.ToString.Substring(FindTab + 1, lstReturnFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub btnReceiptAck_Click(sender As System.Object, e As System.EventArgs) Handles btnReceiptAck.Click
        Dim strDirResult As String
        Dim strDateTimeToPrint As String

        strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        If strDateTimeToPrint = "" Then
            MsgBox("Receipt Acknowledgement was canceled.")
            Exit Sub
        End If

        'If _useMFiles Then
        '    Dim fullPath As String = _profileConsoleTempPath & "VA" & Me.lstVFNumbers.Text & ".txt"
        '    Using objWriter As New System.IO.StreamWriter(fullPath, True)
        '        objWriter.WriteLine(mUserName)
        '        objWriter.WriteLine(strDateTimeToPrint)
        '        objWriter.Close()
        '    End Using
        '    Try
        '        Dim deliverable As Integer = 12
        '        If _docsAccess.UploadDocToMfiles(fullPath, 52, GetRefNumCboText(), deliverable, -1, DateTime.Today) Then
        '            MsgBox("VA" & Me.lstVFNumbers.Text & " was saved to M-Files")
        '        Else
        '            MsgBox("VA" & Me.lstVFNumbers.Text & " was NOT saved to M-Files")
        '        End If
        '        Try
        '            File.Delete(fullPath)
        '        Catch
        '        End Try
        '    Catch ex As Exception
        '        MsgBox("VA" & Me.lstVFNumbers.Text & " was NOT saved to M-Files: " + ex.Message)
        '    End Try
        '    'RefreshMFilesCollections(0)
        'Else
        strDirResult = Dir(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt")
        If strDirResult <> "" Then
            MsgBox("Receipt of VF" & Me.lstVFNumbers.Text & " has already been noted.")
            Exit Sub
        End If

        strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        If strDateTimeToPrint <> "" Then

            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)

            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strDateTimeToPrint)
            objWriter.Close()

            MsgBox("VA" & Me.lstVFNumbers.Text & ".txt has been built.")
        Else
            MsgBox("Receipt Acknowledgement was canceled.")
        End If
        'End If
    End Sub

    Private Sub btnBuildVRFile_Click(sender As System.Object, e As System.EventArgs)

        Dim strDirResult As String
        Dim strTextForFile As String

        strDirResult = Dir(CorrPath & "VR" & Me.lstVFNumbers.Text & ".txt")
        If strDirResult <> "" Then
            MsgBox("There already is a VR" & Me.lstVFNumbers.Text & " file.")
            Exit Sub
        End If

        strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        If strTextForFile <> "" Then
            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)
            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strTextForFile)
            objWriter.Close()
            MsgBox("VR" & Me.lstVFNumbers.Text & ".txt has been built.")
        Else
            MsgBox("VR file build was cancelled.")
        End If

    End Sub

    'Private Sub ValCheckList_ItemCheck(sender As Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles ValCheckList.ItemCheck

    '    Try
    '        If e.CurrentValue = CheckState.Checked Then
    '            e.NewValue = CheckState.Unchecked
    '            UpdateIssue("N")
    '        Else
    '            e.NewValue = CheckState.Checked
    '            UpdateIssue("Y")
    '        End If

    '    Catch ex As System.Exception
    '        db.WriteLog("ValCheckList_ItemCheck", ex)
    '        MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Private Sub ValCheckList_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ValCheckList.SelectedIndexChanged
        T("ValCheckList_SelectedIndexChanged")
        If Not ValCheckList.SelectedItem Is Nothing Then

            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueTitle/" & Me.ValCheckList.SelectedItem.ToString.Trim)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString

                    txtDescription.Text = row("IssueText").ToString

                End If


            End If

        End If



    End Sub

    Public Sub SetTree(tvw As TreeView, mRootPath As String)
        T("SetTree")
        Try
            tvw.Nodes.Clear()
            Dim files() As String = Directory.GetFiles(mRootPath)
            Dim nf As String
            Dim mRootNode As New TreeNode
            For Each f In files
                nf = Path.GetFileName(f)
                If f.Substring(f.Length - 9, 9) <> "Thumbs.db" Then
                    If f.Substring(f.Length - 3, 3) = "lnk" Then
                        f = Utilities.GetFileFromLink(f)
                    End If
                    mRootNode = tvw.Nodes.Add(nf.Replace(" - Shortcut.lnk", "").Replace(".lnk", ""))
                    mRootNode.ImageKey = CacheShellIcon(f)
                    mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
                    mRootNode.Tag = f
                End If
            Next

            tvw.ExpandAll()
            tvw.Refresh()
        Catch ex As System.Exception
            db.WriteLog("SetTree", ex)
        End Try
    End Sub

    Private Sub btnUpdateNotes_Click(sender As System.Object, e As System.EventArgs)


        Dim bIssues As Boolean = False

        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

        If ds.Tables.Count = 0 Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtNotes.Text))

            ds = db.ExecuteStoredProc("Console." & "InsertValidationNotes2014", params)

        Else
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtNotes.Text))

            ds = db.ExecuteStoredProc("Console." & "UpdateValidationNotes2014", params)
        End If

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)


        ds = db.ExecuteStoredProc("Console." & "UpdateComments", params)


        BuildSummarySection()



    End Sub
    Private Sub ClearChecklist()
        tvIssues.Nodes.Clear()
        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtDescription.Text = ""
    End Sub
    Private Sub UpdateCheckList(Optional status As String = "N")

        Dim I As Integer
        Dim row As DataRow
        Dim params As List(Of String)
        Dim intCompleteCount As Integer, intNotCompleteCount As Integer
        Dim node As TreeNode
        RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        RemoveHandler tvIssues.AfterSelect, AddressOf tvIssues_AfterSelect
        RemoveHandler cboCheckListView.SelectionChangeCommitted, AddressOf cboCheckListView_SelectionChangeCommitted
        ValCheckList.Items.Clear()
        tvIssues.Nodes.Clear()

        ' Load the Uncompleted Checklist Item count label
        params = New List(Of String)
        params.Add("RefNum/" + GetRefNumCboText())
        params.Add("Mode/Y")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        params = New List(Of String)
        params.Add("RefNum/" + GetRefNumCboText())
        params.Add("Mode/N")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intNotCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        lblItemCount.Text = intCompleteCount & " Completed    " & intNotCompleteCount & " Not Complete."
        Dim chosenView As String = cboCheckListView.Text
        If chosenView = "All" Or chosenView = "Complete" Then '   status = "Y" Or status = "A" Then
            params = New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            params.Add("Completed/Y")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables.Count > 0 Then
                For I = 0 To ds.Tables(0).Rows.Count - 1
                    row = ds.Tables(0).Rows(I)
                    node = tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))
                    node.Checked = True
                Next
            End If
            'cboCheckListView.SelectedIndex = 1
        End If
        If chosenView = "All" Or chosenView = "Incomplete" Then '   If status = "N" Or status = "A" Then
            params = New List(Of String)
            params.Add("RefNum/" + GetRefNumCboText())
            params.Add("Completed/N")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables.Count > 0 Then
                For I = 0 To ds.Tables(0).Rows.Count - 1
                    row = ds.Tables(0).Rows(I)
                    tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))
                Next
            End If
            'If status = "N" Then
            '    cboCheckListView.SelectedIndex = 0
            'Else
            '    cboCheckListView.SelectedIndex = 2
            'End If
        End If

        AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        AddHandler tvIssues.AfterSelect, AddressOf tvIssues_AfterSelect
        AddHandler cboCheckListView.SelectionChangeCommitted, AddressOf cboCheckListView_SelectionChangeCommitted
        'cboCheckListView.SelectedIndex = 0
    End Sub

    'ORIG:
    'Private Sub UpdateCheckList(Optional status As String = "N")

    '    Dim I As Integer
    '    Dim row As DataRow
    '    Dim params As List(Of String)
    '    Dim intCompleteCount As Integer, intNotCompleteCount As Integer
    '    Dim node As TreeNode
    '    RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck

    '    ValCheckList.Items.Clear()
    '    tvIssues.Nodes.Clear()

    '    ' Load the Uncompleted Checklist Item count label
    '    params = New List(Of String)
    '    params.Add("RefNum/" + GetRefNumCboText())
    '    params.Add("Mode/Y")
    '    ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)
    '    If ds.Tables.Count > 0 Then
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            intCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
    '        End If
    '    End If

    '    params = New List(Of String)
    '    params.Add("RefNum/" + GetRefNumCboText())
    '    params.Add("Mode/N")
    '    ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)

    '    If ds.Tables.Count > 0 Then
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            intNotCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
    '        End If
    '    End If

    '    lblItemCount.Text = intCompleteCount & " Completed    " & intNotCompleteCount & " Not Complete."

    '    params = New List(Of String)
    '    params.Add("RefNum/" + GetRefNumCboText())
    '    params.Add("Completed/Y")
    '    ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For I = 0 To ds.Tables(0).Rows.Count - 1
    '            row = ds.Tables(0).Rows(I)
    '            node = tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))
    '            If row("Completed") = "Y" Then
    '                node.Checked = True
    '            Else
    '                node.Checked = False
    '            End If
    '        Next
    '    End If
    '    AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
    'End Sub

    Private Sub SetSummaryFileButtons()
        T("SetSummaryFileButtons")
        ClearSummaryFileButtonTags()
        RefNum = GetRefNumCboText()
        If RefNum <> "" Then

            btnPolishRpt.Enabled = False
            'If Not _useMFiles Then
            If lblLastCalcDate.Text.Length > 1 Then btnPolishRpt.Enabled = True
            'Else
            '    Try
            '        btnPolishRpt.Enabled = File.Exists(ConfigurationManager.AppSettings("RefiningPathToPolishSpreadsheet").ToString())
            '    Catch
            '    End Try
            'End If

            If Directory.Exists(DrawingPath) Then
                btnDrawings.Enabled = True
                btnDrawings.Tag = DrawingPath
            Else
                btnDrawings.Enabled = False
            End If

            'If Not _useMFiles Then
            If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_UnitReview.xls") Then
                btnUnitReview.Tag = GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_UnitReview.xls"
                btnUnitReview.Enabled = True
            Else
                btnUnitReview.Enabled = False
            End If
            'Else
            '    If _mfilesFailed Then
            '        btnUnitReview.Enabled = False
            '    Else
            '        'poll MFiles for _UnitReview.xls*  in 777EUR SOLOMON - PARIS under General Refinery
            '        Dim errors As String = AssociateButtonWithMFilesGeneralRefineryFile("_UNITREVIEW", "XLS", btnUnitReview)
            '        If errors.Length > 0 Then
            '            MsgBox(errors)
            '        End If
            '    End If
            'End If



            'If Not _useMFiles Then
            '    If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_SF.xls") Then
            '        btnSpecFrac.Enabled = True
            '        btnSpecFrac.Tag = GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_SF.xls"
            '    Else
            '        btnSpecFrac.Enabled = False
            '    End If
            'Else
            '    If _mfilesFailed Then
            '        btnSpecFrac.Enabled = False
            '    Else
            '        'poll MFiles for _SF.xls*  in 777EUR SOLOMON - PARIS under General Refinery
            '        Dim errors As String = AssociateButtonWithMFilesGeneralRefineryFile("_SF", "XLS", btnSpecFrac)
            '        If errors.Length > 0 Then
            '            MsgBox(errors)
            '        End If
            '    End If
            'End If

            'If Not _useMFiles Then
            '    If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_HYC.xls") Then
            '        btnHYC.Enabled = True
            '        btnHYC.Tag = GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_HYC.xls"
            '    Else
            '        btnHYC.Enabled = False
            '    End If
            'Else
            '    If _mfilesFailed Then
            '        btnHYC.Enabled = False
            '    Else
            '        'poll MFiles for _SF.xls*  in 777EUR SOLOMON - PARIS under General Refinery
            '        Dim errors As String = AssociateButtonWithMFilesGeneralRefineryFile("_HYC", "XLS", btnHYC)
            '        If errors.Length > 0 Then
            '            MsgBox(errors)
            '        End If
            '    End If
            'End If


            If File.Exists(RefineryPath & RefNum & "_CT.xlsx") Then
                btnCT.Enabled = True
                btnCT.Tag = RefineryPath & RefNum & "_CT.xlsx"
            Else
                btnCT.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_PA.xls") Then
                btnPA.Enabled = True
                btnPA.Tag = RefineryPath & RefNum & "_PA.xls"
            Else
                btnPA.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_PT.xls") Then
                btnPT.Enabled = True
                btnPT.Tag = RefineryPath & RefNum & "_PT.xls"
            Else
                btnPT.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_RAM.xls") Then
                btnRAM.Enabled = True
                btnRAM.Tag = RefineryPath & RefNum & "_RAM.xls"
            Else
                btnRAM.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_SC.xls") Then
                btnSC.Enabled = True
                btnSC.Tag = RefineryPath & RefNum & "_SC.xls"
            Else
                btnSC.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_VI.xls") Then
                btnVI.Enabled = True
                btnVI.Tag = RefineryPath & RefNum & "_VI.xls"
            Else
                btnVI.Enabled = False
            End If


        End If
        'If _useMFiles Then
        '    btnRefresh.Text = "Open MFiles"
        'Else
        btnRefresh.Text = "Open Explorer"
        'End If
    End Sub

    Private Sub ClearSummaryFileButtonTags()
        T("ClearSummaryFileButtonTags")
        btnSC.Tag = ""
        btnPT.Tag = ""
        btnUnitReview.Tag = ""
        btnSpecFrac.Tag = ""
        btnHYC.Tag = ""
        btnDrawings.Tag = ""
        'btnPolishRpt.Tag = ""
        btnCT.Tag = ""
        btnPA.Tag = ""
        btnPT.Tag = ""
        btnRAM.Tag = ""
        btnVI.Tag = ""
        'btnDataFollder.Tag
        '---------------------------
        btnSC.Enabled = False
        btnPT.Enabled = False
        btnUnitReview.Enabled = False
        btnSpecFrac.Enabled = False
        btnHYC.Enabled = False
        btnDrawings.Enabled = False
        btnPolishRpt.Enabled = False
        btnCT.Enabled = False
        btnPA.Enabled = False
        btnPT.Enabled = False
        btnRAM.Enabled = False
        btnVI.Enabled = False
    End Sub

    Private Function AssociateButtonWithMFilesGeneralRefineryFile(fileNameEndsWith As String,
            fileExtensionStartsWith As String, ByRef btn As Button) As String
        Try
            Dim consoleFilesInfo As New ConsoleFilesInfo(True, _vertical)
            'where does LUB/EUR etc start in refnum?

            Dim studyIndex As Integer = GetRefNumCboText().IndexOf(cboStudy.Text().Substring(0, 3))
            Dim refnumWithoutYear = GetRefNumCboText().Substring(0, studyIndex + 3)
            Dim errors As String = _docsAccess.GetGeneralRefineryDocs(GetRefNumCboText(), GetRefNumCboText(),
                        refnumWithoutYear, consoleFilesInfo, 0, fileNameEndsWith)
            If errors.Length > 0 Then
                Return errors
            Else
                If Not IsNothing(consoleFilesInfo) AndAlso Not IsNothing(consoleFilesInfo.Files) AndAlso
                    consoleFilesInfo.Files.Count > 0 Then
                    For Each fil As ConsoleFile In consoleFilesInfo.Files
                        If fil.FileName.ToUpper().EndsWith(fileNameEndsWith) AndAlso
                                fil.FileExtension.ToUpper().StartsWith(fileExtensionStartsWith) Then
                            btn.Tag = fil.Id
                            btn.Enabled = True
                            Exit For
                        End If
                    Next
                End If
            End If
            Return String.Empty
        Catch ex As Exception
            Return "Error in AssociateButtonWithMFilesFile for button '" + btn.Name + "': " + ex.Message
        End Try
    End Function

    Private Sub PopulateCompCorrTreeView(tvw As TreeView)
        T("PopulateCompCorrTreeView")
        If Directory.Exists(CompCorrPath) Then
            SetTree(tvw, CompCorrPath)
        Else
            Dim new4digitYear As Integer = Get4DigitYearFromCboStudy()
            CompCorrPath = StudyDrive & new4digitYear.ToString() & "\" & StudyRegion & "\Company Correspondence\" & Company & "\"
            SetTree(tvw, CompCorrPath)
        End If

    End Sub

    Private Sub PopulateCorrespondenceTreeView(tvw As TreeView)
        T("PopulateCorrespondenceTreeView")
        If Directory.Exists(CorrPath) Then
            SetTree(tvw, CorrPath)
        Else
            CorrPath = StudyDrive & Get4DigitYearFromCboStudy.ToString() & "\" & StudyRegion + "\" & "Correspondence\" & RefNum
            SetTree(tvw, CorrPath)
        End If

    End Sub

    Private Sub PopulateDrawingTreeView(tvw As TreeView)
        T("PopulateDrawingTreeView")
        If Directory.Exists(DrawingPath) Then
            SetTree(tvw, DrawingPath)
        Else
            DrawingPath = GeneralPath & "Drawings\" 'DrawingPath = StudyDrive & "\" & ConsoleVertical.ToString() & "\" & StudyRegion & "\General\"
            SetTree(tvw, DrawingPath)
        End If

    End Sub

    Private Sub PopulateCATreeView(tvw As TreeView)
        T("PopulateCATreeView")
        If Directory.Exists(ClientAttachmentsPath) Then
            SetTree(tvw, ClientAttachmentsPath)
        End If

    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnJustLooking.Click
        LaunchValidation(True)
    End Sub

    Private Sub btnValidate_Click(sender As System.Object, e As System.EventArgs) Handles btnValidate.Click
        btnValidate.Enabled = False
        LaunchValidation(False)

    End Sub

    Private Sub LaunchValidation(JustLooking As Boolean)
        Try
            'Added to make sure the IDRPath is exist and reachable
            If Directory.Exists(IDRPath) Then
                bJustLooking = JustLooking
                If cboConsultant.Text = "" Then
                    MessageBox.Show("You have not assigned a consultant.")
                    btnValidate.Enabled = True
                Else
                    btnValidate.Enabled = False
                    btnValidate.Text = "Reviewing"
                    'Launch Validation
                    Dim ValidationFile As String
                    Select Case Get4DigitYearFromCboStudy()
                        Case 2016
                            ValidationFile = "FL16Macros_IDR.xlsm"
                        Case 2014
                            ValidationFile = "FL14Macros_IDR.xls"
                        Case 2018
                            ValidationFile = "FL18Macros_IDR.xlsm"
                        Case 2020
                            ValidationFile = "FL20Macros_IDR.xlsm"
                        Case Else
                            ValidationFile = "FL" & Get2DigitYearFromCboStudy().ToString() & "Macros.xls"
                    End Select
                    'If mStudyYear <> "2014" Then
                    '    ValidationFile = "FL" & StudyYear.Substring(2, 2) & "Macros.xls"
                    'Else
                    '    ValidationFile = "FL" & StudyYear.Substring(2, 2) & "Macros_IDR.xls"
                    'End If

                    IDR(bJustLooking, ValidationFile)
                    btnValidate.Enabled = True
                    btnValidate.Text = "IDR"
                    'minumize the Console application
                    Me.WindowState = FormWindowState.Minimized
                End If
            Else
                Throw New Exception("Directory access error: " + IDRPath)
            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "LaunchValidation")
            db.WriteLog("LaunchValidation", ex)
        End Try

    End Sub



    'If Study Changes, then re-populate RefNums
    Private Sub cboStudy_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboStudy.SelectedIndexChanged
        Exit Sub 'ChangedStudy()
    End Sub
    Private Sub cboStudy_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboStudy.SelectionChangeCommitted
        'CboStudyChangedRoutine()
        Me.Cursor = Cursors.WaitCursor
        Try
            Dim refNumToStartWith As String = String.Empty
            For count As Integer = 0 To _lastStudyAndRefnum.Count - 1
                Dim parts As String() = _lastStudyAndRefnum(count).Split(_delim)
                If parts(0) = cboStudy.SelectedItem.ToString() Then
                    refNumToStartWith = parts(1)
                    Exit For
                End If
            Next
            'need to reload these
            cboRefNum.Items.Clear()
            cboCompany.Items.Clear()
            cboRefNum.Text = String.Empty
            cboCompany.Text = String.Empty
            ClearAllControls()

            Dim yearRefnumPlantsInfo As YearRefnumPlantsInfo = ChangeStudy(cboStudy.SelectedItem.ToString(), "")

            If Not IsNothing(yearRefnumPlantsInfo) Then
                For Each refn As String In yearRefnumPlantsInfo.RefNums
                    cboRefNum.Items.Add(refn)
                Next
                For Each cmp As String In yearRefnumPlantsInfo.Plants
                    cboCompany.Items.Add(cmp)
                Next
                If refNumToStartWith.Length > 0 Then
                    If ChangedRefnumOrCompany(refNumToStartWith, "") Then
                        'won't get here if MFiles failed...

                        'these are called in ChangedRefnumOrCompany already
                        'ClearAllControls()
                        'If Not PopulateForm() Then Exit Sub
                        'If Not PopulateTabs() Then Exit Sub
                    End If
                End If
            End If


        Catch ex As Exception
            MsgBox("Console error: " + ex.Message)
        End Try



        Me.Cursor = Cursors.Default
    End Sub
    Private Sub CboStudyChangedRoutine(Optional fuelsAndLubesButtonClicked As Boolean = False)
        T("CboStudyChangedRoutine")
        'Dim oldStudyYear As String =_yearRefnumsAndPlants. _yearRefnumsAndPlants._studyYear
        'cboConsultant.Items.Clear()
        'cboConsultant.Text = String.Empty
        ClearPasswordsAndConsultant()
        Study = cboStudy.Text.Substring(0, 3)
        UpdateLastStudyRefnum(_yearRefnumsAndPlants.GetStudyYear(), GetRefNumCboText())
        Dim studyChangedTo As String = cboStudy.Text
        Dim year As String = "20" + System.Text.RegularExpressions.Regex.Replace(cboStudy.Text, "[^0-9.]", "")
        'If Int32.Parse(year) = 2016 Then
        '    _useMFiles = True
        'Else
        '    _useMFiles = False
        'End If

        Try
            '_mfilesFailed = False
            '_docsAccess = New DocsFileSystemAccess(ConsoleVertical, _useMFiles)
            _docsAccess = New DocsFileSystemAccess(ConsoleVertical, False)
        Catch exMFiles As Exception
            'If exMFiles.Message.Contains("Unable to find Benchmarking Participant") Then
            '    _mfilesFailed = True
            'End If
            'MsgBox(exMFiles.Message + Environment.NewLine + Environment.NewLine + "Please notify your MFiles Administrator to set up this Benchmarking Participant in MFiles.")
            MsgBox(exMFiles.Message)
        End Try
        'If _mfilesFailed Then
        '    MFilesFailedRoutine()
        'Else
        '    UnhideMFilesTabs()
        'End If

        Dim results As YearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangeYear(studyChangedTo, db, GetRefNumCboText())

        If results.SelectedRefNum.Length < 1 And Not IsNothing(_lastStudyAndRefnum) Then
            For count As Integer = 0 To _lastStudyAndRefnum.Count - 1
                Dim parts As String() = _lastStudyAndRefnum(count).Split(_delim)
                If parts(0) = results.SelectedStudyYear AndAlso parts(1).Length > 0 Then
                    If fuelsAndLubesButtonClicked Then
                        results = _yearRefnumsAndPlants.ChangeYear(studyChangedTo, db, btnFuelLube.Text)
                    Else
                        results = _yearRefnumsAndPlants.ChangeYear(studyChangedTo, db, parts(1))
                    End If
                    Exit For
                End If
            Next
        End If

        'Not calling 'ChangedYear(studyChangedTo) because would duplicate cboStudy changes
        'ChangedYear(studyChangedTo)
        'StudyYear = studyChangedTo
        cboCompany.Items.Clear()
        For Each Company As String In results.Plants
            cboCompany.Items.Add(Company)
        Next
        cboRefNum.Items.Clear()
        For Each rnum As String In results.RefNums
            cboRefNum.Items.Add(rnum)
        Next

        ChangeColocRefnum(results)

        BuildTabs()
        'legacy method
        'only changed refnum cbo: ChangedStudy()
        PopulateConsultantDropdown(GetRefNumCboText(), Study, year)

        CheckFuelsLubes()
        CheckForCurrentStudyYear()
    End Sub
    Private Sub CboStudyStartupRoutine(startupInfo As YearRefnumPlantsInfo)
        T("CboStudyStartupRoutine")
        If startupInfo.SelectedStudyYear.Length < 1 Then Exit Sub
        'ConsoleTabs.TabPages.Remove(tabDD)
        ClearPasswordsAndConsultant()
        Study = startupInfo.SelectedStudyYear.Substring(0, 3)
        UpdateLastStudyRefnum(_yearRefnumsAndPlants.GetStudyYear(), startupInfo.SelectedStudyYear)
        Dim studyChangedTo As String = startupInfo.SelectedStudyYear
        Dim year As String = "20" + System.Text.RegularExpressions.Regex.Replace(startupInfo.SelectedStudyYear, "[^0-9.]", "") ' get only the numbers from cboStudy
        'If Int32.Parse(year) = 2016 Then
        '    _useMFiles = True
        'Else
        '    _useMFiles = False
        'End If

        Try
            '_mfilesFailed = False
            '_docsAccess = New DocsFileSystemAccess(ConsoleVertical, _useMFiles)
            _docsAccess = New DocsFileSystemAccess(ConsoleVertical, False)
        Catch exMFiles As Exception
            'If exMFiles.Message.Contains("Unable to find Benchmarking Participant") Then
            '    _mfilesFailed = True
            'End If
            'MsgBox(exMFiles.Message + Environment.NewLine + Environment.NewLine + "Please notify your MFiles Administrator to set up this Benchmarking Participant in MFiles.")
            MsgBox(exMFiles.Message)
        End Try

        'If _mfilesFailed Then
        '    MFilesFailedRoutine()
        'Else
        '    UnhideMFilesTabs()
        'End If

        'Dim results As YearRefnumPlantsInfo = _yearRefnumsAndPlants.ChangeYear(studyChangedTo, db, startupInfo.SelectedRefNum)

        If startupInfo.SelectedRefNum.Length < 1 Then
            For count As Integer = 0 To _lastStudyAndRefnum.Count - 1
                Dim parts As String() = _lastStudyAndRefnum(count).Split(_delim)
                If parts(0) = startupInfo.SelectedStudyYear AndAlso parts(1).Length > 0 Then
                    startupInfo = _yearRefnumsAndPlants.ChangeYear(studyChangedTo, db, parts(1))
                    Exit For
                End If
            Next
        End If

        'Not calling 'ChangedYear(studyChangedTo) because would duplicate cboStudy changes
        'ChangedYear(studyChangedTo)
        'StudyYear = studyChangedTo
        cboCompany.Items.Clear()
        If cboCompany.Items.Count < 1 Then
            For Each Company As String In startupInfo.Plants
                cboCompany.Items.Add(Company)
            Next
        End If
        cboRefNum.Items.Clear()
        If cboRefNum.Items.Count < 1 Then
            For Each rnum As String In startupInfo.RefNums
                cboRefNum.Items.Add(rnum)
            Next
        End If

        ChangeColocRefnum(startupInfo)

        BuildTabs()
        'legacy method
        'only changed refnum cbo: ChangedStudy()
        PopulateConsultantDropdown(GetRefNumCboText(), Study, year)


    End Sub

    'Method to populate the labels for Contacts
    Private Sub FillContacts()
        T("FillContacts")
        Try
            ClearFields()

            If Not RefineryContact Is Nothing Then

                lblRefCoName.Text = RefineryContact.FullName
                lblRefCoEmail.Text = RefineryContact.Email
                lblRefCoPhone.Text = RefineryContact.Phone
                lblMaintMgrName.Text = RefineryContact.MaintMgrName
                lblMaintMgrEmail.Text = RefineryContact.MaintEmail


                lblOpsMgrName.Text = RefineryContact.OpsMgrName
                lblOpsMgrEmail.Text = RefineryContact.OpsEmail

                lblTechMgrName.Text = RefineryContact.TechMgrName
                lblTechMgrEmail.Text = RefineryContact.TechEmail

                lblRefMgrName.Text = RefineryContact.RefMgrName
                lblRefMgrEmail.Text = RefineryContact.RefEmail


                lblRefAltName.Text = RefineryContact.AltFullName
                lblRefAltEmail.Text = RefineryContact.AltEmail
                lblRefAltPhone.Text = RefineryContact.AltPhone


            End If

            If Not InterimContact Is Nothing Then
                lblInterimName.Text = InterimContact.FirstName & " " & InterimContact.LastName
                lblInterimEmail.Text = InterimContact.Email
                lblInterimPhone.Text = InterimContact.Phone
            End If




            lblName.Text = CompanyContact.FirstName & " " & CompanyContact.LastName
            lblEmail.Text = CompanyContact.Email
            lblPhone.Text = CompanyContact.Phone
            btnCompanyPW.Text = CompanyContact.Password
            CompanyPassword = btnCompanyPW.Text

            lblAltName.Text = AltCompanyContact.FirstName & " " & AltCompanyContact.LastName
            lblAltEmail.Text = AltCompanyContact.Email
            lblAltPhone.Text = AltCompanyContact.Phone

            'btnReturnPW.Text = GetReturnPassword(Trim(UCase(RefNum)), "fuelslubes")

            'ReturnPassword = btnReturnPW.Text

        Catch ex As System.Exception
            db.WriteLog("FillContacts", ex)
        End Try

    End Sub

    Private Function GetReturnPassword(RefNum As String, salt As String) As String
        Return Utilities.XOREncryption(Trim(UCase(RefNum)), salt)
    End Function

    Public Sub BuildTabs()
        T("BuildTabs")
        Try
            'cboRefNum.Text = GetRefNumCboText()
            Company = GetCompanyName(GetRefNumCboText())
            If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
            SetPaths()
            GetCompanyContactInfo()
            GetPricingContactInfo()
            GetAltCompanyContactInfo()
            GetInterimContactInfo()
            GetContacts()
            FillContacts()
            GetConsultants()
            BuildSummarySection()

            BuildNotesTab()
            BuildContinuingIssuesTab()

            BuildCheckListTab("")
            cboCheckListView.SelectedIndex = 0

            BuildCorrespondenceTab()
            BuildSupportTab()
            CheckFuelsLubes()
            CheckPreliminaryPricing()
            btnValidate.Enabled = True

        Catch ex As System.Exception
            db.WriteLog("BuildTabs", ex)
        End Try
    End Sub

    Private Sub btnDirRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnDirRefresh.Click
        'ConsoleTabs.TabPages.Remove(tabSS)
    End Sub

    Private Sub chkSQL_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSQL.CheckedChanged
        If Not chkSQL.Checked Then
            Label25.Text = "Clippy Query"
        Else
            Label25.Text = "SQL Query"
        End If
    End Sub

    Private Sub btnKillProcesses_Click(sender As System.Object, e As System.EventArgs) Handles btnKillProcesses.Click
        Dim dr As New DialogResult()
        dr = MessageBox.Show("This will kill all OPEN Excel and Word Documents.  Please <SAVE> any Word or Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel)
        If dr = DialogResult.OK Then
            Utilities.KillProcesses("WinWord")
            Utilities.KillProcesses("Excel")
        End If
    End Sub

#End Region

    Private Sub CheckForCurrentStudyYear()
        T("CheckForCurrentStudyYear")
        If cboStudy.Text.Length = 5 Then
            Try
                CurrentStudyYear = ConfigurationManager.AppSettings("RefiningCurrentStudyYear")
            Catch
            End Try
            'StudyYear = cboStudy.Text.Substring(cboStudy.Text.Length - 2, 2)
            If Get4DigitYearFromCboStudy().ToString() <> CurrentStudyYear Then
                cboStudy.BackColor = Color.Yellow
                lblWarning.Visible = True
            Else
                cboStudy.BackColor = Color.White
                lblWarning.Visible = False
            End If

            Study = cboStudy.Text.Substring(0, cboStudy.Text.Length - 2)

            'studyyear is same as cboStudy.Text
            'If (StudyYear > 50) Then
            '    StudyYear = "19" + StudyYear
            'Else
            '    StudyYear = "20" + StudyYear
            'End If
        End If
    End Sub

    Private Function SaveNotes() As Boolean
        Dim params As New List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Consultant/" + UserName)
        params.Add("Issue/" & Utilities.FixQuotes(txtNotes.Text.Replace("/", "-")))

        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)
        If c = 0 Then
            lblStatus.Text = "IDR Notes did not update"
            Return False
        Else
            lblStatus.Text = "IDR Notes Saved...."
            Return True
        End If
    End Function

    Private Sub btnSavePN_Click(sender As System.Object, e As System.EventArgs) Handles btnSavePN.Click
        Me.Cursor = Cursors.WaitCursor
        System.Threading.Thread.Sleep(1000)
        Me.Cursor = Cursors.Default
        Dim text As String = Utilities.FixQuotes(txtNotes.Text.Replace("/", "-"))

        Dim tempRefnum As String = GetRefNumCboText().Replace("LIV", "LUB")
        tempRefnum = tempRefnum.Replace("LEV", "LUB")
        Dim sql As String = "SELECT NOTE from Console.ValidationNotes where COnsultant = '" & UserName & "' And Refnum = '" & tempRefnum & "' and deleted = 0 order by noteid desc"
        db.SQL = sql
        Dim ds As DataSet = db.Execute
        db.SQL = String.Empty
        Dim saved As Boolean = False
        If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("Note").ToUpper() = text.ToUpper() Then saved = True
        End If
        If Not saved Then
            SaveNotes()
        End If

        ClearNotesTab()
        BuildNotesTab()

    End Sub

    Private Sub btnCreatePN_Click(sender As System.Object, e As System.EventArgs) Handles btnCreatePN.Click
        'If Not _useMFiles Then
        PresentersNotesLegacy()
        'Else
        '    PresentersNotesMFiles()
        'End If
    End Sub

    Private Sub PresentersNotesLegacy()
        If File.Exists(CorrPath & "PN_" & cboCompany.Text & ".doc") Then
            Process.Start(CorrPath & "PN_" & cboCompany.Text & ".doc")
        Else
            Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will lock the field.", "Wait", MessageBoxButtons.YesNo)
            If dr = System.Windows.Forms.DialogResult.Yes Then
                Try

                    Dim myText As String = "Presenter Notes for" & vbCrLf & Coloc & vbCrLf & vbCrLf
                    Dim params = New List(Of String)
                    Dim row As DataRow

                    lblStatus.Text = "Creating Presenter's Word file..."
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            row = ds.Tables(0).Rows(0)
                            myText += row("Note").ToString
                        Else
                            myText += ""
                        End If
                    End If
                    Dim strw As New StreamWriter(_profileConsoleTempPath & "PN_" & Coloc & ".txt")

                    strw.WriteLine(myText)
                    strw.Close()
                    strw.Dispose()
                    Dim doc As String = Utilities.ConvertToDoc(_profileConsoleTempPath, _profileConsoleTempPath & "PN_" & Coloc & ".txt")
                    File.Move(_profileConsoleTempPath & "PN_" & cboCompany.Text & ".doc", CorrPath & "PN_" & Coloc & ".doc")
                    File.Delete(_profileConsoleTempPath & "PN_" & cboCompany.Text & ".txt")
                    lblStatus.Text = "Presenter's Word file created and locked..."
                    txtNotes.Text = "~" & txtNotes.Text
                    lblStatus.Text = UpdatePN(RefNum)
                    BuildNotesTab()
                    Process.Start(CorrPath & "PN_" & Coloc & ".doc")
                    btnCreatePN.Text = "Open PN File"

                Catch ex As System.Exception
                    lblStatus.Text = "Error: " & ex.Message
                End Try
            End If
        End If
    End Sub

    Private Sub PresentersNotesMFiles()
        Dim myText As String = "Presenter Notes for" & vbCrLf & Coloc & vbCrLf & vbCrLf
        'pull from MFiles if exists there. Else create.
        'Dim docFileAccess As New DocsFileSystemAccess(VerticalType.OLEFINS)
        Dim docs As ConsoleFilesInfo = _docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant(GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
        For Each item As ConsoleFile In docs.Files
            If item.DeliverableType = "Presenters Notes" Then 'deliverable = 17  'Presenters Notes" Then
                _docsAccess.OpenFileFromMfiles(item.Id, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                Exit Sub
            End If
        Next
        Dim pnTemplatePath As String = String.Empty
        Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will push the notes to MFiles.", "Wait", MessageBoxButtons.YesNo)
        If dr = System.Windows.Forms.DialogResult.Yes Then
            Try
                Dim params = New List(Of String)
                Dim row As DataRow
                lblStatus.Text = "Creating Presenter's Word file..."
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        myText += row("Note").ToString
                    Else
                        myText += ""
                    End If
                End If
                Dim strw As New StreamWriter(_profileConsoleTempPath & "PN_" & Coloc & ".txt")
                strw.WriteLine(myText)
                strw.Close()
                strw.Dispose()
                Dim docPath As String = Utilities.ConvertToDoc(_profileConsoleTempPath, _profileConsoleTempPath & "PN_" & Coloc & ".txt")
                Dim strfilename As String = Utilities.ExtractFileName(docPath)
                Dim deliverable As Integer = 17
                If _docsAccess.UploadDocToMfiles(_profileConsoleTempPath + strfilename, 52, GetRefNumCboText(), deliverable, -1, DateTime.Today) Then
                    'then reopen from MFIles and check out, delete local file at end of this method
                    Dim mFilesFilename As String = Utilities.FileNameWithoutExtension(strfilename)
                    '_docsAccess.OpenFileFromMfiles(mFilesFilename, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                    Dim result As String = _docsAccess.OpenFileFromMfiles(mFilesFilename, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                    If result.Length > 0 Then
                        Throw New Exception(result)
                    End If
                End If
                Try
                    File.Delete(_profileConsoleTempPath + strfilename)
                Catch 'not critical that it get deleted at this second
                End Try
                Try
                    File.Delete(_profileConsoleTempPath & "PN_" & Coloc & ".txt")
                Catch 'not critical that it get deleted at this second
                End Try

                lblStatus.Text = "Presenter's Word file created and locked..."
                txtNotes.Text = "~" & txtNotes.Text
                lblStatus.Text = UpdatePN(RefNum)
                BuildNotesTab()
                btnCreatePN.Text = "Open PN File"
                'RefreshMFilesCollections(0)
            Catch ex As System.Exception
                lblStatus.Text = "Error: " & ex.Message
            End Try
            txtNotes.ReadOnly = True
        End If
        ''LEGACY

        'If File.Exists(CorrPath & "PN_" & cboCompany.Text & ".doc") Then
        '    Process.Start(CorrPath & "PN_" & cboCompany.Text & ".doc")
        'Else
        '    Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will lock the field.", "Wait", MessageBoxButtons.YesNo)
        '    If dr = System.Windows.Forms.DialogResult.Yes Then
        '        Try

        '            Dim myText As String = "Presenter Notes for" & vbCrLf & Coloc & vbCrLf & vbCrLf
        '            Dim params = New List(Of String)
        '            Dim row As DataRow

        '            lblStatus.Text = "Creating Presenter's Word file..."
        '            params.Add("RefNum/" + RefNum)
        '            ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

        '            If ds.Tables.Count > 0 Then
        '                If ds.Tables(0).Rows.Count > 0 Then
        '                    row = ds.Tables(0).Rows(0)
        '                    myText += row("Note").ToString
        '                Else
        '                    myText += ""
        '                End If
        '            End If
        '            Dim strw As New StreamWriter(_profileConsoleTempPath & "PN_" & Coloc & ".txt")

        '            strw.WriteLine(myText)
        '            strw.Close()
        '            strw.Dispose()
        '            Dim doc As String = Utilities.ConvertToDoc(_profileConsoleTempPath, _profileConsoleTempPath & "PN_" & Coloc & ".txt")
        '            File.Move(_profileConsoleTempPath & "PN_" & cboCompany.Text & ".doc", CorrPath & "PN_" & Coloc & ".doc")
        '            File.Delete(_profileConsoleTempPath & "PN_" & cboCompany.Text & ".txt")
        '            lblStatus.Text = "Presenter's Word file created and locked..."
        '            txtNotes.Text = "~" & txtNotes.Text
        '            lblStatus.Text = UpdatePN(RefNum)
        '            BuildNotesTab()
        '            Process.Start(CorrPath & "PN_" & Coloc & ".doc")
        '            btnCreatePN.Text = "Open PN File"

        '        Catch ex As System.Exception
        '            lblStatus.Text = "Error: " & ex.Message
        '        End Try
        '    End If
        'End If
    End Sub

    Private Sub btnUnlockPN_Click(sender As System.Object, e As System.EventArgs) Handles btnUnlockPN.Click
        'If Not _useMFiles Then
        If txtNotes.Text.Substring(0, 1) = "~" Then
            txtNotes.Text = txtNotes.Text.Substring(1, txtNotes.Text.Length - 1)
            btnCreatePN.Text = "Create Final PN File"
            lblStatus.Text = UpdatePN(GetRefNumCboText())
            lblStatus.Text = "PN File unlocked..."
            btnUnlockPN.Enabled = False
        End If
        'End If
    End Sub

    Private Sub btnCreateMissingPNFiles_Click(sender As System.Object, e As System.EventArgs) Handles btnCreateMissingPNFiles.Click
        PresenterNotesReport()
    End Sub

    Private Sub lblName_DoubleClick(sender As System.Object, e As System.EventArgs)

        Dim Contacts As String = Nothing
        Contacts = lblName.Text & vbTab & lblEmail.Text & vbTab & lblPhone.Text & vbCrLf
        Contacts += lblAltName.Text & vbTab & lblAltEmail.Text & vbTab & lblAltPhone.Text
        Clipboard.SetText(Contacts)
        MessageBox.Show("Data Copied to Clipboard")

    End Sub

    Private Sub lblRefCoName_DoubleClick(sender As System.Object, e As System.EventArgs)

        Dim Contacts As String = Nothing
        Contacts = lblRefCoName.Text & vbTab & lblRefCoEmail.Text & vbTab & lblRefCoPhone.Text & vbCrLf
        Contacts += lblRefAltName.Text & vbTab & lblRefAltEmail.Text & vbTab & lblRefAltPhone.Text & vbCrLf
        Contacts += lblOpsMgrName.Text & vbTab & lblOpsMgrEmail.Text & vbCrLf
        Contacts += lblMaintMgrName.Text & vbTab & lblMaintMgrEmail.Text & vbCrLf
        Contacts += lblTechMgrName.Text & vbTab & lblTechMgrEmail.Text & vbCrLf
        Clipboard.SetText(Contacts)
        MessageBox.Show("Data Copied to Clipboard")

    End Sub

    Private Sub btnBug_Click(sender As System.Object, e As System.EventArgs) Handles btnBug.Click
        '      Dim bug As New frmBug(cboStudy.Text, cboStudy.Items, GetRefNumCboText(), UserName)
        'bug.Show()
        Dim bug As New frmBug(Main.ConsoleVertical, GetRefNumCboText(), UserName, db.ConnectionString)
        bug.Show()
    End Sub

    Private Sub OpenEmail(MsgTo As String)
        If MsgTo.Length > 10 And MsgTo.Contains("@") Then
            Utilities.SendEmail(MsgTo, "", "", "", Nothing, True)
        Else
            MessageBox.Show("Bad Email Format", "Warning...")
        End If
    End Sub

    Private Sub OpenEmailExtended()
        Dim toList As String = String.Empty
        Dim ccList As String = String.Empty
        Dim bccList As String = String.Empty

        PrepEmailToCcBcc(cboCCEmail1, lblEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboACEmail2, lblAltEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboICEmail, lblInterimEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboRCEmail, lblRefCoEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboRACEmail, lblRefAltEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboPCEmail, lblPricingContactEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboRMEmail, lblRefMgrEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboOMEmail, lblOpsMgrEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboMMEmail, lblMaintMgrEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboTMEmail, lblTechMgrEmail, toList, ccList, bccList)
        PrepEmailToCcBcc(cboPCCEmail, lblPCCEmail, toList, ccList, bccList)
        Utilities.SendEmail(toList, ccList, "", "", Nothing, True, bccList)
    End Sub

    Private Sub PrepEmailToCcBcc(cbo As ComboBox, emailLabel As TextBox, ByRef toList As String,
                                      ByRef ccList As String, ByRef bccList As String)
        If Not IsNothing(cbo) AndAlso cbo.Text.Trim().Length > 0 Then
            If emailLabel.Text.Length > 0 AndAlso emailLabel.Text.Contains("@") Then
                Select Case cbo.Text.Trim().ToUpper()
                    Case "TO"
                        toList += emailLabel.Text + ";"
                    Case "CC"
                        ccList += emailLabel.Text + ";"
                    Case "BCC"
                        bccList += emailLabel.Text + ";"
                    Case Else

                End Select
            End If
        End If
    End Sub

    Private Sub CCEmail1_Click(sender As Object, e As EventArgs) Handles PCCEmail.Click, PCEmail.Click, CCEmail1.Click, ACEmail2.Click, ICEmail.Click, RCEmail.Click, RACEmail.Click, RMEmail.Click, OMEmail.Click, MMEmail.Click, TMEmail.Click
        OpenEmailExtended()
        Exit Sub
        Select Case (CType(sender, PictureBox).Name)
            Case "CCEmail1"
                If lblEmail.Text.Contains("@") And lblEmail.Text.Length > 9 Then OpenEmail(lblEmail.Text)
            Case "ACEmail2"
                If lblAltEmail.Text.Contains("@") And lblAltEmail.Text.Length > 9 Then OpenEmail(lblAltEmail.Text)
            Case "ICEmail"
                If lblInterimEmail.Text.Contains("@") And lblInterimEmail.Text.Length > 9 Then OpenEmail(lblInterimEmail.Text)
            Case "RCEmail"
                If lblRefCoEmail.Text.Contains("@") And lblRefCoEmail.Text.Length > 9 Then OpenEmail(lblRefCoEmail.Text)
            Case "RACEmail"
                If lblRefAltEmail.Text.Contains("@") And lblRefAltEmail.Text.Length > 9 Then OpenEmail(lblRefAltEmail.Text)
            Case "PCEmail"
                If lblPricingContactEmail.Text.Contains("@") And lblPricingContactEmail.Text.Length > 9 Then OpenEmail(lblPricingContactEmail.Text)
            Case "RMEmail"
                If lblRefMgrEmail.Text.Contains("@") And lblRefMgrEmail.Text.Length > 9 Then OpenEmail(lblRefMgrEmail.Text)
            Case "OMEmail"
                If lblOpsMgrEmail.Text.Contains("@") And lblOpsMgrEmail.Text.Length > 9 Then OpenEmail(lblOpsMgrEmail.Text)
            Case "MMEmail"
                If lblMaintMgrEmail.Text.Contains("@") And lblMaintMgrEmail.Text.Length > 9 Then OpenEmail(lblMaintMgrEmail.Text)
            Case "TMEmail"
                If lblTechMgrEmail.Text.Contains("@") And lblTechMgrEmail.Text.Length > 9 Then OpenEmail(lblTechMgrEmail.Text)
            Case "PCCEmail"
                If lblPCCEmail.Text.Contains("@") And lblPCCEmail.Text.Length > 9 Then OpenEmail(lblPCCEmail.Text)
        End Select
    End Sub

    Private Sub BuildEmailToString()

    End Sub
    Private Sub BuildEmailCcString()

    End Sub
    Private Sub BuildEmailBccString()

    End Sub

    Private Sub btnSS_Click(sender As Object, e As EventArgs) Handles btnSS.Click
        BuildSecureSendTab()
    End Sub

    Private Sub BuildSecureSendTab()
        'If _useMFiles Then
        '    If _mfilesFailed Then Exit Sub
        '    'RefreshMFilesCollections(0)
        '    'RefreshMFilesCollections(1)
        '    'RefreshMFilesCollections(2)
        'End If
        Try
            BuildDirectoriesTab()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        If Not ConsoleTabs.TabPages.Contains(tabSS) Then ConsoleTabs.TabPages.Add(tabSS)
        'ConsoleTabs.SelectTab(tabSS)
        Dim result As String = GetRefineryAppSettingsXmlSetting("chkIDRChecked")
        If result.Length > 0 AndAlso result.ToUpper().Trim() = "TRUE" Then
            chkIDR.Checked = True
        Else
            chkIDR.Checked = False
        End If
    End Sub

    Private Sub btnDBQuery_Click(sender As Object, e As EventArgs)
        If Not ConsoleTabs.TabPages.Contains(tabClippy) Then ConsoleTabs.TabPages.Add(tabClippy)
        ConsoleTabs.SelectTab(tabClippy)
    End Sub

    Private Sub btnQueryQuit_Click(sender As Object, e As EventArgs) Handles btnQueryQuit.Click
        'ConsoleTabs.TabPages.Remove(tabClippy)
    End Sub

    Private Sub btnGradeExit_Click(sender As Object, e As EventArgs) Handles btnGradeExit.Click
        'ConsoleTabs.TabPages.Remove(tabTimeGrade)
    End Sub

    Private Sub lblItemsRemaining_Click(sender As Object, e As EventArgs)
        ConsoleTabs.SelectTab(tabCheckList)

    End Sub

    Private Sub cboNotes_SelectedIndexChanged(sender As Object, e As EventArgs)
        T("cboNotes_SelectedIndexChanged")
        BuildNotesTab()
    End Sub

    Private Sub ClearTimeGrade()
        If Not ConsoleTabs.TabPages.Contains(tabTimeGrade) Then Exit Sub
        dgGrade.DataSource = Nothing
        dgSummary.DataSource = Nothing
        dgHistory.DataSource = Nothing
    End Sub
    Private Sub ShowTimeGrade()
        T("ShowTimeGrade")
        If Not ConsoleTabs.TabPages.Contains(tabTimeGrade) Then ConsoleTabs.TabPages.Add(tabTimeGrade)
        'ConsoleTabs.SelectTab(tabTimeGrade)
        BuildGradeGrid()
        TimeHistory_Load()
        TimeSummary_Load()
    End Sub

    Private Sub btnMain_Click(sender As Object, e As EventArgs) Handles btnMain.Click
        ShowTimeGrade()
    End Sub

    Private Sub txtCompanyPassword_Click_1(sender As Object, e As EventArgs) Handles btnCompanyPW.Click, btnReturnPW.Click
        If CType(sender, Button).Name = "btnCompanyPW" Then
            If btnCompanyPW.Text.Length > 1 Then Clipboard.SetText(btnCompanyPW.Text)
        Else
            If btnReturnPW.Text.Length > 1 Then Clipboard.SetText(btnReturnPW.Text)
        End If
    End Sub

    Private Sub btnValFax_Click_1(sender As Object, e As EventArgs) Handles btnValFax.Click
        'If _useMFiles Then
        '    ValFax_Build_Mfiles("")
        'Else
        ValFax_Build_Legacy("")
        'End If
        BuildCorrespondenceTab()
    End Sub

    Private Sub btnBuildVRFile_Click_2(sender As Object, e As EventArgs) Handles btnBuildVRFile.Click
        BuildVR()
    End Sub

    Private Function TabIt(str1 As String, str2 As String) As String
        T("TabIt")
        Dim NewString As String

        NewString = str1 & Space(50 - str1.Length) & str2

        Return NewString
    End Function

    Private Sub btnSC_Click(sender As Object, e As EventArgs) Handles btnSC.Click
        Process.Start(btnSC.Tag)
    End Sub

    Private Sub btnPT_Click(sender As Object, e As EventArgs) Handles btnPT.Click
        Process.Start(btnPT.Tag)
    End Sub

    Private Sub btnUnitReview_Click(sender As Object, e As EventArgs) Handles btnUnitReview.Click
        'If Not _useMFiles Then
        Process.Start(btnUnitReview.Tag)
        'Else
        '    If Not _mfilesFailed Then
        '        Dim errors As String = _docsAccess.OpenFileFromMfiles(btnUnitReview.Tag, GetRefNumCboText(), GetRefNumCboText())
        '        If errors.Length > 0 Then
        '            MsgBox(errors)
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub btnSpecFrac_Click(sender As Object, e As EventArgs) Handles btnSpecFrac.Click
        'If Not _useMFiles Then
        Process.Start(btnSpecFrac.Tag)
        'Else
        '    If Not _mfilesFailed Then
        '        Dim errors As String = _docsAccess.OpenFileFromMfiles(btnSpecFrac.Tag, GetRefNumCboText(), GetRefNumCboText())
        '        If errors.Length > 0 Then
        '            MsgBox(errors)
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub btnHYC_Click(sender As Object, e As EventArgs) Handles btnHYC.Click
        'If Not _useMFiles Then
        Process.Start(btnHYC.Tag)
        'Else
        '    If Not _mfilesFailed Then
        '        Dim errors As String = _docsAccess.OpenFileFromMfiles(btnHYC.Tag, GetRefNumCboText(), GetRefNumCboText())
        '        If errors.Length > 0 Then
        '            MsgBox(errors)
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub btnDrawings_Click(sender As Object, e As EventArgs) Handles btnDrawings.Click
        Dim new4digitYear As Integer = Get4DigitYearFromCboStudy()
        'If new4digitYear <> 2016 Then
        Process.Start(btnDrawings.Tag)
        'Else
        '    If _mfilesFailed Then
        '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you.")
        '        Exit Sub
        '    End If
        '    Dim vpath As String = GetMfilesRefiningGeneralVPath()
        '    Try
        '        Process.Start(vpath)
        '    Catch ex As Exception
        '        MsgBox("Console was unable to open the M-Files path '" & vpath & "' for you. Please try opening M-Files, making sure it is not Offline, and try clicking this button again.")
        '    End Try
        'End If
    End Sub

    Private Sub btnCT_Click(sender As Object, e As EventArgs) Handles btnCT.Click
        Process.Start(btnCT.Tag)
    End Sub

    Private Sub btnPA_Click(sender As Object, e As EventArgs) Handles btnPA.Click
        Process.Start(btnPA.Tag)
    End Sub

    Private Sub btnRAM_Click(sender As Object, e As EventArgs) Handles btnRAM.Click
        Process.Start(btnRAM.Tag)
    End Sub

    Private Sub btnVI_Click(sender As Object, e As EventArgs) Handles btnVI.Click
        Process.Start(btnVI.Tag)
    End Sub

    Private Sub btnPolishRpt_Click(sender As Object, e As EventArgs) Handles btnPolishRpt.Click
        'mfiles not pertinent here, even if in 2016 study
        'If Not _useMFiles Then
        OpenPolishReport(Get2DigitYearFromCboStudy().ToString())

    End Sub

    Private Function FormatRefNum(refnum As String) As String
        T("FormatRefNum")
        refnum = refnum.Replace("LIV", "LUB").Replace("LEV", "LUB")
        If refnum.Substring(refnum.Length - 1, 1) = "A" Then
            Return refnum.Substring(0, refnum.Length - 3)
        Else
            Return refnum.Substring(0, refnum.Length - 2)
        End If
    End Function

    Private Sub btnSaveCI_Click(sender As Object, e As EventArgs) Handles btnSaveCI.Click
        Dim testText As String = Utilities.FixQuotes(txtCI.Text.Replace("/", "-"))
        'If testText.Trim().Length < 1 Then Exit Sub
        Me.Cursor = Cursors.WaitCursor

        System.Threading.Thread.Sleep(1000)
        Me.Cursor = Cursors.Default

        Dim rowToUse As Integer = 0
        If dgContinuingIssues.Rows.Count < 1 Then
            If Not SaveCI() Then
                MsgBox("Continuing Issue not saved")
                Exit Sub
            End If
        Else
            rowToUse = dgContinuingIssues.Rows.Count - 1
            Dim savedText As String = dgContinuingIssues.Rows(rowToUse).Cells(2).Value.ToString()
            dgContinuingIssues.Height = 385

            'no need to save if both blank.
            If savedText.Trim().Length < 1 AndAlso testText.Trim().Length < 1 Then
                Return
            End If

            If dgContinuingIssues.Rows(rowToUse).Cells(2).Value.ToString().ToUpper() <> testText.ToUpper() Then
                If Not SaveCI() Then
                    MsgBox("Continuing Issue not saved")
                    Exit Sub
                End If
            End If
        End If
        ClearContinuingIssuesTab()
        BuildContinuingIssuesTab()

    End Sub

    Private Function SaveCI(Optional CalledByLostFocus As Boolean = False) As Boolean
        Dim whatToSave As String = Utilities.FixQuotes(txtCI.Text.Replace("/", "-"))
        'If whatToSave.Trim().Length < 1 Then Return True
        If CalledByLostFocus Then
            Dim sql As String = "select Note from [Console].[ContinuingIssues] where IssueID = " & CI
            db.SQL = sql
            ds = db.Execute()
            db.SQL = ""
            If Not ds Is Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0)("Note").ToString() = whatToSave Then
                Return True
            End If
        End If

        Dim thisRefNum As String = FormatRefNum(GetRefNumCboText())
        Dim params As New List(Of String)
        params = New List(Of String)
        params.Add("RefNum/" + thisRefNum)
        params.Add("ID/" + CI)
        params.Add("EditedBy/" + UserName)
        params.Add("Issue/" & whatToSave)

        If btnSaveCI.Text = "Update Issue" Then
            ds = db.ExecuteStoredProc("Console.UpdateContinuingIssues2014", params)
            Try
                CI = ds.Tables(0).Rows(0)(0)
                'confirm save
                Dim sql As String = "select Note from [Console].[ContinuingIssues] where IssueID = " & CI
                db.SQL = sql
                ds = db.Execute()
                db.SQL = ""
            Catch exSave As Exception
                Dim errMsg As String = exSave.Message
            End Try
        Else
            'save new issue
            ds = db.ExecuteStoredProc("Console.InsertContinuingIssues2014", params)
            Dim sql As String = "select Note from [Console].[ContinuingIssues] where RefId = '" & thisRefNum &
                "' and Consultant = '" & UserName & "' and Deleted = 0 order by IssueID Desc"
            db.SQL = sql
            ds = db.Execute()
            db.SQL = ""
        End If

        If Not ds Is Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0)("Note").ToString() = whatToSave Then
            Return True
        Else
            Dim descriptiveError As String = String.Empty
            If ds Is Nothing OrElse ds.Tables.Count = 0 OrElse ds.Tables(0).Rows.Count = 0 Then
                descriptiveError = " No data was saved to the database."
            ElseIf ds.Tables(0).Rows(0)("Note").ToString() <> whatToSave Then
                descriptiveError = " Note was found in the database, but it is not an exact match with what you were trying to save."
            End If
            If CalledByLostFocus Then
                Try
                    Dim notesPath As String = ProfilePath & "dgContinuingIssuesLostFocus_" & DateTime.Now.ToString("MM-dd-yyyy HH-mm-ss") & ".txt"
                    Using sw As New StreamWriter(notesPath)
                        sw.Write(whatToSave)
                    End Using
                    MsgBox("Continuing Issue was not saved. Please see Notepad for the text that should have been saved." & descriptiveError)
                    Process.Start(notesPath)
                Catch ex As Exception
                    Dim breakpoint As String = ex.Message
                End Try
            End If
        End If
        Return False
    End Function




    Private Sub txtCI_TextChanged(sender As Object, e As EventArgs) Handles txtCI.TextChanged
        If txtCI.Text.Length > 1 Then

            btnSaveCI.Enabled = True
        End If
    End Sub

    Private Sub btnFLValidation_Click(sender As Object, e As EventArgs) Handles btnFLValidation.Click
        If File.Exists(IDRPath & "Combined Site\Compare Fuels and Lubes " & Get2DigitYearFromCboStudy().ToString() & ".xls") Then
            Process.Start(IDRPath & "Combined Site\Compare Fuels and Lubes " & Get2DigitYearFromCboStudy().ToString() & ".xls")
        Else
            MessageBox.Show(IDRPath & "Combined Site\Compare Fuels and Lubes " & Get2DigitYearFromCboStudy().ToString() & ".xls is Missing.  Contact Sung.Pak@solomononline.com", "Missing")
        End If
    End Sub

    Private Sub dgContinuingIssues_Click(sender As Object, e As EventArgs) Handles dgContinuingIssues.Click
        T("dgContinuingIssues_Click")
        Dim row As DataGridViewRow
        dgContinuingIssues.Height = 240
        txtCI.Text = ""
        If dgContinuingIssues.SelectedRows.Count > 0 Then
            row = dgContinuingIssues.SelectedRows(0)

            CI = row.Cells(0).Value.ToString
            txtCI.Text = row.Cells(2).Value.ToString
            CIConsultant = row.Cells(3).Value.ToString
            CIIssueDate = row.Cells(1).Value.ToString
            CIDeletedBy = row.Cells(4).Value.ToString
            CIDeletedDate = row.Cells(5).Value.ToString
            CIDeleted = row.Cells(6).Value.ToString
            btnSaveCI.Text = "Update Issue"


        End If


    End Sub

    Private Sub BuildContinuingIssuesTab()
        T("BuildContinuingIssuesTab")
        Dim thisRefNum As String = GetRefNumCboText()
        If ParseRefNum(thisRefNum, 2) = "LUB" Then thisRefNum = thisRefNum.Replace("LUB", "LIV")
        'Dim dt As String
        'Dim row As DataRow
        Dim params = New List(Of String)
        'txtNotes.Text = ""
        'params = New List(Of String)

        dgContinuingIssues.Rows.Clear()
        dgContinuingIssues.Height = 385
        If thisRefNum.Length < 1 Then Exit Sub
        params.Add("RefNum/" + FormatRefNum(thisRefNum))
        ds = db.ExecuteStoredProc("Console." & "GetContinuingIssues2014", params)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr In ds.Tables(0).Rows
                If dr("note").ToString().Trim().Length > 0 Then
                    dgContinuingIssues.Rows.Add({dr("IssueID").ToString, Convert.ToDateTime(dr("EntryDate")), dr("note").ToString, dr("consultant").ToString, dr("deletedby").ToString, dr("deletedDate").ToString, dr("deleted").ToString})
                End If
            Next
            btnExportCI.Enabled = True
        End If

    End Sub

    Private Sub btnNewIssue_Click(sender As Object, e As EventArgs) Handles btnNewIssue.Click

        dgContinuingIssues.Height = 220
        txtCI.Text = ""
        btnSaveCI.Text = "Save Issue"
        btnSaveCI.Enabled = True

    End Sub

    Private Sub cboConsultant_KeyDown(sender As Object, e As KeyEventArgs) Handles cboConsultant.KeyDown
        If e.KeyCode = Keys.Enter Then
            cboConsultant.Text = cboConsultant.Text.ToUpper()
            UpdateConsultant(cboConsultant.Text)
        End If
    End Sub

    Private Sub cboConsultant_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboConsultant.SelectionChangeCommitted
        UpdateConsultant(cboConsultant.SelectedItem.Trim().ToUpper())
    End Sub
    Private Sub SearchQA(searchString As String)
        Dim results As String = ""

        tv.Nodes.Clear()
        Dim root As New TreeNode("SearchResults")
        tv.Nodes.Add(root)


        Dim params As New List(Of String)
        'TODO: CHANGE (rrh)
        Dim dd As New DataObject(DataObject.StudyTypes.REFINING_DEV, "rrh", "rrh#4279")


        params = New List(Of String)

        params.Add("SearchString/" + searchString)
        If chkUseRefnum.Checked Then
            params.Add("RefNum/" + GetRefNumCboText())
        Else
            params.Add("RefNum/ALL")
        End If

        ds = dd.ExecuteStoredProc("SearchQA", params)

        Dim count As Integer = 1
        For Each r In ds.Tables(0).Rows
            tv.Nodes(0).Nodes.Add(count.ToString & ". " & r(1))
            count = count + 1
        Next
        tv.ExpandAll()


    End Sub

    Private Sub SearchQALink(searchString As String)
        Dim results As String = ""
        Dim n As TreeNode
        tv.Nodes.Clear()
        Dim root As New TreeNode("SearchResults")
        tv.Nodes.Add(root)


        Dim params As New List(Of String)
        'TODO: CHANGE (rrh)
        Dim dd As New DataObject(DataObject.StudyTypes.REFINING_DEV, "rrh", "rrh#4279")


        params = New List(Of String)

        params.Add("SearchString/" + searchString)
        If chkUseRefnum.Checked Then
            params.Add("RefNum/" + GetRefNumCboText())
        Else
            params.Add("RefNum/ALL")
        End If

        ds = dd.ExecuteStoredProc("SearchQACategory", params)

        Dim count As Integer = 1
        For Each r In ds.Tables(0).Rows
            n = New TreeNode(count.ToString & ". " & r(1))

            tv.Nodes(0).Nodes.Add(n)
            count = count + 1
        Next
        tv.ExpandAll()


    End Sub

    Private Sub lnkRefineryHistory_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkRefineryHistory.LinkClicked, lnkEnergy.LinkClicked, lnkMaintenance.LinkClicked, lnkMaterialBalance.LinkClicked, lnkMisc.LinkClicked, lnkOpex.LinkClicked, lnkPersonnel.LinkClicked, lnkPricing.LinkClicked, lnkProcessData.LinkClicked, lnkResultsPresentation.LinkClicked, lnkStudyBoundary.LinkClicked
        SearchQALink(CType(sender, LinkLabel).Tag)

    End Sub

    Private Sub btnExportCI_Click(sender As Object, e As EventArgs) Handles btnExportCI.Click
        Dim thisRefNum As String = GetRefNumCboText()
        If ParseRefNum(thisRefNum, 2) = "LUB" Then thisRefNum = thisRefNum.Replace("LUB", "LIV")
        'Dim dt As String
        'Dim row As DataRow
        Dim params = New List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + FormatRefNum(thisRefNum))


        ds = db.ExecuteStoredProc("Console." & "GetContinuingIssues2014", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                BuildCIExport(ds.Tables(0).Rows, RefNum)
            End If
        End If
    End Sub

    Private Sub BuildCIExport(rows As DataRowCollection, RefNum As String)
        'Dim Body As String
        Dim Header As String
        Dim Footer As String

        Header = "Refinery ID: " & FormatRefNum(RefNum)

        Footer = "Copyright Solomon Associates 2015"

        'Dim file As System.IO.FileStream
        Try
            ' Indicate whether the text file exists
            If My.Computer.FileSystem.FileExists(_profileConsoleTempPath & "CI.TXT") Then
                System.IO.File.Delete(_profileConsoleTempPath & "CI.TXT")
            End If

            Dim addInfo As New System.IO.StreamWriter(_profileConsoleTempPath & "CI.TXT")

            addInfo.WriteLine(Header)
            addInfo.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------")
            addInfo.WriteLine("") ' blank line of text
            For Each r In rows
                If Not chkNoDates.Checked Then
                    addInfo.WriteLine(r(1) & " - " & r(2))
                End If
                addInfo.WriteLine(r(3))
                addInfo.WriteLine("") ' blank line of text
            Next
            addInfo.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------")
            addInfo.WriteLine(Footer)
            addInfo.Close()
        Catch ex As FileIO.MalformedLineException
            MessageBox.Show(ex.Message)
        End Try
        Process.Start(_profileConsoleTempPath & "CI.TXT")
    End Sub

    Private Sub DeleteContinuingIssue(id As Integer)
        Dim params As New List(Of String)
        params = New List(Of String)

        params.Add("IssueID/" + CI)
        params.Add("DeletedBy/" + UserName)


        ds = db.ExecuteStoredProc("Console.DeleteContinuingIssue2014", params)
    End Sub

    Private Sub dgContinuingIssues_KeyDown(sender As Object, e As KeyEventArgs) Handles dgContinuingIssues.KeyDown
        If dgContinuingIssues.SelectedRows.Count > 0 Then
            Dim Row As DataGridViewRow = dgContinuingIssues.SelectedRows(0)
            If e.KeyCode = Keys.Delete Then
                Dim dr As DialogResult = MessageBox.Show("This will remove this issue from list, are you sure you want to delete?", "Wait", MessageBoxButtons.YesNo)
                If dr = System.Windows.Forms.DialogResult.Yes Then
                    DeleteContinuingIssue(Row.Cells(0).Value)
                    BuildContinuingIssuesTab()
                    txtCI.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub QASearch_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QASearch.KeyPress
        If e.KeyChar = Chr(13) Then SearchQA(QASearch.Text)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Dim new4digitYear As Integer = Get4DigitYearFromCboStudy()
        'If new4digitYear <> 2016 Then
        Try
            Process.Start(CorrPath)
        Catch ex As Exception

        End Try
        'Else
        '    Dim vpath As String = GetMfilesBenchmarkingParticipantVPath()
        '    Try
        '        Process.Start(vpath)
        '    Catch ex As Exception
        '        MsgBox("Console was unable to open the M-Files path '" & vpath & "' for you. Please try opening M-Files, making sure it is not Offline, and try clicking this button again.")
        '    End Try
        'End If
    End Sub

    'Private Sub ShowMfilesBenchmarking()
    '    If _mfilesFailed Then
    '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you.")
    '        Exit Sub
    '    End If
    '    Dim url As String = _mfilesDesktopUIPath\Benchmarking Particpants\"
    '    Try
    '        _mfilesDesktopUIPath\Benchmarking Particpants\2016\lub\888LUB16 - SOLOMON - EXAMPLE
    '        url = url & Get4DigitYearFromCboStudy() & "\"
    '        url = url & cboStudy.Text.Substring(0, 3).ToLower() & "\"
    '        'url = url & GetRefNumCboText().Substring(3, 3).ToLower() & "\"
    '        db.SQL = "SELECT m.FacilityID, m.CoLoc FROM MFilesSupport.dbo.CPAFacilities m WHERE m.FacilityId = (SELECT TOP 1 CONCAT(RefNumber,STudy) from RefiningWork.dbo.TSort where refineryID = '" + GetRefNumCboText().Trim() + "')"
    '        Dim ds As DataSet = db.Execute()
    '        db.SQL = ""
    '        If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '            url = url & GetRefNumCboText().Trim() & " - " & ds.Tables(0).Rows(0)("CoLoc").ToString()
    '            Process.Start(url)
    '        End If
    '    Catch ex As Exception
    '        Dim err As String = ex.Message
    '        '_mfilesFailed = True
    '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you." & Environment.NewLine & url)
    '        db.SQL = String.Empty
    '    End Try
    'End Sub

    'Private Sub ShowMfilesCompany()
    '    If _mfilesFailed Then
    '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you.")
    '        Exit Sub
    '    End If
    '    Dim url As String = _mfilesDesktopUIPath\Benchmarking Particpants\"
    '    Try
    '        _mfilesDesktopUIPath\Benchmarking Particpants\2016\eur\EUR16Co-EXAMPLE - SOLOMON (Company)
    '        url = url & Get4DigitYearFromCboStudy() & "\"
    '        url = url & cboStudy.Text.Substring(0, 3).ToLower() & "\"
    '        Dim companyBenchmarkRefnum As String = String.Empty
    '        'Dim studyText As String = GetRefNumCboText().IndexOf(cboStudy.Text.TrimEnd())
    '        Dim yr As Integer = GetRefNumCboText().IndexOf(cboStudy.Text.TrimEnd())
    '        Dim filter As String = GetRefNumCboText().Substring(0, yr + 3)

    '        Dim sql As String = "SELECT CONCAT( C.CompanyID, ' - ', C.COmpanyName) FROM [MFilesSupport].[dbo].[FacilityCompany] FC inner join [MFilesSupport].[dbo].[Companies] C on c.CompanyID = FC.CompanyiD where facilityid = '" + filter + "'"
    '        db.SQL = sql
    '        Try
    '            Dim ds As DataSet = db.Execute()
    '            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0)(0).ToString().Length > 0 Then
    '                'EUR16Co-EXAMPLE
    '                companyBenchmarkRefnum = cboStudy.Text.Trim() + "Co-" + ds.Tables(0).Rows(0)(0).ToString().Trim() + " (Company)"
    '                db.SQL = String.Empty
    '                url = url & companyBenchmarkRefnum
    '                Process.Start(url)
    '            End If
    '        Catch ex As Exception
    '            'reset to no data so we don't try to populate list or err while doing it
    '            companyBenchmarkRefnum = String.Empty
    '            '_mfilesFailed = True
    '            MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you." & Environment.NewLine & url)
    '        End Try
    '        '===========================
    '        'Dim url As String = "_mfilesDesktopUIPath\Benchmarking Particpants\"
    '        'url = url & Get4DigitYearFromCboStudy() & "\"
    '        'url = url & cboStudy.Text.Substring(0, 3).ToLower() & "\"
    '        ''url = url & GetRefNumCboText().Substring(3, 3).ToLower() & "\"
    '        'db.SQL = "SELECT m.FacilityID, m.CoLoc FROM MFilesSupport.dbo.CPAFacilities m WHERE m.FacilityId = (SELECT TOP 1 CONCAT(RefNumber,STudy) from RefiningWork.dbo.TSort where refineryID = '" + GetRefNumCboText().Trim() + "')"
    '        'Dim ds As DataSet = db.Execute()
    '        'db.SQL = ""
    '        'If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '        '    url = url & ds.Tables(0).Rows(0)("FacilityID").ToString().Trim() & " - " & ds.Tables(0).Rows(0)("CoLoc").ToString().Trim()
    '        '    _mfilesDesktopUIPath\Refining General\eur\102EUR ENI - TARANTO
    '        '    Process.Start(url)
    '        'End If

    '    Catch ex As Exception
    '        Dim err As String = ex.Message
    '        '_mfilesFailed = True
    '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you." & Environment.NewLine & url)
    '        db.SQL = String.Empty
    '    End Try
    'End Sub

    'Private Sub ShowMfilesRefiningGeneral()
    '    If _mfilesFailed Then
    '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you.")
    '        Exit Sub
    '    End If
    '    Dim url As String = _mfilesDesktopUIPath & "Refining General\"
    '    Try
    '        Dim studyIndex As Integer = GetRefNumCboText().IndexOf(cboStudy.Text().Substring(0, 3))
    '        Dim refnumWithoutYear = GetRefNumCboText().Substring(0, studyIndex)

    '        url = url & cboStudy.Text.Substring(0, 3).ToLower() & "\"
    '        db.SQL = "SELECT m.FacilityID, m.CoLoc FROM MFilesSupport.dbo.CPAFacilities m WHERE m.FacilityId = (SELECT TOP 1 CONCAT(RefNumber,STudy) from RefiningWork.dbo.TSort where refineryID = '" + GetRefNumCboText().Trim() + "')"
    '        Dim ds As DataSet = db.Execute()
    '        db.SQL = ""
    '        If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '            url = url & ds.Tables(0).Rows(0)("FacilityID").ToString().Trim() & " " & ds.Tables(0).Rows(0)("CoLoc").ToString().Trim()
    '            '_mfilesDesktopUIPath\Refining General\eur\102EUR ENI - TARANTO
    '            Process.Start(url)
    '        End If
    '        'Dim docsMFilesAccess As New DocsMFilesAccess(ConsoleVertical, GetRefNumCboText(), GetRefNumCboText())
    '        'Dim uri As New System.Uri(docsMFilesAccess.GetUrlToOpenMfilesClient(163))
    '        'WebBrowser1.Url = uri
    '        'WebBrowser1.Refresh()
    '    Catch ex As Exception
    '        Dim err As String = ex.Message
    '        '_mfilesFailed = True
    '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you." & Environment.NewLine & url)
    '        db.SQL = String.Empty
    '    End Try
    'End Sub

    Private Sub refreshForm()
        Me.Cursor = Cursors.WaitCursor
        ClearAllControls()
        PopulateForm()
        PopulateTabs()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub MainConsole_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then
            refreshForm()
        End If

    End Sub

    Private Sub btnIssueCancel_Click(sender As Object, e As EventArgs) Handles btnIssueCancel.Click
        txtCI.Text = ""
        dgContinuingIssues.Height = 385
        btnSaveCI.Text = "Update Issue"
        btnSaveCI.Enabled = False
    End Sub

    Private Sub btnDDExit_Click(sender As Object, e As EventArgs) Handles btnDDExit.Click
        RemoveTab("tabDD")
        ConsoleTabs.SelectedIndex = 1
    End Sub

    Private Sub txtNotes_Leave(sender As Object, e As EventArgs) Handles txtNotes.Leave
        'If txtNotes.Text <> "" Then SaveNotes()
        'If txtNotes.Text <> "" Then MsgBox("About to save notes")
        'If txtNotes.Text.Trim() <> "" Then SaveNotes()
    End Sub

    Private Sub txtNotes_LostFocus(sender As Object, e As System.EventArgs) Handles txtNotes.LostFocus
        If txtNotes.ReadOnly = True Then Exit Sub
        If txtNotes.Text <> "" Then
            If Not SaveNotes() Then
                Try
                    Dim notesPath As String = ProfilePath & "txtNotesLostFocus_" & DateTime.Now.ToString("MM-dd-yyyy HH-mm-ss") & ".txt"
                    Using sw As New StreamWriter(notesPath)
                        sw.Write(txtNotes.Text.Replace("/", "-"))
                    End Using
                    MsgBox("Notes were not saved. Please see Notepad for the text that should have been saved.")
                    Process.Start(notesPath)
                Catch ex As Exception
                    Dim breakpoint As String = ex.Message
                End Try
            End If
        End If
    End Sub

    Private Sub BuildContactTab()
        T("BuildContactTab")
        GetCompanyContactInfo()
        GetPricingContactInfo()
        GetAltCompanyContactInfo()
        GetInterimContactInfo()
        GetContacts()
        FillContacts()
    End Sub

    Private Sub BuildTab(current As String)
        T("BuildTab")
        Select Case current
            Case "Contacts"
                ClearFields() 'cleaers contact tab AND top right of form !!!
                BuildContactTab()
            Case "Correspondence", "IDR Correspondence"
                BuildCorrespondenceTab() 'this also clears the existing data
                SetSummaryFileButtons()
            Case "Checklist"
                ClearChecklist()
                BuildCheckListTab("")
            Case "Notes"
                BuildNotesTab() 'this also clears old data
            Case "Continuing Issues"
                BuildContinuingIssuesTab() 'this also clears old data
            Case "Time/Grade"
                ClearTimeGrade()
                ShowTimeGrade()
                'Case "Drag and Drop"

        End Select
        BuildSummarySection() 'top right corner of form
        CheckFuelsLubes()
        'BuildContactTab()
        CheckPreliminaryPricing()
        cboRefNum.Text = cboRefNum.Text.Trim
        Company = GetCompanyName(GetRefNumCboText())
        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        SetPaths()
        btnValidate.Enabled = True
    End Sub
    Private Sub BuildTab_Legacy(current As String)
        'T("BuildTab")
        'Select Case current
        '    Case "Correspondence"
        '        BuildCorrespondenceTab()
        '    Case "Checklist"
        '        BuildCheckListTab("")
        '    Case "Notes"
        '        BuildNotesTab()
        '    Case "Continuing Issues"
        '        BuildContinuingIssuesTab()
        '    Case "Time/Grade"
        '        ShowTimeGrade()
        '    Case "Contacts"
        '        BuildContactTab()

        'End Select
        'BuildSummarySection()
        'CheckFuelsLubes()
        ''BuildContactTab()
        'CheckPreliminaryPricing()
        'cboRefNum.Text = cboRefNum.Text.Trim
        'Company = GetCompanyName(GetRefNumCboText())
        'If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        'SetPaths()
        'btnValidate.Enabled = True
    End Sub

    Private Sub ConsoleTabs_Selected(sender As Object, e As TabControlEventArgs) Handles ConsoleTabs.Selected

        Try
            T("ConsoleTabs_Selected: " + ConsoleTabs.SelectedTab.Text)
        Catch
        End Try

        'CurrentTab = ConsoleTabs.SelectedTab.Text
        'Try
        ' BuildTab(CurrentTab)
        ' Catch ex As Exception
        ' MsgBox(ex.Message)
        ' End Try
    End Sub

    Private Sub PrepCorrListview_old(listView As ListView, startsWith As String, items As List(Of Object),
                                 vfNumber As String, append As Boolean)
        Dim heading As String = String.Empty
        Select Case startsWith.ToUpper()
            Case "VF", "VFFL"
                heading = "IDR Fax Files (VF*.*)"
            Case "VR", "VRFL"
                heading = "IDR Reply Files (VR*.*)"
            Case Else
                heading = "Return Files"
                If ConsoleVertical = VerticalType.REFINING And startsWith.ToUpper() = "VA" Then
                    heading = "IDR Reply Files (VR*.*)"
                End If
        End Select
        If Not append Then
            If listView.Columns.Count < 1 Then listView.Columns.Add(heading)
            listView.Items.Clear()
            listView.View = Windows.Forms.View.Details
            listView.FullRowSelect = True
            listView.Columns(0).Width = listView.Width - 4 - SystemInformation.VerticalScrollBarWidth '4 - SystemInformation.VerticalScrollBarWidth
        End If
        For Each item As Object In items
            Dim itemText As String = item.ToString()
            Dim checkedOut As Boolean = itemText.Contains("(Checked out to")
            itemText = itemText.Replace("Checked out to ", "")
            Dim itemParts() As String = Split(itemText, " | ")
            Dim fileName As String = itemParts(1)
            If heading <> "Return Files" Then
                If vfNumber = Mid(fileName, 3, 1) And fileName.StartsWith(startsWith) OrElse
                    vfNumber = Mid(fileName, 5, 1) And fileName.StartsWith(startsWith & "FL") Then
                    Dim listViewItem As New ListViewItem(itemText)
                    'listViewItem.Tag=item.
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                End If
            Else 'return files box
                'If (lstVFNumbers.SelectedItem = Mid(filename, Len(filename) - 4, 1) _
                ''If (lstVFNumbers.SelectedItem = Mid(objArr(1), Len(objArr(1)) - 4, 1) _
                '-Or InStr(1, filename, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                If vfNumber = Mid(fileName, Len(fileName) - 4, 1) Or
                    fileName.Contains("Return" + vfNumber) Then
                    Dim listViewItem As New ListViewItem(itemText)
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                End If
            End If
        Next
    End Sub

    Private Sub PrepCorrListview2(listView As ListView, startsWith As String, items As List(Of ConsoleFile),
                                 vfNumber As String, append As Boolean)
        Dim heading As String = String.Empty
        Select Case startsWith.ToUpper()
            Case "VF", "VFFL"
                heading = "IDR Fax Files (VF*.*)"
            Case "VR", "VRFL"
                heading = "IDR Reply Files (VR*.*)"
            Case Else
                heading = "Return Files"
                If ConsoleVertical = VerticalType.REFINING And startsWith.ToUpper() = "VA" Then
                    heading = "IDR Reply Files (VR*.*)"
                End If
        End Select
        If Not append Then
            If listView.Columns.Count < 1 Then listView.Columns.Add(heading)
            listView.Items.Clear()
            listView.View = Windows.Forms.View.Details
            listView.FullRowSelect = True
            listView.Columns(0).Width = listView.Width - 4 - SystemInformation.VerticalScrollBarWidth '4 - SystemInformation.VerticalScrollBarWidth
        End If
        For Each item As ConsoleFile In items
            Dim itemText As String = item.FileName.ToString()
            Dim checkedOut As Boolean = itemText.Contains("(Checked out to")
            itemText = itemText.Replace("Checked out to ", "")
            Dim itemParts() As String = Split(itemText, " | ")
            Dim fileName As String = itemParts(1)
            If heading <> "Return Files" Then
                If vfNumber = Mid(fileName, 3, 1) And fileName.StartsWith(startsWith) OrElse
                    vfNumber = Mid(fileName, 5, 1) And fileName.StartsWith(startsWith & "FL") Then
                    Dim listViewItem As New ListViewItem(itemText)
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                    'it seems we must do this after adding item to list else tag will be null
                    If item.Id > 0 Then listView.Items(listView.Items.Count - 1).Tag = item.Id.ToString()
                End If
            Else 'return files box
                'If (lstVFNumbers.SelectedItem = Mid(filename, Len(filename) - 4, 1) _
                ''If (lstVFNumbers.SelectedItem = Mid(objArr(1), Len(objArr(1)) - 4, 1) _
                '-Or InStr(1, filename, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                'If vfNumber = Mid(fileName, Len(fileName) - 4, 1) Or _
                If fileName.Contains("Return" + vfNumber) Then
                    Dim listViewItem As New ListViewItem(itemText)
                    If checkedOut Then
                        listViewItem.BackColor = Color.Red
                    End If
                    listView.Items.Add(listViewItem)
                    'it seems we must do this after adding item to list else tag will be null
                    If item.Id > 0 Then listView.Items(listView.Items.Count - 1).Tag = item.Id.ToString()
                End If
            End If
        Next
    End Sub
    Private Function PrepCorrListview(listView As ListView, startsWith As String, files As ConsoleFilesInfo,
                                 vfNumber As String, append As Boolean) As Boolean
        Try
            Dim heading As String = String.Empty
            Select Case startsWith.ToUpper()
                Case "VF"
                    heading = "IDR Fax Files (VF*.*)"
                Case "VR"
                    heading = "IDR Reply Files (VR*.*)"
                Case Else
                    heading = "Return Files"
                    If ConsoleVertical = VerticalType.REFINING And startsWith.ToUpper() = "VA" Then
                        heading = "IDR Reply Files (VR*.*)"
                    End If
            End Select
            If Not append Then
                If listView.Columns.Count < 1 Then listView.Columns.Add(heading)
                listView.Items.Clear()
                listView.View = Windows.Forms.View.Details
                listView.FullRowSelect = True
                listView.Columns(0).Width = listView.Width - 4 - SystemInformation.VerticalScrollBarWidth '4 - SystemInformation.VerticalScrollBarWidth
            End If
            For Each cf As ConsoleFile In files.Files
                Dim checkedOut As Boolean = Not String.IsNullOrEmpty(cf.CheckedOutToUserName) ' itemText.Contains("(Checked out to")
                Dim itemText As String = String.Empty
                If checkedOut Then
                    itemText = "(" & cf.CheckedOutToUserName & ") "
                End If
                itemText = itemText & cf.FileName
                'itemText = itemText.Replace("Checked out to ", "")
                'Dim itemParts() As String = Split(itemText, " | ")
                'Dim fileName As String = itemParts(1)
                If heading <> "Return Files" Then
                    If vfNumber = Mid(cf.FileName, 3, 1) And cf.FileName.StartsWith(startsWith) Then
                        Dim listViewItem As New ListViewItem(itemText)
                        If cf.Id > 0 Then
                            listViewItem.Tag = cf.Id.ToString()
                        End If
                        If checkedOut Then
                            listViewItem.BackColor = Color.Red
                        End If
                        listView.Items.Add(listViewItem)
                    End If
                Else 'return files box
                    'If (lstVFNumbers.SelectedItem = Mid(filename, Len(filename) - 4, 1) _
                    ''If (lstVFNumbers.SelectedItem = Mid(objArr(1), Len(objArr(1)) - 4, 1) _
                    '-Or InStr(1, filename, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                    If vfNumber = Mid(cf.FileName, Len(cf.FileName) - 4, 1) Or
                        cf.FileName.Contains("Return" + vfNumber) Then
                        Dim listViewItem As New ListViewItem(itemText)
                        If cf.Id > 0 Then
                            listViewItem.Tag = cf.Id.ToString()
                        End If
                        If checkedOut Then
                            listViewItem.BackColor = Color.Red
                        End If
                        listView.Items.Add(listViewItem)
                    End If
                End If
            Next
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function


    Private Sub DeleteExtractedFolder()
        If Directory.Exists(_profileConsoleTempPath) = False Then Directory.CreateDirectory(_profileConsoleTempPath)
        Dim extractedFilesFolderPath As String = _profileConsoleTempPath + "Extracted Files\"

        System.Threading.Thread.Sleep(2000)
        If Directory.Exists(extractedFilesFolderPath) Then
            If Dir(extractedFilesFolderPath) <> "" Then
                Dim zfilenames() As String = Directory.GetFiles(extractedFilesFolderPath, "*.*")
                For Each sFile In zfilenames
                    Try
                        File.Delete(sFile)
                    Catch
                        'it's not that important. Will get cleared out during next Drag-N-Drop
                    End Try
                Next
                Try
                    Directory.Delete(extractedFilesFolderPath)
                Catch
                    'it's not that important. Will get cleared out during next Drag-N-Drop
                End Try
            End If
        End If
    End Sub

    Private Sub ClearDragDropTab()
        _dragDropMode = 0
        dgFiles.RowCount = 1
        dgFiles.Rows.Clear()
        'For Each foundFile As String In Directory.GetFiles(_profileConsoleTempPath)
        '    Try
        '        File.Delete(foundFile)
        '    Catch
        '    End Try
        'Next
        DeleteAllButRefiningValidationFileSiblings()
        txtMessageFilename.Text = ""
        btnFilesSave.Enabled = False
    End Sub




    Private Sub chkIDR_CheckedChanged(sender As Object, e As EventArgs) Handles chkIDR.CheckedChanged
        If _formIsLoaded Then
            SaveRefineryAppSettingsXmlSetting("chkIDRChecked", chkIDR.Checked)
        End If
    End Sub

    Private Sub listViewCorrespondenceVF_DoubleClick(sender As Object, e As EventArgs) Handles listViewCorrespondenceVF.DoubleClick
        listViewCorrespondence_Shell(CType(sender, ListView), "F")
    End Sub

    Private Sub listViewCorrespondence_Shell(listView As ListView, v As Char)
        Dim file As String
        'file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
        Dim selections As ListView.SelectedListViewItemCollection =
            listView.SelectedItems
        If selections.Count <> 1 Then
            'MsgBox("Please select only 1 V" + v.ToString().ToUpper())
            Return
        End If
        For Each item As ListViewItem In selections
            Dim tagId As String = item.Tag
            If IsNumeric(tagId) Then
                Dim id As Integer = Int32.Parse(tagId)
                Dim docsAccess As New DocsFileSystemAccess(VerticalType.REFINING, True)
                Dim errorMsg As String = docsAccess.OpenFileFromMfiles(id, GetRefNumCboText(), GetRefNumCboText())
                If errorMsg.Length > 0 Then MsgBox(errorMsg)
                Return
            End If
            Dim FindTab As String = item.Text.ToString.IndexOf("|")
            file = item.Text.Substring(
                FindTab + 1, item.Text.ToString.Length - FindTab - 1)
            If Get4DigitYearFromCboStudy() = 2016 Then
                Dim docsAccess As New DocsFileSystemAccess(VerticalType.REFINING, True) ', ShortRefNum, "20" + ShortRefNum)
                docsAccess.lstVFFiles_DoubleClick(file.Trim(), CorrPath, GetRefNumCboText(), GetRefNumCboText())
            Else
                Process.Start(CorrPath & file.Trim)
            End If
            Return 'don't do more than 1
        Next
    End Sub

    Private Sub listViewCorrespondenceVR_DoubleClick(sender As Object, e As EventArgs) Handles listViewCorrespondenceVR.DoubleClick
        listViewCorrespondence_Shell(CType(sender, ListView), "R")
    End Sub

    Private Sub listViewCorrespondenceVReturn_DoubleClick(sender As Object, e As EventArgs) Handles listViewCorrespondenceVReturn.DoubleClick
        listViewCorrespondence_Shell(CType(sender, ListView), "")
    End Sub

    Private Sub CleanFilesFromFolder(folderPath As String)
        T("CleanFilesFromFolder")
        If Directory.Exists(folderPath) Then
            Dim dir As New DirectoryInfo(folderPath)
            Dim files() As FileInfo = dir.GetFiles()
            For i As Integer = 0 To files.Count - 1
                Try
                    Kill(files(i).FullName)
                Catch
                End Try
            Next
        End If
    End Sub

    Private Sub dgFiles_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgFiles.DataError
        '"datagridview" datagridviewcomboboxcell value is not valid" ".Items.Add("
#If DEBUG Then
        MsgBox("Private Sub dgFiles_DataError entered")
#End If
    End Sub

    Private Sub btnDataFolder_Click(sender As Object, e As EventArgs) Handles btnDataFolder.Click

        Try
            Process.Start(DataFolderPath)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnOpenCompanyCorresp_Click(sender As Object, e As EventArgs) Handles btnOpenCompanyCorresp.Click
        'If Not _useMFiles Then
        Try
            Process.Start(CompanyPath)
        Catch ex As Exception
        End Try
        'Else
        '    If _mfilesFailed Then
        '        MsgBox("There was a failure to get MFiles information. Console is unable to open MFiles for you.")
        '        Exit Sub
        '    End If
        '    Dim vpath As String = GetMfilesBenchmarkingCompanyVPath()
        '    Try
        '        Process.Start(vpath)
        '    Catch ex As Exception
        '        MsgBox("Console was unable to open the M-Files path '" & vpath & "' for you. Please try opening M-Files, making sure it is not Offline, and try clicking this button again.")
        '    End Try
        'End If
    End Sub

    Private Function GetFLMacrosPath() As String
        Try
            Dim flmacrosPath As String = StudyDrive
            flmacrosPath = flmacrosPath & "20" & Get2DigitYearFromCboStudy() & "\"
            flmacrosPath = flmacrosPath & "Software\Participant Website\FL" & Get2DigitYearFromCboStudy() & "Macros.xlsm"
            If Int32.Parse(Get2DigitYearFromCboStudy()) < 16 Then
                flmacrosPath = flmacrosPath.Replace(".xlsm", ".xls")
            End If
            Return flmacrosPath
        Catch ex As Exception
            'cboStudy might not be populated yet
            Return String.Empty
        End Try
    End Function

    'Private Function GetMfilesBenchmarkingParticipantVPath() As String
    '    PopulateMFilesVPaths(GetRefNumCboText())
    '    Return _mfilesBenchmarkingParticipantVPath
    'End Function
    'Private Function GetMfilesBenchmarkingCompanyVPath() As String
    '    PopulateMFilesVPaths(GetRefNumCboText())
    '    Return _mfilesBenchmarkingCompanyVPath
    'End Function
    'Private Function GetMfilesRefiningGeneralVPath() As String
    '    PopulateMFilesVPaths(GetRefNumCboText())
    '    Return _mfilesRefiningGeneralVPath
    'End Function
    'Private Sub PopulateMFilesVPaths(refnumToUse As String)
    '    If _mfilesBenchmarkingParticipantVPath.Length < 1 Or
    '        _mfilesBenchmarkingCompanyVPath.Length < 1 Or
    '        _mfilesRefiningGeneralVPath.Length < 1 Then
    '        Try
    '            Dim sql As String = "SELECT RefineryGeneral = '" + _mfilesDesktopUIPath + "Refining General\' + RTRIM(t.Study) + '\' + RTRIM(m.FacilityID) + ' ' + m.CoLoc"
    '            sql = sql & ", RefStudyFolder = '" + _mfilesDesktopUIPath + "Benchmarking Particpants\' + CAST(t.StudyYear as varchar(4)) +'\' + RTRIM(p.Study) + '\' + RTRIM(p.Refnum) + ' - ' + p.CoLoc"
    '            sql = sql & ", CoStudyFolder = '" + _mfilesDesktopUIPath + "Benchmarking Particpants\' + CAST(t.StudyYear as varchar(4)) +'\' + RTRIM(p.Study) + '\' + RTRIM(c.Refnum) + ' - '+ c.CoLoc"
    '            sql = sql & " FROM dbo.TSort t CROSS APPLY (VALUES (REPLACE(REPLACE(REPLACE(REPLACE(t.RefID,'LEV','LUB'),'LIV','LUB'),'AER','ARM'),'AIR','ARM'),REPLACE(REPLACE(REPLACE(REPLACE(t.Refnum,'LEV','LUB'),'LIV','LUB'),'AER','ARM'),'AIR','ARM'))) x(GenRefID, GenRefnum)"
    '            sql = sql & " LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID = x.GenRefID"
    '            sql = sql & " LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.Refnum = x.GenRefnum"
    '            sql = sql & " LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID)"
    '            sql = sql & " ON c.Study = t.Study AND c.StudyYear = t.StudyYear AND fc.FacilityID = x.GenRefID"
    '            sql = sql & " WHERE t.RefineryID = '" & refnumToUse & "' "
    '            db.SQL = sql
    '            Dim ds As DataSet = db.Execute()
    '            If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '                'excluding LUB because will always get 2 records, one LIV one LEV, and GLC said both will be the same.
    '                If ds.Tables(0).Rows.Count > 1 And Not refnumToUse.Contains("LUB") Then
    '                    sql = sql & " and t.CoLoc = M.Coloc"
    '                    db.SQL = sql
    '                    ds = db.Execute()
    '                    If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '                        If Not IsDBNull(_mfilesBenchmarkingParticipantVPath = ds.Tables(0).Rows(0)("RefStudyFolder")) Then
    '                            _mfilesBenchmarkingParticipantVPath = ds.Tables(0).Rows(0)("RefStudyFolder").ToString()
    '                        End If
    '                        If Not IsDBNull(_mfilesBenchmarkingCompanyVPath = ds.Tables(0).Rows(0)("CoStudyFolder")) Then
    '                            _mfilesBenchmarkingCompanyVPath = ds.Tables(0).Rows(0)("CoStudyFolder").ToString()
    '                        End If
    '                        If Not IsDBNull(_mfilesRefiningGeneralVPath = ds.Tables(0).Rows(0)("RefineryGeneral")) Then
    '                            _mfilesRefiningGeneralVPath = ds.Tables(0).Rows(0)("RefineryGeneral").ToString()
    '                        End If
    '                    End If
    '                Else 'only 1 row
    '                    If Not IsDBNull(_mfilesBenchmarkingParticipantVPath = ds.Tables(0).Rows(0)("RefStudyFolder")) Then
    '                        _mfilesBenchmarkingParticipantVPath = ds.Tables(0).Rows(0)("RefStudyFolder").ToString()
    '                    End If
    '                    If Not IsDBNull(_mfilesBenchmarkingCompanyVPath = ds.Tables(0).Rows(0)("CoStudyFolder")) Then
    '                        _mfilesBenchmarkingCompanyVPath = ds.Tables(0).Rows(0)("CoStudyFolder").ToString()
    '                    End If
    '                    If Not IsDBNull(_mfilesRefiningGeneralVPath = ds.Tables(0).Rows(0)("RefineryGeneral")) Then
    '                        _mfilesRefiningGeneralVPath = ds.Tables(0).Rows(0)("RefineryGeneral").ToString()
    '                    End If
    '                End If
    '            End If
    '        Catch ex As Exception
    '            db.WriteLog("PopulateMFilesVPaths: " & refnumToUse, ex)
    '        Finally
    '            db.SQL = String.Empty
    '        End Try
    '    End If
    'End Sub


    Private Sub cboCheckListView_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboCheckListView.SelectionChangeCommitted
        RebuildChecklist(db, GetRefNumCboText(), cboCheckListView.SelectedItem.ToString())
    End Sub

    Private Sub cboCompany_TextChanged(sender As Object, e As EventArgs) Handles cboCompany.TextChanged
        cboCompany.DroppedDown = False
    End Sub

    Private Sub cboRefNum_TextChanged(sender As Object, e As EventArgs) Handles cboRefNum.TextChanged
        cboRefNum.DroppedDown = False
    End Sub

    Private Sub btnSecureSendRefresh_Click(sender As Object, e As EventArgs) Handles btnSecureSendRefresh.Click
        Me.Cursor = Cursors.WaitCursor
        ClearSecureSendTab()
        PopulateSecureSendTab()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnCorrespRefresh_Click(sender As Object, e As EventArgs) Handles btnCorrespRefresh.Click
        Me.Cursor = Cursors.WaitCursor
        ClearCorrespondenceTab()
        PopulateCorrespondenceTab()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub txtCI_LostFocus(sender As Object, e As EventArgs) Handles txtCI.LostFocus
        If SaveCI(True) Then
            ClearContinuingIssuesTab()
            BuildContinuingIssuesTab()
        End If
    End Sub

    Private Function GetCompanyBenchmarkName() As String
        'Per Greg, use RefineryID instead of Refnum
        Dim result As String = String.Empty
        Dim sql As String = "SELECT COMPANYID FROM DBO.TSORT T WHERE RefineryId = '" + GetRefNumCboText() + "'"
        db.SQL = sql
        Try
            Dim ds As DataSet = db.Execute()
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0)("COMPANYID").ToString().Length > 0 Then
                'EUR16Co-EXAMPLE
                result = cboStudy.Text.Trim() + "Co-" + ds.Tables(0).Rows(0)("COMPANYID").ToString().Trim()
            End If
        Catch
        Finally
            db.SQL = String.Empty
        End Try
        Return result
    End Function

    Private Function GetDataFor_lstVFNumbersMFiles(filter As String, numsFoundAlready As List(Of String),
        Optional containsNotStartsWith As Boolean = False) As List(Of String)
        T("GetDataFor_lstVFNumbersMFiles enter")
        If IsNothing(numsFoundAlready) Then
            numsFoundAlready = New List(Of String)
        End If
        T("GetDataFor_lstVFNumbersMFiles 1")
        'get files in correspondence for this refnum
        filter = filter.Replace("*", "")
        T("GetDataFor_lstVFNumbersMFiles 2")
        If Not IsNothing(_benchmarkingParticipantRefineryFiles) AndAlso Not IsNothing(_benchmarkingParticipantRefineryFiles.Files) Then
            For Each fil As ConsoleFile In _benchmarkingParticipantRefineryFiles.Files
                T("GetDataFor_lstVFNumbersMFiles 3")
                If Not containsNotStartsWith Then
                    T("GetDataFor_lstVFNumbersMFiles 4")
                    If (fil.FileName.StartsWith(filter)) OrElse (fil.FileName.StartsWith(filter & "FL")) Then   'AndAlso _
                        '(SelectedItemString = Mid(item.Files(count).Name, 3, 1)) Then  'i.e., the "1" in "VF1"
                        T("GetDataFor_lstVFNumbersMFiles 5")
                        Dim found As Boolean = False
                        Dim thisNumber As String = fil.FileName.Substring(2, 1)
                        If IsNumeric(thisNumber) Then
                            For Each num As String In numsFoundAlready
                                If num = thisNumber Then
                                    found = True
                                    Exit For
                                End If
                            Next
                            If Not found Then
                                T("GetDataFor_lstVFNumbersMFiles 6")
                                numsFoundAlready.Add(thisNumber)
                            End If
                        End If
                    End If
                Else
                    T("GetDataFor_lstVFNumbersMFiles 20")
                    If fil.FileName.Contains(filter) Then
                        T("GetDataFor_lstVFNumbersMFiles 21")
                        Dim found As Boolean = False
                        T("GetDataFor_lstVFNumbersMFiles 22")
                        Dim fileName As String = fil.FileName
                        T("GetDataFor_lstVFNumbersMFiles 23")
                        Dim index As Integer = fileName.IndexOf(filter)
                        T("GetDataFor_lstVFNumbersMFiles 24")
                        If Not fileName.EndsWith(filter) Then ' doesn't end with a number so don't count it
                            T("GetDataFor_lstVFNumbersMFiles 25")
                            Dim thisNumber As String = fileName.Substring(index + filter.Length, 1)
                            T("GetDataFor_lstVFNumbersMFiles 26")
                            If IsNumeric(thisNumber) Then
                                T("GetDataFor_lstVFNumbersMFiles 27")
                                For Each num As String In numsFoundAlready
                                    If num = thisNumber Then
                                        found = True
                                        Exit For
                                    End If
                                Next
                                If Not found Then
                                    T("GetDataFor_lstVFNumbersMFiles 30")
                                    numsFoundAlready.Add(thisNumber)
                                End If
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return numsFoundAlready
        T("GetDataFor_lstVFNumbersMFiles-exiting")
    End Function

    Private Sub PopulateVFFilesMFiles(ByVal nameFilter As String,
                            ByVal SelectedItemString As String, ByRef VFFiles As List(Of ConsoleFile),
                            Optional ByVal containsNotStartsWith As Boolean = False)
        For Each fil As ConsoleFile In _benchmarkingParticipantRefineryFiles.Files
            Dim filter = nameFilter.Replace("*", "")
            filter = filter & SelectedItemString
            Dim match = False
            If Not containsNotStartsWith Then
                If (fil.FileName.StartsWith(filter)) Then
                    ' AndAlso (SelectedItemString = Mid(fil.FileName, 3, 1)) Then  'i.e., the "1" in "VF1"
                    match = True
                End If
            Else
                If (fil.FileName.Contains(filter)) Then
                    match = True
                End If
            End If

            If match Then
                Dim formatDocumentDate As String = String.Empty
                If fil.CheckedOutTo > 0 Then
                    formatDocumentDate = "(Checked out to " + fil.CheckedOutToUserName + "):  "
                End If
                'VFFiles.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                formatDocumentDate = formatDocumentDate & fil.LastModifiedUtcDate.ToString("dd-MMM-yy")
                Dim cfile As New ConsoleFile(True)
                cfile.Id = fil.Id
                cfile.FileName = Format(formatDocumentDate & " | " & fil.FileName & "." & fil.FileExtension)
                VFFiles.Add(cfile)
            End If
        Next
    End Sub

    'Private Function 'RefreshMFilesCollections(Optional forceRefresh As Integer = -1) As Boolean
    '    Dim companyBenchmarkName As String = GetCompanyBenchmarkName()
    '    Return _docsAccess.'RefreshMFilesCollections(GetRefNumCboText(), companyBenchmarkName, _benchmarkingParticipantRefineryFiles,
    '                                             _benchmarkingParticipantCompanyFiles, _refiningGeneralFiles, forceRefresh)
    'End Function


    Private Sub radChecklistAddNew_Click(sender As Object, e As EventArgs) Handles radChecklistAddNew.Click
        radChecklistEditIssue.Checked = False
        txtIssueID.ReadOnly = False
        txtIssueID.Text = String.Empty
        txtIssueName.Text = String.Empty
        txtDescription.Text = String.Empty
        tvIssues.SelectedNode = Nothing
        Dim results As New RefiningChecklistResults()
        For Each n As TreeNode In tvIssues.Nodes
            n.BackColor = Color.Empty
        Next
        tvIssues.SelectedNode = Nothing
    End Sub



    Private Sub lbl_ItemRemaining_Click(sender As Object, e As EventArgs) Handles lbl_ItemRemaining.Click
        ConsoleTabs.SelectTab(tabCheckList)
    End Sub

    Private Sub lbl_BlueFlags_Click(sender As Object, e As EventArgs) Handles lbl_BlueFlags.Click
        ShowTimeGrade()
        ConsoleTabs.SelectTab(tabTimeGrade)
    End Sub

    Private Sub lbl_RedFlags_Click(sender As Object, e As EventArgs) Handles lbl_RedFlags.Click
        ShowTimeGrade()
        ConsoleTabs.SelectTab(tabTimeGrade)
    End Sub

    Private Sub btnVerifySend_Click(sender As Object, e As EventArgs) Handles btnVerifySend.Click
        Try
            'check verify send status
            btnVerifySend.Text = "Checking"
            btnVerifySend.BackColor = Color.Blue
            VerifySendButtonColor()
            Thread.Sleep(1000)
        Catch ex As Exception
            MsgBox("Checking VerifySend Error: " + ex.Message)
            Exit Sub
        End Try
        btnVerifySend.Text = "VerifySend"

    End Sub

    Private Sub radChecklistEditIssue_Click(sender As Object, e As EventArgs) Handles radChecklistEditIssue.Click
        radChecklistAddNew.Checked = False
        txtIssueID.ReadOnly = True
    End Sub

    Private Sub cboConsultant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboConsultant.SelectedIndexChanged

    End Sub

    Private Sub cboRefNum_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRefNum.SelectedIndexChanged
    End Sub



    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click
        'btnSendPrelimPricing_Click(sender, e)
        'Dim help As String = String.Empty
        'Dim w As Excel.Workbook
        'Try
        '    Dim xl As Excel.Application
        '    xl = Utilities.GetExcelProcess()
        '    w = xl.Workbooks.Open("C:\temp\test.xlsm", False, True)
        '    xl.Visible = False
        '    help = xl.Run("Macro1", "Test Project Text")
        'Catch exXls As Exception
        '    If exXls.Message.Contains("HRESULT") Then
        '        MsgBox("Error geting ")
        '    End If
        'Finally
        '    Try
        '        w.Close()  '
        '    Catch
        '    End Try
        'End Try
    End Sub



    Private qtyDone As Integer = 0


    'Private Sub btnMassPushPNToMFiles_Click(sender As Object, e As EventArgs)
    '    qtyDone = 0
    '    Dim ofd As New OpenFileDialog()
    '    Dim csvPath As String = String.Empty
    '    Dim results As String = String.Empty
    '    Dim tempPath As String = InputBox("Enter path to working folder to use")
    '    If Not Directory.Exists(tempPath) Then
    '        MsgBox("No working folder exists, quitting")
    '        Return
    '    End If
    '    ofd.Title = "Choose .csv file of refnums and colocs:"
    '    ofd.ShowDialog()
    '    If ofd.FileName.Length < 0 OrElse Not ofd.FileName.ToUpper().EndsWith(".CSV") Then
    '        MsgBox("No csv file selected, quitting")
    '        Return
    '    End If
    '    csvPath = ofd.FileName
    '    Using rdr As New StreamReader(csvPath)
    '        While Not rdr.EndOfStream
    '            Dim arr As String() = rdr.ReadLine().Split(",")
    '            Dim result As String = PresentersMassNotesMFiles(arr(0), arr(1), tempPath)
    '            If result.Length > 0 Then results = results & Environment.NewLine & result
    '        End While
    '    End Using
    '    MsgBox("Quantity done: " & qtyDone.ToString())
    '    If results.Trim().Length > 0 Then
    '        MsgBox(results)
    '    Else
    '        MsgBox("Done")
    '    End If
    '    ''RefreshMFilesCollections(0)
    'End Sub
    Private Function PresentersMassNotesMFiles(localRefNum As String, localCoLoc As String, tempPath As String) As String
        Throw New System.Exception("PresentersMassNotesMFiles() does not clean up memory and Close files correctly; do not use")
        Try
            Dim myText As String = "Presenter Notes for" & vbCrLf & localCoLoc & vbCrLf & vbCrLf
            'pull from MFiles if exists there. Else create.
            'Dim docFileAccess As New DocsFileSystemAccess(VerticalType.OLEFINS)
            Dim docs As ConsoleFilesInfo = _docsAccess.GetDocNamesAndIdsByBenchmarkingParticipant(localRefNum, localRefNum)
            For Each item As ConsoleFile In docs.Files
                If item.DeliverableType = "Presenters Notes" Then 'deliverable = 17  'Presenters Notes" Then
                    '_docsAccess.OpenFileFromMfiles(item.Id, localRefNum, localRefNum)
                    qtyDone = qtyDone + 1
                    Return String.Empty
                End If
            Next
            Dim pnTemplatePath As String = String.Empty
            'Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will push the notes to MFiles.", "Wait", MessageBoxButtons.YesNo)
            Try
                Dim params = New List(Of String)
                Dim row As DataRow
                lblStatus.Text = "Creating Presenter's Word file..."
                params.Add("RefNum/" + localRefNum)
                ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        myText += row("Note").ToString
                    Else
                        myText += ""
                    End If
                End If
                Dim strw As New StreamWriter(tempPath & "\PN_" & localCoLoc & ".txt")
                strw.WriteLine(myText)
                strw.Close()
                strw.Dispose()
                Dim docPath As String = Utilities.ConvertToDoc(tempPath & "\", tempPath & "\PN_" & localCoLoc & ".txt")
                Dim strfilename As String = Utilities.ExtractFileName(docPath)
                Dim deliverable As Integer = 17
                If _docsAccess.UploadDocToMfiles(tempPath & "\" & strfilename, 52, localRefNum, deliverable, -1, DateTime.Today) Then
                    ''then reopen from MFIles and check out, delete local file at end of this method
                    'Dim mFilesFilename As String = Utilities.FileNameWithoutExtension(strfilename)
                    ''_docsAccess.OpenFileFromMfiles(mFilesFilename, GetRefNumCboText(), Utilities.GetLongRefnum(GetRefNumCboText()))
                    'Dim result As String = _docsAccess.OpenFileFromMfiles(mFilesFilename, localRefNum, localRefNum)
                    'If result.Length > 0 Then
                    '    Return "Error uploading in PresentersNotesMFiles() for Refnum " & localRefNum & ": " & result
                    'End If
                Else
                    Return "Error uploading in PresentersNotesMFiles() for Refnum " & localRefNum
                End If
                Try
                    File.Delete(tempPath + strfilename)
                Catch 'not critical that it get deleted at this second
                End Try
                Try
                    File.Delete(tempPath & "PN_" & localCoLoc & ".txt")
                Catch 'not critical that it get deleted at this second
                End Try

                lblStatus.Text = "Presenter's Word file created and locked..."
                txtNotes.Text = "~" & txtNotes.Text
                lblStatus.Text = UpdatePN(localRefNum)
                'BuildNotesTab()
                btnCreatePN.Text = "Open PN File"
                ''RefreshMFilesCollections(0)
            Catch ex1 As System.Exception
                Return "Error1 in PresentersNotesMFiles() for Refnum " & localRefNum & ": " & ex1.Message
            End Try
            txtNotes.ReadOnly = True
            Return String.Empty
        Catch ex As Exception
            Return "Error in PresentersNotesMFiles() for Refnum " & localRefNum & ": " & ex.Message
        End Try
    End Function

    Private Sub MainConsole_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        'check verify send status
        VerifySendButtonColor()

    End Sub

    Private Sub tvwClientTools_NodeMouseDoubleClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles tvwClientTools.NodeMouseDoubleClick
        Try
            Process.Start(e.Node.Tag)
        Catch ex As System.Exception
            db.WriteLog("tvwClientTools_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try

    End Sub

    Private Sub tvwClientTools2_NodeMouseDoubleClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles tvwClientTools2.NodeMouseDoubleClick
        Try
            Process.Start(e.Node.Tag)
        Catch ex As System.Exception
            db.WriteLog("tvwClientTools2_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try

    End Sub
End Class
