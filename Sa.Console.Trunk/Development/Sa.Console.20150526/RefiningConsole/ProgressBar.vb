﻿Public Class PNProgressBar
    Private _minimum As Integer
    Public Property Minimum() As Integer
        Get
            Return _minimum
        End Get
        Set(ByVal value As Integer)
            _minimum = value
        End Set
    End Property

    Private _maximum As Integer
    Public Property Maximum() As Integer
        Get
            Return _maximum
        End Get
        Set(ByVal value As Integer)
            _maximum = value
        End Set
    End Property
    Private _step As String
    Public Property CountStep() As String
        Get
            Return _step
        End Get
        Set(ByVal value As String)
            _step = value
        End Set
    End Property

    Private Sub ProgressBar_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        pbCreated.Minimum = _minimum
        pbCreated.Maximum = _maximum
        pbCreated.Step = _step
    End Sub

    Public Sub InitializePB(ByVal _Minimum As Integer, ByVal _Maximum As Integer, ByVal _Step As Integer)
        If Me.InvokeRequired Then
            pbCreated.Invoke(Sub() InitializePB(_Minimum, _Maximum, _Step))
        Else
            pbCreated.Minimum = _Minimum
            pbCreated.Maximum = _Maximum
            pbCreated.Step = _Step
            Me.Refresh()
        End If
    End Sub
    Public Sub PerformStepPB(_study As String)
        If Me.InvokeRequired Then
            pbCreated.Invoke(Sub() PerformStepPB(_study))
        Else
            pbCreated.PerformStep()
            Label1.Text = "Creating " & _study
            Me.Refresh()
        End If
    End Sub


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

   
End Class