﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Console")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("HSB Solomon Associates LLC")> 
<Assembly: AssemblyProduct("Console")> 
<Assembly: AssemblyCopyright("© 2018 HSB Solomon Associates LLC")> 
<Assembly: AssemblyTrademark("HSB Solomon Associates LLC")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1ae6b327-faf2-4e79-b2a1-3424ec47320d")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("5.4.4.5")>


