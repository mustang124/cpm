﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PowerService;
using sa.Internal.Console.DataObject;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using Sa.Console.Common;


namespace PowerService.Tests
{
    [TestClass]
    public class PowerTests1
    {
        DataObject _dAL;
        PowerManager _mgr;
        string _profileConsolePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + @"\" + @"Console\";

        [TestInitialize]
        public void Startup()
        {
            _dAL = new DataObject(DataObject.StudyTypes.POWER,string.Empty,string.Empty);
            //_dAL.ConnectionString = ConfigurationManager.ConnectionStrings["PowerConx"].ToString();
            _mgr= new PowerManager(_dAL);
            
        }

        [TestMethod]
        public void GetStudiesTest()
        {
               var studies =_mgr.GetStudies();
                Assert.IsTrue(studies.Count==1);
                Assert.AreEqual(studies[0], 2016m);
        }

        [TestMethod]
        public void GetSiteIdsTest()
        {
            var siteIds = _mgr.GetSiteIds(2016);
            Assert.IsTrue(siteIds.Count > 0);
        }

        [TestMethod]
        public void GetCompanynamesWithSiteIdsTest()
        {
            var siteIds = _mgr.GetCompanynamesWithSiteIds(2016);
            Assert.IsTrue(siteIds.Count > 0);
        }

        [TestMethod]
        public void GetRefNumsTest()
        {
            var refNums = _mgr.GetRefNums("YYYCC16", 2016);
            Assert.IsTrue(refNums.Count > 0);
        }

        [TestMethod]
        public void GetUnitnamesWithRefNumsTest()
        {
            var unitNames = _mgr.GetUnitnamesWithRefNums("YYYCC16", 2016);
            Assert.IsTrue(unitNames.Count > 0);
        }

        [TestMethod]
        public void GetConsultantInitialsTest()
        {
            string c = _mgr.GetConsultantInitials("YYYCC16");
            Assert.IsTrue(c.Length > 0);
        }

        [TestMethod]
        public void GetConsultantNameTest()
        {
            string c = _mgr.GetConsultantName("AJC");
            Assert.IsTrue(c.Length > 0);
        }

        [TestMethod]
        public void GetConsultantEmailTest()
        {
            string c = _mgr.GetConsultantEmail("YYYCC16");
            Assert.IsTrue(c.Length > 0);
        }

        [TestMethod]
        public void GetConsultantTest()
        {
            string c = _mgr.GetConsultant("YYYCC16", 2016);
            Assert.IsTrue(c.Length > 3);
        }

        [TestMethod]
        public void ChangeConsultantTest()
        {
            bool erred = false;
            try
            {
                _mgr.ChangeConsultant("YYYCC16", 2016, "AJC");
            }
            catch
            {
                erred = true;
            }
            Assert.IsFalse(erred);
        }

        [TestMethod]
        public void GetAllConsultantsTest()
        {
            var cs = _mgr.GetAllConsultants( 2016);
            Assert.IsTrue(cs.Count >0);
        }

        [TestMethod]
        public void GetContactInfoTest()
        {
            string siteId = "YYYCC16"; // "299PN16";
            Sa.Console.Common.PowerClientInfo clientInfo = _mgr.GetClientInfo(siteId);
            Assert.IsTrue(clientInfo.SiteID == siteId);
        }
      
        [TestMethod]
        public void MakeDefaultConsoleSettingsTest()
        {
            string filePath =_profileConsolePath +  @"temp\ConsoleSettings\POWERSettings.txt";
            bool result = _mgr.MakeDefaultPowerSettingsFile(filePath);
            Assert.IsTrue(result);
        }
        
        [TestMethod]
        public void GetConsoleSettingsTest()
        {
            string filePath = _profileConsolePath + @"temp\ConsoleSettings\POWERSettings.txt";
            Dictionary<string,string> settings = _mgr.GetPowerSettings( filePath);
            Assert.IsTrue(settings.Count==1);
        }

        [TestMethod]
        public void MakeWriteLastStudyTest()
        {
            string year = "2016";
            string filePath = _profileConsolePath + @"temp\ConsoleSettings\POWERLastStudy.txt";
            System.IO.File.Move(filePath,filePath + ".orig");
            bool result = _mgr.WriteLastStudy(filePath, year);
            Assert.IsTrue(result);
            System.IO.File.Delete(filePath);
            System.IO.File.Move(filePath + ".orig",filePath);
        }

        [TestMethod]
        public void GetLastStudyTest()
        {
            string filePath = _profileConsolePath + @"temp\ConsoleSettings\POWERLastStudy.txt";
            Assert.IsTrue(_mgr.GetLastStudy(filePath) == "2016");
        }

        [TestMethod]
        public void GetCompanyPasswordTest()
        {
            string pw = _mgr.GetCompanyPassword("YYYCC16", 2016);
            //var refNums = _mgr.GetRefNums("VGE9PN15P", 2015);
            Assert.IsTrue(pw.Length>0);
        }

		[TestMethod]
        public void GetReturnFilePasswordTest()
        {
            string notes = _mgr.GetReturnFilePassword("YYYCC16");
            Assert.IsTrue(notes.Length > 0);
        } 	
		
        [TestMethod]
        public void GetNotesTest()
        {
            string notes = _mgr.GetNotes("YYYNA1C16"); //"VGE9PN15P");
            Assert.IsTrue(notes.Length > 0);
        }

        [TestMethod]
        public void AddNotesTest()
        {
            string notes1 = _mgr.GetNotes("YYYNA1C16"); 
            string notes2 = notes1 +  DateTime.Now.ToLongTimeString();
            _mgr.UpdateNotes("YYYNA1C16", notes2);
            Assert.IsTrue(_mgr.GetNotes("YYYNA1C16") == notes2);
            _mgr.UpdateNotes("YYYNA1C16", notes1);
            Assert.IsTrue(_mgr.GetNotes("YYYNA1C16") == notes1);
        }    

		[TestMethod]
        public void GetConsultingOpportunitiesTest()
        {
            string notes = _mgr.GetConsultingOpportunities("YYYNA1C16"); //"VGE9PN15P");
            Assert.IsTrue(notes.Length > 0);
        }  

		[TestMethod]
        public void GetContinuingIssuesTest()
        {
            DataTable table = _mgr.GetContinuingIssues("YYYNA1C16"); //"VGE9PN15P");
            Assert.IsTrue(table.Rows.Count>0);
        }

        [TestMethod]
        public void GetContinuingIssueTest()
        {
            DataSet ds = _mgr.GetContinuingIssue("YYYNA1C16", 1);
            Assert.IsTrue(ds.Tables[0].Rows.Count > 0);
        }

        [TestMethod]
        public void GetContinuingIssueTest2()
        {
            DataSet ds = _mgr.GetContinuingIssueByConsultant("YYYNA1C16", "SFB");
            Assert.IsTrue(ds.Tables[0].Rows.Count > 0);
        }

        [TestMethod]
        public void GetChecklistItemsTest()
        {
            bool foundOne = false;
            DataTable table = _mgr.GetChecklistItems("YYYCC16", "A");
            if (table.Rows.Count > 0)
                foundOne = true;
            table = _mgr.GetChecklistItems("YYYCC16", "C");
            if (table.Rows.Count > 0)
                foundOne = true;
            table = _mgr.GetChecklistItems("YYYCC16", "I");
            if (table.Rows.Count > 0)
                foundOne = true;
            Assert.IsTrue(foundOne);
        }

        [TestMethod]
        public void GetCompanyCoordinatorTest()
        {
            Sa.Console.Common.Contact coord = _mgr.GetCoord("YYYCC16");
            Assert.IsTrue(coord.ContactType == "COORD");
        }
        [TestMethod]
        public void ChangeCompanyCoordinatorTest()
        {
            bool result = _mgr.ChangeCompanyCoord("YYYCC16", "KARSTO CCGT", "IVAR MOSAKER", null, null, null, "KARSTO", "NORWAY", null, null, null, "TEST.NO");
            Assert.IsTrue(result==true);
            _mgr.ChangeCompanyCoord("YYYCC16", "KARSTO CCGT", "IVAR MOSAKER", null, null, null, "KARSTO", "NORWAY", null, null, null, "IVAR.MOSAKER@NATURKRAFT.NO");
        }

        [TestMethod]
        public void GetAlternateContactTest()
        {
            Sa.Console.Common.Contact coord = _mgr.GetCompanyContactInfo("YYYCC16", 2016, "ALT");
            Assert.IsTrue(coord.ContactType == "ALT");
       }

        [TestMethod]
        public void GetIdrXlsPathTest()
        {
            Assert.IsTrue(_mgr.GetIdrXlsPath(Int32.Parse("2016"), "YYYCC16").Length > 0);
        }

        [TestMethod]
        public void PopulateVFFilesMFilesTest()
        {
            List<Sa.Console.Common.ConsoleFile> mfiles = new List<Sa.Console.Common.ConsoleFile>();
            mfiles.Add(new ConsoleFile(true) { Id = 500, FileName = "VF0", FileExtension = "docx", DeliverableType = "12", CheckedOutTo = 99, CheckedOutToUserName = "Stuart Bard", ClassType = "52" });
            mfiles.Add(new ConsoleFile(true) { Id = 501, FileName = "VF1", FileExtension = "docx", DeliverableType = "12", CheckedOutTo = 99, CheckedOutToUserName = "Stuart Bard", ClassType = "52" });
            mfiles.Add(new ConsoleFile(true) { Id = 502, FileName = "VR1", FileExtension = "docx", DeliverableType = "12", CheckedOutTo = 99, CheckedOutToUserName = "Stuart Bard", ClassType = "52" });

            string nameFilter="VF";
            string selectedItemString="1";
            List<Sa.Console.Common.ConsoleFile> vfFiles= new List<Sa.Console.Common.ConsoleFile>();
            bool containsNotStartsWith = false;            
            string errors = _mgr.PopulateVFFilesMFiles(nameFilter, selectedItemString, mfiles,ref  vfFiles, 
                containsNotStartsWith);
            Assert.IsTrue(errors.Length < 1);
            Assert.IsTrue(vfFiles.Count == 1);
            Assert.IsTrue(vfFiles[0].Id == 501);

            selectedItemString = "9";
            vfFiles = new List<Sa.Console.Common.ConsoleFile>();
            containsNotStartsWith = false;
            errors = _mgr.PopulateVFFilesMFiles(nameFilter, selectedItemString, mfiles, ref  vfFiles,
                containsNotStartsWith);
            Assert.IsTrue(errors.Length < 1);
            Assert.IsTrue(vfFiles.Count == 0);

            selectedItemString = "";
            nameFilter = "VF";
            vfFiles = new List<Sa.Console.Common.ConsoleFile>();
            containsNotStartsWith = true;
            _mgr.PopulateVFFilesMFiles(nameFilter, selectedItemString, mfiles, ref  vfFiles,
                containsNotStartsWith);
            Assert.IsTrue(errors.Length < 1);
            Assert.IsTrue(vfFiles.Count == 2);
        }

        [TestMethod]
        public void DragDropForFilesCheckTest()
        {
            System.Collections.Specialized.StringCollection fileNames = new System.Collections.Specialized.StringCollection();
            fileNames.Add("Test.txt");
            bool fileGroupDescriptor=false;
            bool useMfiles=false;
            string error=string.Empty;
            /*
            List<string> filePaths = new List<string>(){@"C:\temp"};
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(filePaths[0] + @"\" + fileNames[0]))
            {
                writer.WriteLine("TEST");
            }
            */
            //0 is error, 1 is dragdrop for files, 2 is email dragged
            int result = _mgr.DragDropForFilesCheck(fileNames, fileGroupDescriptor, useMfiles, ref error);
            //System.IO.File.Delete(filePaths[0] + @"\" + fileNames[0]);
            Assert.IsTrue(error.Length < 1);
            Assert.IsTrue(result == 1);

            //email
            fileNames = new System.Collections.Specialized.StringCollection();
            fileGroupDescriptor = true;
            result = _mgr.DragDropForFilesCheck(fileNames, fileGroupDescriptor, useMfiles, ref error);
            Assert.IsTrue(error.Length <1);
            Assert.IsTrue(result == 2);

            //error: folder
            fileNames = new System.Collections.Specialized.StringCollection() { @"C:\temp" };
            result = _mgr.DragDropForFilesCheck(fileNames, fileGroupDescriptor, useMfiles, ref error);
            Assert.IsTrue(error.Length >0);
            Assert.IsTrue(result == 0);
        }

        [TestMethod]
        public void GetAllChecklistItemsTest()
        {
                DataTable dt = _mgr.GetAllChecklistItems("YYYCC16");
                Assert.IsTrue(dt.Rows.Count==1);
        }


/*
        [TestMethod]
        public void NextVxLegacyTest()
        {
            int success = 0;
            if (_mgr.NextVxLegacy("VF", "doc",  @"C:\Temp") == "1")
                success++;
            using (System.IO.StreamWriter w2 = new System.IO.StreamWriter(@"C:\temp\VFZ.doc"))
            { }
            if (_mgr.NextVxLegacy("VF", "doc",  @"C:\Temp") == "1")
                success++;
            using (System.IO.StreamWriter w2 = new System.IO.StreamWriter(@"C:\temp\VF1.docx"))
            { }
            // ? is one or one char wildcard. *  is 0 or more
            if (_mgr.NextVxLegacy("VF", "doc*",  @"C:\Temp") == "2")
                success++;
            System.IO.File.Delete(@"C:\temp\VFZ.doc");
            System.IO.File.Delete(@"C:\temp\VF1.docx");
            Assert.IsTrue(success == 3);
        }
        */
	}
}
