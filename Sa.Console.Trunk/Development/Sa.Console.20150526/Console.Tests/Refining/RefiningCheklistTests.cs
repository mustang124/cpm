﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SA.Console;
using sa.Internal.Console.DataObject;
using System.Collections.Generic;

namespace Console.Tests.Refining
{
    [TestClass]
    public class RefiningCheklistTests
    {
        DataObject _dAL = null;
        string _refnum = "777EUR16A";
        string _issueID = "S1";

        string _issueName = "S1Name";
        string _issueDesc = "S1Desc";

        [TestMethod]
        public void TestMethod1()
        {            
            string viewOrAdd = "Add";
            string chosenView = "All";
            ResetData();
            RefiningChecklistManager mgr = new RefiningChecklistManager(_dAL, _refnum, "SFB");

            //prep to add - clear boxes etc.
            List<string> changes = null;
            RefiningChecklistResults results = mgr.GetResults(chosenView, viewOrAdd, _issueID, false, changes);
            Assert.IsTrue(results.BoxesContent[0] == string.Empty);
            Assert.IsTrue(results.BoxesContent[1] == string.Empty);
            Assert.IsTrue(results.BoxesContent[2] == string.Empty);
            Assert.IsTrue(results.Completed.ToString().ToUpper()=="FALSE");
            Assert.IsTrue(results.IsChecked==false);            
            Assert.IsTrue(results.IsCustom.ToString().ToUpper()=="TRUE");
            Assert.IsTrue(results.SelectedNodeText == string.Empty);
            Assert.IsTrue(results.ShadedSelectedNodeText == string.Empty);
            Assert.IsTrue(results.Visible == false);

            //now user has entered text into boxes, pass them in here for commit to db
            //and redisplay
            changes = new List<string>(){_issueID,_issueName,_issueDesc};
            results = mgr.GetResults(chosenView, viewOrAdd, _issueID, false, changes);
            Assert.IsTrue(results.BoxesContent[0] == _issueID);
            Assert.IsTrue(results.BoxesContent[1] == _issueName);
            Assert.IsTrue(results.BoxesContent[2] == _issueDesc);
            Assert.IsTrue(results.Completed.ToString().ToUpper() == "N");
            Assert.IsTrue(results.IsChecked == false);
            Assert.IsTrue(results.IsCustom.ToString().ToUpper() == "TRUE");
            Assert.IsTrue(results.SelectedNodeText == string.Empty);
            Assert.IsTrue(results.ShadedSelectedNodeText == string.Empty);
            Assert.IsTrue(results.Visible == false);

            //User now edits existing record
            viewOrAdd = "View";
            //-show record as it is in DB
            results = mgr.GetResults(chosenView, viewOrAdd, _issueID, false, null);
            Assert.IsTrue(results.BoxesContent[0] == _issueID);
            Assert.IsTrue(results.BoxesContent[1] == _issueName);
            Assert.IsTrue(results.BoxesContent[2] == _issueDesc);
            Assert.IsTrue(results.Completed.ToString().ToUpper() == "N");
            Assert.IsTrue(results.IsChecked == false);
            Assert.IsTrue(results.IsCustom.ToString().ToUpper() == "TRUE");
            Assert.IsTrue(results.SelectedNodeText =="S1 - S1Name");
            Assert.IsTrue(results.ShadedSelectedNodeText == "S1 - S1Name");
            Assert.IsTrue(results.Visible == true);

            //-save user changes to DB and redisplay
            _issueName = "S1A";
            _issueDesc = "S1Desc A";
            changes = new List<string>() { _issueID, _issueName, _issueDesc };
            results = mgr.GetResults(chosenView, viewOrAdd, _issueID, false, changes);
            Assert.IsTrue(results.BoxesContent[0] == _issueID);
            Assert.IsTrue(results.BoxesContent[1] == _issueName);
            Assert.IsTrue(results.BoxesContent[2] == _issueDesc);
            Assert.IsTrue(results.Completed.ToString().ToUpper() == "N");
            Assert.IsTrue(results.IsChecked == false);
            Assert.IsTrue(results.IsCustom.ToString().ToUpper() == "TRUE");
            Assert.IsTrue(results.SelectedNodeText == "S1 - S1A");
            Assert.IsTrue(results.ShadedSelectedNodeText == "S1 - S1A");
            Assert.IsTrue(results.Visible == true);





            Assert.IsTrue("" == "Change below to use the  Console.AddIssue proc");
        }

        private void ResetData()
        {
            if(_dAL==null) 
            {
                _dAL = new DataObject(DataObject.StudyTypes.REFINING, "", "");
                // set in test prj .config: _dAL.ConnectionString=@"Data Source=(localdb)\ProjectsV12;Initial Catalog=Refining;Integrated Security=True";
                _dAL.SQL=string.Empty;
            }
            string sql = "DELETE FROM Val.Checklist Where Refnum = '" + _refnum + "' and IssueId = '" + _issueID + "'"; // and IsCustom=1"
            /*
            string sql = "UPDATE Val.Checklist SET IssueTitle = '', IssueText='', ";
            sql += "PostedBy = '', COmpleted = '', SetBY = '', SEtTIme = null, IsCustom = 0";
            sql +=  "Where Refnum = '" + _refnum + "' and IssueId = '" + _issueID + "'"; // and IsCustom=1"
            */
            _dAL.SQL = sql;
            _dAL.ExecuteNonQuery();
            _dAL.SQL = string.Empty;
        }
    }
}
