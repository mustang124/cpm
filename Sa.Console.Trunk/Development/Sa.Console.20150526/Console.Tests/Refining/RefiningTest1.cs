﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SA.Console;


namespace Console.Tests.Refining
{
    [TestClass]
    public class RefiningTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string yr = SA.Console.Utilities.GetRegionFromRefnum("777EUR16A", 0);
            Assert.IsTrue(yr == "EUR");
        }

        [TestMethod]
        public void TestMethod2()
        {
            string yr = SA.Console.Utilities.GetYearFromRefnum("777EUR16A", 0);
            Assert.IsTrue(yr == "16");
        }

        [TestMethod]
        public void TestMethod3()
        {
            string site = SA.Console.Utilities.GetSiteNumberFromRefnum("777EUR16A", 0);
            Assert.IsTrue(site == "777");
        }

        [TestMethod]
        public void TestCleanFilename()
        {
            string start = ".aZ/:*?<>|.txt";
            string site = SA.Console.Utilities.CleanFileName(start);
            Assert.IsTrue(site == ".aZ -     .txt");
        }

    }
}
