﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using SA.Console;
using System.Windows.Forms;

namespace ConsoleTests.Olefins
{
    [TestClass]
    public class DocsAccessTests
    {
        private string GetDevConfigHelperSetting(string settingName)
        {
            string value = string.Empty;
            try
            {
                value = DevConfigHelper.ReadConfig(settingName);                 
            }
            catch (NullReferenceException nullReferenceExceptionId)
            {
                try
                {
                    value = ConfigurationManager.AppSettings[settingName].ToString();
                }
                catch (Exception exId)
                {
                    Assert.Fail("Can't find setting " + settingName);
                }
            }
            return value;
        }

#region "DocsFileSystemAccess tests"
        [TestMethod]
        public void lstVRFilesItemsTestD()
        {
            string path = (@"C:\");
            SA.Console.DocsFileSystemAccess docsFileSystemAccess = new SA.Console.DocsFileSystemAccess(Main.VerticalType.OLEFINS);
            PrepTestDocsFileSystemAccessTestFiles(true, path + "VF1TEST.txt");
            PrepTestDocsFileSystemAccessTestFiles(true, path + "VR1TEST.txt");
            var items = docsFileSystemAccess.lstVRFilesItems(@"C:\", "1","15PCH998");
            docsFileSystemAccess = null;
            Assert.IsTrue(items.Count >0);
            //cleanup
            PrepTestDocsFileSystemAccessTestFiles(false, path + "VF1TEST.txt");
            PrepTestDocsFileSystemAccessTestFiles(false, path + "VR1TEST.txt");
        }

        private void PrepTestDocsFileSystemAccessTestFiles(bool CreateElseDelete, string FilePath)
        {
            if (CreateElseDelete)
            {
                StreamWriter writer = new StreamWriter(FilePath);
                writer.Close();
            }
            else
            {
                File.Delete(FilePath);
            }
        }

        [TestMethod]
        public void btnReceiptAck_ClickTestD()
        {
            Assert.Inconclusive("Can't test due to MsgBoxes and InputBox");
        }
        

        [TestMethod]
        public void TestValFax_BuildD()
        {
            bool success = false;
            string RefNum = "15PCH235"; //2015PCH235

            //must call mfiles.ValFax_Build directly instead; docsFileSystemAccess.ValFax_Build has an
            //input box that can't be tested
            //SA.Console.DocsFileSystemAccess docsFileSystemAccess = new SA.Console.DocsFileSystemAccess(Main.VerticalType.OLEFINS);
            SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, RefNum, "20" + RefNum);

            try
            {   
                //will need this path created:  C:\Users\SBARD\Console
                Assert.IsTrue(Directory.Exists(@"C:\Users\SBARD\Console"));
                //need this but it can just be an empty doc:
                Assert.IsTrue(File.Exists(@"C:\Users\SBARD\Console\New Microsoft Word Document.docx"));
                
                string Password = GetDevConfigHelperSetting("TestDbPassword");
                string strValFaxTemplate = string.Empty; // "Trans Pricing.docx";
                string           UserName ="sbard";
                string TemplatePath = @"C:\Users\Sbard\Console\";
                string           btnValFaxText ="TEST";
                string          TempDrive =@"C:\Users\";
                string           userInitials ="JSJ";
                

                sa.Internal.Console.DataObject.DataObject db =
                    new sa.Internal.Console.DataObject.DataObject(
                        sa.Internal.Console.DataObject.DataObject.StudyTypes.OLEFINS,UserName,Password);

                string StudyDrive =@"C:\";
                string StudyYear ="2015";
                string StudyRegion ="NA";
                string Study = "OLEFINSDEV";
                string TempPath =@"C:\Temp\";
                string CurrentRefNum = "15PCH235";  //2015PCH235
                string CorrPathParameter = @"C:\Users\SBARD\Console\";
                string deadlineDays = "5";
                OlefinsMainConsole.WordTemplate TemplateValues=new OlefinsMainConsole.WordTemplate();
                string VFFileName = string.Empty;

                /*
                //must call mfiles.ValFax_Build directly instead; docsFileSystemAccess.ValFax_Build has an
                //input box that can't be tested
                docsFileSystemAccess.ValFax_Build(ref strValFaxTemplate, ref UserName, TemplatePath,ref  btnValFaxText, 
                    TempDrive,ref  mUserName, RefNum, ref db, StudyDrive, StudyYear, StudyRegion, Study,
                    TempPath, ref CurrentRefNum, CorrPathParameter, ref  TemplateValues,ref VFFileName);
                */
                docsMFilesAccess.ValFax_Build(ref strValFaxTemplate,ref UserName, TemplatePath, ref  btnValFaxText,
                    ref  userInitials, RefNum, ref db, StudyDrive, StudyYear, StudyRegion, Study,
                    TempPath, ref CurrentRefNum, CorrPathParameter, deadlineDays, ref  TemplateValues, ref VFFileName);

                //would ordinarily call WordTemplateReplace() here, but it would upload new doc to MFiles, and we 
                // don't have a real test enviro.

                success = true;
            }
            catch (Exception ex)
            {
                string debugError = ex.Message;
                string error = docsMFilesAccess.ErrorLogType;
            }
            Assert.IsTrue(success);
        }

        //[TestMethod]  //This just calls mfiles directly. Tested below
        public void BuildVRFileTestD()
        {
            return;
        }
        
        /*
        [TestMethod]
        public void btnReceiptAck_ClickTestD()
        {
            //this just calls upload to mfiles, nothing else to test.
        }

        [TestMethod]
        public void BuildVRFileTestD()
        {
          //this just calls Mfiles method, nothing to test.
        }

        [TestMethod]
        public void GetDataFor_lstVFNumbersTestD()
        {
            //this just calls Mfiles method, nothing to test.
        }

        [TestMethod]
        public void lstVFFiles_DoubleClickTestD()
        {
            //this just calls Mfiles method, nothing to test.
        }

        [TestMethod]
        public void lstVRFilesItemsTestD()
        {
            //this just calls Mfiles method, nothing to test.
        }

        [TestMethod]
        public void NextVTestD()
        {
            //this just calls Mfiles method, nothing to test.
        }

        [TestMethod]
        public void UploadDocToMfilesTestD()
        {
               //don't do this since not much of a test enviro.
        }
        */

        [TestMethod]
        public void BuildVRFileTestM()
        {   
            string mUserName = "SFB";
            string strTextForFile = "TEST";
            string CorrPathVR = @"C:\Users\SBARD\Console\";
            string lstVFNumbersText = "1";
            ////string shortrefNum = "15PCH998";
            DocsMFilesAccess docsMFilesAccessM = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            //string result = docsMFilesAccess.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText);
            //Assert.IsTrue(result == "VR" + lstVFNumbersText + ".txt has been built.");
            //strTextForFile = string.Empty;
            //result = docsMFilesAccess.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText);
            //Assert.IsTrue(result == "VR file build was cancelled.");
            //File.Delete(CorrPathVR + "VA" + lstVFNumbersText + ".txt");
            Assert.Inconclusive("??"); 
        }



#endregion

//#region "DocsMFilesAccess tests"
/*        
        [TestMethod]
        public void BuildVRFileTestM()
        {
            string mUserName = "SFB";
            string strTextForFile = "TEST";
            string CorrPathVR = @"C:\Users\SBARD\Console\";
            string lstVFNumbersText = "1";
            //string shortrefNum = "15PCH998";
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            string result = docsMFilesAccess.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText);
            Assert.IsTrue(result == "VR" + lstVFNumbersText + ".txt has been built.");
            strTextForFile = string.Empty;
            result = docsMFilesAccess.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText);
            Assert.IsTrue(result == "VR file build was cancelled.");
            File.Delete(CorrPathVR + "VA" + lstVFNumbersText + ".txt");
        }
        

                [TestMethod]
                public void GetMfilesTestFileM()
                {
                    DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
                    ConsoleFilesInfo files = docsMFilesAccess.GetVF1FileInfo();
                    Assert.IsTrue(files.Files[0].FileName.StartsWith("VF1"));
                }

                [TestMethod]
                public void GetMfileslstVRFilesItemsM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH162", "2013PCH162");
                    List<object> files = docsMFilesAccess.lstVRFilesItems("","2");
                    bool found = false;
                    foreach (object obj in files)
                    {
                        if (obj.ToString().Contains("VR2") == true)
                        {
                            found = true;
                            break;
                        }
                    }
                    Assert.IsTrue(found);
                }

                [TestMethod]
                public void GetVF1FileInfoTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH162", "2013PCH162");
                    ///GetVF1FileInfo() As ConsoleFilesInfo
                    ConsoleFilesInfo info = docsMFilesAccess.GetVF1FileInfo();
                    Assert.IsTrue(info.Files.Count >0);
                }

                [TestMethod]
                public void lstVRFilesItemsTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH162", "2013PCH162");
                    List<object> lst = docsMFilesAccess.lstVRFilesItems("13PCH162", "1");
                    Assert.IsTrue(lst.Count > 0);
                }

                //tested above in docsFileSystemAccess
                public void ValFaxBuildTestM()
                {
        
                }
        

        
                //would ordinarily test this, but it would upload new doc to MFiles, and we 
                // don't have a real test enviro.
                [TestMethod]         
                public void BuildMfilesVRFileTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    Assert.Inconclusive("Don't upload VR quite yet");
                    Assert.IsTrue(docsMFilesAccess.BuildVRFile("JSJ", "TEST", @"C:\Temp", "1").Contains("has been built"));
                }
        

                [TestMethod]
                public void DocExistsTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH162", "2013PCH162");
                    Assert.IsTrue(docsMFilesAccess.DocExists("VF1", "2015PCH998", true) != null);
                }
                [TestMethod]
                public void DownloadFileTestM()
                {
                    string folderPath = @"C:\temp";
                    string fileName = "VF1 Solomon - SINGAPORE.doc.doc";
                    int docId = 3452;
                    bool success = false;
                    try
                    {
                        File.Delete(folderPath + @"\" + fileName);
                    } catch(Exception)
                    {  }
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    object result = docsMFilesAccess.DownloadFile(folderPath, docId);
                    success = File.Exists(folderPath + @"\" + fileName);
                    try
                    {
                        File.Delete(folderPath + @"\" + fileName);
                    }
                    catch (Exception) { }
                    Assert.IsTrue(success);
                }

                [TestMethod]
                public void GetDataFor_lstVFNumbersTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    List<string> results =  docsMFilesAccess.GetDataFor_lstVFNumbers("VF1","",null );
                    Assert.IsTrue(results.Count > 0);
                }

                [TestMethod]
                public void GetDocByNameAndRefnumTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    var result = docsMFilesAccess.GetDocByNameAndRefnum("VF1", "2015PCH998", true);
                    Assert.IsNotNull(result);
                }

                [TestMethod]
                public void GetDocNamesAndIdsByBenchmarkingParticipantTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    ConsoleFilesInfo results = docsMFilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant("2015PCH998");
                    Assert.IsTrue(results.Files.Count > 0);
                }

                [TestMethod]
                public void GetDocNamesByBenchmarkingParticipantTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    List<string> results = docsMFilesAccess.GetDocNamesByBenchmarkingParticipant("2015PCH998");
                    Assert.IsTrue(results.Count > 0);
                }

                [TestMethod]
                public void GetDocsByBenchmarkingParticipantTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    var result = docsMFilesAccess.GetDocsByBenchmarkingParticipant("2015PCH998");
                    Assert.IsNotNull(result);
                }

                [TestMethod]
                public void GetValueOfPropertyValueTestM()
                {
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    object[] ctor = new object[]
                    {
                        Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998"
                    };
                    PrivateObject privateObject = new PrivateObject(typeof(DocsMFilesAccess), ctor);
                    List<RestTools.PropertyValue> list = new List<RestTools.PropertyValue>();
                    RestTools.TypedValue tv = new RestTools.TypedValue();
                    tv.DataType =  RestTools.MFDataType.Lookup;
                    RestTools.Lookup lu = new RestTools.Lookup();
            
            
            
            
            
                    //call GetVF1FileInfo to get props to use






                    lu.Item=157;
                    tv.Lookup=lu;
                    RestTools.PropertyValue pv = new RestTools.PropertyValue();
                    pv.PropertyDef = 1211;
                    pv.TypedValue = tv;

                    object[] params9 = new object[2];
                    params9[0] = list;
                    params9[1] = 12;
                    var test = privateObject.Invoke("GetValueOfPropertyValue", params9);
            
                     //GetValueOfPropertyValue(propertyValues As List(Of RestTools.PropertyValue), propertyDefNumber As Integer) As RestTools.TypedValue
                    Assert.Inconclusive("Need to fix this later");
                }

                [TestMethod]
                public void lstVFFiles_DoubleClickTestM()
                {
                    string folderPath  = @"C:\temp";
                    if (!folderPath.EndsWith(@"\"))
                        folderPath.Remove(folderPath.Length - 1);
                    string fileName = "VF1 Solomon - SINGAPORE.doc";
                    int docId = 3452;
                    try
                    {
                        File.Delete(folderPath + @"\" + fileName);
                    }
                    catch(Exception){}
                    SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(
                        Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
                    //{"Input string was not in a correct format."}
                    docsMFilesAccess.lstVFFiles_DoubleClick(fileName, folderPath);
                    Assert.IsTrue(File.Exists(folderPath + @"\" + fileName));
                    File.Delete(folderPath + @"\" + fileName);
                }

                [TestMethod]
                public void NextReturnFileTestM()
                {
                }

                [TestMethod]
                public void NextVTestM()
                {
                }

                [TestMethod]
                public void PopulateVFFilesTestM()
                {
                }

                [TestMethod]
                public void UploadNewIdrTestM()
                {
                }

                [TestMethod]
                public void UploadToMFilesTestM()
                {
                }

                #endregion
        */
    }
}
