﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Configuration;

namespace Console.Tests
{
    public static class DevConfigHelper
    {
        private static char _delim = Convert.ToChar(14);
        public static string ReadConfig(string key)
        {
            string result = string.Empty;
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString();
            try
            {
                StreamReader streamReader = new StreamReader(folder + @"\DevConfigHelper.txt");
                List<string> valueslist = new List<string>();
                while(!streamReader.EndOfStream)
                {
                    valueslist.Add(streamReader.ReadLine());
                }
                streamReader.Close();
                foreach (string item in valueslist)
                {
                    //string[] delimiter = { "^^^" };
                    string[] keyValuePair = item.Split(_delim); //delimiter);
                    if(keyValuePair[0].ToUpper() == key.ToUpper())
                    {
                        result = keyValuePair[1];
                        break;
                    }
                }                
            }
            catch (Exception exRead)
            {
                //'do the next try below
            }

            try
            {
                if (result.Length < 1)
                {
                    result = ConfigurationManager.AppSettings[key].ToString();
                }
            }
            catch (Exception exConfig)
            {
                return string.Empty;
            }
            return result;
        }

        private static string TrimDirectories(string directories, int countToRemove)
        {
            string[] startPath = directories.Split('\\');
            string endPath = string.Empty;
            for(int count = 0;count < (startPath.Length- countToRemove);count++)
            {
                endPath +=startPath[count] + "\\";
            }
            return endPath.Substring(0,endPath.Length-1);
        }
    }
}
