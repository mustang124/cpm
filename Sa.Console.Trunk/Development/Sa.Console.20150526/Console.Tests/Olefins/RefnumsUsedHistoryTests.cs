﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SA.Console;


namespace Console.Tests.Olefins
{
    [TestClass]
    public class RefnumsUsedHistoryTests
    {
        [TestMethod]
        public void CreateClassTest()
        {
            RefnumsUsedHistory history = new RefnumsUsedHistory(Main.VerticalType.OLEFINS);
            string origRefnum = history.GetLastRefnum("PCH13");
            Assert.IsTrue(origRefnum.Length > 0);
            history.UpdateRefnum( "PCH13","ABCDEF");
            string testRefnum = history.GetLastRefnum("PCH13");
            Assert.IsTrue(testRefnum == "ABCDEF");
            history.UpdateRefnum("PCH13", origRefnum);
        }

    }
}
