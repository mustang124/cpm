﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sa.Internal.Console.DataObject;
using System.Data;
using System.IO;
using Sa.Console.Common;

namespace Sa.Console.OlefinsService
{
    public class OlefinsManager
    {
        private sa.Internal.Console.DataObject.DataObject _dAL;        
        private char _delim = Convert.ToChar(14);

        public OlefinsManager(sa.Internal.Console.DataObject.DataObject dAL)
        {
            _dAL = dAL;            
        }

        public IList<string> GetStudies()
        {
            string sql = "SELECT distinct studyyear FROM [Olefins].[cons].[TSort] where studyid = 'PCH' order by studyyear desc;";
            DataTable dt = Select(sql);
            List<int> allYears = GetDataFromColumn<int>("StudyYear", dt);
            List<string> fiveYears = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                string thisYear = "PCH" + allYears[i].ToString().Substring(2, 2);
                fiveYears.Add(thisYear);
            }
            return fiveYears;
        }

        public bool MakeDefaultOlefinsSettingsFile(string filePath)
        {
            bool result = false;
            try
            {
                IList<string> studies = GetStudies(); 
                using (StreamWriter writer = new StreamWriter(filePath, false))
                {
                    foreach (string study in studies)
                    {
                        string line = study.Replace("PCH", "");
                        writer.WriteLine("20" + line + _delim + "");
                    }
                    result = true;
                }
            }
            catch
            { }
            return result;
        }

        public Dictionary<string, string> GetOlefinsSettings(string filePath)
        {
            Dictionary<string, string> result = null;
            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        if (result == null)
                            result = new Dictionary<string, string>();
                        string line = reader.ReadLine();
                        string[] lineParts = line.Split(_delim);
                        result.Add(lineParts[0], lineParts[1]);
                    }
                }
            }
            catch { }
            return result;
        }

        public string GetLastStudy(string filePath)
        {
            string result = string.Empty;
            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    result = reader.ReadLine();
                }
            }
            catch
            {
                result = string.Empty;
            }
            return result;
        }


        //===============================================
        #region "Data methods"
        private List<T> GetDataFromColumn<T>(string columnName, DataTable dt)
        {
            List<T> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<T>();
                foreach (DataRow row in dt.Rows)
                {
                    try
                    {
                        if (typeof(T) == typeof(System.String))
                        {
                            string val = row[columnName].ToString().Trim();
                            list.Add((T)Convert.ChangeType(val, typeof(String))); //Convert.ChangeType(row[columnName], typeof(System.String));
                        }
                        else
                        {
                            list.Add((T)Convert.ChangeType(row[columnName], typeof(T)));
                        }
                    }
                    catch (Exception ex)
                    {
                        string debugHere = ex.Message;
                    }
                }
            }
            return list;
        }

        private DataTable Select(string sql)
        {
            DataSet result = new DataSet();
            try
            {
                _dAL.SQL = sql;
                result = _dAL.Execute();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                _dAL.SQL = string.Empty;
            }
            return result.Tables[0];
        }

        private DataTable Exec(string procName, List<string> parameters)
        {
            DataSet result = new DataSet();
            try
            {
                result = _dAL.ExecuteStoredProc(procName, parameters);
            }
            catch (Exception ex)
            {
                string breakpoint = ex.Message;
                return null;
            }
            if (result == null)
            {
                return null;
            }
            else
            {
                if (result.Tables.Count > 0)
                {
                    return result.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        #endregion
    }
}
