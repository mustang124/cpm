﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sa.Console.Common
{
    public class ConsoleFile
    {
        bool _usingMfiles =false;
        string _class = string.Empty;
        int _id =-1;
        string _ext = string.Empty;

        public ConsoleFile(bool mFiles)
        {
            _usingMfiles = mFiles;
        }

        public string ClassType { get; set; }
        public string FileName { get; set; }
        public int Id { get; set; }
        public string DeliverableType { get; set; }
        public DateTime? DocumentDate { get; set; }
        public string FilePath { get; set; }
        public DateTime? CreatedUtcDate { get; set; }
        public string FileExtension { get; set; }
        public int CheckedOutTo { get; set; }
        public string CheckedOutToUserName { get; set; }
        public DateTime LastModifiedUtcDate { get; set; }
        
    }
}
