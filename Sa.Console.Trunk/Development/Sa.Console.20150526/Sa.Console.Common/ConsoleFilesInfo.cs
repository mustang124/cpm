﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sa.Console.Common
{
    public class ConsoleFilesInfo
    {

        bool _usingMfiles=false;
        string _benchmarkingParticipantName= string.Empty;
        string _refNumOrPowerSiteid = string.Empty;

        public ConsoleFilesInfo(bool mfiles, Sa.Console.Common.VerticalType vertical)
        {
            _usingMfiles = mfiles;
        }

        private  List<ConsoleFile> _files = new List<ConsoleFile>();
        public List<ConsoleFile> Files //{get;set;}
        {
            get { return _files; }
            set { _files = value; }
        }
        public int BenchmarkingParticipantId { get; set; }
        public string BenchmarkingParticipantName 
        {
            get { return _benchmarkingParticipantName; }
            set
            {
                _benchmarkingParticipantName = value;
                string[] refnumber = _benchmarkingParticipantName.Split(" - ".ToCharArray());
                _refNumOrPowerSiteid = refnumber[0];
            }
        }
        public string RefNumOrPowerSiteid
        {
            get { return _refNumOrPowerSiteid; }
        }

    }
}
