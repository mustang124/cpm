﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sa.Console.Common
{
    public enum VerticalType
    {
        REFINING,
        OLEFINS,
        POWER,
        RAM,
        PIPELINES,
        NGPP
    }
}
