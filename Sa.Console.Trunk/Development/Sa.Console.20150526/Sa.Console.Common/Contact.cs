﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sa.Console.Common
{
    public class Contact
    {
        public string AltEmail { get; set; }
        public string AltFirstName { get; set; }
        public string AltFullName { get; set; }
        public string AltLastName { get; set; }
        public string AltPhone { get; set; }
        public string ContactType { get; set; }
        public string DCEmail { get; set; }
        public string DCName { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Fax2 { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string MailAdd1 { get; set; }
        public string MailAdd2 { get; set; }
        public string MailAdd3 { get; set; }
        public string MailCity { get; set; }
        public string MailCountry { get; set; }
        public string MailState { get; set; }
        public string MailZip { get; set; }
        public string MaintEmail { get; set; }
        public string MaintMgrName { get; set; }
        public string OpsEmail { get; set; }
        public string OpsMgrName { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string PricingEmail { get; set; }
        public string PricingName { get; set; }
        public string RefAddEmail { get; set; }
        public string RefAddName { get; set; }
        public string RefEmail { get; set; }
        public string RefineryCountry { get; set; }
        public string RefinerySPD { get; set; }
        public string RefMgrName { get; set; }
        public string SendMethod { get; set; }
        public string StreetAdd1 { get; set; }
        public string StreetAdd2 { get; set; }
        public string StreetAdd3 { get; set; }
        public string StreetCity { get; set; }
        public string StreetCountry { get; set; }
        public string StreetState { get; set; }
        public string StreetZip { get; set; }
        public string TechEmail { get; set; }
        public string TechMgrName { get; set; }
        public string Title { get; set; }
    
        public void Clear()
        {
            FullName = string.Empty;
            AltFullName = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            Email = string.Empty;
            Phone = string.Empty;
            AltFirstName = string.Empty;
            AltLastName = string.Empty;
            AltEmail = string.Empty;
            AltPhone = string.Empty;
            Fax = string.Empty;
            MailAdd1 = string.Empty;
            MailAdd2 = string.Empty;
            MailAdd3 = string.Empty;
            MailCity = string.Empty;
            MailState = string.Empty;
            MailCountry = string.Empty;
            MailZip = string.Empty;
            StreetAdd1 = string.Empty;
            StreetAdd2 = string.Empty;
            StreetAdd3 = string.Empty;
            StreetCity = string.Empty;
            StreetState = string.Empty;
            StreetCountry = string.Empty;
            Title = string.Empty;
            Password = string.Empty;
            SendMethod = string.Empty;
            PricingName = string.Empty;
            PricingEmail = string.Empty;
            DCEmail = string.Empty;
            DCName = string.Empty;
            MaintEmail = string.Empty;
            MaintMgrName = string.Empty;
            TechEmail = string.Empty;
            TechMgrName = string.Empty;
            OpsEmail = string.Empty;
            OpsMgrName = string.Empty;
            RefAddEmail = string.Empty;
            RefAddName = string.Empty;
            RefinerySPD = string.Empty;
            RefineryCountry = string.Empty;

        }
        
    }
}
