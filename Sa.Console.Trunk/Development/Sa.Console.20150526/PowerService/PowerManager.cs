﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sa.Internal.Console.DataObject;
using System.Data;
using System.IO;
using Sa.Console.Common;



namespace PowerService
{
    public class PowerManager
    {
        private sa.Internal.Console.DataObject.DataObject _dAL;

        private int d = 14;
        private char _delim = Convert.ToChar(14);

        public PowerManager(sa.Internal.Console.DataObject.DataObject dAL) 
        {
            _dAL = dAL;            
        }

        public IList<int>  GetStudies() 
        {            
            DataTable dt = Select("Select distinct StudyYear FROM [dbo].[TSort] where studyyear >= 2016 order by studyyear desc;");
            return GetDataFromColumn<int>("StudyYear", dt);
         }

        public IList<string> GetSiteIds(int year)
        {
            List<string> args = new List<string>();
            args.Add("@Year/" + year);
            DataTable dt = Exec("Console.GetSiteIds", args);
            return GetDataFromColumn<string>("SiteID", dt);
        }

        public IList<string> GetCompanynamesWithSiteIds(int year)
        {
            List<string> args = new List<string>();
            args.Add("@Year/" + year);
            DataTable dt = Exec("Console.GetCompanynamesWithSiteIds", args);
            return GetDataFromColumn<string>("Site", dt);
        }

        public IList<string> GetRefNums(string siteId, int year)
        {
            List<string> args = new List<string>();
            args.Add("@SiteId/" + siteId);
            args.Add("@Year/" + year);
            DataTable dt = Exec("Console.GetRefNumsBySiteId", args);
            return GetDataFromColumn<string>("Refnum", dt);
        }
        
        public IList<string> GetUnitnamesWithRefNums(string siteId, int year)
        {
            List<string> args = new List<string>();
            args.Add("@SiteId/" + siteId);
            args.Add("@Year/" + year);
            DataTable dt = Exec("Console.GetUnitnamesWithRefNums", args);
            return GetDataFromColumn<string>("UnitnameRefnum", dt);
        }

        public string GetConsultant(string siteId, int year)
        {
            List<string> args = new List<string>();
            args.Add("@SiteId/" + siteId);
            DataTable dt = Exec("Console.GetConsultantBySiteId", args);
            var results = GetDataFromColumn<string>("ConsultantName", dt);
            return results == null ? "" : results[0];
        }

        public string GetConsultantInitials(string siteId)
        {
            List<string> args = new List<string>();
            args.Add("@SiteId/" + siteId);
            DataTable dt = Exec("Console.GetConsultantInitials", args);
            var results = GetDataFromColumn<string>("Consultant", dt);
            return results == null ? "" : results[0];
        }
        
        public string GetConsultantName(string consultantInitials)
        {
            List<string> args = new List<string>();
            args.Add("@ConsultantInitials/" + consultantInitials);
            DataTable dt = Exec("Console.GetConsultantName", args);
            return GetDataFromColumn<string>("ConsultantName", dt).FirstOrDefault();
        }

        public string GetConsultantEmail(string siteId)
        {
            List<string> args = new List<string>();
            args.Add("@SiteId/" + siteId);
            DataTable dt = Exec("Console.GetConsultantEmail", args);
            return GetDataFromColumn<string>("Email", dt).FirstOrDefault();
        }

        public IList<string> GetAllConsultants(int year)
        {
            //GetAllConsultantsByYear
            List<string> args = new List<string>();
            args.Add("@Year/" + year);
            DataTable dt = Exec("Console.GetAllConsultantsByYear", args);
            IList<string> results = GetDataFromColumn<string>("Consultant", dt);
            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].Length < 1)
                    results.RemoveAt(i);
            }
            return results;
        }

        public void ChangeConsultant(string siteId, int year, string consultant)
        {
            try
            {
                List<string> args = new List<string>();
                args.Add("@SiteId/" + siteId);
                args.Add("@Year/" + year);
                args.Add("@Consultant/" + consultant);
                DataTable dt = Exec("Console.ChangeConsultant", args);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Sa.Console.Common.PowerClientInfo GetClientInfo(string siteId)
        {
            Sa.Console.Common.PowerClientInfo result = null;
            string sql = "SELECT SiteID, CorpName, AffName, PlantName, City, State, CoordName, CoordTitle, CoordAddr1, CoordAddr2, CoordCity, CoordState, CoordZip, CoordPhone, CoordFax, CoordEMail, RptUOM, RptHV, RptSteamMethod ";
            sql += " FROM [dbo].[ClientInfo]  where  SiteID = '" + siteId + "'";
            DataTable dt = Select(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                result = new Sa.Console.Common.PowerClientInfo();
                DataRow dr = dt.Rows[0];
                result.SiteID = dr["SiteID"] == DBNull.Value ? string.Empty : dr["SiteID"].ToString().Trim();
                result.CorpName = dr["CorpName"] == DBNull.Value ? string.Empty : dr["CorpName"].ToString().Trim();
                result.AffName = dr["AffName"] == DBNull.Value ? string.Empty : dr["AffName"].ToString().Trim();
                result.PlantName = dr["PlantName"] == DBNull.Value ? string.Empty : dr["PlantName"].ToString().Trim();
                result.City = dr["City"] == DBNull.Value ? string.Empty : dr["City"].ToString().Trim();
                result.State = dr["State"] == DBNull.Value ? string.Empty : dr["State"].ToString().Trim();

                //these moved to CoContactInfo table
                result.CoordName = dr["CoordName"] == DBNull.Value ? string.Empty : dr["CoordName"].ToString().Trim();
                result.CoordTitle = dr["CoordTitle"] == DBNull.Value ? string.Empty : dr["CoordTitle"].ToString().Trim();
                result.CoordAddr1 = dr["CoordAddr1"] == DBNull.Value ? string.Empty : dr["CoordAddr1"].ToString().Trim();
                result.CoordAddr2 = dr["CoordAddr2"] == DBNull.Value ? string.Empty : dr["CoordAddr2"].ToString().Trim();
                result.CoordCity = dr["CoordCity"] == DBNull.Value ? string.Empty : dr["CoordCity"].ToString().Trim();
                result.CoordState = dr["CoordState"] == DBNull.Value ? string.Empty : dr["CoordState"].ToString().Trim();
                result.CoordZip = dr["CoordZip"] == DBNull.Value ? string.Empty : dr["CoordZip"].ToString().Trim();
                result.CoordPhone = dr["CoordPhone"] == DBNull.Value ? string.Empty : dr["CoordPhone"].ToString().Trim();
                result.CoordFax = dr["CoordFax"] == DBNull.Value ? string.Empty : dr["CoordFax"].ToString().Trim();
                result.CoordEMail = dr["CoordEMail"] == DBNull.Value ? string.Empty : dr["CoordEMail"].ToString().Trim();

                result.RptUOM = dr["RptUOM"] == DBNull.Value ? string.Empty : dr["RptUOM"].ToString().Trim();
                result.RptHV = dr["RptHV"] == DBNull.Value ? string.Empty : dr["RptHV"].ToString().Trim();
                result.RptSteamMethod = dr["RptSteamMethod"] == DBNull.Value ? string.Empty : dr["RptSteamMethod"].ToString().Trim();
            }
            return result;
        }

        public bool MakeDefaultPowerSettingsFile(string filePath)
        {
            bool result = false;
            try
            {
                using(StreamWriter writer = new StreamWriter(filePath,false))
                {
                    writer.WriteLine("2016" + _delim + "");
                    result= true;
                }
            }
            catch
            {}
            return result;
        }

        public Dictionary<string, string> GetPowerSettings(string filePath)
        {
            Dictionary<string, string> result = null;
            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        if (result == null)
                            result = new Dictionary<string, string>();
                        string line = reader.ReadLine();
                        string[] lineParts = line.Split(_delim);
                        result.Add(lineParts[0], lineParts[1]);
                    }
                }
            }
            catch { }
            return result;
        }

        public bool WriteLastStudy(string filePath, string year)
        {
            bool result = false;
            try
            {
                using (StreamWriter writer = new StreamWriter(filePath, false))
                {
                    writer.WriteLine(year);
                    result = true;
                }
            }
            catch
            { }
            return result;
        }

        public string GetLastStudy(string filePath)
        {
            string result = string.Empty;
            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    result = reader.ReadLine();
                }
            }
            catch
            {
                result =string.Empty;
            }
            return result;
        }

        public string GetCompanyPassword(string siteId, int studyYear)
        {
            List<string> args = new List<string>();
            args.Add("@SiteID/" + siteId);
            args.Add("@StudyYear/" + studyYear.ToString());
            DataTable dt = Exec("Console.GetCompanyPassword", args);
            var results = GetDataFromColumn<string>("CompanyPassword", dt);
            return results == null ? "" : results[0];
        }
		
		public string GetReturnFilePassword(string siteId)
		{
			return "mousepad"; //this is the same across all customers and all years !!!!
		}

        public string GetNotes(string refNum)
        {
            List<string> args = new List<string>();
            args.Add("@RefNum/" + refNum);
            DataTable dt = Exec("[Console].[GetValidationNotes]", args); // Select("select ValidationNotes from Val.Notes where RTRIM(Refnum) = '" + refNum.Trim() + "';");
            var results = GetDataFromColumn<string>("ValidationNotes", dt);
            return results == null ? null : results[0];
        }
        public bool UpdateNotes(string refNum, string notes)
        {
            try
            {
                DataTable result = null;
                List<string> args = new List<string>();
                args.Add("@RefNum/" + refNum);
                args.Add("@Notes/" + notes);
                var txt = GetNotes(refNum);
                if (txt == null )
                {
                    result = Exec("Console.InsertValidationNotes", args);
                }
                else
                {
                    result = Exec("Console.UpdateValidationNotes", args);
                }

                if (result == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /*
        public bool UpdateNotes(string refNum, string notes)
        {
            try
            {
                DataTable result = null;
                List<string> args = new List<string>();
                args.Add("@RefNum/" + refNum);
                args.Add("@Notes/" + notes);
                if (GetNotes(refNum) == null )
                {
                    result = Exec("Console.InsertValidationNotes", args);
                }
                else
                {                    
                    result = Exec("Console.UpdateValidationNotes", args);
                }

                if (result == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
         * */

        public string GetConsultingOpportunities(string refNum)
        {
            List<string> args = new List<string>();
            args.Add("@RefNum/" + refNum);
            DataTable dt = Exec("Console.GetConsultingOpportunities", args); // Select("select ValidationNotes from Val.Notes where RTRIM(Refnum) = '" + refNum.Trim() + "';");
            var results = GetDataFromColumn<string>("ConsultingOpportunities", dt);
            return results == null ? null : results[0];
            //return results == null ? "" : ( results.Count==0? string.Empty :  results[0]);
        }
        public bool UpdateConsultingOpportunities(string refNum, string consultingOpportunities)
        {
            try
            {
                DataTable result = null;
                List<string> args = new List<string>();
                args.Add("@RefNum/" + refNum);
                args.Add("@ConsultingOpportunities/" + consultingOpportunities);
                var txt = GetConsultingOpportunities(refNum);
                if (txt == null )
                {
                    result = Exec("Console.InsertConsultingOpportunities", args);
                }
                else
                {
                    result = Exec("Console.UpdateConsultingOpportunities", args);
                }

                if (result == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataTable GetContinuingIssues(string refNum)
        {           
            //DataTable dt = Select("select ContinuingIssues from Val.Notes where RTRIM(Refnum) = '" + refNum.Trim() + "';");
            DataTable dt = Select("SELECT IssueID, EntryDate,consultant,note,deleted,deleteddate,deletedby From [Console].[ContinuingIssues] Where RTRIM(RefNum) = '" + refNum.Trim() + "'  and deleted = 0 order by EntryDate;");
            return dt;
            //return results == null ? "" : (results.Count == 0 ? string.Empty : results[0]);
            //var results = GetDataFromColumn<string>("ValidationNotes", dt);
            //return results; // == null ? "" : results[0];
        }

        public DataSet GetContinuingIssue(string refNum, int issueId)
        {
            List<string> parameters = new List<string>();
            parameters.Add("@RefNum/" + refNum);
            parameters.Add("@IssueId/" + issueId);
            DataSet ds = _dAL.ExecuteStoredProc("Console.GetContinuingIssue", parameters);
            return ds;
        }
        
        public DataSet GetContinuingIssueByConsultant(string refNum, string  consultant)
        {
            List<string> parameters = new List<string>();
            parameters.Add("@RefNum/" + refNum);
            parameters.Add("@Consultant/" + consultant);
            DataSet ds = _dAL.ExecuteStoredProc("Console.GetContinuingIssueByConsultant", parameters);
            return ds;
        }
                        
        public DataTable GetAllChecklistItems(string siteId)
        {
            List<string> parameters = new List<string>();
            parameters.Add("@SiteID/" + siteId);
            DataTable dt = Exec("Console.GetChecklist", parameters);
            return dt;
        }
        
        public DataTable GetChecklistItems(string siteId, string filter)
        {
            string sql = "select SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime from Val.Checklist where RTRIM(SiteId) = '" + siteId.Trim() + "'";
            switch (filter.ToUpper())
            {
                case "Y":
                    sql += " AND Status = 'Y';";
                    break;
                case "N":
                    sql += " AND Status = 'N';";
                    break;
                default:
                    sql += ";";
                    break;
            }
            DataTable dt = Select( sql);
            return dt;
            //var results = GetDataFromColumn<string>("ValidationNotes", dt);
            //return results; // == null ? "" : results[0];
        }

        public DataRow GetChecklistItem(string siteId, string issueId)
        {
            string sql = "select SiteId,IssueID,IssueTitle,IssueText,PostedBy,PostedTime,Completed,SetBy,SetTime from Val.Checklist where RTRIM(SiteId) = '" + siteId.Trim() + "' AND IssueID = '" + issueId + "';";
            DataTable dt = Select(sql);
            var results = GetDataFromColumn<string>("ConsultingOpportunities", dt);
            return dt == null ? null : (dt.Rows.Count == 0 ? null : dt.Rows[0]);
        }

        public Sa.Console.Common.Contact GetCoord(string siteID)
        {
            Sa.Console.Common.Contact person = new Sa.Console.Common.Contact();
            StringBuilder builder = new StringBuilder();
            List<string> parameters = new List<string>();
            parameters.Add("@SiteID/" + siteID);
            DataTable dt = Exec("Console.GetCoord", parameters);
            //populate person
            if (dt != null && dt.Rows.Count == 1)
            {
                //builder.Append("[SANumber],[ContactType],[FirstName],[LastName],[JobTitle],[PersonalTitle],[Phone],");
                person.ContactType = "COORD";
                person.FullName = dt.Rows[0]["CoordName"].ToString();
                person.Title = dt.Rows[0]["CoordTitle"].ToString();
                person.Phone = dt.Rows[0]["CoordPhone"].ToString();
                //person.AltPhone= dt.Rows[0]["PhoneSecondary"].ToString();
                person.Fax = dt.Rows[0]["CoordFax"].ToString();
                person.Email = dt.Rows[0]["CoordEMail"].ToString();


                //person.StreetAdd1 = dt.Rows[0]["CoordAddr1"].ToString();
                //person.StreetAdd2 = dt.Rows[0]["CoordAddr2"].ToString();
                ////person.StreetAdd3 = dt.Rows[0]["StrAdd3"].ToString();
                //person.StreetCity = dt.Rows[0]["CoordCity"].ToString();
                //person.StreetState = dt.Rows[0]["CoordState"].ToString();
                //person.StreetZip = dt.Rows[0]["CoordZip"].ToString();
                //person.StreetCountry = dt.Rows[0]["StrCountry"].ToString();
                person.MailAdd1 = dt.Rows[0]["CoordAddr1"].ToString();
                person.MailAdd2 = dt.Rows[0]["CoordAddr2"].ToString();
                //person.MailAdd3 = dt.Rows[0]["MailAddr3"].ToString();
                person.MailCity = dt.Rows[0]["CoordCity"].ToString();
                person.MailState = dt.Rows[0]["CoordState"].ToString();
                person.MailZip = dt.Rows[0]["CoordZip"].ToString();
                //person.MailCountry = dt.Rows[0]["MailCountry"].ToString();
                //person.AltFirstName = dt.Rows[0]["AltFirstName"].ToString();
                //person.AltLastName = dt.Rows[0]["AltLastName"].ToString();
                //person.AltEmail = dt.Rows[0]["AltEmail"].ToString();
                //person.CCAlt = dt.Rows[0]["CCAlt"].ToString();
                //person.SendMethod = dt.Rows[0]["SendMethod"].ToString();
                //person.Comment = dt.Rows[0]["Comment"].ToString();
            }
            return person;
        }

		
        public Sa.Console.Common.Contact GetCompanyContactInfo(string siteId, int year, string contactType)
        {
            Sa.Console.Common.Contact person = new Sa.Console.Common.Contact();
            StringBuilder builder = new StringBuilder();
            List<string> parameters = new List<string>();
            parameters.Add("@SiteId/" + siteId);
            parameters.Add("@StudyYear/" + year);
            parameters.Add("@ContactType/" + contactType);
            DataTable dt = Exec("[Console].[GetCompanyContactInfo]", parameters);
            //populate person
            if (dt != null && dt.Rows.Count==1)
            {
                //builder.Append("[SANumber],[ContactType],[FirstName],[LastName],[JobTitle],[PersonalTitle],[Phone],");
                person.ContactType=dt.Rows[0]["ContactType"].ToString().Trim();
                person.FirstName = dt.Rows[0]["FirstName"].ToString();
                person.LastName = dt.Rows[0]["LastName"].ToString();
                person.Title = dt.Rows[0]["JobTitle"].ToString();
                //person.PersonalTitle = dt.Rows[0]["PersonalTitle "].ToString();
                person.Phone = dt.Rows[0]["Phone"].ToString();
                person.AltPhone= dt.Rows[0]["PhoneSecondary"].ToString();
                person.Fax = dt.Rows[0]["Fax"].ToString();
                person.Email = dt.Rows[0]["Email"].ToString();
                person.StreetAdd1= dt.Rows[0]["StrAddr1"].ToString();
                person.StreetAdd2 = dt.Rows[0]["StrAddr2"].ToString();
                person.StreetAdd3 = dt.Rows[0]["StrAdd3"].ToString();
                person.StreetCity = dt.Rows[0]["StrCity"].ToString();
                person.StreetState = dt.Rows[0]["StrState"].ToString();
                person.StreetZip = dt.Rows[0]["StrZip"].ToString();
                person.StreetCountry = dt.Rows[0]["StrCountry"].ToString();
                person.MailAdd1 = dt.Rows[0]["MailAddr1"].ToString();
                person.MailAdd2 = dt.Rows[0]["MailAddr2"].ToString();
                person.MailAdd3 = dt.Rows[0]["MailAddr3"].ToString();
                person.MailCity = dt.Rows[0]["MailCity"].ToString();
                person.MailState = dt.Rows[0]["MailState"].ToString();
                person.MailZip = dt.Rows[0]["MailZip"].ToString();
                person.MailCountry = dt.Rows[0]["MailCountry"].ToString();
                person.AltFirstName = dt.Rows[0]["AltFirstName"].ToString();
                person.AltLastName = dt.Rows[0]["AltLastName"].ToString();
                person.AltEmail = dt.Rows[0]["AltEmail"].ToString();
                //person.CCAlt = dt.Rows[0]["CCAlt"].ToString();
                person.SendMethod = dt.Rows[0]["SendMethod"].ToString();
                //person.Comment = dt.Rows[0]["Comment"].ToString();
            }            
            return person;
        }

        public bool ChangeCompanyCoord(string siteID, string plantName, string name, string title, string Addr1, 
            string Addr2, string city, string state, string zip, string phone, string fax, string email)
        {
            StringBuilder builder = new StringBuilder();
            List<string> parameters = new List<string>();
            parameters.Add("@SiteID/" + siteID);
            parameters.Add("@PlantName/" + plantName);
            parameters.Add("@CoordName/" + name );            
            parameters.Add("@CoordTitle/" + title);
            parameters.Add("@CoordAddr1/" + Addr1);
            parameters.Add("@CoordAddr2/" + Addr2);
            parameters.Add("@CoordCity/" + city);
            parameters.Add("@CoordState/" + state );
            parameters.Add("@CoordZip/" + zip);
            parameters.Add("@CoordPhone/" + phone);
            parameters.Add("@CoordFax/" + fax);
            parameters.Add("@CoordEMail/" + email);

            try
            {
                DataTable dt = Exec("Console.ChangeCoord", parameters);
                return true;
            }
            catch
            {
                return false;
            }
        }
		

        public string GetIdrXlsPath(int studyYear, string siteId)
        {
            string folder = @"K:\STUDY\Power\" + studyYear.ToString() + @"\Plant\" + siteId.Trim() + @"\";
            string xlsFile =  siteId.Trim() + ".xls";
            //string pathTest = @"K:\STUDY\Power\2016\Plant\YYYCC16";
            //bool test = Directory.Exists(pathTest);
            try
            {
                DirectoryInfo di = new DirectoryInfo(folder);
                FileInfo[] fi = di.GetFiles(xlsFile);
                if (fi.Count() == 1)
                {
                    return fi[0].FullName;
                }
                else
                {
                    xlsFile += "x";
                    fi = di.GetFiles(xlsFile);
                    if (fi.Count() == 1)
                    {
                        return fi[0].FullName;
                    }
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }



        public string GetVFTemplates(string templatesPath, ref List<string> fileNames)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(templatesPath);
                FileInfo[] fi = di.GetFiles("*.doc*");
                foreach (FileInfo f in fi)
                {
                    if(f.Name.Contains("Cover"))
                        fileNames.Add(f.Name);
                }
                if (fileNames.Count() < 1)
                    return "No VF Templates found for this study";
                return string.Empty;
            }
            catch (Exception ex)
            {
                return "Error in GetPathToVFTemplate(): " + ex.Message;
            }
        }

        public string PrepVF(string templatePath, string vfTextFilePath,
            DateTime deadline, string consultantInitials, string consultantName,
            string coordName, string coordEmail, 
            ref Sa.Console.Common.WordTemplate templateValues,
            string siteId, string correspPath, ref string coloc)
        {
            try
            {
                templateValues = new Sa.Console.Common.WordTemplate();
                using (StreamWriter writer = new StreamWriter(vfTextFilePath, true))
                {
                    writer.WriteLine(DateTime.Now.ToString("dd-MMM-yy"));
                    //templateValues.Add("_TodaysDate", DateTime.Now.ToString("dd-MMM-yy"));
                    templateValues.Field.Add("_TodaysDate");
                    templateValues.RField.Add(DateTime.Now.ToString("dd-MMM-yy"));

                    writer.WriteLine(deadline.ToString("dd-MMM-yy"));
                    //templateValues.Add("_DueDate", deadline.ToString("dd-MMM-yy"));
                    templateValues.Field.Add("_DueDate");
                    templateValues.RField.Add(deadline.ToString("dd-MMM-yy"));
                    try
                    {
                        DataTable dt = Select("SELECT CompanyName, UnitName, UnitLabel from dbo.TSort where SiteID = '" + siteId + "';");
                        string company = GetDataFromColumn<string>("CompanyName", dt)[0];
                        writer.WriteLine(company);
                        //templateValues.Add("_Company", company);
                        templateValues.Field.Add("_Company");
                        templateValues.RField.Add(company);

                        string location = GetDataFromColumn<string>("UnitName", dt)[0] + " " + GetDataFromColumn<string>("UnitLabel", dt)[0];
                        writer.WriteLine(location);
                        //templateValues.Add("_Refinery", location);
                        templateValues.Field.Add("_Refinery");
                        templateValues.RField.Add(location);

                        coloc = company + "- " + location;
                    }
                    catch { }

                    writer.WriteLine(coordName);
                    //templateValues.Add("_Contact", coordName);
                    templateValues.Field.Add("_Contact");
                    templateValues.RField.Add(coordName);
                    writer.WriteLine(coordEmail);
                    //templateValues.Add("_EMail", coordEmail);
                    templateValues.Field.Add("_EMail");
                    templateValues.RField.Add(coordEmail);
                    writer.WriteLine(consultantName);
                    // templateValues.Add("_ConsultantName", consultantName);
                    templateValues.Field.Add("_ConsultantName");
                    templateValues.RField.Add(consultantName);

                    templateValues.Field.Add("_Initials");
                    templateValues.RField.Add(consultantInitials);
                    //consultantInitials

                    string userInitials = System.Environment.UserName.ToUpper();
                    userInitials = userInitials.Replace(".", "");
                    userInitials = userInitials.Replace("DC1", "");

                    writer.WriteLine(correspPath + "\\" + siteId + ".docx");

                    //I don't think we need these yet
                    /*  
                      objWriter.WriteLine(strFaxName)
                    TemplateValues.Field.Add("_CoCoContact")
                    TemplateValues.RField.Add(strFaxName)

                    ' Company Contact Email Address
                    objWriter.WriteLine(strFaxEmail)
                    TemplateValues.Field.Add("_CoCoEMail")
                    TemplateValues.RField.Add(strFaxEmail)
                    */

                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return "Error in PrepVF: " + ex.Message;
            }
        }
        public string PopulateVFFilesMFiles(string nameFilter, string selectedItemString,
            List<Sa.Console.Common.ConsoleFile> mfiles,
            ref List<Sa.Console.Common.ConsoleFile> vfFiles, bool containsNotStartsWith = false)
        {
            try
            {
                foreach (Sa.Console.Common.ConsoleFile fil in mfiles)
                {
                    string filter = nameFilter.Replace("*", "") + selectedItemString;
                    bool match = false;
                    if (!containsNotStartsWith)
                    {
                        if(fil.FileName.StartsWith(filter))
                        {
                            match = true;
                        }
                    }
                    else
                    {
                        if(fil.FileName.Contains(filter))
                        {
                            match=true;
                        }
                    }
                    if (match)
                    {
                        string formatDocumentDate = string.Empty;
                        if (fil.CheckedOutTo > 0)
                        {
                            formatDocumentDate = "(Checked out to " + fil.CheckedOutToUserName + "):  ";
                        }
                        formatDocumentDate = formatDocumentDate + fil.LastModifiedUtcDate.ToString("dd-MMM-yy");
                        Sa.Console.Common.ConsoleFile cfile = new Sa.Console.Common.ConsoleFile(true);

                        cfile.CheckedOutTo = fil.CheckedOutTo;
                        cfile.CheckedOutToUserName = fil.CheckedOutToUserName;
                        cfile.ClassType = fil.ClassType;
                        cfile.CreatedUtcDate = fil.CreatedUtcDate;
                        cfile.DeliverableType = fil.DeliverableType;
                        cfile.DocumentDate = fil.DocumentDate;
                        cfile.FileExtension = fil.FileExtension;
                        cfile.FileName = formatDocumentDate + " | " + fil.FileName + "." + fil.FileExtension;
                        cfile.FilePath = fil.FilePath;
                        cfile.Id = fil.Id;
                        cfile.LastModifiedUtcDate = fil.LastModifiedUtcDate;
                        vfFiles.Add(cfile);
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return "Error in PopulateVFFilesMFiles(): " +  ex.Message;
            }
        }

        public string GetSiteIdFromSiteCbo(string cboSelectedItem)
        {
            string result = string.Empty;
            if (cboSelectedItem != null && cboSelectedItem.Length > 0)
            {
                string[] parts = cboSelectedItem.Split("-".ToCharArray());
                result = parts[parts.Length - 1];
            }
            return result;
        }

        public string GetCompanyBenchmarkName(string siteId, string twoDigitStudyYear, string mfilesDesktopUIPath)
        {
            //Example: mfilesDesktopUIPath  = 'V:\Solomon\';
            string result = string.Empty;
            string facilityId = siteId.Substring(0, siteId.Length - 2);
            /*
            //string sql ="SELECT '" +  mfilesDesktopUIPath + @"Benchmarking Particpants\' + CAST(t.StudyYear as varchar(4)) +'\' + RTRIM(p.Study) + '\' + RTRIM(c.Refnum) + ' - '+ c.CoLoc";
            string sql = "SELECT RTRIM(c.Refnum) + ' - '+ c.CoLoc";
            sql +=" FROM PowerWork.dbo.TSort t ";
            sql += " LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID = '" + facilityId + "' ";
            sql += " LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.facilityid =  '" + facilityId + "'  ";
            sql +=" LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID) ";
            sql += " ON c.Study = 'Power' AND c.StudyYear = t.StudyYear AND fc.FacilityID =  '" + facilityId + "'  ";
            sql +=" WHERE t.Siteid = '" + siteId + "'; ";

            try
            {
                _dAL.SQL = sql;
                DataSet ds = _dAL.Execute();
                if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
                {
                    result = ds.Tables[0].Rows[0][0].ToString();
                }
            }
            catch
            {}
            _dAL.SQL = string.Empty;
            */
            List<string> parms = new List<string>();
            parms.Add("@FacilityID/" + facilityId);
            parms.Add("@SiteId/" + siteId );
            DataSet ds = _dAL.ExecuteStoredProc("Console.GetMFilesCompanyBenchmarkName", parms);
            //Console.GetMFilesCompanyBenchmarkName
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                result = ds.Tables[0].Rows[0][0].ToString();
            }

            return result;
        }

        public string BuildCorrPath(string studyYear, string siteId)
        {
            //K:\STUDY\Power\2016\Plant\120CC16
            return @"K:\STUDY\Power\" + studyYear + @"\Plant\" + siteId + @"\"; // + studyYear.SubString(2,2);
        }

        public string BuildCompanyCorrPath(string studyYear, string siteId)
        {
            string sql = "SELECT CompanyID from dbo.StudySites where SiteID = '" + siteId + "' and StudyYear= " + studyYear ;
            DataTable dt = Select(sql);
            List<string> results = GetDataFromColumn<string>("CompanyID", dt);
            string returnValue = string.Empty;
            if(results != null && results.Count==1)
                returnValue= @"K:\STUDY\Power\" + studyYear + @"\Company\" + results[0] + @"\"; 
            return returnValue;
        }

        public string GetDataFor_lstVFNumbersMFiles(string filter, ref List<string> numsFoundAlready,
            bool containsNotStartsWith, Sa.Console.Common.ConsoleFilesInfo siteFiles)
        {
            try
            {
                if (numsFoundAlready == null)
                    numsFoundAlready = new List<string>();
                //        'get files in correspondence for this refnum
                filter = filter.Replace("*", "");
                foreach (Sa.Console.Common.ConsoleFile fil in siteFiles.Files)
                {
                    if (!containsNotStartsWith)
                    {
                        if (fil.FileName.StartsWith(filter)) // || fil.FileName.StartsWith(filter + "FL"))
                        {
                            bool found = false;
                            string thisNumber = fil.FileName.Substring(2, 1);
                            try
                            {
                                int thisInt = Int32.Parse(thisNumber);
                                foreach (string num in numsFoundAlready)
                                {
                                    if (num == thisInt.ToString())
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                    numsFoundAlready.Add(thisNumber);
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        if (fil.FileName.Contains(filter))
                        {
                            bool found = false;
                            string fileName = fil.FileName;
                            int index = fileName.IndexOf(filter);
                            if (!fileName.EndsWith(filter)) //' doesn't end with a number so don't count it
                            {
                                string thisNumber = fileName.Substring(index + filter.Length, 1);
                                try
                                {
                                    int thisNum = Int32.Parse(thisNumber);
                                    foreach (string num in numsFoundAlready)
                                    {
                                        if (num == thisNumber.ToString())
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                        numsFoundAlready.Add(thisNumber);
                                }
                                catch { }
                            }
                        }
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return "Error in GetDataFor_lstVFNumbersMFiles(): " + ex.Message; 
            }            
        }
        /*
        public string NextVxLegacy(string vType, string fileExtension, string corrPath)
        {
            //for vType, expect "VF" or "VR" etc.
            DirectoryInfo di = new DirectoryInfo(corrPath);
            FileInfo[] files = di.GetFiles("*." + fileExtension);
            for (int i = 1; i < 21; i++)
            {
                bool found = false;
                for (int arrayPosition = 0; arrayPosition < files.Length; arrayPosition++)
                {
                    if (files[arrayPosition].Name.ToUpper().StartsWith(vType.ToUpper() + i.ToString()))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return i.ToString();
            }
            return string.Empty;
        }
        */

        public string GetCompanyName(string siteId)
        {
            DataTable dt = Select("SELECT RTRIM(CompanyName) as CompanyName from dbo.TSort where SiteID = '" + siteId + "' ");
            return GetDataFromColumn<string>("CompanyName", dt)[0];
        }


        public string GetLocation(string siteId)
        {
            DataTable dt = Select("SELECT RTRIM(SiteName) as SiteName from [PowerWork].[dbo].[StudySites] where SiteID = '" + siteId + "' ");
            return GetDataFromColumn<string>("SiteName", dt)[0];
        }

        public bool GetConsultantInfo(string initials, ref string fullName, ref string email, ref string phone)
        {
            try
            {
                DataTable dt = Select("SELECT RTRIM(FullName) as FullName, RTRIM(EmailAddress) as EmailAddress, RTRIM(PhoneNumber) as PhoneNumber from Console.ConsultantInfo where UPPER(Initials) = '" + initials.Trim().ToUpper() + "' ");
                fullName = GetDataFromColumn<string>("FullName", dt)[0];
                email = GetDataFromColumn<string>("EmailAddress", dt)[0];
                phone = GetDataFromColumn<string>("PhoneNumber", dt)[0];
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int DragDropForFilesCheck(System.Collections.Specialized.StringCollection fileNames,
            bool fileGroupDescriptor,
            bool useMfiles, //ref List<string> filePaths,
            ref string error)
        {
            //'0 is error
            //'1 is yes, drag drop for files,
            //'2 is no, drag drop for email,
            try
            {
                bool isEmail = false;
                string draggedFileName = string.Empty;
                if (fileNames.Count == 1)
                {
                    /*
                    if (!useMfiles)
                    {
                        error = "Drag-Drop for files requires a Site that is using M-Files";
                        return 0;
                    }
                    */
                    if (Directory.Exists(fileNames[0]))
                    {
                        error = fileNames[0] + " is a folder, cannot save in M-Files";
                        return 0;
                    }
                    else
                    {
                        draggedFileName = fileNames[0];
                    }
                }
                else if(fileNames.Count <1)
                {
                    if (fileGroupDescriptor)
                    {
                        isEmail = true;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    /*
                    if(!useMfiles)
                    {
                        error="Drag-Drop for files requires a Site that is using M-Files";
                        return 0;
                    }
                    */
                }

                if(!isEmail)
                {
                    /*
                    filePaths=new List<string>();

                    if(fileNames.Count>1)
                    {
                        foreach(var f in fileNames)
                        {
                            filePaths.Add(f);
                        }
                    }
                    else
                    {
                        filePaths.Add(fileNames[0]);
                    }
                    */
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            catch (Exception ex)
            {
                error = "Error in DragDropForFilesCheck(): " + ex.Message;
                return 0;
            }
        }

        /*
                If Not IsNothing(consoleFilesInfo) AndAlso Not IsNothing(consoleFilesInfo.Files) AndAlso consoleFilesInfo.Files.Count > 0 Then
            Try
                Dim temp As New ConsoleFilesInfo(_useMFiles, Common.VerticalType.POWER)
                'If Not IsNothing(consoleFilesInfo.BenchmarkingParticipantId) Then
                '   temp.BenchmarkingParticipantId = consoleFilesInfo.BenchmarkingParticipantId
                'End If
                'temp.RefNum = populated by BenchmarkingParticipant
                Dim dates As New List(Of DateTime)
                For Each fil As ConsoleFile In consoleFilesInfo.Files
                    dates.Add(fil.DocumentDate)
                Next
                Dim foundIds As String = String.Empty
                dates.Sort()
                For i As Integer = dates.Count - 1 To 0 Step -1
                    Dim toFind As DateTime = dates(i)
                    For j As Integer = 0 To consoleFilesInfo.Files.Count - 1
                        If consoleFilesInfo.Files(j).DocumentDate = toFind And Not foundIds.Contains(consoleFilesInfo.Files(j).Id.ToString()) Then
                            temp.Files.Add(consoleFilesInfo.Files(j))
                            foundIds += consoleFilesInfo.Files(j).Id.ToString() + "|"
                            Exit For
                        End If
                    Next
                Next
                Return temp
            Catch ex As Exception
                _dAL.WriteLog("SortConsoleFilesByDate", ex)
                Return consoleFilesInfo
            End Try
        Else
            Return consoleFilesInfo 'dont sort
        End If
        */

        public ConsoleFilesInfo SortConsoleFilesByDate(ConsoleFilesInfo consoleFilesInfo, bool useMFiles)
        {
            if (consoleFilesInfo != null && consoleFilesInfo.Files != null && consoleFilesInfo.Files.Count > 0)
            {
                try
                {
                    ConsoleFilesInfo temp = new ConsoleFilesInfo(useMFiles, VerticalType.POWER);
                    List<DateTime> dates = new List<DateTime>();
                    foreach (ConsoleFile fil in consoleFilesInfo.Files)
                    {
                        if(fil.DocumentDate!=null)
                            dates.Add(Convert.ToDateTime(fil.DocumentDate));
                    }
                    string foundIds = string.Empty;
                    dates.Sort();

                    for (int i = dates.Count - 1; i > -1; i--)
                    {
                        DateTime toFind = dates[i];
                        for (int j = 0; j < consoleFilesInfo.Files.Count; j++)
                        {
                            if(consoleFilesInfo.Files[j].DocumentDate==toFind && !foundIds.Contains(consoleFilesInfo.Files[j].Id.ToString()))
                            {
                                temp.Files.Add(consoleFilesInfo.Files[j]);
                                foundIds+=consoleFilesInfo.Files[j].Id.ToString() + "|";
                                break;
                            }
                        }
                    }
                    //now adde the docs without dates at the bottom
                    foreach (ConsoleFile fil in consoleFilesInfo.Files)
                    {
                        if (fil.DocumentDate == null)
                            dates.Add(Convert.ToDateTime(fil.DocumentDate));
                    }
                    for (int i = dates.Count - 1; i > -1; i--)
                    {
                        DateTime toFind = dates[i];
                        for (int j = 0; j < consoleFilesInfo.Files.Count; j++)
                        {
                            if (!foundIds.Contains(consoleFilesInfo.Files[j].Id.ToString()))
                            {
                                temp.Files.Add(consoleFilesInfo.Files[j]);
                                foundIds += consoleFilesInfo.Files[j].Id.ToString() + "|";
                                break;
                            }
                        }
                    }
                    return temp;
                }
                catch (Exception ex)
                {
                    _dAL.WriteLog("SortConsoleFilesByDate", ex);
                    return consoleFilesInfo ;
                }
            }
            else
            {
                return consoleFilesInfo; //don't sort
            }

        }

        public System.Windows.Forms.TreeNode PopulateTreeViewNodeWithMfilesFiles(ConsoleFile fil)
        {
            try
            {
                string info = string.Empty;
                bool checkedOut = false;
                if (fil.CheckedOutTo > 0)
                {
                    info = "(" + fil.CheckedOutToUserName + ") ";
                    checkedOut = true;
                }
                info += fil.FileName + "." + fil.FileExtension;
                System.Windows.Forms.TreeNode tn = new System.Windows.Forms.TreeNode(info);
                if (checkedOut)
                    tn.BackColor = System.Drawing.Color.Red;
                tn.Tag = fil.Id.ToString();
                try
                {
                    tn.ImageIndex = 1; //0 'I added the blank File.bmp to the imagelist, and it's ordinal is 1
                }
                catch 
                { }
                return tn;
            }
            catch (Exception ex)
            {
                _dAL.WriteLog("PopulateTreeViewNodeWithMfilesFiles", ex);
                return null;
            }
        }

        public bool LookupMFilesVPaths(string mfilesDesktopUIPath,
            string trimmedSiteId,
            ref string refStudyFolder,
            ref string coStudyFolder)
        {
            try
            {
                string sql = "SELECT ";
                sql = sql + "RefStudyFolder = '" + mfilesDesktopUIPath + "Benchmarking Particpants\' + CAST(t.StudyYear as varchar(4)) +'\' ";
                sql = sql + "+ RTRIM(p.Study) + '\' + RTRIM(p.Refnum) + ' - ' + p.CoLoc, ";
                sql = sql + "CoStudyFolder = '" + mfilesDesktopUIPath + "' + 'Benchmarking Particpants\' + CAST(t.StudyYear as varchar(4)) +'\' ";
                sql = sql + "+ RTRIM(p.Study) + '\' ";
                sql = sql + "+ (SELECT CONCAT(RTRIM(c.Refnum), ' - ', RTRIM(c.CoLoc)) ";
                sql = sql + "	As CompanyBenchmarkName FROM PowerWork.dbo.TSort t  LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID =  LEFT('" + trimmedSiteId + "',LEN('" + trimmedSiteId + "')-2) ";
                sql = sql + "	LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.facilityid = LEFT('" + trimmedSiteId + "',LEN('" + trimmedSiteId + "')-2) ";
                sql = sql + "	LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc  ";
                sql = sql + "	INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID)  ON c.Study = 'Power' AND c.StudyYear = t.StudyYear AND fc.FacilityID =   LEFT('" + trimmedSiteId + "',LEN('" + trimmedSiteId + "')-2) ";
                sql = sql + "	WHERE t.Siteid = '" + trimmedSiteId + "') ";
                sql = sql + "    FROM dbo.TSort t  ";
                sql = sql + "    LEFT JOIN MFilesSupport.dbo.CPAFacilities m ON m.FacilityID = t.SiteID ";
                sql = sql + "    LEFT JOIN MFilesSupport.dbo.CPAParticipants p ON p.Refnum = t.SiteID ";
                sql = sql + "    LEFT JOIN (MFilesSupport.dbo.FacilityCompany fc INNER JOIN MFilesSupport.dbo.CPAParticipants c ON c.FacilityID = fc.CompanyID) ";
                sql = sql + "    ON c.Study = p.Study AND c.StudyYear = t.StudyYear AND fc.FacilityID = t.SiteID ";
                sql = sql + "    WHERE t.SiteID = '" + trimmedSiteId + "'";
                DataTable dt = Select(sql);
                try
                {
                    refStudyFolder = GetDataFromColumn<string>("RefStudyFolder", dt)[0];
                }
                catch { }
                try
                {
                    coStudyFolder = GetDataFromColumn<string>("CoStudyFolder", dt)[1];
                }
                catch { }
                return true;
            }
            catch (Exception ex)
            {
                _dAL.WriteLog("LookupMFilesVPaths", ex);
                return false ;
            }
        }


        //===============================================
        #region "Data methods"
        private List<T> GetDataFromColumn<T>(string columnName, DataTable dt)
        {
            List<T> list=null ;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<T>();                
                foreach (DataRow row in dt.Rows)
                {
                    try
                    {
                        if (typeof(T) == typeof(System.String))
                        {
                            string val = row[columnName].ToString().Trim();
                            list.Add((T)Convert.ChangeType(val, typeof(String))); //Convert.ChangeType(row[columnName], typeof(System.String));
                        }
                        else
                        {
                            list.Add((T)Convert.ChangeType(row[columnName], typeof(T)));
                        }
                    }
                    catch (Exception ex)
                    {
                        string debugHere = ex.Message;
                    }
                }
            }
            return list;
        }

        private DataTable Select(string sql)
        {
            DataSet result = new DataSet();
            try
            {
                _dAL.SQL = sql;
                result = _dAL.Execute();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                _dAL.SQL = string.Empty;
            }
            return result.Tables[0];   
        }

        private DataTable Exec(string procName, List<string> parameters)
        {
            DataSet result = new DataSet();
            try
            {
                result = _dAL.ExecuteStoredProc(procName, parameters);
            }
            catch (Exception ex)
            {
                string breakpoint = ex.Message;
                return null;
            }
            if (result == null)
            {
                return null;
            }
            else
            {
                if (result.Tables.Count > 0)
                {
                    return result.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
#endregion
    }
}
