﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using SA.Console;
using System.Windows.Forms;
using Solomon.Common.DocumentManagement.MFiles.WebAccess;


namespace Console.Tests.Olefins
{
    [TestClass]
    public class DocsMFilesAccessTests
    {
        private string GetDevConfigHelperSetting(string settingName)
        {
            string value = string.Empty;
            try
            {
                value = Console.Tests.DevConfigHelper.ReadConfig(settingName);
            }
            catch (NullReferenceException nullReferenceExceptionId)
            {
                try
                {
                    value = ConfigurationManager.AppSettings[settingName].ToString();
                }
                catch (Exception exId)
                {
                    Assert.Fail("Can't find setting " + settingName);
                }
            }
            return value;
        }
                
        [TestMethod]
        public void TestValFax_Build()
        {
            bool success = false;
            string RefNum = "15PCH235"; //2015PCH235

            //must call mfiles.ValFax_Build directly instead; docsFileSystemAccess.ValFax_Build has an
            //input box that can't be tested
            //SA.Console.DocsFileSystemAccess docsFileSystemAccess = new SA.Console.DocsFileSystemAccess(Main.VerticalType.OLEFINS);
            SA.Console.DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, RefNum, "20" + RefNum);

            try
            {
                //will need this path created:  C:\Users\SBARD\Console
                Assert.IsTrue(Directory.Exists(@"C:\Users\SBARD\Console"));
                //need this but it can just be an empty doc:
                Assert.IsTrue(File.Exists(@"C:\Users\SBARD\Console\New Microsoft Word Document.docx"));

                string Password = GetDevConfigHelperSetting("TestDbPassword");
                string strValFaxTemplate = string.Empty; // "Trans Pricing.docx";
                string UserName = "sbard";
                string TemplatePath = @"C:\Users\Sbard\Console\";
                string btnValFaxText = "TEST";
                string TempDrive = @"C:\Users\";
                string userInitials = "JSJ";


                sa.Internal.Console.DataObject.DataObject db =
                    new sa.Internal.Console.DataObject.DataObject(
                        sa.Internal.Console.DataObject.DataObject.StudyTypes.OLEFINS, UserName, Password);

                string StudyDrive = @"C:\";
                string StudyYear = "2015";
                string StudyRegion = "NA";
                string Study = "OLEFINSDEV";
                string TempPath = @"C:\Temp\";
                string CurrentRefNum = "15PCH235";  //2015PCH235
                string CorrPathParameter = @"C:\Users\SBARD\Console\";
                string deadlineDays = "5";
                //OlefinsMainConsole.WordTemplate TemplateValues = new OlefinsMainConsole.WordTemplate();
                Object TemplateValues = new OlefinsMainConsole.WordTemplate();
                string VFFileName = string.Empty;

                /*
                //must call mfiles.ValFax_Build directly instead; docsFileSystemAccess.ValFax_Build has an
                //input box that can't be tested
                docsFileSystemAccess.ValFax_Build(ref strValFaxTemplate, ref UserName, TemplatePath,ref  btnValFaxText, 
                    TempDrive,ref  mUserName, RefNum, ref db, StudyDrive, StudyYear, StudyRegion, Study,
                    TempPath, ref CurrentRefNum, CorrPathParameter, ref  TemplateValues,ref VFFileName);
                */
                docsMFilesAccess.ValFax_Build(ref strValFaxTemplate, ref UserName, TemplatePath, ref  btnValFaxText,
                    ref  userInitials, RefNum, ref db, StudyDrive, StudyYear, StudyRegion, Study,
                    TempPath, ref CurrentRefNum, CorrPathParameter, deadlineDays, ref  TemplateValues, ref VFFileName);

                //would ordinarily call WordTemplateReplace() here, but it would upload new doc to MFiles, and we 
                // don't have a real test enviro.

                success = true;
            }
            catch (Exception ex)
            {
                string debugError = ex.Message;
                string error = docsMFilesAccess.ErrorLogType;
            }
            Assert.IsTrue(success);
        }

        [TestMethod]
        public void BuildVRFileTest()
        {
            Assert.Inconclusive("SKIPPING"); //we don't want to create new doc every time
            string mUserName = "SFB";
            string strTextForFile = "TEST";
            string CorrPathVR = @"C:\Users\SBARD\Console\";
            string lstVFNumbersText = "1";
            ////string shortrefNum = "15PCH998";
            DocsMFilesAccess docsMFilesAccessM = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            string result = docsMFilesAccessM.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText);
            Assert.IsTrue(result == "VR" + lstVFNumbersText + ".txt has been built.");
            strTextForFile = string.Empty;
            result = docsMFilesAccessM.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText);
            Assert.IsTrue(result == "VR file build was cancelled.");
            File.Delete(CorrPathVR + "VA" + lstVFNumbersText + ".txt");
        }

        [TestMethod]
        public void CheckInAndOutAndStatusTest()
        {
            int docId = 6433;
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            if (docsMFilesAccess.GetCheckedOutStatus(docId) != MFilesCheckOutStatus.CheckedIn)
            {
                throw new Exception("Please find another doc to use for test CheckInAndOut()");
            }
            ObjectVersion ver=  docsMFilesAccess.CheckOutToMe(docId);
            Assert.IsTrue(ver.CheckedOutTo > 0);
            ver = docsMFilesAccess.CheckIn(docId);
            Assert.IsTrue(ver.CheckedOutTo == 0);
        }

        [TestMethod]
        public void DeleteFileTest()
        {
            //we don't want to really do this
            Assert.Inconclusive("SKIPPING");
            return;
        }

        [TestMethod]
        public void DocExistsTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
            Assert.IsTrue(docsMFilesAccess.DocExists("VF4 Solomon - SINGAPORE", "2015PCH998", true));
        }

        [TestMethod]
        public void DownloadFileTest()
        {
            int docId = 6433;
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            Assert.IsTrue(docsMFilesAccess.DownloadFile(@"C:\Temp", docId));
        }

        [TestMethod]
        public void GetDataFor_lstVFNumbersTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
            List<string> results = docsMFilesAccess.GetDataFor_lstVFNumbers("VF1", @"C:\Temp\", null);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void GetDocByNameAndRefnumTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            ConsoleFilesInfo files = docsMFilesAccess.GetDocByNameAndRefnum("VF", "2015PCH998", true);
            Assert.IsTrue(files.Files.Count > 0);
        }

        [TestMethod]
        public void GetDocNamesAndIdsByBenchmarkingParticipantTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            ConsoleFilesInfo files = docsMFilesAccess.GetDocNamesAndIdsByBenchmarkingParticipant("2015PCH998");
            Assert.IsTrue(files.Files.Count > 0);
        }

        [TestMethod]
        public void GetDocNamesByBenchmarkingParticipantTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            List<string> files = docsMFilesAccess.GetDocNamesByBenchmarkingParticipant("2015PCH998");
            Assert.IsTrue(files.Count > 0);
        }

        [TestMethod]
        public void GetDocsByBenchmarkingParticipantTest()
        {
            string[] args = new string[1];
            args[0]="2015PCH998";
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            PrivateObject privateObject = new PrivateObject(docsMFilesAccess);
            List<ObjectVersion> results = (List<ObjectVersion>)privateObject.Invoke("GetDocsByBenchmarkingParticipant", args);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void GetValueOfPropertyValueTest()
        {
            object[] args = new object[2];
            List<PropertyValue> pvs = new List<PropertyValue>();
            PropertyValue pv = new PropertyValue();
            pv.PropertyDef = 100;
            pv.TypedValue=new TypedValue { Value="TEST"};
            pvs.Add(pv);
            args[0]=pvs;
            args[1]=100;
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            PrivateObject privateObject = new PrivateObject(docsMFilesAccess);
            TypedValue result = (TypedValue)privateObject.Invoke("GetValueOfPropertyValue", args);
            Assert.IsTrue(result.Value == "TEST");
        }

        [TestMethod]
        public void GetVF1FileInfoTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            ConsoleFilesInfo files = docsMFilesAccess.GetVF1FileInfo();
            Assert.IsTrue(files.Files.Count > 0);
        }

        [TestMethod]
        public void lstVFFiles_DoubleClick()
        {
            //Can't test, it opens a file from MFiles which would need to be closed again.
            Assert.Inconclusive("SKIPPING");
            return;
        }

        [TestMethod]
        public void lstVRFilesItemsTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            List<object> results= docsMFilesAccess.lstVRFilesItems("", "1");
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void NextReturnFileTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH053", "2013PCH053");
            int result = docsMFilesAccess.NextReturnFile("2013PCH053");
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void NextVTest()
        {
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            object result = docsMFilesAccess.NextV("VF", "2015PCH998");
            Assert.IsTrue((int)result > 0);
        }

        [TestMethod]
        public void OpenFileFromMfilesDocIdTest()
        {
            //Can't test, it opens a file from MFiles which would need to be closed again.
            Assert.Inconclusive("SKIPPING");
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
            docsMFilesAccess.OpenFileFromMfiles(6544);
            return;
        }

        [TestMethod]
        public void OpenFileFromMfilesFileNameTest()
        {
            //Can't test, it opens a file from MFiles which would need to be closed again.
            Assert.Inconclusive("SKIPPING");
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
            docsMFilesAccess.OpenFileFromMfiles("VA6");
            return;
        }

        [TestMethod]
        public void PopulateVFFilesTest()
        {
            string path = System.Environment.CurrentDirectory.ToString();
            //"C:\\tfs\\CPA\\Sa.Console.Trunk\\Development\\Sa.Console.20150526\\Console.Tests\\bin\\Debug"
            

            object[] args = new object[4];
            String a = string.Empty;
            string b = "VF";
            string c = "1";
            object d = new List<object>();

            args[0] = a;
            args[1] = b;
            args[2] = c;
            args[3] = d;
            
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "13PCH998", "2013PCH998");
            PrivateObject privateObject = new PrivateObject(docsMFilesAccess);
            privateObject.Invoke("PopulateVFFiles", args);
            List<object> e = (List<object>)args[3];
            Assert.IsTrue(e.Count>0);
        }

        //[TestMethod] We don't want to do this every time.
        public void UploadNewDocToMFiles()
        {
            //we don't want to really do this every time
            Assert.Inconclusive("SKIPPING");
            return;
        }

        //[TestMethod] We don't want to do this every time.
        public void UploadNewIdr()
        {
            //we don't want to really do this every time
            Assert.Inconclusive("SKIPPING");
            return;
        }

        //[TestMethod] We don't want to do this every time.
        public void UploadNewVersionOfDocToMFiles()
        {
            //we don't want to really do this every time
            Assert.Inconclusive("SKIPPING");
            return;
        }

        [TestMethod]
        public void GetMultipleRefnumsDocsTest()
        {
            Assert.Inconclusive("SKIPPING - original purpose can't be achieved, but could be reused for something else later.");
            DocsMFilesAccess docsMFilesAccess = new SA.Console.DocsMFilesAccess(Main.VerticalType.OLEFINS, "15PCH998", "2015PCH998");
            //char delim = (char)14; // '^'; //DONT USE ANY OF THE CHARS THAT GO INTO THE SEARCH FILTER
            List<int> refnums = new List<int>();
            refnums.Add(5323);
            refnums.Add(8849);
            /*
            args = AddNewBenchmarkingParticipantArg(args, 1144, delim, "=", "5323"); //41. all
            args = AddNewBenchmarkingParticipantArg(args, 0, delim, "^=", "VF");  //15
            args = AddNewBenchmarkingParticipantArg(args, 1144, delim, "=", "8849"); //2015PCH134 - shoud be 1 and it is VF
            //-yes this adds 1
            */
            SA.Console.ConsoleFilesInfo results = docsMFilesAccess.GetConsultantTabInfo(refnums, "VF", null);            

            //then: try adding in filter for docname starts with VF
            //(0, "*=", "VF"); - this only works if its the only filter.
            // ‘ˆ=’ ; Starts With
            Assert.IsTrue(results.Files.Count > 0);
        }

        private List<string> AddNewBenchmarkingParticipantArg(List<string> list, int propertyDefId, char delim, string searchOperator, string propertyValue)
        {
            if (list == null)
            {
                list = new List<string>();
            }
            list.Add(propertyDefId.ToString() + delim + searchOperator + delim + propertyValue);
            return list;
        }




    }
}
