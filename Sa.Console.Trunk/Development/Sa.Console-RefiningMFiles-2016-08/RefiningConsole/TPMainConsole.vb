﻿
Imports System.Data.SqlClient
Imports System.Configuration
Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.Text
Imports System.IO.Compression
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports SA.Internal.Console.DataObject




Public Class TPMainConsole
    Public Class WordTemplate
        Public Field As New List(Of String)
        Public RField As New List(Of String)
    End Class

#Region "PRIVATE VARIABLES"
    Dim SavedFile As String = Nothing
    Dim CompleteIssue As String = "N"
    Dim CurrentTab As Integer = 0
    Dim ValidationIssuesIsDirty As Boolean = False
    Dim Time As Integer = 0
    Dim LastTime As Integer = 0
    Dim CompanyContact As Contacts
    Dim OlefinsContact As Contacts
    Dim TPLContact As Contacts
    Dim InterimContact As Contacts
    Dim PlantContact As Contacts
    Dim PowerContact As Contacts
    Dim OpsContact As Contacts
    Dim TechContact As Contacts
    Dim PricingContact As Contacts
    Dim DataContact As Contacts
    Dim CompanyPassword As String = ""

    Dim TabDirty(9) As Boolean
    Dim ds As DataSet
    'Dim db As New DataAccess()
    Dim db As DataObject


    Dim sDir(10) As String
    Dim StudyRegion As String = ""
    Dim Study As String = ""

    Dim SANumber As String

    Dim CurrentConsultant As String
    Dim CurrentRefNum As String
    Dim ValidationFile As String
    Dim ValidatePath As String
    Dim JustLooking As Boolean = False
    Dim CurrentCompany As String
    Dim Company As String
    Dim CompCorrPath As String
    Dim GeneralPath As String
    Dim ProjectPath As String
    Dim TempPath As String
    Dim OldTempPath As String = ""
    Dim TemplatePath As String
    Dim DirResult As String

    Dim StudyDrive As String
    Dim TempDrive As String = "C:\USERS\"
    Dim CurrentPath As String
    Dim RefNum As String
    Dim bJustLooking As Boolean = True
    Dim RefNumPart As String
    Dim LubRefNum As String
    Dim StudySave(20) As String
    Dim RefNumSave(20) As String
    Dim FirstTime As Boolean = True
    Dim KeyEntered As Boolean = False

    Private mDBConnection As String
    Private mStudyYear As String
    Private mPassword As String
    Private mRefNum As String
    Private mSpawn As Boolean
    Private mStudyType As String
    Private mUserName As String
    Private mReturnPassword As String

    'Private Err As ErrorLog
#End Region

#Region "PUBLIC PROPERTIES"

    Private mVPN As Boolean
    Public Property VPN() As Boolean
        Get
            Return mVPN
        End Get
        Set(ByVal value As Boolean)
            mVPN = value
        End Set
    End Property

    Public Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
        Set(ByVal value As String)
            mDBConnection = value
        End Set
    End Property
    Private mCurrentStudyYear As String
    Public Property CurrentStudyYear() As String
        Get
            Return mCurrentStudyYear
        End Get
        Set(ByVal value As String)
            mCurrentStudyYear = value
        End Set
    End Property
    Public Property StudyYear() As String
        Get
            Return mStudyYear
        End Get
        Set(ByVal value As String)
            mStudyYear = value
        End Set
    End Property

    Public Property UserName() As String

        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set

    End Property
    Public Property ReturnPassword() As String
        Get
            Return mReturnPassword
        End Get
        Set(ByVal value As String)
            mReturnPassword = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property


    Public Property StudyType() As String
        Get
            Return mStudyType
        End Get
        Set(ByVal value As String)
            mStudyType = value
        End Set
    End Property

    Public Property ReferenceNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property

    Public Property Spawn() As Boolean
        Get
            Return mSpawn
        End Get
        Set(ByVal value As Boolean)
            mSpawn = value
        End Set
    End Property
#End Region

#Region "INITIALIZATION"
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub Initialize()
        Try


            Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim version As System.Version = assembly.GetName().Version
            txtVersion.Text = String.Format("Version {0}", version)

            'Populate Consultant Tab with login
            tabConsultant.Text = UserName
            SetTabDirty(True)


            'Populate the Form Title
            Me.Text = "Console." & StudyType & " Validation Console"



            'Fill Study Combo Box

            cboStudy.Items.Add("PL12")
            cboStudy.Items.Add("TK12")
            cboStudy.Items.Add("NG10")
            cboStudy.Items.Add("NG13")
            StudyDrive = "K:\STUDY\LIQUID PIPELINE\"
            db = New DataObject(DataObject.StudyTypes.PIPELINES, UserName, Password)



            'Setup Database connection for ErrorLog
            'db.DBConnection = mDBConnection
            'Err = New ErrorLog(db) 'StudyType, UserName, Password)
            'Assigned Combo Box


            ds = db.ExecuteStoredProc("Console." & "GetAllConsultants")

            If ds.Tables.Count > 0 Then
                For Each Row In ds.Tables(0).Rows
                    cboConsultant.Items.Add(Row("Initials").ToString.ToUpper.Trim)
                    cbConsultants.Items.Add(Row("Initials").ToString.ToUpper.Trim)
                Next
            End If
            'Delivery Methods


            'Fill Directory Drop Down

            cboDir.Items.Add("Correspondence")
            cboDir2.Items.Add("Correspondence")
            cboCheckListView.SelectedIndex = 0

            cboDir.Items.Add("Company Correspondence")
            cboDir.Items.Add("Client Attachments")

            cboDir2.Items.Add("Company Correspondence")
            cboDir2.Items.Add("Client Attachments")


            cboDir.SelectedIndex = 0
            cboDir2.SelectedIndex = 2

            If UserName <> "JDW" And UserName <> "KRP" And UserName <> "DBB" And UserName <> "GLC" Then
                RemoveTab("tabClippy")
            End If




            'Check for existance of Settings file, if missing, copy default one

            GetSettingsFile()

            'If not spawned off an existing instance of the program, read the settings file and set the drop downs accordingly

            If Not Spawn Then
                ReadLastStudy()
            Else
                If ReferenceNum <> "" Then
                    cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(ReferenceNum, 2) & StudyYear)
                Else
                    cboStudy.SelectedIndex = 0
                End If

            End If


            GetCompanyContactInfo()

            Me.Cursor = Cursors.Default


        Catch ex As System.Exception
            db.WriteLog("Initialize", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

#Region "BUILD TABS"

    Private Sub BuildSummaryTab()

        mRefNum = cboRefNum.Text
        SetSummaryFileButtons()
        lblLastCalcDate.Text = ""
        lblLastFileSave.Text = ""
        lblLastUpload.Text = ""

        txtValidationIssues.Text = ""
        lblCalcUpload.Visible = False
        lblOSIMUpload.Visible = False
        lblOU.Visible = False

        If File.Exists(StudyDrive & StudyYear & "\Correspondence\" & mRefNum & "\" & RefNum & ".xls") Then
            btnValidate.Enabled = True
        Else
            btnValidate.Enabled = False
        End If


        Try

            If File.Exists(RefineryPath & mRefNum & ".xls") = False Then
                lblValidationStatus.ForeColor = Color.DarkRed
                lblValidationStatus.Text = "Not Received"
            Else
                lblLastFileSave.Text = FileDateTime(RefineryPath & mRefNum & ".xls")
                lblValidationStatus.ForeColor = Color.DarkGreen
                lblValidationStatus.Text = "In Progress"
            End If


            Dim dt As String
            Dim row As DataRow
            Dim params = New List(Of String)


            params.Add("RefNum/" + mRefNum)
            params.Add("MessageType/7")
            ds = db.ExecuteStoredProc("Console." & "GetMessageLog", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    dt = row("MessageTime").ToString
                    lblLastUpload.Text = dt.ToString
                End If
            End If
            ' Get the last time it was Calced from MessageLog
            'Add Parameters and call GetConsultants
            params = New List(Of String)
            params.Add("RefNum/" + mRefNum)
            ds = db.ExecuteStoredProc("Console." & "GetLastCalcDate", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    dt = row("MessageTime").ToString
                    lblLastCalcDate.Text = dt.ToString

                End If

            End If

            params = New List(Of String)
            params.Add("RefNum/" + mRefNum)
            ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    txtValidationIssues.Text = row("ValidationNotes").ToString
                Else
                    txtValidationIssues.Text = ""
                End If
            End If

            If File.Exists(CorrPath & "PN_" & Company & ".doc") Then
                btnCreatePN.Text = "Open PN File"
            Else
                btnCreatePN.Text = "Create PN File"
            End If

            If txtValidationIssues.TextLength > 0 Then
                If txtValidationIssues.Text.Substring(0, 1) = "~" Then

                    txtValidationIssues.ReadOnly = True
                    btnCreatePN.Text = "Open PN File"
                    btnUnlockPN.Enabled = True

                Else
                    txtValidationIssues.ReadOnly = False
                    btnUnlockPN.Enabled = False
                End If
            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
            db.WriteLog("BuildSummaryTab", ex)
        End Try



        'TODO:  This should be ROLE BASED when we get ACTIVE DIRECTORY

        ' Only the polishers get to push the button to lock the notes and create the Word file.
        ' Not sure if CRT and JDW are supposed to be on this list, but not a big deal.
        ' Added LS 9/9/09 per DEJ so she can go thru and extract the ones that have not been done.
        mUserName = mUserName.ToUpper



    End Sub

    Private Sub BuildCheckListTab(strYesNoAll As String) ' Y for completed, N for not completed, A for all

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""


        Select Case strYesNoAll
            Case "Complete"
                strYesNoAll = "Y"

            Case "Incomplete"
                strYesNoAll = "N"

            Case Else
                strYesNoAll = "A"

        End Select

        UpdateCheckList(strYesNoAll)


    End Sub

    Private Sub BuildCorrespondenceTab()

        Dim I As Integer
        Dim blnAlready As Boolean

        CorrPath = BuildCorrPath(cboRefNum.Text)
        lstVFNumbers.Items.Clear()
        DirResult = Dir(CorrPath & "VQ*")
        Do While DirResult <> ""

            ' Get the next VF file.
            I = 0
            blnAlready = False
            ' Is it already in the list?
            Do While I < lstVFNumbers.Items.Count
                If lstVFNumbers.Items(I) = Mid(DirResult, 3, 1) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            ' No, put it there
            If Not blnAlready Then
                lstVFNumbers.Items.Add(Mid(DirResult, 3, 1)) ' & Chr(9) & FileDateTime(strCorrPath & strDirResult)
            End If
            ' Look for next file
            DirResult = Dir()
        Loop

        ' Now look for any orphan VR files
        ' Same logic as above except VR.
        DirResult = Dir(CorrPath & "VR*")
        Do While DirResult <> ""

            I = 0
            blnAlready = False
            Do While I < lstVFNumbers.Items.Count
                If lstVFNumbers.Items(I) = Mid(DirResult, 3, 1) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            If Not blnAlready Then
                lstVFNumbers.Items.Add(Mid(DirResult, 3, 1)) ' & Chr(9) & FileDateTime(strCorrPath & strDirResult)
            End If
            DirResult = Dir()
        Loop


        lstVFNumbers.SelectedIndex = lstVFNumbers.Items.Count - 1
        ' Using this routine to actually load the Corr tab and also
        ' to find highest VF file number for fraCons_Handle.
        ' Only do the SetCorr routine if we are doing the Corr tab.

        SetCorr()

    End Sub

    Private Sub BuildIssuesTab()

        txtContinuingIssues.Text = ""
        txtConsultingOpps.Text = ""
        Dim row As DataRow
        Dim params As List(Of String)


        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetContinuingIssues", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            row = ds.Tables(0).Rows(0)
            txtContinuingIssues.Text = row("ContinuingIssues").ToString
        End If

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetConsultingOpportunities", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            row = ds.Tables(0).Rows(0)
            txtConsultingOpps.Text = row("ConsultingOpportunities").ToString
        End If

    End Sub

    Private Sub BuildDirectoriesTab()
        SetPaths()
        PopulateCompCorrTreeView(tvwCompCorr)
        PopulateCorrespondenceTreeView(tvwCorrespondence)
        PopulateCompCorrTreeView(tvwCompCorr2)
        PopulateCorrespondenceTreeView(tvwCorrespondence2)


        PopulateDrawingTreeView(tvwDrawings)
        PopulateCATreeView(tvwClientAttachments)
        PopulateDrawingTreeView(tvwDrawings2)
        PopulateCATreeView(tvwClientAttachments2)


    End Sub

#End Region

#Region "UTILITIES"

    Private Sub GetConsultants()

        Dim params As List(Of String)

        params = New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetAConsultant", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                CurrentConsultant = ds.Tables(0).Rows(0).Item("AssignedTo").ToString.ToUpper.Trim
            End If
        End If

        cboConsultant.Text = CurrentConsultant


    End Sub

    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function

    Private Sub SetTabDirty(status As Boolean)
        For i = 0 To 8
            TabDirty(i) = status
        Next
    End Sub
    Private Sub LoadTab(tab As Integer)
        Dim TabName As String = ""
        Select Case tab
            Case 0 'Consultant Tab


                SetConsultant(UserName)

                TabName = "Consultants Tab"

            Case 1, 2 'Refining/Plant and Company Tab

                GetCompanyContactInfo()
                GetContactInfo()
                GetInterimContactInfo()
                FillContacts()
                GetConsultants()

                If tab = 1 Then
                    TabName = "Company Tab"
                Else
                    TabName = "Terminal/Pipeline Tab"
                End If


            Case 3 'Summary Tab

                BuildSummaryTab()

                TabName = "Summary Tab"

            Case 4  'CheckList Tab

                BuildCheckListTab("Incomplete")

                TabName = "Checklist Tab"

            Case 5  'Correspondence Tab

                BuildCorrespondenceTab()

                TabName = "Correspondence Tab"


            Case 6  'Issues or Directory Tab


                BuildIssuesTab()

                TabName = "Issues Tab"

            Case 7 'Directory Tab



                BuildDirectoriesTab()

                TabName = "Secure Send Tab"


        End Select
        lblStatus.Text = TabName
    End Sub
    Private Function BuildNewSARecord() As String
        Dim params = New List(Of String)
        params.Add("RefNum/" + RefNum)

        If Not CheckSAMaster(RefNum) Then
            ds = db.ExecuteStoredProc("Console.InsertSARecord", params)
        Else
            MsgBox(CurrentCompany & " already has a record in SAMaster for " & Study & StudyYear)
            Return ""
        End If
        Return ds.Tables(0).Rows(0).Item(0)
    End Function
    Private Function CheckSAMaster(RefNum) As Boolean

        Dim params = New List(Of String)

        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console.CheckSANumber", params)
        If ds.Tables.Count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Private Function RemovePassword(File As String, password As String, companypassword As String) As Boolean
        Dim success As Boolean = True
        If File.Substring(File.Length - 3, 3).ToUpper = "XLS" Then
            If File.ToUpper.Contains("RETURN") Then
                success = Utilities.ExcelPassword(File, password, 1)
            Else
                success = Utilities.ExcelPassword(File, companypassword, 1)
            End If
        End If
        If File.Substring(File.Length - 3, 3).ToUpper = "DOC" Or File.Substring(File.Length - 3, 3).ToUpper = "OCX" Then
            success = Utilities.WordPassword(File, companypassword, 1)
        End If
        Return success
    End Function
    Private Function FixFilename(ext As String, strFile As String)
        Dim strNewFilename As String
        Dim NewExt As String = GetExtension(strFile)
        strNewFilename = strFile.Replace(NewExt, ext)
        Return strNewFilename
    End Function
    Private Function GetExtension(strFile As String)
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, 3)
        Catch
            ext = ""
        End Try
        Return ext
    End Function
    Function FindModifier(strNextNum As String) As String
        ' Given a number to use in VRx, see if we need to add a letter to it.
        Dim strWork1 As String
        Dim strWork2 As String

        strWork1 = ""
        strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & cboCompany.Text & " Email Text.txt")

        If strWork2 > strWork1 Then
            strWork1 = strWork2
        End If

        For I = 0 To dgFiles.Rows.Count - 1
            If dgFiles.Rows(I).Cells(0).ToString.Substring(dgFiles.Rows(I).Cells(0).ToString.Length - 4, 4).ToUpper = ".XLS" And dgFiles.Rows(I).Cells(0).ToString.ToUpper.Contains("RETURN") Then
                strWork2 = PlugLetterIfNeeded("Return" & strNextNum & ".xls")
            Else
                strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & dgFiles.Rows(I).Cells(0).ToString)
            End If
            If strWork2 > strWork1 Then
                strWork1 = strWork2
            End If
        Next I
        Return strWork1
    End Function

    Public Function PlugLetterIfNeeded(strFileName As String) As String
        Dim strWorkFileName As String
        Dim strDirResult As String
        Dim strCorrPath As String
        Dim strABC(10) As String
        Dim I As Integer
        Dim intInsertPoint As Integer

        strABC(0) = ""
        strABC(1) = "A"
        strABC(2) = "B"
        strABC(3) = "C"
        strABC(4) = "D"
        strABC(5) = "E"
        strABC(6) = "F"
        strABC(7) = "G"
        strABC(8) = "H"
        strABC(9) = "I"

        strCorrPath = CorrPath
        If strFileName.Substring(0, 6).ToUpper = "RETURN" Then
            intInsertPoint = 7
        Else
            intInsertPoint = 3
        End If

        For I = 0 To 9
            strWorkFileName = strFileName.Substring(0, intInsertPoint) & strABC(I) & Mid(strFileName, intInsertPoint + 1, 99)
            strDirResult = Dir(CorrPath & strWorkFileName)
            If strDirResult = "" Then
                Return strABC(I)
                Exit Function
            End If
        Next I

        MsgBox("More than 9 iterations of same file?", vbOKOnly, "Check with Joe Waters (JDW)?")

        PlugLetterIfNeeded = strFileName

    End Function
    Private Sub GetSettingsFile()
        Dim yr As String
        Try
            TempPath = TempDrive & mUserName & "\Console\Temp\"
            If Directory.Exists(TempDrive & mUserName & "\Console") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console")
            If Directory.Exists(TempDrive & mUserName & "\Console\Temp") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp")
            If Directory.Exists(TempDrive & mUserName & "\Console\Temp\ConsoleSettings") = False Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp\ConsoleSettings")

            If File.Exists(TempPath & "ConsoleSettings\" & StudyType & "Settings.txt") = False Then
                Dim objWriter As New System.IO.StreamWriter(TempPath & "ConsoleSettings\" & StudyType & "Settings.txt", False)


                objWriter.WriteLine("PL12")
                objWriter.WriteLine("12PAA105")
                objWriter.WriteLine("TK12")
                objWriter.WriteLine("12PAA105")
                objWriter.WriteLine("NG10")
                objWriter.WriteLine("10PAA100")
                objWriter.WriteLine("NG13")
                objWriter.WriteLine("13PAA100")

                objWriter.Close()
            End If

        Catch ex As System.Exception
            db.WriteLog("GetSettingsFile", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ReadLastStudy()
        Try
            Dim objReader As New IO.StreamReader(TempPath & "ConsoleSettings\" & StudyType & "settings.txt")

            For i = 0 To 3
                StudySave(i) = objReader.ReadLine()
                RefNumSave(i) = objReader.ReadLine()
            Next


            Dim lastStudy As String = objReader.ReadLine()
            cboStudy.Text = lastStudy
            Dim rn As String = objReader.ReadLine()
            cboRefNum.Text = rn
            RefNum = rn
            objReader.Close()
            GetRefNumRecord(rn)
        Catch ex As System.Exception
            db.WriteLog("ReadLastStudy", ex)
        End Try

    End Sub

    Private Sub SaveLastStudy()
        Try
            If cboRefNum.Text <> "" Then
                If Directory.Exists(TempPath & "ConsoleSettings\") = False Then GetSettingsFile()
                Dim objWriter As New System.IO.StreamWriter(TempPath & "ConsoleSettings\" & StudyType & "Settings.txt", False)


                For i = 0 To 3
                    objWriter.WriteLine(StudySave(i))
                    objWriter.WriteLine(RefNumSave(i))
                Next


                objWriter.WriteLine(cboStudy.Text)
                objWriter.WriteLine(cboRefNum.Text)
                objWriter.Close()
            End If
        Catch ex As System.Exception
            db.WriteLog("SaveLastStudy", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ClearFields()



        lblName.Text = ""
        lblEmail.Text = ""
        lblPhone.Text = ""


        lblRefCoName.Text = ""
        lblRefCoEmail.Text = ""
        lblRefCoPhone.Text = ""


        txtCompanyPassword.Text = ""


        txtSendFormat.SelectedText = ""


        txtConsultingOpps.Text = ""
        txtValidationIssues.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtDescription.Text = ""
        txtIssueName.Text = ""


        lblItemCount.Text = ""
        txtReturnPassword.Text = ""


    End Sub
    Private Sub ValFax_Build(strValFaxTemplate As String)

        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.


        Dim strDeadline As String
        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim TemplateValues As New WordTemplate()
        Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""

        Try
            If File.Exists("C:\USERS\" & UserName & "\Console\ValFax.txt") Then File.Delete("C:\USERS\" & UserName & "\Console\ValFax.txt")
            If strValFaxTemplate = "" Then
                ' Find out which template they want to use.
                strProgress = "cboTemplates.Clear"
                frmTemplates.cboTemplates.Items.Clear()

                strDirResult = Dir(TemplatePath & "*.doc*")
                If strDirResult = "" Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)

                    Exit Sub
                End If
                strProgress = "Loading cboTemplates"
                Do While strDirResult <> ""
                    If InStr(1, strDirResult, "Cover") > 0 And strDirResult.Contains(ParseRefNum(RefNum, 2)) Then
                        frmTemplates.cboTemplates.Items.Add(strDirResult)
                        'Debug.Print strDirResult, frmTemplates.cboTemplates.ListCount
                    End If
                    strDirResult = Dir()
                Loop

                If frmTemplates.cboTemplates.Items.Count < 1 Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)

                    Exit Sub
                End If

                ' Default to first one.
                frmTemplates.cboTemplates.SelectedIndex = 0

                ' If there is only one, don't make them choose,
                ' just go.
                If frmTemplates.cboTemplates.Items.Count > 1 Then
                    'MsgBox "Show form modal"
                    ' We use this form for other purposes as well (like selecting which SpecFrac file to open).
                    ' So, reload the caption and label items to the origian "ValFax" values.
                    frmTemplates.Text = "Select a Cover Template"

                    ' Ready to show them the form.
                    frmTemplates.ShowDialog()

                    ' Load strValFaxTemplate from what they selected on
                    ' frmTemplates. (strTemplateFile is a global variable.)
                    strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem
                    'MsgBox "After show form modal"
                Else
                    strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem

                End If

            End If

            ' At this point we have the name of the template to use.
            'MsgBox "Prepare new doc"
            btnValFax.Text = "Preparing New Document"
            strProgress = "Preparing New Document"
            ' Create the file to pass the data in.
            'Open gstrTempFolder & "ValFax.txt" For Output As #1
            'If Dir(TempDrive & _UserName & "\Console\", vbDirectory) = "" Then
            '    MkDir(TempDrive & _UserName & "\Console\")
            'End If
            If Not Directory.Exists(TempDrive & mUserName & "\Console\") Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\")
            Dim objWriter As New System.IO.StreamWriter(TempDrive & mUserName & "\Console\ValFax.txt", True)


            ' Todays date
            strProgress = "Printing Long Date"
            objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
            TemplateValues.Field.Add("_TodaysDate")
            TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))



            strProgress = "Getting business days to respond."
            ' Due date
            ' Need a way to make sure we get valid input here.
            If Dir(CorrPath & "vq*") = "" Then
                intNumDays = 10
            Else
                intNumDays = 5
            End If
            strDeadline = InputBox("How many business days do you want to give them to respond?", "Deadline", intNumDays)
            datDeadline = Utilities.ValFaxDateDue(strDeadline)
            objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
            TemplateValues.Field.Add("_DueDate")
            TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
            strProgress = "Getting Company name"
            ' Company Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)

            If ds.Tables.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                objWriter.WriteLine(row("Company"))
                TemplateValues.Field.Add("_Company")
                TemplateValues.RField.Add(row("Company").ToString)

                strProgress = "Getting Location"
                ' Refinery Name
                objWriter.WriteLine(row("Location"))
                TemplateValues.Field.Add("_Refinery")
                TemplateValues.RField.Add(row("Location").ToString)
                Coloc = row("Coloc").ToString

            End If

            strProgress = "Getting Contact info"


            ' Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
            If ds.Tables.Count = 0 Then

                If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    btnValFax.Text = "New Validation Fax"
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    strFaxName = row("RefName").ToString
                    strFaxEmail = row("RefEmail").ToString
                End If
            End If

            objWriter.WriteLine(strFaxName)

            TemplateValues.Field.Add("_Contact")
            TemplateValues.RField.Add(strFaxName)
            ' Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_EMail")
            TemplateValues.RField.Add(strFaxEmail)
            strProgress = "Getting consultant info"
            ' Consultant Name
            params = New List(Of String)
            params.Add("Initials/" + mUserName)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)

                    If row("ConsultantName") Is Nothing Or IsDBNull(row("ConsultantName")) Then
                        strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                    Else
                        strName = row("ConsultantName").ToString
                    End If
                Else

                    strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                End If
            End If

            objWriter.WriteLine(strName)
            TemplateValues.Field.Add("_ConsultantName")
            TemplateValues.RField.Add(strName)
            ' Consultant Initials
            objWriter.WriteLine(mUserName)
            TemplateValues.Field.Add("_Initials")

            TemplateValues.RField.Add(mUserName)
            strProgress = "Path to store it in"
            ' Path to store it in
            ' If it is Trans Pricing or FLCOMP, put it, without VQ#, into Refinery Corr folder.
            If strValFaxTemplate.Substring(0, 17) = "Trans Pricing.doc" Or strValFaxTemplate.Substring(strValFaxTemplate.Length - 10, 10) = "FLCOMP.doc" Then
                Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\" & Coloc & "\"
                ' If it has "Company Wide" in the name, store it in the Company Corr folder
            ElseIf InStr(1, strValFaxTemplate, "Company Wide") > 0 Then
                Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Coloc & "\"
                ' Otherwise -- MOST CASES HERE -- store it in the Refinery Corr folder with VF# on the front.
            Else
                ' Request from DEJ 3/2011 -- if it a Lubes refinery, put "Lube" at end of doc name.
                Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\VQ" & NextVQ() & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
            End If

            objWriter.WriteLine(Filename)
            ' --------- Add new items here 4/20/2007 ---------

            strProgress = "Getting Company Contact info"

            ' Company Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)

            If ds.Tables.Count = 0 Then

                If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then


                    btnValFax.Text = "New Validation Fax"
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                    strFaxEmail = row("Email").ToString
                End If
            End If

            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_CoCoContact")
            TemplateValues.RField.Add(strFaxName)

            ' Company Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_CoCoEMail")
            TemplateValues.RField.Add(strFaxEmail)
            objWriter.Close()
            ' --------- End of new items 4/20/2007 ---------



            ' Make sure the template file is available to copy.
            'On Error GoTo File1NotAvailable
            'intAttr = GetAttr(strTemplatePath & strValFaxTemplate)
            ' Next 2 lines commented 12/1/04 to try new approach
            ' due to problem with compression of network files.
            'SetAttr strTemplatePath & strValFaxTemplate, intAttr
            'On Error GoTo 0

            strProgress = "Copy doc to hard drive"
            ' Bring the doc down to the hard drive.

            If File.Exists(TempPath & strValFaxTemplate) Then

                Kill(TempPath & strValFaxTemplate)

            End If

            If File.Exists(TemplatePath & strValFaxTemplate) Then
                TemplatePath = TemplatePath
            End If

            ' Copy the Template to my work area.
            File.Copy(TemplatePath & strValFaxTemplate, TempPath & strValFaxTemplate, True)
            'varShellReturn = Shell("XCopy """ & strTemplatePath & strValFaxTemplate & """ """ & gstrTempFolder & """ /Y")
            'Stop
            timStart = Now()
            'MsgBox Now()
            Do While File.Exists(TempPath & strValFaxTemplate) = False

                If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                    MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                    'Exit Do
                End If
            Loop
            WordTemplateReplace(TempPath & strValFaxTemplate, TemplateValues, Filename)

            ' Copy the Macros File to my work area.
            'File.Copy(TemplatePath & "ValFaxMacros.doc", TempPath & "ValFaxMacros.doc", True)
            'strProgress = "Set up Word object"
            '' Set up an object that will be Word.

            'MSWord = New Word.Application
            '' Make Word visible
            'MSWord.Visible = True

            '' Open the document
            '' This was .Add which added a new document based on a .dot
            '' Changed it to open followed by .Run of AutoNew.
            'docNewFax = MSWord.Documents.Open(TempPath & strValFaxTemplate)
            'docValFaxMacros = MSWord.Documents.Open(TempPath & "ValFaxMacros.doc")

            'MSWord.Run("AutoNew", docNewFax.Name)

            'MSWord.Documents("ValFaxMacros.doc").Close()

            '' Cut the connection.


            '    btnValFax.Text = "New Validation Fax"
        Catch ex As System.Exception
            MSWord = Nothing
            db.WriteLog("Validation: " & CurrentRefNum.ToString, ex)
            MessageBox.Show(ex.Message)
        End Try
        MSWord = Nothing
    End Sub


    Public Sub WordTemplateReplace(strfile As String, fields As WordTemplate, strfilename As String)

        Dim oWord As Object
        Dim oDoc As Object
        Dim range As Object
        Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
        For count = 0 To fields.Field.Count - 1
            DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
        Next
        MessageBox.Show(DoubleCheck, "Template Field Check")

        oWord = CreateObject("Word.Application")
        oWord.Visible = True
        oDoc = oWord.Documents.Open(strfile)
        range = oDoc.Content
        Try
            For count = 0 To fields.Field.Count - 1
                With oWord.Selection.Find
                    .Text = fields.Field(count)
                    .Replacement.Text = fields.RField(count)
                    .Forward = True
                    .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                    .Format = False
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
            Next

            If File.Exists(strfilename) Then
                strfilename = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilename)
            Else
                'Stop
                strfilename = InputBox("Here is the suggested name for this New Validation Fax document." & Chr(10) & "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename)
            End If
            ' If they hit cancel, just stop.
            If strfilename = "" Then Exit Sub
            '
            oDoc.SaveAs(strfilename, ADDTORECENTFILES:=True)
            MessageBox.Show("Name is now " & oDoc.Name)
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Function NextVQ()

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 9
            strDirResult = Dir(CorrPath & "\VQ" & CStr(I) & "*.*")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextVR()

        Dim strDirResult As String
        Dim I As Integer
        For I = 0 To 9
            strDirResult = Dir(CorrPath & "\vr" & CStr(I) & "*.*")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextReturnFile()
        Dim strDirResult As String
        Dim I As Integer
        For I = 0 To 9
            strDirResult = Dir(CorrPath & "*F_Return" & CStr(I) & "*.xls")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing

    End Function


    Function CacheShellIcon(ByVal argPath As String) As String
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            mKey = "folder"
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function



    Sub SetCorr()

        Dim strDirResult As String

        lstVFFiles.Items.Clear()

        lstVRFiles.Items.Clear()

        lstReturnFiles.Items.Clear()

        strDirResult = Dir(CorrPath & "VQ*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem = Mid(strDirResult, 3, 1) Then

                lstVFFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))

            End If

            strDirResult = Dir()
        Loop
        strDirResult = Dir(CorrPath & "VR*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem = Mid(strDirResult, 3, 1) Then
                lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))

            End If
            strDirResult = Dir()
        Loop
        '------------------
        ' Added 1/30/2006 by FRS
        ' Include any VA files in lstVRFiles so the user can see whether
        ' the VQ has been ack'ed.

        strDirResult = Dir(CorrPath & "VA*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem = Mid(strDirResult, 3, 1) Then
                lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))

            End If
            strDirResult = Dir()
        Loop
        '------------------
        strDirResult = Dir(CorrPath & "*Return*")
        Do While strDirResult <> ""
            If lstVFNumbers.SelectedItem > "" _
                And (lstVFNumbers.SelectedItem = Mid(strDirResult, Len(strDirResult) - 4, 1) _
                    Or InStr(1, strDirResult, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                lstReturnFiles.Items.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))

            End If
            strDirResult = Dir()
        Loop

    End Sub
    Function DatePulledFromVAFile(ByVal strDirAndFileName As String) As Date

        Dim strWork As String = ""
        ' Read the second line of the VA file. Get the date from there.
        Dim objReader As New System.IO.StreamReader(strDirAndFileName, True)

        ' Read and toss the first line
        strWork = objReader.ReadLine()
        ' Read the date-time line
        strWork = objReader.ReadLine()

        DatePulledFromVAFile = strWork

    End Function


    Private Function VChecked(mRefNum As String, mode As String) As Boolean
        Dim ret As Boolean = False
        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("IssueID/" + mode)
        Dim ds As DataSet = db.ExecuteStoredProc("Console." & "IssueCheckV", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0).ToString = "Y" Then Return True
            End If
        End If
        Return False
    End Function
    Private Sub GetDirectories(ByVal subDirs() As DirectoryInfo, ByVal nodeToAddTo As TreeNode)

        Dim aNode As TreeNode
        Dim subSubDirs() As DirectoryInfo
        Dim subDir As DirectoryInfo
        For Each subDir In subDirs
            aNode = New TreeNode(subDir.Name, 0, 0)
            aNode.Tag = subDir
            aNode.ImageKey = "folder"
            subSubDirs = subDir.GetDirectories()
            If subSubDirs.Length <> 0 Then
                GetDirectories(subSubDirs, aNode)
            End If
            nodeToAddTo.Nodes.Add(aNode)
        Next subDir

    End Sub
    'Validation
    Private Sub Validation(JustLooking As Boolean)
        Dim strFLMacroDev As String = ""
        Dim AlreadyOpen As Boolean = False
        Dim templatePath As String
        'Launch Excel Validation
        ' Create new Application.
        Dim xl As Excel.Application

        'Dim obj As Object = Interaction.GetObject(TempPath & ValidationFile)


        xl = Utilities.GetExcelProcess()

        Try
            If Not Directory.Exists(TempDrive & UserName & "\VAL12\") Then Directory.CreateDirectory(TempDrive & UserName & "\VAL12\")
            If Not Directory.Exists(TempDrive & UserName & "\VAL12\temp\") Then Directory.CreateDirectory(TempDrive & UserName & "\VAL12\temp\")

            templatePath = TempDrive & UserName & "\VAL12\temp\"
            For Each wb In xl.Workbooks
                If wb.Name = ValidationFile Then
                    AlreadyOpen = True
                    Exit For
                End If
            Next wb

            Dim w As Excel.Workbook
            ' Open Excel spreadsheet.
            If Not AlreadyOpen Then
                If UserName = "DBB" Or UserName = "JDW" Or UserName = "SWP" Or UserName = "GLC" Then
                    If MsgBox("Use FLMacros from Development?", vbYesNoCancel, "Careful!") = vbYes Then
                        'gstrValidateFolder = gstrValidateFolder & "Development\"
                        strFLMacroDev = "Development\"
                    End If
                End If

                If Not IO.File.Exists(templatePath & ValidationFile) Then File.Delete(templatePath & ValidationFile)
                FileCopy(ValidatePath & strFLMacroDev & ValidationFile, templatePath & ValidationFile)
                If (VPN) Then
                    Dim b As DialogResult = MessageBox.Show("Since you are on VPN, we are going to pause to give the file enough time to load", "Wait", MessageBoxButtons.OK)
                End If
                System.Threading.Thread.Sleep(3000)
                w = xl.Workbooks.Open(templatePath & ValidationFile)
                xl.Visible = True
                w.RunAutoMacros(1)


            End If

            If xl.Workbooks(ValidationFile).Windows(ValidationFile).Visible = False Then xl.Workbooks(ValidationFile).Windows(ValidationFile).Visible = True
            If bJustLooking Then
                'Run Validation Macro
                xl.Run("JustLookin", CurrentRefNum)
            Else
                xl.Run("DelayLoadRefinery", CurrentRefNum)
            End If

        Catch ex As System.Exception
            db.WriteLog("Validation: " & CurrentRefNum.ToString, ex)
            MessageBox.Show("Trouble launching " & ValidationFile & ".  Make sure your Macro File is not already opened" & vbCrLf & ex.Message)
        End Try

    End Sub
    'Set all directory paths
    Private Sub SetPaths()
        Dim mRefNum As String = cboRefNum.Text
        StudyRegion = "Pipelines"

        CurrentCompany = GetCompanyName(mRefNum)
        If CurrentCompany.Length > 0 And CurrentCompany.IndexOf("-") > -1 Then
            CurrentCompany = CurrentCompany.Substring(0, CurrentCompany.IndexOf("-") - 1)
        End If
        GeneralPath = StudyDrive & "General\" & ParseRefNum(mRefNum, 1) & StudyRegion & "\"
        ProjectPath = StudyDrive & StudyYear & "\Project\"

        If Not Directory.Exists(TempDrive & mUserName & "\Console\Temp\") Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\Temp\")
        TempPath = TempDrive & mUserName & "\Console\Temp\"



        RefineryPath = StudyDrive & StudyYear & "\Correspondence\" & mRefNum & "\"
        CorrPath = StudyDrive & StudyYear & "\Correspondence\" & mRefNum & "\"
        If Directory.Exists(StudyDrive & StudyYear & "\Company\" & CurrentCompany & "\") Then
            CompCorrPath = StudyDrive & StudyYear & "\Company\" & CurrentCompany & "\"
        Else
            CompCorrPath = StudyDrive & StudyYear & "\Company\"
        End If
        CompanyPath = StudyDrive & StudyYear & "\Company\" & CurrentCompany & "\"
        ValidatePath = StudyDrive & StudyYear & "\Software\Validate\"
        DrawingPath = GeneralPath & "Drawings\"
        ClientAttachmentsPath = ValidatePath & "Client Attachments\"
        TemplatePath = ValidatePath & "Template\"




    End Sub
    'Method to remove specific tab from tabcontrol
    Private Sub RemoveTab(tabname As String)
        Dim tab As TabPage
        tab = ConsoleTabs.TabPages(tabname)
        ConsoleTabs.TabPages.Remove(tab)
    End Sub

    Private Function ParseRefNum(mRefNum As String, mode As Integer) As String
        Dim parsed As String = Utilities.GetRefNumPart(mRefNum, mode)
        Return parsed
    End Function

    'Method to save last refnum visited per Study for settings file
    Private Sub SaveRefNum(strStudy As String, strRefNum As String)

        For i = 0 To 11
            If StudySave(i) = strStudy Then
                RefNumSave(i) = strRefNum
            End If
        Next

    End Sub
#End Region

#Region "DATABASE LOOKUPS"

    Private Sub btnClippySearch_Click(sender As System.Object, e As System.EventArgs) Handles btnClippySearch.Click
        Dim da As SqlDataAdapter
        Dim conn As New SqlConnection(db.ConnectionString)
        Dim cmd As SqlCommand

        If chkSQL.Checked Then

            If txtClippySearch.Text.ToUpper.Contains("INSERT") Or txtClippySearch.Text.ToUpper.Contains("DELETE") Or txtClippySearch.Text.ToUpper.Contains("UPDATE") Or txtClippySearch.Text.ToUpper.Contains("DROP") Then
                lblError.Text = "CAN ONLY SELECT DATA - READ ONLY"
                Exit Sub
            End If
            cmd = New SqlCommand
            da = New SqlDataAdapter
            ds = New DataSet
            lblError.Text = ""
            Try

                Try

                    conn.Open()
                Catch ex As System.Exception
                    lblError.Text = ex.Message
                    Exit Sub
                End Try
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text

                cmd.CommandText = txtClippySearch.Text

                da.SelectCommand = cmd
                da.Fill(ds)
                dgClippyResults.DataSource = ds.Tables(0)
            Catch ex As System.Exception
                MessageBox.Show(ex.Message, "SQL ERROR")
            End Try

            conn.Close()

        End If
    End Sub

    Private Sub UpdateContinuingIssues()
        Dim params As New List(Of String)


        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Issue/" & Utilities.FixQuotes(txtContinuingIssues.Text))

        ds = db.ExecuteStoredProc("Console." & "UpdateContinuingIssues", params)
        lblStatus.Text = "Continuing Issues Updated..."
        BuildIssuesTab()
    End Sub
    Private Sub UpdateConsultingOpportunities()
        Dim params As New List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Issue/" & Utilities.FixQuotes(txtConsultingOpps.Text))

        ds = db.ExecuteStoredProc("Console." & "UpdateConsultingOpportunity", params)
        lblStatus.Text = "Consulting Opportunities Updated..."
        BuildIssuesTab()
    End Sub


    Private Function IsValidRefNum(refnum As String) As String
        Dim ret As String = ""
        Dim params = New List(Of String)

        params.Add("@RefNum/" + cboRefNum.Text)
        params.Add("@StudyYear/" + StudyYear.Substring(2, 2))

        Dim c As DataSet = db.ExecuteStoredProc("Console." & "IsValidRefNum", params)
        If c.Tables.Count > 0 Then
            If c.Tables(0).Rows.Count > 0 Then
                ret = c.Tables(0).Rows(0)(0).ToString
            End If
        End If
        Return ret

    End Function
    Private Function FindString(lb As ComboBox, str As String) As Integer
        Dim count As Integer = 0
        For Each item As String In lb.Items
            If item.Contains(str) Then
                Return count
            End If
            count += 1
        Next
    End Function


    Private Sub UpdateConsultant()
        Dim params = New List(Of String)

        params.Add("RefNum/" + ParseRefNum(RefNum, 0))
        params.Add("Consultant/" + cboConsultant.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)
        If c = 0 Then
            MessageBox.Show("Consultant did not save.")
        End If

    End Sub
    Private Sub UpdateIssue(status As String)
        Dim params As List(Of String)
        If Me.tvIssues.SelectedNode IsNot Nothing Then
            Dim node As String = tvIssues.SelectedNode.Text.ToString.Trim
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueTitle/" & node.Substring(0, node.IndexOf("-") - 1).Trim)
            params.Add("Status/" & status)
            params.Add("UpdatedBy/" & UserName)
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateIssue", params)
        End If
        UpdateCheckList()

    End Sub


    Private Function FillRefNums() As Integer
        Dim params As List(Of String)
        params = New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("StudyType/" + cboStudy.Text.Substring(0, 1))

        ds = db.ExecuteStoredProc("Console." & "GetRefNums", params)
        Dim row As DataRow 'Represents a row of data in Systems.data.datatable
        For Each row In ds.Tables(0).Rows
            cboRefNum.Items.Add(row("RefNum").ToString.Trim)
            cboCompany.Items.Add(row("CoLoc").ToString.Trim)
        Next
        Return ds.Tables(0).Rows.Count
    End Function
    'Method to populate refnums combo box
    Private Sub GetRefNums(Optional strRefNum As String = "")
        Dim NoRefNums As Integer = 0
        Dim selectItem As String = cboStudy.SelectedItem


        StudyYear = selectItem.Substring(selectItem.Length - 2, 2)
        If StudyYear <> CurrentStudyYear Then
            cboStudy.BackColor = Color.Yellow
            lblWarning.Visible = True
        Else
            cboStudy.BackColor = Color.White
            lblWarning.Visible = False
        End If

        Study = selectItem.Substring(0, selectItem.Length - 2)
        If (StudyYear > 50) Then
            StudyYear = "19" + StudyYear
        Else
            StudyYear = "20" + StudyYear
        End If

        If Not strRefNum Is Nothing Then
            cboRefNum.Items.Clear()
            cboCompany.Items.Clear()

            'Study RefNum Box
            NoRefNums = FillRefNums()

            If NoRefNums = 0 Then
                MessageBox.Show("There are no records for " & StudyYear & " study.", "Not Found")
                RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
                cboStudy.Text = "PL12"
                StudyYear = "2012"
                NoRefNums = FillRefNums()
                AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            End If



            If Not strRefNum Is Nothing And strRefNum.Length > 5 Then

                cboRefNum.SelectedIndex = cboRefNum.FindString(strRefNum)
            Else
                If ReferenceNum Is Nothing Then
                    'RefNum = cboRefNum.Items(0).ToString
                    cboRefNum.SelectedIndex = 0
                Else
                    RefNum = ReferenceNum.Trim
                    cboRefNum.SelectedIndex = cboRefNum.FindString(ReferenceNum)
                End If
            End If
        End If
    End Sub
    'Method to see if there is a combo refinery and show and populate the button




    'Function to retrieve Company name by RefNum
    Private Function GetCompanyName(str As String) As String

        'Get Company Name
        Dim params As List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetCompanyName", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then Return ds.Tables(0).Rows(0)("CoLoc").ToString.Trim
        End If

        ClearFields()
        Return ""

    End Function

    'If exists, this will retrieve Interim Contact info
    Private Sub GetInterimContactInfo()
        InterimContact = New Contacts()
        Dim params As New List(Of String)

        'Check for Interim Contact Info
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetInterimContactInfo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                InterimContact.FirstName = ds.Tables(0).Rows(0).Item("FirstName").ToString
                InterimContact.LastName = ds.Tables(0).Rows(0).Item("LastName").ToString
                InterimContact.Email = ds.Tables(0).Rows(0).Item("Email").ToString
                InterimContact.Phone = ds.Tables(0).Rows(0).Item("Phone").ToString
            End If
        Else
            InterimContact.Clear()
        End If

    End Sub

    'Method to retrieve Company Contact Information
    Private Sub GetCompanyContactInfo()

        CompanyContact = New Contacts()
        Dim row As DataRow
        Dim params As New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("ContactType/COORD")
        ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then

                row = ds.Tables(0).Rows(0)

                cboConsultant.SelectedItem = CurrentConsultant

                CompanyContact.FirstName = row("FirstName").ToString
                CompanyContact.LastName = row("LastName").ToString
                CompanyContact.Email = row("Email").ToString
                CompanyContact.Phone = row("Phone").ToString
                CompanyContact.AltFirstName = row("AltFirstName").ToString
                CompanyContact.AltLastName = row("AltLastName").ToString
                CompanyContact.AltEmail = row("AltEmail").ToString
                'CompanyContact.AltPhone = row("PhoneSecondary").ToString
                CompanyContact.Fax = row("Fax").ToString
                CompanyContact.MailAdd1 = row("MailAddr1").ToString
                CompanyContact.MailAdd2 = row("MailAddr2").ToString
                CompanyContact.MailAdd3 = row("MailAddr3").ToString
                CompanyContact.MailCity = row("MailCity").ToString
                CompanyContact.MailState = row("MailState").ToString
                CompanyContact.MailZip = row("MailZip").ToString
                CompanyContact.MailCountry = row("MailCountry").ToString
                CompanyContact.StreetAdd1 = row("StrAddr1").ToString
                CompanyContact.StreetAdd2 = row("StrAddr2").ToString
                CompanyContact.StreetCity = row("StrCity").ToString
                CompanyContact.StreetState = row("StrState").ToString
                CompanyContact.StreetZip = row("StrZip").ToString
                CompanyContact.StreetCountry = row("StrCountry").ToString
                CompanyContact.Title = row("JobTitle").ToString
                CompanyContact.Password = row("CompanyPassword").ToString.Trim
                CompanyPassword = CompanyContact.Password



            Else
                CompanyContact.Clear()
            End If
        End If

    End Sub

    'Method to retrieve Refinery Contact information
    Private Sub GetContactInfo()

        Dim row As DataRow
        Dim params As New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)



        TPLContact = New Contacts()
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                TPLContact.FullName = row("CoordName").ToString
                TPLContact.Title = row("CoordTitle").ToString
                TPLContact.Phone = row("WkPhone").ToString
                TPLContact.Email = row("Email").ToString

            Else
                TPLContact.Clear()
            End If
        End If


    End Sub
    'Method to retrieve all infomation per Ref Num
    Public Sub GetARefNumRecord(mRefNum As String)

        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum

        'If Refnum is lubes, change variable according to need

        Company = GetCompanyName(mRefNum)
        If Company.Length > 0 And Company.IndexOf("-") > -1 Then
            CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        End If

        cboCompany.Text = Company
        StudyRegion = ParseRefNum(mRefNum, 2)
        cboRefNum.Text = CurrentRefNum
        'Main

        GetCompanyContactInfo()
        GetConsultants()

        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub
    'Method to retrieve all infomation per Ref Num
    Public Sub GetRefNumRecord(mRefNum As String)
        CompanyPassword = ""
        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum

        StudyRegion = ParseRefNum(mRefNum, 2)
        Company = GetCompanyName(mRefNum)
        'If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        cboCompany.Text = Company

        cboRefNum.Text = CurrentRefNum
        'Main 

        GetCompanyContactInfo()
        GetConsultants()
        GetContactInfo()
        BuildSummaryTab()
        SetPaths()
        SetTabDirty(True)
        LoadTab(CurrentTab)

    End Sub

    'Retrieve all Refnums per Consultant, Study Type and Study Year for Consultant Tab
    Private Sub SetConsultant(consultant As String)

        Me.Cursor = Cursors.WaitCursor
        tvConsultant.Visible = False
        Dim params As New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("Study/" + Study.Substring(0, 1))
        params.Add("Consultant/" & consultant)
        Dim ds As DataSet = db.ExecuteStoredProc("Console." & "GetRefNumsByConsultant", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                tvConsultant.Visible = True
                tvConsultant.Nodes.Clear()
                FillConsultantTree(ds.Tables(0))
            End If
        End If


        cbConsultants.SelectedIndex = cbConsultants.FindStringExact(consultant)
        Me.Cursor = Cursors.Default
    End Sub

    'Routine to populate the Consultant Treeview with Refnums and outstanding issues
    Private Sub FillConsultantTree(dt As System.Data.DataTable)
        Dim root As TreeNode
        Dim node As TreeNode
        Dim dr As DataRow
        Dim row As DataRow
        Dim count As Integer = 0
        Dim params As List(Of String)
        Dim rn As String
        tvConsultant.Nodes.Clear()
        root = tvConsultant.Nodes.Add(Utilities.Pad(cbConsultants.Text, 60) & Utilities.Pad("VQ#", 7) & Utilities.Pad("Days", 7) & "File Date")

        For Each dr In dt.Rows
            rn = dr("RefNum").ToString.Trim
            If rn.Length < 8 Then rn = Space(8 - rn.Length) + rn
            node = root.Nodes.Add(Utilities.Pad(rn & " - " & dr("Coloc"), 59) & R3Data(dr("RefNum").ToString.Trim))


            params = New List(Of String)
            params.Add("RefNum/" + dr("RefNum").ToString.Trim)
            params.Add("Completed/N")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables.Count > 0 Then
                For I = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(I)
                        node.Nodes.Add(Utilities.Pad(row("IssueTitle").ToString.Trim, 60))

                    End If
                Next
            End If
            count += 1
        Next
        root.Expand()
    End Sub
    Function R3Data(refnum As String) As String
        Dim strTempCorrPath As String = Nothing
        Dim lstVFNumbers As New List(Of String)
        Dim dteFileDateLatest As Date
        Dim dteFileDate As Date
        Dim strDirResult As String
        Dim blnAlready As Boolean
        Dim I As Integer
        Dim R3 As String = Nothing

        ' lstVFNumbers is a non-visible list control that I use to
        ' hold and sort the VF numbers.
        strTempCorrPath = BuildCorrPath(refnum)
        strDirResult = Dir(strTempCorrPath & "VQ*")
        Do While strDirResult <> ""
            I = 0
            blnAlready = False
            Do While I < lstVFNumbers.Count
                If lstVFNumbers(I) = Mid(strDirResult, 3, 1) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            If Not blnAlready Then
                If IsNumeric(Mid(strDirResult, 3, 1)) Then
                    lstVFNumbers.Add(Mid(strDirResult, 3, 1))
                End If
            End If
            dteFileDate = FileDateTime(strTempCorrPath & strDirResult)
            If dteFileDate > dteFileDateLatest Then
                dteFileDateLatest = dteFileDate
            End If
            strDirResult = Dir()
        Loop

        Dim intHighVF As Integer
        If lstVFNumbers.Count > 0 Then
            If lstVFNumbers(lstVFNumbers.Count - 1) <> "" Then
                intHighVF = lstVFNumbers(lstVFNumbers.Count - 1)
                strDirResult = Dir(strTempCorrPath & "VR" & CStr(intHighVF) & "*")
                If strDirResult = "" Then
                    R3 = intHighVF & "      " & DateDiff(DateInterval.Day, dteFileDateLatest, Now) & "       " & Format(dteFileDateLatest, "dd-MMM-yy")
                Else
                    R3 = "        "
                End If
            End If
        Else
            R3 = "        "
        End If

        Return R3
    End Function
    Private Function BuildCorrPath(refnum As String) As String
        Dim path As String = Nothing


        If Directory.Exists(StudyDrive & StudyYear & "\Correspondence\" & refnum & "\") Then
            path = StudyDrive & StudyYear & "\Correspondence\" & refnum & "\"
        End If


        Return path
    End Function
#End Region

#Region "UI METHODS"
    Private Sub txtContinuingIssues_LostFocus(sender As Object, e As System.EventArgs) Handles txtContinuingIssues.LostFocus
        UpdateContinuingIssues()
    End Sub

    Private Sub txtConsultingOpps_LostFocus(sender As Object, e As System.EventArgs) Handles txtConsultingOpps.LostFocus
        UpdateConsultingOpportunities()
    End Sub

    Private Sub ConsoleTabs_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ConsoleTabs.SelectedIndexChanged
        CurrentTab = ConsoleTabs.SelectedIndex
        LoadTab(CurrentTab)
    End Sub

    Private Sub btnValStat_Click(sender As System.Object, e As System.EventArgs) Handles btnValStat.Click
        Process.Start("K:\STUDY\Liquid Pipeline\" & StudyYear & "\Project\Validation\Val Status PL.xls")
    End Sub


    Private Sub UpdateNotes()
        Dim params = New List(Of String)


        If ValidationIssuesIsDirty Then
            params.Add("RefNum/" + cboRefNum.Text)
            params.Add("Notes/" + Utilities.FixQuotes(txtValidationIssues.Text))
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
            If c = 1 Then

                ValidationIssuesIsDirty = False
            Else
                lblStatus.Text = "Validation Notes did not update"
            End If
        End If
    End Sub
    Private Sub txtValidationIssues_LostFocus(sender As Object, e As System.EventArgs) Handles txtValidationIssues.LostFocus
        UpdateNotes()
    End Sub


    Private Sub txtValidationIssues_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtValidationIssues.TextChanged
        ValidationIssuesIsDirty = True
    End Sub



    Private Sub tvIssues_AfterCheck(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterCheck

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""

        Try
            tvIssues.SelectedNode = e.Node

            If e.Node.Checked Then

                UpdateIssue("Y")
            Else

                UpdateIssue("N")
            End If

        Catch ex As System.Exception
            db.WriteLog("tvIssues_AfterCheck:" & RefNum, ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub tvIssues_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterSelect
        Dim IssueID As String = Nothing
        If Not tvIssues.SelectedNode Is Nothing Then


            IssueID = tvIssues.SelectedNode.Text
            IssueID = IssueID.Substring(0, IssueID.IndexOf("-")).Trim
            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueID/" & IssueID)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString
                    txtPostedBy.Text = row("PostedBy").ToString
                    txtPostedOn.Text = row("PostedTime").ToString
                    txtCompletedBy.Text = row("SetBy").ToString
                    txtCompletedOn.Text = row("SetTime").ToString
                    txtDescription.Text = row("IssueText").ToString

                End If


            End If

        End If
    End Sub

    Private Sub btnPricing_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Pricing", RefNum, GetCompanyName(RefNum), mDBConnection)
        ContactForm.Show()
    End Sub

    Private Sub btnDataCoordinator_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "DC", RefNum, GetCompanyName(RefNum), mDBConnection)
        ContactForm.Show()
    End Sub

    Private Sub btnDensity_Click(sender As System.Object, e As System.EventArgs)
        If Dir("K:\Study\Refining\General\DensityCalc.xls") <> "" Then
            Process.Start("K:\Study\Refining\General\DensityCalc.xls")
        End If
    End Sub

    Dim WithEvents ContactForm As ContactFormPopup

    'Show Contact Popup form
    Private Sub btnShow_Click(sender As System.Object, e As System.EventArgs) Handles btnShow.Click

        ContactForm = New ContactFormPopup(UserName, "Company", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()

    End Sub

    Private Sub btnShowAltCo_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "AltContact", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()

    End Sub
    Private Sub btnPlantCoordinator_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Plant", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()
    End Sub

    Private Sub btnShowIntCo_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Interim", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()

    End Sub


    Private Sub btnShowRefCo_Click(sender As System.Object, e As System.EventArgs) Handles btnShowRefCo.Click

        ContactForm = New ContactFormPopup(UserName, "REFINERY", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()

    End Sub

    Private Sub btnShowRefAltCo_Click(sender As System.Object, e As System.EventArgs)

        ContactForm = New ContactFormPopup(UserName, "ALTREF", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()

    End Sub
    Private Sub ContactForm_UpdateContact() Handles ContactForm.UpdateContact
        GetCompanyContactInfo()
        GetInterimContactInfo()
        FillContacts()
    End Sub
    Private Sub cbConsultants_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbConsultants.SelectedIndexChanged
        SetConsultant(cbConsultants.Text)
    End Sub

    Private Sub tvConsultant_DoubleClick(sender As Object, e As System.EventArgs) Handles tvConsultant.DoubleClick
        Dim refnum As String = ""
        Dim tab As String = cbConsultants.Text

        If Not tvConsultant.SelectedNode Is Nothing Then

            If tvConsultant.SelectedNode.Text.Substring(0, cbConsultants.Text.Length) <> cbConsultants.Text Then
                If Not tvConsultant.SelectedNode.Parent Is Nothing Then
                    If tvConsultant.SelectedNode.Parent.Text.Substring(0, cbConsultants.Text.Length) = cbConsultants.Text Then
                        refnum = tvConsultant.SelectedNode.Text
                        tab = "tabCompany"
                    Else
                        refnum = tvConsultant.SelectedNode.Parent.Text
                        tab = "tabCheckList"

                    End If
                    refnum = refnum.Substring(0, refnum.IndexOf("-")).Trim

                    cboStudy.Text = ParseRefNum(refnum, 2) & ParseRefNum(refnum, 3)
                    cboRefNum.Text = refnum
                    ConsoleTabs.SelectTab(tab)

                End If
            End If


        End If



    End Sub

    Private Sub btnCopyRetPassword_Click(sender As System.Object, e As System.EventArgs) Handles btnCopyRetPassword.Click
        Clipboard.SetText(txtReturnPassword.Text)
    End Sub

    Private Sub btnCopyCompanyPassword_Click(sender As System.Object, e As System.EventArgs) Handles btnCopyCompanyPassword.Click
        Clipboard.SetText(txtCompanyPassword.Text)
    End Sub



    Private Sub tvwCorrespondence_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tvwCorrespondence.KeyDown, tvwDrawings.KeyDown, tvwClientAttachments.KeyDown, tvwCompCorr.KeyDown, tvwCorrespondence2.KeyDown, tvwDrawings2.KeyDown, tvwClientAttachments2.KeyDown, tvwCompCorr2.KeyDown
        If e.KeyCode = Keys.Space Then
            Process.Start("C:\")
        End If
    End Sub

    Private Sub cboCompany_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        RemoveHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged
        Dim params = New List(Of String)

        Try
            params.Add("CoLoc/" + cboCompany.Text)
            params.Add("Study/" + Study)
            params.Add("StudyYear/" + StudyYear)
            ds = db.ExecuteStoredProc("Console." & "GetRefNumsByCompany", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables.Count > 0 Then
                    cboRefNum.Text = ds.Tables(0).Rows(0)("RefNum").ToString.Trim
                    GetARefNumRecord(cboRefNum.Text)

                End If
            End If
            Company = cboCompany.Text


            AddHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged

        Catch ex As System.Exception
            db.WriteLog("cboCompany_SelectedIndexChanged", ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub btnOpenCorrFolder_Click(sender As System.Object, e As System.EventArgs) Handles btnOpenCorrFolder.Click
        Process.Start(CorrPath)
    End Sub
    Public Sub UpdatePN()
        Dim params = New List(Of String)

        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Notes/" + txtValidationIssues.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)
        If c = 1 Then
            lblStatus.Text = "Validation Notes updated successfully"
        Else
            MessageBox.Show("Validation Notes did not update")
        End If
    End Sub
    Private Sub btnUpdatePresenterNotes_Click(sender As System.Object, e As System.EventArgs)
        If txtValidationIssues.Text <> "" Then
            UpdatePN()
        End If
    End Sub


    Private Sub btnUpdateIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateIssue.Click
        Dim params As List(Of String)
        ' Be sure they gave us an IssueID and an IssueTitle
        If txtIssueID.Text = "" Or txtIssueName.Text = "" Then
            MsgBox("Please enter both an ID and a Title")
            Exit Sub
        End If

        ' Drop any single quotes in ID, Title, and Text.
        txtIssueID.Text = Utilities.DropQuotes(txtIssueID.Text)
        txtIssueName.Text = Utilities.DropQuotes(txtIssueName.Text)
        txtDescription.Text = Utilities.DropQuotes(txtDescription.Text)

        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("IssueID/" + txtIssueID.Text)
        ' Be sure it does not already exist.
        ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                MsgBox("That IssueID already exists for this Refnum.", vbOKOnly)
                Exit Sub
            End If

        End If

        ' Everything looks OK, add it here.
        'Study Combo Box
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("IssueID/" + txtIssueID.Text)
        params.Add("IssueTitle/" + txtIssueName.Text)
        params.Add("IssueText/" + txtDescription.Text)
        params.Add("UserName/" + UserName)
        ds = db.ExecuteStoredProc("Console." & "AddIssue", params)
        BuildCheckListTab("Incomplete")
        btnAddIssue.Enabled = True
        txtDescription.ReadOnly = True
    End Sub

    Private Sub btnCancelIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelIssue.Click
        btnAddIssue.Enabled = True
    End Sub


    Private Sub btnSecureSend_Click_1(sender As System.Object, e As System.EventArgs) Handles btnSecureSend.Click
        Dim ss As New SecureSend(db, StudyType, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, Nothing, Nothing)

        ss.RefNum = RefNum
        ss.LubeRefNum = LubRefNum
        ss.Study = StudyType
        ss.TemplatePath = TemplatePath
        ss.CorrPath = CorrPath
        ss.DrawingPath = DrawingPath
        ss.ClientAttachmentsPath = ClientAttachmentsPath
        ss.Loc = Company
        ss.User = UserName

        ss.CompCorrPath = CompCorrPath
        ss.CorrPath2 = CorrPath
        ss.DrawingPath2 = DrawingPath
        ss.ClientAttachmentsPath2 = ClientAttachmentsPath

        ss.CompCorrPath2 = CompCorrPath

        ss.TempPath = TempPath
        ss.SendMethod = txtSendFormat.Text
        ss.CompanyPassword = CompanyPassword

        ss.Show()
    End Sub


    Private Sub cboConsultant_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles cboConsultant.KeyDown
        Dim params As List(Of String)


        If e.KeyCode = Keys.Enter Then
            cboConsultant.Text = cboConsultant.Text.ToUpper
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Consultant/" & cboConsultant.Text)
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)

            ConsoleTabs.SelectTab("tabConsultant")
            cbConsultants.Text = cboConsultant.Text
        End If

    End Sub

    Private Sub cboRefNum_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cboRefNum.KeyDown
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True

            cboRefNum.SelectedIndex = FindString(cboRefNum, cboRefNum.Text)

            If cboRefNum.SelectedIndex <> -1 Then
                RefNumChanged(cboRefNum.Text)
            Else
                cboRefNum.SelectedIndex = cboRefNum.FindString(CurrentRefNum)
            End If
        End If

    End Sub




    Private Sub btnFilesSave_Click(sender As System.Object, e As System.EventArgs) Handles btnFilesSave.Click

        Dim CopyFile As String
        Dim Dest As String
        Dim DestFile As String
        Dim ext As String
        Dim success As Boolean = True
        Try


            For i = 0 To dgFiles.Rows.Count - 1
                Dest = dgFiles.Rows(i).Cells(2).Value
                If Dest <> "Do Not Save" Then
                    lblStatus.Text = "Saving " & dgFiles.Rows(i).Cells(0).Value & "...."
                    Try
                        ext = GetExtension(dgFiles.Rows(i).Cells(0).Value)
                    Catch
                        ext = ""
                    End Try

                    CopyFile = TempPath & FixFilename(ext, dgFiles.Rows(i).Cells(0).Value)
                    DestFile = FixFilename(ext, dgFiles.Rows(i).Cells(1).Value)

                    Select Case (Dest)
                        Case "Correspondence"
                            sDir(i) = CorrPath
                        Case "Drawings"
                            sDir(i) = DrawingPath
                        Case "Company"
                            sDir(i) = CompCorrPath
                        Case Else
                            sDir(i) = GeneralPath

                    End Select
                    If (ext.Substring(0, 3).ToUpper = "DOC" Or ext.Substring(0, 3).ToUpper = "XLS") Then
                        If CompanyPassword.Length = 0 Then
                            MessageBox.Show("Must have a Company Password to Encrypt files", "Save Error")
                            Exit Sub
                        End If
                        success = RemovePassword(CopyFile, GetReturnPassword(RefNum, "fuelslubes"), CompanyPassword)
                    End If
                    If success Then
                        File.Copy(CopyFile, sDir(i) & DestFile, True)
                    Else
                        MessageBox.Show("Appears the password is incorrect for " & DestFile & ".  Make sure this is the correct company: " & Company)

                    End If
                    File.Delete(CopyFile)
                End If


            Next
            File.Copy(TempPath & "Email.txt", CorrPath & Utilities.CleanFileName(txtMessageFilename.Text), True)
            File.Delete(TempPath & "Email.txt")

            dgFiles.Rows.Clear()

            'Dim d As DialogResult = MessageBox.Show("Email and attachments saved.  Would you like to open the correspondence directory?", "Open Folder", MessageBoxButtons.YesNo)
            'If d = System.Windows.Forms.DialogResult.Yes Then Process.Start(CorrPath)
            txtMessageFilename.Text = ""
            btnFilesSave.Enabled = False
            If OldTempPath.Length > 0 Then TempPath = OldTempPath
            lblStatus.Text = "Attachments Saved"
        Catch ex As System.Exception
            db.WriteLog("btnSaveFiles", ex)
            lblStatus.Text = "Error saving files: " & ex.Message
        End Try
    End Sub





    Private Sub btnSecureSend_Click(sender As System.Object, e As System.EventArgs)

    End Sub


    Private Sub cboDir_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir.SelectedIndexChanged
        Select Case cboDir.Text

            Case "Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = True

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Client Attachments"
                tvwClientAttachments.Visible = True
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Drawings"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = True
                tvwCompCorr.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = True
        End Select
    End Sub
    Private Sub cboDir2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir2.SelectedIndexChanged
        Select Case cboDir2.Text

            Case "Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = True

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Client Attachments"
                tvwClientAttachments2.Visible = True
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Drawings"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = True
                tvwCompCorr2.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = True
        End Select
    End Sub


    Private Sub btnOpenWord_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As New Word.Application
        Dim docPN As New Word.Document
        Try
            If Dir(CorrPath & "PN_" & Company & ".doc") = "" Then
                If MsgBox("Do you want to create PN_" & Company & " .doc and stop updating the Validation Notes / Presenter's Notes field through Console?", vbYesNoCancel + vbDefaultButton2, "Click Yes to create the file.") = vbYes Then
                    'MsgBox "Create the file, lock the field, open the file with word."

                    Dim strWork As String = "~ " & Now & " The Presenters Notes document has been built so this field is locked. Please use the Presenters Notes button to open the file. " & Chr(13) & Chr(10) & "----------" & Chr(13) & Chr(10)

                    ' Add the tilde to the Issues Notes field
                    ' First, see if a record exists for this refnum
                    Dim row As DataRow
                    Dim params As New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

                    If ds.Tables.Count > 0 Then
                        row = ds.Tables(0).Rows(0)

                        txtValidationIssues.Text = row("ValidationNotes").ToString


                    Else

                        params = New List(Of String)
                        params.Add("RefNum/" + RefNum)
                        params.Add("Notes/" + txtValidationIssues.Text)

                        Dim Count As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes", params)

                    End If

                    Dim objWriter As New System.IO.StreamWriter(TempPath & "\PNotes.txt", True)
                    objWriter.WriteLine(RefNum)
                    objWriter.WriteLine(txtValidationIssues.Text)
                    objWriter.Close()

                    ' Create the document.
                    File.Copy(TemplatePath & "PN_Template.doc", TempPath & "PN_Template.doc", True)
                    msword = New Word.Application
                    'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                    docPN = msword.Documents.Open(TempPath & "PN_Template.doc")

                    docPN.SaveAs(CorrPath & "PN_" & Company & ".doc")

                    ' Put the CoLoc and the Notes into the document.
                    msword.Run("SubsAuto")
                End If
            Else

                msword = New Word.Application
                'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                docPN = msword.Documents.Open(CorrPath & "PN_" & Company & ".doc")
                msword.Visible = True
            End If

        Catch Ex As System.Exception
            db.WriteLog("btnOpenWord", Ex)
        End Try


    End Sub


    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        BuildCorrespondenceTab()
    End Sub


    Private Sub MainConsole_DragDrop(ByVal sender As Object, _
                        ByVal e As System.Windows.Forms.DragEventArgs) _
                        Handles MyBase.DragDrop, ConsoleTabs.DragDrop, tvwClientAttachments.DragDrop, tvwClientAttachments2.DragDrop, tvwCompCorr.DragDrop, tvwCompCorr2.DragDrop,
                        tvwCorrespondence.DragDrop, tvwCorrespondence2.DragDrop, tvwDrawings.DragDrop, tvwDrawings2.DragDrop

        Dim strFile As String = String.Empty
        Dim ext As String
        Dim zext As String
        Dim strNextNum = NextVQ().ToString
        Dim strNextRetNum = NextReturnFile().ToString
        Dim SavedFiles As New List(Of String)
        Dim strFileName As String
        Dim ReturnFile As Boolean = False
        Dim OrigFile As Boolean = False
        Dim NextLetter As String = ""

        If cboRefNum.Text <> "" Then
            Dim strNumModifier = FindModifier(strNextNum)
            Try
                Dim filenames() As String = Directory.GetFiles(TempPath, "*.*")
                For Each sFile In filenames
                    Try
                        File.Delete(sFile)
                    Catch ex As System.Exception
                        db.WriteLog("DragDrop", ex)
                        Debug.Print(ex.Message)
                    End Try
                Next

                Me.ConsoleTabs.SelectedIndex = 8


                If dgFiles.Rows.Count = 0 Then
                    If Not e.Data.GetDataPresent(DataFormats.Text) = False Then
                        'check if this is an outlook message. The outlook messages, all contain a FileContents attribute. If not, exit.
                        Dim formats() As String = e.Data.GetFormats()

                        If formats.Contains("FileContents") = False Then Exit Sub

                        'they are dragging the attachment
                        If (e.Data.GetDataPresent("Object Descriptor")) Then
                            Dim app As New Microsoft.Office.Interop.Outlook.Application() ' // get current selected items
                            Dim selection As Microsoft.Office.Interop.Outlook.Selection
                            Dim myText As String = ""
                            selection = app.ActiveExplorer.Selection
                            If selection IsNot Nothing Then

                                Dim mailItem As Outlook._MailItem
                                For i As Integer = 0 To selection.Count - 1

                                    mailItem = TryCast(selection.Item(i + 1), Outlook._MailItem)

                                    If mailItem IsNot Nothing Then
                                        Try
                                            'If mailItem.Recipients.Count > 0 Then Solomon = mailItem.Recipients(1).Address.Contains("SOLOMON")
                                            myText = ""
                                            myText = e.Data.GetData("Text")  'header text
                                            myText += vbCrLf + vbCrLf
                                            myText += mailItem.Body  'Plain Text Body Message


                                            'now save the attachments with the same file name and then 1,2,3 next to it
                                            For Each att As Attachment In mailItem.Attachments
                                                If att.FileName.Substring(0, 5).ToUpper <> "IMAGE" Then
                                                    Try
                                                        ext = att.FileName.Substring(att.FileName.LastIndexOf(".") + 1, att.FileName.Length - att.FileName.LastIndexOf(".") - 1)
                                                    Catch
                                                        ext = ""
                                                    End Try

                                                    If ext.ToUpper = "ZIP" Then
                                                        If Dir(TempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(TempPath & "Extracted Files\")
                                                        Dim zfilenames() As String = Directory.GetFiles(TempPath & "Extracted Files", "*.*")
                                                        For Each sFile In zfilenames
                                                            Try
                                                                File.Delete(sFile)
                                                            Catch ex As System.Exception
                                                                db.WriteLog("DragDrop", ex)
                                                                Debug.Print(ex.Message)
                                                            End Try
                                                        Next
                                                        att.SaveAsFile(TempPath & "Extracted Files\" & att.FileName)
                                                        Utilities.UnZipFiles(TempPath & "Extracted Files\" & att.FileName, TempPath, CompanyPassword)
                                                        File.Delete(TempPath & "Extracted Files\" & att.FileName)
                                                        OldTempPath = TempPath & "Extracted Files\"

                                                        For Each mfile In Directory.GetFiles(OldTempPath)
                                                            mfile = Utilities.ExtractFileName(mfile)
                                                            Dim zr = dgFiles.Rows.Add()

                                                            Dim zcbcell = New DataGridViewComboBoxCell
                                                            zcbcell.Items.Add("Do Not Save")
                                                            zcbcell.Items.Add("Correspondence")
                                                            zcbcell.Items.Add("Company")
                                                            zcbcell.Items.Add("Drawings")
                                                            zcbcell.Items.Add("General")
                                                            zcbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                                                            dgFiles.Rows(zr).Cells(2) = zcbcell
                                                            zext = mfile.Substring(mfile.LastIndexOf(".") + 1, 3)
                                                            Select Case (zext.ToUpper)
                                                                Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX"
                                                                    zcbcell.Value = "Correspondence"
                                                                Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG", "VSO"
                                                                    zcbcell.Value = "Drawings"
                                                                Case Else
                                                                    zcbcell.Value = "General"
                                                            End Select

                                                            dgFiles.Rows(zr).Cells(0).Value = mfile
                                                            If mfile.Substring(0, 2).ToUpper() = "VQ" Then
                                                                strFile = mfile
                                                                dgFiles.Rows(zr).Cells(1).Value = "VR" & NextVR() & "-" & mfile
                                                            Else
                                                                strFile = mfile.Substring(0, mfile.Length - 4) & "." & zext
                                                            End If

                                                            If mfile.ToUpper.Contains("RETURN") Then
                                                                dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & strNextRetNum & "." & zext
                                                            Else
                                                                dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & "." & zext
                                                            End If



                                                            File.Copy(OldTempPath & strFile, TempPath & strFile)
                                                        Next

                                                    Else

                                                        Dim r = dgFiles.Rows.Add()
                                                        dgFiles.Rows(r).Cells(0).Value = att.FileName
                                                        Dim cbcell = New DataGridViewComboBoxCell
                                                        cbcell.Items.Add("Do Not Save")
                                                        cbcell.Items.Add("Correspondence")
                                                        cbcell.Items.Add("Company")
                                                        cbcell.Items.Add("Drawings")
                                                        cbcell.Items.Add("General")
                                                        cbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                                                        dgFiles.Rows(r).Cells(2) = cbcell

                                                        Select Case (ext.ToUpper.Substring(0, 3))
                                                            Case "DOC", "XLS", "PDF", "XL", "TXT", "MSG"
                                                                cbcell.Value = "Correspondence"


                                                            Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG"
                                                                cbcell.Value = "Drawings"

                                                            Case Else
                                                                cbcell.Value = "General"


                                                        End Select


                                                        dgFiles.Rows(r).Cells(0).Value = att.FileName

                                                        If File.Exists(CorrPath & att.FileName) Then

                                                            strFile = IncrementFileNumber(att.FileName.Substring(0, att.FileName.Length - ext.Length - 1), ext)
                                                            NextLetter = strFile.Substring(strFile.Length - 1, 1)
                                                            OrigFile = True
                                                            dgFiles.Rows(r).Cells(1).Value = strFile
                                                        Else
                                                            If att.FileName.Substring(0, 2).ToUpper() = "VQ" Then
                                                                ReturnFile = True
                                                                strFile = "VR" & NextVR() & "-" & att.FileName
                                                                dgFiles.Rows(r).Cells(1).Value = "VR" & NextVR() & "-" & att.FileName
                                                            Else
                                                                strFile = att.FileName.Substring(0, att.FileName.Length - ext.Length - 1) & "." & ext
                                                                If strFile.ToUpper.Contains("RETURN") Then
                                                                    ReturnFile = True
                                                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, att.FileName.Length - ext.Length - 1) & strNextRetNum & "." & ext
                                                                Else
                                                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, att.FileName.Length - ext.Length - 1) & "." & ext
                                                                End If
                                                            End If
                                                        End If


                                                        ' dgFiles.Rows(r).Cells(1).Value = strFile
                                                        strFileName = strFile

                                                        att.SaveAsFile(TempPath & dgFiles.Rows(r).Cells(0).Value)

                                                    End If
                                                End If
                                            Next
                                            Dim strEmailFile As String
                                            dgFiles.EditMode = DataGridViewEditMode.EditOnEnter
                                            'save the mail message

                                            strEmailFile = TempPath & "email.txt"
                                            Dim strw As New StreamWriter(strEmailFile, False)
                                            strw.WriteLine(myText)
                                            strw.Close()
                                            strw.Dispose()

                                            strFileName = cboCompany.Text


                                            ' More often, use this technique to name the text file.

                                            If strFileName.Contains("VQ") Then

                                                txtMessageFilename.Text = "VR" & NextVR() & " " & mailItem.Subject.Replace(":", " ") & ".txt"
                                            Else
                                                If OrigFile Then
                                                    txtMessageFilename.Text = mailItem.Subject.Replace(":", " ") & "-" & NextLetter & ".txt"
                                                Else
                                                    txtMessageFilename.Text = mailItem.Subject.Replace(":", " ") & ".txt"
                                                End If
                                            End If





                                        Catch ex As System.Exception
                                            db.WriteLog("DragDrop", ex)
                                        Finally
                                            Marshal.ReleaseComObject(mailItem)
                                        End Try

                                    End If
                                Next
                            End If

                        End If
                    End If
                End If
            Catch ex As System.Exception
                db.WriteLog("DragDrop", ex)
                MessageBox.Show(ex.Message)
            End Try
            If txtMessageFilename.Text.Length > 0 Then btnFilesSave.Enabled = True
        End If
    End Sub
    Private Function IncrementFileNumber(strFile As String, ext As String) As String

        Dim NewFile As String = strFile & "-A." & ext

        Dim Letters As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        For Each letter In Letters
            If File.Exists(CorrPath & strFile & "-" & letter & ext) Then
                NewFile = strFile & "-" & Chr(Asc(letter) + 1)
            End If
        Next


        Return NewFile


    End Function
    Private Sub dgFiles_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFiles.CellEndEdit

    End Sub


    Private Sub dgFiles_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFiles.CellEnter
        If e.ColumnIndex = 1 Then
            SendKeys.Send("{F4}")
        End If
        SavedFile = dgFiles.CurrentCell.Value
    End Sub

    Private Sub btnGVClear_Click(sender As System.Object, e As System.EventArgs) Handles btnGVClear.Click
        dgFiles.RowCount = 1
        dgFiles.Rows.Clear()
        For Each foundFile As String In Directory.GetFiles(TempPath)
            File.Delete(foundFile)
        Next
        txtMessageFilename.Text = ""
        btnFilesSave.Enabled = False
    End Sub
    Private Sub tvwCorrespondence_BeforeExpand(sender As System.Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvwCorrespondence.BeforeExpand, tvwDrawings.BeforeExpand, tvwClientAttachments.BeforeExpand, tvwCompCorr.BeforeExpand, tvwCorrespondence2.BeforeExpand, tvwDrawings2.BeforeExpand, tvwClientAttachments2.BeforeExpand, tvwCompCorr2.BeforeExpand
        e.Node.Nodes.Clear()
        ' get the directory representing this node
        Dim mNodeDirectory As IO.DirectoryInfo
        mNodeDirectory = New IO.DirectoryInfo(e.Node.Tag.ToString)

        ' add each subdirectory from the file system to the expanding node as a child node
        For Each mDirectory As IO.DirectoryInfo In mNodeDirectory.GetDirectories
            ' declare a child TreeNode for the next subdirectory
            Dim mDirectoryNode As New TreeNode
            ' store the full path to this directory in the child TreeNode's Tag property
            mDirectoryNode.Tag = mDirectory.FullName
            ' set the child TreeNodes's display text
            mDirectoryNode.Text = mDirectory.Name
            ' add a dummy TreeNode to this child TreeNode to make it expandable
            mDirectoryNode.Nodes.Add("*DUMMY*")
            mDirectoryNode.ImageKey = CacheShellIcon(mDirectoryNode.Tag)
            mDirectoryNode.SelectedImageKey = mDirectoryNode.ImageKey & "-open"
            ' add this child TreeNode to the expanding TreeNode
            e.Node.Nodes.Add(mDirectoryNode)
        Next

        ' add each file from the file system that is a child of the argNode that was passed in
        For Each mFile As IO.FileInfo In mNodeDirectory.GetFiles
            ' declare a TreeNode for this file
            Dim mFileNode As New TreeNode
            ' store the full path to this file in the file TreeNode's Tag property
            mFileNode.Tag = mFile.FullName
            ' set the file TreeNodes's display text
            mFileNode.Text = mFile.Name
            mFileNode.ImageKey = CacheShellIcon(mFileNode.Tag)
            mFileNode.SelectedImageKey = mFileNode.ImageKey
            mFileNode.SelectedImageKey = mFileNode.ImageKey & "-open"
            ' add this file TreeNode to the TreeNode that is being populated
            e.Node.Nodes.Add(mFileNode)
        Next

    End Sub

    Private Sub tvwCorrespondence_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvwCorrespondence.NodeMouseDoubleClick, tvwDrawings.NodeMouseDoubleClick, tvwClientAttachments.NodeMouseDoubleClick, tvwCompCorr.NodeMouseDoubleClick, tvwCorrespondence2.NodeMouseDoubleClick, tvwDrawings2.NodeMouseDoubleClick, tvwClientAttachments2.NodeMouseDoubleClick, tvwCompCorr2.NodeMouseDoubleClick
        ' try to open the file
        Try
            Process.Start(e.Node.Tag)
        Catch ex As System.Exception
            db.WriteLog("tvwCorrespondence_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try
    End Sub
    Private Sub MainConsole_DragEnter(ByVal sender As Object, _
                           ByVal e As System.Windows.Forms.DragEventArgs) _
                           Handles MyBase.DragEnter, ConsoleTabs.DragEnter, tvwCorrespondence.DragEnter, tvwDrawings.DragEnter, tvwCompCorr.DragEnter, tvwCompCorr2.DragEnter, tvwClientAttachments.DragEnter, tvwCorrespondence2.DragEnter, tvwDrawings2.DragEnter, tvwClientAttachments2.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub btnAddIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnAddIssue.Click

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtCompletedBy.Text = ""
        txtCompletedOn.Text = ""
        txtPostedBy.Text = ""
        txtPostedOn.Text = ""
        txtDescription.Text = ""
        txtIssueID.Focus()
        btnAddIssue.Enabled = False

        'IssueForm.DBConn = mDBConnection
        'IssueForm.RefNum = RefNum
        'IssueForm.StudyType = StudyType
        'IssueForm.UserName = mUserName
        'IssueForm.Show()
        txtDescription.ReadOnly = False

    End Sub


    Private Sub lstVFNumbers_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstVFNumbers.Click

        SetCorr()

    End Sub

    Private Sub btnHelp_Click(sender As System.Object, e As System.EventArgs) Handles btnHelp.Click



        Dim strHelpFilePath As String

        strHelpFilePath = "K:\STUDY\SAI Software\Console2012\"


        Process.Start(strHelpFilePath & "Console 2012.docx")

    End Sub

    Private Sub btnOpenValFax_Click(sender As System.Object, e As System.EventArgs) Handles btnOpenValFax.Click

        ' This routine will look for a personal ValFax for this user.
        ' If it does not exist, give a message to the user.
        ' If it does exist, open it in Word
        Dim docMyValFax As Word.Document

        Dim strTemplateFile As String
        Dim strDirResult As String

        'On Error GoTo MyValFaxError

        ' If they have a "WithSubstitutions" template,
        ' Call ValFax_Build and do not do anything else in this routine.
        strTemplateFile = Dir(TemplatePath & "MyValFax\WithSubstitutions\" & mUserName & ".doc")
        If strTemplateFile > "" Then
            ValFax_Build(strTemplateFile)
            Exit Sub
        End If

        ' No WithSubtitutions template, look for a vanilla template.
        strDirResult = Dir(TemplatePath & "MyValFax\" & mUserName & ".doc")
        If strDirResult = "" Then
            MsgBox("No MyValFax was found for " & mUserName & ". Please create the Word document " & TemplatePath & "MyValFax\" & mUserName & ".doc. See Joe Waters if you have questions.", vbOKOnly)
            'gblnTemplateOK = False
            Exit Sub
        End If

        ' Found a vanilla template, process it.
        strTemplateFile = strDirResult
        Dim MSWord As New Word.Application

        ' Make Word visible
        MSWord.Visible = True
        ' Open the document
        ' This was .Add which added a new document based on a .dot
        ' Changed it to open followed by .Run of AutoNew.


        ' Open the document
        docMyValFax = MSWord.Documents.Open(TemplatePath & "MyValFax\" & strTemplateFile)

        ' Cut the connection.
        MSWord = Nothing


    End Sub

    Private Sub btnLog_Click(sender As System.Object, e As System.EventArgs)
        ' These are the "file" buttons on the right of the
        ' Validation Summary tab.
        ' Basically, here we just open the path/file stored in the tag
        ' property of the button.
        ' This property was set by SetFileButtons earlier when the Refnum
        ' was set/changed.
        Dim wb As Object
        Dim xl As New Excel.Application


        Try

            xl.Visible = True
            wb = xl.Workbooks.Open("K:\Study\" & "Console." & StudyType & "\" & StudyYear & "\" & cboStudy.SelectedItem.ToString.Substring(0, 3) & "\Refinery\" & RefNum & "\" & RefNum & "_Log.xls")
            xl.Visible = True

            ' Next line is important.
            ' If you don't do this, if the user closes Excel then clicks a file button
            ' again, you will get an Excel window with just the 'frame'. The inside
            ' of the 'window' will be empty!
            xl = Nothing

        Catch ex As System.Exception

            MsgBox("Unable to open file. If you have Excel open, and are editing a cell, get out of that mode and retry. ", vbOKOnly, "Unable to open file")
            db.WriteLog("btnLOG_Click", ex)
        End Try
    End Sub


    Private Sub MainConsole_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Initialize()
    End Sub

    Private Sub MainConsole_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not Spawn Then SaveLastStudy()
        End
    End Sub


    Private Sub lstVFFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstVFFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstVFFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub lstVRFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstVRFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstVRFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVRFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVRFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub lstReturnFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstReturnFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstReturnFiles.SelectedItem.ToString.IndexOf("|")
        file = lstReturnFiles.SelectedItem.ToString.Substring(FindTab + 1, lstReturnFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub btnReceiptAck_Click(sender As System.Object, e As System.EventArgs) Handles btnReceiptAck.Click

        Dim strDirResult As String
        Dim strDateTimeToPrint As String

        strDirResult = Dir(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt")
        If strDirResult <> "" Then
            MsgBox("Receipt of VQ" & Me.lstVFNumbers.Text & " has already been noted.")
            Exit Sub
        End If

        strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        If strDateTimeToPrint <> "" Then

            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)

            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strDateTimeToPrint)
            objWriter.Close()

            MsgBox("VA" & Me.lstVFNumbers.Text & ".txt has been built.")
        Else
            MsgBox("Receipt Acknowledgement was canceled.")
        End If

    End Sub

    Private Sub btnBuildVRFile_Click(sender As System.Object, e As System.EventArgs) Handles btnBuildVRFile.Click

        Dim strDirResult As String
        Dim strTextForFile As String

        strDirResult = Dir(CorrPath & "VR" & Me.lstVFNumbers.Text & ".txt")
        If strDirResult <> "" Then
            MsgBox("There already is a VR" & Me.lstVFNumbers.Text & " file.")
            Exit Sub
        End If

        strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        If strTextForFile <> "" Then
            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)
            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strTextForFile)
            objWriter.Close()
            MsgBox("VR" & Me.lstVFNumbers.Text & ".txt has been built.")
        Else
            MsgBox("VR file build was cancelled.")
        End If

    End Sub

    Private Sub btnValFax_Click(sender As System.Object, e As System.EventArgs) Handles btnValFax.Click

        ValFax_Build("")

    End Sub

    Private Sub ValCheckList_ItemCheck(sender As Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles ValCheckList.ItemCheck

        Try
            If e.CurrentValue = CheckState.Checked Then
                e.NewValue = CheckState.Unchecked
                UpdateIssue("N")
            Else
                e.NewValue = CheckState.Checked
                UpdateIssue("Y")
            End If

        Catch ex As System.Exception
            db.WriteLog("ValCheckList_ItemCheck", ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ValCheckList_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ValCheckList.SelectedIndexChanged

        If Not ValCheckList.SelectedItem Is Nothing Then

            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueTitle/" & Me.ValCheckList.SelectedItem.ToString.Trim)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString
                    txtCompletedBy.Text = row("PostedBy").ToString
                    txtCompletedOn.Text = row("PostedTime").ToString
                    txtPostedBy.Text = row("SetBy").ToString
                    txtPostedOn.Text = row("SetTime").ToString
                    txtDescription.Text = row("IssueText").ToString

                End If


            End If

        End If



    End Sub


    Public Sub SetTree(tvw As TreeView, mRootPath As String)
        Try
            tvw.Nodes.Clear()
            Dim mRootNode As New TreeNode
            mRootNode.Text = mRootPath
            mRootNode.Tag = mRootPath
            mRootNode.ImageKey = CacheShellIcon(mRootPath)
            mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
            mRootNode.Nodes.Add("*")
            tvw.Nodes.Add(mRootNode)
            tvw.ExpandAll()
            tvw.Refresh()
        Catch ex As System.Exception
            db.WriteLog("SetTree", ex)
        End Try
    End Sub

    Private Sub btnUpdateNotes_Click(sender As System.Object, e As System.EventArgs)


        Dim bIssues As Boolean = False

        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

        If ds.Tables.Count = 0 Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "InsertValidationNotes", params)

        Else
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtValidationIssues.Text))

            ds = db.ExecuteStoredProc("Console." & "UpdateValidationNotes", params)
        End If

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)


        ds = db.ExecuteStoredProc("Console." & "UpdateComments", params)


        BuildSummaryTab()



    End Sub



    Private Sub btnPrintIssues_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As New Word.Application
        Dim docIssues As New Word.Document
        Dim objWriter As New System.IO.StreamWriter(TempPath & "\Issues.txt", True)
        objWriter.WriteLine(RefNum)
        objWriter.WriteLine("Printed by: " & mUserName & " At: " & Now())
        objWriter.WriteLine("")
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.WriteLine("Validation Issues/Presenter's Notes:")
        objWriter.WriteLine(txtValidationIssues.Text)
        objWriter.WriteLine("")
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.WriteLine("Consulting Opportunities:")
        objWriter.WriteLine(txtConsultingOpps.Text)
        objWriter.WriteLine("------------------------------------------------------")
        objWriter.Close()

        ' Set up an object that will be Word.

        docIssues = msword.Documents.Open(TempPath & "Issues.txt")
        docIssues.PrintOut()

        ' Make Word visible
        'MSWord.Visible = True

        docIssues.Close()

        ' Cut the connection.
        msword = Nothing

        MsgBox("The text in the 'Validation Issues / Presenter's Notes' box has been sent to your default printer.")

    End Sub

    Private Sub cboCheckListView_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCheckListView.SelectedIndexChanged
        BuildCheckListTab(cboCheckListView.SelectedItem)
    End Sub

    Private Sub UpdateCheckList(Optional status As String = "N")

        Dim I As Integer
        Dim row As DataRow
        Dim params As List(Of String)
        Dim intCompleteCount As Integer, intNotCompleteCount As Integer
        Dim node As TreeNode
        RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        RemoveHandler cboCheckListView.SelectedIndexChanged, AddressOf cboCheckListView_SelectedIndexChanged
        ValCheckList.Items.Clear()
        tvIssues.Nodes.Clear()

        ' Load the Uncompleted Checklist Item count label
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Mode/Y")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Mode/N")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intNotCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        lblItemCount.Text = intCompleteCount & " Completed    " & intNotCompleteCount & " Not Complete."



        If status = "Y" Or status = "A" Then
            params = New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            params.Add("Completed/Y")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables(0).Rows.Count > 0 Then
                For I = 0 To ds.Tables(0).Rows.Count - 1
                    row = ds.Tables(0).Rows(I)

                    node = tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))

                    node.Checked = True

                Next

            End If
            cboCheckListView.SelectedIndex = 1
        End If

        If status = "N" Or status = "A" Then
            params = New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            params.Add("Completed/N")
            ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For I = 0 To ds.Tables(0).Rows.Count - 1
                        row = ds.Tables(0).Rows(I)

                        tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))

                    Next

                End If
            End If
            If status = "N" Then
                cboCheckListView.SelectedIndex = 0
            Else
                cboCheckListView.SelectedIndex = 2
            End If
        End If
        AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
        AddHandler cboCheckListView.SelectedIndexChanged, AddressOf cboCheckListView_SelectedIndexChanged
    End Sub
    Private Sub SetSummaryFileButtons()
        RefNum = cboRefNum.Text
        If RefNum <> "" Then

            If File.Exists("K:\STUDY\Liquid Pipeline\" & StudyYear & "\Project\Validation\Val Status PL.xls") Then
                btnValStat.Enabled = True
            Else
                btnValStat.Enabled = False
            End If
            If StudyType.ToUpper = "TERMINALS" Then
                If File.Exists(StudyDrive & StudyYear & "\Project\Validation\ValDet_TK.xls") Then
                    btnValDet.Enabled = True
                Else
                    btnValDet.Enabled = False
                End If
            Else
                If File.Exists(StudyDrive & StudyYear & "\Project\Validation\ValDet_PL.xls") Then
                    btnValDet.Enabled = True
                Else
                    btnValDet.Enabled = False
                End If
            End If

            If File.Exists(StudyDrive & StudyYear & "\Company\" & GetShortCompanyName(RefNum) & "\" & GetLocationName(RefNum) & ".xls") Then
                btnValidatedInput.Enabled = True
            Else
                btnValidatedInput.Enabled = False
            End If

            If File.Exists(StudyDrive & StudyYear & "\" & StudyType & "\" & RefNum & "\" & RefNum & "_CT.xls") Then
                btnClientTable.Enabled = True
            Else
                btnClientTable.Enabled = False
            End If

        End If

    End Sub
    Private Function GetShortCompanyName(mRefnum As String) As String
        Dim mLocation As String = ""
        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetLocation", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                mLocation = ds.Tables(0).Rows(0)("ShortCompanyName").ToString.Trim
            End If
        End If
        Return mLocation

    End Function
    Private Function GetLocationName(mRefnum As String) As String
        Dim mLocation As String = ""
        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetLocation", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                mLocation = ds.Tables(0).Rows(0)("ShortLocation").ToString.Trim
            End If
        End If
        Return mLocation

    End Function
    Private Sub PopulateCompCorrTreeView(tvw As TreeView)

        If Directory.Exists(CompCorrPath) Then
            SetTree(tvw, CompCorrPath)
        Else
            CompCorrPath = StudyDrive & "Console." & StudyType & "\" & StudyYear & "\" & StudyRegion + "\" & Study & "\Company Correspondence\"
            SetTree(tvw, CompCorrPath)
        End If

    End Sub
    Private Sub PopulateCorrespondenceTreeView(tvw As TreeView)

        If Directory.Exists(CorrPath) Then
            SetTree(tvw, CorrPath)
        Else
            CorrPath = StudyDrive & "Console." & StudyType & "\" & StudyYear & "\" & StudyRegion + "\" & Study & "\Correspondence\"
            SetTree(tvw, CorrPath)
        End If

    End Sub


    Private Sub PopulateDrawingTreeView(tvw As TreeView)

        If Directory.Exists(DrawingPath) Then
            SetTree(tvw, DrawingPath)
        Else
            DrawingPath = StudyDrive & "Console." & StudyType & "\" & StudyRegion & "\General\"
            SetTree(tvw, DrawingPath)
        End If

    End Sub
    Private Sub PopulateCATreeView(tvw As TreeView)

        If Directory.Exists(ClientAttachmentsPath) Then
            SetTree(tvw, ClientAttachmentsPath)
        End If

    End Sub
    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs)

        LaunchValidation(True)

    End Sub
    Private Sub btnValidate_Click(sender As System.Object, e As System.EventArgs) Handles btnValidate.Click
        If File.Exists(StudyDrive & StudyYear & "\Correspondence\" & RefNum & "\" & RefNum & ".xls") Then
            Process.Start(StudyDrive & StudyYear & "\Correspondence\" & RefNum & "\" & RefNum & ".xls")
        Else
            MessageBox.Show("Validation File Missing for: " & RefNum)
        End If

    End Sub

    Private Sub LaunchValidation(JustLooking As Boolean)
        Try

            bJustLooking = JustLooking
            If cboConsultant.Text = "" Then
                MessageBox.Show("You have not assigned a consultant.")
            Else
                btnValidate.Enabled = False
                btnValidate.Text = "Validating"
                'Launch Validation

                ValidationFile = "FL" & StudyYear.Substring(2, 2) & "Macros.xls"

                Validation(bJustLooking)
                btnValidate.Enabled = True
                btnValidate.Text = "Validate"

            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "LaunchValidation")
            db.WriteLog("LaunchValidation", ex)
        End Try

    End Sub
    Private Sub ChangedStudy(Optional mRefNum As String = "")

        If mRefNum = "" Then
            Select Case cboStudy.Text.Substring(0, 1)
                Case "T"
                    StudyDrive = "k:\study\terminals\"
                    lblCompany.Text = "Terminal"
                    StudyType = "Terminals"
                Case "N"
                    StudyDrive = "k:\study\ng pipeline\"
                    lblCompany.Text = "NG Pipeline"
                    StudyType = "Pipeline"
                Case "P"
                    StudyDrive = "k:\study\liquid pipeline\"
                    lblCompany.Text = "Liquid Pipeline"
                    StudyType = "Pipeline"
            End Select

            For i = 0 To 3
                If StudySave(i) = cboStudy.Text Then
                    GetRefNums(RefNumSave(i))
                    Exit Sub
                End If
            Next

        Else
            GetRefNums(mRefNum)
        End If


    End Sub
    'If Study Changes, then re-populate RefNums
    Private Sub cboStudy_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboStudy.SelectedIndexChanged
        ChangedStudy()
    End Sub

    'Method to populate the labels for Contacts
    Private Sub FillContacts()

        ClearFields()



        lblRefCoName.Text = TPLContact.FullName
        lblRefCoEmail.Text = TPLContact.Email
        lblRefCoPhone.Text = TPLContact.Phone


        lblName.Text = CompanyContact.FirstName & " " & CompanyContact.LastName
        lblEmail.Text = CompanyContact.Email
        lblPhone.Text = CompanyContact.Phone
        txtCompanyPassword.Text = CompanyContact.Password
        CompanyPassword = txtCompanyPassword.Text



        txtReturnPassword.Text = GetReturnPassword(Trim(UCase(RefNum)), "fuelslubes")

        ReturnPassword = txtReturnPassword.Text


    End Sub
    <Obsolete("Been incorrectly implemented, change for 2013 Olefins and 2014 Fuels", False)>
    Private Function GetReturnPassword(RefNum As String, salt As String) As String
        Return Utilities.XOREncryption(Trim(UCase(RefNum)), salt)
    End Function
    Public Sub RefNumChanged(Optional mRefNum As String = "")

        RemoveHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged

        SetTabDirty(True)
        SetPaths()
        cboRefNum.Text = mRefNum
        GetRefNumRecord(mRefNum)
        SaveRefNum(cboStudy.Text, mRefNum)

        AddHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
        If mRefNum.Length > 0 Then RefNum = mRefNum
        'TabControl1.SelectedIndex = 0
    End Sub
    Private Sub cboRefNum_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboRefNum.SelectedIndexChanged
        RefNumChanged(cboRefNum.Text)
    End Sub

    'Private Sub cboConsultant_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboConsultant.SelectedIndexChanged
    'End Sub


    Private Sub btnDirRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnDirRefresh.Click
        BuildDirectoriesTab()
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Dim t As New TimeSpan(0, 0, Time)
        Dim l As New TimeSpan(0, 0, LastTime)
        Dim hours As String
        Dim minutes As String
        Dim seconds As String
        Dim lhours As String
        Dim lminutes As String
        Dim lseconds As String
        hours = t.Hours.ToString()
        minutes = t.Minutes.ToString
        seconds = t.Seconds.ToString
        If hours.Length = 1 Then hours = "0" & hours
        If minutes.Length = 1 Then minutes = "0" & minutes
        If seconds.Length = 1 Then seconds = "0" & seconds
        lhours = l.Hours.ToString()
        lminutes = l.Minutes.ToString
        lseconds = l.Seconds.ToString
        If lhours.Length = 1 Then lhours = "0" & lhours
        If lminutes.Length = 1 Then lminutes = "0" & lminutes
        If lseconds.Length = 1 Then lseconds = "0" & lseconds
        Time += 1
        ConsoleTimer.Text = "Timer : " & hours & ":" & minutes & ":" & seconds & "   Last Time: " & lhours & ":" & lminutes & ":" & lseconds
    End Sub


    Private Sub btnRefreshConsultantTab_Click(sender As System.Object, e As System.EventArgs) Handles btnRefreshConsultantTab.Click
        SetConsultant(cbConsultants.Text)
    End Sub


    Private Sub ConsoleTimer_DoubleClick(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.DoubleClick
        LastTime = Time
        Time = 0
    End Sub

    Private Sub ConsoleTimer_Click_1(sender As System.Object, e As System.EventArgs) Handles ConsoleTimer.Click
        If Timer1.Enabled Then
            ConsoleTimer.ForeColor = Color.DarkRed
            Timer1.Enabled = False
        Else
            ConsoleTimer.ForeColor = Color.Green
            Timer1.Enabled = True
        End If
    End Sub


    Private Sub chkSQL_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSQL.CheckedChanged
        If Not chkSQL.Checked Then
            Label25.Text = "Clippy Query"
        Else
            Label25.Text = "SQL Query"
        End If
    End Sub


    Private Sub btnKillProcesses_Click(sender As System.Object, e As System.EventArgs) Handles btnKillProcesses.Click
        Dim dr As New DialogResult()
        dr = MessageBox.Show("This will kill all OPEN Excel and Word Documents.  Please <SAVE> any Word or Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel)
        If dr = DialogResult.OK Then
            Utilities.KillProcesses("WinWord")
            Utilities.KillProcesses("Excel")
        End If
    End Sub


#End Region


    Private Sub cboConsultant_LostFocus(sender As Object, e As System.EventArgs) Handles cboConsultant.LostFocus
        Dim params As List(Of String)
        cboConsultant.Text = cboConsultant.Text.ToUpper
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Consultant/" & cboConsultant.Text)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)

        ConsoleTabs.SelectTab("tabConsultant")
        cbConsultants.Text = cboConsultant.Text

    End Sub


    Private Sub btnSavePN_Click(sender As System.Object, e As System.EventArgs) Handles btnSavePN.Click
        UpdateNotes()
        lblStatus.Text = "Presenter Notes Saved...."
    End Sub


    Private Sub btnCreatePN_Click(sender As System.Object, e As System.EventArgs) Handles btnCreatePN.Click
        If File.Exists(CorrPath & "PN_" & Company & ".doc") Then
            Process.Start(CorrPath & "PN_" & Company & ".doc")
        Else
            Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will lock the field.", "Wait", MessageBoxButtons.YesNo)
            If dr = System.Windows.Forms.DialogResult.Yes Then
                Try

                    Dim myText As String = "Presenter Notes for" & vbCrLf & Company & vbCrLf & vbCrLf
                    Dim params = New List(Of String)
                    Dim row As DataRow

                    lblStatus.Text = "Creating Presenter's Word file..."
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes", params)

                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            row = ds.Tables(0).Rows(0)
                            myText += row("ValidationNotes").ToString
                        Else
                            myText += ""
                        End If
                    End If
                    Dim strw As New StreamWriter(TempPath & "PN_" & Company & ".txt")

                    strw.WriteLine(myText)
                    strw.Close()
                    strw.Dispose()
                    Dim doc As String = Utilities.ConvertToDoc(TempPath, TempPath & "PN_" & Company & ".txt")
                    File.Move(TempPath & "PN_" & Company & ".doc", CorrPath & "PN_" & Company & ".doc")
                    File.Delete(TempPath & "PN_" & Company & ".txt")
                    lblStatus.Text = "Presenter's Word file created and locked..."
                    txtValidationIssues.Text = "~" & txtValidationIssues.Text
                    UpdatePN()
                    BuildSummaryTab()
                    Process.Start(CorrPath & "PN_" & Company & ".doc")
                    btnCreatePN.Text = "Open PN File"

                Catch ex As System.Exception
                    lblStatus.Text = "Error: " & ex.Message
                End Try
            End If
        End If
    End Sub

    Private Sub btnUnlockPN_Click(sender As System.Object, e As System.EventArgs) Handles btnUnlockPN.Click
        If txtValidationIssues.Text.Substring(0, 1) = "~" Then
            txtValidationIssues.ReadOnly = False
            txtValidationIssues.Text = txtValidationIssues.Text.Substring(1, txtValidationIssues.Text.Length - 1)
            btnCreatePN.Text = "Create PN File"
            UpdatePN()
            lblStatus.Text = "PN File unlocked..."
            btnUnlockPN.Enabled = False
            If File.Exists(CorrPath & "PN_" & Company & ".doc") Then File.Delete(CorrPath & "PN_" & Company & ".doc")
        End If
    End Sub

    Private Sub btnClientTable_Click(sender As System.Object, e As System.EventArgs) Handles btnClientTable.Click
        Process.Start(StudyDrive & StudyYear & "\" & StudyType & "\" & RefNum & "\" & RefNum & "_CT.xls")
    End Sub

    Private Sub tabSummary_Click(sender As System.Object, e As System.EventArgs) Handles tabSummary.Click

    End Sub

    Private Sub btnValidatedInput_Click(sender As System.Object, e As System.EventArgs) Handles btnValidatedInput.Click
        Process.Start(StudyDrive & StudyYear & "\Company\" & GetShortCompanyName(RefNum) & "\" & GetLocationName(RefNum) & ".xls")
    End Sub

    Private Sub btnValDet_Click(sender As System.Object, e As System.EventArgs) Handles btnValDet.Click
        If StudyType.ToUpper = "TERMINALS" Then
            Process.Start(StudyDrive & StudyYear & "\Project\Validation\ValDet_TK.xls")
        Else
            Process.Start(StudyDrive & StudyYear & "\Project\Validation\ValDet_PL.xls")
        End If
    End Sub


End Class
