﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SecureSend
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstFilesToSend = New System.Windows.Forms.ListBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboTemplate = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtResponseDays = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboSendMethod = New System.Windows.Forms.ComboBox()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.fileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCC = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkSaveCopy = New System.Windows.Forms.CheckBox()
        Me.chkPCC = New System.Windows.Forms.CheckBox()
        Me.txtPCC = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lstFilesToSend
        '
        Me.lstFilesToSend.AllowDrop = True
        Me.lstFilesToSend.FormattingEnabled = True
        Me.lstFilesToSend.Location = New System.Drawing.Point(25, 60)
        Me.lstFilesToSend.Name = "lstFilesToSend"
        Me.lstFilesToSend.Size = New System.Drawing.Size(558, 173)
        Me.lstFilesToSend.TabIndex = 1
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(22, 31)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(92, 17)
        Me.Label59.TabIndex = 22
        Me.Label59.Text = "Files to send:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(36, 271)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 17)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Select Template:"
        '
        'cboTemplate
        '
        Me.cboTemplate.FormattingEnabled = True
        Me.cboTemplate.Location = New System.Drawing.Point(239, 271)
        Me.cboTemplate.Name = "cboTemplate"
        Me.cboTemplate.Size = New System.Drawing.Size(348, 21)
        Me.cboTemplate.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(36, 242)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(189, 17)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Working Days for Response:"
        '
        'txtResponseDays
        '
        Me.txtResponseDays.Location = New System.Drawing.Point(239, 245)
        Me.txtResponseDays.MaxLength = 2
        Me.txtResponseDays.Name = "txtResponseDays"
        Me.txtResponseDays.Size = New System.Drawing.Size(32, 20)
        Me.txtResponseDays.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(36, 298)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 17)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Send Method:"
        '
        'cboSendMethod
        '
        Me.cboSendMethod.FormattingEnabled = True
        Me.cboSendMethod.Location = New System.Drawing.Point(239, 298)
        Me.cboSendMethod.Name = "cboSendMethod"
        Me.cboSendMethod.Size = New System.Drawing.Size(348, 21)
        Me.cboSendMethod.TabIndex = 4
        '
        'btnSend
        '
        Me.btnSend.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnSend.Location = New System.Drawing.Point(243, 441)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(150, 23)
        Me.btnSend.TabIndex = 9
        Me.btnSend.Text = "Encrypt Files and Build Email"
        Me.btnSend.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(508, 441)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(36, 350)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(30, 17)
        Me.lblEmail.TabIndex = 40
        Me.lblEmail.Text = "CC:"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(239, 405)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(154, 20)
        Me.txtPassword.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(36, 405)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(136, 17)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "Company Password:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(225, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(164, 29)
        Me.Label7.TabIndex = 44
        Me.Label7.Text = "Secure Send"
        '
        'fileDialog
        '
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(239, 325)
        Me.txtEmail.MaxLength = 80
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(344, 20)
        Me.txtEmail.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(36, 324)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(154, 17)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "Contact Email Address:"
        '
        'txtCC
        '
        Me.txtCC.Location = New System.Drawing.Point(239, 351)
        Me.txtCC.MaxLength = 80
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(344, 20)
        Me.txtCC.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(393, 408)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(169, 26)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "<--Password Copied to Clipboard " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                  after Build Email"
        '
        'chkSaveCopy
        '
        Me.chkSaveCopy.AutoSize = True
        Me.chkSaveCopy.Location = New System.Drawing.Point(427, 21)
        Me.chkSaveCopy.Name = "chkSaveCopy"
        Me.chkSaveCopy.Size = New System.Drawing.Size(171, 17)
        Me.chkSaveCopy.TabIndex = 51
        Me.chkSaveCopy.Text = "Save Copy to Correspondence"
        Me.chkSaveCopy.UseVisualStyleBackColor = True
        Me.chkSaveCopy.Visible = False
        '
        'chkPCC
        '
        Me.chkPCC.AutoSize = True
        Me.chkPCC.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkPCC.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPCC.Location = New System.Drawing.Point(39, 376)
        Me.chkPCC.Name = "chkPCC"
        Me.chkPCC.Size = New System.Drawing.Size(111, 21)
        Me.chkPCC.TabIndex = 52
        Me.chkPCC.Text = "Include PCC?"
        Me.chkPCC.UseVisualStyleBackColor = True
        '
        'txtPCC
        '
        Me.txtPCC.Enabled = False
        Me.txtPCC.Location = New System.Drawing.Point(239, 377)
        Me.txtPCC.MaxLength = 80
        Me.txtPCC.Name = "txtPCC"
        Me.txtPCC.Size = New System.Drawing.Size(344, 20)
        Me.txtPCC.TabIndex = 53
        '
        'SecureSend
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(613, 473)
        Me.Controls.Add(Me.txtPCC)
        Me.Controls.Add(Me.chkPCC)
        Me.Controls.Add(Me.chkSaveCopy)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtCC)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.cboSendMethod)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtResponseDays)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboTemplate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.lstFilesToSend)
        Me.Name = "SecureSend"
        Me.Text = "SecureSend"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstFilesToSend As System.Windows.Forms.ListBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboTemplate As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtResponseDays As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboSendMethod As System.Windows.Forms.ComboBox
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents fileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCC As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkSaveCopy As System.Windows.Forms.CheckBox
    Friend WithEvents chkPCC As System.Windows.Forms.CheckBox
    Friend WithEvents txtPCC As System.Windows.Forms.TextBox
End Class
