﻿
Public Class ConsoleFilesInfo
    Dim _usingMfiles As Boolean
    Dim _files As New List(Of ConsoleFile)
    Dim _benchmarkingParticipant As String
    Dim _refNum As String

    Public Sub New(MFiles As Boolean)
        _usingMfiles = MFiles
    End Sub

    Public ReadOnly Property Files As List(Of ConsoleFile)
        Get
            Return _files
        End Get
        'Set(value As List(Of ConsoleFileInfo))
        '    _files = value
        'End Set
    End Property

    Public Property BenchmarkingParticipant As String
        Get
            Return _benchmarkingParticipant
        End Get
        Set(ByVal value As String)
            _benchmarkingParticipant = value
            Dim refnumber() As String = _benchmarkingParticipant.Split(" - ")
            _refNum = refnumber(0)
        End Set
    End Property

    Public ReadOnly Property RefNum As String
        Get
            Return _refNum
        End Get
    End Property
End Class


