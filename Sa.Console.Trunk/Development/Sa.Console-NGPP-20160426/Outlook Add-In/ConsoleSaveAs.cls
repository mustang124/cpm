VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisOutlookSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit
 'define the constant for saving

Sub SaveToCorr()
Dim CorrPath As String
Dim mypath As String

    Dim RefNum As String
    Dim StudyType As String
    Dim Ref2 As String
    Dim Ref3 As String
    
    RefNum = InputBox("Enter RefNum:", "Console Save-As")
    Ref3 = GetRefNumPart(RefNum, 3)
    Ref2 = GetRefNumPart(RefNum, 2)
    StudyType = GetStudyType(Ref2)
    
    mypath = "K:\STUDY\" & StudyType & "\20" & Ref3 & "\" & Ref2 & "\Correspondence\" & RefNum & "\"
     'the mail we want to process
    Dim objItem As Outlook.MailItem
     'question for saving, use subject to save
    Dim strPrompt As String, strname As String
     'variables for the replacement of illegal characters
    Dim sreplace As String, mychar As Variant, strdate As String
     'put active mail in this object holder
    Set objItem = Outlook.ActiveExplorer.Selection.Item(1)
     'check if it's an email ... need to take a closer look cause
     'gives an error when something else (contact, task) is selected
     'because objItem is defined as a mailitem and code errors out
     'saving does work, if you take care that a mailitem is selected
     'before executing this code
    If objItem.Class = olMail Then
         'check on subject
        If objItem.Subject <> vbNullString Then
            strname = objItem.Subject
        Else
            strname = "No_Subject"
        End If
        strdate = objItem.ReceivedTime
         'define the character that will replace illegal characters
        sreplace = "_"
         'create an array to loop through illegal characters (saves lines)
        For Each mychar In Array("/", "\", ":", "?", Chr(34), "<", ">", "�")
             'do the replacement for each character that's illegal
            strname = Replace(strname, mychar, sreplace)
            strdate = Replace(strdate, mychar, sreplace)
        Next mychar
         'Prompt the user for confirmation
        strPrompt = "Are you sure you want to save the item?"
        If MsgBox(strPrompt, vbYesNo + vbQuestion) = vbYes Then
            objItem.SaveAs mypath & strname & "--" & strdate & ".msg", olMSG
        Else
            MsgBox "You chose not to save."
        End If
    End If
End Sub
