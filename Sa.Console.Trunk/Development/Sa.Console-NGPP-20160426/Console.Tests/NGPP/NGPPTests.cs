﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using sa.Internal.Console.DataObject;
using SA.Console;
using System.Data;
using System.Collections.Generic;
using System.Xml;
using System.Configuration;
using Microsoft.Office.Interop.Excel;

namespace Console.Tests.NGPP
{
    [TestClass]
    public class NGPPTests
    {
        List<string> _studyYears = new List<string>();
        private static string _profilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + @"\";
        private static string _profileConsolePath = _profilePath + @"Console\";
        private static string _profileConsoleTempPath = _profileConsolePath + @"temp\";

        [TestMethod]
        public void CbosTest()
        {
            DataObject db = new DataObject(DataObject.StudyTypes.NGPP, DevConfigHelper.ReadConfig("TestDbId"), DevConfigHelper.ReadConfig("TestDbPassword"));
            db.SQL = "Select distinct StudyYear from dbo.TSort order by StudyYear desc";
            DataSet ds  = db.Execute();
            db.SQL = ""; // string.Empty;
            foreach(DataRow row in ds.Tables[0].Rows)
            {
                _studyYears.Add(row.ToString());
            }
            YearRefnumsAndPlants cbo = new YearRefnumsAndPlants(SA.Console.Main.VerticalType.NGPP,ref db);
            YearRefnumPlantsInfo info = cbo.ChangeYear("2014", ref db, string.Empty);
            Assert.IsTrue(_studyYears.Count == info.StudyYears.Length);
            info = cbo.ChangeRefnum("777PCH14");
            Assert.IsTrue(info.SelectedRefNum.Contains("777PCH14"));
            Assert.IsTrue(info.SelectedStudyYear.Contains("2014"));
            info = cbo.ChangePlant("MOL - Molve");
            Assert.IsTrue(info.SelectedRefNum.Contains("691NGP14"));
            //db.Close();
            
            /*
            ngpp.GetSettingsFile();
            ngpp.SaveLastStudy();
            ngpp.ReadLastStudy();  //_profileConsoleTempPath & "ConsoleSettings\" & StudyType & "LastStudy.txt"
            ngpp._lastStudyAndRefnum = "2014";
            ngpp.SaveCboSettings();
            */
                       
        }

        
        public void ValFax_Build_Test()
        {
            //ValFax_Build(): Can't Unit Test, it has an InputBox
            //object[] args = new object[1];
            //args[0] = "";
            //SA.Console.NgppConsole ngpp = new NgppConsole();
            //PrivateObject privateObject = new PrivateObject(ngpp);
            //privateObject.Invoke("ValFax_Build", args);
        }

        [TestMethod]
        public void IDR_Test()
        {
            string templatePath = ConfigurationManager.AppSettings["NgppIdrTemplatePath"];
            string templateFile = ConfigurationManager.AppSettings["NgppIdrTemplateFile"];
            if (System.IO.File.Exists(templateFile))
            {
                Assert.Inconclusive("Need to have this file: " + templateFile +  "in IDR_Test()");
            }
            object[] args = new object[3];
            args[0] = false;
            args[1] = templatePath;
            args[2] = templateFile;
             DataObject db = new DataObject(DataObject.StudyTypes.NGPP, DevConfigHelper.ReadConfig("TestDbId"), DevConfigHelper.ReadConfig("TestDbPassword"));
           SA.Console.NgppConsole ngpp = new NgppConsole(db);
            PrivateObject privateObject = new PrivateObject(ngpp);
            privateObject.Invoke("IDR", args);
            bool found = false;
            Microsoft.Office.Interop.Excel.Application xl =
                (Microsoft.Office.Interop.Excel.Application)Microsoft.VisualBasic.Interaction.GetObject(null, "Excel.Application");
            Assert.IsTrue(xl != null,"No instances of Excel were opened in IDR_Test()");
            foreach (Workbook wb in xl.Workbooks)
            {
                if(wb.Name==templateFile)
                {
                    found=true;
                    break;
                }
            }
            xl.Quit();
            Assert.IsTrue(found);            
        }

        public void BuildIDREmail_Test()
        {
            //Can't test this, it has msgbox/inputbox
        }
       
        public void MainConsole_DragDrop_Test()
        {
            //Can't test, this requires user interaction
        }

        [TestMethod]
        public void BuildVRFile_test()
        {
            System.IO.File.Delete(_profileConsoleTempPath + "VR9.txt");
            DocsFileSystemAccess fileSys = new DocsFileSystemAccess(Main.VerticalType.NGPP);
            string result =fileSys.BuildVRFile("SFB", "TEST", _profileConsoleTempPath, "9", "777NGP14");
            Assert.IsTrue(result.Contains("has been built"));
            System.IO.File.Delete(_profileConsoleTempPath + "VR9.txt");
        }



    }
}
