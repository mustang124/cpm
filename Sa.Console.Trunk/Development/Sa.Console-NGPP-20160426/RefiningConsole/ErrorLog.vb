﻿Imports Microsoft.Office.Interop.Outlook
Imports SA.Internal.Console.DataObject

Public Class ErrorLog

    Private da As DataObject
    Private mLogType As Integer
    Private dbConn As String
    Private st As String

    Public Sub New(StudyType As String, username As String, password As String, Optional ByRef dataObject As DataObject = Nothing)

        If Not IsNothing(dataObject) Then
            da = dataObject
        Else
            st = StudyType

            Select Case st
                Case "REFINING"
                    da = New DataObject(dataObject.StudyTypes.REFINING, username, password)
                Case "OLEFINS"
                    da = New DataObject(dataObject.StudyTypes.OLEFINS, username, password)
                Case "POWER"
                    da = New DataObject(dataObject.StudyTypes.POWER, username, password)
                Case "RAM"
                    da = New DataObject(dataObject.StudyTypes.RAM, username, password)

            End Select
        End If

    End Sub

    Public Sub New(ByRef dataObject As DataObject, vertical As VerticalType)
        da = dataObject
    End Sub


    Public Sub WriteLog(LogType As String, ex As System.Exception)
        If ConsoleVertical = VerticalType.NGPP Then
            LogType = ConsoleVertical.ToString()
            Try
                'Dim email As Integer = My.Settings.SendErrorEmail
                Dim params As New List(Of String)
                params.Add("UserName/" + Environment.UserName)
                params.Add("LogType/" + LogType)
                Dim msg As String = ex.Message
                If Not IsNothing(ex.InnerException) AndAlso Not IsNothing(ex.InnerException.Message) Then
                    msg += "|" + ex.InnerException.Message
                End If
                params.Add("Message/" + msg)
                da.ExecuteStoredProc("Console.WriteLog", params)
            Catch exNgpp As System.Exception
                Dim exmsg As String = exNgpp.Message
            End Try
        Else
            If st <> "RAM" Then
                Try
                    Dim email As Integer = My.Settings.SendErrorEmail
                    Dim params As New List(Of String)
                    params.Add(("UserName/" + Environment.UserName).Substring(0, 19))
                    params.Add(("LogType/" + LogType).Substring(0, 19))
                    params.Add("Message/" + ex.Message.ToString)
                    da.ExecuteNonQuery("Console.WriteLog", params)
                    If email = 1 Then
                        Dim body As String = "Method: " & LogType & vbCrLf & vbCrLf & "Error: " & ex.Message
                        'Utilities.SendEmail(My.Settings.LogEmail, "", st & " Console Error", body, Nothing, False)
                    End If
                Catch
                End Try
            End If
        End If
    End Sub

End Class
