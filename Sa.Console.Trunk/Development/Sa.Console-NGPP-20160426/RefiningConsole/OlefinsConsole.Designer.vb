﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OlefinsConsole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OlefinsConsole))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.txtVersion = New System.Windows.Forms.TextBox()
        Me.VersionToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnHYC = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btnKillProcesses = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.btnBug = New System.Windows.Forms.Button()
        Me.tabClippy = New System.Windows.Forms.TabPage()
        Me.btnQueryQuit = New System.Windows.Forms.Button()
        Me.lblError = New System.Windows.Forms.Label()
        Me.chkSQL = New System.Windows.Forms.CheckBox()
        Me.dgClippyResults = New System.Windows.Forms.DataGridView()
        Me.btnClippySearch = New System.Windows.Forms.Button()
        Me.txtClippySearch = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.tabDD = New System.Windows.Forms.TabPage()
        Me.optCompanyCorr = New System.Windows.Forms.CheckBox()
        Me.txtMessageFilename = New System.Windows.Forms.TextBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnGVClear = New System.Windows.Forms.Button()
        Me.btnFilesSave = New System.Windows.Forms.Button()
        Me.dgFiles = New System.Windows.Forms.DataGridView()
        Me.OriginalFilename = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Filenames = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FileDestination = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.tabSS = New System.Windows.Forms.TabPage()
        Me.btnDirRefresh = New System.Windows.Forms.Button()
        Me.btnSecureSend = New System.Windows.Forms.Button()
        Me.tvwCompCorr2 = New System.Windows.Forms.TreeView()
        Me.tvwCompCorr = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence2 = New System.Windows.Forms.TreeView()
        Me.tvwDrawings2 = New System.Windows.Forms.TreeView()
        Me.tvwClientAttachments2 = New System.Windows.Forms.TreeView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboDir2 = New System.Windows.Forms.ComboBox()
        Me.tvwClientAttachments = New System.Windows.Forms.TreeView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDir = New System.Windows.Forms.ComboBox()
        Me.tvwDrawings = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence = New System.Windows.Forms.TreeView()
        Me.tabTimeGrade = New System.Windows.Forms.TabPage()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgHistory = New System.Windows.Forms.DataGridView()
        Me.dgSummary = New System.Windows.Forms.DataGridView()
        Me.btnGradeExit = New System.Windows.Forms.Button()
        Me.btnSection = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgGrade = New System.Windows.Forms.DataGridView()
        Me.tabCorr = New System.Windows.Forms.TabPage()
        Me.btnBuildVRFile = New System.Windows.Forms.Button()
        Me.btnReceiptAck = New System.Windows.Forms.Button()
        Me.lstReturnFiles = New System.Windows.Forms.ListBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.lstVRFiles = New System.Windows.Forms.ListBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.lstVFFiles = New System.Windows.Forms.ListBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.lstVFNumbers = New System.Windows.Forms.ListBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.tabCheckList = New System.Windows.Forms.TabPage()
        Me.tvIssues = New System.Windows.Forms.TreeView()
        Me.txtIssueName = New System.Windows.Forms.TextBox()
        Me.txtIssueID = New System.Windows.Forms.TextBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.btnCancelIssue = New System.Windows.Forms.Button()
        Me.btnUpdateIssue = New System.Windows.Forms.Button()
        Me.lblItemCount = New System.Windows.Forms.Label()
        Me.btnAddIssue = New System.Windows.Forms.Button()
        Me.cboCheckListView = New System.Windows.Forms.ComboBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.ValCheckList = New System.Windows.Forms.CheckedListBox()
        Me.tabNotes = New System.Windows.Forms.TabPage()
        Me.cboNotes = New System.Windows.Forms.ComboBox()
        Me.btnUnlockPN = New System.Windows.Forms.Button()
        Me.btnCreatePN = New System.Windows.Forms.Button()
        Me.btnSavePN = New System.Windows.Forms.Button()
        Me.txtNotes = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.tabContacts = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.pnlCompany = New System.Windows.Forms.Panel()
        Me.lblACPhone = New System.Windows.Forms.Label()
        Me.lblAltEmail = New System.Windows.Forms.Label()
        Me.lblAltName = New System.Windows.Forms.Label()
        Me.lblCCPhone = New System.Windows.Forms.Label()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.btnShowAltCo = New System.Windows.Forms.Button()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.ACEmail2 = New System.Windows.Forms.PictureBox()
        Me.CCEmail1 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.MainPanel = New System.Windows.Forms.Panel()
        Me.pnlRefinery = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblDataEmail = New System.Windows.Forms.Label()
        Me.lblDataCoordinator = New System.Windows.Forms.Label()
        Me.lblPricingEmail = New System.Windows.Forms.Label()
        Me.lblPricingName = New System.Windows.Forms.Label()
        Me.lblPCPhone = New System.Windows.Forms.Label()
        Me.lblPCEmail = New System.Windows.Forms.Label()
        Me.lblPCName = New System.Windows.Forms.Label()
        Me.RMEmail = New System.Windows.Forms.PictureBox()
        Me.RACEmail = New System.Windows.Forms.PictureBox()
        Me.RCEmail = New System.Windows.Forms.PictureBox()
        Me.ConsoleTabs = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnRAM = New System.Windows.Forms.Button()
        Me.btnVI = New System.Windows.Forms.Button()
        Me.btnSC = New System.Windows.Forms.Button()
        Me.btnPA = New System.Windows.Forms.Button()
        Me.btnCT = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnDrawings = New System.Windows.Forms.Button()
        Me.btnPT = New System.Windows.Forms.Button()
        Me.btnSpecFrac = New System.Windows.Forms.Button()
        Me.btnUnitReview = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lstFiles = New System.Windows.Forms.ListBox()
        Me.dgCorr = New System.Windows.Forms.DataGridView()
        Me.Cycle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValFaxes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValReply = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReturnFiles = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinWorked = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consultant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblOSIMUpload = New System.Windows.Forms.Label()
        Me.lblOU = New System.Windows.Forms.Label()
        Me.lblCalcUpload = New System.Windows.Forms.Label()
        Me.lblCU = New System.Windows.Forms.Label()
        Me.lblItemsRemaining = New System.Windows.Forms.Label()
        Me.lblBlueFlags = New System.Windows.Forms.Label()
        Me.lblRedFlags = New System.Windows.Forms.Label()
        Me.lblLastCalcDate = New System.Windows.Forms.Label()
        Me.lblLastUpload = New System.Windows.Forms.Label()
        Me.lblLastFileSave = New System.Windows.Forms.Label()
        Me.lblValidationStatus = New System.Windows.Forms.Label()
        Me.lblLU = New System.Windows.Forms.Label()
        Me.lblLFS = New System.Windows.Forms.Label()
        Me.lblLCD = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btnSS = New System.Windows.Forms.Button()
        Me.btnValidate = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btnReturnPW = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnCompanyPW = New System.Windows.Forms.Button()
        Me.btnMain = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.lblCompany = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboConsultant = New System.Windows.Forms.ComboBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cboRefNum = New System.Windows.Forms.ComboBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.cboStudy = New System.Windows.Forms.ComboBox()
        Me.Help = New System.Windows.Forms.HelpProvider()
        Me.tabClippy.SuspendLayout()
        CType(Me.dgClippyResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDD.SuspendLayout()
        CType(Me.dgFiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSS.SuspendLayout()
        Me.tabTimeGrade.SuspendLayout()
        CType(Me.dgHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCorr.SuspendLayout()
        Me.tabCheckList.SuspendLayout()
        Me.tabNotes.SuspendLayout()
        Me.tabContacts.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlCompany.SuspendLayout()
        CType(Me.ACEmail2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CCEmail1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainPanel.SuspendLayout()
        Me.pnlRefinery.SuspendLayout()
        CType(Me.RMEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RACEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RCEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConsoleTabs.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgCorr, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnHelp
        '
        Me.btnHelp.Location = New System.Drawing.Point(441, 5)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(27, 23)
        Me.btnHelp.TabIndex = 32
        Me.btnHelp.Text = "&?"
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.SystemColors.Control
        Me.txtVersion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersion.Location = New System.Drawing.Point(888, 705)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.ReadOnly = True
        Me.txtVersion.Size = New System.Drawing.Size(100, 15)
        Me.txtVersion.TabIndex = 39
        Me.VersionToolTip.SetToolTip(Me.txtVersion, "This is the new version")
        '
        'btnHYC
        '
        Me.btnHYC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnHYC.Enabled = False
        Me.btnHYC.Location = New System.Drawing.Point(852, 191)
        Me.btnHYC.Margin = New System.Windows.Forms.Padding(1)
        Me.btnHYC.Name = "btnHYC"
        Me.btnHYC.Size = New System.Drawing.Size(109, 30)
        Me.btnHYC.TabIndex = 92
        Me.btnHYC.Text = "HC Details"
        Me.VersionToolTip.SetToolTip(Me.btnHYC, "Hydrocracker Details")
        Me.btnHYC.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'btnKillProcesses
        '
        Me.btnKillProcesses.Location = New System.Drawing.Point(474, 5)
        Me.btnKillProcesses.Name = "btnKillProcesses"
        Me.btnKillProcesses.Size = New System.Drawing.Size(27, 23)
        Me.btnKillProcesses.TabIndex = 42
        Me.btnKillProcesses.Text = "X"
        Me.btnKillProcesses.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Black
        Me.lblStatus.Location = New System.Drawing.Point(9, 702)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(47, 15)
        Me.lblStatus.TabIndex = 44
        Me.lblStatus.Text = "Ready"
        '
        'btnBug
        '
        Me.btnBug.Enabled = False
        Me.btnBug.Image = CType(resources.GetObject("btnBug.Image"), System.Drawing.Image)
        Me.btnBug.Location = New System.Drawing.Point(408, 5)
        Me.btnBug.Name = "btnBug"
        Me.btnBug.Size = New System.Drawing.Size(27, 23)
        Me.btnBug.TabIndex = 46
        Me.btnBug.UseVisualStyleBackColor = True
        '
        'tabClippy
        '
        Me.tabClippy.Controls.Add(Me.btnQueryQuit)
        Me.tabClippy.Controls.Add(Me.lblError)
        Me.tabClippy.Controls.Add(Me.chkSQL)
        Me.tabClippy.Controls.Add(Me.dgClippyResults)
        Me.tabClippy.Controls.Add(Me.btnClippySearch)
        Me.tabClippy.Controls.Add(Me.txtClippySearch)
        Me.tabClippy.Controls.Add(Me.Label25)
        Me.tabClippy.Location = New System.Drawing.Point(4, 22)
        Me.tabClippy.Name = "tabClippy"
        Me.tabClippy.Size = New System.Drawing.Size(972, 458)
        Me.tabClippy.TabIndex = 11
        Me.tabClippy.Text = "Query"
        Me.tabClippy.UseVisualStyleBackColor = True
        '
        'btnQueryQuit
        '
        Me.btnQueryQuit.Location = New System.Drawing.Point(870, 438)
        Me.btnQueryQuit.Name = "btnQueryQuit"
        Me.btnQueryQuit.Size = New System.Drawing.Size(69, 23)
        Me.btnQueryQuit.TabIndex = 46
        Me.btnQueryQuit.Text = "Exit"
        Me.btnQueryQuit.UseVisualStyleBackColor = True
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblError.ForeColor = System.Drawing.Color.DarkRed
        Me.lblError.Location = New System.Drawing.Point(0, 445)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 13)
        Me.lblError.TabIndex = 45
        '
        'chkSQL
        '
        Me.chkSQL.AutoSize = True
        Me.chkSQL.Checked = True
        Me.chkSQL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSQL.Location = New System.Drawing.Point(111, 133)
        Me.chkSQL.Name = "chkSQL"
        Me.chkSQL.Size = New System.Drawing.Size(47, 17)
        Me.chkSQL.TabIndex = 44
        Me.chkSQL.Text = "SQL"
        Me.chkSQL.UseVisualStyleBackColor = True
        '
        'dgClippyResults
        '
        Me.dgClippyResults.AllowUserToAddRows = False
        Me.dgClippyResults.AllowUserToDeleteRows = False
        Me.dgClippyResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgClippyResults.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgClippyResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgClippyResults.Location = New System.Drawing.Point(21, 158)
        Me.dgClippyResults.Name = "dgClippyResults"
        Me.dgClippyResults.ReadOnly = True
        Me.dgClippyResults.Size = New System.Drawing.Size(918, 274)
        Me.dgClippyResults.TabIndex = 43
        '
        'btnClippySearch
        '
        Me.btnClippySearch.Location = New System.Drawing.Point(21, 129)
        Me.btnClippySearch.Name = "btnClippySearch"
        Me.btnClippySearch.Size = New System.Drawing.Size(69, 23)
        Me.btnClippySearch.TabIndex = 42
        Me.btnClippySearch.Text = "Search"
        Me.btnClippySearch.UseVisualStyleBackColor = True
        '
        'txtClippySearch
        '
        Me.txtClippySearch.Location = New System.Drawing.Point(21, 34)
        Me.txtClippySearch.Multiline = True
        Me.txtClippySearch.Name = "txtClippySearch"
        Me.txtClippySearch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtClippySearch.Size = New System.Drawing.Size(918, 89)
        Me.txtClippySearch.TabIndex = 39
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(8, 11)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(83, 17)
        Me.Label25.TabIndex = 38
        Me.Label25.Text = "SQL Query:"
        '
        'tabDD
        '
        Me.tabDD.BackColor = System.Drawing.Color.AliceBlue
        Me.tabDD.Controls.Add(Me.optCompanyCorr)
        Me.tabDD.Controls.Add(Me.txtMessageFilename)
        Me.tabDD.Controls.Add(Me.lblMessage)
        Me.tabDD.Controls.Add(Me.btnGVClear)
        Me.tabDD.Controls.Add(Me.btnFilesSave)
        Me.tabDD.Controls.Add(Me.dgFiles)
        Me.tabDD.Location = New System.Drawing.Point(4, 22)
        Me.tabDD.Name = "tabDD"
        Me.tabDD.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDD.Size = New System.Drawing.Size(972, 458)
        Me.tabDD.TabIndex = 8
        Me.tabDD.Text = "Drag and Drop"
        '
        'optCompanyCorr
        '
        Me.optCompanyCorr.AutoSize = True
        Me.optCompanyCorr.Location = New System.Drawing.Point(788, 32)
        Me.optCompanyCorr.Name = "optCompanyCorr"
        Me.optCompanyCorr.Size = New System.Drawing.Size(151, 17)
        Me.optCompanyCorr.TabIndex = 38
        Me.optCompanyCorr.Text = "Company Correspondence"
        Me.optCompanyCorr.UseVisualStyleBackColor = True
        '
        'txtMessageFilename
        '
        Me.txtMessageFilename.Location = New System.Drawing.Point(209, 29)
        Me.txtMessageFilename.Name = "txtMessageFilename"
        Me.txtMessageFilename.Size = New System.Drawing.Size(557, 20)
        Me.txtMessageFilename.TabIndex = 37
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(20, 29)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(183, 17)
        Me.lblMessage.TabIndex = 36
        Me.lblMessage.Text = "Filename for Email Text:"
        '
        'btnGVClear
        '
        Me.btnGVClear.Location = New System.Drawing.Point(779, 438)
        Me.btnGVClear.Name = "btnGVClear"
        Me.btnGVClear.Size = New System.Drawing.Size(75, 23)
        Me.btnGVClear.TabIndex = 2
        Me.btnGVClear.Text = "Clear"
        Me.btnGVClear.UseVisualStyleBackColor = True
        '
        'btnFilesSave
        '
        Me.btnFilesSave.Location = New System.Drawing.Point(872, 438)
        Me.btnFilesSave.Name = "btnFilesSave"
        Me.btnFilesSave.Size = New System.Drawing.Size(75, 23)
        Me.btnFilesSave.TabIndex = 1
        Me.btnFilesSave.Text = "Save"
        Me.btnFilesSave.UseVisualStyleBackColor = True
        '
        'dgFiles
        '
        Me.dgFiles.AllowUserToAddRows = False
        Me.dgFiles.AllowUserToDeleteRows = False
        Me.dgFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgFiles.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OriginalFilename, Me.Filenames, Me.FileDestination})
        Me.dgFiles.Location = New System.Drawing.Point(23, 63)
        Me.dgFiles.Name = "dgFiles"
        Me.dgFiles.Size = New System.Drawing.Size(924, 359)
        Me.dgFiles.TabIndex = 0
        '
        'OriginalFilename
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.OriginalFilename.DefaultCellStyle = DataGridViewCellStyle1
        Me.OriginalFilename.HeaderText = "Original Filename"
        Me.OriginalFilename.Name = "OriginalFilename"
        Me.OriginalFilename.ReadOnly = True
        Me.OriginalFilename.Width = 300
        '
        'Filenames
        '
        Me.Filenames.HeaderText = """Save As"" Filename"
        Me.Filenames.Name = "Filenames"
        Me.Filenames.Width = 300
        '
        'FileDestination
        '
        Me.FileDestination.DropDownWidth = 200
        Me.FileDestination.HeaderText = "Save Attachment Location"
        Me.FileDestination.Name = "FileDestination"
        Me.FileDestination.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.FileDestination.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.FileDestination.Width = 300
        '
        'tabSS
        '
        Me.tabSS.BackColor = System.Drawing.Color.AliceBlue
        Me.tabSS.Controls.Add(Me.btnDirRefresh)
        Me.tabSS.Controls.Add(Me.btnSecureSend)
        Me.tabSS.Controls.Add(Me.tvwCompCorr2)
        Me.tabSS.Controls.Add(Me.tvwCompCorr)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence2)
        Me.tabSS.Controls.Add(Me.tvwDrawings2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments2)
        Me.tabSS.Controls.Add(Me.Label13)
        Me.tabSS.Controls.Add(Me.cboDir2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments)
        Me.tabSS.Controls.Add(Me.Label12)
        Me.tabSS.Controls.Add(Me.cboDir)
        Me.tabSS.Controls.Add(Me.tvwDrawings)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence)
        Me.tabSS.Location = New System.Drawing.Point(4, 22)
        Me.tabSS.Name = "tabSS"
        Me.tabSS.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSS.Size = New System.Drawing.Size(972, 458)
        Me.tabSS.TabIndex = 7
        Me.tabSS.Text = "Secure Send"
        '
        'btnDirRefresh
        '
        Me.btnDirRefresh.Location = New System.Drawing.Point(866, 428)
        Me.btnDirRefresh.Name = "btnDirRefresh"
        Me.btnDirRefresh.Size = New System.Drawing.Size(84, 23)
        Me.btnDirRefresh.TabIndex = 50
        Me.btnDirRefresh.Text = "Exit"
        Me.btnDirRefresh.UseVisualStyleBackColor = True
        '
        'btnSecureSend
        '
        Me.btnSecureSend.Location = New System.Drawing.Point(776, 428)
        Me.btnSecureSend.Name = "btnSecureSend"
        Me.btnSecureSend.Size = New System.Drawing.Size(84, 23)
        Me.btnSecureSend.TabIndex = 49
        Me.btnSecureSend.Text = "Secure Send"
        Me.btnSecureSend.UseVisualStyleBackColor = True
        '
        'tvwCompCorr2
        '
        Me.tvwCompCorr2.AllowDrop = True
        Me.tvwCompCorr2.CheckBoxes = True
        Me.tvwCompCorr2.ImageIndex = 0
        Me.tvwCompCorr2.ImageList = Me.ImageList1
        Me.tvwCompCorr2.Location = New System.Drawing.Point(489, 35)
        Me.tvwCompCorr2.Name = "tvwCompCorr2"
        Me.tvwCompCorr2.SelectedImageIndex = 0
        Me.tvwCompCorr2.Size = New System.Drawing.Size(461, 386)
        Me.tvwCompCorr2.TabIndex = 48
        Me.tvwCompCorr2.Visible = False
        '
        'tvwCompCorr
        '
        Me.tvwCompCorr.AllowDrop = True
        Me.tvwCompCorr.CheckBoxes = True
        Me.tvwCompCorr.ImageIndex = 0
        Me.tvwCompCorr.ImageList = Me.ImageList1
        Me.tvwCompCorr.Location = New System.Drawing.Point(17, 35)
        Me.tvwCompCorr.Name = "tvwCompCorr"
        Me.tvwCompCorr.SelectedImageIndex = 0
        Me.tvwCompCorr.Size = New System.Drawing.Size(466, 386)
        Me.tvwCompCorr.TabIndex = 47
        Me.tvwCompCorr.Visible = False
        '
        'tvwCorrespondence2
        '
        Me.tvwCorrespondence2.AllowDrop = True
        Me.tvwCorrespondence2.CheckBoxes = True
        Me.tvwCorrespondence2.ImageIndex = 0
        Me.tvwCorrespondence2.ImageList = Me.ImageList1
        Me.tvwCorrespondence2.Location = New System.Drawing.Point(489, 36)
        Me.tvwCorrespondence2.Name = "tvwCorrespondence2"
        Me.tvwCorrespondence2.SelectedImageIndex = 0
        Me.tvwCorrespondence2.Size = New System.Drawing.Size(461, 385)
        Me.tvwCorrespondence2.TabIndex = 46
        '
        'tvwDrawings2
        '
        Me.tvwDrawings2.AllowDrop = True
        Me.tvwDrawings2.CheckBoxes = True
        Me.tvwDrawings2.ImageIndex = 0
        Me.tvwDrawings2.ImageList = Me.ImageList1
        Me.tvwDrawings2.Location = New System.Drawing.Point(489, 35)
        Me.tvwDrawings2.Name = "tvwDrawings2"
        Me.tvwDrawings2.SelectedImageIndex = 0
        Me.tvwDrawings2.Size = New System.Drawing.Size(461, 386)
        Me.tvwDrawings2.TabIndex = 45
        Me.tvwDrawings2.Visible = False
        '
        'tvwClientAttachments2
        '
        Me.tvwClientAttachments2.AllowDrop = True
        Me.tvwClientAttachments2.CheckBoxes = True
        Me.tvwClientAttachments2.ImageIndex = 0
        Me.tvwClientAttachments2.ImageList = Me.ImageList1
        Me.tvwClientAttachments2.Location = New System.Drawing.Point(489, 35)
        Me.tvwClientAttachments2.Name = "tvwClientAttachments2"
        Me.tvwClientAttachments2.SelectedImageIndex = 0
        Me.tvwClientAttachments2.Size = New System.Drawing.Size(461, 386)
        Me.tvwClientAttachments2.TabIndex = 43
        Me.tvwClientAttachments2.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(482, 10)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 17)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Directory:"
        '
        'cboDir2
        '
        Me.cboDir2.FormattingEnabled = True
        Me.cboDir2.Location = New System.Drawing.Point(557, 8)
        Me.cboDir2.Name = "cboDir2"
        Me.cboDir2.Size = New System.Drawing.Size(164, 21)
        Me.cboDir2.TabIndex = 41
        '
        'tvwClientAttachments
        '
        Me.tvwClientAttachments.AllowDrop = True
        Me.tvwClientAttachments.CheckBoxes = True
        Me.tvwClientAttachments.ImageIndex = 0
        Me.tvwClientAttachments.ImageList = Me.ImageList1
        Me.tvwClientAttachments.Location = New System.Drawing.Point(17, 35)
        Me.tvwClientAttachments.Name = "tvwClientAttachments"
        Me.tvwClientAttachments.SelectedImageIndex = 0
        Me.tvwClientAttachments.Size = New System.Drawing.Size(466, 385)
        Me.tvwClientAttachments.TabIndex = 40
        Me.tvwClientAttachments.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(14, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 17)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "Directory:"
        '
        'cboDir
        '
        Me.cboDir.FormattingEnabled = True
        Me.cboDir.Location = New System.Drawing.Point(89, 7)
        Me.cboDir.Name = "cboDir"
        Me.cboDir.Size = New System.Drawing.Size(164, 21)
        Me.cboDir.TabIndex = 38
        '
        'tvwDrawings
        '
        Me.tvwDrawings.AllowDrop = True
        Me.tvwDrawings.CheckBoxes = True
        Me.tvwDrawings.ImageIndex = 0
        Me.tvwDrawings.ImageList = Me.ImageList1
        Me.tvwDrawings.Location = New System.Drawing.Point(17, 34)
        Me.tvwDrawings.Name = "tvwDrawings"
        Me.tvwDrawings.SelectedImageIndex = 0
        Me.tvwDrawings.Size = New System.Drawing.Size(466, 386)
        Me.tvwDrawings.TabIndex = 36
        Me.tvwDrawings.Visible = False
        '
        'tvwCorrespondence
        '
        Me.tvwCorrespondence.AllowDrop = True
        Me.tvwCorrespondence.CheckBoxes = True
        Me.tvwCorrespondence.ImageIndex = 0
        Me.tvwCorrespondence.ImageList = Me.ImageList1
        Me.tvwCorrespondence.Location = New System.Drawing.Point(17, 35)
        Me.tvwCorrespondence.Name = "tvwCorrespondence"
        Me.tvwCorrespondence.SelectedImageIndex = 0
        Me.tvwCorrespondence.Size = New System.Drawing.Size(466, 385)
        Me.tvwCorrespondence.TabIndex = 0
        '
        'tabTimeGrade
        '
        Me.tabTimeGrade.BackColor = System.Drawing.Color.AliceBlue
        Me.tabTimeGrade.Controls.Add(Me.Label15)
        Me.tabTimeGrade.Controls.Add(Me.Label10)
        Me.tabTimeGrade.Controls.Add(Me.dgHistory)
        Me.tabTimeGrade.Controls.Add(Me.dgSummary)
        Me.tabTimeGrade.Controls.Add(Me.btnGradeExit)
        Me.tabTimeGrade.Controls.Add(Me.btnSection)
        Me.tabTimeGrade.Controls.Add(Me.Label9)
        Me.tabTimeGrade.Controls.Add(Me.dgGrade)
        Me.tabTimeGrade.Location = New System.Drawing.Point(4, 22)
        Me.tabTimeGrade.Name = "tabTimeGrade"
        Me.tabTimeGrade.Padding = New System.Windows.Forms.Padding(3)
        Me.tabTimeGrade.Size = New System.Drawing.Size(972, 458)
        Me.tabTimeGrade.TabIndex = 5
        Me.tabTimeGrade.Text = "Time/Grade"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(492, 239)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(69, 17)
        Me.Label15.TabIndex = 54
        Me.Label15.Text = "HISTORY"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(20, 239)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 17)
        Me.Label10.TabIndex = 53
        Me.Label10.Text = "SUMMARY"
        '
        'dgHistory
        '
        Me.dgHistory.AllowUserToAddRows = False
        Me.dgHistory.AllowUserToDeleteRows = False
        Me.dgHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgHistory.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgHistory.Location = New System.Drawing.Point(489, 259)
        Me.dgHistory.Name = "dgHistory"
        Me.dgHistory.ReadOnly = True
        Me.dgHistory.Size = New System.Drawing.Size(460, 170)
        Me.dgHistory.TabIndex = 52
        '
        'dgSummary
        '
        Me.dgSummary.AllowUserToAddRows = False
        Me.dgSummary.AllowUserToDeleteRows = False
        Me.dgSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgSummary.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgSummary.Location = New System.Drawing.Point(23, 259)
        Me.dgSummary.Name = "dgSummary"
        Me.dgSummary.ReadOnly = True
        Me.dgSummary.Size = New System.Drawing.Size(460, 170)
        Me.dgSummary.TabIndex = 51
        '
        'btnGradeExit
        '
        Me.btnGradeExit.Location = New System.Drawing.Point(865, 438)
        Me.btnGradeExit.Name = "btnGradeExit"
        Me.btnGradeExit.Size = New System.Drawing.Size(84, 23)
        Me.btnGradeExit.TabIndex = 50
        Me.btnGradeExit.Text = "Exit"
        Me.btnGradeExit.UseVisualStyleBackColor = True
        '
        'btnSection
        '
        Me.btnSection.Location = New System.Drawing.Point(23, 6)
        Me.btnSection.Name = "btnSection"
        Me.btnSection.Size = New System.Drawing.Size(84, 23)
        Me.btnSection.TabIndex = 39
        Me.btnSection.Text = "Section"
        Me.btnSection.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(422, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 17)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "GRADING"
        '
        'dgGrade
        '
        Me.dgGrade.AllowUserToAddRows = False
        Me.dgGrade.AllowUserToDeleteRows = False
        Me.dgGrade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgGrade.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgGrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgGrade.Location = New System.Drawing.Point(23, 32)
        Me.dgGrade.Name = "dgGrade"
        Me.dgGrade.ReadOnly = True
        Me.dgGrade.Size = New System.Drawing.Size(926, 200)
        Me.dgGrade.TabIndex = 0
        '
        'tabCorr
        '
        Me.tabCorr.BackColor = System.Drawing.Color.AliceBlue
        Me.tabCorr.Controls.Add(Me.btnBuildVRFile)
        Me.tabCorr.Controls.Add(Me.btnReceiptAck)
        Me.tabCorr.Controls.Add(Me.lstReturnFiles)
        Me.tabCorr.Controls.Add(Me.Label58)
        Me.tabCorr.Controls.Add(Me.lstVRFiles)
        Me.tabCorr.Controls.Add(Me.Label57)
        Me.tabCorr.Controls.Add(Me.lstVFFiles)
        Me.tabCorr.Controls.Add(Me.Label56)
        Me.tabCorr.Controls.Add(Me.lstVFNumbers)
        Me.tabCorr.Controls.Add(Me.Label55)
        Me.tabCorr.Location = New System.Drawing.Point(4, 22)
        Me.tabCorr.Name = "tabCorr"
        Me.tabCorr.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCorr.Size = New System.Drawing.Size(972, 458)
        Me.tabCorr.TabIndex = 4
        Me.tabCorr.Text = "Correspondence"
        '
        'btnBuildVRFile
        '
        Me.btnBuildVRFile.Location = New System.Drawing.Point(198, 178)
        Me.btnBuildVRFile.Name = "btnBuildVRFile"
        Me.btnBuildVRFile.Size = New System.Drawing.Size(122, 23)
        Me.btnBuildVRFile.TabIndex = 46
        Me.btnBuildVRFile.Text = "Build a VR File"
        Me.btnBuildVRFile.UseVisualStyleBackColor = True
        Me.btnBuildVRFile.Visible = False
        '
        'btnReceiptAck
        '
        Me.btnReceiptAck.Location = New System.Drawing.Point(65, 178)
        Me.btnReceiptAck.Name = "btnReceiptAck"
        Me.btnReceiptAck.Size = New System.Drawing.Size(127, 23)
        Me.btnReceiptAck.TabIndex = 8
        Me.btnReceiptAck.Text = "Receipt Acknowledged"
        Me.btnReceiptAck.UseVisualStyleBackColor = True
        '
        'lstReturnFiles
        '
        Me.lstReturnFiles.ColumnWidth = 300
        Me.lstReturnFiles.FormattingEnabled = True
        Me.lstReturnFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstReturnFiles.Location = New System.Drawing.Point(64, 375)
        Me.lstReturnFiles.Name = "lstReturnFiles"
        Me.lstReturnFiles.Size = New System.Drawing.Size(758, 82)
        Me.lstReturnFiles.TabIndex = 7
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(62, 356)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(63, 13)
        Me.Label58.TabIndex = 6
        Me.Label58.Text = "Return Files"
        '
        'lstVRFiles
        '
        Me.lstVRFiles.ColumnWidth = 300
        Me.lstVRFiles.FormattingEnabled = True
        Me.lstVRFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVRFiles.Location = New System.Drawing.Point(64, 236)
        Me.lstVRFiles.Name = "lstVRFiles"
        Me.lstVRFiles.Size = New System.Drawing.Size(758, 108)
        Me.lstVRFiles.TabIndex = 5
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(62, 216)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(142, 13)
        Me.Label57.TabIndex = 4
        Me.Label57.Text = "Reply Files (VR*.*)"
        '
        'lstVFFiles
        '
        Me.lstVFFiles.ColumnWidth = 300
        Me.lstVFFiles.FormattingEnabled = True
        Me.lstVFFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVFFiles.Location = New System.Drawing.Point(62, 25)
        Me.lstVFFiles.Name = "lstVFFiles"
        Me.lstVFFiles.Size = New System.Drawing.Size(758, 147)
        Me.lstVFFiles.TabIndex = 3
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(59, 7)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(130, 13)
        Me.Label56.TabIndex = 2
        Me.Label56.Text = "Fax Files (VF*.*)"
        '
        'lstVFNumbers
        '
        Me.lstVFNumbers.FormattingEnabled = True
        Me.lstVFNumbers.Location = New System.Drawing.Point(12, 24)
        Me.lstVFNumbers.Name = "lstVFNumbers"
        Me.lstVFNumbers.Size = New System.Drawing.Size(41, 433)
        Me.lstVFNumbers.Sorted = True
        Me.lstVFNumbers.TabIndex = 1
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(9, 7)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(30, 13)
        Me.Label55.TabIndex = 0
        Me.Label55.Text = "VF#"
        '
        'tabCheckList
        '
        Me.tabCheckList.BackColor = System.Drawing.Color.AliceBlue
        Me.tabCheckList.Controls.Add(Me.tvIssues)
        Me.tabCheckList.Controls.Add(Me.txtIssueName)
        Me.tabCheckList.Controls.Add(Me.txtIssueID)
        Me.tabCheckList.Controls.Add(Me.txtDescription)
        Me.tabCheckList.Controls.Add(Me.btnCancelIssue)
        Me.tabCheckList.Controls.Add(Me.btnUpdateIssue)
        Me.tabCheckList.Controls.Add(Me.lblItemCount)
        Me.tabCheckList.Controls.Add(Me.btnAddIssue)
        Me.tabCheckList.Controls.Add(Me.cboCheckListView)
        Me.tabCheckList.Controls.Add(Me.Label52)
        Me.tabCheckList.Controls.Add(Me.Label49)
        Me.tabCheckList.Controls.Add(Me.Label48)
        Me.tabCheckList.Controls.Add(Me.Label47)
        Me.tabCheckList.Controls.Add(Me.ValCheckList)
        Me.tabCheckList.Location = New System.Drawing.Point(4, 22)
        Me.tabCheckList.Name = "tabCheckList"
        Me.tabCheckList.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCheckList.Size = New System.Drawing.Size(972, 458)
        Me.tabCheckList.TabIndex = 3
        Me.tabCheckList.Text = "Checklist"
        '
        'tvIssues
        '
        Me.tvIssues.CheckBoxes = True
        Me.tvIssues.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvIssues.Location = New System.Drawing.Point(3, 54)
        Me.tvIssues.Name = "tvIssues"
        Me.tvIssues.ShowLines = False
        Me.tvIssues.ShowPlusMinus = False
        Me.tvIssues.ShowRootLines = False
        Me.tvIssues.Size = New System.Drawing.Size(413, 417)
        Me.tvIssues.TabIndex = 33
        '
        'txtIssueName
        '
        Me.txtIssueName.Location = New System.Drawing.Point(634, 54)
        Me.txtIssueName.Name = "txtIssueName"
        Me.txtIssueName.Size = New System.Drawing.Size(321, 20)
        Me.txtIssueName.TabIndex = 28
        '
        'txtIssueID
        '
        Me.txtIssueID.Location = New System.Drawing.Point(437, 54)
        Me.txtIssueID.Name = "txtIssueID"
        Me.txtIssueID.Size = New System.Drawing.Size(191, 20)
        Me.txtIssueID.TabIndex = 27
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(433, 104)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(522, 367)
        Me.txtDescription.TabIndex = 31
        '
        'btnCancelIssue
        '
        Me.btnCancelIssue.Location = New System.Drawing.Point(676, 8)
        Me.btnCancelIssue.Name = "btnCancelIssue"
        Me.btnCancelIssue.Size = New System.Drawing.Size(91, 23)
        Me.btnCancelIssue.TabIndex = 32
        Me.btnCancelIssue.UseVisualStyleBackColor = True
        '
        'btnUpdateIssue
        '
        Me.btnUpdateIssue.Location = New System.Drawing.Point(571, 8)
        Me.btnUpdateIssue.Name = "btnUpdateIssue"
        Me.btnUpdateIssue.Size = New System.Drawing.Size(90, 23)
        Me.btnUpdateIssue.TabIndex = 26
        Me.btnUpdateIssue.Text = "Update"
        Me.btnUpdateIssue.UseVisualStyleBackColor = True
        '
        'lblItemCount
        '
        Me.lblItemCount.AutoSize = True
        Me.lblItemCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemCount.Location = New System.Drawing.Point(6, 36)
        Me.lblItemCount.Name = "lblItemCount"
        Me.lblItemCount.Size = New System.Drawing.Size(73, 13)
        Me.lblItemCount.TabIndex = 26
        Me.lblItemCount.Text = "Issue Name"
        '
        'btnAddIssue
        '
        Me.btnAddIssue.Location = New System.Drawing.Point(439, 8)
        Me.btnAddIssue.Name = "btnAddIssue"
        Me.btnAddIssue.Size = New System.Drawing.Size(118, 23)
        Me.btnAddIssue.TabIndex = 25
        Me.btnAddIssue.Text = "Add New Issue"
        Me.btnAddIssue.UseVisualStyleBackColor = True
        '
        'cboCheckListView
        '
        Me.cboCheckListView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckListView.FormattingEnabled = True
        Me.cboCheckListView.Items.AddRange(New Object() {"Incomplete", "Complete", "All"})
        Me.cboCheckListView.Location = New System.Drawing.Point(48, 11)
        Me.cboCheckListView.Name = "cboCheckListView"
        Me.cboCheckListView.Size = New System.Drawing.Size(368, 21)
        Me.cboCheckListView.TabIndex = 24
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.Location = New System.Drawing.Point(8, 14)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(34, 13)
        Me.Label52.TabIndex = 23
        Me.Label52.Text = "View"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(631, 36)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(73, 13)
        Me.Label49.TabIndex = 16
        Me.Label49.Text = "Issue Name"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(434, 38)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(54, 13)
        Me.Label48.TabIndex = 15
        Me.Label48.Text = "Issue ID"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(434, 82)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(71, 13)
        Me.Label47.TabIndex = 14
        Me.Label47.Text = "Description"
        '
        'ValCheckList
        '
        Me.ValCheckList.FormattingEnabled = True
        Me.ValCheckList.HorizontalScrollbar = True
        Me.ValCheckList.Location = New System.Drawing.Point(3, 54)
        Me.ValCheckList.Name = "ValCheckList"
        Me.ValCheckList.Size = New System.Drawing.Size(413, 409)
        Me.ValCheckList.TabIndex = 0
        '
        'tabNotes
        '
        Me.tabNotes.BackColor = System.Drawing.Color.AliceBlue
        Me.tabNotes.Controls.Add(Me.cboNotes)
        Me.tabNotes.Controls.Add(Me.btnUnlockPN)
        Me.tabNotes.Controls.Add(Me.btnCreatePN)
        Me.tabNotes.Controls.Add(Me.btnSavePN)
        Me.tabNotes.Controls.Add(Me.txtNotes)
        Me.tabNotes.Controls.Add(Me.Label46)
        Me.tabNotes.Location = New System.Drawing.Point(4, 22)
        Me.tabNotes.Name = "tabNotes"
        Me.tabNotes.Padding = New System.Windows.Forms.Padding(3)
        Me.tabNotes.Size = New System.Drawing.Size(972, 458)
        Me.tabNotes.TabIndex = 2
        Me.tabNotes.Text = "Notes"
        '
        'cboNotes
        '
        Me.cboNotes.FormattingEnabled = True
        Me.cboNotes.Items.AddRange(New Object() {"Notes", "Continuing Issues", "Consulting Opportunities"})
        Me.cboNotes.Location = New System.Drawing.Point(73, 16)
        Me.cboNotes.Name = "cboNotes"
        Me.cboNotes.Size = New System.Drawing.Size(272, 21)
        Me.cboNotes.TabIndex = 35
        '
        'btnUnlockPN
        '
        Me.btnUnlockPN.Location = New System.Drawing.Point(858, 16)
        Me.btnUnlockPN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnlockPN.Name = "btnUnlockPN"
        Me.btnUnlockPN.Size = New System.Drawing.Size(89, 28)
        Me.btnUnlockPN.TabIndex = 34
        Me.btnUnlockPN.Text = "Unlock PN File"
        Me.btnUnlockPN.UseVisualStyleBackColor = True
        Me.btnUnlockPN.Visible = False
        '
        'btnCreatePN
        '
        Me.btnCreatePN.Location = New System.Drawing.Point(760, 16)
        Me.btnCreatePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCreatePN.Name = "btnCreatePN"
        Me.btnCreatePN.Size = New System.Drawing.Size(89, 28)
        Me.btnCreatePN.TabIndex = 33
        Me.btnCreatePN.Text = "Create PN File"
        Me.btnCreatePN.UseVisualStyleBackColor = True
        '
        'btnSavePN
        '
        Me.btnSavePN.Location = New System.Drawing.Point(875, 419)
        Me.btnSavePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSavePN.Name = "btnSavePN"
        Me.btnSavePN.Size = New System.Drawing.Size(74, 28)
        Me.btnSavePN.TabIndex = 13
        Me.btnSavePN.Text = "Save"
        Me.btnSavePN.UseVisualStyleBackColor = True
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(15, 51)
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNotes.Size = New System.Drawing.Size(932, 355)
        Me.txtNotes.TabIndex = 12
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(15, 19)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(39, 13)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Type:"
        '
        'tabContacts
        '
        Me.tabContacts.Controls.Add(Me.Panel2)
        Me.tabContacts.Controls.Add(Me.MainPanel)
        Me.tabContacts.Location = New System.Drawing.Point(4, 22)
        Me.tabContacts.Name = "tabContacts"
        Me.tabContacts.Padding = New System.Windows.Forms.Padding(3)
        Me.tabContacts.Size = New System.Drawing.Size(972, 458)
        Me.tabContacts.TabIndex = 0
        Me.tabContacts.Text = "Contacts"
        Me.tabContacts.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.pnlCompany)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(966, 129)
        Me.Panel2.TabIndex = 21
        '
        'pnlCompany
        '
        Me.pnlCompany.Controls.Add(Me.lblACPhone)
        Me.pnlCompany.Controls.Add(Me.lblAltEmail)
        Me.pnlCompany.Controls.Add(Me.lblAltName)
        Me.pnlCompany.Controls.Add(Me.lblCCPhone)
        Me.pnlCompany.Controls.Add(Me.lblEmail)
        Me.pnlCompany.Controls.Add(Me.lblName)
        Me.pnlCompany.Controls.Add(Me.btnShowAltCo)
        Me.pnlCompany.Controls.Add(Me.btnShow)
        Me.pnlCompany.Controls.Add(Me.ACEmail2)
        Me.pnlCompany.Controls.Add(Me.CCEmail1)
        Me.pnlCompany.Controls.Add(Me.Label6)
        Me.pnlCompany.Controls.Add(Me.Label5)
        Me.pnlCompany.Controls.Add(Me.Label4)
        Me.pnlCompany.Location = New System.Drawing.Point(4, 3)
        Me.pnlCompany.Name = "pnlCompany"
        Me.pnlCompany.Size = New System.Drawing.Size(945, 121)
        Me.pnlCompany.TabIndex = 0
        '
        'lblACPhone
        '
        Me.lblACPhone.AutoSize = True
        Me.lblACPhone.Location = New System.Drawing.Point(769, 66)
        Me.lblACPhone.Name = "lblACPhone"
        Me.lblACPhone.Size = New System.Drawing.Size(83, 13)
        Me.lblACPhone.TabIndex = 72
        Me.lblACPhone.Text = "Alternate Phone"
        '
        'lblAltEmail
        '
        Me.lblAltEmail.AutoSize = True
        Me.lblAltEmail.Location = New System.Drawing.Point(329, 66)
        Me.lblAltEmail.Name = "lblAltEmail"
        Me.lblAltEmail.Size = New System.Drawing.Size(77, 13)
        Me.lblAltEmail.TabIndex = 71
        Me.lblAltEmail.Text = "Alternate Email"
        '
        'lblAltName
        '
        Me.lblAltName.AutoSize = True
        Me.lblAltName.Location = New System.Drawing.Point(136, 66)
        Me.lblAltName.Name = "lblAltName"
        Me.lblAltName.Size = New System.Drawing.Size(80, 13)
        Me.lblAltName.TabIndex = 70
        Me.lblAltName.Text = "Alternate Name"
        '
        'lblCCPhone
        '
        Me.lblCCPhone.AutoSize = True
        Me.lblCCPhone.Location = New System.Drawing.Point(769, 37)
        Me.lblCCPhone.Name = "lblCCPhone"
        Me.lblCCPhone.Size = New System.Drawing.Size(142, 13)
        Me.lblCCPhone.TabIndex = 69
        Me.lblCCPhone.Text = "Company Coordinator Phone"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(329, 37)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(136, 13)
        Me.lblEmail.TabIndex = 68
        Me.lblEmail.Text = "Company Coordinator Email"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(136, 37)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(139, 13)
        Me.lblName.TabIndex = 67
        Me.lblName.Text = "Company Coordinator Name"
        '
        'btnShowAltCo
        '
        Me.btnShowAltCo.Location = New System.Drawing.Point(3, 61)
        Me.btnShowAltCo.Name = "btnShowAltCo"
        Me.btnShowAltCo.Size = New System.Drawing.Size(116, 23)
        Me.btnShowAltCo.TabIndex = 66
        Me.btnShowAltCo.Text = " Alt Company Coord"
        Me.btnShowAltCo.UseVisualStyleBackColor = True
        '
        'btnShow
        '
        Me.btnShow.Location = New System.Drawing.Point(3, 32)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(116, 23)
        Me.btnShow.TabIndex = 65
        Me.btnShow.Text = "Company Coordinator"
        Me.btnShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'ACEmail2
        '
        Me.ACEmail2.Image = CType(resources.GetObject("ACEmail2.Image"), System.Drawing.Image)
        Me.ACEmail2.Location = New System.Drawing.Point(740, 63)
        Me.ACEmail2.Name = "ACEmail2"
        Me.ACEmail2.Size = New System.Drawing.Size(23, 21)
        Me.ACEmail2.TabIndex = 63
        Me.ACEmail2.TabStop = False
        '
        'CCEmail1
        '
        Me.CCEmail1.Image = CType(resources.GetObject("CCEmail1.Image"), System.Drawing.Image)
        Me.CCEmail1.Location = New System.Drawing.Point(740, 33)
        Me.CCEmail1.Name = "CCEmail1"
        Me.CCEmail1.Size = New System.Drawing.Size(23, 21)
        Me.CCEmail1.TabIndex = 62
        Me.CCEmail1.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(766, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 49
        Me.Label6.Text = "Telephone"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(332, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Email Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(139, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Name"
        '
        'MainPanel
        '
        Me.MainPanel.BackColor = System.Drawing.Color.AliceBlue
        Me.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MainPanel.Controls.Add(Me.pnlRefinery)
        Me.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainPanel.Location = New System.Drawing.Point(3, 3)
        Me.MainPanel.Name = "MainPanel"
        Me.MainPanel.Size = New System.Drawing.Size(966, 452)
        Me.MainPanel.TabIndex = 20
        '
        'pnlRefinery
        '
        Me.pnlRefinery.Controls.Add(Me.Label16)
        Me.pnlRefinery.Controls.Add(Me.Label7)
        Me.pnlRefinery.Controls.Add(Me.Label3)
        Me.pnlRefinery.Controls.Add(Me.lblDataEmail)
        Me.pnlRefinery.Controls.Add(Me.lblDataCoordinator)
        Me.pnlRefinery.Controls.Add(Me.lblPricingEmail)
        Me.pnlRefinery.Controls.Add(Me.lblPricingName)
        Me.pnlRefinery.Controls.Add(Me.lblPCPhone)
        Me.pnlRefinery.Controls.Add(Me.lblPCEmail)
        Me.pnlRefinery.Controls.Add(Me.lblPCName)
        Me.pnlRefinery.Controls.Add(Me.RMEmail)
        Me.pnlRefinery.Controls.Add(Me.RACEmail)
        Me.pnlRefinery.Controls.Add(Me.RCEmail)
        Me.pnlRefinery.Location = New System.Drawing.Point(6, 135)
        Me.pnlRefinery.Name = "pnlRefinery"
        Me.pnlRefinery.Size = New System.Drawing.Size(943, 114)
        Me.pnlRefinery.TabIndex = 108
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(19, 72)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(111, 13)
        Me.Label16.TabIndex = 142
        Me.Label16.Text = "Data Coordinator :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(8, 43)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 13)
        Me.Label7.TabIndex = 141
        Me.Label7.Text = "Pricing Coordinator :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 13)
        Me.Label3.TabIndex = 140
        Me.Label3.Text = "Plant Coordinator  :"
        '
        'lblDataEmail
        '
        Me.lblDataEmail.AutoSize = True
        Me.lblDataEmail.Location = New System.Drawing.Point(328, 72)
        Me.lblDataEmail.Name = "lblDataEmail"
        Me.lblDataEmail.Size = New System.Drawing.Size(50, 13)
        Me.lblDataEmail.TabIndex = 139
        Me.lblDataEmail.Text = "DC Email"
        '
        'lblDataCoordinator
        '
        Me.lblDataCoordinator.AutoSize = True
        Me.lblDataCoordinator.Location = New System.Drawing.Point(137, 72)
        Me.lblDataCoordinator.Name = "lblDataCoordinator"
        Me.lblDataCoordinator.Size = New System.Drawing.Size(53, 13)
        Me.lblDataCoordinator.TabIndex = 138
        Me.lblDataCoordinator.Text = "DC Name"
        '
        'lblPricingEmail
        '
        Me.lblPricingEmail.AutoSize = True
        Me.lblPricingEmail.Location = New System.Drawing.Point(328, 43)
        Me.lblPricingEmail.Name = "lblPricingEmail"
        Me.lblPricingEmail.Size = New System.Drawing.Size(67, 13)
        Me.lblPricingEmail.TabIndex = 137
        Me.lblPricingEmail.Text = "Pricing Email"
        '
        'lblPricingName
        '
        Me.lblPricingName.AutoSize = True
        Me.lblPricingName.Location = New System.Drawing.Point(137, 43)
        Me.lblPricingName.Name = "lblPricingName"
        Me.lblPricingName.Size = New System.Drawing.Size(70, 13)
        Me.lblPricingName.TabIndex = 136
        Me.lblPricingName.Text = "Pricing Name"
        '
        'lblPCPhone
        '
        Me.lblPCPhone.AutoSize = True
        Me.lblPCPhone.Location = New System.Drawing.Point(766, 14)
        Me.lblPCPhone.Name = "lblPCPhone"
        Me.lblPCPhone.Size = New System.Drawing.Size(122, 13)
        Me.lblPCPhone.TabIndex = 135
        Me.lblPCPhone.Text = "Plant Coordinator Phone"
        '
        'lblPCEmail
        '
        Me.lblPCEmail.AutoSize = True
        Me.lblPCEmail.Location = New System.Drawing.Point(328, 14)
        Me.lblPCEmail.Name = "lblPCEmail"
        Me.lblPCEmail.Size = New System.Drawing.Size(116, 13)
        Me.lblPCEmail.TabIndex = 134
        Me.lblPCEmail.Text = "Plant Coordinator Email"
        '
        'lblPCName
        '
        Me.lblPCName.AutoSize = True
        Me.lblPCName.Location = New System.Drawing.Point(137, 14)
        Me.lblPCName.Name = "lblPCName"
        Me.lblPCName.Size = New System.Drawing.Size(119, 13)
        Me.lblPCName.TabIndex = 133
        Me.lblPCName.Text = "Plant Coordinator Name"
        '
        'RMEmail
        '
        Me.RMEmail.Image = CType(resources.GetObject("RMEmail.Image"), System.Drawing.Image)
        Me.RMEmail.Location = New System.Drawing.Point(738, 64)
        Me.RMEmail.Name = "RMEmail"
        Me.RMEmail.Size = New System.Drawing.Size(28, 21)
        Me.RMEmail.TabIndex = 129
        Me.RMEmail.TabStop = False
        '
        'RACEmail
        '
        Me.RACEmail.Image = CType(resources.GetObject("RACEmail.Image"), System.Drawing.Image)
        Me.RACEmail.Location = New System.Drawing.Point(738, 39)
        Me.RACEmail.Name = "RACEmail"
        Me.RACEmail.Size = New System.Drawing.Size(28, 21)
        Me.RACEmail.TabIndex = 128
        Me.RACEmail.TabStop = False
        '
        'RCEmail
        '
        Me.RCEmail.Image = CType(resources.GetObject("RCEmail.Image"), System.Drawing.Image)
        Me.RCEmail.Location = New System.Drawing.Point(738, 14)
        Me.RCEmail.Name = "RCEmail"
        Me.RCEmail.Size = New System.Drawing.Size(28, 21)
        Me.RCEmail.TabIndex = 127
        Me.RCEmail.TabStop = False
        '
        'ConsoleTabs
        '
        Me.ConsoleTabs.AllowDrop = True
        Me.ConsoleTabs.CausesValidation = False
        Me.ConsoleTabs.Controls.Add(Me.tabCorr)
        Me.ConsoleTabs.Controls.Add(Me.tabContacts)
        Me.ConsoleTabs.Controls.Add(Me.tabCheckList)
        Me.ConsoleTabs.Controls.Add(Me.tabSS)
        Me.ConsoleTabs.Controls.Add(Me.tabDD)
        Me.ConsoleTabs.Controls.Add(Me.tabClippy)
        Me.ConsoleTabs.Controls.Add(Me.tabTimeGrade)
        Me.ConsoleTabs.Controls.Add(Me.tabNotes)
        Me.ConsoleTabs.Controls.Add(Me.TabPage1)
        Me.ConsoleTabs.Enabled = False
        Me.ConsoleTabs.Location = New System.Drawing.Point(12, 211)
        Me.ConsoleTabs.Name = "ConsoleTabs"
        Me.ConsoleTabs.SelectedIndex = 0
        Me.ConsoleTabs.Size = New System.Drawing.Size(980, 484)
        Me.ConsoleTabs.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage1.Controls.Add(Me.btnRAM)
        Me.TabPage1.Controls.Add(Me.btnVI)
        Me.TabPage1.Controls.Add(Me.btnSC)
        Me.TabPage1.Controls.Add(Me.btnPA)
        Me.TabPage1.Controls.Add(Me.btnCT)
        Me.TabPage1.Controls.Add(Me.btnRefresh)
        Me.TabPage1.Controls.Add(Me.btnDrawings)
        Me.TabPage1.Controls.Add(Me.btnHYC)
        Me.TabPage1.Controls.Add(Me.btnPT)
        Me.TabPage1.Controls.Add(Me.btnSpecFrac)
        Me.TabPage1.Controls.Add(Me.btnUnitReview)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.lstFiles)
        Me.TabPage1.Controls.Add(Me.dgCorr)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(972, 458)
        Me.TabPage1.TabIndex = 12
        Me.TabPage1.Text = "New VR/VF Tabpage"
        '
        'btnRAM
        '
        Me.btnRAM.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnRAM.Enabled = False
        Me.btnRAM.Location = New System.Drawing.Point(852, 349)
        Me.btnRAM.Margin = New System.Windows.Forms.Padding(1)
        Me.btnRAM.Name = "btnRAM"
        Me.btnRAM.Size = New System.Drawing.Size(109, 30)
        Me.btnRAM.TabIndex = 88
        Me.btnRAM.Text = "RAM Tables (RAM)"
        Me.btnRAM.UseVisualStyleBackColor = True
        '
        'btnVI
        '
        Me.btnVI.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnVI.Enabled = False
        Me.btnVI.Location = New System.Drawing.Point(852, 381)
        Me.btnVI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnVI.Name = "btnVI"
        Me.btnVI.Size = New System.Drawing.Size(109, 30)
        Me.btnVI.TabIndex = 89
        Me.btnVI.Text = "Validated Input (VI)"
        Me.btnVI.UseVisualStyleBackColor = True
        '
        'btnSC
        '
        Me.btnSC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSC.Enabled = False
        Me.btnSC.Location = New System.Drawing.Point(852, 63)
        Me.btnSC.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSC.Name = "btnSC"
        Me.btnSC.Size = New System.Drawing.Size(109, 30)
        Me.btnSC.TabIndex = 86
        Me.btnSC.Text = "Sum Calc (SC)"
        Me.btnSC.UseVisualStyleBackColor = True
        '
        'btnPA
        '
        Me.btnPA.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPA.Enabled = False
        Me.btnPA.Location = New System.Drawing.Point(852, 317)
        Me.btnPA.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPA.Name = "btnPA"
        Me.btnPA.Size = New System.Drawing.Size(109, 30)
        Me.btnPA.TabIndex = 87
        Me.btnPA.Text = "Gap File (PA)"
        Me.btnPA.UseVisualStyleBackColor = True
        '
        'btnCT
        '
        Me.btnCT.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCT.Enabled = False
        Me.btnCT.Location = New System.Drawing.Point(852, 285)
        Me.btnCT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCT.Name = "btnCT"
        Me.btnCT.Size = New System.Drawing.Size(109, 30)
        Me.btnCT.TabIndex = 85
        Me.btnCT.Text = "Client Table (CT)"
        Me.btnCT.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(852, 17)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(109, 30)
        Me.btnRefresh.TabIndex = 83
        Me.btnRefresh.Text = "Open Explorer"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnDrawings
        '
        Me.btnDrawings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnDrawings.Enabled = False
        Me.btnDrawings.Location = New System.Drawing.Point(852, 223)
        Me.btnDrawings.Margin = New System.Windows.Forms.Padding(1)
        Me.btnDrawings.Name = "btnDrawings"
        Me.btnDrawings.Size = New System.Drawing.Size(109, 30)
        Me.btnDrawings.TabIndex = 93
        Me.btnDrawings.Text = "Drawings"
        Me.btnDrawings.UseVisualStyleBackColor = True
        '
        'btnPT
        '
        Me.btnPT.Enabled = False
        Me.btnPT.Location = New System.Drawing.Point(852, 95)
        Me.btnPT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPT.Name = "btnPT"
        Me.btnPT.Size = New System.Drawing.Size(109, 30)
        Me.btnPT.TabIndex = 84
        Me.btnPT.Text = "Prelim Client (PT)"
        Me.btnPT.UseVisualStyleBackColor = True
        '
        'btnSpecFrac
        '
        Me.btnSpecFrac.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSpecFrac.Enabled = False
        Me.btnSpecFrac.Location = New System.Drawing.Point(852, 159)
        Me.btnSpecFrac.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSpecFrac.Name = "btnSpecFrac"
        Me.btnSpecFrac.Size = New System.Drawing.Size(109, 30)
        Me.btnSpecFrac.TabIndex = 91
        Me.btnSpecFrac.Text = "Spec Frac"
        Me.btnSpecFrac.UseVisualStyleBackColor = True
        '
        'btnUnitReview
        '
        Me.btnUnitReview.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnUnitReview.Enabled = False
        Me.btnUnitReview.Location = New System.Drawing.Point(852, 127)
        Me.btnUnitReview.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnitReview.Name = "btnUnitReview"
        Me.btnUnitReview.Size = New System.Drawing.Size(109, 30)
        Me.btnUnitReview.TabIndex = 90
        Me.btnUnitReview.Text = "Unit Review"
        Me.btnUnitReview.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(24, 413)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(108, 13)
        Me.Label19.TabIndex = 79
        Me.Label19.Text = "Double-Click to Open"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(12, 276)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(47, 17)
        Me.Label18.TabIndex = 82
        Me.Label18.Text = "Files:"
        '
        'lstFiles
        '
        Me.lstFiles.BackColor = System.Drawing.Color.FloralWhite
        Me.lstFiles.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstFiles.FormattingEnabled = True
        Me.lstFiles.ItemHeight = 15
        Me.lstFiles.Location = New System.Drawing.Point(22, 301)
        Me.lstFiles.MultiColumn = True
        Me.lstFiles.Name = "lstFiles"
        Me.lstFiles.Size = New System.Drawing.Size(821, 109)
        Me.lstFiles.TabIndex = 1
        '
        'dgCorr
        '
        Me.dgCorr.AllowUserToAddRows = False
        Me.dgCorr.AllowUserToDeleteRows = False
        Me.dgCorr.BackgroundColor = System.Drawing.Color.FloralWhite
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgCorr.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgCorr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgCorr.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Cycle, Me.ValFaxes, Me.ValReply, Me.ReturnFiles, Me.MinWorked, Me.Consultant})
        Me.dgCorr.GridColor = System.Drawing.Color.PapayaWhip
        Me.dgCorr.Location = New System.Drawing.Point(18, 17)
        Me.dgCorr.MultiSelect = False
        Me.dgCorr.Name = "dgCorr"
        Me.dgCorr.ReadOnly = True
        Me.dgCorr.RowHeadersVisible = False
        Me.dgCorr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgCorr.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgCorr.Size = New System.Drawing.Size(825, 256)
        Me.dgCorr.TabIndex = 0
        '
        'Cycle
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Cycle.DefaultCellStyle = DataGridViewCellStyle3
        Me.Cycle.HeaderText = "#"
        Me.Cycle.Name = "Cycle"
        Me.Cycle.ReadOnly = True
        Me.Cycle.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cycle.Width = 50
        '
        'ValFaxes
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ValFaxes.DefaultCellStyle = DataGridViewCellStyle4
        Me.ValFaxes.HeaderText = "Val Faxes"
        Me.ValFaxes.Name = "ValFaxes"
        Me.ValFaxes.ReadOnly = True
        Me.ValFaxes.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ValFaxes.Width = 175
        '
        'ValReply
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ValReply.DefaultCellStyle = DataGridViewCellStyle5
        Me.ValReply.HeaderText = "Val Replies"
        Me.ValReply.Name = "ValReply"
        Me.ValReply.ReadOnly = True
        Me.ValReply.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ValReply.Width = 175
        '
        'ReturnFiles
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReturnFiles.DefaultCellStyle = DataGridViewCellStyle6
        Me.ReturnFiles.HeaderText = "Return Files"
        Me.ReturnFiles.Name = "ReturnFiles"
        Me.ReturnFiles.ReadOnly = True
        Me.ReturnFiles.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReturnFiles.Width = 175
        '
        'MinWorked
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.MinWorked.DefaultCellStyle = DataGridViewCellStyle7
        Me.MinWorked.HeaderText = "Worked"
        Me.MinWorked.Name = "MinWorked"
        Me.MinWorked.ReadOnly = True
        Me.MinWorked.Width = 125
        '
        'Consultant
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Consultant.DefaultCellStyle = DataGridViewCellStyle8
        Me.Consultant.HeaderText = "Consultant"
        Me.Consultant.Name = "Consultant"
        Me.Consultant.ReadOnly = true
        Me.Consultant.Width = 125
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.PapayaWhip
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblOSIMUpload)
        Me.Panel3.Controls.Add(Me.lblOU)
        Me.Panel3.Controls.Add(Me.lblCalcUpload)
        Me.Panel3.Controls.Add(Me.lblCU)
        Me.Panel3.Controls.Add(Me.lblItemsRemaining)
        Me.Panel3.Controls.Add(Me.lblBlueFlags)
        Me.Panel3.Controls.Add(Me.lblRedFlags)
        Me.Panel3.Controls.Add(Me.lblLastCalcDate)
        Me.Panel3.Controls.Add(Me.lblLastUpload)
        Me.Panel3.Controls.Add(Me.lblLastFileSave)
        Me.Panel3.Controls.Add(Me.lblValidationStatus)
        Me.Panel3.Controls.Add(Me.lblLU)
        Me.Panel3.Controls.Add(Me.lblLFS)
        Me.Panel3.Controls.Add(Me.lblLCD)
        Me.Panel3.Controls.Add(Me.Label35)
        Me.Panel3.Controls.Add(Me.btnBug)
        Me.Panel3.Controls.Add(Me.btnKillProcesses)
        Me.Panel3.Controls.Add(Me.btnHelp)
        Me.Panel3.Location = New System.Drawing.Point(478, 12)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(510, 132)
        Me.Panel3.TabIndex = 68
        '
        'lblOSIMUpload
        '
        Me.lblOSIMUpload.AutoSize = true
        Me.lblOSIMUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblOSIMUpload.Location = New System.Drawing.Point(103, 81)
        Me.lblOSIMUpload.Name = "lblOSIMUpload"
        Me.lblOSIMUpload.Size = New System.Drawing.Size(130, 13)
        Me.lblOSIMUpload.TabIndex = 61
        Me.lblOSIMUpload.Text = "6/8/2010 3:51:08 PM"
        Me.lblOSIMUpload.Visible = false
        '
        'lblOU
        '
        Me.lblOU.AutoSize = true
        Me.lblOU.Location = New System.Drawing.Point(7, 81)
        Me.lblOU.Name = "lblOU"
        Me.lblOU.Size = New System.Drawing.Size(74, 13)
        Me.lblOU.TabIndex = 60
        Me.lblOU.Text = "OSIM Upload:"
        Me.lblOU.Visible = false
        '
        'lblCalcUpload
        '
        Me.lblCalcUpload.AutoSize = true
        Me.lblCalcUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblCalcUpload.Location = New System.Drawing.Point(103, 97)
        Me.lblCalcUpload.Name = "lblCalcUpload"
        Me.lblCalcUpload.Size = New System.Drawing.Size(130, 13)
        Me.lblCalcUpload.TabIndex = 59
        Me.lblCalcUpload.Text = "6/8/2010 3:51:08 PM"
        Me.lblCalcUpload.Visible = false
        '
        'lblCU
        '
        Me.lblCU.AutoSize = true
        Me.lblCU.Location = New System.Drawing.Point(7, 97)
        Me.lblCU.Name = "lblCU"
        Me.lblCU.Size = New System.Drawing.Size(68, 13)
        Me.lblCU.TabIndex = 58
        Me.lblCU.Text = "Calc Upload:"
        Me.lblCU.Visible = false
        '
        'lblItemsRemaining
        '
        Me.lblItemsRemaining.AutoSize = true
        Me.lblItemsRemaining.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblItemsRemaining.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblItemsRemaining.Location = New System.Drawing.Point(282, 81)
        Me.lblItemsRemaining.Name = "lblItemsRemaining"
        Me.lblItemsRemaining.Size = New System.Drawing.Size(174, 13)
        Me.lblItemsRemaining.TabIndex = 57
        Me.lblItemsRemaining.Text = "10 Checklist Items Remaining"
        '
        'lblBlueFlags
        '
        Me.lblBlueFlags.AutoSize = true
        Me.lblBlueFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblBlueFlags.ForeColor = System.Drawing.Color.Navy
        Me.lblBlueFlags.Location = New System.Drawing.Point(282, 59)
        Me.lblBlueFlags.Name = "lblBlueFlags"
        Me.lblBlueFlags.Size = New System.Drawing.Size(91, 13)
        Me.lblBlueFlags.TabIndex = 56
        Me.lblBlueFlags.Text = "100 Blue Flags"
        '
        'lblRedFlags
        '
        Me.lblRedFlags.AutoSize = true
        Me.lblRedFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblRedFlags.ForeColor = System.Drawing.Color.Red
        Me.lblRedFlags.Location = New System.Drawing.Point(282, 36)
        Me.lblRedFlags.Name = "lblRedFlags"
        Me.lblRedFlags.Size = New System.Drawing.Size(82, 13)
        Me.lblRedFlags.TabIndex = 55
        Me.lblRedFlags.Text = "17 Red Flags"
        '
        'lblLastCalcDate
        '
        Me.lblLastCalcDate.AutoSize = true
        Me.lblLastCalcDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblLastCalcDate.Location = New System.Drawing.Point(103, 65)
        Me.lblLastCalcDate.Name = "lblLastCalcDate"
        Me.lblLastCalcDate.Size = New System.Drawing.Size(130, 13)
        Me.lblLastCalcDate.TabIndex = 54
        Me.lblLastCalcDate.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastUpload
        '
        Me.lblLastUpload.AutoSize = true
        Me.lblLastUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblLastUpload.Location = New System.Drawing.Point(103, 49)
        Me.lblLastUpload.Name = "lblLastUpload"
        Me.lblLastUpload.Size = New System.Drawing.Size(130, 13)
        Me.lblLastUpload.TabIndex = 53
        Me.lblLastUpload.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastFileSave
        '
        Me.lblLastFileSave.AutoSize = true
        Me.lblLastFileSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblLastFileSave.Location = New System.Drawing.Point(103, 33)
        Me.lblLastFileSave.Name = "lblLastFileSave"
        Me.lblLastFileSave.Size = New System.Drawing.Size(130, 13)
        Me.lblLastFileSave.TabIndex = 52
        Me.lblLastFileSave.Text = "6/8/2010 3:51:08 PM"
        '
        'lblValidationStatus
        '
        Me.lblValidationStatus.AutoSize = true
        Me.lblValidationStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblValidationStatus.Location = New System.Drawing.Point(103, 10)
        Me.lblValidationStatus.Name = "lblValidationStatus"
        Me.lblValidationStatus.Size = New System.Drawing.Size(63, 13)
        Me.lblValidationStatus.TabIndex = 51
        Me.lblValidationStatus.Text = "Validating"
        '
        'lblLU
        '
        Me.lblLU.AutoSize = true
        Me.lblLU.Location = New System.Drawing.Point(7, 49)
        Me.lblLU.Name = "lblLU"
        Me.lblLU.Size = New System.Drawing.Size(67, 13)
        Me.lblLU.TabIndex = 50
        Me.lblLU.Text = "Last Upload:"
        '
        'lblLFS
        '
        Me.lblLFS.AutoSize = true
        Me.lblLFS.Location = New System.Drawing.Point(7, 33)
        Me.lblLFS.Name = "lblLFS"
        Me.lblLFS.Size = New System.Drawing.Size(77, 13)
        Me.lblLFS.TabIndex = 49
        Me.lblLFS.Text = "Last File Save:"
        '
        'lblLCD
        '
        Me.lblLCD.AutoSize = true
        Me.lblLCD.Location = New System.Drawing.Point(7, 65)
        Me.lblLCD.Name = "lblLCD"
        Me.lblLCD.Size = New System.Drawing.Size(80, 13)
        Me.lblLCD.TabIndex = 48
        Me.lblLCD.Text = "Last Calc Date:"
        '
        'Label35
        '
        Me.Label35.AutoSize = true
        Me.Label35.Location = New System.Drawing.Point(7, 10)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(89, 13)
        Me.Label35.TabIndex = 47
        Me.Label35.Text = "Status:"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.PapayaWhip
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.btnSS)
        Me.Panel4.Controls.Add(Me.btnValidate)
        Me.Panel4.Location = New System.Drawing.Point(478, 151)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(510, 54)
        Me.Panel4.TabIndex = 69
        '
        'btnSS
        '
        Me.btnSS.Location = New System.Drawing.Point(106, 3)
        Me.btnSS.Name = "btnSS"
        Me.btnSS.Size = New System.Drawing.Size(84, 23)
        Me.btnSS.TabIndex = 51
        Me.btnSS.Text = "Secure Send"
        Me.btnSS.UseVisualStyleBackColor = true
        '
        'btnValidate
        '
        Me.btnValidate.Location = New System.Drawing.Point(16, 2)
        Me.btnValidate.Name = "btnValidate"
        Me.btnValidate.Size = New System.Drawing.Size(84, 23)
        Me.btnValidate.TabIndex = 50
        Me.btnValidate.Text = "Validate"
        Me.btnValidate.UseVisualStyleBackColor = true
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.PapayaWhip
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.btnReturnPW)
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.btnCompanyPW)
        Me.Panel5.Controls.Add(Me.btnMain)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Controls.Add(Me.Label14)
        Me.Panel5.Controls.Add(Me.cboCompany)
        Me.Panel5.Controls.Add(Me.lblCompany)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Controls.Add(Me.cboConsultant)
        Me.Panel5.Controls.Add(Me.lblWarning)
        Me.Panel5.Controls.Add(Me.Label60)
        Me.Panel5.Controls.Add(Me.cboRefNum)
        Me.Panel5.Controls.Add(Me.Label59)
        Me.Panel5.Controls.Add(Me.cboStudy)
        Me.Help.SetHelpKeyword(Me.Panel5, "help\hs0001.htm")
        Me.Help.SetHelpNavigator(Me.Panel5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Panel5.Location = New System.Drawing.Point(19, 12)
        Me.Panel5.Name = "Panel5"
        Me.Help.SetShowHelp(Me.Panel5, true)
        Me.Panel5.Size = New System.Drawing.Size(444, 193)
        Me.Panel5.TabIndex = 70
        '
        'btnReturnPW
        '
        Me.btnReturnPW.Location = New System.Drawing.Point(261, 151)
        Me.btnReturnPW.Name = "btnReturnPW"
        Me.btnReturnPW.Size = New System.Drawing.Size(121, 23)
        Me.btnReturnPW.TabIndex = 81
        Me.btnReturnPW.UseVisualStyleBackColor = true
        '
        'Label17
        '
        Me.Label17.AutoSize = true
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label17.Location = New System.Drawing.Point(192, 154)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(62, 17)
        Me.Label17.TabIndex = 80
        Me.Label17.Text = "Return "
        '
        'btnCompanyPW
        '
        Me.btnCompanyPW.Location = New System.Drawing.Point(95, 151)
        Me.btnCompanyPW.Name = "btnCompanyPW"
        Me.btnCompanyPW.Size = New System.Drawing.Size(90, 23)
        Me.btnCompanyPW.TabIndex = 79
        Me.btnCompanyPW.UseVisualStyleBackColor = true
        '
        'btnMain
        '
        Me.btnMain.Location = New System.Drawing.Point(95, 117)
        Me.btnMain.Name = "btnMain"
        Me.btnMain.Size = New System.Drawing.Size(90, 23)
        Me.btnMain.TabIndex = 78
        Me.btnMain.UseVisualStyleBackColor = true
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 154)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 17)
        Me.Label2.TabIndex = 77
        Me.Label2.Text = "Company"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 117)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 17)
        Me.Label1.TabIndex = 76
        Me.Label1.Text = "Main"
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(2, 124)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 13)
        Me.Label14.TabIndex = 75
        '
        'cboCompany
        '
        Me.cboCompany.FormattingEnabled = true
        Me.cboCompany.Location = New System.Drawing.Point(95, 88)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(287, 21)
        Me.cboCompany.Sorted = true
        Me.cboCompany.TabIndex = 74
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = true
        Me.lblCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 10!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblCompany.Location = New System.Drawing.Point(11, 88)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(45, 17)
        Me.lblCompany.TabIndex = 73
        Me.lblCompany.Text = "Plant"
        '
        'Label8
        '
        Me.Label8.AutoSize = true
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label8.Location = New System.Drawing.Point(192, 120)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 17)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = "Current "
        '
        'cboConsultant
        '
        Me.cboConsultant.FormattingEnabled = true
        Me.cboConsultant.Location = New System.Drawing.Point(261, 119)
        Me.cboConsultant.MaxLength = 3
        Me.cboConsultant.Name = "cboConsultant"
        Me.cboConsultant.Size = New System.Drawing.Size(121, 21)
        Me.cboConsultant.TabIndex = 70
        '
        'lblWarning
        '
        Me.lblWarning.AutoSize = true
        Me.lblWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(82, 7)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(108, 13)
        Me.lblWarning.TabIndex = 69
        Me.lblWarning.Text = "Not Current Study"
        Me.lblWarning.Visible = false
        '
        'Label60
        '
        Me.Label60.AutoSize = true
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 10!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label60.Location = New System.Drawing.Point(11, 58)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(65, 17)
        Me.Label60.TabIndex = 68
        Me.Label60.Text = "RefNum"
        '
        'cboRefNum
        '
        Me.cboRefNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRefNum.FormattingEnabled = true
        Me.cboRefNum.Location = New System.Drawing.Point(96, 59)
        Me.cboRefNum.Name = "cboRefNum"
        Me.cboRefNum.Size = New System.Drawing.Size(90, 21)
        Me.cboRefNum.TabIndex = 67
        '
        'Label59
        '
        Me.Label59.AutoSize = true
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label59.Location = New System.Drawing.Point(11, 27)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(49, 17)
        Me.Label59.TabIndex = 66
        Me.Label59.Text = "Study"
        '
        'cboStudy
        '
        Me.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStudy.FormattingEnabled = true
        Me.cboStudy.Location = New System.Drawing.Point(96, 28)
        Me.cboStudy.Name = "cboStudy"
        Me.cboStudy.Size = New System.Drawing.Size(90, 21)
        Me.cboStudy.TabIndex = 64
        '
        'Help
        '
        Me.Help.HelpNamespace = "help\Console2014.chm"
        '
        'OlefinsConsole
        '
        Me.AllowDrop = true
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PapayaWhip
        Me.ClientSize = New System.Drawing.Size(1004, 726)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.txtVersion)
        Me.Controls.Add(Me.ConsoleTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.MaximizeBox = false
        Me.Name = "OlefinsConsole"
        Me.Text = "Olefins Console"
        Me.tabClippy.ResumeLayout(false)
        Me.tabClippy.PerformLayout
        CType(Me.dgClippyResults,System.ComponentModel.ISupportInitialize).EndInit
        Me.tabDD.ResumeLayout(false)
        Me.tabDD.PerformLayout
        CType(Me.dgFiles,System.ComponentModel.ISupportInitialize).EndInit
        Me.tabSS.ResumeLayout(false)
        Me.tabSS.PerformLayout
        Me.tabTimeGrade.ResumeLayout(false)
        Me.tabTimeGrade.PerformLayout
        CType(Me.dgHistory,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.dgSummary,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.dgGrade,System.ComponentModel.ISupportInitialize).EndInit
        Me.tabCorr.ResumeLayout(false)
        Me.tabCorr.PerformLayout
        Me.tabCheckList.ResumeLayout(false)
        Me.tabCheckList.PerformLayout
        Me.tabNotes.ResumeLayout(false)
        Me.tabNotes.PerformLayout
        Me.tabContacts.ResumeLayout(false)
        Me.Panel2.ResumeLayout(false)
        Me.pnlCompany.ResumeLayout(false)
        Me.pnlCompany.PerformLayout
        CType(Me.ACEmail2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CCEmail1,System.ComponentModel.ISupportInitialize).EndInit
        Me.MainPanel.ResumeLayout(false)
        Me.pnlRefinery.ResumeLayout(false)
        Me.pnlRefinery.PerformLayout
        CType(Me.RMEmail,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RACEmail,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RCEmail,System.ComponentModel.ISupportInitialize).EndInit
        Me.ConsoleTabs.ResumeLayout(false)
        Me.TabPage1.ResumeLayout(false)
        Me.TabPage1.PerformLayout
        CType(Me.dgCorr,System.ComponentModel.ISupportInitialize).EndInit
        Me.Panel3.ResumeLayout(false)
        Me.Panel3.PerformLayout
        Me.Panel4.ResumeLayout(false)
        Me.Panel5.ResumeLayout(false)
        Me.Panel5.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents txtVersion As System.Windows.Forms.TextBox
    Friend WithEvents VersionToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents btnKillProcesses As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents btnBug As System.Windows.Forms.Button
    Friend WithEvents tabClippy As System.Windows.Forms.TabPage
    Friend WithEvents btnQueryQuit As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents chkSQL As System.Windows.Forms.CheckBox
    Friend WithEvents dgClippyResults As System.Windows.Forms.DataGridView
    Friend WithEvents btnClippySearch As System.Windows.Forms.Button
    Friend WithEvents txtClippySearch As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents tabDD As System.Windows.Forms.TabPage
    Friend WithEvents optCompanyCorr As System.Windows.Forms.CheckBox
    Friend WithEvents txtMessageFilename As System.Windows.Forms.TextBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents btnGVClear As System.Windows.Forms.Button
    Friend WithEvents btnFilesSave As System.Windows.Forms.Button
    Friend WithEvents dgFiles As System.Windows.Forms.DataGridView
    Friend WithEvents tabSS As System.Windows.Forms.TabPage
    Friend WithEvents btnDirRefresh As System.Windows.Forms.Button
    Friend WithEvents btnSecureSend As System.Windows.Forms.Button
    Friend WithEvents tvwCompCorr2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwDrawings2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwClientAttachments2 As System.Windows.Forms.TreeView
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboDir2 As System.Windows.Forms.ComboBox
    Friend WithEvents tvwClientAttachments As System.Windows.Forms.TreeView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDir As System.Windows.Forms.ComboBox
    Friend WithEvents tvwDrawings As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence As System.Windows.Forms.TreeView
    Friend WithEvents tabTimeGrade As System.Windows.Forms.TabPage
    Friend WithEvents btnSection As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgGrade As System.Windows.Forms.DataGridView
    Friend WithEvents tabCorr As System.Windows.Forms.TabPage
    Friend WithEvents btnReceiptAck As System.Windows.Forms.Button
    Friend WithEvents lstReturnFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents lstVRFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents lstVFFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents lstVFNumbers As System.Windows.Forms.ListBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents tabCheckList As System.Windows.Forms.TabPage
    Friend WithEvents tvIssues As System.Windows.Forms.TreeView
    Friend WithEvents txtIssueID As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelIssue As System.Windows.Forms.Button
    Friend WithEvents btnUpdateIssue As System.Windows.Forms.Button
    Friend WithEvents lblItemCount As System.Windows.Forms.Label
    Friend WithEvents btnAddIssue As System.Windows.Forms.Button
    Friend WithEvents cboCheckListView As System.Windows.Forms.ComboBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents ValCheckList As System.Windows.Forms.CheckedListBox
    Friend WithEvents tabNotes As System.Windows.Forms.TabPage
    Friend WithEvents btnUnlockPN As System.Windows.Forms.Button
    Friend WithEvents btnCreatePN As System.Windows.Forms.Button
    Friend WithEvents btnSavePN As System.Windows.Forms.Button
    Friend WithEvents txtNotes As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents tabContacts As System.Windows.Forms.TabPage
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlCompany As System.Windows.Forms.Panel
    Friend WithEvents ACEmail2 As System.Windows.Forms.PictureBox
    Friend WithEvents CCEmail1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents MainPanel As System.Windows.Forms.Panel
    Friend WithEvents pnlRefinery As System.Windows.Forms.Panel
    Friend WithEvents RMEmail As System.Windows.Forms.PictureBox
    Friend WithEvents RACEmail As System.Windows.Forms.PictureBox
    Friend WithEvents RCEmail As System.Windows.Forms.PictureBox
    Friend WithEvents ConsoleTabs As System.Windows.Forms.TabControl
    Friend WithEvents btnGradeExit As System.Windows.Forms.Button
    Friend WithEvents OriginalFilename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Filenames As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FileDestination As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents txtIssueName As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnSS As System.Windows.Forms.Button
    Friend WithEvents btnValidate As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboConsultant As System.Windows.Forms.ComboBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents cboRefNum As System.Windows.Forms.ComboBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents cboStudy As System.Windows.Forms.ComboBox
    Friend WithEvents cboNotes As System.Windows.Forms.ComboBox
    Friend WithEvents btnMain As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgHistory As System.Windows.Forms.DataGridView
    Friend WithEvents dgSummary As System.Windows.Forms.DataGridView
    Friend WithEvents btnCompanyPW As System.Windows.Forms.Button
    Friend WithEvents btnReturnPW As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents dgCorr As System.Windows.Forms.DataGridView
    Friend WithEvents lstFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents btnBuildVRFile As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnRAM As System.Windows.Forms.Button
    Friend WithEvents btnVI As System.Windows.Forms.Button
    Friend WithEvents btnSC As System.Windows.Forms.Button
    Friend WithEvents btnPA As System.Windows.Forms.Button
    Friend WithEvents btnCT As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnDrawings As System.Windows.Forms.Button
    Friend WithEvents btnHYC As System.Windows.Forms.Button
    Friend WithEvents btnPT As System.Windows.Forms.Button
    Friend WithEvents btnSpecFrac As System.Windows.Forms.Button
    Friend WithEvents btnUnitReview As System.Windows.Forms.Button
    Friend WithEvents Cycle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValFaxes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValReply As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReturnFiles As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinWorked As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Consultant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Help As System.Windows.Forms.HelpProvider
    Friend WithEvents lblOSIMUpload As System.Windows.Forms.Label
    Friend WithEvents lblOU As System.Windows.Forms.Label
    Friend WithEvents lblCalcUpload As System.Windows.Forms.Label
    Friend WithEvents lblCU As System.Windows.Forms.Label
    Friend WithEvents lblItemsRemaining As System.Windows.Forms.Label
    Friend WithEvents lblBlueFlags As System.Windows.Forms.Label
    Friend WithEvents lblRedFlags As System.Windows.Forms.Label
    Friend WithEvents lblLastCalcDate As System.Windows.Forms.Label
    Friend WithEvents lblLastUpload As System.Windows.Forms.Label
    Friend WithEvents lblLastFileSave As System.Windows.Forms.Label
    Friend WithEvents lblValidationStatus As System.Windows.Forms.Label
    Friend WithEvents lblLU As System.Windows.Forms.Label
    Friend WithEvents lblLFS As System.Windows.Forms.Label
    Friend WithEvents lblLCD As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblDataEmail As System.Windows.Forms.Label
    Friend WithEvents lblDataCoordinator As System.Windows.Forms.Label
    Friend WithEvents lblPricingEmail As System.Windows.Forms.Label
    Friend WithEvents lblPricingName As System.Windows.Forms.Label
    Friend WithEvents lblPCPhone As System.Windows.Forms.Label
    Friend WithEvents lblPCEmail As System.Windows.Forms.Label
    Friend WithEvents lblPCName As System.Windows.Forms.Label
    Friend WithEvents lblACPhone As System.Windows.Forms.Label
    Friend WithEvents lblAltEmail As System.Windows.Forms.Label
    Friend WithEvents lblAltName As System.Windows.Forms.Label
    Friend WithEvents lblCCPhone As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents btnShowAltCo As System.Windows.Forms.Button
    Friend WithEvents btnShow As System.Windows.Forms.Button


End Class
