﻿Public Class AddIssue
    Dim ds As DataSet
    Dim db As New DataAccess()
    Private _UserName As String
    Public Property UserName() As String
        Get
            Return _UserName
        End Get
        Set(ByVal value As String)
            _UserName = value
        End Set
    End Property

    Private _StudyType As String
    Public Property StudyType() As String
        Get
            Return _StudyType
        End Get
        Set(ByVal value As String)
            _StudyType = value
        End Set
    End Property
    Private _RefNum As String
    Public Property RefNum() As String
        Get
            Return _RefNum
        End Get
        Set(ByVal value As String)
            _RefNum = value
        End Set
    End Property
    Private mDBConn As String
    Public Property DBConn() As String
        Get
            Return mDBConn
        End Get
        Set(ByVal value As String)
            mDBConn = value
        End Set
    End Property
    Event AddIssueToChecklist()


    Private Sub btnAddIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnAddIssue.Click
        db.DBConnection = mDBConn
        Dim params As List(Of String)
        ' Be sure they gave us an IssueID and an IssueTitle
        If txtIssueID.Text = "" Or txtIssueTitle.Text = "" Then
            MsgBox("Please enter both an ID and a Title")
            Exit Sub
        End If

        ' Drop any single quotes in ID, Title, and Text.
        txtIssueID.Text = Utilities.DropQuotes(txtIssueID.Text)
        txtIssueTitle.Text = Utilities.DropQuotes(txtIssueTitle.Text)
        txtIssueText.Text = Utilities.DropQuotes(txtIssueText.Text)

        params = New List(Of String)
        params.Add("RefNum/" + _RefNum)
        params.Add("IssueID/" + txtIssueID.Text)
        ' Be sure it does not already exist.
        ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                MsgBox("That IssueID already exists for this Refnum.", vbOKOnly)
                Exit Sub
            End If

        End If

        ' Everything looks OK, add it here.
        'Study Combo Box
        params = New List(Of String)
        params.Add("RefNum/" + _RefNum)
        params.Add("IssueID/" + txtIssueID.Text)
        params.Add("IssueTitle/" + txtIssueTitle.Text)
        params.Add("IssueText/" + txtIssueText.Text)
        params.Add("UserName/" + _UserName)
        ds = db.ExecuteStoredProc("Console." & "AddIssue", params)
        RaiseEvent AddIssueToChecklist()
        Me.Close()


    End Sub



End Class