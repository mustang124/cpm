﻿Imports System.Web
Imports System.Net.Mail
Imports SA.Internal.Console.DataObject
Imports System.Collections
Imports TFSWorkItem
Imports SA.Console.MainConsole
Imports System.Configuration

Public Class frmBug

    Private _study As Main.VerticalType

    Private _username As String
    Public Property UserName() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property

    Private _refnum As ComboBox.ObjectCollection
    Public Property RefNum() As ComboBox.ObjectCollection
        Get
            Return _refnum
        End Get
        Set(ByVal value As ComboBox.ObjectCollection)
            _refnum = value
        End Set
    End Property

    Private _conxString As String = String.Empty


    Public Sub New(vertical As Main.VerticalType, currentrefnum As String, username As String, connectionString As String)
        InitializeComponent()

        Me.AllowDrop = True

        _study = vertical
        _username = username
        _refnum = RefNum
        _conxString = connectionString

        cboType.SelectedIndex = 0

        'lblStudy.Text = _study.ToString()
        lblStudy.Text = vertical.ToString()
        cboRefNum.Text = currentrefnum

    End Sub


    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSubmit_Click(sender As System.Object, e As System.EventArgs) Handles btnSubmit.Click
        Me.Cursor = Cursors.WaitCursor
        'Dim requestID As String = InsertRequest()
        InsertTFSWorkItem()
        'doesn't work: SendEmail(requestID, _username, cboPriority.Text, txtChangeRequest.Text & vbCrLf & vbCrLf & " -" & UserName & " - " & Now.ToString)
        MsgBox("Request Submitted.", MsgBoxStyle.Information)
        Me.Cursor = Cursors.Default
    End Sub
    'Private Function GetStudyType(sType As String) As DataObject.StudyTypes
    '    Dim ST As DataObject.StudyTypes
    '    Select Case sType
    '        Case "EUR", "PAC", "LUB", "NSA"
    '            ST = DataObject.StudyTypes.REFINING
    '    End Select
    '    Return ST
    'End Function
    Private Function InsertRequest() As String
        Dim rt As String = "0"
        Dim ST As DataObject.StudyTypes
        ST = ConsoleVertical ' lblStudy.Text
        'TODO: CHANGE (rrh)
        Dim db As New DataObject(ST, "rrh", "rrh#4279")
        db.ConnectionString = _conxString
        Dim ps As New List(Of String)
        ps.Add("@Study/" & lblStudy.Text)
        ps.Add("@Priority/" & cboType.Text)
        ps.Add("@Request/" & txtDescription.Text)
        ps.Add("@Status/1 - REQUESTED")
        ps.Add("@Requestor/" & _username)

        Dim requestID As Integer
        requestID = db.ExecuteScalar("Console.SubmitRequest", ps)
        InsertTFSWorkItem()
        Return requestID
    End Function

    Private Sub InsertTFSWorkItem()
        Try
            Dim WorkItem As New TFSWorkItem.TFSWorkItem
            'WorkItem.Server = "http://dbs7:8080/tfs/CPA_20130129"
            'WorkItem.Project = "Data Management"
            'If cboType.Text = "Bug" Then
            '    WorkItem.WorkItemType = "Bug"
            'Else
            '    WorkItem.WorkItemType = "Task"
            'End If

            'WorkItem.WorkItemText = txtDescription.Text
            Dim rtn As Boolean ' = String.Empty
            Dim files As List(Of String)
            Dim tfsProjectCollectionUrl As String = String.Empty
            Dim tfsProjectName As String = String.Empty
            Dim tfsIterationPath As String = String.Empty
            Dim tfsProductBacklogId As String = String.Empty
            Dim tfsAreaPath As String = String.Empty
            Try
                tfsProjectCollectionUrl = ConfigurationManager.AppSettings("TfsProjectCollectionUrl").ToString()
                tfsProjectName = ConfigurationManager.AppSettings("TfsProjectName").ToString()
                tfsIterationPath = ConfigurationManager.AppSettings("TfsIterationPath").ToString()
                tfsProductBacklogId = ConfigurationManager.AppSettings("TfsProductBacklogId").ToString()
                tfsAreaPath = ConfigurationManager.AppSettings("TfsAreaPath").ToString()
            Catch exConfig As System.Exception
                MsgBox("Couldn't find all config entries needed for TFS")
                Exit Sub
            End Try
            Dim productBacklogId As Integer = Int32.Parse(tfsProductBacklogId)
            Dim requestType As String = "Task"
            If cboType.Text <> "Request" Then requestType = "Bug"
            Dim title As String = requestType + ": " + txtTitle.Text + " -  " + _study.ToString() + " - " + cboRefNum.Text
            If lstFiles.Items.Count > 0 Then
                files = New List(Of String)
                For Each item In lstFiles.Items
                    files.Add(item.ToString())
                Next
                rtn = WorkItem.InsertWorkItem(tfsProjectCollectionUrl, tfsProjectName, requestType,
                             title, txtDescription.Text, tfsIterationPath, tfsAreaPath,
                              productBacklogId, files)
            Else
                rtn = WorkItem.InsertWorkItem(tfsProjectCollectionUrl, tfsProjectName, requestType,
                                             title, txtDescription.Text, tfsIterationPath, tfsAreaPath,
                                              productBacklogId)
            End If
        Catch Ex As System.Exception
            MessageBox.Show("There was a problem logging your request in TFS: " + Ex.Message)
        End Try
    End Sub

    Private Sub SendEmail(RequestID As String, From As String, Priority As String, ChangeRequest As String)

        Dim MailTo As String = My.MySettings.Default.ErrorMailTo

        Dim fromAddress As New MailAddress(From & "@solomononline.com")
        If cboType.Text.Substring(0, 1) = "4" Then _username = "jdw@solomononline.com"
        Dim toAddress As New MailAddress(MailTo)
        Dim message As New MailMessage(fromAddress, toAddress)
        Select Case Priority.Substring(0, 1)
            Case "1", "2"
                message.Priority = MailPriority.High
            Case "3"
                message.Priority = MailPriority.Normal

            Case Else
                message.Priority = MailPriority.Low
        End Select

        Dim mailSender As SmtpClient = New SmtpClient("EX1.dc1.solomononline.com", 25)
        With mailSender
            '     CHANGE
            .Credentials = New Net.NetworkCredential("rogge.heflin", "solomon2012")
            .UseDefaultCredentials = False
            .EnableSsl = True

        End With

        message.Bcc.Add(fromAddress)
        message.Subject = "Change Request #" & RequestID & "  -  Priority: " & Priority
        message.IsBodyHtml = False
        message.Body = ChangeRequest


        Try
            mailSender.Send(message)
            MsgBox("Request Submitted.", MsgBoxStyle.Information)
            Me.Close()
        Catch ex As SystemException
            MsgBox(ex.ToString, MsgBoxStyle.Critical)
            Throw New SystemException(ex.ToString)
        End Try

    End Sub


    'Private Sub cboStudy_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    cboRefNum.Items.Clear()
    '    'TODO: CHANGE (rrh)
    '    Dim db As New SA.Internal.Console.DataObject.DataObject(DataObject.StudyTypes.REFINING, "rrh", "rrh#4279")
    '    db.ConnectionString = _conxString
    '    Dim ds As DataSet
    '    Dim params As List(Of String)
    '    params = New List(Of String)
    '    Dim studyYear As String = String.Empty
    '    Select Case _study
    '        Case VerticalType.REFINING
    '            studyYear = "20" + lblStudy.Text.Substring(3, 2)
    '        Case VerticalType.NGPP
    '            studyYear = lblStudy.Text
    '        Case Else
    '            studyYear = "20" + lblStudy.Text.Substring(3, 2)
    '    End Select

    '    params.Add("StudyYear/" + studyYear)
    '    params.Add("Study/" + lblStudy.Text.Substring(0, 3))

    '    ds = db.ExecuteStoredProc("Console." & "GetRefNums", params)
    '    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '        For Each r In ds.Tables(0).Rows
    '            cboRefNum.Items.Add(r(0).ToString)
    '        Next
    '        cboRefNum.SelectedIndex = 0
    '    End If
    'End Sub
    Private Sub frmBug_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles Me.DragDrop
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop)
        For Each path In files
            lstFiles.Items.Add(path)
            'MsgBox(path)
        Next
    End Sub

    Private Sub frmBug_DragEnter(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles Me.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub
End Class


