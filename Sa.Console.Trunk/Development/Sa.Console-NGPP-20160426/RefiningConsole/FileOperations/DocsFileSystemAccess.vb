﻿Imports System.Configuration
Imports System

Public Class DocsFileSystemAccess

    Dim _vertical As VerticalType 'Olefins, Refining, etc.
    Dim _errorLogType As String
    Private _profilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + "\"
    Private _profileConsolePath As String = _profilePath + "Console\"
    Private _templatePath As String = String.Empty ' "K:\STUDY\Olefins\2015\Correspondence\!Validation\Templates\"
    Private _dropdownChoice As String = String.Empty

    Public ReadOnly Property Vertical() As VerticalType 'Implements DocsAccessInterfaces.Vertical
        Get
            Return _vertical
        End Get
        'Set(ByVal value As String)
        '    _vertical = value
        'End Set
    End Property

    Public Property ErrorLogType() As String 'Implements DocsAccessInterfaces.ErrorLogType
        Get
            Return _errorLogType
        End Get
        Set(value As String)
            _errorLogType = value
        End Set
    End Property

    Public Sub New(Vertical As VerticalType)
        _vertical = Vertical
        If Vertical = VerticalType.OLEFINS Then
            Try
                _templatePath = System.Configuration.ConfigurationManager.AppSettings("OlefinsTemplatePath").ToString()
            Catch ex As System.Exception
                Throw New System.Exception("Unable to find app setting for _templatePath in DocsFileSystemAccess.vb")
            End Try
        End If
    End Sub

#Region "Correspondence Tab"
    Private Sub PopulateVFFiles(ByVal CorrPath As String, ByVal CorrPathV As String,
                                ByVal SelectedItemString As String, ByRef VFFiles As List(Of Object))
        'called by lstVRFilesItems() below
        Dim strDirResult As String
        strDirResult = Dir(CorrPath + CorrPathV)
        Do While strDirResult <> ""
            If SelectedItemString = Mid(strDirResult, 3, 1) Then
                Dim found As Boolean = False
                For Each item As String In VFFiles
                    If item.Contains(strDirResult) Then
                        found = True
                        Exit For
                    End If
                Next
                If Not found Then
                    VFFiles.Add(Format(FileDateTime(CorrPath & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult))
                End If
            End If
            strDirResult = Dir()
        Loop
    End Sub
    Public Function lstVRFilesItems(ByVal CorrPath As String, ByVal SelectedItemString As String, _
                                    shortRefNum As String) As List(Of Object) 'Implements DocsAccessInterfaces.lstVRFilesItems
        If Me.Vertical = VerticalType.OLEFINS Then
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, "20" + shortRefNum)
            Return mfiles.lstVRFilesItems(String.Empty, SelectedItemString)
        Else
            'Use the VFFiles to populate lstVRFiles
            Dim vFFiles As New List(Of Object)
            PopulateVFFiles(CorrPath, "VF*", SelectedItemString, vFFiles)
            PopulateVFFiles(CorrPath, "VR*", SelectedItemString, vFFiles)
            '------------------
            ' Added 1/30/2006 by FRS
            ' Include any VA files in lstVRFiles so the user can see whether
            ' the VF has been ack'ed.
            PopulateVFFiles(CorrPath, "VA*", SelectedItemString, vFFiles)
            PopulateVFFiles(CorrPath, "*Return*", SelectedItemString, vFFiles)
            Return vFFiles
        End If
    End Function

    Public Function lstVFilesItems(ByVal path As String, ByVal VFileTypeAndVNumber As String, _
                                   Optional shortRefNum As String = "") As List(Of Object)
        Dim vFFiles As New List(Of Object)
        Dim strDirResult As String
        If Not path.EndsWith("\") Then path = path + "\"
        strDirResult = Dir(path + VFileTypeAndVNumber + "*")
        Do While strDirResult <> ""
            vFFiles.Add(File.GetLastWriteTime(path + strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult)
            'vFFiles.Add(Format(path & strDirResult).ToString("dd-MMM-yy") & " | " & strDirResult)
            strDirResult = Dir()
        Loop
        Return vFFiles
    End Function

    Public Sub btnReceiptAck_Click(lstVFNumbersText As String, mUserName As String, shortRefNum As String)
        Dim strDirResult As String
        Dim strDateTimeToPrint As String
        If IsNothing(lstVFNumbersText) Then lstVFNumbersText = String.Empty

        Dim mfiles As DocsMFilesAccess = Nothing
        If Me.Vertical = VerticalType.OLEFINS Then
            mfiles = New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, "20" + shortRefNum)
            'reoving the '".txt" part because not part of name in mfiles
            If mfiles.DocExists("VA" + lstVFNumbersText, shortRefNum, True) Then
                MsgBox("Receipt of VF" & lstVFNumbersText & " has already been noted.")
                Exit Sub
            End If
        Else
            strDirResult = Dir(CorrPath & "VA" & lstVFNumbersText & ".txt")
            If strDirResult <> "" Then
                MsgBox("Receipt of VA" & lstVFNumbersText & " has already been noted.")
                Exit Sub
            End If
        End If

        strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        If strDateTimeToPrint <> "" Then
            If Me.Vertical = VerticalType.OLEFINS Then
                Dim path As String = _profileConsolePath & "VA" & lstVFNumbersText & ".txt"
                Dim objWriter As New System.IO.StreamWriter(path, True)
                objWriter.WriteLine(mUserName)
                objWriter.WriteLine(strDateTimeToPrint)
                objWriter.Close()
                mfiles.UploadNewDocToMFiles(path, 4, -1)
            Else
                Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & lstVFNumbersText & ".txt", True)
                objWriter.WriteLine(mUserName)
                objWriter.WriteLine(strDateTimeToPrint)
                objWriter.Close()
            End If
            MsgBox("VA" & lstVFNumbersText & ".txt has been built.")
        Else
            MsgBox("Receipt Acknowledgement was canceled.")
        End If
    End Sub

    Public Sub ValFax_Build(ByRef strValFaxTemplate As String,
        ByRef UserName As String,
        TemplatePath As String,
        ByRef btnValFaxText As String,
        TempDrive As String,
        ByRef mUserName As String,
        RefNum As String,
        ByRef db As SA.Internal.Console.DataObject.DataObject,
        StudyDrive As String,
        StudyYear As String,
        StudyRegion As String,
        Study As String,
        TempPath As String,
        ByRef CurrentRefNum As String,
        CorrPathParameter As String,
        ByRef TemplateValues As Object,
        ByRef VFFileName As String,
        shortRefNumForMfiles As String,
        longRefNumForMfiles As String) 'Implements DocsAccessInterfaces.ValFax_Build
        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.
        Dim strDeadline As String
        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""

        Try
            If Me.Vertical = VerticalType.OLEFINS Then
                TemplateValues = TryCast(TemplateValues, OlefinsMainConsole.WordTemplate)
                Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumForMfiles, longRefNumForMfiles)
                'see if any VFs. Then popup the deadline box
                Dim vfExists As Boolean = mfiles.DocExists("V", "20" + RefNum, True)
                If Not vfExists Then
                    intNumDays = 10
                Else
                    intNumDays = 5
                End If
                Dim DeadlineDays As String = InputBox("How many business days do you want to give them to respond?", "", intNumDays)
                TemplatePath = _templatePath
                mfiles.ValFax_Build(strValFaxTemplate, UserName, TemplatePath, btnValFaxText, mUserName,
                                    RefNum, db, StudyDrive, StudyYear, StudyRegion, Study, TempPath, CurrentRefNum,
                                    CorrPathParameter, DeadlineDays, TemplateValues, VFFileName)
            ElseIf Me.Vertical = VerticalType.NGPP Then
                TemplateValues = TryCast(TemplateValues, SA.Console.WordTemplate)
                ValFax_Build_NGPP(strValFaxTemplate, UserName, TemplatePath, btnValFaxText, TempDrive, mUserName, RefNum, db, StudyDrive, _
                                  StudyYear, StudyRegion, Study, TempPath, CurrentRefNum, CorrPathParameter, TemplateValues, VFFileName, _
                                  shortRefNumForMfiles, longRefNumForMfiles)
            Else 'not mfiles or NGPP
                If Dir(CorrPath & "vf*") = "" Then
                    intNumDays = 10
                Else
                    intNumDays = 5
                End If
                Dim DeadlineDays As String = InputBox("How many business days do you want to give them to respond?", "", intNumDays)
                If File.Exists(_profileConsolePath + "ValFax.txt") Then File.Delete(_profileConsolePath + "\ValFax.txt")
                If strValFaxTemplate = "" Then
                    ' Find out which template they want to use.
                    strProgress = "cboTemplates.Clear"
                    frmTemplates.cboTemplates.Items.Clear()
                    strDirResult = Dir(TemplatePath & "*.doc*")
                    If strDirResult = "" Then
                        MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)
                        Exit Sub
                    End If
                    strProgress = "Loading cboTemplates"
                    Do While strDirResult <> ""
                        frmTemplates.cboTemplates.Items.Add(strDirResult)
                        'Debug.Print strDirResult, frmTemplates.cboTemplates.ListCount
                        strDirResult = Dir()
                    Loop
                    If frmTemplates.cboTemplates.Items.Count < 1 Then
                        MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)
                        Exit Sub
                    End If
                    ' Default to first one.
                    frmTemplates.cboTemplates.SelectedIndex = 0
                    ' If there is only one, don't make them choose,
                    ' just go.
                    If frmTemplates.cboTemplates.Items.Count > 1 Then
                        'MsgBox "Show form modal"
                        ' We use this form for other purposes as well (like selecting which SpecFrac file to open).
                        ' So, reload the caption and label items to the origian "ValFax" values.
                        frmTemplates.Text = "Select a Cover Template"
                        ' Ready to show them the form.
                        frmTemplates.ShowDialog()
                        If frmTemplates.OKClick = False Then
                            Exit Sub
                        End If
                        ' Load strValFaxTemplate from what they selected on
                        ' frmTemplates. (strTemplateFile is a global variable.)
                        strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem
                        'MsgBox "After show form modal"
                    Else
                        strValFaxTemplate = frmTemplates.cboTemplates.SelectedItem
                    End If
                End If
                ' At this point we have the name of the template to use.
                'MsgBox "Prepare new doc"
                btnValFaxText = "Preparing New Document"
                strProgress = "Preparing New Document"
                ' Create the file to pass the data in.
                'Open gstrTempFolder & "ValFax.txt" For Output As #1
                'If Dir(TempDrive & _UserName & "\Console\", vbDirectory) = "" Then
                '    MkDir(TempDrive & _UserName & "\Console\")
                'End If
                Dim objWriterPath As String = String.Empty
                If Vertical = VerticalType.OLEFINS Then
                    objWriterPath = _profileConsolePath
                Else
                    objWriterPath = TempDrive & mUserName & "\Console\"
                End If
                'If Not Directory.Exists(TempDrive & mUserName & "\Console\") Then Directory.CreateDirectory(TempDrive & mUserName & "\Console\")
                If Not Directory.Exists(objWriterPath) Then Directory.CreateDirectory(objWriterPath)
                'Dim objWriter As New System.IO.StreamWriter(TempDrive & mUserName & "\Console\ValFax.txt", True)
                Dim objWriter As New System.IO.StreamWriter(objWriterPath + "ValFax.txt", True)
                ' Todays date
                strProgress = "Printing Long Date"
                objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
                TemplateValues.Field.Add("_TodaysDate")
                TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))
                TemplateValues.Field.Add("_VFNumber")
                If Me.Vertical = VerticalType.OLEFINS Then
                    TemplateValues.RField.Add(OlefinsMainConsole.NextVF().ToString())
                End If
                strProgress = "Getting business days to respond."
                ' Due date
                ' Need a way to make sure we get valid input here.

                'SB all this moved to calling app:
                'If Dir(CorrPathParameter & "vf*") = "" Then
                '    intNumDays = 10
                'Else
                '    intNumDays = 5
                'End If
                'strDeadline = InputBox("How many business days do you want to give them to respond?", "", intNumDays)
                If DeadlineDays = "" Then
                    objWriter.Close()
                    Exit Sub
                End If
                datDeadline = Utilities.ValFaxDateDue(DeadlineDays)
                objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
                TemplateValues.Field.Add("_DueDate")
                TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
                strProgress = "Getting Company name"
                ' Company Name
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)
                If ds.Tables.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    objWriter.WriteLine(row("Company"))
                    TemplateValues.Field.Add("_Company")
                    TemplateValues.RField.Add(row("Company").ToString)
                    strProgress = "Getting Location"
                    ' Refinery Name
                    objWriter.WriteLine(row("Location"))
                    TemplateValues.Field.Add("_Plant")
                    TemplateValues.RField.Add(row("Location").ToString)
                    Coloc = row("Coloc").ToString
                End If
                strProgress = "Getting Contact info"
                ' Contact Name
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
                If ds.Tables.Count = 0 Then
                    If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                        btnValFaxText = "New Validation Fax"
                        Exit Sub
                    Else
                        strFaxName = "NAME UNAVAILABLE"
                        strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                    End If
                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        strFaxName = row("NameFull").ToString
                        strFaxEmail = row("Email").ToString
                    End If
                End If
                objWriter.WriteLine(strFaxName)
                TemplateValues.Field.Add("_Contact")
                TemplateValues.RField.Add(strFaxName)
                ' Contact Email Address
                objWriter.WriteLine(strFaxEmail)
                TemplateValues.Field.Add("_EMail")
                TemplateValues.RField.Add(strFaxEmail)
                strProgress = "Getting consultant info"
                ' Consultant Name
                params = New List(Of String)
                params.Add("Initials/" + mUserName)
                ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        If row("EmployeeName") Is Nothing Or IsDBNull(row("EmployeeName")) Then
                            strName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
                        Else
                            strName = row("EmployeeName").ToString
                        End If
                    Else
                        strName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
                    End If
                End If
                If strName = "" Then
                    objWriter.Close()
                    Exit Sub
                End If
                objWriter.WriteLine(strName)
                TemplateValues.Field.Add("_ConsultantName")
                TemplateValues.RField.Add(strName)
                ' Consultant Initials
                objWriter.WriteLine(mUserName)
                TemplateValues.Field.Add("_Initials")
                TemplateValues.RField.Add(mUserName)
                strProgress = "Path to store it in"
                ' Path to store it in
                ' If it is Trans Pricing or FLCOMP, put it, without VF#, into Refinery Corr folder.
                If strValFaxTemplate.Substring(0, 17) = "Trans Pricing.doc" Or strValFaxTemplate.Substring(strValFaxTemplate.Length - 10, 10) = "FLCOMP.doc" Then
                    Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\" & Coloc & "\"
                    ' If it has "Company Wide" in the name, store it in the Company Corr folder
                ElseIf InStr(1, strValFaxTemplate, "Company Wide") > 0 Then
                    Filename = StudyDrive & StudyYear & "\Company Correspondence\" & Coloc & "\"
                    ' Otherwise -- MOST CASES HERE -- store it in the Refinery Corr folder with VF# on the front.                Else
                    ' Request from DEJ 3/2011 -- if it a Lubes refinery, put "Lube" at end of doc name.
                    If Me.Vertical = VerticalType.OLEFINS Then
                        Filename = StudyDrive & StudyYear & "\Correspondence\" & RefNum & "\VF" & OlefinsMainConsole.NextVF() & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                    End If
                End If
                objWriter.WriteLine(Filename)
                ' --------- Add new items here 4/20/2007 ---------
                strProgress = "Getting Company Contact info"
                ' Company Contact Name
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                params.Add("ContactType/COORD")
                params.Add("StudyYear/" & StudyYear)
                ds = db.GetCompanyContacts(params)
                If ds.Tables.Count = 0 Then
                    If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                        btnValFaxText = "New Validation Fax"
                        Exit Sub
                    Else
                        strFaxName = "NAME UNAVAILABLE"
                        strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                    End If
                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        row = ds.Tables(0).Rows(0)
                        strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                        strFaxEmail = row("Email").ToString
                    End If
                End If
                objWriter.WriteLine(strFaxName)
                TemplateValues.Field.Add("_CoCoContact")
                TemplateValues.RField.Add(strFaxName)
                ' Company Contact Email Address
                objWriter.WriteLine(strFaxEmail)
                TemplateValues.Field.Add("_CoCoEMail")
                TemplateValues.RField.Add(strFaxEmail)
                objWriter.Close()
                ' --------- End of new items 4/20/2007 ---------
                ' Make sure the template file is available to copy.
                'On Error GoTo File1NotAvailable
                'intAttr = GetAttr(strTemplatePath & strValFaxTemplate)
                ' Next 2 lines commented 12/1/04 to try new approach
                ' due to problem with compression of network files.
                'SetAttr strTemplatePath & strValFaxTemplate, intAttr
                'On Error GoTo 0
                strProgress = "Copy doc to hard drive"
                ' Bring the doc down to the hard drive.
                If File.Exists(TempPath & strValFaxTemplate) Then
                    Kill(TempPath & strValFaxTemplate)
                End If
                If File.Exists(TemplatePath & strValFaxTemplate) Then
                    TemplatePath = TemplatePath
                End If
                ' Copy the Template to my work area.
                File.Copy(TemplatePath & strValFaxTemplate, TempPath & strValFaxTemplate, True)
                'varShellReturn = Shell("XCopy """ & strTemplatePath & strValFaxTemplate & """ """ & gstrTempFolder & """ /Y")
                'Stop
                timStart = Now()
                'MsgBox Now()
                Do While File.Exists(TempPath & strValFaxTemplate) = False
                    If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                        MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                        'Exit Do
                    End If
                Loop
            End If  'enf of not mfiles or NGPP
        Catch ex As System.Exception
            ErrorLogType = "Validation: " & CurrentRefNum.ToString()
            Throw ex
            '!!need to do this in calling method:  MessageBox.Show(ex.Message)
        Finally
            MSWord = Nothing
        End Try
    End Sub

    Private Sub ReturnChoice(sender As System.Object)
        _dropdownChoice = sender.ToString()
    End Sub

    Public Sub ValFax_Build_NGPP(ByRef strValFaxTemplate As String,
        ByRef UserName As String,
        TemplatePath As String,
        ByRef btnValFaxText As String,
        TempDrive As String,
        ByRef mUserName As String,
        RefNum As String,
        ByRef db As SA.Internal.Console.DataObject.DataObject,
        StudyDrive As String,
        StudyYear As String,
        StudyRegion As String,
        Study As String,
        TempPath As String,
        ByRef CurrentRefNum As String,
        CorrPathParameter As String,
        ByVal TemplateValues As Object,
        ByRef VFFileName As String,
        shortRefNumForMfiles As String,
        longRefNumForMfiles As String) 'Implements DocsAccessInterfaces.ValFax_Build
        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.
        Dim strDeadline As String
        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""

        Try
            'TemplateValues = TryCast(TemplateValues, SA.Console.WordTemplate)
            Dim networkTemplatePath As String = String.Empty
            Dim choice As DropdownForm
            Try
                networkTemplatePath = ConfigurationManager.AppSettings("NgppVFTemplatePath").ToString()
                Dim choices As New List(Of String)
                Dim folder As New DirectoryInfo(networkTemplatePath)
                Dim files = folder.GetFiles("*.docx")
                For count As Integer = 0 To files.Length - 1
                    choices.Add(files(count).Name)
                Next
                choice = New DropdownForm("Choose your VF template", choices)
                choice.choice = New DropdownForm.ReturnChoice(AddressOf ReturnChoice)
                _dropdownChoice = String.Empty
                choice.ShowDialog()
                If _dropdownChoice.Trim().Length < 1 Then
                    MsgBox("No template chosen. Quitting VF.")
                    Return
                End If
                strValFaxTemplate = _dropdownChoice 'networkTemplatePath + _dropdownChoice 'ConfigurationManager.AppSettings("NgppVFTemplateFile").ToString()
                If Not File.Exists(networkTemplatePath + strValFaxTemplate) Then
                    MsgBox("Can't find VF Template at " + networkTemplatePath + strValFaxTemplate)
                    Return
                End If
            Catch idrException As Exception
                MsgBox("VF Template Error: " + idrException.Message)
                Exit Sub
            Finally
                Try
                    choice.Close()
                Catch
                End Try
                choice = Nothing
            End Try

            strProgress = "Preparing New Document"
            ' Create the file to pass the data in.
            Dim objWriter As New System.IO.StreamWriter(networkTemplatePath & "ValFax.txt", True)
            ' Todays date
            strProgress = "Printing Long Date"
            objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
            TemplateValues = New SA.Console.WordTemplate
            TemplateValues.Field.Add("_TodaysDate")
            TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))
            strProgress = "Getting business days to respond."
            ' Due date
            ' Need a way to make sure we get valid input here.
            If Dir(CorrPath & "vf*") = "" Then
                intNumDays = 10
            Else
                intNumDays = 5
            End If
            strDeadline = InputBox("How many business days do you want to give them to respond?", "Deadline", intNumDays)
            datDeadline = Utilities.ValFaxDateDue(strDeadline)
            objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
            TemplateValues.Field.Add("_DueDate")
            TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
            strProgress = "Getting Company name"
            ' Company Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)
            If ds.Tables.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                objWriter.WriteLine(row("CompanyName"))
                TemplateValues.Field.Add("_Company")
                TemplateValues.RField.Add(row("CompanyName").ToString)
                strProgress = "Getting Location"
                ' Refinery Name
                objWriter.WriteLine(row("FacilityName"))
                TemplateValues.Field.Add("_Plant")
                TemplateValues.RField.Add(row("FacilityName").ToString)
                Coloc = row("Coloc").ToString
            End If
            strProgress = "Getting Contact info"
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim lfn As String = ""
                    For Each row In ds.Tables(0).Rows
                        If lfn <> row("RefName").ToString Then
                            strFaxName += row("RefName").ToString + IIf(ds.Tables(0).Rows.Count > 1, ", ", "")
                            lfn = row("RefName").ToString
                            strFaxEmail += row("RefEmail").ToString + IIf(ds.Tables(0).Rows.Count > 1, "; ", "")
                        Else
                            strFaxName = strFaxName.Replace(",", "")
                            strFaxEmail = strFaxEmail.Replace(";", "")
                        End If
                    Next
                End If
            End If
            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_Contact")
            TemplateValues.RField.Add(strFaxName)
            ' Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_EMail")
            TemplateValues.RField.Add(strFaxEmail)
            strFaxEmail = ""
            strFaxName = ""
            strProgress = "Getting consultant info"
            ' Consultant Name
            params = New List(Of String)
            params.Add("Initials/" + mUserName)
            params.Add("StudyYear/" + StudyYear)
            params.Add("StudyType/" + Study)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    If row("ConsultantName") Is Nothing Or IsDBNull(row("ConsultantName")) Then
                        strName = InputBox("I cannot find you in the employee table, please give me your name: ", "Employee data base")
                    Else
                        strName = row("ConsultantName").ToString
                    End If
                Else
                    strName = InputBox("I cannot find you in the employee table, please give me your full name: ", "Employee data base")
                End If
            End If
            objWriter.WriteLine(strName)
            TemplateValues.Field.Add("_ConsultantName")
            TemplateValues.RField.Add(strName)
            ' Consultant Initials
            objWriter.WriteLine(mUserName)
            TemplateValues.Field.Add("_Initials")
            TemplateValues.RField.Add(mUserName)
            strProgress = "Path to store it in"
            'Filename = StudyDrive & StudyYear & "\Correspondence\" & RefNum.Trim() & "\VF" & NextVF() & " " & Coloc & ".docx"

            Filename = CorrPath & "VF" & NextV("F", CurrentRefNum, CurrentRefNum) & " " & Coloc & ".docx"

            objWriter.WriteLine(Filename)
            strProgress = "Getting Company Contact info"
            ' Company Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("ContactType/Coord")
            params.Add("StudyYear/" + StudyYear)
            ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)
            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row In ds.Tables(0).Rows
                        strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                        strFaxEmail = row("Email").ToString
                    Next
                End If
            End If
            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_CoCoContact")
            TemplateValues.RField.Add(strFaxName)
            ' Company Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_CoCoEMail")
            TemplateValues.RField.Add(strFaxEmail)
            objWriter.Close()
            strProgress = "Copy doc to hard drive"
            ' Bring the doc down to the hard drive.


            'MsgBox("Expeting TempPath to be _profile")


            If File.Exists(TempPath & strValFaxTemplate) Then
                Kill(TempPath & strValFaxTemplate)
            End If
            ' Copy the Template to my work area.
            'File.Copy(localTemplatePath & strValFaxTemplate, _profileConsoleTempPath & strValFaxTemplate, True)
            File.Copy(networkTemplatePath & strValFaxTemplate, TempPath & strValFaxTemplate, True)
            timStart = Now()
            Do While File.Exists(TempPath & strValFaxTemplate) = False
                If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                    MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                End If
            Loop
            Utilities.WordTemplateReplace(TempPath & strValFaxTemplate, TemplateValues, Filename, 1)
            File.Delete(networkTemplatePath & "ValFax.txt")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function BuildVRFile(mUserName As String, strTextForFile As String, CorrPathVR As String,
                                 lstVFNumbersText As String, shortRefNum As String) As String 'Implements DocsAccessInterfaces.BuildVRFile
        If Me.Vertical = VerticalType.OLEFINS Then
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, "20" + shortRefNum)
            Return mfiles.BuildVRFile(mUserName, strTextForFile, CorrPathVR, lstVFNumbersText)
        Else
            Dim strDirResult As String
            Dim fullPath As String = CorrPathVR + "VR" + lstVFNumbersText + ".txt"
            strDirResult = Dir(fullPath)
            If strDirResult <> "" Then
                Return "There already is a VR" & lstVFNumbersText & " file."
            End If

            If strTextForFile <> "" Then
                Dim objWriter As New System.IO.StreamWriter(fullPath, True)
                objWriter.WriteLine(mUserName)
                objWriter.WriteLine(strTextForFile)
                objWriter.Close()
                Return "VR" & lstVFNumbersText & ".txt has been built."
            Else
                Return "VR file build was cancelled."
            End If
        End If
    End Function

    Public Function UploadDocToMfiles(fullPath As String, docClass As Integer, shortRefNum As String,
                                      deliverableType As Integer, Optional benchMarkingParticipant As Integer = -1,
                                       Optional DocumentDate As DateTime? = Nothing) As String
        Try
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, "20" + shortRefNum)
            If deliverableType = 12 Then
                Return mfiles.UploadNewIdr(fullPath)
            Else
                Return mfiles.UploadNewDocToMFiles(fullPath, deliverableType, benchMarkingParticipant, DocumentDate)
            End If
            Return True
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Sub btnBuildVRFile_Click(lstVFNumbersText As String, mUserName As String, shortRefNum As String)
        Dim strTextForFile As String

        If IsNothing(lstVFNumbersText) Then lstVFNumbersText = String.Empty

        Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNum, "20" + shortRefNum)
        'reoving the '".txt" part because not part of name in mfiles
        If mfiles.DocExists("VR" + lstVFNumbersText, shortRefNum, True) Then
            MsgBox("There already is a VR" & lstVFNumbersText & " file.")
            Exit Sub
        End If
        strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        Dim result As String = BuildVRFile(mUserName, strTextForFile, CorrPath, lstVFNumbersText, shortRefNum)
        MsgBox(result)

    End Sub

    Public Function GetDataFor_lstVFNumbers(filter As String, path As String, _
                                            numsFoundAlready As List(Of String), shortRefNumber As String, longRefNum As String) As List(Of String)

        '###################can use this in DocsMFilesAcces.vb:  GetDataFor_lstVFNumbers()

        If IsNothing(numsFoundAlready) Then
            numsFoundAlready = New List(Of String)
        End If

        'arg Path will be the Full paht from BuildCorrPath

        If Me.Vertical = VerticalType.OLEFINS Then
            Dim mfiles As New DocsMFilesAccess(Me.Vertical, shortRefNumber, longRefNum)
            numsFoundAlready = mfiles.GetDataFor_lstVFNumbers(filter, path, numsFoundAlready)
        Else  'NGPP, etc
            Dim dirResult As String
            dirResult = Dir(path & filter)
            Do While dirResult <> ""
                ' Get the next V file.
                Dim blnAlready As Boolean = False
                ' Is it already in the list?
                For Each refNum As String In numsFoundAlready
                    If refNum = Mid(dirResult, 3, 1) Then
                        blnAlready = True
                        Exit For
                    End If
                Next

                ' No, put it there
                If Not blnAlready Then
                    numsFoundAlready.Add(Mid(dirResult, 3, 1)) ' & Chr(9) & FileDateTime(strCorrPath & strDirResult)
                End If
                ' Look for next file
                dirResult = Dir()
            Loop

            ' Now look for any orphan VR files
            ' Same logic as above except VR.
        End If
        Return numsFoundAlready
    End Function

    Public Sub lstVFFiles_DoubleClick(fileName As String, pathToFile As String, shortRefNum As String, longRefNum As String)
        If Vertical = VerticalType.OLEFINS Then
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, longRefNum)
            mfiles.lstVFFiles_DoubleClick(fileName.Trim(), _profileConsolePath)
        Else
            Process.Start(pathToFile + fileName)
        End If
    End Sub

    Public Function DownloadFile(folderPath As String, docId As Integer, shortRefNum As String, longRefNum As String)
        Try
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, longRefNum)
            Return mfiles.DownloadFile(folderPath, docId)
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Private Function NextFile(findFile As String, findExt As String)
        Try
            Dim strDirResult As String
            Dim I As Integer
            For I = 1 To 20
                strDirResult = Dir(findFile & CStr(I) & "*." & findExt)
                If strDirResult = "" Then
                    Return I
                    Exit For
                End If
            Next I
            Return Nothing
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function NextV(filter As String, shortRefNum As String, longRefNum As String) As Integer
        If Vertical = VerticalType.OLEFINS Then
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, longRefNum)
            Return mfiles.NextV(filter, longRefNum)
        Else
            Dim strDirResult As String
            Dim I As Integer
            For I = 1 To 20
                strDirResult = Dir(CorrPath & "V" & filter & CStr(I) & "*.doc")
                If strDirResult = "" Then
                    strDirResult = Dir(CorrPath & "V" & filter & CStr(I) & "*.docx")
                    If strDirResult = "" Then
                        Return I
                        Exit For
                    End If
                End If
            Next I
            Return Nothing
        End If
    End Function

#End Region

    Public Function GetDocInfoByNameAndRefnum(fileName As String, shortRefNum As String, _
                                              longRefNum As String, startsWith As Boolean) As ConsoleFilesInfo
        Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNum, longRefNum)
        Return mfiles.GetDocByNameAndRefnum(fileName, longRefNum, False)
    End Function

    Public Function CheckOutToMe(docId As Integer, shortRefNumber As String, longRefNum As String) As Boolean
        Try
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
            mfiles.CheckOutToMe(docId)
            Return True
        Catch ex As System.Exception
            Return False
        End Try
    End Function

    Public Function CheckIn(docId As Integer, shortRefNumber As String, longRefNum As String) As Boolean
        Try
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
            mfiles.CheckIn(docId)
            Return True
        Catch ex As System.Exception
            Return False
        End Try
    End Function

    Public Function GetCheckedOutStatus(docId As Integer, shortRefNumber As String, longRefNum As String) As MFilesCheckOutStatus
        Try
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
            Return mfiles.GetCheckedOutStatus(docId)
        Catch ex As System.Exception
            Return Nothing
        End Try
    End Function

    Public Function OpenFileFromMfiles(fileNameWithoutExtension As String, shortRefNumber As String, longRefNum As String) As String
        Try
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
            Return mfiles.OpenFileFromMfiles(fileNameWithoutExtension)
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function OpenFileFromMfiles(docId As Integer, shortRefNumber As String, longRefNum As String) As String
        Try
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
            Return mfiles.OpenFileFromMfiles(docId)
        Catch ex As System.Exception
            Throw ex
        End Try
    End Function

    Public Function GetDocNamesAndIdsByBenchmarkingParticipant( _
                shortRefNumber As String, longRefNum As String) As ConsoleFilesInfo
        Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
        Return mfiles.GetDocNamesAndIdsByBenchmarkingParticipant(longRefNum)
    End Function

    'Public Function GetConsultantTabInfo(shortRefNumber As String, longRefNum As String, refNumIds As List(Of Integer), _
    '        Optional ByVal docNameStartsWithFilter As String = "", Optional ByVal docNameContainsFilter As String = "") As ConsoleFilesInfo
    '    Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
    '    Return mfiles.GetConsultantTabInfo(refNumIds, docNameStartsWithFilter, docNameContainsFilter)
    'End Function

    Public Function DocExists(fileName As String, shortRefNumber As String, _
                              longRefNum As String, startsWith As Boolean)
        Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
        Return mfiles.DocExists(fileName, longRefNum, startsWith)
    End Function

    Public Function NextReturnFile(shortRefNumber As String, _
                              longRefNum As String) As Integer
        If Vertical = VerticalType.OLEFINS Then
            Dim mfiles As New DocsMFilesAccess(VerticalType.OLEFINS, shortRefNumber, longRefNum)
            Return mfiles.NextReturnFile(longRefNum)
        ElseIf Vertical = VerticalType.NGPP Then
            Dim strDirResult As String
            Dim I As Integer
            For I = 0 To 20
                strDirResult = Dir(CorrPath & "*G_Return" & CStr(I) & "*.xls")
                If strDirResult = "" Then
                    strDirResult = Dir(CorrPath & "*G_Return" & CStr(I) & "*.xlsx")
                    If strDirResult = "" Then
                        Return I
                        Exit For
                    End If
                End If
            Next I
            Return Nothing
        Else
            Dim strDirResult As String
            Dim I As Integer
            For I = 0 To 20
                strDirResult = Dir(CorrPath & "*_Return" & CStr(I) & "*.xls")
                If strDirResult = "" Then
                    strDirResult = Dir(CorrPath & "*F_Return" & CStr(I) & "*.xlsx")
                    If strDirResult = "" Then
                        Return I
                        Exit For
                    End If
                End If
            Next I
            Return Nothing
        End If
    End Function

    Public Function FileExists(path As String) As Boolean
        Try
            Return File.Exists(path)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function FileSearchDirCommand(Optional path As String = "") As String
        'expecting wildcard char in path
        Return Dir(path)
    End Function

    Public Function FileDateTime(path As String) As String
        Return FileDateTime(path)
    End Function

    Public Sub FileMove(pathFrom As String, pathTo As String)
        Try
            File.Move(pathFrom, pathTo)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub FileDelete(path As String)
        Try
            File.Delete(path)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub WriteStream(path As String, append As Boolean, text As System.Text.StringBuilder)
        Dim stream As New StreamWriter(path, append)
        stream.Write(text.ToString())
        stream.Close()
    End Sub

    Public Function ReadStream(path As String) As String()
        Dim output As String()
        Dim count As Integer = 0
        Dim reader As StreamReader
        Try
            reader = New StreamReader(path)
            While Not reader.EndOfStream
                ReDim Preserve output(count)
                output(count) = reader.ReadLine()
            End While
        Catch ex As Exception
            Dim blank() As String
            Return blank
        Finally
            Try
                reader.Close()
            Catch
            End Try
        End Try
        Return output
    End Function

    Public Function OpenXlWorkbook(path As String, ByRef xl As Excel.Application, _
                                   Optional readAccess As Boolean = False) As Excel.Workbook
        xl = Utilities.GetExcelProcess()
        Dim w As Excel.Workbook = xl.Workbooks.Open(path, , readAccess)
        Return w
    End Function

    Public Function DirectoryExists(path As String) As Boolean
        'path = path.Remove(path.Length - 1, 1)
        Return Directory.Exists(path)
    End Function

    Public Sub CreateDirectory(path As String)
        Try
            Directory.CreateDirectory(path)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function FileCopy(pathFrom As String, pathTo As String, Optional overWrite As Boolean = True)
        Try
            File.Copy(pathFrom, pathTo, overWrite)
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Public Function GetFiles(path As String, Optional filter As String = "") As String()
        If filter = String.Empty Then filter = "*"
        Return Directory.GetFiles(path, filter)
    End Function

    'Public Function GetPathToFile(folderPath As String, fileName As String, Optional LongRefNum As String = "") As String
    '    If _vertical = VerticalType.NGPP Then
    '        Return folderPath + fileName
    '    Else
    '        'download from mfiles and return path to that.
    '        Return String.Empty
    '    End If
    'End Function

End Class



