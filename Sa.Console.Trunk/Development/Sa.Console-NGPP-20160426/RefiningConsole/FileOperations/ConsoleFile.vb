﻿Public Class ConsoleFile
    'use this for MFIles and for files on network
    Dim _usingMfiles As Boolean
    Dim _class As String = String.Empty
    Dim _fileName As String = String.Empty
    Dim _id As Integer
    Dim _deliverableType As String = String.Empty
    Dim _documentDate As Date = DateTime.MinValue
    Dim _filePath As String = String.Empty
    Dim _createdUtcDate As Date?
    Dim _ext As String = String.Empty

    Public Sub New(MFiles As Boolean)
        _usingMfiles = MFiles
    End Sub

    Public Property ClassType As String 'MFiles class
        Get
            Return _class
        End Get
        Set(ByVal value As String)
            _class = value
        End Set
    End Property

    Public Property FileName As String 'MFiles Filename
        Get
            Return _fileName
        End Get
        Set(ByVal value As String)
            _fileName = value
        End Set
    End Property

    Public Property Id As Integer 'MFiles File ID
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Public Property DeliverableType As String
        Get
            Return _deliverableType
        End Get
        Set(ByVal value As String)
            _deliverableType = value
        End Set
    End Property

    Public Property DocumentDate As DateTime?
        Get
            If _documentDate = DateTime.MinValue Then
                Return Nothing
            Else
                Return _documentDate
            End If
        End Get
        Set(ByVal value As DateTime?)
            If value <> DateTime.MinValue Then
                _documentDate = value
            End If
        End Set
    End Property

    Public Property FilePath As String 'for files on network
        Get
            Return _filePath
        End Get
        Set(ByVal value As String)
            _filePath = value
        End Set
    End Property

    Public Property CreatedUtcDate As Date?
        Get
            Return _createdUtcDate
        End Get
        Set(value As Date?)
            _createdUtcDate = value
        End Set
    End Property

    Public Property FileExtension As String
        Get
            Return _ext
        End Get
        Set(value As String)
            _ext = value
        End Set
    End Property

    Dim _checkedOutTo As Integer = 0
    Public Property CheckedOutTo As Integer
        Get
            Return _checkedOutTo
        End Get
        Set(value As Integer)
            _checkedOutTo = value
        End Set
    End Property

    Dim _checkedOutToUserName As String = String.Empty
    Public Property CheckedOutToUserName As String
        Get
            Return _checkedOutToUserName
        End Get
        Set(value As String)
            _checkedOutToUserName = value
        End Set
    End Property

    Private _lastModifiedUtcDate As Date
    Public Property LastModifiedUtcDate As Date
        Get
            Return _lastModifiedUtcDate
        End Get
        Set(value As Date)
            _lastModifiedUtcDate = value
        End Set
    End Property

End Class