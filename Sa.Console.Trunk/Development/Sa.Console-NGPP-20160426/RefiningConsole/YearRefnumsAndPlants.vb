﻿Imports System.Data
Imports SA.Internal.Console.DataObject

Public Class YearRefnumsAndPlants
    Implements IYearRefnumsAndPlants

    'Public Function ChangedByMouse(sender As ComboBox) As Boolean

    Private _refnumsAndPlants As DataSet
    Private _vertical As VerticalType
    Private _refnumWithoutYear As String

    Private _studyYear As String
    Private _refNum As String
    Private _plant As String

    Private _profilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + "\"
    Private _profileConsolePath As String = _profilePath + "Console\"

    Private _studyYears() As String

    'legacy
    'Private _studySave(20) As String  'used for Olefins only. Re
    'Private _refNumSave(20) As String  'used for Olefins only


    Public ReadOnly Property GetStudyYear As String Implements IYearRefnumsAndPlants.GetStudyYear
        Get
            Return _studyYear
        End Get
    End Property
    Public ReadOnly Property StudyYears As String() Implements IYearRefnumsAndPlants.StudyYears
        Get
            Return _studyYears
        End Get
    End Property

    Public Sub New(vertical As VerticalType, ByRef db As DataObject) ', studyYear As Integer)
        _vertical = vertical
        If _vertical = VerticalType.NGPP Then
            db.SQL = "Select distinct StudyYear from dbo.TSort order by StudyYear desc"
            Dim ds As DataSet = db.Execute()
            If Not IsNothing(ds) And ds.Tables.Count > 0 Then
                ReDim _studyYears(ds.Tables(0).Rows.Count - 1)
                Dim count As Integer = 0
                For Each row As DataRow In ds.Tables(0).Rows
                    _studyYears(count) = row("StudyYear").ToString()
                    count += 1
                Next
            End If
        End If

    End Sub

    Public Function ChangeYear(ByVal newYear As String, ByRef db As SA.Internal.Console.DataObject.DataObject, priorRefnum As String) As YearRefnumPlantsInfo Implements IYearRefnumsAndPlants.ChangeYear
        If _vertical = VerticalType.NGPP Then
            Dim result As New YearRefnumPlantsInfo()

            'chec if null yr.
            If newYear.Length < 1 Then
                'if not, do htis
                ReadLastStudy(result)
                If result.SelectedStudyYear.Length > 1 Then
                    newYear = result.SelectedStudyYear
                End If
                'else read study, then
            End If

            result.StudyYears = _studyYears ' PopulateStudyYears()
            If result.SelectedStudyYear.Length < 1 Then
                result.SelectedStudyYear = newYear ' result.StudyYears(0).ToString()
            End If

            If newYear.Length < 1 AndAlso result.SelectedStudyYear.Length > 1 Then
                newYear = result.SelectedStudyYear
            End If

            _studyYear = newYear

            Dim params As List(Of String)
            params = New List(Of String)
            Dim yearOnly As Integer = Int32.Parse(newYear.Substring(2, 2))
            Dim yearText As String = yearOnly.ToString()
            If yearText.Length = 1 Then yearText = yearText.PadLeft(2, "0")
            params.Add("StudyYear/" + "20" + yearText)
            params.Add("Study/NGP")

            If newYear.Length > 0 Then
                _refnumsAndPlants = db.ExecuteStoredProc("Console." & "GetRefNums", params)
                If IsNothing(_refnumsAndPlants) OrElse _refnumsAndPlants.Tables.Count < 1 OrElse _refnumsAndPlants.Tables(0).Rows.Count < 1 Then
                    Throw New System.Exception("No data available for finding refnums and colocs for year " + newYear)
                End If
                'Dim row As DataRow 'Represents a row of data in Systems.data.datatable
                'For Each row In ds.Tables(0).Rows
                '    If row("CoLoc").ToString.Trim.Length > 0 Then
                '        cboRefNum.Items.Add(row("RefNum").ToString.Trim)
                '        cboCompany.Items.Add(row("CoLoc").ToString.Trim)
                '    End If
                'Next
            Else
                ReadLastStudy(result)
            End If

            result.Plants = PopulatePlants()
            result.RefNums = PopulateRefnums()

            Dim selectedRefnum As String = String.Empty
            Dim newRefNumToSearchFor As String = String.Empty
            If _vertical = VerticalType.OLEFINS Then
                'expect priorRefnum like "15PCH999"
                If priorRefnum.Length > 0 Then
                    Dim new2digitYear As String = result.SelectedStudyYear.Substring(2, 2)
                    newRefNumToSearchFor = new2digitYear + priorRefnum.Trim().Substring(2, priorRefnum.Trim().Length - 2)
                    selectedRefnum = SameRefnumDifferentYear(newRefNumToSearchFor)
                End If
            ElseIf _vertical = VerticalType.NGPP Then
                If priorRefnum.Length > 0 Then
                    Dim new2digitYear As String = result.SelectedStudyYear.Substring(2, 2)
                    newRefNumToSearchFor = priorRefnum.Trim().Substring(0, priorRefnum.Trim().Length - 2) + new2digitYear
                    selectedRefnum = SameRefnumDifferentYear(newRefNumToSearchFor)
                    If selectedRefnum.Length < 1 Then
                        If priorRefnum.EndsWith("R") Then
                            'remove R and try again
                            Dim priorRefnumNoR As String = priorRefnum.TrimEnd("R")
                            newRefNumToSearchFor = priorRefnumNoR.Trim().Substring(0, priorRefnumNoR.Trim().Length - 2) + new2digitYear
                            selectedRefnum = SameRefnumDifferentYear(newRefNumToSearchFor)
                        Else
                            'retry with "R" at end
                            selectedRefnum = SameRefnumDifferentYear(newRefNumToSearchFor + "R")
                        End If
                    End If
                End If

                If selectedRefnum.Length > 0 Then
                    result.SelectedRefNum = selectedRefnum 'overwrite
                    Dim selectedPlant As String = GetMatchingPlant(selectedRefnum)
                    If selectedPlant.Length > 0 Then
                        result.SelectedPlant = selectedPlant 'overwrite
                    End If
                End If
            End If
            Return result
        End If
        Return Nothing
    End Function


    Public Function ChangeRefnum(newRefnum As String) As YearRefnumPlantsInfo Implements IYearRefnumsAndPlants.ChangeRefnum
        '_refnumWithoutYear = _refNum.Substring(3, _refNum.Length - 2)
        Dim result As New YearRefnumPlantsInfo()
        result.StudyYears = PopulateStudyYears()
        result.SelectedStudyYear = _studyYear

        result.RefNums = PopulateRefnums()
        result.SelectedRefNum = newRefnum

        result.Plants = PopulatePlants()
        result.SelectedPlant = GetMatchingPlant(newRefnum)

        Return result
    End Function

    Private Function GetMatchingPlant(refNum As String) As String
        If Not IsNothing(_refnumsAndPlants) AndAlso _refnumsAndPlants.Tables.Count = 1 AndAlso _refnumsAndPlants.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In _refnumsAndPlants.Tables(0).Rows
                If row("RefNum").ToString().Trim.ToUpper() = refNum.Trim().ToUpper() Then
                    Return row("CoLoc").ToString()
                End If
            Next
        Else
            Throw New System.Exception("No data available for matching a plant with RefNum " + refNum)
        End If
        Return String.Empty
    End Function

    Private Function SameRefnumDifferentYear(newRefNumToFind As String) As String
        Dim result As String = String.Empty
        If newRefNumToFind.Length > 0 Then
            If Not IsNothing(_refnumsAndPlants) AndAlso _refnumsAndPlants.Tables.Count = 1 AndAlso _refnumsAndPlants.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In _refnumsAndPlants.Tables(0).Rows
                    If row("RefNum").ToString().Trim.ToUpper() = newRefNumToFind.Trim().ToUpper() Then
                        result = newRefNumToFind
                        Exit For
                    End If
                Next
            End If
        End If
        Return result
    End Function

    Public Function ChangePlant(newPlant As String) As YearRefnumPlantsInfo Implements IYearRefnumsAndPlants.ChangePlant
        Dim result As New YearRefnumPlantsInfo()
        result.StudyYears = PopulateStudyYears()
        result.SelectedStudyYear = _studyYear

        result.Plants = PopulatePlants()
        result.SelectedPlant = newPlant

        'do I noew see if refnum changes or not? or will it alwasy chance with plant changes?
        result.RefNums = PopulateRefnums()
        Dim currentRefnum As String = _refNum
        If Not IsNothing(_refnumsAndPlants) AndAlso _refnumsAndPlants.Tables.Count = 1 AndAlso _refnumsAndPlants.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In _refnumsAndPlants.Tables(0).Rows
                If row("CoLoc").ToString().Trim.ToUpper() = newPlant.Trim().ToUpper() Then
                    result.SelectedRefNum = row("RefNum").ToString().Trim()
                    Exit For
                End If
            Next
        Else
            Throw New System.Exception("No data available for coloc " + newPlant)
        End If
        Return result
    End Function

    Private Function PopulateRefnums() As List(Of String)
        Dim results As New List(Of String)
        If Not IsNothing(_refnumsAndPlants) AndAlso _refnumsAndPlants.Tables.Count = 1 AndAlso _refnumsAndPlants.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In _refnumsAndPlants.Tables(0).Rows
                results.Add(row("RefNum").ToString().Trim())
            Next
        Else
            Throw New System.Exception("No data available for finding refnums")
        End If
        Return results
    End Function

    Private Function PopulatePlants() As List(Of String)
        Dim results As New List(Of String)
        If Not IsNothing(_refnumsAndPlants) AndAlso _refnumsAndPlants.Tables.Count = 1 AndAlso _refnumsAndPlants.Tables(0).Rows.Count > 0 Then
            'need this because cboCompany is Sorted
            Dim sortedView As DataView = New DataView(_refnumsAndPlants.Tables(0), _
                "", _
                    "CoLoc", _
                    DataViewRowState.CurrentRows)
            Dim sortedTable As DataTable = sortedView.ToTable()
            For Each row As DataRow In sortedTable.Rows
                results.Add(row("CoLoc").ToString().Trim())
            Next
        Else
            Throw New System.Exception("No data available for finding colocs")
        End If
        Return results
    End Function

    Private Function PopulateStudyYears() As String()
        'Used by Olefins Only. Replace with db lookup instead of hardcoding
        Return Nothing

        'Dim results As New List(Of String)
        'If _vertical = VerticalType.OLEFINS Then
        '    'results.Add("PCH15")
        '    'results.Add("PCH13")
        '    'results.Add("PCH11")
        '    'results.Add("PCH09")
        'ElseIf _vertical = VerticalType.NGPP Then
        '    results
        'End If
        'Return results
    End Function

    Private Sub ReadLastStudy(ByRef yearRefnumPlantsInfo As YearRefnumPlantsInfo)
        If _vertical = VerticalType.OLEFINS Then
            Dim objReader As IO.StreamReader
            Try
                Dim TempPath As String = _profileConsolePath & "Temp\"
                objReader = New IO.StreamReader(TempPath & "ConsoleSettings\Olefinssettings.txt")

                'For i = 0 To _studySave.Length - 1
                '    _studySave(i) = objReader.ReadLine()
                '    _refNumSave(i) = objReader.ReadLine()
                'Next

                Dim lastStudy As String = objReader.ReadLine()
                yearRefnumPlantsInfo.SelectedStudyYear = lastStudy
                'cboStudy.SelectedIndex = cboStudy.FindString(lastStudy)

                Dim rn As String = objReader.ReadLine()
                yearRefnumPlantsInfo.SelectedRefNum = rn
                'cboRefNum.Text = rn
                'RefNum = rn

            Catch ex As System.Exception
                'Err.WriteLog("ReadLastStudy", ex)
            Finally
                Try
                    objReader.Close()
                Catch
                End Try

            End Try
        End If

    End Sub

    Public Function ChangedByMouse(sender As ComboBox) As Boolean Implements IYearRefnumsAndPlants.ChangedByMouse
        Dim result As Boolean = False
        Select Case (sender.Name)
            Case "cboStudy"
                If sender.SelectedItem.ToString() <> _studyYear Then
                    result = True
                Else
                    result = False
                End If
            Case "cboCompany"
                If sender.SelectedItem.ToString() <> _plant Then
                    result = True
                Else
                    result = False
                End If
            Case "cboRefNum"
                If sender.SelectedItem.ToString() <> _refNum Then
                    result = True
                Else
                    result = False
                End If
        End Select
        Return result
    End Function

End Class



