﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace KNPCService
{
    public class DbConnectionCheck
    {

      
        public string ValidateConnection(string connectionString)
        {
            string connection = "Invalid Connection";
            string sql = "SELECT Count(*) FROM dbo.TSort"; //change per Joe to not expose RefNum
            string value = string.Empty;

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {

                    SqlCommand cmdReport = new SqlCommand(sql, sqlConnection);
                    cmdReport.CommandType = CommandType.Text;

                    sqlConnection.Open();
                    SqlDataReader reader = cmdReport.ExecuteReader();

                    if (reader.HasRows)
                    {
                        int index = 0;

                        while (reader.Read())
                        {
                            if (index == 0)
                            {
                                value = (reader[0] == DBNull.Value) ? null : Convert.ToString(reader[0]);
                                connection = "Validated count from table: TSort. Value =  " + value;
                                break;
                            }
                        }
                    }

                    sqlConnection.Close();

                }

                return connection;

           
            }
            catch (Exception ex)
            {
                string exception = " - Message: " + ex.Message;
                return connection + exception;
            }
           
        }
    }
}
