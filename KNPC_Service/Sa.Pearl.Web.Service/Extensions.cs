﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public static class Extensions
{
    /// <summary>
    /// Renders an exception with messages and stack traces
    /// </summary>
    /// <param name="ex">The exception to render</param>    
    /// <param name="noTrace">Whether trace information should be omitted</param>
    /// <returns>A string containing messages and stack traces</returns>
    public static string Render(this Exception ex, bool noTrace = false)
    {
        var s = new StringBuilder("\n");
        int i = 0;
        do
        {
            s.AppendFormat("{0:#\\. inner E;;E}xception ({1}):\n   {2}\n",
                i++,
                ex.GetType().Name,
                ex.Message.Replace("\n", "\n   "));
            if (ex is System.Data.Entity.Core.UpdateException)
            {
                foreach (var stateEntry in ((System.Data.Entity.Core.UpdateException)ex).StateEntries)
                {
                    var entity = stateEntry.Entity ?? new object();
                    s.AppendFormat("     {0} {1}: {2}\n", stateEntry.State, entity.GetType().Name, entity);
                    var values =
                        stateEntry.State == System.Data.Entity.EntityState.Deleted
                            ? stateEntry.OriginalValues
                            : stateEntry.CurrentValues;
                    for (int j = 0; j < values.FieldCount; j++)
                    {
                        var currentValue = values[j];
                        var originalValue =
                            stateEntry.State == System.Data.Entity.EntityState.Added
                                ? currentValue
                                : stateEntry.OriginalValues[j];
                        s.AppendFormat(originalValue.Equals(currentValue) ? "      {0}: <{1}>\n" : "      {0}: <{1}>→<{2}\n",
                                       values.GetName(j), originalValue, currentValue);
                    }
                }
            }
            s.AppendFormat(noTrace ? "\n" : "Trace:\n{0}\n", ex.StackTrace);
            ex = ex.InnerException;
        }
        while (ex != null);
        return s.ToString();
    }
}