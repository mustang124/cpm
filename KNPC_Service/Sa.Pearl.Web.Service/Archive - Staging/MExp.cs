//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KNPCService
{
    using System;
    using System.Collections.Generic;
    
    public partial class MExp
    {
        public int MExpID { get; set; }
        public int SubmissionID { get; set; }
        public Nullable<float> ComplexCptl { get; set; }
        public Nullable<float> NonRefExcl { get; set; }
        public Nullable<float> TAMaintCptl { get; set; }
        public Nullable<float> RoutMaintCptl { get; set; }
        public Nullable<float> NonMaintInvestExp { get; set; }
        public Nullable<float> NonRegUnit { get; set; }
        public Nullable<float> ConstraintRemoval { get; set; }
        public Nullable<float> RegExp { get; set; }
        public Nullable<float> RegGaso { get; set; }
        public Nullable<float> RegDiesel { get; set; }
        public Nullable<float> RegOth { get; set; }
        public Nullable<float> SafetyOth { get; set; }
        public Nullable<float> Energy { get; set; }
        public Nullable<float> OthInvest { get; set; }
        public Nullable<float> MaintExpTA { get; set; }
        public Nullable<float> MaintExpRout { get; set; }
        public Nullable<float> MaintExp { get; set; }
        public Nullable<float> MaintOvhdTA { get; set; }
        public Nullable<float> MaintOvhdRout { get; set; }
        public Nullable<float> MaintOvhd { get; set; }
        public Nullable<float> TotMaint { get; set; }
    
        public virtual Submission Submission { get; set; }
    }
}
