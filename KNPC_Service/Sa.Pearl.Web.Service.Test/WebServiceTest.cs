﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Reflection;
using Sa.Pearl.Client;
using output = Sa.dbs.Output;
using System.Collections;

namespace Sa.Pearl.Web.Service.Test
{
    [TestClass]
    public class WebServiceTest
    {
        [TestMethod]
        public void TestCheckService()
        {
            KNPCService.Service1 svc = new KNPCService.Service1();
            Assert.IsTrue(svc.CheckService().Contains("Successful"));
        }

        [TestMethod]
        public void TestSubmitDataAuthentication()
        {
            string submission = "<Submission></Submission>";
            string ClientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFpGvr9pwg75y9mDQTWftZpAPWv9HCWGJOBRgmdpN/+s4";
            //Assert.IsTrue(TestSubmitDataAuthenticationWithThisClientKey(ClientKey));
            //This should be debugged to test authentication



            //svc.SubmitData(
        }

        public bool TestSubmitDataAuthenticationWithThisClientKey(string clientKey)
        {
            //string submission = "<Submission></Submission>";
            try
            {
                KNPCService.Service1 svc = new KNPCService.Service1();
                byte[] clientKeyReader = Sa.Serialization.ToByteArray(clientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);
                byte[] submissionReader = Sa.Serialization.ToByteArray(new Sa.Pearl.ClientSubmission());
                byte[] encryptedSubmission = Sa.Crypto.Encrypt(submissionReader);
                svc.SubmitData(encryptedSubmission,encryptedClientKey);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [TestMethod]
        public void TestGetReferenceData2()
        {
            string msg = string.Empty;            
            string clientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFpGvr9pwg75y9mDQTWftZpAPWv9HCWGJOBRgmdpN/+s4";
            byte[] clientKeyReader = Sa.Serialization.ToByteArray(clientKey);
            byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);

            KNPCService.Service1 svc2 = new KNPCService.Service1();
            svc2.GetReference(encryptedClientKey, out msg);
            Assert.IsTrue(msg.Length == 0);
        }


        [TestMethod]
        public void TestGetKpis()
        {
            
            string submissionId = "137"; ; // "101"; // "32"; // "103";
            string msg = string.Empty;

            //MAA  163EUR
            string clientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWe6ZDsrdKkjik9bAyFj6y+4DZmWBjOpb4MmIQK+eDrG5yf/nLAhWMx";

            //MAB  166EUR
            //string clientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFpGvr9pwg75y9Mef8zMWSZtqwIi61iMyzN1+Y88eKz2clGbtfEOLgjc=";

            byte[] clientKeyReader = Sa.Serialization.ToByteArray(clientKey);
            byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);
            /*
            KNPCService.Service1 svc2 = new KNPCService.Service1();
            byte[] kpiInfo = null;
            svc2.GetKPI(kpiInfo, encryptedClientKey,out msg);
            Assert.IsTrue(msg.Length == 0);
            */
            List<String> errors = GetKPIsFromService(submissionId, clientKey);
            //Assert.IsTrue(errors.Count == 0);
            Assert.Inconclusive("Method GetKPIsFromService() uses code copied from Sa.Pearl.Client. So any changes to code in Sa.Pearl.Client should be replicated in GetKPIsFromService()");
        }

        private List<String> GetKPIsFromService(String submissionID, String ClientKey)
        {
            List<String> errors = new List<String>();
            try
            {
                ////convert the supplied submission ID string to int then encrypt it
                int intSubmissionID = Convert.ToInt32(submissionID);
                byte[] submissionIDReader = Sa.Serialization.ToByteArray(intSubmissionID);
                byte[] encryptedSubmissionID = Sa.Crypto.Encrypt(submissionIDReader);

                //encrypt the ClientKey
                byte[] clientKeyReader = Sa.Serialization.ToByteArray(ClientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);

                //submit to the web service
                String returnerror = string.Empty; 
                byte[] output = null;
                KNPCService.Service1 scProd = new KNPCService.Service1();
                output = scProd.GetKPI(encryptedSubmissionID, encryptedClientKey, out returnerror);
                if (returnerror.Length > 0)
                {
                    errors.Add(returnerror);
                    return errors;
                }
                ////decrypt the result and put it into a new submission object
                byte[] decryptedKPIs = Sa.Crypto.Decrypt(output);
                Sa.dbs.Output.ShortOutput returnedKPIs = new Sa.dbs.Output.ShortOutput();
                returnedKPIs = Sa.Serialization.ToObject<Sa.dbs.Output.ShortOutput>(decryptedKPIs);
                                
                    try
                    {
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"C:\temp\DataKeys.txt",false))
                        {
                            foreach (Sa.dbs.Output.SubmissionData sub in returnedKPIs.DataSubmissions)
                            {
                                sw.WriteLine(sub.DataKey + "|" + sub.NumberValue.ToString());
                            }

                        }
                    }
                    catch { }         



                //move all the items into the existing object
                Sa.dbs.Output.ShortOutput shortOut = new Sa.dbs.Output.ShortOutput();
                shortOut.DataKeys = returnedKPIs.DataKeys;
                shortOut.GroupKeys = returnedKPIs.GroupKeys;
                shortOut.DataSubmissions = returnedKPIs.DataSubmissions;
                shortOut.ProcessDatas = returnedKPIs.ProcessDatas;                 
                
               SaveKPIDataToXML(@"C:\Temp\Output.xml",shortOut);
                
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }
            return errors;
        }

         private List<String> SaveKPIDataToXML(String filepath ,   output.ShortOutput obj )
         {
             try{
             System.Xml.XmlWriterSettings setting = new System.Xml.XmlWriterSettings();
                setting.ConformanceLevel = ConformanceLevel.Auto;

                using (XmlWriter writer = XmlWriter.Create(filepath, setting))
                {
                    writer.WriteStartElement("KPIs");//start of the file

                    //write the elements that belong just to the Submission (have to write these separately)
                    foreach (PropertyInfo propertyInfo in this.GetType().GetProperties().Where(propertyInfo => propertyInfo.GetGetMethod().GetParameters().Count() == 0))
                    {

                        if (!propertyInfo.PropertyType.ToString().Contains("ICollection"))// to know if it is a collection or not
                        {
                            writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(this, null) ?? string.Empty).ToString());
                        }
                    }

                    ////then go through each table and add them one at a time (got to be a better way to do this, right?)
                    //foreach (output.Datum a in this.Data) { XMLStuff.WriteXML(a, "Data", writer); }
                    foreach (output.DataKey a in obj.DataKeys) { XMLStuff2.WriteXML(a, "DataKeys", writer); }
                    foreach ( output.SubmissionData a in obj.DataSubmissions) { XMLStuff2.WriteXML(a, "DataSubmissions", writer); }
                    foreach (output.GroupKey a in obj.GroupKeys) { XMLStuff2.WriteXML(a, "GroupKeys", writer); }
                    //foreach (dim.Energy_LU a in this.Energy_LU) { XMLStuff.WriteXML(a, "Energy_LU", writer); }
                    foreach (output.ProcessData a in obj.ProcessDatas) { XMLStuff2.WriteXML(a, "ProcessDatas", writer); }
                    foreach (output.Message a in obj.Messages) { XMLStuff2.WriteXML(a, "Messages", writer); }
                    //and write the end of the file
                    writer.WriteEndElement();
                    writer.Flush();
                }
                return new List<String>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
}

        [TestMethod]
        public void TestGetReferenceData()
        {
            //string clientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFpGvr9pwg75y9mDQTWftZpAPWv9HCWGJOBRgmdpN/+s4";
            string clientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFpGvr9pwg75y9mDQTWftZpAPWv9HCWGJOBRgmdpN/+s4";
            List<String> results = GetReferencesFromService(clientKey);
            //Assert.IsTrue(results.Count == 0);
            Assert.IsTrue(results.Count == 0);
            //Assert.Inconclusive("Method GetReferencesFromService() uses code copied from Sa.Pearl.Client. So any changes to code in Sa.Pearl.Client should be replicated in GetReferencesFromService()");
        }

        private List<String> GetReferencesFromService(String ClientKey)
        {
            List<String> errors = new List<String>();
            byte[] output = null;
            try
            {
                byte[] clientKeyReader = Sa.Serialization.ToByteArray(ClientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);
                String returnerror = "";
                KNPCService.Service1 scProd = new KNPCService.Service1();
                output = scProd.GetReference(encryptedClientKey, out returnerror);
                if (returnerror.Length > 0)
                {
                    errors.Add(returnerror);
                    return errors;
                }
                byte[] decryptedReferences = Sa.Crypto.Decrypt(output);
                Sa.dbs.Dim.ShortDim returnedReferences = new Sa.dbs.Dim.ShortDim();
                returnedReferences = Sa.Serialization.ToObject<Sa.dbs.Dim.ShortDim>(decryptedReferences);

                //move all the items into the existing object
                //we do it this way because we already have an object that the user is referencing, and we want to fill it with the data
                Sa.dbs.Dim.ShortDim shortDim = new Sa.dbs.Dim.ShortDim();
                shortDim.Absence_LU = returnedReferences.Absence_LU;
                //shortDim.CptlCode_LU = returnedReferences.CptlCode_LU;
                shortDim.Crude_LU = returnedReferences.Crude_LU;
                shortDim.EmissionType_LU = returnedReferences.EmissionType_LU;
                shortDim.Energy_LU = returnedReferences.Energy_LU;
                shortDim.Material_LU = returnedReferences.Material_LU;
                shortDim.OpEx_LU = returnedReferences.OpEx_LU;
                shortDim.Pers_LU = returnedReferences.Pers_LU;
                shortDim.PressureRange_LU = returnedReferences.PressureRange_LU;
                shortDim.Process_LU = returnedReferences.Process_LU;
                shortDim.ProductGrade_LU = returnedReferences.ProductGrade_LU;
                shortDim.TankType_LU = returnedReferences.TankType_LU;
                shortDim.UOM = returnedReferences.UOM;
                shortDim.YieldCategory_LU = returnedReferences.YieldCategory_LU;
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }
            return errors;
        }

        [TestMethod]
        public void TestGetSubmissionData()
        {
            string submissionId = "9";
            string clientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFpGvr9pwg75y9mDQTWftZpAPWv9HCWGJOBRgmdpN/+s4";
            List<String> results = GetSubmissionFromService(submissionId,clientKey);
            //Assert.IsTrue(results.Count == 0);
            Assert.Inconclusive("Method GetSubmissionFromService() uses code copied from Sa.Pearl.Client. So any changes to code in Sa.Pearl.Client should be replicated in GetSubmissionFromService()");
        }

        private List<String> GetSubmissionFromService(String submissionID, String ClientKey)
        {
            List<String> errors = new List<String>();
            byte[] output = null;
            try
            {
                int intSubmissionID = Convert.ToInt32(submissionID);
                byte[] submissionIDReader = Sa.Serialization.ToByteArray(intSubmissionID);
                byte[] encryptedSubmissionID = Sa.Crypto.Encrypt(submissionIDReader);//, eKey, Vector);
                byte[] clientKeyReader = Sa.Serialization.ToByteArray(ClientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);
                String returnerror = "";
                KNPCService.Service1 scProd = new KNPCService.Service1();
                output = scProd.GetSubmission(encryptedSubmissionID, encryptedClientKey, out returnerror);
                if (returnerror.Length > 0)
                {
                    errors.Add(returnerror);
                    return errors;
                }
                byte[] decryptedSubmission = Sa.Crypto.Decrypt(output);//, eKey, Vector);
                Sa.dbs.Stage.Submission returnedSubmission = new Sa.dbs.Stage.Submission();
                returnedSubmission = Sa.Serialization.ToObject<Sa.dbs.Stage.Submission>(decryptedSubmission);
                Sa.dbs.Stage.Submission tempSubmission = new Sa.dbs.Stage.Submission();
                tempSubmission.Absences = returnedSubmission.Absences;
                tempSubmission.Configs = returnedSubmission.Configs;
                tempSubmission.ConfigBuoys = returnedSubmission.ConfigBuoys;
                tempSubmission.ConfigRS = returnedSubmission.ConfigRS;
                tempSubmission.Crudes = returnedSubmission.Crudes;
                tempSubmission.Diesels = returnedSubmission.Diesels;
                tempSubmission.Emissions = returnedSubmission.Emissions;
                tempSubmission.Energies = returnedSubmission.Energies;
                tempSubmission.FiredHeaters = returnedSubmission.FiredHeaters;
                tempSubmission.Gasolines = returnedSubmission.Gasolines;
                tempSubmission.GeneralMiscs = returnedSubmission.GeneralMiscs;
                tempSubmission.Inventories = returnedSubmission.Inventories;
                tempSubmission.Kerosenes = returnedSubmission.Kerosenes;
                tempSubmission.LPGs = returnedSubmission.LPGs;
                tempSubmission.MaintRouts = returnedSubmission.MaintRouts;
                tempSubmission.MaintTAs = returnedSubmission.MaintTAs;
                tempSubmission.MarineBunkers = returnedSubmission.MarineBunkers;
                tempSubmission.MExps = returnedSubmission.MExps;
                tempSubmission.MiscInputs = returnedSubmission.MiscInputs;
                tempSubmission.OpExes = returnedSubmission.OpExes;
                tempSubmission.Personnels = returnedSubmission.Personnels;
                tempSubmission.ProcessDatas = returnedSubmission.ProcessDatas;
                tempSubmission.Resids = returnedSubmission.Resids;
                tempSubmission.RPFResids = returnedSubmission.RPFResids;
                tempSubmission.Steams = returnedSubmission.Steams;
                tempSubmission.Yields = returnedSubmission.Yields;
                foreach (Sa.dbs.Stage.Absence a in tempSubmission.Absences) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Config a in tempSubmission.Configs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.ConfigBuoy a in tempSubmission.ConfigBuoys) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.ConfigR a in tempSubmission.ConfigRS) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Crude a in tempSubmission.Crudes) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Diesel a in tempSubmission.Diesels) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Emission a in tempSubmission.Emissions) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Energy a in tempSubmission.Energies) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.FiredHeater a in tempSubmission.FiredHeaters) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Gasoline a in tempSubmission.Gasolines) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.GeneralMisc a in tempSubmission.GeneralMiscs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Inventory a in tempSubmission.Inventories) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Kerosene a in tempSubmission.Kerosenes) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.LPG a in tempSubmission.LPGs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.MaintRout a in tempSubmission.MaintRouts) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.MaintTA a in tempSubmission.MaintTAs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.MarineBunker a in tempSubmission.MarineBunkers) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.MExp a in tempSubmission.MExps) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.MiscInput a in tempSubmission.MiscInputs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.OpEx a in tempSubmission.OpExes) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Personnel a in tempSubmission.Personnels) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.ProcessData a in tempSubmission.ProcessDatas) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Resid a in tempSubmission.Resids) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.RPFResid a in tempSubmission.RPFResids) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Steam a in tempSubmission.Steams) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (Sa.dbs.Stage.Yield a in tempSubmission.Yields) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                tempSubmission.SubmissionID = returnedSubmission.SubmissionID;
                tempSubmission.ClientSubmissionID = returnedSubmission.ClientSubmissionID;
                tempSubmission.PeriodBeg = returnedSubmission.PeriodBeg;
                tempSubmission.PeriodEnd = returnedSubmission.PeriodEnd;
                tempSubmission.PeriodDuration_Days = returnedSubmission.PeriodDuration_Days;
                tempSubmission.RptCurrency = returnedSubmission.RptCurrency;
                tempSubmission.UOM = returnedSubmission.UOM;
                tempSubmission.RefineryID = returnedSubmission.RefineryID;
                tempSubmission.Notes = returnedSubmission.Notes;
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }
            return errors;
        }


        [TestMethod]
        public void TestSubmitData()
        {
            //Step 1:
            //LoadXML file
            //string xmlFilePath = @"C:\Users\sfb.DC1\Desktop\FailedSubmission2016-06-14.xml";
            //string xmlFilePath = @"C:\LastSubmission.xml"; //  
            
            
            string xmlFilePath = @"K:\Development\KNPC\SubmissionsFromCustomer\Adjusted File.xml"; //"K:\Development\KNPC\SubmissionsFromCustomer\MAA_SubmissionData_20_01Oct16_27Oct16011150.xml";
            
            //string xmlFilePath = @"K:\Development\KNPC\Test Submissions\MAA Submission 32.xml";
            
            string clientKey = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFpGvr9pwg75y9mDQTWftZpAPWv9HCWGJOBRgmdpN/+s4";
                //"QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFjPwo9kHMAY+uC/iBaNsXEYsKBnr59iCOPxU7c5FNBkC";
            int clientSubmissionId = 9999;
            Assert.IsFalse(xmlFilePath.Length<1);
            Assert.IsFalse(clientKey.Length<1);
            Assert.IsTrue(clientSubmissionId >0);
            Sa.Pearl.ClientSubmission submission = new ClientSubmission();
            List<String> results = submission.FlushSubmission();
            results = submission.LoadSubmissionFromXML(xmlFilePath);
            Assert.IsTrue(results.Count == 0);
            //step 2.
            //Submit file
            List<String> errors = new List<String>();
            //sa.dbs.stage.Submission
            Sa.dbs.Stage.Submission submissionToSend = new Sa.dbs.Stage.Submission();
                        
            PrivateObject prepObj = new PrivateObject(submission);
            object[] parms = new object[1];
            parms[0]=errors;
            submissionToSend=(Sa.dbs.Stage.Submission)prepObj.Invoke("PrepSubmissionToSend", parms);                
            
            ///note - MIGHT HAVE TO SEND AS ref
            //submissionToSend = submission.PrepSubmissionToSend(ref errors);
            submissionToSend.SubmissionID = submission.SubmissionID;
            submissionToSend.ClientSubmissionID = clientSubmissionId;
            submissionToSend.PeriodBeg = submission.PeriodBeg;
            submissionToSend.PeriodEnd = submission.PeriodEnd;
            submissionToSend.PeriodDuration_Days = submission.PeriodDuration_Days;
            submissionToSend.RptCurrency = submission.RptCurrency;
            submissionToSend.UOM = submission.UOM;
            submissionToSend.RefineryID = submission.RefineryID;
            submissionToSend.Notes = submission.Notes;
            byte[] reader = Sa.Serialization.ToByteArray(submissionToSend);
            byte[] encrypted = Sa.Crypto.Encrypt(reader);//, eKey, sKey, Vector);
            //encrypt the ClientKey
            byte[] clientKeyReader = Sa.Serialization.ToByteArray(clientKey);
            byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);

            KNPCService.Service1 wcf = new KNPCService.Service1();
            string TryToFindUrlHere = wcf.CheckService();
            bool checkToEnsureUsingLocalUrl = false;
            string wcfResult = string.Empty;
            if (checkToEnsureUsingLocalUrl)
            {
                wcfResult = wcf.SubmitData(encrypted, encryptedClientKey);
            }
            Assert.IsTrue(wcfResult.Length < 1);
        }

        [TestMethod]
        public void TestResubmitToDboFromStage()
        {
            Sa.Pearl.StagingToDbo stage = new Sa.Pearl.StagingToDbo();
            PrivateObject prepObj = new PrivateObject(stage);
            object[] parms = new object[3];
            parms[0]=288;
            parms[1] = "163EUR";
            int discard = 1;
            parms[2] = discard;
            //success = stagingToDbo.MoveStagingToDbo(stagingSubmissionid, refnum, ref dboSubmissionId);
            bool success = (bool)prepObj.Invoke("MoveStagingToDbo", parms);
            if (!success)
                Assert.Fail("Not successful");            
        }
    


    }    


    public class XMLStuff2
    {

        public static void WriteXML<T>(T a, string name, XmlWriter writer)
        {
            //generic method to write each table to XML
            writer.WriteStartElement(name);
            foreach (PropertyInfo p in a.GetType().GetProperties())
            {
                if (p.Name.ToString() != "Submission")
                {
                    writer.WriteElementString(p.Name.ToString(), (p.GetValue(a, null) ?? string.Empty).ToString());
                }
            }

            writer.WriteEndElement();

        }

        public static void WriteClassToXML(object theClass, XmlWriter writer)
        {

            Type objType = theClass.GetType();

            writer.WriteStartElement(objType.Name);

            PropertyInfo[] properties = objType.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                Type tColl = typeof(ICollection<>);
                Type t = property.PropertyType;
                string name = property.Name;


                object propValue = property.GetValue(theClass, null);
                //check for nested classes as properties
                if (property.PropertyType.Assembly == objType.Assembly)
                {
                    //string _result = string.Format("{0}{1}:", indentString, property.Name);
                    //log.Info(_result);
                    //LogObject(propValue, indent + 2);
                }
                else
                {
                    //string _result = string.Format("{0}{1}: {2}", indentString, property.Name, propValue);
                    //log.Info(_result);
                }

                //check for collection
                if (t.IsGenericType && tColl.IsAssignableFrom(t.GetGenericTypeDefinition()) ||
                    t.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == tColl))
                {





                    ////var get = property.GetGetMethod();
                    IEnumerable listObject = (IEnumerable)property.GetValue(theClass, null);
                    if (listObject != null)
                    {
                        foreach (object o in listObject)
                        {

                            writer.WriteStartElement(name);

                            foreach (PropertyInfo p in o.GetType().GetProperties())
                            {
                                writer.WriteElementString(p.Name.ToString(), (p.GetValue(o, null) ?? string.Empty).ToString());

                            }
                            //writer.WriteElementString(o.Name.ToString(), (o.GetValue(theClass, null) ?? string.Empty).ToString());
                            //LogObject(o, indent + 2);

                            writer.WriteEndElement();

                        }
                    }





                }
            }

            writer.WriteEndElement();

        }

    }
}
