﻿using System;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
	[TestClass]
	public class Crypto
	{
		const string LoremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
		readonly byte[] serializationLoremIpsum = Sa.Serialization.ToByteArray(LoremIpsum);

		readonly byte[] encryptionKey = Encoding.ASCII.GetBytes("{E365BCA2-7332-4CC7-B62A-B61703A2275C}");
		readonly byte[] encryptionSalt = Encoding.ASCII.GetBytes("{CFD1FB53-6E05-4123-B24B-5E6B704B8829}");
		readonly byte[] signatureKey = Encoding.ASCII.GetBytes("{67F18B3B-1F57-4C63-B2FF-A4FEF5BB1A7F}");

		readonly byte[] forgedKey = Encoding.ASCII.GetBytes("{61F8C741-5D50-447C-9FE0-69EA403F6B06}");
		readonly byte[] forgedSalt = Encoding.ASCII.GetBytes("{CDE10FF7-239E-4DA5-8FCE-0594878F0075}");
		readonly byte[] forgedSig = Encoding.ASCII.GetBytes("{193105A7-5ABE-4122-96F6-E026277735CE}");

		readonly byte[] shortSalt = new byte[4] { 0, 1, 2, 3 };

		[TestMethod]
		public void Compress()
		{
			byte[] c;
			byte[] d;

			Sa.Zip.Compress(this.serializationLoremIpsum, out c);

			Assert.IsTrue(this.serializationLoremIpsum.Length >= c.Length, "Sa.Zip.Compress(byte[], out byte[]) failed.");

			Sa.Zip.Expand(c, out d);

			Assert.IsTrue(c.Length <= d.Length, "Sa.Zip.Expand(byte[], out byte[]) failed.");

			Assert.IsTrue(this.serializationLoremIpsum.SequenceEqual(d), "Sa.Zip did not Compress/Expand correctly.");
		}

		[TestMethod]
		public void Serialization()
		{
			string d = Sa.Serialization.ToObject<string>(this.serializationLoremIpsum);

			Assert.AreEqual(LoremIpsum, d, false, "Sa.Serialization{} failed to Serialize/Deserialize the string '{0}'.", this.serializationLoremIpsum);
		}

		[TestMethod]
		public void BytesSplit_Serialization()
		{
			byte[] primary = Sa.Serialization.ToByteArray("Lorem ipsum dolor sit amet, ");
			byte[] remaining = Sa.Serialization.ToByteArray("consectetur adipiscing elit");
			byte[] concatenated = primary.Concat(remaining).ToArray();

			byte[] outPrimary;
			byte[] outRemaining;

			Sa.Bytes.Split(concatenated, primary.Length, out outPrimary, out outRemaining);

			Assert.IsTrue(primary.SequenceEqual(outPrimary));
			Assert.IsTrue(remaining.SequenceEqual(outRemaining));
		}

		[TestMethod]
		public void BytesSplit_EncodingASCII()
		{
			byte[] primary = Encoding.ASCII.GetBytes("Lorem ipsum dolor sit amet, ");
			byte[] remaining = Encoding.ASCII.GetBytes("consectetur adipiscing elit");
			byte[] concatenated = Encoding.ASCII.GetBytes("Lorem ipsum dolor sit amet, consectetur adipiscing elit");

			byte[] outPrimary;
			byte[] outRemaining;

			Sa.Bytes.Split(concatenated, primary.Length, out outPrimary, out outRemaining);

			Assert.IsTrue(primary.SequenceEqual(outPrimary));
			Assert.IsTrue(remaining.SequenceEqual(outRemaining));
		}

		[TestMethod]
		public void Signature()
		{
			byte[] emptySigned = Sa.Signature.Append(new byte[0], new byte[0]);
			Assert.IsTrue(emptySigned.Length == Sa.Signature.Length, "Actual signature length {0} does not equal expected signature length {1}.", emptySigned.Length, Sa.Signature.Length);
			Assert.IsTrue(Sa.Signature.Verify(new byte[0], new byte[0], emptySigned), "Sa.Signature.Verify(byte[], byte[], byte[]) failed to validate signature for new byte[0]).");
		}

		[TestMethod]
		public void Signature_Validate()
		{
			byte[] signedInfor = Sa.Signature.Append(this.signatureKey, this.serializationLoremIpsum);
			Assert.IsTrue(Sa.Signature.Verify(this.serializationLoremIpsum, this.signatureKey, signedInfor), "Sa.Signature.Verify(byte[], byte[], byte[]) failed to validate signature).");
		}

		[TestMethod]
		public void Signature_InValidate()
		{
			byte[] signedInfo = Sa.Signature.Append(this.signatureKey, this.serializationLoremIpsum);
			byte[] forgedInfo = Sa.Signature.Append(this.forgedSig, this.serializationLoremIpsum);

			Assert.IsFalse(Sa.Signature.Verify(this.serializationLoremIpsum, this.signatureKey, forgedInfo), "Sa.Signature.Verify(byte[], byte[], byte[]) failed to invalidate forged signed data).");
			Assert.IsFalse(Sa.Signature.Verify(this.serializationLoremIpsum, this.forgedSig, signedInfo), "Sa.Signature.Verify(byte[], byte[], byte[]) failed to invalidate forged key).");

			Assert.IsFalse(Sa.Signature.Verify(new byte[0], this.signatureKey, signedInfo), "Sa.Signature.Verify(byte[], byte[], byte[]) failed to invalidate unsiged data).");
		}

		[TestMethod]
		public void Aes_EncryptionDecryption()
		{
			byte[] plainText;
			byte[] cryptoText;

			Sa.AesCryptography.EncryptBytes(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, out cryptoText);
			Sa.AesCryptography.DecryptBytes(cryptoText, this.encryptionKey, this.encryptionSalt, out plainText);

			Assert.IsTrue(serializationLoremIpsum.SequenceEqual(plainText));
		}

		[TestMethod]
		public void Aes_EncryptionDecryption_forgedKey()
		{
			byte[] plainText;
			byte[] cryptoText;

			Sa.AesCryptography.EncryptBytes(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, out cryptoText);
			Sa.AesCryptography.DecryptBytes(cryptoText, this.forgedKey, this.encryptionSalt, out plainText);

			Assert.IsFalse(serializationLoremIpsum.SequenceEqual(plainText));
		}

		[TestMethod]
		public void Aes_EncryptionDecryption_forgedSalt()
		{
			byte[] plainText;
			byte[] cryptoText;

			Sa.AesCryptography.EncryptBytes(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, out cryptoText);
			Sa.AesCryptography.DecryptBytes(cryptoText, this.encryptionKey, this.forgedSalt, out plainText);

			Assert.IsFalse(serializationLoremIpsum.SequenceEqual(plainText));
		}

		[TestMethod]
		public void Aes_Encryption_SaltLength()
		{
			byte[] cryptoText;

			Sa.AesCryptography.EncryptBytes(this.serializationLoremIpsum, this.encryptionKey, this.shortSalt, out cryptoText);

			Assert.IsTrue(cryptoText.Length == 0);
		}

		[TestMethod]
		public void Aes_Decryption_SaltLength()
		{
			byte[] plainText;
			byte[] cryptoText;

			Sa.AesCryptography.EncryptBytes(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, out cryptoText);
			Sa.AesCryptography.DecryptBytes(cryptoText, this.encryptionKey, this.shortSalt, out plainText);

			Assert.IsTrue(plainText.Length == 0);
		}

		[TestMethod]
		public void Aes_Encryption_RandomIV()
		{
			byte[] cryptoTextA;
			byte[] cryptoTextB;

			Sa.AesCryptography.EncryptBytes(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, out cryptoTextA);
			Sa.AesCryptography.EncryptBytes(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, out cryptoTextB);

			Assert.IsFalse(cryptoTextA.SequenceEqual(cryptoTextB));
		}

		[TestMethod]
		public void Aes_EncryptDecrypt()
		{
			byte[] cryptoText = Sa.Crypto.Encrypt(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, this.signatureKey);
			byte[] plainText = Sa.Crypto.Decrypt(cryptoText, this.encryptionKey, this.encryptionSalt, this.signatureKey);

			Assert.IsTrue(serializationLoremIpsum.SequenceEqual(plainText));
		}

		[TestMethod]
		public void Aes_EncryptDecrypt_KeyFiles()
		{
			byte[] cryptoText = Sa.Crypto.Encrypt(serializationLoremIpsum);
			byte[] plainText = Sa.Crypto.Decrypt(cryptoText);

			Assert.IsTrue(serializationLoremIpsum.SequenceEqual(plainText));
		}

		[TestMethod]
		public void Aes_EncryptDecrypt_forgedSignature()
		{
			byte[] cryptoText = Sa.Crypto.Encrypt(this.serializationLoremIpsum, this.encryptionKey, this.encryptionSalt, this.signatureKey);
			byte[] plainText = Sa.Crypto.Decrypt(cryptoText, this.encryptionKey, this.encryptionSalt, this.forgedSig);

			Assert.IsFalse(serializationLoremIpsum.SequenceEqual(plainText));
		}
	}
}