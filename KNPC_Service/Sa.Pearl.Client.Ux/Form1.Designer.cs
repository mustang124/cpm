﻿namespace Sa.Pearl.Client.Ux
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnClearData = new System.Windows.Forms.Button();
            this.lstStatus = new System.Windows.Forms.ListBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SubmitRefineryDataFileButton = new System.Windows.Forms.Button();
            this.GetReferenceDataButton = new System.Windows.Forms.Button();
            this.btnRetrieveSubmission = new System.Windows.Forms.Button();
            this.RequestKPIResultsButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubmissionID = new System.Windows.Forms.TextBox();
            this.btnChooseClientKey = new System.Windows.Forms.Button();
            this.lblClientKey = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSaveToXML = new System.Windows.Forms.Button();
            this.btnLoadFromXML = new System.Windows.Forms.Button();
            this.btnLoadSavePath = new System.Windows.Forms.Button();
            this.txtLoadSavePath = new System.Windows.Forms.TextBox();
            this.btnSaveKPIsToXML = new System.Windows.Forms.Button();
            this.btnSaveReferenceToXML = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.applicationModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkWcfServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkDbConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnClearData
            // 
            this.btnClearData.Location = new System.Drawing.Point(253, 362);
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.Size = new System.Drawing.Size(178, 23);
            this.btnClearData.TabIndex = 17;
            this.btnClearData.Text = "Clear Submission Data";
            this.btnClearData.UseVisualStyleBackColor = true;
            this.btnClearData.Click += new System.EventHandler(this.btnClearData_Click);
            // 
            // lstStatus
            // 
            this.lstStatus.FormattingEnabled = true;
            this.lstStatus.HorizontalScrollbar = true;
            this.lstStatus.Location = new System.Drawing.Point(12, 411);
            this.lstStatus.Name = "lstStatus";
            this.lstStatus.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstStatus.Size = new System.Drawing.Size(679, 147);
            this.lstStatus.TabIndex = 20;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(9, 395);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(40, 13);
            this.lblStatus.TabIndex = 19;
            this.lblStatus.Text = "Status:";
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(552, 333);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 18;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // SubmitRefineryDataFileButton
            // 
            this.SubmitRefineryDataFileButton.Location = new System.Drawing.Point(253, 275);
            this.SubmitRefineryDataFileButton.Name = "SubmitRefineryDataFileButton";
            this.SubmitRefineryDataFileButton.Size = new System.Drawing.Size(178, 23);
            this.SubmitRefineryDataFileButton.TabIndex = 22;
            this.SubmitRefineryDataFileButton.Text = "Submit Refinery Data";
            this.SubmitRefineryDataFileButton.UseVisualStyleBackColor = true;
            this.SubmitRefineryDataFileButton.Click += new System.EventHandler(this.SubmitRefineryDataFileButton_Click);
            // 
            // GetReferenceDataButton
            // 
            this.GetReferenceDataButton.Location = new System.Drawing.Point(31, 246);
            this.GetReferenceDataButton.Name = "GetReferenceDataButton";
            this.GetReferenceDataButton.Size = new System.Drawing.Size(154, 23);
            this.GetReferenceDataButton.TabIndex = 23;
            this.GetReferenceDataButton.Text = "Retrieve Data";
            this.GetReferenceDataButton.UseVisualStyleBackColor = true;
            this.GetReferenceDataButton.Click += new System.EventHandler(this.GetReferenceDataButton_Click);
            // 
            // btnRetrieveSubmission
            // 
            this.btnRetrieveSubmission.Location = new System.Drawing.Point(253, 304);
            this.btnRetrieveSubmission.Name = "btnRetrieveSubmission";
            this.btnRetrieveSubmission.Size = new System.Drawing.Size(178, 23);
            this.btnRetrieveSubmission.TabIndex = 24;
            this.btnRetrieveSubmission.Text = "Retrieve Submission";
            this.btnRetrieveSubmission.UseVisualStyleBackColor = true;
            this.btnRetrieveSubmission.Click += new System.EventHandler(this.btnRetrieveSubmission_Click);
            // 
            // RequestKPIResultsButton
            // 
            this.RequestKPIResultsButton.Location = new System.Drawing.Point(512, 246);
            this.RequestKPIResultsButton.Name = "RequestKPIResultsButton";
            this.RequestKPIResultsButton.Size = new System.Drawing.Size(154, 23);
            this.RequestKPIResultsButton.TabIndex = 25;
            this.RequestKPIResultsButton.Text = "Retrieve KPI Results";
            this.RequestKPIResultsButton.UseVisualStyleBackColor = true;
            this.RequestKPIResultsButton.Click += new System.EventHandler(this.RequestKPIResultsButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Client Submission ID:";
            // 
            // txtSubmissionID
            // 
            this.txtSubmissionID.Location = new System.Drawing.Point(143, 182);
            this.txtSubmissionID.Name = "txtSubmissionID";
            this.txtSubmissionID.Size = new System.Drawing.Size(73, 20);
            this.txtSubmissionID.TabIndex = 26;
            // 
            // btnChooseClientKey
            // 
            this.btnChooseClientKey.Location = new System.Drawing.Point(27, 71);
            this.btnChooseClientKey.Name = "btnChooseClientKey";
            this.btnChooseClientKey.Size = new System.Drawing.Size(154, 22);
            this.btnChooseClientKey.TabIndex = 28;
            this.btnChooseClientKey.Text = "Choose Client Key File";
            this.btnChooseClientKey.UseVisualStyleBackColor = true;
            this.btnChooseClientKey.Click += new System.EventHandler(this.btnChooseClientKey_Click);
            // 
            // lblClientKey
            // 
            this.lblClientKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblClientKey.Location = new System.Drawing.Point(187, 71);
            this.lblClientKey.Name = "lblClientKey";
            this.lblClientKey.Size = new System.Drawing.Size(479, 22);
            this.lblClientKey.TabIndex = 29;
            this.lblClientKey.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "You must choose a Client Key File.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(312, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "You must enter a Submission ID to retrieve a Submission or KPIs.";
            // 
            // btnSaveToXML
            // 
            this.btnSaveToXML.Location = new System.Drawing.Point(253, 333);
            this.btnSaveToXML.Name = "btnSaveToXML";
            this.btnSaveToXML.Size = new System.Drawing.Size(178, 23);
            this.btnSaveToXML.TabIndex = 32;
            this.btnSaveToXML.Text = "Save Data To XML";
            this.btnSaveToXML.UseVisualStyleBackColor = true;
            this.btnSaveToXML.Click += new System.EventHandler(this.btnSaveToXML_Click);
            // 
            // btnLoadFromXML
            // 
            this.btnLoadFromXML.Location = new System.Drawing.Point(253, 246);
            this.btnLoadFromXML.Name = "btnLoadFromXML";
            this.btnLoadFromXML.Size = new System.Drawing.Size(178, 23);
            this.btnLoadFromXML.TabIndex = 33;
            this.btnLoadFromXML.Text = "Load Data From XML";
            this.btnLoadFromXML.UseVisualStyleBackColor = true;
            this.btnLoadFromXML.Click += new System.EventHandler(this.btnLoadFromXML_Click);
            // 
            // btnLoadSavePath
            // 
            this.btnLoadSavePath.Location = new System.Drawing.Point(253, 102);
            this.btnLoadSavePath.Name = "btnLoadSavePath";
            this.btnLoadSavePath.Size = new System.Drawing.Size(247, 23);
            this.btnLoadSavePath.TabIndex = 34;
            this.btnLoadSavePath.Text = "Choose XML Path To Load From/Save To";
            this.btnLoadSavePath.UseVisualStyleBackColor = true;
            this.btnLoadSavePath.Click += new System.EventHandler(this.btnLoadSavePath_Click);
            // 
            // txtLoadSavePath
            // 
            this.txtLoadSavePath.Location = new System.Drawing.Point(187, 131);
            this.txtLoadSavePath.Name = "txtLoadSavePath";
            this.txtLoadSavePath.Size = new System.Drawing.Size(479, 20);
            this.txtLoadSavePath.TabIndex = 35;
            // 
            // btnSaveKPIsToXML
            // 
            this.btnSaveKPIsToXML.Location = new System.Drawing.Point(512, 275);
            this.btnSaveKPIsToXML.Name = "btnSaveKPIsToXML";
            this.btnSaveKPIsToXML.Size = new System.Drawing.Size(154, 23);
            this.btnSaveKPIsToXML.TabIndex = 36;
            this.btnSaveKPIsToXML.Text = "Save KPI Data To XML";
            this.btnSaveKPIsToXML.UseVisualStyleBackColor = true;
            this.btnSaveKPIsToXML.Click += new System.EventHandler(this.btnSaveKPIsToXML_Click);
            // 
            // btnSaveReferenceToXML
            // 
            this.btnSaveReferenceToXML.Location = new System.Drawing.Point(31, 275);
            this.btnSaveReferenceToXML.Name = "btnSaveReferenceToXML";
            this.btnSaveReferenceToXML.Size = new System.Drawing.Size(154, 23);
            this.btnSaveReferenceToXML.TabIndex = 37;
            this.btnSaveReferenceToXML.Text = "Save Data to XML";
            this.btnSaveReferenceToXML.UseVisualStyleBackColor = true;
            this.btnSaveReferenceToXML.Click += new System.EventHandler(this.btnSaveReferenceToXML_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applicationModeToolStripMenuItem,
            this.checkWcfServiceToolStripMenuItem,
            this.checkDbConnectionToolStripMenuItem,
            this.logTestToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(703, 24);
            this.menuStrip1.TabIndex = 38;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // applicationModeToolStripMenuItem
            // 
            this.applicationModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.localToolStripMenuItem,
            this.devToolStripMenuItem,
            this.qAToolStripMenuItem,
            this.prodToolStripMenuItem});
            this.applicationModeToolStripMenuItem.Name = "applicationModeToolStripMenuItem";
            this.applicationModeToolStripMenuItem.Size = new System.Drawing.Size(114, 20);
            this.applicationModeToolStripMenuItem.Text = "Application Mode";
            this.applicationModeToolStripMenuItem.Visible = false;
            // 
            // localToolStripMenuItem
            // 
            this.localToolStripMenuItem.Name = "localToolStripMenuItem";
            this.localToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.localToolStripMenuItem.Text = "Local";
            this.localToolStripMenuItem.Click += new System.EventHandler(this.localToolStripMenuItem_Click);
            // 
            // devToolStripMenuItem
            // 
            this.devToolStripMenuItem.Name = "devToolStripMenuItem";
            this.devToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.devToolStripMenuItem.Text = "Dev";
            this.devToolStripMenuItem.Click += new System.EventHandler(this.devToolStripMenuItem_Click);
            // 
            // qAToolStripMenuItem
            // 
            this.qAToolStripMenuItem.Name = "qAToolStripMenuItem";
            this.qAToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.qAToolStripMenuItem.Text = "QA";
            this.qAToolStripMenuItem.Click += new System.EventHandler(this.qAToolStripMenuItem_Click);
            // 
            // prodToolStripMenuItem
            // 
            this.prodToolStripMenuItem.Name = "prodToolStripMenuItem";
            this.prodToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.prodToolStripMenuItem.Text = "Prod";
            this.prodToolStripMenuItem.Click += new System.EventHandler(this.prodToolStripMenuItem_Click);
            // 
            // checkWcfServiceToolStripMenuItem
            // 
            this.checkWcfServiceToolStripMenuItem.Name = "checkWcfServiceToolStripMenuItem";
            this.checkWcfServiceToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.checkWcfServiceToolStripMenuItem.Text = "Check Wcf Service";
            this.checkWcfServiceToolStripMenuItem.Click += new System.EventHandler(this.checkWcfServiceToolStripMenuItem_Click);
            // 
            // checkDbConnectionToolStripMenuItem
            // 
            this.checkDbConnectionToolStripMenuItem.Name = "checkDbConnectionToolStripMenuItem";
            this.checkDbConnectionToolStripMenuItem.Size = new System.Drawing.Size(135, 20);
            this.checkDbConnectionToolStripMenuItem.Text = "Check Db Connection";
            this.checkDbConnectionToolStripMenuItem.Visible = false;
            this.checkDbConnectionToolStripMenuItem.Click += new System.EventHandler(this.checkDbConnectionToolStripMenuItem_Click);
            // 
            // logTestToolStripMenuItem
            // 
            this.logTestToolStripMenuItem.Name = "logTestToolStripMenuItem";
            this.logTestToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.logTestToolStripMenuItem.Text = "LogTest";
            this.logTestToolStripMenuItem.Click += new System.EventHandler(this.logTestToolStripMenuItem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(31, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Mode = ";
            this.label4.Visible = false;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMode.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblMode.Location = new System.Drawing.Point(79, 28);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(68, 13);
            this.lblMode.TabIndex = 40;
            this.lblMode.Text = "Production";
            this.lblMode.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 210);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(192, 100);
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(227, 210);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(230, 191);
            this.pictureBox2.TabIndex = 42;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(493, 210);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(192, 100);
            this.pictureBox3.TabIndex = 43;
            this.pictureBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(31, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 15);
            this.label5.TabIndex = 44;
            this.label5.Text = "Reference Data";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(250, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 15);
            this.label6.TabIndex = 45;
            this.label6.Text = "Submission Data";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(509, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 15);
            this.label7.TabIndex = 46;
            this.label7.Text = "Results Data";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 566);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSaveReferenceToXML);
            this.Controls.Add(this.btnSaveKPIsToXML);
            this.Controls.Add(this.txtLoadSavePath);
            this.Controls.Add(this.btnLoadSavePath);
            this.Controls.Add(this.btnLoadFromXML);
            this.Controls.Add(this.btnSaveToXML);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblClientKey);
            this.Controls.Add(this.btnChooseClientKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSubmissionID);
            this.Controls.Add(this.RequestKPIResultsButton);
            this.Controls.Add(this.btnRetrieveSubmission);
            this.Controls.Add(this.GetReferenceDataButton);
            this.Controls.Add(this.SubmitRefineryDataFileButton);
            this.Controls.Add(this.lstStatus);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.btnClearData);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Client Input Form";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnClearData;
		private System.Windows.Forms.ListBox lstStatus;
		private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button CloseButton;
		private System.Windows.Forms.Button SubmitRefineryDataFileButton;
		private System.Windows.Forms.Button GetReferenceDataButton;
		private System.Windows.Forms.Button btnRetrieveSubmission;
		private System.Windows.Forms.Button RequestKPIResultsButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSubmissionID;
		private System.Windows.Forms.Button btnChooseClientKey;
		private System.Windows.Forms.Label lblClientKey;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnSaveToXML;
		private System.Windows.Forms.Button btnLoadFromXML;
		private System.Windows.Forms.Button btnLoadSavePath;
		private System.Windows.Forms.TextBox txtLoadSavePath;
		private System.Windows.Forms.Button btnSaveKPIsToXML;
		private System.Windows.Forms.Button btnSaveReferenceToXML;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem checkWcfServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prodToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.ToolStripMenuItem checkDbConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logTestToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
	}
}

