﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Sa.Crypto")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HSB Solomon Associates LLC")]
[assembly: AssemblyProduct("Sa.Crypto")]
[assembly: AssemblyCopyright("© 2016 HSB Solomon Associates LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f9c14294-ed18-46b0-b161-246611272156")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: CLSCompliantAttribute(true)]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Test.Crypto, PublicKey=0024000004800000140200000602000000240000525341310010000001000100b91aed88648242619a0f8fa9d59a5f299b6ba55648ea0473e7ef2ee767fff0149302f65a252b04e878ca52ddfb5425f664a72dfbb64f9a0d61568bf4ea921ab434b43d350eda21135dfe8bbd89fb97131fb6ddf27acab691a947b9d13319307ff06701e3f9771dc38d8d66d8b5358052b7d40b3c1b6d081b4a12df7b8a63c57d3955dea345461f4be2895650f7cc08c21783328a5a0c55e4a049ad6550a2af413e550903a544216b74d5f9b73d223f55c71b0f98754eeeb4b818fc9193f278574344f92054837f459d670adca4ce49b9ede60a2d2b8629f64be07329970d3bc2cbfd3c4e8a093c4ef49d4201dc934c1b96f13c2e020542259a8fbab36c6f2d6622ee7ffa6fd337ad72f08e19819ae6927d8a88873e5b29d302357ab9e6ac1d4d9c481da9a90be8ca20d5d785463a1e692f88c3a414baa2fe013e8caae0f519ab9af0804869f4df68edf9f510b156900980e8cdbde27fdce9d195f8e3d7700658a0c92dc8d4f838a8e9c8bc404462787999ae72d9413ba1875dde6ff90ad83a1b599042b91613cf5a002eb5cdf4a419b8cc79a22d3e0c66bcb294acfb4f0a8737e59fd9027ef9f59a7e75e96f672ef4d12ece04aea23f7222e04c2532ebb7c06d0478e6404ba1a6357f0425cd3496bb07141849c363ea9045714855b11ece953bef89c15bc9628c47388b852787154c4d01b09cec246734f74d986d563aadd1d8")]