﻿using System.IO;
using System.IO.Compression;

namespace Sa
{
	static class Zip
	{
		internal static void Compress(byte[] raw, out byte[] compressed)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				using (GZipStream gz = new GZipStream(ms, CompressionMode.Compress, false))
				{
					gz.Write(raw, 0, raw.Length);
					gz.Close();
				}
				compressed = ms.ToArray();
				ms.Close();
			}
		}

		internal static void Expand(byte[] raw, out byte[] expanded)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				using (MemoryStream cs = new MemoryStream(raw, 0, raw.Length, false, false))
				{
					using (GZipStream gz = new GZipStream(cs, CompressionMode.Decompress, false))
					{
						gz.CopyTo(ms);
						gz.Close();
					}
					cs.Close();
				}
				expanded = ms.ToArray();
				ms.Close();
			}
		}
	}
}
