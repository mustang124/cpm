//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class DS_UserDefinedSchema_Result
    {
        public string HeaderText { get; set; }
        public string VariableDesc { get; set; }
        public decimal RptValue { get; set; }
        public decimal RptValue_Target { get; set; }
        public decimal RptValue_Avg { get; set; }
        public decimal RptValue_YTD { get; set; }
        public int DecPlaces { get; set; }
    }
}
