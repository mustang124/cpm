//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class MaintTACost
    {
        public string RefineryID { get; set; }
        public string DataSet { get; set; }
        public int UnitID { get; set; }
        public int TAID { get; set; }
        public string Currency { get; set; }
        public Nullable<float> TACost { get; set; }
        public Nullable<float> TAMatl { get; set; }
        public Nullable<float> AnnTACost { get; set; }
        public Nullable<float> AnnTAMatl { get; set; }
        public Nullable<System.DateTime> TADate { get; set; }
        public Nullable<System.DateTime> NextTADate { get; set; }
        public string ProcessID { get; set; }
        public Nullable<System.DateTime> RestartDate { get; set; }
        public Nullable<float> AnnTACptl { get; set; }
        public Nullable<float> AnnTAExp { get; set; }
        public Nullable<float> AnnTALaborCost { get; set; }
        public Nullable<float> AnnTAOvhd { get; set; }
        public Nullable<float> TACptl { get; set; }
        public Nullable<float> TAExp { get; set; }
        public Nullable<float> TALaborCost { get; set; }
        public Nullable<float> TAOvhd { get; set; }
    }
}
