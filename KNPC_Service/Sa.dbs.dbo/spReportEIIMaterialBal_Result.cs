//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class spReportEIIMaterialBal_Result
    {
        public double CrudeBbl { get; set; }
        public double RMB { get; set; }
        public double OTHRM { get; set; }
        public double M106 { get; set; }
        public double TotInputBbl { get; set; }
        public Nullable<double> FinProd { get; set; }
        public Nullable<double> Coke { get; set; }
        public Nullable<double> Asphalt { get; set; }
        public Nullable<double> RPFConsFOE { get; set; }
        public Nullable<double> RPFSoldFOE { get; set; }
        public Nullable<double> CogenSalesFOE { get; set; }
        public Nullable<double> TotProd { get; set; }
        public Nullable<double> Gain { get; set; }
        public Nullable<double> RPFCons { get; set; }
        public Nullable<double> RPFSold { get; set; }
        public Nullable<double> RPFCogen { get; set; }
        public Nullable<double> CDUCharge { get; set; }
        public Nullable<double> CrudeDelta { get; set; }
    }
}
