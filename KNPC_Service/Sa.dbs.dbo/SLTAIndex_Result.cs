//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class SLTAIndex_Result
    {
        public string FactorSet { get; set; }
        public string Currency { get; set; }
        public Nullable<double> TAIndex { get; set; }
        public Nullable<double> TAMatlIndex { get; set; }
        public Nullable<double> TAEffIndex { get; set; }
    }
}
