//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class MaterialNetCalc
    {
        public int SubmissionID { get; set; }
        public string Scenario { get; set; }
        public string Category { get; set; }
        public string MaterialID { get; set; }
        public Nullable<double> BBL { get; set; }
        public Nullable<double> MT { get; set; }
        public Nullable<float> PricePerBbl { get; set; }
        public Nullable<float> PricePerMT { get; set; }
    }
}
