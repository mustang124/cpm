//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class DS_ConfigRSSchema_Result
    {
        public string ProcessID { get; set; }
        public Nullable<float> RailcarBBL { get; set; }
        public Nullable<float> TankTruckBBL { get; set; }
        public Nullable<float> TankerBerthBBL { get; set; }
        public Nullable<float> OffshoreBuoyBBL { get; set; }
        public Nullable<float> BargeBerthBBL { get; set; }
        public Nullable<float> PipelineBBL { get; set; }
    }
}
