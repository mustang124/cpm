//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class DataCheck
    {
        public int SubmissionID { get; set; }
        public string DataCheckID { get; set; }
        public int ItemSortKey { get; set; }
        public string ItemDesc { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
    }
}
