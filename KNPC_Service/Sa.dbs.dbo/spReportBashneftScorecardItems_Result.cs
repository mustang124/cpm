//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class spReportBashneftScorecardItems_Result
    {
        public string ChartTitle { get; set; }
        public string ChartTitleRussian { get; set; }
        public string SectionHeader { get; set; }
        public string SectionHeaderRussian { get; set; }
        public short SortKey { get; set; }
        public string AxisLabelUS { get; set; }
        public string AxisLabelMetric { get; set; }
        public string DataTable { get; set; }
        public string TotField { get; set; }
        public string TargetField { get; set; }
        public string YTDField { get; set; }
        public string AVGField { get; set; }
        public Nullable<byte> DecPlaces { get; set; }
        public string ValueField1 { get; set; }
    }
}
