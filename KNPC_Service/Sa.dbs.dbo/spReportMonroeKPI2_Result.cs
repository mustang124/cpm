//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class spReportMonroeKPI2_Result
    {
        public Nullable<float> EII { get; set; }
        public Nullable<float> EII_YTD { get; set; }
        public Nullable<float> EII_AVG { get; set; }
        public Nullable<float> EnergyUseDay { get; set; }
        public Nullable<float> EnergyUseDay_YTD { get; set; }
        public Nullable<float> EnergyUseDay_AVG { get; set; }
        public Nullable<float> TotStdEnergy { get; set; }
        public Nullable<float> TotStdEnergy_YTD { get; set; }
        public Nullable<float> TotStdEnergy_AVG { get; set; }
        public Nullable<float> EDC { get; set; }
        public Nullable<float> EDC_YTD { get; set; }
        public Nullable<float> EDC_AVG { get; set; }
        public Nullable<float> UtilOSTA { get; set; }
        public Nullable<float> UtilOSTA_YTD { get; set; }
        public Nullable<float> UtilOSTA_AVG { get; set; }
        public Nullable<float> ProcessEDC { get; set; }
        public Nullable<float> ProcessEDC_YTD { get; set; }
        public Nullable<float> ProcessEDC_AVG { get; set; }
        public Nullable<float> ProcessUEDC { get; set; }
        public Nullable<float> ProcessUEDC_YTD { get; set; }
        public Nullable<float> ProcessUEDC_AVG { get; set; }
        public Nullable<float> MechUnavailTA_Act { get; set; }
        public Nullable<float> MechUnavailTA_Act_YTD { get; set; }
        public Nullable<float> MechUnavailTA_Act_AVG { get; set; }
        public Nullable<float> VEI { get; set; }
        public Nullable<float> VEI_YTD { get; set; }
        public Nullable<float> VEI_AVG { get; set; }
        public Nullable<float> NetInputBPD { get; set; }
        public Nullable<float> NetInputBPD_YTD { get; set; }
        public Nullable<float> NetInputBPD_AVG { get; set; }
        public Nullable<float> ReportLossGain { get; set; }
        public Nullable<float> ReportLossGain_YTD { get; set; }
        public Nullable<float> ReportLossGain_AVG { get; set; }
        public Nullable<float> EstGain { get; set; }
        public Nullable<float> EstGain_YTD { get; set; }
        public Nullable<float> EstGain_AVG { get; set; }
        public Nullable<float> MechAvail { get; set; }
        public Nullable<float> MechAvail_YTD { get; set; }
        public Nullable<float> MechAvail_AVG { get; set; }
        public Nullable<float> MechUnavailTA { get; set; }
        public Nullable<float> MechUnavailTA_YTD { get; set; }
        public Nullable<float> MechUnavailTA_AVG { get; set; }
        public Nullable<float> MechUnavailNonTA { get; set; }
        public Nullable<float> MechUnavailNonTA_YTD { get; set; }
        public Nullable<float> MechUnavailNonTA_AVG { get; set; }
        public Nullable<float> OpAvail { get; set; }
        public Nullable<float> OpAvail_YTD { get; set; }
        public Nullable<float> OpAvail_AVG { get; set; }
        public Nullable<float> RegUnavail { get; set; }
        public Nullable<float> RegUnavail_YTD { get; set; }
        public Nullable<float> RegUnavail_AVG { get; set; }
        public Nullable<float> TotWHrEDC { get; set; }
        public Nullable<float> TotWHrEDC_YTD { get; set; }
        public Nullable<float> TotWHrEDC_AVG { get; set; }
        public Nullable<float> AnnTAWhr { get; set; }
        public Nullable<float> AnnTAWhr_YTD { get; set; }
        public Nullable<float> AnnTAWhr_AVG { get; set; }
        public Nullable<float> NonTAWHr { get; set; }
        public Nullable<float> NonTAWHr_YTD { get; set; }
        public Nullable<float> NonTAWHr_AVG { get; set; }
        public Nullable<float> MPEI { get; set; }
        public Nullable<float> MPEI_YTD { get; set; }
        public Nullable<float> MPEI_AVG { get; set; }
        public Nullable<float> MPEIStd { get; set; }
        public Nullable<float> MPEIStd_YTD { get; set; }
        public Nullable<float> MPEIStd_AVG { get; set; }
        public Nullable<float> NonTAMaintHrs { get; set; }
        public Nullable<float> NonTAMaintHrs_YTD { get; set; }
        public Nullable<float> NonTAMaintHrs_AVG { get; set; }
        public Nullable<float> MaintIndex { get; set; }
        public Nullable<float> MaintIndex_YTD { get; set; }
        public Nullable<float> MaintIndex_AVG { get; set; }
        public Nullable<float> TAAdj { get; set; }
        public Nullable<float> TAAdj_YTD { get; set; }
        public Nullable<double> TAAdj_AVG { get; set; }
        public Nullable<float> RoutCost { get; set; }
        public Nullable<float> RoutCost_YTD { get; set; }
        public Nullable<float> RoutCost_AVG { get; set; }
        public Nullable<float> MEI { get; set; }
        public Nullable<float> MEI_YTD { get; set; }
        public Nullable<float> MEI_AVG { get; set; }
        public Nullable<float> MEIStd { get; set; }
        public Nullable<float> MEIStd_YTD { get; set; }
        public Nullable<float> MEIStd_AVG { get; set; }
        public Nullable<float> NEOpexEDC { get; set; }
        public Nullable<float> NEOpexEDC_YTD { get; set; }
        public Nullable<float> NEOpexEDC_AVG { get; set; }
        public Nullable<float> NEOpex { get; set; }
        public Nullable<float> NEOpex_YTD { get; set; }
        public Nullable<float> NEOpex_AVG { get; set; }
        public Nullable<float> TotCashOpexUEDC { get; set; }
        public Nullable<float> TotCashOpexUEDC_YTD { get; set; }
        public Nullable<float> TotCashOpexUEDC_AVG { get; set; }
        public Nullable<float> EnergyCost { get; set; }
        public Nullable<float> EnergyCost_YTD { get; set; }
        public Nullable<float> EnergyCost_AVG { get; set; }
        public Nullable<float> TotCashOpex { get; set; }
        public Nullable<float> TotCashOpex_YTD { get; set; }
        public Nullable<float> TotCashOpex_AVG { get; set; }
        public Nullable<float> UEDC { get; set; }
        public Nullable<float> UEDC_YTD { get; set; }
        public Nullable<float> UEDC_AVG { get; set; }
    }
}
