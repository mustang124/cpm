//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class MultLimit
    {
        public string FactorSet { get; set; }
        public string MultGroup { get; set; }
        public decimal MinMultiplicity { get; set; }
        public decimal MaxMultiplicity { get; set; }
    }
}
