//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class spReportUnitAvailCalc_Result
    {
        public string UnitName { get; set; }
        public string ProcessID { get; set; }
        public Nullable<double> AvgEDC { get; set; }
        public Nullable<double> RegDown { get; set; }
        public Nullable<double> MaintDown { get; set; }
        public Nullable<double> OthDown { get; set; }
        public Nullable<double> Slowdowns { get; set; }
        public Nullable<int> PeriodHrs { get; set; }
        public Nullable<float> MechUnavailTA { get; set; }
        public Nullable<double> MechAvail { get; set; }
        public Nullable<double> OpAvail { get; set; }
        public Nullable<double> OnStream { get; set; }
        public Nullable<double> OnStreamSlow { get; set; }
    }
}
