//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class Absence_LU
    {
        public string CategoryID { get; set; }
        public string Description { get; set; }
        public Nullable<int> SortKey { get; set; }
        public string IncludeForInput { get; set; }
        public string ParentID { get; set; }
        public string DetailStudy { get; set; }
        public string DetailProfile { get; set; }
        public Nullable<byte> Indent { get; set; }
    }
}
