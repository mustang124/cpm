//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class SS_GetMaintRout_Result
    {
        public int UnitID { get; set; }
        public string ProcessID { get; set; }
        public Nullable<float> RoutCostLocal { get; set; }
        public Nullable<float> RoutExpLocal { get; set; }
        public Nullable<float> RoutCptlLocal { get; set; }
        public Nullable<float> RoutOvhdLocal { get; set; }
        public Nullable<short> RegNum { get; set; }
        public Nullable<short> MaintNum { get; set; }
        public Nullable<short> OthNum { get; set; }
        public Nullable<float> OthDownEconomic { get; set; }
        public Nullable<float> OthDownExternal { get; set; }
        public Nullable<float> OthDownUnitUpsets { get; set; }
        public Nullable<float> OthDownOffsiteUpsets { get; set; }
        public Nullable<float> RegDown { get; set; }
        public Nullable<float> MaintDown { get; set; }
        public Nullable<float> OthDown { get; set; }
        public Nullable<short> SortKey { get; set; }
        public string UnitName { get; set; }
    }
}
