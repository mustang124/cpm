﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace Sa.Pearl
{
	public static class SHA512ClientKey
	{
		public static string Generate()
		{
			byte[] b;

			using (SHA512 hash = SHA512.Create())
			{
				byte[] bStream = Guid.NewGuid().ToByteArray();
				b = hash.ComputeHash(bStream);
			}

			return ByteToString(b);
		}

		private static string ByteToString(byte[] bytes)
		{
			string sbinary = "";

			for (int i = 0; i < bytes.Length; i++)
			{
				sbinary += bytes[i].ToString("X2"); // hex format
			}
			return (sbinary);
		}
	}
}