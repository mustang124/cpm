﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Sa.Pearl
{
    public class PearlAccess<TEntity> where TEntity : class
    {
        #region Private members

        private DbContext context;
        private DbSet<TEntity> entityInstance;
        private DbContextTransaction _transaction;

        #endregion

        #region Constructors

        public PearlAccess(DbContext context)
        {
            this.context = context;
        }

        public PearlAccess()
        {
            this.context = new Pearl.PearlEntities();
        }

        #endregion

        #region Properties

        public DbContext Context
        {
            get
            {
                return context;
            }
        }

        public DbSet<TEntity> EntityInstance
        {
            get
            {
                if (this.entityInstance == null)
                    this.entityInstance = context.Set<TEntity>();

                return this.entityInstance;
            }
        }

        #endregion

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> where)
        {
            //return Repository.GetAll(x => x.CustomerID == customerId).ToList();
            return EntityInstance.Where(where);
        }

        public IEnumerable<TEntity> Select(TEntity entity, int Id)
        {
            var submissioQuery = from subm in EntityInstance
                                 select subm;
            List<TEntity> submList = submissioQuery.ToList();
            return submList;
        }

        public TEntity Get(Expression<Func<TEntity, Boolean>> where)
        {
            return EntityInstance.FirstOrDefault(where);
        }

        public bool Insert(TEntity entity)
        {
            bool returnValue = false;
            try
            {
                EntityInstance.Add(entity);
                Context.SaveChanges();
                returnValue= true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException validationEx)
            {
                string errormessage = String.Empty;
                if (validationEx.EntityValidationErrors.Count() > 0)
                {
                    foreach (System.Data.Entity.Validation.DbEntityValidationResult ex in validationEx.EntityValidationErrors)
                    {
                        foreach (System.Data.Entity.Validation.DbValidationError validationError in ex.ValidationErrors)
                        {
                            errormessage += "Data Validation error " + validationError.ErrorMessage + " for " + validationError.PropertyName + " in ProfileFuels12Access.Insert().  ";
                        }
                    }
                }
                Logger.LogException(new Exception(errormessage)); //so can see where the error happened; LogException won't do that right now.
                Logger.LogException(validationEx);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                if (ex.InnerException != null)
                    err += " | " + ex.InnerException.ToString();
                Logger.LogException(new Exception(err));  //so can see where the error happened; LogException won't do that right now.
                Logger.LogException(ex);
            }
            return returnValue;
        }

        public void Update(TEntity entity)
        {
            EntityInstance.Attach(entity);
            context.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            EntityInstance.Remove(entity);
            Context.SaveChanges();
        }

        public bool BeginTransaction()
        {
            //if this is called, and Commit() is never called, no changes to db are made
            try
            {
                _transaction = Context.Database.BeginTransaction();
                return true;
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }
        public void Rollback()
        {
            _transaction.Rollback();
        }

        public bool Commit()
        {
            try
            {
                _transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        //public bool UpdateFailedValueInXrefTable(bool failed, int Id)
        //{
            
        //    using (var db = new Sa.Pearl.PearlEntities())
        //    {
        //        db.get
                
        //    }
        //}
    }
}
