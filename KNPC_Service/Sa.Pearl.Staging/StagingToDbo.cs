﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbsDbo = Sa.dbs.Dbo;
using DbsDim = Sa.dbs.Dim;
using DbsStage = Sa.dbs.Stage;
using System.Configuration;
using Sa.dbs.Dbo;

namespace Sa.Pearl
{
    public class StagingToDbo
    {
        //decided not to make this class static because Solomon submission ID can/will change
        int _submissionId; //the Staging submission ID
        string _dataSet = string.Empty;
        string _refineryId = string.Empty;
        private Exception _exToClient = new Exception("Error submitting data");
        private Knpc _knpc = new Knpc();
        string _currency = string.Empty;
        int? _configUnitIDForHycProcessId = -1;

        public bool callSpCompleteSubmission(int dboSubmissionId, bool? batchLoading)
        {
            //all the proc does is fire off a job. The job does the calcs.
            //So check the submission record every 15 seconds, fail out at 2 minutes.
            bool returnValue = false;
            using (var db = new DboEntities())
            {
                db.Database.Connection.Open();
                try
                {
                    db.spCompleteSubmission(dboSubmissionId, batchLoading);
                    returnValue= true;
                }
                catch (Exception calcsException)
                {
                    if (calcsException.Message.Contains("Error 50001")) //50001 kicks off  calcs which are done by another job
                    {
                        returnValue = true;
                    }
                    else
                    {
                        Sa.Logger.LogException(calcsException);
                    }
                }
            }

            return returnValue;
        }

        public StagingToDbo()
        {
            _dataSet = GetDatasetSetting();
        }

        private string GetDatasetSetting()
        {
            try
            {
                return ConfigurationManager.AppSettings["DataSet"].ToString();
            }
            catch (Exception ex)
            {
                Logger.LogException(new Exception("Error getting DataSet setting from config file in Sa.Pearl.Staging"));
                throw _exToClient;
            }
        }

        public bool MoveStagingToDbo(int stagingSubmissionId, string refineryId, ref int dboSubmissionId)
        {
            string erroredMethod = String.Empty;
            bool success = true;
            string rptCurrency = string.Empty;
            _configUnitIDForHycProcessId = null;

            try
            {
                erroredMethod = "SubmissionToDbo";
                Logger.LogInfo("Entering MoveStagingToDbo() with stagingSubmissionId " + stagingSubmissionId.ToString());
                Sa.dbs.Dbo.SubmissionsAll pfSubmission = SubmissionToDbo(stagingSubmissionId, refineryId);
                if (pfSubmission.SubmissionID < 1)
                {
                    Logger.LogException(new Exception("Error creating new ProfileFuels12 submission in MoveStagingToDbo()"));
                    return false;
                }

                try
                {
                    DbsStage.StagingDbAccess<DbsStage.Submission> currencyLookup = new DbsStage.StagingDbAccess<DbsStage.Submission>();

                }
                catch (Exception currencyException)
                {
                    Sa.Logger.LogException(new Exception("Unable to do currency lookup."));
                    Sa.Logger.LogException(currencyException);
                    success = false;
                }
                rptCurrency = pfSubmission.RptCurrency;
                _currency=rptCurrency ;
                dboSubmissionId = pfSubmission.SubmissionID;
                Logger.LogInfo("Created new Submission, id: " + pfSubmission.SubmissionID.ToString());
                //string refineryId = pfSubmission.RefineryID;
                int profileFuels12SubmissionId = pfSubmission.SubmissionID;

                /* This class (Staging.Submissions_Current) is now missing so must comment this method out.
                Staging.Submissions_Current stagingSubmission = new Staging.Submissions_Current();
                erroredMethod = "NewDboSubmission";
                NewDboSubmission(stagingSubmission);
                */

                erroredMethod = "AbsenceToDbo";
                DbsStage.StagingDbAccess<DbsStage.Absence> absenceAccess = new DbsStage.StagingDbAccess<DbsStage.Absence>();
                var absences = absenceAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Absence absenceItem in absences)
                {
                    if (!AbsenceToDbo(absenceItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");


                erroredMethod = "ConfigToDbo";
                DbsStage.StagingDbAccess<DbsStage.Config> configAccess = new DbsStage.StagingDbAccess<DbsStage.Config>();
                var configs = configAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Config configItem in configs)
                {
                    if (!ConfigToDbo(configItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "ConfigBuoyToDbo";
                /* find what db to add ConfgBuoy to
                StagingDbAccess<DbsStage.ConfigBuoy> configBuoyAccess = new StagingDbAccess<DbsStage.ConfigBuoy>();
                var configBuoys = configBuoyAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.ConfigBuoy configBuoyItem in configBuoys)
                {
                ConfigBuoyToDbo(configBuoyItem);
                }
                */
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "ConfigRToDbo";
                DbsStage.StagingDbAccess<DbsStage.ConfigR> configRAccess = new DbsStage.StagingDbAccess<DbsStage.ConfigR>();
                var configRs = configRAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.ConfigR configRItem in configRs)
                {
                    if (!ConfigRsToDbo(configRItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "CrudeToDbo";
                DbsStage.StagingDbAccess<DbsStage.Crude> crudeAccess = new DbsStage.StagingDbAccess<DbsStage.Crude>();
                var crudes = crudeAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Crude crudeItem in crudes)
                {
                    if (!CrudeToDbo(crudeItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "DieselToDbo";
                DbsStage.StagingDbAccess<DbsStage.Diesel> dieselAccess = new DbsStage.StagingDbAccess<DbsStage.Diesel>();
                var diesels = dieselAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Diesel dieselItem in diesels)
                {
                    if (!DieselToDbo(dieselItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "EmissionToDbo";
                DbsStage.StagingDbAccess<DbsStage.Emission> emissionAccess = new DbsStage.StagingDbAccess<DbsStage.Emission>();
                var emissions = emissionAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Emission emissionItem in emissions)
                {
                    if (!EmissionToDbo(emissionItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "EnergyToDbo";
                DbsStage.StagingDbAccess<DbsStage.Energy> energyAccess = new DbsStage.StagingDbAccess<DbsStage.Energy>();
                var energys = energyAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                string electricityCodes = string.Empty;
                try
                {
                    electricityCodes = ConfigurationManager.AppSettings["ElectricSettings"].ToString();

                }
                catch (Exception)
                {
                    Logger.LogException(new Exception("Error reading setting ElectricSettings from config file in MoveStagingToDbo()"));
                    throw _exToClient;
                }

                short energyTransCode = 1;
                foreach (DbsStage.Energy energyItem in energys)
                {                    
                    if (electricityCodes.ToUpper().Contains(energyItem.EnergyType.ToUpper()))
                    {
                        if (!ElectricityToDbo(energyItem, energyTransCode))
                            success = false;

                        energyTransCode++;
                    }
                    else
                    {
                        if (!EnergyToDbo(energyItem, energyTransCode))
                            success = false;
                        
                        energyTransCode++;
                    }
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "FinProdPropToDbo";
                DbsStage.StagingDbAccess<DbsStage.FinProdProp> finProdPropAccess = new DbsStage.StagingDbAccess<DbsStage.FinProdProp>();
                var finProdProps = finProdPropAccess.GetAll(x => x.SubmissionId == stagingSubmissionId).ToList();
                foreach (DbsStage.FinProdProp finProdPropItem in finProdProps)
                {
                    if (!FinProdPropToDbo(finProdPropItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "FiredHeaterToDbo";
                DbsStage.StagingDbAccess<DbsStage.FiredHeater> firedHeaterAccess = new DbsStage.StagingDbAccess<DbsStage.FiredHeater>();
                var firedHeaters = firedHeaterAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.FiredHeater firedHeaterItem in firedHeaters)
                {
                    if (!FiredHeaterToDbo(firedHeaterItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "GasolineToDbo";
                DbsStage.StagingDbAccess<DbsStage.Gasoline> gasolineAccess = new DbsStage.StagingDbAccess<DbsStage.Gasoline>();
                var gasolines = gasolineAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Gasoline gasolineItem in gasolines)
                {
                    if (!GasolineToDbo(gasolineItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "GeneralMiscToDbo";
                DbsStage.StagingDbAccess<DbsStage.GeneralMisc> generalMiscAccess = new DbsStage.StagingDbAccess<DbsStage.GeneralMisc>();
                var generalMiscs = generalMiscAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.GeneralMisc generalMiscItem in generalMiscs)
                {
                    if (!GeneralMiscToDbo(generalMiscItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "InventoryToDbo";
                DbsStage.StagingDbAccess<DbsStage.Inventory> inventoryAccess = new DbsStage.StagingDbAccess<DbsStage.Inventory>();
                var inventorys = inventoryAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Inventory inventoryItem in inventorys)
                {
                    if (!InventoryToDbo(inventoryItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "KeroseneToDbo";
                DbsStage.StagingDbAccess<DbsStage.Kerosene> keroseneAccess = new DbsStage.StagingDbAccess<DbsStage.Kerosene>();
                var kerosenes = keroseneAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Kerosene keroseneItem in kerosenes)
                {
                    if (!KeroseneToDbo(keroseneItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "LPGToDbo";
                DbsStage.StagingDbAccess<DbsStage.LPG> lPGAccess = new DbsStage.StagingDbAccess<DbsStage.LPG>();
                var lPGs = lPGAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.LPG lPGItem in lPGs)
                {
                    if (!LPGToDbo(lPGItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "MaintRoutToDbo";
                DbsStage.StagingDbAccess<DbsStage.MaintRout> maintRoutAccess = new DbsStage.StagingDbAccess<DbsStage.MaintRout>();
                var maintRouts = maintRoutAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.MaintRout maintRoutItem in maintRouts)
                {
                    if (!MaintRoutToDbo(maintRoutItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "MaintTAToDbo";
                DbsStage.StagingDbAccess<DbsStage.MaintTA> maintTAAccess = new DbsStage.StagingDbAccess<DbsStage.MaintTA>();
                var maintTAs = maintTAAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.MaintTA maintTAItem in maintTAs)
                {
                    if (!MaintTAToDbo(maintTAItem, rptCurrency))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "MarineBunkerToDbo";
                DbsStage.StagingDbAccess<DbsStage.MarineBunker> marineBunkerAccess = new DbsStage.StagingDbAccess<DbsStage.MarineBunker>();
                var marineBunkers = marineBunkerAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.MarineBunker marineBunkerItem in marineBunkers)
                {
                    if (!MarineBunkerToDbo(marineBunkerItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "MExpToDbo";
                DbsStage.StagingDbAccess<DbsStage.MExp> mExpAccess = new DbsStage.StagingDbAccess<DbsStage.MExp>();
                var mExps = mExpAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.MExp mExpItem in mExps)
                {
                    if (!MExpToDbo(mExpItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "MiscInputToDbo";
                DbsStage.StagingDbAccess<DbsStage.MiscInput> miscInputAccess = new DbsStage.StagingDbAccess<DbsStage.MiscInput>();
                var miscInputs = miscInputAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.MiscInput miscInputItem in miscInputs)
                {
                    if (!MiscInputToDbo(miscInputItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                
                
                
                erroredMethod = "OpexToDbo";

                Sa.dbs.Dbo.OpExAll dboOpExAll = new Sa.dbs.Dbo.OpExAll();
                dboOpExAll.SubmissionID = _submissionId;
                dboOpExAll.Currency = _currency;
                dboOpExAll.DataType = "RPT";
                dboOpExAll.Scenario = "CLIENT";



                DbsStage.StagingDbAccess<DbsStage.OpEx> opexAccess = new DbsStage.StagingDbAccess<DbsStage.OpEx>();
                var opexs = opexAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.OpEx opexItem in opexs)
                {
                    if (!OpexToDbo(opexItem, ref dboOpExAll))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");



                erroredMethod = "OpexAllInsert";
                {
                    if (!OpexAllInsert(dboOpExAll))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");



                erroredMethod = "PersonnelToDbo";
                DbsStage.StagingDbAccess<DbsStage.Personnel> personnelAccess = new DbsStage.StagingDbAccess<DbsStage.Personnel>();
                var personnels = personnelAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Personnel personnelItem in personnels)
                {
                    if (!PersonnelToDbo(personnelItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "ProcessDataToDbo";
                DbsStage.StagingDbAccess<DbsStage.ProcessData> processDataAccess = new DbsStage.StagingDbAccess<DbsStage.ProcessData>();
                var processDatas = processDataAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.ProcessData processDataItem in processDatas)
                {
                    if (!ProcessDataToDbo(processDataItem))
                    {
                        success = false;
                    }
                    else
                    { //if property is FeedDensity, load same value into FeedGravity. Should have been done in XML but wasn't discovered in testing.
                        if (processDataItem.Property == "FeedDensity")
                        {
                            processDataItem.Property = "FeedGravity";
                            if (!ProcessDataToDbo(processDataItem))
                            {
                                success = false;
                            }
                        }
                    }
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "ResidToDbo";
                DbsStage.StagingDbAccess<DbsStage.Resid> residAccess = new DbsStage.StagingDbAccess<DbsStage.Resid>();
                var resids = residAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Resid residItem in resids)
                {
                    if (!ResidToDbo(residItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "RPFResidToDbo";
                DbsStage.StagingDbAccess<DbsStage.RPFResid> rPFResidAccess = new DbsStage.StagingDbAccess<DbsStage.RPFResid>();
                var rPFResids = rPFResidAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.RPFResid rPFResidItem in rPFResids)
                {
                    if (!RPFResidToDbo(rPFResidItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                /* 
                erroredMethod = "StagingEntitiesToDbo";
                DbsStage.StagingDbAccess<DbsStage.StagingEntities> stagingEntitiesAccess = new DbsStage.StagingDbAccess<DbsStage.StagingEntities>();
                var stagingEntitiess = stagingEntitiesAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.StagingEntities stagingEntitiesItem in stagingEntitiess)
                {
                    StagingEntitiesToDbo(stagingEntitiesItem);
                }Logger.LogInfo("Completed " + erroredMethod + "()");

                */
                erroredMethod = "SteamToDbo";
                DbsStage.StagingDbAccess<DbsStage.Steam> steamAccess = new DbsStage.StagingDbAccess<DbsStage.Steam>();
                var steams = steamAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Steam steamItem in steams)
                {
                    if (!SteamToDbo(steamItem))
                        success = false;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                erroredMethod = "YieldToDbo";                
                /*
                --Per Greg: 100*MaterialCategory_LU.SortKey + (row number for that category) 
                -- So I will: order by materialcategory
                --if sortkey is 0 then make it 1
                --multiply sortkey * 100
                --make list in memory
                --if sortkey is already in List then increment sortkey by 1
                --use srtkey
                --add srtkey to that list
                */
                short? lastSortKeyInUse = 100;

                DbsStage.StagingDbAccess<DbsStage.Yield> yieldAccess = new DbsStage.StagingDbAccess<DbsStage.Yield>();
                var yields = yieldAccess.GetAll(x => x.SubmissionID == stagingSubmissionId).ToList();
                foreach (DbsStage.Yield yieldItem in yields)
                {
                    if (!YieldToDbo(yieldItem, lastSortKeyInUse))
                    { success = false; }
                    lastSortKeyInUse++;
                }
                Logger.LogInfo("Completed " + erroredMethod + "()");

                //flag all other submissions as UseSubmission = 0;
                //this is done in SetUseSUbmission, which is called by SpCoompleteSubmission, later.

                ProfileFuels12Access<DbsDbo.SubmissionsAll> saveSubmission = new ProfileFuels12Access<DbsDbo.SubmissionsAll>();

                if (success)  //don't save if errors
                {
                    pfSubmission.CalcsNeeded = null; // "Y" in it causes an error, not an expected value. Leave it null per Greg.
                    //pfSubmission.UseSubmission = true;  this is done in SetUseSUbmission, which is called by SpCompleteSubmission, later.
                    saveSubmission.Update(pfSubmission);
                }
                return success;
            }
            catch (Exception ex)
            {
                //in case of Entity Framework Validation Errors, add this as a watch:  ((System.Data.Entity.Validation.DbEntityValidationException)$exception).EntityValidationErrors
                Exception newException = new Exception("Exception in " + erroredMethod + " : " + ex.Message + "."
                 + ex.InnerException ?? " " + ex.InnerException.ToString() + ".");
                Logger.LogException(newException);
                return false;
            }
        }

        /*  This class (Staging.Submissions_Current) now is missing. So must remove this method
        public void NewDboSubmission(Staging.Submissions_Current staging)
        {
            Sa.dbs.Dbo.Submission dbo = new Sa.dbs.Dbo.Submission();
            //DO set submission id so it matches staging's
            dbo.SubmissionID=staging.SubmissionID;
            dbo.PeriodStart = staging.PeriodBeg ;
            dbo.PeriodEnd = staging.PeriodEnd ;
            dbo.NumDays = (short?)staging.PeriodDuration_Days;
            dbo.RptCurrency = staging.RptCurrency ;
            dbo.UOM = staging.UOM ;
            dbo.RefineryID = staging.RefineryID ;
            //dbo.Notes = staging.Notes ;
            //staging.ClientSubmissionID;
            
            //dbo.DataSet=
            dbo.UseSubmission = true;
        }
        */

        public DbsStage.Submission GetStagingSubmissionEntity(int submissionId)
        {
            DbsStage.StagingDbAccess<DbsStage.Submission> stagingDb = new DbsStage.StagingDbAccess<DbsStage.Submission>();
            var query = stagingDb.GetAll(x => x.SubmissionID == submissionId).ToList();
            return query[0];
        }

        public Sa.dbs.Dbo.SubmissionsAll SubmissionToDbo(int stagingSubmissionId, string refineryId)
        {
            DbsStage.Submission staging = GetStagingSubmissionEntity(stagingSubmissionId);
            Sa.dbs.Dbo.SubmissionsAll pfSubmission = new Sa.dbs.Dbo.SubmissionsAll();
            //Can't do this, this is an Identity  staging.SubmissionID = profileFuelsSubmissionId;
            if (staging.PeriodBeg != null)
            {
                pfSubmission.PeriodStart = (DateTime)staging.PeriodBeg;
            }
            else
            {
                pfSubmission.PeriodStart = null;// DateTime.Today;
            }
            if (staging.PeriodEnd != null)
            {
                pfSubmission.PeriodEnd = (DateTime)staging.PeriodEnd;
            }
            else
            {
                if (pfSubmission.PeriodStart != null)
                    pfSubmission.PeriodEnd = ((DateTime)pfSubmission.PeriodStart).AddMonths(1);
            }

            if (staging.PeriodDuration_Days != null)
            {
                pfSubmission.NumDays = (short?)staging.PeriodDuration_Days;
            }

            try
            {
                pfSubmission.BridgeVersion = ConfigurationManager.AppSettings["BridgeVersion"].ToString();
            }
            catch (Exception)
            {
                Logger.LogException(new Exception("Error finding BridgeVersion in config, in SubmissionToDbo()"));
                throw _exToClient;
            }
            pfSubmission.CalcsNeeded = null;  //per Greg, leave it null.
            try
            {
                pfSubmission.ClientVersion = ConfigurationManager.AppSettings["ClientVersion"].ToString();
            }
            catch (Exception)
            {
                Logger.LogException(new Exception("Error finding ClientVersion in config, in SubmissionToDbo()"));
                throw _exToClient;
            }

            ProfileFuels12Access<Sa.dbs.Dbo.TSort> tSortDb = new ProfileFuels12Access<Sa.dbs.Dbo.TSort>(); //SubmissionstagingDbAccess 
            var tSorts = tSortDb.GetAll(x => x.RefineryID.ToUpper() == refineryId.ToUpper());
            if (tSorts.Count() != 1)
            {
                Logger.LogException(new Exception("Problem finding RefineryID in TSort table; the count of records with RefineryID '" + refineryId + "' is '" + tSorts.Count().ToString() + "' , in SubmissionToDbo()"));
                throw _exToClient;
            }
            foreach (TSort tsort in tSorts)
            {
                pfSubmission.Company = tsort.Company;
                pfSubmission.Location = tsort.Location;
                pfSubmission.RefineryID = tsort.RefineryID;
                _refineryId = tsort.RefineryID;
                break;
            }

            //pfSubmission.CoordEMail;
            //pfSubmission.CoordName;
            //pfSumission.CoordPhone;
            //pfSubmission.CoordTitle;
            pfSubmission.DataSet = _dataSet;
            //pfSubmission.FractionOfYear; this gets populated in the calcs. 
            //pfSubmission.LastCalc;


            if (pfSubmission.PeriodStart != null)
            {
                pfSubmission.PeriodMonth = ((Byte)((DateTime)pfSubmission.PeriodStart).Month);
                pfSubmission.PeriodYear = ((short)((DateTime)pfSubmission.PeriodStart).Year);
            }

            //Note: even though nullable type, calcs will fail if null.
            if (staging.PeriodDuration_Days != null)
            {
                pfSubmission.NumDays = (short?)staging.PeriodDuration_Days;
            }
            else
            {
                DateTime dateEnd = (DateTime)pfSubmission.PeriodEnd;
                DateTime dateStart = (DateTime)pfSubmission.PeriodStart;
                TimeSpan timeSpan = dateEnd.Subtract(dateStart);
                pfSubmission.NumDays = (short?)timeSpan.Days;
            }
            //_refineryId = staging.RefineryID;
            //pfSubmission.RefineryID = _refineryId;

            pfSubmission.RptCurrency = staging.RptCurrency;
            pfSubmission.RptCurrencyT15 = staging.RptCurrency;
            pfSubmission.Submitted = staging.tsInserted.Date;
            pfSubmission.UOM = staging.UOM;
            pfSubmission.UseSubmission = false;

            ProfileFuels12Access<Sa.dbs.Dbo.SubmissionsAll> pfDb = new ProfileFuels12Access<Sa.dbs.Dbo.SubmissionsAll>(); //SubmissionstagingDbAccess 
            pfDb.Insert(pfSubmission);

            _submissionId = pfSubmission.SubmissionID;

            pfDb = null;
            return pfSubmission;
        }

        public Sa.dbs.Dbo.Submission GetProfileFuels12SubmissionEntity(int submissionId)
        {
            ProfileFuels12Access<Sa.dbs.Dbo.Submission> pfDb = new ProfileFuels12Access<Sa.dbs.Dbo.Submission>();
            Sa.dbs.Dbo.Submission pfSubmission = new Sa.dbs.Dbo.Submission();
            var submissioQuery = pfDb.GetAll(x => x.SubmissionID == submissionId);

            /*
                       var submissioQuery = from subm in _StDb.Submissions
                                 where subm.SubmissionID == submissionId
                                 select subm;
            */
            List<Sa.dbs.Dbo.Submission> submList = submissioQuery.ToList();
            return submList[0];
        }

        private bool AbsenceToDbo(DbsStage.Absence staging)
        {
            try
            {
                Sa.dbs.Dbo.Absence dbo = new Sa.dbs.Dbo.Absence();
                dbo.SubmissionID = _submissionId;
                dbo.CategoryID = staging.Category;
                dbo.MPSAbs = staging.MPSAbs;
                //dbo.MPSPcnt=staging.");
                dbo.OCCAbs = staging.OCCAbs;
                //dbo.OCCpct=staging.");
                ProfileFuels12Access<Sa.dbs.Dbo.Absence_LU> sortDB = new ProfileFuels12Access<Sa.dbs.Dbo.Absence_LU>();
                var lookups = sortDB.GetAll(x => x.CategoryID == staging.Category);
                sortDB = null;
                foreach (var lu in lookups)
                {
                    if (lu.CategoryID == staging.Category)
                    {
                        if (lu.SortKey != null)
                        {
                            dbo.SortKey = (short)lu.SortKey;
                        }
                    }
                }
                ProfileFuels12Access<Sa.dbs.Dbo.Absence> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Absence>();
                return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool ConfigToDbo(DbsStage.Config staging)
        {
            try
            {
            Sa.dbs.Dbo.Config dbo = new Sa.dbs.Dbo.Config();
            //dbo.ActualCap = staging.
            //dbo.ActualMFCap = staging.
            //dbo.ActualStmCap=staging.
            if (staging.AllocPcntOfCap!= null && (staging.AllocPcntOfCap<= -1000 || staging.AllocPcntOfCap>= 1000))
            {
                Logger.LogInfo("Divided Config AllocPcntOfCap " + staging.AllocPcntOfCap.ToString() + " by 10");
                staging.AllocPcntOfCap = staging.AllocPcntOfCap / 10;                
            }
            dbo.AllocPcntOfCap = (decimal?)staging.AllocPcntOfCap;
            //dbo.AllocPcntOfTime = staging.
            //dbo.AllocUtilPcnt
            dbo.BlockOp = staging.BlockOp;
            dbo.RptCap = staging.Cap; //changed from dbo.Cap
            //dbo.CapClass=staging.
            dbo.ControlType = staging.ControlType;
            //dbo.DesignFeedSulfur=staging.
            dbo.EnergyPcnt = staging.EnergyPcnt;
            dbo.InserviceCap = staging.InServicePcnt; //Per Ralph 2018-02-21, this gets overwritten by a calcs Proc anyway.
            //dbo.InserviceMFCap=staging.



            //Changes per Richard Thut 2018-02-22
            /*
            if (staging.InServicePcnt!= null && (staging.InServicePcnt<= -1000 || staging.InServicePcnt>= 1000))
            {
                Logger.LogInfo("Divided Config InServicePcnt " + staging.InServicePcnt.ToString() + " by 10");
                staging.InServicePcnt = staging.InServicePcnt / 10;                
            }
            dbo.InServicePcnt = (decimal?)staging.InServicePcnt;
            */
            float stagingInsvcpcnt = staging.InServicePcnt ?? 100;
            dbo.InServicePcnt = (decimal)stagingInsvcpcnt;
            //if (staging.SubmissionID < 0)
            //    return true; //means this is a test
            //End changes per Richard Thut 2018-02-22



            //dbo.InserviceStmCap
            //dbo.MFCap=staging.
            //dbo.MFUtilCap
            //dbo.MFUtilPcnt
            dbo.MHPerWeek = staging.MHPerWeek;
            //dbo.ModePcnt=staging.
            dbo.PostPerShift = (decimal?)staging.PostPerShift;
            dbo.ProcessID = staging.ProcessID;
            if (staging.ProcessID == "HYC")
            {
               _configUnitIDForHycProcessId = staging.UnitID;
            }
            dbo.ProcessType = staging.ProcessType;
            //dbo.RptCap=staging.
            //dbo.RptCapUOM = staging.
            //dbo.RptStmCap=staging.
            //dbo.RptStmCapUOM
            //dbo.SortKey=staging.
            dbo.StmCap = staging.StmCap;
            //dbo.StmUtilCap=staging.
            if (staging.StmUtilPcnt!= null && (staging.StmUtilPcnt<= -1000 || staging.StmUtilPcnt>= 1000))
            {
                Logger.LogInfo("Divided Config StmUtilPcnt " + staging.StmUtilPcnt.ToString() + " by 10");
                staging.StmUtilPcnt = staging.StmUtilPcnt / 10;                
            }
            dbo.StmUtilPcnt = (decimal?)staging.StmUtilPcnt;
            dbo.SubmissionID = _submissionId;
            dbo.UnitID = staging.UnitID ?? 0;
            dbo.UnitName = staging.UnitName;
            //dbo.UnusedCapPcnt=staging.
            //dbo.UtilCap=staging.
            if (staging.UtilPcnt != null && (staging.UtilPcnt <= -1000 || staging.UtilPcnt >= 1000))
            {
                Logger.LogInfo("Divided Config UtilPcnt " + staging.UtilPcnt.ToString() + " by 10");
                staging.UtilPcnt = staging.UtilPcnt / 10;                
            }
            dbo.UtilPcnt = (decimal?)staging.UtilPcnt;
            dbo.YearsOper = (decimal?)staging.YearsOper;
            ProfileFuels12Access<Sa.dbs.Dbo.Config> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Config>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        /*  ConfigBuoy doesn't seem to be coming from Staging
        private void ConfigBuoyToDbo(DbsStage..ConfigBuoy staging)
        {
            Sa.dbs.Dbo.ConfigBuoy dbo = new Sa.dbs.Dbo.ConfigBuoy();
            dbo.SubmissionID = _submissionId;
            dbo.LineSize = staging.LineSize;
            dbo.PcntOwnership = staging.PcntOwnership;
            dbo.ProcessID = staging.ProcessID;
            dbo.ShipCap = staging.ShipCap;
            dbo.UnitID = staging.UnitID ?? (int)staging.UnitID;
            dbo.UnitName = staging.UnitName;
            ProfileFuels12Access<Sa.dbs.Dbo.ConfigBuoy> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.ConfigBuoy>();
            pfDB.Insert(dbo);
        }
        */

        private bool ConfigRsToDbo(DbsStage.ConfigR staging)
        {
            try
            {
            Sa.dbs.Dbo.ConfigR dbo = new Sa.dbs.Dbo.ConfigR();
            dbo.AvgSize = staging.AvgSize;
            dbo.ProcessID = staging.ProcessID;
            dbo.ProcessType = staging.ProcessType;
            dbo.SubmissionID = _submissionId;
            //dbo.Submissions1 --this is a collection of all the other entities Absence, Config, etc.
            dbo.Throughput = staging.Throughput;
            dbo.UnitID = (int)staging.UnitID;
            ProfileFuels12Access<Sa.dbs.Dbo.ConfigR> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.ConfigR>();
            return pfDB.Insert(dbo);
             }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool CrudeToDbo(DbsStage.Crude staging)
        {
             try
            {
            Sa.dbs.Dbo.Crude dbo = new Sa.dbs.Dbo.Crude();
            dbo.SubmissionID = _submissionId;
            dbo.BBL = (double)staging.BBL;
            dbo.CNum = staging.CNum;
            if (staging.CrudeID != null)
            {
                dbo.CrudeID = (short)staging.CrudeID;
            }
            //dbo.CostPerBBL = null;
            dbo.CrudeName = staging.CrudeName;
            dbo.Density = null;
            dbo.Gravity = staging.Gravity;
            dbo.MT = null;
            //dbo.Period = staging.Period;
            dbo.Sulfur = staging.Sulfur;
            ProfileFuels12Access<Sa.dbs.Dbo.Crude> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Crude>();
            return pfDB.Insert(dbo);
            }
             catch (Exception ex)
             {
                 Sa.Logger.LogException(ex);
                 return false;
             }
        }

        private bool DieselToDbo(DbsStage.Diesel staging)
        {
            try
            {

            Sa.dbs.Dbo.Diesel dbo = new Sa.dbs.Dbo.Diesel();
            dbo.SubmissionID = _submissionId;
            dbo.BlendID = (int)staging.BlendID;
            dbo.Grade = staging.Grade;
            dbo.Market = staging.Market;
            dbo.Type = staging.Type;
            dbo.Density = staging.Density;
            dbo.Cetane = staging.Cetane;
            dbo.PourPt = staging.PourPt;
            dbo.Sulfur = staging.Sulfur;
            dbo.CloudPt = staging.CloudPt;
            dbo.ASTM90 = staging.ASTM90;
            dbo.E350 = staging.E350;
            dbo.BiodieselPcnt = staging.BiodieselPcnt;
            dbo.KMT = staging.KMT;
            ProfileFuels12Access<Sa.dbs.Dbo.Diesel> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Diesel>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool EmissionToDbo(DbsStage.Emission staging)
        {
             try
            {

            Sa.dbs.Dbo.Emission dbo = new Sa.dbs.Dbo.Emission();
            dbo.SubmissionID = _submissionId;
            dbo.EmissionType = staging.EmissionType.Trim();
            dbo.RefEmissions = staging.RefEmissions;
            ProfileFuels12Access<Sa.dbs.Dbo.Emission> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Emission>();
           return  pfDB.Insert(dbo);
            }
             catch (Exception ex)
             {
                 Sa.Logger.LogException(ex);
                 return false;
             }
        }

        private bool ElectricityToDbo(DbsStage.Energy staging, short transCode)
        {
            try
            {

            Sa.dbs.Dbo.Electric dbo = new Sa.dbs.Dbo.Electric();
            dbo.SubmissionID = _submissionId;
            dbo.TransCode = transCode;
            dbo.EnergyType = staging.EnergyType;
            Single genEff = 0;
            if (Single.TryParse(staging.GenEff, out genEff))
            {
                dbo.GenEff = genEff;
            }
            if (staging.PriceLocal != null)
                dbo.PriceLocal = (Single)staging.PriceLocal;
            //dbo.PriceUS;
            //dbo.RptGenEff;
            dbo.RptMWH = staging.Amount ?? 0;
            //dbo.SourceMWH;
            //dbo.TransCode;
            //dbo.TransferTo;
            dbo.TransType = staging.TransType;
            //dbo.UsageMWH;

            ProfileFuels12Access<Sa.dbs.Dbo.Electric> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Electric>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }
        private bool EnergyToDbo(DbsStage.Energy staging, short transCode)
        {
             try
            {

            Sa.dbs.Dbo.Energy dbo = new Sa.dbs.Dbo.Energy();
            dbo.SubmissionID = _submissionId;
            dbo.TransCode = transCode;
            dbo.EnergyType = staging.EnergyType;
            dbo.TransType = staging.TransType;
            if (staging.Amount == null)
            {
                dbo.RptSource = 0;
            }
            else
            {
                dbo.RptSource = (double)staging.Amount;
            }
            //dbo.GenEff = staging.GenEff;
            dbo.Hydrogen = staging.Hydrogen;
            dbo.Methane = staging.Methane;
            dbo.Ethane = staging.Ethane;
            dbo.Ethylene = staging.Ethylene;
            dbo.Propane = staging.Propane;
            dbo.Propylene = staging.Propylene;
            dbo.Butane = staging.Butane;
            dbo.Isobutane = staging.Isobutane;
            dbo.Butylenes = staging.Butylenes;
            dbo.C5Plus = staging.C5Plus;
            dbo.CO = staging.CO;
            dbo.CO2 = staging.CO2;
            dbo.N2 = staging.N2;
            dbo.SourceMBTU = staging.MBTUOut;
            dbo.H2S = staging.H2S;
            dbo.NH3 = staging.NH3;
            dbo.SO2 = staging.SO2;
            //dbo.OverrideCalcs = staging.OverrideCalcs;
            //dbo.PriceLocal = staging.PriceLocal;
            ProfileFuels12Access<Sa.dbs.Dbo.Energy> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Energy>();
            return pfDB.Insert(dbo);
            }
             catch (Exception ex)
             {
                 Sa.Logger.LogException(ex);
                 return false;
             }
        }

        private bool FinProdPropToDbo(DbsStage.FinProdProp staging)
        {
            try
            {
            DbsDbo.FinProdProp dbo = new DbsDbo.FinProdProp();
            dbo.BlendId = staging.BlendId;
            dbo.MaterialId = staging.MaterialId;
            dbo.PropertyId = staging.PropertyId;
            dbo.RptDVal = staging.RptDVal;
            dbo.RptNVal = staging.RptNVal;
            dbo.RptTVal = staging.RptTVal;
            dbo.SubmissionId = staging.SubmissionId;
            ProfileFuels12Access<Sa.dbs.Dbo.FinProdProp> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.FinProdProp>();
            return pfDB.Insert(dbo);
            }
             catch (Exception ex)
             {
                 Sa.Logger.LogException(ex);
                 return false;
             }
        }

        private bool FiredHeaterToDbo(DbsStage.FiredHeater staging)
        {
            try
            {
            Sa.dbs.Dbo.FiredHeater dbo = new Sa.dbs.Dbo.FiredHeater();
            dbo.SubmissionID = _submissionId;
            dbo.HeaterNo = (int)staging.HeaterNo;
            dbo.UnitID = (int)staging.UnitID;
            dbo.ProcessID = staging.ProcessID;
            dbo.HeaterName = staging.HeaterName;
            dbo.Service = staging.Service;
            dbo.ProcessFluid = staging.ProcessFluid;
            dbo.ThroughputRpt = staging.ThroughputRpt;
            dbo.ThroughputUOM = staging.ThroughputUOM;
            dbo.FiredDuty = staging.FiredDuty;
            dbo.FuelType = staging.FuelType;
            dbo.OthCombDuty = staging.OthCombDuty;
            dbo.FurnInTemp = staging.FurnInTemp;
            dbo.FurnOutTemp = staging.FurnOutTemp;
            dbo.StackTemp = staging.StackTemp;
            dbo.StackO2 = staging.StackO2;
            dbo.HeatLossPcnt = staging.HeatLossPcnt;
            dbo.CombAirTemp = staging.CombAirTemp;
            dbo.AbsorbedDuty = staging.AbsorbedDuty;
            dbo.ProcessDuty = staging.ProcessDuty;
            dbo.SteamDuty = staging.SteamDuty;

                dbo.SteamSuperHeated = TransformToYN(staging.SteamSuperHeated);

            dbo.ShaftDuty = staging.ShaftDuty;
            dbo.OtherDuty = staging.OtherDuty;
            ProfileFuels12Access<Sa.dbs.Dbo.FiredHeater> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.FiredHeater>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }


        private string TransformToYN(string value)
        {
            try
            {
                if (value.ToUpper().StartsWith("Y") || value.ToUpper().StartsWith("T") || value =="1" || value== "-1")
                {
                    return "Y";
                }
                else if (value.ToUpper().StartsWith("N") || value.ToUpper().StartsWith("F")  || value.ToUpper().StartsWith("0"))
                {
                    return "F";
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        private bool GasolineToDbo(DbsStage.Gasoline staging)
        {
            try
            {

            Sa.dbs.Dbo.Gasoline dbo = new Sa.dbs.Dbo.Gasoline();
            dbo.SubmissionID = _submissionId;
            dbo.BlendID = (int)staging.BlendID;
            dbo.Grade = staging.Grade;
            dbo.Market = staging.Market;
            dbo.Type = staging.Type;
            dbo.Density = staging.Density;
            dbo.RVP = staging.RVP;
            dbo.RON = staging.RON;
            dbo.MON = staging.MON;
            dbo.Oxygen = staging.Oxygen;
            dbo.Ethanol = staging.Ethanol;
            dbo.MTBE = staging.MTBE;
            dbo.ETBE = staging.ETBE;
            dbo.TAME = staging.TAME;
            dbo.OthOxygen = staging.OthOxygen;
            dbo.Sulfur = staging.Sulfur;
            dbo.Lead = staging.Lead;
            dbo.KMT = staging.KMT;
            ProfileFuels12Access<Sa.dbs.Dbo.Gasoline> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Gasoline>();
                            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool GeneralMiscToDbo(DbsStage.GeneralMisc staging)
        {
            try
            {

            Sa.dbs.Dbo.GeneralMisc dbo = new Sa.dbs.Dbo.GeneralMisc();
            dbo.SubmissionID = _submissionId;
            dbo.FlareLossMT = staging.FlareLossMT;
            dbo.TotLossMT = staging.TotLossMT;
            ProfileFuels12Access<Sa.dbs.Dbo.GeneralMisc> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.GeneralMisc>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool InventoryToDbo(DbsStage.Inventory staging)
        {
            try
            {
            Sa.dbs.Dbo.Inventory dbo = new Sa.dbs.Dbo.Inventory();
            dbo.SubmissionID = _submissionId;
            dbo.NumTank = (short?)staging.NumTank;
            dbo.AvgLevel = staging.AvgLevel;
            dbo.LeasedPcnt = staging.LeasedPcnt;
            dbo.MandStorage = staging.MandStorage;
            dbo.TankType = staging.TankType;
            dbo.TotStorage = staging.TotStorage;


            //FuelsStorage change per Ralph 2018-02-21
            float totStorage = staging.TotStorage ?? 0;
            float mandStorage = staging.MandStorage ?? 0;
            //dbo.FuelsStorage = staging.FuelStorage;
            dbo.FuelsStorage = totStorage - mandStorage;
            //if (staging.SubmissionID < 0)
            //    return true; //means this is a test



            ProfileFuels12Access<Sa.dbs.Dbo.Inventory> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Inventory>();
             return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool KeroseneToDbo(DbsStage.Kerosene staging)
        {
            try
            {
            Sa.dbs.Dbo.Kerosene dbo = new Sa.dbs.Dbo.Kerosene();
            dbo.SubmissionID = _submissionId;
            dbo.BlendID = (int)staging.BlendID;
            dbo.Grade = staging.Grade;
            dbo.Type = staging.Type;
            dbo.Density = staging.Density;
            dbo.Sulfur = staging.Sulfur;
            dbo.KMT = staging.KMT;
            ProfileFuels12Access<Sa.dbs.Dbo.Kerosene> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Kerosene>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool LPGToDbo(DbsStage.LPG staging)
        {
            try
            {
            Sa.dbs.Dbo.LPG dbo = new Sa.dbs.Dbo.LPG();
            dbo.SubmissionID = _submissionId;
            dbo.BlendID = (int)staging.BlendID;
            dbo.MolOrVol = staging.MolOrVol;
            dbo.VolC2Lt = staging.VolC2Lt;
            dbo.VolC3 = staging.VolC3;
            dbo.VolC3ene = staging.VolC3ene;
            dbo.VoliC4 = staging.VoliC4;
            dbo.VolnC4 = staging.VolnC4;
            dbo.VolC4ene = staging.VolC4ene;
            dbo.VolC5Plus = staging.VolC5Plus;
            ProfileFuels12Access<Sa.dbs.Dbo.LPG> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.LPG>();
             return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool MaintRoutToDbo(DbsStage.MaintRout staging)
        {
            try
            {
            Sa.dbs.Dbo.MaintRout dbo = new Sa.dbs.Dbo.MaintRout();
            dbo.SubmissionID = _submissionId;
            dbo.UnitID = (int)staging.UnitID;
            dbo.ProcessID = staging.ProcessID.Trim();
            if (staging.RegNum != null)
            {
                dbo.RegNum = (short)staging.RegNum;
            }
            dbo.RegDown = staging.RegDown;
            if (staging.MaintNum != null)
            {
                dbo.MaintNum = (short)staging.MaintNum;
            }
            dbo.MaintDown = staging.MaintDown;
            if (staging.OthNum != null)
            {
                dbo.OthNum = (short)staging.OthNum;
            }
            dbo.OthDown = staging.OthDown;
            dbo.RoutCostLocal = staging.RoutCostLocal;
            dbo.RoutMatlLocal = staging.RoutMatlLocal;
            dbo.RoutCostUS = staging.RoutCostUS;
            dbo.RoutMatlUS = staging.RoutMatlUS;
            dbo.RoutMatlPcnt = staging.RoutMatlPcnt;
            dbo.OthDownEconomic = staging.OthDownEconomic;
            dbo.OthDownExternal = staging.OthDownExternal;
            dbo.OthDownUnitUpsets = staging.OthDownUnitUpsets;
            dbo.OthDownOffsiteUpsets = staging.OthDownOffsiteUpsets;
            dbo.OthDownOther = staging.OthDownOther;
            dbo.RoutExpLocal = staging.RoutExpLocal;
            dbo.RoutCptlLocal = staging.RoutCptlLocal;
            dbo.RoutOvhdLocal = staging.RoutOvhdLocal;
            dbo.OthSlow = staging.OthSlow;
            if (staging.UsePcntOfCost != null)
            {
                dbo.UsePcntOfCost = (decimal?)staging.UsePcntOfCost;
            }
            ProfileFuels12Access<Sa.dbs.Dbo.MaintRout> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.MaintRout>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool MaintTAToDbo(DbsStage.MaintTA staging, string rptCurrency)
        {
            /* legacy
            try
            {
            Sa.dbs.Dbo.MaintTA dbo = new Sa.dbs.Dbo.MaintTA();
            dbo.RefineryID = _refineryId;
            dbo.DataSet = _dataSet;
            dbo.UnitID = (int)staging.UnitID;
            dbo.TAID = staging.MaintTAID;
            dbo.TADate = staging.TADate;
            dbo.TAHrsDown = staging.TAHrsDown;
            dbo.TACostLocal = staging.TACostLocal;
            dbo.TACostUS = staging.TACostUS;
            dbo.TAMatlLocal = staging.TAMatlLocal;
            dbo.TAMatlUS = staging.TAMatlUS;
            dbo.TAOCCSTH = staging.TAOCCSTH;
            dbo.TAOCCOVT = staging.TAOCCOVT;
            dbo.TAMPSSTH = staging.TAMPSSTH;
            dbo.TAMPSOVTPcnt = staging.TAMPSOVTPcnt;
            dbo.TAContOCC = staging.TAContOCC;
            dbo.TAContMPS = staging.TAContMPS;
            dbo.PrevTADate = staging.PrevTADate;
            dbo.TAExceptions = (short)staging.TAExceptions;
            dbo.ProcessID = staging.ProcessID;
            dbo.TAOvhdLocal = staging.TAOvhdLocal;
            dbo.TALaborCostLocal = staging.TALaborCostLocal;
            dbo.TACptlLocal = staging.TACptlLocal;
            dbo.TAExpLocal = staging.TAExpLocal;
            ProfileFuels12Access<Sa.dbs.Dbo.MaintTA> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.MaintTA>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
            */

            try
            {
                Sa.dbs.Dbo.LoadTA dbo = new Sa.dbs.Dbo.LoadTA();
                dbo.SubmissionID = _submissionId;
                dbo.UnitID = staging.UnitID?? 0;
                dbo.TAID = staging.MaintTAID;
                //dbo.SchedTAInt = staging.  ???  Asked Joe 12/21
                dbo.TADate = staging.TADate;
                dbo.TAHrsDown = staging.TAHrsDown;
                dbo.TACostLocal = staging.TACostLocal;
                dbo.TAMatlLocal = staging.TAMatlLocal;
                // no such field in LoadTA   dbo.TAMatlUS = staging.TAMatlUS;
                dbo.TAOCCSTH = staging.TAOCCSTH;
                dbo.TAOCCOVT = staging.TAOCCOVT;
                dbo.TAMPSSTH = staging.TAMPSSTH;
                dbo.TAMPSOVTPcnt = staging.TAMPSOVTPcnt;
                dbo.TAContOCC = staging.TAContOCC;
                dbo.TAContMPS = staging.TAContMPS;
                dbo.PrevTADate = staging.PrevTADate;
                dbo.TAExceptions = (short?)staging.TAExceptions;
                dbo.TACurrency = rptCurrency;
                dbo.ProcessID = staging.ProcessID.Trim();
                dbo.TACptlLocal = staging.TACptlLocal;
                dbo.TAExpLocal = staging.TAExpLocal;
                dbo.TALaborCostLocal = staging.TALaborCostLocal;
                dbo.TAOvhdLocal = staging.TAOvhdLocal;
                // no such field in LoadTA dbo.TACostUS = staging.TACostUS;
                ProfileFuels12Access<Sa.dbs.Dbo.LoadTA> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.LoadTA>();
                return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }


        }

        private bool  MarineBunkerToDbo(DbsStage.MarineBunker staging)
        {
            try
            {
            Sa.dbs.Dbo.MarineBunker dbo = new Sa.dbs.Dbo.MarineBunker();
            dbo.SubmissionID = _submissionId;
            dbo.BlendID = (int)staging.BlendID;
            dbo.CrackedStock = staging.CrackedStock;
            dbo.Density = staging.Density;
            dbo.PourPt = staging.PourPt;
            dbo.Sulfur = staging.Sulfur;
            dbo.KMT = staging.KMT;
            dbo.ViscCSAtTemp = staging.ViscCSAtTemp;
            dbo.ViscTemp = staging.ViscTemp;
            dbo.Grade = staging.Grade;
            ProfileFuels12Access<Sa.dbs.Dbo.MarineBunker> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.MarineBunker>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool MExpToDbo(DbsStage.MExp staging)
        {
            try
            {
            Sa.dbs.Dbo.MExp dbo = new Sa.dbs.Dbo.MExp();
            dbo.SubmissionID = _submissionId;
            dbo.ComplexCptl = staging.ComplexCptl;
            dbo.NonRefExcl = staging.NonRefExcl;
            dbo.TAMaintCptl = staging.TAMaintCptl;
            dbo.RoutMaintCptl = staging.RoutMaintCptl;
            dbo.NonMaintInvestExp = staging.NonMaintInvestExp;
            dbo.NonRegUnit = staging.NonRegUnit;
            dbo.ConstraintRemoval = staging.ConstraintRemoval;
            dbo.RegExp = staging.RegExp;
            dbo.RegGaso = staging.RegGaso;
            dbo.RegDiesel = staging.RegDiesel;
            dbo.RegOth = staging.RegOth;
            dbo.SafetyOth = staging.SafetyOth;
            dbo.Energy = staging.Energy;
            dbo.OthInvest = staging.OthInvest;
            dbo.MaintExpTA = staging.MaintExpTA;
            dbo.MaintExpRout = staging.MaintExpRout;
            dbo.MaintExp = staging.MaintExp;
            dbo.MaintOvhdTA = staging.MaintOvhdTA;
            dbo.MaintOvhdRout = staging.MaintOvhdRout;
            dbo.MaintOvhd = staging.MaintOvhd;
            dbo.TotMaint = staging.TotMaint;
            ProfileFuels12Access<Sa.dbs.Dbo.MExp> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.MExp>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool MiscInputToDbo(DbsStage.MiscInput staging)
        {
            try
            {
            Sa.dbs.Dbo.MiscInput dbo = new Sa.dbs.Dbo.MiscInput();
            dbo.SubmissionId = _submissionId;
            dbo.CDUChargeBbl = staging.CDUChargeBbl;
            dbo.CDUChargeMT = staging.CDUChargeMT;
            dbo.OffSiteEnergyPcnt = staging.OffSiteEnergyPcnt;
            ProfileFuels12Access<Sa.dbs.Dbo.MiscInput> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.MiscInput>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        //Change from OpexRpt to OpexAll below
        //private bool  OpexToDbo(DbsStage.OpEx staging)
        //{
        //    try
        //    {
        //    Sa.dbs.Dbo.OpexRpt dbo = new Sa.dbs.Dbo.OpexRpt();
        //    dbo.SubmissionID = _submissionId;
        //    dbo.OpexCategory = staging.Property;
        //    dbo.OpexID = staging.OpexID;
        //    dbo.OthDescription = staging.OthDescription;
        //    dbo.RptValue = (decimal?)staging.RptValue;
        //    ProfileFuels12Access<Sa.dbs.Dbo.OpexRpt> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.OpexRpt>();
        //    return pfDB.Insert(dbo);
        //    }
        //    catch (Exception ex)
        //    {
        //        Sa.Logger.LogException(ex);
        //        return false;
        //    }
        //}
        private bool OpexToDbo(DbsStage.OpEx staging, ref Sa.dbs.Dbo.OpExAll dbo )
        {
            try
            {
                //Sa.dbs.Dbo.OpExAll dbo = new Sa.dbs.Dbo.OpExAll();
                //dbo.SubmissionID = _submissionId;
                //dbo.Currency=_currency;
                //dbo.DataType = "RPT";
                //dbo.Scenario = "CLIENT";
                string dboFieldName = _knpc.FindDboFieldname(staging.Property.Trim());
                switch (dboFieldName)
                {
                    case "CatalystsDHYT": dbo.CatalystsDHYT = staging.RptValue; break;
                    case "CatalystsFCCAdditives": dbo.CatalystsFCCAdditives = staging.RptValue; break;
                    case "CatalystsFCCECat": dbo.CatalystsFCCECat = staging.RptValue; break;
                    case "CatalystsFCCECatSales": dbo.CatalystsFCCECatSales = staging.RptValue; break;
                    case "CatalystsFCCFresh": dbo.CatalystsFCCFresh = staging.RptValue; break;
                    case "CatalystsHYC": dbo.CatalystsHYC = staging.RptValue; break;
                    case "CatalystsHYG": dbo.CatalystsHYG = staging.RptValue; break;
                    case "CatalystsNKSHYT": dbo.CatalystsNKSHYT = staging.RptValue; break;
                    case "CatalystsOth": dbo.CatalystsOth = staging.RptValue; break;
                    case "CatalystsPetChem": dbo.CatalystsPetChem = staging.RptValue; break;
                    case "CatalystsREF": dbo.CatalystsREF = staging.RptValue; break;
                    case "CatalystsRHYT": dbo.CatalystsRHYT = staging.RptValue; break;
                    case "CatalystsS2Plant": dbo.CatalystsS2Plant = staging.RptValue; break;
                    case "CatalystsVHYT": dbo.CatalystsVHYT = staging.RptValue; break;
                    case "ChemicalsAlkyAcid": dbo.ChemicalsAlkyAcid = staging.RptValue; break;
                    case "ChemicalsAmines": dbo.ChemicalsAmines = staging.RptValue; break;
                    case "ChemicalsASESolv": dbo.ChemicalsASESolv = staging.RptValue; break;
                    case "ChemicalsClay": dbo.ChemicalsClay = staging.RptValue; break;
                    case "ChemicalsDieselAdd": dbo.ChemicalsDieselAdd = staging.RptValue; break;
                    case "ChemicalsGasAdd": dbo.ChemicalsGasAdd = staging.RptValue; break;
                    case "ChemicalsH2OTreat": dbo.ChemicalsH2OTreat = staging.RptValue; break;
                    case "ChemicalsLube": dbo.ChemicalsLube = staging.RptValue; break;
                    case "ChemicalsO2": dbo.ChemicalsO2 = staging.RptValue; break;
                    case "ChemicalsOth": dbo.ChemicalsOth = staging.RptValue; break;
                    case "ChemicalsOthAcid": dbo.ChemicalsOthAcid = staging.RptValue; break;
                    case "ChemicalsOthAdd": dbo.ChemicalsOthAdd = staging.RptValue; break;
                    case "ChemicalsProcess": dbo.ChemicalsProcess = staging.RptValue; break;
                    case "ChemicalsWasteH2O": dbo.ChemicalsWasteH2O = staging.RptValue; break;
                    case "Cogen": dbo.Cogen = staging.RptValue; break;
                    case "ContMaintInspect": dbo.ContMaintInspect = staging.RptValue; break;
                    case "ContMaintLabor": dbo.ContMaintLabor = staging.RptValue; break;
                    case "ContMaintMatl": dbo.ContMaintMatl = staging.RptValue; break;
                    case "EnvirDisp": dbo.EnvirDisp = staging.RptValue; break;
                    case "EnvirEng": dbo.EnvirEng = staging.RptValue; break;
                    case "EnvirFines": dbo.EnvirFines = staging.RptValue; break;
                    case "EnvirLab": dbo.EnvirLab = staging.RptValue; break;
                    case "EnvirMonitor": dbo.EnvirMonitor = staging.RptValue; break;
                    case "EnvirOth": dbo.EnvirOth = staging.RptValue; break;
                    case "EnvirPermits": dbo.EnvirPermits = staging.RptValue; break;
                    case "EnvirSpill": dbo.EnvirSpill = staging.RptValue; break;
                    case "EquipMaint": dbo.EquipMaint = staging.RptValue; break;
                    case "EquipNonMaint": dbo.EquipNonMaint = staging.RptValue; break;
                    case "ExclEnvirFines": dbo.ExclEnvirFines = staging.RptValue; break;
                    case "ExclOth": dbo.ExclOth = staging.RptValue; break;
                    case "ExclFireSafety": dbo.ExclFireSafety = staging.RptValue; break;
                    case "GAPers": dbo.GAPers = staging.RptValue; break;
                    case "InsurBI": dbo.InsurBI = staging.RptValue; break;
                    case "InsurOth": dbo.InsurOth = staging.RptValue; break;
                    case "InsurPC": dbo.InsurPC = staging.RptValue; break;
                    case "MaintMatl": dbo.MaintMatl = staging.RptValue; break;
                    case "MPSBenInsur": dbo.MPSBenInsur = staging.RptValue; break;
                    case "MPSBenPension": dbo.MPSBenPension = staging.RptValue; break;
                    case "MPSBenStock": dbo.MPSBenStock = staging.RptValue; break;
                    case "MPSBenSub": dbo.MPSBenSub = staging.RptValue; break;
                    case "MPSBenTaxMed": dbo.MPSBenTaxMed = staging.RptValue; break;
                    case "MPSBenTaxOth": dbo.MPSBenTaxOth = staging.RptValue; break;
                    case "MPSBenTaxPen": dbo.MPSBenTaxPen = staging.RptValue; break;
                    case "MPSSal": dbo.MPSSal = staging.RptValue; break;
                    case "OCCBenInsur": dbo.OCCBenInsur = staging.RptValue; break;
                    case "OCCBenPension": dbo.OCCBenPension = staging.RptValue; break;
                    case "OCCBenStock": dbo.OCCBenStock = staging.RptValue; break;
                    case "OCCBenSub": dbo.OCCBenSub = staging.RptValue; break;
                    case "OCCBenTaxMed": dbo.OCCBenTaxMed = staging.RptValue; break;
                    case "OCCBenTaxOth": dbo.OCCBenTaxOth = staging.RptValue; break;
                    case "OCCBenTaxPen": dbo.OCCBenTaxPen = staging.RptValue; break;
                    case "OCCSal": dbo.OCCSal = staging.RptValue; break;
                    case "OthCont": dbo.OthCont = staging.RptValue; break;
                    case "OthContAdmin": dbo.OthContAdmin = staging.RptValue; break;
                    case "OthContComputing": dbo.OthContComputing = staging.RptValue; break;
                    case "OthContConsult": dbo.OthContConsult = staging.RptValue; break;
                    case "OthContFire": dbo.OthContFire = staging.RptValue; break;
                    case "OthContFoodSvc": dbo.OthContFoodSvc = staging.RptValue; break;
                    case "OthContJan": dbo.OthContJan = staging.RptValue; break;
                    case "OthContLab": dbo.OthContLab = staging.RptValue; break;
                    case "OthContLegal": dbo.OthContLegal = staging.RptValue; break;
                    case "OthContOth": dbo.OthContOth = staging.RptValue; break;
                    case "OthContProcOp": dbo.OthContProcOp = staging.RptValue; break;
                    case "OthContSecurity": dbo.OthContSecurity = staging.RptValue; break;
                    case "OthContTransOp": dbo.OthContTransOp = staging.RptValue; break;
                    case "OthNonVolComm": dbo.OthNonVolComm = staging.RptValue; break;
                    case "OthNonVolComputer": dbo.OthNonVolComputer = staging.RptValue; break;
                    case "OthNonVolDonations": dbo.OthNonVolDonations = staging.RptValue; break;
                    case "OthNonVolDues": dbo.OthNonVolDues = staging.RptValue; break;
                    case "OthNonVolExtraExpat": dbo.OthNonVolExtraExpat = staging.RptValue; break;
                    case "OthNonVolNonContribPers": dbo.OthNonVolNonContribPers = staging.RptValue; break;
                    case "OthNonVolNonPersSafety": dbo.OthNonVolNonPersSafety = staging.RptValue; break;
                    case "OthNonVolOth": dbo.OthNonVolOth = staging.RptValue; break;
                    case "OthNonVolSafety": dbo.OthNonVolSafety = staging.RptValue; break;
                    case "OthNonVolSupply": dbo.OthNonVolSupply = staging.RptValue; break;
                    case "OthNonVolTanks": dbo.OthNonVolTanks = staging.RptValue; break;
                    case "OthNonVolTrain": dbo.OthNonVolTrain = staging.RptValue; break;
                    case "OthNonVolTravel": dbo.OthNonVolTravel = staging.RptValue; break;
                    case "OthRevenue": dbo.OthRevenue = staging.RptValue; break;
                    case "OthVol": dbo.OthVol = staging.RptValue; break;
                    case "EmissionsTaxes": dbo.EmissionsTaxes = staging.RptValue; break;
                    case "PMAA": dbo.PMAA = staging.RptValue; break;
                    case "POXO2": dbo.POXO2 = staging.RptValue; break;
                    case "PurElec": dbo.PurElec = staging.RptValue; break;
                    case "PurFG": dbo.PurFG = staging.RptValue; break;
                    case "PurLiquid": dbo.PurLiquid = staging.RptValue; break;
                    case "PurOth": dbo.PurOth = staging.RptValue; break;
                    case "PurOthH2O": dbo.PurOthH2O = staging.RptValue; break;
                    case "PurOthN2": dbo.PurOthN2 = staging.RptValue; break;
                    case "PurSolid": dbo.PurSolid = staging.RptValue; break;
                    case "PurSteam": dbo.PurSteam = staging.RptValue; break;
                    case "RefProdFG": dbo.RefProdFG = staging.RptValue; break;
                    case "RefProdOth": dbo.RefProdOth = staging.RptValue; break;
                    case "Royalties": dbo.Royalties = staging.RptValue; break;
                    case "Tax": dbo.Tax = staging.RptValue; break;
                    case "ThirdPartyTerminalProd": dbo.ThirdPartyTerminalProd = staging.RptValue; break;
                    case "ThirdPartyTerminalRM": dbo.ThirdPartyTerminalRM = staging.RptValue; break;
                }

                //ProfileFuels12Access<Sa.dbs.Dbo.OpExAll> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.OpExAll>();
                //return pfDB.Insert(dbo);
                return true;
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        public bool OpexAllInsert(Sa.dbs.Dbo.OpExAll dbo)
        {
            try
            {
                //Sa.dbs.Dbo.OpExAll dbo 
                ProfileFuels12Access<Sa.dbs.Dbo.OpExAll> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.OpExAll>();
                return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }


        private bool PersonnelToDbo(DbsStage.Personnel staging)
        {
            try
            {
            Sa.dbs.Dbo.Per dbo = new Sa.dbs.Dbo.Per();
            dbo.SubmissionID = _submissionId;
            dbo.PersID = staging.PersID;
            dbo.NumPers = staging.NumPers;
            dbo.STH = staging.STH;
            dbo.OVTHours = staging.OVTHours;
            dbo.OVTPcnt = staging.OVTPcnt;
            dbo.Contract = staging.Contract;
            dbo.GA = staging.GA;
            dbo.AbsHrs = staging.AbsHrs;
            //dbo.Submission = staging.Submission;
            ProfileFuels12Access<Sa.dbs.Dbo.Per> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Per>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool  ProcessDataToDbo(DbsStage.ProcessData staging)
        {
            try
            {
                Sa.dbs.Dbo.ProcessData dbo = new Sa.dbs.Dbo.ProcessData();
                dbo.SubmissionID = _submissionId;
                dbo.UnitID = (int)staging.UnitID;
                dbo.Property = staging.Property;
                if (staging.RptDVal != null)
                    dbo.RptDValue = (DateTime)staging.RptDVal;
                dbo.RptValue = staging.RptNVal;
                dbo.RptTValue = staging.RptTVal;
                dbo.RptUOM = staging.UOM;
                if (_configUnitIDForHycProcessId != null &&   dbo.UnitID ==_configUnitIDForHycProcessId 
                    && dbo.Property == "ReactorPress")
                    dbo.Property = "ReactorPressure";
                ProfileFuels12Access<Sa.dbs.Dbo.ProcessData> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.ProcessData>();
                return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool  ResidToDbo(DbsStage.Resid staging)
        {
            try
            {
            Sa.dbs.Dbo.Resid dbo = new Sa.dbs.Dbo.Resid();
            dbo.SubmissionID = _submissionId;
            dbo.BlendID = (int)staging.BlendID;
            dbo.Density = staging.Density;
            dbo.PourPT = staging.PourPt;
            dbo.Sulfur = staging.Sulfur;
            dbo.KMT = staging.KMT;
            dbo.ViscCSAtTemp = staging.ViscCSAtTemp;
            dbo.ViscTemp = staging.ViscTemp;
            dbo.Grade = staging.Grade;
            //dbo.Submission = staging.Submission;
            ProfileFuels12Access<Sa.dbs.Dbo.Resid> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Resid>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool  RPFResidToDbo(DbsStage.RPFResid staging)
        {
            try
            {
            Sa.dbs.Dbo.RPFResid dbo = new Sa.dbs.Dbo.RPFResid();
            dbo.SubmissionID = _submissionId;
            dbo.Density = staging.Density;
            dbo.Sulfur = staging.Sulfur;
            dbo.ViscCSAtTemp = staging.ViscCSAtTemp;
            dbo.ViscTemp = staging.ViscTemp;
            dbo.EnergyType = staging.EnergyType.Trim();
            ProfileFuels12Access<Sa.dbs.Dbo.RPFResid> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.RPFResid>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool  SteamToDbo(DbsStage.Steam staging)
        {
            try
            {
            Sa.dbs.Dbo.SteamSystem dbo = new Sa.dbs.Dbo.SteamSystem();
            dbo.SubmissionID = _submissionId;
            dbo.PressureRange = staging.PressureRange;
            dbo.ActualPress = staging.ActualPress;
            dbo.H2PlantExport = staging.H2PlantExport;
            dbo.FiredProcessHeater = staging.FiredProcessHeater;
            dbo.FiredBoiler = staging.FiredBoiler;
            dbo.FCCCatCoolers = staging.FCCCatCoolers;
            dbo.FCCStackGas = staging.FCCStackGas;
            dbo.FluidCokerCOBoiler = staging.FluidCokerCOBoiler;
            dbo.Calciner = staging.Calciner;
            dbo.FTCogen = staging.FTCogen;
            dbo.WasteHeatFCC = staging.WasteHeatFCC;
            dbo.WasteHeatTCR = staging.WasteHeatTCR;
            dbo.WasteHeatCOK = staging.WasteHeatCOK;
            dbo.WasteHeatOth = staging.WasteHeatOth;
            dbo.OthSource = staging.OthSource;
            dbo.STProd = staging.STProd;
            dbo.Sold = staging.Sold;
            dbo.Pur = staging.Pur;
            dbo.NetPur = staging.NetPur;
            dbo.TotSupply = staging.TotSupply;
            dbo.ConsProcessCDU = staging.ConsProcessCDU;
            dbo.ConsProcessCOK = staging.ConsProcessCOK;
            dbo.ConsProcessFCC = staging.ConsProcessFCC;
            dbo.ConsProcessOth = staging.ConsProcessOth;
            dbo.ConsReboil = staging.ConsReboil;
            dbo.ConsOthHeaters = staging.ConsOthHeaters;
            dbo.ConsCondTurb = staging.ConsCondTurb;
            dbo.ConsTopTurbHigh = staging.ConsTopTurbHigh;
            dbo.ConsTopTurbLow = staging.ConsTopTurbLow;
            dbo.ConsCombAirPreheat = staging.ConsCombAirPreheat;
            dbo.ConsPressControlHigh = staging.ConsPressControlHigh;
            dbo.ConsPressControlLow = staging.ConsPressControlLow;
            dbo.ConsTracingHeat = staging.ConsTracingHeat;
            dbo.ConsDeaerators = staging.ConsDeaerators;
            dbo.ConsFlares = staging.ConsFlares;
            dbo.ConsOth = staging.ConsOth;
            dbo.TotCons = staging.TotCons;
            ProfileFuels12Access<Sa.dbs.Dbo.SteamSystem> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.SteamSystem>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

        private bool GetSubmissionEntityAndSubEntities(DbsStage.Submission staging)
        {
            try
            {
            Sa.dbs.Dbo.Submission dbo = new Sa.dbs.Dbo.Submission();
            dbo.SubmissionID = _submissionId;
            //dbo.ClientSubmissionID = staging.ClientSubmissionID;
            dbo.PeriodStart = staging.PeriodBeg;
            dbo.PeriodEnd = staging.PeriodEnd;
            dbo.NumDays = (short)staging.PeriodDuration_Days;
            dbo.RptCurrency = staging.RptCurrency;
            dbo.UOM = staging.UOM;
            dbo.RefineryID = staging.RefineryID;
            //dbo.Notes = staging.Notes;
            //dbo.tsInserted = staging.tsInserted;
            //dbo.tsInsertedHost = staging.tsInsertedHost;
            //dbo.tsInsertedUser = staging.tsInsertedUser;
            //dbo.tsInsertedApp = staging.tsInsertedApp;
            ProfileFuels12Access<Sa.dbs.Dbo.Submission> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Submission>();
           return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
            

            /*
             * If I need this part, can change the public void *ToDbo to from void to the Entity and call them below
            dbo.Absences = staging.Absences;
            dbo.Configs = staging.Configs;
            dbo.ConfigBuoys = staging.ConfigBuoys;
            dbo.ConfigRS = staging.ConfigRS;
            dbo.Crudes = staging.Crudes;
            dbo.Diesels = staging.Diesels;
            dbo.Emissions = staging.Emissions;
            dbo.Energies = staging.Energies;
            dbo.FiredHeaters = staging.FiredHeaters;
            dbo.Gasolines = staging.Gasolines;
            dbo.GeneralMiscs = staging.GeneralMiscs;
            dbo.Inventories = staging.Inventories;
            dbo.Kerosenes = staging.Kerosenes;
            dbo.LPGs = staging.LPGs;
            dbo.MaintRouts = staging.MaintRouts;
            dbo.MaintTAs = staging.MaintTAs;
            dbo.MarineBunkers = staging.MarineBunkers;
            dbo.MExps = staging.MExps;
            dbo.MiscInputs = staging.MiscInputs;
            dbo.Opexes = staging.Opexes;
            dbo.Personnels = staging.Personnels;
            dbo.ProcessDatas = staging.ProcessDatas;
            dbo.Resids = staging.Resids;
            dbo.RPFResids = staging.RPFResids;
            dbo.Steams = staging.Steams;
            dbo.Yields = staging.Yields;
             */
        }


        private bool YieldToDbo(DbsStage.Yield staging, short? lastSortKeyUsed)
        {
            try
            {
                Sa.dbs.Dbo.Yield dbo = new Sa.dbs.Dbo.Yield();
                dbo.SubmissionID = _submissionId;
                dbo.MaterialID = staging.MaterialID;
                dbo.Category = staging.Category;
                dbo.SortKey = (int)lastSortKeyUsed;
                dbo.MaterialName = staging.MaterialName;
                dbo.BBL = (double)staging.BBL;
                dbo.Density = staging.Density;
                dbo.MT = staging.MT;
                ProfileFuels12Access<Sa.dbs.Dbo.Yield> pfDB = new ProfileFuels12Access<Sa.dbs.Dbo.Yield>();
            return pfDB.Insert(dbo);
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

       

    }
}
