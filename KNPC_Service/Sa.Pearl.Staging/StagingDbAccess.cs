﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;
using DbsDbo = Sa.dbs.Dbo;
using DbsDim = Sa.dbs.Dim;
using DbsStage = Sa.dbs.Stage;

namespace Sa.Pearl
{
    public class StagingDbAccess<TEntity> where TEntity : class
    {
        #region Private members

        private DbContext context;
        private DbSet<TEntity> entityInstance;

        #endregion

        #region Constructors

        public StagingDbAccess(DbContext context)
        {
            this.context = context;
        }

        public StagingDbAccess()
        {
            this.context = new DbsStage.StageEntities();
        }

        #endregion

        #region Properties

        public DbContext Context
        {
            get
            {
                return context;
            }
        }

        public DbSet<TEntity> EntityInstance
        {
            get
            {
                if (this.entityInstance == null)
                    this.entityInstance = context.Set<TEntity>();

                return this.entityInstance;
            }
        }

        #endregion

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> where)
        {
            //return Repository.GetAll(x => x.CustomerID == customerId).ToList();
            return EntityInstance.Where(where);
        }

        public IEnumerable<TEntity> Select(TEntity entity, int Id)
        {
            var submissioQuery = from subm in EntityInstance
                                 select subm;
            List<TEntity> submList = submissioQuery.ToList();
            return submList;
        }

        public void Insert(TEntity entity)
        {
            try
            {
                EntityInstance.Add(entity);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
        }

        public void Update(TEntity entity)
        {
            EntityInstance.Attach(entity);
            context.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            EntityInstance.Remove(entity);
            Context.SaveChanges();
        }
    }
}
