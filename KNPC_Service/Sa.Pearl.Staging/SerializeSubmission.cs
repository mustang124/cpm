﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Reflection;
using staging = Sa.dbs.Stage;
using System.Linq;
using System.Collections;

namespace Sa.Pearl
{
    public class SerializeSubmission
    {
        public List<String> WriteToXml(string filepath, dbs.Stage.Submission stageSubmission, bool replaceSubmissionId)
        {
            {
                string replacementSubmissionId = null;
                if (replaceSubmissionId)
                    replacementSubmissionId = stageSubmission.ClientSubmissionID.ToString();
                List<String> errors = new List<String>();

                try
                {
                    XmlWriterSettings setting = new XmlWriterSettings();
                    setting.ConformanceLevel = ConformanceLevel.Auto;

                    using (XmlWriter writer = XmlWriter.Create(filepath, setting))
                    {
                        writer.WriteStartElement("Submission");//start of the file

                        //write the elements that belong just to the Submission (have to write these separately)
                        foreach (PropertyInfo propertyInfo in stageSubmission.GetType().GetProperties().Where(propertyInfo => propertyInfo.GetGetMethod().GetParameters().Count() == 0))
                        {
                            if (!propertyInfo.PropertyType.ToString().Contains("ICollection"))//holy cow this was hard to figure out how to do. there has to be an easier way to know if it is a collection or not
                            {
                                switch (propertyInfo.Name.ToString())
                                {
                                    case "SubmissionID":
                                        string submissionIdToUse = (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString();
                                        if (replaceSubmissionId)
                                            submissionIdToUse = replacementSubmissionId;  //Doing this at Joe's request.
                                        writer.WriteElementString(propertyInfo.Name.ToString(), submissionIdToUse);
                                        break;
                                    case "ClientSubmissionID":
                                        writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    case "PeriodDuration_Days":
                                        writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    case "RefineryID":
                                        writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    case "UOM":
                                        writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    case "Notes":
                                        writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    case "RptCurrency":
                                        writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    case "PeriodBeg":
                                        writer.WriteElementString("PeriodBeg", (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    case "PeriodEnd":
                                        writer.WriteElementString("PeriodEnd", (propertyInfo.GetValue(stageSubmission, null) ?? string.Empty).ToString());
                                        break;
                                    default://any other field doesn't get written to the XML
                                        break;
                                }
                            }
                        }

                        //then go through each table and add them one at a time (got to be a better way to do this, right?)
                        foreach (staging.Absence a in stageSubmission.Absences) { XMLStuff.WriteXML(a, "Absence", writer, replacementSubmissionId); }
                        foreach (staging.Comment c in stageSubmission.Comments) { XMLStuff.WriteXML(c, "Comment", writer, replacementSubmissionId); }
                        foreach (staging.Config c in stageSubmission.Configs) { XMLStuff.WriteXML(c, "Config", writer, replacementSubmissionId); }
                        foreach (staging.ConfigBuoy a in stageSubmission.ConfigBuoys) { XMLStuff.WriteXML(a, "ConfigBuoy", writer, replacementSubmissionId); }
                        foreach (staging.ConfigR a in stageSubmission.ConfigRS) { XMLStuff.WriteXML(a, "ConfigRS", writer, replacementSubmissionId); }
                        foreach (staging.Crude a in stageSubmission.Crudes) { XMLStuff.WriteXML(a, "Crude", writer, replacementSubmissionId); }
                        foreach (staging.Diesel a in stageSubmission.Diesels) { XMLStuff.WriteXML(a, "Diesel", writer, replacementSubmissionId); }
                        foreach (staging.Emission a in stageSubmission.Emissions) { XMLStuff.WriteXML(a, "Emission", writer, replacementSubmissionId); }
                        foreach (staging.Energy a in stageSubmission.Energies) { XMLStuff.WriteXML(a, "Energy", writer, replacementSubmissionId); }
                        foreach (staging.FinProdProp a in stageSubmission.FinProdProps) { XMLStuff.WriteXML(a, "FinProdProp", writer, replacementSubmissionId); }
                        foreach (staging.FiredHeater a in stageSubmission.FiredHeaters) { XMLStuff.WriteXML(a, "FiredHeater", writer, replacementSubmissionId); }
                        foreach (staging.Gasoline g in stageSubmission.Gasolines) { XMLStuff.WriteXML(g, "Gasoline", writer, replacementSubmissionId); }
                        foreach (staging.GeneralMisc a in stageSubmission.GeneralMiscs) { XMLStuff.WriteXML(a, "GeneralMisc", writer, replacementSubmissionId); }
                        foreach (staging.Inventory a in stageSubmission.Inventories) { XMLStuff.WriteXML(a, "Inventory", writer, replacementSubmissionId); }
                        foreach (staging.Kerosene a in stageSubmission.Kerosenes) { XMLStuff.WriteXML(a, "Kerosene", writer, replacementSubmissionId); }
                        foreach (staging.LPG a in stageSubmission.LPGs) { XMLStuff.WriteXML(a, "LPG", writer, replacementSubmissionId); }
                        foreach (staging.MaintRout a in stageSubmission.MaintRouts) { XMLStuff.WriteXML(a, "MaintRout", writer, replacementSubmissionId); }
                        foreach (staging.MaintTA a in stageSubmission.MaintTAs) { XMLStuff.WriteXML(a, "MaintTA", writer, replacementSubmissionId); }
                        foreach (staging.MarineBunker a in stageSubmission.MarineBunkers) { XMLStuff.WriteXML(a, "MarineBunker", writer, replacementSubmissionId); }
                        foreach (staging.MExp a in stageSubmission.MExps) { XMLStuff.WriteXML(a, "MExp", writer, replacementSubmissionId); }
                        foreach (staging.MiscInput a in stageSubmission.MiscInputs) { XMLStuff.WriteXML(a, "MiscInput", writer, replacementSubmissionId); }
                        foreach (staging.OpEx a in stageSubmission.OpExes) { XMLStuff.WriteXML(a, "OpEx", writer, replacementSubmissionId); }
                        foreach (staging.Personnel a in stageSubmission.Personnels) { XMLStuff.WriteXML(a, "Personnel", writer, replacementSubmissionId); }
                        foreach (staging.ProcessData a in stageSubmission.ProcessDatas) { XMLStuff.WriteXML(a, "ProcessData", writer, replacementSubmissionId); }
                        foreach (staging.Resid r in stageSubmission.Resids) { XMLStuff.WriteXML(r, "Resid", writer, replacementSubmissionId); }
                        foreach (staging.RPFResid a in stageSubmission.RPFResids) { XMLStuff.WriteXML(a, "RPFResid", writer, replacementSubmissionId); }
                        foreach (staging.Steam a in stageSubmission.Steams) { XMLStuff.WriteXML(a, "Steam", writer, replacementSubmissionId); }
                        foreach (staging.Yield a in stageSubmission.Yields) { XMLStuff.WriteXML(a, "Yield", writer, replacementSubmissionId); }

                        //and write the end of the file
                        writer.WriteEndElement();
                        writer.Flush();
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(ex.Message.ToString());
                }

                return errors;
            }

        }
    }
    internal class XMLStuff
    {

        public static void WriteXML<T>(T a, string name, XmlWriter writer, string replacementSubmissionId = null)
        {
            //generic method to write each table to XML
            writer.WriteStartElement(name);
            foreach (PropertyInfo p in a.GetType().GetProperties())
            {
                if (p.Name.ToString() == "SubmissionID")
                {
                    string submissionId = replacementSubmissionId ?? (p.GetValue(a, null) ?? string.Empty).ToString();
                    writer.WriteElementString(p.Name.ToString(), submissionId);
                }
                else if (p.Name.ToString() != "Submission")
                {
                    writer.WriteElementString(p.Name.ToString(), (p.GetValue(a, null) ?? string.Empty).ToString());
                }
            }

            writer.WriteEndElement();

        }

        public static void WriteClassToXML(object theClass, XmlWriter writer)
        {

            Type objType = theClass.GetType();

            writer.WriteStartElement(objType.Name);

            PropertyInfo[] properties = objType.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                Type tColl = typeof(ICollection<>);
                Type t = property.PropertyType;
                string name = property.Name;


                object propValue = property.GetValue(theClass, null);
                //check for nested classes as properties
                if (property.PropertyType.Assembly == objType.Assembly)
                {
                    //string _result = string.Format("{0}{1}:", indentString, property.Name);
                    //log.Info(_result);
                    //LogObject(propValue, indent + 2);
                }
                else
                {
                    //string _result = string.Format("{0}{1}: {2}", indentString, property.Name, propValue);
                    //log.Info(_result);
                }

                //check for collection
                if (t.IsGenericType && tColl.IsAssignableFrom(t.GetGenericTypeDefinition()) ||
                    t.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == tColl))
                {
                    ////var get = property.GetGetMethod();
                    IEnumerable listObject = (IEnumerable)property.GetValue(theClass, null);
                    if (listObject != null)
                    {
                        foreach (object o in listObject)
                        {
                            writer.WriteStartElement(name);
                            foreach (PropertyInfo p in o.GetType().GetProperties())
                            {
                                writer.WriteElementString(p.Name.ToString(), (p.GetValue(o, null) ?? string.Empty).ToString());
                            }
                            //writer.WriteElementString(o.Name.ToString(), (o.GetValue(theClass, null) ?? string.Empty).ToString());
                            //LogObject(o, indent + 2);
                            writer.WriteEndElement();
                        }
                    }
                }
            }
            writer.WriteEndElement();
        }
    }
}
