﻿

CREATE   PROC [dbo].[spProfileLiteKPIData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListQTR dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd)


DECLARE	
	@EII real, @EII_YTD real, @EII_AVG real, 
	@EnergyUseDay real, @EnergyUseDay_YTD real, @EnergyUseDay_AVG real, 
	@TotStdEnergy real, @TotStdEnergy_YTD real, @TotStdEnergy_AVG real, 

	@RefUtilPcnt real, @RefUtilPcnt_YTD real, @RefUtilPcnt_AVG real, 
	@EDC real, @EDC_YTD real, @EDC_AVG real, 

	@ProcessEffIndex real, @ProcessEffIndex_YTD real, @ProcessEffIndex_AVG real, 

	@VEI real, @VEI_YTD real, @VEI_AVG real, 
	@ReportLossGain real, @ReportLossGain_YTD real, @ReportLossGain_AVG real, 
	@EstGain real, @EstGain_YTD real, @EstGain_AVG real, 
	@NetInputBPD real, @NetInputBPD_YTD real, @NetInputBPD_AVG real,
	
	@OpAvail real, @OpAvail_YTD real, @OpAvail_AVG real, 
	@MechUnavailTA real, @MechUnavailTA_YTD real, @MechUnavailTA_AVG real, 
	@NonTAUnavail real, @NonTAUnavail_YTD real, @NonTAUnavail_AVG real, 

	@PersIndex real, @PersIndex_YTD real, @PersIndex_AVG real, 
	@AnnTAWhr real, @AnnTAWhr_YTD real, @AnnTAWhr_AVG real,
	@NonTAWHr real, @NonTAWHr_YTD real, @NonTAWHr_AVG real,
	@TAWHrEDC real,
	
	@MaintIndex real, @MaintIndex_YTD real, @MaintIndex_AVG real, 
	@RoutCost real, @RoutCost_YTD real, @RoutCost_AVG real, 
	@AnnTACost real, @AnnTACost_YTD real, @AnnTACost_AVG real,

	@NEOpexEDC real, @NEOpexEDC_YTD real, @NEOpexEDC_AVG real, 
	@NEOpex real, @NEOpex_YTD real, @NEOpex_AVG real, 

	@OpexUEDC real, @OpexUEDC_YTD real, @OpexUEDC_AVG real,
	@EnergyCost real, @EnergyCost_YTD real, @EnergyCost_AVG real, 
	@TAAdj real, @TAAdj_YTD real, @TAAdj_AVG real,
	@TotCashOpex real, @TotCashOpex_YTD real, @TotCashOpex_AVG real,
	@UEDC real, @UEDC_YTD real, @UEDC_AVG real
	
SELECT @EII = NULL, @EII_YTD = NULL, @EII_AVG = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_YTD = NULL, @EnergyUseDay_AVG = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_YTD = NULL, @TotStdEnergy_AVG = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_YTD = NULL, @RefUtilPcnt_AVG = NULL, 
	@EDC = NULL, @EDC_YTD = NULL, @EDC_AVG = NULL, 
	@UEDC = NULL, @UEDC_YTD = NULL, @UEDC_AVG = NULL, 
	@VEI = NULL, @VEI_YTD = NULL, @VEI_AVG = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_YTD = NULL, @ReportLossGain_AVG = NULL, 
	@EstGain = NULL, @EstGain_YTD = NULL, @EstGain_AVG = NULL, 
	@OpAvail = NULL, @OpAvail_YTD = NULL, @OpAvail_AVG = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_YTD = NULL, @MechUnavailTA_AVG = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_YTD = NULL, @NonTAUnavail_AVG = NULL, 
	@MaintIndex = NULL, @MaintIndex_YTD = NULL, @MaintIndex_AVG = NULL,
	@RoutCost = NULL, @RoutCost_YTD = NULL, @RoutCost_AVG = NULL, 
	@PersIndex = NULL, @PersIndex_YTD = NULL, @PersIndex_AVG = NULL, 
	@AnnTAWhr = NULL, @AnnTAWhr_YTD = NULL, @AnnTAWhr_AVG = NULL,
	@NonTAWHr = NULL, @NonTAWHr_YTD = NULL, @NonTAWHr_AVG = NULL,
	@NEOpexEDC = NULL, @NEOpexEDC_YTD = NULL, @NEOpexEDC_AVG = NULL, 
	@NEOpex = NULL, @NEOpex_YTD = NULL, @NEOpex_AVG = NULL, 
	@OpexUEDC = NULL, @OpexUEDC_YTD = NULL, @OpexUEDC_AVG = NULL, 
	@TAAdj = NULL, @TAAdj_YTD = NULL, @TAAdj_AVG = NULL,
	@EnergyCost = NULL, @EnergyCost_YTD = NULL, @EnergyCost_AVG = NULL, 
	@TotCashOpex = NULL, @TotCashOpex_YTD = NULL, @TotCashOpex_AVG = NULL

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

SELECT @EII = EII, @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
	, @RefUtilPcnt = RefUtilPcnt, @EDC = EDC, @UEDC = UEDC
	, @VEI = VEI, @ReportLossGain = ReportLossGain, @EstGain = EstGain
	, @ProcessEffIndex = ProcessEffIndex, @NetInputBPD = NetInputBPD
	, @OpAvail = OpAvail, @MechUnavailTA = MechUnavailTA, @NonTAUnavail = MechUnavailRout + RegUnavail
	, @MaintIndex = MaintIndex, @RoutCost = RoutCost, @AnnTACost = AnnTACost
	, @PersIndex = PersIndex, @AnnTAWhr = NULL, @NonTAWHr = NULL
	, @NEOpexEDC = NEOpexEDC, @NEOpex = NEOpex
	, @OpexUEDC = OpexUEDC, @TAAdj = AnnTACost, @EnergyCost = EnergyCost, @TotCashOpex = TotCashOpex
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubListMonth)
SELECT @TAWHrEDC = (ISNULL(p.OCCTAWHrEDC,0) + ISNULL(p.MPSTAWHrEDC,0)) FROM dbo.SLPersTAAdj(@SubListMonth, @FactorSet) p
SELECT @AnnTAWHr = @TAWHrEDC*@EDC*100

SELECT @EII_YTD = EII, @EnergyUseDay_YTD = EnergyUseDay, @TotStdEnergy_YTD = TotStdEnergy
	, @RefUtilPcnt_YTD = RefUtilPcnt, @EDC_YTD = EDC, @UEDC_YTD = UEDC
	, @VEI_YTD = VEI, @ReportLossGain_YTD = ReportLossGain, @EstGain_YTD = EstGain
	, @ProcessEffIndex_YTD = ProcessEffIndex, @NetInputBPD_YTD = NetInputBPD
	, @OpAvail_YTD = OpAvail, @MechUnavailTA_YTD = MechUnavailTA, @NonTAUnavail_YTD = MechUnavailRout + RegUnavail
	, @MaintIndex_YTD = MaintIndex, @RoutCost_YTD = RoutCost, @AnnTACost_YTD = AnnTACost
	, @PersIndex_YTD = PersIndex, @AnnTAWhr_YTD = NULL, @NonTAWHr_YTD = NULL
	, @NEOpexEDC_YTD = NEOpexEDC, @NEOpex_YTD = NEOpex
	, @OpexUEDC_YTD = OpexUEDC, @TAAdj_YTD = AnnTACost, @EnergyCost_YTD = EnergyCost, @TotCashOpex_YTD = TotCashOpex
FROM dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr_YTD = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubListYTD)
SELECT @AnnTAWHr_YTD = @TAWHrEDC*@EDC_YTD*100

SELECT @EII_AVG = EII, @EnergyUseDay_AVG = EnergyUseDay, @TotStdEnergy_AVG = TotStdEnergy
	, @RefUtilPcnt_AVG = RefUtilPcnt, @EDC_AVG = EDC, @UEDC_AVG = UEDC
	, @VEI_AVG = VEI, @ReportLossGain_AVG = ReportLossGain, @EstGain_AVG = EstGain
	, @ProcessEffIndex_AVG = ProcessEffIndex, @NetInputBPD_AVG = NetInputBPD
	, @PersIndex_AVG = PersIndex, @AnnTAWhr_AVG = NULL, @NonTAWHr_AVG = NULL
	, @NEOpexEDC_AVG = NEOpexEDC, @NEOpex_AVG = NEOpex
	, @OpexUEDC_AVG = OpexUEDC, @TAAdj_AVG = AnnTACost, @EnergyCost_AVG = EnergyCost, @TotCashOpex_AVG = TotCashOpex
FROM dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr_YTD = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubList12Mo)
SELECT @AnnTAWHr_AVG = @TAWHrEDC*@EDC_AVG*100

SELECT @OpAvail_AVG = OpAvail, @MechUnavailTA_AVG = MechUnavailTA, @NonTAUnavail_AVG = MechUnavailRout + RegUnavail
	, @MaintIndex_AVG = MaintIndex, @RoutCost_AVG = RoutCost, @AnnTACost_AVG = AnnTACost
FROM dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM)


SELECT 
	EII = @EII, EII_YTD = @EII_YTD, EII_AVG = @EII_AVG, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_YTD = @EnergyUseDay_YTD, EnergyUseDay_AVG = @EnergyUseDay_AVG, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_YTD = @TotStdEnergy_YTD, TotStdEnergy_AVG = @TotStdEnergy_AVG, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_YTD = @RefUtilPcnt_YTD, UtilPcnt_AVG = @RefUtilPcnt_AVG, 
	EDC = @EDC, EDC_YTD = @EDC_YTD, EDC_AVG = @EDC_AVG, 
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_YTD = @EDC_YTD*@RefUtilPcnt_YTD/100, UtilUEDC_AVG = @EDC_AVG*@RefUtilPcnt_AVG/100, 
	
	ProcessEffIndex = @ProcessEffIndex, ProcessEffIndex_YTD = @ProcessEffIndex_YTD, ProcessEffIndex_AVG = @ProcessEffIndex_AVG, 
	VEI = @VEI, VEI_YTD = @VEI_YTD, VEI_AVG = @VEI_AVG, 
	NetInputBPD = @NetInputBPD, NetInputBPD_YTD = @NetInputBPD_YTD, NetInputBPD_AVG = @NetInputBPD_Avg,
	ReportLossGain = @ReportLossGain, ReportLossGain_YTD = @ReportLossGain_YTD, ReportLossGain_AVG = @ReportLossGain_AVG, 
	EstGain = @EstGain, EstGain_YTD = @EstGain_YTD, EstGain_AVG = @EstGain_AVG, 

	OpAvail = @OpAvail, OpAvail_YTD = @OpAvail_YTD, OpAvail_AVG = @OpAvail_AVG, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_YTD = @MechUnavailTA_YTD, MechUnavailTA_AVG = @MechUnavailTA_AVG, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_YTD = @NonTAUnavail_YTD, NonTAUnavail_AVG = @NonTAUnavail_AVG, 

	TotWHrEDC = @PersIndex, TotWHrEDC_YTD = @PersIndex_YTD, TotWHrEDC_AVG = @PersIndex_AVG, 
	AnnTAWhr = @AnnTAWhr, AnnTAWhr_YTD = @AnnTAWhr_YTD, AnnTAWhr_AVG = @AnnTAWhr_AVG,
	NonTAWHr = @NonTAWHr, NonTAWHr_YTD = @NonTAWHr_YTD, NonTAWHr_AVG = @NonTAWHr_AVG, 

	MaintIndex = @MaintIndex, MaintIndex_YTD = @MaintIndex_YTD, MaintIndex_AVG = @MaintIndex_AVG, 
	RoutCost = @RoutCost, RoutCost_YTD = @RoutCost_YTD, RoutCost_AVG = @RoutCost_AVG, 
	AnnTACost = @AnnTACost, AnnTACost_YTD = @AnnTACost_YTD, AnnTACost_AVG = @AnnTACost_AVG,

	NEOpexEDC = @NEOpexEDC, NEOpexEDC_YTD = @NEOpexEDC_YTD, NEOpexEDC_AVG = @NEOpexEDC_AVG, 
	NEOpex = @NEOpex, NEOpex_YTD = @NEOpex_YTD, NEOpex_AVG = @NEOpex_AVG, 

	TotCashOpexUEDC = @OpexUEDC, TotCashOpexUEDC_YTD = @OpexUEDC_YTD, TotCashOpexUEDC_AVG = @OpexUEDC_AVG, 
	TAAdj = @TAAdj, TAAdj_YTD = @TAAdj_YTD, TAAdj_AVG = @TAAdj_AVG,
	EnergyCost = @EnergyCost, EnergyCost_YTD = @EnergyCost_YTD, EnergyCost_AVG = @EnergyCost_AVG, 
	TotCashOpex = @TotCashOpex, TotCashOpex_YTD = @TotCashOpex_YTD, TotCashOpex_AVG = @TotCashOpex_AVG, 
	UEDC = @UEDC, UEDC_YTD = @UEDC_YTD, UEDC_AVG = @UEDC_AVG







