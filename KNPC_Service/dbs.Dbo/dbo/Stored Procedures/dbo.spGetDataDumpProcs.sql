﻿CREATE  PROCEDURE [dbo].[spGetDataDumpProcs](@RefineryID varchar(6), @ReportName varchar(50))
AS
IF EXISTS (SELECT * FROM dbo.Report_LU WHERE ReportName = @ReportName AND CustomGroup = 0)
	SELECT     ReportProcs.DataDumpTableName, ReportProcs.ProcName
	FROM         Report_LU INNER JOIN
	                      ReportProcs ON Report_LU.CustomGroup = ReportProcs.CustomGroup AND Report_LU.ReportCode = ReportProcs.ReportCode
	WHERE     (Report_LU.CustomGroup = 0) AND (Report_LU.ReportName = @ReportName AND ISNULL(ReportProcs.DataDumpTableName, '') <> '')
	ORDER BY ReportProcs.DataDumpOrder
ELSE BEGIN
	SELECT     ReportProcs.DataDumpTableName, ReportProcs.ProcName
	FROM         Report_LU INNER JOIN
	                      ReportProcs ON Report_LU.CustomGroup = ReportProcs.CustomGroup AND Report_LU.ReportCode = ReportProcs.ReportCode
	WHERE     (Report_LU.ReportName = @ReportName AND ISNULL(ReportProcs.DataDumpTableName, '') <> '')
		AND (Report_LU.CustomGroup IN (SELECT CustomGroup
			FROM CoCustom cc INNER JOIN TSort t ON t.CompanyID = cc.CompanyID
			WHERE cc.CustomType = 'R' AND t.RefineryID = @RefineryID))
	ORDER BY ReportProcs.DataDumpOrder
END

