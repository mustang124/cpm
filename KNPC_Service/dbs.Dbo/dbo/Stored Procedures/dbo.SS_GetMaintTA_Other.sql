﻿CREATE PROC [dbo].[SS_GetMaintTA_Other]

	@RefineryID nvarchar(10)
	
AS

SELECT mt.TAID,mt.UnitID,P.SortKey,RTRIM(P.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown,mt.TAExpLocal,mt.TACptlLocal, mt.TAOvhdLocal,mt.TALaborCostLocal,
            mt.TACostLocal,mt.TAOCCSTH,mt.TAOCCOVT,mt.TAMPSSTH,mt.TAMPSOVTPcnt,
            RTRIM(P.Description) as UnitName,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate,
            mt.TAExceptions 
            FROM dbo.MaintTA mt, ProcessID_LU P WHERE  mt.RefineryId=@RefineryID AND 
              P.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND 
             mt.ProcessID = P.ProcessID AND ProfileProcFacility = 'N' AND MaintDetails = 'Y'


