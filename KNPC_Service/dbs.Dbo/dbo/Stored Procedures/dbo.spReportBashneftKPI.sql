﻿
CREATE   PROC [dbo].[spReportBashneftKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @Dataset)

IF @SubmissionID IS NULL
BEGIN
	--RAISERROR (N'Data has not been uploaded for this month.', -- Message text.
 --          10, -- Severity,
 --          1 --State
 --          );
	RETURN 1
END
ELSE IF @CalcsNeeded IS NOT NULL
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE	
	@EII real, @EII_AVG real, @EII_QTR real, 
	@EnergyUseDay real, @EnergyUseDay_AVG real, @EnergyUseDay_QTR real, 
	@FuelUseDay real, @FuelUseDay_AVG real, @FuelUseDay_QTR real, 
	@TotStdEnergy real, @TotStdEnergy_AVG real, @TotStdEnergy_QTR real, 

	@ProcessUtilPcnt real, @ProcessUtilPcnt_AVG real, @ProcessUtilPcnt_QTR real, 
	@TotProcessEDC real, @TotProcessEDC_AVG real, @TotProcessEDC_QTR real, 
	@TotProcessUEDC real, @TotProcessUEDC_AVG real, @TotProcessUEDC_QTR real, 
	@OffsitesEDC real, @OffsitesEDC_AVG real, @OffsitesEDC_QTR real, 
	@Complexity real, @Complexity_AVG real, @Complexity_QTR real, 

	@VEI real, @VEI_AVG real, @VEI_QTR real, 
	@ReportLossGain real, @ReportLossGain_AVG real, @ReportLossGain_QTR real, 
	@EstGain real, @EstGain_AVG real, @EstGain_QTR real, 
	
	@OpAvail real, @OpAvail_AVG real, @OpAvail_QTR real, 
	@MechAvail real, @MechAvail_AVG real, @MechAvail_QTR real, 
	@MechUnavailTA real, @MechUnavailTA_AVG real, @MechUnavailTA_QTR real, 
	@NonTAMechUnavail real, @NonTAMechUnavail_AVG real, @NonTAMechUnavail_QTR real, 
	@NonTAUnavail real, @NonTAUnavail_AVG real, @NonTAUnavail_QTR real, 
	@RegUnavail real, @RegUnavail_AVG real, @RegUnavail_QTR real, 
	@TADD real, @TADD_AVG real, @TADD_QTR real,
	@NTAMDD real, @NTAMDD_AVG real, @NTAMDD_QTR real,
	@RPDD real, @RPDD_AVG real, @RPDD_QTR real,
	@NumDays_AVG real, @NumDays_QTR real,
	
	@MaintEffIndex real, @MaintEffIndex_AVG real, @MaintEffIndex_QTR real, 
	@TAAdj real, @TAAdj_AVG real, @TAAdj_QTR real, @TAEffIndex_Avg real,
	@AnnTACost real, @AnnTACost_AVG real, @AnnTACost_QTR real, 
	@RoutCost real, @RoutCost_AVG real, @RoutCost_QTR real, 
	@MaintEffDiv real, @MaintEffDiv_AVG real, @MaintEffDiv_QTR real, 
	@EDC real, @EDC_AVG real, @EDC_QTR real, 

	@nmPersEffIndex real, @nmPersEffIndex_AVG real, @nmPersEffIndex_QTR real,
	@NonMaintWHr real, @NonMaintWHr_AVG real, @NonMaintWHr_QTR real,
	@OCCNonMaintWHr real, @OCCNonMaintWHr_AVG real, @OCCNonMaintWHr_QTR real,
	@MPSNonMaintWHr real, @MPSNonMaintWHr_AVG real, @MPSNonMaintWHr_QTR real,
	@nmPersEffDiv real, @nmPersEffDiv_AVG real, @nmPersEffDiv_QTR real,

	@NEOpexEDC real, @NEOpexEDC_AVG real, @NEOpexEDC_QTR real, 
	@NEOpex real, @NEOpex_AVG real, @NEOpex_QTR real, 

	@OpexUEDC real, @OpexUEDC_AVG real, @OpexUEDC_QTR real,
	@EnergyCost real, @EnergyCost_AVG real, @EnergyCost_QTR real, 
	@TotCashOpex real, @TotCashOpex_AVG real, @TotCashOpex_QTR real,
	@UEDC real, @UEDC_AVG real, @UEDC_QTR real
DECLARE @ExchRate real, @ExchRate_AVG real, @ExchRate_QTR real

SELECT @EII = NULL, @EII_AVG = NULL, @EII_QTR = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_AVG = NULL, @EnergyUseDay_QTR = NULL, 
	@FuelUseDay = NULL, @FuelUseDay_AVG = NULL, @FuelUseDay_QTR = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_AVG = NULL, @TotStdEnergy_QTR = NULL, 
	@ProcessUtilPcnt = NULL, @ProcessUtilPcnt_AVG = NULL, @ProcessUtilPcnt_QTR = NULL, 
	@TotProcessEDC = NULL, @TotProcessEDC_AVG = NULL, @TotProcessEDC_QTR = NULL, 
	@TotProcessUEDC = NULL, @TotProcessUEDC_AVG = NULL, @TotProcessUEDC_QTR = NULL, 
	@Complexity = NULL, @Complexity_AVG = NULL, @Complexity_QTR = NULL, 
	@EDC = NULL, @EDC_AVG = NULL, @EDC_QTR = NULL, 
	@UEDC = NULL, @UEDC_AVG = NULL, @UEDC_QTR = NULL, 
	@VEI = NULL, @VEI_AVG = NULL, @VEI_QTR = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_AVG = NULL, @ReportLossGain_QTR = NULL, 
	@EstGain = NULL, @EstGain_AVG = NULL, @EstGain_QTR = NULL, 
	@OpAvail = NULL, @OpAvail_AVG = NULL, @OpAvail_QTR = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_AVG = NULL, @MechUnavailTA_QTR = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_AVG = NULL, @NonTAUnavail_QTR = NULL, 
	@RoutCost = NULL, @RoutCost_AVG = NULL, @RoutCost_QTR = NULL, 
	@nmPersEffIndex = NULL, @nmPersEffIndex_AVG = NULL, @nmPersEffIndex_QTR = NULL, 
	@NonMaintWHr = NULL, @NonMaintWHr_AVG = NULL, @NonMaintWHr_QTR = NULL,
	@OCCNonMaintWHr = NULL, @OCCNonMaintWHr_AVG = NULL, @OCCNonMaintWHr_QTR = NULL,
	@MPSNonMaintWHr = NULL, @MPSNonMaintWHr_AVG = NULL, @MPSNonMaintWHr_QTR = NULL,
	@nmPersEffDiv = NULL, @nmPersEffDiv_AVG = NULL, @nmPersEffDiv_QTR = NULL,
	@NEOpexEDC = NULL, @NEOpexEDC_AVG = NULL, @NEOpexEDC_QTR = NULL, 
	@NEOpex = NULL, @NEOpex_AVG = NULL, @NEOpex_QTR = NULL, 
	@OpexUEDC = NULL, @OpexUEDC_AVG = NULL, @OpexUEDC_QTR = NULL, 
	@TAAdj = NULL, @TAAdj_AVG = NULL, @TAAdj_QTR = NULL, @TAEffIndex_Avg = NULL,
	@EnergyCost = NULL, @EnergyCost_AVG = NULL, @EnergyCost_QTR = NULL, 
	@TotCashOpex = NULL, @TotCashOpex_AVG = NULL, @TotCashOpex_QTR = NULL


--- Everything Already Available in Gensum
SELECT	@ProcessUtilPcnt = ProcessUtilPcnt, @ProcessUtilPcnt_AVG = ProcessUtilPcnt_AVG,
	@OpAvail = OpAvail, @OpAvail_AVG = OpAvail_AVG,
	@MechAvail = MechAvail, @MechAvail_AVG = MechAvail_Avg,
	@EII = EII, @EII_AVG = EII_AVG, @VEI = VEI, @VEI_AVG = VEI_AVG, 
	@nmPersEffIndex = NonMaintPEI, @nmPersEffIndex_AVG = NonMaintPEI_AVG, 
	@EDC = EDC/1000, @EDC_AVG = EDC_AVG/1000, @UEDC = UEDC/1000, @UEDC_AVG = UEDC_AVG/1000,
	@NEOpexEDC = NEOpexEDC, @NEOpexEDC_AVG = NEOpexEDC_AVG
FROM Gensum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_QTR = MechUnavailTA_Ann, @MechUnavailTA_AVG = MechUnavailTA_Ann,
	@NonTAUnavail = 100 - OpAvail_Ann - MechUnavailTA_Ann, 	@NonTAMechUnavail = 100 - MechAvail_Ann - MechUnavailTA_Ann, @RegUnavail = MechAvail_Ann - OpAvail_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @NonTAUnavail_QTR = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NonTAMechUnavail_QTR = SUM((100 - MechAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @RegUnavail_QTR = SUM((MechAvail_Ann - OpAvail_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NumDays_QTR = SUM(s.NumDays)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail_Avg = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NonTAMechUnavail_AVG = SUM((100 - MechAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @RegUnavail_AVG = SUM((MechAvail_Ann - OpAvail_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	, @NumDays_AVG = SUM(s.NumDays)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

IF @NonTAUnavail < 0.05
	SELECT @NonTAUnavail = 0, @NonTAMechUnavail = 0, @RegUnavail = 0
IF @NonTAUnavail_QTR < 0.05
	SELECT @NonTAUnavail_QTR = 0, @NonTAMechUnavail_QTR = 0, @RegUnavail_QTR = 0
IF @NonTAUnavail_AVG < 0.05
	SELECT @NonTAUnavail_AVG = 0, @NonTAMechUnavail_AVG = 0, @RegUnavail_AVG = 0
	
SELECT @OpAvail = 100 - @NonTAUnavail - @MechUnavailTA, 
		@OpAvail_QTR = 100 - @NonTAUnavail_QTR - @MechUnavailTA, 
		@OpAvail_AVG = 100 - @NonTAUnavail_Avg - @MechUnavailTA

SELECT @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays,
	@nmPersEffDiv = f.NonMaintPersEffDiv*s.FractionOfYear,
	@MaintEffDiv = f.MaintEffDiv*s.FractionOfYear,
	@TotProcessEDC = TotProcessEDC/1000, @TotProcessUEDC = TotProcessUEDC/1000,
	@Complexity = Complexity
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

SELECT 	@Complexity_AVG = Complexity, @TotProcessEDC_AVG = AvgProcessEDC/1000, @TotProcessUEDC_AVG = AvgProcessUEDC/1000,
		@EnergyUseDay_AVG = EnergyUseDay, @TotStdEnergy_AVG = AvgStdEnergy, 
		@ReportLossGain_AVG = LossGainBpD, @EstGain_AVG = AvgStdGainBpD, 
		@nmPersEffDiv_AVG = NonMaintPersEffDiv, @MaintEffDiv_AVG = MaintEffDiv
FROM dbo.CalcAverageFactors(@RefineryID, @Dataset, @FactorSet, @Start12Mo, @PeriodEnd)

SELECT 	@EII_QTR = EII, @VEI_QTR = VEI, @EDC_QTR = AvgEDC/1000, @UEDC_QTR = AvgUEDC/1000, @Complexity_QTR = Complexity,
		@ProcessUtilPcnt_QTR = ProcessUtilPcnt, @TotProcessEDC_QTR = AvgProcessEDC/1000, @TotProcessUEDC_QTR = AvgProcessUEDC/1000,
		@EnergyUseDay_QTR = EnergyUseDay, @TotStdEnergy_QTR = AvgStdEnergy, @ReportLossGain_QTR = LossGainBpD, @EstGain_QTR = AvgStdGainBpD, 
		@nmPersEffDiv_QTR = NonMaintPersEffDiv, @MaintEffDiv_QTR = MaintEffDiv
FROM dbo.CalcAverageFactors(@RefineryID, @Dataset, @FactorSet, @Start3Mo, @PeriodEnd)
/*
SELECT @MaintEffDiv_QTR = SUM(MaintEffDiv*s.FractionOfYear),
	@nmPersEffDiv_QTR = SUM(NonMaintPersEffDiv*s.FractionOfYear)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet
SELECT 	@MaintEffDiv_AVG = SUM(MaintEffDiv*s.FractionOfYear)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
INNER JOIN MaintIndex i On i.SubmissionID = s.SubmissionID AND i.Currency = @Currency AND i.FactorSet = f.FactorSet
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet
*/
SELECT @FuelUseDay_AVG = SUM(PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU)/SUM(s.NumDays*1.0)
	, @FuelUseDay = SUM(CASE WHEN s.SubmissionID = @SubmissionID THEN PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU ELSE 0 END)/SUM(CASE WHEN s.SubmissionID = @SubmissionID THEN s.NumDays*1.0 END)
	, @FuelUseDay_QTR = SUM(CASE WHEN s.PeriodStart >= @Start3Mo THEN PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU ELSE 0 END)/SUM(CASE WHEN s.PeriodStart >= @Start3Mo THEN s.NumDays*1.0 END)
FROM EnergyTot e INNER JOIN Submissions s ON s.SubmissionID = e.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1


SELECT	@MaintEffIndex = RoutEffIndex + TAEffIndex_Avg, @TAEffIndex_Avg = TAEffIndex_Avg,
		@MaintEffIndex_AVG = MaintEffIndex_Avg
FROM MaintIndex
WHERE SubmissionID = @SubmissionID AND Currency = @Currency AND FactorSet = @FactorSet AND FactorSet = @FactorSet

SELECT	@MaintEffIndex_QTR = SUM(i.RoutEffIndex*f.MaintEffDiv)/SUM(f.MaintEffDiv) + @TAEffIndex_Avg
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
INNER JOIN MaintIndex i On i.SubmissionID = s.SubmissionID AND i.Currency = @Currency AND i.FactorSet = f.FactorSet
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @nmPersEffIndex_QTR = NonMaintPEI
FROM dbo.CalcAveragePersKPIs(@RefineryID, @Dataset, @Start3Mo, @PeriodEnd, @FactorSet)

SELECT @NonMaintWHr = NonMaintWHr FROM PersTot WHERE SubmissionID = @SubmissionID

SELECT @NonMaintWHr_AVG = NonMaintWHr
FROM dbo.SumPersTot(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)

SELECT @NonMaintWHr_QTR = NonMaintWHr
FROM dbo.SumPersTot(@RefineryID, @Dataset, @Start3Mo, @PeriodEnd)
/*
SELECT @NonMaintWHr_QTR = SUM(NonMaintWHr)
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
*/
SELECT @OCCNonMaintWHr_AVG = SUM(CASE WHEN SectionID IN ('OO','OA') THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@MPSNonMaintWHr_AVG = SUM(CASE WHEN SectionID IN ('MO','MT','MA') THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@OCCNonMaintWHr = SUM(CASE WHEN SectionID IN ('OO','OA') AND p.SubmissionID = @SubmissionID THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@MPSNonMaintWHr = SUM(CASE WHEN SectionID IN ('MO','MT','MA') AND p.SubmissionID = @SubmissionID THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@OCCNonMaintWHr_QTR = SUM(CASE WHEN SectionID IN ('OO','OA') AND s.PeriodStart >= @Start3Mo THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@MPSNonMaintWHr_QTR = SUM(CASE WHEN SectionID IN ('MO','MT','MA') AND s.PeriodStart >= @Start3Mo THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END)
	--@NonMaintWHr_AVG = SUM(CASE WHEN SectionID IN ('OO','OA','MO','MT','MA') THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END)
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.PersID IN (SELECT PersID FROM Pers_LU WHERE IncludeForInput = 'Y')

SELECT @AnnTACost = AllocAnnTACost, @RoutCost = CurrRoutCost
FROM dbo.SumMaintCost(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd, @Currency)

SELECT @AnnTACost_AVG = AllocAnnTACost, @RoutCost_AVG = CurrRoutCost
FROM dbo.SumMaintCost(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd, @Currency)

SELECT @AnnTACost_QTR = AllocAnnTACost, @RoutCost_QTR = CurrRoutCost
FROM dbo.SumMaintCost(@RefineryID, @Dataset, @Start3Mo, @PeriodEnd, @Currency)

SELECT @NEOpex = NEOpex, @EnergyCost = EnergyCost, @TotCashOpex = TotCashOpex, @TAAdj = TAAdj
FROM dbo.SumOpex(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd, @Currency)

SELECT @OpexUEDC = TotCashOpex
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpex_AVG = NEOpex, @EnergyCost_AVG = EnergyCost, @TotCashOpex_AVG = TotCashOpex, @TAAdj_AVG = TAAdj
FROM dbo.SumOpex(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, @Currency)

SELECT @OpexUEDC_AVG = TotCashOpexUEDC, @NEOpexEDC_AVG = NEOpexEDC
FROM dbo.CalcAverageOpex(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, @FactorSet, @Scenario, @Currency)

SELECT @NEOpex_QTR = NEOpex, @EnergyCost_QTR = EnergyCost, @TotCashOpex_QTR = TotCashOpex, @TAAdj_QTR = TAAdj
FROM dbo.SumOpex(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @Currency)

SELECT @OpexUEDC_QTR = TotCashOpexUEDC, @NEOpexEDC_QTR = NEOpexEDC
FROM dbo.CalcAverageOpex(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, @Scenario, @Currency)

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_AVG = @EnergyUseDay_AVG * 1.055, @EnergyUseDay_QTR = @EnergyUseDay_QTR * 1.055, 
			@FuelUseDay = @FuelUseDay * 1.055, @FuelUseDay_AVG = @FuelUseDay_AVG * 1.055, @FuelUseDay_QTR = @FuelUseDay_QTR * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_AVG = @TotStdEnergy_AVG * 1.055, @TotStdEnergy_QTR = @TotStdEnergy_QTR * 1.055

SELECT @OffsitesEDC = @EDC - @TotProcessEDC, @OffsitesEDC_QTR = @EDC_QTR - @TotProcessEDC_QTR, @OffsitesEDC_AVG = @EDC_AVG - @TotProcessEDC_AVG

SELECT @ExchRate = dbo.AvgExchangeRate('RUB', @PeriodStart, @PeriodEnd)
	, @ExchRate_QTR = dbo.AvgExchangeRate('RUB', MIN(CASE WHEN PeriodStart >= @Start3Mo THEN PeriodStart ELSE @PeriodStart END), @PeriodEnd)
	, @ExchRate_AVG = dbo.AvgExchangeRate('RUB', MIN(PeriodStart), @PeriodEnd)
FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodStart >= @Start12Mo AND PeriodStart < @PeriodEnd AND UseSubmission = 1

SELECT 
	EII = @EII, EII_AVG = @EII_AVG, EII_QTR = @EII_QTR, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_AVG = @EnergyUseDay_AVG, EnergyUseDay_QTR = @EnergyUseDay_QTR, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_AVG = @TotStdEnergy_AVG, TotStdEnergy_QTR = @TotStdEnergy_QTR, 
	FuelUseDay = @FuelUseDay, FuelUseDay_AVG = @FuelUseDay_AVG, FuelUseDay_QTR = @FuelUseDay_QTR, 

	ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_AVG = @ProcessUtilPcnt_AVG, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, 
	ProcessEDC = @TotProcessEDC, ProcessEDC_AVG = @TotProcessEDC_AVG, ProcessEDC_QTR = @TotProcessEDC_QTR, 
	ProcessUEDC = @TotProcessUEDC, ProcessUEDC_AVG = @TotProcessUEDC_AVG, ProcessUEDC_QTR = @TotProcessUEDC_QTR, 
	OffsitesEDC = @OffsitesEDC, OffsitesEDC_AVG = @OffsitesEDC_AVG, OffsitesEDC_QTR = @OffsitesEDC_QTR, 
	Complexity = @Complexity, Complexity_AVG = @Complexity_AVG, Complexity_QTR = @Complexity_QTR, 
	
	VEI = @VEI, VEI_AVG = @VEI_AVG, VEI_QTR = @VEI_QTR, 
	ReportLossGain = @ReportLossGain, ReportLossGain_AVG = @ReportLossGain_AVG, ReportLossGain_QTR = @ReportLossGain_QTR, 
	EstGain = @EstGain, EstGain_AVG = @EstGain_AVG, EstGain_QTR = @EstGain_QTR, 

	OpAvail = @OpAvail, OpAvail_AVG = @OpAvail_AVG, OpAvail_QTR = @OpAvail_QTR, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_AVG = @MechUnavailTA_AVG, MechUnavailTA_QTR = @MechUnavailTA_QTR, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_AVG = @NonTAUnavail_AVG, NonTAUnavail_QTR = @NonTAUnavail_QTR, 
	TADD = @MechUnavailTA/100*@NumDays, TADD_AVG = @MechUnavailTA/100*@NumDays_AVG, TADD_QTR = @MechUnavailTA/100*@NumDays_QTR,
	NTAMDD = @NonTAMechUnavail/100*@NumDays, NTAMDD_AVG = @NonTAMechUnavail_AVG/100*@NumDays_AVG, NTAMDD_QTR = @NonTAMechUnavail_QTR/100*@NumDays_QTR,
	RPDD = @RegUnavail/100*@NumDays, RPDD_AVG = @RegUnavail_AVG/100*@NumDays_AVG, RPDD_QTR = @RegUnavail_QTR/100*@NumDays_QTR,
	NumDays = @NumDays, NumDays_AVG = @NumDays_AVG, NumDays_QTR = @NumDays_QTR,

	nmPersEffIndex = @nmPersEffIndex, nmPersEffIndex_AVG = @nmPersEffIndex_AVG, nmPersEffIndex_QTR = @nmPersEffIndex_QTR, 
	NonMaintWHr = @NonMaintWHr/1000, NonMaintWHr_AVG = @NonMaintWHr_AVG/1000, NonMaintWHr_QTR = @NonMaintWHr_QTR/1000,
	OCCNonMaintWHr = @OCCNonMaintWHr/1000, OCCNonMaintWHr_AVG = @OCCNonMaintWHr_AVG/1000, OCCNonMaintWHr_QTR = @OCCNonMaintWHr_QTR/1000,
	MPSNonMaintWHr = @MPSNonMaintWHr/1000, MPSNonMaintWHr_AVG = @MPSNonMaintWHr_AVG/1000, MPSNonMaintWHr_QTR = @MPSNonMaintWHr_QTR/1000,
	nmPersEffDiv = @nmPersEffDiv/1000, nmPersEffDiv_AVG = @nmPersEffDiv_AVG/1000, nmPersEffDiv_QTR = @nmPersEffDiv_QTR/1000,

	MaintEffIndex = @MaintEffIndex, MaintEffIndex_AVG = @MaintEffIndex_AVG, MaintEffIndex_QTR = @MaintEffIndex_QTR, 
	AnnTACost = @TAAdj/1000, AnnTACost_AVG = @TAAdj_AVG/1000, AnnTACost_QTR = @TAAdj_QTR/1000,
	RoutCost = @RoutCost/1000, RoutCost_AVG = @RoutCost_AVG/1000, RoutCost_QTR = @RoutCost_QTR/1000, 
	MaintEffDiv = @MaintEffDiv/1000000, MaintEffDiv_QTR = @MaintEffDiv_QTR/1000000, MaintEffDiv_Avg = @MaintEffDiv_AVG/1000000,

	NEOpexEDC = @NEOpexEDC, NEOpexEDC_AVG = @NEOpexEDC_AVG, NEOpexEDC_QTR = @NEOpexEDC_QTR, 
	NEOpex = @NEOpex/1000, NEOpex_AVG = @NEOpex_AVG/1000, NEOpex_QTR = @NEOpex_QTR/1000, 
	EDC = @EDC, EDC_AVG = @EDC_AVG, EDC_QTR = @EDC_QTR,

	TotCashOpexUEDC = @OpexUEDC, TotCashOpexUEDC_AVG = @OpexUEDC_AVG, TotCashOpexUEDC_QTR = @OpexUEDC_QTR, 
	TAAdj = @TAAdj/1000, TAAdj_AVG = @TAAdj_AVG/1000, TAAdj_QTR = @TAAdj_QTR/1000,
	EnergyCost = @EnergyCost/1000, EnergyCost_AVG = @EnergyCost_AVG/1000, EnergyCost_QTR = @EnergyCost_QTR/1000, 
	TotCashOpex = @TotCashOpex/1000, TotCashOpex_AVG = @TotCashOpex_AVG/1000, TotCashOpex_QTR = @TotCashOpex_QTR/1000, 
	UEDC = @UEDC, UEDC_AVG = @UEDC_AVG, UEDC_QTR = @UEDC_QTR,
	
	ExchRate = @ExchRate, ExchRate_AVG = @ExchRate_AVG, ExchRate_QTR = @ExchRate_QTR








