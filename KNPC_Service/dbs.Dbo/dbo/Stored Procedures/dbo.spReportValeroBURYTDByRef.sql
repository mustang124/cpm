﻿CREATE    PROC [dbo].[spReportValeroBURYTDByRef] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1,
	@KEDC_YTD real = NULL OUTPUT, @MechAvail_YTD real = NULL OUTPUT, @AdjMaintIndex_YTD real = NULL OUTPUT, 
	@UtilPcnt_YTD real = NULL OUTPUT, @NEOpexEDC_YTD real = NULL OUTPUT, @EII_YTD real = NULL OUTPUT, 
	@TotWHrEDC_YTD real = NULL OUTPUT, @NetInputKBPD_YTD real = NULL OUTPUT, @CrudeKBPD_YTD real = NULL OUTPUT, @FCCRateKBPD_YTD real = NULL OUTPUT,  
	@NumContPers_YTD real = NULL OUTPUT, @RoutMechUnavail_YTD real = NULL OUTPUT, @TAMechUnavail_YTD real = NULL OUTPUT)

AS
SET NOCOUNT ON
DECLARE @SubmissionID int
SELECT @SubmissionID = SubmissionID
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, 'Actual')

SELECT	@KEDC_YTD = NULL, @MechAvail_YTD = NULL, @AdjMaintIndex_YTD = NULL, 
	@UtilPcnt_YTD = NULL, @NEOpexEDC_YTD = NULL, @EII_YTD = NULL, 
	@TotWHrEDC_YTD = NULL, @NetInputKBPD_YTD = NULL, @CrudeKBPD_YTD = NULL, @FCCRateKBPD_YTD = NULL,  
	@NumContPers_YTD = NULL, @RoutMechUnavail_YTD = NULL, @TAMechUnavail_YTD = NULL
IF @SubmissionID IS NULL
	RETURN 0

CREATE TABLE #Subs
(	SubmissionID int, 
	NumDays real,
	FCCUtilCap real NULL,
	CDUUtilCap real NULL
)
INSERT #Subs 
SELECT SubmissionID, NumDays,
	FCCUtilCap = (SELECT SUM(UtilCap) FROM Config WHERE SubmissionID = Submissions.SubmissionID AND ProcessID = 'FCC'),
	CDUUtilCap = (SELECT SUM(UtilCap) FROM Config WHERE SubmissionID = Submissions.SubmissionID AND ProcessID = 'CDU')
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth <= @PeriodMonth AND DataSet = 'Actual' AND UseSubmission = 1

SELECT @KEDC_YTD = EDC_YTD/1000, @MechAvail_YTD = MechAvail_YTD, @AdjMaintIndex_YTD = MaintIndex_YTD,
	@UtilPcnt_YTD = UtilPcnt_YTD, @NEOpexEDC_YTD = NEOpexEDC_YTD, @EII_YTD = EII_YTD, @TotWHrEDC_YTD = TotWHrEDC_YTD,
	@NetInputKBPD_YTD = NetInputBPD_YTD, @CrudeKBPD_YTD = NULL, @FCCRateKBPD_YTD = NULL, 
	@RoutMechUnavail_YTD = NULL, @TAMechUnavail_YTD = NULL
FROM Gensum 
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @NumContPers_YTD = SUM(Contract)/(SELECT SUM(NumDays) FROM #subs)*365.0/2080.0
FROM Pers p 
WHERE SubmissionID IN (SELECT SubmissionID FROM #subs) 
AND p.PersID NOT IN ('OCCTAADJ','MPSTAADJ') AND p.SectionID IN ('OO','OM','OA','MO','MM','MT','MA','MG')

SELECT @RoutMechUnavail_YTD = 100-MechAvail_Ann_YTD - MechUnavailTA_Ann, @TAMechUnavail_YTD = MechUnavailTA_Ann 
FROM MaintAvailCalc 
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet
IF @RoutMechUnavail_YTD < 0
	SET @RoutMechUnavail_YTD = 0

SELECT @FCCRateKBPD_YTD = SUM(FCCUtilCap*NumDays)/SUM(NumDays)/1000,
	@CrudeKBPD_YTD = SUM(CDUUtilCap*NumDays)/SUM(NumDays)/1000
FROM #subs


