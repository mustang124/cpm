﻿CREATE PROCEDURE [dbo].[SS_InputOpexAll]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS
BEGIN
		SELECT OCCSal, 
                   MPSSal, OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub,  
                   OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, OCCBen,  
                   MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock,  
                   MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, MaintMatl,  
                   ContMaintMatl, EquipMaint, MaintMatlST, ContMaintLabor,  
                   ContMaintInspect, ContMaintLaborST, OthContProcOp, OthContTransOp, 
                   OthContFire, OthContVacTrucks, OthContConsult, OthContSecurity, 
                   OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin, 
                   OthContLegal, OthContOth, OthCont, TAAdj, EnvirDisp, EnvirPermits,  
                   EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, Envir, EquipNonMaint, 
                   Tax, InsurBI, InsurPC, InsurOth, OthNonVolSupply, OthNonVolSafety,  
                   OthNonVolNonPersSafety, OthNonVolComm, OthNonVolDonations,  
                   OthNonVolNonContribPers, OthNonVolDues, OthNonVolTravel, OthNonVolTrain, 
                   OthNonVolComputer, OthNonVolTanks, OthNonVolExtraExpat, OthNonVolOth,  
                   OthNonVol, STNonVol, GAPers, ChemicalsAntiknock, ChemicalsAlkyAcid,  
                   ChemicalsLube, ChemicalsH2OTreat, ChemicalsProcess, ChemicalsOthAcid, 
                   ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2,  
                   ChemicalsClay, ChemicalsAmines, ChemicalsASESolv, ChemicalsWasteH2O,  
                   ChemicalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene, 
                   ChemicalsPropane, ChemicalsOthSolv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth,  
                   Chemicals, CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT,  
                   CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, CatalystsHYG, CatalystsS2Plant, 
                   CatalystsPetChem, CatalystsOth, Catalysts, PurOthN2, PurOthH2O, PurOthOth, PurOth, 
                   Royalties, EmissionsTaxes, EmissionsPurch, EmissionsCredits, OthVolOth, OthVol,  
                   GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen,  
                   OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, OthVolDemCrude,  
                   OthVolDemLightering, OthVolDemProd, STVol, TotCashOpEx, ExclFireSafety, ExclEnvirFines,  
                   ExclOth, TotExpExcl, STSal, STBen, PersCostExclTA, PersCost, EnergyCost, NEOpex  
                   FROM ProfileFuels12.dbo.OpExAll op  , dbo.Submissions s 
             WHERE op.SubmissionID=s.SubmissionID AND op.DataType='RPT' AND s.SubmissionID IN 
             (SELECT DISTINCT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1)
                
END


