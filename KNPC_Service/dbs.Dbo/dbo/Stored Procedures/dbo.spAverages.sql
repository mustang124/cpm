﻿CREATE               PROC [dbo].[spAverages] (@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @RefineryID varchar(6), @DataSet varchar(15)
DECLARE @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @RefineryID = RefineryID, @DataSet = DataSet
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
SELECT @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD FROM dbo.GetPeriods(@SubmissionID) p

IF @RefineryID IN ('106FL','150FL','322EUR') AND DATEPART(yy, @Start12Mo) < 2011 AND DATEPART(yy, @PeriodStart) >= 2011
BEGIN
	SELECT @Start12Mo = '12/31/2010', @Start24Mo = '12/31/2010'
	IF @StartYTD < @Start12Mo
		SET @StartYTD = @Start12Mo
END

DECLARE @SubListAVG dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListAVG(SubmissionID)
SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
INSERT @SubListYTD(SubmissionID)
SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)
INSERT @SubList24Mo(SubmissionID)
SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd)

EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Started'

--EXEC spAverageMaint @SubmissionID
-- 24-month maintenance indexes
UPDATE MaintIndex
SET RoutIndex_Avg = c.RoutIndex, RoutMatlIndex_Avg = c.RoutMatlIndex
	, MaintIndex_Avg = c.MaintIndex, MaintMatlIndex_Avg = c.MaintMatlIndex
	, RoutEffIndex_Avg = c.RoutEffIndex, MaintEffIndex_Avg = c.MaintEffIndex
FROM MaintIndex LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, NULL, NULL) c ON c.FactorSet = MaintIndex.FactorSet AND c.Currency = MaintIndex.Currency
WHERE MaintIndex.SubmissionID = @SubmissionID

-- Update Gensum with 24-month maintenance indexes
UPDATE GenSum
SET RoutIndex_Avg = m.RoutIndex_Avg, TAIndex_Avg = m.TAIndex_Avg, MaintIndex_Avg = m.MaintIndex_Avg
	, MEI_Rout_Avg = m.RoutEffIndex_Avg, MEI_TA_Avg = m.TAEffIndex_Avg, MEI_Avg = m.MaintEffIndex_Avg
FROM GenSum LEFT JOIN MaintIndex m ON m.SubmissionID = GenSum.SubmissionID AND m.FactorSet = GenSum.FactorSet AND m.Currency = GenSum.Currency

-- Year-to-date maintenance indexes
UPDATE GenSum
SET RoutIndex_YTD = c.RoutIndex, TAIndex_YTD = c.TAIndex, MaintIndex_YTD = c.MaintIndex
	, MEI_Rout_YTD = c.RoutEffIndex, MEI_TA_YTD = c.TAEffIndex, MEI_YTD = c.MaintEffIndex
FROM GenSum LEFT JOIN dbo.SLMaintIndex(@SubListYTD, 0, NULL, NULL) c ON c.FactorSet = GenSum.FactorSet AND c.Currency = GenSum.Currency
WHERE GenSum.SubmissionID = @SubmissionID

-- Annualized maintenance costs by unit
DECLARE @UnitAnnRout TABLE(UnitID int NOT NULL, Currency varchar(4) NOT NULL, RoutCost real NULL, RoutMatl real NULL)
INSERT @UnitAnnRout(UnitID, Currency, RoutCost, RoutMatl)
SELECT m.UnitID, m.Currency, RoutCost = SUM(m.CurrRoutCost)/SUM(s.FractionOfYear), RoutMatl = SUM(m.CurrRoutMatl)/SUM(s.FractionOfYear)
FROM MaintCost m INNER JOIN @SubList24Mo sl ON sl.SubmissionID = m.SubmissionID
INNER JOIN Submissions s ON s.SubmissionID = sl.SubmissionID
GROUP BY s.RefineryID, s.DataSet, m.UnitID, m.Currency

UPDATE @UnitAnnRout SET RoutCost = 0 WHERE RoutCost < 0
UPDATE @UnitAnnRout SET RoutMatl = 0 WHERE RoutMatl < 0

UPDATE MaintCost
SET AnnRoutCost = ISNULL(r.RoutCost, 0), AnnRoutMatl = r.RoutMatl,
	AnnMaintCost = ISNULL(MaintCost.AnnTACost, 0) + ISNULL(r.RoutCost, 0),
	AnnMaintMatl = ISNULL(MaintCost.AnnTAMatl, 0) + ISNULL(r.RoutMatl, 0)
FROM MaintCost INNER JOIN @UnitAnnRout r ON r.UnitID = MaintCost.UnitID AND r.Currency = MaintCost.Currency
WHERE MaintCost.SubmissionID = @SubmissionID

IF EXISTS (SELECT * FROM Submissions WHERE SubmissionID = @SubmissionID AND CalcsNeeded = 'M')
BEGIN
	UPDATE Submissions SET CalcsNeeded = NULL
	WHERE SubmissionID = @SubmissionID AND CalcsNeeded = 'M'

	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Maintenance Indexes updated'
END
ELSE BEGIN
--EXEC spAverageAvail @SubmissionID
UPDATE MaintCalc
SET PeriodHrs_Avg = a.PeriodHrs, PeriodHrsOSTA_Avg = a.PeriodHrsOSTA,
	MechUnavailTA_Act_Avg = a.MechUnavailTA_Act, MechAvailOSTA_Avg = a.MechAvailOSTA, 
	MechAvail_Act_Avg = a.MechAvail_Act, MechAvailSlow_Act_Avg = a.MechAvailSlow_Act, 
	OpAvail_Act_Avg = a.OpAvail_Act, OpAvailSlow_Act_Avg = a.OpAvailSlow_Act, 
	OnStream_Act_Avg = a.OnStream_Act, OnStreamSlow_Act_Avg = a.OnStreamSlow_Act,
	MechAvail_Ann_Avg = a.MechAvail_Ann, MechAvailSlow_Ann_Avg = a.MechAvailSlow_Ann, 
	OpAvail_Ann_Avg = a.OpAvail_Ann, OpAvailSlow_Ann_Avg = a.OpAvailSlow_Ann, 
	OnStream_Ann_Avg = a.OnStream_Ann, OnStreamSlow_Ann_Avg = a.OnStreamSlow_Ann
FROM MaintCalc LEFT JOIN dbo.SLUnitAvailability(@SubList24Mo) a ON a.UnitID = MaintCalc.UnitID AND a.FactorSet = dbo.GetCurrentFactorSet()
WHERE MaintCalc.SubmissionID = @SubmissionID

UPDATE MaintCalc
SET PeriodHrs_YTD = a.PeriodHrs, PeriodHrsOSTA_YTD = a.PeriodHrsOSTA,
	MechUnavailTA_Act_YTD = a.MechUnavailTA_Act, MechAvailOSTA_YTD = a.MechAvailOSTA, 
	MechAvail_Act_YTD = a.MechAvail_Act, MechAvailSlow_Act_YTD = a.MechAvailSlow_Act, 
	OpAvail_Act_YTD = a.OpAvail_Act, OpAvailSlow_Act_YTD = a.OpAvailSlow_Act, 
	OnStream_Act_YTD = a.OnStream_Act, OnStreamSlow_Act_YTD = a.OnStreamSlow_Act,
	MechAvail_Ann_YTD = a.MechAvail_Ann, MechAvailSlow_Ann_YTD = a.MechAvailSlow_Ann, 
	OpAvail_Ann_YTD = a.OpAvail_Ann, OpAvailSlow_Ann_YTD = a.OpAvailSlow_Ann, 
	OnStream_Ann_YTD = a.OnStream_Ann, OnStreamSlow_Ann_YTD = a.OnStreamSlow_Ann
FROM MaintCalc LEFT JOIN dbo.SLUnitAvailability(@SubListYTD) a ON a.UnitID = MaintCalc.UnitID AND a.FactorSet = dbo.GetCurrentFactorSet()
WHERE MaintCalc.SubmissionID = @SubmissionID

UPDATE MaintProcess
SET MechAvail_Ann_Avg=a.MechAvail_Ann, 
MechAvail_Act_Avg=a.MechAvail_Act, 
MechAvailSlow_Ann_Avg=a.MechAvailSlow_Ann, 
MechAvailSlow_Act_Avg=a.MechAvailSlow_Act, 
MechAvailOSTA_Avg=a.MechAvailOSTA, 
OpAvail_Ann_Avg=a.OpAvail_Ann, 
OpAvail_Act_Avg=a.OpAvail_Act, 
OpAvailSlow_Ann_Avg=a.OpAvailSlow_Ann, 
OpAvailSlow_Act_Avg=a.OpAvailSlow_Act, 
OnStream_Ann_Avg=a.OnStream_Ann, 
OnStream_Act_Avg=a.OnStream_Act, 
OnStreamSlow_Ann_Avg=a.OnStreamSlow_Ann, 
OnStreamSlow_Act_Avg=a.OnStreamSlow_Act, 
MechAvail_Ann_YTD=y.MechAvail_Ann, 
MechAvail_Act_YTD=y.MechAvail_Act, 
MechAvailSlow_Ann_YTD=y.MechAvailSlow_Ann, 
MechAvailSlow_Act_YTD=y.MechAvailSlow_Act, 
MechAvailOSTA_YTD=y.MechAvailOSTA, 
OpAvail_Ann_YTD=y.OpAvail_Ann, 
OpAvail_Act_YTD=y.OpAvail_Act, 
OpAvailSlow_Ann_YTD=y.OpAvailSlow_Ann, 
OpAvailSlow_Act_YTD=y.OpAvailSlow_Act, 
OnStream_Ann_YTD=y.OnStream_Ann, 
OnStream_Act_YTD=y.OnStream_Act, 
OnStreamSlow_Ann_YTD=y.OnStreamSlow_Ann, 
OnStreamSlow_Act_YTD=y.OnStreamSlow_Act, 
MechAvail_Ann_Target=t.MechAvail_Ann_Target, 
MechAvail_Act_Target=t.MechAvail_Act_Target, 
MechAvailSlow_Ann_Target=t.MechAvailSlow_Ann_Target, 
MechAvailSlow_Act_Target=t.MechAvailSlow_Act_Target, 
OpAvail_Ann_Target=t.OpAvail_Ann_Target, 
OpAvail_Act_Target=t.OpAvail_Act_Target, 
OpAvailSlow_Ann_Target=t.OpAvailSlow_Ann_Target, 
OpAvailSlow_Act_Target=t.OpAvailSlow_Act_Target, 
OnStream_Ann_Target=t.OnStream_Ann_Target, 
OnStream_Act_Target=t.OnStream_Act_Target, 
OnStreamSlow_Ann_Target=t.OnStreamSlow_Ann_Target, 
OnStreamSlow_Act_Target=t.OnStreamSlow_Act_Target
FROM MaintProcess LEFT JOIN dbo.SLProcessAvailability(@SubList24Mo) a ON a.ProcessID = MaintProcess.ProcessID AND a.FactorSet = MaintProcess.FactorSet
LEFT JOIN dbo.SLProcessAvailability(@SubListYTD) y ON y.ProcessID = MaintProcess.ProcessID AND y.FactorSet = MaintProcess.FactorSet
LEFT JOIN dbo.CalcProcessAvailTargets(@SubmissionID) t ON t.ProcessID = MaintProcess.ProcessID
WHERE MaintProcess.SubmissionID = @SubmissionID

UPDATE MaintAvailCalc
SET MechAvail_Ann_Avg=p.MechAvail_Ann_Avg,MechAvailSlow_Ann_Avg=p.MechAvailSlow_Ann_Avg, 
OpAvail_Ann_Avg=p.OpAvail_Ann_Avg, OpAvailSlow_Ann_Avg=p.OpAvailSlow_Ann_Avg, 
OnStream_Ann_Avg=p.OnStream_Ann_Avg, OnStreamSlow_Ann_Avg=p.OnStreamSlow_Ann_Avg, 
MechAvail_Act_Avg=p.MechAvail_Act_Avg, MechAvailSlow_Act_Avg=p.MechAvailSlow_Act_Avg, 
OpAvail_Act_Avg=p.OpAvail_Act_Avg, OpAvailSlow_Act_Avg=p.OpAvailSlow_Act_Avg, 
OnStream_Act_Avg=p.OnStream_Act_Avg, OnStreamSlow_Act_Avg=p.OnStreamSlow_Act_Avg, 
MechAvailOSTA_Avg=p.MechAvailOSTA_Avg,
MechAvail_Ann_YTD=p.MechAvail_Ann_YTD, MechAvailSlow_Ann_YTD=p.MechAvailSlow_Ann_YTD, 
OpAvail_Ann_YTD=p.OpAvail_Ann_YTD, OpAvailSlow_Ann_YTD=p.OpAvailSlow_Ann_YTD, 
OnStream_Ann_YTD=p.OnStream_Ann_YTD, OnStreamSlow_Ann_YTD=p.OnStreamSlow_Ann_YTD, 
MechAvail_Act_YTD=p.MechAvail_Act_YTD, MechAvailSlow_Act_YTD=p.MechAvailSlow_Act_YTD, 
OpAvail_Act_YTD=p.OpAvail_Act_YTD, OpAvailSlow_Act_YTD=p.OpAvailSlow_Act_YTD, 
OnStream_Act_YTD=p.OnStream_Act_YTD, OnStreamSlow_Act_YTD=p.OnStreamSlow_Act_YTD, 
MechAvailOSTA_YTD=p.MechAvailOSTA_YTD
FROM MaintAvailCalc LEFT JOIN MaintProcess p ON p.SubmissionID = MaintAvailCalc.SubmissionID AND p.FactorSet = MaintAvailCalc.FactorSet
INNER JOIN FactorSets fs ON fs.FactorSet = MaintAvailCalc.FactorSet
WHERE MaintAvailCalc.SubmissionID = @SubmissionID AND p.ProcessID = CASE WHEN fs.IdleUnitsInProcessResults = 'Y' THEN 'TotProc' ELSE 'OperProc' END

UPDATE Gensum
SET MechAvail = m.MechAvail_Ann, MechAvail_Avg = m.MechAvail_Ann_Avg, MechAvail_YTD = m.MechAvail_Ann_YTD, 
/*MechAvailSlow = m.MechAvailSlow_Ann, MechAvailSlow_Avg = m.MechAvailSlow_Ann_Avg, MechAvailSlow_YTD = m.MechAvailSlow_Ann_YTD, */
OpAvail = m.OpAvail_Ann, OpAvail_Avg = m.OpAvail_Ann_Avg, OpAvail_YTD = m.OpAvail_Ann_YTD, 
/*OpAvailSlow = m.OpAvailSlow_Ann, OpAvailSlow_Avg = m.OpAvailSlow_Ann_Avg, OpAvailSlow_YTD = m.OpAvailSlow_Ann_YTD, */
OnStream = m.OnStream_Ann, OnStream_Avg = m.OnStream_Ann_Avg, OnStream_YTD = m.OnStream_Ann_YTD, 
OnStreamSlow = m.OnStreamSlow_Ann, OnStreamSlow_Avg = m.OnStreamSlow_Ann_Avg, OnStreamSlow_YTD = m.OnStreamSlow_Ann_YTD
FROM Gensum LEFT JOIN MaintAvailCalc m ON m.SubmissionID = Gensum.SubmissionID AND m.FactorSet = Gensum.FactorSet
WHERE Gensum.SubmissionID = @SubmissionID

-- Average Crude Gravity and Sulfur
DECLARE @CrudeAPI_Avg real, @CrudeAPI_YTD real, @CrudeSulfur_Avg real, @CrudeSulfur_YTD real
SELECT @CrudeAPI_Avg = Gravity, @CrudeSulfur_Avg = Sulfur FROM dbo.SLAverageCrude(@SubListAVG)
SELECT @CrudeAPI_YTD = Gravity, @CrudeSulfur_YTD = Sulfur FROM dbo.SLAverageCrude(@SubListYTD)

-- Average Net Input, kBPD and Gain, %
DECLARE @GainPcnt_Avg real, @GainPcnt_YTD real, @NetInputBPD_Avg real, @NetInputBPD_YTD real
SELECT @NetInputBPD_Avg = NetInputkBPD, @GainPcnt_Avg = GainPcnt FROM dbo.SLAverageYield(@SubListAVG)
SELECT @NetInputBPD_YTD = NetInputkBPD, @GainPcnt_YTD = GainPcnt FROM dbo.SLAverageYield(@SubListYTD)

-- Average Personnel percentages and ratios
DECLARE @MPSAbsPcnt_Avg real, @MPSAbsPcnt_YTD real, @MPSOvtPcnt_Avg real, @MPSOvtPcnt_YTD real
DECLARE @OCCAbsPcnt_Avg real, @OCCAbsPcnt_YTD real, @OCCOvtPcnt_Avg real, @OCCOvtPcnt_YTD real
DECLARE @MaintOCCMPSRatio_Avg real, @MaintOCCMPSRatio_YTD real, @ProcOCCMPSRatio_Avg real, @ProcOCCMPSRatio_YTD real
SELECT @OCCAbsPcnt_Avg = dbo.SLAvgOCCAbsPcnt(@SubListAVG), @MPSAbsPcnt_Avg = dbo.SLAvgMPSAbsPcnt(@SubListAVG)
	, @OCCOvtPcnt_Avg = dbo.SLAvgOvertimePcnt(@SubListAVG, 'TO'), @MPSOvtPcnt_Avg = dbo.SLAvgOvertimePcnt(@SubListAVG, 'TM')
	, @ProcOCCMPSRatio_Avg = dbo.SLAvgOCCMPSRatio(@SubListAVG,'OO','MO'), @MaintOCCMPSRatio_Avg = dbo.SLAvgOCCMPSRatio(@SubListAVG,'OM','MM')
SELECT @OCCAbsPcnt_YTD = dbo.SLAvgOCCAbsPcnt(@SubListYTD), @MPSAbsPcnt_YTD = dbo.SLAvgMPSAbsPcnt(@SubListYTD)
	, @OCCOvtPcnt_YTD = dbo.SLAvgOvertimePcnt(@SubListYTD, 'TO'), @MPSOvtPcnt_YTD = dbo.SLAvgOvertimePcnt(@SubListYTD, 'TM')
	, @ProcOCCMPSRatio_YTD = dbo.SLAvgOCCMPSRatio(@SubListYTD,'OO','MO'), @MaintOCCMPSRatio_YTD = dbo.SLAvgOCCMPSRatio(@SubListYTD,'OM','MM')

-- Energy Consumption per Bbl Input
DECLARE @KBTUPerBbl_Avg real, @MJPerBbl_Avg real, @KBTUPerBbl_YTD real, @MJPerBbl_YTD real
SELECT @KBTUPerBbl_Avg = dbo.SLAvgEnergyConsKBTUPerBbl(@SubListAVG)
	, @KBTUPerBbl_YTD = dbo.SLAvgEnergyConsKBTUPerBbl(@SubListYTD)
SELECT @MJPerBbl_Avg = GlobalDB.dbo.UnitsConv(@KBTUPerBbl_Avg, 'KBTU/B','MJ/B')
	, @MJPerBbl_YTD = GlobalDB.dbo.UnitsConv(@KBTUPerBbl_YTD, 'KBTU/B','MJ/B')

-- Update Gensum with these simple averages that are independent of UOM, Currency and FactorSet
UPDATE Gensum
SET CrudeAPI_Avg = @CrudeAPI_Avg, CrudeAPI_YTD = @CrudeAPI_YTD, CrudeSulfur_Avg = @CrudeSulfur_Avg, CrudeSulfur_YTD = @CrudeSulfur_YTD,
GainPcnt_Avg = @GainPcnt_Avg, GainPcnt_YTD = @GainPcnt_YTD, NetInputBPD_Avg = @NetInputBPD_Avg, NetInputBPD_YTD = @NetInputBPD_YTD,
MPSAbsPcnt_Avg = @MPSAbsPcnt_Avg, MPSAbsPcnt_YTD = @MPSAbsPcnt_YTD, MPSOvtPcnt_Avg = @MPSOvtPcnt_Avg, MPSOvtPcnt_YTD = @MPSOvtPcnt_YTD,
OCCAbsPcnt_Avg = @OCCAbsPcnt_Avg, OCCAbsPcnt_YTD = @OCCAbsPcnt_YTD, OCCOvtPcnt_Avg = @OCCOvtPcnt_Avg, OCCOvtPcnt_YTD = @OCCOvtPcnt_YTD,
MaintOCCMPSRatio_Avg = @MaintOCCMPSRatio_Avg, ProcOCCMPSRatio_Avg = @ProcOCCMPSRatio_Avg, 
MaintOCCMPSRatio_YTD = @MaintOCCMPSRatio_YTD, ProcOCCMPSRatio_YTD = @ProcOCCMPSRatio_YTD,
EnergyConsPerBbl_Avg = CASE UOM WHEN 'US' THEN @KBTUPerBbl_Avg WHEN 'MET' THEN @MJPerBbl_Avg END, 
EnergyConsPerBbl_YTD = CASE UOM WHEN 'US' THEN @KBTUPerBbl_YTD WHEN 'MET' THEN @MJPerBbl_YTD END
WHERE SubmissionID = @SubmissionID
-- Averages for variables that are a function of FactorSet only
DECLARE @FactorSet FactorSet
	UPDATE Gensum 
	SET  EDC_Avg = favg.AvgEDC, UEDC_Avg = favg.AvgUEDC, EII_Avg = favg.EII, VEI_Avg = favg.VEI, UtilPcnt_Avg = favg.UtilPcnt, UtilOSTA_Avg = favg.UtilOSTA, ProcessUtilPcnt_Avg = favg.ProcessUtilPcnt,
	     EDC_YTD = fytd.AvgEDC, UEDC_YTD = fytd.AvgUEDC, EII_YTD = favg.EII, VEI_YTD = favg.VEI, UtilPcnt_YTD = favg.UtilPcnt, UtilOSTA_YTD = favg.UtilOSTA, ProcessUtilPcnt_YTD = favg.ProcessUtilPcnt,
	     OCCWHrEDC_Avg = pavg.OCCWHrEDC, MPSWhrEDC_Avg = pavg.MPSWhrEDC, TotWhrEDC_Avg = pavg.TotWHrEDC,
	     OCCWHrEDC_YTD = pytd.OCCWHrEDC, MPSWhrEDC_YTD = pytd.MPSWhrEDC, TotWhrEDC_YTD = pytd.TotWHrEDC,
	     OCCEqPEDC_Avg = NULL, MPSEqPEDC_Avg = NULL, TotEqPEDC_Avg = NULL,
	     OCCEqPEDC_YTD = NULL, MPSEqPEDC_YTD = NULL, TotEqPEDC_YTD = NULL,
	     TotMaintForceWHrEDC_Avg = pavg.TotMaintForceWHrEDC, TotMaintForceWHrEDC_YTD = pytd.TotMaintForceWHrEDC,
	     PEI_Avg = pavg.PEI, MaintPEI_Avg = pavg.MaintPEI, NonMaintPEI_Avg = pavg.NonMaintPEI, 
	     PEI_YTD = pytd.PEI, MaintPEI_YTD = pytd.MaintPEI, NonMaintPEI_YTD = pytd.NonMaintPEI
	FROM GenSum LEFT JOIN dbo.SLAverageFactors(@SubListAVG, NULL) favg ON favg.FactorSet = GenSum.FactorSet
		LEFT JOIN dbo.SLAverageFactors(@SubListYTD, NULL) fytd ON fytd.FactorSet = GenSum.FactorSet
		LEFT JOIN dbo.SLAveragePersKPIs(@SubListAVG, NULL) pavg ON pavg.FactorSet = GenSum.FactorSet
		LEFT JOIN dbo.SLAveragePersKPIs(@SubListYTD, NULL) pytd ON pytd.FactorSet = GenSum.FactorSet
	WHERE Gensum.SubmissionID = @SubmissionID 
	
-- Average CEI
UPDATE CEI2008
SET CEI_Avg = dbo.SLAverageCEI(@SubListAVG, FactorSet)
, CEI_YTD = dbo.SLAverageCEI(@SubListYTD, FactorSet)
WHERE SubmissionID = @SubmissionID

-- Average Energy Costs
UPDATE Gensum
SET EnergyCost_Avg = CASE UOM WHEN 'US' THEN a.TotCostMBTU WHEN 'MET' THEN a.TotCostGJ END
	, EnergyCost_Prod_Avg = CASE UOM WHEN 'US' THEN a.ProdCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
	, EnergyCost_Pur_Avg = CASE UOM WHEN 'US' THEN a.PurCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
FROM GenSum INNER JOIN dbo.SLAverageEnergyCost(@SubListAVG) a ON a.Currency = Gensum.Currency
WHERE GenSum.SubmissionID = @SubmissionID

UPDATE Gensum
SET EnergyCost_YTD = CASE UOM WHEN 'US' THEN a.TotCostMBTU WHEN 'MET' THEN a.TotCostGJ END
	, EnergyCost_Prod_YTD = CASE UOM WHEN 'US' THEN a.ProdCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
	, EnergyCost_Pur_YTD = CASE UOM WHEN 'US' THEN a.PurCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
FROM GenSum INNER JOIN dbo.SLAverageEnergyCost(@SubListYTD) a ON a.Currency = Gensum.Currency
WHERE GenSum.SubmissionID = @SubmissionID 

-- Average Opex KPIs
UPDATE Gensum
SET TotCashOpexUEDC_Avg = o.TotCashOpexUEDC, VolOpexUEDC_Avg = o.VolOpexUEDC, NonVolOpexUEDC_Avg = o.NonVolOpexUEDC
    , NEOpexUEDC_Avg = o.NEOpexUEDC, NEOpexEDC_Avg = o.NEOpexEDC, NEI_Avg = o.NEI
    , TotCashOpexBbl_Avg = o.TotCashOpexBbl
	, TotCashOpexUEDC_YTD = y.TotCashOpexUEDC, VolOpexUEDC_YTD = y.VolOpexUEDC, NonVolOpexUEDC_YTD = y.NonVolOpexUEDC 
    , NEOpexUEDC_YTD = y.NEOpexUEDC, NEOpexEDC_YTD = y.NEOpexEDC, NEI_YTD = y.NEI
    , TotCashOpexBbl_YTD = y.TotCashOpexBbl
FROM GenSum LEFT JOIN [dbo].[SLAverageOpex](@SubListAVG, NULL, 'CLIENT', NULL) o ON o.FactorSet = Gensum.FactorSet AND o.Currency = Gensum.Currency
	LEFT JOIN [dbo].[SLAverageOpex](@SubListYTD, NULL, 'CLIENT', NULL) y ON y.FactorSet = Gensum.FactorSet AND y.Currency = Gensum.Currency
WHERE Gensum.SubmissionID = @SubmissionID

-- Average Margins
UPDATE Gensum
SET GPV_Avg = a.GPV, RMC_Avg = a.RMC, GrossMargin_Avg = a.GrossMargin, CashMargin_Avg = a.GrossMargin + a.OthRev - GenSum.TotCashOpexBbl_Avg
   ,GPV_YTD = y.GPV, RMC_YTD = y.RMC, GrossMargin_YTD = y.GrossMargin, CashMargin_YTD = y.GrossMargin + y.OthRev - GenSum.TotCashOpexBbl_YTD
FROM GenSum LEFT JOIN dbo.SLAverageGrossMargin(@SubListAVG) a ON Gensum.Scenario = a.Scenario AND Gensum.Currency = a.Currency
	LEFT JOIN dbo.SLAverageGrossMargin(@SubListYTD) y ON Gensum.Scenario = y.Scenario AND Gensum.Currency = y.Currency
WHERE Gensum.SubmissionID = @SubmissionID

UPDATE Gensum
SET ROI_Avg = r.ROI, RV_Avg = r.RV, TotCptl_Avg = r.TotCptl
FROM GenSum LEFT JOIN dbo.SLAverageMarginROI(@SubListAVG, NULL, NULL, NULL) r ON r.FactorSet = Gensum.FactorSet AND r.Currency = Gensum.Currency AND r.Scenario = Gensum.Scenario
WHERE Gensum.SubmissionID = @SubmissionID 

UPDATE Gensum
SET ROI_YTD = r.ROI, RV_YTD = r.RV, TotCptl_YTD = r.TotCptl
FROM GenSum LEFT JOIN dbo.SLAverageMarginROI(@SubListYTD, NULL, NULL, NULL) r ON r.FactorSet = Gensum.FactorSet AND r.Currency = Gensum.Currency AND r.Scenario = Gensum.Scenario
WHERE Gensum.SubmissionID = @SubmissionID 

UPDATE Submissions SET CalcsNeeded = NULL
WHERE SubmissionID = @SubmissionID 
AND CalcsNeeded IN ('A','M')

EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Completed'

END



