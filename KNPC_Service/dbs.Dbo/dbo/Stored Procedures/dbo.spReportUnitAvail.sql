﻿


CREATE     PROC [dbo].[spReportUnitAvail] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SELECT UnitName = ISNULL(c.UnitName, pid.Description), mr.RegDown, mr.MaintDown, mr.OthDown
	, mc.RegUnavail, NonTAMechUnavail = mc.MechUnavail_Act - mc.MechUnavailTA_Act, mc.MechUnavailTA_Ann, mc.MechUnavail_Ann, mc.OthUnavail
	, mc.OpAvail_Ann, mc.MechAvail_Ann, mc.OnStream_Ann
	, mc.OpAvail_Ann_Avg, mc.MechAvail_Ann_Avg, mc.OnStream_Ann_Avg
FROM dbo.MaintCalc mc INNER JOIN ProcessID_LU pid ON pid.ProcessID = mc.ProcessID
INNER JOIN Config c ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID 
INNER JOIN MaintRout mr ON mr.SubmissionID = c.SubmissionID AND mr.UnitID = c.UnitID
WHERE mc.SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @Dataset))
ORDER BY pid.SortKey, mc.UnitID



