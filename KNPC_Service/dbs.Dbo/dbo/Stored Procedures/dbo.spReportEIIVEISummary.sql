﻿

CREATE   PROC [dbo].[spReportEIIVEISummary] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

SELECT s.SubmissionID, s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod,
f.EII, f.EnergyUseDay AS EIIDailyUsage, f.TotStdEnergy AS EIIStdEnergy,
f.VEI, f.EstGain AS VEIStdGain, f.ReportLossGain AS VEIActualGain
INTO #Monthly
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE FactorSet=@FactorSet
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1

SELECT s1.SubmissionID, s1.PeriodStart, NumDays_YTD = SUM(s2.NumDays), EII_YTD = SUM(fc.EnergyUseDay*s2.NumDays)/SUM(fc.TotStdEnergy*s2.NumDays)*100,
EIIUsage_YTD = SUM(fc.EnergyUseDay*s2.NumDays), EIIEstMBTU_YTD = SUM(fc.TotStdEnergy*s2.NumDays), EIIStdEnergy_YTD = SUM(fc.TotStdEnergy*s2.NumDays)/SUM(s2.NumDays),
VEIStdGain_YTD = SUM(fc.EstGain), VEIActualGain_YTD = SUM(fc.ReportLossGain)
INTO #YTD
FROM Submissions s1 INNER JOIN Submissions s2 ON s2.RefineryID = s1.RefineryID AND s2.DataSet = s1.DataSet AND s2.PeriodYear = s1.PeriodYear AND s2.PeriodMonth <= s1.PeriodMonth
INNER JOIN FactorTotCalc fc ON fc.submissionId = s2.submissionid 
WHERE fc.factorset=@FactorSet 
And s1.RefineryID = @RefineryID AND s1.DataSet=@DataSet AND s1.PeriodYear = ISNULL(@PeriodYear, s1.PeriodYear) AND s1.PeriodMonth = ISNULL(@PeriodMonth, s1.PeriodMonth) AND s1.UseSubmission = 1 AND s2.UseSubmission = 1
GROUP BY s1.SubmissionID, s1.PeriodStart

SET NOCOUNT OFF

SELECT m.Location, m.PeriodStart, m.PeriodEnd, m.DaysInPeriod, Currency= @Currency, UOM = @UOM, 
m.EII, m.EIIDailyUsage, m.EIIStdEnergy, m.VEI, m.VEIStdGain, m.VEIActualGain, 
y.EII_YTD, y.EIIUsage_YTD, y.EIIEstMBTU_YTD, y.EIIStdEnergy_YTD, 
VEI_YTD = CASE WHEN y.VEIStdGain_YTD <> 0 THEN y.VEIActualGain_YTD/y.VEIStdGain_YTD*100 END, y.VEIStdGain_YTD, y.VEIActualGain_YTD
FROM #Monthly m INNER JOIN #YTD y ON y.SubmissionID = m.SubmissionID
ORDER BY m.PeriodStart DESC

DROP TABLE #Monthly
DROP TABLE #YTD




