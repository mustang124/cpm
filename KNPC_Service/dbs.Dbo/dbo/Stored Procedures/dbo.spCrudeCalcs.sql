﻿CREATE        PROC [dbo].[spCrudeCalcs](@SubmissionID int)
AS
SET NOCOUNT ON
UPDATE Crude
SET Density = (141.5/(Gravity+131.5))*1000,
MT = Bbl*((141.5/(Gravity+131.5))*1000)/6289
WHERE SubmissionID = @SubmissionID
DELETE FROM CrudeTot WHERE SubmissionID = @SubmissionID
INSERT INTO CrudeTot (SubmissionID, TotBbl, TotMT, AvgCost, AvgDensity, AvgGravity, AvgSulfur)
SELECT @SubmissionID, TotBbl = SUM(Bbl), TotMT = SUM(MT), 
AvgCost = CASE WHEN SUM(Bbl) > 0 THEN SUM(Bbl*CostPerBbl)/SUM(Bbl) END,
AvgDensity = CASE WHEN SUM(MT) > 0 THEN SUM(MT*Density)/SUM(CASE WHEN Density IS NOT NULL AND MT <> 0 THEN MT END) END,
AvgGravity = CASE WHEN SUM(MT)>0 THEN 141.5/(SUM(MT)/SUM(Bbl)*6.289)-131.5 END, 
AvgSulfur = CASE WHEN SUM(CASE WHEN Sulfur IS NOT NULL AND MT <> 0 THEN MT END) > 0 THEN SUM(MT*Sulfur)/SUM(CASE WHEN Sulfur IS NOT NULL AND MT <> 0 THEN MT END) END
FROM Crude
WHERE @SubmissionID = SubmissionID

DECLARE @RefineryType varchar(5)
SELECT @RefineryType = dbo.GetRefineryType(SubmissionsAll.RefineryID)
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
IF @RefineryType <> 'LUBES'
BEGIN
	IF EXISTS (SELECT * FROM Yield WHERE MaterialId = 'CRD' AND SubmissionID = @SubmissionID)
		UPDATE Yield
		SET Bbl = ISNULL(TotBbl, 0), PriceUS = ISNULL(AvgCost,0),
		PriceLocal = ISNULL(AvgCost*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM SubmissionsAll WHERE SubmissionID = @SubmissionID), 0)
		FROM Yield INNER JOIN CrudeTot ON CrudeTot.SubmissionID = Yield.SubmissionID
		WHERE Yield.SubmissionID = @SubmissionID AND MaterialID = 'CRD'
	ELSE
		INSERT INTO Yield (SubmissionID, SortKey, Category, MaterialID, MaterialName, Bbl, PriceUS, PriceLocal)
		SELECT SubmissionID, ISNULL((SELECT MIN(SortKey) FROM Yield s WHERE s.SubmissionID = @SubmissionID) - 1, 101), 'RMI', 'CRD', 'Crude Oil & Condensate', ISNULL(TotBbl, 0), ISNULL(AvgCost, 0), ISNULL(AvgCost*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM SubmissionsAll WHERE SubmissionID = @SubmissionID), 0)
		FROM CrudeTot WHERE SubmissionID = @SubmissionID
END
EXEC spCrudeCost @SubmissionID


