﻿

CREATE  PROC [dbo].[spPrepareForUpload](@SubmissionID int)
AS
DECLARE @RefineryID varchar(6), @DataSet varchar(15)
SELECT @RefineryID = RefineryID, @DataSet = DataSet FROM SubmissionsAll WHERE SubmissionId = @SubmissionID

DELETE FROM dbo.Config WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.ConfigRS WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Inventory WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.ProcessData WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.OpEx WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.OpexAdd WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.OpExAll WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Pers WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Absence WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintRout WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Crude WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Yield WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Energy WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Electric WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.RefTargets WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.UnitTargets WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.UnitTargetsNew WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.LoadEDCStabilizers WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.LoadRoutHist WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.LoadTA WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.UserDefined WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.CustomUnitData WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Comments WHERE SubmissionID = @SubmissionID

DELETE FROM dbo.GenSum WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.UnitFactorData WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.ProcessSum WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.ProcessTotCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.PersST WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.PersTot WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintPersCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.AbsenceTot WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.CrudeTot WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.CrudeCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.CrudeTotPrice WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialNet WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialNetCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialST WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialSTCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialSumCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialTot WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialCostCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaterialTotCost WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MarginCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.ROICalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.DataChecks WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintIndex WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintAvailCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintProcess WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintCost WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintTotCost WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.EnergyCons WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.EnergyCost WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.EnergyCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.EnergyTot WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.EnergyTotCost WHERE SubmissionID = @SubmissionID
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'CEI' AND type = 'u')
	DELETE FROM dbo.CEI WHERE SubmissionID = @SubmissionID
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'CEI2008' AND type = 'u')
	DELETE FROM dbo.CEI2008 WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.InventoryTot WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.FactorCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.FactorProcessCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.FactorTotCalc WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.Divisors WHERE SubmissionID = @SubmissionID
DELETE FROM dbo.MaintDivisors WHERE SubmissionID = @SubmissionID





