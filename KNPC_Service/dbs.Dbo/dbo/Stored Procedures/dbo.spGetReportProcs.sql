﻿CREATE PROCEDURE [dbo].[spGetReportProcs](@RefineryID varchar(6), @ReportName varchar(50))
AS
IF EXISTS (SELECT * FROM dbo.Report_LU WHERE ReportName = @ReportName AND CustomGroup IN (0,255))
	SELECT     ReportProcs.DataTableName, ReportProcs.ProcName, Report_LU.Template
	FROM         Report_LU INNER JOIN
	                      ReportProcs ON Report_LU.CustomGroup = ReportProcs.CustomGroup AND Report_LU.ReportCode = ReportProcs.ReportCode
	WHERE     (Report_LU.CustomGroup IN (0,255)) AND (Report_LU.ReportName = @ReportName)
ELSE BEGIN
	SELECT     ReportProcs.DataTableName, ReportProcs.ProcName, Report_LU.Template
	FROM         Report_LU INNER JOIN
	                      ReportProcs ON Report_LU.CustomGroup = ReportProcs.CustomGroup AND Report_LU.ReportCode = ReportProcs.ReportCode
	WHERE     (Report_LU.ReportName = @ReportName)
		AND (Report_LU.CustomGroup IN (SELECT CustomGroup
			FROM CoCustom cc INNER JOIN TSort t ON t.CompanyID = cc.CompanyID
			WHERE cc.CustomType = 'R' AND t.RefineryID = @RefineryID))
END

