﻿CREATE PROCEDURE [dbo].[DS_UnitTargetsNew]
		@RefineryID nvarchar(10),
		@Dataset nvarchar(20)='ACTUAL'
AS
BEGIN
		SELECT ut.UnitID,c.UnitName,c.ProcessID,ut.Property,ut.Target,ut.CurrencyCode, 
                   ul.USDescription, ul.MetDescription, ul.USDecPlaces, ul.MetDecPlaces,ul.SortKey 
		FROM UnitTargetsNew ut,UnitTargets_LU ul, Config c  
		WHERE ut.SubmissionID = c.SubmissionID AND ut.UnitID = c.UnitID AND ul.Property = ut.Property
		AND (ul.ProcessID='ALL' OR ul.ProcessID=c.ProcessID)  
		AND c.SubmissionID=(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @Dataset ORDER BY PeriodStart DESC) 
        ORDER BY ul.SortKey
END


