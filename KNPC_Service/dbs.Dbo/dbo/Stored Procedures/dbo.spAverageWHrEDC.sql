﻿CREATE     PROC [dbo].[spAverageWHrEDC](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @OCCWHrEDC real OUTPUT, @MPSWHrEDC real OUTPUT, @TotWHrEDC real OUTPUT, @TotMaintForceWHrEDC real OUTPUT, 
	@PEI real OUTPUT, @MaintPEI real OUTPUT, @NonMaintPEI real OUTPUT)
AS
SELECT p.SectionID, TotWHrEDC = SUM(p.TotWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor), 
	PEI = SUM(p.TotWHrEffIndex*p.EffDivisor)/SUM(CASE WHEN p.EffDivisor = 0 THEN NULL ELSE p.EffDivisor END)
INTO #calc
FROM PersSTCalc p 
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND p.SectionID IN ('TO','TM','TP')
GROUP BY p.SectionID
HAVING SUM(p.WHrEDCDivisor) > 0

SELECT 	@OCCWHrEDC = AVG(CASE WHEN SectionID = 'TO' THEN TotWHrEDC END),
	@MPSWHrEDC = AVG(CASE WHEN SectionID = 'TM' THEN TotWHrEDC END),
	@TotWHrEDC = AVG(CASE WHEN SectionID = 'TP' THEN TotWHrEDC END),
	@PEI = AVG(CASE WHEN SectionID = 'TP' THEN PEI END)
FROM #calc

SELECT @TotMaintForceWHrEDC = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor),
@MaintPEI = SUM(CASE WHEN p.MaintPEIDivisor > 0 THEN p.MaintPEI*p.MaintPEIDivisor END)/SUM(CASE WHEN p.MaintPEIDivisor > 0 THEN p.MaintPEIDivisor END),
@NonMaintPEI = SUM(CASE WHEN p.NonMaintPEIDivisor > 0 THEN p.NonMaintPEI*p.NonMaintPEIDivisor END)/SUM(CASE WHEN p.NonMaintPEIDivisor > 0 THEN p.NonMaintPEIDivisor END)
FROM PersTotCalc p
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd AND p.Currency = 'USD'


