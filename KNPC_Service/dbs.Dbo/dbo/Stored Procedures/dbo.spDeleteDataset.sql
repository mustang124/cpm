﻿CREATE PROCEDURE [dbo].[spDeleteDataset](@RefineryID varchar(6), @Dataset varchar(15))
AS

DECLARE @SubmissionID int
WHILE EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @Dataset)
BEGIN
	SELECT TOP 1 @SubmissionID = SubmissionID FROM SubmissionsAll WHERE RefineryID = @RefineryID AND DataSet = @Dataset ORDER BY PeriodStart DESC
	EXEC spDeleteSubmission @SubmissionID
END

/*DELETE FROM LoadTA WHERE RefineryID = @RefineryID AND DataSet = @Dataset -- now keyed with SubmissionID */
DELETE FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @Dataset
DELETE FROM MaintTACost WHERE RefineryID = @RefineryID AND DataSet = @Dataset
DELETE FROM ReadyForCalcs WHERE RefineryID = @RefineryID AND DataSet = @Dataset
DELETE FROM MaintRoutHist WHERE RefineryID = @RefineryID AND DataSet = @Dataset
--DELETE FROM LoadRoutHist WHERE RefineryID = @RefineryID AND DataSet = @Dataset
DELETE FROM LoadedEDCStabilizers WHERE RefineryID = @RefineryID AND DataSet = @Dataset
--DELETE FROM LoadEDCStabilizers WHERE RefineryID = @RefineryID AND DataSet = @Dataset
DELETE FROM StudyTankage WHERE RefineryID = @RefineryID AND DataSet = @Dataset



