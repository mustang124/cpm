﻿CREATE PROC [dbo].[spReportMonroeKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @StartYTD smalldatetime, @Start12Mo smalldatetime, @Start24Mo smalldatetime
SELECT @StartYTD = p.StartYTD, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd)

SELECT 
	EII = m.EII, EII_YTD = avgYTD.EII, EII_AVG = avg12mo.EII, 
	EnergyUseDay = m.EnergyUseDay, EnergyUseDay_YTD = avgYTD.EnergyUseDay, EnergyUseDay_AVG = avg12mo.EnergyUseDay, 
	TotStdEnergy = m.TotStdEnergy, TotStdEnergy_YTD = avgYTD.TotStdEnergy, TotStdEnergy_AVG = avg12mo.TotStdEnergy, 

	EDC = m.EDC/1000, EDC_YTD = avgYTD.EDC/1000, EDC_AVG = avg12mo.EDC/1000, 

	UtilOSTA = m.UtilOSTA, UtilOSTA_YTD = avgYTD.UtilOSTA, UtilOSTA_AVG = avg12mo.UtilOSTA, 
	ProcessEDC = m.TotProcessEDC/1000, ProcessEDC_YTD = avgYTD.TotProcessEDC/1000, ProcessEDC_AVG = avg12mo.TotProcessEDC/1000, 
	ProcessUEDC = m.TotProcessUEDC/1000, ProcessUEDC_YTD = avgYTD.TotProcessUEDC/1000, ProcessUEDC_AVG = avg12mo.TotProcessUEDC/1000, 
	MechUnavailTA_Act = m.MechUnavailTA_Act, MechUnavailTA_Act_YTD = avgYTD.MechUnavailTA_Act, MechUnavailTA_Act_AVG = avg12mo.MechUnavailTA_Act,
	
	VEI = m.VEI, VEI_YTD = avgYTD.VEI, VEI_AVG = avg12mo.VEI, 
	NetInputBPD = m.NetInputBPD, NetInputBPD_YTD = avgYTD.NetInputBPD, NetInputBPD_AVG = avg12mo.NetInputBPD,
	ReportLossGain = m.ReportLossGain, ReportLossGain_YTD = avgYTD.ReportLossGain, ReportLossGain_AVG = avg12mo.ReportLossGain, 
	EstGain = m.EstGain, EstGain_YTD = avgYTD.EstGain, EstGain_AVG = avg12mo.EstGain, 

	OpAvail = m.OpAvail, OpAvail_YTD = avgYTD.OpAvail, OpAvail_AVG = avg24mo.OpAvail, 
	MechUnavailTA = m.MechUnavailTA, MechUnavailTA_YTD = avgYTD.MechUnavailTA, MechUnavailTA_AVG = avg24mo.MechUnavailTA, 
	NonTAUnavail = m.MechUnavailRout + m.RegUnavail, NonTAUnavail_YTD = avgYTD.MechUnavailRout + avgYTD.RegUnavail, NonTAUnavail_AVG = avg24mo.MechUnavailRout + avg24mo.RegUnavail, 

	TotWHrEDC = m.PersIndex, TotWHrEDC_YTD = avgYTD.PersIndex, TotWHrEDC_AVG = avg12mo.PersIndex, 
	AnnTAWhr = m.MaintTAWHr_k, AnnTAWhr_YTD = avgYTD.MaintTAWHr_k, AnnTAWhr_AVG = avg12mo.MaintTAWHr_k,
	NonTAWHr = m.TotNonTAWHr_k, NonTAWHr_YTD = avgYTD.TotNonTAWHr_k, NonTAWHr_AVG = avg12mo.TotNonTAWHr_k, 

	MaintIndex = m.MaintIndex, MaintIndex_YTD = avgYTD.MaintIndex, MaintIndex_AVG = avg24mo.MaintIndex, 
	TAAdj = m.AnnTACost, TAAdj_YTD = avgYTD.AnnTACost, TAAdj_AVG = mi24.TAEffIndex*avg24mo.MaintEffDiv/100/1000,
	RoutCost = m.RoutCost, RoutCost_YTD = avgYTD.RoutCost, RoutCost_AVG = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1000, 

	NEOpexEDC = m.NEOpexEDC, NEOpexEDC_YTD = avgYTD.NEOpexEDC, NEOpexEDC_AVG = avg12mo.NEOpexEDC, 
	NEOpex = m.NEOpex, NEOpex_YTD = avgYTD.NEOpex, NEOpex_AVG = avg12mo.NEOpex, 

	TotCashOpexUEDC = m.OpexUEDC, TotCashOpexUEDC_YTD = avgYTD.OpexUEDC, TotCashOpexUEDC_AVG = avg12mo.OpexUEDC, 
	EnergyCost = m.EnergyCost, EnergyCost_YTD = avgYTD.EnergyCost, EnergyCost_AVG = avg12mo.EnergyCost, 
	TotCashOpex = m.TotCashOpex, TotCashOpex_YTD = avgYTD.TotCashOpex, TotCashOpex_AVG = avg12mo.TotCashOpex, 
	UEDC = m.UEDC/1000, UEDC_YTD = avgYTD.UEDC/1000, UEDC_AVG = avg12mo.UEDC/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM) avgYTD ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1

