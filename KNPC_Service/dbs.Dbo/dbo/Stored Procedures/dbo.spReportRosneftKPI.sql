﻿CREATE   PROC [dbo].[spReportRosneftKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'RUB', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
BEGIN
	--RAISERROR (N'Data has not been uploaded for this month.', -- Message text.
 --          10, -- Severity,
 --          1 --State
 --          );
	RETURN 1
END
ELSE IF @CalcsNeeded IS NOT NULL
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubList3Mo dbo.SubmissionIDList, @SubListQTR dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd)
INSERT @SubList3Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start3Mo, @PeriodEnd)
INSERT @SubListQTR SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartQTR, @EndQuarter)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd)


SELECT UtilPcnt = m.RefUtilPcnt, UtilPcnt_QTR = avgQtr.RefUtilPcnt, UtilPcnt_3MO = avg3mo.RefUtilPcnt, UtilPcnt_AVG = avg12mo.RefUtilPcnt, 
	EDC = m.EDC/1000.0, EDC_QTR = avgQtr.EDC/1000.0, EDC_3MO = avg3mo.EDC/1000.0, EDC_AVG = avg12mo.EDC/1000.0, 
	UtilUEDC = m.UtilUEDC/1000.0, UtilUEDC_QTR = avgQtr.UtilUEDC/1000.0, UtilUEDC_3MO = avg3mo.UtilUEDC/1000.0, UtilUEDC_AVG = avg12mo.UtilUEDC/1000.0, 
	UEDC = m.UEDC/1000.0, UEDC_QTR = avgQtr.UEDC/1000.0, UEDC_3MO = avg3mo.UEDC/1000.0, UEDC_AVG = avg12mo.UEDC/1000.0, 
	ProcessUtilPcnt = m.ProcessUtilPcnt, ProcessUtilPcnt_QTR = avgQtr.ProcessUtilPcnt, ProcessUtilPcnt_3MO = avg3mo.ProcessUtilPcnt, ProcessUtilPcnt_AVG = avg12mo.ProcessUtilPcnt, 
	TotProcessEDC = m.TotProcessEDC/1000.0, TotProcessEDC_QTR = avgQtr.TotProcessEDC/1000.0, TotProcessEDC_3MO = avg3mo.TotProcessEDC/1000.0, TotProcessEDC_AVG = avg12mo.TotProcessEDC/1000.0, 
	TotProcessUEDC = m.TotProcessUEDC/1000.0, TotProcessUEDC_QTR = avgQtr.TotProcessUEDC/1000.0, TotProcessUEDC_3MO = avg3mo.TotProcessUEDC/1000.0, TotProcessUEDC_AVG = avg12mo.TotProcessUEDC/1000.0, 
	OpAvail = m.OpAvail, OpAvail_QTR = avgQtr.OpAvail, OpAvail_3MO = avg3mo.OpAvail, OpAvail_AVG = avg24mo.OpAvail, 
	MechUnavailTA = m.MechUnavailTA, MechUnavailTA_QTR = avgQtr.MechUnavailTA, MechUnavailTA_3MO = avg3mo.MechUnavailTA, MechUnavailTA_AVG = avg24mo.MechUnavailTA, 
	NonTAUnavail = m.MechUnavailRout + m.RegUnavail, NonTAUnavail_QTR = avgQtr.MechUnavailRout + avgQtr.RegUnavail, NonTAUnavail_3MO = avg3mo.MechUnavailRout + avg3mo.RegUnavail, NonTAUnavail_AVG = avg24mo.MechUnavailRout + avg24mo.RegUnavail, 
	EII = m.EII, EII_QTR = avgQtr.EII, EII_3MO = avg3mo.EII, EII_AVG = avg12mo.EII, 
	EnergyUseDay = m.EnergyUseDay/1000, EnergyUseDay_QTR = avgQtr.EnergyUseDay/1000, EnergyUseDay_3MO = avg3mo.EnergyUseDay/1000, EnergyUseDay_AVG = avg12mo.EnergyUseDay/1000, 
	TotStdEnergy = m.TotStdEnergy/1000, TotStdEnergy_QTR = avgQtr.TotStdEnergy/1000, TotStdEnergy_3MO = avg3mo.TotStdEnergy/1000, TotStdEnergy_AVG = avg12mo.TotStdEnergy/1000, 
	ProcessEffIndex = m.ProcessEffIndex, ProcessEffIndex_QTR = avgQtr.ProcessEffIndex, ProcessEffIndex_3MO = avg3mo.ProcessEffIndex, ProcessEffIndex_AVG = avg12mo.ProcessEffIndex, 
	VEI = m.VEI, VEI_QTR = avgQtr.VEI, VEI_3MO = avg3mo.VEI, VEI_AVG = avg12mo.VEI, 
	ReportLossGain = m.ReportLossGain, ReportLossGain_QTR = avgQtr.ReportLossGain, ReportLossGain_3MO = avg3mo.ReportLossGain, ReportLossGain_AVG = avg12mo.ReportLossGain, 
	EstGain = m.EstGain, EstGain_QTR = avgQtr.EstGain, EstGain_3MO = avg3mo.EstGain, EstGain_AVG = avg12mo.EstGain, 
	NetInputBPD = m.NetInputBPD, NetInputBPD_QTR = avgQtr.NetInputBPD, NetInputBPD_3MO = avg3mo.NetInputBPD, NetInputBPD_AVG = avg12mo.NetInputBPD, 
	Gain = -m.GainPcnt, Gain_QTR = -avgQtr.GainPcnt, Gain_3MO = -avg3mo.GainPcnt, Gain_AVG = -avg12mo.GainPcnt, 
	RawMatl = m.RawMatlKBpD, RawMatl_QTR = avgQtr.RawMatlKBpD, RawMatl_3MO = avg3mo.RawMatlKBpD, RawMatl_AVG = avg12mo.RawMatlKBpD, 
	ProdYield = m.ProdYieldKBpD, ProdYield_QTR = avgQtr.ProdYieldKBpD, ProdYield_3MO = avg3mo.ProdYieldKBpD, ProdYield_AVG = avg12mo.ProdYieldKBpD, 
	PersIndex = m.PersIndex, PersIndex_QTR = avgQtr.PersIndex, PersIndex_3MO = avg3mo.PersIndex, PersIndex_AVG = avg12mo.PersIndex, 
	NonMaintWHr = m.NonMaintWHr_k, NonMaintWHr_QTR = avgQtr.NonMaintWHr_k, NonMaintWHr_3MO = avg3mo.NonMaintWHr_k, NonMaintWHr_AVG = avg12mo.NonMaintWHr_k, 
	TotMaintForceWHr = m.TotMaintForceWHr_k, TotMaintForceWHr_QTR = avgQtr.TotMaintForceWHr_k, TotMaintForceWHr_3MO = avg3mo.TotMaintForceWHr_k, TotMaintForceWHr_AVG = avg12mo.TotMaintForceWHr_k, 
	TotMaintForceWHrEDC = m.TotMaintForceWHrEDC, TotMaintForceWHrEDC_QTR = avgQtr.TotMaintForceWHrEDC, TotMaintForceWHrEDC_3MO = avg3mo.TotMaintForceWHrEDC, TotMaintForceWHrEDC_AVG = avg12mo.TotMaintForceWHrEDC, 
	MaintTAWhr = m.MaintTAWHr_k, MaintTAWhr_QTR = avgQtr.MaintTAWHr_k, MaintTAWhr_3MO = avg3mo.MaintTAWHr_k, MaintTAWhr_AVG = avg12mo.MaintTAWHr_k, 
	MaintNonTAWHr = m.MaintNonTAWHr_k, MaintNonTAWHr_QTR = avgQtr.MaintNonTAWHr_k, MaintNonTAWHr_3MO = avg3mo.MaintNonTAWHr_k, MaintNonTAWHr_AVG = avg12mo.MaintNonTAWHr_k, 
	MaintIndex = m.MaintIndex, MaintIndex_QTR = avgQtr.MaintIndex, MaintIndex_3MO = avg3mo.MaintIndex, MaintIndex_AVG = mi24.MaintIndex, 
	AnnTACost = m.AnnTACost/1000, AnnTACost_QTR = avgQtr.AnnTACost/1000, AnnTACost_3MO = avg3mo.AnnTACost/1000, AnnTACost_AVG = mi24.TAEffIndex*avg24mo.MaintEffDiv/100/1e6, 
	RoutCost = m.RoutCost/1000, RoutCost_QTR = avgQtr.RoutCost/1000, RoutCost_3MO = avg3mo.RoutCost/1000, RoutCost_AVG = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1e6, 
	NEOpexEDC = m.NEOpexEDC, NEOpexEDC_QTR = avgQtr.NEOpexEDC, NEOpexEDC_3MO = avg3mo.NEOpexEDC, NEOpexEDC_AVG = avg12mo.NEOpexEDC, 
	NEOpex = m.NEOpex/1000, NEOpex_QTR = avgQtr.NEOpex/1000, NEOpex_3MO = avg3mo.NEOpex/1000, NEOpex_AVG = avg12mo.NEOpex/1000, 
	TotCashOpexUEDC = m.OpexUEDC, TotCashOpexUEDC_QTR = avgQtr.OpexUEDC, TotCashOpexUEDC_3MO = avg3mo.OpexUEDC, TotCashOpexUEDC_AVG = avg12mo.OpexUEDC, 
	OpexEDC = m.OpexEDC, OpexEDC_QTR = avgQtr.OpexEDC, OpexEDC_3MO = avg3mo.OpexEDC, OpexEDC_AVG = avg12mo.OpexEDC, 
	EnergyCost = m.EnergyCost/1000, EnergyCost_QTR = avgQtr.EnergyCost/1000, EnergyCost_3MO = avg3mo.EnergyCost/1000, EnergyCost_AVG = avg12mo.EnergyCost/1000, 
	TotCashOpex = m.TotCashOpex/1000, TotCashOpex_QTR = avgQtr.TotCashOpex/1000, TotCashOpex_3MO = avg3mo.TotCashOpex/1000, TotCashOpex_AVG = avg12mo.TotCashOpex/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList3Mo, @FactorSet, @Scenario, @Currency, @UOM) avg3mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubListQTR, @FactorSet, @Scenario, @Currency, @UOM) avgQtr ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1

