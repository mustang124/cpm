﻿CREATE  PROC [dbo].[spAverageEqPEDC](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @OCCEqPEDC real OUTPUT, @MPSEqPEDC real OUTPUT, @TotEqPEDC real OUTPUT)
AS
SELECT p.SectionID, TotEqPEDC = SUM(p.TotEqPEDC*p.EqPEDCDivisor)/SUM(p.EqPEDCDivisor)
INTO #calc
FROM PersSTCalc p 
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND p.SectionID IN ('TO','TM','TP')
GROUP BY p.FactorSet, p.SectionID
HAVING SUM(p.EqPEDCDivisor) > 0

SELECT 	@OCCEqPEDC = AVG(CASE WHEN SectionID = 'TO' THEN TotEqPEDC END),
	@MPSEqPEDC = AVG(CASE WHEN SectionID = 'TM' THEN TotEqPEDC END),
	@TotEqPEDC = AVG(CASE WHEN SectionID = 'TP' THEN TotEqPEDC END)
FROM #calc


