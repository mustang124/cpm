﻿CREATE PROC [dbo].[StartUpload](@RefineryID varchar(6), @DataSet varchar(15) = 'Actual', @OK bit OUTPUT)
AS
	SELECT @OK = 0
	IF NOT EXISTS (SELECT * FROM ReadyForCalcs WHERE RefineryID = @RefineryID AND DataSet = @DataSet )
	BEGIN
		INSERT INTO ReadyForCalcs (RefineryID, DataSet, CalcsStarted, Uploading, LastUpdate)
		SELECT @RefineryID, @DataSet, 0, 1, GetDate()

		SELECT @OK = 1
	END
	ELSE
	BEGIN
		UPDATE ReadyForCalcs
		SET Uploading = 1, LastUpdate = GetDate()
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet 
		AND (CalcsStarted = 0 AND Uploading = 0)
		IF @@ROWCOUNT > 0
			SELECT @OK = 1
	END

