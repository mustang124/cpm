﻿CREATE PROC [dbo].[spAverageYield](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
		@NetInputkBPD real OUTPUT, @GainPcnt real OUTPUT)
AS
SELECT @GainPcnt = 100*CASE WHEN SUM(NetInputBbl) > 0 THEN SUM(GainBbl)/SUM(NetInputBbl) ELSE NULL END,
@NetInputkBPD = CASE WHEN SUM(NumDays) > 0 THEN SUM(NetInputBbl/1000)/SUM(NumDays) ELSE NULL END
FROM MaterialTot a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd


