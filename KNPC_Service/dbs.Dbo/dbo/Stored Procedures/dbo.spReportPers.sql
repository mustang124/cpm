﻿CREATE          PROC [dbo].[spReportPers] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod
	, p.SectionID, p.PersID, pLU.SortKey, pLU.[Description]
	, p.CompWhrEDC, p.ContWHrEDC, p.GAWHrEDC, p.TotWHrEDC, p.OVTPcnt
	, p.STH, p.OVTHours, p.GA, -1*P.AbsHrs as AbsHrs, p.CompWHr, p.Contract, p.TotWhr
FROM dbo.Submissions s INNER JOIN dbo.PersCalc p ON p.SubmissionID = s.SubmissionID
INNER JOIN dbo.Pers_LU pLU ON pLU.PersID = p.PersID
WHERE	s.RefineryID = @RefineryID
	AND s.PeriodYear = @PeriodYear
	AND s.PeriodMonth = @PeriodMonth
	AND s.DataSet = @DataSet
	AND s.UseSubmission = 1
	AND p.FactorSet = @FactorSet
UNION

SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod
	, p.SectionID, PersID = p.SectionID, SortKey = 9999, p.[Description]
	, p.CompWhrEDC, p.ContWHrEDC, p.GAWHrEDC, p.TotWHrEDC, p.OVTPcnt
	, p.STH, p.OVTHours, p.GA, -1*P.AbsHrs as AbsHrs, p.CompWHr, p.Contract, p.TotWhr
FROM dbo.Submissions s INNER JOIN dbo.PersSTCalc p ON p.SubmissionID = s.SubmissionID
WHERE 	s.RefineryID = @RefineryID
	AND s.PeriodYear = @PeriodYear
	AND s.PeriodMonth = @PeriodMonth
	AND s.UseSubmission = 1
	AND p.FactorSet = @FactorSet

ORDER BY pLU.SortKey



