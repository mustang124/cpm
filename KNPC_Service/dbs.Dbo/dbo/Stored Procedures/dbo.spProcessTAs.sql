﻿CREATE PROC [dbo].[spProcessTAs](@SubmissionID int)
AS

SET NOCOUNT ON

DECLARE @RefineryID varchar(6), @DataSet varchar(15), @RptCurrency CurrencyCode, @PeriodEnd smalldatetime
SELECT @RefineryID = RefineryID, @DataSet = DataSet, @RptCurrency = RptCurrency, @PeriodEnd = PeriodEnd
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID

IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodEnd > @PeriodEnd AND Submitted > PeriodEnd AND ClientVersion LIKE '%Lit%' AND UseSubmission = 1)
	RETURN 1
ELSE BEGIN

UPDATE LoadTA
SET TACurrency = @RptCurrency
WHERE SubmissionID = @SubmissionID AND TACurrency IS NULL


DECLARE @LoadTA TABLE (
	[RefineryID] [char](6) NOT NULL,
	[DataSet] [varchar](15) NOT NULL,
	[UnitID] [int] NOT NULL,
	[TAID] [int] NOT NULL,
	[SchedTAInt] [real] NULL,
	[TADate] [smalldatetime] NULL,
	[TAHrsDown] [real] NULL,
	[TACostLocal] [real] NULL,
	
	[TALaborCostLocal] [real] NULL,
	[TAExpLocal] [real] NULL,
	[TACptlLocal] [real] NULL,
	[TAOvhdLocal] [real] NULL,
	[TAOCCSTH] [real] NULL,
	[TAOCCOVT] [real] NULL,
	[TAMPSSTH] [real] NULL,
	[TAMPSOVTPcnt] [real] NULL,
	[TAContOCC] [real] NULL,
	[TAContMPS] [real] NULL,
	[PrevTADate] [smalldatetime] NULL,
	[TAExceptions] [smallint] NULL,
	[TACurrency] varchar(4) NULL,
	[ProcessID] varchar(8) NULL
)

DECLARE @MaintTA TABLE (
	[RefineryID] [char](6) NOT NULL,
	[DataSet] [varchar](15) NOT NULL,
	[UnitID] [int] NOT NULL,
	[TAID] [int] NOT NULL,
	[SchedTAInt] [real] NULL,
	[TADate] [smalldatetime] NULL,
	[TAHrsDown] [real] NULL,
	[TACostLocal] [real] NULL,
	
	[TALaborCostLocal] [real] NULL,
	[TAExpLocal] [real] NULL,
	[TACptlLocal] [real] NULL,
	[TAOvhdLocal] [real] NULL,
	[TAOCCSTH] [real] NULL,
	[TAOCCOVT] [real] NULL,
	[TAMPSSTH] [real] NULL,
	[TAMPSOVTPcnt] [real] NULL,
	[TAContOCC] [real] NULL,
	[TAContMPS] [real] NULL,
	[PrevTADate] [smalldatetime] NULL,
	[TAExceptions] [smallint] NULL,
	[TACurrency] varchar(4) NULL,
	[ProcessID] varchar(8) NULL,
	SaveAction char(1) NULL,
	LoadTAID int NULL,
	OrderedTAID int NULL
)

INSERT @LoadTA ([RefineryID], [DataSet], [UnitID], [TAID], [SchedTAInt], [TADate], [TAHrsDown], [TACostLocal], 
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID])
SELECT @RefineryID, @DataSet, [UnitID], [TAID], [SchedTAInt], [TADate], [TAHrsDown], [TACostLocal], 
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID]
FROM LoadTA WHERE SubmissionID = @SubmissionID

DELETE FROM @LoadTA WHERE TADate <= PrevTADate
DELETE FROM ta FROM @LoadTA ta WHERE EXISTS (SELECT * FROM @LoadTA d WHERE d.RefineryID = ta.RefineryID AND d.DataSet = ta.DataSet AND d.UnitID = ta.UnitID AND d.TAID > ta.TAID AND d.TADate = ta.TADate)

INSERT @MaintTA ([RefineryID], [DataSet], [UnitID], [TAID], [SchedTAInt], [TADate], [TAHrsDown], [TACostLocal], 
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID])
SELECT [RefineryID], [DataSet], [UnitID], [TAID], [SchedTAInt], [TADate], [TAHrsDown], [TACostLocal], 
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID]
FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet

UPDATE mta
SET LoadTAID = (SELECT MIN(TAID) FROM @LoadTA lta WHERE lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TADate IS NULL)
FROM @MaintTA mta WHERE mta.TADate IS NULL

UPDATE mta
SET LoadTAID = (SELECT MAX(TAID) FROM @LoadTA lta WHERE lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND (ABS(DATEDIFF(dd, lta.TADate, mta.TADate)) < 90 OR ABS(DATEDIFF(dd, lta.PrevTADate, mta.PrevTADate)) < 10))
FROM @MaintTA mta WHERE LoadTAID IS NULL AND mta.TADate IS NOT NULL

UPDATE mta SET [SchedTAInt] = NULL, SaveAction = 'S'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE mta.SchedTAInt IS NOT NULL AND lta.SchedTAInt IS NULL

UPDATE mta SET [SchedTAInt] = lta.SchedTAInt, SaveAction = 'S'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.SchedTAInt IS NULL AND lta.SchedTAInt IS NOT NULL) OR ABS(mta.SchedTAInt - lta.SchedTAInt)>0.01

UPDATE mta SET 	[ProcessID] = lta.ProcessID, SaveAction = 'S'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE ISNULL(mta.ProcessID,'') <> ISNULL(lta.ProcessID,'')

UPDATE mta SET [TADate] = lta.TADate, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TADate IS NULL AND lta.TADate IS NOT NULL) OR (mta.TADate IS NOT NULL AND lta.TADate IS NULL) OR (mta.TADate <> lta.TADate)

UPDATE mta SET [TAHrsDown] = lta.TAHrsDown, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAHrsDown IS NULL AND lta.TAHrsDown IS NOT NULL) OR (mta.TAHrsDown IS NOT NULL AND lta.TAHrsDown IS NULL) OR ABS(mta.TAHrsDown - lta.TAHrsDown) > 0.1

UPDATE mta SET [TACostLocal] = lta.TACostLocal, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TACostLocal IS NULL AND lta.TACostLocal IS NOT NULL) OR (mta.TACostLocal IS NOT NULL AND lta.TACostLocal IS NULL) OR ABS(mta.TACostLocal - lta.TACostLocal) > 0.1
	
UPDATE mta SET [TALaborCostLocal] = lta.TALaborCostLocal, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TALaborCostLocal IS NULL AND lta.TALaborCostLocal IS NOT NULL) OR (mta.TALaborCostLocal IS NOT NULL AND lta.TALaborCostLocal IS NULL) OR ABS(mta.TALaborCostLocal - lta.TALaborCostLocal) > 0.1

UPDATE mta SET [TAExpLocal] = lta.TAExpLocal, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAExpLocal IS NULL AND lta.TAExpLocal IS NOT NULL) OR (mta.TAExpLocal IS NOT NULL AND lta.TAExpLocal IS NULL) OR ABS(mta.TAExpLocal - lta.TAExpLocal) > 0.1

UPDATE mta SET [TACptlLocal] = lta.TACptlLocal, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TACptlLocal IS NULL AND lta.TACptlLocal IS NOT NULL) OR (mta.TACptlLocal IS NOT NULL AND lta.TACptlLocal IS NULL) OR ABS(mta.TACptlLocal - lta.TACptlLocal) > 0.1

UPDATE mta SET [TAOvhdLocal] = lta.TAOvhdLocal, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAOvhdLocal IS NULL AND lta.TAOvhdLocal IS NOT NULL) OR (mta.TAOvhdLocal IS NOT NULL AND lta.TAOvhdLocal IS NULL) OR ABS(mta.TAOvhdLocal - lta.TAOvhdLocal) > 0.1

UPDATE mta SET [TAOCCSTH] = lta.TAOCCSTH, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAOCCSTH IS NULL AND lta.TAOCCSTH IS NOT NULL) OR (mta.TAOCCSTH IS NOT NULL AND lta.TAOCCSTH IS NULL) OR ABS(mta.TAOCCSTH - lta.TAOCCSTH) > 1

UPDATE mta SET [TAOCCOVT] = lta.TAOCCOVT, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAOCCOVT IS NULL AND lta.TAOCCOVT IS NOT NULL) OR (mta.TAOCCOVT IS NOT NULL AND lta.TAOCCOVT IS NULL) OR ABS(mta.TAOCCOVT - lta.TAOCCOVT) > 1

UPDATE mta SET [TAMPSSTH] = lta.TAMPSSTH, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAMPSSTH IS NULL AND lta.TAMPSSTH IS NOT NULL) OR (mta.TAMPSSTH IS NOT NULL AND lta.TAMPSSTH IS NULL) OR ABS(mta.TAMPSSTH - lta.TAMPSSTH) > 1

UPDATE mta SET [TAMPSOVTPcnt] = lta.TAMPSOVTPcnt, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAMPSOVTPcnt IS NULL AND lta.TAMPSOVTPcnt IS NOT NULL) OR (mta.TAMPSOVTPcnt IS NOT NULL AND lta.TAMPSOVTPcnt IS NULL) OR ABS(mta.TAMPSOVTPcnt - lta.TAMPSOVTPcnt) > 0.01

UPDATE mta SET [TAContOCC] = lta.TAContOCC, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAContOCC IS NULL AND lta.TAContOCC IS NOT NULL) OR (mta.TAContOCC IS NOT NULL AND lta.TAContOCC IS NULL) OR ABS(mta.TAContOCC - lta.TAContOCC) > 1

UPDATE mta SET [TAContMPS] = lta.TAContMPS, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAContMPS IS NULL AND lta.TAContMPS IS NOT NULL) OR (mta.TAContMPS IS NOT NULL AND lta.TAContMPS IS NULL) OR ABS(mta.TAContMPS - lta.TAContMPS) > 1

UPDATE mta SET [PrevTADate] = lta.PrevTADate, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.PrevTADate IS NULL AND lta.PrevTADate IS NOT NULL) OR (mta.PrevTADate IS NOT NULL AND lta.PrevTADate IS NULL) OR (mta.PrevTADate <> lta.PrevTADate)

UPDATE mta SET [TAExceptions] = lta.TAExceptions, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE (mta.TAExceptions IS NULL AND lta.TAExceptions IS NOT NULL) OR (mta.TAExceptions IS NOT NULL AND lta.TAExceptions IS NULL) OR (mta.TAExceptions <> lta.TAExceptions)

UPDATE mta SET 	[TACurrency] = lta.TACurrency, SaveAction = 'U'
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE ISNULL(mta.TACurrency,'') <> ISNULL(lta.TACurrency,'')

-- If the turnaround is no longer in LoadTA, then delete it from the database
UPDATE @MaintTA
SET SaveAction = 'D'
WHERE LoadTAID IS NULL
-- Because Profile Lite only has current and previous T/A, 
-- need to make sure that this isn't an old one that has been pushed off the input stack but should still be included
UPDATE mta
SET SaveAction = NULL
FROM @MaintTA mta
WHERE SaveAction = 'D' 
AND NOT EXISTS (SELECT * FROM @LoadTA lta 
	WHERE lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID
	AND (lta.PrevTADate < mta.TADate))

INSERT @MaintTA ([RefineryID], [DataSet], [UnitID], [TAID], [SchedTAInt], [TADate], [TAHrsDown], [TACostLocal], 
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID], SaveAction, LoadTAID)
SELECT DISTINCT [RefineryID], [DataSet], [UnitID], [TAID] = -1, 
	[SchedTAInt], [TADate], [TAHrsDown], [TACostLocal],
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID], 'A', lta.TAID
FROM @LoadTA lta
WHERE NOT EXISTS (SELECT * FROM @MaintTA mta WHERE mta.RefineryID = lta.RefineryID AND mta.DataSet = lta.DataSet AND mta.UnitID = lta.UnitID AND mta.LoadTAID = lta.TAID)

-- Assign TA IDs for the new turnarounds
DECLARE @UnitID int, @TAID int, @LoadTAID int
WHILE EXISTS (SELECT * FROM @MaintTA WHERE TAID = -1)
BEGIN
	SELECT TOP 1 @UnitID = UnitID, @LoadTAID = LoadTAID
	FROM @MaintTA WHERE TAID = -1
	ORDER BY UnitID, TADate

	SELECT @TAID = NULL
	SELECT @TAID = MAX(TAID)+1 FROM @MaintTA WHERE UnitID = @UnitID AND TAID > 0
	IF @TAID IS NULL
		SET @TAID = 1
	UPDATE @MaintTA SET TAID = @TAID WHERE UnitID = @UnitID AND LoadTAID = @LoadTAID
END

-- Determine what submissions need to be recalculated after updates are applied
DECLARE @RecalcFrom smalldatetime
SELECT	@RecalcFrom = MIN(CASE WHEN lta.TADate < mta.TADate THEN lta.TADate ELSE mta.TADate END)
FROM @MaintTA mta INNER JOIN @LoadTA lta ON lta.RefineryID = mta.RefineryID AND lta.DataSet = mta.DataSet AND lta.UnitID = mta.UnitID AND lta.TAID = mta.LoadTAID
WHERE SaveAction IN ('A','D','U')

-- Save the changes to MaintTA and call calculations for each
DELETE FROM MaintTACost
WHERE RefineryID = @RefineryID AND DataSet = @DataSet 
	AND EXISTS (SELECT * FROM @MaintTA x WHERE x.UnitID = MaintTACost.UnitID AND x.TAID = MaintTACost.TAID AND x.SaveAction IS NOT NULL)
DELETE FROM MaintTA 
WHERE RefineryID = @RefineryID AND DataSet = @DataSet 
	AND EXISTS (SELECT * FROM @MaintTA x WHERE x.UnitID = MaintTA.UnitID AND x.TAID = MaintTA.TAID AND x.SaveAction IS NOT NULL)
INSERT MaintTA ([RefineryID], [DataSet], [UnitID], [TAID], [SchedTAInt], [TADate], [TAHrsDown], [TACostLocal], 
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID])
SELECT [RefineryID], [DataSet], [UnitID], [TAID], [SchedTAInt], [TADate], [TAHrsDown], [TACostLocal],
	[TALaborCostLocal], [TAExpLocal], [TACptlLocal], [TAOvhdLocal],
	[TAOCCSTH], [TAOCCOVT], [TAMPSSTH], [TAMPSOVTPcnt], [TAContOCC], [TAContMPS], [PrevTADate], [TAExceptions], [TACurrency], [ProcessID]
FROM @MaintTA WHERE SaveAction IS NOT NULL AND SaveAction <> 'D'

WHILE EXISTS (SELECT * FROM @MaintTA WHERE SaveAction IS NOT NULL AND SaveAction <> 'D')
BEGIN
	SELECT TOP 1 @UnitID = UnitID, @TAID = TAID
	FROM @MaintTA WHERE SaveAction IS NOT NULL AND SaveAction <> 'D'
	ORDER BY UnitID, TAID
	
	EXEC spTACalcs @RefineryID, @DataSet, @UnitID, @TAID

	DELETE FROM @MaintTA WHERE UnitID = @UnitID AND TAID = @TAID
END

-- Update the Next T/A date in MaintTA and MaintTACost
EXEC spUpdateNextTADate @RefineryID, @DataSet

-- Tag the submissions to make updates for turnaround changes
UPDATE SubmissionsAll
SET CalcsNeeded = 'T'
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
AND PeriodEnd >= @RecalcFrom AND CalcsNeeded IS NULL AND UseSubmission = 1

END


