﻿CREATE PROC [dbo].[SS_GetConfig]
	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
AS

	SELECT c.UnitID,RTRIM(c.ProcessID) as ProcessID,c.SortKey,RTRIM(c.UnitName) AS UnitName,RTRIM(c.ProcessType) AS ProcessType,c.RptCap, RTRIM(p.ProcessGroup) AS ProcessGroup,
            c.UtilPcnt,c.RptStmCap,c.StmUtilPcnt,c.InServicePcnt,c.DesignFeedSulfur,c.EnergyPcnt 
             FROM dbo.Config c,ProcessID_LU p WHERE  c.ProcessID=p.ProcessID AND  
             p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND 
            SubmissionID IN (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1 AND (PeriodStart BETWEEN @PeriodStart AND DATEADD(day, -1, @PeriodEnd)))


