﻿CREATE PROC [dbo].[DS_UnitTargets_LU]
	@CompanyID nvarchar(20)
AS

SELECT RTRIM(ProcessID) AS ProcessID, RTRIM(Property) AS Property, 
                   RTRIM(USDescription) AS USDescription,RTRIM(MetDescription) AS MetDescription, 
                   RTRIM(USDecPlaces) AS USDecPlaces,RTRIM(MetDecPlaces) AS MetDecPlaces,SortKey  
                   FROM UnitTargets_LU WHERE  
                  CustomGroup=0 OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CompanyID=@CompanyID AND CustomType='T2')

