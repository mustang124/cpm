﻿CREATE   PROC [dbo].[spReportBashneftChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @Dataset)

IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	ProcessUtilPcnt real NULL, 
	VEI real NULL, 
	OpAvail real NULL, 
	MaintEffIndex real NULL,
	nmPersEffIndex real NULL, 
	NEOpexEDC real NULL,
	OpexUEDC real NULL
)
DECLARE @msgString varchar(255)
SELECT @msgString = CAST(@PeriodEnd as varchar(20))

INSERT INTO @data (PeriodStart, PeriodEnd, EII, ProcessUtilPcnt, VEI, OpAvail, MaintEffIndex, nmPersEffIndex, NEOpexEDC, OpexUEDC)
SELECT PeriodStart, PeriodEnd, EII, ProcessUtilPcnt, VEI, OpAvail, MaintEffIndex, nmPersEffIndex, NEOpexEDC, OpexUEDC
FROM dbo.GetCommonProfileLiteChartData(@RefineryID, @PeriodYear, @PeriodMonth, @Dataset, @FactorSet, @Scenario, @Currency, @UOM, 12)

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, ProcessUtilPcnt, VEI, OpAvail, MaintEffIndex, nmPersEffIndex, NEOpexEDC, OpexUEDC
FROM @Data
ORDER BY PeriodStart ASC


