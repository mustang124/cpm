﻿CREATE      PROC [dbo].[spMaterialCalc] (@SubmissionID int)
AS

SET NOCOUNT ON 

DECLARE @RMI float, @OTHRM float, @FCHEM float, @FLUBE float, @RCHEM float, @RLUBE float, 
	@PROD float, @MPROD float, @SOLV float, @ASP float, @COK float
DECLARE @TotInput float, @TotYield float, @Gain float, @NetInput float, @SpecialInputBbl float, @MarginInputBbl float
DECLARE @Study varchar(3), @PVPYield float, @PVPInput float

DELETE FROM MaterialTot WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialNet WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialST WHERE SubmissionID = @SubmissionID

SELECT @RMI = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'RMI'

SELECT @OTHRM = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'OTHRM'

SELECT @FCHEM = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'FCHEM'

SELECT @FLUBE = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'FLUBE'

SELECT @RCHEM = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'RCHEM'

SELECT @RLUBE = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'RLUBE'

SELECT @PROD = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category IN ('PROD', 'RPF') AND MaterialID <> 'GAIN'

SELECT @SOLV = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'SOLV'

SELECT @ASP = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'ASP'

SELECT @COK = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'COKE'

SELECT @MPROD = SUM(Bbl) FROM Yield
WHERE SubmissionID = @SubmissionID AND Category = 'MPROD'

SELECT 	@RMI = ISNULL(@RMI, 0), @OTHRM = ISNULL(@OTHRM, 0), @RCHEM = ISNULL(@RCHEM, 0), @RLUBE = ISNULL(@RLUBE, 0),
	@Prod = ISNULL(@Prod, 0), @MPROD = ISNULL(@MPROD, 0), @FCHEM = ISNULL(@FCHEM, 0), @FLUBE = ISNULL(@FLUBE, 0), 
	@SOLV = ISNULL(@SOLV, 0), @ASP = ISNULL(@ASP, 0), @COK = ISNULL(@COK, 0)

SELECT @TotInput = ISNULL(@RMI, 0) + ISNULL(@OTHRM, 0) + ISNULL(@RCHEM, 0) + ISNULL(@RLUBE, 0)
SELECT @TotYield = ISNULL(@Prod, 0) + ISNULL(@MPROD, 0) + ISNULL(@FCHEM, 0) + ISNULL(@FLUBE, 0) + ISNULL(@SOLV, 0) + ISNULL(@ASP, 0) + ISNULL(@COK, 0)
SELECT @Gain = @TotYield - @TotInput

SELECT MaterialID, OTHRMBbl = SUM(CASE WHEN Category = 'OTHRM' THEN ISNULL(Bbl, 0) ELSE 0 END), 
RCHEMBbl = SUM(CASE WHEN Category = 'RCHEM' THEN ISNULL(Bbl, 0) ELSE 0 END), 
RLUBEBbl = SUM(CASE WHEN Category = 'RLUBE' THEN ISNULL(Bbl, 0) ELSE 0 END), 
FCHEMBbl = SUM(CASE WHEN Category = 'FCHEM' THEN ISNULL(Bbl, 0) ELSE 0 END),
FLUBEBbl = SUM(CASE WHEN Category = 'FLUBE' THEN ISNULL(Bbl, 0) ELSE 0 END),
MPRODBbl = SUM(CASE WHEN Category = 'MPROD' THEN ISNULL(Bbl, 0) ELSE 0 END), 
SOLVBbl = SUM(CASE WHEN Category = 'SOLV' THEN ISNULL(Bbl, 0) ELSE 0 END), 
ASPBbl = SUM(CASE WHEN Category = 'ASP' THEN ISNULL(Bbl, 0) ELSE 0 END),
COKEBbl = SUM(CASE WHEN Category = 'COKE' THEN ISNULL(Bbl, 0) ELSE 0 END)
INTO #Net
FROM Yield
WHERE SubmissionID = @SubmissionID
AND Category IN ('OTHRM', 'MPROD', 'SOLV', 'ASP', 'COKE', 'RCHEM', 'RLUBE', 'FCHEM', 'FLUBE') 
GROUP BY MaterialID

UPDATE #Net
SET 	OTHRMBbl = OTHRMBbl - (MPRODBbl + ASPBbl + COKEBbl),
	MPRODBbl = 0, ASPBbl = 0, COKEBbl = 0
WHERE OTHRMBbl >= (MPRODBbl + ASPBbl + COKEBbl)

UPDATE #Net
SET	MPRODBbl = MPRODBbl - OTHRMBbl, OTHRMBbl = OTHRMBbl - MPRODBbl
WHERE MPRODBbl > 0 AND OTHRMBbl > 0

UPDATE #Net
SET	ASPBbl = ASPBbl - OTHRMBbl, OTHRMBbl = OTHRMBbl - ASPBbl
WHERE ASPBbl > 0 AND OTHRMBbl > 0

UPDATE #Net
SET	COKEBbl = COKEBbl - OTHRMBbl, OTHRMBbl = OTHRMBbl - COKEBbl
WHERE COKEBbl > 0 AND OTHRMBbl > 0

UPDATE #Net
SET	FCHEMBbl = FCHEMBbl - RCHEMBbl, RCHEMBbl = RCHEMBbl - FCHEMBbl
WHERE FCHEMBbl > 0 AND RCHEMBbl > 0

UPDATE #Net
SET	FLUBEBbl = FLUBEBbl - RLUBEBbl, RLUBEBbl = RLUBEBbl - FLUBEBbl
WHERE FLUBEBbl > 0 AND RLUBEBbl > 0



INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'OTHRM', MaterialID, OTHRMBbl
FROM #Net WHERE OTHRMBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'RCHEM', MaterialID, RCHEMBbl
FROM #Net WHERE RCHEMBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'RLUBE', MaterialID, RLUBEBbl
FROM #Net WHERE RLUBEBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'FCHEM', MaterialID, FCHEMBbl
FROM #Net WHERE FCHEMBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'FLUBE', MaterialID, FLUBEBbl
FROM #Net WHERE FLUBEBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'MPROD', MaterialID, MPRODBbl
FROM #Net WHERE MPRODBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'ASP', MaterialID, ASPBbl
FROM #Net WHERE ASPBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'COKE', MaterialID, COKEBbl
FROM #Net WHERE COKEBbl > 0
INSERT INTO MaterialNet (SubmissionID, Category, MaterialID, Bbl)
SELECT @SubmissionID, 'SOLV', MaterialID, SOLVBbl
FROM #Net WHERE SOLVBbl > 0

DROP TABLE #Net

INSERT INTO MaterialST (SubmissionID, Category, GrossBbl, NetBbl)
SELECT @SubmissionID, Category, SUM(Bbl), SUM(Bbl)
FROM Yield
WHERE SubmissionID = @SubmissionID 
GROUP BY Category

UPDATE MaterialST
SET NetBbl = CASE WHEN @FCHEM > @RCHEM THEN @FCHEM - @RCHEM ELSE 0 END
WHERE SubmissionID = @SubmissionID AND Category = 'FCHEM'
UPDATE MaterialST
SET NetBbl = CASE WHEN @RCHEM > @FCHEM THEN @RCHEM - @FCHEM ELSE 0 END
WHERE SubmissionID = @SubmissionID AND Category = 'RCHEM'
UPDATE MaterialST
SET NetBbl = CASE WHEN @FLUBE > @RLUBE THEN @FLUBE - @RLUBE ELSE 0 END
WHERE SubmissionID = @SubmissionID AND Category = 'FLUBE'
UPDATE MaterialST
SET NetBbl = CASE WHEN @RLUBE > @FLUBE THEN @RLUBE - @FLUBE ELSE 0 END
WHERE SubmissionID = @SubmissionID AND Category = 'RLUBE'

UPDATE MaterialST
SET NetBbl = (SELECT SUM(Bbl) FROM MaterialNet WHERE MaterialNet.SubmissionId = MaterialST.SubmissionID AND MaterialNet.Category = MaterialST.Category)
WHERE SubmissionID = @SubmissionID AND Category IN ('OTHRM','MPROD','ASP','COKE')

SELECT @NetInput = SUM(NetBbl) 
FROM MaterialST
WHERE SubmissionID = @SubmissionID AND Category IN ('RMI','OTHRM','RCHEM','RLUBE')

SELECT @SpecialInputBbl = SUM(Bbl)
FROM MaterialNet WHERE SubmissionID = @SubmissionID AND Category In (SELECT Category FROM MaterialCategory_LU WHERE NetGroup = 'I')
AND MaterialID IN (SELECT MaterialID FROM Material_LU WHERE IncludeForMarginCalc = 'N')

IF @SpecialInputBbl IS NULL
	SET @SpecialInputBbl = 0

SELECT @MarginInputBbl = @NetInput-@SpecialInputBbl

SELECT @Study = Study FROM TSort
WHERE RefineryID = (SELECT RefineryID FROM SubmissionsAll WHERE SubmissionID = @SubmissionID)
IF @Study IS NULL 
	SELECT @Study = 'NSA'

-- Calculate Premium-Value Products (BBL, Percent)
SELECT 	@PVPYield = SUM(CASE WHEN Category = 'OTHRM' THEN 0 ELSE BBL END),
	@PVPInput = SUM(CASE WHEN Category = 'OTHRM' THEN Bbl ELSE 0 END)
FROM Yield 
WHERE SubmissionID = @SubmissionID
AND Category IN ('PROD', 'SOLV', 'ASP', 'COKE', 'MPROD', 'OTHRM')
AND MaterialID IN (SELECT MaterialID FROM Material_LU
	WHERE (@Study = 'NSA' AND PVPNSA = 'Y') 
	OR (@Study = 'EUR' AND PVPEUR = 'Y') 
	OR (@Study = 'PAC' AND PVPPAC = 'Y'))

SELECT @PVPYield = ISNULL(@PVPYield, 0) + ISNULL(@FCHEM, 0) + ISNULL(@FLUBE, 0) - ISNULL(@RCHEM, 0) - ISNULL(@RLUBE, 0) - ISNULL(@PVPInput, 0)

IF EXISTS (SELECT * FROM TSort t INNER JOIN SubmissionsAll s ON s.RefineryID = t.RefineryID WHERE s.SubmissionID = @SubmissionID AND t.Study IN ('LUB','FL'))
BEGIN
	DECLARE @LubeBbl float, @WaxBbl float, @SpecBbl float, @LWBbl float, @LWSBbl float, @ByProdBbl float, 
		@LWSPcntInput float, @LWSBblPerDay float
	SELECT 	@LubeBbl = SUM(CASE WHEN l.LWSCategory = 'L' THEN Bbl ELSE 0 END), -- Lubes
		@WaxBbl = SUM(CASE WHEN l.LWSCategory = 'W' THEN Bbl ELSE 0 END), -- Waxes
		@SpecBbl = SUM(CASE WHEN l.LWSCategory = 'S' THEN Bbl ELSE 0 END) -- Specialties
	FROM Yield y, Material_LU l
	WHERE y.MaterialID = l.MaterialID
	AND y.SubmissionID = @SubmissionID AND y.Category IN ('PROD', 'MPROD')
	IF @LubeBbl IS NULL
		SET @LubeBbl = 0
	IF @WaxBbl IS NULL
		SET @WaxBbl = 0
	IF @SpecBbl IS NULL
		SET @SpecBbl = 0
		
	-- By-products
	SELECT @ByProdBbl = SUM(Bbl)
	FROM Yield WHERE Category IN (SELECT Category FROM MaterialCategory_LU WHERE NetGroup = 'BP')
	AND SubmissionID = @SubmissionID
	IF @ByProdBbl IS NULL
		SET @ByProdBbl = 0
		
	SELECT @LWBbl = @LubeBbl + @WaxBbl
	SELECT @LWSBbl = @LWBbl + @SpecBbl
	SELECT @LWSBblPerDay = @LWSBbl/SubmissionsAll.NumDays FROM SubmissionsAll WHERE SubmissionID = @SubmissionID AND NumDays > 0
	IF @TotInput > 0
		SELECT @LWSPcntInput = @LWSBbl/@TotInput*100
END

INSERT INTO MaterialTot (SubmissionID, TotInputBbl, NetInputBbl, MarginInputBbl, GainBbl, PVP, PVPPcnt, LubeBbl, WaxBbl, SpecBbl, LWBbl, LWSBbl, ByProdBbl, LWSPcntInput, LWSBblPerDay)
SELECT @SubmissionID, @TotInput, @NetInput, @MarginInputBbl, @Gain, @PVPYield, CASE WHEN @NetInput > 0 THEN @PVPYield/@NetInput*100 END
	, @LubeBbl, @WaxBbl, @SpecBbl, @LWBbl, @LWSBbl, @ByProdBbl, @LWSPcntInput, @LWSBblPerDay


