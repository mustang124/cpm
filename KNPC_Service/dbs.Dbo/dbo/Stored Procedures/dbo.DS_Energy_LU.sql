﻿CREATE PROC [dbo].[DS_Energy_LU]
	
AS

SELECT RTRIM(Header) as Header, RTRIM(TransType) as TransType,
                    RTRIM(TransTypeDesc) as TransTypeDesc, RTRIM(TransferTo) as TransferTo,
                    RTRIM(EnergyType) as EnergyType, RTRIM(EnergyTypeDesc) as EnergyTypeDesc, Composition,
                     SortKey  
                     FROM Energy_LU 
                    ORDER BY SortKey

