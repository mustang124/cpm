﻿CREATE   PROC [dbo].[spReportRosneftChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'RUB', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	RefUtilPcnt real NULL, 
	ProcessUtilPcnt real NULL, 
	OpAvail real NULL, 
	EII real NULL, 
	ProcessEffIndex real NULL, 
	VEI real NULL, 
	PersIndex real NULL, 
	MaintIndex real NULL, 
	NEOpexEDC real NULL,
	OpexEDC real NULL,
	OpexUEDC real NULL,
	RefUtilPcnt_QTR real NULL, 
	ProcessUtilPcnt_QTR real NULL, 
	OpAvail_QTR real NULL, 
	EII_QTR real NULL, 
	ProcessEffIndex_QTR real NULL, 
	VEI_QTR real NULL, 
	PersIndex_QTR real NULL, 
	MaintIndex_QTR real NULL, 
	NEOpexEDC_QTR real NULL,
	OpexEDC_QTR real NULL,
	OpexUEDC_QTR real NULL
)

--- Everything Already Available in Gensum (missing gain and Opex on an EDC basis)
INSERT INTO @data (PeriodStart, PeriodEnd, RefUtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex, MaintIndex, NEOpexEDC, OpexUEDC, OpexEDC)
SELECT	s.PeriodStart, s.PeriodEnd, UtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex = TotWHrEDC, MaintIndex = RoutIndex + TAIndex_Avg, NEOpexEDC, TotCashOpexUEDC
	, OpexEDC = (SELECT TotCashOpex FROM OpexCalc o WHERE o.SubmissionID = Gensum.SubmissionID AND o.Currency = Gensum.Currency AND o.Scenario = 'CLIENT' AND o.FactorSet = Gensum.FactorSet AND o.DataType = 'EDC')
FROM Gensum INNER JOIN Submissions s ON s.SubmissionID = GenSum.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @PeriodStart12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND Gensum.FactorSet = @FactorSet AND Gensum.Currency = @Currency AND Gensum.UOM = @UOM AND Gensum.Scenario = @Scenario


DECLARE @ProcessEffIndex real
DECLARE @StartQTR smalldatetime, @EndQTR smalldatetime

DECLARE cMonths CURSOR LOCAL FAST_FORWARD
FOR SELECT PeriodStart, PeriodEnd FROM @data
OPEN cMonths 
FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @ProcessEffIndex = f.PEI
	FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND f.FactorSet = @FactorSet 
	AND s.PeriodStart = @PeriodStart AND s.UseSubmission = 1

	SELECT @StartQTR = dbo.BuildDate(DATEPART(yy, @PeriodEnd), DATEPART(QUARTER, DATEADD(dd, -1, @PeriodEnd))*3-2, 1)
	SELECT @EndQTR = DATEADD(mm, 3, @StartQTR)

	UPDATE @data
	SET ProcessEffIndex = @ProcessEffIndex
	WHERE PeriodStart = @PeriodStart

	UPDATE data
	SET RefUtilPcnt_QTR = q.RefUtilPcnt, ProcessUtilPcnt_QTR = q.ProcessUtilPcnt,
		OpAvail_QTR = q.OpAvail, EII_QTR = q.EII, 
		ProcessEffIndex_QTR = q.ProcessEffIndex, VEI_QTR = q.VEI, 
		PersIndex_QTR = q.PersIndex, MaintIndex_QTR = q.MaintIndex, 
		NEOpexEDC_QTR = q.NEOpexEDC, OpexEDC_QTR = q.OpexEDC, OpexUEDC_QTR = q.OpexUEDC
	FROM @Data data CROSS APPLY dbo.GetProfileLiteKPIs(@RefineryID, @Dataset, @StartQTR, @EndQTR, @FactorSet, @Scenario, @Currency, @UOM) q
	FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
END
CLOSE cMonths
DEALLOCATE cMonths

IF (SELECT COUNT(*) FROM @data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
			INSERT @data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

/* Achinsk & Komsomolsk need PEI instead of VEI */
SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilPcnt = RefUtilPcnt, VolGainKPI = CASE WHEN @RefineryID IN ('324EUR') THEN ProcessEffIndex ELSE VEI END
	, OpAvail, PersIndex, MaintIndex, NEOpexEDC, OpexUEDC = OpexUEDC/100
	, EII_QTR, UtilPcnt = RefUtilPcnt_QTR, VolGainKPI_QTR = CASE WHEN @RefineryID IN ('324EUR') THEN ProcessEffIndex_QTR ELSE VEI_QTR END
	, OpAvail_QTR, PersIndex_QTR, MaintIndex_QTR, NEOpexEDC_QTR, OpexUEDC_QTR
--  , OpexEDC
FROM @Data
ORDER BY PeriodStart ASC



