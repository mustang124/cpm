﻿CREATE PROC [dbo].[DS_ProcessIDFC_LU]
	
AS

SELECT p.SortKey,RTRIM(Description) AS ProcessDesc, RTRIM(ProcessID) AS ProcessID,  
                      RTRIM(ProcessGroup) AS ProcessGroup, RTRIM(ProfileProcFacility) AS ProfileProcFacility, RTRIM(MaintDetails) AS MaintDetails,  
                      RTRIM(DisplayTextUS) AS DisplayTextUS,  
                      RTRIM(DisplayTextMet) AS DisplayTextMet,  
                      RTRIM(SulfurAdj) AS SulfurAdj  
                      FROM ProcessID_LU p, DisplayUnits_LU d  
                      WHERE p.DisplayUnits = d.DisplayUnits  
                      AND p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD')  
                      ORDER BY p.SortKey

