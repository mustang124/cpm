﻿CREATE PROC [dbo].[SS_GetInputElectric]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) as TransferTo, RTRIM(e.EnergyType) as EnergyType,e.TransCode,e.RptGenEff,e.RptMWH,e.PriceLocal,elu.SortKey  
            FROM  
            dbo.Electric e ,Energy_LU elu  
            ,dbo.Submissions s  
            WHERE  
            e.SubmissionID = s.SubmissionID AND 
            elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND elu.EnergyType=e.EnergyType AND elu.SortKey > 100 AND  
            (e.SubmissionID IN  
            (SELECT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1))


