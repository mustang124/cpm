﻿
CREATE   PROC [dbo].[spReportChevronKPI_REF] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1,
	@UtilPcnt real = NULL OUTPUT, @KEDC real = NULL OUTPUT, @KUEDC real = NULL OUTPUT, 
	@MechAvail real = NULL OUTPUT, @KMAEDC real = NULL OUTPUT, @KProcEDC real = NULL OUTPUT,
	@EII real = NULL OUTPUT, @KEnergyUseDay real = NULL OUTPUT, @KTotStdEnergy real = NULL OUTPUT,
	@AdjMaintIndex real = NULL OUTPUT, @TAIndex_Avg real = NULL OUTPUT, @RoutIndex real = NULL OUTPUT,   
	@TotEqPEDC real = NULL OUTPUT, 
	@NEOpexUEDC real = NULL OUTPUT, @TotCashOpexUEDC real = NULL OUTPUT,
	@CashMargin real = NULL OUTPUT, @NetInputBPD real = NULL OUTPUT)

AS
SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = 'Actual' AND UseSubmission = 1

SELECT	@UtilPcnt = NULL, @KEDC = NULL, @KUEDC = NULL, 
	@MechAvail = NULL, @KMAEDC = NULL, @KProcEDC = NULL,
	@EII = NULL, @KEnergyUseDay = NULL, @KTotStdEnergy = NULL,
	@AdjMaintIndex = NULL, @TAIndex_Avg = NULL, @RoutIndex = NULL,   
	@TotEqPEDC = NULL, 
	@NEOpexUEDC = NULL, @TotCashOpexUEDC = NULL,
	@CashMargin = NULL, @NetInputBPD = NULL
IF @SubmissionID IS NULL
	RETURN 0

SELECT	@UtilPcnt = UtilPcnt, @KEDC = EDC/1000, @KUEDC = UEDC/1000, 
	@MechAvail = MechAvail, 
	@EII = EII,
	@AdjMaintIndex = RoutIndex+TAIndex_Avg, @TAIndex_Avg = TAIndex_Avg, @RoutIndex = RoutIndex,   
	@TotEqPEDC = TotEqPEDC, 
	@NEOpexUEDC = NEOpexUEDC, @TotCashOpexUEDC = TotCashOpexUEDC,
	@CashMargin = CashMargin, @NetInputBPD = NetInputBPD
FROM Gensum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @KMAEDC = TotProcessEDC*@MechAvail/100/1000, @KProcEDC = TotProcessEDC/1000,
	@KEnergyUseDay = EnergyUseDay/1000, @KTotStdEnergy = TotStdEnergy/1000
FROM FactorTotCalc 
WHERE SubmissionID = @SubmissionID AND FactorSet = '2008'







