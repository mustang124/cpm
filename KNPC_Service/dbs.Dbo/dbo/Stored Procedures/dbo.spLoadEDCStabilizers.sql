﻿CREATE      PROC [dbo].[spLoadEDCStabilizers] (@SubmissionID int)
AS

IF NOT EXISTS (SELECT * FROM LoadEDCStabilizers WHERE SubmissionID = @SubmissionID)
	RETURN 0
	
DECLARE @RefineryID varchar(6), @DataSet varchar(15)
DECLARE @LoadEffDate smalldatetime, @LoadEffUntil smalldatetime,
	@LoadAnnInputBbl float, @LoadAnnCokeBbl float, @LoadAnnElecConsMWH float,
	@LoadAnnRSCRUDE_RAIL float, @LoadAnnRSCRUDE_TT float, @LoadAnnRSCRUDE_TB float, @LoadAnnRSCRUDE_OMB float, @LoadAnnRSCRUDE_BB float, 
	@LoadAnnRSPROD_RAIL float, @LoadAnnRSPROD_TT float, @LoadAnnRSPROD_TB float, @LoadAnnRSPROD_OMB float, @LoadAnnRSPROD_BB float
DECLARE @OldEffDate smalldatetime, @OldEffUntil smalldatetime,
	@OldAnnInputBbl float, @OldAnnCokeBbl float, @OldAnnElecConsMWH float,
	@OldAnnRSCRUDE_RAIL float, @OldAnnRSCRUDE_TT float, @OldAnnRSCRUDE_TB float, @OldAnnRSCRUDE_OMB float, @OldAnnRSCRUDE_BB float, 
	@OldAnnRSPROD_RAIL float, @OldAnnRSPROD_TT float, @OldAnnRSPROD_TB float, @OldAnnRSPROD_OMB float, @OldAnnRSPROD_BB float
DECLARE @EffDate smalldatetime, @FOMEffDate smalldatetime, @UpdateAction char(1), @RecalcFrom smalldatetime, @RecalcTo smalldatetime
DECLARE @dates TABLE (
	EffDate smalldatetime NOT NULL,
	FOMEffDate smalldatetime NULL,
	EffUntil smalldatetime NULL,
	UpdateAction char(1) NULL,
	RecalcFrom smalldatetime NULL,
	RecalcTo smalldatetime NULL
)
SELECT @RefineryID = RefineryID, @DataSet = DataSet
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID

INSERT INTO @dates (EffDate, FOMEffDate)
SELECT EffDate, EffDate
FROM LoadEDCStabilizers
WHERE SubmissionID = @SubmissionID
UPDATE @dates SET FOMEffDate = DATEADD(dd, 10, EffDate) WHERE DATEPART(dd, EffDate) > 27
UPDATE @dates SET FOMEffDate = CAST(CAST(DATEPART(mm, FOMEffDate) as varchar(2)) + '/1/' + CAST(DATEPART(yyyy, FOMEffDate) as varchar(4)) as smalldatetime)

INSERT INTO @dates (EffDate, EffUntil)
SELECT EffDate, EffUntil
FROM LoadedEDCStabilizers
WHERE RefineryID = @RefineryID AND DataSet = @DataSet 
AND EffDate NOT IN (SELECT FOMEffDate FROM @dates) AND EffDate NOT IN (SELECT EffDate FROM @dates)

DECLARE cDates CURSOR SCROLL 
FOR SELECT EffDate, FOMEffDate FROM @dates ORDER BY EffDate
OPEN cDates
FETCH NEXT FROM cDates INTO @EffDate, @FOMEffDate
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @UpdateAction = NULL, @RecalcFrom = NULL, @RecalcTo = NULL
	SELECT @LoadEffDate = NULL, @LoadEffUntil = NULL,
	@LoadAnnInputBbl = NULL, @LoadAnnCokeBbl = NULL, @LoadAnnElecConsMWH = NULL,
	@LoadAnnRSCRUDE_RAIL = NULL, @LoadAnnRSCRUDE_TT = NULL, @LoadAnnRSCRUDE_TB = NULL, @LoadAnnRSCRUDE_OMB = NULL, @LoadAnnRSCRUDE_BB  = NULL, 
	@LoadAnnRSPROD_RAIL = NULL, @LoadAnnRSPROD_TT = NULL, @LoadAnnRSPROD_TB = NULL, @LoadAnnRSPROD_OMB = NULL, @LoadAnnRSPROD_BB = NULL
	SELECT @OldEffDate = NULL, @OldEffUntil = NULL,
	@OldAnnInputBbl = NULL, @OldAnnCokeBbl = NULL, @OldAnnElecConsMWH = NULL,
	@OldAnnRSCRUDE_RAIL = NULL, @OldAnnRSCRUDE_TT = NULL, @OldAnnRSCRUDE_TB = NULL, @OldAnnRSCRUDE_OMB = NULL, @OldAnnRSCRUDE_BB  = NULL, 
	@OldAnnRSPROD_RAIL = NULL, @OldAnnRSPROD_TT = NULL, @OldAnnRSPROD_TB = NULL, @OldAnnRSPROD_OMB = NULL, @OldAnnRSPROD_BB = NULL

	SELECT @LoadEffDate = EffDate, @LoadEffUntil = ISNULL((SELECT MIN(EffDate) FROM LoadEDCStabilizers n WHERE n.SubmissionID = @SubmissionID AND EffDate > @EffDate), '1/1/2050'),
	@LoadAnnInputBbl = AnnInputBbl, @LoadAnnCokeBbl = AnnCokeBbl, @LoadAnnElecConsMWH = AnnElecConsMWH,
	@LoadAnnRSCRUDE_RAIL = AnnRSCRUDE_RAIL, @LoadAnnRSCRUDE_TT = AnnRSCRUDE_TT, @LoadAnnRSCRUDE_TB = AnnRSCRUDE_TB, @LoadAnnRSCRUDE_OMB = AnnRSCRUDE_OMB, @LoadAnnRSCRUDE_BB  = AnnRSCRUDE_BB, 
	@LoadAnnRSPROD_RAIL = AnnRSPROD_RAIL, @LoadAnnRSPROD_TT = AnnRSPROD_TT, @LoadAnnRSPROD_TB = AnnRSPROD_TB, @LoadAnnRSPROD_OMB = AnnRSPROD_OMB, @LoadAnnRSPROD_BB = AnnRSPROD_BB
	FROM LoadEDCStabilizers
	WHERE SubmissionID = @SubmissionID AND EffDate = @EffDate
	IF DATEPART(dd, @LoadEffUntil) > 27
		SELECT @LoadEffUntil = DATEADD(dd, 10, @LoadEffUntil)
	SELECT @LoadEffUntil = CAST(CAST(DATEPART(mm, @LoadEffUntil) as varchar(2)) + '/1/' + CAST(DATEPART(yyyy, @LoadEffUntil) as varchar(4)) as smalldatetime)

	SELECT @LoadAnnInputBbl = CASE WHEN ISNULL(@LoadAnnInputBbl, 0) = 0 THEN NULL WHEN @LoadAnnInputBbl < 1 THEN 0 ELSE @LoadAnnInputBbl END
	SELECT @LoadAnnCokeBbl = CASE WHEN ISNULL(@LoadAnnCokeBbl, 0) = 0 THEN NULL WHEN @LoadAnnCokeBbl < 1 THEN 0 ELSE @LoadAnnCokeBbl END
	SELECT @LoadAnnElecConsMWH = CASE WHEN ISNULL(@LoadAnnElecConsMWH, 0) = 0 THEN NULL WHEN @LoadAnnElecConsMWH < 1 THEN 0 ELSE @LoadAnnElecConsMWH END
	SELECT @LoadAnnRSCRUDE_RAIL = CASE WHEN ISNULL(@LoadAnnRSCRUDE_RAIL, 0) = 0 THEN NULL WHEN @LoadAnnRSCRUDE_RAIL < 1 THEN 0 ELSE @LoadAnnRSCRUDE_RAIL END
	SELECT @LoadAnnRSCRUDE_TT = CASE WHEN ISNULL(@LoadAnnRSCRUDE_TT, 0) = 0 THEN NULL WHEN @LoadAnnRSCRUDE_TT < 1 THEN 0 ELSE @LoadAnnRSCRUDE_TT END
	SELECT @LoadAnnRSCRUDE_TB = CASE WHEN ISNULL(@LoadAnnRSCRUDE_TB, 0) = 0 THEN NULL WHEN @LoadAnnRSCRUDE_TB < 1 THEN 0 ELSE @LoadAnnRSCRUDE_TB END
	SELECT @LoadAnnRSCRUDE_OMB = CASE WHEN ISNULL(@LoadAnnRSCRUDE_OMB, 0) = 0 THEN NULL WHEN @LoadAnnRSCRUDE_OMB < 1 THEN 0 ELSE @LoadAnnRSCRUDE_OMB END
	SELECT @LoadAnnRSCRUDE_BB = CASE WHEN ISNULL(@LoadAnnRSCRUDE_BB, 0) = 0 THEN NULL WHEN @LoadAnnRSCRUDE_BB < 1 THEN 0 ELSE @LoadAnnRSCRUDE_BB END
	SELECT @LoadAnnRSPROD_RAIL = CASE WHEN ISNULL(@LoadAnnRSPROD_RAIL, 0) = 0 THEN NULL WHEN @LoadAnnRSPROD_RAIL < 1 THEN 0 ELSE @LoadAnnRSPROD_RAIL END
	SELECT @LoadAnnRSPROD_TT = CASE WHEN ISNULL(@LoadAnnRSPROD_TT, 0) = 0 THEN NULL WHEN @LoadAnnRSPROD_TT < 1 THEN 0 ELSE @LoadAnnRSPROD_TT END
	SELECT @LoadAnnRSPROD_TB = CASE WHEN ISNULL(@LoadAnnRSPROD_TB, 0) = 0 THEN NULL WHEN @LoadAnnRSPROD_TB < 1 THEN 0 ELSE @LoadAnnRSPROD_TB END
	SELECT @LoadAnnRSPROD_OMB = CASE WHEN ISNULL(@LoadAnnRSPROD_OMB, 0) = 0 THEN NULL WHEN @LoadAnnRSPROD_OMB < 1 THEN 0 ELSE @LoadAnnRSPROD_OMB END
	SELECT @LoadAnnRSPROD_BB = CASE WHEN ISNULL(@LoadAnnRSPROD_BB, 0) = 0 THEN NULL WHEN @LoadAnnRSPROD_BB < 1 THEN 0 ELSE @LoadAnnRSPROD_BB END

	SELECT @OldEffDate = EffDate, @OldEffUntil = EffUntil,
	@OldAnnInputBbl = AnnInputBbl, @OldAnnCokeBbl = AnnCokeBbl, @OldAnnElecConsMWH = AnnElecConsMWH,
	@OldAnnRSCRUDE_RAIL = AnnRSCRUDE_RAIL, @OldAnnRSCRUDE_TT = AnnRSCRUDE_TT, @OldAnnRSCRUDE_TB = AnnRSCRUDE_TB, @OldAnnRSCRUDE_OMB = AnnRSCRUDE_OMB, @OldAnnRSCRUDE_BB  = AnnRSCRUDE_BB, 
	@OldAnnRSPROD_RAIL = AnnRSPROD_RAIL, @OldAnnRSPROD_TT = AnnRSPROD_TT, @OldAnnRSPROD_TB = AnnRSPROD_TB, @OldAnnRSPROD_OMB = AnnRSPROD_OMB, @OldAnnRSPROD_BB = AnnRSPROD_BB
	FROM LoadedEDCStabilizers
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND EffDate = @FOMEffDate

	IF @OldEffDate IS NULL AND @LoadEffDate IS NOT NULL
		SELECT @UpdateAction = 'A', @RecalcFrom = @LoadEffDate
	IF @LoadEffDate IS NULL
		SELECT @UpdateAction = 'D', @RecalcFrom = @OldEffDate
	IF @UpdateAction IS NULL
	BEGIN
		IF NOT (((@OldEffUntil IS NULL AND @LoadEffUntil IS NULL) OR (@OldEffUntil = @LoadEffUntil))
			AND ((@OldAnnInputBbl IS NULL AND @LoadAnnInputBbl IS NULL) OR ABS(ISNULL(@OldAnnInputBbl,0) - ISNULL(@LoadAnnInputBbl,0))<0.01)
			AND ((@OldAnnCokeBbl IS NULL AND @LoadAnnCokeBbl IS NULL) OR ABS(ISNULL(@OldAnnCokeBbl,0) - ISNULL(@LoadAnnCokeBbl,0))<0.01)
			AND ((@OldAnnElecConsMWH IS NULL AND @LoadAnnElecConsMWH IS NULL) OR ABS(ISNULL(@OldAnnElecConsMWH,0) - ISNULL(@LoadAnnElecConsMWH,0))<0.01)
			AND ((@OldAnnRSCRUDE_RAIL IS NULL AND @LoadAnnRSCRUDE_RAIL IS NULL) OR ABS(ISNULL(@OldAnnRSCRUDE_RAIL,0) - ISNULL(@LoadAnnRSCRUDE_RAIL,0))<0.01)
			AND ((@OldAnnRSCRUDE_TT IS NULL AND @LoadAnnRSCRUDE_TT IS NULL) OR ABS(ISNULL(@OldAnnRSCRUDE_TT,0) - ISNULL(@LoadAnnRSCRUDE_TT,0))<0.01)
			AND ((@OldAnnRSCRUDE_TB IS NULL AND @LoadAnnRSCRUDE_TB IS NULL) OR ABS(ISNULL(@OldAnnRSCRUDE_TB,0) - ISNULL(@LoadAnnRSCRUDE_TB,0))<0.01)
			AND ((@OldAnnRSCRUDE_OMB IS NULL AND @LoadAnnRSCRUDE_OMB IS NULL) OR ABS(ISNULL(@OldAnnRSCRUDE_OMB,0) - ISNULL(@LoadAnnRSCRUDE_OMB,0))<0.01)
			AND ((@OldAnnRSCRUDE_BB IS NULL AND @LoadAnnRSCRUDE_BB IS NULL) OR ABS(ISNULL(@OldAnnRSCRUDE_BB,0) - ISNULL(@LoadAnnRSCRUDE_BB,0))<0.01)
			AND ((@OldAnnRSPROD_RAIL IS NULL AND @LoadAnnRSPROD_RAIL IS NULL) OR ABS(ISNULL(@OldAnnRSPROD_RAIL,0) - ISNULL(@LoadAnnRSPROD_RAIL,0))<0.01)
			AND ((@OldAnnRSPROD_TT IS NULL AND @LoadAnnRSPROD_TT IS NULL) OR ABS(ISNULL(@OldAnnRSPROD_TT,0) - ISNULL(@LoadAnnRSPROD_TT,0))<0.01)
			AND ((@OldAnnRSPROD_TB IS NULL AND @LoadAnnRSPROD_TB IS NULL) OR ABS(ISNULL(@OldAnnRSPROD_TB,0) - ISNULL(@LoadAnnRSPROD_TB,0))<0.01)
			AND ((@OldAnnRSPROD_OMB IS NULL AND @LoadAnnRSPROD_OMB IS NULL) OR ABS(ISNULL(@OldAnnRSPROD_OMB,0) - ISNULL(@LoadAnnRSPROD_OMB,0))<0.01)
			AND ((@OldAnnRSPROD_BB IS NULL AND @LoadAnnRSPROD_BB IS NULL) OR ABS(ISNULL(@OldAnnRSPROD_BB,0) - ISNULL(@LoadAnnRSPROD_BB,0))<0.01))
			SELECT @UpdateAction = 'U', @RecalcFrom = CASE WHEN @FOMEffDate < @OldEffDate THEN @FOMEffDate ELSE @OldEffDate END
	END

	SELECT @RecalcFrom = MIN(s.PeriodStart)
	FROM Submissions s 
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @RecalcFrom

	UPDATE @dates
	SET UpdateAction = @UpdateAction, RecalcFrom = @RecalcFrom, RecalcTo = @LoadEffUntil
	WHERE EffDate = @EffDate

	IF @UpdateAction IN ('U','D')
		DELETE FROM LoadedEDCStabilizers 
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND EffDate = @FOMEffDate
	IF @UpdateAction IN ('A','U')
	BEGIN
		INSERT INTO LoadedEDCStabilizers (RefineryID, DataSet, EffDate, EffUntil,
			AnnInputBbl, AnnCokeBbl, AnnElecConsMWH,
			AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB,
			AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB)
		VALUES (@RefineryID, @DataSet, @FOMEffDate, @LoadEffUntil, 
			@LoadAnnInputBbl, @LoadAnnCokeBbl, @LoadAnnElecConsMWH,
			@LoadAnnRSCRUDE_RAIL, @LoadAnnRSCRUDE_TT, @LoadAnnRSCRUDE_TB, @LoadAnnRSCRUDE_OMB, @LoadAnnRSCRUDE_BB,
			@LoadAnnRSPROD_RAIL, @LoadAnnRSPROD_TT, @LoadAnnRSPROD_TB, @LoadAnnRSPROD_OMB, @LoadAnnRSPROD_BB)
	END

	FETCH NEXT FROM cDates INTO @EffDate, @FOMEffDate
END
CLOSE cDates
DEALLOCATE cDates


SELECT @RecalcFrom = MIN(RecalcFrom), @RecalcTo = MAX(RecalcTo)
FROM @dates
WHERE UpdateAction IN ('A','D','U')

UPDATE Submissions
SET CalcsNeeded = 'F'
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
AND PeriodStart <= @RecalcTo AND PeriodEnd >= @RecalcFrom
AND ISNULL(CalcsNeeded, '') <> 'F' AND UseSubmission = 1










