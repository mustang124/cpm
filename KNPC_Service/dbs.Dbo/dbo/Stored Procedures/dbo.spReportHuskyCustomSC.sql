﻿CREATE PROC [dbo].[spReportHuskyCustomSC] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE	
	@EII real, @EII_YTD real, @EII_AVG real, 
	@EnergyUseDay real, @EnergyUseDay_YTD real, @EnergyUseDay_AVG real, 
	@TotStdEnergy real, @TotStdEnergy_YTD real, @TotStdEnergy_AVG real, 

	@RefUtilPcnt real, @RefUtilPcnt_YTD real, @RefUtilPcnt_AVG real, 
	@EDC real, @EDC_YTD real, @EDC_AVG real, 
/*	
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_AVG real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_AVG real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_AVG real, 
*/	
	@MechAvail real, @MechAvail_YTD real, @MechAvail_AVG real, 
	@MechUnavailTA real, @MechUnavailTA_YTD real, @MechUnavailTA_AVG real, 
	@NonTAUnavail real, @NonTAUnavail_YTD real, @NonTAUnavail_AVG real, 

	@PersIndex real, @PersIndex_YTD real, @PersIndex_AVG real, 
	@CompWHr real, @CompWHr_YTD real, @CompWHr_AVG real,
	@ContWHr real, @ContWHr_YTD real, @ContWHr_AVG real,

	@MaintIndex real, @MaintIndex_YTD real, @MaintIndex_AVG real, 
	@TAAdj real, @TAAdj_YTD real, @TAAdj_AVG real,
	@AnnTACost real, @AnnTACost_YTD real, @AnnTACost_AVG real, 
	@RoutCost real, @RoutCost_YTD real, @RoutCost_AVG real, 

	@NEOpexUEDC real, @NEOpexUEDC_YTD real, @NEOpexUEDC_AVG real, 
	@NEOpex real, @NEOpex_YTD real, @NEOpex_AVG real, 

	@OpexUEDC real, @OpexUEDC_YTD real, @OpexUEDC_AVG real,
	@EnergyCost real, @EnergyCost_YTD real, @EnergyCost_AVG real, 
	@TotCashOpex real, @TotCashOpex_YTD real, @TotCashOpex_AVG real,
	@UEDC real, @UEDC_YTD real, @UEDC_AVG real
DECLARE @EII_Target real, @RefUtilPcnt_Target real, @MechAvail_Target real, @MaintIndex_Target real, @PersIndex_Target real, @NEOpexUEDC_Target real, @OpexUEDC_Target real
	
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)

SELECT @EII = NULL, @EII_YTD = NULL, @EII_AVG = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_YTD = NULL, @EnergyUseDay_AVG = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_YTD = NULL, @TotStdEnergy_AVG = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_YTD = NULL, @RefUtilPcnt_AVG = NULL, 
	@EDC = NULL, @EDC_YTD = NULL, @EDC_AVG = NULL, 
	@UEDC = NULL, @UEDC_YTD = NULL, @UEDC_AVG = NULL, 
	@MechAvail = NULL, @MechAvail_YTD = NULL, @MechAvail_AVG = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_YTD = NULL, @MechUnavailTA_AVG = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_YTD = NULL, @NonTAUnavail_AVG = NULL, 
	@RoutCost = NULL, @RoutCost_YTD = NULL, @RoutCost_AVG = NULL, 
	@PersIndex = NULL, @PersIndex_YTD = NULL, @PersIndex_AVG = NULL, 
	@CompWHr = NULL, @CompWHr_YTD = NULL, @CompWHr_AVG = NULL,
	@ContWHr = NULL, @ContWHr_YTD = NULL, @ContWHr_AVG = NULL,
	@NEOpexUEDC = NULL, @NEOpexUEDC_YTD = NULL, @NEOpexUEDC_AVG = NULL, 
	@NEOpex = NULL, @NEOpex_YTD = NULL, @NEOpex_AVG = NULL, 
	@OpexUEDC = NULL, @OpexUEDC_YTD = NULL, @OpexUEDC_AVG = NULL, 
	@TAAdj = NULL, @TAAdj_YTD = NULL, @TAAdj_AVG = NULL,
	@EnergyCost = NULL, @EnergyCost_YTD = NULL, @EnergyCost_AVG = NULL, 
	@TotCashOpex = NULL, @TotCashOpex_YTD = NULL, @TotCashOpex_AVG = NULL,
	@EII_Target = NULL, @RefUtilPcnt_Target = NULL, @MechAvail_Target = NULL, @MaintIndex_Target = NULL, @PersIndex_Target = NULL, @NEOpexUEDC_Target = NULL, @OpexUEDC_Target = NULL
	
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
DECLARE @NumDays24Mo real, @NumDays12Mo real
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)

--- Everything Already Available in Gensum
SELECT	@RefUtilPcnt = UtilPcnt, @RefUtilPcnt_YTD = UtilPcnt_YTD, @RefUtilPcnt_AVG = UtilPcnt_AVG,
	@MechAvail = MechAvail, @MechAvail_YTD = MechAvail_YTD, @MechAvail_AVG = MechAvail_AVG,
	@EII = EII, @EII_YTD = EII_YTD, @EII_AVG = EII_AVG,
	@PersIndex = TotWHrEDC, @PersIndex_YTD = TotWHrEDC_YTD, @PersIndex_AVG = TotWHrEDC_AVG,
	@EDC = EDC/1000, @EDC_YTD = EDC_YTD/1000, @EDC_AVG = EDC_AVG/1000,
	@UEDC = UEDC/1000, @UEDC_YTD = UEDC_YTD/1000, @UEDC_AVG = UEDC_AVG/1000,
	@MaintIndex = RoutIndex + TAIndex_AVG, @MaintIndex_YTD = MaintIndex_YTD, @MaintIndex_AVG = MaintIndex_AVG,
	@NEOpexUEDC = NEOpexUEDC, @NEOpexUEDC_YTD = NEOpexUEDC_YTD, @NEOpexUEDC_AVG = NEOpexUEDC_AVG,
	@EII_Target = EII_Target, @RefUtilPcnt_Target = UtilPcnt_Target, @MechAvail_Target = MechAvail_Target, @MaintIndex_Target = MaintIndex_Target, 
	@PersIndex_Target = TotWHrEDC_Target, @NEOpexUEDC_Target = NEOpexUEDC_Target, 
	@OpexUEDC = TotCashOpexUEDC, @OpexUEDC_YTD = TotCashOpexUEDC_YTD, @OpexUEDC_AVG = TotCashOpexUEDC_AVG, @OpexUEDC_Target = TotCashOpexUEDC_Target
FROM Gensum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_YTD = MechUnavailTA_Ann, @MechUnavailTA_AVG = MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet
/*
SELECT @MechUnavailTA_YTD=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet
*/
SELECT @MechUnavailTA_AVG=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail = 100 - @MechAvail - @MechUnavailTA, @NonTAUnavail_YTD = 100 - @MechAvail_YTD - @MechUnavailTA_YTD, @NonTAUnavail_AVG = 100 - @MechAvail_AVG - @MechUnavailTA_AVG
IF @NonTAUnavail < 0.05
	SET @NonTAUnavail = 0
IF @NonTAUnavail_YTD < 0.05
	SET @NonTAUnavail_YTD = 0
IF @NonTAUnavail_AVG < 0.05
	SET @NonTAUnavail_AVG = 0
	
SELECT @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet
/*
SELECT @EnergyUseDay_YTD = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_YTD = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet
*/
SELECT @EnergyUseDay_AVG = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_AVG = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@NumDays12Mo = SUM(s.NumDays)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @CompWHr = CompWHr+GAWHr, @ContWHr = ContWHr
FROM PersST WHERE SubmissionID = @SubmissionID AND SectionID = 'TP'
/*
SELECT @CompWHr_YTD = SUM(CompWHr+GAWHr), @ContWHr_YTD = SUM(ContWHr)
FROM PersST p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.SectionID = 'TP'
*/
SELECT @CompWHr_AVG = SUM(CompWHr+GAWHr), @ContWHr_AVG = SUM(ContWHr)
FROM PersST p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.SectionID = 'TP'

SELECT @AnnTACost = AllocAnnTACost, @RoutCost = CurrRoutCost
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency
/*
SELECT @AnnTACost_YTD = SUM(AllocAnnTACost), @RoutCost_YTD = SUM(CurrRoutCost)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency
*/
SELECT @RoutCost_AVG = SUM(CurrRoutCost), @NumDays24Mo = SUM(s.NumDays)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency

SELECT @NEOpex = NEOpex*Divisor, @EnergyCost = EnergyCost*Divisor, @TotCashOpex = TotCashOpex*Divisor, @TAAdj = TAAdj*Divisor
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

/*
SELECT @NEOpex_YTD = SUM(NEOpex*Divisor), @EnergyCost_YTD = SUM(EnergyCost*Divisor)
	, @TotCashOpex_YTD = SUM(TotCashOpex*Divisor), @TAAdj_YTD = SUM(TAAdj*Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpexUEDC_YTD = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'
*/
SELECT @NEOpex_AVG = SUM(NEOpex*Divisor), @EnergyCost_AVG = SUM(EnergyCost*Divisor), @TAAdj_AVG = SUM(TAAdj*Divisor)
	, @TotCashOpex_AVG = SUM(TotCashOpex*Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'
/*
SELECT @OpexUEDC_AVG = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'
*/
IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_YTD = @EnergyUseDay_YTD * 1.055, @EnergyUseDay_AVG = @EnergyUseDay_AVG * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_YTD = @TotStdEnergy_YTD * 1.055, @TotStdEnergy_AVG = @TotStdEnergy_AVG * 1.055

SELECT 
	@AnnTACost = @AnnTACost/365.25, 
	@RoutCost = @RoutCost/@NumDays, @RoutCost_AVG = @RoutCost_AVG/@NumDays24Mo, 
	@CompWHr = @CompWHr/@NumDays, @CompWHr_AVG = @CompWHr_AVG/@NumDays12Mo, 
	@ContWHr = @ContWHr/@NumDays, @ContWHr_AVG = @ContWHr_AVG/@NumDays12Mo, 
	@NEOpex = @NEOpex/@NumDays, @NEOpex_AVG = @NEOpex_AVG/@NumDays12Mo, 
	@EnergyCost = @EnergyCost/@NumDays, @EnergyCost_AVG = @EnergyCost_AVG/@NumDays12Mo, 
	@TotCashOpex = @TotCashOpex/@NumDays, @TotCashOpex_AVG = @TotCashOpex_AVG/@NumDays12Mo
SELECT @AnnTACost_AVG = @AnnTACost

DECLARE @EII_Study real, @EII_StudyP1 real, @EnergyUseDay_Study real, @EnergyUseDay_StudyP1 real, @TotStdEnergy_Study real, @TotStdEnergy_StudyP1 real
DECLARE @RefUtilPcnt_Study real, @RefUtilPcnt_StudyP1 real, @EDC_Study real, @EDC_StudyP1 real, @UtilUEDC_Study real, @UtilUEDC_StudyP1 real
DECLARE @MechAvail_Study real, @MechAvail_StudyP1 real, @MechUnavailTA_Study real, @MechUnavailTA_StudyP1 real, @NonTAUnavail_Study real, @NonTAUnavail_StudyP1 real
DECLARE @MaintIndex_Study real, @MaintIndex_StudyP1 real, @TAAdj_Study real, @TAAdj_StudyP1 real, @RoutCost_Study real, @RoutCost_StudyP1 real
DECLARE @PersIndex_Study real, @PersIndex_StudyP1 real, @CompWHr_Study real, @CompWHr_StudyP1 real, @ContWHr_Study real, @ContWHr_StudyP1 real
DECLARE @NEOpexUEDC_Study real, @NEOpexUEDC_StudyP1 real, @NEOpex_Study real, @NEOpex_StudyP1 real, @UEDC_Study real, @UEDC_StudyP1 real
DECLARE @OpexUEDC_Study real, @OpexUEDC_StudyP1 real, @EnergyCost_Study real, @EnergyCost_StudyP1 real, @TotCashOpex_Study real, @TotCashOpex_StudyP1 real

-- Get study values
SELECT @EII_Study = StudyValue, @EII_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EII'
SELECT @EnergyUseDay_Study = StudyValue, @EnergyUseDay_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EnergyUseDay'
SELECT @TotStdEnergy_Study = StudyValue, @TotStdEnergy_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotStdEnergy'
SELECT @RefUtilPcnt_Study = StudyValue, @RefUtilPcnt_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'RefUtilPcnt'
SELECT @EDC_Study = StudyValue, @EDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EDC'
SELECT @UtilUEDC_Study = StudyValue, @UtilUEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'UEDC_Util'
SELECT @MechAvail_Study = StudyValue, @MechAvail_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MechAvail'
SELECT @MechUnavailTA_Study = StudyValue, @MechUnavailTA_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MechUnavailTA'
SELECT @NonTAUnavail_Study = 100 - @MechAvail_Study - @MechUnavailTA_Study, @NonTAUnavail_StudyP1 = 100 - @MechAvail_StudyP1 - @MechUnavailTA_StudyP1
SELECT @MaintIndex_Study = StudyValue, @MaintIndex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MaintIndex'
SELECT @TAAdj_Study = StudyValue, @TAAdj_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'AnnTACostK$/Day'
SELECT @RoutCost_Study = StudyValue, @RoutCost_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'AnnRoutCostK$/Day'
SELECT @PersIndex_Study = StudyValue, @PersIndex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'PersIndex'
SELECT @CompWHr_Study = StudyValue, @CompWHr_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'CompWHr/Day'
SELECT @ContWHr_Study = StudyValue, @ContWHr_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'ContWHr/Day'
SELECT @NEOpexUEDC_Study = StudyValue, @NEOpexUEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'NEOpexUEDC'
SELECT @NEOpex_Study = StudyValue, @NEOpex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'NEOpexK$/Day'
SELECT @UEDC_Study = StudyValue, @UEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'UEDC_Opex'
SELECT @OpexUEDC_Study = StudyValue, @OpexUEDC_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotCashOpexUEDC'
SELECT @EnergyCost_Study = StudyValue, @EnergyCost_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EnergyCostK$/Day'
SELECT @TotCashOpex_Study = StudyValue, @TotCashOpex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryId = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotCashOpexK$/Day'

SELECT 
	EII = @EII, EII_AVG = @EII_AVG, EII_Study = @EII_Study, EII_StudyP1 = @EII_StudyP1, EII_Target = @EII_Target,
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_AVG = @EnergyUseDay_AVG, EnergyUseDay_Study = @EnergyUseDay_Study, EnergyUseDay_StudyP1 = @EnergyUseDay_StudyP1, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_AVG = @TotStdEnergy_AVG, TotStdEnergy_Study = @TotStdEnergy_Study, TotStdEnergy_StudyP1 = @TotStdEnergy_StudyP1, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_AVG = @RefUtilPcnt_AVG, UtilPcnt_Study = @RefUtilPcnt_Study, UtilPcnt_StudyP1 = @RefUtilPcnt_StudyP1, UtilPcnt_Target = @RefUtilPcnt_Target,
	EDC = @EDC, EDC_AVG = @EDC_AVG, EDC_Study = @EDC_Study, EDC_StudyP1 = @EDC_StudyP1,
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_AVG = @EDC_AVG*@RefUtilPcnt_AVG/100, UtilUEDC_Study = @UtilUEDC_Study, UtilUEDC_StudyP1 = @UtilUEDC_StudyP1,
	
	MechAvail = @MechAvail, MechAvail_AVG = @MechAvail_AVG, MechAvail_Study = @MechAvail_Study, MechAvail_StudyP1 = @MechAvail_StudyP1, MechAvail_Target = @MechAvail_Target,
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_AVG = @MechUnavailTA_AVG, MechUnavailTA_Study = @MechUnavailTA_Study, MechUnavailTA_StudyP1 = @MechUnavailTA_StudyP1,
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_AVG = @NonTAUnavail_AVG, NonTAUnavail_Study = @NonTAUnavail_Study, NonTAUnavail_StudyP1 = @NonTAUnavail_StudyP1,

	MaintIndex = @MaintIndex, MaintIndex_AVG = @MaintIndex_AVG, MaintIndex_Study = @MaintIndex_Study, MaintIndex_StudyP1 = @MaintIndex_StudyP1, MaintIndex_Target = @MaintIndex_Target,
	TAAdj = @AnnTACost, TAAdj_AVG = @AnnTACost_AVG, TAAdj_Study = @TAAdj_Study, TAAdj_StudyP1 = @TAAdj_StudyP1,
	RoutCost = @RoutCost, RoutCost_AVG = @RoutCost_AVG, RoutCost_Study = @RoutCost_Study, RoutCost_StudyP1 = @RoutCost_StudyP1,

	TotWHrEDC = @PersIndex, TotWHrEDC_AVG = @PersIndex_AVG, TotWHrEDC_Study = @PersIndex_Study, TotWHrEDC_StudyP1 = @PersIndex_StudyP1, TotWHrEDC_Target = @PersIndex_Target,
	CompWHr = @CompWHr, CompWHr_AVG = @CompWHr_AVG, CompWHr_Study = @CompWHr_Study, CompWHr_StudyP1 = @CompWHr_StudyP1,
	ContWHr = @ContWHr, ContWHr_AVG = @ContWHr_AVG, ContWHr_Study = @ContWHr_Study, ContWHr_StudyP1 = @ContWHr_StudyP1,

	NEOpexUEDC = @NEOpexUEDC, NEOpexUEDC_AVG = @NEOpexUEDC_AVG, NEOpexUEDC_Study = @NEOpexUEDC_Study, NEOpexUEDC_StudyP1 = @NEOpexUEDC_StudyP1, NEOpexUEDC_Target = @NEOpexUEDC_Target,
	NEOpex = @NEOpex, NEOpex_AVG = @NEOpex_AVG, NEOpex_Study = @NEOpex_Study, NEOpex_StudyP1 = @NEOpex_StudyP1,
	UEDC = @UEDC,  UEDC_AVG = @UEDC_AVG, UEDC_Study = @UEDC_Study, UEDC_StudyP1 = @UEDC_StudyP1,

	TotCashOpexUEDC = @OpexUEDC, TotCashOpexUEDC_AVG = @OpexUEDC_AVG, TotCashOpexUEDC_Study = @OpexUEDC_Study, TotCashOpexUEDC_StudyP1 = @OpexUEDC_StudyP1, TotCashOpexUEDC_Target = @OpexUEDC_Target,
	EnergyCost = @EnergyCost, EnergyCost_AVG = @EnergyCost_AVG, EnergyCost_Study = @EnergyCost_Study, EnergyCost_StudyP1 = @EnergyCost_StudyP1,
	TotCashOpex = @TotCashOpex, TotCashOpex_AVG = @TotCashOpex_AVG, TotCashOpex_Study = @TotCashOpex_Study, TotCashOpex_StudyP1 = @TotCashOpex_StudyP1


