﻿CREATE PROCEDURE [dbo].[DS_LoadEDCStabilizers]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT EffDate,AnnInputBbl,AnnCokeBbl,AnnElecConsMWH, 
                  AnnRSCRUDE_RAIL,AnnRSCRUDE_TT,AnnRSCRUDE_TB,AnnRSCRUDE_OMB, 
                  AnnRSCRUDE_BB,AnnRSPROD_RAIL,AnnRSPROD_TT,AnnRSPROD_TB,AnnRSPROD_OMB,
                  AnnRSPROD_BB 
                  FROM LoadedEDCStabilizers 
                  WHERE RefineryID = @RefineryID
END

