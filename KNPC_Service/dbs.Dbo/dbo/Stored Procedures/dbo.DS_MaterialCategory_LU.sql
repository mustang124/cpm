﻿CREATE PROC [dbo].[DS_MaterialCategory_LU]
	
AS

SELECT RTRIM(Category) as Category, RTRIM(CategoryName) as CategoryName 
FROM MaterialCategory_LU 
WHERE FuelsInput = 1
ORDER BY SortKey

