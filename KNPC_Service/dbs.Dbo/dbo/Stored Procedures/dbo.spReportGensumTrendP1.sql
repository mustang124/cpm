﻿CREATE     PROC [dbo].[spReportGensumTrendP1] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
	DECLARE @Month smallint, @Year smallint
	IF @PeriodMonth = 1 
		SELECT @Month = 12, @Year = @PeriodYear - 1
	ELSE
		SELECT @Month = @PeriodMonth - 1, @Year = @PeriodYear
	
	EXEC dbo.spReportGensum @RefineryID, @Year, @Month, @DataSet, @FactorSet, @Scenario, @Currency, @UOM, @IncludeTarget, @IncludeYTD, @IncludeAvg

