﻿CREATE         PROC [dbo].[spEnergyCalc] (@SubmissionID int)
AS
SET NOCOUNT ON 
DELETE FROM EnergyCons WHERE SubmissionID = @SubmissionID
DELETE FROM EnergyCost WHERE SubmissionID = @SubmissionID
DELETE FROM EnergyTot WHERE SubmissionID = @SubmissionID
DECLARE @RptCurrency CurrencyCode
SELECT @RptCurrency = RptCurrency FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
-- Electricity calculations
DECLARE @MWH float, @TotTherm float, @DistCost float, @MissingGenEff bit
DECLARE @ElecPur float, @ElecPurCost float, @ElecProd float, @ProdNonCombustible float,
	@ElecTransIn float, @ElecTransOut float, @ElecSold float, @AvgGenEff float
DECLARE @TransType varchar(3), @EnergyType varchar(3), @SourceMWH float, @UsageMWH float, @ElecPriceLocal real, @GenEff real
SELECT	@DistCost = 0, @MWH = 0, @TotTherm = 0, @ElecPur = 0, @ElecPurCost = 0, 
	@ElecProd = 0, @ProdNonCombustible = 0, @ElecTransIn = 0, @ElecTransOut = 0, @ElecSold = 0
DECLARE cElec CURSOR
FOR
	SELECT TransType, EnergyType, SourceMWH, UsageMWH, PriceLocal, GenEff
	FROM Electric
	WHERE (SubmissionID = @SubmissionID) AND TransType <> 'CON'
	AND (SourceMWH > 0 OR UsageMWH > 0)
OPEN cElec
FETCH NEXT FROM cElec INTO @TransType, @EnergyType, @SourceMWH, @UsageMWH, @ElecPriceLocal, @GenEff
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @TransType = 'PUR'
		SELECT @ElecPur = @ElecPur + ISNULL(@SourceMWH, 0), @ElecPurCost = @ElecPurCost + ISNULL(@SourceMWH * @ElecPriceLocal/100, 0)
	IF @TransType = 'PRO'
	BEGIN
		IF @EnergyType = 'ENC'
			SELECT @ProdNonCombustible = @ProdNonCombustible + ISNULL(@SourceMWH, 0)
		ELSE
		BEGIN
			SELECT @ElecProd = @ElecProd + ISNULL(@SourceMWH, 0)
	        	IF @GenEff > 0 
				SELECT 	@MWH = @MWH + ISNULL(@SourceMWH, 0), 
					@TotTherm = @TotTherm + ISNULL((@GenEff * @SourceMWH), 0)
			ELSE
				SELECT @MissingGenEff = 1
		END
	END
	IF @TransType = 'DST'
	BEGIN
		SELECT 	@ElecTransIn = @ElecTransIn + ISNULL(@SourceMWH, 0),
			@ElecTransOut = @ElecTransOut + ISNULL(@UsageMWH, 0)
		IF @SourceMWH > 0
			SELECT @DistCost = @DistCost + ISNULL(@SourceMWH * @ElecPriceLocal/100, 0)
	END
	IF @TransType = 'SOL'
        	SELECT @ElecSold = @ElecSold + ISNULL(@UsageMWH, 0)
	FETCH NEXT FROM cElec INTO @TransType, @EnergyType, @SourceMWH, @UsageMWH, @ElecPriceLocal, @GenEff
END
CLOSE cElec
DEALLOCATE cElec
/* Calculate an average purchase price */
/* in the event that there was only electricity distributed in. */
DECLARE @ElecPurPrice real
IF @ElecPur > 0 
	SELECT @ElecPurPrice = @ElecPurCost*100/@ElecPur
ELSE 
BEGIN
	IF @ElecTransIn > 0 
		SELECT @ElecPurPrice = @DistCost*100/@ElecTransIn
	ELSE
		SELECT @ElecPurPrice = 0
END
/* If an efficiency was not reported */
/* then set average efficiency to BTUpKWH */
IF @MissingGenEff = 1 
	SELECT @AvgGenEff = 9090
ELSE
BEGIN
	IF @MWH = 0 
		SELECT @AvgGenEff = 0
	ELSE 
		SELECT @AvgGenEff = @TotTherm/@MWH
END
DECLARE @PurPowerConsMWH float, @ProdPowerConsMWH float, @TotProdMWH float, @ElecSoldOrTrans float
SELECT 	@PurPowerConsMWH = @ElecPur + @ElecTransIn,
	@TotProdMWH = @ElecProd + @ProdNonCombustible,
	@ElecSoldOrTrans = @ElecTransOut + @ElecSold
SELECT	@ProdPowerConsMWH = @TotProdMWH - @ElecSoldOrTrans
IF @ProdPowerConsMWH < 0
BEGIN
	SELECT @PurPowerConsMWH = @PurPowerConsMWH + @ProdPowerConsMWH
	SELECT @ProdPowerConsMWH = 0
END
DECLARE @PurPowerAdj real, @PurElecCostKLocal real, @ProdPowerAdj real
SELECT @PurPowerAdj = (@ElecPur + @ElecTransIn) * 9.09
IF (@AvgGenEff < 9090) AND (@ElecProd > 0)
BEGIN
	IF (@ElecPur + @ElecTransIn) < (@ElecSold + @ElecTransOut)
		SELECT @PurPowerAdj = @PurPowerAdj - (@ElecPur + @ElecTransIn) * (9090 - @AvgGenEff)/1000
	ELSE
    		SELECT @PurPowerAdj = @PurPowerAdj - (@ElecSold + @ElecTransOut) * (9090 - @AvgGenEff)/1000
END
IF @PurPowerAdj <= 0
	SELECT @PurElecCostKLocal = 0
ELSE
	SELECT @PurElecCostKLocal = @PurPowerAdj * (@ElecPurPrice*10/9090)
IF @AvgGenEff > 9090 
	SELECT @ProdPowerAdj = (@ElecSold + @ElecTransOut) * (@AvgGenEff-9090)/1000
ELSE
	SELECT @ProdPowerAdj = 0
DECLARE @PurElecWasSold bit, @PurElecWasTransferred bit
SELECT @PurElecWasSold = 0, @PurElecWasTransferred = 0
IF @ElecSold > @TotProdMWH
	SET @PurElecWasSold = 1
IF (@TotProdMWH - @ElecSold) < @ElecTransOut
	SET @PurElecWasTransferred = 1
-- Check for GE records in Energy
IF @ElecProd > 0 AND @ElecSoldOrTrans > 0 -- Electricity was generated from thermal sources and was sold/transferred out of the refinery
BEGIN
	IF NOT EXISTS (SELECT * FROM Energy WHERE SubmissionId = @SubmissionID AND EnergyType = 'GE') -- No data was provided. Assume is data provided then it is correct.
	BEGIN
		DECLARE @ElecProdLeft float, @UnitConvRatio real, @TransCode smallint
		DECLARE @MBTU float
		SELECT @ElecProdLeft = @ElecProd
		SELECT @UnitConvRatio = ABS(RptSource/SourceMBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND SourceMBTU > 0 AND TransType In ('PRO','PUR')
		SELECT @TransCode = MAX(TransCode) FROM Energy WHERE SubmissionID = @SubmissionID
		IF @ElecSold > 0
		BEGIN
			SELECT @TransCode = ISNULL(@TransCode, 0) + 1, @MWH = CASE WHEN @ElecSold > @ElecProd THEN @ElecProd ELSE @ElecSold END
			SELECT @MBTU = @MWH * @AvgGenEff/1000, @ElecProdLeft = @ElecProdLeft - @MWH
			INSERT INTO Energy (SubmissionID, TransCode, EnergyType, TransType, TransferTo, UsageMBTU, RptSource)
			SELECT @SubmissionID, @TransCode, 'GE', 'SOL', 'OTH', @MBTU, @MBTU * @UnitConvRatio
		END
		IF @ElecTransOut > 0 AND @ElecProdLeft > 0
		BEGIN
			SELECT @TransCode = ISNULL(@TransCode, 0) + 1, @MWH = CASE WHEN @ElecTransOut > @ElecProd THEN @ElecProd ELSE @ElecTransOut END
			SELECT @MBTU = @MWH * @AvgGenEff/1000, @ElecProdLeft = @ElecProdLeft - @MWH
			INSERT INTO Energy (SubmissionID, TransCode, EnergyType, TransType, TransferTo, UsageMBTU, RptSource)
			SELECT @SubmissionID, @TransCode, 'GE', 'DST', 'AFF', @MBTU, @MBTU * @UnitConvRatio
		END
	END
END
INSERT INTO EnergyCons (SubmissionID, TransType, EnergyType, MBTU)
SELECT @SubmissionID, 'CPU', 'PT', @PurPowerAdj
INSERT INTO EnergyCost (SubmissionID, Scenario, Currency, TransType, EnergyType, CostMBTU)
SELECT @SubmissionID, 'CLIENT', @RptCurrency, 'CPU', 'PT', CASE WHEN @PurPowerAdj > 0 THEN @PurElecCostKLocal*1000/@PurPowerAdj ELSE 0 END
INSERT INTO EnergyCons (SubmissionID, TransType, EnergyType, MBTU)
SELECT @SubmissionID, 'CPR', 'PT', @ProdPowerAdj
INSERT INTO EnergyCost (SubmissionID, Scenario, Currency, TransType, EnergyType, CostMBTU)
SELECT @SubmissionID, 'CLIENT', @RptCurrency, 'CPR', 'PT', NULL
/*
IF (@ElecTransOut + @ElecSold) > (@ElecProd + @ProdNonCombustible) 
	SELECT 	@PurPowerConsMWH = (@ElecPur + @ElecTransIn) - ((@ElecTransOut + @ElecSold) - (@ElecProd + @ProdNonCombustible)),
		@ProdPowerConsMWH = 0
ELSE
	SELECT	@PurPowerConsMWH = (@ElecPur + @ElecTransIn),
		@ProdPowerConsMWH = (@ElecProd + @ProdNonCombustible) - (@ElecTransOut + @ElecSold)
*/
-- Thermal Energy Calculations
DECLARE @StmSource float, @PurFuelLessSpecial float, @PurSpecial float, @Prod float
DECLARE @StmToAff float, @StmToOthers float, @ThermalToAff float, @ThermalToOthers float
SELECT @StmSource = SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND EnergyType IN ('LLH','STM') AND TransType IN ('PUR', 'DST')
SELECT @PurSpecial = SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType IN ('PUR') AND EnergyType IN ('C2','C3','C4','NAP','DST')
SELECT @PurFuelLessSpecial = SUM(SourceMBTU)
FROM Energy
WHERE (SubmissionID = @SubmissionID AND EnergyType NOT IN ('LLH','STM'))
AND ((TransType = 'PUR' AND EnergyType NOT IN ('C2','C3','C4','NAP','DST','NGH','HPF'))
OR TransType = 'DST')
SELECT @Prod = SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'PRO'
SELECT @StmToAff = SUM(UsageMBTU) 
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND EnergyType IN ('STM','LLH')
SELECT @StmToOthers = SUM(UsageMBTU) 
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType IN ('STM','LLH')
SELECT @ThermalToAff = SUM(UsageMBTU) 
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND EnergyType NOT IN ('STM','LLH')
SELECT @ThermalToOthers = SUM(UsageMBTU) 
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType NOT IN ('STM','LLH')
IF @StmSource IS NULL
	SELECT @StmSource = 0
IF @PurSpecial IS NULL
	SELECT @PurSpecial = 0
IF @PurFuelLessSpecial IS NULL
	SELECT @PurFuelLessSpecial = 0
IF @Prod IS NULL
	SELECT @Prod = 0
IF @StmToAff IS NULL
	SELECT @StmToAff = 0
IF @StmToOthers IS NULL
	SELECT @StmToOthers = 0
IF @ThermalToAff IS NULL
	SELECT @ThermalToAff = 0
IF @ThermalToOthers IS NULL
	SELECT @ThermalToOthers = 0
DECLARE @PurSteamToAff float, @PurSteamToOthers float, @PurFuelToAffAsSteam float, @PurFuelToOthersAsSteam float, @ProdToAffAsSteam float, @ProdToOthersAsSteam float
DECLARE @PurFuelToAff float, @PurFuelToOthers float, @ProdToAff float, @ProdToOthers float
DECLARE @PurSteamCons float, @PurFuelCons float, @ProdCons float
DECLARE @PurSteamLeft float, @PurFuelLeft float, @ProdLeft float
DECLARE @StmToAffLeft float, @StmToOthersLeft float, @FuelToAffLeft float, @FuelToOthersLeft float
SELECT 	@PurSteamLeft = @StmSource, @PurFuelLeft = @PurFuelLessSpecial, @ProdLeft = @Prod,
	@StmToAffLeft = @StmToAff, @StmToOthersLeft = @StmToOthers, 
	@FuelToAffLeft = @ThermalToAff, @FuelToOthersLeft = @ThermalToOthers
SELECT @PurSteamToAff = CASE WHEN @PurSteamLeft < @StmToAffLeft THEN @PurSteamLeft ELSE @StmToAffLeft END
SELECT @PurSteamLeft = @PurSteamLeft - @PurSteamToAff, @StmToAffLeft = @StmToAffLeft - @PurSteamToAff
SELECT @PurSteamToOthers = CASE WHEN @PurSteamLeft < @StmToOthersLeft THEN @PurSteamLeft ELSE @StmToOthersLeft END
SELECT @PurSteamLeft = @PurSteamLeft - @PurSteamToOthers, @StmToOthersLeft = @StmToOthersLeft - @PurSteamToOthers
SELECT @PurFuelToAffAsSteam = CASE WHEN @PurFuelLeft < @StmToAffLeft THEN @PurFuelLeft ELSE @StmToAffLeft END
SELECT @PurFuelLeft = @PurFuelLeft - @PurFuelToAffAsSteam, @StmToAffLeft = @StmToAffLeft - @PurFuelToAffAsSteam
SELECT @PurFuelToOthersAsSteam = CASE WHEN @PurFuelLeft < @StmToOthersLeft THEN @PurFuelLeft ELSE @StmToOthersLeft END
SELECT @PurFuelLeft = @PurFuelLeft - @PurFuelToOthersAsSteam, @StmToOthersLeft = @StmToOthersLeft - @PurFuelToOthersAsSteam
SELECT @ProdToAffAsSteam = CASE WHEN @ProdLeft < @StmToAffLeft THEN @ProdLeft ELSE @StmToAffLeft END
SELECT @ProdLeft = @ProdLeft - @ProdToAffAsSteam, @StmToAffLeft = @StmToAffLeft - @ProdToAffAsSteam
SELECT @ProdToOthersAsSteam = CASE WHEN @ProdLeft < @StmToOthersLeft THEN @ProdLeft ELSE @StmToOthersLeft END
SELECT @ProdLeft = @ProdLeft - @ProdToOthersAsSteam, @StmToOthersLeft = @StmToOthersLeft - @ProdToOthersAsSteam
SELECT @PurFuelToAff = CASE WHEN @PurFuelLeft < @FuelToAffLeft THEN @PurFuelLeft ELSE @FuelToAffLeft END
SELECT @PurFuelLeft = @PurFuelLeft - @PurFuelToAff, @FuelToAffLeft = @FuelToAffLeft - @PurFuelToAff
SELECT @PurFuelToOthers = CASE WHEN @PurFuelLeft < @FuelToOthersLeft THEN @PurFuelLeft ELSE @FuelToOthersLeft END
SELECT @PurFuelLeft = @PurFuelLeft - @PurFuelToOthers, @FuelToOthersLeft = @FuelToOthersLeft - @PurFuelToOthers
SELECT @ProdToAff = CASE WHEN @ProdLeft < @FuelToAffLeft THEN @ProdLeft ELSE @FuelToAffLeft END
SELECT @ProdLeft = @ProdLeft - @ProdToAff, @FuelToAffLeft = @FuelToAffLeft - @ProdToAff
SELECT @ProdToOthers = CASE WHEN @ProdLeft < @FuelToOthersLeft THEN @ProdLeft ELSE @FuelToOthersLeft END
SELECT @ProdLeft = @ProdLeft - @ProdToOthers, @FuelToOthersLeft = @FuelToOthersLeft - @ProdToOthers
SELECT @PurSteamCons = @PurSteamLeft, @PurFuelCons = @PurFuelLeft + @PurSpecial, @ProdCons = @ProdLeft
--SELECT @PurSteamCons, @PurFuelCons, @ProdCons
-- Purchased Steam Consumed
IF @PurSteamLeft > 0 
	INSERT INTO EnergyCons (SubmissionID, TransType, EnergyType, MBTU)
	SELECT @SubmissionID, 'CPU', 'STM', @PurSteamLeft
-- Special Purchases that don't net
INSERT INTO EnergyCons (SubmissionID, TransType, EnergyType, MBTU)
SELECT SubmissionID, 'CPU', EnergyType, SourceMBTU
FROM Energy 
WHERE SubmissionID = @SubmissionID AND SourceMBTU > 0
AND TransType = 'PUR' AND EnergyType IN ('C2','C3','C4','NAP','DST') 
-- Other Purchases reduced by percent of purchased fuel exported
IF @PurFuelLeft > 0
	INSERT INTO EnergyCons (SubmissionID, TransType, EnergyType, MBTU)
	SELECT SubmissionID, 'CPU', EnergyType, SUM(SourceMBTU) * @PurFuelLeft / @PurFuelLessSpecial
	FROM Energy 
	WHERE SubmissionID = @SubmissionID AND SourceMBTU > 0
	AND EnergyType NOT IN ('STM','LLH')
	AND ((TransType = 'PUR' AND EnergyType NOT IN ('C2','C3','C4','NAP','DST','NGH','HPF')) OR (TransType = 'DST' AND TransferTo = 'REF'))
	GROUP BY SubmissionID, EnergyType
-- Produced Fuel reduced by percent of produced fuel exported
IF @ProdLeft > 0
	INSERT INTO EnergyCons (SubmissionID, TransType, EnergyType, MBTU)
	SELECT SubmissionID, 'CPR', EnergyType, SUM(SourceMBTU * @ProdLeft / @Prod)
	FROM Energy 
	WHERE SubmissionID = @SubmissionID AND SourceMBTU > 0 AND TransType = 'PRO'
	GROUP BY SubmissionID, EnergyType
INSERT INTO EnergyCost (SubmissionID, Scenario, Currency, TransType, EnergyType, CostMBTU)
SELECT SubmissionID, 'CLIENT', @RptCurrency, 'CPU', EnergyType, SUM(PriceMBTULocal*SourceMBTU)/SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND SourceMBTU > 0 AND PriceMBTULocal IS NOT NULL AND EnergyType NOT IN ('NGH','HPF')
GROUP BY SubmissionID, EnergyType
INSERT INTO EnergyCost (SubmissionID, Scenario, Currency, TransType, EnergyType, CostMBTU)
SELECT SubmissionID, 'CLIENT', @RptCurrency, 'CPU', EnergyType, SUM(PriceMBTULocal*SourceMBTU)/SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND SourceMBTU > 0 AND PriceMBTULocal IS NOT NULL
AND NOT EXISTS (SELECT * FROM EnergyCost p WHERE p.SubmissionID = @SubmissionID AND p.TransType = 'CPU' AND p.EnergyType = Energy.EnergyType)
GROUP BY SubmissionID, EnergyType
INSERT INTO EnergyCost (SubmissionID, Scenario, Currency, TransType, EnergyType, CostMBTU)
SELECT SubmissionID, 'CLIENT', @RptCurrency, 'CPR', EnergyType, SUM(PriceMBTULocal*SourceMBTU)/SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND SourceMBTU > 0 AND PriceMBTULocal IS NOT NULL
GROUP BY SubmissionID, EnergyType

DECLARE @PurFGMBTU real, @PurLiquidMBTU real, @PurSolidMBTU real, @PurThermMBTU real, @PurTotMBTU real, 
	@ProdFGMBTU real, @ProdOthMBTU real, @ProdTotMBTU real, @TotEnergyConsMBTU real, 
	@TotPowerConsMWH real
SELECT	@PurFGMBTU = SUM(MBTU)
FROM EnergyCons
WHERE SubmissionID = @SubmissionID AND TransType = 'CPU' AND EnergyType IN ('C2', 'FG', 'FG_', 'LBG', 'LCV')
SELECT	@PurSolidMBTU = SUM(MBTU)
FROM EnergyCons
WHERE SubmissionID = @SubmissionID AND TransType = 'CPU' AND EnergyType = 'COK'
SELECT	@PurLiquidMBTU = SUM(MBTU)
FROM EnergyCons
WHERE SubmissionID = @SubmissionID AND TransType = 'CPU' AND EnergyType NOT IN ('C2', 'FG', 'FG_', 'LBG', 'LCV', 'COK', 'STM', 'LLH', 'PT')
SELECT	@ProdFGMBTU = SUM(MBTU)
FROM EnergyCons
WHERE SubmissionID = @SubmissionID AND TransType = 'CPR' AND EnergyType IN ('FG','FG_')
SELECT	@ProdOthMBTU = SUM(MBTU)
FROM EnergyCons
WHERE SubmissionID = @SubmissionID AND TransType = 'CPR' AND EnergyType NOT IN ('FG', 'FG_', 'PT')

SELECT 	@PurFGMBTU = ISNULL(@PurFGMBTU, 0), @PurSolidMBTU = ISNULL(@PurSolidMBTU, 0), @PurLiquidMBTU = ISNULL(@PurLiquidMBTU, 0), 
	@ProdFGMBTU = ISNULL(@ProdFGMBTU, 0), @ProdOthMBTU = ISNULL(@ProdOthMBTU, 0)
SELECT	@PurThermMBTU = @PurSteamLeft + @PurFuelLeft + @PurSpecial
SELECT 	@PurTotMBTU = @PurThermMBTU + ISNULL(@PurPowerAdj, 0),
	@ProdTotMBTU = @ProdFGMBTU + @ProdOthMBTU + ISNULL(@ProdPowerAdj, 0)
SELECT	@TotEnergyConsMBTU = @PurTotMBTU + @ProdTotMBTU,
	@TotPowerConsMWH = @PurPowerConsMWH + @ProdPowerConsMWH
INSERT INTO EnergyTot(SubmissionID, PurFGMBTU, PurLiquidMBTU, PurSolidMBTU, PurSteamMBTU, PurThermMBTU, PurPowerMBTU, PurTotMBTU, ProdFGMBTU, ProdOthMBTU, ProdPowerAdj, ProdTotMBTU, TotEnergyConsMBTU, PurPowerConsMWH, ProdPowerConsMWH, TotPowerConsMWH)
VALUES (@SubmissionID, @PurFGMBTU, @PurLiquidMBTU, @PurSolidMBTU, @PurSteamLeft, @PurThermMBTU, @PurPowerAdj, @PurTotMBTU, 
@ProdFGMBTU, @ProdOthMBTU, @ProdPowerAdj, @ProdTotMBTU, @TotEnergyConsMBTU, 
@PurPowerConsMWH, @ProdPowerConsMWH, @TotPowerConsMWH)
UPDATE EnergyCons
SET PercentOfTotal = 100*(MBTU/@TotEnergyConsMBTU)
WHERE @TotEnergyConsMBTU > 0 AND SubmissionID = @SubmissionID
DELETE FROM Yield
WHERE SubmissionID = @SubmissionID AND MaterialID IN ('M106','RFC','RFS','RFCG')
DECLARE @SortKey int
SELECT @SortKey = MAX(SortKey) FROM Yield WHERE SubmissionID = @SubmissionID
INSERT INTO Yield (SubmissionID, SortKey, Category, MaterialID, MaterialName, Bbl)
SELECT @SubmissionID, @SortKey+1, 'RPF', 'RFC', 'RPF - Consumed in Refinery', @ProdLeft/6.05
INSERT INTO Yield (SubmissionID, SortKey, Category, MaterialID, MaterialName, Bbl)
SELECT @SubmissionID, @SortKey+2, 'RPF', 'RFS', 'RPF - Distributed to Affiliates', @ProdToAff/6.05
INSERT INTO Yield (SubmissionID, SortKey, Category, MaterialID, MaterialName, Bbl)
SELECT @SubmissionID, @SortKey+3, 'RPF', 'RFCG', 'RPF - Cogeneration and Other Sales', (@StmToOthers + @ThermalToOthers)/6.05
INSERT INTO Yield (SubmissionID, SortKey, Category, MaterialID, MaterialName, Bbl)
SELECT @SubmissionID, @SortKey+4, 'RMI', 'M106', 'Purch Energy Consumed for Sales', (@PurFuelToOthersAsSteam+@PurFuelToOthers)/6.05
INSERT INTO EnergyCost (SubmissionID, Scenario, Currency, TransType, EnergyType, ProductRef, CostMBTU, CostBbl, CostMT, CostK)
SELECT e.SubmissionID, e.Scenario, c.Currency, e.TransType, e.EnergyType, e.ProductRef, 
e.CostMBTU*dbo.ExchangeRate(s.RptCurrency, c.Currency, s.PeriodStart), 
e.CostBbl*dbo.ExchangeRate(s.RptCurrency, c.Currency, s.PeriodStart), 
e.CostMT*dbo.ExchangeRate(s.RptCurrency, c.Currency, s.PeriodStart), 
e.CostK*dbo.ExchangeRate(s.RptCurrency, c.Currency, s.PeriodStart)
FROM EnergyCost e INNER JOIN SubmissionsAll s ON s.SubmissionID = e.SubmissionID AND s.RptCurrency = e.Currency
INNER JOIN CurrenciesToCalc c ON c.RefineryID = s.RefineryID
WHERE c.Currency <> s.RptCurrency AND e.SubmissionID = @SubmissionID

-- Gas Compositions
EXEC CalcGasComposition @SubmissionID


