﻿CREATE PROC [dbo].[SS_GetRefTargets]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
	
AS

SELECT ISNULL(RTRIM(r.CurrencyCode),'USD') as CurrencyCode,r.Target,RTRIM(r.Property) As Property,RTRIM(c.SectionHeader) AS SectionHeader,c.SortKey 
             FROM dbo.RefTargets r ,Chart_LU c WHERE c.TargetField=r.Property AND c.Sortkey<800 
              AND  (SubmissionID IN 
             (SELECT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd))))


