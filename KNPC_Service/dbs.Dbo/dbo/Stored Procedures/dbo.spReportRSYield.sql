﻿CREATE PROC [dbo].[spReportRSYield](@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL',
	@FactorSet varchar(8) = '2012', @Scenario varchar(8) = 'CLIENT', @Currency varchar(4) = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

EXEC spReportDynamic @RefineryID, @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM, 7, @IncludeTarget, @IncludeYTD, @IncludeAvg


