﻿CREATE PROC [dbo].[SS_GetInputAbsence]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT  
s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            OCCAbs,MPSAbs,RTRIM(CategoryID) As CategoryID  
            FROM  
            dbo.Absence a 
            ,dbo.Submissions s  
            WHERE   
            a.SubmissionID = s.SubmissionID AND 
            (a.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions 
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1))


