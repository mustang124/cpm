﻿CREATE    PROC [dbo].[spRoutIndex](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @cRout CURSOR VARYING OUTPUT)
AS
SET NOCOUNT ON
DECLARE @tblRout TABLE (
	Currency char(4) NOT NULL,
	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NOT NULL,
	RoutCost real NULL,
	RoutMatl real NULL
)
INSERT INTO @tblRout (Currency, PeriodStart, PeriodEnd, RoutCost, RoutMatl)
SELECT Currency, PeriodStart, PeriodEnd, RoutCost, RoutMatl
FROM MaintRoutHist
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodStart >= @PeriodStart AND PeriodEnd <= @PeriodEnd
SELECT @PeriodStart = MIN(PeriodStart) FROM @tblRout
DECLARE @tblEDC TABLE (
	FactorSet char(8) NOT NULL,
	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NOT NULL,
	PlantEDC real NULL,
	PlantMaintEffDiv real NULL
)
INSERT INTO @tblEDC (FactorSet, PeriodStart, PeriodEnd, PlantEDC, PlantMaintEffDiv)
SELECT f.FactorSet, s.PeriodStart, s.PeriodEnd, f.PlantEDC, f.PlantMaintEffDiv
FROM Submissions s INNER JOIN FactorTotCalc f ON f.SubmissionID = s.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodEnd <= @PeriodEnd
DECLARE @MinStart smalldatetime, @MinStartID int
SELECT TOP 1 @MinStart = PeriodStart, @MinStartID = SubmissionID
FROM Submissions
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
AND EXISTS (SELECT * FROM FactorTotCalc f WHERE f.SubmissionID = Submissions.SubmissionID AND PlantEDC > 0)
ORDER BY PeriodStart ASC
IF @MinStart > @PeriodStart
	INSERT INTO @tblEDC (FactorSet, PeriodStart, PeriodEnd, PlantEDC, PlantMaintEffDiv)
	SELECT FactorSet, @PeriodStart, @MinStart, PlantEDC, PlantMaintEffDiv
	FROM FactorTotCalc WHERE SubmissionID = @MinStartID
--SET NOCOUNT OFF
SET @cRout = CURSOR FORWARD_ONLY STATIC FOR
SELECT e.FactorSet, r.Currency, 
	RoutIndex = r.RoutCost*1000/e.PlantEDC, 
	RoutMatlIndex = r.RoutMatl*1000/e.PlantEDC, 
	RoutEffIndex = r.RoutCost*100000/e.PlantMaintEffDiv
FROM 	(SELECT Currency, RoutCost = SUM(RoutCost), RoutMatl = SUM(RoutMatl) FROM @tblRout GROUP BY Currency) r,
	(SELECT FactorSet, PlantEDC = SUM(PlantEDC*CAST(DATEDIFF(dd, PeriodStart, PeriodEnd) AS real)/365), 
		PlantMaintEffDiv = SUM(PlantMaintEffDiv*CAST(DATEDIFF(dd, PeriodStart, PeriodEnd) AS real)/365)
	 FROM @tblEDC GROUP BY FactorSet) e
OPEN @cRout


