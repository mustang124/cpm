﻿CREATE   PROC [dbo].[spAverageEnergyCost](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
		@Currency CurrencyCode, @Scenario Scenario, @TotCostMBTU real OUTPUT, @PurCostMBTU real OUTPUT, @ProdCostMBTU real OUTPUT,
		@TotCostGJ real OUTPUT, @PurCostGJ real OUTPUT, @ProdCostGJ real OUTPUT)
AS
SET @Scenario = 'CLIENT' -- Using client energy prices for all pricing scenarios

SELECT @TotCostMBTU = CASE WHEN m.TotEnergyConsMBTU > 0 THEN c.TotCostK*1000/m.TotEnergyConsMBTU END,
@PurCostMBTU = CASE WHEN m.PurTotMBTU > 0 THEN c.PurTotCostK*1000/m.PurTotMBTU END,
@ProdCostMBTU = CASE WHEN m.ProdTotMBTU > 0 THEN c.ProdTotCostK*1000/m.ProdTotMBTU END
FROM EnergyTot m INNER JOIN EnergyTotCost c ON c.SubmissionID = m.SubmissionID
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE c.Scenario = @Scenario AND c.Currency = @Currency
AND s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND PeriodStart>=@PeriodStart AND PeriodStart < @PeriodEnd

SELECT @TotCostGJ = @TotCostMBTU/1.055, @PurCostGJ = @PurCostMBTU/1.055, @ProdCostGJ = @ProdCostMBTU/1.055



