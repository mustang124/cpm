﻿CREATE           PROC [dbo].[spCompleteSubmission](@SubmissionID int, @BatchLoading bit = 0)
AS

UPDATE SubmissionsAll
SET 	PeriodStart = dbo.BuildDate(PeriodYear, PeriodMonth, 1),
	PeriodEnd = DATEADD(mm, 1, dbo.BuildDate(PeriodYear, PeriodMonth, 1)),
	RptCurrencyT15 = ISNULL(RptCurrencyT15, RptCurrency)
WHERE SubmissionID = @SubmissionID

UPDATE SubmissionsAll
SET 	NumDays = DATEDIFF(d, PeriodStart, PeriodEnd),
	FractionOfYear = DATEDIFF(d, PeriodStart, PeriodEnd)/365.0
WHERE SubmissionID = @SubmissionID

EXEC spProcessTAs @SubmissionID
EXEC spProcessRoutHist @SubmissionID
EXEC spLoadEDCStabilizers @SubmissionID
EXEC TransferUnitTargets @SubmissionID

IF EXISTS (SELECT * FROM SubmissionsAll WHERE SubmissionID = @SubmissionID AND ClientVersion LIKE 'ProfileLit%')
	UPDATE Yield
	SET MaterialID = 'M154'
	WHERE Category = 'OTHRM' AND SubmissionID = @SubmissionID AND MaterialID = 'M123'

IF EXISTS (SELECT * FROM ProcessData WHERE Property = 'FeedGasRate' AND SubmissionID = @SubmissionID)
	UPDATE ProcessData SET Property = 'FeedRateGas' WHERE Property = 'FeedGasRate' AND SubmissionID = @SubmissionID

UPDATE SubmissionsAll 
SET CalcsNeeded = 'F' , Submitted = getdate()
WHERE SubmissionID = @SubmissionID

EXEC SetUseSubmission @SubmissionID

IF @BatchLoading = 0
BEGIN
	EXEC spClearUploading @SubmissionID
	RAISERROR (50001, 7, 1)
END




