﻿
CREATE PROCEDURE [dbo].[spReportBashneftKPI2](@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON 
SET @FactorSet = '2012'

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
DECLARE @CompAvg bit = 0
IF (@RefineryID IN ('330EUR','331EUR','154FL') AND @DataSet = 'BASHONEREF') OR @RefineryID = '155FL'
	SET @CompAvg = 1

IF @CompAvg = 1
	SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
	FROM dbo.GetSubmission('330EUR', @PeriodYear, @PeriodMonth, 'Actual')
ELSE
	SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
	FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
BEGIN
	--RAISERROR (N'Data has not been uploaded for this month.', -- Message text.
 --          10, -- Severity,
 --          1 --State
 --          );
	RETURN 1
END
ELSE IF @CalcsNeeded IS NOT NULL
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p
DECLARE @StartQ2 smalldatetime, @StartQ3 smalldatetime, @StartQ4 smalldatetime, @EOY smalldatetime
SELECT @StartQ2 = DATEADD(mm, 3, @StartYTD), @StartQ3 = DATEADD(mm, 6, @StartYTD), @StartQ4 = DATEADD(mm, 9, @StartYTD), @EOY = DATEADD(YEAR, 1, @StartYTD)

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListQTR dbo.SubmissionIDList, @SubListQ1 dbo.SubmissionIDList, @SubListQ2 dbo.SubmissionIDList, @SubListQ3 dbo.SubmissionIDList, @SubListQ4 dbo.SubmissionIDList
	, @SubListYTQ2 dbo.SubmissionIDList, @SubListYTQ3 dbo.SubmissionIDList, @SubListYear dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
IF @CompAvg = 1
BEGIN
	DECLARE @Refs TABLE (RefineryID varchar(6) NOT NULL, Dataset varchar(8) NOT NULL)
	INSERT @Refs VALUES ('154FL', 'Fuels'), ('330EUR', 'Actual'), ('331EUR', 'Actual')

	INSERT @SubListMonth SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @PeriodStart, @PeriodEnd)
	INSERT @SubListQTR SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartQTR, @EndQuarter)
	INSERT @SubListQ1 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartYTD, @StartQ2)
	INSERT @SubListQ2 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartQ2, @StartQ3)
	INSERT @SubListQ3 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartQ3, @StartQ4)
	INSERT @SubListQ4 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartQ4, @EOY)
	INSERT @SubListYTQ2 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartYTD, @StartQ3)
	INSERT @SubListYTQ3 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartYTD, @StartQ4)
	INSERT @SubListYear SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @StartYTD, @EOY)
	INSERT @SubList12Mo SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @Start12Mo, @PeriodEnd)
	INSERT @SubList24Mo SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.Dataset, @Start24Mo, @PeriodEnd)
	
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListMonth) WHERE ValidList = 'N')
		RETURN 1
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQTR) WHERE ValidList = 'N')
		DELETE FROM @SubListQTR
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ1) WHERE ValidList = 'N')
		DELETE FROM @SubListQ1
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ2) WHERE ValidList = 'N')
		DELETE FROM @SubListQ2
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ3) WHERE ValidList = 'N')
		DELETE FROM @SubListQ3
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ4) WHERE ValidList = 'N')
		DELETE FROM @SubListQ4
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListYTQ2) WHERE ValidList = 'N')
		DELETE FROM @SubListYTQ2
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListYTQ3) WHERE ValidList = 'N')
		DELETE FROM @SubListYTQ3
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListYear) WHERE ValidList = 'N')
		DELETE FROM @SubListYear
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubList12Mo) WHERE ValidList = 'N')
		DELETE FROM @SubList12Mo
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubList24Mo) WHERE ValidList = 'N')
		DELETE FROM @SubList24Mo

END
ELSE BEGIN
	INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd)
	INSERT @SubListQTR SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartQTR, @EndQuarter)
	INSERT @SubListQ1 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @StartQ2)
	INSERT @SubListQ2 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartQ2, @StartQ3)
	INSERT @SubListQ3 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartQ3, @StartQ4)
	INSERT @SubListQ4 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartQ4, @EOY)
	INSERT @SubListYTQ2 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @StartQ3)
	INSERT @SubListYTQ3 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @StartQ4)
	INSERT @SubListYear SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @EOY)
	INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
	INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd)
END

IF EXISTS (SELECT * FROM Submissions WHERE CalcsNeeded IS NOT NULL AND SubmissionID IN (SELECT SubmissionID FROM @SubList24Mo))
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

SELECT ProcessUtilPcnt = m.ProcessUtilPcnt, ProcessEDC = m.ProcessEDC, ProcessUEDC = m.ProcessUEDC, OffsitesEDC = m.OffsitesEDC, Complexity = m.Complexity
	, EII = m.EII, EnergyUseDay = m.EnergyUseDay, TotStdEnergy = m.TotStdEnergy, FuelUseDay = m.FuelUseDay
	, OpAvail = m.OpAvail, MechUnavailTA = m.MechUnavailTA, NonTAOpUnavail = m.NonTAOpUnavail, TADD = m.TADD, NTAMDD = m.NTAMDD, RPDD = m.RPDD, NumDays = m.NumDays, MechAvail = m.MechAvail, NonTAMechUnavail = m.NonTAMechUnavail
	, MaintEffIndex = m.MaintEffIndex, RoutCost = m.RoutCost, AnnTACost = m.AnnTACost, MaintEffDiv = m.MaintEffDiv, TAIndex = m.TAIndex
	, mPersEffIndex = m.mPersEffIndex, MaintWHr = m.MaintWHr, OCCMaintWHr = m.OCCMaintWHr, MPSMaintWHr = m.MPSMaintWHr, mPersEffDiv = m.mPersEffDiv
	, TotCashOpexUEDC = m.TotCashOpexUEDC, TAAdj = m.TAAdj, EnergyCost = m.EnergyCost, TotCashOpex = m.TotCashOpex, UEDC = m.UEDC
	, NEOpexEDC = m.NEOpexEDC, NEOpex = m.NEOpex, EDC = m.EDC, ExchRate = m.ExchRate
	, ProcessUtilPcnt_QTR = q.ProcessUtilPcnt, ProcessEDC_QTR = q.ProcessEDC, ProcessUEDC_QTR = q.ProcessUEDC, OffsitesEDC_QTR = q.OffsitesEDC, Complexity_QTR = q.Complexity
	, EII_QTR = q.EII, EnergyUseDay_QTR = q.EnergyUseDay, TotStdEnergy_QTR = q.TotStdEnergy, FuelUseDay_QTR = q.FuelUseDay
	, OpAvail_QTR = q.OpAvail, MechUnavailTA_QTR = q.MechUnavailTA, NonTAOpUnavail_QTR = q.NonTAOpUnavail, TADD_QTR = q.TADD, NTAMDD_QTR = q.NTAMDD, RPDD_QTR = q.RPDD, NumDays_QTR = q.NumDays, MechAvail_QTR = q.MechAvail, NonTAMechUnavail_QTR = q.NonTAMechUnavail
	, MaintEffIndex_QTR = q.MaintEffIndex, RoutCost_QTR = q.RoutCost, AnnTACost_QTR = q.AnnTACost, MaintEffDiv_QTR = q.MaintEffDiv, TAIndex_QTR = q.TAIndex
	, mPersEffIndex_QTR = q.mPersEffIndex, MaintWHr_QTR = q.MaintWHr, OCCMaintWHr_QTR = q.OCCMaintWHr, MPSMaintWHr_QTR = q.MPSMaintWHr, mPersEffDiv_QTR = q.mPersEffDiv
	, TotCashOpexUEDC_QTR = q.TotCashOpexUEDC, TAAdj_QTR = q.TAAdj, EnergyCost_QTR = q.EnergyCost, TotCashOpex_QTR = q.TotCashOpex, UEDC_QTR = q.UEDC
	, NEOpexEDC_QTR = q.NEOpexEDC, NEOpex_QTR = q.NEOpex, EDC_QTR = q.EDC, ExchRate_QTR = q.ExchRate
	, ProcessUtilPcnt_Q1 = q1.ProcessUtilPcnt, ProcessEDC_Q1 = q1.ProcessEDC, ProcessUEDC_Q1 = q1.ProcessUEDC, OffsitesEDC_Q1 = q1.OffsitesEDC, Complexity_Q1 = q1.Complexity
	, EII_Q1 = q1.EII, EnergyUseDay_Q1 = q1.EnergyUseDay, TotStdEnergy_Q1 = q1.TotStdEnergy, FuelUseDay_Q1 = q1.FuelUseDay
	, OpAvail_Q1 = q1.OpAvail, MechUnavailTA_Q1 = q1.MechUnavailTA, NonTAOpUnavail_Q1 = q1.NonTAOpUnavail, TADD_Q1 = q1.TADD, NTAMDD_Q1 = q1.NTAMDD, RPDD_Q1 = q1.RPDD, NumDays_Q1 = q1.NumDays, MechAvail_Q1 = q1.MechAvail, NonTAMechUnavail_Q1 = q1.NonTAMechUnavail
	, MaintEffIndex_Q1 = q1.MaintEffIndex, RoutCost_Q1 = q1.RoutCost, AnnTACost_Q1 = q1.AnnTACost, MaintEffDiv_Q1 = q1.MaintEffDiv, TAIndex_Q1 = q1.TAIndex
	, mPersEffIndex_Q1 = q1.mPersEffIndex, MaintWHr_Q1 = q1.MaintWHr, OCCMaintWHr_Q1 = q1.OCCMaintWHr, MPSMaintWHr_Q1 = q1.MPSMaintWHr, mPersEffDiv_Q1 = q1.mPersEffDiv
	, TotCashOpexUEDC_Q1 = q1.TotCashOpexUEDC, TAAdj_Q1 = q1.TAAdj, EnergyCost_Q1 = q1.EnergyCost, TotCashOpex_Q1 = q1.TotCashOpex, UEDC_Q1 = q1.UEDC
	, NEOpexEDC_Q1 = q1.NEOpexEDC, NEOpex_Q1 = q1.NEOpex, EDC_Q1 = q1.EDC, ExchRate_Q1 = q1.ExchRate
	, ProcessUtilPcnt_Q2 = q2.ProcessUtilPcnt, ProcessEDC_Q2 = q2.ProcessEDC, ProcessUEDC_Q2 = q2.ProcessUEDC, OffsitesEDC_Q2 = q2.OffsitesEDC, Complexity_Q2 = q2.Complexity
	, EII_Q2 = q2.EII, EnergyUseDay_Q2 = q2.EnergyUseDay, TotStdEnergy_Q2 = q2.TotStdEnergy, FuelUseDay_Q2 = q2.FuelUseDay
	, OpAvail_Q2 = q2.OpAvail, MechUnavailTA_Q2 = q2.MechUnavailTA, NonTAOpUnavail_Q2 = q2.NonTAOpUnavail, TADD_Q2 = q2.TADD, NTAMDD_Q2 = q2.NTAMDD, RPDD_Q2 = q2.RPDD, NumDays_Q2 = q2.NumDays, MechAvail_Q2 = q2.MechAvail, NonTAMechUnavail_Q2 = q2.NonTAMechUnavail
	, MaintEffIndex_Q2 = q2.MaintEffIndex, RoutCost_Q2 = q2.RoutCost, AnnTACost_Q2 = q2.AnnTACost, MaintEffDiv_Q2 = q2.MaintEffDiv, TAIndex_Q2 = q2.TAIndex
	, mPersEffIndex_Q2 = q2.mPersEffIndex, MaintWHr_Q2 = q2.MaintWHr, OCCMaintWHr_Q2 = q2.OCCMaintWHr, MPSMaintWHr_Q2 = q2.MPSMaintWHr, mPersEffDiv_Q2 = q2.mPersEffDiv
	, TotCashOpexUEDC_Q2 = q2.TotCashOpexUEDC, TAAdj_Q2 = q2.TAAdj, EnergyCost_Q2 = q2.EnergyCost, TotCashOpex_Q2 = q2.TotCashOpex, UEDC_Q2 = q2.UEDC
	, NEOpexEDC_Q2 = q2.NEOpexEDC, NEOpex_Q2 = q2.NEOpex, EDC_Q2 = q2.EDC, ExchRate_Q2 = q2.ExchRate
	, ProcessUtilPcnt_Q3 = q3.ProcessUtilPcnt, ProcessEDC_Q3 = q3.ProcessEDC, ProcessUEDC_Q3 = q3.ProcessUEDC, OffsitesEDC_Q3 = q3.OffsitesEDC, Complexity_Q3 = q3.Complexity
	, EII_Q3 = q3.EII, EnergyUseDay_Q3 = q3.EnergyUseDay, TotStdEnergy_Q3 = q3.TotStdEnergy, FuelUseDay_Q3 = q3.FuelUseDay
	, OpAvail_Q3 = q3.OpAvail, MechUnavailTA_Q3 = q3.MechUnavailTA, NonTAOpUnavail_Q3 = q3.NonTAOpUnavail, TADD_Q3 = q3.TADD, NTAMDD_Q3 = q3.NTAMDD, RPDD_Q3 = q3.RPDD, NumDays_Q3 = q3.NumDays, MechAvail_Q3 = q3.MechAvail, NonTAMechUnavail_Q3 = q3.NonTAMechUnavail
	, MaintEffIndex_Q3 = q3.MaintEffIndex, RoutCost_Q3 = q3.RoutCost, AnnTACost_Q3 = q3.AnnTACost, MaintEffDiv_Q3 = q3.MaintEffDiv, TAIndex_Q3 = q3.TAIndex
	, mPersEffIndex_Q3 = q3.mPersEffIndex, MaintWHr_Q3 = q3.MaintWHr, OCCMaintWHr_Q3 = q3.OCCMaintWHr, MPSMaintWHr_Q3 = q3.MPSMaintWHr, mPersEffDiv_Q3 = q3.mPersEffDiv
	, TotCashOpexUEDC_Q3 = q3.TotCashOpexUEDC, TAAdj_Q3 = q3.TAAdj, EnergyCost_Q3 = q3.EnergyCost, TotCashOpex_Q3 = q3.TotCashOpex, UEDC_Q3 = q3.UEDC
	, NEOpexEDC_Q3 = q3.NEOpexEDC, NEOpex_Q3 = q3.NEOpex, EDC_Q3 = q3.EDC, ExchRate_Q3 = q3.ExchRate
	, ProcessUtilPcnt_Q4 = q4.ProcessUtilPcnt, ProcessEDC_Q4 = q4.ProcessEDC, ProcessUEDC_Q4 = q4.ProcessUEDC, OffsitesEDC_Q4 = q4.OffsitesEDC, Complexity_Q4 = q4.Complexity
	, EII_Q4 = q4.EII, EnergyUseDay_Q4 = q4.EnergyUseDay, TotStdEnergy_Q4 = q4.TotStdEnergy, FuelUseDay_Q4 = q4.FuelUseDay
	, OpAvail_Q4 = q4.OpAvail, MechUnavailTA_Q4 = q4.MechUnavailTA, NonTAOpUnavail_Q4 = q4.NonTAOpUnavail, TADD_Q4 = q4.TADD, NTAMDD_Q4 = q4.NTAMDD, RPDD_Q4 = q4.RPDD, NumDays_Q4 = q4.NumDays, MechAvail_Q4 = q4.MechAvail, NonTAMechUnavail_Q4 = q4.NonTAMechUnavail
	, MaintEffIndex_Q4 = q4.MaintEffIndex, RoutCost_Q4 = q4.RoutCost, AnnTACost_Q4 = q4.AnnTACost, MaintEffDiv_Q4 = q4.MaintEffDiv, TAIndex_Q4 = q4.TAIndex
	, mPersEffIndex_Q4 = q4.mPersEffIndex, MaintWHr_Q4 = q4.MaintWHr, OCCMaintWHr_Q4 = q4.OCCMaintWHr, MPSMaintWHr_Q4 = q4.MPSMaintWHr, mPersEffDiv_Q4 = q4.mPersEffDiv
	, TotCashOpexUEDC_Q4 = q4.TotCashOpexUEDC, TAAdj_Q4 = q4.TAAdj, EnergyCost_Q4 = q4.EnergyCost, TotCashOpex_Q4 = q4.TotCashOpex, UEDC_Q4 = q4.UEDC
	, NEOpexEDC_Q4 = q4.NEOpexEDC, NEOpex_Q4 = q4.NEOpex, EDC_Q4 = q4.EDC, ExchRate_Q4 = q4.ExchRate
	, ProcessUtilPcnt_YTQ2 = ytq2.ProcessUtilPcnt, ProcessEDC_YTQ2 = ytq2.ProcessEDC, ProcessUEDC_YTQ2 = ytq2.ProcessUEDC, OffsitesEDC_YTQ2 = ytq2.OffsitesEDC, Complexity_YTQ2 = ytq2.Complexity
	, EII_YTQ2 = ytq2.EII, EnergyUseDay_YTQ2 = ytq2.EnergyUseDay, TotStdEnergy_YTQ2 = ytq2.TotStdEnergy, FuelUseDay_YTQ2 = ytq2.FuelUseDay
	, OpAvail_YTQ2 = ytq2.OpAvail, MechUnavailTA_YTQ2 = ytq2.MechUnavailTA, NonTAOpUnavail_YTQ2 = ytq2.NonTAOpUnavail, TADD_YTQ2 = ytq2.TADD, NTAMDD_YTQ2 = ytq2.NTAMDD, RPDD_YTQ2 = ytq2.RPDD, NumDays_YTQ2 = ytq2.NumDays, MechAvail_YTQ2 = ytq2.MechAvail, NonTAMechUnavail_YTQ2 = ytq2.NonTAMechUnavail
	, MaintEffIndex_YTQ2 = ytq2.MaintEffIndex, RoutCost_YTQ2 = ytq2.RoutCost, AnnTACost_YTQ2 = ytq2.AnnTACost, MaintEffDiv_YTQ2 = ytq2.MaintEffDiv, TAIndex_YTQ2 = ytq2.TAIndex
	, mPersEffIndex_YTQ2 = ytq2.mPersEffIndex, MaintWHr_YTQ2 = ytq2.MaintWHr, OCCMaintWHr_YTQ2 = ytq2.OCCMaintWHr, MPSMaintWHr_YTQ2 = ytq2.MPSMaintWHr, mPersEffDiv_YTQ2 = ytq2.mPersEffDiv
	, TotCashOpexUEDC_YTQ2 = ytq2.TotCashOpexUEDC, TAAdj_YTQ2 = ytq2.TAAdj, EnergyCost_YTQ2 = ytq2.EnergyCost, TotCashOpex_YTQ2 = ytq2.TotCashOpex, UEDC_YTQ2 = ytq2.UEDC
	, NEOpexEDC_YTQ2 = ytq2.NEOpexEDC, NEOpex_YTQ2 = ytq2.NEOpex, EDC_YTQ2 = ytq2.EDC, ExchRate_YTQ2 = ytq2.ExchRate
	, ProcessUtilPcnt_YTQ3 = ytq3.ProcessUtilPcnt, ProcessEDC_YTQ3 = ytq3.ProcessEDC, ProcessUEDC_YTQ3 = ytq3.ProcessUEDC, OffsitesEDC_YTQ3 = ytq3.OffsitesEDC, Complexity_YTQ3 = ytq3.Complexity
	, EII_YTQ3 = ytq3.EII, EnergyUseDay_YTQ3 = ytq3.EnergyUseDay, TotStdEnergy_YTQ3 = ytq3.TotStdEnergy, FuelUseDay_YTQ3 = ytq3.FuelUseDay
	, OpAvail_YTQ3 = ytq3.OpAvail, MechUnavailTA_YTQ3 = ytq3.MechUnavailTA, NonTAOpUnavail_YTQ3 = ytq3.NonTAOpUnavail, TADD_YTQ3 = ytq3.TADD, NTAMDD_YTQ3 = ytq3.NTAMDD, RPDD_YTQ3 = ytq3.RPDD, NumDays_YTQ3 = ytq3.NumDays, MechAvail_YTQ3 = ytq3.MechAvail, NonTAMechUnavail_YTQ3 = ytq3.NonTAMechUnavail
	, MaintEffIndex_YTQ3 = ytq3.MaintEffIndex, RoutCost_YTQ3 = ytq3.RoutCost, AnnTACost_YTQ3 = ytq3.AnnTACost, MaintEffDiv_YTQ3 = ytq3.MaintEffDiv, TAIndex_YTQ3 = ytq3.TAIndex
	, mPersEffIndex_YTQ3 = ytq3.mPersEffIndex, MaintWHr_YTQ3 = ytq3.MaintWHr, OCCMaintWHr_YTQ3 = ytq3.OCCMaintWHr, MPSMaintWHr_YTQ3 = ytq3.MPSMaintWHr, mPersEffDiv_YTQ3 = ytq3.mPersEffDiv
	, TotCashOpexUEDC_YTQ3 = ytq3.TotCashOpexUEDC, TAAdj_YTQ3 = ytq3.TAAdj, EnergyCost_YTQ3 = ytq3.EnergyCost, TotCashOpex_YTQ3 = ytq3.TotCashOpex, UEDC_YTQ3 = ytq3.UEDC
	, NEOpexEDC_YTQ3 = ytq3.NEOpexEDC, NEOpex_YTQ3 = ytq3.NEOpex, EDC_YTQ3 = ytq3.EDC, ExchRate_YTQ3 = ytq3.ExchRate
	, ProcessUtilPcnt_YTD = yr.ProcessUtilPcnt, ProcessEDC_YTD = yr.ProcessEDC, ProcessUEDC_YTD = yr.ProcessUEDC, OffsitesEDC_YTD = yr.OffsitesEDC, Complexity_YTD = yr.Complexity
	, EII_YTD = yr.EII, EnergyUseDay_YTD = yr.EnergyUseDay, TotStdEnergy_YTD = yr.TotStdEnergy, FuelUseDay_YTD = yr.FuelUseDay
	, OpAvail_YTD = yr.OpAvail, MechUnavailTA_YTD = yr.MechUnavailTA, NonTAOpUnavail_YTD = yr.NonTAOpUnavail, TADD_YTD = yr.TADD, NTAMDD_YTD = yr.NTAMDD, RPDD_YTD = yr.RPDD, NumDays_YTD = yr.NumDays, MechAvail_YTD = yr.MechAvail, NonTAMechUnavail_YTD = yr.NonTAMechUnavail
	, MaintEffIndex_YTD = yr.MaintEffIndex, RoutCost_YTD = yr.RoutCost, AnnTACost_YTD = yr.AnnTACost, MaintEffDiv_YTD = yr.MaintEffDiv, TAIndex_YTD = yr.TAIndex
	, mPersEffIndex_YTD = yr.mPersEffIndex, MaintWHr_YTD = yr.MaintWHr, OCCMaintWHr_YTD = yr.OCCMaintWHr, MPSMaintWHr_YTD = yr.MPSMaintWHr, mPersEffDiv_YTD = yr.mPersEffDiv
	, TotCashOpexUEDC_YTD = yr.TotCashOpexUEDC, TAAdj_YTD = yr.TAAdj, EnergyCost_YTD = yr.EnergyCost, TotCashOpex_YTD = yr.TotCashOpex, UEDC_YTD = yr.UEDC
	, NEOpexEDC_YTD = yr.NEOpexEDC, NEOpex_YTD = yr.NEOpex, EDC_YTD = yr.EDC, ExchRate_YTD = yr.ExchRate
	, ProcessUtilPcnt_AVG = avg12.ProcessUtilPcnt, ProcessEDC_AVG = avg12.ProcessEDC, ProcessUEDC_AVG = avg12.ProcessUEDC, OffsitesEDC_AVG = avg12.OffsitesEDC, Complexity_AVG = avg12.Complexity
	, EII_AVG = avg12.EII, EnergyUseDay_AVG = avg12.EnergyUseDay, TotStdEnergy_AVG = avg12.TotStdEnergy, FuelUseDay_AVG = avg12.FuelUseDay
	, OpAvail_AVG = avg24.OpAvail, MechUnavailTA_AVG = avg24.MechUnavailTA, NonTAOpUnavail_AVG = avg24.NonTAOpUnavail, TADD_AVG = avg24.TADD, NTAMDD_AVG = avg24.NTAMDD, RPDD_AVG = avg24.RPDD, NumDays_AVG = avg24.NumDays, MechAvail_AVG = avg24.MechAvail, NonTAMechUnavail_AVG = avg24.NonTAMechUnavail
	, MaintEffIndex_AVG = mi24.MaintEffIndex, RoutCost_AVG = mi24.RoutEffIndex*avg24.MaintEffDiv/100, AnnTACost_AVG = mi24.TAEffIndex*avg24.MaintEffDiv/100, MaintEffDiv_AVG = avg24.MaintEffDiv, TAIndex_AVG = mi24.TAIndex
	, mPersEffIndex_AVG = avg12.mPersEffIndex, MaintWHr_AVG = avg12.MaintWHr, OCCMaintWHr_AVG = avg12.OCCMaintWHr, MPSMaintWHr_AVG = avg12.MPSMaintWHr, mPersEffDiv_AVG = avg12.mPersEffDiv
	, TotCashOpexUEDC_AVG = avg12.TotCashOpexUEDC, TAAdj_AVG = avg12.TAAdj, EnergyCost_AVG = avg12.EnergyCost, TotCashOpex_AVG = avg12.TotCashOpex, UEDC_AVG = avg12.UEDC
	, NEOpexEDC_AVG = avg12.NEOpexEDC, NEOpex_AVG = avg12.NEOpex, EDC_AVG = avg12.EDC, ExchRate_AVG = avg12.ExchRate
FROM dbo.CalcBashneftKPIs2(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQTR, @FactorSet, @Scenario, @Currency, @UOM) q ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ1, @FactorSet, @Scenario, @Currency, @UOM) q1 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ2, @FactorSet, @Scenario, @Currency, @UOM) q2 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ3, @FactorSet, @Scenario, @Currency, @UOM) q3 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ4, @FactorSet, @Scenario, @Currency, @UOM) q4 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListYTQ2, @FactorSet, @Scenario, @Currency, @UOM) ytq2 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListYTQ3, @FactorSet, @Scenario, @Currency, @UOM) ytq3 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListYear, @FactorSet, @Scenario, @Currency, @UOM) yr ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24 ON 1=1
   LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1


