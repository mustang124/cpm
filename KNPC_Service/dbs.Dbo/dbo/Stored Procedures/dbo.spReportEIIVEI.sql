﻿

CREATE     PROC [dbo].[spReportEIIVEI] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @SensHeatFormula varchar(50), @BarrelDesc varchar(50)
SELECT @SensHeatFormula = CAST(SensHeatConstant as varchar(4)) + CASE WHEN ISNULL(SensHeatAPIFactor, 0) = 0 THEN '' ELSE CASE WHEN SensHeatAPIFactor < 0 THEN '-(' ELSE '+(' END + CAST(ABS(SensHeatAPIFactor) AS varchar(4)) + '*CrudeGravity)' END,
@BarrelDesc = CASE WHEN CrudeInSensHeat = 'Y' THEN 'Gross Input Barrels' ELSE 'Gross Non-Crude Processed Barrels' END
FROM FactorSets WHERE RefineryType = ISNULL(dbo.GetRefineryType(@RefineryID), 'FUELS') AND FactorSet = @FactorSet

SELECT s.SubmissionID, s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod,
f.EII, f.EnergyUseDay AS EIIDailyUsage, f.TotStdEnergy AS EIIStdEnergy,
f.VEI, f.EstGain AS VEIStdGain, f.ReportLossGain AS VEIActualGain, f.SensHeatUtilCap AS SensGrossInput, f.SensHeatStdEnergy,
f.OffsitesUtilCap, f.OffsitesStdEnergy, c.AvgGravity AS CrudeGravity
INTO #Monthly
FROM FactorTotCalc f INNER JOIN CrudeTot c ON c.SubmissionID = f.SubmissionID
INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE FactorSet=@FactorSet
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1

SELECT s1.SubmissionID, s1.PeriodStart, NumDays_YTD = SUM(s2.NumDays), EII_YTD = SUM(fc.EnergyUseDay*s2.NumDays)/SUM(fc.TotStdEnergy*s2.NumDays)*100,
EIIUsage_YTD = SUM(fc.EnergyUseDay*s2.NumDays), EIIEstMBTU_YTD = SUM(fc.TotStdEnergy*s2.NumDays), EIIStdEnergy_YTD = SUM(fc.TotStdEnergy*s2.NumDays)/SUM(s2.NumDays),
VEIStdGain_YTD = SUM(fc.EstGain), VEIActualGain_YTD = SUM(fc.ReportLossGain),
SensGrossInput_YTD = SUM(fc.SensHeatUtilCap*s2.NumDays)/SUM(s2.NumDays), EstSensHeatMBTU_YTD = SUM(fc.SensHeatStdEnergy*s2.NumDays), SensHeatStdEnergy_YTD = SUM(fc.SensHeatStdEnergy*s2.NumDays)/SUM(s2.NumDays),
OffsitesUtilCap_YTD = SUM(fc.OffsitesUtilCap*s2.NumDays)/SUM(s2.NumDays), EstOffsitesMBTU_YTD = SUM(fc.OffsitesStdEnergy*s2.NumDays), OffsitesStdEnergy_YTD = SUM(fc.OffsitesStdEnergy*s2.NumDays)/SUM(s2.NumDays)
INTO #YTD
FROM Submissions s1 INNER JOIN Submissions s2 ON s2.RefineryID = s1.RefineryID AND s2.DataSet = s1.DataSet AND s2.PeriodYear = s1.PeriodYear AND s2.PeriodMonth <= s1.PeriodMonth
INNER JOIN FactorTotCalc fc ON fc.submissionId = s2.submissionid 
WHERE fc.factorset=@FactorSet 
And s1.RefineryID = @RefineryID AND s1.DataSet=@DataSet AND s1.PeriodYear = ISNULL(@PeriodYear, s1.PeriodYear) AND s1.PeriodMonth = ISNULL(@PeriodMonth, s1.PeriodMonth) AND s1.UseSubmission = 1 AND s2.UseSubmission = 1
GROUP BY s1.SubmissionID, s1.PeriodStart

SET NOCOUNT OFF

SELECT m.Location, m.PeriodStart, m.PeriodEnd, m.DaysInPeriod, Currency= @Currency, UOM = @UOM, 
m.EII, m.EIIDailyUsage, m.EIIStdEnergy,
m.VEI, m.VEIStdGain, m.VEIActualGain, m.SensGrossInput, m.SensHeatStdEnergy,
SensHeatFormula = @SensHeatFormula, m.OffsitesUtilCap, m.OffsitesStdEnergy, m.CrudeGravity, BarrelDesc = @BarrelDesc,
y.EII_YTD, y.EIIUsage_YTD, y.EIIEstMBTU_YTD, y.EIIStdEnergy_YTD, VEI_YTD = y.VEIActualGain_YTD/y.VEIStdGain_YTD*100, y.VEIStdGain_YTD, y.VEIActualGain_YTD, 
y.SensGrossInput_YTD, y.EstSensHeatMBTU_YTD, y.SensHeatStdEnergy_YTD, 
y.OffsitesUtilCap_YTD, y.EstOffsitesMBTU_YTD, y.OffsitesStdEnergy_YTD
FROM #Monthly m INNER JOIN #YTD y ON y.SubmissionID = m.SubmissionID
ORDER BY m.PeriodStart DESC

DROP TABLE #Monthly
DROP TABLE #YTD




