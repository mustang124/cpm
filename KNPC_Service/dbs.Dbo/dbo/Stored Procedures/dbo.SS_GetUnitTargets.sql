﻿CREATE PROC [dbo].[SS_GetUnitTargets]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
	
AS

SELECT u.UnitID,u.MechAvail,u.OpAvail,u.OnStream,u.UtilPcnt,
             u.RoutCost,u.TACost,ISNULL(RTRIM(u.CurrencyCode),'USD') as CurrencyCode,
             RTRIM(cfg.ProcessID)AS ProcessID,cfg.SortKey,RTRIM(cfg.UnitName) AS UnitName
             FROM dbo.UnitTargets u , Config cfg WHERE  
             u.UnitId = cfg.UnitId AND u.SubmissionID=cfg.SubmissionID AND u.SubmissionID IN 
             (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd)))


