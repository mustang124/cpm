﻿



CREATE   PROC [dbo].[spReportGazpromKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE	@RefUtilPcnt real, @RefUtilPcnt_QTR real, @RefUtilPcnt_AVG real, 
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_AVG real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_AVG real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_AVG real, 
	
	@OpAvail real, @OpAvail_QTR real, @OpAvail_AVG real, 
	@MechUnavailTA real, @MechUnavailTA_QTR real, @MechUnavailTA_AVG real, 
	@NonTAUnavail real, @NonTAUnavail_QTR real, @NonTAUnavail_AVG real, 

	@EII real, @EII_QTR real, @EII_AVG real, 
	@EnergyUseDay real, @EnergyUseDay_QTR real, @EnergyUseDay_AVG real, 
	@TotStdEnergy real, @TotStdEnergy_QTR real, @TotStdEnergy_AVG real, 

	@VEI real, @VEI_QTR real, @VEI_AVG real, 
	@ReportLossGain real, @ReportLossGain_QTR real, @ReportLossGain_AVG real, 
	@EstGain real, @EstGain_QTR real, @EstGain_AVG real, 

	@Gain real, @Gain_QTR real, @Gain_AVG real, 
	@RawMatl real, @RawMatl_QTR real, @RawMatl_AVG real, 
	@ProdYield real, @ProdYield_QTR real, @ProdYield_AVG real, 

	@PersIndex real, @PersIndex_QTR real, @PersIndex_AVG real, 
	@AnnTAWhr real, @AnnTAWhr_QTR real, @AnnTAWhr_AVG real,
	@NonTAWHr real, @NonTAWHr_QTR real, @NonTAWHr_AVG real,
	@EDC real, @EDC_QTR real, @EDC_AVG real, 
	@UEDC real, @UEDC_QTR real, @UEDC_AVG real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_AVG real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_AVG real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_AVG real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_AVG real, 
	@RoutIndex real, @RoutIndex_QTR real, @RoutIndex_AVG real,
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_AVG real, 
	@RoutCost real, @RoutCost_QTR real, @RoutCost_AVG real, 

	@NEOpexEDC real, @NEOpexEDC_QTR real, @NEOpexEDC_AVG real, 
	@NEOpex real, @NEOpex_QTR real, @NEOpex_AVG real, 

	@OpexUEDC real, @OpexUEDC_QTR real, @OpexUEDC_AVG real, 
	@EnergyCost real, @EnergyCost_QTR real, @EnergyCost_AVG real, 
	@TAAdj real, @TAAdj_QTR real, @TAAdj_AVG real,
	@TotCashOpex real, @TotCashOpex_QTR real, @TotCashOpex_AVG real 
DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc] @RefineryID, @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @EII OUTPUT, @EII_QTR = @EII_QTR OUTPUT, @EII_AVG = @EII_AVG OUTPUT, 
	@EnergyUseDay = @EnergyUseDay OUTPUT, @EnergyUseDay_QTR = @EnergyUseDay_QTR OUTPUT, @EnergyUseDay_AVG = @EnergyUseDay_AVG OUTPUT, 
	@TotStdEnergy = @TotStdEnergy OUTPUT, @TotStdEnergy_QTR = @TotStdEnergy_QTR OUTPUT, @TotStdEnergy_AVG = @TotStdEnergy_AVG OUTPUT, 
	@RefUtilPcnt = @RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_AVG = @RefUtilPcnt_AVG OUTPUT, 
	@EDC = @EDC OUTPUT, @EDC_QTR = @EDC_QTR OUTPUT, @EDC_AVG = @EDC_AVG OUTPUT, 
	@UEDC = @UEDC OUTPUT, @UEDC_QTR = @UEDC_QTR OUTPUT, @UEDC_AVG = @UEDC_AVG OUTPUT, 
	@VEI = @VEI OUTPUT, @VEI_QTR = @VEI_QTR OUTPUT, @VEI_AVG = @VEI_AVG OUTPUT, 
	@ReportLossGain = @ReportLossGain OUTPUT, @ReportLossGain_QTR = @ReportLossGain_QTR OUTPUT, @ReportLossGain_AVG = @ReportLossGain_AVG OUTPUT, 
	@EstGain = @EstGain OUTPUT, @EstGain_QTR = @EstGain_QTR OUTPUT, @EstGain_AVG = @EstGain_AVG OUTPUT, 
	@OpAvail = @OpAvail OUTPUT, @OpAvail_QTR = @OpAvail_QTR OUTPUT, @OpAvail_AVG = @OpAvail_AVG OUTPUT, 
	@MechUnavailTA = @MechUnavailTA OUTPUT, @MechUnavailTA_QTR = @MechUnavailTA_QTR OUTPUT, @MechUnavailTA_AVG = @MechUnavailTA_AVG OUTPUT, 
	@NonTAUnavail = @NonTAUnavail OUTPUT, @NonTAUnavail_QTR = @NonTAUnavail_QTR OUTPUT, @NonTAUnavail_AVG = @NonTAUnavail_AVG OUTPUT, 
	@RoutIndex = @RoutIndex OUTPUT, @RoutIndex_QTR = @RoutIndex_QTR OUTPUT, @RoutIndex_AVG = @RoutIndex_AVG OUTPUT,
	@RoutCost = @RoutCost OUTPUT, @RoutCost_QTR = @RoutCost_QTR OUTPUT, @RoutCost_AVG = @RoutCost_AVG OUTPUT, 
	@PersIndex = @PersIndex OUTPUT, @PersIndex_QTR = @PersIndex_QTR OUTPUT, @PersIndex_AVG = @PersIndex_AVG OUTPUT, 
	@AnnTAWhr = @AnnTAWhr OUTPUT, @AnnTAWhr_QTR = @AnnTAWhr_QTR OUTPUT, @AnnTAWhr_AVG = @AnnTAWhr_AVG OUTPUT,
	@NonTAWHr = @NonTAWHr OUTPUT, @NonTAWHr_QTR = @NonTAWHr_QTR OUTPUT, @NonTAWHr_AVG = @NonTAWHr_AVG OUTPUT,
	@NEOpexEDC = @NEOpexEDC OUTPUT, @NEOpexEDC_QTR = @NEOpexEDC_QTR OUTPUT, @NEOpexEDC_AVG = @NEOpexEDC_AVG OUTPUT, 
	@NEOpex = @NEOpex OUTPUT, @NEOpex_QTR = @NEOpex_QTR OUTPUT, @NEOpex_AVG = @NEOpex_AVG OUTPUT, 
	@OpexUEDC = @OpexUEDC OUTPUT, @OpexUEDC_QTR = @OpexUEDC_QTR OUTPUT, @OpexUEDC_AVG = @OpexUEDC_AVG OUTPUT, 
	@TAAdj = @TAAdj OUTPUT, @TAAdj_QTR = @TAAdj_QTR OUTPUT, @TAAdj_AVG = @TAAdj_AVG OUTPUT,
	@EnergyCost = @EnergyCost OUTPUT, @EnergyCost_QTR = @EnergyCost_QTR OUTPUT, @EnergyCost_AVG = @EnergyCost_AVG OUTPUT, 
	@TotCashOpex = @TotCashOpex OUTPUT, @TotCashOpex_QTR = @TotCashOpex_QTR OUTPUT, @TotCashOpex_AVG = @TotCashOpex_AVG OUTPUT

IF @spResult > 0	
	RETURN @spResult
ELSE 

SELECT 
	EII = @EII, EII_QTR = @EII_QTR, EII_AVG = @EII_AVG, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_QTR = @EnergyUseDay_QTR, EnergyUseDay_AVG = @EnergyUseDay_AVG, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_QTR = @TotStdEnergy_QTR, TotStdEnergy_AVG = @TotStdEnergy_AVG, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_QTR = @RefUtilPcnt_QTR, UtilPcnt_AVG = @RefUtilPcnt_AVG, 
	EDC = @EDC, EDC_QTR = @EDC_QTR, EDC_AVG = @EDC_AVG, 
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_QTR = @EDC_QTR*@RefUtilPcnt_QTR/100, UtilUEDC_AVG = @EDC_AVG*@RefUtilPcnt_AVG/100, 
	
--	ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, ProcessUtilPcnt_AVG = @ProcessUtilPcnt_AVG,
--	TotProcessEDC = @TotProcessEDC, TotProcessEDC_QTR = @TotProcessEDC_QTR, TotProcessEDC_AVG = @TotProcessEDC_AVG, 
--	TotProcessUEDC = @TotProcessUEDC, TotProcessUEDC_QTR = @TotProcessUEDC_QTR, TotProcessUEDC_AVG = @TotProcessUEDC_AVG, 
	
	VEI = @VEI, VEI_QTR = @VEI_QTR, VEI_AVG = @VEI_AVG, 
	ReportLossGain = @ReportLossGain, ReportLossGain_QTR = @ReportLossGain_QTR, ReportLossGain_AVG = @ReportLossGain_AVG, 
	EstGain = @EstGain, EstGain_QTR = @EstGain_QTR, EstGain_AVG = @EstGain_AVG, 

	OpAvail = @OpAvail, OpAvail_QTR = @OpAvail_QTR, OpAvail_AVG = @OpAvail_AVG, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_QTR = @MechUnavailTA_QTR, MechUnavailTA_AVG = @MechUnavailTA_AVG, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_QTR = @NonTAUnavail_QTR, NonTAUnavail_AVG = @NonTAUnavail_AVG, 

	MaintIndex = @MaintIndex, MaintIndex_QTR = @MaintIndex_QTR, MaintIndex_AVG = @MaintIndex_AVG, 
	AnnTACost = @AnnTACost, AnnTACost_QTR = @AnnTACost_QTR, AnnTACost_AVG = @AnnTACost_AVG, 
	RoutCost = @RoutCost, RoutCost_QTR = @RoutCost_QTR, RoutCost_AVG = @RoutCost_AVG, 
	RoutIndex = @RoutIndex, RoutIndex_QTR = @RoutIndex_QTR, RoutIndex_AVG = @RoutIndex_AVG, 

	TotWHrEDC = @PersIndex, TotWHrEDC_QTR = @PersIndex_QTR, TotWHrEDC_AVG = @PersIndex_AVG, 
	AnnTAWhr = @AnnTAWhr, AnnTAWhr_QTR = @AnnTAWhr_QTR, AnnTAWhr_AVG = @AnnTAWhr_AVG,
	NonTAWHr = @NonTAWHr, NonTAWHr_QTR = @NonTAWHr_QTR, NonTAWHr_AVG = @NonTAWHr_AVG, 

	NEOpexEDC = @NEOpexEDC, NEOpexEDC_QTR = @NEOpexEDC_QTR, NEOpexEDC_AVG = @NEOpexEDC_AVG, 
	NEOpex = @NEOpex, NEOpex_QTR = @NEOpex_QTR, NEOpex_AVG = @NEOpex_AVG, 

	TotCashOpexUEDC = @OpexUEDC, TotCashOpexUEDC_QTR = @OpexUEDC_QTR, TotCashOpexUEDC_AVG = @OpexUEDC_AVG, 
	TAAdj = @TAAdj, TAAdj_QTR = @TAAdj_QTR, TAAdj_AVG = @TAAdj_AVG,
	EnergyCost = @EnergyCost, EnergyCost_QTR = @EnergyCost_QTR, EnergyCost_AVG = @EnergyCost_AVG, 
	TotCashOpex = @TotCashOpex, TotCashOpex_QTR = @TotCashOpex_QTR, TotCashOpex_AVG = @TotCashOpex_AVG, 
	UEDC = @UEDC, UEDC_QTR = @UEDC_QTR, UEDC_AVG = @UEDC_AVG



