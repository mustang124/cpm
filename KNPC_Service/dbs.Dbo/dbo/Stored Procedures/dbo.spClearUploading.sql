﻿CREATE  PROC [dbo].[spClearUploading](@SubmissionID int)
AS

UPDATE ReadyForCalcs 
SET Uploading = 0, LastUpdate = GetDate()
FROM ReadyForCalcs INNER JOIN SubmissionsAll s ON s.RefineryID = ReadyForCalcs.RefineryID AND s.DataSet = ReadyForCalcs.DataSet
WHERE s.SubmissionID = @SubmissionID


