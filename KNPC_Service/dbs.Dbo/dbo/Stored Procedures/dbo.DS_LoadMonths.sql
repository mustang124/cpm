﻿CREATE PROCEDURE [dbo].[DS_LoadMonths]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS
BEGIN
	SELECT PeriodYear, PeriodMonth 
	FROM dbo.Submissions 
	WHERE RefineryID = @RefineryID  and DataSet = @Dataset and UseSubmission=1
    ORDER BY PeriodStart DESC
END


