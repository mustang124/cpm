﻿CREATE PROC [dbo].[SS_GetInputRefTargets]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            ISNULL(RTRIM(r.CurrencyCode),'USD') as CurrencyCode,r.Target,RTRIM(r.Property) As Property,RTRIM(c.SectionHeader) AS SectionHeader,c.SortKey  
            FROM  
            dbo.RefTargets r ,Chart_LU c  
            ,dbo.Submissions s  
            WHERE  
            r.SubmissionID = s.SubmissionID AND 
            c.TargetField=r.Property AND c.Sortkey<800  
            AND  (r.SubmissionID IN  
            (SELECT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1))


