﻿CREATE   PROC [dbo].[spReportThaiOilChartData2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	ProcessUtilPcnt real NULL, 
	OpAvail real NULL, 
	EII real NULL, 
	VEI real NULL, 
	GainPcnt real NULL, 
	PersIndex real NULL, 
	TotMaintForceWHrEDC real NULL, 
	MaintIndex real NULL, 
	NEOpexEDC real NULL,
	OpexEDC real NULL,
	OpexUEDC real NULL,
	ProcessUtilPcnt_3Mo real NULL, 
	OpAvail_3Mo real NULL, 
	EII_3Mo real NULL, 
	VEI_3Mo real NULL, 
	GainPcnt_3Mo real NULL, 
	PersIndex_3Mo real NULL, 
	TotMaintForceWHrEDC_3Mo real NULL, 
	MaintIndex_3Mo real NULL, 
	NEOpexEDC_3Mo real NULL,
	OpexEDC_3Mo real NULL,
	OpexUEDC_3Mo real NULL,
	ProcessUtilPcnt_12Mo real NULL, 
	OpAvail_24Mo real NULL, 
	EII_12Mo real NULL, 
	VEI_12Mo real NULL, 
	GainPcnt_12Mo real NULL, 
	PersIndex_12Mo real NULL, 
	TotMaintForceWHrEDC_12Mo real NULL, 
	MaintIndex_24Mo real NULL, 
	NEOpexEDC_12Mo real NULL,
	OpexEDC_12Mo real NULL,
	OpexUEDC_12Mo real NULL	
)

--- Everything Already Available in Gensum (missing gain and Opex on an EDC basis)
INSERT INTO @data (PeriodStart, PeriodEnd, ProcessUtilPcnt, OpAvail, EII, VEI, GainPcnt, PersIndex, TotMaintForceWHrEDC, MaintIndex, NEOpexEDC, OpexUEDC, OpexEDC,
	ProcessUtilPcnt_12Mo, OpAvail_24Mo, EII_12Mo, VEI_12Mo, GainPcnt_12Mo, PersIndex_12Mo, TotMaintForceWHrEDC_12Mo, MaintIndex_24Mo, NEOpexEDC_12Mo, OpexUEDC_12Mo)
SELECT	s.PeriodStart, s.PeriodEnd, ProcessUtilPcnt, OpAvail, EII, VEI, -GainPcnt, PersIndex = TotWHrEDC, TotMaintForceWHrEDC, MaintIndex = RoutIndex + TAIndex_Avg, NEOpexEDC, TotCashOpexUEDC
	, OpexEDC = (SELECT TotCashOpex FROM OpexCalc o WHERE o.SubmissionID = g.SubmissionID AND o.Currency = g.Currency AND o.Scenario = 'CLIENT' AND o.FactorSet = g.FactorSet AND o.DataType = 'EDC')
	, ProcessUtilPcnt_Avg, OpAvail_Avg, EII_Avg, VEI_Avg, -GainPcnt_Avg, TotWHrEDC_Avg, TotMaintForceWHrEDC_Avg, MaintIndex_Avg, NEOpexEDC_Avg, TotCashOpexUEDC_Avg
FROM Gensum g INNER JOIN Submissions s ON s.SubmissionID = g.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @PeriodStart12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND g.FactorSet = @FactorSet AND g.Currency = 'USD' AND g.UOM = @UOM AND g.Scenario = @Scenario


DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime
DECLARE @ProcessUtilPcnt_3Mo real, @OpAvail_3Mo real, @EII_3Mo real, @VEI_3Mo real, @GainPcnt_3Mo real, @PersIndex_3Mo real, @TotMaintForceWHrEDC_3Mo real, @MaintIndex_3Mo real, @NEOpexEDC_3Mo real, @OpexUEDC_3Mo real, @OpexEDC_3Mo real, @OpexEDC_12Mo real
DECLARE @SubList3Mo dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList

DECLARE cMonths CURSOR LOCAL FAST_FORWARD
FOR SELECT PeriodStart, PeriodEnd FROM @data
OPEN cMonths 
FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
WHILE @@FETCH_STATUS = 0
BEGIN
	DELETE FROM @SubList3Mo
	DELETE FROM @SubList12Mo
	INSERT @SubList3Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, DATEADD(mm, -3, @PeriodEnd), @PeriodEnd)
	INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, DATEADD(mm, -12, @PeriodEnd), @PeriodEnd)

	UPDATE d
	SET ProcessUtilPcnt_3Mo = avg3mo.ProcessUtilPcnt, 
		OpAvail_3Mo = avg3mo.OpAvail, 
		EII_3Mo = avg3mo.EII, 
		VEI_3Mo = avg3mo.VEI, 
		GainPcnt_3Mo = avg3mo.GainPcnt, 
		PersIndex_3Mo = avg3mo.PersIndex, 
		TotMaintForceWHrEDC_3Mo = avg3mo.TotMaintForceWHrEDC, 
		MaintIndex_3Mo = avg3mo.MaintIndex, 
		NEOpexEDC_3Mo = avg3mo.NEOpexEDC,
		OpexUEDC_3Mo = avg3mo.OpexUEDC,
		OpexEDC_3Mo = avg3mo.OpexEDC,
		OpexEDC_12Mo = avg12mo.OpexEDC
	FROM @data d LEFT JOIN dbo.SLProfileLiteKPIs(@SubList3Mo, @FactorSet, @Scenario, @Currency, @UOM) avg3mo ON 1=1
				 LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
	WHERE d.PeriodStart = @PeriodStart

	FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
END
CLOSE cMonths
DEALLOCATE cMonths

IF (SELECT COUNT(*) FROM @data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
			INSERT @data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, ProcessUtilPcnt, OpAvail, EII, VEI, /*GainPcnt,*/ OpexUEDC, PersIndex, TotMaintForceWHrEDC, MaintIndex, NEOpexEDC, OpexEDC
	, ProcessUtilPcnt_3Mo, OpAvail_3Mo, EII_3Mo, VEI_3Mo, /*GainPcnt_3Mo,*/ OpexUEDC_3Mo, PersIndex_3Mo, TotMaintForceWHrEDC_3Mo, MaintIndex_3Mo, NEOpexEDC_3Mo, OpexEDC_3Mo
	, ProcessUtilPcnt_12Mo, OpAvail_24Mo, EII_12Mo, VEI_12Mo, /*GainPcnt_12Mo,*/ OpexUEDC_12Mo, PersIndex_12Mo, TotMaintForceWHrEDC_12Mo, MaintIndex_24Mo, NEOpexEDC_12Mo, OpexEDC_12Mo
FROM @Data
ORDER BY PeriodStart ASC

