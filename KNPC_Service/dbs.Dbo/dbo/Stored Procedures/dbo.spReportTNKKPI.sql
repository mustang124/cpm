﻿CREATE   PROC [dbo].[spReportTNKKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'RUB', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @Dataset)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubList3Mo dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd)
INSERT @SubList3Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start3Mo, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd)

SELECT UtilPcnt = m.RefUtilPcnt, UtilPcnt_QTR = avg3mo.RefUtilPcnt, UtilPcnt_AVG = avg12mo.RefUtilPcnt, 
	EDC = m.EDC/1000, EDC_QTR = avg3mo.EDC/1000, EDC_AVG = avg12mo.EDC/1000, 
	UtilUEDC = m.UtilUEDC/1000, UtilUEDC_QTR = avg3mo.UtilUEDC/1000, UtilUEDC_AVG = avg12mo.UtilUEDC/1000, 
	UEDC = m.UEDC/1000, UEDC_QTR = avg3mo.UEDC/1000, UEDC_AVG = avg12mo.UEDC/1000, 

	ProcessUtilPcnt = m.ProcessUtilPcnt, ProcessUtilPcnt_QTR = avg3mo.ProcessUtilPcnt, ProcessUtilPcnt_AVG = avg12mo.ProcessUtilPcnt, 
	TotProcessEDC = m.TotProcessEDC/1000, TotProcessEDC_QTR = avg3mo.TotProcessEDC/1000, TotProcessEDC_AVG = avg12mo.TotProcessEDC/1000, 
	TotProcessUEDC = m.TotProcessUEDC/1000, TotProcessUEDC_QTR = avg3mo.TotProcessUEDC/1000, TotProcessUEDC_AVG = avg12mo.TotProcessUEDC/1000, 
	
	OpAvail = m.OpAvail, OpAvail_QTR = avg3mo.OpAvail, OpAvail_AVG = avg24mo.OpAvail, 
	MechUnavailTA = m.MechUnavailTA, MechUnavailTA_QTR = avg3mo.MechUnavailTA, MechUnavailTA_AVG = avg24mo.MechUnavailTA, 
	NonTAUnavail = m.MechUnavailRout + m.RegUnavail, NonTAUnavail_QTR = avg3mo.MechUnavailRout + avg3mo.RegUnavail, NonTAUnavail_AVG = avg24mo.MechUnavailRout + avg24mo.RegUnavail, 

	EII = m.EII, EII_QTR = avg3mo.EII, EII_AVG = avg12mo.EII, 
	EnergyUseDay = m.EnergyUseDay/1000, EnergyUseDay_QTR = avg3mo.EnergyUseDay/1000, EnergyUseDay_AVG = avg12mo.EnergyUseDay/1000, 
	TotStdEnergy = m.TotStdEnergy/1000, TotStdEnergy_QTR = avg3mo.TotStdEnergy/1000, TotStdEnergy_AVG = avg12mo.TotStdEnergy/1000, 

	ProcessEffIndex = m.ProcessEffIndex, ProcessEffIndex_QTR = avg3mo.ProcessEffIndex, ProcessEffIndex_AVG = avg12mo.ProcessEffIndex, 
	VEI = m.VEI, VEI_QTR = avg3mo.VEI, VEI_AVG = avg12mo.VEI, 
	ReportLossGain = m.ReportLossGain, ReportLossGain_QTR = avg3mo.ReportLossGain, ReportLossGain_AVG = avg12mo.ReportLossGain, 
	EstGain = m.EstGain, EstGain_QTR = avg3mo.EstGain, EstGain_AVG = avg12mo.EstGain, 
	NetInputBPD = m.NetInputBPD, NetInputBPD_QTR = avg3mo.NetInputBPD, NetInputBPD_AVG = avg12mo.NetInputBPD,

	Gain = -m.GainPcnt, Gain_QTR = -avg3mo.GainPcnt, Gain_AVG = -avg12mo.GainPcnt, 
	RawMatl = m.RawMatlKBpD, RawMatl_QTR = avg3mo.RawMatlKBpD, RawMatl_AVG = avg12mo.RawMatlKBpD, 
	ProdYield = m.ProdYieldKBpD, ProdYield_QTR = avg3mo.ProdYieldKBpD, ProdYield_AVG = avg12mo.ProdYieldKBpD, 

	PersIndex = m.PersIndex, PersIndex_QTR = avg3mo.PersIndex, PersIndex_AVG = avg12mo.PersIndex, 
	NonMaintWHr = m.NonMaintWHr_k, NonMaintWHr_QTR = avg3mo.NonMaintWHr_k, NonMaintWHr_AVG = avg12mo.NonMaintWHr_k, 
	TotMaintForceWHr = m.TotMaintForceWHr_k, TotMaintForceWHr_QTR = avg3mo.TotMaintForceWHr_k, TotMaintForceWHr_AVG = avg12mo.TotMaintForceWHr_k, 

	TotMaintForceWHrEDC = m.TotMaintForceWHrEDC, TotMaintForceWHrEDC_QTR = avg3mo.TotMaintForceWHrEDC, TotMaintForceWHrEDC_AVG = avg12mo.TotMaintForceWHrEDC, 
	MaintTAWHr = m.MaintTAWHr_k, MaintTAWHr_QTR = avg3mo.MaintTAWHr_k, MaintTAWHr_AVG = avg12mo.MaintTAWHr_k, 
	MaintNonTAWHr = m.MaintNonTAWHr_k, MaintNonTAWHr_QTR = avg3mo.MaintNonTAWHr_k, MaintNonTAWHr_AVG = avg12mo.MaintNonTAWHr_k, 

	MaintIndex = m.MaintIndex, MaintIndex_QTR = avg3mo.MaintIndex, MaintIndex_AVG = mi24.MaintIndex, 
	AnnTACost = m.AnnTACost/1000, AnnTACost_QTR = avg3mo.AnnTACost/1000, AnnTACost_AVG = mi24.TAEffIndex*avg24mo.MaintEffDiv/100/1e6, 
	RoutCost = m.RoutCost/1000, RoutCost_QTR = avg3mo.RoutCost/1000, RoutCost_AVG = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1e6, 

	NEOpexEDC = m.NEOpexEDC, NEOpexEDC_QTR = avg3mo.NEOpexEDC, NEOpexEDC_AVG = avg12mo.NEOpexEDC, 
	NEOpex = m.NEOpex/1000, NEOpex_QTR = avg3mo.NEOpex/1000, NEOpex_AVG = avg12mo.NEOpex/1000, 

	TotCashOpexUEDC = m.OpexUEDC, TotCashOpexUEDC_QTR = avg3mo.OpexUEDC, TotCashOpexUEDC_AVG = avg12mo.OpexUEDC, 
	OpexEDC = m.OpexEDC, OpexEDC_QTR = avg3mo.OpexEDC, OpexEDC_AVG = avg12mo.OpexEDC, 
	EnergyCost = m.EnergyCost/1000, EnergyCost_QTR = avg3mo.EnergyCost/1000, EnergyCost_AVG = avg12mo.EnergyCost/1000, 
	TotCashOpex = m.TotCashOpex/1000, TotCashOpex_QTR = avg3mo.TotCashOpex/1000, TotCashOpex_AVG = avg12mo.TotCashOpex/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList3Mo, @FactorSet, @Scenario, @Currency, @UOM) avg3mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1

