﻿


CREATE   PROC [dbo].[spReportGazpromKPICalc] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15), 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5),
	@EII real = NULL OUTPUT, @EII_QTR real = NULL OUTPUT, @EII_AVG real = NULL OUTPUT, 
	@EnergyUseDay real = NULL OUTPUT, @EnergyUseDay_QTR real = NULL OUTPUT, @EnergyUseDay_AVG real = NULL OUTPUT, 
	@TotStdEnergy real = NULL OUTPUT, @TotStdEnergy_QTR real = NULL OUTPUT, @TotStdEnergy_AVG real = NULL OUTPUT, 
	@RefUtilPcnt real = NULL OUTPUT, @RefUtilPcnt_QTR real = NULL OUTPUT, @RefUtilPcnt_AVG real = NULL OUTPUT, 
	@EDC real = NULL OUTPUT, @EDC_QTR real = NULL OUTPUT, @EDC_AVG real = NULL OUTPUT, 
	@UEDC real = NULL OUTPUT, @UEDC_QTR real = NULL OUTPUT, @UEDC_AVG real = NULL OUTPUT, 
	@VEI real = NULL OUTPUT, @VEI_QTR real = NULL OUTPUT, @VEI_AVG real = NULL OUTPUT, 
	@ReportLossGain real = NULL OUTPUT, @ReportLossGain_QTR real = NULL OUTPUT, @ReportLossGain_AVG real = NULL OUTPUT, 
	@EstGain real = NULL OUTPUT, @EstGain_QTR real = NULL OUTPUT, @EstGain_AVG real = NULL OUTPUT, 
	@OpAvail real = NULL OUTPUT, @OpAvail_QTR real = NULL OUTPUT, @OpAvail_AVG real = NULL OUTPUT, 
	@MechUnavailTA real = NULL OUTPUT, @MechUnavailTA_QTR real = NULL OUTPUT, @MechUnavailTA_AVG real = NULL OUTPUT, 
	@NonTAUnavail real = NULL OUTPUT, @NonTAUnavail_QTR real = NULL OUTPUT, @NonTAUnavail_AVG real = NULL OUTPUT, 
	@RoutIndex real = NULL OUTPUT, @RoutIndex_QTR real = NULL OUTPUT, @RoutIndex_AVG real = NULL OUTPUT,
	@RoutCost real = NULL OUTPUT, @RoutCost_QTR real = NULL OUTPUT, @RoutCost_AVG real = NULL OUTPUT, 
	@PersIndex real = NULL OUTPUT, @PersIndex_QTR real = NULL OUTPUT, @PersIndex_AVG real = NULL OUTPUT, 
	@AnnTAWhr real = NULL OUTPUT, @AnnTAWhr_QTR real = NULL OUTPUT, @AnnTAWhr_AVG real = NULL OUTPUT,
	@NonTAWHr real = NULL OUTPUT, @NonTAWHr_QTR real = NULL OUTPUT, @NonTAWHr_AVG real = NULL OUTPUT,
	@NEOpexEDC real = NULL OUTPUT, @NEOpexEDC_QTR real = NULL OUTPUT, @NEOpexEDC_AVG real = NULL OUTPUT, 
	@NEOpex real = NULL OUTPUT, @NEOpex_QTR real = NULL OUTPUT, @NEOpex_AVG real = NULL OUTPUT, 
	@OpexUEDC real = NULL OUTPUT, @OpexUEDC_QTR real = NULL OUTPUT, @OpexUEDC_AVG real = NULL OUTPUT, 
	@TAAdj real = NULL OUTPUT, @TAAdj_QTR real = NULL OUTPUT, @TAAdj_AVG real = NULL OUTPUT,
	@EnergyCost real = NULL OUTPUT, @EnergyCost_QTR real = NULL OUTPUT, @EnergyCost_AVG real = NULL OUTPUT, 
	@TotCashOpex real = NULL OUTPUT, @TotCashOpex_QTR real = NULL OUTPUT, @TotCashOpex_AVG real = NULL OUTPUT
	)
AS

SELECT @EII = NULL, @EII_QTR = NULL, @EII_AVG = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_QTR = NULL, @EnergyUseDay_AVG = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_QTR = NULL, @TotStdEnergy_AVG = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_QTR = NULL, @RefUtilPcnt_AVG = NULL, 
	@EDC = NULL, @EDC_QTR = NULL, @EDC_AVG = NULL, 
	@UEDC = NULL, @UEDC_QTR = NULL, @UEDC_AVG = NULL, 
	@VEI = NULL, @VEI_QTR = NULL, @VEI_AVG = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_QTR = NULL, @ReportLossGain_AVG = NULL, 
	@EstGain = NULL, @EstGain_QTR = NULL, @EstGain_AVG = NULL, 
	@OpAvail = NULL, @OpAvail_QTR = NULL, @OpAvail_AVG = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_QTR = NULL, @MechUnavailTA_AVG = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_QTR = NULL, @NonTAUnavail_AVG = NULL, 
	@RoutIndex = NULL, @RoutIndex_QTR = NULL, @RoutIndex_AVG = NULL,
	@RoutCost = NULL, @RoutCost_QTR = NULL, @RoutCost_AVG = NULL, 
	@PersIndex = NULL, @PersIndex_QTR = NULL, @PersIndex_AVG = NULL, 
	@AnnTAWhr = NULL, @AnnTAWhr_QTR = NULL, @AnnTAWhr_AVG = NULL,
	@NonTAWHr = NULL, @NonTAWHr_QTR = NULL, @NonTAWHr_AVG = NULL,
	@NEOpexEDC = NULL, @NEOpexEDC_QTR = NULL, @NEOpexEDC_AVG = NULL, 
	@NEOpex = NULL, @NEOpex_QTR = NULL, @NEOpex_AVG = NULL, 
	@OpexUEDC = NULL, @OpexUEDC_QTR = NULL, @OpexUEDC_AVG = NULL, 
	@TAAdj = NULL, @TAAdj_QTR = NULL, @TAAdj_AVG = NULL,
	@EnergyCost = NULL, @EnergyCost_QTR = NULL, @EnergyCost_AVG = NULL, 
	@TotCashOpex = NULL, @TotCashOpex_QTR = NULL, @TotCashOpex_AVG = NULL

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)
IF DATEPART(yy, @Start3Mo) < 2010
	SET @Start3Mo = '12/31/2009'
IF DATEPART(yy, @Start12Mo) < 2010
	SET @Start12Mo = '12/31/2009'
DECLARE	
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_AVG real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_AVG real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_AVG real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_AVG real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_AVG real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_AVG real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_AVG real, 
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_AVG real


--- Everything Already Available in Gensum
SELECT	@RefUtilPcnt = UtilPcnt, @RefUtilPcnt_AVG = UtilPcnt_AVG,
	@ProcessUtilPcnt = ProcessUtilPcnt, @ProcessUtilPcnt_AVG = ProcessUtilPcnt_AVG,
	@OpAvail = OpAvail, @OpAvail_AVG = OpAvail_AVG,
	@EII = EII, @EII_AVG = EII_AVG,
	@VEI = VEI, @VEI_AVG = VEI_AVG,
	@PersIndex = TotWHrEDC, @PersIndex_AVG = TotWHrEDC_AVG,
	@EDC = EDC/1000, @EDC_AVG = EDC_AVG/1000,
	@UEDC = UEDC/1000, @UEDC_AVG = UEDC_AVG/1000,
	@TotMaintForceWHrEDC = TotMaintForceWHrEDC, @TotMaintForceWHrEDC_AVG = TotMaintForceWHrEDC_Avg,
	@MaintIndex = RoutIndex + TAIndex_AVG, @MaintIndex_AVG = MaintIndex_AVG,
	@RoutIndex = RoutIndex, @RoutIndex_AVG = RoutIndex_AVG,
	@NEOpexEDC = NEOpexEDC, @NEOpexEDC_AVG = NEOpexEDC_AVG
FROM Gensum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_QTR = MechUnavailTA_Ann, @MechUnavailTA_AVG = MechUnavailTA_Ann,
	@NonTAUnavail = 100 - OpAvail_Ann - MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @NonTAUnavail_QTR = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	--, @MechUnavailTA_QTR=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail_Avg = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	--, @MechUnavailTA_Avg=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

IF @NonTAUnavail < 0.05
	SET @NonTAUnavail = 0
IF @NonTAUnavail_QTR < 0.05
	SET @NonTAUnavail_QTR = 0
IF @NonTAUnavail_AVG < 0.05
	SET @NonTAUnavail_AVG = 0
SELECT @OpAvail = 100 - @NonTAUnavail - @MechUnavailTA, 
		@OpAvail_QTR = 100 - @NonTAUnavail_QTR - @MechUnavailTA, 
		@OpAvail_AVG = 100 - @NonTAUnavail_Avg - @MechUnavailTA
	
EXEC spAverageFactors @RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, 
		@EII = @EII_QTR OUTPUT, @VEI = @VEI_QTR OUTPUT, @UtilPcnt = @RefUtilPcnt_QTR OUTPUT, @UtilOSTA = NULL, @EDC = @EDC_QTR OUTPUT, @UEDC = @UEDC_QTR OUTPUT, 
		@ProcessUtilPcnt = @ProcessUtilPcnt_QTR OUTPUT, @TotProcessEDC = @TotProcessEDC_QTR OUTPUT, @TotProcessUEDC = @TotProcessUEDC_QTR OUTPUT

SELECT @TotProcessEDC = TotProcessEDC, @TotProcessUEDC = TotProcessUEDC,
	@EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_QTR = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_QTR = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_QTR = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_QTR = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_QTR = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_QTR = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_AVG = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_AVG = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_AVG = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_AVG = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_AVG = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_AVG = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @TotMaintForceWHrEDC_QTR = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @PersIndex_QTR = SUM(g.TotWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @MaintIndex_QTR = SUM((g.RoutIndex + g.TAIndex_AVG)*g.EDC)/SUM(g.EDC)
, @RoutIndex_QTR = SUM(g.RoutIndex*g.EDC)/SUM(g.EDC)
, @NEOpexEDC_QTR = SUM(g.NEOpexEDC*g.EDC)/SUM(g.EDC)
FROM PersTotCalc p INNER JOIN Gensum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet AND g.UOM = @UOM AND s.UseSubmission = 1
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

SELECT @AnnTAWHr = SUM(TotWHr)/1000 FROM Pers WHERE SubmissionID = @SubmissionID AND PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr = TotNonTAWHr/1000
FROM PersTot
WHERE SubmissionID = @SubmissionID

SELECT @AnnTAWHr_QTR = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_QTR = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1

SELECT @AnnTAWHr_AVG = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_AVG = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1

SELECT @AnnTACost = AllocAnnTACost/1000, @RoutCost = CurrRoutCost/1000
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency

SELECT @AnnTACost_QTR = SUM(AllocAnnTACost)/1000, @RoutCost_QTR = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency

SELECT @AnnTACost_AVG = SUM(AllocAnnTACost)/1000, @RoutCost_AVG = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency

SELECT @NEOpex = NEOpex*Divisor/1000, @EnergyCost = EnergyCost*Divisor/1000, @TotCashOpex = TotCashOpex*Divisor/1000, @TAAdj = TAAdj*Divisor/1000
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpexUEDC = TotCashOpex
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpex_QTR = SUM(NEOpex*Divisor)/1000, @EnergyCost_QTR = SUM(EnergyCost*Divisor)/1000
	, @TotCashOpex_QTR = SUM(TotCashOpex*Divisor)/1000, @TAAdj_QTR = SUM(TAAdj*Divisor)/1000
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpexUEDC_QTR = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpex_AVG = SUM(NEOpex*Divisor)/1000, @EnergyCost_AVG = SUM(EnergyCost*Divisor)/1000
	, @TotCashOpex_AVG = SUM(TotCashOpex*Divisor)/1000, @TAAdj_AVG = SUM(TAAdj*Divisor)/1000
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpexUEDC_AVG = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_QTR = @EnergyUseDay_QTR * 1.055, @EnergyUseDay_AVG = @EnergyUseDay_AVG * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_QTR = @TotStdEnergy_QTR * 1.055, @TotStdEnergy_AVG = @TotStdEnergy_AVG * 1.055

SELECT 
	@EnergyUseDay = @EnergyUseDay/1000, @EnergyUseDay_QTR = @EnergyUseDay_QTR/1000, @EnergyUseDay_AVG = @EnergyUseDay_AVG/1000, 
	@TotStdEnergy = @TotStdEnergy/1000, @TotStdEnergy_QTR = @TotStdEnergy_QTR/1000, @TotStdEnergy_AVG = @TotStdEnergy_AVG/1000, 

	@EDC_QTR = @EDC_QTR/1000, @UEDC_QTR = @UEDC_QTR/1000,
	@TotProcessEDC = @TotProcessEDC/1000, @TotProcessEDC_QTR = @TotProcessEDC_QTR/1000, @TotProcessEDC_AVG = @TotProcessEDC_AVG/1000, 
	@TotProcessUEDC = @TotProcessUEDC/1000, @TotProcessUEDC_QTR = @TotProcessUEDC_QTR/1000, @TotProcessUEDC_AVG = @TotProcessUEDC_AVG/1000
	






