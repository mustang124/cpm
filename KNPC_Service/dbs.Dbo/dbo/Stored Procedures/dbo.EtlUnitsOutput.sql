﻿CREATE PROCEDURE [dbo].[EtlUnitsOutput]
	@Methodology VARCHAR(20),
	@SubmissionId INT
AS

	DECLARE @UnitId			nvarchar(10);
	DECLARE @DeleteCommand		NVARCHAR(70);
	
	--limit this to only submissions from KNPC or others in the ClientKeys table
	IF EXISTS (select c.ClientKeysID from pearl.ClientKeys c inner join dbo.SubmissionsAll s on s.RefineryID = c.Refnumber where s.SubmissionID=@SubmissionId) 
	BEGIN

		SET @DeleteCommand = N'DELETE FROM output.ProcessData where SubmissionID =' 
			+  CAST(@SubmissionId AS varchar(20));	
		exec sp_executesql @DeleteCommand;

		DECLARE SqlCursor CURSOR FAST_FORWARD
		FOR
		SELECT DISTINCT UnitID 
		from dbo.ProcessData 
		where SubmissionID =@SubmissionId 
		ORDER BY UnitID ASC;
		
		OPEN SqlCursor;
		FETCH NEXT FROM SqlCursor INTO @UnitId;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			--BEGIN TRY  --With only 48 char to write to dbo.Logging, and each must be unique, GUID to make text unique would take up too
						--many chars to be worthwhile. So just throw error back to calling proc.

				--IF NOT EXISTS (select UnitID from output.ProcessData where SubmissionID =@SubmissionId ) 
					EXECUTE [dbo].[EtlUnitOutput] @Methodology,@SubmissionId,@UnitId;
			FETCH NEXT FROM SqlCursor INTO @UnitId;
		END; --END WHILE
		CLOSE SqlCursor;
		DEALLOCATE SqlCursor;
	END
RETURN
