﻿CREATE   PROC [dbo].[spReportAvail] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT * FROM MaintAvailCalc
WHERE SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID AND DataSet=@DataSet AND PeriodYear = ISNULL(@PeriodYear, PeriodYear) AND PeriodMonth = ISNULL(@PeriodMonth, PeriodMonth) AND UseSubmission = 1)
AND FactorSet = @FactorSet


