﻿




CREATE   PROC [dbo].[spReportGazpromKPI2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012'

DECLARE	@RefUtilPcnt real, @RefUtilPcnt_QTR real, @RefUtilPcnt_AVG real, @RefUtilPcnt_YTD real, 
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_AVG real, @ProcessUtilPcnt_YTD real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_AVG real, @TotProcessEDC_YTD real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_AVG real, @TotProcessUEDC_YTD real, 
	
	@OpAvail real, @OpAvail_QTR real, @OpAvail_AVG real, @OpAvail_YTD real, 
	@MechUnavailTA real, @MechUnavailTA_QTR real, @MechUnavailTA_AVG real, @MechUnavailTA_YTD real, 
	@NonTAUnavail real, @NonTAUnavail_QTR real, @NonTAUnavail_AVG real, @NonTAUnavail_YTD real, 

	@EII real, @EII_QTR real, @EII_AVG real, @EII_YTD real, 
	@EnergyUseDay real, @EnergyUseDay_QTR real, @EnergyUseDay_AVG real, @EnergyUseDay_YTD real, 
	@TotStdEnergy real, @TotStdEnergy_QTR real, @TotStdEnergy_AVG real, @TotStdEnergy_YTD real, 

	@VEI real, @VEI_QTR real, @VEI_AVG real, @VEI_YTD real, 
	@ReportLossGain real, @ReportLossGain_QTR real, @ReportLossGain_AVG real, @ReportLossGain_YTD real, 
	@EstGain real, @EstGain_QTR real, @EstGain_AVG real, @EstGain_YTD real, 

	@Gain real, @Gain_QTR real, @Gain_AVG real, @Gain_YTD real, 
	@RawMatl real, @RawMatl_QTR real, @RawMatl_AVG real, @RawMatl_YTD real, 
	@ProdYield real, @ProdYield_QTR real, @ProdYield_AVG real, @ProdYield_YTD real, 

	@PersIndex real, @PersIndex_QTR real, @PersIndex_AVG real, @PersIndex_YTD real, 
	@AnnTAWhr real, @AnnTAWhr_QTR real, @AnnTAWhr_AVG real, @AnnTAWhr_YTD real,
	@NonTAWHr real, @NonTAWHr_QTR real, @NonTAWHr_AVG real, @NonTAWHr_YTD real,
	@EDC real, @EDC_QTR real, @EDC_AVG real, @EDC_YTD real, 
	@UEDC real, @UEDC_QTR real, @UEDC_AVG real, @UEDC_YTD real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_AVG real, @TotMaintForceWHrEDC_YTD real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_AVG real, @MaintTAWHr_YTD real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_AVG real, @MaintNonTAWHr_YTD real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_AVG real, @MaintIndex_YTD real, 
	@RoutIndex real, @RoutIndex_QTR real, @RoutIndex_AVG real, @RoutIndex_YTD real,
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_AVG real, @AnnTACost_YTD real, 
	@RoutCost real, @RoutCost_QTR real, @RoutCost_AVG real, @RoutCost_YTD real, 

	@NEOpexEDC real, @NEOpexEDC_QTR real, @NEOpexEDC_AVG real, @NEOpexEDC_YTD real, 
	@NEOpex real, @NEOpex_QTR real, @NEOpex_AVG real, @NEOpex_YTD real, 

	@OpexUEDC real, @OpexUEDC_QTR real, @OpexUEDC_AVG real, @OpexUEDC_YTD real, 
	@EnergyCost real, @EnergyCost_QTR real, @EnergyCost_AVG real, @EnergyCost_YTD real, 
	@TAAdj real, @TAAdj_QTR real, @TAAdj_AVG real, @TAAdj_YTD real,
	@TotCashOpex real, @TotCashOpex_QTR real, @TotCashOpex_AVG real , @TotCashOpex_YTD real
DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc2] @RefineryID, @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @EII OUTPUT, @EII_QTR = @EII_QTR OUTPUT, @EII_AVG = @EII_AVG OUTPUT, @EII_YTD = @EII_YTD OUTPUT, 
	@EnergyUseDay = @EnergyUseDay OUTPUT, @EnergyUseDay_QTR = @EnergyUseDay_QTR OUTPUT, @EnergyUseDay_AVG = @EnergyUseDay_AVG OUTPUT, @EnergyUseDay_YTD = @EnergyUseDay_YTD OUTPUT, 
	@TotStdEnergy = @TotStdEnergy OUTPUT, @TotStdEnergy_QTR = @TotStdEnergy_QTR OUTPUT, @TotStdEnergy_AVG = @TotStdEnergy_AVG OUTPUT, @TotStdEnergy_YTD = @TotStdEnergy_YTD OUTPUT, 
	@RefUtilPcnt = @RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_AVG = @RefUtilPcnt_AVG OUTPUT, @RefUtilPcnt_YTD = @RefUtilPcnt_YTD OUTPUT, 
	@EDC = @EDC OUTPUT, @EDC_QTR = @EDC_QTR OUTPUT, @EDC_AVG = @EDC_AVG OUTPUT, @EDC_YTD = @EDC_YTD OUTPUT, 
	@UEDC = @UEDC OUTPUT, @UEDC_QTR = @UEDC_QTR OUTPUT, @UEDC_AVG = @UEDC_AVG OUTPUT, @UEDC_YTD = @UEDC_YTD OUTPUT, 
	@VEI = @VEI OUTPUT, @VEI_QTR = @VEI_QTR OUTPUT, @VEI_AVG = @VEI_AVG OUTPUT, @VEI_YTD = @VEI_YTD OUTPUT, 
	@ReportLossGain = @ReportLossGain OUTPUT, @ReportLossGain_QTR = @ReportLossGain_QTR OUTPUT, @ReportLossGain_AVG = @ReportLossGain_AVG OUTPUT, @ReportLossGain_YTD = @ReportLossGain_YTD OUTPUT, 
	@EstGain = @EstGain OUTPUT, @EstGain_QTR = @EstGain_QTR OUTPUT, @EstGain_AVG = @EstGain_AVG OUTPUT, @EstGain_YTD = @EstGain_YTD OUTPUT, 
	@OpAvail = @OpAvail OUTPUT, @OpAvail_QTR = @OpAvail_QTR OUTPUT, @OpAvail_AVG = @OpAvail_AVG OUTPUT, @OpAvail_YTD = @OpAvail_YTD OUTPUT, 
	@MechUnavailTA = @MechUnavailTA OUTPUT, @MechUnavailTA_QTR = @MechUnavailTA_QTR OUTPUT, @MechUnavailTA_AVG = @MechUnavailTA_AVG OUTPUT, @MechUnavailTA_YTD = @MechUnavailTA_YTD OUTPUT, 
	@NonTAUnavail = @NonTAUnavail OUTPUT, @NonTAUnavail_QTR = @NonTAUnavail_QTR OUTPUT, @NonTAUnavail_AVG = @NonTAUnavail_AVG OUTPUT, @NonTAUnavail_YTD = @NonTAUnavail_YTD OUTPUT, 
	@RoutIndex = @RoutIndex OUTPUT, @RoutIndex_QTR = @RoutIndex_QTR OUTPUT, @RoutIndex_AVG = @RoutIndex_AVG OUTPUT, @RoutIndex_YTD = @RoutIndex_YTD OUTPUT,
	@RoutCost = @RoutCost OUTPUT, @RoutCost_QTR = @RoutCost_QTR OUTPUT, @RoutCost_AVG = @RoutCost_AVG OUTPUT, @RoutCost_YTD = @RoutCost_YTD OUTPUT, 
	@PersIndex = @PersIndex OUTPUT, @PersIndex_QTR = @PersIndex_QTR OUTPUT, @PersIndex_AVG = @PersIndex_AVG OUTPUT, @PersIndex_YTD = @PersIndex_YTD OUTPUT, 
	@AnnTAWhr = @AnnTAWhr OUTPUT, @AnnTAWhr_QTR = @AnnTAWhr_QTR OUTPUT, @AnnTAWhr_AVG = @AnnTAWhr_AVG OUTPUT, @AnnTAWhr_YTD = @AnnTAWhr_YTD OUTPUT,
	@NonTAWHr = @NonTAWHr OUTPUT, @NonTAWHr_QTR = @NonTAWHr_QTR OUTPUT, @NonTAWHr_AVG = @NonTAWHr_AVG OUTPUT, @NonTAWHr_YTD = @NonTAWHr_YTD OUTPUT,
	@NEOpexEDC = @NEOpexEDC OUTPUT, @NEOpexEDC_QTR = @NEOpexEDC_QTR OUTPUT, @NEOpexEDC_AVG = @NEOpexEDC_AVG OUTPUT, @NEOpexEDC_YTD = @NEOpexEDC_YTD OUTPUT, 
	@NEOpex = @NEOpex OUTPUT, @NEOpex_QTR = @NEOpex_QTR OUTPUT, @NEOpex_AVG = @NEOpex_AVG OUTPUT, @NEOpex_YTD = @NEOpex_YTD OUTPUT, 
	@OpexUEDC = @OpexUEDC OUTPUT, @OpexUEDC_QTR = @OpexUEDC_QTR OUTPUT, @OpexUEDC_AVG = @OpexUEDC_AVG OUTPUT, @OpexUEDC_YTD = @OpexUEDC_YTD OUTPUT, 
	@TAAdj = @TAAdj OUTPUT, @TAAdj_QTR = @TAAdj_QTR OUTPUT, @TAAdj_AVG = @TAAdj_AVG OUTPUT, @TAAdj_YTD = @TAAdj_YTD OUTPUT,
	@EnergyCost = @EnergyCost OUTPUT, @EnergyCost_QTR = @EnergyCost_QTR OUTPUT, @EnergyCost_AVG = @EnergyCost_AVG OUTPUT, @EnergyCost_YTD = @EnergyCost_YTD OUTPUT, 
	@TotCashOpex = @TotCashOpex OUTPUT, @TotCashOpex_QTR = @TotCashOpex_QTR OUTPUT, @TotCashOpex_AVG = @TotCashOpex_AVG OUTPUT, @TotCashOpex_YTD = @TotCashOpex_YTD OUTPUT

IF @spResult > 0	
	RETURN @spResult
ELSE 

SELECT 
	EII = @EII, EII_QTR = @EII_QTR, EII_AVG = @EII_AVG, EII_YTD = @EII_YTD, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_QTR = @EnergyUseDay_QTR, EnergyUseDay_AVG = @EnergyUseDay_AVG, EnergyUseDay_YTD = @EnergyUseDay_YTD, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_QTR = @TotStdEnergy_QTR, TotStdEnergy_AVG = @TotStdEnergy_AVG, TotStdEnergy_YTD = @TotStdEnergy_YTD, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_QTR = @RefUtilPcnt_QTR, UtilPcnt_AVG = @RefUtilPcnt_AVG, UtilPcnt_YTD = @RefUtilPcnt_YTD, 
	EDC = @EDC, EDC_QTR = @EDC_QTR, EDC_AVG = @EDC_AVG, EDC_YTD = @EDC_YTD, 
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_QTR = @EDC_QTR*@RefUtilPcnt_QTR/100, UtilUEDC_AVG = @EDC_AVG*@RefUtilPcnt_AVG/100, UtilUEDC_YTD = @EDC_YTD*@RefUtilPcnt_YTD/100, 
	
--	ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, ProcessUtilPcnt_AVG = @ProcessUtilPcnt_AVG,
--	TotProcessEDC = @TotProcessEDC, TotProcessEDC_QTR = @TotProcessEDC_QTR, TotProcessEDC_AVG = @TotProcessEDC_AVG, 
--	TotProcessUEDC = @TotProcessUEDC, TotProcessUEDC_QTR = @TotProcessUEDC_QTR, TotProcessUEDC_AVG = @TotProcessUEDC_AVG, 
	
	VEI = @VEI, VEI_QTR = @VEI_QTR, VEI_AVG = @VEI_AVG, VEI_YTD = @VEI_YTD, 
	ReportLossGain = @ReportLossGain, ReportLossGain_QTR = @ReportLossGain_QTR, ReportLossGain_AVG = @ReportLossGain_AVG, ReportLossGain_YTD = @ReportLossGain_YTD, 
	EstGain = @EstGain, EstGain_QTR = @EstGain_QTR, EstGain_AVG = @EstGain_AVG, EstGain_YTD = @EstGain_YTD, 

	OpAvail = @OpAvail, OpAvail_QTR = @OpAvail_QTR, OpAvail_AVG = @OpAvail_AVG, OpAvail_YTD = @OpAvail_YTD, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_QTR = @MechUnavailTA_QTR, MechUnavailTA_AVG = @MechUnavailTA_AVG, MechUnavailTA_YTD = @MechUnavailTA_YTD, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_QTR = @NonTAUnavail_QTR, NonTAUnavail_AVG = @NonTAUnavail_AVG, NonTAUnavail_YTD = @NonTAUnavail_YTD, 

	MaintIndex = @MaintIndex, MaintIndex_QTR = @MaintIndex_QTR, MaintIndex_AVG = @MaintIndex_AVG, MaintIndex_YTD = @MaintIndex_YTD, 
	AnnTACost = @AnnTACost, AnnTACost_QTR = @AnnTACost_QTR, AnnTACost_AVG = @AnnTACost_AVG, AnnTACost_YTD = @AnnTACost_YTD, 
	RoutCost = @RoutCost, RoutCost_QTR = @RoutCost_QTR, RoutCost_AVG = @RoutCost_AVG, RoutCost_YTD = @RoutCost_YTD, 
	RoutIndex = @RoutIndex, RoutIndex_QTR = @RoutIndex_QTR, RoutIndex_AVG = @RoutIndex_AVG, RoutIndex_YTD = @RoutIndex_YTD, 

	TotWHrEDC = @PersIndex, TotWHrEDC_QTR = @PersIndex_QTR, TotWHrEDC_AVG = @PersIndex_AVG, TotWHrEDC_YTD = @PersIndex_YTD, 
	AnnTAWhr = @AnnTAWhr, AnnTAWhr_QTR = @AnnTAWhr_QTR, AnnTAWhr_AVG = @AnnTAWhr_AVG, AnnTAWhr_YTD = @AnnTAWhr_YTD,
	NonTAWHr = @NonTAWHr, NonTAWHr_QTR = @NonTAWHr_QTR, NonTAWHr_AVG = @NonTAWHr_AVG, NonTAWHr_YTD = @NonTAWHr_YTD, 

	NEOpexEDC = @NEOpexEDC, NEOpexEDC_QTR = @NEOpexEDC_QTR, NEOpexEDC_AVG = @NEOpexEDC_AVG, NEOpexEDC_YTD = @NEOpexEDC_YTD, 
	NEOpex = @NEOpex, NEOpex_QTR = @NEOpex_QTR, NEOpex_AVG = @NEOpex_AVG, NEOpex_YTD = @NEOpex_YTD, 

	TotCashOpexUEDC = @OpexUEDC, TotCashOpexUEDC_QTR = @OpexUEDC_QTR, TotCashOpexUEDC_AVG = @OpexUEDC_AVG, TotCashOpexUEDC_YTD = @OpexUEDC_YTD, 
	TAAdj = @TAAdj, TAAdj_QTR = @TAAdj_QTR, TAAdj_AVG = @TAAdj_AVG, TAAdj_YTD = @TAAdj_YTD,
	EnergyCost = @EnergyCost, EnergyCost_QTR = @EnergyCost_QTR, EnergyCost_AVG = @EnergyCost_AVG, EnergyCost_YTD = @EnergyCost_YTD, 
	TotCashOpex = @TotCashOpex, TotCashOpex_QTR = @TotCashOpex_QTR, TotCashOpex_AVG = @TotCashOpex_AVG, TotCashOpex_YTD = @TotCashOpex_YTD, 
	UEDC = @UEDC, UEDC_QTR = @UEDC_QTR, UEDC_AVG = @UEDC_AVG, UEDC_YTD = @UEDC_YTD




