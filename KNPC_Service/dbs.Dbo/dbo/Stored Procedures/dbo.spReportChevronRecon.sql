﻿



CREATE   PROC [dbo].[spReportChevronRecon] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

DECLARE @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)


DECLARE	
	@UEDC real, @PurUtilUEDC real, @RefUtilUEDC real, @EDC real, @UtilPcnt real,
	@TotProcessUEDC real, @TotProcessEDC real, @ProcessUtilPcnt real, 
	
	@MechUnavail_Act real, @MechUnavailSlow_Act real, @OpUnavail_Act real, @OpUnavailSlow_Act real, @OffStream_Act real, @OffStreamSlow_Act real,
	@ProcessUtilPcntCalc real,

	@TotCashOpex real, @TotCashOpex_YTD real, @TotCashOpex_AVG real, @TotCashOpexUEDC real, @TotCashOpexUEDC_YTD real, 
	@TotCashOpexUEDC_AVG real, @TotPurEnergy real,
	@TotPurEnergy_YTD real, @TotPurEnergy_AVG real, @TotPurEnergyUEDC real, @TotPurEnergyUEDC_YTD real,
	@TotPurEnergyUEDC_AVG real, @TotProdEnergy real, @TotProdEnergy_YTD real, @TotProdEnergy_AVG real,
    @TotProdEnergyUEDC real, @TotProdEnergyUEDC_YTD real, @TotProdEnergyUEDC_AVG real, @TotOthEnergy real,
	@TotOthEnergy_YTD real, @TotOthEnergy_AVG real, @TotOthEnergyUEDC real, @TotOthEnergyUEDC_YTD real,
	@TotOthEnergyUEDC_AVG real, @NEOpex real, @NEOpex_YTD real, @NEOpex_AVG real, @NEOpexUEDC real,
	@NEOpexUEDC_YTD real, @NEOpexUEDC_AVG real, @UEDC_YTD real, @UEDC_AVG real, 
	
	@OCCPO_STH real, @OCCPO_OVT real, @OCCPO_Contract real, @OCCPO_GA real, @OCCPO_AbsHrs real, @OCCPO_TotWHr real, @OCCPO_TotEqPEDC real,
	@OCCTAADJ_STH real, @OCCTAADJ_OVT real, @OCCTAADJ_Contract real, @OCCTAADJ_GA real, @OCCTAADJ_AbsHrs real, @OCCTAADJ_TotWHr real, @OCCTAADJ_TotEqPEDC real,
	@OCCMA_STH real, @OCCMA_OVT real, @OCCMA_Contract real, @OCCMA_GA real, @OCCMA_AbsHrs real, @OCCMA_TotWHr real, @OCCMA_TotEqPEDC real,
	@OCCAS_STH real, @OCCAS_OVT real, @OCCAS_Contract real, @OCCAS_GA real, @OCCAS_AbsHrs real, @OCCAS_TotWHr real, @OCCAS_TotEqPEDC real,
	@OCC_STH real, @OCC_OVT real, @OCC_Contract real, @OCC_GA real, @OCC_AbsHrs real, @OCC_TotWHr real, @OCC_TotEqPEDC real,

	@MPSPO_STH real, @MPSPO_OVT real, @MPSPO_Contract real, @MPSPO_GA real, @MPSPO_AbsHrs real, @MPSPO_TotWHr real, @MPSPO_TotEqPEDC real,
	@MPSTAADJ_STH real, @MPSTAADJ_OVT real, @MPSTAADJ_Contract real, @MPSTAADJ_GA real, @MPSTAADJ_AbsHrs real, @MPSTAADJ_TotWHr real, @MPSTAADJ_TotEqPEDC real,
	@MPSMA_STH real, @MPSMA_OVT real, @MPSMA_Contract real, @MPSMA_GA real, @MPSMA_AbsHrs real, @MPSMA_TotWHr real, @MPSMA_TotEqPEDC real,
	@MPSTS_STH real, @MPSTS_OVT real, @MPSTS_Contract real, @MPSTS_GA real, @MPSTS_AbsHrs real, @MPSTS_TotWHr real, @MPSTS_TotEqPEDC real,
	@MPSAS_STH real, @MPSAS_OVT real, @MPSAS_Contract real, @MPSAS_GA real, @MPSAS_AbsHrs real, @MPSAS_TotWHr real, @MPSAS_TotEqPEDC real,
	@MPS_STH real, @MPS_OVT real, @MPS_Contract real, @MPS_GA real, @MPS_AbsHrs real, @MPS_TotWHr real, @MPS_TotEqPEDC real,

	@AnnTACost real, @CurrRoutCost real, @TotMaintCost real, @TAIndex real, @RoutIndex real, @MaintIndex real,
	@AnnTACost_YTD real, @CurrRoutCost_YTD real, @TotMaintCost_YTD real, @RoutIndex_YTD real, @MaintIndex_YTD real,
	@AnnTACost_AVG real, @CurrRoutCost_AVG real, @TotMaintCost_AVG real, @RoutIndex_AVG real, @MaintIndex_AVG real,
	
	@EDC_YTD real,	@EDC_AVG real, 
	
	@RptSource_PUR real, @RptSource_PRO real, @RptSource_IN real, @RptSource_OUT real, @RptSource_SOL real,
	@SourceMWH_PUR real, @SourceMWH_PRO real, @SourceMWH_IN real, @SourceMWH_OUT real, @SourceMWH_SOL real,
	@TotEnergyConsMBTU real, @EnergyUseDay real, @ProcessStdEnergy real, @SensHeatStdEnergy real, 
	@OffsitesStdEnergy real, @AspStdEnergy real, @TotStdEnergy real, @EII real, 
	
	@RMI real, @RMIPriceUS real, @RMICost real, @RMI_YTD real, @RMIPriceUS_YTD real, @RMICost_YTD real,
	@OTHRM real, @OTHRMPriceUS real, @OTHRMCost real, @OTHRM_YTD real, @OTHRMPriceUS_YTD real, @OTHRMCost_YTD real,
	@RM real, @RMCost real, @RM_YTD real, @RMCost_YTD real,	@NetInputBPD real, @NetInputBPD_YTD real, 
	
	@PROD real, @PRODPriceUS real, @PRODCost real, @PROD_YTD real, @PRODPriceUS_YTD real, @PRODCost_YTD real,
	@RPF real, @RPFPriceUS real, @RPFCost real, @RPF_YTD real, @RPFPriceUS_YTD real, @RPFCost_YTD real,
	@ASP real, @ASPPriceUS real, @ASPCost real, @ASP_YTD real, @ASPPriceUS_YTD real, @ASPCost_YTD real,
	@SOLV real, @SOLVPriceUS real, @SOLVCost real, @SOLV_YTD real, @SOLVPriceUS_YTD real, @SOLVCost_YTD real,
	@COKE real, @COKEPriceUS real, @COKECost real, @COKE_YTD real, @COKEPriceUS_YTD real, @COKECost_YTD real,
	@MPROD real, @MPRODPriceUS real, @MPRODCost real, @MPROD_YTD real, @MPRODPriceUS_YTD real, @MPRODCost_YTD real,
	@YIELD real, @YIELDPriceUS real, @YIELDCost real, @YIELD_YTD real, @YIELDPriceUS_YTD real, @YIELDCost_YTD real,

	@GrossMargin real, @GrossMargin_YTD real, @TotGrossMargin real,	@TotGrossMargin_YTD real, 
	@TotCashOpexBbl real, @TotCashOpexBbl_YTD real, @TotCashMargin real, @CashMargin real,
	@TotCashMargin_YTD real, @CashMargin_YTD real

--Get Standard Variables Out of Gensum First (38)
SELECT @UEDC = UEDC/1000, @EDC = EDC/1000, @UtilPcnt = UtilPcnt, @ProcessUtilPcnt = ProcessUtilPcnt, @TotCashOpexUEDC = TotCashOpexUEDC, 
	@TotCashOpexUEDC_YTD = TotCashOpexUEDC_YTD, @TotCashOpexUEDC_AVG = TotCashOpexUEDC_AVG, @NEOpexUEDC = NEOpexUEDC, 
	@NEOpexUEDC_YTD = NEOpexUEDC_YTD, @NEOpexUEDC_AVG = NEOpexUEDC_AVG, @UEDC_YTD = UEDC_YTD/1000, @UEDC_AVG = UEDC_AVG/1000, 
	@TAIndex = TAIndex_AVG, @RoutIndex = RoutIndex, @RoutIndex_YTD = RoutIndex_YTD, @RoutIndex_AVG = RoutIndex_AVG, 
	@MaintIndex = RoutIndex+TAIndex_AVG, @MaintIndex_YTD = MaintIndex_YTD, @MaintIndex_AVG = MaintIndex_AVG, @EDC_YTD = EDC_YTD/1000, 
	@EDC_AVG = EDC_AVG/1000, @EII = EII, @NetInputBPD = NetInputBPD * @NumDays, @NetInputBPD_YTD = NetInputBPD_YTD * @NumDays, @GrossMargin = GrossMargin, 
	@GrossMargin_YTD = GrossMargin_YTD, @TotCashOpexBbl = TotCashOpexBbl, @TotCashOpexBbl_YTD = TotCashOpexBbl_YTD, @CashMargin = CashMargin,  @CashMargin_YTD = CashMargin_YTD 
FROM Gensum
WHERE SubmissionID = @SubmissionID AND DataSet='Actual' AND FactorSet = @FactorSet AND Currency = @Currency AND UOM = @UOM AND Scenario = @Scenario

SELECT @TotProcessUEDC = TotProcessUEDC/1000, @TotProcessEDC = TotProcessEDC/1000, @PurUtilUEDC = (ISNULL(PurElecUEDC,0)+ISNULL(PurStmUEDC,0))/1000
FROM FactorTotCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @RefUtilUEDC = @UEDC - @PurUtilUEDC

-- Get the Availability Data
SELECT @MechUnavail_Act = 100 - MechAvail_Act, @MechUnavailSlow_Act = MechAvail_Act - MechAvailSlow_Act,
	@OpUnavail_Act = MechAvail_Act-OpAvail_Act, @OpUnavailSlow_Act = MechAvailSlow_Act - OpAvailSlow_Act,
	@OffStream_Act = OpAvail_Act-Onstream_Act, @OffStreamSlow_Act = OpAvailSlow_Act - OnstreamSlow_Act
FROM MaintAvailCalc WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet 

SELECT @ProcessUtilPcntCalc = 100 - @MechUnavail_Act - @MechUnavailSlow_Act - @OpUnavail_Act - @OpUnavailSlow_Act - @OffStream_Act - @OffStreamSlow_Act

--Get the OPEX Dollars Out of OpexAll
SELECT @TotCashOpex = TotCashOpex/1000, @TotPurEnergy = (ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0))/1000,
	@TotProdEnergy = (ISNULL(RefProdFG,0)+ISNULL(RefProdOTH,0))/1000, @TotOthEnergy = ISNULL(PurOth,0)/1000, @NEOpex = NEOpex/1000, @AnnTACost = TAAdj/1000 
FROM OpexAll
WHERE SubmissionID = @SubmissionID AND DataType='Adj' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotCashOpex_YTD = SUM(TotCashOpex)/1000, @TotPurEnergy_YTD = (SUM(ISNULL(PurElec,0))+SUM(ISNULL(PurSteam,0))+SUM(ISNULL(PurFG,0))+SUM(ISNULL(PurSolid,0))+SUM(ISNULL(PurLiquid,0)))/1000,
	@TotProdEnergy_YTD = (SUM(ISNULL(RefProdFG,0))+SUM(ISNULL(RefProdOTH,0)))/1000, @TotOthEnergy_YTD = SUM(ISNULL(PurOth,0))/1000, @NEOpex_YTD = SUM(NEOpex)/1000, @AnnTACost_YTD = SUM(TAAdj)/1000  
FROM OpexAll o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and DataType='Adj' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotCashOpex_AVG = SUM(TotCashOpex)/1000, @TotPurEnergy_AVG = (SUM(ISNULL(PurElec,0))+SUM(ISNULL(PurSteam,0))+SUM(ISNULL(PurFG,0))+SUM(ISNULL(PurSolid,0))+SUM(ISNULL(PurLiquid,0)))/1000,
	@TotProdEnergy_AVG = (SUM(ISNULL(RefProdFG,0))+SUM(ISNULL(RefProdOTH,0)))/1000, @TotOthEnergy_AVG = SUM(ISNULL(PurOth,0))/1000, @NEOpex_AVG = SUM(NEOpex)/1000, @AnnTACost_AVG = 2*SUM(TAAdj)/1000  
FROM OpexAll o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and DataType='Adj' AND Currency = @Currency AND Scenario = @Scenario

--Get the OPEX/UEDC Dollars Out of OpexAll
SELECT @TotPurEnergyUEDC = ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0),
	@TotProdEnergyUEDC = ISNULL(RefProdFG,0)+ISNULL(RefProdOTH,0), @TotOthEnergyUEDC = ISNULL(PurOth,0) 
FROM OpexCalc
WHERE SubmissionID = @SubmissionID AND DataType='UEDC' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotPurEnergyUEDC_YTD = SUM((ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0))*Divisor)/SUM(Divisor),
	@TotProdEnergyUEDC_YTD = SUM((ISNULL(RefProdFG,0)+ISNULL(RefProdOTH,0))*Divisor)/SUM(Divisor), @TotOthEnergyUEDC_YTD = SUM(ISNULL(PurOth,0)*Divisor)/SUM(Divisor)
FROM OpexCalc o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and DataType='UEDC' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotPurEnergyUEDC_AVG = SUM((ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0))*Divisor)/SUM(Divisor),
	@TotProdEnergyUEDC_AVG = SUM((ISNULL(RefProdFG,0)+ISNULL(RefProdOTH,0))*Divisor)/SUM(Divisor), @TotOthEnergyUEDC_AVG = SUM(ISNULL(PurOth,0)*Divisor)/SUM(Divisor)
FROM OpexCalc o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.SubmissionID = 1
and DataType='UEDC' AND Currency = @Currency AND Scenario = @Scenario

-- Get the Pers Hours by Subtotal Category
SELECT @OCCPO_STH = p.STH, @OCCPO_OVT = p.OVTHours, @OCCPO_Contract = p.Contract, @OCCPO_GA = p.GA, @OCCPO_AbsHrs = p.AbsHrs, @OCCPO_TotWHr = pc.TotWHr, @OCCPO_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'OCCPO' AND pc.FactorSet = 2008

SELECT @OCCTAADJ_STH = p.STH, @OCCTAADJ_OVT = p.OVTHours, @OCCTAADJ_Contract = p.Contract, @OCCTAADJ_GA = p.GA, @OCCTAADJ_AbsHrs = p.AbsHrs, @OCCTAADJ_TotWHr = pc.TotWHr, @OCCTAADJ_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'OCCTAADJ' AND pc.FactorSet = 2008

SELECT @OCCMA_STH = p.STH, @OCCMA_OVT = p.OVTHours, @OCCMA_Contract = p.Contract, @OCCMA_GA = p.GA, @OCCMA_AbsHrs = p.AbsHrs, @OCCMA_TotWHr = pc.TotWHr, @OCCMA_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'OCCMA' AND pc.FactorSet = 2008

SELECT @OCCAS_STH = p.STH, @OCCAS_OVT = p.OVTHours, @OCCAS_Contract = p.Contract, @OCCAS_GA = p.GA, @OCCAS_AbsHrs = p.AbsHrs, @OCCAS_TotWHr = pc.TotWHr, @OCCAS_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'OCCAS' AND pc.FactorSet = 2008

SELECT @OCC_STH = SUM(p.STH), @OCC_OVT = SUM(p.OVTHours), @OCC_Contract = SUM(p.Contract), @OCC_GA = SUM(p.GA), @OCC_AbsHrs = SUM(p.AbsHrs), @OCC_TotWHr = SUM(pc.TotWHr), @OCC_TotEqPEDC = SUM(pc.TotEqPEDC)
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID IN ('OCCPO','OCCMA','OCCTAADJ','OCCAS') AND pc.FactorSet = 2008

SELECT @MPSPO_STH = p.STH, @MPSPO_OVT = p.OVTHours, @MPSPO_Contract = p.Contract, @MPSPO_GA = p.GA, @MPSPO_AbsHrs = p.AbsHrs, @MPSPO_TotWHr = pc.TotWHr, @MPSPO_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'MPSPO' AND pc.FactorSet = 2008

SELECT @MPSTAADJ_STH = p.STH, @MPSTAADJ_OVT = p.OVTHours, @MPSTAADJ_Contract = p.Contract, @MPSTAADJ_GA = p.GA, @MPSTAADJ_AbsHrs = p.AbsHrs, @MPSTAADJ_TotWHr = pc.TotWHr, @MPSTAADJ_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'MPSTAADJ' AND pc.FactorSet = 2008

SELECT @MPSMA_STH = p.STH, @MPSMA_OVT = p.OVTHours, @MPSMA_Contract = p.Contract, @MPSMA_GA = p.GA, @MPSMA_AbsHrs = p.AbsHrs, @MPSMA_TotWHr = pc.TotWHr, @MPSMA_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'MPSMA' AND pc.FactorSet = 2008

SELECT @MPSTS_STH = p.STH, @MPSTS_OVT = p.OVTHours, @MPSTS_Contract = p.Contract, @MPSTS_GA = p.GA, @MPSTS_AbsHrs = p.AbsHrs, @MPSTS_TotWHr = pc.TotWHr, @MPSTS_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'MPSTS' AND pc.FactorSet = 2008

SELECT @MPSAS_STH = p.STH, @MPSAS_OVT = p.OVTHours, @MPSAS_Contract = p.Contract, @MPSAS_GA = p.GA, @MPSAS_AbsHrs = p.AbsHrs, @MPSAS_TotWHr = pc.TotWHr, @MPSAS_TotEqPEDC = pc.TotEqPEDC
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID = 'MPSAS' AND pc.FactorSet = 2008

SELECT @MPS_STH = SUM(p.STH), @MPS_OVT = SUM(p.OVTHours), @MPS_Contract = SUM(p.Contract), @MPS_GA = SUM(p.GA), @MPS_AbsHrs = SUM(p.AbsHrs), @MPS_TotWHr = SUM(pc.TotWHr), @MPS_TotEqPEDC = SUM(pc.TotEqPEDC)
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.Persid = pc.PersID
WHERE p.SubmissionID = @SubmissionID AND p.PersID IN ('MPSPO','MPSMA','MPSTAADJ','MPSTS','MPSAS') AND pc.FactorSet = 2008

-- Get Maintenance Dollars
SELECT @CurrRoutCost = CurrRoutCost/1000, @TotMaintCost = ISNULL(CurrRoutCost,0)/1000+@AnnTACost
FROM MaintTotCost
WHERE SubmissionID = @SubmissionID AND Currency = @Currency

SELECT @CurrRoutCost_YTD = SUM(CurrRoutCost)/1000
FROM MaintTotCost m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and Currency = @Currency

SELECT @TotMaintCost_YTD = @CurrRoutCost_YTD+@AnnTACost_YTD

SELECT @CurrRoutCost_AVG = SUM(CurrRoutCost)/1000
FROM MaintTotCost m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID and Currency = @Currency AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1

SELECT @TotMaintCost_AVG = @CurrRoutCost_AVG+@AnnTACost_AVG

-- Get Energy Data
SELECT @RptSource_PUR = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR'
SELECT @RptSource_PRO = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO'
SELECT @RptSource_IN = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'REF'
SELECT @RptSource_OUT = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'AFF'
SELECT @RptSource_SOL = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL'

-- Get Electric Data
SELECT @SourceMWH_PUR = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'PUR'
SELECT @SourceMWH_PRO = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'PRO'
SELECT @SourceMWH_IN = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'REF'
SELECT @SourceMWH_OUT = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'AFF'
SELECT @SourceMWH_SOL = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'SOL'

-- Total Consumption and Standard Energy
SELECT @TotEnergyConsMBTU = e.TotEnergyConsMBTU, @EnergyUseDay = EnergyUseDay, 
	@ProcessStdEnergy = TotStdEnergy - ISNULL(SensHeatStdEnergy,0) - ISNULL(OffsitesStdEnergy,0) - ISNULL(AspStdEnergy,0),
	@SensHeatStdEnergy = SensHeatStdEnergy, @OffsitesStdEnergy = OffsitesStdEnergy, @AspStdEnergy = AspStdEnergy,
	@TotStdEnergy = TotStdEnergy
FROM EnergyTot e INNER JOIN FactorTotCalc f ON e.SubmissionID = f.SubmissionID
WHERE e.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

-- Get Raw Materials
SELECT @RMI = SUM(ISNULL(BBL,0))/1000, @RMIPriceUS = SUM(BBL*PriceUS)/SUM(BBL), @RMICost = SUM(BBL*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'RMI'

SELECT @RMI_YTD = SUM(ISNULL(BBL,0))/1000, @RMIPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @RMICost_YTD = SUM(BBL*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'RMI'

SELECT @OTHRM = SUM(ISNULL(BBL,0))/1000, @OTHRMPriceUS = SUM(BBL*PriceUS)/SUM(BBL), @OTHRMCost = SUM(BBL*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'OTHRM'

SELECT @OTHRM_YTD = SUM(ISNULL(BBL,0))/1000, @OTHRMPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @OTHRMCost_YTD = SUM(BBL*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'OTHRM'

SELECT @RM = @RMI+@OTHRM, @RMCost = @RMICost+@OTHRMCost, @RM_YTD = @RMI_YTD+@OTHRM_YTD, @RMCost_YTD = @RMICost_YTD+@OTHRMCost_YTD

-- Get Product Yields
SELECT @PROD = SUM(ISNULL(BBL,0))/1000, @PRODPriceUS = SUM(BBL*PriceUS)/SUM(BBL), @PRODCost = SUM(BBL*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'PROD'

SELECT @PROD_YTD = SUM(ISNULL(BBL,0))/1000, @PRODPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @PRODCost_YTD = SUM(BBL*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'PROD'

SELECT @RPF = SUM(ISNULL(BBL,0))/1000, @RPFPriceUS = SUM(BBL*PriceUS)/SUM(BBL), @RPFCost = SUM(BBL*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'RPF'

SELECT @RPF_YTD = SUM(ISNULL(BBL,0))/1000, @RPFPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @RPFCost_YTD = SUM(BBL*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'RPF'

SELECT @ASP = SUM(ISNULL(BBL,0))/1000, @ASPPriceUS = SUM(BBL*PriceUS)/SUM(BBL), @ASPCost = SUM(BBL*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'ASP'

SELECT @ASP_YTD = SUM(ISNULL(BBL,0))/1000, @ASPPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @ASPCost_YTD = SUM(BBL*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'ASP'

SELECT @COKE = SUM(ISNULL(BBL,0))/1000, @COKEPriceUS = SUM(BBL*PriceUS)/SUM(BBL), @COKECost = SUM(BBL*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'COKE'

SELECT @COKE_YTD = SUM(ISNULL(BBL,0))/1000, @COKEPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @COKECost_YTD = SUM(BBL*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'COKE'

SELECT @SOLV = SUM(ISNULL(BBL,0))/1000, @SOLVPriceUS = SUM(BBL*PriceUS)/SUM(BBL), @SOLVCost = SUM(BBL*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'SOLV'

SELECT @SOLV_YTD = SUM(ISNULL(BBL,0))/1000, @SOLVPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @SOLVCost_YTD = SUM(BBL*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'SOLV'

SELECT @MPROD = SUM(ISNULL(BBL,0))/1000, @MPRODPriceUS = SUM(ISNULL(BBL,0)*PriceUS)/SUM(BBL), @MPRODCost = SUM(ISNULL(BBL,0)*ISNULL(PriceUS,0))/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'MPROD'

SELECT @MPROD_YTD = SUM(ISNULL(BBL,0))/1000, @MPRODPriceUS_YTD = SUM(BBL*PriceUS)/SUM(BBL), @MPRODCost_YTD = SUM(ISNULL(BBL,0)*ISNULL(PriceUS,0))/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'MPROD'

SELECT @YIELD = ISNULL(@PROD,0)+ISNULL(@RPF,0)+ISNULL(@ASP,0)+ISNULL(@COKE,0)+ISNULL(@SOLV,0)+ISNULL(@MPROD,0), 
	@YIELDCost = ISNULL(@PRODCost,0)+ISNULL(@RPFCost,0)+ISNULL(@ASPCost,0)+ISNULL(@COKECost,0)+ISNULL(@SOLVCost,0)+ISNULL(@MPRODCost,0), 
	@YIELD_YTD = ISNULL(@PROD_YTD,0)+ISNULL(@RPF_YTD,0)+ISNULL(@ASP_YTD,0)+ISNULL(@COKE_YTD,0)+ISNULL(@SOLV_YTD,0)+ISNULL(@MPROD_YTD,0), 
	@YIELDCost_YTD = ISNULL(@PRODCost_YTD,0)+ISNULL(@RPFCost_YTD,0)+ISNULL(@ASPCost_YTD,0)+ISNULL(@COKECost_YTD,0)+ISNULL(@SOLVCost_YTD,0)+ISNULL(@MPRODCost_YTD,0)
	
SELECT @TotGrossMargin = @YIELDCost - @RMCost, @TotGrossMargin_YTD = @YIELDCost_YTD - @RMCost_YTD

SELECT @TotCashMargin = @TotGrossMargin-@TotCashOpex, @TotCashMargin_YTD = @TotGrossMargin_YTD-@TotCashOpex_YTD

SELECT UEDC = @UEDC,
	PurUtilUEDC = @PurUtilUEDC,
	RefUtilUEDC = @RefUtilUEDC,
	EDC = @EDC,
	UtilPcnt = @UtilPcnt,
	TotProcessUEDC = @TotProcessUEDC,
	TotProcessEDC = @TotProcessEDC,
	ProcessUtilPcnt = @ProcessUtilPcnt,
	MechUnavail_Act = @MechUnavail_Act,
	MechUnavailSlow_Act = @MechUnavailSlow_Act,
	OpUnavail_Act = @OpUnavail_Act,
	OpUnavailSlow_Act = @OpUnavailSlow_Act,
	OffStream_Act = @OffStream_Act,
	OffStreamSlow_Act = @OffStreamSlow_Act,
	ProcessUtilPcntCalc = @ProcessUtilPcntCalc,
	TotCashOpex = @TotCashOpex,
	TotCashOpex_YTD = @TotCashOpex_YTD,
	TotCashOpex_AVG = @TotCashOpex_AVG,
	TotCashOpexUEDC = @TotCashOpexUEDC,
	TotCashOpexUEDC_YTD = @TotCashOpexUEDC_YTD,
	TotCashOpexUEDC_AVG = @TotCashOpexUEDC_AVG,
	TotPurEnergy = @TotPurEnergy,
	TotPurEnergy_YTD = @TotPurEnergy_YTD,
	TotPurEnergy_AVG = @TotPurEnergy_AVG,
	TotPurEnergyUEDC = @TotPurEnergyUEDC,
	TotPurEnergyUEDC_YTD = @TotPurEnergyUEDC_YTD,
	TotPurEnergyUEDC_AVG = @TotPurEnergyUEDC_AVG,
	TotProdEnergy = @TotProdEnergy,
	TotProdEnergy_YTD = @TotProdEnergy_YTD,
	TotProdEnergy_AVG = @TotProdEnergy_AVG,
	TotProdEnergyUEDC = @TotProdEnergyUEDC,
	TotProdEnergyUEDC_YTD = @TotProdEnergyUEDC_YTD,
	TotProdEnergyUEDC_AVG = @TotProdEnergyUEDC_AVG,
	TotOthEnergy = @TotOthEnergy,
	TotOthEnergy_YTD = @TotOthEnergy_YTD,
	TotOthEnergy_AVG = @TotOthEnergy_AVG,
	TotOthEnergyUEDC = @TotOthEnergyUEDC,
	TotOthEnergyUEDC_YTD = @TotOthEnergyUEDC_YTD,
	TotOthEnergyUEDC_AVG = @TotOthEnergyUEDC_AVG,
	NEOpex = @NEOpex,
	NEOpex_YTD = @NEOpex_YTD,
	NEOpex_AVG = @NEOpex_AVG,
	NEOpexUEDC = @NEOpexUEDC,
	NEOpexUEDC_YTD = @NEOpexUEDC_YTD,
	NEOpexUEDC_AVG = @NEOpexUEDC_AVG,
	UEDC_YTD = @UEDC_YTD,
	UEDC_AVG = @UEDC_AVG,
	OCCPO_STH = @OCCPO_STH,
	OCCPO_OVT = @OCCPO_OVT,
	OCCPO_Contract = @OCCPO_Contract,
	OCCPO_GA = @OCCPO_GA,
	OCCPO_AbsHrs = @OCCPO_AbsHrs,
	OCCPO_TotWHr = @OCCPO_TotWHr,
	OCCPO_TotEqPEDC = @OCCPO_TotEqPEDC,
	OCCTAADJ_STH = @OCCTAADJ_STH,
	OCCTAADJ_OVT = @OCCTAADJ_OVT,
	OCCTAADJ_Contract = @OCCTAADJ_Contract,
	OCCTAADJ_GA = @OCCTAADJ_GA,
	OCCTAADJ_AbsHrs = @OCCTAADJ_AbsHrs,
	OCCTAADJ_TotWHr = @OCCTAADJ_TotWHr,
	OCCTAADJ_TotEqPEDC = @OCCTAADJ_TotEqPEDC,
	OCCMA_STH = @OCCMA_STH,
	OCCMA_OVT = @OCCMA_OVT,
	OCCMA_Contract = @OCCMA_Contract,
	OCCMA_GA = @OCCMA_GA,
	OCCMA_AbsHrs = @OCCMA_AbsHrs,
	OCCMA_TotWHr = @OCCMA_TotWHr,
	OCCMA_TotEqPEDC = @OCCMA_TotEqPEDC,
	OCCAS_STH = @OCCAS_STH,
	OCCAS_OVT = @OCCAS_OVT,
	OCCAS_Contract = @OCCAS_Contract,
	OCCAS_GA = @OCCAS_GA,
	OCCAS_AbsHrs = @OCCAS_AbsHrs,
	OCCAS_TotWHr = @OCCAS_TotWHr,
	OCCAS_TotEqPEDC = @OCCAS_TotEqPEDC,
	OCC_STH = @OCC_STH,
	OCC_OVT = @OCC_OVT,
	OCC_Contract = @OCC_Contract,
	OCC_GA = @OCC_GA,
	OCC_AbsHrs = @OCC_AbsHrs,
	OCC_TotWHr = @OCC_TotWHr,
	OCC_TotEqPEDC = @OCC_TotEqPEDC,
	MPSPO_STH = @MPSPO_STH,
	MPSPO_OVT = @MPSPO_OVT,
	MPSPO_Contract = @MPSPO_Contract,
	MPSPO_GA = @MPSPO_GA,
	MPSPO_AbsHrs = @MPSPO_AbsHrs,
	MPSPO_TotWHr = @MPSPO_TotWHr,
	MPSPO_TotEqPEDC = @MPSPO_TotEqPEDC,
	MPSTAADJ_STH = @MPSTAADJ_STH,
	MPSTAADJ_OVT = @MPSTAADJ_OVT,
	MPSTAADJ_Contract = @MPSTAADJ_Contract,
	MPSTAADJ_GA = @MPSTAADJ_GA,
	MPSTAADJ_AbsHrs = @MPSTAADJ_AbsHrs,
	MPSTAADJ_TotWHr = @MPSTAADJ_TotWHr,
	MPSTAADJ_TotEqPEDC = @MPSTAADJ_TotEqPEDC,
	MPSMA_STH = @MPSMA_STH,
	MPSMA_OVT = @MPSMA_OVT,
	MPSMA_Contract = @MPSMA_Contract,
	MPSMA_GA = @MPSMA_GA,
	MPSMA_AbsHrs = @MPSMA_AbsHrs,
	MPSMA_TotWHr = @MPSMA_TotWHr,
	MPSMA_TotEqPEDC = @MPSMA_TotEqPEDC,
	MPSTS_STH = @MPSTS_STH,
	MPSTS_OVT = @MPSTS_OVT,
	MPSTS_Contract = @MPSTS_Contract,
	MPSTS_GA = @MPSTS_GA,
	MPSTS_AbsHrs = @MPSTS_AbsHrs,
	MPSTS_TotWHr = @MPSTS_TotWHr,
	MPSTS_TotEqPEDC = @MPSTS_TotEqPEDC,
	MPSAS_STH = @MPSAS_STH,
	MPSAS_OVT = @MPSAS_OVT,
	MPSAS_Contract = @MPSAS_Contract,
	MPSAS_GA = @MPSAS_GA,
	MPSAS_AbsHrs = @MPSAS_AbsHrs,
	MPSAS_TotWHr = @MPSAS_TotWHr,
	MPSAS_TotEqPEDC = @MPSAS_TotEqPEDC,
	MPS_STH = @MPS_STH,
	MPS_OVT = @MPS_OVT,
	MPS_Contract = @MPS_Contract,
	MPS_GA = @MPS_GA,
	MPS_AbsHrs = @MPS_AbsHrs,
	MPS_TotWHr = @MPS_TotWHr,
	MPS_TotEqPEDC = @MPS_TotEqPEDC,
	AnnTACost = @AnnTACost,
	CurrRoutCost = @CurrRoutCost,
	TotMaintCost = @TotMaintCost,
	TAIndex = @TAIndex,
	RoutIndex = @RoutIndex,
	MaintIndex = @MaintIndex,
	AnnTACost_YTD = @AnnTACost_YTD,
	CurrRoutCost_YTD = @CurrRoutCost_YTD,
	TotMaintCost_YTD = @TotMaintCost_YTD,
	RoutIndex_YTD = @RoutIndex_YTD,
	MaintIndex_YTD = @MaintIndex_YTD,
	AnnTACost_AVG = @AnnTACost_AVG,
	CurrRoutCost_AVG = @CurrRoutCost_AVG,
	TotMaintCost_AVG = @TotMaintCost_AVG,
	RoutIndex_AVG = @RoutIndex_AVG,
	MaintIndex_AVG = @MaintIndex_AVG,
	EDC_YTD = @EDC_YTD,
	EDC_AVG = @EDC_AVG,
	RptSource_PUR = @RptSource_PUR,
	RptSource_PRO = @RptSource_PRO,
	RptSource_IN = @RptSource_IN,
	RptSource_OUT = @RptSource_OUT,
	RptSource_SOL = @RptSource_SOL,
	SourceMWH_PUR = @SourceMWH_PUR,
	SourceMWH_PRO = @SourceMWH_PRO,
	SourceMWH_IN = @SourceMWH_IN,
	SourceMWH_OUT = @SourceMWH_OUT,
	SourceMWH_SOL = @SourceMWH_SOL,
	TotEnergyConsMBTU = @TotEnergyConsMBTU,
	EnergyUseDay = @EnergyUseDay,
	ProcessStdEnergy = @ProcessStdEnergy,
	SensHeatStdEnergy = @SensHeatStdEnergy,
	OffsitesStdEnergy = @OffsitesStdEnergy,
	AspStdEnergy = @AspStdEnergy,
	TotStdEnergy = @TotStdEnergy,
	EII = @EII,
	RMI = @RMI,
	RMIPriceUS = @RMIPriceUS,
	RMICost = @RMICost,
	RMI_YTD = @RMI_YTD,
	RMIPriceUS_YTD = @RMIPriceUS_YTD,
	RMICost_YTD = @RMICost_YTD,
	OTHRM = @OTHRM,
	OTHRMPriceUS = @OTHRMPriceUS,
	OTHRMCost = @OTHRMCost,
	OTHRM_YTD = @OTHRM_YTD,
	OTHRMPriceUS_YTD = @OTHRMPriceUS_YTD,
	OTHRMCost_YTD = @OTHRMCost_YTD,
	RM = @RM,
	RMCost = @RMCost,
	RM_YTD = @RM_YTD,
	RMCost_YTD = @RMCost_YTD,
	NetInputBPD = @NetInputBPD,
	NetInputBPD_YTD = @NetInputBPD_YTD,
	PROD = @PROD,
	PRODPriceUS = @PRODPriceUS,
	PRODCost = @PRODCost,
	PROD_YTD = @PROD_YTD,
	PRODPriceUS_YTD = @PRODPriceUS_YTD,
	PRODCost_YTD = @PRODCost_YTD,
	RPF = @RPF,
	RPFPriceUS = @RPFPriceUS,
	RPFCost = @RPFCost,
	RPF_YTD = @RPF_YTD,
	RPFPriceUS_YTD = @RPFPriceUS_YTD,
	RPFCost_YTD = @RPFCost_YTD,
	ASP = @ASP,
	ASPPriceUS = @ASPPriceUS,
	ASPCost = @ASPCost,
	ASP_YTD = @ASP_YTD,
	ASPPriceUS_YTD = @ASPPriceUS_YTD,
	ASPCost_YTD = @ASPCost_YTD,
	SOLV = @SOLV,
	SOLVPriceUS = @SOLVPriceUS,
	SOLVCost = @SOLVCost,
	SOLV_YTD = @SOLV_YTD,
	SOLVPriceUS_YTD = @SOLVPriceUS_YTD,
	SOLVCost_YTD = @SOLVCost_YTD,
	COKE = @COKE,
	COKEPriceUS = @COKEPriceUS,
	COKECost = @COKECost,
	COKE_YTD = @COKE_YTD,
	COKEPriceUS_YTD = @COKEPriceUS_YTD,
	COKECost_YTD = @COKECost_YTD,
	MPROD = @MPROD,
	MPRODPriceUS = @MPRODPriceUS,
	MPRODCost = @MPRODCost,
	MPROD_YTD = @MPROD_YTD,
	MPRODPriceUS_YTD = @MPRODPriceUS_YTD,
	MPRODCost_YTD = @MPRODCost_YTD,
	YIELD = @YIELD,
	YIELDPriceUS = @YIELDPriceUS,
	YIELDCost = @YIELDCost,
	YIELD_YTD = @YIELD_YTD,
	YIELDPriceUS_YTD = @YIELDPriceUS_YTD,
	YIELDCost_YTD = @YIELDCost_YTD,
	GrossMargin = @GrossMargin,
	GrossMargin_YTD = @GrossMargin_YTD,
	TotGrossMargin = @TotGrossMargin,
	TotGrossMargin_YTD = @TotGrossMargin_YTD,
	TotCashOpexBbl = @TotCashOpexBbl,
	TotCashOpexBbl_YTD = @TotCashOpexBbl_YTD,
	TotCashMargin = @TotCashMargin,
	CashMargin = @CashMargin,
	TotCashMargin_YTD = @TotCashMargin_YTD,
	CashMargin_YTD = @CashMargin_YTD


