﻿CREATE    PROC [dbo].[spReportPUScorecardUnit] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency= @Currency, UOM = @UOM, 
p.SortKey, p.Description, c.UnitName, c.ProcessID, c.ProcessType, 
Cap = CASE WHEN @UOM = 'US' THEN c.Cap ELSE c.RptCap END, 
CapUnits = CASE WHEN @UOM = 'US' THEN d.DisplayTextUS WHEN s.UOM = 'US' THEN d.DisplayTextUS ELSE d.DisplayTextMet END,
ISNULL(fc.EDCNoMult,0) * ISNULL(fc.MultiFactor,1) as EDC, ISNULL(fc.UEDCNoMult,0) * ISNULL(fc.MultiFactor,1) as UEDC, 
fc.UtilPcnt, u.UtilPcnt as UtilPcnt_Target,
m.MechAvail_Ann as MechAvail, u.MechAvail as MechAvail_Target, 
m.OpAvail_Ann as OpAvail, u.OpAvail as OpAvail_Target,
m.OnStream_Ann as OnStream, u.OnStream as OnStream_Target,
CASE WHEN c.RptCap > 0 THEN 1000*AnnTACost/CASE WHEN @UOM = 'US' THEN c.Cap ELSE c.RptCap END END as TACost, TACost as TACost_Target, 
CASE WHEN c.RptCap > 0 THEN 1000*AnnRoutCost/CASE WHEN @UOM = 'US' THEN c.Cap ELSE c.RptCap END END as RoutCost, RoutCost as RoutCost_Target, u.CurrencyCode,
fc.UnitEII, u.UnitEII as UnitEII_Target, 
ISNULL(p.DescriptionRussian, p.Description) AS DescriptionRussian,
CapUnitsRussian = CASE WHEN @UOM = 'US' THEN ISNULL(d.DisplayTextUSRussian, d.DisplayTextUS) WHEN s.UOM = 'US' THEN ISNULL(d.DisplayTextUSRussian,d.DisplayTextUS) ELSE ISNULL(d.DisplayTextMetRussian,d.DisplayTextMet) END
FROM Submissions s INNER JOIN Config c ON c.SubmissionID = s.SubmissionID
INNER JOIN ProcessID_LU p on c.ProcessID = p.ProcessID 
INNER JOIN FactorCalc fc on fc.SubmissionID = c.SubmissionID and c.UnitID = fc.UnitID 
INNER JOIN FactorProcessCalc fpc on fc.SubmissionID = fpc.SubmissionID and fpc.ProcessID = c.ProcessID AND fc.FactorSet = fpc.FactorSet
INNER JOIN MaintCalc m on m.SubmissionID = c.SubmissionID and c.UnitID = m.UnitID 
INNER JOIN MaintCost mc on mc.SubmissionID = c.SubmissionID and c.UnitID = mc.UnitID 
INNER JOIN DisplayUnits_LU d on p.DisplayUnits = d.DisplayUnits 
LEFT JOIN UnitTargets u on c.SubmissionID = u.SubmissionID and c.UnitID = u.UnitID 
WHERE fc.FactorSet=@FactorSet AND mc.Currency = @Currency 
AND s.RefineryID=@RefineryID AND s.DataSet = @DataSet AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.UseSubmission = 1
ORDER BY s.PeriodStart DESC, p.SortKey



