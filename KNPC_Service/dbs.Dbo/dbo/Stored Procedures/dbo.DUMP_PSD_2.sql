﻿CREATE PROCEDURE [dbo].[DUMP_PSD_2]

	@RefNum nvarchar(10),
	@DataSetID nvarchar(10),
	@CapColName nvarchar(10)
	
	AS
	DECLARE @SQL as nvarchar(2000)
	
	SET @SQL = 'SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,s.RptCurrency as Currency,s.UOM, 
                                    c.UnitID,c.UnitName, c.ProcessID, c.ProcessType, ISNULL(c.' + @CapColName + ' ,0) AS ' + @CapColName + '  , ISNULL(c.UtilPcnt,0) AS UtilPcnt,  
                                    ISNULL(EDCNoMult,0) AS EDC, ISNULL(UEDCNoMult,0) AS UEDC, d.DisplayTextUS, d.DisplayTextMET  
                                    FROM config c,factorcalc fc, processid_lu p ,displayunits_lu d,Submissions s  
                                     WHERE c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits AND  
                                    c.unitid = fc.unitid AND c.submissionid = fc.submissionid AND  
                                    c.ProcessID IN (''STEAMGEN'', ''ELECGEN'', ''FCCPOWER'')  
                                     AND fc.factorset= + studyYear.ToString +  AND s.SubmissionID=c.SubmissionID AND c.SubmissionId IN  
                                    (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=' + @DataSetID + ' AND  
                                     RefineryID=' + @RefNum + ') ORDER BY PeriodStart DESC'
                                     
                                     EXEC @SQL


