﻿
CREATE FUNCTION [dbo].[GetUnitAvailability](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT m.UnitID, m.FactorSet, m.PeriodHrs, PeriodHrsOSTA = CASE WHEN (m.PeriodHrs-m.TADown) > 0 THEN (m.PeriodHrs-m.TADown) ELSE 0 END, m.PeriodEDC,
	MechUnavailTA_Act = ISNULL(m.TADown, 0)/m.PeriodHrs * 100, 
	MechAvailOSTA = CASE WHEN (m.PeriodHrs-m.TADown) > 0 THEN (1 - ISNULL(m.MaintDown, 0)/(m.PeriodHrs-m.TADown)) * 100 END,
	MechAvail_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs) * 100, 
	MechAvailSlow_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.MaintSlow, 0))/m.PeriodHrs) * 100, 
	OpAvail_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0))/m.PeriodHrs) * 100,
	OpAvailSlow_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0))/m.PeriodHrs) * 100,
	OnStream_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0))/m.PeriodHrs) * 100,
	OnStreamSlow_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0) + ISNULL(m.OthSlow, 0))/m.PeriodHrs) * 100,  
	MechUnavailTA_Ann = ISNULL(m.MechUnavailTA, 0),
	MechAvail_Ann = (1 - (ISNULL(m.MaintDown, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
	MechAvailSlow_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.MaintSlow, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
	OpAvail_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
	OpAvailSlow_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
	OnStream_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
	OnStreamSlow_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0) + ISNULL(m.OthSlow, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
	MechUnavailPlan = (ISNULL(m.MaintDown, 0) - ISNULL(m.UnpMaintDown, 0))/m.PeriodHrs * 100,
	MechUnavailUnp = ISNULL(m.UnpMaintDown, 0)/m.PeriodHrs * 100,
	MechUnavail_Ann = ISNULL(m.MaintDown, 0)/m.PeriodHrs*100 + ISNULL(m.MechUnavailTA, 0),
	MechUnavail_Act = (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs * 100,  
	RegUnavail = ISNULL(m.RegDown, 0)/m.PeriodHrs * 100,
	RegUnavailPlan = (ISNULL(m.RegDown, 0) - ISNULL(m.UnpRegDown, 0))/m.PeriodHrs * 100,
	RegUnavailUnp = ISNULL(m.UnpRegDown, 0)/m.PeriodHrs * 100,
	OpUnavail_Ann = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0))/m.PeriodHrs*100 + ISNULL(m.MechUnavailTA, 0),
	OpUnavail_Act = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs * 100, 
	OthUnavailEconomic = ISNULL(m.OthDownEconomic,0)/m.PeriodHrs*100, 
	OthUnavailExternal = ISNULL(m.OthDownExternal,0)/m.PeriodHrs*100, 
	OthUnavailUnitUpsets = ISNULL(m.OthDownUnitUpsets,0)/m.PeriodHrs*100, 
	OthUnavailOffsiteUpsets = ISNULL(m.OthDownOffsiteUpsets,0)/m.PeriodHrs*100, 
	OthUnavailOther = ISNULL(m.OthDownOther,0)/m.PeriodHrs*100, 
	OthUnavail = ISNULL(m.OthDown,0)/m.PeriodHrs*100, 
	OthUnavailPlan = (ISNULL(m.OthDown, 0) - ISNULL(m.UnpOthDown, 0))/m.PeriodHrs * 100,
	OthUnavailUnp = ISNULL(m.UnpOthDown, 0)/m.PeriodHrs * 100,
	TotUnavail_Ann = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0))/m.PeriodHrs*100 + ISNULL(m.MechUnavailTA, 0),
	TotUnavail_Act = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs * 100, 
	TotUnavailUnp = (ISNULL(m.UnpMaintDown, 0) + ISNULL(m.UnpRegDown, 0) + ISNULL(m.UnpOthDown, 0))/m.PeriodHrs * 100
	FROM dbo.GetDowntimes(@RefineryID, @Dataset, @StartDate, @EndDate) m
)



