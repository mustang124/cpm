﻿CREATE  FUNCTION [dbo].[CurrencyConvRate](@CurrencyCode CurrencyCode, @StartDate smalldatetime)
RETURNS real
AS
BEGIN
	DECLARE @ConvRate real
	SELECT @ConvRate = ConvRate FROM CurrencyConv 
	WHERE CurrencyCode = @CurrencyCode AND [Month] = DATEPART(mm, @StartDate) AND [Year] = DATEPART(yy, @StartDate)

	RETURN @ConvRate
END

