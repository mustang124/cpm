﻿CREATE FUNCTION [dbo].[GetAvgOCCMPSRatio](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @OCCSection char(2), @MPSSection char(2))
RETURNS real
AS
BEGIN
	DECLARE @OCCMPSRatio real
	SELECT @OCCMPSRatio = SUM(o.NumPers)/SUM(m.NumPers)
	FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartDate, @EndDate) s 
	INNER JOIN PersST o ON o.SubmissionID = s.SubmissionID AND o.SectionID = @OCCSection
	INNER JOIN PersST m ON m.SubmissionID = s.SubmissionID AND m.SectionID = @MPSSection AND m.NumPers > 0
	
	RETURN @OCCMPSRatio
END

