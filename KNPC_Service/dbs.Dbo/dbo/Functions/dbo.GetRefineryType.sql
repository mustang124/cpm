﻿CREATE FUNCTION [dbo].[GetRefineryType](@RefineryID varchar(6))
RETURNS varchar(5)
AS
BEGIN
	DECLARE @RefineryType varchar(5)
	SELECT @RefineryType = CASE
		WHEN Study = 'LUB' THEN 'LUBES'
		ELSE 'FUELS'
	END		
	FROM TSort WHERE RefineryID = @RefineryID

	RETURN @RefineryType
END



