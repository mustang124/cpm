﻿CREATE FUNCTION [dbo].[GetPeriods](@SubmissionID int)
RETURNS TABLE
AS
RETURN (
	SELECT PeriodStart, PeriodEnd
		, Start3Mo = DATEADD(mm, -3, PeriodEnd)
		, Start12Mo = DATEADD(mm, -12, PeriodEnd)
		, StartYTD = dbo.BuildDate(DATEPART(yy, PeriodStart), 1, 1)
		, Start24Mo = DATEADD(mm, -24, PeriodEnd)
		, StartQTR = dbo.BuildDate(DATEPART(yy, DATEADD(dd, -1, PeriodEnd)), DATEPART(QUARTER, DATEADD(dd, -1, PeriodEnd))*3-2, 1)
		, EndQTR = DATEADD(mm, 3, dbo.BuildDate(DATEPART(yy, DATEADD(dd, -1, PeriodEnd)), DATEPART(QUARTER, DATEADD(dd, -1, PeriodEnd))*3-2, 1))
	FROM dbo.SubmissionsAll
	WHERE SubmissionID = @SubmissionID 
	)


