﻿
CREATE FUNCTION [dbo].[SLAvailability](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet varchar(8))
RETURNS TABLE
AS

	RETURN (
		WITH avail AS (	
			SELECT s.RefineryID, s.DataSet, s.PeriodStart, m.MechUnavailTA_Ann, MechUnavailRout = 100 - m.MechAvail_Ann - m.MechUnavailTA_Ann, RegUnavail = m.MechAvail_Ann - m.OpAvail_Ann, OthUnavail = m.OpAvail_Ann - m.OnStream_Ann, m.MechUnavailTA_Act
				, f.EDC * s.FractionOfYear AS WtFactor
			FROM MaintAvailCalc m INNER JOIN dbo.ProcessFactors f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
			INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = m.SubmissionID
			INNER JOIN @SubmissionList sl ON sl.SubmissionID = m.SubmissionID
			WHERE m.FactorSet = @FactorSet
		) 
		SELECT MechAvail_Ann = [$(dbsGlobal)].[dbo].WtAvg(100 - t.MechUnavailTA_Ann - a.MechUnavailRout, a.WtFactor)
			, OpAvail_Ann = [$(dbsGlobal)].[dbo].WtAvg(100 - t.MechUnavailTA_Ann - a.MechUnavailRout - a.RegUnavail, a.WtFactor)
			, OnStream_Ann = [$(dbsGlobal)].[dbo].WtAvg(100 - t.MechUnavailTA_Ann - a.MechUnavailRout - a.RegUnavail - a.OthUnavail, a.WtFactor)
			, MechAvail_Act = [$(dbsGlobal)].[dbo].WtAvg(100 - a.MechUnavailTA_Act - a.MechUnavailRout, a.WtFactor)
			, OpAvail_Act = [$(dbsGlobal)].[dbo].WtAvg(100 - a.MechUnavailTA_Act - a.MechUnavailRout - a.RegUnavail, a.WtFactor)
			, OnStream_Act = [$(dbsGlobal)].[dbo].WtAvg(100 - a.MechUnavailTA_Act - a.MechUnavailRout - a.RegUnavail - a.OthUnavail, a.WtFactor)
			, MechUnavailTA_Ann = [$(dbsGlobal)].[dbo].WtAvg(t.MechUnavailTA_Ann, a.WtFactor)
			, MechUnavailRout = [$(dbsGlobal)].[dbo].WtAvg(a.MechUnavailRout, a.WtFactor)
			, RegUnavail = [$(dbsGlobal)].[dbo].WtAvg(a.RegUnavail, a.WtFactor)
			, OthUnavail = [$(dbsGlobal)].[dbo].WtAvg(a.OthUnavail, a.WtFactor)
			, MechUnavailTA_Act = [$(dbsGlobal)].[dbo].WtAvg(a.MechUnavailTA_Act, a.WtFactor)
		FROM avail a INNER JOIN avail t ON t.RefineryID = a.RefineryID AND t.DataSet = a.DataSet AND t.PeriodStart = (SELECT MAX(PeriodStart) FROM avail WHERE avail.RefineryID = a.RefineryID AND avail.DataSet = a.DataSet)
	)

	
