﻿CREATE TABLE [dbo].[RefTargets] (
    [SubmissionID] INT                  NOT NULL,
    [Property]     VARCHAR (50)         NOT NULL,
    [Target]       REAL                 NULL,
    [CurrencyCode] [dbo].[CurrencyCode] NULL,
    CONSTRAINT [PK_RefTargets] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Property] ASC) WITH (FILLFACTOR = 90)
);

