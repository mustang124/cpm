﻿CREATE TABLE [dbo].[ReadyForCalcs] (
    [RefineryID]   CHAR (6)      NOT NULL,
    [DataSet]      VARCHAR (15)  NOT NULL,
    [CalcsStarted] BIT           CONSTRAINT [DF_ReadyForCalcs_CalcsStarted] DEFAULT ((0)) NOT NULL,
    [Uploading]    BIT           CONSTRAINT [DF_ReadyForCalcs_Uploading] DEFAULT ((0)) NOT NULL,
    [LastUpdate]   SMALLDATETIME NULL,
    CONSTRAINT [PK_ReadyForCalcs] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [DataSet] ASC) WITH (FILLFACTOR = 90)
);

