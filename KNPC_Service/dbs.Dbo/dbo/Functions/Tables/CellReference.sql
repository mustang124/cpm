﻿CREATE TABLE [dbo].[CellReference](
	
	[ID] [int] NOT NULL,
	[RefineryId][int] NOT NULL,
	[TableNo] [int] NOT NULL,
	[TableName]	[nvarchar] (50) NOT NULL,
	[ValueColumnName] [nvarchar] (50) NOT NULL,
	[ReferenceColumnName] [nvarchar] (50) NOT NULL,
	[ReferenceName]	[nvarchar] (50) NOT NULL,
	[ValueFilterColumnName]	[nvarchar] (50) NULL,
	[ValueFilter] [nvarchar] (50) NULL,
	[Description] [nvarchar] (Max) NULL,
	[HelpText] [nvarchar] (50) NULL,
	[LowLimit] [float] NULL,
	[HighLimit] [float] NULL

)
