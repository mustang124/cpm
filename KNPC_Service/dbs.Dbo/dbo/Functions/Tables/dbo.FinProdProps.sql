﻿CREATE TABLE [dbo].[FinProdProps](
	[SubmissionId] [int] NOT NULL,
	[MaterialId] [varchar](5) NOT NULL,
	[BlendId] [smallint] NOT NULL,
	[PropertyId] [int] NOT NULL,
	[RptNVal] [real] NULL,
	[RptTVal] [varchar] (20) NULL,
	[RptDVal] [smalldatetime] NULL, 
    CONSTRAINT [PK_FinProdProps] PRIMARY KEY ([SubmissionId], [MaterialId], [BlendId], [PropertyId])
) 