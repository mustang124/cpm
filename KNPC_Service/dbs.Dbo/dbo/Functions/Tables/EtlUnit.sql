﻿CREATE TABLE [dbo].[EtlUnit](
	[EtlUnitId] [int] IDENTITY(1,1) NOT NULL,
	[DataKey] [varchar](60) NOT NULL,
	[RptDatabase] [varchar](20) NOT NULL DEFAULT ('Refining14'),
	[RptSchema] [varchar](8) NOT NULL DEFAULT ('dbo'),
	[RptTable] [varchar](40) NOT NULL,
	[RptColumn] [varchar](30) NOT NULL,
	[WhereClause] [varchar](98) NULL,
PRIMARY KEY CLUSTERED 
(
	[EtlUnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[EtlUnit]  WITH CHECK ADD CHECK  (([DataKey]<>''))
GO

ALTER TABLE [dbo].[EtlUnit]  WITH CHECK ADD CHECK  (([RptColumn]<>''))
GO

ALTER TABLE [dbo].[EtlUnit]  WITH CHECK ADD CHECK  (([RptDatabase]<>''))
GO

ALTER TABLE [dbo].[EtlUnit]  WITH CHECK ADD CHECK  (([RptSchema]<>''))
GO

ALTER TABLE [dbo].[EtlUnit]  WITH CHECK ADD CHECK  (([RptTable]<>''))
GO

ALTER TABLE [dbo].[EtlUnit]  WITH CHECK ADD CHECK  (([WhereClause]<>''))
GO

