﻿CREATE TABLE [dbo].[EnergyCalc] (
    [SubmissionID]   INT                     NOT NULL,
    [Scenario]       [dbo].[Scenario]        NOT NULL,
    [TransType]      [dbo].[EnergyTransType] NOT NULL,
    [EnergyType]     [dbo].[EnergyType]      NOT NULL,
    [MBTU]           FLOAT (53)              NULL,
    [CostMBTU]       REAL                    NULL,
    [ProductRef]     CHAR (15)               NULL,
    [CostBbl]        REAL                    NULL,
    [CostMT]         REAL                    NULL,
    [CostKUS]        REAL                    NULL,
    [PercentOfTotal] REAL                    NULL,
    [MBTUPerUEDC]    REAL                    NULL,
    [KBTUPerBbl]     REAL                    NULL,
    [CostMBTULocal]  REAL                    NULL,
    CONSTRAINT [PK_EnergyCalc_BU_1__14] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [TransType] ASC, [EnergyType] ASC) WITH (FILLFACTOR = 90)
);

