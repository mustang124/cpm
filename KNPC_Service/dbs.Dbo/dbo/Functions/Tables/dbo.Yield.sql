﻿CREATE TABLE [dbo].[Yield](
	[SubmissionID] [int] NOT NULL,
	[SortKey] [int] NOT NULL,
	[Category] [dbo].[YieldCategory] NOT NULL,
	[MaterialID] [char](5) NOT NULL,
	[MaterialName] [nvarchar](50) NOT NULL CONSTRAINT [DF_Yield_MaterialName]  DEFAULT (''),
	[BBL] [float] NOT NULL CONSTRAINT [DF_Yield_BBL]  DEFAULT (0),
	[Gravity] [real] NULL,
	[Density] [real] NULL,
	[MT] [float] NULL,
	[PriceLocal] [real] NOT NULL CONSTRAINT [DF_Yield_PriceLocal]  DEFAULT (0),
	[PriceUS] [real] NULL,
 CONSTRAINT [PK_Yield] PRIMARY KEY CLUSTERED 
(
	[SubmissionID] ASC,
	[SortKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]


GO
CREATE NONCLUSTERED INDEX [YieldByCategory]
    ON [dbo].[Yield]([Category] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [YieldByMaterialID]
    ON [dbo].[Yield]([MaterialID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [YieldSubmissionIDCat]
    ON [dbo].[Yield]([SubmissionID] ASC, [Category] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [YieldSubmissionIDMat]
    ON [dbo].[Yield]([SubmissionID] ASC, [MaterialID] ASC) WITH (FILLFACTOR = 90);

