﻿CREATE TABLE [dbo].[Config] (
    [SubmissionID]     INT                 NOT NULL,
    [UnitID]           [dbo].[UnitID]      NOT NULL,
    [SortKey]          SMALLINT            NOT NULL,
    [ProcessID]        [dbo].[ProcessID]   NOT NULL,
    [UnitName]         NVARCHAR (40)       NOT NULL,
    [ProcessType]      [dbo].[ProcessType] NOT NULL,
    [Cap]              REAL                NULL,
    [UtilPcnt]         NUMERIC (5, 2)      NULL,
    [StmCap]           REAL                NULL,
    [StmUtilPcnt]      NUMERIC (5, 2)      NULL,
    [InServicePcnt]    NUMERIC (5, 2)      CONSTRAINT [DF_Config_InServicePcnt_1__13] DEFAULT ((100)) NULL,
    [YearsOper]        NUMERIC (5, 2)      NULL,
    [MHPerWeek]        REAL                NULL,
    [PostPerShift]     NUMERIC (9, 3)      NULL,
    [BlockOp]          VARCHAR (6)         NULL,
    [ControlType]      CHAR (4)            NULL,
    [CapClass]         [dbo].[Class]       NULL,
    [UtilCap]          REAL                NULL,
    [StmUtilCap]       REAL                NULL,
    [AllocPcntOfTime]  NUMERIC (5, 2)      CONSTRAINT [DF_Config_AllocPcntOfTime] DEFAULT ((100)) NULL,
    [AllocPcntOfCap]   NUMERIC (5, 2)      CONSTRAINT [DF_Config_AllocPcntOfCap] DEFAULT ((100)) NULL,
    [AllocUtilPcnt]    NUMERIC (5, 2)      NULL,
    [ActualCap]        REAL                NULL,
    [ActualStmCap]     REAL                NULL,
    [InserviceCap]     REAL                NULL,
    [InserviceStmCap]  REAL                NULL,
    [ModePcnt]         NUMERIC (5, 2)      NULL,
    [UnusedCapPcnt]    REAL                NULL,
    [RptCap]           REAL                NULL,
    [RptCapUOM]        [dbo].[UOM]         NULL,
    [RptStmCap]        REAL                NULL,
    [RptStmCapUOM]     [dbo].[UOM]         NULL,
    [DesignFeedSulfur] REAL                NULL,
    [EnergyPcnt]       REAL                NULL,
    [ActualMFCap]      REAL                NULL,
    [InserviceMFCap]   REAL                NULL,
    [MFCap]            REAL                NULL,
    [MFUtilCap]        REAL                NULL,
    [MFUtilPcnt]       REAL                NULL,
    CONSTRAINT [PK_Config_1__14] PRIMARY KEY NONCLUSTERED ([SubmissionID] ASC, [UnitID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE CLUSTERED INDEX [ConfigSortKey]
    ON [dbo].[Config]([SubmissionID] ASC, [SortKey] ASC, [UnitID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ConfigByProcess]
    ON [dbo].[Config]([SubmissionID] ASC, [ProcessID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ConfigByProcType]
    ON [dbo].[Config]([SubmissionID] ASC, [ProcessID] ASC, [ProcessType] ASC) WITH (FILLFACTOR = 90);

