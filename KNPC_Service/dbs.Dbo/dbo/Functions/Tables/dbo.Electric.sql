﻿CREATE TABLE [dbo].[Electric] (
    [SubmissionID] INT                     NOT NULL,
    [TransCode]    SMALLINT                NOT NULL,
    [EnergyType]   [dbo].[EnergyType]      NOT NULL,
    [TransType]    [dbo].[EnergyTransType] NOT NULL,
    [TransferTo]   CHAR (3)                NULL,
    [RptMWH]       REAL                    CONSTRAINT [DF_Electric_RptMWH] DEFAULT ((0)) NOT NULL,
    [PriceLocal]   REAL                    CONSTRAINT [DF_Electric_PriceLocal] DEFAULT ((0)) NOT NULL,
    [RptGenEff]    REAL                    NULL,
    [SourceMWH]    FLOAT (53)              NULL,
    [UsageMWH]     FLOAT (53)              NULL,
    [PriceUS]      REAL                    NULL,
    [GenEff]       REAL                    NULL,
    CONSTRAINT [PK_Electric_1] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [TransCode] ASC) WITH (FILLFACTOR = 90)
);

