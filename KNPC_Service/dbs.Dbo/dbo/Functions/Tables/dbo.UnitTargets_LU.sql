﻿CREATE TABLE [dbo].[UnitTargets_LU] (
    [ProcessID]      VARCHAR (10)  NOT NULL,
    [Property]       VARCHAR (50)  NOT NULL,
    [SortKey]        SMALLINT      CONSTRAINT [DF_UnitTargets_LU_SortKey] DEFAULT ((10000)) NOT NULL,
    [USDescription]  VARCHAR (100) NOT NULL,
    [USDecPlaces]    TINYINT       CONSTRAINT [DF_UnitTargets_LU_USDecPlaces] DEFAULT ((1)) NOT NULL,
    [MetDescription] VARCHAR (100) CONSTRAINT [DF_UnitTargets_LU_MetDescription] DEFAULT ('') NOT NULL,
    [MetDecPlaces]   TINYINT       CONSTRAINT [DF_UnitTargets_LU_MetDecPLaces] DEFAULT ((1)) NOT NULL,
    [USUOM]          VARCHAR (20)  NULL,
    [MetricUOM]      VARCHAR (20)  NULL,
    [CustomGroup]    INT           CONSTRAINT [DF_UnitTargets_LU_CustomGroup] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_UnitTargets_LU] PRIMARY KEY CLUSTERED ([ProcessID] ASC, [Property] ASC) WITH (FILLFACTOR = 90)
);

