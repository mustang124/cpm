﻿
CREATE FUNCTION [dbo].[GetFCCCOBlrFiredCap](@SubmissionID int, @UnitID int)
RETURNS real
AS BEGIN
	DECLARE @COBlrFiredCap real
	SELECT @COBlrFiredCap = COBlrFiredCap
	FROM dbo.StudyT2_FCC t2 INNER JOIN dbo.SubmissionsAll s ON s.RefineryID = t2.RefineryID
	WHERE s.SubmissionID = @SubmissionID AND t2.UnitID = @UnitID
	AND s.PeriodStart >= t2.EffDate AND s.PeriodStart < t2.EffUntil
	
	IF @COBlrFiredCap IS NULL
		SELECT @COBlrFiredCap = 0
	RETURN @COBlrFiredCap
END



