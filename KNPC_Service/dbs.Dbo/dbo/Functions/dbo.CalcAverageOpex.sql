﻿CREATE  FUNCTION [dbo].[CalcAverageOpex](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode)
RETURNS @Opex TABLE (FactorSet varchar(8), Scenario dbo.Scenario, Currency dbo.CurrencyCode, TotCashOpexUEDC real NULL, VolOpexUEDC real NULL, NonVolOpexUEDC real NULL, 
	NEOpexUEDC real NULL, NEOpexEDC real NULL, NEI real NULL)
AS
BEGIN

DECLARE @OpexScenario dbo.Scenario; SET @OpexScenario = 'CLIENT' -- Using Client energy prices for all pricing scenarios

INSERT INTO @Opex (FactorSet, Scenario, Currency, NEOpexEDC, TotCashOpexUEDC, VolOpexUEDC, NonVolOpexUEDC, NEOpexUEDC, NEI)
SELECT e.FactorSet, e.Scenario, e.Currency, e.NEOpex, u.TotCashOpex, u.STVol, u.STNonVol, u.NEOpex, nei.NEOpex
FROM dbo.CalcAverageOpexByDataType(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet, @OpexScenario, @Currency, 'EDC') e
LEFT JOIN dbo.CalcAverageOpexByDataType(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet, @OpexScenario, @Currency, 'UEDC') u ON u.FactorSet = e.FactorSet AND u.Scenario = e.Scenario AND u.Currency = e.Currency
LEFT JOIN dbo.CalcAverageOpexByDataType(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet, @OpexScenario, @Currency, 'NEI') nei ON nei.FactorSet = e.FactorSet AND nei.Scenario = e.Scenario AND nei.Currency = e.Currency

RETURN
END

