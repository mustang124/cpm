﻿
CREATE FUNCTION [dbo].[SLSumPersTot](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT NonMaintEqP = SUM(NonMaintEqP), NonMaintWHr = SUM(NonMaintWHr), TotMaintForceWHr = SUM(TotMaintForceWHr), MaintForceCompWHr = SUM(MaintForceCompWHr), MaintForceContWHr = SUM(MaintForceContWHr)
		, ProcessWHr = SUM(ProcessWHr), MaintWHr = SUM(MaintWHr), AdminWHr = SUM(AdminWHr), CompWHr = SUM(CompWHr)
		, OCCCompNonTASTH = SUM(OCCCompNonTASTH), MPSCompNonTASTH = SUM(MPSCompNonTASTH), TotCompNonTASTH = SUM(TotCompNonTASTH)
		, ContMaintLaborWHr = SUM(ContMaintLaborWHr), OthContWHr = SUM(OthContWHr)
		, OCCNumPers = SUM(OCCNumPers), MPSNumPers = SUM(MPSNumPers), TotNumPers = SUM(TotNumPers)
		, CompOCCNonMaintWHr = SUM(CompOCCNonMaintWHr), CompMPSNonMaintWHr = SUM(CompMPSNonMaintWHr), CompNonMaintWHr = SUM(CompNonMaintWHr), ContNonMaintWHr = SUM(ContNonMaintWHr), GANonMaintWHr = SUM(GANonMaintWHr), TotNonMaintWHr = SUM(TotNonMaintWHr)
		, CompOCCNonTAWHr = SUM(CompOCCNonTAWHr), CompMPSNonTAWHr = SUM(CompMPSNonTAWHr), CompNonTAWHr = SUM(CompNonTAWHr), ContNonTAWHr = SUM(ContNonTAWHr), GANonTAWHr = SUM(GANonTAWHr), TotNonTAWHr = SUM(TotNonTAWHr)
	FROM PersTot a INNER JOIN @SubmissionList s ON s.SubmissionID = a.SubmissionID   
)


