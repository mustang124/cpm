﻿CREATE   FUNCTION [dbo].[CalcAverageEnergyCost](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS @AvgEnergyCost TABLE (Currency varchar(4), TotCostMBTU real NULL, PurCostMBTU real NULL, ProdCostMBTU real NULL, TotCostGJ real NULL, PurCostGJ real NULL, ProdCostGJ real NULL)
AS
BEGIN
	-- Use client energy prices for all pricing scenarios
	INSERT @AvgEnergyCost (Currency, TotCostMBTU, PurCostMBTU, ProdCostMBTU)
	SELECT c.Currency, TotCostMBTU = CASE WHEN m.TotEnergyConsMBTU > 0 THEN c.TotCostK*1000/m.TotEnergyConsMBTU END
		, PurCostMBTU = CASE WHEN m.PurTotMBTU > 0 THEN c.PurTotCostK*1000/m.PurTotMBTU END
		, ProdCostMBTU = CASE WHEN m.ProdTotMBTU > 0 THEN c.ProdTotCostK*1000/m.ProdTotMBTU END
	FROM dbo.SumEnergyCons(@RefineryID, @DataSet, @StartDate, @EndDate) m, dbo.SumEnergyCost(@RefineryID, @DataSet, @StartDate, @EndDate) c
	
	UPDATE @AvgEnergyCost SET TotCostGJ = TotCostMBTU/1.055, PurCostGJ = PurCostMBTU/1.055, ProdCostGJ = ProdCostMBTU/1.055

RETURN

END

