﻿CREATE FUNCTION [dbo].[GetSubmission](@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15))
RETURNS TABLE
AS
RETURN (
	SELECT SubmissionID, PeriodStart, PeriodEnd, NumDays, FractionOfYear, RptCurrency, RptCurrencyT15, UOM, CalcsNeeded, Submitted, LastCalc
	FROM dbo.Submissions
	WHERE RefineryID = @RefineryID AND Dataset = @DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND UseSubmission = 1
	)


