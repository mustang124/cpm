﻿

CREATE FUNCTION [dbo].[SLSumCrudes](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT c.CNum, Bbl = SUM(Bbl), TotMT = SUM(MT)
		, Density = [$(dbsGlobal)].[dbo].WtAvg(Density, MT)
		, Gravity = dbo.KGM3toAPI([$(dbsGlobal)].[dbo].WtAvg(Density, MT))
		, Sulfur = [$(dbsGlobal)].[dbo].WtAvgNZ(Sulfur,MT)
	FROM Crude c INNER JOIN @SubmissionList s ON s.SubmissionID = c.SubmissionID
	GROUP BY c.CNum
)


