﻿CREATE FUNCTION [dbo].[SLSumEnergyCost](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT Currency
		, PurFGCostK = SUM(PurFGCostK)
		, PurLiquidCostK = SUM(PurLiquidCostK)
		, PurSolidCostK = SUM(PurSolidCostK)
		, PurSteamCostK = SUM(PurSteamCostK)
		, PurThermCostK = SUM(PurThermCostK)
		, PurPowerCostK = SUM(PurPowerCostK)
		, PurTotCostK = SUM(PurTotCostK)
		, ProdFGCostK = SUM(ProdFGCostK)
		, ProdOthCostK = SUM(ProdOthCostK)
		, ProdTotCostK = SUM(ProdTotCostK)
		, TotCostK = SUM(TotCostK)
	FROM dbo.EnergyTotCost c INNER JOIN @SubmissionList s ON s.SubmissionID = c.SubmissionID
	WHERE c.Scenario = 'CLIENT'
	GROUP BY Currency
)


