﻿
CREATE FUNCTION [dbo].[GetCurrentFactorSet]() 
RETURNS dbo.FactorSet
AS
BEGIN
	DECLARE @FactorSet dbo.FactorSet
	SELECT @FactorSet = FactorSet 
	FROM dbo.FactorSets WHERE CurrentFactors = 'Y'
	RETURN @FactorSet
END



