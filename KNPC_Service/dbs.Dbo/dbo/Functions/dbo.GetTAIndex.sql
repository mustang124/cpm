﻿CREATE FUNCTION [dbo].[GetTAIndex](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @FactorSet varchar(8), @Currency char(4))
RETURNS TABLE
AS
RETURN (
	SELECT FactorSet, Currency, TAIndex = TAIndex_Avg, TAMatlIndex = TAMatlIndex_Avg, TAEffIndex = TAEffIndex_Avg
	FROM MaintIndex
	WHERE SubmissionID = (SELECT TOP 1 SubmissionID FROM dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodStart < @EndDate AND UseSubmission = 1 ORDER BY PeriodStart DESC)
	AND FactorSet = ISNULL(@FactorSet, FactorSet) AND Currency = ISNULL(@Currency, Currency)
)


