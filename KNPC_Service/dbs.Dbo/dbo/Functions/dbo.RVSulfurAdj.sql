﻿CREATE FUNCTION [dbo].[RVSulfurAdj](@DesignSulfur AS real, @BaseSulfur AS real, @SulfurAdj char(1))
RETURNS real
AS
BEGIN
	DECLARE @Adj real
	SELECT @Adj = 1
	IF @SulfurAdj = 'Y' AND @DesignSulfur > 0 AND @BaseSulfur > 0 
	BEGIN
		SELECT @Adj = dbo.RVSulfurValue(@DesignSulfur) / dbo.RVSulfurValue(@BaseSulfur)
	END
	RETURN @Adj
END

