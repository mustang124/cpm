﻿CREATE FUNCTION [dbo].[SLTAAdj](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet FactorSet, @Currency CurrencyCode)
RETURNS @TAAdj TABLE (FactorSet varchar(8) NOT NULL, Currency dbo.CurrencyCode NOT NULL, TAEffIndex real NULL, AnnMEIDivisor float NULL, NumDays real NULL, PeriodTAAdj real NULL)
AS
BEGIN
	INSERT @TAAdj (FactorSet, Currency, TAEffIndex, AnnMEIDivisor, NumDays)
	SELECT ta.FactorSet, ta.Currency, ta.TAEffIndex, f.AvgAnnMaintEffDiv, f.NumDays
	FROM dbo.SLTAIndex(@SubmissionList, @FactorSet, @Currency) ta 
	INNER JOIN dbo.SLAverageFactors(@SubmissionList, @FactorSet) f ON f.FactorSet = ta.FactorSet

	UPDATE @TAAdj SET PeriodTAAdj = TAEffIndex*AnnMEIDivisor/100000/365.25*NumDays

	RETURN
END

