﻿CREATE FUNCTION [dbo].[CalcAveragePersKPIs](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @FactorSet FactorSet)
RETURNS @KPIs TABLE (FactorSet varchar(8) NOT NULL, OCCWHrEDC real NULL, MPSWHrEDC real NULL, TotWHrEDC real NULL, TotMaintForceWHrEDC real NULL
		, PEI real NULL, MaintPEI real NULL, NonMaintPEI real NULL
		, OCCEqPEDC real NULL, MPSEqPEDC real NULL, TotEqPEDC real NULL)
AS
BEGIN
	INSERT @KPIs (FactorSet, TotWHrEDC, OCCWHrEDC, MPSWHrEDC, PEI, TotEqPEDC, OCCEqPEDC, MPSEqPEDC, TotMaintForceWHrEDC, MaintPEI, NonMaintPEI)
	SELECT ptc.FactorSet, TotWHrEDC = [$(dbsGlobal)].[dbo].WtAvg(tp.TotWHrEDC, tp.WHrEDCDivisor), OCCWHrEDC = [$(dbsGlobal)].[dbo].WtAvg(o.TotWHrEDC, tp.WHrEDCDivisor), MPSWHrEDC = [$(dbsGlobal)].[dbo].WtAvg(m.TotWHrEDC, tp.WHrEDCDivisor)
		, PEI = [$(dbsGlobal)].[dbo].WtAvg(tp.TotWHrEffIndex, tp.EffDivisor)
		, TotEqPEDC = [$(dbsGlobal)].[dbo].WtAvg(tp.TotEqPEDC, tp.EqPEDCDivisor), OCCEqPEDC = [$(dbsGlobal)].[dbo].WtAvg(o.TotEqPEDC, tp.EqPEDCDivisor), MPSEqPEDC = [$(dbsGlobal)].[dbo].WtAvg(m.TotEqPEDC, tp.EqPEDCDivisor)
		, TotMaintForceWHrEDC = [$(dbsGlobal)].[dbo].WtAvg(ptc.TotMaintForceWHrEDC, ptc.WHrEDCDivisor)
		, MaintPEI = [$(dbsGlobal)].[dbo].WtAvg(ptc.MaintPEI, ptc.MaintPEIDivisor)
		, NonMaintPEI = [$(dbsGlobal)].[dbo].WtAvg(ptc.NonMaintPEI, ptc.NonMaintPEIDivisor)
	FROM Submissions s INNER JOIN PersTotCalc ptc ON ptc.SubmissionID = s.SubmissionID AND ptc.Currency = 'USD'
	INNER JOIN PersSTCalc tp ON tp.SubmissionID = s.SubmissionID AND tp.SectionID = 'TP' AND tp.FactorSet = ptc.FactorSet
	LEFT JOIN PersSTCalc o ON o.SubmissionID = s.SubmissionID AND o.SectionID = 'TO' AND o.FactorSet = ptc.FactorSet
	LEFT JOIN PersSTCalc m ON m.SubmissionID = s.SubmissionID AND m.SectionID = 'TM' AND m.FactorSet = ptc.FactorSet
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	AND ptc.FactorSet = ISNULL(@FactorSet, ptc.FactorSet)
	GROUP BY ptc.FactorSet


RETURN

END



