﻿create function [dbo].[ConvTemp](@valToConv float, @fromUnits varchar(255), @toUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @TempF float, @Result float
	--if  units are the same then return the value
	IF @fromUnits = @toUnits 
		SET @Result = @valToConv
	ELSE BEGIN
		-- First, convert temperature to Farenheit
		IF @fromUnits = 'TF' 
			SET @TempF = @valToConv
		ELSE IF @fromUnits = 'TC' 
			SELECT @TempF = 1.8*@valToConv+32
		ELSE IF @fromUnits = 'TK' 
			SELECT @TempF = 1.8*(@valToconv-273)+32
		ELSE IF @fromUnits = 'TR' 
			SELECT @TempF = @valToConv - 460
		ELSE 
			SELECT @TempF = NULL
		-- Second, convert from F to specified units
		IF @toUnits = 'TF' 
			SET @Result = @TempF
		ELSE IF @toUnits = 'TC'
			SELECT @Result = (@TempF-32)/1.8
		ELSE IF @toUnits = 'TK' 
			SELECT @Result = (@TempF-32)/1.8+273
		ELSE IF @toUnits = 'TR' 
			SELECT @Result = @TempF + 460
		ELSE
			SET @Result = NULL
	END
	RETURN @Result
END

