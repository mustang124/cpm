﻿CREATE FUNCTION [dbo].[SLSumAbsenceTot](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT OCCAbs = SUM(a.OCCAbs), MPSAbs = SUM(a.MPSAbs), TotAbs = SUM(a.TotAbs)
	FROM @SubmissionList s INNER JOIN AbsenceTot a ON a.SubmissionID = s.SubmissionID
	)


