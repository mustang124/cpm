﻿
CREATE FUNCTION [dbo].[SLSumMaterialTot](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT TotInputBbl = SUM(a.TotInputBbl), NetInputBbl = SUM(a.NetInputBbl), PVP = SUM(a.PVP), GainBbl = SUM(a.GainBbl)
	FROM MaterialTot a INNER JOIN @SubmissionList s ON s.SubmissionID = a.SubmissionID   
)


