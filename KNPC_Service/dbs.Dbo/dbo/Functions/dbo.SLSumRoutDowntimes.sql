﻿

CREATE FUNCTION [dbo].[SLSumRoutDowntimes](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
		SELECT s.RefineryID, s.Dataset, mr.UnitID, fc.FactorSet, MaintDown = SUM(mr.MaintDown), MaintSlow = SUM(mr.MaintSlow), 
		RegDown = SUM(mr.RegDown), RegSlow = SUM(mr.RegSlow), 
		OthDown = SUM(mr.OthDown), OthSlow = SUM(mr.OthSlow), PeriodHrs = SUM(mr.PeriodHrs),
		OthDownEconomic = SUM(mr.OthDownEconomic), OthDownExternal = SUM(mr.OthDownExternal), OthDownUnitUpsets = SUM(mr.OthDownUnitUpsets), OthDownOffsiteUpsets = SUM(mr.OthDownOffsiteUpsets), OthDownOther = SUM(mr.OthDownOther), 
		UnpRegDown = SUM(mr.UnpRegDown), UnpMaintDown = SUM(mr.UnpMaintDown), UnpOthDown  = SUM(mr.UnpOthDown),
		PeriodEDC = SUM(mr.PeriodHrs/365.*fc.EDCNoMult)
		FROM MaintRout mr INNER JOIN @SubmissionList sl ON sl.SubmissionID = mr.SubmissionID
		INNER JOIN FactorCalc fc ON fc.SubmissionID = mr.SubmissionID AND fc.UnitID = mr.UnitID
		INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = sl.SubmissionID
		WHERE mr.PeriodHrs > 0
		GROUP BY s.RefineryID, s.Dataset, mr.UnitID, fc.FactorSet
	)





