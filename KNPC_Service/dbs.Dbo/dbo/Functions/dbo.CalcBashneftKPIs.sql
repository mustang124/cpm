﻿/*
@ProcessUtilPcnt, @TotProcessEDC, @TotProcessUEDC, @OffsitesEDC, @Complexity
@EII, @EnergyUseDay, @TotStdEnergy, @FuelUseDay 
@OpAvail, @MechUnavailTA, NonTAOpUnavail = @NonTAUnavail, @NumDays
MechAvail, @NonTAMechUnavail
@MaintEffIndex, @RoutCost, @MaintEffDiv, TAIndex
mPersEffIndex, MaintWHr, OCCMaintWHr, MPSMaintWHr, mPersEffDiv
@OpexUEDC, @TAAdj, @EnergyCost, @TotCashOpex, @UEDC
@NEOpexEDC, @NEOpex, @EDC
@ExchRate
*/

CREATE FUNCTION [dbo].[CalcBashneftKPIs] (@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
RETURNS @KPIData TABLE (
	ProcessUtilPcnt real NULL, ProcessEDC real NULL, ProcessUEDC real NULL, OffsitesEDC real NULL, Complexity real NULL,
	EII real NULL, EnergyUseDay real NULL, TotStdEnergy real NULL, FuelUseDay real NULL,
	OpAvail real NULL, MechUnavailTA real NULL, NonTAOpUnavail real NULL, TADD real NULL, NTAMDD real NULL, RPDD real NULL, NumDays real NULL,
	MechAvail real NULL, NonTAMechUnavail real NULL,
	MaintEffIndex real NULL, AnnTACost real NULL, RoutCost real NULL, MaintEffDiv real NULL, TAIndex real NULL,
	mPersEffIndex real NULL, MaintWHr real NULL, OCCMaintWHr real NULL, MPSMaintWHr real NULL, mPersEffDiv real NULL, 
	NEOpexEDC real NULL, NEOpex real NULL, EDC real NULL,
	TotCashOpexUEDC real NULL, TAAdj real NULL, EnergyCost real NULL, TotCashOpex real NULL, UEDC real NULL, ExchRate real NULL
)

AS
BEGIN

SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

IF EXISTS (SELECT * FROM Submissions WHERE CalcsNeeded IS NOT NULL AND SubmissionID IN (SELECT SubmissionID FROM @SubmissionList))
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN
END

DECLARE	
	@ProcessUtilPcnt real, @TotProcessEDC real, @TotProcessUEDC real, @OffsitesEDC real, @Complexity real,
	@EII real, @EnergyUseDay real, @FuelUseDay real, @TotStdEnergy real,
	@OpAvail real, @MechAvail real, @MechUnavailTA real, @NonTAMechUnavail real, @NonTAOpUnavail real, @RegUnavail real,
	@TADD real, @NTAMDD real, @RPDD real, @NumDays real,
	@MaintEffIndex real, @TAAdj real, @AnnTACost real, @RoutCost real, @MaintEffDiv real, @EDC real, @TAIndex real,
	@mPersEffIndex real, @MaintWHr real, @OCCMaintWHr real, @MPSMaintWHr real, @mPersEffDiv real,
	@OpexUEDC real, @EnergyCost real, @TotCashOpex real, @UEDC real, @NEOpexEDC real, @NEOpex real, @ExchRate real


IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @FuelUseDay = @FuelUseDay * 1.055, @TotStdEnergy = @TotStdEnergy * 1.055

SELECT @OffsitesEDC = @EDC - @TotProcessEDC

SELECT @ExchRate = dbo.AvgExchangeRate('RUB', MIN(PeriodStart), MAX(PeriodEnd))
FROM Submissions WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)

INSERT @KPIData (ProcessUtilPcnt, ProcessEDC, ProcessUEDC, OffsitesEDC, Complexity,
	EII, EnergyUseDay, TotStdEnergy, FuelUseDay, 
	OpAvail, MechUnavailTA, NonTAOpUnavail, TADD, NTAMDD, RPDD, NumDays,
	MechAvail, NonTAMechUnavail,
	MaintEffIndex, AnnTACost, RoutCost, MaintEffDiv, TAIndex,
	mPersEffIndex, MaintWHr, OCCMaintWHr, MPSMaintWHr, mPersEffDiv, 
	NEOpexEDC, NEOpex, EDC,
	TotCashOpexUEDC, TAAdj, EnergyCost, TotCashOpex, UEDC, ExchRate)
SELECT @ProcessUtilPcnt, @TotProcessEDC, @TotProcessUEDC, @OffsitesEDC, @Complexity, 	
	@EII, @EnergyUseDay, @TotStdEnergy, @FuelUseDay,
	@OpAvail, @MechUnavailTA, @NonTAOpUnavail, TADD = @MechUnavailTA/100*@NumDays, NTAMDD = @NonTAMechUnavail/100*@NumDays, RPDD = @RegUnavail/100*@NumDays, @NumDays,
	@MechAvail, @NonTAMechUnavail,
	@MaintEffIndex, @TAAdj/1000, @RoutCost/1000, @MaintEffDiv/1000000, @TAIndex,
	@mPersEffIndex, @MaintWHr/1000, @OCCMaintWHr/1000, @MPSMaintWHr/1000, @mPersEffDiv/1000,
	@NEOpexEDC, @NEOpex/1000, @EDC,
	@OpexUEDC, @TAAdj/1000, @EnergyCost/1000, @TotCashOpex/1000, @UEDC, @ExchRate

	RETURN
END

