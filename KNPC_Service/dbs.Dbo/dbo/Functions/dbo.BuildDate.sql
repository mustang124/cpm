﻿CREATE FUNCTION [dbo].[BuildDate] (@Year smallint, @Month smallint, @Day smallint)  
RETURNS smalldatetime AS  
BEGIN 
	DECLARE @dt smalldatetime
	SELECT @dt = '1/1/2000'
	SELECT @dt = DATEADD(yy, @Year-2000, @dt)
	SELECT @dt = DATEADD(mm, @Month - 1, @dt)
	SELECT @dt = DATEADD(d, @Day - 1, @dt)
	RETURN @dt
END

