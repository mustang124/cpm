﻿CREATE FUNCTION [dbo].[SumPersSection](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @SectionID char(2))
RETURNS TABLE
AS
RETURN (
	SELECT p.SectionID, NumPers = SUM(p.NumPers), STH = SUM(p.STH), OVTHours = SUM(p.OVTHours), OVTPcnt = CASE WHEN SUM(p.STH) > 0 THEN SUM(p.OVTHours)/SUM(p.STH)*100 END, [Contract] = SUM(p.Contract), GA = SUM(p.GA), AbsHrs = SUM(p.AbsHrs)
		, CompEqP = [$(dbsGlobal)].[dbo].WtAvg(p.CompEqP, s.FractionOfYear), ContEqP = [$(dbsGlobal)].[dbo].WtAvg(p.ContEqP, s.FractionOfYear), GAEqP = [$(dbsGlobal)].[dbo].WtAvg(p.GAEqP, s.FractionOfYear), TotEqP = [$(dbsGlobal)].[dbo].WtAvg(p.TotEqP, s.FractionOfYear)
		, CompWHr = SUM(p.CompWHr), ContWHr = SUM(p.ContWHr), GAWHr = SUM(p.GAWHr), TotWHr = SUM(p.TotWHr)
	FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartDate, @EndDate) s 
	INNER JOIN PersST p ON p.SubmissionID = s.SubmissionID AND p.SectionID = ISNULL(@SectionID, p.SectionID)
	GROUP BY p.SectionID
)

