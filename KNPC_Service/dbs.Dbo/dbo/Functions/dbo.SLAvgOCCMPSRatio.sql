﻿
CREATE FUNCTION [dbo].[SLAvgOCCMPSRatio](@SubmissionList dbo.SubmissionIDList READONLY, @OCCSection char(2), @MPSSection char(2))
RETURNS real
AS
BEGIN
	DECLARE @OCCMPSRatio real
	SELECT @OCCMPSRatio = SUM(o.NumPers)/SUM(m.NumPers)
	FROM @SubmissionList s 
	INNER JOIN PersST o ON o.SubmissionID = s.SubmissionID AND o.SectionID = @OCCSection
	INNER JOIN PersST m ON m.SubmissionID = s.SubmissionID AND m.SectionID = @MPSSection AND m.NumPers > 0
	
	RETURN @OCCMPSRatio
END


