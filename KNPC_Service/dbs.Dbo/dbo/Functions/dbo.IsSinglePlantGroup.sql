﻿CREATE FUNCTION [dbo].[IsSinglePlantGroup] (@GroupKey	VARCHAR(20))
		
RETURNS INT
AS
BEGIN
	RETURN (ISNUMERIC(LEFT(@GroupKey, 2)));
END

