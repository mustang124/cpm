﻿CREATE FUNCTION [dbo].[SumMaterialTot](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT TotInputBbl = SUM(a.TotInputBbl), NetInputBbl = SUM(a.NetInputBbl), PVP = SUM(a.PVP), GainBbl = SUM(a.GainBbl), NumDays = SUM(s.NumDays)
	FROM MaterialTot a INNER JOIN dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartDate, @EndDate) s ON s.SubmissionID = a.SubmissionID   
)

