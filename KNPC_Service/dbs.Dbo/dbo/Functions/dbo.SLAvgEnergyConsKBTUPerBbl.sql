﻿
CREATE FUNCTION [dbo].[SLAvgEnergyConsKBTUPerBbl](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS real
AS
BEGIN
	DECLARE @KBTUPerBbl real
	SELECT @KBTUPerBbl = CASE WHEN SUM(m.NetInputBbl)>0 THEN SUM(e.TotEnergyConsMBTU)/SUM(m.NetInputBbl/1000) ELSE NULL END
	FROM MaterialTot m INNER JOIN EnergyTot e ON e.SubmissionID = m.SubmissionID
	INNER JOIN @SubmissionList s ON s.SubmissionID = m.SubmissionID

	RETURN @KBTUPerBbl
END


