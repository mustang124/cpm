﻿CREATE FUNCTION [dbo].[SumOpex](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @Currency varchar(4))
RETURNS TABLE
AS
RETURN (
	SELECT a.Currency, OCCSal = SUM(a.OCCSal), MPSSal = SUM(a.MPSSal), OCCBen = SUM(a.OCCBen), MPSBen = SUM(a.MPSBen), MaintMatlST = SUM(a.MaintMatlST), ContMaintLaborST = SUM(a.ContMaintLaborST), TAAdj = SUM(a.TAAdj), Envir = SUM(a.Envir), OthNonVol = SUM(a.OthNonVol), STNonVol = SUM(a.STNonVol)
	, STVol = SUM(a.STVol), TotCashOpEx = SUM(a.TotCashOpex), PersCostExclTA = SUM(a.PersCostExclTA), EnergyCost = SUM(a.EnergyCost), NEOpex = SUM(a.NEOpex)
	FROM OpexAll a INNER JOIN dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartDate, @EndDate) s ON s.SubmissionID = a.SubmissionID   
	WHERE a.DataType = 'ADJ' AND Scenario = 'CLIENT' AND a.Currency = ISNULL(@Currency, a.Currency)
	GROUP BY a.Currency
)

