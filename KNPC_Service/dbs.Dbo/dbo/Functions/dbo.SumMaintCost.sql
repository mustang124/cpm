﻿CREATE FUNCTION [dbo].[SumMaintCost](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @Currency varchar(4))
RETURNS TABLE
AS
RETURN (
	SELECT a.Currency
		, CurrTACost = SUM(CurrTACost), CurrRoutCost = SUM(CurrRoutCost), CurrMaintCost = SUM(CurrMaintCost)
		, CurrTAMatl = SUM(CurrTAMatl), CurrRoutMatl = SUM(CurrRoutMatl), CurrMaintMatl = SUM(CurrMaintMatl)
		, AllocAnnTACost = SUM(AllocAnnTACost), AllocAnnTAMatl = SUM(AllocAnnTAMatl), AllocCurrRoutCost = SUM(AllocCurrRoutCost), AllocCurrRoutMatl = SUM(AllocCurrRoutMatl)
	FROM MaintTotCost a INNER JOIN dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartDate, @EndDate) s ON s.SubmissionID = a.SubmissionID
	WHERE a.Currency = ISNULL(@Currency, a.Currency)
	GROUP BY a.Currency
)

