﻿CREATE FUNCTION [dbo].[CalcAverageAvail](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT m.FactorSet, 
	MechAvail_Ann=[$(dbsGlobal)].[dbo].WtAvg(m.MechAvail_Ann,f.TotProcessEDC*s.FractionOfYear),
	MechAvailSlow_Ann=[$(dbsGlobal)].[dbo].WtAvg(MechAvailSlow_Ann,f.TotProcessEDC*s.FractionOfYear), 
	OpAvail_Ann=[$(dbsGlobal)].[dbo].WtAvg(OpAvail_Ann,f.TotProcessEDC*s.FractionOfYear), 
	OpAvailSlow_Ann=[$(dbsGlobal)].[dbo].WtAvg(OpAvailSlow_Ann,f.TotProcessEDC*s.FractionOfYear), 
	OnStream_Ann=[$(dbsGlobal)].[dbo].WtAvg(OnStream_Ann,f.TotProcessEDC*s.FractionOfYear), 
	OnStreamSlow_Ann=[$(dbsGlobal)].[dbo].WtAvg(OnStreamSlow_Ann,f.TotProcessEDC*s.FractionOfYear), 
	MechAvail_Act=[$(dbsGlobal)].[dbo].WtAvg(MechAvail_Act,f.TotProcessEDC*s.FractionOfYear), 
	MechAvailSlow_Act=[$(dbsGlobal)].[dbo].WtAvg(MechAvailSlow_Act,f.TotProcessEDC*s.FractionOfYear), 
	OpAvail_Act=[$(dbsGlobal)].[dbo].WtAvg(OpAvail_Act,f.TotProcessEDC*s.FractionOfYear), 
	OpAvailSlow_Act=[$(dbsGlobal)].[dbo].WtAvg(OpAvailSlow_Act,f.TotProcessEDC*s.FractionOfYear), 
	OnStream_Act=[$(dbsGlobal)].[dbo].WtAvg(OnStream_Act,f.TotProcessEDC*s.FractionOfYear), 
	OnStreamSlow_Act=[$(dbsGlobal)].[dbo].WtAvg(OnStreamSlow_Act,f.TotProcessEDC*s.FractionOfYear), 
	MechAvailOSTA=[$(dbsGlobal)].[dbo].WtAvg(MechAvailOSTA,f.TotProcessEDC*s.FractionOfYear)
	FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
	INNER JOIN dbo.Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	GROUP BY m.FactorSet
)



