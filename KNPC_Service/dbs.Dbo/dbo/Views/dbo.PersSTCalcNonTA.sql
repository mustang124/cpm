﻿
CREATE VIEW [dbo].[PersSTCalcNonTA]
AS
SELECT p.SubmissionID, p.FactorSet, p.SectionID, p.Description
	, NumPers = p.NumPers - SUM(ISNULL(pc.NumPers,0)), STH = p.STH - SUM(ISNULL(pc.STH,0)), OVTHours = p.OVTHours - SUM(ISNULL(pc.OVTHours,0)), Contract = p.Contract - SUM(ISNULL(pc.Contract,0)), GA = p.GA - SUM(ISNULL(pc.GA,0)), AbsHrs = p.AbsHrs - SUM(ISNULL(pc.AbsHrs,0))
	, CompWHr = p.CompWHr - SUM(ISNULL(pc.CompWHr,0)), ContWHr = p.ContWHr - SUM(ISNULL(pc.ContWHr,0)), GAWHr = p.GAWHr - SUM(ISNULL(pc.GAWHr,0)), TotWHr = p.TotWHr - SUM(ISNULL(pc.TotWHr,0))
	, CompWHrEDC = p.CompWHrEDC - SUM(ISNULL(pc.CompWHrEDC,0)), ContWHrEDC = p.ContWHrEDC - SUM(ISNULL(pc.ContWHrEDC,0)), GAWHrEDC = p.GAWHrEDC - SUM(ISNULL(pc.GAWHrEDC,0)), TotWHrEDC = p.TotWHrEDC - SUM(ISNULL(pc.TotWHrEDC,0))
	, TotWHrEffIndex = p.TotWHrEffIndex - SUM(ISNULL(pc.TotWHrEffIndex,0)), p.WHrEDCDivisor, p.EffDivisor
FROM PersSTCalc p LEFT JOIN ((VALUES ('OM','OCCTAADJ'),('TO','OCCTAADJ'),('MM','MPSTAADJ'),('TM','MPSTAADJ'),('TP','OCCTAADJ'),('TP','MPSTAADJ')) s(SectionID,TAPersID) 
INNER JOIN PersCalc pc ON pc.PersID = s.TAPersID) ON pc.SubmissionID = p.SubmissionID AND pc.FactorSet = p.FactorSet AND s.SectionID = p.SectionID
GROUP BY p.SubmissionID, p.FactorSet, p.SectionID, p.Description, p.NumPers, p.STH, p.OVTHours, p.OVTPcnt, p.Contract, p.GA, p.AbsHrs
	, p.CompWHr, p.CompWHrEDC, p.ContWHr, p.ContWHrEDC, p.GAWHr, p.GAWHrEDC, p.TotWHr, p.TotWHrEDC
	, p.TotWHrEffIndex, p.WHrEDCDivisor, p.EffDivisor
	


