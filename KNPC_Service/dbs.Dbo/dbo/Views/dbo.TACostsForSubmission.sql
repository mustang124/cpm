﻿

CREATE    VIEW [dbo].[TACostsForSubmission] AS
SELECT s.PeriodStart, s.SubmissionID, c.*, 
CurrTACost = ISNULL(c.TACost*CASE WHEN c.TADate BETWEEN s.PeriodStart AND s.PeriodEnd AND c.RestartDate BETWEEN s.PeriodStart AND s.PeriodEnd THEN 1.0 WHEN c.RestartDate > s.PeriodStart AND c.TADate < s.PeriodEnd THEN CONVERT(real, DATEDIFF(hh, CASE WHEN c.TADate < s.PeriodStart THEN s.PeriodStart ELSE c.TADate END, CASE WHEN c.RestartDate > s.PeriodEnd THEN s.PeriodEnd ELSE c.RestartDate END))/DATEDIFF(hh, TADate, RestartDate) END, 0),
CurrTAMatl = ISNULL(c.TAMatl*CASE WHEN c.TADate BETWEEN s.PeriodStart AND s.PeriodEnd AND c.RestartDate BETWEEN s.PeriodStart AND s.PeriodEnd THEN 1.0 WHEN c.RestartDate > s.PeriodStart AND c.TADate < s.PeriodEnd THEN CONVERT(real, DATEDIFF(hh, CASE WHEN c.TADate < s.PeriodStart THEN s.PeriodStart ELSE c.TADate END, CASE WHEN c.RestartDate > s.PeriodEnd THEN s.PeriodEnd ELSE c.RestartDate END))/DATEDIFF(hh, TADate, RestartDate) END, 0),
CurrTALaborCost = ISNULL(c.TALaborCost*CASE WHEN c.TADate BETWEEN s.PeriodStart AND s.PeriodEnd AND c.RestartDate BETWEEN s.PeriodStart AND s.PeriodEnd THEN 1.0 WHEN c.RestartDate > s.PeriodStart AND c.TADate < s.PeriodEnd THEN CONVERT(real, DATEDIFF(hh, CASE WHEN c.TADate < s.PeriodStart THEN s.PeriodStart ELSE c.TADate END, CASE WHEN c.RestartDate > s.PeriodEnd THEN s.PeriodEnd ELSE c.RestartDate END))/DATEDIFF(hh, TADate, RestartDate) END, 0),
CurrTAExp = ISNULL(c.TAExp*CASE WHEN c.TADate BETWEEN s.PeriodStart AND s.PeriodEnd AND c.RestartDate BETWEEN s.PeriodStart AND s.PeriodEnd THEN 1.0 WHEN c.RestartDate > s.PeriodStart AND c.TADate < s.PeriodEnd THEN CONVERT(real, DATEDIFF(hh, CASE WHEN c.TADate < s.PeriodStart THEN s.PeriodStart ELSE c.TADate END, CASE WHEN c.RestartDate > s.PeriodEnd THEN s.PeriodEnd ELSE c.RestartDate END))/DATEDIFF(hh, TADate, RestartDate) END, 0),
CurrTACptl = ISNULL(c.TACptl*CASE WHEN c.TADate BETWEEN s.PeriodStart AND s.PeriodEnd AND c.RestartDate BETWEEN s.PeriodStart AND s.PeriodEnd THEN 1.0 WHEN c.RestartDate > s.PeriodStart AND c.TADate < s.PeriodEnd THEN CONVERT(real, DATEDIFF(hh, CASE WHEN c.TADate < s.PeriodStart THEN s.PeriodStart ELSE c.TADate END, CASE WHEN c.RestartDate > s.PeriodEnd THEN s.PeriodEnd ELSE c.RestartDate END))/DATEDIFF(hh, TADate, RestartDate) END, 0),
CurrTAOvhd = ISNULL(c.TAOvhd*CASE WHEN c.TADate BETWEEN s.PeriodStart AND s.PeriodEnd AND c.RestartDate BETWEEN s.PeriodStart AND s.PeriodEnd THEN 1.0 WHEN c.RestartDate > s.PeriodStart AND c.TADate < s.PeriodEnd THEN CONVERT(real, DATEDIFF(hh, CASE WHEN c.TADate < s.PeriodStart THEN s.PeriodStart ELSE c.TADate END, CASE WHEN c.RestartDate > s.PeriodEnd THEN s.PeriodEnd ELSE c.RestartDate END))/DATEDIFF(hh, TADate, RestartDate) END, 0)
FROM dbo.SubmissionsAll s INNER JOIN MaintTACost c ON s.RefineryID = c.RefineryID AND s.DataSet = c.DataSet
WHERE (s.PeriodEnd < c.NextTADate OR c.NextTADate IS NULL) AND s.PeriodEnd > c.TADate
AND (EXISTS (SELECT * FROM Config WHERE config.SubmissionID = s.SubmissionID AND config.UnitID = c.UnitID AND Config.Cap > 0)
OR c.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE MaintDetails = 'Y' AND ProcessGroup = 'U' AND ProfileProcFacility = 'N'))
AND c.TAID > 0



