﻿


CREATE VIEW [dbo].[AbsenceCalc] AS
SELECT a.SubmissionID, e.FactorSet, a.SortKey, a.CategoryID, a.OCCAbs, a.MPSAbs, a.TotAbs,
a.OCCPcnt, a.MPSPcnt, a.TotPcnt,
OCCAbsEDC = a.OCCAbs/(e.EDC/100*s.FractionOfYear), 
MPSAbsEDC = a.MPSAbs/(e.EDC/100*s.FractionOfYear), 
TotAbsEDC = a.TotAbs/(e.EDC/100*s.FractionOfYear)
FROM Absence a INNER JOIN FactorTotCalc e ON e.SubmissionID = a.SubmissionID
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = a.SubmissionID



