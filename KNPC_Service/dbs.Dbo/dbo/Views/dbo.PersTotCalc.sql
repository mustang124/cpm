﻿





CREATE       VIEW [dbo].[PersTotCalc]
AS
SELECT p.SubmissionID, f.FactorSet, o.Scenario, o.Currency,
OCCWages = CASE WHEN p.OCCNumPers > 0 THEN o.OCCSal*1000/p.OCCNumPers/s.FractionOfYear END,
MPSWages = CASE WHEN p.MPSNumPers > 0 THEN o.MPSSal*1000/p.MPSNumPers/s.FractionOfYear END,
TotWages = CASE WHEN p.TotNumPers > 0 THEN o.STSal*1000/p.TotNumPers/s.FractionOfYear END,
OCCBen = CASE WHEN p.OCCNumPers > 0 THEN o.OCCBen*1000/p.OCCNumPers/s.FractionOfYear END,
MPSBen = CASE WHEN p.MPSNumPers > 0 THEN o.MPSBen*1000/p.MPSNumPers/s.FractionOfYear END,
TotBen = CASE WHEN p.TotNumPers > 0 THEN o.STBen*1000/p.TotNumPers/s.FractionOfYear END,
OCCComp = CASE WHEN p.OCCNumPers > 0 THEN (o.OCCSal+o.OCCBen)*1000/p.OCCNumPers/s.FractionOfYear END,
MPSComp = CASE WHEN p.MPSNumPers > 0 THEN (o.MPSSal+o.MPSBen)*1000/p.MPSNumPers/s.FractionOfYear END,
TotComp = CASE WHEN p.TotNumPers > 0 THEN (o.STSal+o.STBen)*1000/p.TotNumPers/s.FractionOfYear END,
ContMaintLaborCost = CASE WHEN p.ContMaintLaborWHr > 0 THEN o.ContMaintLabor*1000/p.ContMaintLaborWHr END,
OthContLaborCost = CASE WHEN p.OthContWHr > 0 THEN o.OthCont*1000/p.OthContWHr END,
ProcessOCCMPSRatio, MaintOCCMPSRatio, AdminOCCMPSRatio,
NonMaintEqP, NonMaintEqPEDC = p.NonMaintEqP/(f.PlantEDC/100000),
OCCCompNonTASTH, MPSCompNonTASTH, TotCompNonTASTH, 
NonMaintWHr, NonMaintWHrEDC = p.NonMaintWHr/(f.PlantEDC/100*s.FractionOfYear),
TotMaintForceWHr, TotMaintForceWHrEDC = p.TotMaintForceWHr/(f.PlantEDC/100*s.FractionOfYear),
MaintForceCompWHr, MaintForceCompWHrEDC = p.MaintForceCompWHr/(f.PlantEDC/100*s.FractionOfYear),
MaintForceContWHr, MaintForceContWHrEDC = p.MaintForceContWHr/(f.PlantEDC/100*s.FractionOfYear),
TotMaintForceEqP, TotMaintForceEqPEDC = p.TotMaintForceEqP/(f.PlantEDC/100000),
MaintForceCompEqP, MaintForceCompEqPEDC = p.MaintForceCompEqP/(f.PlantEDC/100000),
MaintForceContEqP, MaintForceContEqPEDC = p.MaintForceContEqP/(f.PlantEDC/100000),
MaintForceCompPcnt, MaintForceContPcnt, 
ContMaintLaborWHr, OthContWHr, OCCNumPers, MPSNumPers, TotNumPers,
ProcessWHr, AdminWHr, CompWHr, ProcessContPcnt, MaintContPcnt, TechContPcnt, AdminContPcnt, CompOvtPcnt,
CompOCCNonMaintEqP, CompMPSNonMaintEqP, CompNonMaintEqP, ContNonMaintEqP, GANonMaintEqP, TotNonMaintEqP,
CompOCCNonMaintWHr, CompMPSNonMaintWHr, CompNonMaintWHr, ContNonMaintWHr, GANonMaintWHr, TotNonMaintWHr,
CompOCCNonTAWHr, CompMPSNonTAWHr, CompNonTAWHr, ContNonTAWHr, GANonTAWHr, TotNonTAWHr,
CompOCCNonMaintPcnt, CompMPSNonMaintPcnt, CompNonMaintPcnt, ContNonMaintPcnt, GANonMaintPcnt, TotNonMaintPcnt,
AvgNonMaintComp = CASE WHEN (p.CompOCCNonMaintEqP + p.CompMPSNonMaintEqP + p.ContNonMaintEqP) = 0 THEN NULL ELSE
		  (CASE WHEN p.OCCNumPers > 0 THEN (o.OCCSal+o.OCCBen)*1000/p.OCCNumPers END * CompOCCNonMaintEqP +
		   CASE WHEN p.MPSNumPers > 0 THEN (o.MPSSal+o.MPSBen)*1000/p.MPSNumPers END * CompMPSNonMaintEqP +
		   o.OthCont*1000) / (p.CompOCCNonMaintEqP + p.CompMPSNonMaintEqP + p.ContNonMaintEqP) END,
MaintPEI = p.TotMaintForceWHr/(f.PlantMaintPersEffDiv/100*s.FractionOfYear),
NonMaintPEI = p.NonMaintWHr/(f.PlantNonMaintPersEffDiv/100*s.FractionOfYear),
f.PlantEDC/100000 AS EqPEDCDivisor,
f.PlantEDC*s.FractionOfYear/100 AS WHrEDCDivisor,
f.PlantMaintPersEffDiv/100*s.FractionOfYear AS MaintPEIDivisor,
f.PlantNonMaintPersEffDiv/100*s.FractionOfYear AS NonMaintPEIDivisor,
NonTAMaintForceWHr, NonTAMaintForceWHrEDC = p.NonTAMaintForceWHr/(f.PlantEDC/100*s.FractionOfYear),
NonTAMaintForceCompWHr, NonTAMaintForceCompWHrEDC = p.NonTAMaintForceCompWHr/(f.PlantEDC/100*s.FractionOfYear),
NonTAMaintForceContWHr, NonTAMaintForceContWHrEDC = p.NonTAMaintForceContWHr/(f.PlantEDC/100*s.FractionOfYear),
NonTAMaintForceEqP, NonTAMaintForceEqPEDC = p.NonTAMaintForceEqP/(f.PlantEDC/100000),
NonTAMaintForceCompEqP, NonTAMaintForceCompEqPEDC = p.NonTAMaintForceCompEqP/(f.PlantEDC/100000),
NonTAMaintForceContEqP, NonTAMaintForceContEqPEDC = p.NonTAMaintForceContEqP/(f.PlantEDC/100000),
NonTAMaintPEI = p.NonTAMaintForceWHr/(f.PlantMaintPersEffDiv/100*s.FractionOfYear)
FROM PersTot p INNER JOIN FactorTotCalc f ON f.SubmissionID = p.SubmissionID
INNER JOIN OpexAll o ON o.SubmissionID = p.SubmissionID AND o.DataType = 'ADJ'
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = p.SubmissionID
WHERE f.PlantEDC>0 















