﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace KNPCService
{
    public class DbConnectionCheck
    {


        public string ValidateConnection(string connectionString)
        {
            string connection = "Invalid Connection";
            string sql = "SELECT TOP 1 RefineryID FROM TSort";
            string value = string.Empty;

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    

                    SqlCommand cmdReport = new SqlCommand(sql, sqlConnection);
                    cmdReport.CommandType = CommandType.Text;

                    sqlConnection.Open();
                    SqlDataReader reader = cmdReport.ExecuteReader();

                    if (reader.HasRows)
                    {
                        int index = 0;

                        while (reader.Read())
                        {
                            if (index == 0)
                            {
                                value = (reader["RefineryID"] == DBNull.Value) ? null : Convert.ToString(reader["RefineryID"]);
                                connection = "Validated Top 1 RefineryID from table: TSort. Value =  " + value;
                                break;
                            }
                        }
                    }

                    sqlConnection.Close();

                }

                return connection;


            }
            catch (Exception ex)
            {
                string exception = " - Message: " + ex.Message;
                return connection + exception;
            }

        }
    }
}
