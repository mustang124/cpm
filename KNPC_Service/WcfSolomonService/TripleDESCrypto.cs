﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


public class TripleDesCryptor : IDisposable
{
    private byte[] privateKey;
    private byte[] privateIV;
    private Encoding privateEncoder;

    public TripleDesCryptor(Encoding encodingToBeUser)
    {
        TripleDESCryptoServiceProvider tempProvider = new TripleDESCryptoServiceProvider();

        tempProvider.GenerateKey();
        this.privateKey = tempProvider.Key.ToArray();

        tempProvider.GenerateIV();
        this.privateIV = tempProvider.IV.ToArray();

        tempProvider.Clear();

        this.privateEncoder = encodingToBeUser;
    }

    public string Encrypt(string stringToBeEncrypted)
    {
        string result = string.Empty;

        TripleDESCryptoServiceProvider tempProvider = new TripleDESCryptoServiceProvider();

        MemoryStream tempStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(tempStream, new TripleDESCryptoServiceProvider().CreateEncryptor(this.privateKey, this.privateIV), CryptoStreamMode.Write);

        byte[] plainBytes = this.privateEncoder.GetBytes(stringToBeEncrypted);

        cryptoStream.Write(plainBytes, 0, plainBytes.Length);
        cryptoStream.FlushFinalBlock();

        byte[] encryptedBytes = tempStream.ToArray();
        result = this.privateEncoder.GetString(encryptedBytes);

        tempProvider.Clear();
        tempStream.Close();
        cryptoStream.Close();
        plainBytes = null;
        encryptedBytes = null;

        return result;
    }

    public string Decrypt(string encryptedString)
    {
        string result = string.Empty;

        TripleDESCryptoServiceProvider tempProvider = new TripleDESCryptoServiceProvider();
        byte[] encryptedBytes = this.privateEncoder.GetBytes(encryptedString);
        MemoryStream tempStream = new MemoryStream(encryptedBytes);

        CryptoStream cryptoStream = new CryptoStream(tempStream, new TripleDESCryptoServiceProvider().CreateDecryptor(this.privateKey, this.privateIV), CryptoStreamMode.Read);

        byte[] decryptedBytes = new byte[encryptedBytes.Length];
        cryptoStream.Read(decryptedBytes, 0, decryptedBytes.Length);

        result = this.privateEncoder.GetString(decryptedBytes);

        tempProvider.Clear();
        tempStream.Close();
        cryptoStream.Close();
        decryptedBytes = null;
        encryptedBytes = null;

        return result;
    }

    public byte[] EncryptAsBytes(string stringToBeEncrypted)
    {
        TripleDESCryptoServiceProvider tempProvider = new TripleDESCryptoServiceProvider();

        MemoryStream tempStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(tempStream, new TripleDESCryptoServiceProvider().CreateEncryptor(this.privateKey, this.privateIV), CryptoStreamMode.Write);

        byte[] plainBytes = this.privateEncoder.GetBytes(stringToBeEncrypted);

        cryptoStream.Write(plainBytes, 0, plainBytes.Length);
        cryptoStream.FlushFinalBlock();

        byte[] encryptedBytes = tempStream.ToArray();

        tempProvider.Clear();
        tempStream.Close();
        cryptoStream.Close();
        plainBytes = null;

        return encryptedBytes;
    }

    public string DecryptAsBytes(byte[] encryptedString)
    {
        string result = string.Empty;

        TripleDESCryptoServiceProvider tempProvider = new TripleDESCryptoServiceProvider();
        byte[] encryptedBytes = encryptedString;
        MemoryStream tempStream = new MemoryStream(encryptedBytes);

        CryptoStream cryptoStream = new CryptoStream(tempStream, new TripleDESCryptoServiceProvider().CreateDecryptor(this.privateKey, this.privateIV), CryptoStreamMode.Read);

        byte[] decryptedBytes = new byte[encryptedBytes.Length];
        cryptoStream.Read(decryptedBytes, 0, decryptedBytes.Length);

        result = this.privateEncoder.GetString(decryptedBytes);

        tempProvider.Clear();
        tempStream.Close();
        cryptoStream.Close();
        decryptedBytes = null;
        encryptedBytes = null;

        return result;
    }

    public void Dispose()
    {
        GC.Collect();
    }
}