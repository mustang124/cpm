﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using DbsDbo = Sa.dbs.Dbo;
using DbsDim = Sa.dbs.Dim;
using DbsOutput = Sa.dbs.Output;
using DbsStage = Sa.dbs.Stage;
using Sa.Pearl;

namespace KNPCService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        private string connectionString = string.Empty;

        public Service1()
        {
            connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["KnpcTestConnection"]);
        }



        #region "CheckService"
        public string CheckService()
        {
            string machineName = Environment.MachineName;
            string domain = Environment.UserDomainName.ToString();
            Sa.Logger.LogInfo("CheckService() called by " + domain + @"\" + machineName);
            return "Wcf Service Successful: Domain = " + domain + Environment.NewLine + "Machine Name = " + machineName + Environment.NewLine;
        }
        #endregion "CheckService"

        #region "ValidateDatabaseConnection"

        public string ValidateDatabaseConnection()
        {
            string retVal = string.Empty;
            DbConnectionCheck dbCheck = new DbConnectionCheck();
            retVal = dbCheck.ValidateConnection(connectionString); // + "Database connection string:" + connectionString;
            return retVal;
        }

        #endregion "ValidateDatabaseConnection"

        #region Send KNPC Data Test
        public List<string> SendKNPCDataTEST(string table, byte[] xmlstringBytes)
        {
            string abc = string.Empty;
            //GetReference(new byte[0], out abc);

            GetSubmission(new byte[4], new byte[4], out abc);
            byte[] salt = Encoding.UTF8.GetBytes("dog");
            byte[] key = { 248, 215, 80, 175, 235, 183, 19, 200, 195, 97, 212, 123, 110, 0, 192, 83, 148, 33, 49, 121, 147, 240, 138, 188, 113, 177, 209, 112, 145, 219, 53, 78 };
            byte[] vector = { 86, 102, 197, 82, 75, 101, 134, 69, 250, 94, 79, 187, 146, 83, 18, 128 };
            Encoding myEncoding = Encoding.ASCII;

            //TripleDesCryptor ts = new TripleDesCryptor(myEncoding);


            DataSet ds = new DataSet();

            //string buffer = cryptor.Decrypt(xmlstringBytes, salt.Length, key, vector);
            //string xml = DecompressData(Encoding.UTF8.GetBytes(buffer));

            // string buffer = ts.DecryptAsBytes(xmlstringBytes);

            //ds.ReadXml(XmlReader.Create(new StringReader(buffer)));
            //List<string> errors = SubmitData(table, ds);
            //return errors;
            return new List<string>();

        }
        #endregion

        #region Get Reference
        public byte[] GetReference(byte[] clientKeyBytes, out string statusMessage)
        {
            byte[] encryptedReferenceBytes;
            string clientKeyDecrypted;
            try
            {
                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);

                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        statusMessage = "Error in GetReference. Invalid Client Key.";
                        return new byte[0];
                    }
                }
                DbsDim.ShortDim shortDimList = new DbsDim.ShortDim();

                //Explanation: We originally made some new tables in the dim schema.
                //On promotion to Prod, we realized these tables already existed in dbo schema.
                //So we are repointing the dim DTO to the dbo DTO for the following tables:
                using (DbsDbo.DboEntities dbo = new DbsDbo.DboEntities())
                {
                    foreach (DbsDbo.Absence_LU absence_lu in dbo.Absence_LU)
                        shortDimList.Absence_LU.Add(new DbsDim.Absence_LU { CategoryID = absence_lu.CategoryID, Description = absence_lu.Description });

                    foreach (DbsDbo.Crude_LU crude_lu in dbo.Crude_LU)
                        shortDimList.Crude_LU.Add(new DbsDim.Crude_LU { CNum = crude_lu.CNum, CrudeName = crude_lu.CrudeName, ProdCountry = crude_lu.ProdCountry, TypicalAPI = crude_lu.TypicalAPI, TypicalSulfur = crude_lu.TypicalSulfur });

                    foreach (DbsDbo.Energy_LU energy_lu in dbo.Energy_LU)                    
                        shortDimList.Energy_LU.Add(new DbsDim.Energy_LU { Composition = energy_lu.Composition, EnergySection = energy_lu.EnergySection, EnergyType = energy_lu.EnergyType, EnergyTypeDesc = energy_lu.EnergyTypeDesc, SortKey = decimal.ToInt32(energy_lu.SortKey.HasValue ? energy_lu.SortKey.Value : 0m), TransType = energy_lu.TransType, TransTypeDesc = energy_lu.TransTypeDesc });
                    
                    foreach (DbsDbo.Material_LU material_lu in dbo.Material_LU)
                        shortDimList.Material_LU.Add(new DbsDim.Material_LU { AllowInASP = material_lu.AllowInASP.ToString(), AllowInCOKE = material_lu.AllowInCOKE.ToString(), AllowInFCHEM = material_lu.AllowInFCHEM.ToString(), AllowInFLUBE = material_lu.AllowInFLUBE.ToString(), AllowInMPROD = material_lu.AllowInMPROD.ToString(), AllowInOTHRM = material_lu.AllowInOTHRM.ToString(), AllowInPROD = material_lu.AllowInPROD.ToString(), AllowInRCHEM = material_lu.AllowInRCHEM.ToString(), AllowInRLUBE = material_lu.AllowInRLUBE.ToString(), AllowInRMI = material_lu.AllowInRMI.ToString(), AllowInSOLV = material_lu.AllowInSOLV.ToString(), MaterialID = material_lu.MaterialID, SAIName = material_lu.SAIName, TypicalDensity = material_lu.TypicalDensity, TankType = material_lu.TankType });
                                        
                    foreach (DbsDbo.Pers_LU pers_lu in dbo.Pers_LU)
                        shortDimList.Pers_LU.Add(new DbsDim.Pers_LU { PersCode = int.Parse(pers_lu.PersCode.HasValue ? pers_lu.PersCode.Value.ToString() : "0"), PersID = pers_lu.PersID, SectionID = pers_lu.SectionID, PersDescription = pers_lu.Description});

                    foreach (DbsDbo.Opex_LU opex_lu in dbo.Opex_LU)
                        shortDimList.OpEx_LU.Add(new DbsDim.OpEx_LU { Description = opex_lu.Description, Property = opex_lu.OpexID, SortOrder = opex_lu.SortKey?? 0 });
                    
                    foreach (DbsDbo.TankType_LU tanktype_lu in dbo.TankType_LU)
                        shortDimList.TankType_LU.Add(new DbsDim.TankType_LU { Description = tanktype_lu.Description, TankType = tanktype_lu.TankType });

                    foreach (DbsDbo.UOM uom in dbo.UOMs)
                        shortDimList.UOM.Add(new DbsDim.UOM { UOMCode = uom.UOMCode, UOMDesc = uom.UOMDesc, UOMGroup = uom.UOMGroup});
                }

                using (DbsDim.DimEntities db = new DbsDim.DimEntities())
                {
                    /* We aren't using this anymore
                    foreach (DbsDim.CptlCode_LU cptlcode_lu in db.CptlCode_LU)
                        shortDimList.CptlCode_LU.Add(cptlcode_lu);
                    */
                    foreach (DbsDim.EmissionType_LU emissiontype_lu in db.EmissionType_LU)
                        shortDimList.EmissionType_LU.Add(emissiontype_lu);

                    foreach (DbsDim.MaterialProp_LU materialProp_lu in db.MaterialProp_LU)
                        shortDimList.MaterialProp_LU.Add(materialProp_lu);

                    foreach (DbsDim.PressureRange_LU pressurerange_lu in db.PressureRange_LU)
                        shortDimList.PressureRange_LU.Add(pressurerange_lu);

                    foreach (DbsDim.Process_LU process_lu in db.Process_LU)
                        shortDimList.Process_LU.Add(process_lu);

                    foreach (DbsDim.ProductGrade_LU productgrade_lu in db.ProductGrade_LU)
                        shortDimList.ProductGrade_LU.Add(productgrade_lu);

                    foreach (DbsDim.ProductProperties_LU productProperties_lu in db.ProductProperties_LU)
                        shortDimList.ProductProperties_LU.Add(productProperties_lu);

                    foreach (DbsDim.ValidValuesList_LU validValuesList_lu in db.ValidValuesList_LU)
                        shortDimList.ValidValuesList_LU.Add(validValuesList_lu);

                    foreach (DbsDim.YieldCategory_LU yieldcategory_lu in db.YieldCategory_LU)
                        shortDimList.YieldCategory_LU.Add(yieldcategory_lu);
                    
                    foreach (DbsDim.FHFuelType_LU fHFuelType_LU in db.FHFuelType_LU)
                        shortDimList.FHFuelType_LU.Add(fHFuelType_LU);

                    foreach (DbsDim.FHProcessFluid_LU fHProcessFluid_LU in db.FHProcessFluid_LU)
                        shortDimList.FHProcessFluid_LU.Add(fHProcessFluid_LU);

                    foreach (DbsDim.FHService_LU fHService_LU in db.FHService_LU)
                        shortDimList.FHService_LU.Add(fHService_LU);


                    byte[] referenceBytes = Sa.Serialization.ToByteArray(shortDimList);
                    encryptedReferenceBytes = Sa.Crypto.Encrypt(referenceBytes);

                    statusMessage = string.Empty;
                    return encryptedReferenceBytes;
                }
            }
            catch (Exception exDim)
            {
                Exception errLocation = new Exception("Error in KNPC WCF Service.GetReference");
                Sa.Logger.LogException(errLocation);
                Sa.Logger.LogException(exDim);
                statusMessage = "Error in GetReference. Please try again later or contact Solomon Associates.";
                return new byte[0];
            }

        }        
        #endregion

        #region Get KPI
        public byte[] GetKPI(byte[] clientSubmissionIDBytes, byte[] clientKeyBytes, out string statusMessage)
        {
            byte[] encryptedKPIBytes;
            int clientSubmissionIDDecrypted;
            string clientKeyDecrypted;
            try
            {

                byte[] decrypedid = Sa.Crypto.Decrypt(clientSubmissionIDBytes);
                clientSubmissionIDDecrypted = Sa.Serialization.ToObject<int>(decrypedid);

                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);

                
                int dboSubmissionId = -1;
                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    //submissionID = db.ClientKeys_CurrentSubmissions.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted && s.ClientSubmissionID == clientSubmissionIDDecrypted).SubmissionId.ToString();
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        statusMessage = "Error in GetKPI. Invalid Client Key.";
                        return new byte[0];
                    }

                    dboSubmissionId =(int)db.SubmissionsToUse(clientSubmissionIDDecrypted).FirstOrDefault().DboSubmissionId;
                }
                string dboSubmissionID = dboSubmissionId.ToString();
                //need to get dbosubmission where it is not 'don't use' (meaning it = 1) and it matches clientsubmission key in [pearl].[RefnumberSubmissionMap]; order by submission id desc I guess

                using (DbsOutput.OutputEntities db = new DbsOutput.OutputEntities())
                {
                    DbsOutput.ShortOutput shortoutput = new DbsOutput.ShortOutput();

                    foreach (DbsOutput.DataSubmission dataSubmission in db.DataSubmissions.Where(s => s.SubmissionID == dboSubmissionId))
                    {
                        shortoutput.DataSubmissions.Add(
                            new DbsOutput.SubmissionData 
                            { 
                                CalcTime = dataSubmission.CalcTime,
                                DataKey = dataSubmission.DataKey,
                                NumberValue = dataSubmission.NumberValue,
                                StudyMethodology = dataSubmission.StudyMethodology,
                                SubmissionID = dataSubmission.SubmissionID,
                                TextValue = dataSubmission.TextValue,
                                DateVal = dataSubmission.DateVal
                            });
                    }
                    foreach (DbsOutput.DataTable datum in db.DataTables.Where(s => s.DataKey == dboSubmissionID))
                        shortoutput.DataTables.Add(datum);

                    foreach (DbsOutput.DataKey datakey in db.DataKeys.Where(s => s.DataKey1 == dboSubmissionID))
                        shortoutput.DataKeys.Add(datakey);

                    foreach (DbsOutput.GroupKey groupkey in db.GroupKeys.Where(s => s.GroupKey1 == dboSubmissionID))
                        shortoutput.GroupKeys.Add(groupkey);

                    foreach (DbsOutput.ProcessData processData in db.ProcessDatas.Where(s => s.SubmissionID == dboSubmissionId))
                        shortoutput.ProcessDatas.Add(processData);

                    byte[] KPIBytes = Sa.Serialization.ToByteArray(shortoutput);
                    encryptedKPIBytes = Sa.Crypto.Encrypt(KPIBytes);

                    statusMessage = string.Empty;
                    return encryptedKPIBytes;
                }
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                statusMessage = "Error in GetKPI. Please try again later or contact Solomon Associates.";
                return new byte[0];
            }

        }
        #endregion

        #region Get Status
        public byte[] GetStatus()
        {
            return new byte[5];
        }
        #endregion

        #region Get Submission
        public byte[] GetSubmission(byte[] clientSubmissionIDBytes, byte[] clientKeyBytes, out string statusMessage)
        {
            string clientKeyDecrypted;
            int clientSubmissionIDDecrypted;
            try
            {
                byte[] decrypedid = Sa.Crypto.Decrypt(clientSubmissionIDBytes);
                clientSubmissionIDDecrypted = Sa.Serialization.ToObject<int>(decrypedid);

                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);

                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        statusMessage = "Error in GetSubmission. Invalid Client Key.";
                        return new byte[0];
                    }
                    else
                    {
                        if (db.ClientKeys_CurrentSubmissions.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted && s.ClientSubmissionID == clientSubmissionIDDecrypted) == null)
                        {
                            statusMessage = "Error in GetSubmission. Invalid Client Submission ID.";
                            return new byte[0];
                        }
                    }
                }

                using (DbsStage.StageEntities db = new DbsStage.StageEntities())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    db.Configuration.LazyLoadingEnabled = false;

                    //wrong, it returns SOlomon submission ID  int clientSubmissionID = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).SubmissionID;
                    int solomonSubmissionID = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).SubmissionID;
                    DbsStage.Submission submission = new DbsStage.Submission();

                    submission.SubmissionID = clientSubmissionIDDecrypted;
                    submission.ClientSubmissionID = clientSubmissionIDDecrypted;
                    submission.RefineryID = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).RefineryID;
                    submission.PeriodBeg = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).PeriodBeg;
                    submission.UOM = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).UOM;
                    submission.RptCurrency = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).RptCurrency;

                    foreach (DbsStage.Absence absence in db.Absences.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Absences.Add(absence);

                    foreach (DbsStage.Comment comment in db.Comments.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Comments.Add(comment);

                    foreach (DbsStage.Config config in db.Configs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Configs.Add(config);

                    foreach (DbsStage.ConfigBuoy configbuoy in db.ConfigBuoys.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.ConfigBuoys.Add(configbuoy);

                    foreach (DbsStage.ConfigR configr in db.ConfigRS.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.ConfigRS.Add(configr);

                    foreach (DbsStage.Crude crude in db.Crudes.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Crudes.Add(crude);

                    foreach (DbsStage.Diesel diesel in db.Diesels.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Diesels.Add(diesel);

                    foreach (DbsStage.Emission emission in db.Emissions.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Emissions.Add(emission);

                    foreach (DbsStage.Energy energy in db.Energies.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Energies.Add(energy);

                    foreach (DbsStage.FinProdProp finProdProp in db.FinProdProps.Where(s => s.SubmissionId == solomonSubmissionID))
                        submission.FinProdProps.Add(finProdProp);

                    foreach (DbsStage.FiredHeater firedheater in db.FiredHeaters.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.FiredHeaters.Add(firedheater);

                    foreach (DbsStage.Gasoline gasoline in db.Gasolines.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Gasolines.Add(gasoline);

                    foreach (DbsStage.GeneralMisc generalmisc in db.GeneralMiscs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.GeneralMiscs.Add(generalmisc);

                    foreach (DbsStage.Inventory inventory in db.Inventories.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Inventories.Add(inventory);

                    foreach (DbsStage.Kerosene kerosene in db.Kerosenes.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Kerosenes.Add(kerosene);

                    foreach (DbsStage.LPG lpg in db.LPGs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.LPGs.Add(lpg);

                    foreach (DbsStage.MaintRout maintrout in db.MaintRouts.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MaintRouts.Add(maintrout);

                    foreach (DbsStage.MaintTA maintta in db.MaintTAs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MaintTAs.Add(maintta);

                    foreach (DbsStage.MarineBunker marinebunker in db.MarineBunkers.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MarineBunkers.Add(marinebunker);

                    foreach (DbsStage.MExp mexp in db.MExps.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MExps.Add(mexp);

                    foreach (DbsStage.MiscInput miscinput in db.MiscInputs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MiscInputs.Add(miscinput);

                    foreach (DbsStage.OpEx opex in db.OpExes.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.OpExes.Add(opex);

                    foreach (DbsStage.Personnel personnel in db.Personnels.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Personnels.Add(personnel);

                    foreach (DbsStage.ProcessData processdata in db.ProcessDatas.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.ProcessDatas.Add(processdata);

                    foreach (DbsStage.Resid resid in db.Resids.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Resids.Add(resid);

                    foreach (DbsStage.RPFResid rpfresid in db.RPFResids.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.RPFResids.Add(rpfresid);

                    foreach (DbsStage.Steam steam in db.Steams.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Steams.Add(steam);

                    foreach (DbsStage.Yield yield in db.Yields.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Yields.Add(yield);

                    byte[] submissionBytes = Sa.Serialization.ToByteArray(submission);
                    byte[] encryptedSubmissionBytes = Sa.Crypto.Encrypt(submissionBytes);

                    statusMessage = string.Empty;
                    return encryptedSubmissionBytes;
                }
            }
            catch (Exception)
            {
                statusMessage = "Error in GetSubmission. Please try again later or contact Solomon Associates.";
                return new byte[0];
            }
        }
        #endregion

        #region Submit Data
        public string SubmitData(byte[] SubmissionBytes, byte[] clientKeyBytes)
        {
            Sa.Logger.LogInfo("Entering Service.svc.cs.SubmitData()");
            string clientKeyDecrypted;
            string status = string.Empty;
            string refnum = string.Empty;
            int stagingSubmissionid;
            int dboSubmissionId = -1;
            int clientSubmissionId;
            try
            {
                byte[] unencryptedSubmissionBytes = Sa.Crypto.Decrypt(SubmissionBytes);
                DbsStage.Submission submission = Sa.Serialization.ToObject<DbsStage.Submission>(unencryptedSubmissionBytes);

                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);
                Sa.Logger.LogInfo("Decryption done");
                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        Sa.Logger.LogException(new Exception("Error in SubmitData. Invalid Client Key: " + clientKeyDecrypted));
                        status = "Error in SubmitData. Invalid Client Key.";
                        return status;
                    }
                    else
                    {
                        refnum = db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted).Refnumber;
                    }
                }
                bool success = false;

                using (var db = new DbsStage.StageEntities())
                {
                    Sa.Logger.LogInfo("Preparing to create new StageEntities.submission record.");
                    try
                    {
                        db.Database.Connection.Open();
                        submission.PeriodEnd = null;
                        submission.tsInserted = DateTimeOffset.Now;
                        Assembly ai = Assembly.GetExecutingAssembly();
                        submission.tsInsertedApp = ai.GetName().Name + " (" + ai.GetName().Version + ")";
                        submission.tsInsertedHost = System.Environment.MachineName;
                        submission.tsInsertedUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                        db.Submissions.Add(submission);
                        db.SaveChanges();
                        stagingSubmissionid = submission.SubmissionID;
                        clientSubmissionId = submission.ClientSubmissionID;
                    }
                    catch (Exception exStagingMove)
                    {
                        Exception exStaging = new Exception("Error moving KNPC data to Staging for refnum " + refnum);
                        Sa.Logger.LogException(exStaging);
                        Sa.Logger.LogException(exStagingMove);
                        status = "There was a problem moving your data in the Solomon database.";
                        return status;
                    }
                }

                Sa.Pearl.StagingToDbo stagingToDbo = new Sa.Pearl.StagingToDbo();
                success = stagingToDbo.MoveStagingToDbo(stagingSubmissionid, refnum, ref dboSubmissionId);
                if (!success)
                {
                    Exception exStaging = new Exception("Error moving KNPC data from Staging to Dbo for Staging submission " + stagingSubmissionid.ToString());
                    Sa.Logger.LogException(exStaging);
                    status = "There was a problem moving your data in the Solomon database.";
                    return status;
                }

                //insert submission info into pearl x-ref table
                Sa.Logger.LogInfo("Going to enter record into RefnumberSubmissionMap");
                try
                {
                    using (var db = new Sa.Pearl.PearlEntities())
                    {
                        db.Database.Connection.Open();
                        Sa.Pearl.RefnumberSubmissionMap rnsm = new Sa.Pearl.RefnumberSubmissionMap();
                        rnsm.Refnumber = refnum;
                        rnsm.SubmissionId = stagingSubmissionid;
                        rnsm.ClientSubmissionId = clientSubmissionId;
                        rnsm.DboSubmissionId = dboSubmissionId;
                        db.RefnumberSubmissionMaps.Add(rnsm);
                        db.SaveChanges();
                    }
                }
                catch (Exception submissionMapException)
                {
                    Sa.Logger.LogException(new Exception("Error writing to Sa.Pearl.RefnumberSubmissionMap table for Staging submission " + stagingSubmissionid.ToString()));
                    Sa.Logger.LogException(submissionMapException);
                    status = "There was a problem moving your data in the Solomon database.";
                    return status;
                }
                
                //run calcs
                Sa.Logger.LogInfo("Going to call stagingToDbo.callSpCompleteSubmission for submission " + dboSubmissionId.ToString());
                //StagingToDbo stagingToDbo = new StagingToDbo();
                if (!stagingToDbo.callSpCompleteSubmission(dboSubmissionId, false))
                {
                    //error already logged in callSpCompleteSubmission
                    status = "There was a problem while running your calculations in the Solomon database.";
                    return status;
                }

                using (var db = new Sa.dbs.Dbo.DboEntities())
                {
                    db.Database.Connection.Open();
                    try
                    {
                        //call dbs.dbo.[dbo].[EtlOutput] to move from dbo to output.                         
                        string methodology = ConfigurationManager.AppSettings["Methodology"].ToString();
                        object cursor = db.EtlOutput(methodology, dboSubmissionId);
                    }
                    catch (Exception exEtl)
                    {
                        Exception exMethodology = new Exception("Error getting Methodology setting from app.config in SubmitData()");
                        Sa.Logger.LogException(exMethodology);
                        Sa.Logger.LogException(exEtl);
                        status = "There was a problem moving your data in the Solomon database.";
                        return status;
                    }
                }
            }
            catch (Exception exSubmitData)
            {
                Sa.Logger.LogException(exSubmitData);
                status = "Error in SubmitData. Please try again later or contact Solomon Associates.";
            }
            return status;
        }
        #endregion

        #region Create Data Table
        public static DataTable CreateDataTable<T>(string tableName)
        {
            var dt = new DataTable(tableName);

            var propList = typeof(T).GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

            foreach (MemberInfo info in propList)
            {
                if (info is PropertyInfo)
                    //dt.Columns.Add(new DataColumn(info.Name, (info as PropertyInfo).PropertyType));
                    //dt.Columns.Add(info.Name, Nullable.GetUnderlyingType((info as PropertyInfo).PropertyType) ?? (info as PropertyInfo).PropertyType);
                    dt.Columns.Add(info.Name);
                else if (info is FieldInfo)
                    dt.Columns.Add(new DataColumn(info.Name, (info as FieldInfo).FieldType));
            }

            return dt;
        }
        #endregion

        #region Fill Data Table
        public static void FillDataTable<T>(DataTable dt, List<T> items)
        {
            var propList = typeof(T).GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

            foreach (T t in items)
            {
                var row = dt.NewRow();
                foreach (MemberInfo info in propList)
                {
                    if (info is PropertyInfo)
                    {
                        row[info.Name] = (info as PropertyInfo).GetValue(t, null);
                    }
                    else if (info is FieldInfo)
                        row[info.Name] = (info as FieldInfo).GetValue(t);
                }
                dt.Rows.Add(row);
            }
        }
        #endregion

    }
}
