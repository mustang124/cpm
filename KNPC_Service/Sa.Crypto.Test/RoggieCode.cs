﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//Had to name this Sa.CryptoDll instead of Sa.Crypto due to naming clash

namespace Sa.CryptoDll.Test
{

    //There was a console app that was used to test; replacing it with this unit test.

    [TestClass]
    public class RoggieCode
    {
        const string LoremIpsum = "Lorem ipsum dolor sit amet";//, consectetur adipiscing elit. Nullam pellentesque facilisis imperdiet. Nam iaculis ex vitae purus lobortis rhoncus. Duis eget purus massa. Donec quis lectus non magna luctus vehicula at nec turpis. Sed pretium tristique tellus, semper gravida nibh sodales a. Proin in sagittis purus. Aenean massa diam, pretium at dignissim sit amet, malesuada et nibh. Pellentesque cursus urna quis lorem laoreet, et dapibus ante facilisis. Integer eget volutpat lacus. Etiam feugiat vehicula dui, non bibendum velit tincidunt in. Integer et mollis quam, id volutpat ipsum. Nam ac nunc nulla. Mauris ac augue dapibus magna consequat imperdiet eu vel nisl. Quisque a magna eu dolor cursus tincidunt. Etiam aliquam semper urna eget ornare. Integer tincidunt sapien urna, eu laoreet nisl maximus ac. Etiam molestie tortor in augue semper, ac euismod elit aliquet. Morbi sed tortor eu urna ultricies imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc elit nisi, semper sit amet dapibus et, volutpat nec lacus. Nunc aliquet posuere elit scelerisque malesuada. Fusce ut neque hendrerit, ultrices leo eget, varius ex. Morbi at purus et justo facilisis blandit. Aliquam et tellus dapibus, viverra justo id, mollis odio. Donec metus diam, condimentum eget ultricies sit amet, ornare sit amet leo. Quisque faucibus augue ac nunc ultrices lobortis. Aliquam volutpat tincidunt tellus ullamcorper varius. Ut blandit, libero vel pulvinar viverra, mauris nunc laoreet nisl, eget ornare neque leo a felis. Integer ornare finibus cursus. Vivamus lobortis lorem ipsum, ut mollis lorem mattis ac. In suscipit scelerisque mollis. Integer non magna varius, fermentum elit sed, feugiat sapien. Aliquam eleifend ornare ex at convallis. Proin nec tortor metus. Aenean semper, ipsum facilisis auctor luctus, massa nibh aliquet velit, vel cursus eros magna ac turpis. Vestibulum ut turpis mattis, interdum nibh eget, pretium magna. Fusce diam velit, rutrum in sem sit amet, lobortis porttitor urna. Etiam at mattis libero, sed iaculis mi. Praesent fringilla libero nec sem dignissim viverra. Nullam pulvinar euismod orci vitae dictum. In non aliquet est. Curabitur quis congue augue. Nulla ut libero facilisis ante interdum vestibulum non eu ligula. Aenean consequat tellus nec ligula suscipit feugiat. Integer id mollis orci. Nulla sed ligula ut felis facilisis luctus. Praesent et tempor nisi, ac feugiat magna. Nunc vel orci metus. Nullam semper mi diam, id malesuada mauris vulputate ornare. Donec elit augue, euismod sed efficitur vitae, sodales sed turpis. Donec vitae felis eget dolor commodo aliquam eget at ante. Praesent rhoncus fringilla lacus nec placerat. Donec tincidunt, lacus nec varius eleifend, elit massa condimentum ex, at vestibulum lorem tellus sollicitudin nisl. Integer vel lectus massa. Cras semper id leo quis dapibus.";

        [TestMethod]
        public void RoggieCode1()
        {
            //byte[] PlainData = Encoding.ASCII.GetBytes(p);
            byte[] PlainData = Sa.Serialization.ToByteArray(LoremIpsum);
            
            //byte[] Encrypted = Sa.Crypto.Encrypt(PlainData, ePassword, eSalt, sKey);
            byte[] Encrypted = Sa.Crypto.Encrypt(PlainData);

            // Send Data...

            //byte[] Decrypted = Sa.Crypto.Decrypt(Encrypted, ePassword, eSalt, sKey);
            byte[] Decrypted = Sa.Crypto.Decrypt(Encrypted);

            //string PlainText = Encoding.ASCII.GetString(Decrypted);
            string PlainText = Sa.Serialization.ToObject<string>(Decrypted);

            Assert.AreEqual(PlainText, LoremIpsum, true);

        }

        [TestMethod]
        public void RoggieCode2()
        {
            string s = Sa.Pearl.SHA512ClientKey.Generate();
            //Console.WriteLine(s);

            //using (Sa.Pearl.PearlEntities pe = new Pearl.PearlEntities())
            //{
            //    string refnumber = pe.ClientKeys.FirstOrDefault(ck => ck.ClientKey1 == "<ClientKeyString>").Refnumber;


            //}

            //Console.WriteLine("press any key");
            //Console.ReadKey();
            Assert.IsTrue(true); //Nothing to compare. He was just running the code to see a result.
        }
    }
}
