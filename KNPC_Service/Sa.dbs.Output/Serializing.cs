﻿using System;

namespace Sa.dbs.Output
{

    //since the [Serializable] decoration in the .tt file gets lost when regenerate edmx, 
    //an using this page to try to merge in the decoration during build.

    [Serializable]
    partial class CalcTime { }
    [Serializable]
    partial class DataKey { }
    [Serializable]
    partial class DataKeysUnit { }
    [Serializable]
    partial class DataSubmission { }
    [Serializable]
    partial class DataTable { }
    [Serializable]
    partial class GroupKey { }
    [Serializable]
    partial class Message { }
    [Serializable]
    partial class ProcessData { }
}