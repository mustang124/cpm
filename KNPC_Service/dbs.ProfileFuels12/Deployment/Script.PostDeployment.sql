﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


SET NOCOUNT ON;

:r .\Dim\Absence_LU.sql			--	 10
:r .\Dim\CptlCode_LU.sql		--	  4
:r .\Dim\Crude_LU.sql			--	869
:r .\Dim\EmissionType_LU.sql	--	  3
:r .\Dim\Energy_LU.sql			--	109
:r .\Dim\Material_LU.sql		--  290
:r .\Dim\OpEx_LU.sql			--	112
:r .\Dim\Pers_LU.sql			--	 74
:r .\Dim\PressureRange.sql		--	  5
:r .\Dim\Process_LU.sql			--	292
:r .\Dim\ProductGrade_LU.sql	--	 64
:r .\Dim\TankType_LU.sql		--	  6
:r .\Dim\UOM.sql				--	116
:r .\Dim\YieldCategory_LU.sql	--	 15

:r .\Dim\FHFuelType_LU.sql		--	  4
:r .\Dim\FHProcessFluid_LU.sql	--	  7
:r .\Dim\FHService_LU.sql		--	 13

:r .\Pearl\ClientKeys.sql

:r .\Dim\dim.ProductPropertiesLuInsert.sql	
:r .\Dim\dim.MaterialPropLuInsert.sql	
:r .\Dim\dim.ValidValuesListLuInsert.sql

:r .\Dbo\EtlInsert.sql

:r .\Output\DataKeysInsert.sql


INSERT INTO [output].[Messages] ([RefineryID],[MessageType],[MessageValue]) VALUES
           ('162EUR', 'ReferenceDataChanged','FALSE');
INSERT INTO [output].[Messages] ([RefineryID],[MessageType],[MessageValue]) VALUES
           ('163EUR', 'ReferenceDataChanged','FALSE');
INSERT INTO [output].[Messages] ([RefineryID],[MessageType],[MessageValue]) VALUES
           ('166EUR', 'ReferenceDataChanged','FALSE');		     
