﻿DELETE FROM [dim].[Absence_LU];
DELETE FROM [dim].[CptlCode_LU];
DELETE FROM [dim].[Crude_LU];
DELETE FROM [dim].[EmissionType_LU];
DELETE FROM [dim].[Energy_LU];
DELETE FROM [dim].[Material_LU];
DELETE FROM [dim].[OpEx_LU];
DELETE FROM [dim].[Pers_LU];
DELETE FROM [dim].[PressureRange_LU];
DELETE FROM [dim].[Process_LU];
DELETE FROM [dim].[ProductGrade_LU];
DELETE FROM [dim].[TankType_LU];
DELETE FROM [dim].[UOM];
DELETE FROM [dim].[YieldCategory_LU];

DELETE FROM [dim].[FHFuelType_LU];
DELETE FROM [dim].[FHProcessFluid_LU];
DELETE FROM [dim].[FHService_LU];