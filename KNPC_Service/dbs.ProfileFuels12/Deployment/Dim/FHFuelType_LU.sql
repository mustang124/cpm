﻿INSERT INTO [dim].[FHFuelType_LU]
(
	[FuelType],
	[Description]
)
SELECT
	[t].[FuelType],
	[t].[Description]
FROM (VALUES
('CMB', 'Combination Fuel (i.e. Gas/Liquid)'),
('GAS', 'Gas Fuel'),
('LIQ', 'Liquid Fuel'),
('SOL', 'Solid Fuel (i.e. Coal/Coke)')
) [t] ([FuelType], [Description])
LEFT OUTER JOIN
	[dim].[FHFuelType_LU]	[l]
		ON	[l].[FuelType]	= [t].[FuelType]
WHERE	[l].[FuelType]	IS NULL;