﻿INSERT INTO [dim].[EmissionType_LU]
(
	[EmissionType],
	[Description]
)
SELECT
	[t].[EmissionType],
	[t].[Description]
FROM (VALUES
('GHG', 'Greenhouse Gas (CO2 Equivalents)'),
('NOx', 'Nitrogen Oxides (NOx)'),
('SOx', 'Sulfur Dioxide (SO2)')
) [t] ([EmissionType], [Description])
LEFT OUTER JOIN
	[dim].[EmissionType_LU]	[l]
		ON	[l].[EmissionType]	= [t].[EmissionType]
WHERE	[l].[EmissionType]	IS NULL;