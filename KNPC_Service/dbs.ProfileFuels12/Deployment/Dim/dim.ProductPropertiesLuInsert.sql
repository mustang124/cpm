﻿INSERT INTO dim.ProductProperties_LU([PropertyId], [USDescription], [MetDescription], [USUnits], [MetUnits], [USDecPnt], [MetDecPnt], [SortKey], [FldType], [BegEffDate], [EndEffDate]) 

SELECT [t].[PropertyId],[t].[USDescription],[t].[MetDescription],[t].[USUnits],[t].[MetUnits],[t].[USDecPnt],[t].[MetDecPnt],[t].[SortKey],[t].[FldType],[t].[BegEffDate],[t].[EndEffDate]
FROM (VALUES 
(1,'Market', 'Market', NULL, NULL, NULL,NULL,100,'Text', '01/01/2012', NULL),	
(2,'Type', 'Type', NULL, NULL, NULL,NULL,110,'Text', '01/01/2012', NULL),
(3,'Gravity, °API', 'Density, kg/m³ (at standard conditions)', '°API', 'kg/m³', 1,0,120,'Number', '01/01/2012', NULL),
(4,'Reid Vapor Pressure of Blend, psia', 'Reid Vapor Pressure of Blend, bara', 'psia', 'bara', 1,2,130,'Number', '01/01/2012', NULL),
(5,'Octane Number of Blend', 'Octane Number of Blend', NULL, NULL, NULL,NULL,140,'Heading', '01/01/2012', NULL),
(6,'RON', 'RON', 'RON', 'RON', 1,1,150,'Number', '01/01/2012', NULL),
(7,'MON', 'MON', 'MON', 'MON', 1,1,160,'Number', '01/01/2012', NULL),
(8,'Concentration of Oxygenates in Blend, vol %', 'Concentration of Oxygenates in Blend, vol %', NULL, NULL, NULL,NULL,170,'Heading', '01/01/2012', NULL),
(9,'Ethanol', 'Ethanol', 'vol %', 'vol %', 1,1,180,'Number', '01/01/2012', NULL),
(10,'Other (MTBE, ETBE, TAME, etc.)', 'Other (MTBE, ETBE, TAME, etc.)', 'vol %', 'vol %', 1,1,190,'Number', '01/01/2012', NULL),
(11,'Sulfur of Blend, ppm (wt)', 'Sulfur of Blend, ppm (wt)', 'ppm (wt)', 'ppm (wt)', 0,0,200,'Number', '01/01/2012', NULL),
(12,'Market Specifications', 'Market Specifications', NULL, NULL, NULL,NULL,210,'Heading', '01/01/2012', NULL),
(13,'Sulfur Specification, maximum ppm (wt)', 'Sulfur Specification, maximum ppm (wt)', 'ppm (wt)', 'ppm (wt)', 0,0,220,'Number', '01/01/2012', NULL),
(14,'Ethanol, maximum vol %', 'Ethanol, maximum vol %', 'vol %', 'vol %', 1,1,230,'Number', '01/01/2012', NULL),
(15,'Volume, kbbl', 'Weight, k t', 'k bbl', 'k t', 0,0,240,'Number', '01/01/2012', NULL),
(16,'ASTM D-86 90% Distillation Point, °F', 'ASTM D-86 90% Distillation Point, °C', '°F', '°C', 0,0,250,'Number', '01/01/2012', NULL),
(17,'ASTM D-86 E350 °C, vol % (Euro Grades)', 'ASTM D-86 E350 °C, vol % (Euro Grades)', 'vol %', 'vol %', 1,1,260,'Number', '01/01/2012', NULL),
(18,'Biodiesel in Blend, vol %', 'Biodiesel in Blend, vol %', 'vol %', 'vol %', 1,1,270,'Number', '01/01/2012', NULL),
(19,'Cetane Index of Blend', 'Cetane Index of Blend', NULL, NULL, 1,1,280,'Number', '01/01/2012', NULL),
(20,'Cetane Number of Blend', 'Cetane Number of Blend', NULL, NULL, 1,1,290,'Number', '01/01/2012', NULL),
(21,'CFPP of Blend, °F', 'CFPP of Blend, °C', '°F', '°C', 0,0,300,'Number', '01/01/2012', NULL),
(22,'Cloud Point of Blend,°F', 'Cloud Point of Blend,°C', '°F', '°C', 0,0,310,'Number', '01/01/2012', NULL),
(23,'Lead Level of Blend, gm Pb/liter', 'Lead Level of Blend, gm Pb/liter', 'gm Pb/liter', 'gm Pb/liter', 2,2,320,'Number', '01/01/2012', NULL),
(24,'Viscosity, maximum cSt @ 104 °F', 'Viscosity, maximum cSt @ 40 °C', 'cSt @ 104 °F', 'cSt @ 40 °C', 1,1,330,'Number', '01/01/2012', NULL),
(25,'Viscosity, cSt @ 104 °F', 'Viscosity, cSt @ 40 °C', 'cSt @ 104 °F', 'cSt @ 40 °C', 1,1,340,'Number', '01/01/2012', NULL),
(26,'Viscosity, cSt @ 122 °F', 'Viscosity, cSt @ 50 °C', 'cSt @ 122 °F', 'cSt @ 50 °C', 1,1,350,'Number', '01/01/2012', NULL),
(27,'Temperature if Different Than 122 °F', 'Temperature if Different Than 50 °C', NULL, NULL, 0,0,360,'Number', '01/01/2012', NULL),
(28,'Cracked Stock, vol %', 'Cracked Stock, vol %', NULL, NULL, 1,1,370,'Number', '01/01/2012', NULL),
(29,'Pour Point, °F', 'Pour Point, °C', '°F', '°C', 1,1,380,'Number', '01/01/2012', NULL),
(30,'Composition (Mole % / Volume %)', 'Composition (Mole % / Volume %)', NULL, NULL, NULL,NULL,390,'Number', '01/01/2012', NULL),
(31,'C2 and Lighter', 'C2 and Lighter', NULL, NULL, 1,1,400,'Text', '01/01/2012', NULL),
(32,'C3', 'C3', NULL, NULL, 1,1,410,'Number', '01/01/2012', NULL),
(33,'C3=', 'C3=', NULL, NULL, 1,1,420,'Number', '01/01/2012', NULL),
(34,'iC4', 'iC4', NULL, NULL, 1,1,430,'Number', '01/01/2012', NULL),
(35,'nC4', 'nC4', NULL, NULL, 1,1,440,'Number', '01/01/2012', NULL),
(36,'C4=', 'C4=', NULL, NULL, 1,1,450,'Number', '01/01/2012', NULL),
(37,'C5 and Heavier', 'C5 and Heavier', NULL, NULL, 1,1,460,'Number', '01/01/2012', NULL),
(38,'Sulfur, wt %', 'Sulfur, wt %', NULL, NULL, 2,2,470,'Number', '01/01/2012', NULL),
(39,'Sulfur Specification, maximum wt %', 'Sulfur Specification, maximum wt %', NULL, NULL, 2,2,480,'Number', '01/01/2012', NULL)

) [t] (	[PropertyId],[USDescription],[MetDescription],[USUnits],[MetUnits],[USDecPnt],[MetDecPnt],[SortKey],[FldType],[BegEffDate],[EndEffDate])

LEFT OUTER JOIN
	[dim].[ProductProperties_LU]	[l]
		ON	[l].[PropertyId]	= [t].[PropertyId]
		
WHERE	[l].[PropertyId] IS NULL;