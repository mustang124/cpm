﻿INSERT INTO [dim].[FHService_LU]
(
	[Service],
	[Description]
)
SELECT
	[t].[Service],
	[t].[Description]
FROM (VALUES
('BOILHR', 'Boiler - Heat Recovery'),
('BOILPG', 'Boiler - Pressure'),
('BOILSG', 'Boiler - Steam Generation'),
('FCHG', 'Fractionator Charge'),
('FLRPLT', 'Flare Pilot Gas'),
('ICE', 'Internal Combustion Engine'),
('ICN', 'Incinerator'),
('OTH', 'Other'),
('OTHER', 'Other'),
('REB', 'Reboiler'),
('RXCHG', 'Reactor Charge'),
('SMR', 'Steam Methane Reforming'),
('TANK', 'Heavy Oil Tank Heater')
) [t] ([Service], [Description])
LEFT OUTER JOIN
	[dim].[FHService_LU]	[l]
		ON	[l].[Service]	= [t].[Service]
WHERE	[l].[Service]	IS NULL;