﻿INSERT INTO [dim].[PressureRange_LU]
(
	[PressureRange]
)
SELECT
	[t].[PressureRange]
FROM (VALUES
('<51'),
('>600'),
('151-300'),
('301-600'),
('51-150')
) [t] ([PressureRange])
LEFT OUTER JOIN
	[dim].[PressureRange_LU]	[l]
		ON	[l].[PressureRange]	= [t].[PressureRange]
WHERE	[l].[PressureRange]	IS NULL;