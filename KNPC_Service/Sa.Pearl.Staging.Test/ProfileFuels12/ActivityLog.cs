//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Pearl.Staging.Test.ProfileFuels12
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActivityLog
    {
        public System.DateTime ActivityTime { get; set; }
        public string Application { get; set; }
        public string Methodology { get; set; }
        public string RefineryID { get; set; }
        public string CallerIP { get; set; }
        public string UserID { get; set; }
        public string ComputerName { get; set; }
        public string Service { get; set; }
        public string Method { get; set; }
        public string EntityName { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
    }
}
