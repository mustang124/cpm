//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Pearl.Staging.Test.ProfileFuels12
{
    using System;
    using System.Collections.Generic;
    
    public partial class StudyT2_FCC
    {
        public string RefineryID { get; set; }
        public System.DateTime EffDate { get; set; }
        public System.DateTime EffUntil { get; set; }
        public int UnitID { get; set; }
        public Nullable<float> FeedTemp { get; set; }
        public Nullable<float> PHeatTemp { get; set; }
        public Nullable<float> Conv { get; set; }
        public Nullable<float> CatOilRatio { get; set; }
        public Nullable<float> Coke { get; set; }
        public Nullable<float> FeedGravity { get; set; }
        public Nullable<float> FeedDensity { get; set; }
        public Nullable<float> FeedSulfur { get; set; }
        public Nullable<float> FeedConCarbon { get; set; }
        public Nullable<float> FeedASTM10 { get; set; }
        public Nullable<float> FeedASTM50 { get; set; }
        public Nullable<float> FeedASTM90 { get; set; }
        public Nullable<float> FeedAniline { get; set; }
        public Nullable<float> UOPK { get; set; }
        public Nullable<float> EstCokeSulfur { get; set; }
        public Nullable<float> FeedUVirginGO { get; set; }
        public Nullable<float> FeedUCrackedGO { get; set; }
        public Nullable<float> FeedUAGO { get; set; }
        public Nullable<float> FeedUVGO { get; set; }
        public Nullable<float> FeedUDAO { get; set; }
        public Nullable<float> FeedUCGO { get; set; }
        public Nullable<float> FeedUARC { get; set; }
        public Nullable<float> FeedUVR { get; set; }
        public Nullable<float> FeedULube { get; set; }
        public Nullable<float> FeedHVirginGO { get; set; }
        public Nullable<float> FeedHCrackedGO { get; set; }
        public Nullable<float> FeedHAGO { get; set; }
        public Nullable<float> FeedHVGO { get; set; }
        public Nullable<float> FeedHDAO { get; set; }
        public Nullable<float> FeedHCGO { get; set; }
        public Nullable<float> FeedHARC { get; set; }
        public Nullable<float> FeedHVR { get; set; }
        public Nullable<float> FeedHLube { get; set; }
        public Nullable<float> FeedN2 { get; set; }
        public Nullable<float> FeedMetals { get; set; }
        public Nullable<float> FeedUVGOGravity { get; set; }
        public Nullable<float> FeedUVGODensity { get; set; }
        public Nullable<float> FeedUVGOSulfur { get; set; }
        public Nullable<float> FeedUVGOAniline { get; set; }
        public Nullable<float> FeedUVGOUOPK { get; set; }
        public Nullable<float> FeedUVRGravity { get; set; }
        public Nullable<float> FeedUVRDensity { get; set; }
        public Nullable<float> FeedUVRSulfur { get; set; }
        public Nullable<float> FeedUVRConCarbon { get; set; }
        public Nullable<float> FeedHVGOGravity { get; set; }
        public Nullable<float> FeedHVGODensity { get; set; }
        public Nullable<float> FeedHVGOSulfur { get; set; }
        public Nullable<float> FeedHVGOAniline { get; set; }
        public Nullable<float> FeedHVGOUOPK { get; set; }
        public Nullable<float> FeedHVRGravity { get; set; }
        public Nullable<float> FeedHVRDensity { get; set; }
        public Nullable<float> FeedHVRSulfur { get; set; }
        public Nullable<float> FeedHVRConCarbon { get; set; }
        public Nullable<float> FeedMaxSulfur { get; set; }
        public Nullable<float> FeedMaxN2 { get; set; }
        public Nullable<float> ProdC2 { get; set; }
        public Nullable<float> ProdC3 { get; set; }
        public Nullable<float> ProdC4 { get; set; }
        public Nullable<float> ProdC3C4 { get; set; }
        public Nullable<float> ProdC4Lt { get; set; }
        public Nullable<float> ProdGasNap { get; set; }
        public Nullable<float> ProdLCO { get; set; }
        public Nullable<float> ProdHCO { get; set; }
        public Nullable<float> ProdDSO { get; set; }
        public Nullable<float> Yield { get; set; }
        public Nullable<float> ProdSulfur { get; set; }
        public Nullable<float> CokePcnt { get; set; }
        public Nullable<float> TotYield { get; set; }
        public Nullable<float> CSubA { get; set; }
        public Nullable<float> UOP_KFactor { get; set; }
        public Nullable<float> FeedUVGOUCap { get; set; }
        public Nullable<float> FeedUVRUCap { get; set; }
        public Nullable<float> FeedHVGOUCap { get; set; }
        public Nullable<float> FeedHVRUCap { get; set; }
        public Nullable<float> CatGasoUCap { get; set; }
        public Nullable<float> UtilCapMT { get; set; }
        public Nullable<float> FeedUVGOUCapMT { get; set; }
        public Nullable<float> FeedUVRUCapMT { get; set; }
        public Nullable<float> FeedHVGOUCapMT { get; set; }
        public Nullable<float> FeedHVRUCapMT { get; set; }
        public Nullable<float> FeedUOthUCap { get; set; }
        public Nullable<float> FeedHOthUCap { get; set; }
        public Nullable<float> FeedOthUCap { get; set; }
        public Nullable<float> EstFeedUOthUCapMT { get; set; }
        public Nullable<float> EstFeedHOthUCapMT { get; set; }
        public Nullable<float> EstFeedOthUCapMT { get; set; }
        public Nullable<float> EstFeedOthDensity { get; set; }
        public Nullable<float> EstFeedOthSulfur { get; set; }
        public Nullable<float> COBlrFiredCap { get; set; }
    }
}
