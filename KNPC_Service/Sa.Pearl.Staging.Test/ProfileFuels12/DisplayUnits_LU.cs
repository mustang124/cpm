//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Pearl.Staging.Test.ProfileFuels12
{
    using System;
    using System.Collections.Generic;
    
    public partial class DisplayUnits_LU
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DisplayUnits_LU()
        {
            this.ProcessID_LU = new HashSet<ProcessID_LU>();
        }
    
        public string DisplayUnits { get; set; }
        public string DisplayTextUS { get; set; }
        public string DisplayTextMet { get; set; }
        public string DisplayTextUSRussian { get; set; }
        public string DisplayTextMetRussian { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessID_LU> ProcessID_LU { get; set; }
    }
}
