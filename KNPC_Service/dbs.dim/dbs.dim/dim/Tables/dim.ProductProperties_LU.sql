﻿CREATE TABLE [dim].[ProductProperties_LU] (
    [PropertyId]     INT           NOT NULL,
    [USDescription]  NVARCHAR (50) NULL,
    [MetDescription] NVARCHAR (50) NULL,
    [USUnits]        NVARCHAR (20) NULL,
    [MetUnits]       NVARCHAR (20) NULL,
    [USDecPnt]       TINYINT       NULL,
    [MetDecPnt]      TINYINT       NULL,
    [SortKey]        INT           NULL,
    [FldType]        NVARCHAR (10) NULL,
    [BegEffDate]     SMALLDATETIME NULL,
    [EndEffDate]     SMALLDATETIME NULL,
    CONSTRAINT [PK_ProductProperties_LU] PRIMARY KEY CLUSTERED ([PropertyId] ASC)
);

