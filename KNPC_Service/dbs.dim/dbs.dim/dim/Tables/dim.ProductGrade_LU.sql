﻿CREATE TABLE [dim].[ProductGrade_LU] (
    [Grade]       VARCHAR (8)  NOT NULL,
    [ProductType] VARCHAR (24) NOT NULL,
    [Type]        VARCHAR (8)  NULL,
    [Description] VARCHAR (64) NOT NULL,
    [MinDensity]  REAL         NOT NULL,
    [MaxDensity]  REAL         NOT NULL,
    CONSTRAINT [CL_ProductGrade_LU_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_ProductGrade_LU_Grade] CHECK ([Grade]<>''),
    CONSTRAINT [CL_ProductGrade_LU_ProductType] CHECK ([ProductType]<>''),
    CONSTRAINT [CL_ProductGrade_LU_Type] CHECK ([Type]<>''),
    CONSTRAINT [UK_ProductGrade_LU] UNIQUE CLUSTERED ([Grade] ASC, [ProductType] ASC, [Type] ASC)
);

