﻿CREATE TABLE [dim].[OpEx_LU] (
    [Property]    VARCHAR (32)  NOT NULL,
    [Description] VARCHAR (256) NOT NULL,
    [SortOrder]   INT           NOT NULL,
    CONSTRAINT [PK_OpEx_Lu] PRIMARY KEY CLUSTERED ([Property] ASC),
    CONSTRAINT [CL_OpEx_Lu_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_OpEx_Lu_Property] CHECK ([Property]<>''),
    CONSTRAINT [UX_OpEx_Lu_Description] UNIQUE NONCLUSTERED ([Description] ASC)
);

