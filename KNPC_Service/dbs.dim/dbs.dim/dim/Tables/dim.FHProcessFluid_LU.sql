﻿CREATE TABLE [dim].[FHProcessFluid_LU] (
    [ProcessFluid] VARCHAR (8)  NOT NULL,
    [Description]  VARCHAR (64) NOT NULL,
    CONSTRAINT [PK_FHProcessFluid_LU] PRIMARY KEY CLUSTERED ([ProcessFluid] ASC),
    CONSTRAINT [CL_FHProcessFluid_LU_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_FHProcessFluid_LU_Service] CHECK ([ProcessFluid]<>''),
    CONSTRAINT [UX_FHProcessFluid_LU_Description] UNIQUE NONCLUSTERED ([Description] ASC)
);

