﻿CREATE VIEW [dim].[Process_LU]
AS
SELECT pid.ProcessID, ProcessDescription = pid.Description, pt.ProcessType, ProcessTypeDescription = pt.Description, SortKey = CAST(pid.SortKey as int), pid.CapBasis, pid.DBUnits, pid.MetricCapUnits
FROM dbo.ProcessID_LU pid LEFT JOIN dbo.ProcessType_LU pt ON pt.ProcessID = pid.ProcessID
WHERE ISNULL(pid.Hidden,'N') = 'N'