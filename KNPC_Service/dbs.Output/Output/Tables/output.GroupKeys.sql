﻿CREATE TABLE [output].[GroupKeys]
(
	[GroupKey]				VARCHAR(40)				NOT	NULL,	CONSTRAINT [CL_GroupKeys_GroupKey]			CHECK([GroupKey] <> ''),

	[StudyYear]				INT						NOT	NULL,
	[GroupType]				VARCHAR(40)				NOT	NULL,	CONSTRAINT [CL_GroupKeys_GroupType]			CHECK([GroupType] <> ''),
	[GroupName]				VARCHAR(256)			NOT	NULL,	CONSTRAINT [CL_GroupKeys_GroupName]			CHECK([GroupType] <> ''),
	[Tile]					INT							NULL,	CONSTRAINT [CR_GroupKeys_Tile_Lower]		CHECK([Tile] >= 1),
																CONSTRAINT [CR_GroupKeys_Tile_Upper]		CHECK([Tile] <= 4),
	[MaxTiles]				INT							NULL,	CONSTRAINT [CR_GroupKeys_MaxTiles_Lower]	CHECK([Tile] >= 1),
																CONSTRAINT [CR_GroupKeys_MAxTiles_Upper]	CHECK([Tile] <= 4),

	CONSTRAINT [PK_GroupKeys]	PRIMARY KEY CLUSTERED([GroupKey] ASC)
);