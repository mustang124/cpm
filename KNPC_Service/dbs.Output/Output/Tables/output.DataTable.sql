﻿CREATE TABLE [output].[DataTable]
(
	[GroupKey]			VARCHAR(20)		NOT	NULL	CHECK([GroupKey] <> ''),
	[DataKey]			VARCHAR(60)		NOT	NULL	CHECK([DataKey] <> ''),
	[Methodology]		SMALLINT		NOT	NULL	CHECK([Methodology] > 0),
	[NumberValue]		FLOAT				NULL,
	[TextValue]			VARCHAR(256)		NULL,
	[CalcTime]			DATETIME		NOT	NULL,

	PRIMARY KEY CLUSTERED ([DataKey] ASC, [GroupKey] ASC, [Methodology] DESC)
);

