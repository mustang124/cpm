﻿CREATE TABLE [stage].[ConfigRS] (
    [ConfigRSID]   INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [UnitID]       INT           NULL,
    [AvgSize]      REAL          NULL,
    [Throughput]   REAL          NULL,
    [ProcessID]    NVARCHAR (50) NULL,
    [ProcessType]  NVARCHAR (20) NULL,
    CONSTRAINT [PK_ConfigRS] PRIMARY KEY CLUSTERED ([ConfigRSID] ASC),
    CONSTRAINT [FK_ConfigRS_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_ConfigRS] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [UnitID] ASC)
);

