﻿CREATE TABLE [stage].[Resid] (
    [ResidID]      INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [BlendID]      INT           NULL,
    [Density]      REAL          NULL,
    [PourPt]       REAL          NULL,
    [Sulfur]       REAL          NULL,
    [KMT]          REAL          NULL,
    [ViscCSAtTemp] REAL          NULL,
    [ViscTemp]     REAL          NULL,
    [Grade]        NVARCHAR (50) NULL,
    CONSTRAINT [PK_Resid] PRIMARY KEY CLUSTERED ([ResidID] ASC),
    CONSTRAINT [FK_Resid_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Resid] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [BlendID] ASC)
);

