﻿CREATE TABLE [stage].[GeneralMisc] (
    [GeneralMiscID] INT  IDENTITY (1, 1) NOT NULL,
    [SubmissionID]  INT  NOT NULL,
    [FlareLossMT]   REAL NULL,
    [TotLossMT]     REAL NULL,
    CONSTRAINT [PK_GeneralMisc] PRIMARY KEY CLUSTERED ([GeneralMiscID] ASC),
    CONSTRAINT [FK_GeneralMisc_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_GeneralMisc] UNIQUE NONCLUSTERED ([SubmissionID] ASC)
);

