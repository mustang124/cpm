﻿CREATE TABLE [stage].[ValidationResult] (
    [ValidationResultId] INT            IDENTITY (1, 1) NOT NULL,
    [SubmissionId]       INT            NOT NULL,
    [ValidationRule]     NVARCHAR (MAX) NOT NULL,
    [AcknowledgeStatus]  BIT            NOT NULL,
    [CommentId]          INT            NULL,
    CONSTRAINT [PK_ValidationResult] PRIMARY KEY CLUSTERED ([ValidationResultId] ASC)
);

