﻿CREATE TABLE [stage].[ProcessData] (
    [ProcessDataID] INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID]  INT           NOT NULL,
    [UnitID]        INT           NULL,
    [Property]      NVARCHAR (50) NULL,
    [RptDVal]       SMALLDATETIME NULL,
    [RptNVal]       REAL          NULL,
    [RptTVal]       NVARCHAR (50) NULL,
    [UOM]           NVARCHAR (20) NULL,
    CONSTRAINT [PK_ProcessData] PRIMARY KEY CLUSTERED ([ProcessDataID] ASC),
    CONSTRAINT [FK_ProcessData_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_ProcessData] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [UnitID] ASC, [Property] ASC)
);

