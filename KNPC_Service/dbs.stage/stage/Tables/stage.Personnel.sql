﻿CREATE TABLE [stage].[Personnel] (
    [PersonnelID]  INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [PersID]       NVARCHAR (20) NULL,
    [NumPers]      REAL          NULL,
    [STH]          REAL          NULL,
    [OVTHours]     REAL          NULL,
    [OVTPcnt]      REAL          NULL,
    [Contract]     REAL          NULL,
    [GA]           REAL          NULL,
    [AbsHrs]       REAL          NULL,
    CONSTRAINT [PK_Personnel] PRIMARY KEY CLUSTERED ([PersonnelID] ASC),
    CONSTRAINT [FK_Personnel_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Personnel] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [PersID] ASC)
);

