﻿CREATE TABLE [stage].[Inventory] (
    [InventoryID]  INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [NumTank]      INT           NULL,
    [AvgLevel]     REAL          NULL,
    [LeasedPcnt]   REAL          NULL,
    [MandStorage]  REAL          NULL,
    [RefStorage]   REAL          NULL,
    [TankType]     NVARCHAR (50) NULL,
    [TotStorage]   REAL          NULL,
    [FuelStorage]  REAL          NULL,
    CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED ([InventoryID] ASC),
    CONSTRAINT [FK_Inventory_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Inventory] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [TankType] ASC)
);

