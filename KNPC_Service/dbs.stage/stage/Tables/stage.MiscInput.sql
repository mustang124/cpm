﻿CREATE TABLE [stage].[MiscInput] (
    [MiscInputID]       INT  IDENTITY (1, 1) NOT NULL,
    [SubmissionID]      INT  NOT NULL,
    [CDUChargeBbl]      REAL NULL,
    [CDUChargeMT]       REAL NULL,
    [OffSiteEnergyPcnt] REAL NULL,
    CONSTRAINT [PK_MiscInput] PRIMARY KEY CLUSTERED ([MiscInputID] ASC),
    CONSTRAINT [FK_MiscInput_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_MiscInput] UNIQUE NONCLUSTERED ([SubmissionID] ASC)
);

