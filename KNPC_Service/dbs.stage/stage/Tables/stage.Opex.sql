﻿CREATE TABLE [stage].[OpEx] (
    [OpexID]         INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID]   INT           NOT NULL,
    [OthDescription] NVARCHAR (50) NULL,
    [Property]       NVARCHAR (50) NULL,
    [RptValue]       REAL          NULL,
    CONSTRAINT [PK_OpEx] PRIMARY KEY CLUSTERED ([OpexID] ASC),
    CONSTRAINT [FK_OpEx_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_OpEx] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [Property] ASC, [OthDescription] ASC)
);

