﻿CREATE TABLE [stage].[FinProdProps] (
    [SubmissionId] INT           NOT NULL,
    [MaterialId]   VARCHAR (5)   NOT NULL,
    [BlendId]      SMALLINT      NOT NULL,
    [PropertyId]   INT           NOT NULL,
    [RptNVal]      REAL          NULL,
    [RptTVal]      VARCHAR (20)  NULL,
    [RptDVal]      SMALLDATETIME NULL,
    CONSTRAINT [PK_FinProdProps] PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [MaterialId] ASC, [BlendId] ASC, [PropertyId] ASC)
);

