﻿CREATE TABLE [stage].[Absence] (
    [AbsenceID]    INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [Category]     NVARCHAR (50) NULL,
    [MPSAbs]       REAL          NULL,
    [OCCAbs]       REAL          NULL,
    CONSTRAINT [PK_Absence] PRIMARY KEY CLUSTERED ([AbsenceID] ASC),
    CONSTRAINT [FK_Absence_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Absence] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [Category] ASC)
);

