﻿CREATE TABLE [stage].[Comment] (
    [CommentId]       INT            IDENTITY (1, 1) NOT NULL,
    [CellReferenceID] INT            NOT NULL,
    [SubmissionID]    INT            NOT NULL,
    [Note]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED ([CommentId] ASC),
	CONSTRAINT [FK_Comment_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID])
);

