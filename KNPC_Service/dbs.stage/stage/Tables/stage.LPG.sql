﻿CREATE TABLE [stage].[LPG] (
    [LPGID]        INT      IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT      NOT NULL,
    [BlendID]      INT      NULL,
    [MolOrVol]     CHAR (1) NULL,
    [VolC2Lt]      REAL     NULL,
    [VolC3]        REAL     NULL,
    [VolC3ene]     REAL     NULL,
    [VoliC4]       REAL     NULL,
    [VolnC4]       REAL     NULL,
    [VolC4ene]     REAL     NULL,
    [VolC5Plus]    REAL     NULL,
    CONSTRAINT [PK_LPG] PRIMARY KEY CLUSTERED ([LPGID] ASC),
    CONSTRAINT [FK_LPG_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_LPG] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [BlendID] ASC)
);

