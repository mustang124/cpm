﻿CREATE TABLE [stage].[MarineBunker] (
    [MarineBunkerID] INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID]   INT           NOT NULL,
    [BlendID]        INT           NULL,
    [CrackedStock]   REAL          NULL,
    [Density]        REAL          NULL,
    [PourPt]         REAL          NULL,
    [Sulfur]         REAL          NULL,
    [KMT]            REAL          NULL,
    [ViscCSAtTemp]   REAL          NULL,
    [ViscTemp]       REAL          NULL,
    [Grade]          NVARCHAR (50) NULL,
    CONSTRAINT [PK_MarineBunker] PRIMARY KEY CLUSTERED ([MarineBunkerID] ASC),
    CONSTRAINT [FK_MarineBunker_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_MarineBunker] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [BlendID] ASC)
);

