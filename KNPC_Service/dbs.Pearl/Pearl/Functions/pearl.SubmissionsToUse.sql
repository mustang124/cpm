﻿CREATE FUNCTION [pearl].[SubmissionsToUse]
(
	@ClientSubmissionId int
)
RETURNS TABLE AS
RETURN
(SELECT TOP 1
	[ckc].[ClientKeysID],
	[ckc].[Refnumber],
	[ckc].[ClientKey],
	[ckc].[EffectiveAfter],
	[rsm].[SubmissionId],
	[rsm].[ClientSubmissionId], --	[sub].[ClientSubmissionID],
	[rsm].[DboSubmissionId] as DboSubmissionId
FROM
	[pearl].[ClientKeys_Current]		[ckc]
LEFT OUTER JOIN
	[pearl].[RefnumberSubmissionMap]	[rsm]
		ON	[rsm].[Refnumber]		= [ckc].[Refnumber]
LEFT OUTER JOIN
	[stage].[Submissions]				[sub]
		ON	[sub].[SubmissionID]	= [rsm].[SubmissionId]
LEFT OUTER JOIN
	[dbo].[Submissions]					[dbosub]
		on [dbosub].[SubmissionID] = [rsm].[DboSubmissionId]
WHERE [rsm].[ClientSubmissionId] = @ClientSubmissionId
ORDER BY [rsm].[DboSubmissionId] desc)
GO
