﻿CREATE TABLE [pearl].[RefnumberSubmissionMap]
(
	[ClientKeysMapID]		INT					NOT	NULL	IDENTITY(1, 1),
	[Refnumber]				VARCHAR(32)			NOT	NULL,	CONSTRAINT [CL_ClientKeysMap_Refnumber]		CHECK([Refnumber]	<> ''),
	[StgSubmissionId]		INT					NOT	NULL	CONSTRAINT [FK_ClientKeysMap_Submissions]	REFERENCES [stage].[Submissions]([SubmissionID]),
	[ClientSubmissionId]	INT					NOT NULL,
	[DboSubmissionId]		INT					NOT NULL , --yes, I wanted to add this but would cause too much disruption right now: CONSTRAINT [FK_ClientKeysMap_DboSubmissions]	REFERENCES [dbo].[SubmissionsAll]([SubmissionID]),
	[Failed]				CHAR(1)				DEFAULT 'N'
	CONSTRAINT [PK_ClientKeysMap]	PRIMARY KEY NONCLUSTERED([ClientKeysMapID] ASC),
	CONSTRAINT [UK_ClientKeysMap]	UNIQUE CLUSTERED([Refnumber] ASC, [StgSubmissionId] ASC)
);