﻿CREATE TABLE [pearl].[ClientKeys]
(
	[ClientKeysID]			INT					NOT	NULL	IDENTITY(1, 1),

	[Refnumber]				VARCHAR(32)			NOT	NULL,	CONSTRAINT [CL_ClientKeys_Refnumber]		CHECK([Refnumber]	<> ''),
	[ClientKey]				VARCHAR(256)		NOT	NULL,	CONSTRAINT [CL_ClientKeys_ClientKey]		CHECK([ClientKey]	<>''),
	[EffectiveAfter]		DATETIME2(7)		NOT	NULL	CONSTRAINT [DF_ClientKeys_EffectiveAfter]	DEFAULT(SYSDATETIMEOFFSET()),
	[IsValid]				TINYINT				NOT	NULL	CONSTRAINT [DF_ClientKeys_IsValid]			DEFAULT(1),		

	[tsInserted]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ClientKeys_tsInserted]		DEFAULT(SYSDATETIMEOFFSET()),
	[tsInsertedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ClientKeys_tsInsertedHost]	DEFAULT(HOST_NAME()),
	[tsInsertedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ClientKeys_tsInsertedUser]	DEFAULT(SUSER_SNAME()),
	[tsInsertedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ClientKeys_tsInsertedApp]	DEFAULT(APP_NAME()),

	CONSTRAINT [PK_ClientKeys]		PRIMARY KEY NONCLUSTERED([ClientKeysID] ASC),
	CONSTRAINT [UK_ClientKeys]		UNIQUE CLUSTERED([Refnumber] ASC, [EffectiveAfter] DESC),
	CONSTRAINT [UX_ClientKeys]		UNIQUE NONCLUSTERED([ClientKey] ASC)
);