﻿CREATE VIEW [pearl].[ClientKeys_CurrentSubmissions]
WITH SCHEMABINDING
AS
SELECT top 100000 
	[ckc].[Refnumber],
	[ckc].[ClientKey],
	[ckc].[EffectiveAfter],
	[rsm].[StgSubmissionId],
	[rsm].[ClientSubmissionId], 
	[rsm].[DboSubmissionId] 
FROM
	[pearl].[ClientKeys_Current]		[ckc]
LEFT OUTER JOIN
	[pearl].[RefnumberSubmissionMap]	[rsm]
		ON	[rsm].[Refnumber]		= [ckc].[Refnumber]
LEFT OUTER JOIN
	[stage].[Submissions]				[sub]
		ON	[sub].[SubmissionID]	= [rsm].[StgSubmissionId]
WHERE [rsm].[Failed]<>'Y'
ORDER BY [rsm].[StgSubmissionId] desc

