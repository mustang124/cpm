﻿CREATE PROCEDURE [pearl].[Insert_ClientKeysMap]
(
	@SubmissionId		INT,
	@ClientKey			VARCHAR(256)
)
AS
BEGIN

	INSERT INTO [pearl].[RefnumberSubmissionMap]
	(
		[SubmissionId],
		[Refnumber]
	)
	SELECT
		@SubmissionId,
		[c].[Refnumber]
	FROM
		[pearl].[ClientKeys_Current]	[c]
	WHERE
		[c].[ClientKey]	= @ClientKey;

END;