<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProfileDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProfileDialog))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblMessage = New System.Windows.Forms.Label
        Me.pbImage = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.pnlYNC = New System.Windows.Forms.Panel
        Me.Button3 = New System.Windows.Forms.Button
        Me.ilImages = New System.Windows.Forms.ImageList(Me.components)
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.pnlYN = New System.Windows.Forms.Panel
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.pnlIC = New System.Windows.Forms.Panel
        Me.Button6 = New System.Windows.Forms.Button
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel1.SuspendLayout()
        CType(Me.pbImage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlYNC.SuspendLayout()
        Me.pnlYN.SuspendLayout()
        Me.pnlIC.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblMessage)
        Me.Panel1.Controls.Add(Me.pbImage)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.pnlYNC)
        Me.Panel1.Controls.Add(Me.pnlYN)
        Me.Panel1.Controls.Add(Me.pnlIC)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(2, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(6, 3, 6, 6)
        Me.Panel1.Size = New System.Drawing.Size(322, 130)
        Me.Panel1.TabIndex = 0
        '
        'lblMessage
        '
        Me.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblMessage.Location = New System.Drawing.Point(54, 26)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.lblMessage.Size = New System.Drawing.Size(260, 9)
        Me.lblMessage.TabIndex = 36
        Me.lblMessage.Text = "The action you have chosen may take awhile. Would you like to continue?"
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pbImage
        '
        Me.pbImage.BackColor = System.Drawing.Color.Transparent
        Me.pbImage.Dock = System.Windows.Forms.DockStyle.Left
        Me.pbImage.Location = New System.Drawing.Point(6, 26)
        Me.pbImage.Name = "pbImage"
        Me.pbImage.Size = New System.Drawing.Size(48, 9)
        Me.pbImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbImage.TabIndex = 34
        Me.pbImage.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(308, 23)
        Me.PictureBox1.TabIndex = 33
        Me.PictureBox1.TabStop = False
        '
        'pnlYNC
        '
        Me.pnlYNC.Controls.Add(Me.Button3)
        Me.pnlYNC.Controls.Add(Me.Button2)
        Me.pnlYNC.Controls.Add(Me.Button1)
        Me.pnlYNC.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlYNC.Location = New System.Drawing.Point(6, 35)
        Me.pnlYNC.Name = "pnlYNC"
        Me.pnlYNC.Size = New System.Drawing.Size(308, 29)
        Me.pnlYNC.TabIndex = 35
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button3.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.ImageList = Me.ilImages
        Me.Button3.Location = New System.Drawing.Point(195, 0)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(63, 29)
        Me.Button3.TabIndex = 12
        Me.Button3.TabStop = False
        Me.Button3.Text = "Cancel"
        Me.Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button3.UseVisualStyleBackColor = False
        '
        'ilImages
        '
        Me.ilImages.ImageStream = CType(resources.GetObject("ilImages.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilImages.TransparentColor = System.Drawing.Color.Transparent
        Me.ilImages.Images.SetKeyName(0, "information.png")
        Me.ilImages.Images.SetKeyName(1, "Question.bmp")
        Me.ilImages.Images.SetKeyName(2, "Stop.bmp")
        Me.ilImages.Images.SetKeyName(3, "error-1.png")
        Me.ilImages.Images.SetKeyName(4, "help.png")
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button2.DialogResult = System.Windows.Forms.DialogResult.No
        Me.Button2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.ImageList = Me.ilImages
        Me.Button2.Location = New System.Drawing.Point(123, 0)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(63, 29)
        Me.Button2.TabIndex = 11
        Me.Button2.TabStop = False
        Me.Button2.Text = "No"
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.ImageList = Me.ilImages
        Me.Button1.Location = New System.Drawing.Point(54, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(63, 29)
        Me.Button1.TabIndex = 10
        Me.Button1.TabStop = False
        Me.Button1.Text = "Yes"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button1.UseVisualStyleBackColor = False
        '
        'pnlYN
        '
        Me.pnlYN.Controls.Add(Me.Button5)
        Me.pnlYN.Controls.Add(Me.Button4)
        Me.pnlYN.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlYN.Location = New System.Drawing.Point(6, 64)
        Me.pnlYN.Name = "pnlYN"
        Me.pnlYN.Size = New System.Drawing.Size(308, 29)
        Me.pnlYN.TabIndex = 37
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button5.DialogResult = System.Windows.Forms.DialogResult.No
        Me.Button5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.ImageList = Me.ilImages
        Me.Button5.Location = New System.Drawing.Point(159, 0)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(63, 29)
        Me.Button5.TabIndex = 11
        Me.Button5.TabStop = False
        Me.Button5.Text = "No"
        Me.Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button4.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.Button4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.ImageList = Me.ilImages
        Me.Button4.Location = New System.Drawing.Point(87, 0)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(63, 29)
        Me.Button4.TabIndex = 10
        Me.Button4.TabStop = False
        Me.Button4.Text = "Yes"
        Me.Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button4.UseVisualStyleBackColor = False
        '
        'pnlIC
        '
        Me.pnlIC.Controls.Add(Me.Button6)
        Me.pnlIC.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlIC.Location = New System.Drawing.Point(6, 93)
        Me.pnlIC.Name = "pnlIC"
        Me.pnlIC.Size = New System.Drawing.Size(308, 29)
        Me.pnlIC.TabIndex = 38
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button6.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Button6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.ImageList = Me.ilImages
        Me.Button6.Location = New System.Drawing.Point(123, 0)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(63, 29)
        Me.Button6.TabIndex = 11
        Me.Button6.TabStop = False
        Me.Button6.Text = "OK"
        Me.Button6.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(2)
        Me.Panel2.Size = New System.Drawing.Size(326, 134)
        Me.Panel2.TabIndex = 1
        '
        'ProfileDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(334, 142)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "ProfileDialog"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        CType(Me.pbImage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlYNC.ResumeLayout(False)
        Me.pnlYN.ResumeLayout(False)
        Me.pnlIC.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents pbImage As System.Windows.Forms.PictureBox
    Friend WithEvents pnlYNC As System.Windows.Forms.Panel
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ilImages As System.Windows.Forms.ImageList
    Friend WithEvents pnlYN As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents pnlIC As System.Windows.Forms.Panel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
End Class
