Option Explicit On

Imports System.IO
Imports System.Xml
Imports System.Security.Principal
Imports System.Security.Cryptography
Imports System.Text

Friend Module Helper

    Friend Function ValidLogin(ByVal UserId As String, ByVal Password As String) As Boolean
        Return ValidAppUser(UserId.Trim, Password.Trim)
    End Function

    Friend Function ValidUser(ByVal UserId As String, ByVal adminUser As String) As Boolean
        If UserId.Trim() = adminUser Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function ValidAppUser(ByVal userID As String, ByVal password As String) As Boolean
        Dim users As DsUsers = New DsUsers
        Try
            If userID.Trim.ToLower = "solomon" And password.Trim.ToLower = "profile" Then
                If Not File.Exists(pathAdmin & "usersXP.xml") Then
                    Return True
                Else
                    Try
                        ReadEncrpytedXML("usersXP.xml", users)
                        If users.Tables(0).Rows.Count = 0 Then 'No users in the system check for default password                       
                            Return True
                        ElseIf Not HasAdminUsers() Then
                            Stop
                            'Return true if there are not any administrator groups
                            Return True
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                End If


            End If

            ' 20081001RRH Path - If File.Exists(frmMain.AppPath & "\_ADMIN\usersXP.xml") Then
            If File.Exists(pathAdmin & "usersXP.xml") Then
                ReadEncrpytedXML("usersXP.xml", users)
                Dim rows() As DataRow = users.Users.Select("LoginId='" & userID.Trim() & "' and Password='" & password.Trim() & "'")
                If rows.Length > 0 Then Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Console.Write(ex)
        End Try

        Return False

    End Function

    Friend Function IsAdministrator(ByVal user As String) As Boolean

        Dim tablePerms As DsTablePerm = New DsTablePerm
        Dim usersToGroups As DsUserToGroup = New DsUserToGroup
        Dim AppName As String = "PROFILE_II"
        Dim users As DsUsers = New DsUsers

        'Special Case default login 
        If user.Trim.ToLower = "solomon" Then
            ' 20081001 RRH Path - If Not File.Exists(frmMain.AppPath & "\_ADMIN\usersXP.xml") Then
            If Not File.Exists(pathAdmin & "usersXP.xml") Then
                Return True
            Else
                'Return true if there is not any admins 
                ReadEncrpytedXML("usersXP.xml", users)
                If users.Tables(0).Rows.Count = 0 Then
                    Return True
                ElseIf Not HasAdminUsers() Then
                    Return True
                Else
                    Return False
                End If
            End If
        End If

        Try
            ReadEncrpytedXML("usersToGroupsXP.xml", usersToGroups)
            ReadEncrpytedXML("tablePermsXP.xml", tablePerms)

            'Find Adminstrator groups
            Dim perms() As DataRow = tablePerms.TablePerm.Select("TableName = 'Administrator'")
            Dim p As Integer
            For p = 0 To perms.Length - 1
                Dim userRows() As DataRow = usersToGroups.UserToGroup.Select("GroupID=" & CStr(perms(p)("GroupID")) & " and LoginID='" & user.Trim & "'")
                If userRows.Length > 0 Then
                    Return True
                End If
            Next
        Catch ex As Exception
            Console.Write(ex.ToString)
        End Try

        Return False

    End Function

    Friend Function GetYearLimit(ByVal user As String) As Integer
        Dim groups As dsGroups = New dsGroups
        Dim usersToGroups As DsUserToGroup = New DsUserToGroup
        Dim a, i, lowestYear As Integer

        ' 20081001 RRH Path - If Not File.Exists(frmMain.AppPath & "\_ADMIN\usersXP.xml") Then
        If Not File.Exists(pathAdmin & "usersXP.xml") Then
            Return lowestYear
            'If user.Trim.ToLower = "solomon" Then

            'Else
            '    Return lowestYear
            'End If
        End If

        Try
            ReadEncrpytedXML("usersToGroupsXP.xml", usersToGroups)
            ReadEncrpytedXML("groupsXP.xml", groups)


            Dim userRows() As DataRow = usersToGroups.UserToGroup.Select("LoginID='" & user.Trim & "'")
            For a = 0 To userRows.Length - 1
                Dim grpRows() As DataRow = groups.Groups.Select("GroupID=" & CStr(userRows(a)("GroupID")))

                For i = 0 To grpRows.Length - 1
                    If Not IsDBNull(grpRows(i)("YearLimit")) And Not IsNothing(grpRows(i)("YearLimit")) Then
                        If (lowestYear > CInt(grpRows(i)("YearLimit"))) Or (lowestYear = 0) Then
                            lowestYear = CInt(grpRows(i)("YearLimit"))
                        End If
                    End If
                Next
            Next

        Catch ex As Exception
            Console.Write(ex)
            Return lowestYear
        End Try
        Return lowestYear
    End Function

    Friend Function GetPermissions(ByVal user As String) As ArrayList
        Dim tablePerms As DsTablePerm = New DsTablePerm
        Dim usersToGroups As DsUserToGroup = New DsUserToGroup
        Dim a, i As Integer
        Dim permissions As ArrayList = New ArrayList

        If user.Trim.ToLower = "solomon" Then
            '20081001 RRH Path - If Not File.Exists(frmMain.AppPath & "\_ADMIN\usersXP.xml") Then
            If Not File.Exists(pathAdmin & "usersXP.xml") Then
                ' 20081212 RRH permissions.Add("Administrator")
                permissions.Add(strRightsAdmin)
                Return permissions
            Else
                Dim users As DsUsers = New DsUsers
                ReadEncrpytedXML("usersXP.xml", users)
                If users.Tables(0).Rows.Count = 0 Then
                    ' 20081212 RRH permissions.Add("Administrator")
                    permissions.Add(strRightsAdmin)
                    Return permissions
                ElseIf Not HasAdminUsers() Then
                    'Return true if there is not any admins 
                    ' 20081212 RRH permissions.Add("Administrator")
                    permissions.Add(strRightsAdmin)
                    Return permissions
                Else
                    Return permissions
                End If
            End If
        End If


        Try
            ReadEncrpytedXML("usersToGroupsXP.xml", usersToGroups)
            ReadEncrpytedXML("tablePermsXP.xml", tablePerms)

            Dim userRows() As DataRow = usersToGroups.UserToGroup.Select("LoginID='" & user.Trim & "'")
            For a = 0 To userRows.Length - 1
                Dim permRows() As DataRow = tablePerms.TablePerm.Select("GroupID=" & CStr(userRows(a)("GroupID")))

                For i = 0 To permRows.Length - 1
                    If Not permissions.Contains(permRows(i)("TableName")) Then
                        permissions.Add(permRows(i)("TableName"))
                    End If
                Next
            Next

        Catch ex As Exception
            Console.Write(ex)
            Return permissions
        End Try

        Return permissions
    End Function


    Friend Sub ChangePassword(ByVal user As String, ByVal password As String, ByVal newpassword As String)

        If ValidAppUser(user, password) Then
            Dim users As DataSet = New DataSet
            ReadEncrpytedXML("usersXP.xml", users)
            Dim rows() As DataRow = users.Tables(0).Select("LoginID='" & user.Trim & "'")
            If rows.Length > 0 Then
                rows(0)("Password") = newpassword.Trim
                WriteEncrpytedXML("usersXP.xml", users)
            End If
        Else
            Dim ex As Exception = New Exception("User was not found.")
            Throw ex
        End If

    End Sub

    Sub ReadEncrpytedXML(ByVal file As String, ByVal ds As DataSet)
        'Encrypt the data set and write it to file. 
        Dim aFileStream As FileStream = Nothing

        While (True)
            Try
                ' 20081001 RRH Path - aFileStream = New FileStream(frmMain.AppPath & "\_ADMIN\" & file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                aFileStream = New FileStream(pathAdmin & file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Exit While
            Catch ex As Exception
                'keep looping until free
                If ex.GetType() Is Type.GetType("IOException") Then
                    Throw ex
                End If
            End Try
        End While

        'Dim aStreamReader As New StreamReader(aFileStream)
        Dim aUE As New UnicodeEncoding
        Dim key() As Byte = aUE.GetBytes("password")
        Dim RMCrypto As RijndaelManaged = New RijndaelManaged
        Dim aCryptoStream As New CryptoStream(aFileStream, _
        RMCrypto.CreateDecryptor(key, key), CryptoStreamMode.Read)

        ds.ReadXml(aCryptoStream, XmlReadMode.ReadSchema)

        aCryptoStream.Close()
        aFileStream.Close()
        'aStreamReader.Close()

    End Sub

    Sub WriteEncrpytedXML(ByVal file As String, ByVal ds As DataSet)
        'Encrypt the data set and write it to file. 
        Dim aXmlTextWriter As System.Xml.XmlTextWriter
        ' 20081001 RRH Path - aXmlTextWriter = New XmlTextWriter(frmMain.AppPath & "\_ADMIN\" & file, Encoding.UTF8)
        aXmlTextWriter = New XmlTextWriter(pathAdmin & file, Encoding.UTF8)

        Dim aUE As New UnicodeEncoding
        Dim key() As Byte = aUE.GetBytes("password")
        Dim RMCrypto As RijndaelManaged = New RijndaelManaged
        Dim aCryptoStream As New CryptoStream(aXmlTextWriter.BaseStream, _
        RMCrypto.CreateEncryptor(key, key), CryptoStreamMode.Write)

        ds.WriteXml(aCryptoStream, XmlWriteMode.WriteSchema)

        aCryptoStream.Close()

    End Sub

    Friend Function EncryptText(ByVal vstrTextToBeEncrypted As String, _
                                         ByVal vstrEncryptionKey As String) As String

        Dim bytValue() As Byte
        Dim bytKey() As Byte
        Dim bytEncoded() As Byte = Nothing
        Dim bytIV() As Byte = {121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62}
        Dim intLength As Integer
        Dim intRemaining As Integer
        Dim objMemoryStream As New MemoryStream
        Dim objCryptoStream As CryptoStream
        Dim objRijndaelManaged As RijndaelManaged


        '   **********************************************************************
        '   ******  Strip any null character from string to be encrypted    ******
        '   **********************************************************************

        vstrTextToBeEncrypted = StripNullCharacters(vstrTextToBeEncrypted)

        '   **********************************************************************
        '   ******  Value must be within ASCII range (i.e., no DBCS chars)  ******
        '   **********************************************************************

        bytValue = Encoding.ASCII.GetBytes(vstrTextToBeEncrypted.ToCharArray)

        intLength = Len(vstrEncryptionKey)

        '   ********************************************************************
        '   ******   Encryption Key must be 256 bits long (32 bytes)      ******
        '   ******   If it is longer than 32 bytes it will be truncated.  ******
        '   ******   If it is shorter than 32 bytes it will be padded     ******
        '   ******   with upper-case Xs.                                  ****** 
        '   ********************************************************************

        If intLength >= 32 Then
            vstrEncryptionKey = Strings.Left(vstrEncryptionKey, 32)
        Else
            intLength = Len(vstrEncryptionKey)
            intRemaining = 32 - intLength
            vstrEncryptionKey = vstrEncryptionKey & Strings.StrDup(intRemaining, "X")
        End If

        bytKey = Encoding.ASCII.GetBytes(vstrEncryptionKey.ToCharArray)

        objRijndaelManaged = New RijndaelManaged

        '   ***********************************************************************
        '   ******  Create the encryptor and write value to it after it is   ******
        '   ******  converted into a byte array                              ******
        '   ***********************************************************************

        Try

            objCryptoStream = New CryptoStream(objMemoryStream, _
              objRijndaelManaged.CreateEncryptor(bytKey, bytIV), _
              CryptoStreamMode.Write)
            objCryptoStream.Write(bytValue, 0, bytValue.Length)

            objCryptoStream.FlushFinalBlock()

            bytEncoded = objMemoryStream.ToArray
            objMemoryStream.Close()
            objCryptoStream.Close()
        Catch



        End Try

        '   ***********************************************************************
        '   ******   Return encryptes value (converted from  byte Array to   ******
        '   ******   a base64 string).  Base64 is MIME encoding)             ******
        '   ***********************************************************************

        Return Convert.ToBase64String(bytEncoded)

    End Function

    Friend Function DecryptText(ByVal vstrStringToBeDecrypted As String, _
                                        ByVal vstrDecryptionKey As String) As String

        Dim bytDataToBeDecrypted() As Byte
        Dim bytTemp() As Byte
        Dim bytIV() As Byte = {121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62}
        Dim objRijndaelManaged As New RijndaelManaged
        Dim objMemoryStream As MemoryStream
        Dim objCryptoStream As CryptoStream
        Dim bytDecryptionKey() As Byte

        Dim intLength As Integer
        Dim intRemaining As Integer
        Dim strReturnString As String = String.Empty

        '   *****************************************************************
        '   ******   Convert base64 encrypted value to byte array      ******
        '   *****************************************************************

        bytDataToBeDecrypted = Convert.FromBase64String(vstrStringToBeDecrypted)

        '   ********************************************************************
        '   ******   Encryption Key must be 256 bits long (32 bytes)      ******
        '   ******   If it is longer than 32 bytes it will be truncated.  ******
        '   ******   If it is shorter than 32 bytes it will be padded     ******
        '   ******   with upper-case Xs.                                  ****** 
        '   ********************************************************************

        intLength = Len(vstrDecryptionKey)

        If intLength >= 32 Then
            vstrDecryptionKey = Strings.Left(vstrDecryptionKey, 32)
        Else
            intLength = Len(vstrDecryptionKey)
            intRemaining = 32 - intLength
            vstrDecryptionKey = vstrDecryptionKey & Strings.StrDup(intRemaining, "X")
        End If

        bytDecryptionKey = Encoding.ASCII.GetBytes(vstrDecryptionKey.ToCharArray)

        ReDim bytTemp(bytDataToBeDecrypted.Length)

        objMemoryStream = New MemoryStream(bytDataToBeDecrypted)

        '   ***********************************************************************
        '   ******  Create the decryptor and write value to it after it is   ******
        '   ******  converted into a byte array                              ******
        '   ***********************************************************************

        Try

            objCryptoStream = New CryptoStream(objMemoryStream, _
               objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV), _
               CryptoStreamMode.Read)

            objCryptoStream.Read(bytTemp, 0, bytTemp.Length)

            objCryptoStream.FlushFinalBlock()
            objMemoryStream.Close()
            objCryptoStream.Close()

        Catch

        End Try

        '   *****************************************
        '   ******   Return decypted value     ******
        '   *****************************************

        Return StripNullCharacters(Encoding.ASCII.GetString(bytTemp))

    End Function

    Friend Function StripNullCharacters(ByVal vstrStringWithNulls As String) As String

        Dim intPosition As Integer
        Dim strStringWithOutNulls As String

        intPosition = 1
        strStringWithOutNulls = vstrStringWithNulls

        Do While intPosition > 0
            intPosition = InStr(intPosition, vstrStringWithNulls, vbNullChar)

            If intPosition > 0 Then
                strStringWithOutNulls = Left$(strStringWithOutNulls, intPosition - 1) & _
                                  Right$(strStringWithOutNulls, Len(strStringWithOutNulls) - intPosition)
            End If

            If intPosition > strStringWithOutNulls.Length Then
                Exit Do
            End If
        Loop

        Return strStringWithOutNulls

    End Function

    Function HasAdminUsers() As Boolean

        HasAdminUsers = False

        Dim tablePerms As DsTablePerm = New DsTablePerm
        Dim usersToGroups As DsUserToGroup = New DsUserToGroup

        If File.Exists(pathAdmin & "usersToGroupsXP.xml") And _
           File.Exists(pathAdmin & "tablePermsXP.xml") Then

            ReadEncrpytedXML("usersToGroupsXP.xml", usersToGroups)
            ReadEncrpytedXML("tablePermsXP.xml", tablePerms)

            Dim perms() As DataRow = tablePerms.TablePerm.Select("TableName = '" & strRightsAdmin & "'")

            For p As Integer = 0 To perms.Length - 1

                Dim userRows() As DataRow = usersToGroups.UserToGroup.Select("GroupID=" & CStr(perms(p)("GroupID")))
                If userRows.Length > 0 Then Return True

            Next p

        End If

        Return False

    End Function

    

End Module
