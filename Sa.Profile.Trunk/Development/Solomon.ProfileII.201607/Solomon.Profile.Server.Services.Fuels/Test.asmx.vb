﻿

Imports System.Web.Services
Imports System.Collections
Imports System.Collections.Specialized
Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens

Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Imports System.Configuration

Enum RSCRUDE1
    RAIL = 100001
    TT = 100002
    TB = 100003
    OMB = 100004
    BB = 100005
    PL = 100006
End Enum


Enum RSPROD1
    RAIL = 100010
    TT = 100011
    TT0 = 100012
    TB = 100013
    OMB = 100014
    BB = 100015
    PL = 100016
End Enum

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://solomononline.com/FuelProfileWebServices/SubmitServices")> _
Public Class Test1
    Inherits System.Web.Services.WebService

    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
    Dim ActivityLog As New ActivityLog()

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        'InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call
    End Sub



    <WebMethod()> _
    Public Function HelloWorldTest() As String
        Dim svc As New Solomon.Profile.Server.Services.FuelProfileWebServices.SubmitServices()

        Return "Hello WorldTest"
    End Function

End Class