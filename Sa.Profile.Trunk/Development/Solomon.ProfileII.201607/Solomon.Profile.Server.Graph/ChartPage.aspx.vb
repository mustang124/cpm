﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Collections.Generic
Imports System.IO

Imports System.Data.SqlClient
Imports System.Collections.Specialized


Public Class ChartPage
    Inherits System.Web.UI.Page

    Private _refineryId As String = String.Empty
    Protected chrtData As New DataSet
    Protected _totField As String = String.Empty
    Private svc As Solomon.Profile.ChartService.ChartFactory = Nothing
    Private _unitsChart As Boolean = False
    Private _showChrtData As Boolean = True



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'IF error "System.Web.HttpException: No http handler was found for request type 'GET'" then:
        ' put the handlers in web.config  <system.webserver><handlers> :
        '<add name="ChartImg" verb="*" path="ChartImg.axd" type="System.Web.UI.DataVisualization.Charting.ChartHttpHandler,     System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  />

        'Use this URL:
        'http://localhost:2153/ChartPage.aspx?cn=Average+Produced+Fuel+Cost&field1=ISNULL(EnergyCost_Prod%2c0)+AS+%27Singapore%27&field2=&field3=&field4=&field5=&sd=6/1/2015&ed=6/1/2016&UOM=US&currency=USD&sn=2014&chart=Column|Red|||&WsP=X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=
        'troubleshoot this error:

        Dim clientKey As String = Request.QueryString("wsp")
        If Not ValidateKey(clientKey) And Not IsPostBack Then
            Response.Redirect("InvalidUser.htm")
        End If

        Dim chartInfo As String = Request.QueryString("chart")
        Dim chart As Chart = Nothing
        Dim conx As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
        svc = New Solomon.Profile.ChartService.ChartFactory(conx, _refineryId)
        Dim chartInfoRequested As String = Request.QueryString("cn") 'name of set of data requested
        Dim chartInfoParts As String() = chartInfo.Split("|")

        Dim chartType As String = chartInfoParts(0)
        Dim chartColor As String = chartInfoParts(1)
        Dim avgColor As String = chartInfoParts(2)
        Dim ytdColor As String = chartInfoParts(3)
        Dim targetColor As String = chartInfoParts(4)


        Dim fields As New List(Of String)

        If Not IsNothing(Request.QueryString("field1")) Then
            fields.Add(Request.QueryString("field1"))
            _totField = Request.QueryString("field1")
        End If

        If Not IsNothing(Request.QueryString("field2")) Then fields.Add(Request.QueryString("field2"))
        If Not IsNothing(Request.QueryString("field3")) Then fields.Add(Request.QueryString("field3"))
        If Not IsNothing(Request.QueryString("field4")) Then fields.Add(Request.QueryString("field4"))
        If Not IsNothing(Request.QueryString("field5")) Then fields.Add(Request.QueryString("field5"))




        Dim periodStart As String = Request.QueryString("sd")
        Dim periodEnd As String = Request.QueryString("ed")
        Dim currency As String = Request.QueryString("currency")
        Dim scenario As String = Request.QueryString("sn")
        Dim factorSet As String = String.Empty  'lookup from TSort
        Dim uom As String = Request.QueryString("UOM")
        Dim tableName As String = Request.QueryString("tableName")

        If Not IsNothing(Request.QueryString("chrtData")) Then
            If Request.QueryString("chrtData") = "N" Then
                _showChrtData = False
            End If
        End If

        If chartType.ToUpper().Trim() <> "RADAR" Then
            'Chart stackedCol = factory.GetCommonChartTest(fields, "XXPAC", "6/1/2015", "6/1/2016", "Net Raw Material Input", "USD", "2014", "US", "2014", "?", "STACKEDCOLUMN", "Red", "Blue", "Yellow", "Orange");
            If Not Request.QueryString.ToString().Contains("unitId") Then
                '    If (Not IsNothing(Request.QueryString("YTD")) AndAlso Request.QueryString("YTD").Length > 0) OrElse
                '        (Not IsNothing(Request.QueryString("target")) AndAlso Request.QueryString("target").Length > 0) OrElse
                '        (Not IsNothing(Request.QueryString("TwelveMonthAvg")) AndAlso Request.QueryString("TwelveMonthAvg").Length > 0) Then
                chart = svc.GetCommonChartWithTheseFields(fields, _refineryId, periodStart, periodEnd, chartInfoRequested, currency,
                factorSet, uom, scenario, chartInfoRequested, chartType, chartColor, avgColor, ytdColor, targetColor,
                Request.QueryString("YTD"), Request.QueryString("target"), Request.QueryString("TwelveMonthAvg"), tableName.ToUpper())
                'Else
                '    'Throw New Exception("Using Old svc.GetCommonChart")
                '    chart = svc.GetCommonChart(fields, _refineryId, periodStart, periodEnd, chartInfoRequested, currency,
                '    factorSet, uom, scenario, chartInfoRequested, chartType, chartColor, avgColor, ytdColor, targetColor, tableName.ToUpper())
                'End If





                QueryRefinery()
            Else
                _unitsChart = True
                chart = svc.GetUnitsCommonChart(fields, _refineryId, periodStart, periodEnd, chartInfoRequested, currency,
                    "factorset", uom, scenario, chartInfoRequested, chartType, chartColor, avgColor, ytdColor, targetColor,
                    Request.QueryString("unitId"))

                Dim dsTot As DataSet
                'Check if it is a stack chart.If it is use ValueField1 instead of TotField 
                If Not IsNothing(Request.QueryString("field2")) AndAlso _
                  Request.QueryString("field2").Trim.Length > 0 Then
                    dsTot = QueryDb("SELECT 'ISNULL('+ ValueField1 +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString("cn").Trim + "' ")
                Else
                    dsTot = QueryDb("SELECT 'ISNULL('+ replace(TotField,'_Target','') +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString("cn").Trim + "' ")
                End If
                _totField = dsTot.Tables(0).Rows(0)("TotField")
                Dim dsLoc As DataSet = QueryDb("SELECT location FROM Submissions WHERE refineryID='" + _refineryId + "'")
                Dim location As String = dsLoc.Tables(0).Rows(0)("Location")
                _totField = _totField.Replace("Location", location)
                QueryUnits(Request.QueryString("unitId"))
            End If

            If chrtData.Tables.Count <= 0 Or chrtData.Tables(0).Rows.Count <= 0 Then
                Server.Transfer("NoData.htm")
            End If
            Try
                Panel1.Controls.Remove(Chart1)
            Catch ex As Exception
                Throw New Exception("Error removing Chart1: " + ex.Message)
            End Try
            Panel1.Controls.Add(chart)
        Else

            Dim peerGroup As String = Request.QueryString("PeerGroups")
            Dim peerGroups As New List(Of Integer)
            peerGroups.Add(peerGroup)
            Dim rankVariables As New List(Of Integer)
            'Dim breakValue As String = Request.QueryString("breakValue")
            'Dim rankBreak As String = Request.QueryString("rankBreak")

            Dim rankVariableIds() As String = Request.QueryString("rankVariables").Split("|")
            For i As Integer = 0 To rankVariableIds.Length - 1
                rankVariables.Add(rankVariableIds(i))
            Next

            ''split peer group $$
            'Dim delim As String = "$$"
            'Dim delim1 As Integer = peerGroup.IndexOf(delim, 0)
            'Dim delim2 As Integer = peerGroup.IndexOf(delim, delim1 + 4)
            'Dim refListNumber As String = peerGroup.Substring(0, delim1) '-1
            'rankBreak = peerGroup.Substring(delim1 + 2, peerGroup.Length - delim2 - 1)
            'breakValue = peerGroup.Substring(delim2 + 2, peerGroup.Length - delim2 - 2)

            Dim monthColor As String = targetColor ' no targets in radarchart

            chart = svc.GetRadarChart(_refineryId, periodStart, periodEnd, chartInfoRequested,
                    currency, uom, scenario, chartColor, avgColor, ytdColor, monthColor, peerGroups, rankVariables)
        End If
        'Chart1 = chart
        'Panel1.Controls.Add(chart)

        If chartType.ToUpper().Trim() = "RADAR" Then

            If IsNothing(Panel1) Then Response.Write("Panel was not created")
            If IsNothing(chart) Then
                Throw New Exception("Chart was not created. ")
            End If

            Try
                Panel1.Controls.Remove(Chart1)
            Catch ex As Exception
                Throw New Exception("Error removing Chart1: " + ex.Message)
            End Try
            Try
                chart.Height = Int32.Parse(ConfigurationManager.AppSettings("RadarChartsHeight").ToString())
                chart.Width = Int32.Parse(ConfigurationManager.AppSettings("RadarChartsWidth").ToString())
            Catch ex2 As Exception
                Throw New Exception("Error setting chart height: " + ex2.Message)
            End Try
            chart.BorderlineWidth = 0
        End If
        Panel1.Controls.Add(chart)

        'Panel1.Height = 2000

    End Sub

    '    Protected Sub Page_LoadRadarChartTest(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Exit Sub
    '        'later, undo the replacing of Request with testrqst.

    '        Dim testQS As String = "cn=Process+Unit+Utilization&sd=6/1/2015&ed=6/1/2016&UOM=US&currency=USD&sn=2014&field1=ISNULL(ProcessUtilPcnt%2c0)+AS+%27Singapore%27&chart=RADAR|Red|||&study=NSA&breakValue=USA&kpiNames=EII|MaintEffIndex|MaintIndex|MaintPersEffIndex|MechAvail|NEOpexEffIndex|OpAvail|PEI|PersIndex&WsP=X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8="
    '        Dim testrqst As New HttpRequest("", "http://localhost:2153/ChartPage.aspx", testQS)

    '        Dim clientKey As String = testrqst.QueryString("wsp")
    '        If Not ValidateKey(clientKey) And Not IsPostBack Then
    '            Response.Redirect("InvalidUser.htm")
    '        End If

    '        Dim chartInfo As String = testrqst.QueryString("chart")
    '        Dim chart As Chart = Nothing
    '        Dim conx As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
    '        Dim svc As Solomon.Profile.ChartService.ChartFactory = New Solomon.Profile.ChartService.ChartFactory(conx, _refineryId)
    '        Dim chartInfoRequested As String = testrqst.QueryString("cn") 'name of set of data requested
    '        Dim chartInfoParts As String() = chartInfo.Split("|")

    '        Dim chartType As String = chartInfoParts(0)
    '        Dim chartColor As String = chartInfoParts(1)
    '        Dim avgColor As String = chartInfoParts(2)
    '        Dim ytdColor As String = chartInfoParts(3)
    '        Dim targetColor As String = chartInfoParts(4)


    '        Dim fields As New List(Of String)

    '        If Not IsNothing(testrqst.QueryString("field1")) Then fields.Add(testrqst.QueryString("field1"))
    '        If Not IsNothing(testrqst.QueryString("field2")) Then fields.Add(testrqst.QueryString("field2"))
    '        If Not IsNothing(testrqst.QueryString("field3")) Then fields.Add(testrqst.QueryString("field3"))
    '        If Not IsNothing(testrqst.QueryString("field4")) Then fields.Add(testrqst.QueryString("field4"))
    '        If Not IsNothing(testrqst.QueryString("field5")) Then fields.Add(testrqst.QueryString("field5"))

    '        Dim periodStart As String = testrqst.QueryString("sd")
    '        Dim periodEnd As String = testrqst.QueryString("ed")
    '        Dim currency As String = testrqst.QueryString("currency")
    '        Dim scenario As String = testrqst.QueryString("sn")
    '        'Dim factorSet As String = String.Empty  'lookup from TSort
    '        Dim uom As String = testrqst.QueryString("UOM")


    '        If chartType.ToUpper().Trim() <> "RADAR" Then
    '            ' Chart stackedCol = factory.GetCommonChartTest(fields, "XXPAC", "6/1/2015", "6/1/2016", "Net Raw Material Input", "USD", "2014", "US", "2014", "?", "STACKEDCOLUMN", "Red", "Blue", "Yellow", "Orange");

    '            'chart = svc.GetCommonChartTest(fields, _refineryId, periodStart, periodEnd, chartInfoRequested, currency,
    '            '                               uom, scenario, chartInfoRequested, chartType, chartColor, avgColor, ytdColor, targetColor)
    '        Else
    '            Dim breakValue As String = testrqst.QueryString("breakValue")
    '            Dim study As String = testrqst.QueryString("study")
    '            'chart = svc.GetRadarChart("", "Radar", "LightBlue", "Black", "Purple")
    '            'chart = svc.GetRadarChart(chartInfoRequested, "RADAR", avgColor, ytdColor, targetColor)
    '            Dim kpiNames As New List(Of String)

    '            Dim names() As String = testrqst.QueryString("kpiNames").Split("|")
    '            For i As Integer = 0 To names.Length - 1
    '                kpiNames.Add(names(i))
    '            Next



    '            fields = New List(Of String)


    '            '            chart = svc.GetRadarChartTest2(fields, _refineryId, periodStart, periodEnd, chartInfoRequested,
    '            '   currency, uom, scenario, "RADAR", chartColor, avgColor, ytdColor, targetColor, breakValue, study, kpiNames)

    '            '/*                
    '            Dim refListName As String = "NSA14"
    '            breakValue = "USA"
    '            '//string rankVariablesString="EII|OpAvail|MechAvail|PersIndex|PEI|MaintPersEffIndex|MaintIndex|MaintEffIndex|NEOpexEffIndex";
    '            Dim rankVariables As New List(Of String)
    '            rankVariables.Add("EII")
    '            rankVariables.Add("OpAvail")
    '            rankVariables.Add("MechAvail")
    '            rankVariables.Add("PersIndex")
    '            rankVariables.Add("PEI")
    '            rankVariables.Add("MaintPersEffIndex")
    '            rankVariables.Add("MaintIndex")
    '            rankVariables.Add("MaintEffIndex")
    '            rankVariables.Add("NEOpexEffIndex")
    '            ' {"EII", "OpAvail", "MechAvail", "PersIndex", "PEI", "MaintPersEffIndex", "MaintIndex", "MaintEffIndex", "NEOpexEffIndex" }
    '            Dim rankBreak As String = "Area"
    '            '*/
    '            chart = svc.GetRadarChart(_refineryId, periodStart, periodEnd, chartInfoRequested,
    'currency, uom, scenario, chartColor, avgColor, ytdColor, targetColor, refListName, rankBreak, breakValue, rankVariables)

    '            'adjust qs to make this happen:
    '            '        chart = svc.GetRadarChartTest2(fields, "XXPAC", "6/1/2015", "6/1/2016", "TEST NAME",
    '            '"USD", "US", "2014", "RADAR", "Red", "Blue", "Black", "White", "USA", "NSA", kpiNames)

    '        End If

    '        'Dim chart As Chart = svc.GetCommonChart("", "StackedColumn", "Red", "Blue", "Green", "Orange")

    '        Panel1.Controls.Add(chart)
    '    End Sub

    Private Function ValidateKey(clientKey As String) As Boolean
        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(clientKey) Then
                company = Decrypt(clientKey).Split("$".ToCharArray)(0)
                _refineryId = Decrypt(clientKey).Split("$".ToCharArray)(3)
                locIndex = Decrypt(clientKey).Split("$".ToCharArray).Length - 2
                location = Decrypt(clientKey).Split("$".ToCharArray)(locIndex)

                If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Else
                    Return False
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = clientKey.Trim)
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub GetChartData()
        If Not _showChrtData Then Exit Sub
        Dim k As Integer
        Dim strRow, query As String
        Dim s, g As Integer

        If chrtData.Tables.Count > 0 Then
            strRow += "<table frame=box rules=rows width=70% style='font-size: xx-small; font-family: Tahoma;' border=1  cellPadding=1 cellSpacing=0 bordercolor=#000000  id=dataTable>"
            strRow += "<tr bgcolor='dimgray' >"
            strRow += "<th><font color='white'>Month</font></th>"

            'BUILD HEADER
            For k = 4 To chrtData.Tables(0).Columns.Count - 1
                strRow += "<th ><font color='white'>" & chrtData.Tables(0).Columns(k).ColumnName & "</font></th>"
            Next
            strRow += "</tr>"

            'BUILD BODY
            For s = 0 To chrtData.Tables(0).Rows.Count - 1
                Dim row As DataRow = chrtData.Tables(0).Rows(s)
                Dim fieldFormat As String = row(3) 'Assign Decimal Format


                strRow += "<tr>"
                strRow += "<td style=""border-right: solid thin;""><div align=center>" & row(0) & "&nbsp;</div></td>"
                'Fields
                For g = 4 To chrtData.Tables(0).Columns.Count - 1
                    If Not IsDBNull(row(g)) Then
                        strRow += "<td><div align=center>" & String.Format(fieldFormat, row(g)) & "&nbsp;</div></td>"
                    Else
                        strRow += "<td><div align=center> - </div></td>"
                    End If
                Next
                strRow += "</tr>"
            Next
            strRow += "</table> "


        End If
        Response.Write(strRow)
    End Sub

    Private Sub QueryRefinery()
        Dim strRow, query As String
        Dim chartOptions As NameValueCollection = Request.QueryString
        Dim scenario As String = "CLIENT"

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" & _
                " WHEN 0 then '{0:#,##0}'" + _
                " WHEN 1 then '{0:#,##0.0}'" + _
                " WHEN 2 then '{0:N}' " + _
                " END AS DecFormat ," & _totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
           chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        Dim specCases As String

        If Not IsNothing(chartOptions("YTD")) AndAlso _
            chartOptions("YTD").Trim.Length > 0 Then
            'specCases = chartOptions("YTD").Trim
            'If specCases.ToUpper.EndsWith("_YTD") Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
            'End If
            'End If
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
            chartOptions("target").Trim.Length > 0 Then
            'specCases = chartOptions("target").Trim
            'If specCases.ToUpper.EndsWith("_TARGET") Then
            query += ",ISNULL(" & chartOptions("target") + ",0) AS 'Target' "
            'End If
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
           chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            'specCases = chartOptions("twelveMonthAvg").Trim
            'If specCases.ToUpper.EndsWith("_AVG") Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
            'End If
        End If


        'query += " FROM Chart_LU," & chartOptions("table") & " m, Submissions s " & _
        query += " FROM Chart_LU, " & svc.dbtable & " m, Submissions s " & _
             " WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + _refineryId + "' AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
             "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
             "RTRIM(s.DataSet) = 'Actual'  AND  ChartTitle='" + chartOptions("cn").Trim + "' "

        If Not chartOptions("sn") Is Nothing AndAlso _
           chartOptions("sn").Length > 0 Then
            If chartOptions("sn") <> "BASE" Then
                scenario = chartOptions("sn")
            End If
        End If

        Dim factorSet As String = svc.FactorSet

        If svc.dbtable.ToUpper() = "GENSUM" Then
            query += " AND Currency='" + chartOptions("currency") + "' AND m.FactorSet = '" + factorSet + "' AND m.UOM='" + chartOptions("UOM") + "' AND m.Scenario='" + scenario + "' "
        ElseIf svc.dbtable.ToUpper() = "MAINTAVAILCALC" Then
            query += " AND (FactorSet ='" + factorSet + "' ) "
        ElseIf svc.dbtable.ToUpper() = "MAINTINDEX" Then
            query += " AND Currency='" + chartOptions("currency") + "' AND m.FactorSet = '" + factorSet + "' "
        End If

        query += "ORDER BY s.PeriodStart"

        chrtData = QueryDb(query)

    End Sub


    Private Sub QueryUnits(unitId As String)
        Dim strRow, query As String
        Dim selectedUnit As String = unitId
        Dim chartOptions As NameValueCollection = Request.QueryString

        If unitId.Length < 1 Then Exit Sub

        'If Not IsNothing(Request.Form("ListUnits")) AndAlso _
        '   Request.Form("ListUnits").Trim.Length > 0 Then
        '    selectedUnit = Request.Form("ListUnits")
        'Else
        '    Exit Sub
        'End If

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" & _
                " WHEN 0 then '{0:#,##0}'" + _
                " WHEN 1 then '{0:#,##0.0}'" + _
                " WHEN 2 then '{0:N}' " + _
                " END AS DecFormat ," & _totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
           chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        If Not IsNothing(chartOptions("YTD")) AndAlso _
           chartOptions("YTD").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
           chartOptions("target").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("target") + ",0) AS 'Target' "
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
             chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
        End If

        Dim tableName As String = "UnitIndicators"


        'query += " FROM Chart_LU," & chartOptions("table") & " mc ,Submissions s " & _
        '     " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + _refineryId + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
        '     "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
        '     "(RTRIM(s.DataSet) = '" + chartOptions("ds") + "') " + _
        '     " AND ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
        '    " AND mc.Currency='" + chartOptions("currency").Trim + "' " + _
        '    " ORDER BY s.PeriodStart"

        query += " FROM Chart_LU, " & tableName & " mc ,Submissions s " & _
             " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + _refineryId + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
             "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') " + _
             " AND ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
            " AND mc.Currency='" + chartOptions("currency").Trim + "' " + _
            " ORDER BY s.PeriodStart"

        Dim queryUnitName As New System.Text.StringBuilder()
        queryUnitName.Append("SELECT DISTINCT RTRIM(c.ProcessID) +': '+ RTRIM(c.UnitName) AS UnitName ")
        queryUnitName.Append(" FROM Config c, ProcessID_LU p ")
        queryUnitName.Append(" WHERE p.ProcessID=c.ProcessID AND p.MaintDetails='Y' AND c.SubmissionID IN ")
        queryUnitName.Append(" (SELECT SubmissionID FROM Submissions WHERE RefineryID='" + _refineryId + "' AND PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') ")
        queryUnitName.Append(" and c.UnitID = '" + unitId + "'")
        Dim unitName As String = QueryDb(queryUnitName.ToString()).Tables(0).Rows(0)(0).ToString()


        'Dim unitName As String = ListUnits.Items.FindByValue(selectedUnit).Text()
        query = query.Replace("UnitName", unitName.Trim)
        'Response.Write(query)
        chrtData = QueryDb(query)

    End Sub
End Class