﻿Imports System.Web
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports System.Collections.Generic

Public Class UnitIndicators
    Inherits System.Web.UI.Page

    'Protected WithEvents ListUnits As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents PanelUnits As System.Web.UI.WebControls.Panel
    Protected chrtData As New DataSet
    Protected WithEvents ltMessage As System.Web.UI.WebControls.Literal
    Protected totField As String

    Dim _refineryid As String
    Property RefineryID As String
        Get
            Return _refineryid
        End Get
        Set(ByVal Value As String)
            _refineryid = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("UnitIndicatorsQueryString") = Request.QueryString.ToString()
        If IsNothing(Request.QueryString("WsP")) And ListUnits.Visible = False Then
            Response.Redirect("InvalidRequest.htm")
        End If
        If Not ValidateKey(Request.QueryString("WsP")) And Not IsPostBack Then
            Response.Redirect("InvalidUser.htm")
        End If
        'Get refinery id from key
        If Not IsPostBack Then
            RefineryID = GetRefineryID()
            Session("RefID") = RefineryID
        Else
            RefineryID = Session("RefID")
        End If

        Dim dsTot As DataSet
        'Check if it is a stack chart.If it is use ValueField1 instead of TotField 
        If Not IsNothing(Request.QueryString("field2")) AndAlso _
          Request.QueryString("field2").Trim.Length > 0 Then
            dsTot = QueryDb("SELECT 'ISNULL('+ ValueField1 +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString("cn").Trim + "' ")
        Else
            dsTot = QueryDb("SELECT 'ISNULL('+ replace(TotField,'_Target','') +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString("cn").Trim + "' ")
        End If

        totField = dsTot.Tables(0).Rows(0)("TotField")

        Dim dsLoc As DataSet = QueryDb("SELECT location FROM Submissions WHERE refineryID='" + RefineryID + "'")
        Dim location As String = dsLoc.Tables(0).Rows(0)("Location")

        'totField = totField.Replace("Location", location)


        'Select (Request.QueryString("table").ToUpper)
        '   Case "UnitIndicators".ToUpper
        PanelUnits.Visible = True
        If Not IsPostBack Then

            'SB COMMENTED 2017-01-03============================================
            'Dim unitStmt As String = "SELECT DISTINCT c.UnitID, RTRIM(c.ProcessID) +': '+ RTRIM(c.UnitName) AS UnitName,p.SortKey FROM Config c,ProcessID_LU p WHERE p.ProcessID=c.ProcessID AND p.MaintDetails='Y' AND c.SubmissionID IN " + _
            '                         "(SELECT SubmissionID FROM Submissions WHERE RefineryID='" + RefineryID + "'" + _
            '                         " AND PeriodStart BETWEEN '" + Request.QueryString("sd") + "' AND '" + Request.QueryString("ed") + "') ORDER BY p.SortKey"


            'Dim dsUnits As DataSet

            'Dim u As Integer
            'dsUnits = QueryDb(unitStmt)

            ''Populate Unit list
            'ListUnits.DataSource = dsUnits.Tables(0).DefaultView
            '============================================
            Dim svc As New Solomon.Profile.ChartService.ChartFactory(ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString(), RefineryID)
            ListUnits.DataSource = svc.GetUnitsList(RefineryID, Request.QueryString("sd"), Request.QueryString("ed")).DefaultView 'sb added 2017-01-03
            ListUnits.DataTextField = "UnitName"
            ListUnits.DataValueField = "UnitID"
            ListUnits.DataBind()
            ListUnits.Items.Insert(0, "")
        End If

    End Sub

    Private Sub ListUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListUnits.SelectedIndexChanged
        'SB COMMENTED 2017-01-03=================================================
        'If Request.QueryString("table").ToUpper = "CUSTOMUNITDATA" Then
        '    'QueryCustomUnits()
        '    'GetCustomChart()
        'Else
        '    'QueryUnits()

        'End If
        ''GetChart()
        ''GetWindowsChart()
        '========================================================================


        'QueryUnits()
        'build url pointing to ChartPage.aspx
        Dim redirectUrl As String = "ChartPage.aspx?" + "unitId=" + Request.Form("ListUnits") + "&" + Request.QueryString.ToString()
        Response.Redirect(redirectUrl)

    End Sub

    Private Function GetRefineryID() As String
        Dim refId As String

        Dim refLocation As Integer = Decrypt(Request.QueryString("WsP")).Split("$".ToCharArray).Length - 1
        refId = Decrypt(Request.QueryString("WsP")).Split("$".ToCharArray)(refLocation)
        Return refId
    End Function

    Private Function ValidateKey(clientKey As String) As Boolean
        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(clientKey) Then
                company = Decrypt(clientKey).Split("$".ToCharArray)(0)

                locIndex = Decrypt(clientKey).Split("$".ToCharArray).Length - 2
                location = Decrypt(clientKey).Split("$".ToCharArray)(locIndex)

                If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Else
                    Return False
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = clientKey.Trim)
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub QueryCustomUnits()
        Dim strRow, query As String
        Dim selectedUnit As String
        Dim chartOptions As NameValueCollection = Request.QueryString

        If Not IsNothing(Request.Form("ListUnits")) AndAlso _
         Request.Form("ListUnits").Trim.Length > 0 Then
            selectedUnit = Request.Form("ListUnits")
        Else
            Exit Sub
        End If


        If chartOptions("UOM").StartsWith("US") Then
            totField = "USValue AS 'UnitName'"
        Else
            totField = "MetValue AS 'UnitName'"
        End If

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS ,AxisLabelMetric,CASE(Decplaces)" & _
                       " WHEN 0 then '{0:#,##0}'" + _
                       " WHEN 1 then '{0:#,##0.0}'" + _
                       " WHEN 2 then '{0:N}' " + _
                       " END AS DecFormat ," & totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
                  chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        If Not IsNothing(chartOptions("YTD")) AndAlso _
           chartOptions("YTD").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
           chartOptions("target").Trim.Length > 0 Then
            Dim colName As String
            If chartOptions("UOM").StartsWith("US") Then
                colName = "USTarget"
            Else
                colName = "MetTarget"
            End If

            query += ",ISNULL(" & colName + ",0) AS 'Target' "
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
             chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
        End If

        'query += " FROM Chart_LU cl,CustomUnitData mc ,Submissions s " & _
        '     " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
        '     "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
        '     "(RTRIM(s.DataSet) = '" + chartOptions("ds") + "') " + _
        '     " AND ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
        '     " AND(mc.FactorSet='N/A' OR mc.FactorSet='" + chartOptions("YR").Trim + "') " + _
        '     " AND(mc.Currency='N/A' OR mc.Currency='" + chartOptions("currency").Trim + "')  AND mc.Property=replace(cl.TotField,'_Target','')" + _
        '    " ORDER BY s.PeriodStart"

        query += " FROM Chart_LU cl,CustomUnitData mc ,Submissions s " & _
     " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
     "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
     " ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
     " AND(mc.FactorSet='N/A' OR mc.FactorSet='" + chartOptions("sn").Trim + "') " + _
     " AND(mc.Currency='N/A' OR mc.Currency='" + chartOptions("currency").Trim + "')  AND mc.Property=replace(cl.TotField,'_Target','')" + _
    " ORDER BY s.PeriodStart"

        Dim unitName As String = ListUnits.Items.FindByValue(selectedUnit).Text()
        query = query.Replace("UnitName", unitName.Trim)
        'Response.Write(query)
        chrtData = QueryDb(query)

    End Sub

    Private Sub QueryUnits()
        Dim strRow, query As String
        Dim selectedUnit As String
        Dim chartOptions As NameValueCollection = Request.QueryString

        If Not IsNothing(Request.Form("ListUnits")) AndAlso _
           Request.Form("ListUnits").Trim.Length > 0 Then
            selectedUnit = Request.Form("ListUnits")
        Else
            Exit Sub
        End If

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" & _
                " WHEN 0 then '{0:#,##0}'" + _
                " WHEN 1 then '{0:#,##0.0}'" + _
                " WHEN 2 then '{0:N}' " + _
                " END AS DecFormat ," & totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
           chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        If Not IsNothing(chartOptions("YTD")) AndAlso _
           chartOptions("YTD").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
           chartOptions("target").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("target") + ",0) AS 'Target' "
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
             chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
        End If

        Dim miscTable As String = "UnitIndicators"

        'query += " FROM Chart_LU, " & miscTable & " mc ,Submissions s " & _
        '     " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
        '     "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
        '     "(RTRIM(s.DataSet) = '" + chartOptions("ds") + "') " + _
        '     " AND ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
        '    " AND mc.Currency='" + chartOptions("currency").Trim + "' " + _
        '    " ORDER BY s.PeriodStart"

        query += " FROM Chart_LU, " & miscTable & " mc ,Submissions s " & _
     " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
     "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') " + _
     " AND ChartTitle='" + chartOptions("cn").Trim + "' AND mc.UnitID='" + selectedUnit + "' " + _
    " AND mc.Currency='" + chartOptions("currency").Trim + "' " + _
    " ORDER BY s.PeriodStart"
        Dim unitName As String = ListUnits.Items.FindByValue(selectedUnit).Text()
        query = query.Replace("UnitName", unitName.Trim)
        'Response.Write(query)



        chrtData = QueryDb(query)
        'chrtData.WriteXml("C:\UnitIndicatorsChartData.xml")

    End Sub



    Private Sub QueryRefinery()
        Dim strRow, query As String
        Dim chartOptions As NameValueCollection = Request.QueryString
        Dim scenario As String = "CLIENT"

        query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" & _
                " WHEN 0 then '{0:#,##0}'" + _
                " WHEN 1 then '{0:#,##0.0}'" + _
                " WHEN 2 then '{0:N}' " + _
                " END AS DecFormat ," & totField

        If Not IsNothing(chartOptions("field2")) AndAlso _
           chartOptions("field2").Trim.Length > 0 Then
            query += "," & chartOptions("field2")
        End If

        If Not IsNothing(chartOptions("field3")) AndAlso _
           chartOptions("field3").Trim.Length > 0 Then
            query += "," & chartOptions("field3")
        End If

        If Not IsNothing(chartOptions("field4")) AndAlso _
           chartOptions("field4").Trim.Length > 0 Then
            query += "," & chartOptions("field4")
        End If

        If Not IsNothing(chartOptions("field5")) AndAlso _
           chartOptions("field5").Trim.Length > 0 Then
            query += "," & chartOptions("field5")
        End If

        Dim specCases As String

        If Not IsNothing(chartOptions("YTD")) AndAlso _
            chartOptions("YTD").Trim.Length > 0 Then
            'specCases = chartOptions("YTD").Trim
            'If specCases.ToUpper.EndsWith("_YTD") Then
            query += ",ISNULL(" & chartOptions("YTD") + ",0) AS 'Year-To-Date' "
            'End If
            'End If
        End If

        If Not IsNothing(chartOptions("target")) AndAlso _
            chartOptions("target").Trim.Length > 0 Then
            'specCases = chartOptions("target").Trim
            'If specCases.ToUpper.EndsWith("_TARGET") Then
            query += ",ISNULL(" & chartOptions("target") + ",0) AS 'Target' "
            'End If
        End If

        If Not IsNothing(chartOptions("twelveMonthAvg")) AndAlso _
           chartOptions("twelveMonthAvg").Trim.Length > 0 Then
            'specCases = chartOptions("twelveMonthAvg").Trim
            'If specCases.ToUpper.EndsWith("_AVG") Then
            query += ",ISNULL(" & chartOptions("TwelveMonthAvg") + ",0) AS 'Rolling Average' "
            'End If
        End If


        query += " FROM Chart_LU," & chartOptions("table") & " m, Submissions s " & _
             " WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + RefineryID + "' AND (s.PeriodStart BETWEEN '" + CDate(chartOptions("sd")).ToShortDateString + _
             "' AND '" + CDate(chartOptions("ed")).ToShortDateString + "') AND " + _
             "RTRIM(s.DataSet) = '" + chartOptions("ds") + "'  AND  ChartTitle='" + chartOptions("cn").Trim + "' "

        If Not chartOptions("sn") Is Nothing AndAlso _
           chartOptions("sn").Length > 0 Then
            If chartOptions("sn") <> "BASE" Then
                scenario = chartOptions("sn")
            End If
        End If

        If chartOptions("table").ToUpper = "GENSUM" Then
            query += " AND Currency='" + chartOptions("currency") + "' AND m.FactorSet = '" + chartOptions("yr") + "' AND m.UOM='" + chartOptions("UOM") + "' AND m.Scenario='" + scenario + "' "
        ElseIf chartOptions("table").ToUpper = "MAINTAVAILCALC" Then
            query += " AND (FactorSet ='" + chartOptions("yr") + "' ) "
        ElseIf chartOptions("table").ToUpper = "MAINTINDEX" Then
            query += " AND Currency='" + chartOptions("currency") + "' AND m.FactorSet = '" + chartOptions("yr") + "' "
        End If

        query += "ORDER BY s.PeriodStart"

        chrtData = QueryDb(query)

    End Sub

    Private Function CreateHTMLErrorMessage(ByVal msg As String) As String
        Dim txt As String = "<table width=672 height=368 border=0 align=center cellspacing=0 bordercolor=#76777C>" + _
                                  " <tr>" + _
                                      "<td height=35 align=center valign=top><p class=style1>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>&nbsp;</p>" + _
                                      "<p class=noData>" + msg + "</p></td>" + _
                                  "</tr>" + _
                                  "</table>"

        Return txt
    End Function


End Class