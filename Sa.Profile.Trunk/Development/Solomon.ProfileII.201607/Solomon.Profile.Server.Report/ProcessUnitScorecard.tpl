<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)

<table class='small' width="100%" border="0">
  <tr>
    <td width=5></td>
    <td width=5></td>
    <td width=5></td>
    <td width=340></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
    <td width=60></td>
    <td width=5></td>
  <tr>
    <td colspan=4></td>
    <td colspan=2 align=center valign=bottom><strong>ReportPeriod</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Process Target</strong></td>
    <td colspan=4></td>
  </tr>
  <tr>
    <td colspan=12><strong>Process Facilities</strong></td>
  </tr>
  SECTION(ProcessUnits,,)
  BEGIN
  Header('<tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>@Description</strong></td>
  </tr>')
  <tr>
    <td colspan=2></td>
    <td colspan=2 height=30 valign=bottom>Unit Name</td>
    <td colspan=8 height=30 valign=bottom>@UnitName</td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Process ID</td>
    <td colspan=8 valign=bottom>@ProcessID</td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Process Type</td>
    <td colspan=8 valign=bottom>@ProcessType</td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Capacity, @CapUnits </td>
    <td valign=bottom align=right>Format(Cap,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>EDC</td>
    <td valign=bottom align=right>Format(EDC,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>UEDC</td>
    <td valign=bottom align=right>Format(UEDC,'#,##0')</td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Utilization, %</td>
    <td valign=bottom align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(UtilPcnt_Target,'#,##0.0')))</td>
    <td colspan=5></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Mechanical Availability, %</td>
    <td valign=bottom align=right>Format(MechAvail,'#,##0.0')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(MechAvail_Target,'#,##0.0')))</td>
    <td colspan=5></td>
  </tr>
   <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Operational Availability, %</td>
    <td valign=bottom align=right>Format(OpAvail,'#,##0.0')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(OpAvail_Target,'#,##0.0')))</td>
    <td colspan=5></td>
  </tr> 
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>On-Stream Factor, %</td>
    <td valign=bottom align=right>Format(OnStream,'#,##0.0')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(OnStream_Target,'#,##0.0')))</td>
    <td colspan=5></td>
   </tr> 
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Non-Turnaround Expenses, CurrencyCode/bbl</td>
    <td valign=bottom align=right>Format(RoutCost,'N')</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(RoutCost_Target,'N')))</td>
    <td colspan=5></td>
   </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 valign=bottom>Turnaround Expenses, CurrencyCode/bbl</td>
    <td valign=bottom align=right>NoShowZero(Format(TACost,'N'))</td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(TACost_Target,'N')))</td>
    <td colspan=5></td>
   </tr> 

 END
  <tr>
    <td colspan=12></td>
  </tr>
  <tr>
    <td colspan=4></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>Capacity</strong></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>Utilization</br>Percent</strong></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>EDC</strong></td>
    <td height=45 valign=bottom colspan=2 align=center height=30 valign=bottom><strong>UEDC</strong></td>
  </tr>
  <tr>
    <td colspan=12><strong>Utilities and Off-Sites</strong></td>
  </tr>
   
  SECTION(Generated, ProcessID='STEAMGEN' ,)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>Steam Generation,@CapUnits</strong></td>
  </tr>
  ')
  END
  
  SECTION(Generated,ProcessID='STEAMGEN' AND ProcessType='SFB',)
   BEGIN 
  <tr>
    <td colspan=2></td>
    <td colspan=2>Solid Fired Boilers</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
   END
   
  SECTION(Generated, ProcessID='STEAMGEN' AND ProcessType='LGFB',)
   BEGIN 
  <tr>
    <td colspan=2></td>
    <td colspan=2>Liquid/Gas Fired Boilers</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  
  SECTION(Generated, ProcessID='ELECGEN' ,)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>Electric Power Generation, kW</strong></td>
  </tr>
  ')
  END
  
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='SFB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Solid Fired Boiler (Condensing Turbines)</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='LGFB',)
  BEGIN
   <tr>
    <td colspan=2></td>
    <td colspan=2>Liquid/Gas Boiler (Condensing Turbines)</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr> 
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='DSL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Diesel Engine Driver</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='FT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Fired Turbine Driver (No Steam Produced)</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='EXP',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Expander Turbine Driver other than FCC</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='STT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Steam Topping (Let-Down) Turbine Driver</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr>
  END
  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='EXTR',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Extraction Turbine Driver</td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(UEDC,'#,##0')</td>
    <td></td>
  </tr> 
   END
  
SECTION(Generated,ProcessID='FCCPOWER',)
BEGIN
<tr>
    <td></td>
    <td colspan=3 valign=bottom height = 30>Cat Cracker Power Recovery Train, bhp</td>
    <td valign=bottom align=right valign=bottom height = 30>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right valign=bottom height = 30>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right valign=bottom height = 30>Format(EDC,'#,##0')</td>
    <td></td>
    <td align=right valign=bottom height = 30>Format(UEDC,'#,##0')</td>
    <td></td>
</tr>
END

  <tr>
    <td colspan=12></td>
  </tr>
  <tr>
    <td colspan=4></td>
    <td height=30 valign=bottom colspan=2 align=center valign=bottom><strong>Barrels</strong></td>
    <td height=30 valign=bottom colspan=2 align=center valign=bottom><strong>EDC</strong></td>
    <td height=30 valign=bottom colspan=2 align=center valign=bottom><strong>UEDC</strong></td>
    <td colspan=2></td>
  </tr>

  SECTION(RawMaterial,ProcessID='RSCRUDE',)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11><strong>Raw Material Receipts</strong></td>
  </tr>
  ')
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='RAIL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Railcar</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='TT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Tank Truck</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='TB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Tanker Berth</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='OMB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Offshore Buoy</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='BB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Barge Berth</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='PL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Pipeline</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  
  SECTION(RawMaterial,ProcessID='RSPROD',)
  BEGIN
  HEADER('
  <tr>
    <td></td>
    <td colspan=11 height=30 valign=bottom><strong>Product Shipments</strong></td>
  </tr>')
  END
  
  SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='RAIL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Railcar</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='TT',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Tank Truck</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
  SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='TB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Tanker Berth</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='OMB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Offshore Buoy</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='BB',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Barge Berth</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
   SECTION(RawMaterial, ProcessID='RSPROD' AND ProcessType='PL',)
  BEGIN
  <tr>
    <td colspan=2></td>
    <td colspan=2>Pipeline</td>
    <td align=right>NoShowZero(Format(BPD,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td></td>
    <td align=right>NoShowZero(Format(UEDC,'#,##0'))</td>
    <td colspan=3></td>
  </tr>
  END
</table>
<!-- template-end -->