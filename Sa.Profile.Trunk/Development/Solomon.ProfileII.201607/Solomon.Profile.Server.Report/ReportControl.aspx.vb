Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports System.IO
Imports Solomon.Profile.Server.HTML  ' .GenerateInTableHtml ' Solomon.GenerateInTableHtml '  GenrateInTableHtml
Imports System.Collections.Generic

Partial Public Class ReportControl
    Inherits System.Web.UI.Page
    'Private Const C_HTTP_HEADER_CONTENT As String = "Content-Disposition"
    'Private Const C_HTTP_ATTACHMENT As String = "attachment;filename="
    'Private Const C_HTTP_INLINE As String = "inline;filename="
    'Private Const C_HTTP_CONTENT_TYPE_OCTET As String = "application/octet-stream"
    'Private Const C_HTTP_CONTENT_TYPE_EXCEL As String = "application/ms-excel"
    'Private Const C_HTTP_CONTENT_LENGTH As String = "Content-Length"
    'Private Const C_QUERY_PARAM_CRITERIA As String = "Criteria"
    'Protected WithEvents C1SubMenu1 As C1.Web.C1Command.C1SubMenu
    'Private Const C_ERROR_NO_RESULT As String = "Data not found."
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected refineryId As String
    Protected baseControl As Control
    Protected WSP As String = String.Empty
    Private _validateKeyMsg As String = String.Empty

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _validateKeyMsg = String.Empty
        'Put user code to initialize the page here
        System.Net.ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy

        'sample URL: http://localhost:4048/ReportControl.aspx?rn=Personnel Report&sd=6/1/2016&ds=ACTUAL&UOM=US&currency=USD&yr=2014&target=True&avg=True&ytd=True&SN=CLIENT&TS=10/5/2016 5:17:03 PM

        '#######  IN CASE OF ERROR 'File not found', it might be that it can't find the .tpl file (path is in web.config)
        '#######  IN CASE OF InvalidRequest page, it might be mismatch .ct file found in _cert folder.

        WSP = Request.Headers("WsP")
        If IsNothing(WSP) Then
            'Response.Redirect("Test.html")
            WSP = Request.QueryString("WsP")
        End If

        If IsNothing(WSP) Then
            'Response.Redirect("Test2.html")
            Server.Transfer("InvalidRequest.htm")
        End If

        'If My.Computer.Name.Contains("S1F7B50") Then
        '    refineryId = "XXPAC"
        'Else

        If Not ValidateKey() And Not IsPostBack Then
            'Response.Redirect("Test.aspx?msg=" + _validateKeyMsg)
            Server.Transfer("InvalidUser.htm")
        End If

        'Get refinery id from key
        refineryId = GetRefineryID()

        'Response.Redirect("Test.aspx?msg=RefineryId is " + refineryId)


    End Sub

    Private Function GetRefineryID() As String
        Dim refId As String
        Dim refLocation As Integer = Decrypt(WSP).Split("$".ToCharArray).Length - 1
        refId = Decrypt(WSP).Split("$".ToCharArray)(refLocation)
        Return refId
    End Function

    Private Function ValidateKey() As Boolean
        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(WSP) Then
                company = Decrypt(WSP).Split("$".ToCharArray)(0)
                _validateKeyMsg += "Company = " + company + "|"
                locIndex = Decrypt(WSP).Split("$".ToCharArray).Length - 2
                location = Decrypt(WSP).Split("$".ToCharArray)(locIndex)
                _validateKeyMsg += "Location=" + location + "|"
                If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Else
                    _validateKeyMsg += "No key file|"
                    Return False
                End If
                _validateKeyMsg += "path is found|"
                If path.Trim.Length = 0 Then
                    _validateKeyMsg += "PathTrimLen is 0|"
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    _validateKeyMsg += "readKey.Trim is " + readKey.Trim() + "|"
                    _validateKeyMsg += "WSP.Trm is " + WSP.Trim() + "|"
                    Return (readKey.Trim = WSP.Trim)
                End If
            Else
                _validateKeyMsg += "WSP is nothing|"
                Return False
            End If

        Catch ex As Exception
            _validateKeyMsg += "EXCEPTION " + ex.Message
            Throw ex
        End Try
    End Function


    Private Function AreCalcsDone() As Boolean
        Dim sqlstmt As String = "SELECT Distinct SubmissionID,CalcsNeeded FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'"

        Dim ds As DataSet = QueryDb(sqlstmt)
        Dim calcsNeeded As Object

        If ds.Tables(0).Rows.Count > 0 Then
            calcsNeeded = ds.Tables(0).Rows(0)("CalcsNeeded")
            Return IsDBNull(calcsNeeded) Or IsNothing(calcsNeeded)
        Else
            Return False
        End If
    End Function

    Public Function GetCoLoc() As String
        Dim sqlstmt As String = "SELECT Distinct SubmissionID,Company,Location FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'"

        Dim ds As DataSet = QueryDb(sqlstmt)
        Dim colocation As String

        If ds.Tables(0).Rows.Count > 0 Then
            colocation = ds.Tables(0).Rows(0)("Company") & " - " & ds.Tables(0).Rows(0)("Location")
            Return colocation
        Else
            Return String.Empty
        End If
    End Function

    Public Sub GetReport()
        If AreCalcsDone() Then
            Dim ds As New DataSet

            ' GMO 10/16/2007
            Dim tReportName As String = Request.QueryString("rn").ToUpper

            Dim rowsOrdinalsToInclude As String = String.Empty
            If Not IsNothing(Request.QueryString("rows")) Then 'need this IF else might get set to Nothing and err below
                rowsOrdinalsToInclude = Request.QueryString("rows")
            End If

            Dim tReportDataFilter As String
            Dim htmlText As String
            Dim Scenario As String = "CLIENT"
            ' END GMO

            ' GMO 12/5/2007
            Dim tReportTitle As String
            ' END GMO
            If (Not (Request.QueryString("sn") Is Nothing) AndAlso _
                (Request.QueryString("sn").Length > 0)) Then
                If Scenario <> "BASE" Then
                    Scenario = Request.QueryString("sn")
                End If
            End If
            '"http:\\localhost\ReportControls\ReportControl.aspx?rn=Refinery Scorecard&sd=5/1/2013&ds=ACTUAL&UOM=MET&currency=USD&yr=2010&
            'target=True&avg=True&ytd=True&SN=CLIENT&TS=8/18/2014 09:39:11 AM&WsP=KUBJ9o3kg9MjXyCahjocyYpTWjLMdFYLuFnPQirvhJh3CZThYA8Qwg=="
            ds = GetDataSetForReport(GetRefineryID(), _
                                    Request.QueryString("rn"), _
                                    Year(CDate(Request.QueryString("sd").ToString)).ToString, _
                                    Month(CDate(Request.QueryString("sd").ToString)).ToString, _
                                    Request.QueryString("ds").ToString, _
                                    Request.QueryString("yr").ToString, _
                                    Scenario, _
                                    Request.QueryString("Currency"), _
                                    Request.QueryString("UOM"), _
                                    rowsOrdinalsToInclude)
            'ds.WriteXml("C:\\temp\\ds.xml")
            Select Case Request.QueryString("rn").ToUpper

                Case "REFINERY TRENDS REPORT"

                    Dim bYTD As Boolean = CType(Request.QueryString("ytd"), Boolean)
                    Dim bAVG As Boolean = CType(Request.QueryString("avg"), Boolean)

                    If (bYTD = True And bAVG = True) Or (bYTD = False And bAVG = False) Then
                        ' show total fields only
                        tReportDataFilter = "Current Month"
                        ds.Tables("Chart_LU").Columns("TotField").ColumnName = "ValueField1"
                    Else
                        If bYTD = True Then
                            ' show ytd fields only
                            tReportDataFilter = "Year To Date"
                            ds.Tables("Chart_LU").Columns("YTDField").ColumnName = "ValueField1"
                        Else
                            If bAVG = True Then
                                ' show avg fields only
                                ds.Tables("Chart_LU").Columns("AVGField").ColumnName = "ValueField1"
                                tReportDataFilter = "Rolling Average"
                            End If
                        End If
                    End If



                Case "EII AND VEI BREAKDOWN"

                    'Dim n As Integer
                    'Dim dtFunction As DataTable = ds.Tables("Functions")
                    'Dim dtProcessData As DataTable = ds.Tables("ProcessData")
                    'For n = 0 To dtFunction.Rows.Count - 1
                    '    'Do substititons in formulas
                    '    Dim eiiFormula As String = dtFunction.Rows(n)("EIIFormulaForReport").ToString
                    '    Dim veiFormula As String = dtFunction.Rows(n)("VEIFormulaForReport").ToString
                    '    Dim unitID As String = dtFunction.Rows(n)("UnitID").ToString
                    '    ' Dim submissionID As String = dtFunction.Rows(n)("SubmissionID").ToString
                    '    Dim varRows() As DataRow = dtProcessData.Select("UnitId='" + unitID + "'")

                    '    If dtProcessData.Rows.Count > 0 Then
                    '        Dim j As Integer
                    '        Dim variables As String = ""
                    '        Dim fieldFormat As String = ""
                    '        For j = 0 To varRows.Length - 1
                    '            Select Case varRows(j)("UsDecPlaces")
                    '                Case 0
                    '                    fieldFormat = "{0:#,##0}"
                    '                Case 1
                    '                    fieldFormat = "{0:#,##0.0}"
                    '                Case 2
                    '                    fieldFormat = "{0:N}"
                    '            End Select
                    '            eiiFormula = eiiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))
                    '            veiFormula = veiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))

                    '            variables += "<tr>"
                    '            variables += "<td colspan=2>&nbsp;</td>"
                    '            variables += "<td>" + Chr(Asc("A") + j) + " - " + varRows(j)("UsDescription").Trim.ToString + "</td>"
                    '            variables += "<td  align=right>" + String.Format(fieldFormat, varRows(j)("SAValue")).ToString.Trim + "</td>"
                    '            variables += "<td>&nbsp;</td>"
                    '            variables += "</tr>"
                    '        Next
                    '        dtFunction.Rows(n)("EIIFormulaForReport") = eiiFormula
                    '        dtFunction.Rows(n)("VEIFormulaForReport") = veiFormula
                    '        dtFunction.Rows(n)("Variables") = variables

                    '    End If
                    'Next

            End Select

            If ds.Tables.Count = 0 Then
                Exit Sub
            End If
            Dim pg As New GenrateInTableHtml.Page
            Dim replacementValues As New Hashtable
            replacementValues.Add("ReportPeriod", Format(CDate(Request.QueryString("sd")), "MMMM yyyy"))
            replacementValues.Add("IfTargetOn", Request.QueryString("target"))
            replacementValues.Add("IfYtdOn", Request.QueryString("ytd"))
            replacementValues.Add("IfAvgOn", Request.QueryString("avg"))
            replacementValues.Add("CoLoca", GetCoLoc())


            ' GMO 10/16/2007
            If tReportName = "REFINERY TRENDS REPORT" Then
                replacementValues.Add("ReportDataFilter", tReportDataFilter)
            End If
            ' END GMO

            htmlText += pg.createPage(ds, Request.QueryString("rn").ToUpper, Request.QueryString("UOM").Trim(), Request.QueryString("currency").Trim(), replacementValues)




            Response.Write(htmlText)
        Else
            Response.Redirect("CalcsAreNotFinished.htm")
        End If


    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' This section added by GMO 12/6/2007
    ' Function invoked from HTML in order to get long description
    ' for the report title.  Used only for process unit reports.
    Public Function GetReportTitle(ByVal tProcessID As String) As String
        Dim tQuery As String
        Dim tReportTitle As String
        Dim tReportCode As String

        ' First 3 or 4 characters contain code
        tReportCode = Mid(tProcessID, 1, 4)

        ' Construct query with parameters
        tQuery = "SELECT Description FROM ProcessID_LU WHERE ProcessID = '" & Trim(tReportCode) & "'"

        ' Get dataset for the report
        Dim dt As DataTable = QueryDb(tQuery).Tables(0)
        tReportTitle = dt.Rows(0).Item("Description") & " Report"

        GetReportTitle = tReportTitle

    End Function
    ' END GMO 12/4/2007
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private Function GetDataSetForReport(ByVal refineryID As String, _
                                            ByVal reportName As String, _
                                            ByVal reportYear As Integer, _
                                            ByVal reportMonth As Integer, _
                                            ByVal dataSet As String, _
                                            ByVal factorSet As String, _
                                            ByVal scenario As String, _
                                            ByVal currency As String, _
                                            ByVal UOM As String,
                                            rowsToInclude As String) As DataSet
        Dim ds As New DataSet


        Dim dtStProc As DataTable = QueryDb("exec spGetReportProcs '" + refineryID + "','" + reportName + "'").Tables(0)
        'Dim dtStProc As DataTable = MakeTestDataTable()


        Dim a As Integer
        Dim execString As String = String.Empty
        Dim tablenames(dtStProc.Rows.Count) As String

        For a = 0 To dtStProc.Rows.Count - 1
            Dim procedureName As String = dtStProc.Rows(a)("ProcName")
            tablenames(a) = dtStProc.Rows(a)("DataTableName")
            execString += "exec " + procedureName + " '" + _
                                           refineryID + "'," + _
                                           reportYear.ToString() + "," + _
                                           reportMonth.ToString() + ",'" + _
                                           dataSet + "','" + _
                                           factorSet + "','" + _
                                           scenario + "','" + _
                                           currency + "','" + _
                                           UOM + "';"
        Next

        ds = QueryDb(execString)
        For a = 0 To ds.Tables.Count - 1
            Try 'test excluding some procs
                'if rowsToInclude = "" then include ALL rows. else pass in the ones to include.
                If tablenames(a) = "Chart_LU" And rowsToInclude.Length > 0 Then
                    RemoveRows(ds.Tables(a), tablenames(a), rowsToInclude)  ' escluding some rows of the report
                End If
                ds.Tables(a).TableName = tablenames(a)
            Catch
            End Try
        Next

        Return ds
    End Function

    Private Function MakeTestDataTable() As DataTable
        Dim dt As New DataTable
        Dim dc As New DataColumn("DataTableName", GetType(System.String))
        dt.Columns.Add(dc)
        dc = New DataColumn("ProcName", GetType(System.String))
        dt.Columns.Add(dc)
        dc = New DataColumn("Template", GetType(System.String))
        dt.Columns.Add(dc)

        dt = AddRow("Chart_LU", "dbo.spReportChartLUTot", "RefineryScorecard.tpl", dt)
        dt = AddRow("GENSUM", "dbo.spReportGensum", "RefineryScorecard.tpl", dt)
        dt = AddRow("Maintenance", "dbo.spReportRSMaintenance", "RefineryScorecard.tpl", dt)

        'dt = AddRow("Average", "dbo.spReportRSAverage", "RefineryScorecard.tpl", dt)
        'dt = AddRow("Energy", "dbo.spReportRSEnergy", "RefineryScorecard.tpl", dt)
        '
        'dt = AddRow("MAINTAVAILCALC", "dbo.spReportAvail", "RefineryScorecard.tpl", dt)

        'dt = AddRow("OpEx", "dbo.spReportRSOpex", "RefineryScorecard.tpl", dt)
        'dt = AddRow("Performance", "dbo.spReportRSPerformance", "RefineryScorecard.tpl", dt)
        'dt = AddRow("Pers", "dbo.spReportRSPers", "RefineryScorecard.tpl", dt)
        'dt = AddRow("UserDefined", "dbo.spReportUserDefined", "RefineryScorecard.tpl", dt)
        'dt = AddRow("Yield", "dbo.spReportRSYield", "RefineryScorecard.tpl", dt)

        Return dt
    End Function
    Private Function AddRow(col1 As String, col2 As String, col3 As String, ByRef dt As DataTable) As DataTable
        Dim dr As DataRow = dt.NewRow()
        dr(0) = col1
        dr(1) = col2
        dr(2) = col3
        dt.Rows.Add(dr)
        Return dt
    End Function

    Private Sub RemoveRows(ByRef dt As DataTable, friendlyTableName As String, rowsOrdinalsToInclude As String)
        'Dim arr() As String = {"Average Energy Cost", "Average Produced Fuel Cost", "Average Purchased Energy Cost", "Cash Basis ROI", "Cash Margin", "Cash Operating Expenses Barrel Basis", "Cash Operating Expenses UEDC Basis", "Crude Gravity", "Crude Sulfur", "EDC", "Energy Consumption per Barrel", "Energy Intensity Index"}
        Dim RowNamesToInclude As List(Of String) = GetListOfChartRows(friendlyTableName)
        'loop thru list of rows, check it's 1st column's values to see if is in list or rows we want to keep.
        'if not found in list, then delete that row.
        Dim ordinals As List(Of Integer) = GetOrdinals(rowsOrdinalsToInclude)
        For rowcount As Integer = 0 To dt.Rows.Count - 1
            Dim found As Boolean = False
            Dim test As String = dt.Rows(rowcount)(0).ToString().Trim()
            For Each ordinal As Integer In ordinals
                If dt.Rows(rowcount)(0).ToString().Trim() = RowNamesToInclude(ordinal) Then
                    found = True
                    Exit For
                End If
            Next
            If Not found Then dt.Rows(rowcount).Delete()
        Next
        dt.AcceptChanges()
        Return
    End Sub

    Public Function GetOrdinals(rowsOrdinalsToInclude As String) As List(Of Integer)
        Dim list As New List(Of Integer)
        If rowsOrdinalsToInclude.Length > 0 Then
            For i As Integer = 0 To rowsOrdinalsToInclude.Length - 1 Step 2
                Dim ordinal As String = Mid(rowsOrdinalsToInclude, i + 1, 2)
                list.Add(Int32.Parse(ordinal))
            Next
        End If
        Return list
    End Function

    Public Function GetListOfChartRows(friendlyTableName As String) As IList(Of String)
        If friendlyTableName.Contains("Chart_LU") Then
            Dim list As New List(Of String)

            list.Add("Average Data" & Chr(14) & "AVGDATA") 'use a checkbox header object instead
            list.Add("Process Unit Utilization")
            list.Add("Process Unit Utilization Outside T/A")
            list.Add("Refinery Utilization")
            list.Add("Net Raw Material Input")
            list.Add("Crude Gravity")
            list.Add("Crude Sulfur")
            list.Add("EDC") ' 1")
            list.Add("UEDC 1")
            list.Add("Replacement Value")
            list.Add("Performance Indicators" & Chr(14) & "PERFIND") 'use a checkbox header object instead
            list.Add("Energy Intensity Index")
            list.Add("Personnel Index")
            list.Add("Personnel Efficiency Index")
            list.Add("Maintenance Personnel Efficiency Index")
            list.Add("Non-Maintenance Personnel Efficiency Index")
            list.Add("Maintenance Index")
            list.Add("Maintenance Efficiency Index")
            list.Add("Cash Operating Expenses UEDC Basis")
            list.Add("Non-Energy Efficiency Index")
            list.Add("Volumetric Expansion Index")
            list.Add("Cash Margin")
            list.Add("Cash Basis ROI")

            list.Add("Energy" & Chr(14) & "ENERGY") 'use a checkbox header object instead

            list.Add("Average Purchased Energy Cost")
            list.Add("Average Produced Fuel Cost")
            list.Add("Average Energy Cost")
            list.Add("Energy Consumption per Barrel")

            list.Add("Maintenance" & Chr(14) & "MAINT") 'use a checkbox header object instead

            list.Add("Non-Turnaround Index")
            list.Add("Turnaround Index")
            list.Add("Non-Turnaround MEI")
            list.Add("Turnaround MEI")
            list.Add("Mechanical Availability")
            list.Add("Operational Availability")
            list.Add("On-Stream Factor")
            list.Add("On-Stream Factor w/ Slowdowns")
            list.Add("Mechanical Availability Monthly T/A")
            list.Add("Operational Availability Monthly T/A")
            list.Add("On-Stream Factor Monthly T/A")
            list.Add("On-Stream Factor w/ Slowdowns & Monthly T/A")
            list.Add("Mechanical Unavailability due to T/A")
            list.Add("Maintenance Index")

            list.Add("Operating Expenses" & Chr(14) & "OPEX") 'use a checkbox header object instead

            list.Add("Non-Volume-Related Expenses")
            list.Add("Volume-Related Expenses")
            list.Add("Non-Energy Operating Expenses UEDC Basis")
            list.Add("Non-Energy Operating Expenses EDC Basis")

            list.Add("Personnel" & Chr(14) & "PERS") 'use a checkbox header object instead

            list.Add("O,C&C Personnel Index")
            list.Add("M,P&S Personnel Index")
            list.Add("O,C&C Employee Overtime")
            list.Add("M,P&S Employee Overtime")
            list.Add("Process Wage Earners/Supervisors")
            list.Add("Maintenance Wage Earners/Supervisors")
            list.Add("O,C&C Total Absences")
            list.Add("M,P&S Total Absences")
            list.Add("Maintenance Work Force")

            list.Add("Yields and Margins" & Chr(14) & "YIELDS") 'use a checkbox header object instead

            list.Add("Raw Material Costs")
            list.Add("Gross Product Value")
            list.Add("Gross Margin")
            list.Add("Cash Operating Expenses Barrel Basis")
            list.Add("Volumetric Gain")

            Return list
        End If
        Return Nothing
    End Function
    'Private Function GetSelectList(ByVal tTableName As String) As String
    '    Dim dsSelectList As New DataSet
    '    Dim bYTD As Boolean = Request.QueryString("ytd")
    '    Dim bAvg As Boolean = Request.QueryString("avg")
    '    Dim tNameSuffix As String
    '    Dim tSQL As String
    '    Dim tTable As String
    '    Dim iColumnCount As Integer
    '    Dim i As Integer
    '    Dim tColName As String
    '    Dim tSelectList As String = ""
    '    Dim tSuffix As String

    '    If (bYTD = True And bAvg = True) Or (bYTD = False And bAvg = False) Then
    '        tNameSuffix = "NONE"
    '    Else
    '        If bYTD = True Then
    '            tNameSuffix = "_YTD"
    '        Else
    '            If bAvg = True Then
    '                tNameSuffix = "_Avg"
    '            End If
    '        End If
    '    End If

    '    tSQL = _
    '    "SELECT " _
    '    & "o.name, " _
    '    & "c.name, " _
    '    & "SUBSTRING(c.name, (len(c.name)-3), 4) as NameSuffix, " _
    '    & "SUBSTRING(c.name, 1, (len(c.name)-3)) as NamePrefix " _
    '    & "FROM sysobjects o, syscolumns c " _
    '    & "where " _
    '    & "o.name = '" & tTable & "' " _
    '    & "and " _
    '    & "o.id = c.id " _
    '    & "order by NameSuffix desc "

    '    Dim dtSelectList As DataTable = QueryDb(tSQL).Tables(0)
    '    dsSelectList.Merge(dtSelectList)

    '    iColumnCount = dsSelectList.Tables(0).Columns.Count()

    '    If tNameSuffix <> "NONE" Then
    '        For i = 0 To iColumnCount
    '            tColName = dsSelectList.Tables(0).Columns(i).ToString
    '            tSuffix = Mid(Trim(tColName), (Len(tColName) - 4), 4)

    '            If (tSuffix = tNameSuffix) Then
    '                tSelectList = tSelectList & tColName & ", "
    '            Else
    '                If (tSuffix <> "rget") Then
    '                    tSelectList = tSelectList & tColName & ", "
    '                End If
    '            End If
    '        Next
    '    Else
    '        For i = 0 To iColumnCount
    '            tColName = dsSelectList.Tables(0).Columns(i).ToString
    '            tSuffix = Mid(Trim(tColName), (Len(tColName) - 4), 4)

    '            If (tSuffix <> "rget") And (tSuffix <> "_YTD") And (tSuffix <> "_Avg") Then
    '                tSelectList = tSelectList & tColName & ", "
    '            End If
    '        Next
    '    End If

    'End Function ' GetSelectList()_


    'Private Sub TransmitFile(ByVal [output] As String, ByVal extension As String)
    '    Dim filename As String = Page.MapPath("data\report." & extension)
    '    'Dim c1Report As New C1.Web.C1WebReport.C1WebReport
    '    ' Delete old data for precaution 
    '    'If System.IO.File.Exists(filename) Then
    '    'System.IO.File.Delete(filename)
    '    'End If

    '    ' c1Report.Report.RenderToFile(filename, GetFileType([output]))
    '    'Response.WriteFile(filename)
    '    'Dim file As System.IO.FileInfo = New System.IO.FileInfo(filename)

    '    Response.Clear()
    '    Response.ClearHeaders()
    '    Response.AddHeader(C_HTTP_HEADER_CONTENT, C_HTTP_ATTACHMENT & "report." & extension)
    '    Response.ContentType = "application/octet-stream"
    '    'Response.AddHeader(C_HTTP_CONTENT_LENGTH, File.Length())
    '    'Response.TransmitFile(filename)
    '    Response.Flush()

    'End Sub

    'Private Function GetFileType(ByVal [output] As String) As C1.Win.C1Report.FileFormatEnum
    '    Dim ct As C1.Win.C1Report.FileFormatEnum = C1.Win.C1Report.FileFormatEnum.Text
    '    Try
    '        ct = CType([Enum].Parse(GetType(C1.Win.C1Report.FileFormatEnum), [output]), C1.Win.C1Report.FileFormatEnum)
    '    Catch
    '    End Try
    '    Return ct
    'End Function 'getChartType


    'Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    ' C1WebReport1.ShowPDF()
    'End Sub

    'Private Sub WordMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    'Private Sub RTFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("RTF", "rtf")
    'End Sub

    'Private Sub PDFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("PDF", "pdf")
    'End Sub

    'Private Sub ExcelMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub

    'Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub


End Class

