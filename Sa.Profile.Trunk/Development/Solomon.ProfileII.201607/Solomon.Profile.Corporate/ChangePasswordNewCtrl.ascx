﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ChangePasswordNewCtrl.ascx.vb" Inherits="Solomon.Profile.Corporate.ChangePasswordNewCtrl" %>
<div class="row" id="main-work">
        <br />
        <br />
        <br />
        <br />
        <br />

        <form id="frmMain" name="frmMain" runat="server">
            <div class="well">
                <div class="passwordBlock">
                    <div class="form-group row">
                        <label for="inputPassword1" class="col-sm-4 col-form-label margin-top-10">Old Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword1" placeholder="Password" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword2" class="col-sm-4 col-form-label margin-top-10">New Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword2" placeholder="Password" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-4 col-form-label margin-top-10">New Password Again</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10 margin-top-10">
                            <button type="submit" class="btn btn-primary">Change Password</button>
                        </div>
                    </div>
                </div>
            </div>

            <%--              
		<asp:Literal ID="ltJsScript" runat="server"></asp:Literal>--%>
        </form>

</div>