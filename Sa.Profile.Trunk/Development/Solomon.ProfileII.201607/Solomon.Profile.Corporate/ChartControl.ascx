<%@ Control Language="vb" ClassName="ChartControl" CodeBehind="ChartControl.ascx.vb" AutoEventWireup="false" Inherits="ProfileII_Corporate.ChartControl" %>
<%@ Register TagPrefix="c1webchart" Namespace="C1.Web.C1WebChart" Assembly="C1.Web.C1WebChart, Version=1.0.20044.14252, Culture=neutral, PublicKeyToken=360971499c5cdc04" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<style type="text/css">#nav { Z-INDEX: 2 }
	#chart { }
	</style>
		<style type="text/css">.noData { FONT-WEIGHT: bold; FONT-SIZE: 18px; COLOR: #646567; FONT-FAMILY: Tahoma }
	.unitsheader { FONT-SIZE: 10pt; FONT-FAMILY: Tahoma }
	.showPrint { DISPLAY: none }
	.style4 { FONT-WEIGHT: bold; FONT-SIZE: 12pt; FONT-FAMILY: Tahoma }
	.style5 { FONT-SIZE: 10pt; FONT-FAMILY: Tahoma }
	</style>
		<LINK media="print" href="styles/print.css" type="text/css" rel="StyleSheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" color="#FFFFFF">
		<table border="0" height="100%" width="650">
			<tr>
				<td height="20%">
					<table style="Z-INDEX: 120" height="85" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td width="314" background="images\ReportBGLeft.gif" bgColor="dimgray">
								<DIV id="LabelName" align="center"><strong> <font color="white">
											<span style="DISPLAY:block;MARGIN-LEFT:5px;WIDTH:68%">
												<%= ChartName %>
											</span>
										</font></strong>
								</DIV>
								<br>
								<DIV id="Methodology" align="center"><font size="2" color="white">
										<%=  StudyYear.ToString + " Methodology" %>
									</font>
								</DIV>
							</td>
							<td align="right" width="32%" colspan="5"><!--div align="right"--><IMG height="50" alt="" src="https://webservices.solomononline.com/ProfileII/images/SA logo RECT.jpg"
									width="300"><br>
								<!--/div-->
								<!--DIV id="LabelMonth" align="right"--><font size="2">
									<%=  Date.Today.ToLongDateString()%>
								</font>
								<!--/DIV--></td>
						</tr>
					</table>
					<asp:panel id="PanelUnits" style="Z-INDEX: 116" Width="368px" CssClass="noshow" Visible="False"
						runat="server">
						<DIV class="unitsheader"><BR>
							Select
							<asp:DropDownList id="ListUnits" runat="server" Width="155px" Font-Names="Tahoma" Font-Size="7pt"
								Font-Bold="True" ForeColor="DarkBlue" font="Tahoma" Height="338px" AutoPostBack="true"></asp:DropDownList></DIV>
					</asp:panel>
					<hr width="100%" color="#000000" SIZE="1">
						<center>
                            <h5>
                            <%--<%= iif (Request.QueryString("rptOptions").ToString() <> "Current" ,Request.QueryString("rptOptions").ToString(),String.Empty) %>--%>
                            <%  If Request.QueryString("rptOptions") <> "Current" Then%>
                                <% =Request.QueryString("rptOptions") %>
                            <% End If%>
                            </h5>
						</center>
				</td>
			</tr>
			<tr>
				<td align="center" height="352">
                    <c1webchart:C1WebChart ID="C1WebChart1" Width="594px" runat="server" Height="352px" ImageTransferMethod="Cache"
                        ImageAlign="Middle" ImageFormat="Png">
                        <Serializer />

                    </c1webchart:C1WebChart>
				</td>
			</tr>
			<tr>
				<td align="center" height="20%">
					<%=GetChartData()%>
				</td>
			</tr>
		</table>
	</body>
</HTML>
