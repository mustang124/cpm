Imports System.IO
Imports System.Data
Imports System.Net
Imports System.Configuration
Imports System.Net.Mail

Public Class navigator
    Inherits System.Web.UI.Page

    Protected WithEvents sdMethodology As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents DsMethodology1 As Solomon.Profile.Corporate.dsMethodology
    Protected WithEvents sdPeriods As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents DsPeriods1 As Solomon.Profile.Corporate.dsPeriods
    Protected WithEvents ddlUOM As System.Web.UI.WebControls.DropDownList
    Protected WithEvents PlaceHolder1 As System.Web.UI.WebControls.PlaceHolder
    Dim ctree, rtree As Control
    Protected WithEvents sdCurrency As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents PlaceHolder2 As System.Web.UI.WebControls.PlaceHolder
    Protected WithEvents sdChartLU As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents DsChartLU1 As Solomon.Profile.Corporate.dsChartLU
    Protected WithEvents Image1 As System.Web.UI.WebControls.Image
    Protected WithEvents ltSummary As System.Web.UI.WebControls.Literal
    Protected WithEvents ddlCurrency As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlMethodology As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbRefinery As System.Web.UI.WebControls.ListBox
    Protected WithEvents ltJsScript As System.Web.UI.WebControls.Literal
    'Protected WithEvents C1WebTopicBarPanelBodyHelper4 As C1.Web.C1Command.C1WebTopicBarPanelBodyHelper
    Protected WithEvents ddlStart As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlEnd As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnViewReports As System.Web.UI.WebControls.Button
    Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Protected WithEvents rdoReportOptions As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Protected WithEvents sdScenario As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Protected WithEvents DsScenario1 As Solomon.Profile.Corporate.dsScenario
    Protected WithEvents rbScenario As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents DsCurrency1 As Solomon.Profile.Corporate.dsCurrency
    Dim sql As String = ""

#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
    Dim _readVar As String = _connectionString
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        ' Me.C1ContextMenu1 = New C1.Win.C1Command.C1ContextMenu
        ' Me.C1CommandHolder1 = New C1.Win.C1Command.C1CommandHolder
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.sdTSort = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsTSort1 = New Solomon.Profile.Corporate.dsTSort
        Me.sdMethodology = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.DsMethodology1 = New Solomon.Profile.Corporate.dsMethodology
        Me.sdPeriods = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.DsPeriods1 = New Solomon.Profile.Corporate.dsPeriods
        Me.sdCurrency = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.DsCurrency1 = New Solomon.Profile.Corporate.dsCurrency
        Me.sdChartLU = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.DsChartLU1 = New Solomon.Profile.Corporate.dsChartLU
        Me.sdScenario = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.DsScenario1 = New Solomon.Profile.Corporate.dsScenario
        ' CType(Me.C1CommandHolder1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsTSort1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMethodology1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPeriods1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCurrency1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsChartLU1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsScenario1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'C1ContextMenu1
        '
        'Me.C1ContextMenu1.Name = "C1ContextMenu1"
        '
        'C1CommandHolder1
        '
        'Me.C1CommandHolder1.Commands.Add(Me.C1ContextMenu1)
        ' Me.C1CommandHolder1.Owner = Nothing
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = _connectionString
        '
        'sdTSort
        '
        Me.sdTSort.DeleteCommand = Me.SqlDeleteCommand1
        Me.sdTSort.InsertCommand = Me.SqlInsertCommand1
        Me.sdTSort.SelectCommand = Me.SqlSelectCommand1
        Me.sdTSort.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "TSort", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Location", "Location"), New System.Data.Common.DataColumnMapping("RefineryID", "RefineryID")})})
        Me.sdTSort.UpdateCommand = Me.SqlUpdateCommand1

        ' ---------------------------------------------------------------------
        ' SqlDeleteCommand1
        ' 
        sql = "DELETE FROM dbo.TSort "
        sql += "WHERE (RefineryID = @Original_RefineryID) "
        sql += "  AND (Location = @Original_Location OR @Original_Location IS NULL AND Location IS NULL); "

        'Me.SqlDeleteCommand1.CommandText = "DELETE FROM dbo.TSort WHERE (RefineryID = @Original_RefineryID) AND (Location = @" & _
        '"Original_Location OR @Original_Location IS NULL AND Location IS NULL)"
        Me.SqlDeleteCommand1.CommandText = sql
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Location", System.Data.SqlDbType.VarChar, 40, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Location", System.Data.DataRowVersion.Original, Nothing))

        ' ---------------------------------------------------------------------
        ' SqlInsertCommand1
        ' 
        sql = "INSERT INTO dbo.TSort(Location, RefineryID) "
        sql += "VALUES (@Location, @RefineryID); "

        sql += "SELECT Location, RefineryID "
        sql += "FROM dbo.TSort  "
        sql += "WHERE (RefineryID = @RefineryID); "

        'Me.SqlInsertCommand1.CommandText = "INSERT INTO dbo.TSort(Location, RefineryID) VALUES (@Location, @RefineryID); SELE" & _
        '"CT Location, RefineryID FROM dbo.TSort WHERE (RefineryID = @RefineryID)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Location", System.Data.SqlDbType.VarChar, 40, "Location"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Location, RefineryID FROM dbo.TSort"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' SqlUpdateCommand1
        '
        sql = "UPDATE dbo.TSort "
        sql += "SET Location = @Location, RefineryID = @RefineryID "
        sql += "WHERE (RefineryID = @Original_RefineryID) "
        sql += "  AND (Location = @Original_Location OR @Original_Location IS NULL AND Location IS NULL);"

        sql += "SELECT Location, RefineryID FROM dbo.TSort WHERE (RefineryID = @RefineryID); "

        'Me.SqlUpdateCommand1.CommandText = "UPDATE dbo.TSort SET Location = @Location, RefineryID = @RefineryID WHERE (Refine" & _
        '"ryID = @Original_RefineryID) AND (Location = @Original_Location OR @Original_Loc" & _
        '"ation IS NULL AND Location IS NULL); SELECT Location, RefineryID FROM dbo.TSort " & _
        '"WHERE (RefineryID = @RefineryID)"
        Me.SqlUpdateCommand1.CommandText = sql
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Location", System.Data.SqlDbType.VarChar, 40, "Location"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Location", System.Data.SqlDbType.VarChar, 40, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Location", System.Data.DataRowVersion.Original, Nothing))

        ' ---------------------------------------------------------------------
        ' DsTSort1
        '
        Me.DsTSort1.DataSetName = "dsTSort"
        Me.DsTSort1.Locale = New System.Globalization.CultureInfo("en-US")

        ' ---------------------------------------------------------------------
        ' sdMethodology
        '
        Me.sdMethodology.SelectCommand = Me.SqlSelectCommand2
        Me.sdMethodology.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Factors", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("FactorSet", "FactorSet")})})

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT DISTINCT FactorSet FROM dbo.Factors WHERE (IsNumeric(FactorSet) = 1) ORDER" &
        " BY FactorSet DESC"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' DsMethodology1
        '
        Me.DsMethodology1.DataSetName = "dsMethodology"
        Me.DsMethodology1.Locale = New System.Globalization.CultureInfo("en-US")

        ' ---------------------------------------------------------------------
        ' sdPeriods
        '
        Me.sdPeriods.SelectCommand = Me.SqlSelectCommand3
        Me.sdPeriods.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Submissions", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("PeriodStart", "PeriodStart"), New System.Data.Common.DataColumnMapping("PeriodEnd", "PeriodEnd"), New System.Data.Common.DataColumnMapping("StartDate", "StartDate"), New System.Data.Common.DataColumnMapping("EndDate", "EndDate")})})

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT DISTINCT PeriodStart, PeriodEnd, DATENAME(mm, PeriodStart) + ' ' + CONVERT" &
        " (varchar, YEAR(PeriodStart)) AS StartDate, DATENAME(mm, PeriodEnd) + ' ' + CONV" &
        "ERT (varchar, YEAR(PeriodEnd)) AS EndDate FROM dbo.Submissions"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' DsPeriods1
        '
        Me.DsPeriods1.DataSetName = "dsPeriods"
        Me.DsPeriods1.Locale = New System.Globalization.CultureInfo("en-US")

        ' ---------------------------------------------------------------------
        ' sdCurrency
        '
        Me.sdCurrency.SelectCommand = Me.SqlSelectCommand4
        Me.sdCurrency.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Submissions", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RptCurrency", "RptCurrency")})})

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT DISTINCT RptCurrency FROM dbo.Submissions"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' DsCurrency1
        '
        Me.DsCurrency1.DataSetName = "dsCurrency"
        Me.DsCurrency1.Locale = New System.Globalization.CultureInfo("en-US")

        ' ---------------------------------------------------------------------
        ' sdChartLU
        '
        Me.sdChartLU.DeleteCommand = Me.SqlDeleteCommand2
        Me.sdChartLU.InsertCommand = Me.SqlInsertCommand2
        Me.sdChartLU.SelectCommand = Me.SqlSelectCommand5
        Me.sdChartLU.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Chart_LU", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ChartTitle", "ChartTitle"), New System.Data.Common.DataColumnMapping("SectionHeader", "SectionHeader"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("ChartType", "ChartType"), New System.Data.Common.DataColumnMapping("AxisLabelUS", "AxisLabelUS"), New System.Data.Common.DataColumnMapping("AxisLabelMetric", "AxisLabelMetric"), New System.Data.Common.DataColumnMapping("DataTable", "DataTable"), New System.Data.Common.DataColumnMapping("ValueField1", "ValueField1"), New System.Data.Common.DataColumnMapping("Legend1", "Legend1"), New System.Data.Common.DataColumnMapping("ValueField2", "ValueField2"), New System.Data.Common.DataColumnMapping("Legend2", "Legend2"), New System.Data.Common.DataColumnMapping("ValueField3", "ValueField3"), New System.Data.Common.DataColumnMapping("Legend3", "Legend3"), New System.Data.Common.DataColumnMapping("ValueField4", "ValueField4"), New System.Data.Common.DataColumnMapping("Legend4", "Legend4"), New System.Data.Common.DataColumnMapping("ValueField5", "ValueField5"), New System.Data.Common.DataColumnMapping("Legend5", "Legend5"), New System.Data.Common.DataColumnMapping("TargetField", "TargetField"), New System.Data.Common.DataColumnMapping("YTDField", "YTDField"), New System.Data.Common.DataColumnMapping("AvgField", "AvgField"), New System.Data.Common.DataColumnMapping("DecPlaces", "DecPlaces"), New System.Data.Common.DataColumnMapping("TotField", "TotField")})})
        Me.sdChartLU.UpdateCommand = Me.SqlUpdateCommand2

        ' ---------------------------------------------------------------------
        ' SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM dbo.Chart_LU WHERE (ChartTitle = @Original_ChartTitle) AND (AvgField " &
        "= @Original_AvgField OR @Original_AvgField IS NULL AND AvgField IS NULL) AND (Ax" &
        "isLabelMetric = @Original_AxisLabelMetric) AND (AxisLabelUS = @Original_AxisLabe" &
        "lUS) AND (ChartType = @Original_ChartType) AND (DataTable = @Original_DataTable)" &
        " AND (DecPlaces = @Original_DecPlaces OR @Original_DecPlaces IS NULL AND DecPlac" &
        "es IS NULL) AND (Legend1 = @Original_Legend1 OR @Original_Legend1 IS NULL AND Le" &
        "gend1 IS NULL) AND (Legend2 = @Original_Legend2 OR @Original_Legend2 IS NULL AND" &
        " Legend2 IS NULL) AND (Legend3 = @Original_Legend3 OR @Original_Legend3 IS NULL " &
        "AND Legend3 IS NULL) AND (Legend4 = @Original_Legend4 OR @Original_Legend4 IS NU" &
        "LL AND Legend4 IS NULL) AND (Legend5 = @Original_Legend5 OR @Original_Legend5 IS" &
        " NULL AND Legend5 IS NULL) AND (SectionHeader = @Original_SectionHeader) AND (So" &
        "rtKey = @Original_SortKey) AND (TargetField = @Original_TargetField OR @Original" &
        "_TargetField IS NULL AND TargetField IS NULL) AND (TotField = @Original_TotField" &
        " OR @Original_TotField IS NULL AND TotField IS NULL) AND (ValueField1 = @Origina" &
        "l_ValueField1 OR @Original_ValueField1 IS NULL AND ValueField1 IS NULL) AND (Val" &
        "ueField2 = @Original_ValueField2 OR @Original_ValueField2 IS NULL AND ValueField" &
        "2 IS NULL) AND (ValueField3 = @Original_ValueField3 OR @Original_ValueField3 IS " &
        "NULL AND ValueField3 IS NULL) AND (ValueField4 = @Original_ValueField4 OR @Origi" &
        "nal_ValueField4 IS NULL AND ValueField4 IS NULL) AND (ValueField5 = @Original_Va" &
        "lueField5 OR @Original_ValueField5 IS NULL AND ValueField5 IS NULL) AND (YTDFiel" &
        "d = @Original_YTDField OR @Original_YTDField IS NULL AND YTDField IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartTitle", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartTitle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelMetric", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelUS", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataTable", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataTable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DecPlaces", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DecPlaces", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SectionHeader", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SectionHeader", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TargetField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TargetField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_YTDField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "YTDField", System.Data.DataRowVersion.Original, Nothing))

        ' ---------------------------------------------------------------------
        ' SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO dbo.Chart_LU(ChartTitle, SectionHeader, SortKey, ChartType, AxisLabel" &
        "US, AxisLabelMetric, DataTable, ValueField1, Legend1, ValueField2, Legend2, Valu" &
        "eField3, Legend3, ValueField4, Legend4, ValueField5, Legend5, TargetField, YTDFi" &
        "eld, AvgField, DecPlaces, TotField) VALUES (@ChartTitle, @SectionHeader, @SortKe" &
        "y, @ChartType, @AxisLabelUS, @AxisLabelMetric, @DataTable, @ValueField1, @Legend" &
        "1, @ValueField2, @Legend2, @ValueField3, @Legend3, @ValueField4, @Legend4, @Valu" &
        "eField5, @Legend5, @TargetField, @YTDField, @AvgField, @DecPlaces, @TotField); S" &
        "ELECT ChartTitle, SectionHeader, SortKey, ChartType, AxisLabelUS, AxisLabelMetri" &
        "c, DataTable, ValueField1, Legend1, ValueField2, Legend2, ValueField3, Legend3, " &
        "ValueField4, Legend4, ValueField5, Legend5, TargetField, YTDField, AvgField, Dec" &
        "Places, TotField FROM dbo.Chart_LU WHERE (ChartTitle = @ChartTitle)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartTitle", System.Data.SqlDbType.VarChar, 100, "ChartTitle"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SectionHeader", System.Data.SqlDbType.VarChar, 50, "SectionHeader"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartType", System.Data.SqlDbType.VarChar, 50, "ChartType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelUS", System.Data.SqlDbType.VarChar, 50, "AxisLabelUS"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, "AxisLabelMetric"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataTable", System.Data.SqlDbType.VarChar, 50, "DataTable"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField1", System.Data.SqlDbType.VarChar, 50, "ValueField1"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend1", System.Data.SqlDbType.VarChar, 50, "Legend1"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField2", System.Data.SqlDbType.VarChar, 50, "ValueField2"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend2", System.Data.SqlDbType.VarChar, 50, "Legend2"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField3", System.Data.SqlDbType.VarChar, 50, "ValueField3"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend3", System.Data.SqlDbType.VarChar, 50, "Legend3"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField4", System.Data.SqlDbType.VarChar, 50, "ValueField4"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend4", System.Data.SqlDbType.VarChar, 50, "Legend4"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField5", System.Data.SqlDbType.VarChar, 50, "ValueField5"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend5", System.Data.SqlDbType.VarChar, 50, "Legend5"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TargetField", System.Data.SqlDbType.VarChar, 50, "TargetField"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@YTDField", System.Data.SqlDbType.VarChar, 50, "YTDField"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgField", System.Data.SqlDbType.VarChar, 50, "AvgField"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DecPlaces", System.Data.SqlDbType.TinyInt, 1, "DecPlaces"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotField", System.Data.SqlDbType.VarChar, 50, "TotField"))

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand5
        '
        sql = "SELECT	ChartTitle "
        sql += "	,	SectionHeader "
        sql += "	,	SortKey "
        sql += "	,	ChartType "
        sql += "	,	AxisLabelUS "
        sql += "	,	AxisLabelMetric "
        sql += "	,	DataTable "
        sql += "	,	ValueField1 "
        sql += "	,	Legend1 "
        sql += "	,	ValueField2 "
        sql += "	,	Legend2 "
        sql += "	,	ValueField3 "
        sql += "	,	Legend3 "
        sql += "	,	ValueField4 "
        sql += "	,	Legend4 "
        sql += "	,	ValueField5 "
        sql += "	,	Legend5 "
        sql += "	,	TargetField "
        sql += "	,	YTDField "
        sql += "	,	AvgField "
        sql += "	,	DecPlaces "
        sql += "	,	TotField "
        sql += "FROM dbo.Chart_LU;"

        'Me.SqlSelectCommand5.CommandText = "SELECT ChartTitle, SectionHeader, SortKey, ChartType, AxisLabelUS, AxisLabelMetri" & _
        '"c, DataTable, ValueField1, Legend1, ValueField2, Legend2, ValueField3, Legend3, " & _
        '"ValueField4, Legend4, ValueField5, Legend5, TargetField, YTDField, AvgField, Dec" & _
        '"Places, TotField FROM dbo.Chart_LU
        Me.SqlSelectCommand5.CommandText = sql
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' 
        'SqlUpdateCommand2
        '
        sql = "UPDATE dbo.Chart_LU "
        sql += "SET ChartTitle 		= @ChartTitle "
        sql += ",	SectionHeader 	= @SectionHeader "
        sql += ",	SortKey 		= @SortKey "
        sql += ",	ChartType 		= @ChartType "
        sql += ",	AxisLabelUS 	= @AxisLabelUS "
        sql += ",	AxisLabelMetric	= @AxisLabelMetric "
        sql += ",	DataTable 		= @DataTable "
        sql += ",	ValueField1 	= @ValueField1 "
        sql += ",	Legend1 		= @Legend1 "
        sql += ",	ValueField2 	= @ValueField2 "
        sql += ",	Legend2 		= @Legend2 "
        sql += ",	ValueField3 	= @ValueField3 "
        sql += ",	Legend3 		= @Legend3 "
        sql += ",	ValueField4 	= @ValueField4 "
        sql += ",	Legend4 		= @Legend4 "
        sql += ",	ValueField5 	= @ValueField5 "
        sql += ",	Legend5 		= @Legend5 "
        sql += ",	TargetField 	= @TargetField "
        sql += ",	YTDField 		= @YTDField "
        sql += ",	AvgField 		= @AvgField "
        sql += ",	DecPlaces 		= @DecPlaces "
        sql += ",	TotField 		= @TotField "
        sql += "WHERE (ChartTitle = @Original_ChartTitle) "
        sql += "  AND (AvgField = @Original_AvgField OR @Original_AvgField IS NULL AND AvgField IS NULL) "
        sql += "  AND (AxisLabelMetric = @Original_AxisLabelMetric) "
        sql += "  AND (AxisLabelUS = @Original_AxisLabelUS) "
        sql += "  AND (ChartType = @Original_ChartType) "
        sql += "  AND (DataTable = @Original_DataTable) "
        sql += "  AND (DecPlaces = @Original_DecPlaces OR @Original_DecPlaces IS NULL AND DecPlaces IS NULL) "
        sql += "  AND (Legend1 = @Original_Legend1 OR @Original_Legend1 IS NULL AND Legend1 IS NULL) "
        sql += "  AND (Legend2 = @Original_Legend2 OR @Original_Legend2 IS NULL AND Legend2 IS NULL) "
        sql += "  AND (Legend3 = @Original_Legend3 OR @Original_Legend3 IS NULL AND Legend3 IS NULL) "
        sql += "  AND (Legend4 = @Original_Legend4 OR @Original_Legend4 IS NULL AND Legend4 IS NULL) "
        sql += "  AND (Legend5 = @Original_Legend5 OR @Original_Legend5 IS NULL AND Legend5 IS NULL) "
        sql += "  AND (SectionHeader = @Original_SectionHeader) AND (SortKey = @Original_SortKey) "
        sql += "  AND (TargetField = @Original_TargetField OR @Original_TargetField IS NULL AND TargetField IS NULL) "
        sql += "  AND (TotField = @Original_TotField OR @Original_TotField IS NULL AND TotField IS NULL) "
        sql += "  AND (ValueField1 = @Original_ValueField1 OR @Original_ValueField1 IS NULL AND ValueField1 IS NULL) "
        sql += "  AND (ValueField2 = @Original_ValueField2 OR @Original_ValueField2 IS NULL AND ValueField2 IS NULL) "
        sql += "  AND (ValueField3 = @Original_ValueField3 OR @Original_ValueField3 IS NULL AND ValueField3 IS NULL) "
        sql += "  AND (ValueField4 = @Original_ValueField4 OR @Original_ValueField4 IS NULL AND ValueField4 IS NULL) "
        sql += "  AND (ValueField5 = @Original_ValueField5 OR @Original_ValueField5 IS NULL AND ValueField5 IS NULL) "
        sql += "  AND (YTDField = @Original_YTDField OR @Original_YTDField IS NULL AND YTDField IS NULL); "

        sql += "SELECT	ChartTitle "
        sql += "	,	SectionHeader "
        sql += "	,	SortKey "
        sql += "	,	ChartType "
        sql += "	,	AxisLabelUS "
        sql += "	,	AxisLabelMetric "
        sql += "	,	DataTable "
        sql += "	,	ValueField1 "
        sql += "	,	Legend1 "
        sql += "	,	ValueField2 "
        sql += "	,	Legend2 "
        sql += "	,	ValueField3 "
        sql += "	,	Legend3 "
        sql += "	,	ValueField4 "
        sql += "	,	Legend4 "
        sql += "	,	ValueField5 "
        sql += "	,	Legend5 "
        sql += "	,	TargetField "
        sql += "	,	YTDField "
        sql += "	,	AvgField "
        sql += "	,	DecPlaces "
        sql += "	,	TotField "
        sql += "FROM dbo.Chart_LU "
        sql += "WHERE (ChartTitle = @ChartTitle);"

        'Me.SqlUpdateCommand2.CommandText = "UPDATE dbo.Chart_LU SET ChartTitle = @ChartTitle, SectionHeader = @SectionHeader," & _
        '" SortKey = @SortKey, ChartType = @ChartType, AxisLabelUS = @AxisLabelUS, AxisLab" & _
        '"elMetric = @AxisLabelMetric, DataTable = @DataTable, ValueField1 = @ValueField1," & _
        '" Legend1 = @Legend1, ValueField2 = @ValueField2, Legend2 = @Legend2, ValueField3" & _
        '" = @ValueField3, Legend3 = @Legend3, ValueField4 = @ValueField4, Legend4 = @Lege" & _
        '"nd4, ValueField5 = @ValueField5, Legend5 = @Legend5, TargetField = @TargetField," & _
        '" YTDField = @YTDField, AvgField = @AvgField, DecPlaces = @DecPlaces, TotField = " & _
        '"@TotField WHERE (ChartTitle = @Original_ChartTitle) AND (AvgField = @Original_Av" & _
        '"gField OR @Original_AvgField IS NULL AND AvgField IS NULL) AND (AxisLabelMetric " & _
        '"= @Original_AxisLabelMetric) AND (AxisLabelUS = @Original_AxisLabelUS) AND (Char" & _
        '"tType = @Original_ChartType) AND (DataTable = @Original_DataTable) AND (DecPlace" & _
        '"s = @Original_DecPlaces OR @Original_DecPlaces IS NULL AND DecPlaces IS NULL) AN" & _
        '"D (Legend1 = @Original_Legend1 OR @Original_Legend1 IS NULL AND Legend1 IS NULL)" & _
        '" AND (Legend2 = @Original_Legend2 OR @Original_Legend2 IS NULL AND Legend2 IS NU" & _
        '"LL) AND (Legend3 = @Original_Legend3 OR @Original_Legend3 IS NULL AND Legend3 IS" & _
        '" NULL) AND (Legend4 = @Original_Legend4 OR @Original_Legend4 IS NULL AND Legend4" & _
        '" IS NULL) AND (Legend5 = @Original_Legend5 OR @Original_Legend5 IS NULL AND Lege" & _
        '"nd5 IS NULL) AND (SectionHeader = @Original_SectionHeader) AND (SortKey = @Origi" & _
        '"nal_SortKey) AND (TargetField = @Original_TargetField OR @Original_TargetField I" & _
        '"S NULL AND TargetField IS NULL) AND (TotField = @Original_TotField OR @Original_" & _
        '"TotField IS NULL AND TotField IS NULL) AND (ValueField1 = @Original_ValueField1 " & _
        '"OR @Original_ValueField1 IS NULL AND ValueField1 IS NULL) AND (ValueField2 = @Or" & _
        '"iginal_ValueField2 OR @Original_ValueField2 IS NULL AND ValueField2 IS NULL) AND" & _
        '" (ValueField3 = @Original_ValueField3 OR @Original_ValueField3 IS NULL AND Value" & _
        '"Field3 IS NULL) AND (ValueField4 = @Original_ValueField4 OR @Original_ValueField" & _
        '"4 IS NULL AND ValueField4 IS NULL) AND (ValueField5 = @Original_ValueField5 OR @" & _
        '"Original_ValueField5 IS NULL AND ValueField5 IS NULL) AND (YTDField = @Original_" & _
        '"YTDField OR @Original_YTDField IS NULL AND YTDField IS NULL); SELECT ChartTitle," & _
        '" SectionHeader, SortKey, ChartType, AxisLabelUS, AxisLabelMetric, DataTable, Val" & _
        '"ueField1, Legend1, ValueField2, Legend2, ValueField3, Legend3, ValueField4, Lege" & _
        '"nd4, ValueField5, Legend5, TargetField, YTDField, AvgField, DecPlaces, TotField " & _
        '"FROM dbo.Chart_LU WHERE (ChartTitle = @ChartTitle)"
        Me.SqlUpdateCommand2.CommandText = sql
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartTitle", System.Data.SqlDbType.VarChar, 100, "ChartTitle"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SectionHeader", System.Data.SqlDbType.VarChar, 50, "SectionHeader"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartType", System.Data.SqlDbType.VarChar, 50, "ChartType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelUS", System.Data.SqlDbType.VarChar, 50, "AxisLabelUS"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, "AxisLabelMetric"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataTable", System.Data.SqlDbType.VarChar, 50, "DataTable"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField1", System.Data.SqlDbType.VarChar, 50, "ValueField1"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend1", System.Data.SqlDbType.VarChar, 50, "Legend1"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField2", System.Data.SqlDbType.VarChar, 50, "ValueField2"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend2", System.Data.SqlDbType.VarChar, 50, "Legend2"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField3", System.Data.SqlDbType.VarChar, 50, "ValueField3"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend3", System.Data.SqlDbType.VarChar, 50, "Legend3"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField4", System.Data.SqlDbType.VarChar, 50, "ValueField4"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend4", System.Data.SqlDbType.VarChar, 50, "Legend4"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField5", System.Data.SqlDbType.VarChar, 50, "ValueField5"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend5", System.Data.SqlDbType.VarChar, 50, "Legend5"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TargetField", System.Data.SqlDbType.VarChar, 50, "TargetField"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@YTDField", System.Data.SqlDbType.VarChar, 50, "YTDField"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgField", System.Data.SqlDbType.VarChar, 50, "AvgField"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DecPlaces", System.Data.SqlDbType.TinyInt, 1, "DecPlaces"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotField", System.Data.SqlDbType.VarChar, 50, "TotField"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartTitle", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartTitle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelMetric", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelUS", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataTable", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataTable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DecPlaces", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DecPlaces", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SectionHeader", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SectionHeader", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TargetField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TargetField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_YTDField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "YTDField", System.Data.DataRowVersion.Original, Nothing))

        ' ---------------------------------------------------------------------
        ' 
        'DsChartLU1
        '
        Me.DsChartLU1.DataSetName = "dsChartLU"
        Me.DsChartLU1.Locale = New System.Globalization.CultureInfo("en-US")

        ' ---------------------------------------------------------------------
        ' sdScenario
        '
        Me.sdScenario.SelectCommand = Me.SqlSelectCommand6
        Me.sdScenario.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "GenSum", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Scenario", "Scenario")})})

        ' ---------------------------------------------------------------------
        ' SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT DISTINCT Scenario FROM dbo.GenSum"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1

        ' ---------------------------------------------------------------------
        ' DsScenario1
        '
        Me.DsScenario1.DataSetName = "dsScenario"
        Me.DsScenario1.Locale = New System.Globalization.CultureInfo("en-US")
        'CType(Me.C1CommandHolder1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsTSort1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMethodology1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPeriods1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCurrency1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsChartLU1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsScenario1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    'Protected WithEvents C1ContextMenu1 As C1.Win.C1Command.C1ContextMenu
    'Protected WithEvents C1CommandHolder1 As C1.Win.C1Command.C1CommandHolder
    'Protected WithEvents C1WebTopicBar1 As C1.Web.C1Command.C1WebTopicBar
    'Protected WithEvents C1WebTopicBarPanelBodyHelper1 As C1.Web.C1Command.C1WebTopicBarPanelBodyHelper
    'Protected WithEvents C1WebTopicBarPanelBodyHelper3 As C1.Web.C1Command.C1WebTopicBarPanelBodyHelper
    Protected WithEvents btnDownload As System.Web.UI.WebControls.Button
    Protected WithEvents btnPrint As System.Web.UI.WebControls.Button
    'Protected WithEvents C1WebTopicBarPanelBodyHelper2 As C1.Web.C1Command.C1WebTopicBarPanelBodyHelper
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents sdTSort As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents DsTSort1 As Solomon.Profile.Corporate.dsTSort

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        If IsNothing(Session("AppUser")) Then
            Response.Redirect("index.aspx")
        End If

        If Not IsPostBack Then

            sdScenario.SelectCommand.CommandText += " WHERE RefineryID IN (SELECT RefineryID FROM TSort WHERE CompanyID='" + Session("CompID").Trim.ToUpper + "')"
            sdScenario.Fill(DsScenario1)

            'ddlScenario.DataSource = DsScenario1
            'ddlScenario.DataMember = DsScenario1.Tables(0).TableName
            'ddlScenario.DataTextField = DsScenario1.Tables(0).Columns(0).ColumnName
            'ddlScenario.DataValueField = DsScenario1.Tables(0).Columns(0).ColumnName
            'ddlScenario.DataBind()

            'rbScenario.DataSource = DsScenario1
            'rbScenario.DataMember = DsScenario1.Tables(0).TableName
            'rbScenario.DataTextField = DsScenario1.Tables(0).Columns(0).ColumnName
            'rbScenario.DataValueField = DsScenario1.Tables(0).Columns(0).ColumnName
            'rbScenario.DataBind()

            sdCurrency.SelectCommand.CommandText += " WHERE RefineryID IN (SELECT RefineryID FROM TSort WHERE CompanyID='" + Session("CompID").Trim.ToUpper + "')"
            sdCurrency.Fill(DsCurrency1)


            ddlCurrency.DataSource = DsCurrency1
            ddlCurrency.DataMember = DsCurrency1.Tables(0).TableName
            ddlCurrency.DataTextField = DsCurrency1.Tables(0).Columns(0).ColumnName
            ddlCurrency.DataValueField = DsCurrency1.Tables(0).Columns(0).ColumnName
            ddlCurrency.DataBind()
            If Not ddlCurrency.Items.Contains(New ListItem("USD")) Then
                ddlCurrency.Items.Add("USD")
            End If
            ddlCurrency.Items.FindByText("USD").Selected = True
            'End If


            sdTSort.SelectCommand.CommandText += " WHERE CompanyID='" + Session("CompID").Trim.ToUpper + "' ORDER BY Location"
            sdTSort.Fill(DsTSort1)

            lbRefinery.DataSource = DsTSort1
            lbRefinery.DataMember = "TSort"
            lbRefinery.DataTextField = "Location"
            lbRefinery.DataValueField = "RefineryID"
            lbRefinery.DataBind()

            sdMethodology.Fill(DsMethodology1)
            ddlMethodology.DataSource = DsMethodology1
            ddlMethodology.DataMember = "Factors"
            ddlMethodology.DataTextField = "FactorSet"
            ddlMethodology.DataValueField = "FactorSet"
            ddlMethodology.DataBind()

            sdPeriods.SelectCommand.CommandText += " WHERE RefineryID IN (SELECT RefineryID FROM TSort WHERE CompanyID='" + Session("CompID").Trim.ToUpper + "') ORDER BY PeriodStart DESC"
            sdPeriods.Fill(DsPeriods1)
            ddlStart.DataSource = DsPeriods1
            ddlStart.DataMember = "Submissions"
            ddlStart.DataTextField = "StartDate"
            ddlStart.DataValueField = "PeriodStart"
            ddlStart.DataBind()

            If ddlStart.Items.Count > 12 Then
                ddlStart.SelectedIndex = 12
            Else
                ddlStart.SelectedIndex = ddlStart.Items.Count - 1
            End If

            ddlEnd.DataSource = DsPeriods1
            ddlEnd.DataMember = "Submissions"
            ddlEnd.DataTextField = "StartDate"
            ddlEnd.DataValueField = "PeriodStart"
            ddlEnd.DataBind()
        End If

        'Dim section As String
        'If IsPostBack() Then
        '    section = Request.Cookies("pickedsection").Value
        'End If
        'If Not IsNothing(section) And section = "REPORTS" Then
        '    ddlStart.SelectedIndex = 0
        'Else
        '    If ddlStart.Items.Count > 11 Then
        '        ddlStart.SelectedIndex = 11
        '    Else
        '        ddlStart.SelectedIndex = ddlStart.Items.Count - 1
        '    End If
        'End If

    End Sub
#End Region

    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        MyBase.OnInit(e)
        'Added to protect against viewstate hacking
        ViewStateUserKey = Session.SessionID
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim domainName As String = ""

        domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName
        domainName = System.Environment.UserDomainName


        'Load report tree and chart tree
        If Not IsPostBack Then
            rtree = Page.LoadControl("ReportTree.ascx")
            Session("RTree") = rtree

            ctree = Page.LoadControl("ChartTree.ascx")
            Session("CTree") = ctree
        Else
            rtree = Session("RTree")
            ctree = Session("CTree")
        End If

        PlaceHolder2.Controls.Add(rtree)
        PlaceHolder1.Controls.Add(ctree)

        'If rbScenario.SelectedValue = String.Empty Then
        '    rbScenario.SelectedValue = "CLIENT"
        'End If

    End Sub
    'Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
    '    'create the mail message
    '    Dim mail As New MailMessage()

    '    'set the addresses
    '    mail.From = New MailAddress("sandeep.velli@solomononline.com")
    '    mail.[To].Add("sandeep.velli@solomononline.com")

    '    'set the content
    '    mail.Subject = "This is an email"
    '    mail.Body = "this is a sample body"

    '    'set the server
    '    Dim smtp As New SmtpClient("localhost")

    '    'send the message
    '    Try
    '        smtp.Send(mail)
    '        'Response.Write("Your Email has been sent sucessfully - Thank You")
    '    Catch exc As Exception
    '        Dim section As String = exc.ToString()
    '        'Response.Write("Send failure: " & exc.ToString())
    '    End Try
    'End Sub


    Private Sub btnViewReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewReports.Click
        Dim section As String = ""
        Dim selectedNode As String = ""
        Dim c3 As Control
        Dim item As ListItem
        Dim queryString As String = ""

        'CHECK IF NOTHING WAS NOT SELECTED
        If IsNothing(Request.Cookies("pickedsection")) And IsNothing(Request.Cookies("pickednode")) Then
            ltJsScript.Text = "<script language=""javascript""> alert('You must select a chart or report before you can view results.');</script>"
            Exit Sub
        End If

        'section = Request.Cookies("pickedsection").Value
        'selectedNode = Request.Cookies("pickednode").Value
        section = Replace(Request.Cookies("pickedsection").Value, "%20", " ")
        selectedNode = Replace(Request.Cookies("pickednode").Value, "%20", " ")

        'LOAD RESULTS
        queryString += "section=" + section.ToUpper
        queryString += "&node=" + Server.UrlEncode(selectedNode)
        queryString += "&ds=" + "Actual"

        'queryString += "&scenario=" + ddlScenario.SelectedValue.Trim()
        If rbScenario.SelectedValue.Trim().ToUpper() = "CLIENT" Then
            queryString += "&scenario=" + rbScenario.SelectedValue.Trim()
        Else
            queryString += "&scenario=" + ddlMethodology.SelectedValue.Trim()
        End If

        queryString += "&startdate=" + CDate(ddlStart.SelectedValue).ToShortDateString
        queryString += "&methodology=" + ddlMethodology.SelectedValue.Trim
        queryString += "&rptOptions=" + rdoReportOptions.SelectedValue.Trim
        queryString += "&UOM=" + ddlUOM.SelectedValue.Trim
        queryString += "&currency=" + ddlCurrency.SelectedValue.Trim
        'queryString += "&refineries="

        'make list of refinery selections
        Dim refineries As String = String.Empty
        Dim selections As String = String.Empty

        For Each item In lbRefinery.Items
            If item.Selected = True AndAlso item.Value.Trim().Length() > 0 Then
                selections += item.Value.Trim() + ","
            End If
        Next
        'if no refineries selected, make list of all
        If selections.Length < 1 Then
            For Each item In lbRefinery.Items
                If item.Value.Trim().Length() > 0 Then
                    refineries += item.Value.Trim() + ","
                End If
            Next
        Else
            refineries = selections
        End If

        Session("Refineries") = refineries

        'Additional fields for charts
        If section.ToUpper = "CHARTS" Then
            'Pre-populate chart_lu
            sdChartLU.Fill(DsChartLU1)

            Dim row() As DataRow = DsChartLU1.Tables(0).Select("ChartTitle='" + Server.UrlDecode(selectedNode) + "'")
            If row.Length > 0 Then
                queryString += "&enddate=" + CDate(ddlEnd.SelectedValue).ToShortDateString
                queryString += "&table=" + row(0)("DataTable")
                queryString += "&ytd=" + row(0)("YTDField")
                queryString += "&average=" + row(0)("AvgField")
                queryString += "&column=" + Server.UrlEncode("ISNULL(" + row(0)("TotField") + ",0) AS |" + row(0)("ChartTitle") + "|")
            End If
        End If


        'IN CASE OF ERROR "NonComVisibleBaseClass was detected" "A QueryInterface call was made requesting the default IDispatch interface"
        'http://stackoverflow.com/questions/1049742/noncomvisiblebaseclass-was-detected-how-do-i-fix-this
        '"1. Navigate to Debug->Exceptions... 2. Expand "Managed Debugging Assistants" 3. Uncheck the NonComVisibleBaseClass Thrown option. 4. Click [Ok]


        'Open Pop Up window
        ltJsScript.Text = "<script language=""javascript"">var x = window.open('PopUpWin2.aspx?" + queryString + "','','height=700,width=715,scrollbars=yes,status=yes,resizeable=yes'); x.focus();</script>"

    End Sub


    Private Sub sdMethodology_RowUpdated(ByVal sender As System.Object, ByVal e As System.Data.SqlClient.SqlRowUpdatedEventArgs) Handles sdMethodology.RowUpdated

    End Sub
End Class
