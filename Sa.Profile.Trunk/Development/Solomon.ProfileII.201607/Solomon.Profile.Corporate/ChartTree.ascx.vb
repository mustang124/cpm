Imports System.Configuration

Public Class ChartTree
    Inherits System.Web.UI.UserControl
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents DsChartLU1 As Solomon.Profile.Corporate.dsChartLU
    Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents sdChart_LU As System.Data.SqlClient.SqlDataAdapter
	Dim sql As String = ""

#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitializeComponent()
        'Put user code to initialize the page here
        Me.TrackViewState()
    End Sub

   
    Private Sub InitializeComponent()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.sdChart_LU = New System.Data.SqlClient.SqlDataAdapter
        Me.DsChartLU1 = New Solomon.Profile.Corporate.dsChartLU
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.DsChartLU1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = _connectionString
        '
        'sdChart_LU
        '
        Me.sdChart_LU.DeleteCommand = Me.SqlDeleteCommand1
        Me.sdChart_LU.InsertCommand = Me.SqlInsertCommand1
        Me.sdChart_LU.SelectCommand = Me.SqlSelectCommand1
        Me.sdChart_LU.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Chart_LU", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ChartTitle", "ChartTitle"), New System.Data.Common.DataColumnMapping("SectionHeader", "SectionHeader"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("ChartType", "ChartType"), New System.Data.Common.DataColumnMapping("AxisLabelUS", "AxisLabelUS"), New System.Data.Common.DataColumnMapping("AxisLabelMetric", "AxisLabelMetric"), New System.Data.Common.DataColumnMapping("DataTable", "DataTable"), New System.Data.Common.DataColumnMapping("ValueField1", "ValueField1"), New System.Data.Common.DataColumnMapping("Legend1", "Legend1"), New System.Data.Common.DataColumnMapping("ValueField2", "ValueField2"), New System.Data.Common.DataColumnMapping("Legend2", "Legend2"), New System.Data.Common.DataColumnMapping("ValueField3", "ValueField3"), New System.Data.Common.DataColumnMapping("Legend3", "Legend3"), New System.Data.Common.DataColumnMapping("ValueField4", "ValueField4"), New System.Data.Common.DataColumnMapping("Legend4", "Legend4"), New System.Data.Common.DataColumnMapping("ValueField5", "ValueField5"), New System.Data.Common.DataColumnMapping("Legend5", "Legend5"), New System.Data.Common.DataColumnMapping("TargetField", "TargetField"), New System.Data.Common.DataColumnMapping("YTDField", "YTDField"), New System.Data.Common.DataColumnMapping("AvgField", "AvgField"), New System.Data.Common.DataColumnMapping("DecPlaces", "DecPlaces"), New System.Data.Common.DataColumnMapping("TotField", "TotField")})})
        Me.sdChart_LU.UpdateCommand = Me.SqlUpdateCommand1
        '
        'DsChartLU1
        '
        Me.DsChartLU1.DataSetName = "dsChartLU"
        Me.DsChartLU1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT ChartTitle, SectionHeader, SortKey, ChartType, AxisLabelUS, AxisLabelMetri" & _
        "c, DataTable, ValueField1, Legend1, ValueField2, Legend2, ValueField3, Legend3, " & _
        "ValueField4, Legend4, ValueField5, Legend5, TargetField, YTDField, AvgField, Dec" & _
        "Places, TotField FROM dbo.Chart_LU WHERE (SectionHeader <> 'By Process Unit') OR" & _
        "DER BY SortKey"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO dbo.Chart_LU(ChartTitle, SectionHeader, SortKey, ChartType, AxisLabel" & _
        "US, AxisLabelMetric, DataTable, ValueField1, Legend1, ValueField2, Legend2, Valu" & _
        "eField3, Legend3, ValueField4, Legend4, ValueField5, Legend5, TargetField, YTDFi" & _
        "eld, AvgField, DecPlaces, TotField) VALUES (@ChartTitle, @SectionHeader, @SortKe" & _
        "y, @ChartType, @AxisLabelUS, @AxisLabelMetric, @DataTable, @ValueField1, @Legend" & _
        "1, @ValueField2, @Legend2, @ValueField3, @Legend3, @ValueField4, @Legend4, @Valu" & _
        "eField5, @Legend5, @TargetField, @YTDField, @AvgField, @DecPlaces, @TotField); S" & _
        "ELECT ChartTitle, SectionHeader, SortKey, ChartType, AxisLabelUS, AxisLabelMetri" & _
        "c, DataTable, ValueField1, Legend1, ValueField2, Legend2, ValueField3, Legend3, " & _
        "ValueField4, Legend4, ValueField5, Legend5, TargetField, YTDField, AvgField, Dec" & _
        "Places, TotField FROM dbo.Chart_LU WHERE (ChartTitle = @ChartTitle) ORDER BY Sor" & _
        "tKey"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartTitle", System.Data.SqlDbType.VarChar, 100, "ChartTitle"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SectionHeader", System.Data.SqlDbType.VarChar, 50, "SectionHeader"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartType", System.Data.SqlDbType.VarChar, 50, "ChartType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelUS", System.Data.SqlDbType.VarChar, 50, "AxisLabelUS"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, "AxisLabelMetric"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataTable", System.Data.SqlDbType.VarChar, 50, "DataTable"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField1", System.Data.SqlDbType.VarChar, 50, "ValueField1"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend1", System.Data.SqlDbType.VarChar, 50, "Legend1"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField2", System.Data.SqlDbType.VarChar, 50, "ValueField2"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend2", System.Data.SqlDbType.VarChar, 50, "Legend2"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField3", System.Data.SqlDbType.VarChar, 50, "ValueField3"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend3", System.Data.SqlDbType.VarChar, 50, "Legend3"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField4", System.Data.SqlDbType.VarChar, 50, "ValueField4"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend4", System.Data.SqlDbType.VarChar, 50, "Legend4"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField5", System.Data.SqlDbType.VarChar, 50, "ValueField5"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend5", System.Data.SqlDbType.VarChar, 50, "Legend5"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TargetField", System.Data.SqlDbType.VarChar, 50, "TargetField"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@YTDField", System.Data.SqlDbType.VarChar, 50, "YTDField"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgField", System.Data.SqlDbType.VarChar, 50, "AvgField"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DecPlaces", System.Data.SqlDbType.TinyInt, 1, "DecPlaces"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotField", System.Data.SqlDbType.VarChar, 50, "TotField"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE dbo.Chart_LU SET ChartTitle = @ChartTitle, SectionHeader = @SectionHeader," & _
        " SortKey = @SortKey, ChartType = @ChartType, AxisLabelUS = @AxisLabelUS, AxisLab" & _
        "elMetric = @AxisLabelMetric, DataTable = @DataTable, ValueField1 = @ValueField1," & _
        " Legend1 = @Legend1, ValueField2 = @ValueField2, Legend2 = @Legend2, ValueField3" & _
        " = @ValueField3, Legend3 = @Legend3, ValueField4 = @ValueField4, Legend4 = @Lege" & _
        "nd4, ValueField5 = @ValueField5, Legend5 = @Legend5, TargetField = @TargetField," & _
        " YTDField = @YTDField, AvgField = @AvgField, DecPlaces = @DecPlaces, TotField = " & _
        "@TotField WHERE (ChartTitle = @Original_ChartTitle) AND (AvgField = @Original_Av" & _
        "gField OR @Original_AvgField IS NULL AND AvgField IS NULL) AND (AxisLabelMetric " & _
        "= @Original_AxisLabelMetric) AND (AxisLabelUS = @Original_AxisLabelUS) AND (Char" & _
        "tType = @Original_ChartType) AND (DataTable = @Original_DataTable) AND (DecPlace" & _
        "s = @Original_DecPlaces OR @Original_DecPlaces IS NULL AND DecPlaces IS NULL) AN" & _
        "D (Legend1 = @Original_Legend1 OR @Original_Legend1 IS NULL AND Legend1 IS NULL)" & _
        " AND (Legend2 = @Original_Legend2 OR @Original_Legend2 IS NULL AND Legend2 IS NU" & _
        "LL) AND (Legend3 = @Original_Legend3 OR @Original_Legend3 IS NULL AND Legend3 IS" & _
        " NULL) AND (Legend4 = @Original_Legend4 OR @Original_Legend4 IS NULL AND Legend4" & _
        " IS NULL) AND (Legend5 = @Original_Legend5 OR @Original_Legend5 IS NULL AND Lege" & _
        "nd5 IS NULL) AND (SectionHeader = @Original_SectionHeader) AND (SortKey = @Origi" & _
        "nal_SortKey) AND (TargetField = @Original_TargetField OR @Original_TargetField I" & _
        "S NULL AND TargetField IS NULL) AND (TotField = @Original_TotField OR @Original_" & _
        "TotField IS NULL AND TotField IS NULL) AND (ValueField1 = @Original_ValueField1 " & _
        "OR @Original_ValueField1 IS NULL AND ValueField1 IS NULL) AND (ValueField2 = @Or" & _
        "iginal_ValueField2 OR @Original_ValueField2 IS NULL AND ValueField2 IS NULL) AND" & _
        " (ValueField3 = @Original_ValueField3 OR @Original_ValueField3 IS NULL AND Value" & _
        "Field3 IS NULL) AND (ValueField4 = @Original_ValueField4 OR @Original_ValueField" & _
        "4 IS NULL AND ValueField4 IS NULL) AND (ValueField5 = @Original_ValueField5 OR @" & _
        "Original_ValueField5 IS NULL AND ValueField5 IS NULL) AND (YTDField = @Original_" & _
        "YTDField OR @Original_YTDField IS NULL AND YTDField IS NULL); SELECT ChartTitle," & _
        " SectionHeader, SortKey, ChartType, AxisLabelUS, AxisLabelMetric, DataTable, Val" & _
        "ueField1, Legend1, ValueField2, Legend2, ValueField3, Legend3, ValueField4, Lege" & _
        "nd4, ValueField5, Legend5, TargetField, YTDField, AvgField, DecPlaces, TotField " & _
        "FROM dbo.Chart_LU WHERE (ChartTitle = @ChartTitle) ORDER BY SortKey"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartTitle", System.Data.SqlDbType.VarChar, 100, "ChartTitle"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SectionHeader", System.Data.SqlDbType.VarChar, 50, "SectionHeader"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChartType", System.Data.SqlDbType.VarChar, 50, "ChartType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelUS", System.Data.SqlDbType.VarChar, 50, "AxisLabelUS"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, "AxisLabelMetric"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataTable", System.Data.SqlDbType.VarChar, 50, "DataTable"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField1", System.Data.SqlDbType.VarChar, 50, "ValueField1"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend1", System.Data.SqlDbType.VarChar, 50, "Legend1"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField2", System.Data.SqlDbType.VarChar, 50, "ValueField2"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend2", System.Data.SqlDbType.VarChar, 50, "Legend2"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField3", System.Data.SqlDbType.VarChar, 50, "ValueField3"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend3", System.Data.SqlDbType.VarChar, 50, "Legend3"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField4", System.Data.SqlDbType.VarChar, 50, "ValueField4"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend4", System.Data.SqlDbType.VarChar, 50, "Legend4"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueField5", System.Data.SqlDbType.VarChar, 50, "ValueField5"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Legend5", System.Data.SqlDbType.VarChar, 50, "Legend5"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TargetField", System.Data.SqlDbType.VarChar, 50, "TargetField"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@YTDField", System.Data.SqlDbType.VarChar, 50, "YTDField"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgField", System.Data.SqlDbType.VarChar, 50, "AvgField"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DecPlaces", System.Data.SqlDbType.TinyInt, 1, "DecPlaces"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotField", System.Data.SqlDbType.VarChar, 50, "TotField"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartTitle", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartTitle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelMetric", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelUS", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataTable", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataTable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DecPlaces", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DecPlaces", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SectionHeader", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SectionHeader", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TargetField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TargetField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_YTDField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "YTDField", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM dbo.Chart_LU WHERE (ChartTitle = @Original_ChartTitle) AND (AvgField " & _
        "= @Original_AvgField OR @Original_AvgField IS NULL AND AvgField IS NULL) AND (Ax" & _
        "isLabelMetric = @Original_AxisLabelMetric) AND (AxisLabelUS = @Original_AxisLabe" & _
        "lUS) AND (ChartType = @Original_ChartType) AND (DataTable = @Original_DataTable)" & _
        " AND (DecPlaces = @Original_DecPlaces OR @Original_DecPlaces IS NULL AND DecPlac" & _
        "es IS NULL) AND (Legend1 = @Original_Legend1 OR @Original_Legend1 IS NULL AND Le" & _
        "gend1 IS NULL) AND (Legend2 = @Original_Legend2 OR @Original_Legend2 IS NULL AND" & _
        " Legend2 IS NULL) AND (Legend3 = @Original_Legend3 OR @Original_Legend3 IS NULL " & _
        "AND Legend3 IS NULL) AND (Legend4 = @Original_Legend4 OR @Original_Legend4 IS NU" & _
        "LL AND Legend4 IS NULL) AND (Legend5 = @Original_Legend5 OR @Original_Legend5 IS" & _
        " NULL AND Legend5 IS NULL) AND (SectionHeader = @Original_SectionHeader) AND (So" & _
        "rtKey = @Original_SortKey) AND (TargetField = @Original_TargetField OR @Original" & _
        "_TargetField IS NULL AND TargetField IS NULL) AND (TotField = @Original_TotField" & _
        " OR @Original_TotField IS NULL AND TotField IS NULL) AND (ValueField1 = @Origina" & _
        "l_ValueField1 OR @Original_ValueField1 IS NULL AND ValueField1 IS NULL) AND (Val" & _
        "ueField2 = @Original_ValueField2 OR @Original_ValueField2 IS NULL AND ValueField" & _
        "2 IS NULL) AND (ValueField3 = @Original_ValueField3 OR @Original_ValueField3 IS " & _
        "NULL AND ValueField3 IS NULL) AND (ValueField4 = @Original_ValueField4 OR @Origi" & _
        "nal_ValueField4 IS NULL AND ValueField4 IS NULL) AND (ValueField5 = @Original_Va" & _
        "lueField5 OR @Original_ValueField5 IS NULL AND ValueField5 IS NULL) AND (YTDFiel" & _
        "d = @Original_YTDField OR @Original_YTDField IS NULL AND YTDField IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartTitle", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartTitle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelMetric", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelMetric", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AxisLabelUS", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AxisLabelUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChartType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChartType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataTable", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataTable", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DecPlaces", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DecPlaces", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Legend5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Legend5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SectionHeader", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SectionHeader", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TargetField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TargetField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotField", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField1", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField2", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField3", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField3", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField4", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField4", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ValueField5", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValueField5", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_YTDField", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "YTDField", System.Data.DataRowVersion.Original, Nothing))
        CType(Me.DsChartLU1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

   

    Public Function BuildChartMenu() As String
        sdChart_LU.Fill(DsChartLU1)

		Dim branchIdx As Integer = 0
		'Dim treeHtml As String = ""
		Dim treeNavList As String = ""
        Dim dr As DataRow
        Dim dtSec As DataTable = SelectDistinct(DsChartLU1.Tables(0).TableName, DsChartLU1.Tables(0), "SectionHeader", "SortKey")

		treeNavList = "<ul class=""nav nav-list"">"
		treeNavList += "	<li class=""tree"">"
		treeNavList += "		<ul class=""nav nav-list"">"
        For branchIdx = 0 To dtSec.Rows.Count - 1
			'treeHtml += "<div class=""trigger"" onclick=""showBranch('branch" + branchIdx.ToString + "'); swapFolder('folder" + branchIdx.ToString + "');"">"
			'treeHtml += "	<IMG id=""folder" + branchIdx.ToString + """ src=""TreeIcons\Styles\TMenu\plusik.gif"" border=0>"
			'treeHtml += "	<strong>" + dtSec.Rows(branchIdx)("SectionHeader").Trim + "</strong>"
			'treeHtml += "	</div>"
			'treeHtml += "<div class=""branch"" id='branch" + branchIdx.ToString + "'>"

			treeNavList += "			<li id=""folder" + branchIdx.ToString + """><label class=""tree-toggle nav-header"">" + dtSec.Rows(branchIdx)("SectionHeader").Trim + "</label>"
			treeNavList += "				<ul class=""nav nav-list tree"" style=""display:none;"">"

            For Each dr In DsChartLU1.Tables(0).Select("SectionHeader='" + dtSec.Rows(branchIdx)("SectionHeader") + "'", "SortKey")
                Dim nodeText As String = dr("ChartTitle").Trim
              
				'treeHtml += "<span>"
				'treeHtml += "	<IMG src=""TreeIcons\Styles\TMenu\hr_L.gif"">"
				'treeHtml += "</span>"
				'treeHtml += "<a href=""javascript:setChartValue('" + dr("ChartTitle").Trim + "');"">" + nodeText + "</a>"
				'treeHtml += "<br />"

				treeNavList += "					<li class=""chart-tree-selection""><a href=""javascript:setChartValue('" + dr("ChartTitle").Trim + "');"">" + nodeText + "</a></li>"

            Next

			'treeHtml += "</div>"

			treeNavList += "				</ul>"
			treeNavList += "			</li>"
		Next

		treeNavList += "		</ul>"
		treeNavList += "	</li>"
		treeNavList += "</ul>"

		'Return treeHtml
		Return treeNavList

    End Function

   
End Class
