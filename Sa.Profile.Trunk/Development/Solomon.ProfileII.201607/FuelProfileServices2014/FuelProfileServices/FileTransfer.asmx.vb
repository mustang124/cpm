﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Reflection
Imports System.IO
Imports System.Collections.Generic


<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class FileTransfer
    Inherits System.Web.Services.WebService

    Private _refNum As String = String.Empty
    Private _activityLog As New ActivityLog()


    'Public Sub New()
    '    DownloadTplFile("Header.tpl", "6.0.0.0", "X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=")
    'End Sub

    <WebMethod()> _
    Public Function CheckService(clientKey As String) As String
        Try 'running test here
            Dim discard As String = String.Empty
            If Not ValidateKey(clientKey, discard) Then
                Return "Invalid Key"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
        Return "Check Service: Success"
    End Function

    <WebMethod()> _
    Public Function DownloadTplFile(fileinfo As String, appVersion As String, clientKey As String) As Byte()

        Dim discard As String = String.Empty
        If Not ValidateKey(clientKey, discard) Then
            Throw New SoapException("Web Service Error: Your Client Key is invalid.", New System.Xml.XmlQualifiedName())
        End If
        Dim fileStream As System.IO.FileStream = Nothing
        Dim startFolder As DirectoryInfo = New DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory.ToString())
        Dim folderPath As String = FindVersionFolder(appVersion.Split("."), startFolder)
        If folderPath = "-1" Then
            Throw New Exception("Your app version is not valid")
        End If
        Dim filePath As String = folderPath & "\" & fileinfo '+ ".tpl"
        If Not File.Exists(filePath) Then
            _activityLog.Log("", _refNum, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "FileTransfer", "DownloadTplFile", "", "", "", "Can't find tpl file at '" + filePath + "'. ", "Error")
            Throw New SoapException("Server error, unable to find file " + fileinfo, System.Xml.XmlQualifiedName.Empty)
        End If
        Try
            fileStream = System.IO.File.Open(filePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim byteStream As Byte() = New Byte(fileStream.Length) {}
            fileStream.Read(byteStream, 0, fileStream.Length)
            fileStream.Close()
            Return byteStream
        Catch ex As Exception
            Try
                _activityLog.Log("", _refNum, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "FileTransfer", "DownloadTplFile", "", "", "", ex.Message, "Error")
            Catch
            End Try
            Return Nothing
        End Try
    End Function

    <WebMethod()> _
    Public Function UploadFile(fileName As String, bytes As Byte(), clientKey As String) As String
        Dim refnum As String = String.Empty
        If Not ValidateKey(clientKey, refnum) Then
            Throw New SoapException("Web Service Error: Your Client Key is invalid.", New System.Xml.XmlQualifiedName())
        End If
        Try
            Dim dtToday As Date = DateTime.Today
            Dim uploadRootFolder As String = ConfigurationManager.AppSettings("ClientFilesUploadFolder")
            Dim folder As String = uploadRootFolder & "\" & refnum & "\" & dtToday.Year.ToString & dtToday.Month.ToString().PadLeft(2, "0")
            If Not Directory.Exists(folder) Then
                Directory.CreateDirectory(folder)
                'Else
                '    Dim filesToDelete() As String = Directory.GetFiles(folder)
                '    For Each f As String In filesToDelete
                '        File.Delete(f)
                '    Next
            End If
            Dim filePath As String = folder + "\" + fileName
            If File.Exists(filePath) Then File.Delete(filePath)
            Dim fileStream As New FileStream(filePath, FileMode.Create)
            fileStream.Write(bytes, 0, bytes.Length)
            fileStream.Close()
        Catch ex As Exception
            Return ex.Message
        End Try
        Return String.Empty
    End Function

    Private Function ValidateKey(clientKey As String, ByRef refNum As String) As Boolean
        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(clientKey) Then
                company = Decrypt(clientKey).Split("$".ToCharArray)(0)

                locIndex = Decrypt(clientKey).Split("$".ToCharArray).Length - 2
                location = Decrypt(clientKey).Split("$".ToCharArray)(locIndex)
                Dim array As String() = Decrypt(clientKey).Split("$".ToCharArray)
                refNum = array(array.Length - 1) 'last position
                _refNum = refNum

                If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Else
                    Return False
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = clientKey.Trim)
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function FindVersionFolder(appParts As String(), directory As System.IO.DirectoryInfo) As String
        Try
            For i As Integer = 100 To 0 Step -1
                Dim ds As DirectoryInfo() = directory.GetDirectories(i.ToString() & ".*")
                For Each f1 As DirectoryInfo In ds
                    For j As Integer = 100 To 0 Step -1
                        Dim ds1 As DirectoryInfo() = directory.GetDirectories(i.ToString() & "." & j.ToString() & ".*")
                        For Each f2 As DirectoryInfo In ds1
                            Return f2.FullName ' i.ToString() & "." & j.ToString()
                        Next
                    Next
                Next
            Next
            Return "-1"
        Catch
            Return "-1"
        End Try
    End Function

End Class