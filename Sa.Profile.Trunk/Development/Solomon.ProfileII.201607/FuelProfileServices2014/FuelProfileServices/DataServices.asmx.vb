Imports System.Web.Services
Imports System.Data.SqlClient
Imports Microsoft.Web.Services2
Imports System.IO


<System.Web.Services.WebService(Namespace:="http://webservices.solomononline.com/RefineryWSDev/DataServices")> _
Public Class DataServices
    Inherits System.Web.Services.WebService
    Private db As New DBHelper
    Dim ActivityLog As New ActivityLog()
#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub


    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = ConfigurationManager.ConnectionStrings("ProfileFuelsConnectionString").ToString()

        '"packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
        '"1.7"";persist security info=True;initial catalog=ProfileFuels12;password=ProfileFu" & _
        '"els"

    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub

#End Region

    <WebMethod()> _
    Public Function CheckService() As String
        Try 'running test here
            GetLookups("XXPAC", "EXAMPLE")
        Catch ex As Exception

        End Try
        Return "True"
    End Function

    <WebMethod(Description:="Method used to determine if refinery is upgraded to 2012 ")> _
    Public Function RefineryIs2012(RefineryID As String) As Boolean

        Dim Is2012 As String = False
        Dim cn As New SqlConnection()
        cn.ConnectionString = Me.SqlConnection1.ConnectionString

        Dim ds As New DataSet

        Dim sqlstmt As String = "GetRefinery '" & RefineryID & "'"

        Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, cn)
        da.Fill(ds)
        If Not IsDBNull(ds) Then
            Is2012 = ds.Tables(0).Rows(0)(0).ToString
        End If

        cn.Close()
        If Is2012 = "0" Then
            Return False
        End If

        Return True
    End Function
#Region "Get Lookup References"
    <WebMethod(Description:="Method used to lookup tables ")> _
    Public Function GetLookups(refineryID As String, companyID As String) As DataSet

        Dim dsLookups As New DataSet
        Dim fuelsLubeCombo As Boolean
        Dim Q As String = "Enter GetLookups"
        db.RefineryID = refineryID
        Try
            Dim dsFLCombo As DataSet = db.QueryDb("EXEC DS_FLCombo '" + refineryID + "'")
            fuelsLubeCombo = dsFLCombo.Tables(0).Rows(0)(0)
            Q = "DS_FLCombo"

            'Get Currency_LU
            Dim sqlstmt As String = " EXEC DS_Currency_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(0).TableName = "Currency_LU"
            Q = "DS_Currency_LU"
            'Get UnitTargets_LU
            sqlstmt = "EXEC DS_UnitTargets_LU '" + companyID + "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(1).TableName = "UnitTargets_LU"
            Q = "DS_UnitTargets_LU"
            'Get Table2_LU
            sqlstmt = " EXEC DS_Table2_LU '" + companyID + "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(2).TableName = "Table2_LU"
            Q = "DS_Table2_LU"
            'Get Energy_LU
            sqlstmt = "EXEC DS_Energy_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(3).TableName = "Energy_LU"
            Q = "DS_Energy_LU"
            'Get Crude_LU
            sqlstmt = "EXEC DS_Crude_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(4).TableName = "Crude_LU"
            Q = "DS_Crude_LU"
            'Get  MaterialCategory_LU
            sqlstmt = "EXEC DS_MaterialCategory_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(5).TableName = "MaterialCategory_LU"
            Q = "DS_MaterialCategory_LU"
            'Get Material_LU
            sqlstmt = "EXEC GetMaterialLU '" + refineryID + "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(6).TableName = "Material_LU"
            Q = "GetMaterialLU"
            'Get ProcessGroup_LU
            If fuelsLubeCombo Then
                sqlstmt = "EXEC DS_ProcessGroupFC_LU"
            Else
                sqlstmt = "EXEC DS_ProcessGroup_LU"
            End If
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(7).TableName = "ProcessGroup_LU"
            Q = "DS_ProcessGroupFC_LU"

            'Get ProcessID_LU
            If fuelsLubeCombo Then
                sqlstmt = "EXEC DS_ProcessIDFC_LU"
            Else
                sqlstmt = "EXEC DS_ProcessID_LU"
            End If
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(8).TableName = "ProcessID_LU"
            Q = "DS_ProcessIDFC_LU"

            'Get ProcessType_LU
            sqlstmt = "EXEC DS_ProcessType_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(9).TableName = "ProcessType_LU"
            Q = "DS_ProcessType_LU"
            'Get TankType_LU
            sqlstmt = "EXEC DS_TankType_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(10).TableName = "TankType_LU"
            Q = "DS_TankType_LU"
            'Get Pers_LU
            sqlstmt = "EXEC DS_Pers_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(11).TableName = "Pers_LU"
            Q = "DS_Pers_LU"
            'Get Absence_LU
            sqlstmt = "EXEC DS_Absence_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(12).TableName = "Absence_LU"
            Q = "DS_Absence_LU"
            'Get Chart_LU
            sqlstmt = "EXEC DS_Chart_LU '" + companyID + "'"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(13).TableName = "Chart_LU"
            Q = "DS_Chart_LU"

            'Get Report_LU
            'should edit the proc to add new Custom field
            'sqlstmt = "EXEC DS_Report_LU '" + companyID + "'"
            'sqlstmt = "SELECT RTRIM(ReportCode) AS ReportCode, RTRIM(ReportName) AS ReportName, SortKey, CustomGroup FROM Report_LU WHERE CustomGroup=0 OR CustomGroup IN ( SELECT CustomGroup FROM CoCustom WHERE CompanyID= '" + companyID + "' and CustomType IN('R','TG')) ORDER BY SortKey"
            sqlstmt = "SELECT RTRIM(ReportCode) AS ReportCode, RTRIM(ReportName) AS ReportName, SortKey, CustomGroup FROM Report_LU WHERE CustomGroup=0 OR CustomGroup IN ( SELECT CustomGroup FROM CoCustom WHERE CompanyID= '" + companyID + "' and CustomType='R') ORDER BY SortKey"
            'SELECT RTRIM(ReportCode) AS ReportCode, RTRIM(ReportName) AS ReportName, SortKey FROM Report_LU WHERE CustomGroup=0 OR CustomGroup IN ( SELECT CustomGroup FROM CoCustom WHERE CompanyID=@CompanyID and CustomType='R') ORDER BY SortKey
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(14).TableName = "Report_LU"
            Q = "DS_Report_LU"

            'Get Methodology
            sqlstmt = "EXEC DS_Methodology"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(15).TableName = "Methodology"
            Q = "DS_Methodology"

            'Get Opex_LU
            sqlstmt = "EXEC DS_Opex_LU"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(16).TableName = "Opex_LU"
            Q = "DS_Opex_LU"

            'get Targeting choices
            sqlstmt = "select D.ID as PeerGroupID, D.PeerGroupDescription as PEERGROUPDESCRIPTION from targeting.PeerGroupDescriptions D inner join [targeting].[RefineryPeerGroups] P on P.PeerGroupId = D.ID WHERE P.RefineryID =  '" + refineryID + "' order by PeerGroupID ASC;"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(17).TableName = "TargetingPeerGroups"
            sqlstmt = "select D.ID as KpiId, D.KPIDescription from  targeting.KPIDescriptions D inner join targeting.RefineryKPIs K on K.KPIID = D.ID WHERE K.RefineryID = '" + refineryID + "' order by KPIDescription ASC;"
            dsLookups.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsLookups.Tables(18).TableName = "TargetingKpis"
            ActivityLog.Log("", refineryID, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "DataServices", "GetLookups", "", "", "", "", "Success")
            Return dsLookups

        Catch Ex As Exception
            ActivityLog.Log("", refineryID, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "DataServices", "GetLookups", "", "", "", Ex.Message, "Error")
            Return Nothing
        End Try

    End Function
#End Region

#Region "Get Refinery References"
    <WebMethod(Description:="Method used to reference tables ")> _
    Public Function GetReferences(refineryID As String) As DataSet

        Dim dsReferences As New DataSet
        db.RefineryID = refineryID
        Try
            'Get UnitList
            Dim sqlstmt As String = "EXEC DS_UnitList '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(0).TableName = "UnitList"

            'Get Config
            sqlstmt = "EXEC DS_Config '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(1).TableName = "Config"

            'Get MaintTA_Process
            sqlstmt = "EXEC DS_MaintTA_Process '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(2).TableName = "MaintTA_Process"

            'Get MaintTA_Other
            sqlstmt = "EXEC DS_MaintTA_Other '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(3).TableName = "MaintTA_Other"

            'Get LoadedMonths
            sqlstmt = "EXEC DS_LoadMonths '" + refineryID + "'"
            Dim tempDS As DataSet = db.QueryDb(sqlstmt)
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(4).TableName = "LoadedMonths"

            'Get Inventory Schema
            sqlstmt = "EXEC DS_InventorySchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(5).TableName = "Inventory"

            'Get ConfigRS Schema
            sqlstmt = "EXEC DS_ConfigRSSchema "
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(6).TableName = "ConfigRS"

            'Get ProcessData Schema
            sqlstmt = "EXEC DS_ProcessDataSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(7).TableName = "ProcessData"

            'Get MaintRout Schema
            sqlstmt = "EXEC DS_MaintRoutSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(8).TableName = "MaintRout"

            'Get OpEx Schema
            sqlstmt = "EXEC DS_OpexSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(9).TableName = "OpEx"

            'Get OpexAdd Schema
            sqlstmt = "EXEC DS_OpexAddSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(10).TableName = "OpexAdd"

            'Get OpexAll
            sqlstmt = "EXEC DS_OpexAllSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(11).TableName = "OpexAll"

            'Get Pers Schema
            sqlstmt = "EXEC DS_PersSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(12).TableName = "Pers"

            'Get Absence Schema
            sqlstmt = "EXEC DS_AbsenceSchema"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(13).TableName = "Absence"

            'Get Crude Schema
            sqlstmt = "EXEC DS_CrudeSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(14).TableName = "Crude"

            'Get Yield_RM Schema
            sqlstmt = "EXEC DS_YieldRMSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(15).TableName = "Yield_RM"

            'Get Yield_RMB Schema
            sqlstmt = "EXEC DS_YieldRMBSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(16).TableName = "Yield_RMB"

            'Get Yield_Prod Schema
            sqlstmt = "EXEC DS_YieldProdSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(17).TableName = "Yield_Prod"

            'Get Energy Schema
            sqlstmt = "EXEC DS_EnergySchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(18).TableName = "Energy"

            'Get MaintRoutHist Schema
            sqlstmt = "EXEC GetMaintRoutHist '" + refineryID + "', 'ACTUAL'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(19).TableName = "MaintRoutHist"

            'Get Settings Schema
            sqlstmt = "EXEC DS_SettingsSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(20).TableName = "Settings"

            'Get Electric Schema
            sqlstmt = "EXEC DS_ElectricSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(21).TableName = "Electric"

            'Get RefTargets Schema
            sqlstmt = "EXEC DS_RefTargetsSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(22).TableName = "RefTargets"

            'Get UnitTargets Schema
            sqlstmt = "EXEC DS_UnitTargetsSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(23).TableName = "UnitTargets"

            'Get UserDefined Schema
            sqlstmt = "EXEC DS_UserDefinedSchema '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(24).TableName = "UserDefined"

            'Get UnitTargetsNew
            sqlstmt = "EXEC DS_UnitTargetsNew '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(25).TableName = "UnitTargetsNew"

            'Get LoadEDCStabilizers
            sqlstmt = "EXEC DS_LoadEDCStabilizers '" + refineryID + "'"
            dsReferences.Tables.Add(db.QueryDb(sqlstmt).Tables(0).Copy)
            dsReferences.Tables(26).TableName = "EDCStabilizers"

            ActivityLog.Log("", refineryID, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "DataServices", "GetReferences", "", "", "", "", "Success")
            Return dsReferences
        Catch ex As Exception
            ActivityLog.Log("", refineryID, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "DataServices", "GetLookups", "", "", "", ex.Message, "ERROR")
            Return Nothing
        End Try


    End Function
#End Region

#Region "Get Report Data Dump"
    <WebMethod(Description:="Returns the results of a refinery")> _
    Public Function GetDataDump(ByVal ReportCode As String, ByVal ds As String, ByVal scenario As String, ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, ByVal studyYear As Integer, ByVal includeTarget As Boolean, ByVal includeYTD As Boolean, ByVal includeAVG As Boolean, refineryID As String) As DataSet

        Dim DataDump As DataSet = Nothing
        Dim ReportName As String = Nothing
        Try
            db.RefineryID = refineryID
            Dim dsResp As DataSet = db.QueryDb("EXEC DS_GetDataDump '" + ReportCode + "'")
            If dsResp.Tables(0).Rows.Count > 0 Then
                ReportName = dsResp.Tables(0).Rows(0)("ReportName")
                Dim ddmpMgr As New DataDumpManager
                DataDump = ddmpMgr.GetDataDump(ReportCode.ToUpper.Trim, ReportName, refineryID, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG)


            End If
            ActivityLog.Log(scenario, refineryID, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "DataServices", "GetDataDump", ReportName, startDate, "", "", "Success")
            Return DataDump
        Catch ex As Exception
            ActivityLog.Log("", refineryID, Context.Request.UserHostAddress, "", Environment.MachineName.ToString(), "DataServices", "GetDataDump", ReportName, startDate, "", ex.Message, "ERROR")
            Return Nothing
        End Try
    End Function

#End Region


End Class

