Option Explicit On

Friend Class SA_Energy
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tabThermalElec As System.Windows.Forms.TabControl
    Friend WithEvents pnlLU As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbTransaction As System.Windows.Forms.ComboBox
    Friend WithEvents dgvLU As System.Windows.Forms.DataGridView
    Friend WithEvents gbTools As System.Windows.Forms.GroupBox
    Friend WithEvents tpThermal As System.Windows.Forms.TabPage
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents lblTotalThermal As System.Windows.Forms.Label
    Friend WithEvents txtTTotal As System.Windows.Forms.TextBox
    Friend WithEvents gbFooter As System.Windows.Forms.GroupBox
    Friend WithEvents lblHeaderElectricity As System.Windows.Forms.Label
    Friend WithEvents lblHeaderThermal As System.Windows.Forms.Label
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents rbLinks As System.Windows.Forms.RadioButton
    Friend WithEvents rbData As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbSheets As System.Windows.Forms.ComboBox
    Friend WithEvents tbFilePath As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtETotal As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents EnergyTypeDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransferTo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnergyType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Header As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TransTypeDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SortKey_LU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Composition_LU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvThermalLinks As System.Windows.Forms.DataGridView
    Friend WithEvents dgvElecLinks As System.Windows.Forms.DataGridView
    Friend WithEvents dgvThermal As System.Windows.Forms.DataGridView
    Friend WithEvents dgvElec As System.Windows.Forms.DataGridView
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents tpElec As System.Windows.Forms.TabPage
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_Energy))
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmbTransaction = New System.Windows.Forms.ComboBox
        Me.pnlLU = New System.Windows.Forms.Panel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.dgvLU = New System.Windows.Forms.DataGridView
        Me.EnergyTypeDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TransType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TransferTo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EnergyType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Header = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TransTypeDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SortKey_LU = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Composition_LU = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tabThermalElec = New System.Windows.Forms.TabControl
        Me.tpThermal = New System.Windows.Forms.TabPage
        Me.dgvThermal = New System.Windows.Forms.DataGridView
        Me.dgvThermalLinks = New System.Windows.Forms.DataGridView
        Me.lblHeaderThermal = New System.Windows.Forms.Label
        Me.gbFooter = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblTotalThermal = New System.Windows.Forms.Label
        Me.txtTTotal = New System.Windows.Forms.TextBox
        Me.tpElec = New System.Windows.Forms.TabPage
        Me.dgvElec = New System.Windows.Forms.DataGridView
        Me.dgvElecLinks = New System.Windows.Forms.DataGridView
        Me.lblHeaderElectricity = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtETotal = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.gbTools = New System.Windows.Forms.GroupBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.rbLinks = New System.Windows.Forms.RadioButton
        Me.rbData = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.cbSheets = New System.Windows.Forms.ComboBox
        Me.tbFilePath = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlData = New System.Windows.Forms.Panel
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.pnlLU.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvLU, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabThermalElec.SuspendLayout()
        Me.tpThermal.SuspendLayout()
        CType(Me.dgvThermal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvThermalLinks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFooter.SuspendLayout()
        Me.tpElec.SuspendLayout()
        CType(Me.dgvElec, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvElecLinks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.pnlTools.SuspendLayout()
        Me.gbTools.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(6, 0)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(722, 36)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Filter by Transaction Type"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbTransaction
        '
        Me.cmbTransaction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTransaction.FormattingEnabled = True
        Me.cmbTransaction.Location = New System.Drawing.Point(163, 9)
        Me.cmbTransaction.Name = "cmbTransaction"
        Me.cmbTransaction.Size = New System.Drawing.Size(280, 21)
        Me.cmbTransaction.TabIndex = 0
        '
        'pnlLU
        '
        Me.pnlLU.BackColor = System.Drawing.SystemColors.Control
        Me.pnlLU.Controls.Add(Me.TabControl1)
        Me.pnlLU.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLU.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLU.Location = New System.Drawing.Point(4, 40)
        Me.pnlLU.Name = "pnlLU"
        Me.pnlLU.Padding = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.pnlLU.Size = New System.Drawing.Size(742, 556)
        Me.pnlLU.TabIndex = 948
        Me.pnlLU.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 6)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(742, 550)
        Me.TabControl1.TabIndex = 13
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnCancel)
        Me.TabPage2.Controls.Add(Me.btnOK)
        Me.TabPage2.Controls.Add(Me.dgvLU)
        Me.TabPage2.Controls.Add(Me.cmbTransaction)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.TabPage2.Size = New System.Drawing.Size(734, 524)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Select an Energy Type"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.Image = Global.Solomon.Profile.My.Resources.Resources.error_1
        Me.btnCancel.Location = New System.Drawing.Point(664, 4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(64, 28)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.Transparent
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.Image = Global.Solomon.Profile.My.Resources.Resources.check
        Me.btnOK.Location = New System.Drawing.Point(594, 4)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(64, 28)
        Me.btnOK.TabIndex = 8
        Me.btnOK.Text = "OK"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'dgvLU
        '
        Me.dgvLU.AllowUserToAddRows = False
        Me.dgvLU.AllowUserToDeleteRows = False
        Me.dgvLU.AllowUserToResizeColumns = False
        Me.dgvLU.AllowUserToResizeRows = False
        Me.dgvLU.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLU.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLU.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EnergyTypeDesc, Me.TransType, Me.TransferTo, Me.EnergyType, Me.Header, Me.TransTypeDesc, Me.SortKey_LU, Me.Composition_LU})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLU.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvLU.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLU.Location = New System.Drawing.Point(6, 36)
        Me.dgvLU.MultiSelect = False
        Me.dgvLU.Name = "dgvLU"
        Me.dgvLU.RowHeadersWidth = 25
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvLU.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvLU.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLU.Size = New System.Drawing.Size(722, 482)
        Me.dgvLU.TabIndex = 6
        '
        'EnergyTypeDesc
        '
        Me.EnergyTypeDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.EnergyTypeDesc.DataPropertyName = "EnergyTypeDesc"
        Me.EnergyTypeDesc.FillWeight = 250.0!
        Me.EnergyTypeDesc.HeaderText = "Energy Type"
        Me.EnergyTypeDesc.Name = "EnergyTypeDesc"
        Me.EnergyTypeDesc.ReadOnly = True
        Me.EnergyTypeDesc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TransType
        '
        Me.TransType.DataPropertyName = "TransType"
        Me.TransType.FillWeight = 150.0!
        Me.TransType.HeaderText = "Transaction Type Abbreviation"
        Me.TransType.Name = "TransType"
        Me.TransType.ReadOnly = True
        Me.TransType.Visible = False
        Me.TransType.Width = 150
        '
        'TransferTo
        '
        Me.TransferTo.DataPropertyName = "TransferTo"
        Me.TransferTo.FillWeight = 125.0!
        Me.TransferTo.HeaderText = "Transfer To"
        Me.TransferTo.Name = "TransferTo"
        Me.TransferTo.ReadOnly = True
        Me.TransferTo.Visible = False
        Me.TransferTo.Width = 125
        '
        'EnergyType
        '
        Me.EnergyType.DataPropertyName = "EnergyType"
        Me.EnergyType.FillWeight = 125.0!
        Me.EnergyType.HeaderText = "Energy Type Abbreviation"
        Me.EnergyType.Name = "EnergyType"
        Me.EnergyType.ReadOnly = True
        Me.EnergyType.Visible = False
        Me.EnergyType.Width = 125
        '
        'Header
        '
        Me.Header.DataPropertyName = "Header"
        Me.Header.HeaderText = "Header"
        Me.Header.Name = "Header"
        Me.Header.ReadOnly = True
        Me.Header.Visible = False
        '
        'TransTypeDesc
        '
        Me.TransTypeDesc.DataPropertyName = "TransTypeDesc"
        Me.TransTypeDesc.HeaderText = "Transaction"
        Me.TransTypeDesc.Name = "TransTypeDesc"
        Me.TransTypeDesc.ReadOnly = True
        Me.TransTypeDesc.Visible = False
        '
        'SortKey_LU
        '
        Me.SortKey_LU.DataPropertyName = "SortKey"
        Me.SortKey_LU.HeaderText = "SortKey_LU"
        Me.SortKey_LU.Name = "SortKey_LU"
        Me.SortKey_LU.ReadOnly = True
        Me.SortKey_LU.Visible = False
        '
        'Composition_LU
        '
        Me.Composition_LU.DataPropertyName = "Composition"
        Me.Composition_LU.HeaderText = "Composition"
        Me.Composition_LU.Name = "Composition_LU"
        Me.Composition_LU.ReadOnly = True
        Me.Composition_LU.Visible = False
        '
        'tabThermalElec
        '
        Me.tabThermalElec.Controls.Add(Me.tpThermal)
        Me.tabThermalElec.Controls.Add(Me.tpElec)
        Me.tabThermalElec.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabThermalElec.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabThermalElec.ItemSize = New System.Drawing.Size(140, 18)
        Me.tabThermalElec.Location = New System.Drawing.Point(0, 105)
        Me.tabThermalElec.Name = "tabThermalElec"
        Me.tabThermalElec.SelectedIndex = 0
        Me.tabThermalElec.Size = New System.Drawing.Size(742, 451)
        Me.tabThermalElec.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tabThermalElec.TabIndex = 0
        '
        'tpThermal
        '
        Me.tpThermal.BackColor = System.Drawing.Color.Transparent
        Me.tpThermal.Controls.Add(Me.dgvThermal)
        Me.tpThermal.Controls.Add(Me.dgvThermalLinks)
        Me.tpThermal.Controls.Add(Me.lblHeaderThermal)
        Me.tpThermal.Controls.Add(Me.gbFooter)
        Me.tpThermal.Location = New System.Drawing.Point(4, 22)
        Me.tpThermal.Name = "tpThermal"
        Me.tpThermal.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.tpThermal.Size = New System.Drawing.Size(734, 425)
        Me.tpThermal.TabIndex = 0
        Me.tpThermal.Text = "Table 16 - Thermal"
        Me.tpThermal.UseVisualStyleBackColor = True
        '
        'dgvThermal
        '
        Me.dgvThermal.AllowUserToAddRows = False
        Me.dgvThermal.AllowUserToResizeColumns = True
        Me.dgvThermal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvThermal.ColumnHeadersHeight = 36
        Me.dgvThermal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvThermal.Location = New System.Drawing.Point(6, 36)
        Me.dgvThermal.MultiSelect = False
        Me.dgvThermal.Name = "dgvThermal"
        Me.dgvThermal.Size = New System.Drawing.Size(722, 305)
        Me.dgvThermal.TabIndex = 964
        '
        'dgvThermalLinks
        '
        Me.dgvThermalLinks.AllowUserToAddRows = False
        Me.dgvThermalLinks.AllowUserToDeleteRows = False
        Me.dgvThermalLinks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvThermalLinks.ColumnHeadersHeight = 36
        Me.dgvThermalLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvThermalLinks.Location = New System.Drawing.Point(6, 36)
        Me.dgvThermalLinks.MultiSelect = False
        Me.dgvThermalLinks.Name = "dgvThermalLinks"
        Me.dgvThermalLinks.Size = New System.Drawing.Size(722, 305)
        Me.dgvThermalLinks.TabIndex = 963
        '
        'lblHeaderThermal
        '
        Me.lblHeaderThermal.BackColor = System.Drawing.Color.Transparent
        Me.lblHeaderThermal.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeaderThermal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderThermal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblHeaderThermal.Location = New System.Drawing.Point(6, 0)
        Me.lblHeaderThermal.Name = "lblHeaderThermal"
        Me.lblHeaderThermal.Size = New System.Drawing.Size(722, 36)
        Me.lblHeaderThermal.TabIndex = 962
        Me.lblHeaderThermal.Text = "Thermal Energy and Composition:   DATA"
        Me.lblHeaderThermal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFooter
        '
        Me.gbFooter.BackColor = System.Drawing.Color.Transparent
        Me.gbFooter.Controls.Add(Me.Label9)
        Me.gbFooter.Controls.Add(Me.Label10)
        Me.gbFooter.Controls.Add(Me.lblTotalThermal)
        Me.gbFooter.Controls.Add(Me.txtTTotal)
        Me.gbFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbFooter.Location = New System.Drawing.Point(6, 341)
        Me.gbFooter.Name = "gbFooter"
        Me.gbFooter.Size = New System.Drawing.Size(722, 78)
        Me.gbFooter.TabIndex = 958
        Me.gbFooter.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(6, 12)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 13)
        Me.Label9.TabIndex = 960
        Me.Label9.Text = "Helpful hint:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(6, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(154, 39)
        Me.Label10.TabIndex = 959
        Me.Label10.Text = "To delete a row, simply select" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "the entire row and click delete " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "on your keyboar" & _
            "d."
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalThermal
        '
        Me.lblTotalThermal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotalThermal.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalThermal.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.lblTotalThermal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblTotalThermal.Location = New System.Drawing.Point(420, 17)
        Me.lblTotalThermal.Name = "lblTotalThermal"
        Me.lblTotalThermal.Size = New System.Drawing.Size(176, 21)
        Me.lblTotalThermal.TabIndex = 852
        Me.lblTotalThermal.Text = "Total Thermal Energy, MBtu"
        Me.lblTotalThermal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTTotal
        '
        Me.txtTTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTTotal.BackColor = System.Drawing.SystemColors.Control
        Me.txtTTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTTotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtTTotal.Location = New System.Drawing.Point(606, 17)
        Me.txtTTotal.Name = "txtTTotal"
        Me.txtTTotal.ReadOnly = True
        Me.txtTTotal.Size = New System.Drawing.Size(110, 21)
        Me.txtTTotal.TabIndex = 2
        Me.txtTTotal.TabStop = False
        Me.txtTTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'tpElec
        '
        Me.tpElec.Controls.Add(Me.dgvElec)
        Me.tpElec.Controls.Add(Me.dgvElecLinks)
        Me.tpElec.Controls.Add(Me.lblHeaderElectricity)
        Me.tpElec.Controls.Add(Me.GroupBox1)
        Me.tpElec.Location = New System.Drawing.Point(4, 22)
        Me.tpElec.Name = "tpElec"
        Me.tpElec.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.tpElec.Size = New System.Drawing.Size(734, 425)
        Me.tpElec.TabIndex = 1
        Me.tpElec.Text = "Table 16 - Electricity"
        Me.tpElec.UseVisualStyleBackColor = True
        '
        'dgvElec
        '
        Me.dgvElec.AllowUserToAddRows = False
        Me.dgvElec.AllowUserToResizeColumns = True
        Me.dgvElec.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvElec.ColumnHeadersHeight = 36
        Me.dgvElec.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvElec.Location = New System.Drawing.Point(6, 36)
        Me.dgvElec.MultiSelect = False
        Me.dgvElec.Name = "dgvElec"
        Me.dgvElec.Size = New System.Drawing.Size(722, 305)
        Me.dgvElec.TabIndex = 964
        '
        'dgvElecLinks
        '
        Me.dgvElecLinks.AllowUserToAddRows = False
        Me.dgvElecLinks.AllowUserToDeleteRows = False
        Me.dgvElecLinks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvElecLinks.ColumnHeadersHeight = 36
        Me.dgvElecLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvElecLinks.Location = New System.Drawing.Point(6, 36)
        Me.dgvElecLinks.MultiSelect = False
        Me.dgvElecLinks.Name = "dgvElecLinks"
        Me.dgvElecLinks.Size = New System.Drawing.Size(722, 305)
        Me.dgvElecLinks.TabIndex = 963
        '
        'lblHeaderElectricity
        '
        Me.lblHeaderElectricity.BackColor = System.Drawing.Color.Transparent
        Me.lblHeaderElectricity.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeaderElectricity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderElectricity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblHeaderElectricity.Location = New System.Drawing.Point(6, 0)
        Me.lblHeaderElectricity.Name = "lblHeaderElectricity"
        Me.lblHeaderElectricity.Size = New System.Drawing.Size(722, 36)
        Me.lblHeaderElectricity.TabIndex = 961
        Me.lblHeaderElectricity.Text = "Electricity:   DATA"
        Me.lblHeaderElectricity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtETotal)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox1.Location = New System.Drawing.Point(6, 341)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(722, 78)
        Me.GroupBox1.TabIndex = 962
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(6, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 13)
        Me.Label3.TabIndex = 958
        Me.Label3.Text = "Helpful hint:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(6, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(154, 39)
        Me.Label8.TabIndex = 957
        Me.Label8.Text = "To delete a row, simply select" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "the entire row and click delete " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "on your keyboar" & _
            "d."
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtETotal
        '
        Me.txtETotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtETotal.BackColor = System.Drawing.SystemColors.Control
        Me.txtETotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtETotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtETotal.Location = New System.Drawing.Point(606, 17)
        Me.txtETotal.Name = "txtETotal"
        Me.txtETotal.ReadOnly = True
        Me.txtETotal.Size = New System.Drawing.Size(110, 21)
        Me.txtETotal.TabIndex = 3
        Me.txtETotal.TabStop = False
        Me.txtETotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(420, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(176, 21)
        Me.Label7.TabIndex = 956
        Me.Label7.Text = "Total Electricity, MWh"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTools
        '
        Me.pnlTools.Controls.Add(Me.gbTools)
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(0, 0)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 105)
        Me.pnlTools.TabIndex = 0
        '
        'gbTools
        '
        Me.gbTools.Controls.Add(Me.btnBrowse)
        Me.gbTools.Controls.Add(Me.chkComplete)
        Me.gbTools.Controls.Add(Me.rbLinks)
        Me.gbTools.Controls.Add(Me.rbData)
        Me.gbTools.Controls.Add(Me.Label1)
        Me.gbTools.Controls.Add(Me.Label6)
        Me.gbTools.Controls.Add(Me.cbSheets)
        Me.gbTools.Controls.Add(Me.tbFilePath)
        Me.gbTools.Controls.Add(Me.Label4)
        Me.gbTools.Controls.Add(Me.btnEdit)
        Me.gbTools.Controls.Add(Me.btnAdd)
        Me.gbTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTools.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbTools.Location = New System.Drawing.Point(0, 0)
        Me.gbTools.Name = "gbTools"
        Me.gbTools.Size = New System.Drawing.Size(742, 99)
        Me.gbTools.TabIndex = 0
        Me.gbTools.TabStop = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(711, 15)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(21, 21)
        Me.btnBrowse.TabIndex = 965
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for bridge workbook")
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'chkComplete
        '
        Me.chkComplete.AutoSize = True
        Me.chkComplete.BackColor = System.Drawing.Color.Transparent
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkComplete.Location = New System.Drawing.Point(9, 49)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(136, 17)
        Me.chkComplete.TabIndex = 964
        Me.chkComplete.Text = "Check when completed"
        Me.chkComplete.UseVisualStyleBackColor = False
        '
        'rbLinks
        '
        Me.rbLinks.AutoSize = True
        Me.rbLinks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLinks.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbLinks.Location = New System.Drawing.Point(92, 70)
        Me.rbLinks.Name = "rbLinks"
        Me.rbLinks.Size = New System.Drawing.Size(53, 17)
        Me.rbLinks.TabIndex = 957
        Me.rbLinks.Text = "LINKS"
        Me.rbLinks.UseVisualStyleBackColor = True
        '
        'rbData
        '
        Me.rbData.AutoSize = True
        Me.rbData.Checked = True
        Me.rbData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbData.Location = New System.Drawing.Point(41, 70)
        Me.rbData.Name = "rbData"
        Me.rbData.Size = New System.Drawing.Size(52, 17)
        Me.rbData.TabIndex = 956
        Me.rbData.TabStop = True
        Me.rbData.Text = "DATA"
        Me.rbData.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(389, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 962
        Me.Label1.Text = "Workbook"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(6, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 13)
        Me.Label6.TabIndex = 958
        Me.Label6.Text = "View"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbSheets
        '
        Me.cbSheets.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets.FormattingEnabled = True
        Me.cbSheets.Location = New System.Drawing.Point(464, 42)
        Me.cbSheets.Name = "cbSheets"
        Me.cbSheets.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets.TabIndex = 961
        '
        'tbFilePath
        '
        Me.tbFilePath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFilePath.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbFilePath.Location = New System.Drawing.Point(464, 15)
        Me.tbFilePath.Name = "tbFilePath"
        Me.tbFilePath.ReadOnly = True
        Me.tbFilePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbFilePath.Size = New System.Drawing.Size(241, 21)
        Me.tbFilePath.TabIndex = 959
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(389, 45)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 963
        Me.Label4.Text = "Worksheet"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.Image = Global.Solomon.Profile.My.Resources.Resources.log
        Me.btnEdit.Location = New System.Drawing.Point(76, 15)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(64, 28)
        Me.btnEdit.TabIndex = 953
        Me.btnEdit.TabStop = False
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnEdit, "Edit the current table entry")
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.Image = Global.Solomon.Profile.My.Resources.Resources.add
        Me.btnAdd.Location = New System.Drawing.Point(6, 15)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(64, 28)
        Me.btnAdd.TabIndex = 954
        Me.btnAdd.TabStop = False
        Me.btnAdd.Text = "Add"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnAdd, "Create a new table entry")
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon.Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon.Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(500, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 2
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Update input form with linked values")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.tabThermalElec)
        Me.pnlData.Controls.Add(Me.pnlTools)
        Me.pnlData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlData.Location = New System.Drawing.Point(4, 40)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(742, 556)
        Me.pnlData.TabIndex = 959
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1034
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(500, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Energy"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_Energy
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.pnlData)
        Me.Controls.Add(Me.pnlLU)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Name = "SA_Energy"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.pnlLU.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvLU, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabThermalElec.ResumeLayout(False)
        Me.tpThermal.ResumeLayout(False)
        CType(Me.dgvThermal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvThermalLinks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFooter.ResumeLayout(False)
        Me.gbFooter.PerformLayout()
        Me.tpElec.ResumeLayout(False)
        CType(Me.dgvElec, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvElecLinks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnlTools.ResumeLayout(False)
        Me.gbTools.ResumeLayout(False)
        Me.gbTools.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private dv_LU As New DataView
    Private IsLoading As Boolean = True
    Private EnergyRights As Boolean
    Friend EnergyNotSaved As Boolean
    Private MyParent As Main
    'Private SelectedCategory As String
    Private IsEditing As Boolean
    Private dvWB As New DataView

    Private dvThermal As New DataView
    Private dvElec As New DataView
    Private dvThermalLinks As New DataView
    Private dvElecLinks As New DataView

    Private Sub SetUpTransactions()
        cmbTransaction.Items.Clear()
        If tabThermalElec.SelectedTab Is tpThermal Then
            cmbTransaction.Items.Add("Purchased Thermal Energy from Others")
            cmbTransaction.Items.Add("Produced Thermal Energy")
            cmbTransaction.Items.Add("Thermal Energy Distributed to Refinery from Affiliates")
            cmbTransaction.Items.Add("Thermal Energy Distributed to Affiliates from Refinery")
            cmbTransaction.Items.Add("Thermal Energy Sold to Others")
        Else
            cmbTransaction.Items.Add("Purchased Electricity from Others")
            cmbTransaction.Items.Add("Produced Electricity")
            cmbTransaction.Items.Add("Electricity Distributed to Refinery from Affiliates")
            cmbTransaction.Items.Add("Electricity Distributed to Affiliates from Refinery")
            cmbTransaction.Items.Add("Electricity Sold to Others")
        End If
        cmbTransaction.SelectedIndex = 0
    End Sub

    Friend Sub SetDataSet()
        IsLoading = True
        MyParent = CType(Me.ParentForm, Main)

        dvThermal.Table = MyParent.dsEnergy.Tables("Energy")
        dvThermal.Sort = "SortKey ASC"
        dgvThermal.DataSource = dvThermal

        dvElec.Table = MyParent.dsEnergy.Tables("Electric")
        dvElec.Sort = "SortKey ASC"
        dgvElec.DataSource = dvElec

        dv_LU.Table = MyParent.MyParent.ds_Ref.Tables("Energy_LU")
        dv_LU.Sort = "SortKey ASC"
        dgvLU.DataSource = dv_LU

        dvThermalLinks.Table = MyParent.dsMappings.Tables("Energy")
        dvThermalLinks.Sort = "SortKey ASC"
        dgvThermalLinks.DataSource = dvThermalLinks

        dvElecLinks.Table = MyParent.dsMappings.Tables("Electric")
        dvElecLinks.Sort = "SortKey ASC"
        dgvElecLinks.DataSource = dvElecLinks

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Energy'")
        chkComplete.Checked = CBool(row(0)!Checked)

        Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = 'Energy'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            If Not System.IO.File.Exists(dr(0).Item(tbFilePath.Name).ToString) Then
                'Throw New Exception("Mapping file problem: Cannot find file at " & dr(0).Item(tbFilePath.Name).ToString)
            End If
            tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
            cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
        End If

        PopulateSummary()
        IsLoading = False
    End Sub

    Friend Sub SetCurrencyAndUOM(ByVal Curr As String, ByVal UOM As String)

        Dim idxRptSource As Integer
        Dim idxRptPriceLocalTherm As Integer
        Dim idxRptPriceLocalElec As Integer

        For Each c As DataGridViewColumn In dgvThermal.Columns
            If (c.DataPropertyName.ToString = "RptSource") Then idxRptSource = c.Index
            If (c.DataPropertyName.ToString = "RptPriceLocal") Then idxRptPriceLocalTherm = c.Index
        Next c

        For Each c As DataGridViewColumn In dgvElec.Columns
            If (c.DataPropertyName.ToString = "RptPriceLocal") Then idxRptPriceLocalElec = c.Index
        Next c

        If UOM = "US Units" Then
            lblTotalThermal.Text = "Estimated Thermal Energy, MBtu"
            dgvThermal.Columns(idxRptSource).HeaderText = "Source, MBtu"
            dgvThermal.Columns(idxRptPriceLocalTherm).HeaderText = Curr & "/MBtu"
            dgvElec.Columns(idxRptPriceLocalElec).HeaderText = Curr & "/100 kWh"
        Else
            lblTotalThermal.Text = "Estimated Thermal Energy, GJ"
            dgvThermal.Columns(idxRptSource).HeaderText = "Source, GJ"
            dgvThermal.Columns(idxRptPriceLocalTherm).HeaderText = Curr & "/GJ"
            dgvElec.Columns(idxRptPriceLocalElec).HeaderText = Curr & "/100 kWh"
        End If
    End Sub

    Private Sub PopulateSummary()
        Dim adds, subtracts, tmpDBL As Double
        Dim row As DataRow
        Dim TransType, TransferTo As String
        For Each row In MyParent.dsEnergy.Tables("Energy").Rows
            Try
                tmpDBL = Math.Abs(CDbl(row!RptSource))
            Catch ex As Exception
                tmpDBL = 0
            End Try
            If tmpDBL > 0 Then
                TransType = row!TransType.ToString
                TransferTo = row!TransferTo.ToString
                If TransType = "TRA" And (TransferTo = "AFF" Or TransferTo = "OTH") Then
                    subtracts = subtracts + tmpDBL
                Else
                    adds = adds + tmpDBL
                End If
            End If

        Next
        tmpDBL = adds - subtracts
        txtTTotal.Text = FormatDoubles(tmpDBL, 0)

        tmpDBL = 0
        subtracts = 0
        adds = 0

        For Each row In MyParent.dsEnergy.Tables("Electric").Rows
            Try
                tmpDBL = Math.Abs(CDbl(row!RptMWH))
            Catch ex As Exception
                tmpDBL = 0
            End Try
            If tmpDBL > 0 Then
                TransType = row!TransType.ToString
                TransferTo = row!TransferTo.ToString
                If TransType = "TRA" And (TransferTo = "AFF" Or TransferTo = "OTH") Then
                    subtracts = subtracts + tmpDBL
                Else
                    adds = adds + tmpDBL
                End If
            End If
        Next
        tmpDBL = adds - subtracts
        txtETotal.Text = FormatDoubles(tmpDBL, 0)

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        Dim result As DialogResult

        Select Case tabThermalElec.SelectedTab.Name.ToString()
            Case "tpThermal"
                If rbLinks.Checked Then
                    result = ProfileMsgBox("YN", "CLEAR", "thermal energy mappings")
                    If result = DialogResult.Yes Then
                        For Each row As DataGridViewRow In Me.dgvThermalLinks.Rows
                            row.Cells("RptSource").Value = DBNull.Value
                            row.Cells("RptPriceLocal").Value = DBNull.Value
                            row.Cells("Hydrogen").Value = DBNull.Value
                            row.Cells("Methane").Value = DBNull.Value
                            row.Cells("Ethane").Value = DBNull.Value
                            row.Cells("Ethylene").Value = DBNull.Value
                            row.Cells("Propane").Value = DBNull.Value
                            row.Cells("Propylene").Value = DBNull.Value
                            row.Cells("Butane").Value = DBNull.Value
                            row.Cells("Isobutane").Value = DBNull.Value
                            row.Cells("Butylenes").Value = DBNull.Value
                            row.Cells("C5Plus").Value = DBNull.Value
                            row.Cells("H2S").Value = DBNull.Value
                            row.Cells("CO").Value = DBNull.Value
                            row.Cells("NH3").Value = DBNull.Value
                            row.Cells("SO2").Value = DBNull.Value
                            row.Cells("CO2").Value = DBNull.Value
                            row.Cells("N2").Value = DBNull.Value
                        Next row
                    End If
                Else
                    result = ProfileMsgBox("YN", "CLEAR", "thermal energy table")
                    If result = DialogResult.Yes Then
                        For Each row As DataGridViewRow In Me.dgvThermal.Rows
                            row.Cells("RptSource").Value = DBNull.Value
                            row.Cells("RptPriceLocal").Value = DBNull.Value
                            row.Cells("Hydrogen").Value = DBNull.Value
                            row.Cells("Methane").Value = DBNull.Value
                            row.Cells("Ethane").Value = DBNull.Value
                            row.Cells("Ethylene").Value = DBNull.Value
                            row.Cells("Propane").Value = DBNull.Value
                            row.Cells("Propylene").Value = DBNull.Value
                            row.Cells("Butane").Value = DBNull.Value
                            row.Cells("Isobutane").Value = DBNull.Value
                            row.Cells("Butylenes").Value = DBNull.Value
                            row.Cells("C5Plus").Value = DBNull.Value
                            row.Cells("H2S").Value = DBNull.Value
                            row.Cells("CO").Value = DBNull.Value
                            row.Cells("NH3").Value = DBNull.Value
                            row.Cells("SO2").Value = DBNull.Value
                            row.Cells("CO2").Value = DBNull.Value
                            row.Cells("N2").Value = DBNull.Value
                        Next row
                    End If
                End If
            Case "tpElec"
                If rbLinks.Checked Then
                    result = ProfileMsgBox("YN", "CLEAR", "electrical energy mappings")
                    If result = DialogResult.Yes Then
                        For Each row As DataGridViewRow In Me.dgvElecLinks.Rows
                            row.Cells("RptMWH").Value = DBNull.Value
                            row.Cells("PriceLocal").Value = DBNull.Value
                            row.Cells("RptGenEff").Value = DBNull.Value
                        Next row
                    End If
                Else
                    result = ProfileMsgBox("YN", "CLEAR", "electrical energy table")
                    If result = DialogResult.Yes Then
                        For Each row As DataGridViewRow In Me.dgvElec.Rows
                            row.Cells("RptMWH").Value = DBNull.Value
                            row.Cells("PriceLocal").Value = DBNull.Value
                            row.Cells("RptGenEff").Value = DBNull.Value
                        Next row
                    End If
                End If

        End Select

        If result = DialogResult.Yes Then
            ChangesMade(btnSave, chkComplete, EnergyNotSaved)
            PopulateSummary()
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
    End Sub

    Private Sub cmbTransaction_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTransaction.SelectedIndexChanged
        Dim tmpstr As String = ""
        If tabThermalElec.SelectedTab Is tpThermal Then tmpstr = "SortKey < 100 AND " Else tmpstr = "SortKey > 100 AND "
        Select Case cmbTransaction.SelectedIndex
            Case 0 : tmpstr = tmpstr & "TransType = 'PUR'"
            Case 1 : tmpstr = tmpstr & "TransType = 'PRO'"
            Case 2 : tmpstr = tmpstr & "TransType = 'DST' AND TransferTo = 'REF'"
            Case 3 : tmpstr = tmpstr & "TransType = 'DST' AND TransferTo = 'AFF'"
            Case 4 : tmpstr = tmpstr & "TransType = 'SOL'"
        End Select
        dv_LU.RowFilter = ""
        dv_LU.RowFilter = tmpstr
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ClickOK(sender, e)
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
    End Sub

    Private Sub ClickOK(ByVal sender As System.Object, ByVal e As System.EventArgs)
        On Error GoTo NothingSelected

        Dim TransType, TransferTo, EnergyType, strEnergyDesc, strTransDesc As String
        Dim SortKey, Composition As Integer

        Dim idxTransTypeLU As Integer
        Dim idxTransferToLU As Integer
        Dim idxEnergyTypeLU As Integer
        Dim idxSortKeyLU As Integer
        Dim idxCompositionLU As Integer
        Dim idxEnergyTypeDescLU As Integer
        Dim idxTransTypeDescLU As Integer

        For Each c As DataGridViewColumn In dgvLU.Columns
            Debug.Print(c.DataPropertyName.ToString)
            If (c.DataPropertyName.ToString = "TransType") Then idxTransTypeLU = c.Index
            If (c.DataPropertyName.ToString = "TransferTo") Then idxTransferToLU = c.Index
            If (c.DataPropertyName.ToString = "EnergyType") Then idxEnergyTypeLU = c.Index
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKeyLU = c.Index
            If (c.DataPropertyName.ToString = "Composition") Then idxCompositionLU = c.Index
            If (c.DataPropertyName.ToString = "EnergyTypeDesc") Then idxEnergyTypeDescLU = c.Index
            If (c.DataPropertyName.ToString = "TransTypeDesc") Then idxTransTypeDescLU = c.Index
        Next c

        TransType = Trim(dgvLU.CurrentRow.Cells(idxTransTypeLU).Value.ToString)
        TransferTo = Trim(dgvLU.CurrentRow.Cells(idxTransferToLU).Value.ToString)
        EnergyType = Trim(dgvLU.CurrentRow.Cells(idxEnergyTypeLU).Value.ToString)
        SortKey = CInt(Trim(dgvLU.CurrentRow.Cells(idxSortKeyLU).Value.ToString))
        Composition = CInt(Trim(dgvLU.CurrentRow.Cells(idxCompositionLU).Value.ToString))
        strEnergyDesc = Trim(dgvLU.CurrentRow.Cells(idxEnergyTypeDescLU).Value.ToString)
        strTransDesc = Trim(dgvLU.CurrentRow.Cells(idxTransTypeDescLU).Value.ToString)

        Dim idxTransTypeThermal As Integer
        Dim idxTransferToThermal As Integer
        Dim idxEnergyTypeThermal As Integer
        Dim idxSortKeyThermal As Integer
        Dim idxComposition As Integer
        Dim idxEnergyTypeDescThermal As Integer
        Dim idxTransTypeDescThermal As Integer
        Dim idxColBegin As Integer
        Dim idxColEnd As Integer
        Dim idxColTotal As Integer

        For Each c As DataGridViewColumn In dgvThermal.Columns
            If (c.DataPropertyName.ToString = "TransType") Then idxTransTypeThermal = c.Index
            If (c.DataPropertyName.ToString = "TransferTo") Then idxTransferToThermal = c.Index
            If (c.DataPropertyName.ToString = "EnergyType") Then idxEnergyTypeThermal = c.Index
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKeyThermal = c.Index
            If (c.DataPropertyName.ToString = "Composition") Then idxComposition = c.Index
            If (c.DataPropertyName.ToString = "EnergyTypeDesc") Then idxEnergyTypeDescThermal = c.Index
            If (c.DataPropertyName.ToString = "TransTypeDesc") Then idxTransTypeDescThermal = c.Index
            If (c.DataPropertyName.ToString = "Hydrogen") Then idxColBegin = c.Index
            If (c.DataPropertyName.ToString = "Total") Then idxColTotal = c.Index
        Next c

        Dim idxTransTypeElec As Integer
        Dim idxTransferToElec As Integer
        Dim idxEnergyTypeElec As Integer
        Dim idxSortKeyElec As Integer
        Dim idxEnergyTypeDescElec As Integer
        Dim idxTransTypeDescElec As Integer

        For Each c As DataGridViewColumn In dgvElec.Columns
            If (c.DataPropertyName.ToString = "TransType") Then idxTransTypeElec = c.Index
            If (c.DataPropertyName.ToString = "TransferTo") Then idxTransferToElec = c.Index
            If (c.DataPropertyName.ToString = "EnergyType") Then idxEnergyTypeElec = c.Index
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKeyElec = c.Index
            If (c.DataPropertyName.ToString = "EnergyTypeDesc") Then idxEnergyTypeDescElec = c.Index
            If (c.DataPropertyName.ToString = "TransTypeDesc") Then idxTransTypeDescElec = c.Index
        Next c

        Dim dt As DataTable
        If tabThermalElec.SelectedTab Is tpThermal Then
            dt = MyParent.dsEnergy.Tables("Energy")
        Else
            dt = MyParent.dsEnergy.Tables("Electric")
        End If

        Dim row As DataRow = dt.NewRow

        If tabThermalElec.SelectedTab Is tpThermal Then
            If IsEditing Then
                dgvThermal.CurrentRow.Cells(idxTransTypeThermal).Value = TransType
                dgvThermal.CurrentRow.Cells(idxTransferToThermal).Value = TransferTo
                dgvThermal.CurrentRow.Cells(idxEnergyTypeThermal).Value = EnergyType
                dgvThermal.CurrentRow.Cells(idxSortKeyThermal).Value = SortKey
                dgvThermal.CurrentRow.Cells(idxComposition).Value = Composition
                dgvThermal.CurrentRow.Cells(idxEnergyTypeDescThermal).Value = strEnergyDesc
                dgvThermal.CurrentRow.Cells(idxTransTypeDescThermal).Value = strTransDesc

                ' Clear Composition information when selecting a non-composition energy
                If (Composition = 0) Then
                    For i As Integer = idxColBegin To idxColEnd
                        dgvThermal.CurrentRow.Cells(i).Value = DBNull.Value
                    Next i
                    dgvThermal.CurrentRow.Cells(idxColTotal).Value = DBNull.Value
                End If

            Else
                row!TransType = TransType
                row!TransferTo = TransferTo
                row!EnergyType = EnergyType
                row!SortKey = SortKey
                row!Composition = Composition
                row!EnergyTypeDesc = strEnergyDesc
                row!TransTypeDesc = strTransDesc
                dt.Rows.Add(row)
            End If
        Else
            If IsEditing Then
                dgvElec.CurrentRow.Cells(idxTransTypeElec).Value = TransType
                dgvElec.CurrentRow.Cells(idxTransferToElec).Value = TransferTo
                dgvElec.CurrentRow.Cells(idxEnergyTypeElec).Value = EnergyType
                dgvElec.CurrentRow.Cells(idxSortKeyElec).Value = SortKey
                dgvElec.CurrentRow.Cells(idxEnergyTypeDescElec).Value = strEnergyDesc
                dgvElec.CurrentRow.Cells(idxTransTypeDescElec).Value = strTransDesc
            Else
                row!TransType = TransType
                row!TransferTo = TransferTo
                row!EnergyType = EnergyType
                row!SortKey = SortKey
                row!EnergyTypeDesc = strEnergyDesc
                row!TransTypeDesc = strTransDesc
                dt.Rows.Add(row)
            End If
        End If

        tabThermalElec.Visible = True
        pnlLU.Visible = False : pnlData.Visible = True

        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
        IsEditing = False
        refreshDGV(sender, e)

        Exit Sub

NothingSelected:
        HandleNothingSelected()

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetUpTransactions()
        ViewingLUs(pnlHeader, pnlLU, pnlData, False)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim tmpstr As String
        If tabThermalElec.SelectedTab Is tpThermal Then tmpstr = "Thermal Energy" Else tmpstr = "Electricity"
        MyParent.PrintPreviews("Table 16", tmpstr)
    End Sub

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "Energy")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Energy'")
        row(0)!Checked = chk.Checked
        btnSave.BackColor = Color.Red
        btnSave.ForeColor = Color.White
        EnergyNotSaved = True
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EditEnergy()
        ViewingLUs(pnlHeader, pnlLU, pnlData, False)
    End Sub

    Private Sub EditEnergy()

        IsEditing = True
        pnlLU.Visible = True : pnlData.Visible = False
        pnlLU.BringToFront()
        Dim TransType, TransferTo As String
        Dim SortKey As Integer

        On Error GoTo NothingSelected

        Dim idxSortKeyThermal As Integer
        For Each c As DataGridViewColumn In dgvThermal.Columns
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKeyThermal = c.Index
        Next c

        Dim idxSortKeyElec As Integer
        For Each c As DataGridViewColumn In dgvElec.Columns
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKeyElec = c.Index
        Next c

        If tabThermalElec.SelectedTab Is tpThermal Then
            dgvThermal.Rows(dgvThermal.SelectedCells(0).RowIndex).Selected = True
            SortKey = CInt(dgvThermal.CurrentRow.Cells(idxSortKeyThermal).Value)
        Else
            dgvElec.Rows(dgvElec.SelectedCells(0).RowIndex).Selected = True
            SortKey = CInt(dgvElec.CurrentRow.Cells(idxSortKeyElec).Value)
        End If

        dv_LU.RowFilter = "SortKey = " & SortKey
        TransType = Trim(dv_LU.Item(0)!TransType.ToString)
        TransferTo = Trim(dv_LU.Item(0)!TransferTo.ToString)

        SetUpTransactions()

        Select Case TransType
            Case "PUR" : cmbTransaction.SelectedIndex = 0
            Case "PRO" : cmbTransaction.SelectedIndex = 1
                ' 20081216 RRH changed line cmbTransaction.SelectedIndex = 2 = cmbTransaction.SelectedIndex = 3
            Case "DST" : If TransferTo = "REF" Then cmbTransaction.SelectedIndex = 2 : cmbTransaction.SelectedIndex = 3
            Case "SOL" : cmbTransaction.SelectedIndex = 4
        End Select

        For Each SelectRow As DataGridViewRow In dgvLU.Rows
            If CDbl(SelectRow.Cells("SortKey_LU").Value.ToString) = SortKey Then
                SelectRow.Selected = True
                dgvLU.CurrentCell = SelectRow.Cells("EnergyTypeDesc")
                Exit For
            Else
                SelectRow.Selected = False
            End If
        Next
        Exit Sub

NothingSelected:
        HandleNothingSelected()
    End Sub

    Private Sub HandleNothingSelected()
        IsEditing = False
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
        'pnlLU.Visible = False : pnlData.Visible = True
        ProfileMsgBox("CRIT", "SELECT", "energy type")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveEnergy()
        refreshDGV(sender, e)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If EnergyNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Energy")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveEnergy()
                Case DialogResult.No
                    MyParent.dsEnergy.RejectChanges()
                    MyParent.dsMappings.Tables("Energy").RejectChanges()
                    MyParent.dsMappings.Tables("Electric").RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, EnergyNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub SA_Energy_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.tabThermalElec.Visible = True
        Me.pnlLU.Visible = False

        Me.btnSave.TabIndex = 0
        Me.btnAdd.TabIndex = 3
        Me.btnEdit.TabIndex = 4
        Me.btnClear.TabIndex = 5

        Me.tbFilePath.TabIndex = 6
        Me.btnBrowse.TabIndex = 7
        Me.cbSheets.TabIndex = 8
        Me.btnBrowse.TabIndex = 9

        Me.tabThermalElec.TabIndex = 10
        Me.dgvThermal.TabIndex = 11
        Me.dgvElec.TabIndex = 12

        Me.cmbTransaction.TabIndex = 13
        Me.dgvLU.TabIndex = 14
        Me.btnOK.TabIndex = 15
        Me.btnCancel.TabIndex = 16

        Me.btnPreview.TabIndex = 17
        Me.chkComplete.TabIndex = 18
        Me.txtTTotal.TabIndex = 19
        Me.txtETotal.TabIndex = 20

        Me.btnClose.TabIndex = 21

    End Sub

    Private Sub dgvThermal_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvThermal.UserDeletingRow
        If EnergyRights Then
            Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "thermal energy entry")
            If result = DialogResult.Yes Then
                ChangesMade(btnSave, chkComplete, EnergyNotSaved)
                PopulateSummary()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub dgvElec_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvElec.UserDeletingRow
        If EnergyRights Then
            Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "electricity entry")
            If result = DialogResult.Yes Then
                ChangesMade(btnSave, chkComplete, EnergyNotSaved)
                PopulateSummary()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub viewLinksData()
        dgvThermal.Visible = rbData.Checked
        dgvElec.Visible = rbData.Checked
        dgvThermalLinks.Visible = Not rbData.Checked
        dgvElecLinks.Visible = Not rbData.Checked
        btnPreview.Enabled = rbData.Checked

        If rbData.Checked Then
            Me.lblHeaderThermal.Text = "Thermal Energy and Composition:   DATA"
            Me.lblHeaderElectricity.Text = "Electricity:   DATA"
        Else
            Me.lblHeaderThermal.Text = "Thermal Energy and Composition:   LINKS"
            Me.lblHeaderElectricity.Text = "Electricity:   LINKS"
        End If

        If IsLoading And EnergyRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        selectWorkBook(Me.Cursor, tbFilePath, cbSheets, MyParent.MyParent.pnlProgress, dvWB)
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If ProfileMsgBox("YN", "IMPORT", "Energy") = DialogResult.Yes Then importEnergy()
    End Sub

    Friend Function importEnergy() As Boolean

        Dim dtMapping As DataTable = MyParent.dsMappings.Tables("Energy")   ' Making import code more generic
        Dim dtTarget As DataTable = MyParent.dsEnergy.Tables("Energy")    ' Making import code more generic

        Dim MyExcel As Object = OpenExcel()
        Dim MyWB As Object = OpenWorkbook(MyExcel, tbFilePath.Text)
        Dim MyWS As Object = OpenSheet(MyExcel, MyWB, cbSheets.Text)

        If MyExcel Is Nothing Then Exit Function
        If MyWB Is Nothing Then Exit Function
        If MyWS Is Nothing Then Exit Function

        Dim dr() As DataRow

        For Each r As DataRow In dtMapping.Rows

            dr = dtTarget.Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")

            If (dr.GetUpperBound(0) > -1) Then

                For Each c As DataColumn In dtMapping.Columns

                    Select Case c.ColumnName
                        Case "RptSource", "RptPriceLocal", "Hydrogen", "Methane", "Ethane", "Ethylene", "Propane", "Propylene", "Butane", "Isobutane", "Butylenes", "C5Plus", "H2S", "CO", "NH3", "SO2", "CO2", "N2"
                            Try

                                If r.Item(c.ColumnName).ToString() <> "" AndAlso _
                                    Not IsDBNull(r.Item(c.ColumnName).ToString()) AndAlso _
                                    IsNumeric(MyWS.Range(r.Item(c.ColumnName).ToString()).Value) Then

                                    dr(0).Item(c.ColumnName) = CleanseNumericDataFromBridgeFile(MyWS.Range(r.Item(c.ColumnName).ToString()).Value, cbSheets.Text, r.Item(c.ColumnName).ToString())

                                Else
                                    dr(0).Item(c.ColumnName) = DBNull.Value
                                End If

                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Thermal energy for the " & Trim(r.Item("TransTypeDesc").ToString()) & "/" & Trim(r.Item("EnergyTypeDesc").ToString()) & " item")
                                GoTo CleanupExcel
                            End Try

                    End Select
                Next c

            Else

                Dim nr As Integer = 0
                Dim drNew As DataRow = dtTarget.NewRow()

                For Each c As DataColumn In dtMapping.Columns

                    Select Case c.ColumnName
                        Case "RptSource", "RptPriceLocal", "Hydrogen", "Methane", "Ethane", "Ethylene", "Propane", "Propylene", "Butane", "Isobutane", "Butylenes", "C5Plus", "H2S", "CO", "NH3", "SO2", "CO2", "N2"
                            Try

                                If r.Item(c.ColumnName).ToString() <> "" AndAlso _
                                    Not IsDBNull(r.Item(c.ColumnName).ToString()) AndAlso _
                                    IsNumeric(MyWS.Range(r.Item(c.ColumnName).ToString()).Value) Then

                                    drNew.Item(c.ColumnName) = CleanseNumericDataFromBridgeFile(MyWS.Range(r.Item(c.ColumnName).ToString()).Value, cbSheets.Text, r.Item(c.ColumnName).ToString())
                                    nr = nr + 1
                                Else
                                    drNew.Item(c.ColumnName) = DBNull.Value
                                End If

                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Thermal energy for the " & Trim(r.Item("TransTypeDesc").ToString()) & "/" & Trim(r.Item("EnergyTypeDesc").ToString()) & " item")
                                GoTo CleanupExcel

                            End Try

                        Case Else

                            Try
                                If r.Item(c.ColumnName).ToString() <> "" Then drNew.Item(c.ColumnName) = r.Item(c.ColumnName).ToString()
                            Catch ex As Exception
                            End Try

                    End Select
                Next c

                If nr > 0 AndAlso Not drNew("RptSource") Is DBNull.Value AndAlso CSng(drNew("RptSource")) <> 0.0 Then dtTarget.Rows.Add(drNew)

            End If

        Next r

        dtMapping = MyParent.dsMappings.Tables("Electric")   ' Making import code more generic
        dtTarget = MyParent.dsEnergy.Tables("Electric")    ' Making import code more generic

        For Each r As DataRow In dtMapping.Rows

            dr = dtTarget.Select("transtype = '" & r.Item("transtype").ToString() & "' and transferto = '" & r.Item("transferto").ToString() & "' and energytype = '" & r.Item("energytype").ToString() & "'")

            If (dr.GetUpperBound(0) > -1) Then
                For Each c As DataColumn In dtMapping.Columns
                    Select Case c.ColumnName
                        Case "RptGenEff", "RptMWH", "PriceLocal"
                            Try

                                If r.Item(c.ColumnName).ToString() <> "" AndAlso _
                                    Not IsDBNull(r.Item(c.ColumnName).ToString()) AndAlso _
                                    IsNumeric(MyWS.range(r.Item(c.ColumnName).ToString()).value) Then

                                    dr(0).Item(c.ColumnName) = CleanseNumericDataFromBridgeFile(MyWS.range(r.Item(c.ColumnName).ToString()).value, cbSheets.Text, r.Item(c.ColumnName).ToString())

                                Else
                                    dr(0).Item(c.ColumnName) = DBNull.Value
                                End If

                            Catch ex As Exception
                                ProfileMsgBox("crit", "mapping", "electric energy for the " & Trim(r.Item("transtypedesc").ToString()) & "/" & Trim(r.Item("energytypedesc").ToString()) & " item")
                                GoTo cleanupexcel
                            End Try

                    End Select
                Next c
            Else

                Dim nr As Integer = 0
                Dim drNew As DataRow = dtTarget.NewRow()

                For Each c As DataColumn In dtMapping.Columns

                    Select Case c.ColumnName
                        Case "RptGenEff", "RptMWH", "PriceLocal"
                            Try

                                If r.Item(c.ColumnName).ToString() <> "" AndAlso _
                                    Not IsDBNull(r.Item(c.ColumnName).ToString()) AndAlso _
                                    IsNumeric(MyWS.Range(r.Item(c.ColumnName).ToString()).Value) Then

                                    drNew.Item(c.ColumnName) = CleanseNumericDataFromBridgeFile(MyWS.Range(r.Item(c.ColumnName).ToString()).Value, cbSheets.Text, r.Item(c.ColumnName).ToString())
                                    nr = nr + 1
                                Else
                                    drNew.Item(c.ColumnName) = DBNull.Value
                                End If

                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Electric energy for the " & Trim(r.Item("TransTypeDesc").ToString()) & "/" & Trim(r.Item("EnergyTypeDesc").ToString()) & " item")
                                GoTo CleanupExcel
                            End Try
                        Case Else
                            Try
                                If r.Item(c.ColumnName).ToString() <> "" Then drNew.Item(c.ColumnName) = r.Item(c.ColumnName).ToString()
                            Catch ex As Exception
                            End Try
                    End Select
                Next c

                If nr > 0 AndAlso Not drNew("RptMWH") Is DBNull.Value AndAlso CSng(drNew("RptMWH").ToString) <> 0.0 Then dtTarget.Rows.Add(drNew)


            End If

        Next r


        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
        EnergyNotSaved = True

CleanupExcel:
        CloseExcel(MyExcel, MyWB, MyWS)
        Return EnergyNotSaved

    End Function

    Private Sub tbFilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged

        saveWkbWksReferences(MyParent.dsMappings, "Energy", tbFilePath.Name, tbFilePath.Text)
        If Not IsLoading Then ChangesMade(btnSave, chkComplete, EnergyNotSaved)

        My.Settings.workbookEnergy = tbFilePath.Text
        My.Settings.Save()

    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged

        saveWkbWksReferences(MyParent.dsMappings, "Energy", cbSheets.Name, cbSheets.Text)
        If Not IsLoading Then ChangesMade(btnSave, chkComplete, EnergyNotSaved)

        My.Settings.worksheetEnergy = cbSheets.Text
        My.Settings.Save()

    End Sub

    Private Sub dgvThermal_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvThermal.CellContentDoubleClick
        If dgvThermal.Columns(e.ColumnIndex).DisplayIndex < 2 Then EditEnergy()
    End Sub

    Private Sub dgvElec_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvElec.CellContentDoubleClick
        If dgvElec.Columns(e.ColumnIndex).DisplayIndex < 2 Then EditEnergy()
    End Sub

#Region " Change Made "

    Private Sub dgvThermalLinks_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvThermalLinks.CellBeginEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
    End Sub

    Private Sub dgvThermalLinks_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvThermalLinks.CellEndEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
    End Sub

    Private Sub dgvElecLinks_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvElecLinks.CellBeginEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
    End Sub

    Private Sub dgvElecLinks_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvElecLinks.CellEndEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
    End Sub

    Private Sub dgvThermal_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvThermal.CellBeginEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
    End Sub

    Private Sub dgvThermal_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvThermal.CellEndEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
        PopulateSummary()
    End Sub

    Private Sub dgvElec_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvElec.CellBeginEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)

    End Sub

    Private Sub dgvElec_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvElec.CellEndEdit
        ChangesMade(btnSave, chkComplete, EnergyNotSaved)
        PopulateSummary()
    End Sub

#End Region

#Region " Cell Formatting : Binding Context Changed"

    Private Sub refreshDGV(ByVal sender As Object, ByVal e As System.EventArgs)
        dgvThermal_BindingContextChanged(sender, e)
        dgvThermalLinks_BindingContextChanged(sender, e)
        dgvElec_BindingContextChanged(sender, e)
        dgvElecLinks_BindingContextChanged(sender, e)
    End Sub

    Private Sub dgvThermalLinks_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvThermalLinks.BindingContextChanged

        Dim cWidth As Integer = 100

        For Each c As DataGridViewColumn In dgvThermalLinks.Columns
            Select Case c.DataPropertyName.ToString
                Case "TransTypeDesc" : setColumnFormat(c, "Transaction Type" & ControlChars.CrLf & "Cell Reference", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.AllCells, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "EnergyTypeDesc" : setColumnFormat(c, "Energy Type" & ControlChars.CrLf & "Cell Reference", 1, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.AllCells, DataGridViewContentAlignment.TopLeft) : Exit Select

                Case "RptSource" : setColumnFormat(c, "Source" & ControlChars.CrLf & "Cell Reference", 2, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "RptPriceLocal" : setColumnFormat(c, "Price Local" & ControlChars.CrLf & "Cell Reference", 3, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select

                Case "Hydrogen" : setColumnFormat(c, "Hydrogen" & ControlChars.CrLf & "Cell Reference", 4, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Methane" : setColumnFormat(c, "Methane" & ControlChars.CrLf & "Cell Reference", 5, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Ethane" : setColumnFormat(c, "Ethane" & ControlChars.CrLf & "Cell Reference", 6, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Ethylene" : setColumnFormat(c, "Ethylene" & ControlChars.CrLf & "Cell Reference", 7, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Propane" : setColumnFormat(c, "Propane" & ControlChars.CrLf & "Cell Reference", 8, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Propylene" : setColumnFormat(c, "Propylene" & ControlChars.CrLf & "Cell Reference", 9, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Butane" : setColumnFormat(c, "Butane" & ControlChars.CrLf & "Cell Reference", 10, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Isobutane" : setColumnFormat(c, "Isobutane" & ControlChars.CrLf & "Cell Reference", 11, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "Butylenes" : setColumnFormat(c, "Butylenes" & ControlChars.CrLf & "Cell Reference", 12, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "C5Plus" : setColumnFormat(c, "C5+" & ControlChars.CrLf & "Cell Reference", 13, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "H2S" : setColumnFormat(c, "H2S" & ControlChars.CrLf & "Cell Reference", 14, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "CO" : setColumnFormat(c, "CO" & ControlChars.CrLf & "Cell Reference", 15, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "NH3" : setColumnFormat(c, "NH3" & ControlChars.CrLf & "Cell Reference", 16, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "SO2" : setColumnFormat(c, "SO2" & ControlChars.CrLf & "Cell Reference", 17, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "CO2" : setColumnFormat(c, "CO2" & ControlChars.CrLf & "Cell Reference", 18, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "N2" : setColumnFormat(c, "N2" & ControlChars.CrLf & "Cell Reference", 19, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case Else : setColumnFormat(c)
            End Select

        Next c

        For Each r As DataGridViewRow In dgvThermalLinks.Rows
            dgvThermalLinks.Rows.Item(2).Cells("TransTypeDesc").Selected = True

            If Single.Parse(r.Cells("SortKey").Value.ToString, Globalization.NumberStyles.Number) = 1.0 Then r.Visible = False
            If Single.Parse(r.Cells("SortKey").Value.ToString, Globalization.NumberStyles.Number) = 21.0 Then r.Visible = False
            If Single.Parse(r.Cells("SortKey").Value.ToString, Globalization.NumberStyles.Number) = 41.0 Then r.Visible = False
            If Single.Parse(r.Cells("SortKey").Value.ToString, Globalization.NumberStyles.Number) = 61.0 Then r.Visible = False
            If Single.Parse(r.Cells("SortKey").Value.ToString, Globalization.NumberStyles.Number) = 81.0 Then r.Visible = False
            If Single.Parse(r.Cells("SortKey").Value.ToString, Globalization.NumberStyles.Number) = 85.0 Then r.Visible = False

            setRowFormat(r, False, "Composition", "Hydrogen", "N2")

        Next r

        dgvThermalLinks.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing

    End Sub

    Private Sub dgvElecLinks_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvElecLinks.BindingContextChanged

        Dim cWidth As Integer = 100

        For Each c As DataGridViewColumn In dgvElecLinks.Columns
            Select Case c.DataPropertyName.ToString
                Case "TransTypeDesc" : setColumnFormat(c, "Transaction Type" & ControlChars.CrLf & "Cell Reference", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "EnergyTypeDesc" : setColumnFormat(c, "Energy Type" & ControlChars.CrLf & "Cell Reference", 1, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "RptMWH" : setColumnFormat(c, "Source, MWH" & ControlChars.CrLf & "Cell Reference", 2, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "PriceLocal" : setColumnFormat(c, "Price Local" & ControlChars.CrLf & "Cell Reference", 3, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case "RptGenEff" : setColumnFormat(c, "Price Local" & ControlChars.CrLf & "Cell Reference", 4, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter) : Exit Select
                Case Else
                    setColumnFormat(c)
            End Select
            If (c.DataPropertyName.ToString = "RptGenEff") Then setColumnFormat(c, "Efficiency" & ControlChars.CrLf & "Cell Reference", 4, "Body", rbData.Checked, True, cWidth, "", GetType(String), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopCenter)

        Next c

        For Each r As DataGridViewRow In dgvElecLinks.Rows
            If r.Cells("TransType").Value.ToString = "PRO" Then
                editCell(dgvElecLinks, "PriceLocal", r.Index, False, SystemColors.Control)
            Else
                editCell(dgvElecLinks, "RptGenEff", r.Index, False, SystemColors.Control)
            End If
        Next r

    End Sub

    Private Sub dgvThermal_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvThermal.BindingContextChanged

        Dim cWidth As Integer = 100

        For Each c As DataGridViewColumn In dgvThermal.Columns

            Select Case c.DataPropertyName.ToString
                Case "TransTypeDesc" : setColumnFormat(c, "Transaction Type", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.AllCells, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "EnergyTypeDesc" : setColumnFormat(c, "Energy Type", 1, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.AllCells, DataGridViewContentAlignment.TopLeft) : Exit Select

                Case "RptSource" : setColumnFormat(c, "Source", 2, "Body", rbData.Checked, True, cWidth, "N0", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "RptPriceLocal" : setColumnFormat(c, "Price Local (" & MyParent.KCurrency & ")", 3, "Body", rbData.Checked, True, cWidth, "N2", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select

                Case "Hydrogen" : setColumnFormat(c, "Hydrogen", 4, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Methane" : setColumnFormat(c, "Methane", 5, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Ethane" : setColumnFormat(c, "Ethane", 6, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Ethylene" : setColumnFormat(c, "Ethylene", 7, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Propane" : setColumnFormat(c, "Propane", 8, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Propylene" : setColumnFormat(c, "Propylene", 9, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Butane" : setColumnFormat(c, "Butane", 10, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Isobutane" : setColumnFormat(c, "Isobutane", 11, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Butylenes" : setColumnFormat(c, "Butylenes", 12, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "C5Plus" : setColumnFormat(c, "C5+", 13, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "H2S" : setColumnFormat(c, "H2S", 14, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "CO" : setColumnFormat(c, "CO", 15, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "NH3" : setColumnFormat(c, "NH3", 16, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "SO2" : setColumnFormat(c, "SO2", 17, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "CO2" : setColumnFormat(c, "CO2", 18, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "N2" : setColumnFormat(c, "N2", 19, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "Total" : setColumnFormat(c, "Total", 20, "Header", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select

                Case Else
                    setColumnFormat(c)

            End Select
        Next c

        For Each r As DataGridViewRow In dgvThermal.Rows
            setRowFormat(r, True, "Composition", "Hydrogen", "N2")
        Next r

    End Sub

    Private Sub dgvElec_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvElec.BindingContextChanged

        Dim cWidth As Integer = 100

        For Each c As DataGridViewColumn In dgvElec.Columns
            Select Case c.DataPropertyName.ToString
                Case "TransTypeDesc" : setColumnFormat(c, "Transaction Type", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "EnergyTypeDesc" : setColumnFormat(c, "Energy Type", 1, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "RptMWH" : setColumnFormat(c, "Source, MWH", 2, "Body", rbData.Checked, True, cWidth, "N0", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "PriceLocal" : setColumnFormat(c, "Price Local (" & MyParent.KCurrency & ")", 3, "Body", rbData.Checked, True, cWidth, "N2", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case "RptGenEff" : setColumnFormat(c, "Efficiency", 4, "Body", rbData.Checked, True, cWidth, "N0", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case Else
                    setColumnFormat(c)
            End Select

        Next c

        For Each r As DataGridViewRow In dgvElec.Rows
            If r.Cells("TransType").Value.ToString = "PRO" Then
                editCell(dgvElecLinks, "PriceLocal", r.Index, False, SystemColors.Control)
            Else
                editCell(dgvElecLinks, "RptGenEff", r.Index, False, SystemColors.Control)
            End If

        Next r

    End Sub

#End Region ' Cell Formatting

#Region " Cell Validation "

    Private Sub dgvThermal_RowValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvThermal.RowValidated
        verifyCell(e)
    End Sub

    Private Sub dgvThermal_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvThermal.CellValidated
        verifyCell(e)
    End Sub

    Private Sub verifyCell(ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

        Dim numMin As Integer = 0
        Dim numMax As Integer = 100

        Dim idxComposition As Integer
        Dim idxH As Integer
        Dim idxN2 As Integer

        For Each c As DataGridViewColumn In dgvThermal.Columns
            If (c.DataPropertyName.ToString = "Composition") Then idxComposition = c.Index
            If (c.DataPropertyName.ToString = "Hydrogen") Then idxH = c.Index
            If (c.DataPropertyName.ToString = "N2") Then idxN2 = c.Index
        Next c

        ' Verify that the values are in range.
        For i As Integer = idxH To idxN2

            '     Verify the cell is not null
            If Not Me.dgvThermal.Item(i, e.RowIndex).Value Is System.DBNull.Value Then

                'If the value is outside of the range; set the formatting
                If ((CInt(Me.dgvThermal.Item(i, e.RowIndex).Value) < numMin) Or (CInt(Me.dgvThermal.Item(i, e.RowIndex).Value) > numMax)) Then

                    ProfileMsgBox("CRIT", "", "The value is outside of the range. Enter a value between " & numMin & " and " & numMax & ".")
                    Me.dgvThermal.Item(i, e.RowIndex).Style.ForeColor = Color.Red

                Else

                    Me.dgvThermal.Item(i, e.RowIndex).Style.ForeColor = Color.MediumBlue

                End If

            End If
        Next i

        Select Case dgvThermal.Rows(e.RowIndex).Cells(idxComposition).Value.ToString()
            Case "1" : calculateTotals(dgvThermal, e)
        End Select

    End Sub

    Private Sub calculateTotals(ByVal dgv As DataGridView, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

        Dim idxH As Integer
        Dim idxN2 As Integer
        Dim idxTotal As Integer

        For Each c As DataGridViewColumn In dgv.Columns
            If (c.DataPropertyName.ToString = "Hydrogen") Then idxH = c.Index
            If (c.DataPropertyName.ToString = "N2") Then idxN2 = c.Index
            If (c.DataPropertyName.ToString = "Total") Then idxTotal = c.Index
        Next c

        Me.dgvThermal.Item(idxTotal, e.RowIndex).Value = 0.0

        For i As Integer = idxH To idxN2

            With Me.dgvThermal.Rows(e.RowIndex).Cells(i)
                Me.dgvThermal.Item(idxTotal, e.RowIndex).Value = nz(dgvThermal, idxTotal, e) + nz(dgvThermal, i, e)
            End With

        Next i
    End Sub

#End Region ' Cell Validation

    Private Sub dgvThermal_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvThermal.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgvElec_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvElec.DataError
        dgvDataError(sender, e)
    End Sub

End Class
