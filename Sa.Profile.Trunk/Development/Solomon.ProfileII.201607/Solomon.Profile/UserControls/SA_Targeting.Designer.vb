﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SA_Targeting
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTargetingPeer = New System.Windows.Forms.Label()
        Me.cboPeerGroups = New System.Windows.Forms.ComboBox()
        Me.btnTargeting = New System.Windows.Forms.Button()
        Me.lblRegionCodes = New System.Windows.Forms.Label()
        Me.cboRegionCodes = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'lblTargetingPeer
        '
        Me.lblTargetingPeer.AutoSize = True
        Me.lblTargetingPeer.Location = New System.Drawing.Point(13, 29)
        Me.lblTargetingPeer.Name = "lblTargetingPeer"
        Me.lblTargetingPeer.Size = New System.Drawing.Size(66, 13)
        Me.lblTargetingPeer.TabIndex = 0
        Me.lblTargetingPeer.Text = "Peer Groups"
        '
        'cboPeerGroups
        '
        Me.cboPeerGroups.FormattingEnabled = True
        Me.cboPeerGroups.Location = New System.Drawing.Point(16, 45)
        Me.cboPeerGroups.Name = "cboPeerGroups"
        Me.cboPeerGroups.Size = New System.Drawing.Size(355, 21)
        Me.cboPeerGroups.TabIndex = 2
        '
        'btnTargeting
        '
        Me.btnTargeting.Location = New System.Drawing.Point(139, 85)
        Me.btnTargeting.Name = "btnTargeting"
        Me.btnTargeting.Size = New System.Drawing.Size(75, 23)
        Me.btnTargeting.TabIndex = 4
        Me.btnTargeting.Text = "Get Report"
        Me.btnTargeting.UseVisualStyleBackColor = True
        '
        'lblRegionCodes
        '
        Me.lblRegionCodes.AutoSize = True
        Me.lblRegionCodes.Location = New System.Drawing.Point(353, 14)
        Me.lblRegionCodes.Name = "lblRegionCodes"
        Me.lblRegionCodes.Size = New System.Drawing.Size(74, 13)
        Me.lblRegionCodes.TabIndex = 5
        Me.lblRegionCodes.Text = "Region Codes"
        Me.lblRegionCodes.Visible = False
        '
        'cboRegionCodes
        '
        Me.cboRegionCodes.FormattingEnabled = True
        Me.cboRegionCodes.Location = New System.Drawing.Point(356, 40)
        Me.cboRegionCodes.Name = "cboRegionCodes"
        Me.cboRegionCodes.Size = New System.Drawing.Size(121, 21)
        Me.cboRegionCodes.TabIndex = 6
        Me.cboRegionCodes.Visible = False
        '
        'SA_Targeting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cboRegionCodes)
        Me.Controls.Add(Me.lblRegionCodes)
        Me.Controls.Add(Me.btnTargeting)
        Me.Controls.Add(Me.cboPeerGroups)
        Me.Controls.Add(Me.lblTargetingPeer)
        Me.Name = "SA_Targeting"
        Me.Size = New System.Drawing.Size(540, 205)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTargetingPeer As System.Windows.Forms.Label
    Friend WithEvents cboPeerGroups As System.Windows.Forms.ComboBox
    Friend WithEvents btnTargeting As System.Windows.Forms.Button
    Friend WithEvents lblRegionCodes As System.Windows.Forms.Label
    Friend WithEvents cboRegionCodes As System.Windows.Forms.ComboBox

End Class
