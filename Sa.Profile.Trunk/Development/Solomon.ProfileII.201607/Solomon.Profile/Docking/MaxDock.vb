﻿Public Class MaxDock
    Implements IDocker


    Private _buttonOrdinal As Integer = 1
    Private _userControls() As SA_Browser
    Private _panel As System.Windows.Forms.Panel
    Private _visible As Boolean = True
    Private _top As Integer
    Private _left As Integer
    Private _width As Integer
    Private _height As Integer
    Private _form As Dashboard

    Public Property Visible As Boolean
        Get
            Return _visible
        End Get
        Set(value As Boolean)
            _visible = value
            _panel.Visible = _visible
        End Set
    End Property
    Public Property Top As Integer
        Get
            Return _top
        End Get
        Set(value As Integer)
            _top = value
            _panel.Top = value
        End Set
    End Property
    Public Property Left As Integer
        Get
            Return _left
        End Get
        Set(value As Integer)
            _left = value
            _panel.Left = value
        End Set
    End Property
    Public Property Width As Integer
        Get
            Return _width
        End Get
        Set(value As Integer)
            _width = value
            _panel.Width = value
        End Set
    End Property
    Public Property Height As Integer
        Get
            Return _height
        End Get
        Set(value As Integer)
            _height = value
            _panel.Height = value
        End Set
    End Property

    Friend Sub New(panel As System.Windows.Forms.Panel, userControls() As SA_Browser, ByRef owningForm As Dashboard)
        _panel = panel
        Visible = True
        _userControls = userControls
        _form = owningForm
        'PrepForm(userControls)
    End Sub

    Public Sub PrepPanel() Implements IDocker.PrepPanel
        '_userControls = userControls
        'ClearColsRows()
        For count As Integer = 0 To _userControls.Length - 1
            _userControls(count).Visible = True


            '_userControls(count).Dock = DockStyle.Fill
            _userControls(count).Top = _form.pnlButtons.Height + 5
            _userControls(count).Left = 0 'Form1.ControlPanel.Width
            _userControls(count).Height = _form.pnlOther.Height - _form.pnlButtons.Height - 20
            _userControls(count).Width = _form.pnlOther.Width - 20
            ' _userControls(count).BackColor = Color.Red


            _userControls(count).CallingPanel = Me
            _panel.Controls.Add(_userControls(count))
            If count > 0 Then
                AddButton(_userControls(count).Ordinal)
            End If
        Next
    End Sub

    'Moved to Form1 pnlWorkspace_Resize(se
    'Private Sub Max_Resize(sender As Object, e As EventArgs) Handles Me.Resize
    '    pnlButtons.Width = Me.Width - 50

    '    _panel.Width = Me.Width - 50
    '    _panel.Height = Me.Height - 100

    'End Sub

    'Moved to Form1
    Private Sub AddButton(ordinal As Integer)
        Dim btn As New Button
        'Dim buttonsCount As Integer = pnlButtons.Controls.Count
        btn.Height = 23 '  Button1.Height
        btn.Width = 75 ' Button1.Width
        btn.Text = ordinal.ToString()
        btn.Tag = ordinal.ToString()
        _form.pnlButtons.Controls.Add(btn)
        btn.Top = 6 ' Button1.Top
        btn.Left = 100 * (_form.pnlButtons.Controls.Count - 1) ' Button1.Width + 25
        AddHandler (btn.Click), AddressOf PanelButtonsClick
    End Sub

    'Private Sub PanelButtonsClick(sender As Object, e As EventArgs)
    '    Dim button As Button = TryCast(sender, Button)
    '    Dim ordinal As Integer = Int32.Parse(button.Text) ' Integer = 2
    '    MakeTopmost(ordinal)
    'End Sub


    'Moved to Form1
    'Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    MakeTopmost(1)
    'End Sub
    Public Sub PanelButtonsClick(sender As Object, e As EventArgs)
        Dim button As Button = TryCast(sender, Button)
        Dim ordinal As Integer = Int32.Parse(button.Tag) ' Integer = 2
        MakeTopmost(ordinal)
    End Sub

    Public Sub MakeTopmost(ordinal As Integer) Implements IDocker.MakeTopmost
        'Me.Text = ordinal.ToString()
        For Each ctl As Control In _panel.Controls
            If TypeOf (ctl) Is SA_Browser Then
                Dim c As SA_Browser = ctl
                Dim ord As Integer = c.Ordinal
                If ord = ordinal Then
                    c.BringToFront()
                    'c.Dock = DockStyle.Fill
                    Return
                End If
            End If
        Next
        'Panel.Controls(ordinal - 1).BringToFront()
    End Sub
    Public Sub CloseThis(ordinal As Integer) Implements IDocker.CloseThis
        Dim newUserControls(_userControls.Length - 2) As SA_Browser
        Dim iadded As Integer = 0
        'remove from collection
        For i As Integer = 0 To _userControls.Length - 1
            If _userControls(i).Ordinal <> ordinal Then
                newUserControls(iadded) = _userControls(i)
                iadded += 1
            End If
        Next
        _userControls = newUserControls


        For Each ctl As Control In _panel.Controls
            If TypeOf (ctl) Is SA_Browser Then
                _panel.Controls.Remove(ctl)
            End If
        Next

        ClearMaxButtons()

        'For Each ctl As Control In Form1.pnlButtons.Controls
        '    If TypeOf (ctl) Is Button Then
        '        If ctl.Tag <> "1" Then
        '            Form1.pnlButtons.Controls.Remove(ctl) ' .(_userControls(count).Ordinal)
        '        End If
        '    End If
        'Next


        'redisplay
        PrepPanel() '_userControls)
    End Sub

    Public Sub ClearMaxButtons()
        Dim temp As Button = Nothing
        'Dim buttons(2) As Button

        Dim controls(_form.pnlButtons.Controls.Count - 1) As Control

        Dim controlCount As Integer = 0

        For Each ctl As Control In _form.pnlButtons.Controls
            If TypeOf (ctl) Is Button Then
                If ctl.Tag <> "1" Then
                    controls(controlCount) = ctl
                    controlCount += 1
                    'pnlButtons.Controls.Remove(ctl) ' .(_userControls(count).Ordinal)
                End If
            End If
        Next

        For i As Integer = 0 To controls.Length - 1
            If Not IsNothing(controls(i)) Then
                _form.pnlButtons.Controls.Remove(controls(i))
            End If
        Next



    End Sub

    Public Sub AddThis(ordinal As Integer) Implements IDocker.AddThis
        ReDim Preserve _userControls(_userControls.Length)
        Dim v As New SA_Browser()
        v.Ordinal = ordinal
        _userControls(_userControls.Length - 1) = v


        PrepPanel() '_userControls)

    End Sub


    'Private Sub ClearColsRows()
    '    While _panel.Controls.Count > 0
    '        _panel.Controls.RemoveAt(0)
    '    End While
    '    'remove buttons
    '    While pnlButtons.Controls.Count > 1 'leaving 1st for now
    '        pnlButtons.Controls.RemoveAt(1)
    '    End While

    'End Sub



End Class
