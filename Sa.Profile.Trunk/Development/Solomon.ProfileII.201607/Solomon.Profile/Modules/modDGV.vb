Option Compare Binary
Option Explicit On
Option Strict On

Friend Module modDGV

    Dim fontRowHeader As Font = New Font("Tahoma", 8.25, FontStyle.Bold, GraphicsUnit.Point)
    Dim fontRowBody As Font = New Font("Tahoma", 8.25, FontStyle.Bold, GraphicsUnit.Point)

    Friend Sub setColumnFormat(ByVal c As DataGridViewColumn)

        c.DisplayIndex = c.DataGridView.Columns.Count - 1
        c.Visible = False
        c.ReadOnly = True

    End Sub

    Friend Sub setColumnFormat(ByVal c As DataGridViewColumn, _
                               ByVal strName As String, _
                               ByVal intDisplayIndex As Integer, _
                               ByVal strStyle As String, _
                               ByVal boolMandatory As Boolean, _
                               ByVal allowEdits As Boolean, _
                               ByVal intCellWidth As Integer, _
                               ByVal strFormat As String, _
                               ByVal dgvType As Type, _
                               ByVal dgvASM As DataGridViewAutoSizeColumnMode, _
                               ByVal dgvCellAlign As DataGridViewContentAlignment)

        c.DisplayIndex = intDisplayIndex
        c.HeaderText = strName
        c.Resizable = DataGridViewTriState.False
        c.SortMode = DataGridViewColumnSortMode.NotSortable
        c.ValueType = dgvType

        Dim padColHeader As Padding
        padColHeader.Left = 0
        padColHeader.Right = 0
        padColHeader.Top = 0
        padColHeader.Bottom = 0

        Dim styleColHeader As New DataGridViewCellStyle
        With styleColHeader
            .Alignment = DataGridViewContentAlignment.BottomCenter
            .Padding = padColHeader
            .WrapMode = DataGridViewTriState.True
            .Format = strFormat
        End With

        Dim styleRowHeader As New DataGridViewCellStyle

        Dim padRowHeader As Padding
        padRowHeader.Left = 0
        padRowHeader.Right = 10
        padRowHeader.Top = 0
        padRowHeader.Bottom = 0

        With styleRowHeader
            .BackColor = System.Drawing.SystemColors.Control
            .SelectionBackColor = styleRowHeader.BackColor
            .ForeColor = System.Drawing.Color.FromArgb(0, 64, 64, 64)
            .SelectionForeColor = styleRowHeader.ForeColor
            .Alignment = dgvCellAlign
            .Font = fontRowHeader
            .Format = strFormat
        End With

        Dim bkcolor As Color

        If allowEdits Then
            If boolMandatory Then bkcolor = Color.LemonChiffon Else bkcolor = Color.White
        Else : bkcolor = System.Drawing.SystemColors.Control
        End If

        Dim styleBody As New DataGridViewCellStyle
        With styleBody
            .BackColor = bkcolor
            .ForeColor = Color.MediumBlue
            .Alignment = dgvCellAlign
            .Font = fontRowBody
            .Format = strFormat
        End With

        ''''''''''''''''''''''
        Select Case strStyle
            Case "Header"
                If intCellWidth > 0 Then
                    c.Width = intCellWidth
                Else : c.AutoSizeMode = dgvASM
                End If
                c.DataGridView.ColumnHeadersDefaultCellStyle = styleColHeader
                c.ReadOnly = Not allowEdits
                c.DefaultCellStyle.Padding = padRowHeader
                c.DefaultCellStyle = styleRowHeader
            Case "Body"
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                c.Width = intCellWidth
                c.ReadOnly = Not allowEdits
                c.DefaultCellStyle = styleBody
            Case Else
                c.Width = 0
        End Select

    End Sub

    Friend Sub setRowFormat(ByVal r As DataGridViewRow, _
                            ByVal boolMandatory As Boolean, _
                            ByVal colTrigger As String, _
                            ByVal colBegin As String, _
                            ByVal colEnd As String)

        Dim idxTrigger As Integer
        Dim idxColBegin As Integer
        Dim idxColEnd As Integer

        For Each c As DataGridViewColumn In r.DataGridView.Columns
            If (c.DataPropertyName.ToString = colTrigger) Then idxTrigger = c.Index
            If (c.DataPropertyName.ToString = colBegin) Then idxColBegin = c.Index
            If (c.DataPropertyName.ToString = colEnd) Then idxColEnd = c.Index
        Next c

        Dim bkColor As Color

        If idxTrigger <> 0 AndAlso colTrigger <> "Indent" Then
            Select Case r.Cells(idxTrigger).Value.ToString()
                Case "1"
                    If boolMandatory Then bkColor = Color.LemonChiffon Else bkColor = Color.White
                    setCellFormat(r, True, bkColor, idxColBegin, idxColEnd)  ' Allow changes
                Case Else
                    bkColor = System.Drawing.SystemColors.Control
                    setCellFormat(r, False, bkColor, idxColBegin, idxColEnd) ' Deny changes
            End Select
        ElseIf idxTrigger <> 0 AndAlso colTrigger = "Indent" Then
            Select Case r.Cells(idxTrigger).Value.ToString()
                Case "1"
                    bkColor = System.Drawing.SystemColors.Control
                    setCellFormat(r, False, bkColor, idxColBegin, idxColEnd) ' Deny changes
            End Select
        End If

    End Sub

    Private Sub setCellFormat(ByVal r As DataGridViewRow, _
                              ByVal allowEdits As Boolean, _
                              ByVal bkColor As Color, _
                              ByVal idxColBegin As Integer, _
                              ByVal idxColEnd As Integer)

        For i As Integer = idxColBegin To idxColEnd
            r.Cells(i).ReadOnly = Not allowEdits
            r.Cells(i).Style.BackColor = bkColor

            If Not allowEdits AndAlso r.Cells(i).Value.ToString = "0" Then
                r.Cells(i).Value = DBNull.Value
            End If

        Next i

    End Sub

    Friend Sub dgViewAggregate(ByVal dgv As DataGridView, _
                               ByVal viewProfile As Boolean, _
                               ByVal viewData As Boolean, _
                               ByVal strID As String)

        Dim padDesc As Padding
        padDesc.Top = 0
        padDesc.Bottom = 0
        padDesc.Right = 0

        Dim styleDesc As New DataGridViewCellStyle
        styleDesc.BackColor = System.Drawing.SystemColors.Control
        styleDesc.SelectionBackColor = styleDesc.BackColor
        styleDesc.ForeColor = System.Drawing.Color.FromArgb(0, 64, 64, 64)
        styleDesc.SelectionForeColor = styleDesc.ForeColor

        Dim styleRowHeader As New DataGridViewCellStyle
        styleRowHeader.BackColor = System.Drawing.SystemColors.Control
        styleRowHeader.SelectionBackColor = styleRowHeader.BackColor
        styleRowHeader.ForeColor = System.Drawing.Color.FromArgb(0, 64, 64, 64)
        styleRowHeader.SelectionForeColor = styleRowHeader.ForeColor

        Dim styleMandatory As New DataGridViewCellStyle
        styleMandatory.ForeColor = Color.MediumBlue
        styleMandatory.SelectionForeColor = Color.White
        styleMandatory.SelectionBackColor = Color.MediumBlue
        styleMandatory.BackColor = Color.LemonChiffon
        styleMandatory.Format = "N1"

        Dim styleOptional As New DataGridViewCellStyle
        styleOptional.ForeColor = Color.MediumBlue
        styleOptional.SelectionForeColor = Color.White
        styleOptional.SelectionBackColor = Color.MediumBlue
        styleOptional.BackColor = Color.White
        styleOptional.Format = "N1"

        Dim styleTotal As New DataGridViewCellStyle
        styleTotal.BackColor = styleRowHeader.BackColor
        styleTotal.SelectionBackColor = styleTotal.BackColor
        styleTotal.ForeColor = styleRowHeader.ForeColor
        styleTotal.SelectionForeColor = styleTotal.ForeColor
        styleTotal.Format = "N1"

        Dim idxID As Integer = 0
        Dim idxDescription As Integer = 0
        Dim idxSortKey As Integer = 0
        Dim idxIndent As Integer = 0
        Dim idsParentID As Integer = 0
        Dim idxDetailStudy As Integer = 0
        Dim idxDetailProfile As Integer = 0
        Dim idxDisplay As Integer = 0
        Dim idxCatID As Integer = 0

        For Each c As DataGridViewColumn In dgv.Columns
            If (c.DataPropertyName.ToString = strID) Then idxID = c.Index : c.Visible = False
            If (c.DataPropertyName.ToString = "Description") Then idxDescription = c.Index
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKey = c.Index : c.Visible = False
            If (c.DataPropertyName.ToString = "Indent") Then idxIndent = c.Index : c.Visible = False
            If (c.DataPropertyName.ToString = "ParentID") Then idsParentID = c.Index : c.Visible = False
            If (c.DataPropertyName.ToString = "detailStudy") Then idxDetailStudy = c.Index : c.Visible = False
            If (c.DataPropertyName.ToString = "detailProfile") Then idxDetailProfile = c.Index : c.Visible = False
            If (c.DataPropertyName.ToString = "CategoryID") Then idxCatID = c.Index : c.Visible = False

        Next c

        If viewProfile Then idxDisplay = idxDetailProfile Else idxDisplay = idxDetailStudy

        For Each r As DataGridViewRow In dgv.Rows

            Select Case r.Cells(idxDisplay).Value.ToString
                Case "H" ' Header
                    r.Visible = True
                    r.ReadOnly = True
                    r.DefaultCellStyle.ApplyStyle(styleRowHeader)
                    Exit Select

                Case "S", "T" ' Sum (Visible)
                    r.Visible = True
                    r.ReadOnly = True
                    r.DefaultCellStyle.ApplyStyle(styleTotal)
                    Exit Select

                Case "C" ' Calculated (Hidden)
                    dgv.CurrentCell = Nothing
                    r.Visible = False
                    Exit Select

                Case "M" ' Mandatory
                    r.Visible = True
                    r.ReadOnly = False
                    If viewData Then
                        r.DefaultCellStyle.ApplyStyle(styleMandatory)
                    Else
                        r.DefaultCellStyle.ApplyStyle(styleOptional)
                    End If
                    Exit Select

                Case "O" ' Optional
                    r.Visible = True
                    r.ReadOnly = False
                    r.DefaultCellStyle.ApplyStyle(styleOptional)
                    Exit Select

                Case Else ' Do Not Show
                    dgv.CurrentCell = Nothing
                    r.Visible = False

            End Select

            padDesc.Left = CType(IsNull(r.Cells(idxIndent).Value), Integer) * 10

            If (CType(IsNull(r.Cells(idxSortKey).Value), Integer) >= 10000) Then
                dgv.CurrentCell = Nothing
                r.Visible = False
            End If

            r.Cells(idxDescription).Style.Padding = padDesc
            r.Cells(idxDescription).Style.ApplyStyle(styleDesc)
            r.Cells(idxDescription).ReadOnly = True

        Next r

    End Sub

    Friend Sub DEPRECIATED_calculateAggregate(ByVal dgv As DataGridView, _
                                  ByVal e As System.Windows.Forms.DataGridViewCellEventArgs, _
                                  ByVal viewDetail As Boolean, _
                                  ByVal strID As String)

        Dim idxID As Integer = -1
        Dim idxDescription As Integer = -1
        Dim idxSortKey As Integer = -1
        Dim idxIndent As Integer = -1
        Dim idxParentID As Integer = -1
        Dim idxDetailStudy As Integer = -1
        Dim idxDetailProfile As Integer = -1
        Dim idxDisplay As Integer = -1

        For Each c As DataGridViewColumn In dgv.Columns
            If (c.DataPropertyName.ToString = strID) Then idxID = c.Index
            If (c.DataPropertyName.ToString = "Description") Then idxDescription = c.Index
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKey = c.Index
            If (c.DataPropertyName.ToString = "Indent") Then idxIndent = c.Index
            If (c.DataPropertyName.ToString = "ParentID") Then idxParentID = c.Index
            If (c.DataPropertyName.ToString = "detailStudy") Then idxDetailStudy = c.Index
            If (c.DataPropertyName.ToString = "detailProfile") Then idxDetailProfile = c.Index
        Next c

        Dim strParentID As String = dgv.Item(idxID, e.RowIndex).Value.ToString()
        Dim strAggID As String = dgv.Item(idxParentID, e.RowIndex).Value.ToString()
        Dim tmpVal As Double = 0
        Dim tmpTotal As Double = 0

        If (e.ColumnIndex <> idxDescription) Then

            For Each r As DataGridViewRow In dgv.Rows

                If viewDetail Then
                    ' Aggregate Data
                    If strAggID <> "" AndAlso r.Cells(idxParentID).Value.ToString() = strAggID Then
                        tmpVal = tmpVal + CType(IsNull(r.Cells(e.ColumnIndex).Value), Double)
                    End If

                    If r.Cells(idxID).Value.ToString() = strAggID Then
                        r.Cells(e.ColumnIndex).Value = tmpVal
                    End If

                Else

                    ' Clear Data
                    If r.Cells(idxDetailStudy).Value.ToString() <> "H" AndAlso r.Cells(idxParentID).Value.ToString() = strParentID Then
                        r.Cells(e.ColumnIndex).Value = DBNull.Value
                    End If

                End If

            Next r

        End If

    End Sub

    Friend Sub calculateAggregate(ByVal dgv As DataGridView, _
                                  ByVal col As Integer, _
                                  ByVal row As Integer, _
                                  ByVal viewDetail As Boolean, _
                                  ByVal strID As String, _
                         Optional ByVal strWeight As String = "")

        ' strWeight is used to calculate weighted averages for fields ending in "Pcnt"

        Dim idxID As Integer = -1
        Dim idxDescription As Integer = -1
        Dim idxSortKey As Integer = -1
        Dim idxIndent As Integer = -1
        Dim idxParentID As Integer = -1
        Dim idxDetailStudy As Integer = -1
        Dim idxDetailProfile As Integer = -1
        Dim idxDisplay As Integer = -1
        Dim idxWeight As Integer = -1

        For Each c As DataGridViewColumn In dgv.Columns
            If (c.DataPropertyName.ToString = strID) Then idxID = c.Index
            If (c.DataPropertyName.ToString = strWeight) Then idxWeight = c.Index
            If (c.DataPropertyName.ToString = "Description") Then idxDescription = c.Index
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKey = c.Index
            If (c.DataPropertyName.ToString = "Indent") Then idxIndent = c.Index
            If (c.DataPropertyName.ToString = "ParentID") Then idxParentID = c.Index
            If (c.DataPropertyName.ToString = "detailStudy") Then idxDetailStudy = c.Index
            If (c.DataPropertyName.ToString = "detailProfile") Then idxDetailProfile = c.Index
        Next c

        Dim strParentID As String = dgv.Item(idxID, row).Value.ToString()
        Dim strAggID As String = dgv.Item(idxParentID, row).Value.ToString()
        Dim tmpVal As Double = 0
        Dim tmpTotal As Double = 0

        If (col <> idxDescription) Then

            For Each r As DataGridViewRow In dgv.Rows

                If viewDetail Then
                    ' Aggregate Data
                    If strAggID <> "" AndAlso r.Cells(idxParentID).Value.ToString() = strAggID Then

                        If Right(dgv.Columns(col).DataPropertyName, 4) <> "Pcnt" And idxWeight = -1 Then
                            tmpVal = tmpVal + CType(IsNull(r.Cells(col).Value), Double)
                        Else
                            tmpVal = tmpVal + CType(IsNull(r.Cells(col).Value), Double) * CType(IsNull(r.Cells(idxWeight).Value), Double)
                            tmpTotal = tmpTotal + CType(IsNull(r.Cells(idxWeight).Value), Double)
                        End If

                    End If

                    If r.Cells(idxID).Value.ToString() = strAggID Then

                        If Right(dgv.Columns(col).DataPropertyName, 4) <> "Pcnt" And idxWeight = -1 Then
                            r.Cells(col).Value = tmpVal
                        Else
                            If tmpTotal <> 0.0 Then
                                r.Cells(col).Value = tmpVal / tmpTotal
                            Else
                                r.Cells(col).Value = DBNull.Value
                            End If
                        End If

                    End If

                Else

                    ' Clear Data
                    If r.Cells(idxDetailStudy).Value.ToString() <> "H" AndAlso r.Cells(idxParentID).Value.ToString() = strParentID Then
                        r.Cells(col).Value = DBNull.Value
                    End If

                End If

            Next r

        End If

        If dgv.Columns(col).DataPropertyName = strWeight Then
            For k As Integer = 0 To dgv.Columns.Count - 1
                If Right(dgv.Columns(k).DataPropertyName, 4) = "Pcnt" Then
                    calculateAggregate(dgv, k, row, viewDetail, strID, strWeight)
                End If
            Next k
        End If

    End Sub

    Friend Sub calculateAggregateWtAvg(ByVal dgv As DataGridView, _
                              ByVal col As Integer, _
                              ByVal row As Integer, _
                              ByVal viewDetail As Boolean, _
                              ByVal strID As String, _
                     Optional ByVal strWeight As String = "", _
                    Optional ByVal pcntColumnForWeight As String = "")
        'SB created this  2017-09-19 patterned after calculateAggregate for RIchard's change.
        ' strWeight is used to calculate weighted averages for fields ending in "Pcnt"

        Dim idxID As Integer = -1
        Dim idxDescription As Integer = -1
        Dim idxSortKey As Integer = -1
        Dim idxIndent As Integer = -1
        Dim idxParentID As Integer = -1
        Dim idxDetailStudy As Integer = -1
        Dim idxDetailProfile As Integer = -1
        Dim idxDisplay As Integer = -1
        Dim idxWeight As Integer = -1
        Dim idxPcntColumnForWeight As Integer = -1

        For Each c As DataGridViewColumn In dgv.Columns
            If (c.DataPropertyName.ToString = strID) Then idxID = c.Index
            If (c.DataPropertyName.ToString = strWeight) Then idxWeight = c.Index
            If (c.DataPropertyName.ToString = "Description") Then idxDescription = c.Index
            If (c.DataPropertyName.ToString = "SortKey") Then idxSortKey = c.Index
            If (c.DataPropertyName.ToString = "Indent") Then idxIndent = c.Index
            If (c.DataPropertyName.ToString = "ParentID") Then idxParentID = c.Index
            If (c.DataPropertyName.ToString = "detailStudy") Then idxDetailStudy = c.Index
            If (c.DataPropertyName.ToString = "detailProfile") Then idxDetailProfile = c.Index
            'If c.Name.ToUpper() = pcntColumnForWeight.ToUpper() Then idxPcntColumnForWeight = c.Index
            If c.DataPropertyName.ToString.ToUpper() = pcntColumnForWeight.ToUpper() Then idxPcntColumnForWeight = c.Index
        Next c

        Dim strParentID As String = dgv.Item(idxID, row).Value.ToString()
        Dim strAggID As String = dgv.Item(idxParentID, row).Value.ToString()
        Dim tmpVal As Double = 0
        Dim tmpTotal As Double = 0

        If (col <> idxDescription) Then

            For Each r As DataGridViewRow In dgv.Rows

                If viewDetail Then
                    ' Aggregate Data
                    If strAggID <> "" AndAlso r.Cells(idxParentID).Value.ToString() = strAggID Then

                        If Right(dgv.Columns(col).DataPropertyName, 4) <> "Pcnt" And
                            (idxWeight = -1 Or col <> idxWeight) Then

                            tmpVal = tmpVal + CType(IsNull(r.Cells(col).Value), Double)

                        Else
                            If (col = idxWeight) Then
                                tmpVal = tmpVal + CType(IsNull(r.Cells(col).Value), Double) *
                                    CType(IsNull(r.Cells(idxPcntColumnForWeight).Value), Double)
                                tmpTotal = tmpTotal + CType(IsNull(r.Cells(idxWeight).Value), Double)
                            End If
                        End If

                    End If

                    If r.Cells(idxID).Value.ToString() = strAggID Then

                        If Right(dgv.Columns(col).DataPropertyName, 4) <> "Pcnt" And
                            (idxWeight = -1 Or col <> idxWeight) Then
                            r.Cells(col).Value = tmpVal
                        Else
                            If tmpTotal <> 0.0 Then
                                'r.Cells(col).Value = tmpVal / tmpTotal
                                r.Cells(col).Value = tmpTotal
                            Else
                                r.Cells(col).Value = DBNull.Value
                            End If
                        End If

                    End If

                Else

                    ' Clear Data
                    If r.Cells(idxDetailStudy).Value.ToString() <> "H" AndAlso r.Cells(idxParentID).Value.ToString() = strParentID Then
                        r.Cells(col).Value = DBNull.Value
                    End If

                End If

            Next r

        End If

        If dgv.Columns(col).DataPropertyName = strWeight Then
            For k As Integer = 0 To dgv.Columns.Count - 1
                If Right(dgv.Columns(k).DataPropertyName, 4) = "Pcnt" Then
                    calculateAggregate(dgv, k, row, viewDetail, strID, strWeight)
                End If
            Next k
        End If

    End Sub



    Friend Sub dgvDataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs)

        Dim dgv As DataGridView = DirectCast(sender, DataGridView)

        If (e.Context = DataGridViewDataErrorContexts.Parsing + DataGridViewDataErrorContexts.Commit + DataGridViewDataErrorContexts.CurrentCellChange) Then
            ProfileMsgBox("INFO", "", "The value entered in the column """ & dgv.Columns(e.ColumnIndex).HeaderText.ToString & """ is invalid.  The invalid value has been replaced with the original value." & ControlChars.CrLf & ControlChars.CrLf & "Please enter a value with the correct format.")
            dgv.Item(e.ColumnIndex, e.RowIndex).Value = ""
        End If

    End Sub

    Public Sub editCell(ByVal dg As DataGridView, ByVal colname As String, ByVal rowIndex As Integer, ByVal allowEdits As Boolean, ByVal bkColor As Color)

        Dim fColor As Color

        If Not (allowEdits) Then
            bkColor = SystemColors.Control
            fColor = bkColor
        End If

        ' select the row to change using rowIndex
        With dg.Item(colname, rowIndex)
            .ReadOnly = Not allowEdits
            .Style.BackColor = bkColor
            .Style.SelectionBackColor = fColor
        End With
    End Sub

End Module
