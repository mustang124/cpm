Option Compare Binary
Option Explicit On

Imports ProfileEnvironment
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports MSXML2 'for checking internet access

Friend Module modGeneral

    ' RRH 2009 Friend ci As Object
    Private ci As System.Globalization.CultureInfo
    Friend ExcelWasNotRunning As Boolean
    Public LoggedInUser As String

    Public MainForm As Main

    Public BridgeFileConversionErrorsLogPath As String = pathBridge + "BridgeFileConversionErrorsLog.txt"
    Public BridgeFileConversionErrorsOccurred As Boolean = False

    Private _delim As String = Chr(14)

    Private _methodology As String = String.Empty
    Public Property Methodology As String
        Get
            Return _methodology
        End Get
        Set(value As String)
            _methodology = value
        End Set
    End Property

    Private _conversionErrorAt As String = String.Empty
    Public Property ConversionErrorAt As String
        Get
            Return _conversionErrorAt
        End Get
        Set(value As String)
            _conversionErrorAt = value
        End Set
    End Property

    Private _clientKey As String = String.Empty

    Private _adminRights As Boolean = False
    Public Property AdminRights As Boolean
        Get
            Return _adminRights
        End Get
        Set(value As Boolean)
            _adminRights = value
        End Set
    End Property



    'Dim result As String = modGeneral.CleanseNumericDataFromBridgeFile("")
    'result = modGeneral.CleanseNumericDataFromBridgeFile(" ")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("-")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("O")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("NULL")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("nil")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("NILL")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("N/A")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("#VALUE!")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("#REF!")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("#DIV/0!")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("NUM!")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("#NAME?")
    'result = modGeneral.CleanseNumericDataFromBridgeFile("#NULL!")

    'Try
    '    ' Dim result As Boolean
    '    'must be < 5 yrs future, > 10 yrs past
    '    '10/19/2006,  10/19/2021

    '    Dim test As New DateTime(2021, 10, 19) 'T
    '    result = ValidateDateDataFromBridgeFile(test)
    '    test = New DateTime(2021, 10, 20) 'F
    '    result = ValidateDateDataFromBridgeFile(test)
    '    test = New DateTime(2022, 10, 19) 'F
    '    result = ValidateDateDataFromBridgeFile(test)
    '    test = New DateTime(2006, 10, 19) 'S/B T but is F: OK-time issue
    '    result = ValidateDateDataFromBridgeFile(test)
    '    test = New DateTime(2006, 10, 18) 'F
    '    result = ValidateDateDataFromBridgeFile(test)
    '    test = New DateTime(2005, 10, 20) 'F
    '    result = ValidateDateDataFromBridgeFile(test)
    '    test = New DateTime(2006, 10, 20) 'T-OK
    '    result = ValidateDateDataFromBridgeFile(test)
    '    Dim y = "stop"
    'Catch ex As Exception
    '    Dim err As String = ex.Message
    'End Try
    Public Function CleanseNumericDataFromBridgeFile(value As Object, wksheetName As String, cell As String) As String
        If IsNothing(value) Then
            'AppendConversionErrorToLog("Empty value")
            'BridgeFileConversionErrorsOccurred = True
            Return "0" ' String.Empty
        Else
            'Excel.xlCVErr  Range.Value  Coerced to .NET
            '-------------  -----------  ---------------
            '    2000         #NULL!       -2146826288
            '     2007         #DIV/0!      -2146826281
            '     2015         #VALUE!      -2146826273
            '     2023         #REF!        -2146826265
            '     2029         #NAME?       -2146826259
            '     2036         #NUM!        -2146826252
            '     2042         #N/A         -2146826246

            If TypeOf (value) Is String Then
                Select Case value.ToString().Trim().ToUpper()
                    Case "", " ", "-", "O", "NULL", "NIL", "NILL"
                        AppendConversionErrorToLog("Value of '" & value.ToString().Trim().ToUpper() & "' in sheet " & wksheetName & " at " & cell)
                        BridgeFileConversionErrorsOccurred = True
                        Return "0"
                    Case Else
                        Return value
                End Select
            ElseIf IsNumeric(value) Then
                Select Case value
                    Case -2146826288, -2146826281, -2146826273, -2146826265, -2146826259, -2146826252, -2146826246 '"N/A", "#VALUE!", "#REF!", "#DIV/0!", "#NUM!", "#NAME?", "#NULL!"
                        AppendConversionErrorToLog("Value of 'N/A', '#VALUE!', '#REF!', '#DIV/0!', '#NUM!', '#NAME?', or '#NULL!' in sheet " & wksheetName & " at " & cell)
                        BridgeFileConversionErrorsOccurred = True
                        Return "0"
                    Case Else
                        Return value
                End Select
            End If
        End If
    End Function

    Private Sub AppendConversionErrorToLog(message As String)
        Try
            Using log As New System.IO.StreamWriter(BridgeFileConversionErrorsLogPath, True)
                log.WriteLine(DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss") + "   Conversion Error at " + ConversionErrorAt + ": " + message)
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Function ValidateDateDataFromBridgeFile(value As Date) As String
        'Dim z = DateAdd(DateInterval.Year, -10, DateTime.Now)
        'If DateAdd(DateInterval.Year, -10, DateTime.Now) > value Then
        '    'too early
        'End If
        'z = DateAdd(DateInterval.Year, +5, DateTime.Now)
        'If DateAdd(DateInterval.Year, +5, DateTime.Now) < value Then
        '    'too late
        'End If
        If DateAdd(DateInterval.Year, -10, DateTime.Now) > value Or DateAdd(DateInterval.Year, +5, DateTime.Now) < value Then
            Throw New Exception("Invalid Date " + value + " in Bridge File")
        End If
    End Function

    Public Function GetReportLayout(friendlyTableName As String) As DataTable

        'If friendlyTableName.Contains("Chart_LU") Then

        '    'CustomCheckBox
        '    Dim boxesList As New List(Of String)
        '    Dim dsChartLU As New DataSet()
        '    dsChartLU.ReadXml(pathRef & "Reference.xml", XmlReadMode.ReadSchema)
        '    If Not IsNothing(dsChartLU) AndAlso dsChartLU.Tables.Contains("Chart_LU") Then
        '        Dim dtChartLU As New DataTable()
        '        dtChartLU = dsChartLU.Tables("Chart_LU")
        '        Dim lastSection As String = String.Empty
        '        For Each row As DataRow In dtChartLU.Rows
        '            Dim chartTitle As String = row("ChartTitle").ToString()
        '            Dim thisSection As String = row("SectionHeader").ToString()
        '            If thisSection <> lastSection Then
        '                boxesList.Add(_delim & thisSection & _delim & thisSection)
        '                lastSection = thisSection
        '            Else
        '                boxesList.Add(chartTitle & _delim & row("SortKey").ToString())
        '            End If
        '        Next
        '    End If
        '    Return boxesList
        'Else
        If friendlyTableName.Contains("ReportLayout_LU") Then
            Dim boxesList As New List(Of String)
            Dim dsChartLU As New DataSet()
            dsChartLU.ReadXml(pathRef & "Reference.xml", XmlReadMode.ReadSchema)
            If Not IsNothing(dsChartLU) AndAlso dsChartLU.Tables.Contains("Chart_LU") Then
                Dim dtChartLU As New DataTable()
                dtChartLU = dsChartLU.Tables("ReportLayout_LU")
                Return dtChartLU
            End If
        End If
        Return Nothing
    End Function

    Public Function GetListOfChartRows(friendlyTableName As String) As IList(Of String)

        If friendlyTableName.Contains("Chart_LU") Then

            'CustomCheckBox
            Dim boxesList As New List(Of String)
            Dim dsChartLU As New DataSet()
            dsChartLU.ReadXml(pathRef & "Reference.xml", XmlReadMode.ReadSchema)
            If Not IsNothing(dsChartLU) AndAlso dsChartLU.Tables.Contains("Chart_LU") Then
                Dim dtChartLU As New DataTable()
                dtChartLU = dsChartLU.Tables("Chart_LU")
                Dim lastSection As String = String.Empty
                For Each row As DataRow In dtChartLU.Rows
                    Dim chartTitle As String = row("ChartTitle").ToString()
                    Dim thisSection As String = row("SectionHeader").ToString()
                    If thisSection <> lastSection Then
                        boxesList.Add(_delim & thisSection & _delim & thisSection)
                        lastSection = thisSection
                    Else
                        boxesList.Add(chartTitle & _delim & row("SortKey").ToString())
                    End If
                Next
            End If
            Return boxesList
        ElseIf friendlyTableName.Contains("ReportLayout_LU") Then
            Dim boxesList As New List(Of String)
            Dim dsChartLU As New DataSet()
            dsChartLU.ReadXml(pathRef & "Reference.xml", XmlReadMode.ReadSchema)
            If Not IsNothing(dsChartLU) AndAlso dsChartLU.Tables.Contains("Chart_LU") Then
                Dim dtChartLU As New DataTable()
                dtChartLU = dsChartLU.Tables("ReportLayout_LU")
                Dim lastSection As String = String.Empty
                Dim thisSection As String = String.Empty
                For Each row As DataRow In dtChartLU.Rows
                    Dim chartTitle As String = row("Description").ToString()
                    If row("ParentID") = 0 Then
                        thisSection = row("Description").ToString()
                    End If
                    If thisSection <> lastSection Then
                        boxesList.Add(_delim & thisSection & _delim & thisSection & _delim & row("SortKey").ToString())
                        lastSection = thisSection
                    Else
                        boxesList.Add(chartTitle & _delim & row("SortKey").ToString())
                    End If
                Next
            End If
            Return boxesList

        End If
        Return Nothing
    End Function
   
#Region "Diagnostics"
    Public Sub RunDiagnostics()
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim dc As New DataColumn("Results", GetType(System.String))
        dt.Columns.Add(dc)
        Dim dr As DataRow
        Dim list As New List(Of String)
        Dim result As String = TestOpenPort(443, "www.solomononline.com")
        If result.Length > 0 Then
            dr = dt.NewRow()
            dr(0) = result
            dt.Rows.Add(dr)
        End If

        result = TestInternetConnectivity()
        If result.Length > 0 Then
            dr = dt.NewRow()
            dr(0) = result
            dt.Rows.Add(dr)
        End If

        result = TestInternetConnectivity("http://webservices.solomononline.com/refineryws-2012/submitservices.asmx")
        If result.Length > 0 Then
            dr = dt.NewRow()
            dr(0) = result
            dt.Rows.Add(dr)
        End If

        result = TestSolomonWcfService()
        If result.Length > 0 Then
            dr = dt.NewRow()
            dr(0) = result
            dt.Rows.Add(dr)
        End If

        result = TestMemCpuDisk()
        If result.Length > 0 Then
            dr = dt.NewRow()
            dr(0) = result
            dt.Rows.Add(dr)
        End If

        result = OsVersion()
        If result.Length > 0 Then
            dr = dt.NewRow()
            dr(0) = result
            dt.Rows.Add(dr)
        End If
        ds.Tables.Add(dt)
        ds.WriteXml(pathConfig + "Diagnostics.xml")
        'WriteXMLFile(ds, pathConfig + "Diagnostics.xml")
        MsgBox("Diagnostics have been written to " + pathConfig + "Diagnostics.xml. Please send this file to Solomon.")
    End Sub

    Private Function TestOpenPort(port As Integer, hostName As String) As String
        'http://stackoverflow.com/questions/570098/in-c-how-to-check-if-a-tcp-port-is-available
        Dim result As String = String.Empty
        Try
            'Dim ipa = System.Net.Dns.GetHostAddresses("216.58.194.36")(0)
            Dim ipa = System.Net.Dns.GetHostAddresses(hostName)(0)
            Dim sock As New System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp)
            sock.Connect(ipa, port)
            If sock.Connected = True Then
                result = "Port " + port.ToString() + " is in use."
            Else
                result = "Port " + port.ToString() + " is not connected."
            End If
            sock.Close()
            Return result
        Catch ex As System.Net.Sockets.SocketException
            If ex.ErrorCode = 10060 Then
                Return "TestOpenPort() [" + port.ToString() + "]: No connection using " + hostName + ", Port " + port.ToString()
            Else
                Return "TestOpenPort() [" + port.ToString() + "]: " + ex.Message
            End If
        Catch exGeneral As Exception
            Return "TestOpenPort() [port " + port.ToString() + "]: " + exGeneral.Message
        End Try
    End Function
    Private Function TestInternetConnectivity(Optional url As String = "") As String
        Dim result As String = "TestInternetConnectivity(): "
        If url.Length < 1 Then
            url = InputBox("Please enter the address to an internet website that you can access (example: http://www.google.com)", "Profile Diagnostics", "https://www.google.com").Trim()
            If url.Length < 1 Then
                result += "No URL entered"
                Return result
            End If
        End If
        Try
            Dim response As String = String.Empty
            Dim svr As New MSXML2.ServerXMLHTTP
            svr.open("GET", url.Trim(), False, Nothing, Nothing)
            svr.send(String.Empty)
            response = svr.responseText
            svr = Nothing
            result += response
        Catch
            result += "Unable to connect to " + url
        End Try

        Return result
    End Function

    Private Function TestSolomonWcfService() As String
        Return "TestSolomonWcfService() not written yet"
    End Function
    Private Function TestMemCpuDisk() As String
        Dim result As String = "TestMemCpuDisk(): "
        Dim cpuCounter As PerformanceCounter
        Dim ramCounter As PerformanceCounter
        cpuCounter = New PerformanceCounter()
        Try
            cpuCounter.CategoryName = "Processor"
            cpuCounter.CounterName = "% Processor Time"
            cpuCounter.InstanceName = "_Total"
            ramCounter = New PerformanceCounter("Memory", "Available MBytes")
            cpuCounter.NextValue()
            System.Threading.Thread.Sleep(1000)
            result += "CPU: " + cpuCounter.NextValue().ToString() + "%; "
            result += "RAM: " + ramCounter.NextValue().ToString() + "MB;"
            result += TestDiskSpace()
        Catch ex As Exception
            result += "Error in TestMemCpuRam: " + ex.Message + ";"
        End Try
        Return result
    End Function
    Private Function TestDiskSpace() As String
        Dim driveLetter As String = InputBox("Please enter the drive letter where Windows is installed (example: C)", "Profile Diagnostics")
        driveLetter = driveLetter.Substring(0, 1).ToUpper()
        Try
            For Each drv As System.IO.DriveInfo In System.IO.DriveInfo.GetDrives()
                If drv.IsReady AndAlso drv.Name.Substring(0, 1).ToUpper() = driveLetter Then
                    Return "DISK (" + driveLetter + "):" + drv.TotalFreeSpace.ToString() + " Bytes;"
                End If
            Next
        Catch ex As Exception
            Return "Error in TestDiskSpace for drive (" + driveLetter + "): " + ex.Message + ";"
        End Try
    End Function
    Public Function OsVersion() As String
        Dim result As String = "OsVersion(): "
        OsVersion = My.Computer.Info.OSVersion.ToString() + "; "
        If Environment.Is64BitOperatingSystem Then
            result += "64 bit"
        Else
            result += "NOT 64 bit"
        End If
        Return result
    End Function

#End Region

    Friend Sub WriteUCsToFile(controls As SA_Browser(), folderPath As String, dashboardName As String)
        'NOTE: I'm not using XML because I'm writing the URLs, which will have some
        ' characters that XML will need to transform back and forth, etc.

        If IsNothing(controls) Then Exit Sub
        Dim count As Integer = controls.Length
        Dim path As String = folderPath + "\" + dashboardName + ".txt"
        If count > 0 Then
            Try
                Using writer As New StreamWriter(path, False)
                    For i As Integer = 0 To count - 1
                        writer.WriteLine(controls(i).Ordinal.ToString() + _delim + controls(i).Url)
                    Next
                    writer.Flush()
                End Using
            Catch ex As Exception
                MsgBox("Error in WriteUCsToFile()" + ex.Message)
            End Try
        End If
    End Sub

    Friend Function ReadUCsFromFile(folderPath As String, dashboardName As String) As SA_Browser()
        Dim path As String = folderPath + "\" + dashboardName + ".txt"
        Try
            Dim list As New List(Of String)
            Using reader As New StreamReader(path)
                While Not reader.EndOfStream
                    list.Add(reader.ReadLine())
                End While
            End Using
            Dim controls(list.Count - 1) As SA_Browser
            For i As Integer = 0 To list.Count - 1
                Dim parts() As String = list(i).Split(_delim)
                Dim c As New SA_Browser
                c.Ordinal = Int32.Parse(parts(0))
                c.Url = parts(1)
                controls(i) = c
            Next
            Return controls
        Catch ex As Exception
            MsgBox("Error in ReadUCsFromFile(): " + ex.Message)
        End Try
        Return Nothing
    End Function


    Friend Function GetTemplateFilesNamesAndDescriptions() As List(Of String)
        Dim list As New List(Of String)
        For Each key As String In ConfigurationManager.AppSettings.AllKeys
            If ConfigurationManager.AppSettings(key).ToString().EndsWith(".tpl") Then
                list.Add(key)
            End If
        Next
        Return list
    End Function

    Public Function PopulateColors() As List(Of String)
        Dim colors As New List(Of String)
        For Each knownColor As KnownColor In [Enum].GetValues(GetType(KnownColor))
            Dim color As Color = color.FromKnownColor(knownColor)
            If Not color.IsSystemColor Then
                colors.Add(color.ToString().Replace("Color [", "").Replace("]", ""))
            End If
        Next
        Return colors
    End Function


    Friend Sub NoRights(ByVal hdr As Panel, ByVal gb As GroupBox)
        For Each ctl As Control In hdr.Controls
            Select Case ctl.Name
                Case "btnClose", "lblHeader"
                Case Else : ctl.Visible = False
            End Select
        Next
        If Not gb Is Nothing Then gb.Enabled = False
    End Sub

    Friend Sub ViewingLinks(ByVal hdr As Panel, ByVal gb As GroupBox, ByVal viewing As Boolean)
        For Each ctl As Control In hdr.Controls
            Select Case ctl.Name
                Case "btnImport", "btnPreview" : ctl.Enabled = viewing
            End Select
        Next

        For Each ctl As Control In gb.Controls
            Select Case ctl.Name
                Case "lblView", "rbData", "rbLinks"
                Case Else : ctl.Enabled = viewing
            End Select
        Next
    End Sub

    Friend Sub ViewingLUs(ByVal hdr As Panel, ByVal lu As Panel, ByVal data As Panel, ByVal viewing As Boolean)
        For Each ctl As Control In hdr.Controls
            Select Case ctl.Name
                Case "lblHeader"
                Case Else : ctl.Enabled = viewing
            End Select
        Next

        lu.Visible = Not viewing
        data.Visible = viewing
    End Sub

    Friend Sub ReloadXML(ByVal ds As DataSet, ByVal path As String)
        For Each dt As DataTable In ds.Tables
            dt.Rows.Clear()
        Next
        ds.ReadXml(path, XmlReadMode.ReadSchema)
    End Sub

    Friend Sub ChangesMade(ByVal btn As Button, ByVal chk As CheckBox, ByRef IsNotChanged As Boolean)
        IsNotChanged = True
        btn.ForeColor = Color.White
        btn.BackColor = Color.Red
        If Not chk Is Nothing Then chk.Checked = False
    End Sub

    Friend Sub SaveMade(ByVal btn As Button, ByRef IsNotChanged As Boolean)
        IsNotChanged = False
        btn.ForeColor = SystemColors.Window
        btn.BackColor = SystemColors.Highlight
    End Sub

    Friend Function OpenExcel() As Object
        Dim MyExcel As Object
        Dim culture As New ProfileEnvironment.Culture
        Try
            MyExcel = GetObject(, "Excel.Application")
        Catch ex As Exception
            MyExcel = CreateObject("Excel.Application")
            ExcelWasNotRunning = True
        End Try
        modGeneral.ci = System.Threading.Thread.CurrentThread.CurrentCulture

        'TODO:
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")



        Return MyExcel
    End Function

    Friend Function OpenWorkbook(ByVal MyExcel As Object, ByVal FileName As String) As Object
        Try
            Return MyExcel.workbooks.open(FileName, False, True, , , , , , , , False)
        Catch ex As Exception
            ProfileMsgBox("CRIT", "NOWB", FileName)
            CloseExcel(MyExcel, Nothing, Nothing)
            Return Nothing
        End Try
    End Function

    Friend Function OpenSheet(ByVal MyExcel As Object, ByVal MyWB As Object, ByVal SheetName As String) As Object
        Try
            Return MyWB.Worksheets(SheetName)
        Catch ex As Exception
            ProfileMsgBox("CRIT", "NOWS", "")
            CloseExcel(MyExcel, MyWB, Nothing)
            Return Nothing
        End Try
    End Function

    Friend Sub CloseExcel(ByRef MyExcel As Object, ByRef MyWB As Object, ByRef MyWS As Object)

        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyWS)
        Catch
        Finally
            MyWS = Nothing
        End Try
        GC.WaitForPendingFinalizers()

        Try
            MyWB.Close(False)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyWB)
        Catch
        Finally
            MyWB = Nothing
        End Try
        GC.WaitForPendingFinalizers()

        If ExcelWasNotRunning Then MyExcel.Quit()

        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyExcel)
        Catch ex As Exception
        Finally
            MyExcel = Nothing
        End Try
        GC.WaitForPendingFinalizers()

        GC.Collect()

        Try
            'rrh 20091216 System.Threading.Thread.CurrentThread.CurrentCulture = modGeneral.ci
        Finally
        End Try

        ExcelWasNotRunning = False
    End Sub

    Friend Function FormatDoubles(ByVal val As Double, ByVal DecNo As Integer) As String
        Select Case DecNo
            Case 0 : FormatDoubles = Format(val, "#,##0")
            Case 1 : FormatDoubles = Format(val, "#,##0.0")
            Case 2 : FormatDoubles = Format(val, "N")
            Case Else : FormatDoubles = Format(val, "#,##0.0")
        End Select
    End Function

    Friend Function IsNull(ByVal tmpValue As Object) As Double
        If DBNull.Value.Equals(tmpValue) Then IsNull = 0 Else IsNull = CType(tmpValue, Double)
    End Function

    Friend Function strToNull(ByVal str As String) As Object
        If str = "" Then strToNull = DBNull.Value Else strToNull = str
    End Function

    Friend Function nz(ByVal dgv As DataGridView, ByVal intColumn As Integer, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) As Single
        If ((dgv.Item(intColumn, e.RowIndex).Value Is System.DBNull.Value) _
            Or (Trim(dgv.Item(intColumn, e.RowIndex).ToString()) = "")) Then
            nz = CSng(0.0)
        Else
            nz = CSng(dgv.Item(intColumn, e.RowIndex).Value)
        End If
    End Function

    Friend Function GetClientKey() As String
        If _clientKey.Length < 1 Then
            If IO.File.Exists(modAppDeclarations.pathCert & Policy.GetFileName()) Then
                Using reader As New IO.StreamReader(modAppDeclarations.pathCert & Policy.GetFileName())
                    If reader.Peek <> -1 Then
                        _clientKey = reader.ReadLine
                    Else
                        Throw New Exception("A key was not found")
                    End If
                End Using
            End If
        End If
        Return _clientKey
    End Function

    Public Sub T(message As String, Optional startOver As Boolean = False)
        Try
            Dim path As String = "C:\Temp\ProfileTracing.txt"
            If File.Exists(path) Then
                Using writer As New StreamWriter(path, True) ' Not startOver)
                    If startOver Then
                        writer.WriteLine(DateTime.Now().ToString())
                    End If
                    writer.WriteLine(message)
                End Using
            End If
        Catch
        End Try
    End Sub

    Public Function GetWcfReportHeading(reportCode As String, scenario As String, company As String, location As String) As String
        Dim stringBuilder As New System.Text.StringBuilder()
        stringBuilder.Append("<table cellSpacing=" & Chr(34) & "0" & Chr(34) & " cellPadding=" & Chr(34) & "0" & Chr(34) & " width=" & Chr(34) & "100%" & Chr(34) & " border=" & Chr(34) & "0" & Chr(34) & "><tr><td style=" & Chr(34) & "BACKGROUND-REPEAT: no-repeat" & Chr(34) & " width=" & Chr(34) & "350" & Chr(34) & " ")
        stringBuilder.Append(" background = " & Chr(34) & My.Application.Info.DirectoryPath.ToString() & "\Resources\ReportBGLeft.gif" & Chr(34) & " ")
        stringBuilder.Append("bgColor=" & Chr(34) & "white" & Chr(34) & "><DIV id=" & Chr(34) & "LabelName" & Chr(34) & " style=" & Chr(34) & "DISPLAY: inline; FONT-WEIGHT: bold; FONT-SIZE: 12pt; Z-INDEX: 106; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & ">")
        stringBuilder.Append(reportCode) ' 'Operating Expenses")
        stringBuilder.Append("</DIV><br><DIV id=" & Chr(34) & "LabelMonth" & Chr(34) & " style=" & Chr(34) & "DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & ">")
        stringBuilder.Append(scenario.Trim() & ": Methodology")
        stringBuilder.Append("</DIV><br><DIV id=" & Chr(34) & "LabelDataFilter" & Chr(34) & " style=" & Chr(34) & "DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & ">")
        stringBuilder.Append(company & " - " & location)
        stringBuilder.Append("</DIV></td><TD></TD><td width=" & Chr(34) & "42%" & Chr(34) & "><div align=" & Chr(34) & "right" & Chr(34) & "><IMG height=" & Chr(34) & "50" & Chr(34) & " alt=" & Chr(34) & Chr(34) & " ")
        stringBuilder.Append("src=" & Chr(34) & My.Application.Info.DirectoryPath.ToString() & "\Resources\SA logo RECT.jpg" & Chr(34) & " ")
        stringBuilder.Append(" width=" & Chr(34) & "300" & Chr(34) & "><br>")
        stringBuilder.Append("</div><DIV id=" & Chr(34) & "DIV1" & Chr(34) & " style=" & Chr(34) & "FONT-SIZE: 10pt; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & ">")
        stringBuilder.Append(DateTime.Today.ToLongDateString())
        stringBuilder.Append("</DIV></td></tr></table><hr width=" & Chr(34) & "100%" & Chr(34) & " color=" & Chr(34) & "#000000" & Chr(34) & " SIZE=" & Chr(34) & "1" & Chr(34) & ">")
        Return stringBuilder.ToString()
    End Function

    Public Function GetQuerystringVariable(queryString As String, key As String) As String
        Try
            Dim qsParts As String() = queryString.Split("&")
            For i As Integer = 0 To qsParts.Length - 1
                Dim parts As String() = qsParts(i).Split("=")
                If parts(0).ToUpper() = key.ToUpper() Then
                    Return parts(1)
                End If
            Next
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

End Module
