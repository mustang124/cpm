Option Explicit On

Friend Class SA_RefineryInfo
    Private dvEDC As New DataView
    Private dvTargets As New DataView
    Private dvHistory As New DataView
    Private dvInventory As New DataView
    Private CStartDate As Date
    Private CEndDate As Date
    Private ColName As String
    Private USText As String
    Private MetText As String
    Private MyParent As Main

    Private RowFilter As String
    Private IsLoaded, IsFirstStart, HasRights As Boolean
    Friend RefineryInformationNotSaved As Boolean

    Friend Sub SetUOMandCurrency(ByVal tmpUOM As String, ByVal Currency As String)
        If tmpUOM.StartsWith("US Units") Then
            dgRefTargets.Columns("AxisLabelUS").Visible = True
            dgRefTargets.Columns("AxisLabelMetric").Visible = False
        Else
            dgRefTargets.Columns("AxisLabelUS").Visible = False
            dgRefTargets.Columns("AxisLabelMetric").Visible = True
        End If

        Dim cmbRef As DataGridViewComboBoxColumn = CType(dgRefTargets.Columns("cmbCurrencyCode"), DataGridViewComboBoxColumn)
        cmbRef.Items.Clear()
        cmbRef.Items.Add("USD")
        If Trim(Currency) <> "USD" Then cmbRef.Items.Add(Trim(Currency))

        lb1KCurr.Text = "--- (" & Currency & "/1000) ---"
    End Sub

    Friend Sub SetDataSet()
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.RefineryInformationRights

        pageStablizers.Enabled = HasRights

        dvEDC.Table = MyParent.dsRefineryInformation.Tables("EDCStabilizers")
        dvEDC.Sort = "EffDate DESC"

        BindControls(dvEDC)

        With dvTargets
            .Table = MyParent.dsRefineryInformation.Tables("RefTargets")
            .Sort = "SortKey ASC"
            .RowFilter = "SectionHeader = 'Average Data' and ChartTitle <> 'EDC' and ChartTitle <> 'UEDC'"
        End With

        dgRefTargets.DataSource = dvTargets

        With dvHistory
            .Table = MyParent.dsRefineryInformation.Tables("MaintRoutHist")
            .Sort = "PeriodStart DESC"
            .AllowNew = False
        End With

        dgMaintHist.DataSource = dvHistory
        PopulateYears()

        dvInventory.Table = MyParent.dsRefineryInformation.Tables("Inventory")
        dgInventory.DataSource = dvInventory

        RefineryInformationNotSaved = False
        IsLoaded = True
    End Sub

    Private Sub BindControls(ByVal dv As DataView)
        With lbPeriods
            .DisplayMember = "EffDate"
            .ValueMember = "EffDate"
            .DataSource = dv
        End With

        Dim dv_LU As New DataView(MyParent.dsLoadedMonths.Tables(0))
        With EffDate
            .DisplayMember = "PeriodStart"
            .ValueMember = "PeriodStart"
            .DataSource = dv_LU
            .DataBindings.Add("SelectedValue", dv, "EffDate")
        End With

        Dim ctl As Control
        Dim tb As TextBox
        Dim b As Binding
        For Each ctl In pageStablizers.Controls
            If TypeOf ctl Is TextBox Then
                tb = CType(ctl, TextBox)
                b = ctl.DataBindings.Add("Text", dv, ctl.Name)
                AddHandler b.Format, AddressOf FormatDoubles_Handler
                AddHandler tb.Enter, AddressOf TB_Edit
            End If
        Next

    End Sub

    Private Sub FormatDoubles_Handler(ByVal sender As Object, ByVal cevent As ConvertEventArgs)
        Try
            cevent.Value = Format(cevent.Value, "#,##0")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If RefineryInformationNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Refinery Information")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveRefineryInformation()
                Case DialogResult.No
                    MyParent.dsRefineryInformation.RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, RefineryInformationNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim row As DataRow
        Dim result As DialogResult

        If dvEDC.Count > 0 AndAlso MyParent.PeriodStart = CType(dvEDC.Item(0)!EffDate, DateTime) Then
            ProfileMsgBox("CRIT", "TWOPERIODS", "")
            Exit Sub
        End If

        ProfileMsgBox("YN", "PREVPERIODS", "")
        If result = DialogResult.Cancel Then Exit Sub
        With MyParent.dsRefineryInformation.Tables("EDCStabilizers")
            row = .NewRow
            row!EffDate = MyParent.PeriodStart
            If result = DialogResult.Yes Then
                row!AnnInputBbl = dvEDC.Item(lbPeriods.SelectedIndex)!AnnInputBbl
                row!AnnCokeBbl = dvEDC.Item(lbPeriods.SelectedIndex)!AnnCokeBbl
                row!AnnElecConsMWH = dvEDC.Item(lbPeriods.SelectedIndex)!AnnElecConsMWH
                row!AnnRSCRUDE_RAIL = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSCRUDE_RAIL
                row!AnnRSCRUDE_TT = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSCRUDE_TT
                row!AnnRSCRUDE_TB = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSCRUDE_TB
                row!AnnRSCRUDE_OMB = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSCRUDE_OMB
                row!AnnRSCRUDE_BB = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSCRUDE_BB
                row!AnnRSPROD_RAIL = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSPROD_RAIL
                row!AnnRSPROD_TT = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSPROD_TT
                row!AnnRSPROD_TB = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSPROD_TB
                row!AnnRSPROD_OMB = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSPROD_OMB
                row!AnnRSPROD_BB = dvEDC.Item(lbPeriods.SelectedIndex)!AnnRSPROD_BB
            End If
            .Rows.Add(row)
        End With
        lbPeriods.SelectedIndex = 0
        ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
    End Sub

    Private Sub TB_Edit(ByVal sender As Object, ByVal e As System.EventArgs)
        If HasRights Then
            ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
        End If
    End Sub

    Friend Sub LoadingForm()
        ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
    End Sub

    Private Sub HandleNothingSelected()
        ProfileMsgBox("CRIT", "SELECT", "period")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim IsInvalid As Boolean = False

        If (dvEDC.Count > 0) Then

            Dim NewDV As New DataView
            NewDV = dvEDC

            Dim tmp_dt As Date = CDate(NewDV.Item(0)!EffDate)

            If NewDV.Count > 1 Then
                For i As Integer = 1 To NewDV.Count - 1
                    If tmp_dt <= CType(NewDV.Item(i)!EffDate, DateTime) Then
                        IsInvalid = True
                        Exit For
                    End If
                Next i
            End If

        End If

        If IsInvalid Then
            ProfileMsgBox("CRIT", "TWOPERIODS", "")
        Else
            MyParent.SaveRefineryInformation()
        End If

    End Sub

    Private Sub EffDate_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles EffDate.SelectionChangeCommitted
        If IsLoaded Then ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)

    End Sub

    Private Sub SA_EDCStabilizers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.btnSave.TabIndex = 0
        Me.btnAdd.TabIndex = 1
        Me.btnClear.TabIndex = 2

        Me.lbPeriods.TabIndex = 3
        Me.EffDate.TabIndex = 4
        Me.AnnInputBbl.TabIndex = 5
        Me.AnnElecConsMWH.TabIndex = 6
        Me.AnnCokeBbl.TabIndex = 7

        Me.AnnRSCRUDE_RAIL.TabIndex = 8
        Me.AnnRSPROD_RAIL.TabIndex = 9
        Me.AnnRSCRUDE_TT.TabIndex = 10
        Me.AnnRSPROD_TT.TabIndex = 11
        Me.AnnRSCRUDE_TB.TabIndex = 12
        Me.AnnRSPROD_TB.TabIndex = 13
        Me.AnnRSCRUDE_OMB.TabIndex = 14
        Me.AnnRSPROD_OMB.TabIndex = 15
        Me.AnnRSCRUDE_BB.TabIndex = 16
        Me.AnnRSPROD_BB.TabIndex = 17

        Me.btnPreview.TabIndex = 18

        Me.btnClose.TabIndex = 19

    End Sub

    Private Sub dgRefTargets_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgRefTargets.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub cmbDesc_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDesc.SelectedValueChanged
        dvTargets.RowFilter = ""
        dvTargets.RowFilter = "SectionHeader = '" & cmbDesc.SelectedItem.ToString & "' and ChartTitle <> 'EDC' and ChartTitle <> 'UEDC'"
    End Sub

    Private Sub PopulateYears()
        Dim i As Integer
        Dim Rout1, Rout2, Matl1, Matl2 As Double
        For i = 0 To 23
            If i < 12 Then
                Try
                    Rout1 = Rout1 + CDbl(dvHistory.Item(i)!RoutCostLocal)
                Catch ex As Exception
                    Rout1 = Rout1
                End Try
                'Try
                '    Matl1 = Matl1 + CDbl(dvHistory.Item(i)!RoutMatlLocal)
                'Catch ex As Exception
                '    Matl1 = Matl1
                'End Try
            Else
                Try
                    Rout2 = Rout2 + CDbl(dvHistory.Item(i)!RoutCostLocal)
                Catch ex As Exception
                    Rout2 = Rout2
                End Try
                'Try
                '    Matl2 = Matl2 + CDbl(dvHistory.Item(i)!RoutMatlLocal)
                'Catch ex As Exception
                '    Matl2 = Matl2
                'End Try
            End If
        Next

        txtYearTot1.Text = FormatDoubles(Rout1, 1)
        txtYearTot2.Text = FormatDoubles(Rout2, 1)
        txtYearMat1.Text = FormatDoubles(Matl1, 1)
        txtYearMat2.Text = FormatDoubles(Matl2, 1)

    End Sub

    Private Function GetDaysInYear(ByVal IsFirst As Boolean) As Double
        Dim i, j, k As Integer
        If IsFirst Then
            j = 0
            k = 11
        Else
            j = 12
            k = 23
        End If
        Dim days As Integer
        Dim cmonth As Date
        For i = j To k
            cmonth = CDate(dvHistory.Item(i)!PeriodStart)
            days = days + Date.DaysInMonth(Year(cmonth), Month(cmonth))
        Next
        Return days
    End Function

    Private Sub chkIncrement_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncrement.CheckedChanged
        Dim IsChkd As Boolean = chkIncrement.Checked
        Dim fcol, col, tfcol, tcol As Color

        If IsChkd Then btnSubmit.Text = "Submit" Else btnSubmit.Text = "Refresh"

        If IsChkd = False Then
            col = Color.White
            fcol = Color.MediumBlue
            tcol = SystemColors.Control
            tfcol = Color.FromArgb(CType(64, Byte), CType(64, Byte), CType(64, Byte))
        Else
            tcol = Color.White
            tfcol = Color.MediumBlue
            col = Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
            fcol = Color.FromArgb(CType(64, Byte), CType(64, Byte), CType(64, Byte))
        End If

        dgMaintHist.Columns("RoutCostLocal").ReadOnly = IsChkd
        dgMaintHist.Columns("RoutCostLocal").DefaultCellStyle.BackColor = col
        dgMaintHist.Columns("RoutCostLocal").DefaultCellStyle.ForeColor = fcol
        'dgMaintHist.Columns("RoutMatlLocal").ReadOnly = IsChkd
        'dgMaintHist.Columns("RoutMatlLocal").DefaultCellStyle.BackColor = col
        'dgMaintHist.Columns("RoutMatlLocal").DefaultCellStyle.ForeColor = fcol

        txtYearTot1.ReadOnly = Not (IsChkd)
        txtYearMat1.ReadOnly = Not (IsChkd)
        txtYearTot2.ReadOnly = Not (IsChkd)
        txtYearMat2.ReadOnly = Not (IsChkd)
        txtYearTot1.BackColor = tcol
        txtYearMat1.BackColor = tcol
        txtYearTot2.BackColor = tcol
        txtYearMat2.BackColor = tcol
        txtYearTot1.ForeColor = tfcol
        txtYearMat1.ForeColor = tfcol
        txtYearTot2.ForeColor = tfcol
        txtYearMat2.ForeColor = tfcol
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If chkIncrement.Checked Then
            Dim i As Integer
            Dim cmonth As Date
            Dim DaysInYear1 As Integer = CInt(GetDaysInYear(True))
            Dim DaysInYear2 As Integer = CInt(GetDaysInYear(False))
            Dim DaysInMonth As Integer
            For i = 0 To 23
                cmonth = CDate(dvHistory.Item(i)!PeriodStart)
                DaysInMonth = Date.DaysInMonth(Year(cmonth), Month(cmonth))
                Try
                    If i < 12 Then
                        dvHistory.Item(i)!RoutCostLocal = DaysInMonth * CDbl(txtYearTot1.Text) / DaysInYear1
                    Else
                        dvHistory.Item(i)!RoutCostLocal = DaysInMonth * CDbl(txtYearTot2.Text) / DaysInYear2
                    End If
                Catch ex As Exception
                    dvHistory.Item(i)!RoutCostLocal = 0
                End Try
                'Try
                '    If i < 12 Then
                '        dvHistory.Item(i)!RoutMatlLocal = DaysInMonth * CDbl(txtYearMat1.Text) / DaysInYear1
                '    Else
                '        dvHistory.Item(i)!RoutMatlLocal = DaysInMonth * CDbl(txtYearMat2.Text) / DaysInYear2
                '    End If
                'Catch ex As Exception
                '    dvHistory.Item(i)!RoutMatlLocal = 0
                'End Try
            Next
            Try
                txtYearTot1.Text = FormatDoubles(CDbl(txtYearTot1.Text), 1)
            Catch ex As Exception
                txtYearTot1.Text = ""
            End Try
            Try
                txtYearTot2.Text = FormatDoubles(CDbl(txtYearTot2.Text), 1)
            Catch ex As Exception
                txtYearTot2.Text = ""
            End Try
            Try
                txtYearMat1.Text = FormatDoubles(CDbl(txtYearMat1.Text), 1)
            Catch ex As Exception
                txtYearTot1.Text = ""
            End Try
            Try
                txtYearMat2.Text = FormatDoubles(CDbl(txtYearMat2.Text), 1)
            Catch ex As Exception
                txtYearMat2.Text = ""
            End Try
        Else
            PopulateYears()
        End If
        ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
    End Sub

    Private Sub dgMaintHist_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgMaintHist.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim result As DialogResult
        Select Case tcRefInfo.SelectedTab.Name
            Case pageTargets.Name
                result = ProfileMsgBox("YN", "CLEAR", "performance target tables")
                If result = vbOK Then
                    Dim row As DataRow
                    For Each row In MyParent.dsRefineryInformation.Tables("RefTargets").Rows
                        row!Target = DBNull.Value
                    Next

                    For Each row In MyParent.dsRefineryInformation.Tables("UnitTargets").Rows
                        row!MechAvail = DBNull.Value
                        row!OnStream = DBNull.Value
                        row!OpAvail = DBNull.Value
                        row!UtilPcnt = DBNull.Value
                        row!RoutCost = DBNull.Value
                        row!TACost = DBNull.Value
                    Next
                    ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
                End If
            Case pageStablizers.Name
                Dim i As Integer = lbPeriods.SelectedIndex
                On Error GoTo NothingSelected
                result = ProfileMsgBox("YN", "DELETE", "selected period")
                If result = DialogResult.Yes Then
                    dvEDC.Item(i).Delete()
                    ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
                End If
                Exit Sub
            Case pageHistory.Name
                result = ProfileMsgBox("YN", "CLEAR", "historical maintenance costs")
                If result = DialogResult.Yes Then
                    Dim i As Integer
                    For i = 0 To dvHistory.Count - 1
                        dvHistory.Item(i)!RoutCostLocal = DBNull.Value
                        'dvHistory.Item(i)!RoutMatlLocal = DBNull.Value
                    Next
                    PopulateYears()
                    ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
                End If
            Case pageInventory.Name
                result = ProfileMsgBox("YN", "CLEAR", "inventory")
                If result = DialogResult.Yes Then
                    Dim i As Integer
                    For i = 0 To dvInventory.Count - 1
                        dvHistory.Item(i)!NumTanks = DBNull.Value
                        dvHistory.Item(i)!FuelsStorage = DBNull.Value
                    Next
                    ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
                End If
        End Select
        Exit Sub
NothingSelected:
        HandleNothingSelected()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Select Case tcRefInfo.SelectedTab.Name
            Case pageTargets.Name
                MyParent.PrintPreviews("Performance Targets", "Refinery Level")
            Case pageStablizers.Name
                MyParent.PrintPreviews("EDC Stabilizers", Nothing)
            Case pageHistory.Name
                MyParent.PrintPreviews("Historical Maintenance Costs", Nothing)
        End Select
    End Sub

    Private Sub tcRefInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcRefInfo.SelectedIndexChanged
        Select Case tcRefInfo.SelectedTab.Name
            Case pageInventory.Name : btnPreview.Enabled = False
            Case Else : btnPreview.Enabled = True
        End Select
    End Sub

    Private Sub dgInventory_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgInventory.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, Nothing, RefineryInformationNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub txtYearTot1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtYearTot1.Leave
        If sender.readonly = False Then PopulateYears()
    End Sub

    Private Sub txtYearTot2_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtYearTot2.Leave
        If sender.readonly = False Then PopulateYears()
    End Sub

    Private Sub txtYearMat1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtYearMat1.Leave
        If sender.readonly = False Then PopulateYears()
    End Sub

    Private Sub txtYearMat2_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtYearMat2.Leave
        If sender.readonly = False Then PopulateYears()
    End Sub

    Private Sub dgRefTargets_RowPrePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles dgRefTargets.RowPrePaint
        With dgRefTargets
            .Columns("ChartTitle").DisplayIndex = 0
            .Columns("AxisLabelUS").DisplayIndex = 1
            .Columns("AxisLabelMetric").DisplayIndex = 2
            .Columns("Target").DisplayIndex = 3
            .Columns("cmbCurrencyCode").DisplayIndex = 4
            .Columns("PropertyRef").DisplayIndex = 5
            .Columns("SortKeyRef").DisplayIndex = 6
            .Columns("SectionHeader").DisplayIndex = 7
        End With
    End Sub

    Private Sub dgRefTargets_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgRefTargets.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgInventory_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgInventory.DataError
        dgvDataError(sender, e)
    End Sub

    Private Sub dgMaintHist_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgMaintHist.DataError
        dgvDataError(sender, e)
    End Sub
End Class
