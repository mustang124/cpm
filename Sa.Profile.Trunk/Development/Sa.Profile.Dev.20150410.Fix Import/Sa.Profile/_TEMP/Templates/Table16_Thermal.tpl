<!-- config-start -->
FILE=_CONFIG/Energy.xml;
FILE=_REF/Energy_LU.xml;

<!-- config-end -->
<!-- template-start -->
 
<table class='small' width="100%" border="0">
  <tr>
    <td colspan=8></td>
  </tr>
  <tr>
    <td width=2%></td>
    <td width=2%></td>
    <td width=51%></td>
    <td width=10% align=center valign=bottom><strong>Thermal Energy<br />USEUNIT('MBtu','GJ')/month</strong></td>
    <td width=5%></td>
    <td width=8% align=center valign=bottom><strong>Energy Cost<br />CurrencyCode/USEUNIT('MBtu','GJ')</strong></td>
    <td width=7%></td>
    <td width=15%></td>
  </tr>
  SECTION(Energy,,)
	RELATE(Energy_LU,Energy,SortKey,SortKey)
  BEGIN    
  HEADER('
   <tr>
    <td colspan=8 align=left valign=bottom height=30><strong>@Header</strong></td>
  </tr> ')
 HEADER('
   <tr>
    <td></td>
    <td colspan=7>@TransTypeDesc</td>
  </tr> ')
  <tr>
    <td colspan=2></td>
    <td>@EnergyTypeDesc</td>
    <td align="right">Format(RptSource,'#,##0')</td>
    <td></td>
    <td align="right">Format(RptPriceLocal,'N')</td>
    <td colspan=2></td>
  </tr>
  END
</table>

<!-- template-end -->
