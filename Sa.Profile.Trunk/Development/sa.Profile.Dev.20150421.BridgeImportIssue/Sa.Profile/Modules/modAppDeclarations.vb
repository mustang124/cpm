Option Compare Binary
Option Explicit On
Option Strict On

' 20081001 RRH Set the application paths at program statup

Friend Module modAppDeclarations

#Region " Path Declarations and Settings "

    Friend ReadOnly pathStartUp As String = Application.StartupPath & "\"
    Friend ReadOnly pathXMLData As String = pathStartUp & "XMLData\"

    ' These directories use a relative reference from the installation directory.
    Friend ReadOnly pathBridge As String = pathStartUp & "_BRIDGE\"
    Friend ReadOnly pathRef As String = pathStartUp & "_REF\"
    Friend ReadOnly pathTemp As String = pathStartUp & "_TEMP\"

    'Friend ReadOnly pathData As String = setPathData(pathData)
    'Friend ReadOnly pathData As String = Application.UserAppDataPath & "\"
    Friend ReadOnly pathData As String = pathStartUp

    Friend ReadOnly pathMonth As String = pathData
    Friend ReadOnly pathCert As String = pathData & "_CERT\"
    Friend ReadOnly pathAdmin As String = pathData & "_ADMIN\"
    Friend ReadOnly pathConfig As String = pathData & "_CONFIG\"
    Friend ReadOnly pathEnvironment As String = pathData & "_ENVIRONMENT\"

#Region " Modify Paths "

    Private Function setPathData(ByVal currentPath As String) As String

        Dim dsPathData As New DataSet

        dsPathData.ReadXml(pathXMLData & "dataPathSetting.xml")
        setPathData = dsPathData.Tables(0).Rows(0)!pathDataLocation.ToString()

        ' if file does not contain a path then set a path
        If (setPathData = "") Then
            setPathData = verEndSlash(changePathData(currentPath, pathStartUp.ToString()))
        Else
            ' validate path from file exisits
            If Not (verPathExists(setPathData)) Then
                If MessageBox.Show("The directory " & setPathData & " has been deleted.  Do you want to select another data directory.", My.Application.Info.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    setPathData = verEndSlash(changePathData(currentPath, pathStartUp.ToString()))
                End If
            End If
        End If

        dsPathData.Dispose()

    End Function

    Friend Function changePathData(ByVal currentPath As String, _
                                   ByVal notInThisFolder As String) As String

        changePathData = verEndSlash(currentPath)

        Dim fldrBrowser As New System.Windows.Forms.FolderBrowserDialog

        fldrBrowser.Description = "Please select a folder for the data."
        fldrBrowser.ShowNewFolderButton = False

        If fldrBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then

            Dim dsPathData As New DataSet
            dsPathData.ReadXml(pathXMLData & "dataPathSetting.xml")

            dsPathData.Tables(0).Rows(0)!pathDataLocation = verEndSlash(fldrBrowser.SelectedPath)
            dsPathData.Tables(0).Rows(0)!pathAppPathOK = "False"

            If (validatePathData(dsPathData, notInThisFolder)) Then
                dsPathData.AcceptChanges()
                dsPathData.WriteXml(pathXMLData & "dataPathSetting.xml")
                changePathData = verEndSlash(fldrBrowser.SelectedPath)
            Else
                dsPathData.RejectChanges()
            End If

            dsPathData.Dispose()

        End If

        If (Not verPathExists(changePathData)) Then
            MessageBox.Show("The directory is invalid.  Please chose a valid direcotry." & ControlChars.CrLf & changePathData(currentPath, changePathData), My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
            changePathData = verEndSlash(changePathData(currentPath, pathStartUp.ToString()))
        End If

        fldrBrowser.Dispose()

    End Function

    Private Function validatePathData(ByVal dsPathData As DataSet, ByVal notInThisFolder As String) As Boolean

        validatePathData = False

        If ((dsPathData.Tables(0).Rows(0)!pathAppPathOK.ToString() = "False") And _
            (verEndSlash(dsPathData.Tables(0).Rows(0)!pathDataLocation.ToString()) = verEndSlash(notInThisFolder))) Then

            If MessageBox.Show("The data path is: " & verEndSlash(notInThisFolder) & "." & ControlChars.CrLf & "Is this OK?", My.Application.Info.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then
                dsPathData.Tables(0).Rows(0)!pathAppPathOK = "True"
                validatePathData = True
            Else
                changePathData(dsPathData.Tables(0).Rows(0)!pathDataLocation.ToString(), notInThisFolder)
            End If

        ElseIf dsPathData.Tables(0).Rows(0)!pathDataLocation.ToString() = "" Then

            changePathData(dsPathData.Tables(0).Rows(0)!pathDataLocation.ToString(), notInThisFolder)

        Else

            validatePathData = True

        End If

    End Function

    Friend Function verEndSlash(ByVal strPath As String) As String

        If (Right(strPath, 1) <> "\") Then verEndSlash = strPath & "\" Else verEndSlash = strPath

    End Function

    Private Function verPathExists(ByVal strPath As String) As Boolean

        verPathExists = IO.Directory.Exists(strPath)

    End Function

#End Region ' Modify Paths

#End Region ' Paths

#Region " Rights Constants "

    Friend ReadOnly strRightsAdmin As String = "Administrator - Full Access"
    Friend ReadOnly strRightsRefinery As String = "Refinery Information - Performance Targets, Inventory, EDC Stabilizers & Historical Costs"
    Friend ReadOnly strRightsConfiguration As String = "Configuration - Process Facilities, Turnarounds & Performance Targets"
    Friend ReadOnly strRightsProcess As String = "Process Facilities - Tables 1, 2 & 10 "
    Friend ReadOnly strRightsInventory As String = "Inventory - Storage And Transport"
    Friend ReadOnly strRightsOpEx As String = "Operating Expenses - Table 4"
    Friend ReadOnly strRightsPersonnel As String = "Personnel - Tables 5, 6 & 7"
    Friend ReadOnly strRightsCrude As String = "Crude Charge Detail - Table 14"
    Friend ReadOnly strRightsMaterial As String = "Material Balance - Table 15"
    Friend ReadOnly strRightsEnergy As String = "Energy - Table 16"
    Friend ReadOnly strRightsUser As String = "User-Defined Inputs/Outputs"
    Friend ReadOnly strRightsView As String = "View Results - Reports and Charts"

#End Region ' Rights Constants

End Module
