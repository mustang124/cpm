Option Explicit On

Friend Class SA_Import

#Region " Windows Form Designer generated code "

    Inherits System.Windows.Forms.UserControl

    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents tcAdmin As System.Windows.Forms.TabControl
    Private WithEvents pagePrivileges As System.Windows.Forms.TabPage
    Friend WithEvents chkPrivileges As System.Windows.Forms.CheckBox
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents lblHeaderImport As System.Windows.Forms.Label
    Friend WithEvents CB As System.Windows.Forms.ColumnHeader
    Friend WithEvents InputForms As System.Windows.Forms.ColumnHeader
    Friend WithEvents Status As System.Windows.Forms.ColumnHeader
    Friend WithEvents Workbook As System.Windows.Forms.ColumnHeader
    Friend WithEvents Worksheet As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents clbUsers As System.Windows.Forms.CheckedListBox

    Public Sub New()
        MyBase.New()

        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

#Region "The following procedure is required by the Windows Form Designer"

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_Import))
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"", "Process Facilities", "Ready", "C:\ProfileII_Bridge.xls", "Process Facilities"}, -1)
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.tcAdmin = New System.Windows.Forms.TabControl
        Me.pagePrivileges = New System.Windows.Forms.TabPage
        Me.ListView1 = New System.Windows.Forms.ListView
        Me.CB = New System.Windows.Forms.ColumnHeader
        Me.InputForms = New System.Windows.Forms.ColumnHeader
        Me.Status = New System.Windows.Forms.ColumnHeader
        Me.Workbook = New System.Windows.Forms.ColumnHeader
        Me.Worksheet = New System.Windows.Forms.ColumnHeader
        Me.chkPrivileges = New System.Windows.Forms.CheckBox
        Me.lblHeaderImport = New System.Windows.Forms.Label
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.tcAdmin.SuspendLayout()
        Me.pagePrivileges.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "F:\GADSNG Data Entry\AdminConsole\AdminConsole.chm"
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon_Profile.My.Resources.Resources.completeTest
        Me.btnImport.Location = New System.Drawing.Point(580, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 5
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Imports the selected input forms below")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'pnlTools
        '
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 6)
        Me.pnlTools.TabIndex = 3
        '
        'tcAdmin
        '
        Me.tcAdmin.Controls.Add(Me.pagePrivileges)
        Me.tcAdmin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcAdmin.ItemSize = New System.Drawing.Size(125, 18)
        Me.tcAdmin.Location = New System.Drawing.Point(4, 46)
        Me.tcAdmin.Name = "tcAdmin"
        Me.tcAdmin.SelectedIndex = 0
        Me.tcAdmin.Size = New System.Drawing.Size(742, 550)
        Me.tcAdmin.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcAdmin.TabIndex = 4
        '
        'pagePrivileges
        '
        Me.pagePrivileges.Controls.Add(Me.ListView1)
        Me.pagePrivileges.Controls.Add(Me.chkPrivileges)
        Me.pagePrivileges.Controls.Add(Me.lblHeaderImport)
        Me.pagePrivileges.Location = New System.Drawing.Point(4, 22)
        Me.pagePrivileges.Name = "pagePrivileges"
        Me.pagePrivileges.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pagePrivileges.Size = New System.Drawing.Size(734, 524)
        Me.pagePrivileges.TabIndex = 2
        Me.pagePrivileges.Text = "Import"
        Me.pagePrivileges.UseVisualStyleBackColor = True
        '
        'ListView1
        '
        Me.ListView1.CheckBoxes = True
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.CB, Me.InputForms, Me.Status, Me.Workbook, Me.Worksheet})
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListView1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        ListViewItem1.StateImageIndex = 0
        Me.ListView1.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})
        Me.ListView1.Location = New System.Drawing.Point(6, 36)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(722, 482)
        Me.ListView1.TabIndex = 969
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'CB
        '
        Me.CB.Text = ""
        Me.CB.Width = 23
        '
        'InputForms
        '
        Me.InputForms.Text = "Monthly Input Forms"
        Me.InputForms.Width = 167
        '
        'Status
        '
        Me.Status.Text = "Status"
        '
        'Workbook
        '
        Me.Workbook.Text = "Workbook"
        Me.Workbook.Width = 178
        '
        'Worksheet
        '
        Me.Worksheet.Text = "Worksheet"
        Me.Worksheet.Width = 200
        '
        'chkPrivileges
        '
        Me.chkPrivileges.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkPrivileges.AutoSize = True
        Me.chkPrivileges.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPrivileges.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkPrivileges.Location = New System.Drawing.Point(645, 10)
        Me.chkPrivileges.Name = "chkPrivileges"
        Me.chkPrivileges.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkPrivileges.Size = New System.Drawing.Size(83, 17)
        Me.chkPrivileges.TabIndex = 968
        Me.chkPrivileges.Text = "Check ALL"
        Me.chkPrivileges.UseVisualStyleBackColor = True
        '
        'lblHeaderImport
        '
        Me.lblHeaderImport.BackColor = System.Drawing.Color.Transparent
        Me.lblHeaderImport.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeaderImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderImport.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblHeaderImport.Location = New System.Drawing.Point(6, 0)
        Me.lblHeaderImport.Name = "lblHeaderImport"
        Me.lblHeaderImport.Size = New System.Drawing.Size(722, 36)
        Me.lblHeaderImport.TabIndex = 970
        Me.lblHeaderImport.Text = "Select the Input Forms to Import"
        Me.lblHeaderImport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1035
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(580, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Import Utility"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_Import
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tcAdmin)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "SA_Import"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.tcAdmin.ResumeLayout(False)
        Me.pagePrivileges.ResumeLayout(False)
        Me.pagePrivileges.PerformLayout()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#End Region

    Dim MyParent As Main
    Dim Version As String
	Friend Function GetBridgeVersion(tmpWB As String) As String

		Dim MyExcel As Object = OpenExcel()
		Dim MyWB As Object = OpenWorkbook(MyExcel, tmpWB)

		Dim Version As String

		Try
			Version = MyWB.Names("VERSION").value.ToString()
		Catch ex As Exception
			Version = "<Missing from Workbook>"
		End Try

		MyExcel = Nothing
		MyWB = Nothing

		Return Version
		Exit Function

	End Function

    Friend Sub SetDataSet()
        MyParent = CType(Me.ParentForm, Main)
        ListView1.Items.Clear()
        Dim tmpWB As String = ""
        Dim iDataEntry As ListViewItem
        For Each iDataEntry In MyParent.lvDataEntry.Items
            Dim iImport As ListViewItem

            Dim tmpWS As String = ""
            iImport = ListView1.Items.Add(iDataEntry.SubItems(0).Text)
            iImport.SubItems.Add(iDataEntry.SubItems(0).Text)

            Try
                iImport.SubItems.Add(iDataEntry.SubItems(1).Text)
            Catch ex As Exception
                iImport.SubItems.Add("")
            End Try

            Try
                Select Case iDataEntry.SubItems(0).Text
                    Case "Process Facilities" : tmpWB = strImportSetting(MyParent.dsMappings, "Process", "tbFilePath") ' My.Settings.workbookProcess.ToString
                    Case "Inventory" : tmpWB = strImportSetting(MyParent.dsMappings, "Inventory", "tbFilePath") ' My.Settings.workbookInventory.ToString
                    Case "Operating Expenses" : tmpWB = strImportSetting(MyParent.dsMappings, "OpEx", "tbFilePath") ' My.Settings.workbookOpex.ToString
                    Case "Personnel" : tmpWB = strImportSetting(MyParent.dsMappings, "Pers", "tbFilePath") ' My.Settings.workbookPersonnel.ToString
                    Case "Crude Charge Detail" : tmpWB = strImportSetting(MyParent.dsMappings, "Crude", "tbFilePath") ' My.Settings.workbookCrude.ToString
                    Case "Material Balance" : tmpWB = strImportSetting(MyParent.dsMappings, "Material", "tbFilePath") ' My.Settings.workbookMaterial.ToString
                    Case "Energy" : tmpWB = strImportSetting(MyParent.dsMappings, "Energy", "tbFilePath") ' My.Settings.workbookEnergy.ToString
                        'Case "User-Defined Inputs/Outputs" : tmpWB = strImportSetting(MyParent.dsMappings, "User", "tbFilePath") 'My.Settings.workbookUserDefined.ToString
                End Select
                If IO.File.Exists(tmpWB) Then
                    iImport.SubItems.Add(tmpWB).ForeColor = Color.Black
                    
                Else
                    tmpWB = "INVALID FILE PATH"
                    iImport.UseItemStyleForSubItems = False
                    iImport.SubItems.Add(tmpWB).ForeColor = Color.Red
                End If

            Catch ex As Exception
                tmpWB = "NOT SET"
                iImport.UseItemStyleForSubItems = False
                iImport.SubItems.Add(tmpWB).ForeColor = Color.Red
            End Try

            Try
                Select Case iDataEntry.SubItems(0).Text
                    Case "Process Facilities" : tmpWS = strImportSetting(MyParent.dsMappings, "Process", "cbSheets") ' My.Settings.worksheetProcess.ToString
                    Case "Inventory" : tmpWS = strImportSetting(MyParent.dsMappings, "Inventory", "cbSheets") ' My.Settings.worksheetInventory.ToString
                    Case "Operating Expenses" : tmpWS = strImportSetting(MyParent.dsMappings, "OpEx", "cbSheets") ' My.Settings.worksheetOpex.ToString
                    Case "Personnel" : tmpWS = strImportSetting(MyParent.dsMappings, "Pers", "cbSheets") ' My.Settings.worksheetPersonnel.ToString
                    Case "Crude Charge Detail" : tmpWS = strImportSetting(MyParent.dsMappings, "Crude", "cbSheets") ' My.Settings.worksheetCrude.ToString
                    Case "Material Balance" : tmpWS = strImportSetting(MyParent.dsMappings, "Material", "cbSheets") & " and " & strImportSetting(MyParent.dsMappings, "Material", "cbSheets1") ' My.Settings.worksheetMaterial1.ToString & " and " & My.Settings.worksheetMaterial2.ToString
                    Case "Energy" : tmpWS = strImportSetting(MyParent.dsMappings, "Energy", "cbSheets") ' My.Settings.worksheetEnergy.ToString
                        'Case "User-Defined Inputs/Outputs" : tmpWS = strImportSetting(MyParent.dsMappings, "User", "cbSheets") ' My.Settings.worksheetUserDefined.ToString
                End Select
                iImport.SubItems.Add(tmpWS).ForeColor = Color.Black
            Catch ex As Exception
                tmpWB = "NOT SET"
                iImport.UseItemStyleForSubItems = False
                iImport.SubItems.Add(tmpWS).ForeColor = Color.Red
            End Try
        Next
        Dim Bridge As String
        If BridgeFileExists(tmpWB) Then Bridge = GetBridgeVersion(tmpWB)
        Version = Bridge
        MyParent.dsSettings.Tables(0).Rows(0)!BridgeVersion = Version
        MyParent.dsSettings.AcceptChanges()
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim i As ListViewItem
        Dim f As System.Drawing.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)

        Dim result As DialogResult = ProfileMsgBox("YN", "UNSAVED", "the input forms following import")
        For Each i In ListView1.CheckedItems
            Dim IsReady As Boolean
            i.SubItems(2).Font = f
            If i.SubItems(3).Text = "Not set" Or i.SubItems(4).Text = "Not set" Then
                i.SubItems(2).ForeColor = Color.Red
                i.SubItems(2).Text = "Error"
                i.SubItems(2).Font = f
            Else
                If i.SubItems(2).Text = "Ready" Then
                    Dim result1 As DialogResult = ProfileMsgBox("YN", "ISREADY", i.SubItems(1).Text)
                    If result1 = DialogResult.No Then IsReady = True
                End If

                If IsReady = False Then

                    Select Case i.SubItems(1).Text

                        Case "Process Facilities"
                            If MyParent.SA_Process1.ImportProcessFacilities Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SaveProcess()

                        Case "Inventory"
                            If MyParent.SA_Inventory1.ImportInventory Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SaveInventory()

                        Case "Operating Expenses"
                            If MyParent.SA_OperatingExpenses1.importOpEx() Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SaveOpex()

                        Case "Personnel"
                            If MyParent.SA_Personnel1.importPersonnel Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SavePersonnel()

                        Case "Crude Charge Detail"
                            If MyParent.SA_CrudeCharge1.ImportCrude() Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SaveCrude()

                        Case "Material Balance"
                            If MyParent.SA_MaterialBalance1.ImportMaterials Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SaveMaterialBalance()

                        Case "Energy"
                            If MyParent.SA_Energy1.importEnergy Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SaveEnergy()

                        Case "User-Defined Inputs/Outputs"
                            If MyParent.SA_UserDefined1.ImportUserDefined Then
                                i.SubItems(2).ForeColor = Color.Green
                                i.SubItems(2).Text = "OK"
                            Else
                                i.SubItems(2).ForeColor = Color.Red
                                i.SubItems(2).Text = "Error"
                            End If
                            If result = DialogResult.Yes Then MyParent.SaveUserDefined()

                    End Select
                End If
            End If
        Next
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        MyParent.HideTopLayer(Me)
        MyParent.tcControlPanel.Enabled = True
        MyParent.MyParent.MenuFile.Enabled = True
        MyParent.MyParent.MenuEdit.Enabled = True
        MyParent.MyParent.MenuView.Enabled = True
        MyParent.MyParent.MenuTools.Enabled = True
        MyParent.MyParent.MenuWindow.Enabled = True
        MyParent.MyParent.MenuHelp.Enabled = True
        MyParent.btnWorkspace.Enabled = True
    End Sub

    Private Sub chkPrivileges_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPrivileges.CheckedChanged
        Dim i As ListViewItem
        For Each i In ListView1.Items
            If i.Text.Substring(0, 4).ToUpper <> "USER" Then
                i.Checked = chkPrivileges.Checked
            End If
        Next
    End Sub

    Private Function BridgeFileExists(ByVal path As String) As Boolean
        Return IO.File.Exists(path)
    End Function
End Class