Partial Class EIIReport
    Inherits ReportBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        BuildReport()
    End Sub

    Public Sub BuildReport()
        BuildEIIHeader()
        BuildEIIBody()
        BuildEIIFooter()
    End Sub


    Private Sub BuildEIIHeader()
        Dim refInfQry As String
        Dim dsRef As DataSet
        Dim sectionName, fieldFormat As String
        Dim o, n, t As Integer
        Dim desc, unit As String
        Dim processRows() As String = {"", "<strong>Energy Intensity Index</strong>", "Energy Daily Usage, MBtu", "Standard Energy, MBtu", "", _
                                        "<strong>Volumetric Expansion Index</strong>", "Reported Gain, bbl", "Standard Gain, bbl", "", "", "HR", _
                                        "<strong>Sensible Heat</strong>", "Gross Input Barrels", "Standard Energy", "EII Formula", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A- Crude Gravity, �API", "HR"}
        Dim columns() As String = {"", "EII", "EIIDailyUsage", "EIIStdEnergy", "", _
                                   "VEI", "VEIActualGain", "VEIStdGain", "", "", "HR", _
                                    "", "SensGrossInput", "SensHeatStdEnergy", "SensHeatFormula", "Crude", "HR"}

        Dim decPlaces() As Integer = {0, 0, 0, 0, 0, _
                                      1, 0, 0, 0, 0, 0, _
                                      0, 0, 0, 0, 1, 0}

        refInfQry = "SELECT f.SubmissionID,EII,EnergyUseDay AS EIIDailyUsage,TotStdEnergy AS EIIStdEnergy," + _
                            "VEI,EstGain AS VEIStdGain,ReportLossGain AS VEIActualGain," + _
                            "SensHeatUtilCap AS SensGrossInput, SensHeatStdEnergy ,'44-(0.23*A)' As SensHeatFormula,OffsitesUtilCap,OffsitesStdEnergy,AvgGravity AS Crude " + _
                            "FROM FactorTotCalc f,CrudeTot c WHERE c.SubmissionID=f.SubmissionID AND factorSet='" + Request.QueryString("yr") + "'" + _
                            " AND f.SubmissionID IN " + _
                            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                            "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                            Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
                            " )"
        dsRef = QueryDb(refInfQry)


        Response.Write("<br>")
        Response.Write(" <table class='small'  border=0 bordercolor='black' width=638 cellspacing=0 cellpadding=2>")


        'Build headers
        Response.Write("<tr><td>&nbsp;</td>")
        Response.Write("<td  align=right colspan=2 width=35%><strong>" + Format(CDate(Request.QueryString("sd")), "MMMM yyyy") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td></tr>")


        If dsRef.Tables.Count > 0 Then
            For n = 0 To dsRef.Tables(0).Rows.Count - 1

                For t = 0 To processRows.Length - 1
                    'Set number format
                    Select Case decPlaces(t)
                        Case 0
                            fieldFormat = "{0:#,##0}"
                        Case 1
                            fieldFormat = "{0:#,##0.0}"
                        Case 2
                            fieldFormat = "{0:N}"
                    End Select

                    If processRows(t) = "HR" Then
                        Response.Write("<tr><td colspan=3><hr width=100% color=""Black"" SIZE=1></td>")
                        GoTo skipover
                    End If

                    ' Build Description column
                    '----------------------------
                    If processRows(t) = "" Then
                        Response.Write("<tr>")
                        Response.Write("<td>&nbsp;</td>")

                    Else
                        'Description column
                        desc = processRows(t)
                        'Make sub head
                        If t > 0 Then
                            desc = SubHead(processRows(t - 1), desc)
                        End If

                        'Handle Currency
                        desc = desc.Replace("CurrencyCode", Request.QueryString("currency"))


                        Response.Write("<tr>")
                        If desc.IndexOf("<strong>") = -1 Then
                            Response.Write("<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc.Trim + "&nbsp;</td>")
                        Else
                            Response.Write("<td valign=top>" + desc + "&nbsp;</td>")
                        End If
                    End If


                    'Build Value Columns
                    ' -------------------------------
                    If columns(t) <> "" Then
                        'Current Month column
                        If dsRef.Tables(0).Columns.Contains(columns(t)) Then
                            If desc.IndexOf("<strong>") = -1 Then
                                Response.Write("<td  align=right>" + String.Format(fieldFormat, dsRef.Tables(0).Rows(n)(columns(t))).Trim + "</td><td>&nbsp;</td>")
                            Else
                                Response.Write("<td  align=right><strong>" + String.Format(fieldFormat, dsRef.Tables(0).Rows(n)(columns(t))).Trim + "</strong></td><td>&nbsp;</td>")
                            End If

                            'ElseIf processRows(t).ToUpper = "VARIABLES" And dsProcData.Tables.Count > 0 Then
                            '    'skip this line
                        Else
                            Response.Write("<td colspan=2>&nbsp;</td>")
                        End If

skipover:
                    Else
                        Response.Write("<td colspan=2>&nbsp;</td>")
                    End If
                    Response.Write("</tr>")
                Next

                'Add Blank Row 
                Response.Write("<tr><td colspan=3>&nbsp;</td></tr>")
            Next
        End If

        ' Response.Write("</table>")
    End Sub





    Private Sub BuildEIIBody()
        Dim funcQry, processDataQry, refInfQry, productYield As String
        Dim dsRef As DataSet
        Dim dsProcData As DataSet

        Dim sectionName, fieldFormat As String
        Dim o, n, t As Integer
        Dim bodyRows() As String = {"Unit Name", "Process ID", "Process Type", "Utilized Capacity, UOM", "Standard Energy, MBtu/d", "Standard Gain, BPCD", "EII Formula", "VEI Formula", "Variables"}
        Dim bodyColumns() As String = {"Unitname", "ProcessID", "ProcessType", "UtilCap", "StdEnergy", "StdGain", "EIIFormulaForReport", "VEIFormulaForReport", "Variables"}
        Dim bodyDecPlaces() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0}


        Dim processRows() As String
        Dim columns() As String
        Dim decPlaces() As Integer



        funcQry = "SELECT c.SubmissionID, p.Description,c.SortKey,c.UnitName, c.unitid, c.ProcessID, c.processType, c.utilcap, fc.stdenergy, fc.stdgain, VEIFormulaForReport, EIIFormulaForReport, displaytextUS" + _
                    " FROM Config c, factorcalc fc, factors f, processid_lu p, displayunits_lu d " + _
                    " WHERE c.unitid = fc.unitid AND c.submissionId = fc.submissionid  AND f.processid = c.processid AND f.processtype = c.processtype AND f.factorset='" + Request.QueryString("yr") + "'" + _
                    " AND f.factorset=fc.factorset AND c.processid = p.processid and p.displayunits = d.displayunits" + _
                    " AND c.PROCESSID NOT IN ('STEAMGEN','ELECGEN','FCCPOWER','FTCOGEN','BLENDING') AND c.submissionid IN " + _
                    "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                    "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                    Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
                    " ) ORDER BY c.SortKey"
        dsRef = QueryDb(funcQry)

        processDataQry = "SELECT p.SubmissionID, c.UnitID,p.Property,SAValue, UsDescription, UsDecplaces FROM processdata p, table2_LU t, config c " + _
                         " WHERE p.unitid = c.unitid AND p.property = t.property AND c.processid = t.processid  AND c.submissionid = p.submissionid" + _
                         " AND c.submissionid IN " + _
                         "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                        "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                        Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
                        " ) ORDER BY c.UnitID"

        dsProcData = QueryDb(processDataQry)


        'Response.Write(" <table class='small'  border=0 bordercolor='black' width=98% cellspacing=0 cellpadding=2>")

        'BODY 
        processRows = bodyRows
        columns = bodyColumns
        decPlaces = bodyDecPlaces

        'Build headers
        'Response.Write("<tr><td  width=45%><strong>&nbsp;</strong></td>")
        'Response.Write("<td  align=right width=45%><strong>Current Month</strong</td></tr>")


        If dsRef.Tables.Count > 0 Then
            For n = 0 To dsRef.Tables(0).Rows.Count - 1

                If n > 0 Then
                    If Not dsRef.Tables(0).Rows(n - 1)("Description").StartsWith(dsRef.Tables(0).Rows(n)("Description")) Then
                        Response.Write("<tr><td colspan=3><strong>" + dsRef.Tables(0).Rows(n)("Description").Trim + "</strong></td></tr>")
                    End If
                Else
                    Response.Write("<tr><td colspan=3><strong>" + dsRef.Tables(0).Rows(n)("Description").Trim + "</strong></td></tr>")
                End If


                'Do substititons in formulas
                Dim eiiFormula As String = dsRef.Tables(0).Rows(n)("EIIFormulaForReport").ToString
                Dim veiFormula As String = dsRef.Tables(0).Rows(n)("VEIFormulaForReport").ToString
                Dim unitID As String = dsRef.Tables(0).Rows(n)("UnitID").ToString
                Dim submissionID As String = dsRef.Tables(0).Rows(n)("SubmissionID").ToString
                Dim varRows() As DataRow = dsProcData.Tables(0).Select("UnitId='" + unitID + "' AND SubmissionID='" + submissionID + "'")

                If dsProcData.Tables(0).Rows.Count > 0 Then
                    Dim j As Integer
                    For j = 0 To varRows.Length - 1
                        eiiFormula = eiiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))
                        veiFormula = veiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))
                    Next
                    dsRef.Tables(0).Rows(n)("EIIFormulaForReport") = eiiFormula
                    dsRef.Tables(0).Rows(n)("VEIFormulaForReport") = veiFormula
                End If

                For t = 0 To processRows.Length - 1
                    'Set number format
                    Select Case decPlaces(t)
                        Case 0
                            fieldFormat = "{0:#,##0}"
                        Case 1
                            fieldFormat = "{0:#,##0.0}"
                        Case 2
                            fieldFormat = "{0:N}"
                    End Select

                    'Show Process Type only if it is available
                    If (processRows(t) = "Process Type") AndAlso _
                        (IsDBNull(dsRef.Tables(0).Rows(n)(columns(t))) Or _
                         dsRef.Tables(0).Rows(n)(columns(t)).Trim = "") Then
                        GoTo skipover 'Doing this because there is no continue in VB.NET
                    End If


                    ' Build Description column
                    '----------------------------
                    If processRows(t) = "" Then
                        Response.Write("<tr>")
                        Response.Write("<td>&nbsp;</td>")
                    ElseIf processRows(t).ToUpper = "VARIABLES" And dsProcData.Tables.Count > 0 Then
                        'skip this line
                    Else
                        'Description column
                        Dim desc, unit As String
                        desc = processRows(t).Replace("CurrencyCode", Request.QueryString("currency"))
                        unit = "DisplayTextUS"
                        desc = desc.Replace("UOM", dsRef.Tables(0).Rows(n)(unit))
                        Response.Write("<tr>")
                        Response.Write("<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc.Trim + "&nbsp;</td>")
                    End If


                    'Build Value Columns
                    ' -------------------------------
                    If columns(t) <> "" Then
                        'Current Month column
                        If dsRef.Tables(0).Columns.Contains(columns(t)) Then
                            Response.Write("<td  align=right>" + String.Format(fieldFormat, dsRef.Tables(0).Rows(n)(columns(t))).Trim + "</td><td>&nbsp;</td>")
                        ElseIf processRows(t).ToUpper = "VARIABLES" And dsProcData.Tables.Count > 0 Then
                            'skip this line
                        Else
                            Response.Write("<td colspan=2>&nbsp;</td>")
                        End If

skipover:
                    Else
                        Response.Write("<td>&nbsp</td>")
                    End If
                    Response.Write("</tr>")
                Next


                'List formula variables 
                If dsProcData.Tables(0).Rows.Count > 0 Then
                    Dim j As Integer
                    For j = 0 To varRows.Length - 1

                        Select Case varRows(j)("UsDecPlaces")
                            Case 0
                                fieldFormat = "{0:#,##0}"
                            Case 1
                                fieldFormat = "{0:#,##0.0}"
                            Case 2
                                fieldFormat = "{0:N}"
                        End Select


                        Response.Write("<tr>")
                        Response.Write("<td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + _
                                        Chr(Asc("A") + j) + " - " + varRows(j)("UsDescription").Trim.ToString + "&nbsp;</td>")
                        Response.Write("<td  align=right>" + String.Format(fieldFormat, varRows(j)("SAValue")).ToString.Trim + "</td><td>&nbsp;</td>")
                        Response.Write("</tr>")
                    Next
                End If

                'Add Blank Row 
                Response.Write("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>")

                'Display horizontal rule
                If n + 1 < dsRef.Tables(0).Rows.Count Then
                    If dsRef.Tables(0).Rows(n + 1)("Description") <> dsRef.Tables(0).Rows(n)("Description") Then
                        Response.Write("<tr><td colspan=3><hr width=100% color=""Black"" SIZE=1></td></tr>")
                    End If
                End If


            Next
        End If

        'Response.Write("</table>")
    End Sub



    Private Sub BuildEIIFooter()
        Dim refInfQry As String
        Dim dsRef As DataSet
        Dim sectionName, fieldFormat As String
        Dim o, n, t As Integer
        Dim processRows() As String = {"HR", "<strong>Asphalt</strong>", "Utilized Capacity, b/d", "Standard Energy, MBtu/d", "EII Formula", "", _
                                       "HR", "<strong>Utilities And Off-Sites</strong>", "Net Input Barrels", "Standard Energy, MBtu/d", _
                                      "EII Formula", "&nbsp;&nbsp;&nbsp;&nbsp;A- Complexity"}
        Dim columns() As String = {"HR", "", "ASPUtilCap", "ASPStdEnergy", "ASPEIIFormula", "", "HR", "", "OffsitesUtilCap", "OffsitesStdEnergy", "OffsitesFormula", "Complexity"}
        Dim decPlaces() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}

        refInfQry = "SELECT SubmissionID,OffsitesUtilCap,OffsitesStdEnergy,'40+4*(A)' As OffsitesFormula, Complexity, ASPUtilCap, ASPStdEnergy, 115 as ASPEIIFormula " + _
                            "FROM FactorTotCalc WHERE factorSet='" + Request.QueryString("yr") + "'" + _
                            " AND SubmissionID IN " + _
                            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                            "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                            Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
                            " )"
        dsRef = QueryDb(refInfQry)

        If dsRef.Tables.Count > 0 Then
            For n = 0 To dsRef.Tables(0).Rows.Count - 1

                For t = 0 To processRows.Length - 1

                    'Set number format
                    Select Case decPlaces(t)
                        Case 0
                            fieldFormat = "{0:#,##0}"
                        Case 1
                            fieldFormat = "{0:#,##0.0}"
                        Case 2
                            fieldFormat = "{0:N}"
                    End Select


                    'Skip Asphalt if there is not utilized capacity
                    If IsDBNull(dsRef.Tables(0).Rows(n)(columns(2))) Then
                        If t >= 0 And t < 6 Then
                            GoTo skipover
                        End If
                    End If

                    If dsRef.Tables(0).Rows(n)(columns(2)) = 0 Then
                        If t >= 0 And t < 6 Then
                            GoTo skipover
                        End If
                    End If
                    ' End If

                    If processRows(t) = "HR" Then
                        Response.Write("<tr><td colspan=3><hr width=98% color=""Black"" SIZE=1></td>")
                        GoTo skipover
                    End If


                    ' Build Description column
                    '----------------------------
                    If processRows(t) = "" Then
                        Response.Write("<tr>")
                        Response.Write("<td>&nbsp</td>")

                    Else
                        'Description column
                        Dim desc, unit As String
                        desc = processRows(t)
                        'Make sub head
                        If t > 0 Then
                            desc = SubHead(processRows(t - 1), desc)
                        End If

                        'Handle Currency
                        desc = desc.Replace("CurrencyCode", Request.QueryString("currency"))
                        'Handle Units
                        'unit = "DisplayTextUS"
                        'desc = desc.Replace("UOM", dsRef.Tables(0).Rows(n)(unit))

                        Response.Write("<tr>")
                        If desc.IndexOf("<strong>") = -1 Then
                            Response.Write("<td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc.Trim + "&nbsp;</td>")
                        Else
                            Response.Write("<td valign=top>" + desc.Trim + "&nbsp;</td>")
                        End If
                    End If


                    'Build Value Columns
                    ' -------------------------------
                    If columns(t) <> "" Then
                        'Current Month column
                        If dsRef.Tables(0).Columns.Contains(columns(t)) Then
                            Response.Write("<td  align=right>" + String.Format(fieldFormat, dsRef.Tables(0).Rows(n)(columns(t))).Trim + "</td><td>&nbsp;</td>")
                            'ElseIf processRows(t).ToUpper = "VARIABLES" And dsProcData.Tables.Count > 0 Then
                            '    'skip this line
                        Else
                            Response.Write("<td colspan=2>&nbsp;</td>")
                        End If

skipover:
                    Else
                        Response.Write("<td>&nbsp;</td>")
                    End If
                    Response.Write("</tr>")
                Next

                'Add Blank Row 
                Response.Write("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>")
            Next
        End If

        Response.Write("</table>")
    End Sub



End Class
