Partial Class GPVReport
    Inherits ReportBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        BuildReport()
    End Sub


    Public Sub BuildReport()
        Dim yldQry, catQry, yldLubeQry, productYield, fieldFormat As String
        Dim dsYield As DataSet
        Dim dsLubeYield As DataSet
        Dim dsCategories As DataSet
        Dim m, t As Integer
        Dim priceCol As String = IIf(Request.QueryString("Currency").StartsWith("US"), "PriceUS", "PriceLocal")
        Dim columns() As String = {"MaterialName", "BBL", priceCol, "GPV"}
        Dim decPlaces() As Integer = {0, 0, 2, 1}


        yldQry = "SELECT SAIName as MaterialName, ISNULL(SUM(BBL),0) AS BBL, " + _
                 " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                 " ELSE " + _
                 " ISNULL(SUM(BBL*PriceLocal)/SUM(BBL),0) " + _
                 " END AS PriceLocal," + _
                  " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                 " ELSE " + _
                 " ISNULL(SUM(BBL*PriceUS)/SUM(BBL),0) " + _
                 " END AS PriceUS, " + _
                 " ISNULL(SUM(BBL),0)*ISNULL(SUM(" + priceCol + "),0)/1000 As GPV," + _
                 " MIN(m.SortKey) AS thisSortKey " + _
                 " FROM Yield y " + _
                 " INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID " + _
                 "   WHERE AllowInPROD = 1 And LubesOnly = 0 And SubmissionID IN " + _
                "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
                " ) GROUP BY SAIName ORDER BY  thisSortKey "

        dsYield = QueryDb(yldQry)

        catQry = " SELECT (CASE Category " + _
                " WHEN 'MPROD' THEN 'Miscellaneous' " + _
                " WHEN 'ASP' THEN 'Asphalt' " + _
                " WHEN 'SOLV' THEN 'Speciality Solvents' " + _
                " WHEN 'FCHEM' THEN 'Ref. Feedstocks To Chemical Plant' " + _
                " WHEN 'FLUBE' THEN 'Ref. Feedstocks To Lube Refining' " + _
                " WHEN 'COKE' THEN 'Saleable Petroleum Coke (FOE)' " + _
                " WHEN 'RPF' THEN 'Refinery-Produced Fuels' " + _
                "  END) " + _
                " AS MaterialName, SUM(ISNULL(BBL,0)) as BBL, SUM(ISNULL(BBL,0)*ISNULL(PriceLocal,0))/SUM(ISNULL(BBL,0)) as PriceLocal," + _
                " SUM(ISNULL(BBL,0)*ISNULL(PriceUS,0))/SUM(ISNULL(BBL,0)) as PriceUS, SUM(ISNULL(BBL,0)*ISNULL(" + priceCol + ",0))/1000 As GPV " + _
                "   FROM Yield " + _
                " WHERE BBL > 0 AND Category in ('ASP', 'COKE', 'FCHEM', 'FLUBE', 'MPROD', 'SOLV','RPF') AND SubmissionID IN " + _
                " (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
                "  )" + _
                " GROUP BY Category"

        dsCategories = QueryDb(catQry)

        yldLubeQry = "SELECT SAIName as MaterialName, ISNULL(SUM(BBL),0) AS BBL, " + _
                " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                " ELSE " + _
                " ISNULL(SUM(BBL*PriceLocal)/SUM(BBL),0) " + _
                " END AS PriceLocal," + _
                 " CASE WHEN  SUM(y.BBL)=0 THEN 0 " + _
                " ELSE " + _
                " ISNULL(SUM(BBL*PriceUS)/SUM(BBL),0) " + _
                " END AS PriceUS, " + _
                " ISNULL(SUM(BBL),0)*ISNULL(SUM(" + priceCol + "),0)/1000 As GPV," + _
                " MIN(m.SortKey) AS thisSortKey " + _
                " FROM Yield y " + _
                " INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID " + _
                " INNER JOIN TSort t ON t.RefineryID ='" + RefineryID + "'" + _
                "   WHERE AllowInPROD = 1 And LubesOnly = 1 And FuelsLubesCombo=1 And SubmissionID IN " + _
               "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
               "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
               Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
               " ) GROUP BY SAIName ORDER BY  thisSortKey "
        dsLubeYield = QueryDb(yldLubeQry)

        Response.Write("<br>")
        Response.Write(" <table class='small'  border=0 bordercolor='darkcyan' width=98% cellspacing=0 cellpadding=2>")

        'Build headers
        Response.Write("<tr><td  width=50%><strong>Product Yield</strong></td>")
        Response.Write("<td align=center width=1%><strong>&nbsp;&nbsp;</strong></td>")
        Response.Write("<td  align=center colspan=2 width=20%><strong>Total <br>Barrels</strong></td>")
        Response.Write("<td align=center colspan=2 width=20%><strong>Price,<br>" + Request.QueryString("currency") + "/bbl</strong></td>")
        Response.Write("<td align=center colspan=2 width=20%><strong>GPV,<br> " + Request.QueryString("currency") + "/1000</strong></td></tr>")



        For m = 0 To dsYield.Tables(0).Rows.Count - 1
            productYield = dsYield.Tables(0).Rows(m)(columns(0))
            productYield = productYield.Replace("CurrencyCode", Request.QueryString("currency"))

            'Make sub head
            If m > 0 Then
                productYield = SubHead(dsYield.Tables(0).Rows(m - 1)(columns(0)), productYield)
            End If

            Response.Write("<tr nowrap ><td>&nbsp;&nbsp;&nbsp;" + productYield + "</td>")
            Response.Write("<td>&nbsp;&nbsp;</td>")

            For t = 1 To columns.Length - 1
                Select Case decPlaces(t)
                    Case 0
                        fieldFormat = "{0:#,##0}"
                    Case 1
                        fieldFormat = "{0:#,##0.0}"
                    Case 2
                        fieldFormat = "{0:N}"
                End Select
                If Not IsDBNull(dsYield.Tables(0).Rows(m)(columns(t))) Then
                    Response.Write("<td align=right>" + String.Format(fieldFormat, dsYield.Tables(0).Rows(m)(columns(t))) + "</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>")
                Else
                    Response.Write("<td colspan=2>&nbsp;</td>")
                End If
            Next
            Response.Write("<tr>")
        Next


        For m = 0 To dsCategories.Tables(0).Rows.Count - 1
            productYield = dsCategories.Tables(0).Rows(m)(columns(0))

            Response.Write("<tr nowrap ><td>&nbsp;&nbsp;&nbsp;" + productYield + "</td>")
            Response.Write("<td>&nbsp;&nbsp;</td>")

            For t = 1 To columns.Length - 1

                Select Case decPlaces(t)
                    Case 0
                        fieldFormat = "{0:#,##0}"
                    Case 1
                        fieldFormat = "{0:#,##0.0}"
                    Case 2
                        fieldFormat = "{0:N}"
                End Select

                If Not IsDBNull(dsCategories.Tables(0).Rows(m)(columns(t))) Then
                    Response.Write("<td align=right>" + String.Format(fieldFormat, dsCategories.Tables(0).Rows(m)(columns(t))) + "</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>")
                Else
                    Response.Write("<td colspan=2>&nbsp;</td>")
                End If

            Next
            Response.Write("<tr>")
        Next

        'Lube Section
        If dsLubeYield.Tables(0).Rows.Count > 0 Then
            Response.Write("<tr><td colspan=5 ><strong><br><br>Lube Refinery-Product Yield</strong></td></tr>")

            For m = 0 To dsLubeYield.Tables(0).Rows.Count - 1
                productYield = dsLubeYield.Tables(0).Rows(m)(columns(0))
                productYield = productYield.Replace("CurrencyCode", Request.QueryString("currency"))

                'Make sub head
                If m > 0 Then
                    If Not productYield.StartsWith("Group") Then
                        productYield = SubHead(dsLubeYield.Tables(0).Rows(m - 1)(columns(0)), productYield)
                    Else
                        productYield = SubHeadIgnoreFirst(dsLubeYield.Tables(0).Rows(m - 1)(columns(0)), productYield)
                    End If
                End If


                Response.Write("<tr nowrap ><td>&nbsp;&nbsp;&nbsp;" + productYield.Trim + "</td>")
                Response.Write("<td>&nbsp;&nbsp;</td>")

                For t = 1 To columns.Length - 1
                    Select Case decPlaces(t)
                        Case 0
                            fieldFormat = "{0:#,##0}"
                        Case 1
                            fieldFormat = "{0:#,##0.0}"
                        Case 2
                            fieldFormat = "{0:N}"
                    End Select
                    If Not IsDBNull(dsYield.Tables(0).Rows(m)(columns(t))) Then
                        Response.Write("<td align=right>" + String.Format(fieldFormat, dsLubeYield.Tables(0).Rows(m)(columns(t))) + "</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>")
                    Else
                        Response.Write("<td colspan=2>&nbsp;</td>")
                    End If
                Next
                Response.Write("<tr>")
            Next
        End If

        Response.Write("</table>")

    End Sub



End Class
