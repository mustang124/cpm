Option Strict Off
Imports System.Data.SqlTypes
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Configuration

Module DBHelper


#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.AppSettings("SqlConnectionString")
#End Region


#Region "DeNull functions"
    '<summary>
    '   Returns object value if object is not null else null is returned.
    '</summary>
    ' <param name="obj">object</param>
    ' <param name="objType">native system type</param>
    '<returns>Object value is returned. If object is null,it returns 0. </returns>
    Public Function DeNull(ByVal obj As Object, ByVal objType As Type) As Object

        If IsDBNull(obj) Or IsNothing(obj) Then
            If objType.ToString = "System.String" Then
                Return String.Empty
            ElseIf objType.ToString = "System.DateTime" Then
                Return obj
            Else
                Return 0
            End If
        Else
            Return obj
        End If

    End Function


    '<summary>
    '   Returns object value if object is not null else null is returned.
    '</summary>
    ' <param name="row">DataTable's DataRow</param>
    ' <param name="colName"> Column name of  data row</param>
    '<returns>Object value is returned. If object is null,it returns 0. </returns>
    Public Function DeNull(ByVal row As DataRow, ByVal colName As String) As Object
        Dim colType As Type = row.Table.Columns(colName).DataType
        

        If row.IsNull(colName) Then
            If colType.ToString = "System.String" Then
                Return String.Empty
            ElseIf colType.ToString = "System.DateTime" Then
                Return DBNull.Value
            Else
                Return 0
            End If
        Else
            Return row(colName)
        End If
    End Function



#End Region

#Region "QueryDb Functions"
    Function QueryDb(ByVal sqlString As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim sqlstmt As String
        Dim SqlConnection1 As SqlConnection = New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        SqlConnection1.ConnectionString = _connectionString

        Try
            sqlstmt = sqlString

            Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, SqlConnection1)
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw New Exception("There was a database exception." + ex.Message)
        End Try

    End Function
#End Region
End Module
