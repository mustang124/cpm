Imports System.Configuration

Public Class PwdManager
    Inherits System.Web.UI.Page


#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.sdCompanyLU = New System.Data.SqlClient.SqlDataAdapter
        Me.DsCompanyLU1 = New ProfileII_Corporate.DsCompanyLU
        CType(Me.DsCompanyLU1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT CompanyID, CompanyName, CompanyLogin, CompanyPwd, PwdSaltKey FROM dbo.Comp" & _
        "any_LU"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = _connectionString
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO dbo.Company_LU(CompanyID, CompanyName, CompanyLogin, CompanyPwd, PwdS" & _
        "altKey) VALUES (@CompanyID, @CompanyName, @CompanyLogin, @CompanyPwd, @PwdSaltKe" & _
        "y); SELECT CompanyID, CompanyName, CompanyLogin, CompanyPwd, PwdSaltKey FROM dbo" & _
        ".Company_LU WHERE (CompanyID = @CompanyID)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.VarChar, 10, "CompanyID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyName", System.Data.SqlDbType.VarChar, 50, "CompanyName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyLogin", System.Data.SqlDbType.VarChar, 50, "CompanyLogin"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyPwd", System.Data.SqlDbType.VarChar, 50, "CompanyPwd"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdSaltKey", System.Data.SqlDbType.VarChar, 50, "PwdSaltKey"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE dbo.Company_LU SET CompanyID = @CompanyID, CompanyName = @CompanyName, Com" & _
        "panyLogin = @CompanyLogin, CompanyPwd = @CompanyPwd, PwdSaltKey = @PwdSaltKey WH" & _
        "ERE (CompanyID = @Original_CompanyID) AND (CompanyLogin = @Original_CompanyLogin" & _
        ") AND (CompanyName = @Original_CompanyName) AND (CompanyPwd = @Original_CompanyP" & _
        "wd) AND (PwdSaltKey = @Original_PwdSaltKey); SELECT CompanyID, CompanyName, Comp" & _
        "anyLogin, CompanyPwd, PwdSaltKey FROM dbo.Company_LU WHERE (CompanyID = @Company" & _
        "ID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.VarChar, 10, "CompanyID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyName", System.Data.SqlDbType.VarChar, 50, "CompanyName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyLogin", System.Data.SqlDbType.VarChar, 50, "CompanyLogin"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyPwd", System.Data.SqlDbType.VarChar, 50, "CompanyPwd"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdSaltKey", System.Data.SqlDbType.VarChar, 50, "PwdSaltKey"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyLogin", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyLogin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyPwd", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyPwd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PwdSaltKey", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PwdSaltKey", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM dbo.Company_LU WHERE (CompanyID = @Original_CompanyID) AND (CompanyLo" & _
        "gin = @Original_CompanyLogin) AND (CompanyName = @Original_CompanyName) AND (Com" & _
        "panyPwd = @Original_CompanyPwd) AND (PwdSaltKey = @Original_PwdSaltKey)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyLogin", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyLogin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyPwd", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyPwd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PwdSaltKey", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PwdSaltKey", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdCompanyLU
        '
        Me.sdCompanyLU.DeleteCommand = Me.SqlDeleteCommand1
        Me.sdCompanyLU.InsertCommand = Me.SqlInsertCommand1
        Me.sdCompanyLU.SelectCommand = Me.SqlSelectCommand1
        Me.sdCompanyLU.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Company_LU", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CompanyID", "CompanyID"), New System.Data.Common.DataColumnMapping("CompanyName", "CompanyName"), New System.Data.Common.DataColumnMapping("CompanyLogin", "CompanyLogin"), New System.Data.Common.DataColumnMapping("CompanyPwd", "CompanyPwd"), New System.Data.Common.DataColumnMapping("PwdSaltKey", "PwdSaltKey")})})
        Me.sdCompanyLU.UpdateCommand = Me.SqlUpdateCommand1
        '
        'DsCompanyLU1
        '
        Me.DsCompanyLU1.DataSetName = "DsCompanyLU"
        Me.DsCompanyLU1.Locale = New System.Globalization.CultureInfo("en-US")
        CType(Me.DsCompanyLU1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents btnAddPassword As System.Web.UI.WebControls.Button
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents sdCompanyLU As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents DsCompanyLU1 As ProfileII_Corporate.DsCompanyLU
    Protected WithEvents txtPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents ltStatus As System.Web.UI.WebControls.Literal
    Protected WithEvents ddlCompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sdCompanyLU.Fill(DsCompanyLU1)
            ddlCompany.DataBind()
            ddlCompany.Items.Insert(0, "")
        End If
    End Sub

    Private Sub btnAddPassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPassword.Click
        Try
            If ddlCompany.SelectedIndex > -1 Then
                Dim secData As SecurityData
                Dim auth As New Authentication
                Dim row() As DataRow
                sdCompanyLU.Fill(DsCompanyLU1)
                row = DsCompanyLU1.Tables(0).Select("CompanyName='" + ddlCompany.SelectedValue + "'")
                If row.Length > 0 Then
                    secData = auth.GetUserDataByName(ddlCompany.SelectedValue.Trim)
                    If Not IsNothing(secData.Salt) Then
                        row(0)("CompanyPwd") = Encrypt(txtPassword.Text, secData.Salt)
                        sdCompanyLU.Update(row)
                        ltStatus.Text = "Password was sucessfully updated."
                    Else
                        ltStatus.Text = "Password was not updated."
                    End If
                Else
                    ltStatus.Text = "The company selected was not found"
                End If
            End If
        Catch ex As Exception

            ltStatus.Text = "Password was not updated."
        End Try

    End Sub
End Class
