﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Extensible
	{
		internal partial class Load
		{
			internal static void ProcessFacilities(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Process Facilities:   {0}", DateTime.Now.ToString());

				Excel.Worksheet wks = wkb.Worksheets["Process Facilities"];
				Excel.Range rng = null;

				int UnitId;
				string UnitName;
				string ProcessId;
				string PropertyId;
				string CellAddress;

				int rowUnitId = 9;
				int rowUnitName = 10;
				int rowProcessId = 11;

				int colProcessId = 1;
				int colPropertyID = 2;

				int rowRegNum = 137;
				int rowRegDown = 138;
				int rowMaintNum = 139;
				int rowMaintDown = 140;
				int rowOthNum = 141;
				int rowOthDown = 142;

				int rowOthDownEconomic = 148;
				int rowOthDownExternal = 149;
				int rowOthDownUnitUpsets = 150;
				int rowOthDownOffsiteUpsets = 151;
				int rowOthDownOther = 152;

                int rowRoutExpLocal = 143;
                int rowRoutCptlLocal = 144;
                int rowRoutOvhdLocal = 145;
                int rowRoutCostLocal = 146;

                int rowUtilPcnt = 17;
                int rowStmUtilPcnt = 18;
                int rowEnergyPcnt = 19;
                
				map.ProcessData.BeginLoadData();
				map.MaintRout.BeginLoadData();
                map.Config.BeginLoadData();

                //     ProcessData for Operating Units


                for (int c = 4; c <= 78; c++)
                {
                    rng = wks.Cells[rowUnitId, c];

                    if (Common.RangeHasValue(rng))
                    {
                        UnitId = Common.ReturnInt32(rng);

                        rng = wks.Cells[rowUnitName, c];
                        UnitName = Common.ReturnString(rng);

                        rng = wks.Cells[rowProcessId, c];
                        ProcessId = Common.ReturnString(rng);

                        for (int r = 23; r <= 135; r++)
                        {
                            rng = wks.Cells[r, colProcessId];
                            if (ProcessId == Common.ReturnString(rng))
                            {
                                rng = wks.Cells[r, colPropertyID];
                                PropertyId = Common.ReturnString(rng);

                                rng = wks.Cells[r, c];
                                CellAddress = Common.ReturnAddress(rng);

                                Xsd.MappingsSchema.ProcessDataRow dr = map.ProcessData.FindByUnitIDProperty(UnitId, PropertyId);

                                if (dr != null)
                                {
                                    dr.RptValue = CellAddress;
                                }
                                else
                                {
                                    Xsd.MappingsSchema.ProcessDataRow nr = map.ProcessData.NewProcessDataRow();

                                    nr.UnitID = UnitId;
                                    nr.UnitName = UnitName;
                                    nr.ProcessID = ProcessId;
                                    nr.Property = PropertyId;
                                    nr.RptValue = CellAddress;

                                    map.ProcessData.AddProcessDataRow(dr);

                                }
                            }
                        }

                        Xsd.MappingsSchema.ConfigRow cr = map.Config.FindByUnitID(UnitId);

                        if (cr != null)
                        {
                            rng = wks.Cells[rowUtilPcnt, c];
                            cr.UtilPcnt = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowEnergyPcnt , c];
                            cr.EnergyPcnt  = Common.ReturnAddress(rng);
                            
                            if (ProcessId == "FTCOGEN")
                            {
                                rng = wks.Cells[rowStmUtilPcnt, c];
                                cr.StmUtilPcnt = Common.ReturnAddress(rng);
                            }
                        }




                        Xsd.MappingsSchema.MaintRoutRow mr = map.MaintRout.FindByUnitID(UnitId);

                        if (mr != null)
                        {
                            rng = wks.Cells[rowRegNum, c];
                            mr.RegNum = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRegDown, c];
                            mr.RegDown = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowMaintNum, c];
                            mr.MaintNum = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowMaintDown, c];
                            mr.MaintDown = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthNum, c];
                            mr.OthNum = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDown, c];
                            mr.OthDown = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownEconomic, c];
                            mr.OthDownEconomic = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownExternal, c];
                            mr.OthDownExternal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownUnitUpsets, c];
                            mr.OthDownUnitUpsets = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownOffsiteUpsets, c];
                            mr.OthDownOffsiteUpsets = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownOther, c];
                            mr.OthDownOther = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutCostLocal, c];
                            mr.RoutCostLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutCptlLocal, c];
                            mr.RoutCptlLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutOvhdLocal, c];
                            mr.RoutOvhdLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutExpLocal, c];
                            mr.RoutExpLocal = Common.ReturnAddress(rng);
                        }
                        else
                        {
                            Xsd.MappingsSchema.MaintRoutRow nr = map.MaintRout.NewMaintRoutRow();

                            nr.UnitID = UnitId;
                            nr.ProcessID = ProcessId;
                            nr.UnitName = UnitName;

                            rng = wks.Cells[rowRegNum, c];
                            nr.RegNum = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRegDown, c];
                            nr.RegDown = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowMaintNum, c];
                            nr.MaintNum = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowMaintDown, c];
                            nr.MaintDown = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthNum, c];
                            nr.OthNum = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDown, c];
                            nr.OthDown = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownEconomic, c];
                            nr.OthDownEconomic = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownExternal, c];
                            nr.OthDownExternal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownUnitUpsets, c];
                            nr.OthDownUnitUpsets = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownOffsiteUpsets, c];
                            nr.OthDownOffsiteUpsets = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowOthDownOther, c];
                            nr.OthDownOther = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutCostLocal, c];
                            nr.RoutCostLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutCptlLocal, c];
                            nr.RoutCptlLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutOvhdLocal, c];
                            nr.RoutOvhdLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutExpLocal, c];
                            nr.RoutExpLocal = Common.ReturnAddress(rng);

                            map.MaintRout.AddMaintRoutRow(nr);

                        }
                    }
                }
                //     ProcessData for Offsites and Utilities Units


                for (int c = 91; c <= 98; c++)
                {
                    rng = wks.Cells[rowUnitId, c];

                    if (Common.RangeHasValue(rng))
                    {
                        UnitId = Common.ReturnInt32(rng);

                        rng = wks.Cells[rowUnitName, c];
                        UnitName = Common.ReturnString(rng);

                        rng = wks.Cells[rowProcessId, c];
                        ProcessId = Common.ReturnString(rng);

                        Xsd.MappingsSchema.MaintRoutRow mr = map.MaintRout.FindByUnitID(UnitId);

                        if (mr != null)
                        {
                            rng = wks.Cells[rowRoutCostLocal, c];
                            mr.RoutCostLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutCptlLocal, c];
                            mr.RoutCptlLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutOvhdLocal, c];
                            mr.RoutOvhdLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutExpLocal, c];
                            mr.RoutExpLocal = Common.ReturnAddress(rng);
                        }
                        else
                        {
                            Xsd.MappingsSchema.MaintRoutRow nr = map.MaintRout.NewMaintRoutRow();

                            nr.UnitID = UnitId;
                            nr.ProcessID = ProcessId;
                            nr.UnitName = UnitName;

                            rng = wks.Cells[rowRoutCostLocal, c];
                            nr.RoutCostLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutCptlLocal, c];
                            nr.RoutCptlLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutOvhdLocal, c];
                            nr.RoutOvhdLocal = Common.ReturnAddress(rng);

                            rng = wks.Cells[rowRoutExpLocal, c];
                            nr.RoutExpLocal = Common.ReturnAddress(rng);

                            map.MaintRout.AddMaintRoutRow(nr);

                        }
                    }
                }



				map.ProcessData.AcceptChanges();
				map.ProcessData.EndLoadData();

                map.Config.AcceptChanges();
                map.Config.EndLoadData();

				map.MaintRout.AcceptChanges();
				map.MaintRout.EndLoadData();
			}
		}
	}
}