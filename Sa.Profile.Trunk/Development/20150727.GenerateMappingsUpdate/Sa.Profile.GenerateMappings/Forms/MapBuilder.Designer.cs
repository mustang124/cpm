﻿namespace Sa.Forms
{
    partial class frmBuildMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBuildMap));
            this.txtBridgeFile = new System.Windows.Forms.TextBox();
            this.txtTemplate = new System.Windows.Forms.TextBox();
            this.txtNewMap = new System.Windows.Forms.TextBox();
            this.btnTemplate = new System.Windows.Forms.Button();
            this.btnNewMap = new System.Windows.Forms.Button();
            this.btnBridge = new System.Windows.Forms.Button();
            this.chkDefaultMap = new System.Windows.Forms.CheckBox();
            this.btnBuild = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtBridgeFile
            // 
            this.txtBridgeFile.Location = new System.Drawing.Point(131, 177);
            this.txtBridgeFile.Name = "txtBridgeFile";
            this.txtBridgeFile.Size = new System.Drawing.Size(472, 20);
            this.txtBridgeFile.TabIndex = 1;
            this.txtBridgeFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTemplate
            // 
            this.txtTemplate.Location = new System.Drawing.Point(131, 206);
            this.txtTemplate.Name = "txtTemplate";
            this.txtTemplate.Size = new System.Drawing.Size(472, 20);
            this.txtTemplate.TabIndex = 2;
            this.txtTemplate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNewMap
            // 
            this.txtNewMap.Location = new System.Drawing.Point(131, 232);
            this.txtNewMap.Name = "txtNewMap";
            this.txtNewMap.Size = new System.Drawing.Size(472, 20);
            this.txtNewMap.TabIndex = 3;
            this.txtNewMap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnTemplate
            // 
            this.btnTemplate.Location = new System.Drawing.Point(12, 204);
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(113, 23);
            this.btnTemplate.TabIndex = 4;
            this.btnTemplate.Text = "Mapping Template";
            this.btnTemplate.UseVisualStyleBackColor = true;
            this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
            // 
            // btnNewMap
            // 
            this.btnNewMap.Location = new System.Drawing.Point(12, 230);
            this.btnNewMap.Name = "btnNewMap";
            this.btnNewMap.Size = new System.Drawing.Size(113, 23);
            this.btnNewMap.TabIndex = 5;
            this.btnNewMap.Text = "New Mapping";
            this.btnNewMap.UseVisualStyleBackColor = true;
            this.btnNewMap.Click += new System.EventHandler(this.btnNewMap_Click);
            // 
            // btnBridge
            // 
            this.btnBridge.Location = new System.Drawing.Point(12, 175);
            this.btnBridge.Name = "btnBridge";
            this.btnBridge.Size = new System.Drawing.Size(113, 23);
            this.btnBridge.TabIndex = 6;
            this.btnBridge.Text = "Bridge File";
            this.btnBridge.UseVisualStyleBackColor = true;
            this.btnBridge.Click += new System.EventHandler(this.btnBridge_Click);
            // 
            // chkDefaultMap
            // 
            this.chkDefaultMap.AutoSize = true;
            this.chkDefaultMap.Checked = true;
            this.chkDefaultMap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDefaultMap.Location = new System.Drawing.Point(610, 234);
            this.chkDefaultMap.Name = "chkDefaultMap";
            this.chkDefaultMap.Size = new System.Drawing.Size(60, 17);
            this.chkDefaultMap.TabIndex = 8;
            this.chkDefaultMap.Text = "Default";
            this.chkDefaultMap.UseVisualStyleBackColor = true;
            this.chkDefaultMap.CheckedChanged += new System.EventHandler(this.chkDefaultMap_CheckedChanged);
            // 
            // btnBuild
            // 
            this.btnBuild.Location = new System.Drawing.Point(610, 174);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(75, 23);
            this.btnBuild.TabIndex = 9;
            this.btnBuild.Text = "Build";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(609, 203);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.BulletIndent = 10;
            this.richTextBox1.CausesValidation = false;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.richTextBox1.Location = new System.Drawing.Point(12, 4);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox1.Size = new System.Drawing.Size(672, 165);
            this.richTextBox1.TabIndex = 11;
            this.richTextBox1.TabStop = false;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // frmBuildMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 258);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBuild);
            this.Controls.Add(this.chkDefaultMap);
            this.Controls.Add(this.btnBridge);
            this.Controls.Add(this.btnNewMap);
            this.Controls.Add(this.btnTemplate);
            this.Controls.Add(this.txtNewMap);
            this.Controls.Add(this.txtTemplate);
            this.Controls.Add(this.txtBridgeFile);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBuildMap";
            this.Text = "Map File Builder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBridgeFile;
        private System.Windows.Forms.TextBox txtTemplate;
        private System.Windows.Forms.TextBox txtNewMap;
        private System.Windows.Forms.Button btnTemplate;
        private System.Windows.Forms.Button btnNewMap;
        private System.Windows.Forms.Button btnBridge;
        private System.Windows.Forms.CheckBox chkDefaultMap;
        private System.Windows.Forms.Button btnBuild;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}