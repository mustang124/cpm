Imports Microsoft.DSG.Security.CryptoServices

Module Encryptor

    Public EncryptionKey As String = "0S1A2C3T4W5S6G7L8C9DBRMF"
    Public EncryptionIV As String = "SACTW0S1G2L3C4D5B6R7M8F9"

    Public Function Encrypt(ByVal aString As String) As String
        ' Note that the key and IV must be the same for the encrypt and decrypt calls.
        Dim results As String

        Try
            Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
            tdesEngine.StringKey = EncryptionKey
            tdesEngine.StringIV = EncryptionIV
            results = tdesEngine.Encrypt(aString)
        Catch anError As Exception
            Throw anError
        End Try

        Return (results)

    End Function

    Public Function Decrypt(ByVal aString As String) As String
        ' Note that the key and IV must be the same for the encrypt and decript calls.
        Dim results As String

        Try
            Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
            tdesEngine.StringKey = EncryptionKey
            tdesEngine.StringIV = EncryptionIV
            results = tdesEngine.Decrypt(aString)
        Catch anError As Exception
            Throw anError
        End Try

        Return (results)

    End Function
End Module
