Imports System.Web.Services
Imports System.IO
Imports System.Configuration

<System.Web.Services.WebService(Namespace:="http://solomononline.com/FuelProfileServices")> _
Public Class RegistrationService
    Inherits System.Web.Services.WebService

#Region " Web Services Designer Generated Code "

#Region "Private Variables"
    Dim _connectionStringNoPacketSize As String = ConfigurationManager.AppSettings("SqlConnectionStringNoPacketSize")
#End Region

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = _connectionStringNoPacketSize

    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region
  
    <WebMethod(Description:="Returns a key and Activates Fuel Profile ")> _
    Public Function ActivateApplication(ByVal WildSeed As String, ByVal Company As String, ByVal Location As String) As String


        Dim strInputValues, encryptedCSValue As String
        Dim writer As StreamWriter

        Dim cmd As New SqlClient.SqlCommand("SELECT RefineryID FROM TSort WHERE Company='" + Company + "' AND Location='" + Location + "'", SqlConnection1)
        cmd.Connection.Open()

        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)


        If reader.Read Then

            Dim RefineryID As String '= reader.GetString(0)
            RefineryID = reader.GetString(0)
            reader.Close()

            strInputValues = Company.Trim & "$" & WildSeed.Trim & "$" & Location.Trim & "$" & RefineryID.Trim
            encryptedCSValue = Encrypt(strInputValues)


            If Directory.Exists("C:\\ClientKeys\\") Then
                If Not File.Exists("C:\\ClientKeys\\" + Company.Trim() + "_" + Location.Trim + ".key") Then
                    writer = File.CreateText("C:\\ClientKeys\\" + Company.Trim() + "_" + Location.Trim + ".key")
                Else
                    Throw New Exception("This site has been registered. Contact Solomon Associates if you have questions.")
                End If
            Else

                If Not File.Exists("D:\\ClientKeys\\" + Company.Trim() + "_" + Location.Trim + ".key") Then
                    writer = File.CreateText("D:\\ClientKeys\\" + Company.Trim() + "_" + Location.Trim + ".key")
                Else
                    Throw New Exception("This site has been registered. Contact Solomon Associates if you have questions.")
                End If
            End If

            writer.WriteLine(encryptedCSValue)
            writer.Close()
            Return encryptedCSValue
        Else
            Throw New Exception("You will not be able to use this application because your company has not participated in our studies")
        End If

    End Function




End Class
