Imports System
Imports System.IO
Imports System.Net
Imports System.Reflection
Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Security.Policy
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens

Public Class DataServiceExtension
    Inherits Solomon.Query.DataServices

   
    Public Sub New()
        MyBase.New()
        ConfigureProxy()
        SecureWebServiceRequest("_CERT\\policy.ct")
    End Sub


    Public Sub New(ByVal keypath As String)
        MyBase.New()
        SecureWebServiceRequest(keypath)
    End Sub


    Private Sub ConfigureProxy()
        Me.PreAuthenticate = True
        Me.Credentials = CredentialCache.DefaultCredentials
        Me.Proxy = GlobalProxySelection.Select
        Me.Proxy.Credentials = CredentialCache.DefaultCredentials
        Dim wp As System.Net.WebProxy = GlobalProxySelection.Select
        wp.Credentials = CredentialCache.DefaultCredentials

        Dim username As String = System.Configuration.ConfigurationSettings.AppSettings("ProxyUsername")
        If Not IsNothing(username) AndAlso _
          username.Trim().Length > 0 Then
            Dim password As String = System.Configuration.ConfigurationSettings.AppSettings("ProxyPassword")
            Dim cr As New System.Net.NetworkCredential(username, password)

            Me.Proxy.Credentials = cr
            Me.Credentials = cr
            wp.Credentials = cr

        End If
    End Sub


    Private Sub SecureWebServiceRequest(ByVal keyPath As String)

        If Not File.Exists(keyPath) Then
            Throw New Exception(Directory.GetCurrentDirectory() + vbCrLf + "Key was not found. If you have a valid key, restart your application. Otherwise, please register your application " + _
                                "before you download data.")
        End If

        Dim key As String
        Dim token As UsernameToken
        Dim reader As New StreamReader(keyPath)
        Dim password As String

        If reader.Peek <> -1 Then
            key = reader.ReadLine
            reader.Close()
            password = CalcMiniKey(key)
            token = New UsernameToken(key, password, PasswordOption.SendHashed)
            ConfigureProxy(Me.Proxy, token)
        Else
            Throw New Exception("A key was not found")
        End If

    End Sub


    Private Sub ConfigureProxy(ByVal proxy As _
        WebServicesClientProtocol, ByVal token As UsernameToken)
        proxy.RequestSoapContext.Security.Timestamp.TtlInSeconds = -1
        proxy.RequestSoapContext.Security.Tokens.Add(token)
        Dim dk As New DerivedKeyToken(token)
        proxy.RequestSoapContext.Security.Tokens.Add(dk)
        proxy.RequestSoapContext.Security.Elements.Add( _
           New MessageSignature(dk))
        proxy.RequestSoapContext.Security.Elements.Add( _
         New EncryptedData(dk))

    End Sub 'ConfigureProxy


    Private Function CalcMiniKey(ByVal key As String) As String
        Dim lastpos As Integer = key.Length - 1
        Dim firstPart As String = key.Substring(0, Math.Round(lastpos * (3 / 8)))
        Dim secondPart As String = key.Substring(Math.Round(lastpos * 0.75), lastpos - (lastpos * 0.75) + 1)

        Return secondPart + firstPart
    End Function


    'Resolves the 'underlying connection is closed' error
    Protected Overrides Function GetWebRequest(ByVal uri As Uri) As System.Net.WebRequest

        Dim requestPropertyInfo As PropertyInfo = Nothing

        Dim request As WebRequest = MyBase.GetWebRequest(uri)

        If IsNothing(requestPropertyInfo) Then
            requestPropertyInfo = request.GetType().GetProperty("Request")
        End If

        ' Retrieve underlying web request 
        Dim webRequest As HttpWebRequest = CType(requestPropertyInfo.GetValue(request, Nothing), HttpWebRequest)

        ' Setting KeepAlive 
        webRequest.KeepAlive = False

        Return request

    End Function
End Class
