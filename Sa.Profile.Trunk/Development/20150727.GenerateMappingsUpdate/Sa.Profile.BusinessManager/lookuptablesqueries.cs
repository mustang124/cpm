﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ProfileBusinessManager.Classes;
using System.Configuration;

namespace ProfileBusinessManager
{
    public class LookupTablesQueries : DataServiceExtension
    {

        AppCertificate appCertificate;

        public LookupTablesQueries()
        {
            appCertificate = new AppCertificate();
        }

        internal new DataSet GetReferences()
        {

            if (appCertificate.IsValidCertificateInstalled())
            {

                this.Url = ConfigurationManager.AppSettings["DataServicesUrl"].ToString();
                

                DataSet ds = base.GetReferences();
                return ds;
            }
            else
            {
                throw new Exception("Remote service call can not envoke because Solomon Associates\' certificate is not installed on your machine.");
            }
        }

        internal new DataSet GetLookups()
        {

            if (appCertificate.IsValidCertificateInstalled())
            {

                this.Url = ConfigurationManager.AppSettings["DataServicesUrl"].ToString();

                DataSet ds = base.GetLookups();
                return ds;
            }
            else
            {
                throw new Exception("Remote service call can not envoke because Solomon Associates\' certificate is not installed on your machine.");
            }
        }

        internal DataSet GetRefineryData(string ReportCode, string ds, string scenario, string currency, DateTime startDate, string UOM, int studyYear, bool includeTarget, bool includeYTD, bool includeAVG)
        {

            if (appCertificate.IsValidCertificateInstalled())
            {

                this.Url = ConfigurationManager.AppSettings["DataServicesUrl"].ToString();

                DataSet dset = base.GetDataDump(ReportCode, ds, scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, includeAVG);
                return dset;
            }
            else
            {
                throw new Exception("Remote service call can not envoke because Solomon Associates\' certificate is not installed on your machine.");
            }
        }

        internal static void Main()
        {
            LookupTablesQueries service = new LookupTablesQueries();
            DataSet lookups = service.GetLookups();
            int y;
            for (y = 0; (y
                        <= (lookups.Tables.Count - 1)); y++)
            {
                //Console.WriteLine((lookups.Tables[y].TableName + ("," + lookups.Tables[y].Rows.Count.ToString)));
            }
        }

    }
}
