<!-- config-start -->

<!-- config-end -->
<!-- template-start -->

<table class='small' border=0>
  <tr>
    <td width=420></td>
    <td width=80></td>
    <td width=100></td>
    <td width=100></td>
  </tr>
  <tr>
    <td colspan=3><b>ReportPeriod</b></td>
  </tr>
  <tr>
    <td></td>
    <td valign=bottom align=right><strong>k&nbsp;CurrencyCode</strong></td>
    <td valign=bottom align=right><strong>100&nbsp;CurrencyCode</br>per bbl&nbsp;</strong></td>
    <td valign=bottom align=right><strong>100&nbsp;CurrencyCode</br>/UEDC&nbsp;</strong></td>
  </tr>
  <tr>
    <td colspan=4 height=30 valign=bottom><b>Non-Volume-Related Expenses</b></td>
  </tr>
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;Salaries & Wages</td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Operator,Craft & Clerical</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OCCSal,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OCCSal,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OCCSal,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Management,Professional & Staff</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(MPSSal,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(MPSSal,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(MPSSal,'#,##0.0') END </td>
  </tr>
  <tr>
    <td colspan=4 valign=bottom>&nbsp;&nbsp;&nbsp;Employee Benefits</td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Operator,Craft & Clerical</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OCCBen,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OCCBen,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OCCBen,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Management,Professional & Staff</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(MPSBen,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(MPSBen,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(MPSBen,'#,##0.0') END </td>
  </tr>
  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;Maintenance Materials</td>
    <td align=right valign=bottom>Section(Opex, DataType='ADJ',) BEGIN Format(MaintMatlST,'#,##0.0') END </td>
    <td align=right valign=bottom>Section(Opex, DataType='C/BBL',) BEGIN Format(MaintMatlST,'#,##0.0') END </td>
    <td align=right valign=bottom>Section(Opex, DataType='UEDC',) BEGIN Format(MaintMatlST,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Contract Maintenance Labor Expense</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(ContMaintLaborST,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(ContMaintLaborST,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(ContMaintLaborST,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Other Contract Services</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OthCont,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OthCont,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OthCont,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Turnaround Adjustment</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(TAAdj,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(TAAdj,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(TAAdj,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Environmental Expenses</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(Envir,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(Envir,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(Envir,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Other Non-Volume-Related Expenses</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OthNonVol,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OthNonVol,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OthNonVol,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Utilized G&A Personnel Cost</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(GAPers,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(GAPers,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(GAPers,'#,##0.0') END </td>
  </tr>
  <tr>
    <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal Non-Volume-Related Expenses</b></td>
    <td align=right><strong>Section(Opex, DataType='ADJ',) BEGIN Format(STNonVol,'#,##0.0') END </strong></td>
    <td align=right><strong>Section(Opex, DataType='C/BBL',) BEGIN Format(STNonVol,'#,##0.0') END </strong></td>
    <td align=right><strong>Section(Opex, DataType='UEDC',) BEGIN Format(STNonVol,'#,##0.0') END </strong></td>
  </tr>
  <tr>
    <td colspan=4 height=30 valign=bottom><strong>Volume-Related Expenses</strong></td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Chemicals</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(Chemicals,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(Chemicals,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(Chemicals,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Catalysts</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(Catalysts,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(Catalysts,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(Catalysts,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Royalties</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(Royalties,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(Royalties,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(Royalties,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Purchased Energy</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(PurchasedEnergy,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(PurchasedEnergy,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(PurchasedEnergy,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Produced Energy</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(ProducedEnergy,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(ProducedEnergy,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(ProducedEnergy,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Purchased Energy Other than Electric and Steam</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(PurOth,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(PurOth,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(PurOth,'#,##0.0') END </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;Other Volume-Related Expenses</td>
    <td align=right>Section(Opex, DataType='ADJ',) BEGIN Format(OthVol,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='C/BBL',) BEGIN Format(OthVol,'#,##0.0') END </td>
    <td align=right>Section(Opex, DataType='UEDC',) BEGIN Format(OthVol,'#,##0.0') END </td>
  </tr>
  <tr>
    <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal Volume-Related Expenses</b></td>
    <td align=right><strong>Section(Opex, DataType='ADJ',) BEGIN Format(STVol,'#,##0.0') END </strong></td>
    <td align=right><strong>Section(Opex, DataType='C/BBL',) BEGIN Format(STVol,'#,##0.0') END </strong></td>
    <td align=right><strong>Section(Opex, DataType='UEDC',) BEGIN Format(STVol,'#,##0.0') END </strong></td>
  </tr>
  <tr>
    <td height=30 valign=bottom><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Cash Operating Expenses</b></td>
    <td align=right valign=bottom><strong>Section(Opex, DataType='ADJ',) BEGIN Format(TotCashOpEx,'#,##0.0') END </strong></td>
    <td align=right valign=bottom><strong>Section(Opex, DataType='C/BBL',) BEGIN Format(TotCashOpEx,'#,##0.0') END </strong></td>
    <td align=right valign=bottom><strong>Section(Opex, DataType='UEDC',) BEGIN Format(TotCashOpEx,'#,##0.0') END </strong></td>
  </tr>
</table>

<!-- template-end -->