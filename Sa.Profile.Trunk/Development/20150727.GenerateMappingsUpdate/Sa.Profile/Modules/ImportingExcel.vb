Option Compare Binary
Option Explicit On
Option Strict Off

Friend Module ImportingExcel

    Friend Sub selectWorkBook(ByVal cur As System.Windows.Forms.Cursor, _
                              ByVal tbFilePath As System.Windows.Forms.TextBox, _
                              ByVal cbSheets As System.Windows.Forms.ComboBox, _
                              ByVal pnlProgress As System.Windows.Forms.StatusBarPanel, _
                              ByVal dv As DataView)

        Dim BrowseTo As New OpenFileDialog

        BrowseTo.InitialDirectory = tbFilePath.Text
        BrowseTo.Filter = "Excel files (*.xls, *.xlsx, *.xlsm) | *.xls; *.xlsx; *.xlsm"
        BrowseTo.Multiselect = False
        BrowseTo.ReadOnlyChecked = True

        If BrowseTo.ShowDialog() = DialogResult.OK Then

            cur = Cursors.WaitCursor

            pnlProgress.Text = "Updating worksheet list..."

            'dv.Item(0)!lkFilePath = BrowseTo.FileName
            tbFilePath.Text = BrowseTo.FileName
            cbSheets.Text = ""
            cbSheets.Items.Clear()
            UpdateSheets(tbFilePath.Text, cbSheets)

            pnlProgress.Text = "Ready"
            cur = Cursors.Default

        End If

        BrowseTo.Dispose()

    End Sub

    Friend Sub UpdateSheets(ByVal PathName As String, ByVal cmb As ComboBox)
        'Dim ci As Object = System.Threading.Thread.CurrentThread.CurrentCulture
        Dim ci As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Dim culture As New ProfileEnvironment.Culture

        Dim MyXL, MyWB As New Object
        Dim ExcelWasNotRunning As Boolean = False
        Dim ExcelWorkbookOpen As Boolean = False

        Try
            MyXL = GetObject(, "Excel.Application")
        Catch ex As Exception
            MyXL = CreateObject("Excel.Application")
            ExcelWasNotRunning = True
        End Try


        Try

            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
            
            MyXL.Application.DisplayAlerts = False
            MyWB = MyXL.workbooks.open(PathName, 2, True, , , , True, , , False, False, , False)

            Dim ws As Object
            cmb.Items.Clear()
            For Each ws In MyWB.worksheets
                cmb.Items.Add(ws.name)
            Next

            If ExcelWorkbookOpen Then MyWB.close(False)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyWB)

            If ExcelWasNotRunning Then MyXL.quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyXL)

            MyWB = Nothing
            MyXL = Nothing
            GC.Collect()

            'Dww 11-29-2011 Old code
            'System.Threading.Thread.CurrentThread.CurrentCulture = ci
            culture = culture.GetCulture()
            Dim currentUserCulture As String = Convert.ToString(culture.CultureName)
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(currentUserCulture)

            MyXL.Application.DisplayAlerts = True


        Catch ex As Exception
            'HANDLE ERROR
            MyXL = Nothing
            GC.Collect()
            'Dww 11-29-2011 Old code
            'System.Threading.Thread.CurrentThread.CurrentCulture = ci
            culture = culture.GetCulture()
            Dim currentUserCulture As String = Convert.ToString(culture.CultureName)
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(currentUserCulture)
            Exit Sub
        End Try
    End Sub

    Friend Sub saveWkbWksReferences(ByVal ds As DataSet, _
                                ByVal strForm As String, _
                                ByVal strField As String, _
                                ByVal strValue As String)

        Dim dt As DataTable = ds.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = '" & strForm & "'")

        Try
            dt.Columns.Add(strField)
        Catch ex As Exception
        End Try

        If (dr.GetUpperBound(0) = -1) Then
            Dim newDR As DataRow = dt.NewRow
            newDR("frmName") = strForm
            newDR(strField) = strValue
            dt.Rows.Add(newDR)
        Else
            dr(0).Item("frmName") = strForm
            dr(0).Item(strField) = strValue
        End If

        ds.AcceptChanges()

    End Sub

    Friend Function strImportSetting(ByVal ds As DataSet, _
                                     ByVal strForm As String, _
                                     ByVal strField As String) As String
        Dim rtn As String = ""
        Dim dt As DataTable = ds.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = '" & strForm & "'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            rtn = dr(0).Item(strField)
        End If
        Return rtn
    End Function

	Friend Function RangeHasValue(ByVal rng As Object) As Boolean

		Dim b As Boolean

		b = Not IsDBNull(rng.Value) AndAlso CStr(rng.Text) <> ""

		Return b

	End Function

	Friend Function ReturnDouble(ByVal rng As Object) As Double

		Dim d As Double

		d = CDbl(rng.Value2)

		Return d

	End Function

	Friend Function ReturnDouble(ByVal rng As Object, ByVal def As Double) As Double

		Dim d As Double

		If (IsNumeric(rng.Text)) Then
			d = CDbl(rng.Value2)
		Else
			d = 0.0
		End If

		Return d

	End Function

	Friend Function ReturnSingle(ByVal rng As Object) As Single

		Dim s As Single

		s = CSng(rng.Value2)

		Return s

	End Function

	Friend Function ReturnSingle(ByVal rng As Object, ByVal def As Single) As Single

		Dim s As Single

		If (IsNumeric(rng.Text)) Then
			s = CDbl(rng.Value2)
		Else
			s = 0.0
		End If

		Return s

	End Function

End Module
