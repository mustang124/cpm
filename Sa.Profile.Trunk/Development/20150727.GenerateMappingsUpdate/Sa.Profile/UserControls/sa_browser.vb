Option Compare Binary
Option Explicit On
Option Strict On

Imports WSX509 = Microsoft.Web.Services2.Security.X509
Imports System.Configuration

Friend Class SA_Browser

    Private strKey As String = ""


    Friend Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        '20101004 RRH: Changed _CERT Path If IO.File.Exists("_CERT\" & Policy.GetFileName()) Then
        '20101004 RRH: Changed _CERT Path Dim reader As New IO.StreamReader("_CERT\" & Policy.GetFileName())
        If IO.File.Exists(modAppDeclarations.pathCert & Policy.GetFileName()) Then
            Dim reader As New IO.StreamReader(modAppDeclarations.pathCert & Policy.GetFileName())
            Dim password As String = ""

            If reader.Peek <> -1 Then
                strKey = reader.ReadLine
                Console.WriteLine(strKey)
                reader.Close()
            Else
                Throw New Exception("A key was not found")
            End If

            wbWebBrowser.Navigate("about:blank")
        End If

    End Sub

    Private Sub StartUp()
        '20101004 RRH: Changed _CERT Path Dim reader As New IO.StreamReader("_CERT\" & Policy.GetFileName())
        Dim reader As New IO.StreamReader(modAppDeclarations.pathCert & Policy.GetFileName())
        Dim password As String = ""

        If reader.Peek <> -1 Then
            strKey = reader.ReadLine
            strKey = Replace(strKey, "+", "%2B")
            strKey = Replace(strKey, "/", "%2F")
            strKey = Replace(strKey, "\", "%5C")

            Console.WriteLine(strKey)
            reader.Close()
        Else
            Throw New Exception("A key was not found")
        End If
    End Sub

    '20120110 RRH Removing certificate validation
    'Friend Sub InstallCertificate()
    '    Dim store As WSX509.X509CertificateStore = WSX509.X509CertificateStore.CurrentUserStore(WSX509.X509CertificateStore.RootStore)
    '    Dim fopen As Boolean = store.OpenRead

    '    Dim cers As WSX509.X509CertificateCollection = store.FindCertificateBySubjectString("SolomonAssociates")

    '    'Install certificate if not available
    '    If (cers.Count = 0) Then

    '        Dim result As DialogResult = ProfileMsgBox("YN", "NOCERT", "")
    '        If result = DialogResult.Yes Then
    '            '20101004 RRH: Changed _CERT Path System.Diagnostics.Process.Start("_CERT/Solomon Profile� II.cer")
    '            System.Diagnostics.Process.Start(modAppDeclarations.pathCert + "Solomon Profile� II.cer")
    '        End If

    '    End If

    'End Sub

    Friend Sub DisplayRefnumChart(ByVal refineryID As String, _
                                   ByVal location As String, _
                                   ByVal dataset As String, _
                                   ByVal senario As String, _
                                   ByVal periodStart As Date, _
                                   ByVal periodEnd As Date, _
                                   ByVal chartRow As DataRow, _
                                   ByVal UOM As String, _
                                   ByVal currency As String, _
                                   ByVal studyYear As String, _
                                   ByVal TargetField As String, _
                                   ByVal AvgField As String, _
                                   ByVal YTDField As String)

        Dim v1, v2, v3, v4, v5 As String
        Dim u As Integer

        For u = 0 To chartRow.Table.Columns.Count - 1
            If IsDBNull(chartRow(u)) Then
                chartRow(u) = ""
            End If
        Next

        v1 = chartRow("ValueField1").ToString
        v2 = chartRow("ValueField2").ToString
        v3 = chartRow("ValueField3").ToString
        v4 = chartRow("ValueField4").ToString
        v5 = chartRow("ValueField5").ToString

        If Not IsDBNull(chartRow("Legend1")) Then
            If CStr(chartRow("Legend1")).Length > 0 Then
                v1 = "ISNULL(" + v1 + ",0) AS '" + chartRow("Legend1").ToString + "'"
            End If
        Else
            v1 = "ISNULL(" + v1 + ",0) AS '" + v1 + "'"
        End If

        If Not IsNothing(location.Trim) Then
            If location.Trim.Length > 0 Then
                v1 = v1.Replace("Location", location)
            End If
        End If

        If Not IsDBNull(chartRow("Legend2")) Then
            If CStr(chartRow("Legend2")).Length > 0 Then
                v2 = "ISNULL(" + v2 + ",0) AS '" + chartRow("Legend2").ToString + "'"
            End If
        Else
            v2 = "ISNULL(" + v2 + ",0) AS '" + v2 + "'"
        End If

        If Not IsDBNull(chartRow("Legend3")) Then
            If CStr(chartRow("Legend3")).Length > 0 Then
                v3 = "ISNULL(" + v3 + ",0) AS '" + chartRow("Legend3").ToString + "'"
            End If
        Else
            v1 = "ISNULL(" + v3 + ",0) AS '" + v3 + "'"
        End If

        If Not IsDBNull(chartRow("Legend4")) Then
            If CStr(chartRow("Legend4")).Length > 0 Then
                v4 = "ISNULL(" + v4 + ",0) AS '" + chartRow("Legend4").ToString + "'"
            End If
        Else
            v4 = "ISNULL(" + v4 + ",0) AS '" + v4 + "'"
        End If

        If Not IsDBNull(chartRow("Legend5")) Then
            If CStr(chartRow("Legend5")).Length > 0 Then
                v5 = "ISNULL(" + v5 + ",0) AS '" + chartRow("Legend5").ToString + "'"
            End If
        Else
            v1 = "ISNULL(" + v5 + ",0) AS '" + v5 + "'"
        End If

        DisplayRefnumChart(chartRow("ChartTitle").ToString, _
                           refineryID, _
                           location, _
                           dataset, _
                           senario, _
                           periodStart, _
                           periodEnd, _
                           studyYear, _
                           chartRow("DataTable").ToString, _
                           UOM, _
                           currency, _
                           v1, _
                           YTDField, _
                           AvgField, _
                           TargetField, _
                           v2, _
                           v3, _
                           v4, _
                           v5)
    End Sub

    Friend Sub DisplayRefnumChart(ByVal chartName As String, _
                                  ByVal refineryID As String, _
                                  ByVal location As String, _
                                  ByVal dataset As String, _
                                  ByVal senario As String, _
                                  ByVal startDate As Date, _
                                  ByVal endDate As Date, _
                                  ByVal StudyYear As String, _
                                  ByVal table As String, _
                                  ByVal UOM As String, _
                                  ByVal currency As String, _
                                  ByVal field1 As String, _
                                  Optional ByVal YTD As String = "", _
                                  Optional ByVal twelveMonthAvg As String = "", _
                                  Optional ByVal target As String = "", _
                                  Optional ByVal field2 As String = "", _
                                  Optional ByVal field3 As String = "", _
                                  Optional ByVal field4 As String = "", _
                                  Optional ByVal field5 As String = "")


        

        Dim header As String = "WsP: " & strKey + vbCrLf

        chartName = System.Web.HttpUtility.UrlEncode(chartName)
        field1 = System.Web.HttpUtility.UrlEncode(field1)
        field2 = System.Web.HttpUtility.UrlEncode(field2)
        field3 = System.Web.HttpUtility.UrlEncode(field3)
        field4 = System.Web.HttpUtility.UrlEncode(field4)
        field5 = System.Web.HttpUtility.UrlEncode(field5)

        Dim dt As Date = DateTime.Now
        Dim page As String = ConfigurationManager.AppSettings("ChartControlUrl").ToString()
        Dim stackedChart As Boolean = False
        Select Case chartName.ToUpper()
            Case "NON-VOLUME-RELATED+EXPENSES"
                stackedChart = True
            Case "Net+Raw+Material+Input"
                stackedChart = True
            Case "Personnel+Index"
                stackedChart = True
            Case "Maintenance+Index"
                stackedChart = True
            Case "Maintenance+Efficiency+Index"
                stackedChart = True
            Case "Cash+Operating+Expenses+UEDC+Basis"
                stackedChart = True
            Case "Energy+Consumption+per+Barrel"
                stackedChart = True
            Case "Maintenance+Index,+Monthly+NTA+&+Annualized+TA"
                stackedChart = True
            Case "Volume-Related+Expenses"
                stackedChart = True
            Case "O,C&C+Personnel+Index"
                stackedChart = True
            Case "M,P&S+Personnel+Index"
                stackedChart = True
            Case "OcC&C+Total+Absences"
                stackedChart = True
            Case "McP&S+Total+Absences"
                stackedChart = True
            Case "Maintenance+Work+Force"
                stackedChart = True
        End Select
        If stackedChart Then
            page = page.Replace("ChartControl.aspx", "ChartControlStacked.aspx")
        End If
        Dim url As String = page + "?TEST=1&cn=" & chartName & "&sd=" & startDate.ToString("M/d/yyyy") & _
                               "&ed=" & endDate.ToString("M/d/yyyy") & _
                               "&ds=" & dataset & _
                               "&sn=" & senario & "&table=" & table & _
                               "&UOM=" & UOM & "&currency=" & currency & "&YR=" & StudyYear & _
                               "&YTD=" & YTD & "&TwelveMonthAvg=" & twelveMonthAvg & _
                               "&target=" & target & _
                               "&field1=" & field1 & "&field2=" & field2 & _
                               "&field3=" & field3 & "&field4=" & field4 & _
                               "&field5=" & field5 & "&TS=" & dt

        wbWebBrowser.Navigate(url, "", Nothing, header)

    End Sub

    Friend Sub DisplayReport(ByVal refineryID As String, _
                                 ByVal location As String, _
                                 ByVal reportCode As String, _
                                 ByVal periodStart As Date, _
                                 ByVal UOM As String, _
                                 ByVal currency As String, _
                                 ByVal studyYear As String, _
                                 ByVal dataSet As String, _
                                 ByVal Scenario As String, _
                                 ByVal includeTarget As Boolean, _
                                 ByVal includeAvg As Boolean, _
                                 ByVal includeYTD As Boolean)

        Dim header As String = "WsP: " & strKey + vbCrLf

        If UOM.StartsWith("US") Then
            UOM = "US"
        Else
            UOM = "MET"
        End If

        Dim dt As Date = DateTime.Now
        If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            wbWebBrowser.Navigate(ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToShortDateString & _
                                                 "&ref=" & refineryID & "&ds=" & dataSet & _
                                                 "&UOM=" & UOM & "&currency=" & currency & _
                                                 "&yr=" & studyYear & _
                                                 "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                 "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt, "", Nothing, header)
        Else
            Dim url As String = ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToString("M/d/yyyy") & _
                                                   "&ds=" & dataSet & _
                                                  "&UOM=" & UOM & "&currency=" & currency & _
                                                  "&yr=" & studyYear & _
                                                  "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                  "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt


            wbWebBrowser.Navigate(url, "", Nothing, header)


        End If
    End Sub

    Friend Sub DisplayInputReport(ByVal ds As DataSet, ByVal tableName As String, ByVal company As String, ByVal location As String, ByVal description As String, ByVal UOM As String, ByVal currency As String, ByVal period As Date, ByVal ht As Hashtable)
        Dim pg As New Page

        wbWebBrowser.DocumentText = pg.createPage(ds, tableName, _
                                                  company, _
                                                  location, _
                                                  description, _
                                                  UOM, currency, _
                                                  Month(period), Year(period), period, ht)

    End Sub

    Friend Sub PrintPage()
        wbWebBrowser.ShowPrintDialog()
    End Sub

    Friend Sub GoHome()
        lblHeader.Text = "www.SolomonOnline.com"
        wbWebBrowser.Visible = True
        wbWebBrowser.Navigate(ConfigurationManager.AppSettings("SolomonUrl").ToString())
    End Sub

    Friend Sub GoHelp()
        lblHeader.Text = "Online Help"
        wbWebBrowser.Visible = True
        wbWebBrowser.Navigate(ConfigurationManager.AppSettings("HelpUrl").ToString())
    End Sub

    Friend Sub PleaseWait()
        If IO.File.Exists(pathTemp & "Templates\PleaseWait.htm") Then _
            wbWebBrowser.Navigate(New Uri(pathTemp & "Templates\PleaseWait.htm"))
    End Sub

    Private Sub GoBlank()
        wbWebBrowser.Navigate("about:blank")
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim myParent As Main = DirectCast(Me.ParentForm, Main)
        myParent.HideTopLayer(Me)
    End Sub

    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim myParent As Main = DirectCast(Me.ParentForm, Main)
        If myParent.PrinterIsInstalled() = False Then Exit Sub
        Try
            PrintPage()
        Catch ex As Exception
            MsgBox("You must be viewing a page before printing.", MsgBoxStyle.Critical, "No Chart")
        End Try
    End Sub

    Private Sub btnDump_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDump.Click
        Dim myParent As Main = DirectCast(Me.ParentForm, Main)
        myParent.GetDataDump()
    End Sub

    Private Sub btnDataEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDataEntry.Click
        Dim myParent As Main = DirectCast(Me.ParentForm, Main)
        myParent.GoToDataEntry()
    End Sub

End Class
