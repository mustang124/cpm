using System;
using System.IO;
namespace GenrateInTableHtml
{
	/// <summary>
	/// Summary description for FileHelper.
	/// </summary>
	public class FileHelper
	{
		public FileHelper()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		///  Reads the file and return the content
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public string ReadFile(string filename)
		{
			string buf="";
			if (File.Exists(filename))
			{
				StreamReader reader = File.OpenText(filename);
				try
				{
					buf = reader.ReadToEnd();
				}
				finally
				{
					reader.Close();
				}
			}
			return buf;
		}
	}
}
