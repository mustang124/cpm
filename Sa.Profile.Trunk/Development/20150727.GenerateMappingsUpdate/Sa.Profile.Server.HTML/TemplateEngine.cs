using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections;

namespace GenrateInTableHtml
{
	/// <summary>
	/// Summary description for TemplateEngine.
	/// </summary>
	public class TemplateEngine
	{
		private bool _useUsUnits = true;
		public TemplateEngine()
		{
			//
			// TODO: Add constructor logic here
			//
		}


        /// <summary>
        /// Flag used thats indicates is US or Metric 
        /// descriptions should be used. Should set before RenderOutput
        /// is called.
        /// </summary>
		public bool UseUSUnits
		{
			get
			{
				return _useUsUnits;
			}
			set
			{
				_useUsUnits=value;
			}
		}

		/// <summary>
		/// Reads a template renders the output
		/// </summary>
		/// <param name="tableName"></param>
		/// <param name="xmlfile"></param>
		/// <param name="templatefile"></param>
		/// <returns></returns>
		public string RenderOutput(DataSet data,string templatefile,Hashtable replacementValues)
		{
			string tableName = "";
			string xmlfile = "";
			string  text = ReadFile(templatefile);
			string body ="";
			DataSet ds;
			

			if (text.Length ==0)
				return "File not found.";
            Hashtable fieldAttribute= new Hashtable();
			
			if (data == null)
				ds = new DataSet();
			else
				ds = data;
			
			try 
			{
				//Read Config section
				if (data == null)
				{
					//Read header
					Match config = Regex.Match(text,@"(?:\s*<!--\s*config-start\s*-->\s*)([\s\w\d\W]*)(?:\s*<!--\s*config-end\s*-->\s*)",RegexOptions.IgnoreCase);
					if ( config.Success)
					{
						
						string cString = config.Groups[1].Value;
						//Matches key-value pairs
						foreach(Match m in Regex.Matches(cString, @"\w+\s*=\s*[^,;\r\n]+") )
						{
							string str = m.Value;
							string key = str.Split("=".ToCharArray())[0];
							string val = str.Split("=".ToCharArray())[1];

							if (key.StartsWith("DATASET"))
								tableName=val;
							else if (key.StartsWith("FILE"))
							{
								xmlfile=val;
								if (File.Exists(xmlfile))
									ds.ReadXml(xmlfile);
							}
							else
								fieldAttribute.Add(key,val);
						}
					}
				}

				// Read template section
				Match template = Regex.Match(text,@"(?:\s*<!--\s*template-start\s*-->\s*)([\s\w\d\W]*)(?:\s*<!--\s*template-end\s*-->\s*)",RegexOptions.IgnoreCase);
				if ( template.Success)
				{
					body = template.Groups[1].Value;
					body = RenderSection(ds,body);			    
					body = CalculateFormula(ds ,body);
					body = HandleColumnNameFormatting(body);
					body = HandleNoShowZero(body);
				    body = HandleTag(body);
					body = HandleReplacementValues(body ,replacementValues);
					body = HandleShowIf(body);
				}
			}
			finally
			{
				ds.Dispose();
				fieldAttribute = null;
			}
			
			return body;
		}


		/// <summary>
		/// Parses templates return html text 
		/// </summary>
		/// <param name="body"></param>
		/// <returns></returns>
		string RenderSection(DataSet ds,string body)
		{
			foreach(Match m in Regex.Matches(body,@"\bSECTION\s*\((.[^,]*),(\w*[^,]*),(\w*[^\)]*)\)[\w\W]*?BEGIN([\w\W]*?)\bEND\b\s*",RegexOptions.IgnoreCase ))
			{
				ds.Relations.Clear();
				
				//Get Section parameters
				string tableName = m.Groups[1].Value;
				string filter = m.Groups[2].Value;
				string sort = m.Groups[3].Value;

                //Get Template body
				string sectionBody=  m.Groups[4].Value;
                string secResults = "";
			
			
				//Establish tables' relationships if they exist
				RelateTable(ds,m.Value );
				
				//Start rendering output 
				if (ds.Tables[tableName].Select(filter).Length >0)
				{
					foreach (DataRow row in ds.Tables[tableName].Select(filter,sort)) 
					{
						string secRows=sectionBody;

						//Handle first table
						foreach (DataColumn column in ds.Tables[tableName].Columns )
						{
							string columnName =column.ColumnName;
							secRows = HandleFormatting(secRows,columnName,row[columnName]);
						}

						//Handles the parentTable columns in table relationships. It does the following
						//  parentData
						//  childData1
						//  childData2
							
						string selectStmt = "";
						int count = 0;
						int totalRelationships = ds.Tables[tableName].ParentRelations.Count ;
							
						//Build select string column1='xxx' AND column2='dxxx' 
						foreach ( DataRelation  relation in ds.Tables[tableName].ParentRelations)
						{
							string relateCol =  relation.ParentColumns[0].ColumnName;
							selectStmt += relateCol+"='"+ row[relateCol].ToString()+"'";
							if (count < totalRelationships-1)
							{
								selectStmt +=" AND ";
								count++;
							}
						}

						if (ds.Tables[tableName].ParentRelations.Count > 0)
						{
							string parentTable = ds.Tables[tableName].ParentRelations[0].ParentTable.TableName;
							foreach(DataRow parentRow in ds.Tables[parentTable].Select(selectStmt))
							{
								foreach (DataColumn column1 in parentRow.Table.Columns )
								{
									string columnName =column1.ColumnName;
									secRows=HandleFormatting(secRows,columnName,parentRow[columnName]);
								}
							}


						}


						secResults+=secRows;
						secResults = HandleHeaders(secResults);
					}
				}
				else 
				{   
					secResults = "";
				}
				

				secResults = HandleIncludeTable(ds,m.Value,secResults);
				
				body=body.Replace(m.Value,secResults);
			}
           
			return body;
		}
        

		public string HandleSubSections(DataSet ds, string body)
		{

			foreach(Match m in Regex.Matches(body,@"\bSUBSECTION\s*\((.[^,]*),(\w*[^,]*),(\w*[^\)]*)\)[\w\W]*?BEGIN([\w\W]*?)\bEND SUBSECTION\b\s*",RegexOptions.IgnoreCase ))
			{
				ds.Relations.Clear();
				
				//Get Section parameters
				string tableName = m.Groups[1].Value;
				string filter = m.Groups[2].Value;
				string sort = m.Groups[3].Value;
				//Get Template body
				string sectionBody=  m.Groups[4].Value;
				string secResults = "";
			
			
				//Establish tables' relationships if they exist
				RelateTable(ds,m.Value );
				
				//Start rendering output 
				if (ds.Tables[tableName].Select(filter).Length >0)
				{
					foreach (DataRow row in ds.Tables[tableName].Select(filter,sort)) 
					{
						string secRows=sectionBody;

						//Handle first table
						foreach (DataColumn column in ds.Tables[tableName].Columns )
						{
							string columnName =column.ColumnName;
							secRows = HandleFormatting(secRows,columnName,row[columnName]);
					
						}

						//Handles the parentTable columns in table relationships. It does the following
						//  parentData
						//      childData1
						//  childData2
							
						string selectStmt = "";
						int count = 0;
						int totalRelationships = ds.Tables[tableName].ParentRelations.Count ;
							
						//Build select string column1='xxx' AND column2='dxxx' 
						foreach ( DataRelation  relation in ds.Tables[tableName].ParentRelations)
						{
							string relateCol =  relation.ParentColumns[0].ColumnName;
							selectStmt += relateCol+"='"+ row[relateCol].ToString()+"'";
							if (count < totalRelationships-1)
							{
								selectStmt +=" AND ";
								count++;
							}
						}

						if (ds.Tables[tableName].ParentRelations.Count > 0)
						{
							string parentTable = ds.Tables[tableName].ParentRelations[0].ParentTable.TableName;
							foreach(DataRow parentRow in ds.Tables[parentTable].Select(selectStmt))
							{
								foreach (DataColumn column1 in parentRow.Table.Columns )
								{
									string columnName =column1.ColumnName;
									secRows=HandleFormatting(secRows,columnName,parentRow[columnName]);
								}
							}
						}


						secResults+=secRows;
						secResults = HandleHeaders(secResults);
					}
				}
				else 
				{   
					// If there no rows returned ,replace each column name 
					// with empty strings
					string secRows=sectionBody;
					foreach (DataColumn column in ds.Tables[tableName].Columns )
					{
						string columnName =column.ColumnName;
						secRows = HandleFormatting(secRows,columnName,String.Empty);
					}

					foreach ( DataRelation  relation in ds.Tables[tableName].ParentRelations)
					{
						DataTable parentTable = relation.ParentTable ;
						foreach (DataColumn column in parentTable.Columns )
						{
							string columnName =column.ColumnName;
							secRows = HandleFormatting(secRows,columnName,String.Empty);
						}
					}
					secResults+=secRows;
					secResults = HandleHeaders(secResults);
				}
				

				secResults = HandleIncludeTable(ds,m.Value,secResults);
				
				body=body.Replace(m.Value,secResults);
			}
           
			return body;
		}


		/// <summary>
		/// Handles fields that dependent on UOM setting
		/// </summary>
		/// <param name="body"></param>
		/// <returns></returns>
		public string HandleUnits(string body)
		{
			foreach(Match m in Regex.Matches(body,@"\b[Uu]seUnit\((.[^,]*),(.[^,)]*)??\)",RegexOptions.IgnoreCase ))
			{
				string USDecription = m.Groups[1].Value;
				string MetricDescription = m.Groups[2].Value;
				string replacement="";
				if (UseUSUnits)
					replacement = USDecription;
				else
					replacement = MetricDescription;

				//Check if the replacement value is a string or columnname.
				//Anything begining with a ' or " means it is not a column name
                if (replacement.StartsWith("'")||replacement.StartsWith("\"")) 
					replacement=replacement.Split("'\"".ToCharArray())[1];
				else
					replacement = "@" + replacement;

				body = body.Replace(m.Value,replacement);
			}
			return body;
		}

		/// <summary>
		/// Doesn't show zeros
		/// </summary>
		/// <param name="body"></param>
		/// <returns></returns>
		public string HandleNoShowZero(string body)
		{
				foreach(Match m in Regex.Matches(body,@"\b[Nn]oShowZero\(([\w\W]*?)\s*\)",RegexOptions.Multiline ))
				{   		
						string replacement=m.Groups[1].Value.Trim();
					 
					   //Check if it is numercial instead of a blank 
						if (Regex.Match(replacement,@"^[-]?\d").Success)
						{
							if (Convert.ToDouble(replacement)==0)
								replacement = String.Empty;
							body = body.Replace(m.Value,replacement);
						}
						else
						{
							//Handles the case when FORMAT have not been resolved
							if (replacement.LastIndexOf("(") > 0)
							{
								replacement = String.Empty;
								body = body.Replace(m.Value+")",replacement);
							}
							else
							{ //Handles case when Column is unmatched
								replacement = String.Empty;
								body = body.Replace(m.Value,replacement);
							}
						}
				}
				return body;
			
		}


		/// <summary>
		/// Remove duplicate headers
		/// </summary>
		/// <param name="body"></param>
		/// <returns></returns>
		public string HandleHeaders(string body)
		{
			foreach(Match m in Regex.Matches(body,@"\b[Hh]eader\(\'\s*([\s\w\W]*?)\s*\'\)",RegexOptions.IgnoreCase ))
			{   
				string replacement=m.Groups[1].Value ;
				if ( body.IndexOf(replacement)!=0 && (body.IndexOf(replacement) != body.LastIndexOf(replacement)))
					 replacement="";

				
				body = body.Replace(m.Value,replacement);
			}
			
			return body;
		}


		// Return included tables
		public string  HandleIncludeTable(DataSet ds,string section, string body )
		{ 
			string includedTable;
			
			foreach (Match m in Regex.Matches(section,@"\b[Ii]ncludeTable\((.[^,\)]*)\)",RegexOptions.IgnoreCase))
			{
               includedTable= m.Groups[1].Value;
				

				if (ds.Tables[includedTable].Rows.Count > 0)
				{
					//Search for column names in the included tables
					foreach(DataRow parentRow in ds.Tables[includedTable].Rows)
					{
						foreach (DataColumn column1 in parentRow.Table.Columns )
						{
							string columnName =column1.ColumnName;
							body = HandleFormatting(body,columnName,parentRow[columnName]);
						}
					}
				}
			}
			return body;
		}

		/// <summary>
		/// Create relationships between tables
		/// </summary>
		public void  RelateTable(DataSet ds,string body )
		{   
			string parentTableName;
			string childTableName; 
			string field1; 
			string field2;
            
			foreach (Match m in Regex.Matches(body,@"\b[Rr]elate\((.[^,]*),(.[^,]*),(.[^,]*),(.[^,)]*)?\)",RegexOptions.IgnoreCase))
			{
                parentTableName= m.Groups[1].Value;
				childTableName =  m.Groups[2].Value;
				field1 =  m.Groups[3].Value;
				field2 = m.Groups[4].Value;
				
				DataColumn parentColumn = ds.Tables[parentTableName].Columns[field1];
				DataColumn childColumn = ds.Tables[childTableName].Columns[field2];
				ds.Relations.Add(new DataRelation(field1,parentColumn,childColumn,false));
			}
			
		}

		/// <summary>
		/// Handles format that is given from the database
		/// </summary>
		/// <param name="body"></param>
		/// <returns></returns>
		string HandleColumnNameFormatting(string body)
		{
			
			foreach(Match smc in  Regex.Matches(body,@"\b[Ff]ormat\(\s*(\w*[^,a-dg-zA-DG-Z]*)\s*,\s*([\w\W]*?)\s*\){1}",RegexOptions.IgnoreCase))
			{
				string format="";
				string columnValue=smc.Groups[1].Value;
				double val;
				if (columnValue != null && !columnValue.StartsWith("@"))
				{
					if(columnValue.Trim().Length > 0 ) 
					{
						val = Double.Parse(columnValue);
					}
					else
						val = 0;
				
				
					switch (smc.Groups[2].Value.Trim() )
					{
						case "0":
							format = "{0:#,##0}";
							break;
						case "1":
							format = "{0:#,##0.0}";
							break;
						case "2":
							format = "{0:N}";
							break;
						default:

							if (smc.Groups[2].Value.Trim().StartsWith("'"))
							{
								string [] a = smc.Groups[2].Value.Trim().Split("'".ToCharArray());
								if (a.Length > 2)
									format = "{0:" + a[1] + "}"; 
							
							}
							break;						
					}

			
					if (format.Trim().Length >0)
						body = Regex.Replace(body, @"\b[Ff]ormat\(\s*"+ columnValue +@"\s*,\s*" + smc.Groups[2].Value.Trim() + @"\s*?\)",String.Format(format,val).Trim(),RegexOptions.IgnoreCase);	
					else
						body = Regex.Replace(body, @"\b[Ff]ormat\(\s*"+ columnValue +@"\s*,\s*" + smc.Groups[2].Value.Trim() + @"\s*?\)",String.Empty ,RegexOptions.IgnoreCase);	
				}
				else
					body = Regex.Replace(body, @"\b[Ff]ormat\(\s*"+ columnValue +@"\s*,\s*" + smc.Groups[2].Value.Trim() + @"\s*?\)",String.Empty ,RegexOptions.IgnoreCase);	
			}
			
			return body;
		}

		/// <summary>
		/// Handles number formatting and colume name subsitution
		/// </summary>
		/// <param name="secRow"></param>
		/// <param name="columnName"></param>
		/// <param name="columnValue"></param>
		/// <returns></returns>
		string HandleFormatting(string secRow ,string columnName, object columnValue)
		{
			Match sm = Regex.Match(secRow,@"\b[Ff]ormat\(\s*("+columnName+@")\s*\,\'(.[^']*)\'?\)",RegexOptions.IgnoreCase);
			if (columnValue != null)
			{
				if (sm.Success)
				{
					//Added for mixed type columns like varchar
					if ((columnValue is String) && (Regex.Match(columnValue.ToString(),@"^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$").Success ))
						columnValue = Convert.ToDouble(columnValue);
					
					string format=sm.Groups[2].Value ;
					//Replace format clause
					secRow = Regex.Replace(secRow, @"\b[Ff]ormat\(\s*"+columnName+@"\s*\,\s*\'"+ format + @"\'\s*?\)",String.Format("{0:"+format+"}",columnValue).Trim(),RegexOptions.IgnoreCase );	
						
				}
				else
				{
					//Replace column name with value 
					secRow=Regex.Replace(secRow,@"@\b"+columnName+@"\b",columnValue.ToString().Trim(),RegexOptions.IgnoreCase );
				}
			}
			else
				secRow=Regex.Replace(secRow,@"@\b"+columnName+@"\b",String.Empty,RegexOptions.IgnoreCase );
			return secRow;
		}

		/// <summary>
		/// Convert TAGs to a cascade stylesheet class
		/// Can use cascade stylesheets to make elements hidden and more.
		/// </summary>
		/// <param name="secRow"></param>
		/// <param name="columnName"></param>
		/// <param name="columnValue"></param>
		/// <returns></returns>
		string HandleTag(string body)
		{
			foreach(Match sm in Regex.Matches(body,@"\b[Tt]ag\(\s*(\w*)\s*,([\w\W]*?)\){1}",RegexOptions.IgnoreCase))
			{
				string cssClassName = sm.Groups[1].Value ;
				string txt = sm.Groups[2].Value ;
				
					//Replace the TAG clause
					body = Regex.Replace(body, @"\b[Tt]ag\(\s*"+cssClassName+@"\s*\,\s*"+ txt + @"\s*?\)",@"<span class='" + cssClassName + "'>" + txt + @"</span>",RegexOptions.IgnoreCase );	
				
			}
			return body;
		}

		/// <summary>
		/// Convert TAGs to a cascade stylesheet class
		/// Can use cascade stylesheets to make elements hidden and more.
		/// </summary>
		/// <param name="secRow"></param>
		/// <param name="columnName"></param>
		/// <param name="columnValue"></param>
		/// <returns></returns>
		string HandleShowIf(string body)
		{
			foreach(Match sm in Regex.Matches(body,@"\b[Ss]howIf\(\s*(\w*)\s*,([\w\W]*?)\){1}",RegexOptions.IgnoreCase))
			{
				string cssClassName = sm.Groups[1].Value ;
				string test = sm.Groups[2].Value ;
				string hiddenStyle ="<style>." + cssClassName + "{display:none;} </style>";
				//Replace the TAG clause
				
				if (Boolean.Parse(test))
					body = Regex.Replace(body, @"\b[Ss]howIf\(\s*"+cssClassName+@"\s*\,([\w\W]*?)\){1}",String.Empty,RegexOptions.IgnoreCase );	
				else
					body = Regex.Replace(body, @"\b[Ss]howIf\(\s*"+cssClassName+@"\s*\,([\w\W]*?)\){1}",hiddenStyle,RegexOptions.IgnoreCase );	
			}
			return body;
		}
		/// <summary>
		/// Handles that Formula Tag. Computes equation given in the parameter 
		/// </summary>
		/// <param name="table"></param>
		/// <param name="formula"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		string CalculateFormula(DataSet ds , string body  )
		{	
			string formula="";
			string filter="";
			string format="";
			string tableName="";

			foreach(Match m in Regex.Matches(body,@"\b[Ff]ormula\((.[^,]*),(.[^,]*),(.[^,]*)?,\'(.[^']*)\'?\)"))
			{
                string str = m.Value;
			   
				//Get parameters
				
				tableName= m.Groups[1].Value;
				formula = m.Groups[2].Value;
				filter = m.Groups[3].Value;
				format = m.Groups[4].Value;
				
				// Declare an object variable.
				object answer;
				answer = ds.Tables[tableName].Compute(formula, filter);
				body = body.Replace(str,String.Format("{0:"+format+"}",answer));
			}
			return body;
		}

		/// <summary>
		/// Replace custom tags in the template string with the value given.
		/// This is used with SHOWIF and any other future condition statements.
		/// </summary>
		/// <param name="body"></param>
		/// <param name="tags"></param>
		/// <returns></returns>
		string HandleReplacementValues(string body , Hashtable replacementValues)
		{
		
			IDictionaryEnumerator en = replacementValues.GetEnumerator();
			while (en.MoveNext())
			{

				 body = body.Replace((string)en.Key,(string)en.Value );
			}

			return body;
		}

		/// <summary>
		///  Reads the file and return the content
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public string ReadFile(string filename)
		{
			string buf="";
			if (File.Exists(filename))
			{
				StreamReader reader = File.OpenText(filename);
				try
				{
					buf = reader.ReadToEnd();
					buf=HandleUnits(buf);
				}
				finally
				{
					reader.Close();
				}
			}
			return buf;
		}

		

	}
}
