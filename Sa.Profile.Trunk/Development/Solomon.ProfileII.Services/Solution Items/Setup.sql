﻿USE [ProfileFuels12]
GO
/*
DROP VIEW [dbo].[Submissions];
drop TABLE [dbo].[SubmissionsAll];
*/

/*
CREATE TABLE [dbo].[SubmissionsAll](
	[SubmissionID] [int]  NOT NULL,
	[RefineryID] [char](6) NOT NULL,
	[DataSet] [varchar](15) NOT NULL,
	[PeriodStart] [smalldatetime] NULL,
	[PeriodEnd] [smalldatetime] NULL,
	[RptCurrency] [varchar](5) NOT NULL,
	[RptCurrencyT15] [varchar](5) NULL,
	[UOM] [varchar](5) NOT NULL,
	[Company] [nvarchar](30) NULL,
	[Location] [nvarchar](30) NULL,
	[CoordName] [nvarchar](50) NULL,
	[CoordTitle] [nvarchar](50) NULL,
	[CoordPhone] [varchar](40) NULL,
	[CoordEMail] [varchar](255) NULL,
	[NumDays] [smallint] NULL,
	[FractionOfYear] [real] NULL,
	[Submitted] [smalldatetime] NULL,
	[LastCalc] [smalldatetime] NULL,
	[CalcsNeeded] [char](1) NULL,
	[PeriodYear] [smallint] NULL,
	[PeriodMonth] [tinyint] NULL,
	[BridgeVersion] [varchar](10) NULL,
	[ClientVersion] [varchar](10) NULL,
	[UseSubmission] [bit] NULL DEFAULT ((1)),
 CONSTRAINT [PK_Submissions] PRIMARY KEY CLUSTERED 
(
	[SubmissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE VIEW [dbo].[Submissions]
AS
SELECT SubmissionID, RefineryID, DataSet, PeriodStart, PeriodEnd, RptCurrency, RptCurrencyT15, UOM, Company, Location,
	CoordName, CoordTitle, CoordPhone, CoordEMail, NumDays, FractionOfYear, Submitted, LastCalc, CalcsNeeded, PeriodYear, PeriodMonth,
	BridgeVersion, ClientVersion, UseSubmission
FROM dbo.SubmissionsAll WHERE UseSubmission = 1
GO
*/
/*
INSERT INTO
 [ProfileFuels12].[dbo].[SubmissionsAll]
(
[SubmissionID]
      ,[RefineryID]
      ,[DataSet]
      ,[PeriodStart]
      ,[PeriodEnd]
      ,[RptCurrency]
      ,[RptCurrencyT15]
      ,[UOM]
      ,[Company]
      ,[Location]
      ,[CoordName]
      ,[CoordTitle]
      ,[CoordPhone]
      ,[CoordEMail]
      ,[NumDays]
      ,[FractionOfYear]
      ,[Submitted]
      ,[LastCalc]
      ,[CalcsNeeded]
      ,[PeriodYear]
      ,[PeriodMonth]
      ,[BridgeVersion]
      ,[ClientVersion]
      ,[UseSubmission]
)
Values
(
7046,'XXPAC','Actual','2016-06-01','2016-07-01','SGD','USD','MET','Solomon Refining','Singapore','Samantha',
'Solomon Coordinator','972-739-1720','Profile@solomononline.com',
30,0.082191,'2016-07-08','2016-07-13',NULL,2016,6,null,'5.15.7.7',1
)
GO

CREATE PROCEDURE [dbo].[spGetReportProcs](@RefineryID varchar(6), @ReportName varchar(50))
AS
IF EXISTS (SELECT * FROM dbo.Report_LU WHERE ReportName = @ReportName AND CustomGroup IN (0,255))
	SELECT     ReportProcs.DataTableName, ReportProcs.ProcName, Report_LU.Template
	FROM         Report_LU INNER JOIN
	                      ReportProcs ON Report_LU.CustomGroup = ReportProcs.CustomGroup AND Report_LU.ReportCode = ReportProcs.ReportCode
	WHERE     (Report_LU.CustomGroup IN (0,255)) AND (Report_LU.ReportName = @ReportName)
ELSE BEGIN
	SELECT     ReportProcs.DataTableName, ReportProcs.ProcName, Report_LU.Template
	FROM         Report_LU INNER JOIN
	                      ReportProcs ON Report_LU.CustomGroup = ReportProcs.CustomGroup AND Report_LU.ReportCode = ReportProcs.ReportCode
	WHERE     (Report_LU.ReportName = @ReportName)
		AND (Report_LU.CustomGroup IN (SELECT CustomGroup
			FROM CoCustom cc INNER JOIN TSort t ON t.CompanyID = cc.CompanyID
			WHERE cc.CustomType = 'R' AND t.RefineryID = @RefineryID))
END
GO

CREATE TABLE [dbo].[ReportProcs](
	[CustomGroup] [tinyint] NOT NULL,
	[ReportCode] [char](10) NOT NULL,
	[DataTableName] [varchar](50) NOT NULL,
	[ProcName] [sysname] NOT NULL,
	[DataDumpTableName] [varchar](50) NULL,
	[DataDumpOrder] [tinyint] NULL,
 CONSTRAINT [PK_ReportProcs] PRIMARY KEY CLUSTERED 
(
	[CustomGroup] ASC,
	[ReportCode] ASC,
	[DataTableName] ASC
))
GO

INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'Avail1    ','Total','dbo.spReportAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'Avail1    ','Unit','dbo.spReportUnitAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'Avail24   ','Total','dbo.spReportAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'Avail24   ','Unit','dbo.spReportUnitAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EDCFactors','ProcessUnits','dbo.spReportFactorDetailsUnit',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EDCFactors','UnitsOffsites','dbo.spReportFactorDetailsOffsites',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EffFactors','ProcessUnits','dbo.spReportFactorDetailsUnit',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EffFactors','UnitsOffsites','dbo.spReportFactorDetailsOffsites',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EII       ','Asphalt','dbo.spReportEIIOffsitesAsphalt','Offsites','4');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EII       ','Functions','dbo.spReportEIIVEIUnits','[ByUnit]','3');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EII       ','ProcessData','dbo.spReportProcessDataSA',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EII       ','SensHeat','dbo.spReportEIISensHeat','Sensible_Heat','2');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EII       ','Summary','dbo.spReportEIIVEISummary','Summary','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'EIIOld    ','EII','dbo.spReportEIIVEI',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','Asphalt','dbo.spReportGPVAsphalt','Asphalt','3');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','Category','dbo.spReportGPVCategories','Category_Summary','0');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','Coke','dbo.spReportGPVCoke','Coke','5');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','Crudes','dbo.spReportCrude','Crudes','10');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','Lubes','dbo.spReportGPVLubes','Lube_Products','2');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','MiscProd','dbo.spReportGPVMPROD','Misc_Products','6');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','OtherRM','dbo.spReportGPVOTHRM','Other_Raw_Materials','11');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','Prod','dbo.spReportGPVProd','Primary_Products','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'GPV       ','Solvents','dbo.spReportGPVSolvents','Solvents','4');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'PD        ','ProcessUnits','dbo.spReportFactorDetailsUnit','Process Units','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'PD        ','UnitsOffsites','dbo.spReportFactorDetailsOffsites','Units and Offsites','2');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'PERS      ','Personnel','dbo.spReportPers','PersDetails','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'PS        ','Generated','dbo.spReportPUScorecardOffsites','[ByOffsite]','2');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'PS        ','ProcessUnits','dbo.spReportPUScorecardUnit','[ByUnit]','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'PS        ','RawMaterial','dbo.spReportPUScorecardRS','Utilities_Offsites','3');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','Average','dbo.spReportRSAverage','Average_Data','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','Chart_LU','dbo.spReportChartLUTot',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','Energy','dbo.spReportRSEnergy','Energy','3');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','GENSUM','dbo.spReportGensum',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','MAINTAVAILCALC','dbo.spReportAvail',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','Maintenance','dbo.spReportRSMaintenance','Maintenance','4');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','OpEx','dbo.spReportRSOpex','Operating_Expenses','5');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','Performance','dbo.spReportRSPerformance','Performance_Indicators','2');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','Pers','dbo.spReportRSPers','Personnel','6');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','UserDefined','dbo.spReportUserDefined','User_Defined','8');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'RS        ','Yield','dbo.spReportRSYield','Yields_and_Margins','7');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'SENS      ','SensibleHeat','dbo.spReportSensibleHeat2','SensibleHeat','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'T4        ','Opex','dbo.spReportOpex','[ByOpexDataType]','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TA        ','Turnarounds','dbo.spReportTA',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TAAnn     ','TATotals','dbo.spReportTATotals',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TAAnn     ','Turnarounds','dbo.spReportTA',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','Chart_LU','dbo.spReportChartLU',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','GENSUM','dbo.spReportGensum',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','GENSUM_FIRST_PREVIOUS','dbo.spReportGensumTrendP1',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','GENSUM_SECOND_PREVIOUS','dbo.spReportGensumTrendP2',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','GENSUM_THIRD_PREVIOUS','dbo.spReportGensumTrendP3',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','MAINTAVAILCALC','dbo.spReportAvail',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','MAINTAVAILCALC_FIRST_PREVIOUS','dbo.spReportAvailTrendP1',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','MAINTAVAILCALC_SECOND_PREVIOUS','dbo.spReportAvailTrendP2',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','MAINTAVAILCALC_THIRD_PREVIOUS','dbo.spReportAvailTrendP3',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'TRE       ','UserDefined','dbo.spReportUserDefinedTrend',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (0,'VAL       ','DataCheck','dbo.spReportValidation',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (1,'CDU       ','CustomUnitData','dbo.spReportCustomUnitCDU','CDU','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (1,'FCC       ','CustomUnitData','dbo.spReportCustomUnitFCC','FCC','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (1,'HYC       ','CustomUnitData','dbo.spReportCustomUnitHYC','HYC','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (1,'PXYL      ','CustomUnitData','dbo.spReportCustomUnitPXYL','PXYL','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (1,'REF       ','CustomUnitData','dbo.spReportCustomUnitREF','REF','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (2,'CVX_AvgKPI','CVX_AvgKPI','dbo.spReportChevronKPI_Avg','CVX_KPI_AVG','2');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (2,'CVX_KPI   ','CVX_KPI','dbo.spReportChevronKPI','CVX_KPI','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (2,'CVX_RECON ','CVX_Recon','dbo.spReportChevronRecon','CVX_Recon','3');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (3,'BUR       ','GENSUM','dbo.spReportValeroBUR','BUR_Scorecard','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (3,'BURMo     ','BURMonthly','dbo.spReportValeroBURMonthly','BUR_Monthly','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (3,'BURYTD    ','BURYTD','dbo.spReportValeroBUR_YTD','BUR_YTD','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (4,'ChartData ','ChartData','dbo.spReportThaiOilChartData','ChartData','2');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (4,'KPIs      ','KPIs','dbo.spReportThaiOilKPI','KPIs','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (4,'THAIPS    ','Generated','dbo.spReportPUScorecardOffsites','[ByOffsite]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (4,'THAIPS    ','ProcessUnits','dbo.spReportPUScorecardUnit','[ByUnit]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (4,'THAIPS    ','RawMaterial','dbo.spReportPUScorecardRS','Utilities_Offsites',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (6,'HuskySC   ','HuskySC','dbo.spReportHuskyCustomSC','Management Scorecard','1');
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashAvl1  ','Total','dbo.spReportAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashAvl1  ','Unit','dbo.spReportUnitAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashAvl24 ','Total','dbo.spReportAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashAvl24 ','Unit','dbo.spReportUnitAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEDCFac','ProcessUnits','dbo.spReportFactorDetailsUnit',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEDCFac','UnitsOffsites','dbo.spReportFactorDetailsOffsites',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEffFac','ProcessUnits','dbo.spReportFactorDetailsUnit',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEffFac','UnitsOffsites','dbo.spReportFactorDetailsOffsites',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEII   ','Asphalt','dbo.spReportEIIOffsitesAsphalt',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEII   ','CDUUtil','dbo.spReportEIICDUUtil',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEII   ','Functions','dbo.spReportEIIVEIUnits',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEII   ','MatlBal','dbo.spReportEIIMaterialBal',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEII   ','ProcessData','dbo.spReportProcessDataSA',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEII   ','SensHeat','dbo.spReportEIISensHeat',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashEII   ','Summary','dbo.spReportEIIVEISummary',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashPS    ','Generated','dbo.spReportPUScorecardOffsites','[ByOffsite]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashPS    ','ProcessUnits','dbo.spReportPUScorecardUnit','[ByUnit]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashPS    ','RawMaterial','dbo.spReportPUScorecardRS','Utilities_Offsites',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTA    ','Turnarounds','dbo.spReportTA',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTAAnn ','TATotals','dbo.spReportTATotals',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTAAnn ','Turnarounds','dbo.spReportTA',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','Chart_LU','dbo.spReportPLSCItemsRussian',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','GENSUM','dbo.spReportGensum',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','GENSUM_FIRST_PREVIOUS','dbo.spReportGensumTrendP1',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','GENSUM_SECOND_PREVIOUS','dbo.spReportGensumTrendP2',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','GENSUM_THIRD_PREVIOUS','dbo.spReportGensumTrendP3',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','MAINTAVAILCALC','dbo.spReportAvail',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','MAINTAVAILCALC_FIRST_PREVIOUS','dbo.spReportAvailTrendP1',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','MAINTAVAILCALC_SECOND_PREVIOUS','dbo.spReportAvailTrendP2',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','MAINTAVAILCALC_THIRD_PREVIOUS','dbo.spReportAvailTrendP3',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (7,'BashTrend ','UserDefined','dbo.spReportUserDefinedTrend',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (8,'EPPS      ','Generated','dbo.spReportPUScorecardOffsites','[ByOffsite]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (8,'EPPS      ','ProcessUnits','dbo.spReportPUScorecardUnit','[ByUnit]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (8,'EPPS      ','RawMaterial','dbo.spReportPUScorecardRS','Utilities_Offsites',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (10,'DELEKPS   ','Generated','dbo.spReportPUScorecardOffsites','[ByOffsite]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (10,'DELEKPS   ','ProcessUnits','dbo.spReportPUScorecardUnit','[ByUnit]',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (10,'DELEKPS   ','RawMaterial','dbo.spReportPUScorecardRS','Utilities_Offsites',NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'EIIPL     ','Asphalt','dbo.spReportEIIOffsitesAsphalt',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'EIIPL     ','CDUUtil','dbo.spReportEIICDUUtil',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'EIIPL     ','Functions','dbo.spReportEIIVEIUnits',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'EIIPL     ','MatlBal','dbo.spReportEIIMaterialBal',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'EIIPL     ','ProcessData','dbo.spReportProcessDataSA',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'EIIPL     ','SensHeat','dbo.spReportEIISensHeat',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'EIIPL     ','Summary','dbo.spReportEIIVEISummary',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'MechAvl1  ','Total','dbo.spReportAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'MechAvl1  ','Unit','dbo.spReportUnitAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'MechAvl24 ','Total','dbo.spReportAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'MechAvl24 ','Unit','dbo.spReportUnitAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'OpAvl1    ','Total','dbo.spReportAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'OpAvl1    ','Unit','dbo.spReportUnitAvailMonth',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'OpAvl24   ','Total','dbo.spReportAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'OpAvl24   ','Unit','dbo.spReportUnitAvail24Month',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','Chart_LU','dbo.spReportPLScorecardItems',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','GENSUM','dbo.spReportGensum',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','GENSUM_FIRST_PREVIOUS','dbo.spReportGensumTrendP1',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','GENSUM_SECOND_PREVIOUS','dbo.spReportGensumTrendP2',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','GENSUM_THIRD_PREVIOUS','dbo.spReportGensumTrendP3',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','MAINTAVAILCALC','dbo.spReportAvail',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','MAINTAVAILCALC_FIRST_PREVIOUS','dbo.spReportAvailTrendP1',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','MAINTAVAILCALC_SECOND_PREVIOUS','dbo.spReportAvailTrendP2',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','MAINTAVAILCALC_THIRD_PREVIOUS','dbo.spReportAvailTrendP3',NULL,NULL);
INSERT INTO  [ProfileFuels12].[dbo].[ReportProcs] (CustomGroup,ReportCode,DataTableName,ProcName,DataDumpTableName,DataDumpOrder) VALUES (255,'PLTrend   ','UserDefined','dbo.spReportUserDefinedTrend',NULL,NULL);






CREATE PROCEDURE [profile].[spGetChart_LU]
	@CompanyID nvarchar(100)
AS
 SELECT RTRIM(ChartTitle) as ChartTitle,RTRIM(SectionHeader) as SectionHeader, 
 SortKey,RTRIM(ChartType) as ChartType,RTRIM(AxisLabelUS) as AxisLabelUS, 
 RTRIM(AxisLabelMetric) as AxisLabelMetric,RTRIM(DataTable) as DataTable,RTRIM(ValueField1) as ValueField1, 
 RTRIM(ValueField2) as ValueField2,RTRIM(ValueField3) as ValueField3,RTRIM(ValueField4) as ValueField4, 
 RTRIM(ValueField5) as ValueField5,RTRIM(TargetField) as TargetField,RTRIM(YTDField) as YTDField, 
 RTRIM(AvgField) as AvgField,RTRIM(Legend1) As Legend1,RTRIM(Legend2) As Legend2,RTRIM(Legend3) As Legend3, 
 RTRIM(Legend4) As Legend4,RTRIM(Legend5) As Legend5,DecPlaces, VKey FROM Chart_LU WHERE 
 CustomGroup=0 OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CompanyID= @CompanyID AND CustomType='C')  
 ORDER BY SortKey 
GO

CREATE PROCEDURE [profile].[spGetReport_LU]
	@CompanyID nvarchar(100)
AS
 SELECT RTRIM(ReportCode) AS ReportCode, RTRIM(ReportName) AS ReportName, SortKey, CustomGroup FROM Report_LU 
 WHERE CustomGroup=0 OR CustomGroup 
 IN ( SELECT CustomGroup FROM CoCustom WHERE CompanyID= @CompanyID and CustomType='R') ORDER BY SortKey
GO

CREATE PROCEDURE [profile].[spGetTargetingPeerGroups]
	@RefineryID NVARCHAR(15)
AS
select p.PGKey, p.RegionCode,p.StudyYear,p.Methodology,p.LongText
  from ResultsDB.dbo.PeerGroupDef p where p.PGKey in
 (Select  DISTINCT t.PGKey FROM ResultsDB.dbo.PeerGroupDef t
    inner join profile.PeerGroups2014 p on t.PGKey=p.PGKey 
    inner join profile.RefnumStudies r on p.Study =  
     (select CASE 
		WHEN CHARINDEX('PAC',Studies2014) >0 THEN 'PAC' 
		WHEN CHARINDEX('NSA',Studies2014) >0 THEN 'NSA' 
		WHEN CHARINDEX('EUR',Studies2014) >0 THEN 'EUR' 
		ELSE '' 
	  END  from profile.RefnumStudies where RefID= RTRIM(@RefineryID) 
	 )  
  )
 order by p.LongText asc;
GO

CREATE PROCEDURE [profile].[spGetTargetingRegionCodes]
AS
SELECT	p.PGKey,p.Study
,p.StudyYear,p.Methodology,p.RegionCode,p.MajorGroup,p.SubGroup,p.PricingScenario
,p.LongText,p.StudyRefnum,p.SourceDatabase,v.VKey,v.Variable,r.numberValue,r.Currency
 FROM  	ResultsDB.dbo.RefineryData r
 INNER JOIN  ResultsDB.dbo.VarDef v
 ON    r.VKey = v.VKey
 INNER JOIN  ResultsDB.dbo.PeerGroupDef p
 ON    p.PGKey = r.PGKey
 AND    p.StudyYear IN (2010,2012,2014) 
 AND    p.Study IN ('NSA','EUR','PAC') 
 AND    p.RegionCode IN ('AP','APME','CAN','CSE','EAME','Fuels','JPM','LTA','ME','NSA','USA','WEUR','SEA','AUS') 
 AND    p.MajorGroup IN ('All','Area','CashMargin','EDCGroup','EII','MaintIndex','MechAvail','OpAvail','OpexUEDC','PADD','PersIndex','ProcessUtilPcnt','ROI','RSC','RPG')
 WHERE r.VKey IN (462,471,473,474,476,478,480,481,489,491,504,506,507,510,563,570,571,572,583,585,587,588,1630,1820,2071,9849)
 AND    ((p.PublishEuro = 0 AND r.Currency = 'USD') OR (p.PublishEuro = 1 and r.Currency = 'EUR'))
GO

CREATE PROCEDURE [profile].[spGetReportLayout_LU]
AS
	select * from [profile].[ReportLayout_LU] where Active = 1 order by SortKey asc;
GO

*/