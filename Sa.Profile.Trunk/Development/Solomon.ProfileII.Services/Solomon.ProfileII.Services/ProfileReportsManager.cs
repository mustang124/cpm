﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenrateInTableHtml;
using Solomon.ProfileII.Contracts;
using Solomon.ProfileII.Logger;
using System.Web.Services.Protocols;

namespace Solomon.ProfileII.Services
{
	public class ProfileReportsManager 
	{
		char _delim = Convert.ToChar(14);
		string _validateKeyMsg = string.Empty;
		List<string> _peerGroupsChosenForTargeting = new List<string>();


		public ProfileReportsManager(string refineryId)
		{

		}


		public string GetReportHtml(string queryString, string refNum )
		{
			try
			{          
				string reportName = Utilities.GetQuerystringVariable(queryString, "rn");
                if(reportName== "Refinery Scorecard")
                {
                    return GetRefineryScorecardReport(refNum, queryString);
                }
                else if(reportName == "Refinery Trends Report")
                {
                    return GetRefineryTrendsReport(refNum, queryString);
                }
                else if (reportName == "Targeting")
                {
                    return GetTargetingReport(queryString, refNum);
                }
                string rows = Utilities.GetQuerystringVariable(queryString, "rows");
				string sn = Utilities.GetQuerystringVariable(queryString, "SN"); //scenario
				string peers = Utilities.GetQuerystringVariable(queryString, "peers");
				string yr = Utilities.GetQuerystringVariable(queryString, "yr");
				string currency = Utilities.GetQuerystringVariable(queryString, "currency");
				string uOM = Utilities.GetQuerystringVariable(queryString, "UOM");
				string ytd = Utilities.GetQuerystringVariable(queryString, "ytd");
				string avg = Utilities.GetQuerystringVariable(queryString, "avg");
				string startDate = Utilities.GetQuerystringVariable(queryString, "sd");
				string target = Utilities.GetQuerystringVariable(queryString, "target");
				string dataSet = Utilities.GetQuerystringVariable(queryString,"ds");
				return GetReport(reportName, rows, sn, peers, yr, currency, uOM, ytd, avg, startDate, target, dataSet, refNum);
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}

		}

		private string GetReport(string reportName, string rows, string sn, string peers,
			string yr, string currency, string uOM, string ytd, string avg, string startDate, string target, string dataSet,  string refNum)
		{
			try
			{
				string result = string.Empty;
				if (AreCalcsDone(dataSet, startDate, refNum))
				{
					DataSet ds = new DataSet();
					string rowsOrdinalsToInclude = rows;
					string scenario = "CLIENT";
					string tReportDataFilter = string.Empty;
					if (sn != string.Empty && sn.Length > 0)
					{
						scenario = sn != "BASE" ? sn : "CLIENT";
					}
					if (peers != string.Empty && peers.Length > 0)
					{
						string[] peersIds = peers.Split(_delim);
						foreach (string peer in peersIds)
						{
							_peerGroupsChosenForTargeting.Add(peer);
						}
					}
					if (reportName.Trim().ToUpper() != "TARGETING")
					{
						ds = GetDataSetForReport(refNum, reportName, Convert.ToDateTime(startDate).Year,
							Convert.ToDateTime(startDate).Month, dataSet, yr, sn, currency, uOM, rowsOrdinalsToInclude);
					}
					else
					{
                        ds = GetTargetingDataset(_peerGroupsChosenForTargeting[0]); //, refNum);
					}
					switch (reportName.ToUpper())
					{
						case "REFINERY TRENDS REPORT":
							bool bYTD = Convert.ToBoolean(ytd);
							bool bAVG = Convert.ToBoolean(avg);
							if ((bYTD && bAVG) || (!bYTD && !bAVG))
							{
								tReportDataFilter = "Current Month";
								ds.Tables["Chart_LU"].Columns["TotField"].ColumnName = "ValueField1";
							}
							else
							{
								if (bYTD)
								{
									tReportDataFilter = "Year To Date";
									ds.Tables["Chart_LU"].Columns["YTDField"].ColumnName = "ValueField1";
								}
								else
								{
									tReportDataFilter = "Rolling Average";
									ds.Tables["Chart_LU"].Columns["AVGField"].ColumnName = "ValueField1";
								}
							}
							break;
						case "EII AND VEI BREAKDOWN":

							break;
						default:
							break;
					}
					if (ds == null || ds.Tables.Count == 0)
						return "<HTML>No data</HTML>";

					GenrateInTableHtml.Page pg = new Page();
					System.Collections.Hashtable replacementValues = new System.Collections.Hashtable();
					replacementValues.Add("ReportPeriod", Convert.ToDateTime(startDate).ToString("MMMM yyyy") );
					replacementValues.Add("IfTargetOn", target);
					replacementValues.Add("IfYtdOn", ytd);
					replacementValues.Add("IfAvgOn", avg);
					replacementValues.Add("CoLoca", GetCoLoc(startDate, refNum,dataSet));
					if (reportName == "REFINERY TRENDS REPORT")
					{
						replacementValues.Add("ReportDataFilter", tReportDataFilter);
					}
					string htmlText = pg.createPage(ds, reportName.ToUpper(), uOM.Trim(), currency.Trim(), replacementValues);
					return htmlText;
				}
				else
				{
					throw new Exception("Error: Calculations are not finished!");
				}
				return result;
			}
			catch (Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Error in GetReport(): " + ex.Message));
				return ex.Message;
			}
		}

		private string GetCoLoc(string startDate, string refineryId, string dataSet )
		{
			try
			{
				string sql = "SELECT Distinct SubmissionID,Company,Location FROM Submissions WHERE  DataSet='" + dataSet + "' ";
				sql += " AND Month(PeriodStart)=" + Convert.ToDateTime(startDate).Month.ToString() + " AND Year(PeriodStart)=";
				sql += Convert.ToDateTime(startDate).Year.ToString() + " AND RefineryID='" + refineryId + "'";
				DataSet ds = ProfileDbManager.QueryDB(sql);
				string colocation = string.Empty;
				if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
				{
					colocation = ds.Tables[0].Rows[0]["Company"].ToString() + " - " + ds.Tables[0].Rows[0]["Location"].ToString();
				}
				return colocation;
			}
			catch(Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Error in GetCoLoc(): " + ex.Message));
				return string.Empty;
			}

		}

		private DataSet GetDataSetForReport(string refineryId, string reportName,
											int reportYear,
											int reportMonth,
											string dataSet,
											string factorSet,
											string scenario,
											string currency,
											string UOM,
											string rowsToInclude)
		{
			DataSet ds = new DataSet();
			try
			{
				string sql = "exec spGetReportProcs '" + refineryId + "','" + reportName + "'";
				DataTable dtStProc = ProfileDbManager.QueryDB(sql).Tables[0];
				string exec = string.Empty;
				string[] tableNames = new string[dtStProc.Rows.Count];
				for (int i = 0; i < dtStProc.Rows.Count; i++)
				{
					tableNames[i] = dtStProc.Rows[i]["DataTableName"].ToString();
                    exec +=  "exec " + dtStProc.Rows[i]["ProcName"] +
                        " '" + refineryId + "', " +
                        " '" + reportYear.ToString() + "', " +
                        " '" + reportMonth.ToString() + "', " +
                        " '" + dataSet + "', " +
                        " '" + factorSet + "', " +
                        " '" + scenario + "', " +
                        " '" + currency + "', " +
                        " '" + UOM + "'; ";
				}
				ds = ProfileDbManager.QueryDB(exec);
				for (int i = 0; i < ds.Tables.Count; i++)
                {
                    try
                    {
                        if (tableNames[i] == "Chart_LU" && rowsToInclude.Length > 0) //new
                        {
                            DataTable tablei = ds.Tables[i]; //need this because A property or indexer may not be passed as an out or ref parameter
                                                             //tablei is not new object, just a pointer to ds.Tables[i]
                            RemoveRows(ref tablei, tableNames[i], rowsToInclude);
                        }
                        ds.Tables[i].TableName = tableNames[i];
                    }
                    catch (Exception ex)
					{
						ProfileLogManager.LogException(new Exception("Error removing rows in GetDataSetForReport: " +  ex.Message));
					}
				}
				return ds;
			}
			catch (Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Error in GetDataSetForReport: " + ex.Message));
				return null;
			}
		}

		private void RemoveRows(ref DataTable dt, string friendlyTableName, string rowsToInclude)
		{
			try
			{
				IList<int> listOfRowsToInclude = GetListOfRowsToInclude(rowsToInclude);
				for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
				{
					bool found = false;
					foreach (int ordinal in listOfRowsToInclude)
					{
						if (Convert.ToInt16(dt.Rows[rowCount]["SortKey"]) == ordinal) 
						{
							found = true;
							break;
						}
					}
					if (!found)
						dt.Rows[rowCount].Delete();
				}
				dt.AcceptChanges();
			}
			catch (Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Error in RemoveRows: " + ex.Message));
			}
		}
		public IList<int> GetListOfRowsToInclude(string rowsToInclude)
		{
			List<int> list = new List<int>();
			try
			{

				if (rowsToInclude.Length > 0)
				{
					for (int i = 0; i < rowsToInclude.Length; i+=3)
					{
						string rowToInclude = rowsToInclude.Substring(i, 3);
						list.Add(Int32.Parse(rowToInclude));
					}
				}
			}
			catch (Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Error in GetListOfRowsToInclude: " + ex.Message));
			}
			return list;
		}

        private DataSet GetTargetingDataset(string peerGroup)
		{
			try
			{
                string sql = " select r.Description, r.SortKey, r.Indent, r.ParentId, r.AxisLabelUS, r.AxisLabelMetric, " +
                " r.DecPlaces, r.Active, r.IsHeader, q.DataCount, q.NumTiles, q.Top2, q.Tile1Break,  " +
                " q.Tile2Break, q.Tile3Break, q.Bottom2 " +
                " from ProfileFuels12.profile.ReportLayout_LU r " +
                " left outer join ProfileFuels12.profile.TargetingVariablesAvailable a on r.VKey=a.VKey " +
                " left outer join ResultsDB.dbo.VarDef v on r.VKey=v.VKey " +
                " left outer join ResultsDB.dbo.Quartiles q   on q.VKey=r.VKey and q.PGKey=" + peerGroup +
                " where (IsHeader>0 or DataCount Is Not Null)";

                DataSet ds = ProfileDbManager.QueryDB(sql);
				if (ds != null)
					ds.Tables[0].TableName = "Table1";
				return ds;
			}
			catch (Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Error in GetTargetingDataset: " + ex.Message));
				throw ex;
			}
		}

        private string GetTargetingReportHeading(string peerGroup) 
        {
            try
            {
                string result = string.Empty;
                string sql = "select LongText from ResultsDB.dbo.PeerGroupDef p where p.PGKey=" + peerGroup + ";";

                DataSet ds = ProfileDbManager.QueryDB(sql);
                if (ds != null && ds.Tables.Count==1 && ds.Tables[0].Rows.Count==1)
                    result = ds.Tables[0].Rows[0]["LongText"].ToString();
                return result;
            }
            catch (Exception ex)
            {
                ProfileLogManager.LogException(new Exception("Error in GetTargetingDataset: " + ex.Message));
                throw ex;
            }
        }

        private DataSet GetTargetingIndents() 
        {
            try
            {
                string sql = "select distinct Indent from ProfileFuels12.profile.ReportLayout_LU r order by Indent asc";
                return ProfileDbManager.QueryDB(sql);
            }
            catch (Exception ex)
            {
                ProfileLogManager.LogException(new Exception("Error in GetTargetingIndents: " + ex.Message));
                throw ex;
            }
        }

        private bool AreCalcsDone(string dataSet, string  startDate, string refineryId)
		{
			try
			{
				DateTime start = Convert.ToDateTime(startDate);
				string sql = "SELECT Distinct SubmissionID,CalcsNeeded FROM Submissions WHERE  DataSet='" + dataSet + "' ";
				sql += " AND Month(PeriodStart)=" + start.Month + " AND Year(PeriodStart)=";
				sql += start.Year + " AND RefineryID = '" + refineryId + "'";
				DataSet ds = ProfileDbManager.QueryDB(sql);
				object calcsNeeded = null;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    calcsNeeded = ds.Tables[0].Rows[0]["CalcsNeeded"];
                    return (calcsNeeded == DBNull.Value);
                }
                return (calcsNeeded == null); // DBNull.Value);
            }
			catch (Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Error in AreCalcsDone: " + ex.Message));
				throw ex;
			}



		}


        #region New Refinery Scorecard Report format


        private string WhereClause(string dataTable, int submissionId,  string scenario, string currency, string uom,  string factorSet = "")
		{
            string clause = string.Empty;
			try
			{
				switch (dataTable.ToUpper())
				{
                    case "MAINTAVAILCALC":
                        clause += " g " + //LEFT JOIN MaintAvailCalc c ON c.SubmissionID = g.SubmissionID AND c.FactorSet = g.FactorSet" +
                        " LEFT JOIN MaintIndex m ON m.SubmissionID = g.SubmissionID AND m.FactorSet = g.FactorSet " +
                        " where g.SubmissionID = " + submissionId + " and g.FactorSet = '" + factorSet + "';  ";
                        break;
                    case "MAINTINDEX":
                        clause += " g where g.SubmissionID = " + submissionId + " and g.Currency = '" + currency + "'";
                        if (factorSet.Length > 0)
                            clause += " and factorSet = '" + factorSet + "'";
                        clause += ";";
                        break;
                    default: // "GENSUM":
                        clause += " g where g.SubmissionID = " + submissionId + " and g.Scenario = '" + scenario + "' and g.Currency = '" + currency + "' and g.UOM = '" + uom + "'";
                        if (factorSet.Length > 0)
                            clause += " and factorSet = '" + factorSet + "'";
                        clause += ";";
                        break;
				}
			}
			catch (Exception ex)
			{
				Logger.ProfileLogManager.LogException(new Exception("Error in WhereClause for SubmissionId " + submissionId.ToString() + ": " + ex.Message));
				clause = " WhereClause ERROR;";
			}
			return clause;
		}


		private Single ValuesTableResult(string sql)
		{
			Single value = 0;
			DataSet ds = ProfileDbManager.QueryDB(sql);
			if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
			{
				try
				{
                    //Per Richard, change this from Double to Single
                    //value = Convert.ToDouble(ds.Tables[0].Rows[0][0]);
                    value = Convert.ToSingle(ds.Tables[0].Rows[0][0]);
                }
				catch(Exception ex)
				{
					Logger.ProfileLogManager.LogException(new Exception("Error in ValuesTableResult for sql " + sql + ": " + ex.Message));
					value = -9999;
				}    
			}
			return value;
		}

		private string PrepSql(string fieldName, string tableName, int submissionId,  string scenario, string currency, string uom, string factorSet)
		{
			string sql = "SELECT ";
			try
			{
                switch(tableName.ToUpper())
                {
                    case "GENSUM":
                        sql += fieldName + " FROM " + tableName + WhereClause(tableName, submissionId,  scenario, currency, uom,factorSet);
                        break;
                    case "MAINTAVAILCALC":
                        sql += fieldName + " FROM " + tableName + WhereClause(tableName, submissionId,  scenario, currency, uom, factorSet);
                        break;
                    case "MAINTINDEX":
                        sql += fieldName + " FROM  " + tableName + WhereClause(tableName, submissionId,  "", currency, "",factorSet);
                        break;
                    case "VARDEF":
                        sql += fieldName + " FROM ResultsDB.dbo.VarDef where VKey= " + submissionId; // WhereClause(tableName, submissionId, scenario, currency, uom);
                        break;

                }
			}
			catch(Exception ex)
			{
				Logger.ProfileLogManager.LogException(new Exception("Error in PrepSql for sql " + sql + ": " + ex.Message));
				return "ERROR;";
			}
			return sql;
		}

        private string DecPlaces(double value, int places )
        {
            if (value == -9999)
                return string.Empty;

            string format = "#,###.";
            for (int i = 0; i < places; i++)
                format += "0";
            return Math.Round(value, places).ToString(format);
        }
		public string GetRefineryScorecardReport(string refNum, string queryString)
        {
            string html = string.Empty;
            int submissionId = -9999;
            string startDate = Utilities.GetQuerystringVariable(queryString, "sd");
            string scenario = Utilities.GetQuerystringVariable(queryString, "SN");
            string currency = Utilities.GetQuerystringVariable(queryString, "currency");
            string uom = Utilities.GetQuerystringVariable(queryString, "UOM");
            string kpisString = Utilities.GetQuerystringVariable(queryString, "rows");
            string factorSet = Utilities.GetQuerystringVariable(queryString, "yr");
            if (kpisString.Length<1)
            {
                return GetRefineryScorecardReport_Orig(refNum, queryString);
            }
            string sortKeyList = string.Empty;
            int i = 0;
            while(i <= kpisString.Length-1)
            {
                sortKeyList += kpisString.Substring(i, 3) + ",";
                i += 3;
            }
            //remove trailing comma below, after check for if length > 0

            try
            {
                string sql = string.Empty;
                
                //get submission id
                sql = "SELECT SubmissionID from dbo.SubmissionsAll where PeriodStart ='" + startDate + "' and RefineryID = '" + refNum + "' and UseSubmission = 1;";
                DataSet dsSubmission = ProfileDbManager.QueryDB(sql);
                if(dsSubmission != null && dsSubmission.Tables.Count>0 && dsSubmission.Tables[0].Rows.Count==1)
                {
                    submissionId = Convert.ToInt32(dsSubmission.Tables[0].Rows[0][0]);
                }
                else
                {
                    Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryScorecardReport for RefNum " + refNum + ", StartDate " + startDate + ": Submissions found were either none or more than one"));
                    html = "<HTML>ERROR</HTML>";
                    return html;
                }

                if (sortKeyList.Length < 1)
                {
                    sql = "SELECT * FROM profile.ReportLayout_LU where active = 1  order by SortKey asc;";
                }
                else
                {
                    sortKeyList = sortKeyList.Remove(sortKeyList.Length - 1); //remove trailing comma
                    sql = "SELECT * FROM profile.ReportLayout_LU where (SortKey in(" + sortKeyList+ ") OR IsHeader=1)  and active = 1  order by SortKey asc;";
                }
                
                DataSet ds = ProfileDbManager.QueryDB(sql);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    double value1 = 0;
                    double targetValue = -9999;
                    double avgValue = -9999;
                    double ytdValue = -9999;
                    int decPlaces = -9999;
                    //move to bottom: html += RefineryScorecardHtmlStart(startDate);
                    bool addHeaders = false;
                    int lastHeaderIndent = 100;
                    for (int rowCount = ds.Tables[0].Rows.Count - 1; rowCount >=0; rowCount--)
                    {
                        //loop backwards thru rows tofind data, then can find headers that will be above it later in loop.
                        DataRow row = ds.Tables[0].Rows[rowCount];
                        int parentId = Convert.ToInt16(row["ParentId"]);
                        string desc = row["Description"].ToString();
                        int isHeader = Convert.ToInt32(row["IsHeader"]);
                        int indent = row["Indent"] == DBNull.Value ? 0 : Convert.ToInt16(row["Indent"]);

                        //Is this row a header row or a data row?
                        if (isHeader > 0)
                        {
                            //is header row
                            if(addHeaders)
                            {
                                //add this to html if smaller indent than last header written to html
                                if (indent < lastHeaderIndent)
                                {
                                    //add header to html
                                    html = "<tr><td colspan=5 height=25 valign=bottom><strong>" + GetIndent(desc, indent) + "</td></tr>" + html;
                                    lastHeaderIndent = indent;
                                    if(indent==0)
                                    {
                                        //at top level header
                                        addHeaders = false;
                                        lastHeaderIndent = 100;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //is data row
                            //add to html

                            //clear out old values
                            targetValue = -9999;
                            avgValue = -9999;
                            ytdValue = -9999;
                            decPlaces = -9999;

                            addHeaders = true;
                            decPlaces = row["DecPlaces"] == DBNull.Value ? 0 : Convert.ToInt16(row["DecPlaces"]);
                            try
                            {
                                string valuesSql = string.Empty;
                                if (row["ValueField1"] != DBNull.Value)
                                {
                                    valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    value1 = ValuesTableResult(valuesSql);
                                }
                                if (row["TargetField"] != DBNull.Value)
                                {
                                    valuesSql = PrepSql(row["TargetField"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    targetValue = ValuesTableResult(valuesSql);
                                }
                                if (row["AvgField"] != DBNull.Value)
                                {
                                    valuesSql = PrepSql(row["AvgField"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    avgValue = ValuesTableResult(valuesSql);
                                }
                                if (row["YTDField"] != DBNull.Value)
                                {
                                    valuesSql = PrepSql(row["YTDField"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    ytdValue = ValuesTableResult(valuesSql);
                                }
                                string uomLabelField = uom.Contains("US") ? "AxisLabelUS" : "AxisLabelMetric";
                                string uomLabel = row[uomLabelField] == DBNull.Value ? string.Empty : row[uomLabelField].ToString();
                                html = GetHtmlRow(desc, uomLabel, DecPlaces(value1, decPlaces), DecPlaces(targetValue, decPlaces), DecPlaces(avgValue, decPlaces), DecPlaces(ytdValue, decPlaces), indent) + html;
                            }
                            catch (Exception ex)
                            {
                                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryScorecardReport for SubmissionId " + submissionId.ToString() + ": " + ex.Message));
                                html = "<HTML>ERROR</HTML>";
                                break;
                            }

                        }
                        
                    }
                    html = RefineryScorecardHtmlStart(startDate) + html; //prepend to body since we started in the middle
                    html += "  </table></td></tr></table></form></body></HTML>";
                }
            }
            catch(Exception outerEx)
            {
                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryScorecardReport for SubmissionId " + submissionId.ToString() + ": " + outerEx.Message));
                html= "<HTML>ERROR</HTML>";
            }
			return html;
		}

        public string GetRefineryScorecardReport_Orig(string refNum, string queryString)
        {

            //THIS IS NOW ONLY USED FOR WHEN ALL ROWS ARE REQUESTED. NEED TO ADD THIS FUNCTIONALITY IN TO THE NON "_0rig" METHOD AND DELETE THIS METHOD

            string html = string.Empty;
            int submissionId = -9999;
            string startDate = Utilities.GetQuerystringVariable(queryString, "sd");
            string scenario = Utilities.GetQuerystringVariable(queryString, "SN");
            string currency = Utilities.GetQuerystringVariable(queryString, "currency");
            string uom = Utilities.GetQuerystringVariable(queryString, "UOM");
            string kpisString = Utilities.GetQuerystringVariable(queryString, "rows");
            string factorSet = Utilities.GetQuerystringVariable(queryString, "yr");
            string sortKeyList = string.Empty;
            int i = 0;
            while (i <= kpisString.Length - 1)
            {
                sortKeyList += kpisString.Substring(i, 3) + ",";
                i += 3;
            }
            //remove trailing comma below, after check for if length > 0

            try
            {
                string sql = string.Empty;

                //get submission id
                sql = "SELECT SubmissionID from dbo.SubmissionsAll where PeriodStart ='" + startDate + "' and RefineryID = '" + refNum + "' and UseSubmission = 1;";
                DataSet dsSubmission = ProfileDbManager.QueryDB(sql);
                if (dsSubmission != null && dsSubmission.Tables.Count > 0 && dsSubmission.Tables[0].Rows.Count == 1)
                {
                    submissionId = Convert.ToInt32(dsSubmission.Tables[0].Rows[0][0]);
                }
                else
                {
                    Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryScorecardReport for RefNum " + refNum + ", StartDate " + startDate + ": Submissions found were either none or more than one"));
                    html = "<HTML>ERROR</HTML>";
                    return html;
                }

                if (sortKeyList.Length < 1)
                {
                    sql = "SELECT * FROM profile.ReportLayout_LU where active = 1  order by SortKey asc;";
                }
                else
                {
                    sortKeyList = sortKeyList.Remove(sortKeyList.Length - 1); //remove trailing comma
                    sql = "SELECT * FROM profile.ReportLayout_LU where SortKey in(" + sortKeyList + ") and active = 1  order by SortKey asc;";
                }

                DataSet ds = ProfileDbManager.QueryDB(sql);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    double value1 = 0;
                    double targetValue = -9999;
                    double avgValue = -9999;
                    double ytdValue = -9999;
                    int decPlaces = -9999;
                    html += RefineryScorecardHtmlStart(startDate);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        int parentId = Convert.ToInt16(row["ParentId"]);
                        string desc = row["Description"].ToString();
                        int isHeader = Convert.ToInt32(row["IsHeader"]);
                        int indent = row["Indent"] == DBNull.Value ? 0 : Convert.ToInt16(row["Indent"]);
                        if (isHeader > 0)
                        {
                            html += "<tr><td colspan=5 height=25 valign=bottom><strong>" + GetIndent(desc, indent) + "</td></tr>";
                        }
                        else
                        {
                            //clear out old values
                            targetValue = -9999;
                            avgValue = -9999;
                            ytdValue = -9999;
                            decPlaces = -9999;

                            decPlaces = row["DecPlaces"] == DBNull.Value ? 0 : Convert.ToInt16(row["DecPlaces"]);
                            try
                            {
                                bool allNull = true;
                                string valuesSql = string.Empty;
                                if (row["ValueField1"] != DBNull.Value)
                                {
                                    allNull = false;
                                    valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    value1 = ValuesTableResult(valuesSql);
                                }
                                if (row["TargetField"] != DBNull.Value)
                                {
                                    allNull = false;
                                    valuesSql = PrepSql(row["TargetField"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    targetValue = ValuesTableResult(valuesSql);
                                }
                                if (row["AvgField"] != DBNull.Value)
                                {
                                    allNull = false;
                                    valuesSql = PrepSql(row["AvgField"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    avgValue = ValuesTableResult(valuesSql);
                                }
                                if (row["YTDField"] != DBNull.Value)
                                {
                                    allNull = false;
                                    valuesSql = PrepSql(row["YTDField"].ToString(), row["DataTable"].ToString(), submissionId, scenario, currency, uom, factorSet);
                                    ytdValue = ValuesTableResult(valuesSql);
                                }
                                if (!allNull)
                                {
                                    string uomLabelField = uom.Contains("US") ? "AxisLabelUS" : "AxisLabelMetric";
                                    string uomLabel = row[uomLabelField] == DBNull.Value ? string.Empty : row[uomLabelField].ToString();
                                    html += GetHtmlRow(desc, uomLabel, DecPlaces(value1, decPlaces), DecPlaces(targetValue, decPlaces), DecPlaces(avgValue, decPlaces), DecPlaces(ytdValue, decPlaces), indent);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryScorecardReport for SubmissionId " + submissionId.ToString() + ": " + ex.Message));
                                html = "<HTML>ERROR</HTML>";
                                break;
                            }
                        }
                    }
                    html += "  </table></td></tr></table></form></body></HTML>";
                }
            }
            catch (Exception outerEx)
            {
                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryScorecardReport for SubmissionId " + submissionId.ToString() + ": " + outerEx.Message));
                html = "<HTML>ERROR</HTML>";
            }
            return html;
        }

        private string RefineryScorecardHtmlStart(string startDate)
        {
            string startDateFormatted = Convert.ToDateTime(startDate).ToString("MMMM yyyy");
            string result = "<HTML><HEAD><title></title><style>.small { FONT-SIZE: 12px; FONT-FAMILY: tahoma }</style></HEAD><body><form id='Form1' method='post' runat=\"server\"><table border=0><tr><td></td></tr><tr><td>";
            result += "<table class=small border='0'>  <tr>    <td width='300'></td>    <td width='100'></td>    <td width='100'><td width=100></td>    <td width='100'></td>    <td width='100'></td>  </tr>";
            result += "  <tr>    <td></td>";
            result += "    <td align='right' valign='bottom'><strong>" + startDateFormatted  + "</strong></td>";
            result += "    <td align='right' valign='bottom'><strong><span class='Target'>Refinery<br/>Target&nbsp;</strong></span></td>";
            result += " <td align='right' valign='bottom'><strong><span class='Avg'>Rolling&nbsp;</br>Average</strong></span></td>";
            result += "    <td align='right' valign='bottom'><strong><span class='Ytd'>YTD&nbsp;&nbsp;&nbsp;&nbsp;</br>Average</strong></span></td>";
            result += "  </tr>";
            return result;
        }
        
        private string RefineryTrendsHtmlStart(string startDate)
        {
            DateTime start = Convert.ToDateTime(startDate);
            DateTime startMinus1 = start.AddMonths(-1);
            DateTime startMinus2 = start.AddMonths(-2);
            DateTime startMinus3 = start.AddMonths(-3);

            string startDateFormatted = start.ToString("MMMM yyyy");
            string startstartMinus1 = startMinus1.ToString("MMMM yyyy");
            string startstartMinus2 = startMinus2.ToString("MMMM yyyy");
            string startstartMinus3 = startMinus3.ToString("MMMM yyyy");
            
            string result = "<HTML><HEAD><title></title><style>.small { FONT-SIZE: 12px; FONT-FAMILY: tahoma }</style></HEAD><body><form id='Form1' method='post' runat=\"server\"><table border=0><tr><td></td></tr><tr><td>";
            result += "<table class=small border='0'>  <tr>    <td width='300'></td>    <td width='100'></td>    <td width='100'><td width=100></td>    <td width='100'></td>    <td width='100'></td>  </tr>";
            result += "  <tr>    <td></td>";
            result += "    <td align='right' valign='bottom'><strong>" + startDateFormatted + "</strong></td>";
            result += "    <td align='right' valign='bottom'><strong>" + startstartMinus1 + "&nbsp;</strong></span></td>";
            result += " <td align='right' valign='bottom'><strong>" + startstartMinus2 + "&nbsp;</strong></span></td>";
            result += "    <td align='right' valign='bottom'><strong><strong>" + startstartMinus3 + "&nbsp;</strong></span></td>";
            result += "  </tr>";
            return result;
        }
        private string TargetingHtmlStart(string startDate, string peerGroupId)
        {
            string startDateFormatted = Convert.ToDateTime(startDate).ToString("MMMM yyyy");
            string result = "<HTML><HEAD><title></title><style>.small { FONT-SIZE: 12px; FONT-FAMILY: tahoma }</style></HEAD><body><form id='Form1' method='post' runat=\"server\"><table border=0><tr><td></td></tr><tr><td>";
            result += "<table class=small border='0'> ";
            result += "<tr><td><strong>" + GetTargetingReportHeading(peerGroupId) + "</strong></td></tr>";
            result += "  <tr>    <td></td>";

            result += "<tr>    <td width='300'></td>    <td width='100'></td>    <td width='100'><td width=100></td>    <td width='100'></td>    <td width='100'></td>  </tr>";

            result += "  <tr>    <td></td>";
            result += "    <td align='right' valign='bottom'><strong><br/>Top 2</strong></td>";
            result += "    <td align='right' valign='bottom'><strong>Tile 1<br/> Break</strong></td>";
            result += "    <td align='right' valign='bottom'><strong>Tile 2 <br/>Break</strong></td>";
            result += "    <td align='right' valign='bottom'><strong>Tile 3 <br/>Break</strong></td>";
            result += "    <td align='right' valign='bottom'><strong><br/>Bottom 2</strong></td>";
            result += "  </tr>";
            return result;
        }
        public string GetRefineryTrendsReport(string refNum, string queryString)
        {
            //"where g.SubmissionID= 8597 and g.factorset = 2014 and g.Scenario='2014' and g.Currency  ='SGD' and g.UOM = 'MET'";
            string html = string.Empty;
            //int submissionId = -9999;
            string startDate = Utilities.GetQuerystringVariable(queryString, "sd");
            string scenario = Utilities.GetQuerystringVariable(queryString, "SN");
            string currency = Utilities.GetQuerystringVariable(queryString, "currency");
            string uom = Utilities.GetQuerystringVariable(queryString, "UOM");
            string kpisString = Utilities.GetQuerystringVariable(queryString, "rows");
            string factorSet = Utilities.GetQuerystringVariable(queryString, "yr");
            string sortKeyList = string.Empty;
            int submission1 = 0;
            int i = 0;
            while (i <= kpisString.Length - 1)
            {
                sortKeyList += kpisString.Substring(i, 3) + ",";
                i += 3;
            }
            //remove trailing comma below, after check for if length > 0

            if (kpisString.Length < 1)
            {
                return GetRefineryTrendsReport_Orig(refNum, queryString);
            }

            try
            {
                //get prior 3 submissions
                IList<string> submissions = new List<string>();
                string sql = "select TOP 4 SubmissionID from dbo.SubmissionsAll ";
                sql += " where REfineryId = '" + refNum + "' ";
                sql += " and PeriodStart <= '" + startDate + "' ";
                sql += " and UseSubmission=1 order by PeriodStart desc;";
                DataSet dsSubmissions = ProfileDbManager.QueryDB(sql);

                int submission2 = 0;
                int submission3 = 0;
                int submission4 = 0;
                if (dsSubmissions != null && dsSubmissions.Tables.Count > 0 && dsSubmissions.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        submission1 = Convert.ToInt32(dsSubmissions.Tables[0].Rows[0][0]);
                        submission2 = Convert.ToInt32(dsSubmissions.Tables[0].Rows[1][0]);
                        submission3 = Convert.ToInt32(dsSubmissions.Tables[0].Rows[2][0]);
                        submission4 = Convert.ToInt32(dsSubmissions.Tables[0].Rows[3][0]);
                    }
                    catch { }
                }
                else
                {
                    return "<HTML>NO DATA</HTML";
                }
                if (sortKeyList.Length < 2)
                {
                    sql = "SELECT * FROM profile.ReportLayout_LU where active = 1 order by SortKey asc;";
                }
                else
                {
                    sortKeyList = sortKeyList.Remove(sortKeyList.Length - 1); //remove trailing comma
                    sql = "SELECT * FROM profile.ReportLayout_LU where(SortKey in(" + sortKeyList + ") OR IsHeader=1)  and active = 1  order by SortKey asc;";
                }
                    
                    
                DataSet ds = ProfileDbManager.QueryDB(sql);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    double value1 = 0;
                    double value2 = -9999;
                    double value3 = -9999;
                    double value4 = -9999;
                    int decPlaces = -9999;
                    // move to below since we are starting in the middle: html += RefineryTrendsHtmlStart(startDate);
                    bool addHeaders = false;
                    int lastHeaderIndent = 100;
                    for (int rowCount = ds.Tables[0].Rows.Count-1; rowCount >=0; rowCount --)
                    {
                        //loop backwards thru rows tofind data, then can find headers that will be above it later in loop.
                        DataRow row = ds.Tables[0].Rows[rowCount];
                        int parentId = Convert.ToInt16(row["ParentId"]);
                        string desc = row["Description"].ToString();
                        int isHeader = Convert.ToInt32(row["IsHeader"]);
                        int indent = row["Indent"] == DBNull.Value ? 0 : Convert.ToInt16(row["Indent"]);

                        //Is this row a header row or a data row?
                        if (isHeader > 0)
                        {
                            //is header row
                            if (addHeaders)
                            {
                                //add this to html if smaller indent than last header written to html
                                if (indent < lastHeaderIndent)
                                {
                                    //add header to html
                                    html = "<tr><td colspan=5 height=25 valign=bottom><strong>" + GetIndent(desc, indent) + "</td></tr>" + html;
                                    lastHeaderIndent = indent;
                                    if (indent == 0)
                                    {
                                        //at top level header
                                        addHeaders = false;
                                        lastHeaderIndent = 100;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //is data row
                            //add to html
                            addHeaders = true;
                            decPlaces = row["DecPlaces"] == DBNull.Value ? 0 : Convert.ToInt16(row["DecPlaces"]);
                            try
                            {
                                string valuesSql = string.Empty;
                                if (row["ValueField1"] == DBNull.Value)
                                {
                                    value1 = 0;
                                }
                                else
                                {
                                    valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission1, scenario, currency, uom, factorSet);
                                    value1 = ValuesTableResult(valuesSql);
                                }
                                if (submission2 > 0)
                                {
                                    if (row["ValueField1"] == DBNull.Value)
                                    {
                                        value2 = 0;
                                    }
                                    else
                                    {
                                        valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission2, scenario, currency, uom, factorSet);
                                        value2 = ValuesTableResult(valuesSql);
                                    }

                                }
                                if (submission3 > 0)
                                {
                                    if (row["ValueField1"] == DBNull.Value)
                                    {
                                        value3 = 0;
                                    }
                                    else
                                    {
                                        valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission3, scenario, currency, uom, factorSet);
                                        value3 = ValuesTableResult(valuesSql);
                                    }

                                }
                                if (submission4 > 0)
                                {
                                    if (row["ValueField1"] == DBNull.Value)
                                    {
                                        value4 = 0;
                                    }
                                    else
                                    {
                                        valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission4, scenario, currency, uom, factorSet);
                                        value4 = ValuesTableResult(valuesSql);
                                    }

                                }
                                html = GetHtmlRow(desc, string.Empty, DecPlaces(value1, decPlaces), DecPlaces(value2, decPlaces), DecPlaces(value3, decPlaces), DecPlaces(value4, decPlaces), indent) + html;
                            }
                            catch (Exception ex)
                            {
                                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryTrendsReport for SubmissionId " + submission1.ToString() + ": " + ex.Message));
                                html = "<HTML>ERROR</HTML>";
                                break;
                            }
                        }
                    }
                    html = RefineryTrendsHtmlStart(startDate) + html;
                    html += "  </table></td></tr></table></form></body></HTML>";
                }
            }
            catch (Exception outerEx)
            {
                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryTrendsReport for SubmissionId " + submission1.ToString() + ": " + outerEx.Message));
                html = "<HTML>ERROR</HTML>";
            }
            return html;
        }

        public string GetRefineryTrendsReport_Orig(string refNum, string queryString)
        {

            //THIS IS NOW ONLY USED FOR WHEN ALL ROWS ARE REQUESTED. NEED TO ADD THIS FUNCTIONALITY IN TO THE NON "_0rig" METHOD AND DELETE THIS METHOD

            string html = string.Empty;
            string startDate = Utilities.GetQuerystringVariable(queryString, "sd");
            string scenario = Utilities.GetQuerystringVariable(queryString, "SN");
            string currency = Utilities.GetQuerystringVariable(queryString, "currency");
            string uom = Utilities.GetQuerystringVariable(queryString, "UOM");
            string kpisString = Utilities.GetQuerystringVariable(queryString, "rows");
            string factorSet = Utilities.GetQuerystringVariable(queryString, "yr");
            string sortKeyList = string.Empty;
            int submission1 = 0;
            int i = 0;
            while (i <= kpisString.Length - 1)
            {
                sortKeyList += kpisString.Substring(i, 3) + ",";
                i += 3;
            }
            //remove trailing comma below, after check for if length > 0

            try
            {
                //get prior 3 submissions
                IList<string> submissions = new List<string>();
                string sql = "select TOP 4 SubmissionID from dbo.SubmissionsAll ";
                sql += " where REfineryId = '" + refNum + "' ";
                sql += " and PeriodStart <= '" + startDate + "' ";
                sql += " and UseSubmission=1 order by PeriodStart desc;";
                DataSet dsSubmissions = ProfileDbManager.QueryDB(sql);

                int submission2 = 0;
                int submission3 = 0;
                int submission4 = 0;
                if (dsSubmissions != null && dsSubmissions.Tables.Count > 0 && dsSubmissions.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        submission1 = Convert.ToInt16(dsSubmissions.Tables[0].Rows[0][0]);
                        submission2 = Convert.ToInt16(dsSubmissions.Tables[0].Rows[1][0]);
                        submission3 = Convert.ToInt16(dsSubmissions.Tables[0].Rows[2][0]);
                        submission4 = Convert.ToInt16(dsSubmissions.Tables[0].Rows[3][0]);
                    }
                    catch { }
                }
                else
                {
                    return "<HTML>NO DATA</HTML";
                }
                if (sortKeyList.Length < 2)
                {
                    sql = "SELECT * FROM profile.ReportLayout_LU where active = 1 order by SortKey asc;";
                }
                else
                {
                    sortKeyList = sortKeyList.Remove(sortKeyList.Length - 1); //remove trailing comma
                    sql = "SELECT * FROM profile.ReportLayout_LU where SortKey in(" + sortKeyList + ")  and active = 1  order by SortKey asc;";
                }


                DataSet ds = ProfileDbManager.QueryDB(sql);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    double value1 = 0;
                    double value2 = -9999;
                    double value3 = -9999;
                    double value4 = -9999;
                    int decPlaces = -9999;
                    html += RefineryTrendsHtmlStart(startDate);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        int parentId = Convert.ToInt16(row["ParentId"]);
                        string desc = row["Description"].ToString();
                        int isHeader = Convert.ToInt32(row["IsHeader"]);
                        int indent = row["Indent"] == DBNull.Value ? 0 : Convert.ToInt16(row["Indent"]);
                        if (isHeader == 1) // parentId == 0 || row["ValueField1"] == DBNull.Value)
                        {
                            html += "<tr><td colspan=5 height=25 valign=bottom><strong>" + GetIndent(desc, indent) + "</td></tr>";
                        }
                        else
                        {
                            decPlaces = row["DecPlaces"] == DBNull.Value ? 0 : Convert.ToInt16(row["DecPlaces"]);
                            try
                            {
                                string valuesSql = string.Empty;
                                if (row["ValueField1"] == DBNull.Value)
                                {
                                    value1 = 0;
                                }
                                else
                                {
                                    valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission1, scenario, currency, uom, factorSet);
                                    value1 = ValuesTableResult(valuesSql);
                                }
                                if (submission2 > 0)
                                {
                                    if (row["ValueField1"] == DBNull.Value)
                                    {
                                        value2 = 0;
                                    }
                                    else
                                    {
                                        valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission2, scenario, currency, uom, factorSet);
                                        value2 = ValuesTableResult(valuesSql);
                                    }

                                }
                                if (submission3 > 0)
                                {
                                    if (row["ValueField1"] == DBNull.Value)
                                    {
                                        value3 = 0;
                                    }
                                    else
                                    {
                                        valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission3, scenario, currency, uom, factorSet);
                                        value3 = ValuesTableResult(valuesSql);
                                    }

                                }
                                if (submission4 > 0)
                                {
                                    if (row["ValueField1"] == DBNull.Value)
                                    {
                                        value4 = 0;
                                    }
                                    else
                                    {
                                        valuesSql = PrepSql(row["ValueField1"].ToString(), row["DataTable"].ToString(), submission4, scenario, currency, uom, factorSet);
                                        value4 = ValuesTableResult(valuesSql);
                                    }

                                }
                                html += GetHtmlRow(desc, string.Empty, DecPlaces(value1, decPlaces), DecPlaces(value2, decPlaces), DecPlaces(value3, decPlaces), DecPlaces(value4, decPlaces), indent);
                            }
                            catch (Exception ex)
                            {
                                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryTrendsReport for SubmissionId " + submission1.ToString() + ": " + ex.Message));
                                html = "<HTML>ERROR</HTML>";
                                break;
                            }
                        }
                    }
                    html += "  </table></td></tr></table></form></body></HTML>";
                }
            }
            catch (Exception outerEx)
            {
                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryTrendsReport for SubmissionId " + submission1.ToString() + ": " + outerEx.Message));
                html = "<HTML>ERROR</HTML>";
            }
            return html;
        }

        public string GetTargetingReport(string queryString,string refNum ) // startDate,  string scenario, string currency, string uom, List<string> kpiSortKeys)
        {
            //string qs = "rn=Targeting&sd=6/1/2016&ds=ACTUAL&UOM=US&currency=SGD&yr=2014&target=True&avg=True&ytd=True&SN=2014&TS=7/6/2017 2:29:49 PM&peers=6806";
            string html = string.Empty;
            string startDate = Utilities.GetQuerystringVariable(queryString, "sd");
            string peerGroupId = Utilities.GetQuerystringVariable(queryString, "peers");
            string sortKeyList = string.Empty;
            try
            {

                DataSet indentsDs = GetTargetingIndents();
                Dictionary<int, string> indents = new Dictionary<int, string>();
                int indentKeyToRemove = -1;
                if(indentsDs != null && indentsDs.Tables.Count==1 && indentsDs.Tables[0].Rows.Count>0)
                {
                    for(int i=0;i< indentsDs.Tables[0].Rows.Count;i++)
                    {
                        indentKeyToRemove = Convert.ToInt32(indentsDs.Tables[0].Rows[i]["Indent"]);
                        indents.Add(Convert.ToInt32(indentsDs.Tables[0].Rows[i]["Indent"]),string.Empty);
                    }
                    indents.Remove(indentKeyToRemove); //remove last one because it will be data, handled differently than headers
                }
                DataSet ds = GetTargetingDataset(peerGroupId);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    double top2 = 0;
                    double tile1 = 0;
                    double tile2 = 0;
                    double tile3 = 0;
                    double bottom2 = 0;
                    int decPlaces = 0;
                    html += TargetingHtmlStart(startDate, peerGroupId);
                    //GetTargetingReportHeading
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        int parentId = Convert.ToInt16(row["ParentId"]);
                        string desc = row["Description"].ToString();
                        int indent = row["Indent"] == DBNull.Value ? 0 : Convert.ToInt32(row["Indent"]);
                        int isHeader = Convert.ToInt32(row["IsHeader"]);
                        if (isHeader>0)
                        {
                            foreach(int i in indents.Keys)
                            {
                                if(i== indent)
                                {
                                    indents[i]= "<tr><td colspan=5 height=25 valign=bottom><strong>" + GetIndent(desc, indent) + "</td></tr>";
                                    break;
                                }
                            }                            
                        }
                        else
                        {
                            decPlaces = row["DecPlaces"] == DBNull.Value ? 0 : Convert.ToInt16(row["DecPlaces"]);
                            bool allNulls = true;
                            try
                            {                               
                                string valuesSql = string.Empty;
                                if (row["Top2"] != DBNull.Value)
                                {
                                    allNulls = false;
                                    top2 = Convert.ToDouble(row["Top2"]);
                                }
                                if (row["Tile1Break"] != DBNull.Value)
                                {
                                    allNulls = false;
                                    tile1 = Convert.ToDouble(row["Tile1Break"]);
                                }
                                if (row["Tile2Break"] != DBNull.Value)
                                {
                                    allNulls = false;
                                    tile2 = Convert.ToDouble(row["Tile2Break"]);
                                }
                                if (row["Tile3Break"] != DBNull.Value)
                                {
                                    allNulls = false;
                                    tile3 = Convert.ToDouble(row["Tile3Break"]);
                                }
                                if (row["Bottom2"] != DBNull.Value)
                                {
                                    allNulls = false;
                                    bottom2 = Convert.ToDouble(row["Bottom2"]);
                                }
                                string uomLabel = string.Empty;
                                //since we have data in this section, add the header for this section (saved in headerHtml var).
                                if (!allNulls)
                                {
                                    if (indentsDs != null && indentsDs.Tables.Count == 1 && indentsDs.Tables[0].Rows.Count > 0)
                                    {
                                        for (int i = 0; i < indentsDs.Tables[0].Rows.Count-1; i++)
                                        {
                                            int key = Convert.ToInt32(indentsDs.Tables[0].Rows[i]["Indent"]);
                                            html += indents[key];
                                            indents[key] = string.Empty;//clear headerHtml so we only print the header once.
                                        }
                                    }
                                    //now add the row(s) underneath the header.
                                    html += GetHtmlQuartileRow(desc, uomLabel, indent, DecPlaces(top2, decPlaces), DecPlaces(tile1, decPlaces), DecPlaces(tile2, decPlaces), DecPlaces(tile3, decPlaces), DecPlaces(bottom2, decPlaces));
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.ProfileLogManager.LogException(new Exception("Error in GetRefineryScorecardReport for RefNum " + refNum.ToString() + ": " + ex.Message));
                                html = "<HTML>ERROR</HTML>";
                                break;
                            }
                        }
                    }
                    html += "  </table></td></tr></table></form></body></HTML>";
                }
            }
            catch (Exception outerEx)
            {
                Logger.ProfileLogManager.LogException(new Exception("Error occurred in GetRefineryScorecardReport for RefNum " + refNum.ToString() + ": " + outerEx.Message));
                html = "<HTML>ERROR</HTML>";
            }
            return html;
        }

        private string GetHtmlQuartileRow(string fieldName, string uomLabel, int indent, string top2, string tile1, string tile2, string tile3, string bottom2)
        {
            string row = "<tr><td valign=bottom>";
            switch (fieldName)
            {
                case "":
                    row += GetIndent(fieldName, indent);
                    break;
                default:
                    row += GetIndent(fieldName, indent);
                    break;

            }

            if (uomLabel.Length > 0)
            {
                row += "<span align=left>, " + uomLabel + "</span></td>";
            }
            else
            {
                row += "<span align=left></span></td>";
            }
            row += "<td align=right valign=bottom>" + top2 + "</td>";
            row += "<td align=right valign=bottom>" + tile1 + "</td>";
            row += "<td align=right valign=bottom>" + tile2 + "</td>";
            row += "<td align=right valign=bottom>" + tile3 + "</td>";
            row += "<td align=right valign=bottom>" + bottom2 + "</td>";

            return row + "</tr>";
        }

        private string GetHtmlRow(string fieldName, string uomLabel, string value, string target, string avg, string ytd, int indent)
		{
			string row = "<tr><td valign=bottom>";
			switch (fieldName)
			{
				case "":
					row += GetIndent(fieldName, indent);
					break;
				default:
					row += GetIndent(fieldName, indent);
					break;

			}

            if(uomLabel.Length > 0)
            {
                row += "<span align=left>, " + uomLabel + "</span></td>";
            }
            else
            {
                row += "<span align=left></span></td>";
            }
            row += "<td align=right valign=bottom>" + value + "</td>";
			row += "<td align=right><span class='Target'>" + target + "</span></td>";
			row += "<td align=right valign=bottom><span class='Avg'>" + avg + "</span></td>";
			row += "<td align=right valign=bottom><span class='Ytd'>" + ytd + "</span></td>";

			return row + "</tr>";
		}

		private string GetIndent(string text, int indent)
		{
			string result = string.Empty;
			for (int i = 0; i < indent; i++)
			{
				result += "&nbsp;";
			}
			return result + text;
		}

        #endregion
        //public ~ProfileReportsManager()

    }

}
