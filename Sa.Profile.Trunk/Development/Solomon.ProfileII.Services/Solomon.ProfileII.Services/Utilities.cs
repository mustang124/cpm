﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using Solomon.ProfileII.Logger;
using System.Security.Cryptography.X509Certificates;
using ProfileLiteSecurity.Client;
using System.Data.SqlClient;




namespace Solomon.ProfileII.Services
{
    public static class Utilities
    {
        public static string GetQuerystringVariable(string queryString, string key)
        {
            try
            {
                string[] qsParts = queryString.Split('&');
                for (int i = 0; i < qsParts.Length; i++)
                {
                    string[] parts = qsParts[i].Split('=');
                    if (parts[0].ToUpper() == key.ToUpper())
                        return parts[1];
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
            
    public static bool ValidateKey(string clientKey, ref string refNum)
    {
        string company,  location, path = string.Empty;
        int locIndex = 0;
        try
        {
            if (clientKey != null)
            {
                char[] delim = { '$' };
                company = Cryptographer.Decrypt(clientKey).Split(delim)[0]; // (0);
                locIndex = Cryptographer.Decrypt(clientKey).Split(delim).Length - 2;
                location = Cryptographer.Decrypt(clientKey).Split(delim)[locIndex];
                string[] array = Cryptographer.Decrypt(clientKey).Split(delim);
                refNum = array[array.Length - 1]; // 'last position
                string clientKeysFolder = string.Empty;
                try
                {
                    clientKeysFolder = ConfigurationManager.AppSettings["ClientKeysFolder"].ToString();
                }
                catch { }

                if(clientKeysFolder.Length>0)
                {
                    if (System.IO.File.Exists(clientKeysFolder + company + "_" + location + ".key"))
                    {
                        path = clientKeysFolder + company + "_" + location + ".key";
                    }
                }
                else
                {
                    if (System.IO.File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key"))
                    {
                        path = "C:\\ClientKeys\\" + company + "_" + location + ".key";
                    } else if (File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key"))
                    {
                        path = "D:\\ClientKeys\\" + company + "_" + location + ".key";
                     } else
                    {
                        return false;
                    }
                }

                if (path.Trim().Length == 0)
                {
                    return false;
                }

                string readKey = string.Empty;
                using (StreamReader reader = new System.IO.StreamReader(path))
                {
                    readKey = reader.ReadLine();
                }
                if(readKey.Length>0)
                {
                    return (readKey.Trim() == clientKey.Trim());
                }
                return false;
            }
            else
            {
                return false;
            }
     
        }
        catch(Exception ex)
        {
                ProfileLogManager.LogException(new Exception("Error in ValidateKey for RefNum '" + refNum + "', clientKey '" + clientKey + "': " + ex.Message));
            throw ex;
        }

    }

    public static bool AuthenticateByCertificateAndClientId(ref  string errorResponse , byte[] encodedSignedCms ,string clientKey , bool displaySysException )
    {
            try
            {
                ProfileLogManager.LogInfo("Entering AuthenticateByCertificateAndClientId(), with client key " + clientKey);
                X509Certificate2 publicCert = new X509Certificate2();
                ProfileLiteSecurity.Cryptography.Certificate pcc = new ProfileLiteSecurity.Cryptography.Certificate();
                ProfileLiteSecurity.Client.ClientKey profileLiteSecurity = new ProfileLiteSecurity.Client.ClientKey();
                ProfileLiteSecurity.Client.ClientKey profileLiteSecurityClientKey = profileLiteSecurity.GetClientKeyInfoFromToken(clientKey);
                string errors = string.Empty;
                string name = profileLiteSecurityClientKey.Company;
                string refineryId = profileLiteSecurityClientKey.RefNum;
                bool activeAuthenticationAndAuthorizationRecord = false;
                bool certificateRequired = true;
                bool clientKeyRequired = true;
                string clientKeyFromDb = string.Empty;
                ProfileLogManager.LogInfo("Calling userAuthentication.CheckAuthenticationAndAuthorizationTable");
                if (!Utilities.CheckAuthenticationAndAuthorizationTable(refineryId, ref activeAuthenticationAndAuthorizationRecord, ref certificateRequired, ref clientKeyRequired, ref clientKeyFromDb))
                {
                    ProfileLogManager.LogInfo("Error or no record found in AuthenticateByCertificateAndClientId() for RefineryId " + refineryId);
                    throw new Exception("Your account is not active. Please contact Solomon.");
                }

                if (!activeAuthenticationAndAuthorizationRecord)
                {
                    ProfileLogManager.LogInfo("activeAuthenticationAndAuthorizationRecord is false for RefineryId " + refineryId);
                    throw new Exception("Your account is not active. Please contact Solomon.");
                }
                if (clientKeyRequired)
                {
                    if (clientKey.Trim() != clientKeyFromDb.Trim())
                    {
                        ProfileLogManager.LogInfo("Client Key mismatch in AuthenticateByCertificateAndClientId() for RefineryId " + refineryId);
                        throw new Exception("Your key is invalid. Please contact Solomon to get a valid key.");
                    }
                }
                if (certificateRequired)
                {
                    ProfileLogManager.LogInfo("Attempting to check certificate for company " + name + ", refnum " + refineryId);
                    string nameAndRefNum = profileLiteSecurityClientKey.Company + " (" + profileLiteSecurityClientKey.RefNum + ")";
                    if (!pcc.Get(nameAndRefNum, out publicCert))
                    {
                        ProfileLogManager.LogInfo("Call to pcc.Get in AuthenticateByCertificateAndClientId, using name " + nameAndRefNum + " returned False.");
                        throw new Exception("There was a problem with your Certificate. Please contact Solomon.");
                    }
                    byte[] originalMessage = new byte[] { };
                    ProfileLiteSecurity.Cryptography.NonRepudiation pnc = new ProfileLiteSecurity.Cryptography.NonRepudiation();
                    return pnc.Verify(ref errorResponse, encodedSignedCms, publicCert, true, out originalMessage, false);

                }
                return true;
            }
            catch(Exception ex)
            {
                LogError("AuthenticateByCertificateAndClientId", ex.Message, (ex.InnerException == null ? "" : ex.InnerException.Message));
                return false;
            }
    }
    
        public static bool CheckAuthenticationAndAuthorizationTable(string refineryId ,
                                                             ref bool active ,
                                                             ref bool certificateRequired ,
                                                             ref bool clientKeyRequired,
                                                             ref string clientKeyFromDb )
        {
            try
            {
                string sql = "SELECT RefineryGroupId,RefineryId,Active,CertificateRequired,ClientKeyRequired,ClientKey" +
                " FROM dbo.AuthenticationAndAuthorization where RefineryId =@RefineryId";
                using (SqlConnection conx = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString()))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, conx))
                    {
                        try
                        {
                            cmd.Parameters.Add(new SqlParameter("@RefineryID", refineryId));
                            cmd.CommandType = System.Data.CommandType.Text;
                            conx.Open();
                            
                            SqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.Read(); //expect only 1 line
                                active = (bool)reader["Active"];
                                certificateRequired = (bool)reader["CertificateRequired"];
                                clientKeyRequired = (bool)reader["ClientKeyRequired"];
                                clientKeyFromDb = reader.GetString(5);
                                return true;
                            }
                            else
                            {
                                ProfileLogManager.LogInfo("No record for RefineryId " + refineryId + " found in dbo.AuthenticationAndAuthorization()");
                                return false;
                            }                            
                        }
                        catch(Exception ex)
                        {
                            LogError("CheckAuthenticationAndAuthorizationTable", ex.Message, "reading record for RefineryId");
                            return false;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                LogError("CheckAuthenticationAndAuthorizationTable", ex.Message, "unexpected error");
            }
            return false;
        }

        

        private static bool TranslateSqlServerBitTypeFromNumbersToBoolean(byte value )
        {
            switch(value)
            {
                case 0:
                    return false;
                    break;
                case 255:
                    return true;
                    break;
                default:
                    throw new Exception("Sql Server Bit datatype has value other than 0 or 255!");
            }
        }

        private static void LogError(string methodName, string errorMessage, string otherInfo = "")
        {
            try
            {
                string msg = "Error in " + methodName + ", ";
                if (otherInfo.Length > 0)
                {
                    msg += otherInfo + ", ";
                }
                msg += errorMessage;
                ProfileLogManager.LogException(new Exception(msg));
            }
            catch { }

        }

 }


    
}
