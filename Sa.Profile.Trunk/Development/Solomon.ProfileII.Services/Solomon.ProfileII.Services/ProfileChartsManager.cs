﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.ProfileII.Contracts;
using System.ServiceModel;
//using Solomon.ProfileII.ChartService;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;
using Solomon.ProfileII.Logger;


namespace Solomon.ProfileII.Services
{
    [ServiceContract]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
    ConcurrencyMode = ConcurrencyMode.Multiple,
    IncludeExceptionDetailInFaults = false)]
    public class ProfileChartsManager : IProfileChartService
    {


        public ProfileChartsManager(string refineryId)
        {
            //implement logger here
        }

        [OperationContract]
        public string GetCommonChartHtml(string queryString)
        {
            throw new NotImplementedException();
            /*
            string conx = ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString();
            ChartService.ChartFactory factory = new ChartFactory(conx, _refineryId);
            //http://localhost:2153/ChartPage.aspx?cn=Average+Energy+Cost&field1=ISNULL(EnergyCost%2c0)+AS+%27Singapore%27&field2=&field3=&field4=&field5=&sd=6/1/2015&ed=6/1/2016&UOM=US&currency=USD&sn=2014&YTD=EnergyCost_YTD&TwelveMonthAvg=EnergyCost_AVG&target=EnergyCost_Target&tableName=GenSum&chart=Column|Red|Green|Yellow|Blue&WsP=X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=

            //&field1=ISNULL(EnergyCost%2c0)+AS+%27Singapore%27&field2=&field3=&field4=&field5=&
            List<string> fields = null;
            for (int i = 0; i < 5; i++)
            {
                string field = Utilities.GetQuerystringVariable(queryString, "field" + i.ToString());
                if (field.Length>0)
                {
                    if (field == null)
                        fields = new List<string>();
                    fields.Add(field);
                }
            }
            
            string periodStart = Utilities.GetQuerystringVariable(queryString, "sd");
            string periodEnd = Utilities.GetQuerystringVariable(queryString, "ed");
            string chartTitle = Utilities.GetQuerystringVariable(queryString, "cn");
            string currency = Utilities.GetQuerystringVariable(queryString, "currency");
            string factorSet = Utilities.GetQuerystringVariable(queryString, "");
            string uom = Utilities.GetQuerystringVariable(queryString, "UOM");
            string scenario = Utilities.GetQuerystringVariable(queryString, "sn");

            //"&chart=" + ChartType + "|" + ChartColor + "|" + AvgColor + "|" + YtdColor + "|" + TargetColor
            //&chart=Column|Red|Green|Yellow|Blue&
            string chartInfo = Utilities.GetQuerystringVariable(queryString, "chart");
            string[] chartInfoParts = chartInfo.Split('|');
            string chartType = Utilities.GetQuerystringVariable(queryString, chartInfoParts[0]);
            string chartColor = Utilities.GetQuerystringVariable(queryString, chartInfoParts[1]);
            string avgColor = Utilities.GetQuerystringVariable(queryString, chartInfoParts[2]);
            string ytdColor = Utilities.GetQuerystringVariable(queryString, chartInfoParts[3]);
            string targetColor = Utilities.GetQuerystringVariable(queryString, chartInfoParts[4]);

            string ytdField = Utilities.GetQuerystringVariable(queryString, "YTD");
            string targetField = Utilities.GetQuerystringVariable(queryString, "target");
            string aveField = Utilities.GetQuerystringVariable(queryString, "TwelveMonthAvg");
            string tableName = Utilities.GetQuerystringVariable(queryString, "tableName");

            

            Chart chart =  factory.GetCommonChartWithTheseFields(fields, _refineryId, periodEnd, chartTitle, currency,
                factorSet, ref uom, scenario, chartType, chartColor, avgColor, ytdColor, targetColor,
                ytdField, targetField, aveField, tableName);
            */
            

            //PROBLEM: ChartPage.aspx sends .png of the chart to browser control. Can wcf return this???


            //it seems the preferred route is to have wcf return the dataset to the client, andh have the client biuld the chart from it.

            //I see that chart.SaveXml is an option...

            /*
             * reuse existing ChartPage.aspx
             * Call it from WCF
             * Get its response
             * convert response to string  https://www.hiqpdf.com/documentation/html/ec631fb7-c426-4b9a-b84d-5b6a0b97823b.htm
             * convert the string to atream so wcf can return it   http://stackoverflow.com/questions/13753815/return-html-format-on-wcf-service-instead-of-json-or-xml
            */




        }

        [OperationContract]
        public string GetRadarChartHtml(string queryString)
        {
            throw new NotImplementedException();
        }

        [OperationContract]
        public string GetUnitsCommonChartHtml(string queryString)
        {
            throw new NotImplementedException();
        }

        [OperationContract]
        public IEnumerable<ChartData> GetUnits(string queryString)
        {
            throw new NotImplementedException();
        }
    }

}

