﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Solomon.ProfileII.Logger;


namespace Solomon.ProfileII.Services
{
    public static class ProfileDbManager
    {
        static ProfileDbManager()
        {
        }
        public static DataSet QueryDB(string sql)
        {
            DataSet ds =new DataSet();
            try
            {
                using (SqlConnection conx = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString()))
                {
                    if (sql.Trim().ToUpper().StartsWith("SELECT") || sql.Trim().ToUpper().StartsWith("EXEC"))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(sql, conx);
                        da.Fill(ds);
                    }
                }
                return ds;
            }
            catch (Exception ex)
            {
                ProfileLogManager.LogException(new Exception("Error in QueryDB: " +  ex.Message));
                return null;
            }

        }
        internal static DataSet InsertUpdateDelete(string sql)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conx = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString()))
                {
                        SqlDataAdapter da = new SqlDataAdapter(sql, conx);
                        da.Fill(ds);
                    
                }
                return ds;
            }
            catch (Exception ex)
            {
                //ProfileLogManager.LogException(new Exception("Error in QueryDB: " + ex.Message));
                return null;
            }

        }

    }

}
