﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Solomon.ProfileII.Contracts;
using Solomon.ProfileII.Services;
using System.Configuration;
using System.Web;
using System.Reflection;
using System.IO;

namespace Solomon.ProfileII.Services.Tests
{
   
    [TestClass]
    public class DataTest
    {
        //private string _db10Conx = string.Empty;
        private string _db12Conx = string.Empty;
        private string _filesDownloadUploadTestFolderPath = @"C:\tfs\CPA\Sa.Profile.Trunk\Development\Solomon.ProfileII.Services\Solomon.ProfileII.Services.Tests\bin\Debug\7.0.0";
        private string _clientKey = "X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=";
        private string _userHostAddress = "TEST USER HOST ADDRESS";
        
        [TestInitialize]
        public void startup()
        {
            //_db10Conx = ConfigurationManager.ConnectionStrings["Profile10Conx"].ToString();
            _db12Conx = ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString();
        }


        [TestMethod]
        public void DataCheckSvcTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            Assert.IsTrue(svc.CheckService().Contains("Success"));
        }
        [TestMethod]
        public void DataRefineryIs2012Test()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            Assert.IsTrue(svc.RefineryIs2012("XXPAC"));
        }

        [TestMethod]
        public void DataGetLookupsTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetLookups("XXPAC", "123");
            Assert.IsTrue(ds.Tables[0].Rows.Count > 0);
        }

        [TestMethod]
        public void DataGetDataDumpTest()
        {
            DateTime startDate = new DateTime(2016, 6, 1);
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetDataDump("EII", "Actual", "2012", "USD", startDate, "US", 2016, true, true, true, "XXPAC");
            Assert.IsTrue(ds.Tables.Count > 0);
        }

        [TestMethod]
        public void DataGetReferencesTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetReferences("XXPAC");
            Assert.IsTrue(ds.Tables[0].Rows.Count > 0);
        }


        [TestMethod]
        public void FileCheckServiceTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            Assert.IsTrue(svc.CheckService().Contains("Success"));
        }

        [TestMethod]
        public void FileDownloadTplFileTest()
        {
            Assert.Inconclusive("First, create folder defined in AppDomain.CurrentDomain.BaseDirectory for Solomon.ProfileII.Services.ProfileDataManager project. Then update _filesDownloadUploadTestFolderPath above");
            //this will mimic the web server's folder. Since this unit test project will be making the call, it will be "C:\ . . .\Solomon.ProfileII.Services.Tests\bin\Debug"
            string folderOnClientMachine = @"C:\tfs\CPA\Sa.Profile.Trunk\Development\Solomon.ProfileII.Services\Solomon.ProfileII.Services.Tests\bin\Debug\Test\7.0.0";
            string fileName = "Table1.tpl";
            string replaceFilePath = folderOnClientMachine + "\\" + fileName;
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            if (!System.IO.Directory.Exists(folderOnClientMachine))
            {
                System.IO.Directory.CreateDirectory(folderOnClientMachine);
            }
            if (!System.IO.File.Exists(replaceFilePath))
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(replaceFilePath))
                {
                    writer.WriteLine("TEST");
                }
            }
            byte[] result = svc.DownloadTplFile(fileName, "7.1.0.0", "X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");
            Assert.IsTrue(result.Length > 8);
            FileStream fileStream = new FileStream(replaceFilePath + ".temp", FileMode.Create);
            fileStream.Write(result, 0, result.Length);
            fileStream.Close();
            File.Delete(replaceFilePath);
            File.Move(replaceFilePath + ".temp", replaceFilePath);

            //cleanup
            System.IO.Directory.Delete(folderOnClientMachine, true);
        }


        [TestMethod]
        public void VersionMatcherTest()
        {            
            Solomon.ProfileII.Data.FileTransfer trfrMgr = new Data.FileTransfer(null);
            PrivateObject po = new PrivateObject(trfrMgr);            
            List<string> versions = new List<string>();
            versions.Add("5.15.0.7");
            versions.Add("0.0.0.0");
            versions.Add("10.0.0.0");
            versions.Add("999.999.999.999");
            versions.Add("8.789.001.0");
            object[] args = new object[] { "6.0.0.2", versions };
            Assert.IsTrue("5.15.0" == po.Invoke("VersionMatcher", args).ToString());
            versions.Add("6.0.0.2");
            Assert.IsTrue("6.0.0" == po.Invoke("VersionMatcher", args).ToString());
        }


        

         [TestMethod]
        public void FileUploadFileTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            byte[] bytes = new byte[] { 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45 };
            Assert.IsTrue(svc.UploadFile("Table1.tpl",bytes , _clientKey).Length<1);
        }

        
        [TestMethod]
        public void SubmitGetDataByPeriodTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DateTime start = new DateTime(2016, 6, 1);
            DateTime finish = new DateTime(2016, 7, 1);
            DataSet ds = svc.GetDataByPeriod(start,finish,"XXPAC");
            Assert.IsTrue(ds != null);
            Assert.IsTrue(ds.Tables.Count ==25);
            Assert.IsTrue(ds.Tables[0].TableName == "Settings");
            Assert.IsTrue(ds.Tables[24].TableName == "OpexAll");
        }

        [TestMethod]
        public void SubmitGetInputDataTest()
        {
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager( _db12Conx, _userHostAddress);
            DataSet ds = svc.GetInputData ("XXPAC");
            Assert.IsTrue(ds != null);
            Assert.IsTrue(ds.Tables.Count ==23);
            Assert.IsTrue(ds.Tables[0].TableName == "Settings");
            Assert.IsTrue(ds.Tables[22].TableName == "UserDefined");
        }

        [TestMethod]
        public void SubmitSubmitDataTest()
        {
            //DataSet ds =
            /*
            LoadRecords(ds As DataSet, id As Integer, currency As String,
                                  refineryID As String, UserHostAddress As String, ThisMonth As String, ThisYear As String) As Boolean
            */
            Solomon.ProfileII.Data.SubmitServices submitMgr = new Data.SubmitServices(_db12Conx);
            /*
            PrivateObject po = new PrivateObject(submitMgr);
            List<string> versions = new List<string>();
            

            object[] args = new object[] { "6.0.0.2", versions };
            Assert.IsTrue("5.15.0" == po.Invoke("VersionMatcher", args).ToString());
            */
            string pathToDataset = @"C:\tfs\CPA\Sa.Profile.Trunk\Development\Solomon.ProfileII.Services\Solomon.ProfileII.Services.Tests\16081ancap 2019-08.xml";
            DataSet ds = new DataSet();
            ds.ReadXml(pathToDataset);
            string refineryid = "192NSA";
            int result = submitMgr.SubmitRefineryData(ds, refineryid, "TEST");
            Assert.IsTrue(result == 0);
        }
    }
}
