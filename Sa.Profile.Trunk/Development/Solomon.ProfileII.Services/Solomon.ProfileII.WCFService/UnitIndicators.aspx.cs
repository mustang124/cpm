﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;

namespace Solomon.ProfileII.WCFService
{
    public partial class UnitIndicators : System.Web.UI.Page
    {
        protected DataSet chrtData = null;
        //protected Literal ltMessage; //WIthEVents in vb
        private string _totField = string.Empty;

        private string _refineryID = string.Empty;
        private string _clientKey = string.Empty;
        //private string _trace = string.Empty;

        public string RefineryID
        {
            get { return _refineryID; }
            set { _refineryID = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Session["UnitIndicatorsQueryString"] = Request.QueryString.ToString();

            var hdrs = Request.Headers;
            foreach (var hdr in hdrs)
            {
                string hdrText = hdr.ToString();
                if (hdrText == "WsP")
                {
                    
                    _clientKey = Request.Headers["WsP"];
                }
            }

            if(_clientKey.Length<1)
            {
                //'Get refinery id from key
                if (!IsPostBack)
                {
                    
         
                    Session["RefID"] = RefineryID;
                    Session["ClientKey"] = _clientKey; //store here for when we redirect to ChartPage.aspx in ListUnits_SelectedIndexChanged
                }
                else
                {
                    
                    _clientKey = Session["ClientKey"].ToString();//store here for when we redirect to ChartPage.aspx in ListUnits_SelectedIndexChanged
                    if (Session["RefID"]!=null)
                        RefineryID = Session["RefID"].ToString();
                }

                if (_clientKey.Length < 1)
                {
                    
                    if (Request.QueryString["WsP"] == null && ListUnits.Visible == false)
                        Response.Redirect("InvalidRequest.htm");
                    if (Request.QueryString["WsP"] == null && !IsPostBack)
                        Response.Redirect("InvalidRequest.htm");
                    _clientKey = Request.QueryString["WsP"];
                    
                }               
            }

            if (_clientKey.Length < 1)
                Response.Redirect("InvalidRequest.htm");
            

            if (!ValidateKey(_clientKey, ref _refineryID) && !IsPostBack)
                Response.Redirect("InvalidUser.htm");

            Session["ClientKey"] = _clientKey;//store here for when we redirect to ChartPage.aspx in ListUnits_SelectedIndexChanged

            DataSet dsTot = null;
            //'Check if it is a stack chart.If it is use ValueField1 instead of TotField 
            if (Request.QueryString["field2"] != null && Request.QueryString["field2"].Trim().Length > 0)
            {
                dsTot = DbHelper.QueryDb("SELECT 'ISNULL('+ ValueField1 +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString["cn"].Trim() + "' ");
            }
            else
            {
                dsTot = DbHelper.QueryDb("SELECT 'ISNULL('+ replace(TotField,'_Target','') +',0) AS '+ char(39)+  Legend1 + char(39) As TotField FROM Chart_LU WHERE ChartTitle='" + Request.QueryString["cn"].Trim() + "' ");
            }


            _totField = dsTot.Tables[0].Rows[0]["TotField"].ToString();
            DataSet dsLoc = DbHelper.QueryDb("SELECT location FROM Submissions WHERE refineryID='" + RefineryID + "'");
            string location = dsLoc.Tables[0].Rows[0]["Location"].ToString();
            PanelUnits.Visible = true;
            if (!IsPostBack)
            {
                Solomon.Profile.ChartService.ChartFactory svc = new Profile.ChartService.ChartFactory(ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString(), RefineryID);
                ListUnits.DataSource = svc.GetUnitsList(RefineryID, Request.QueryString["sd"], Request.QueryString["ed"]).DefaultView; // 'sb added 2017-01-03
                ListUnits.DataTextField = "UnitName";
                ListUnits.DataValueField = "UnitID";
                ListUnits.DataBind();
                ListUnits.Items.Insert(0, "");
            }
        }

        protected void ListUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Redirecting to ChartPage, so Page_Load on this page will not get fired.
            Response.Redirect("ChartPage.aspx?" + "unitId=" + Request.Form["ListUnits"] + "&" + Request.QueryString.ToString());
        }

        private string GetRefineryID()
        {
            string refId = string.Empty;
            char[] dollar = { '$' };
            int refLocation = Cryptographer.Decrypt(Request.QueryString["WsP"]).Split(dollar).Length - 1;
            refId = Cryptographer.Decrypt(Request.QueryString["WsP"]).Split(dollar)[refLocation];
            return refId;
        }

        public static bool ValidateKey(string clientKey, ref string refNum)
        {
            string company, location, path = string.Empty;
            int locIndex = 0;
            try
            {
                if (clientKey != null)
                {
                    char[] delim = { '$' };
                    company = Cryptographer.Decrypt(clientKey).Split(delim)[0]; // (0);
                    locIndex = Cryptographer.Decrypt(clientKey).Split(delim).Length - 2;
                    location = Cryptographer.Decrypt(clientKey).Split(delim)[locIndex];
                    string[] array = Cryptographer.Decrypt(clientKey).Split(delim);
                    refNum = array[array.Length - 1]; // 'last position
                                                      //_refNum = refNum
                    string clientKeysFolder = string.Empty;
                    try
                    {
                        clientKeysFolder = ConfigurationManager.AppSettings["ClientKeysFolder"].ToString();
                    }
                    catch { }

                    if (clientKeysFolder.Length > 0)
                    {
                        if (System.IO.File.Exists(clientKeysFolder + company + "_" + location + ".key"))
                        {
                            path = clientKeysFolder + company + "_" + location + ".key";
                        }
                    }
                    else
                    {
                        if (System.IO.File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key"))
                        {
                            path = "C:\\ClientKeys\\" + company + "_" + location + ".key";
                        }
                        else if (File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key"))
                        {
                            path = "D:\\ClientKeys\\" + company + "_" + location + ".key";
                        }
                        else
                        {
                            return false;
                        }
                    }

                    if (path.Trim().Length == 0)
                    {
                        return false;
                    }

                    string readKey = string.Empty;
                    using (StreamReader reader = new System.IO.StreamReader(path))
                    {
                        readKey = reader.ReadLine();
                    }
                    if (readKey.Length > 0)
                    {
                        return (readKey.Trim() == clientKey.Trim());
                    }
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        private void QueryCustomUnits()
        {
            string strRow = string.Empty;
            string query = string.Empty;
            string selectedUnit = string.Empty;
            NameValueCollection chartOptions = Request.QueryString;
            if (Request.Form["ListUnits"] != null && Request.Form["ListUnits"].Length < 1)
            {
                selectedUnit = Request.Form["ListUnits"];
            }
            else
            {
                return;
            }

            if (chartOptions["UOM"].StartsWith("US"))
            {
                _totField = "USValue AS 'UnitName'";
            }
            else
            {
                _totField = "MetValue AS 'UnitName'";
            }

            query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" +
                " WHEN 0 then '{0:#,##0}'" +
                " WHEN 1 then '{0:#,##0.0}'" +
                " WHEN 2 then '{0:N}' " +
                " END AS DecFormat ," + _totField;

            if (chartOptions["field2"] != null && chartOptions["field2"].Trim().Length > 0)
                query += "," + chartOptions["field2"];
            if (chartOptions["field3"] != null && chartOptions["field3"].Trim().Length > 0)
                query += "," + chartOptions["field3"];
            if (chartOptions["field4"] != null && chartOptions["field4"].Trim().Length > 0)
                query += "," + chartOptions["field4"];
            if (chartOptions["field5"] != null && chartOptions["field5"].Trim().Length > 0)
                query += "," + chartOptions["field5"];

            if (chartOptions["YTD"] != null && chartOptions["YTD"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["YTD"] + ",0) AS 'Year-To-Date' ";
            if (chartOptions["target"] != null && chartOptions["target"].Trim().Length > 0)
            {
                string colName = chartOptions["UOM"].StartsWith("US") ? "USTarget" : "MetTarget";
                query += ",ISNULL(" + colName + ",0) AS 'Target' ";
            }
            if (chartOptions["twelveMonthAvg"] != null && chartOptions["twelveMonthAvg"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["twelveMonthAvg"] + ",0) AS 'Rolling Average' ";

            query += " FROM Chart_LU cl,CustomUnitData mc, Submissions s " +
             " WHERE mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + Convert.ToDateTime(chartOptions["sd"]).ToShortDateString() + 
            "' AND '" + Convert.ToDateTime(chartOptions["ed"]).ToShortDateString() + "') AND " +
            " ChartTitle='" + chartOptions["cn"].Trim() + "' AND mc.UnitID='" + selectedUnit + "' " + 
            " AND(mc.FactorSet='N/A' OR mc.FactorSet='" + chartOptions["sn"].Trim() + "') " + 
            " AND(mc.Currency='N/A' OR mc.Currency='" + chartOptions["currency"].Trim() + "')  AND mc.Property=replace(cl.TotField,'_Target','')" + 
            " ORDER BY s.PeriodStart";

            string unitName = ListUnits.Items.FindByValue(selectedUnit).Text;
            query = query.Replace("UnitName", unitName.Trim());
            chrtData = DbHelper.QueryDb(query);
        }

        private void QueryUnits()
        {
            string strRow = string.Empty;
            string query = string.Empty;
            string selectedUnit = string.Empty;
            if (Request.Form["ListUnits"] != null && Request.Form["ListUnits"].Length > 0)
            {
                selectedUnit = Request.Form["ListUnits"];
            }
            else
            {
                return;
            }
            NameValueCollection chartOptions = Request.QueryString;

            query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" +
                " WHEN 0 then '{0:#,##0}'" +
                " WHEN 1 then '{0:#,##0.0}'" +
                " WHEN 2 then '{0:N}' " +
                " END AS DecFormat ," + _totField;

            if (chartOptions["field2"] != null && chartOptions["field2"].Trim().Length > 0)
                query += "," + chartOptions["field2"];
            if (chartOptions["field3"] != null && chartOptions["field3"].Trim().Length > 0)
                query += "," + chartOptions["field3"];
            if (chartOptions["field4"] != null && chartOptions["field4"].Trim().Length > 0)
                query += "," + chartOptions["field4"];
            if (chartOptions["field5"] != null && chartOptions["field5"].Trim().Length > 0)
                query += "," + chartOptions["field5"];

            if (chartOptions["YTD"] != null && chartOptions["YTD"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["YTD"] + ",0) AS 'Year-To-Date' ";
            if (chartOptions["target"] != null && chartOptions["target"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["target"] + ",0) AS 'Target' ";
            if (chartOptions["twelveMonthAvg"] != null && chartOptions["twelveMonthAvg"].Trim().Length > 0)
                query += ",ISNULL(" + chartOptions["TwelveMonthAvg"] + ",0) AS 'Rolling Average' ";

            string tableName = "UnitIndicators";

            query += " FROM Chart_LU, " + tableName + " mc ,Submissions s " +
                " WHERE   mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + RefineryID + "') AND (s.PeriodStart BETWEEN '" + Convert.ToDateTime(chartOptions["sd"]).ToShortDateString() +
                "' AND '" + Convert.ToDateTime(chartOptions["ed"]).ToShortDateString() + "') " +
                " AND ChartTitle='" + chartOptions["cn"].Trim() + "' AND mc.UnitID='" + selectedUnit + "' " +
                " AND mc.Currency='" + chartOptions["currency"].Trim() + "' " +
                " ORDER BY s.PeriodStart";
            string unitName = ListUnits.Items.FindByValue(selectedUnit).Text;
            query = query.Replace("UnitName", unitName.Trim());
            chrtData = DbHelper.QueryDb(query);
        }

        private void QueryRefinery()
        {
            string strRow = string.Empty;
            string query = string.Empty;
            NameValueCollection chartOptions = Request.QueryString;
            string scenario = "CLIENT";

            query = "SELECT (CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodStartMod, AxisLabelUS , AxisLabelMetric,CASE(Decplaces)" +
                " WHEN 0 then '{0:#,##0}'" +
                " WHEN 1 then '{0:#,##0.0}'" +
                " WHEN 2 then '{0:N}' " +
                " END AS DecFormat ," + _totField;

            if (chartOptions["field2"] != null && chartOptions["field2"].Trim().Length > 0)
            {
                query += "," + chartOptions["field2"];
            }
            if (chartOptions["field3"] != null && chartOptions["field3"].Trim().Length > 0)
            {
                query += "," + chartOptions["field3"];
            }
            if (chartOptions["field4"] != null && chartOptions["field4"].Trim().Length > 0)
            {
                query += "," + chartOptions["field4"];
            }
            if (chartOptions["field5"] != null && chartOptions["field5"].Trim().Length > 0)
            {
                query += "," + chartOptions["field5"];
            }

            string specCases = string.Empty;

            if (chartOptions["YTD"] != null && chartOptions["YTD"].Trim().Length > 0)
            {
                query += ",ISNULL(" + chartOptions["YTD"] + ",0) AS 'Year-To-Date' ";
            }
            if (chartOptions["target"] != null && chartOptions["target"].Trim().Length > 0)
            {
                query += ",ISNULL(" + chartOptions["target"] + ",0) AS 'Target' ";
            }
            if (chartOptions["twelveMonthAvg"] != null && chartOptions["twelveMonthAvg"].Trim().Length > 0)
            {
                query += ",ISNULL(" + chartOptions["TwelveMonthAvg"] + ",0) AS 'Rolling Average' ";
            }

            query += " FROM Chart_LU, " + chartOptions["table"] + " m, Submissions s " +
             " WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + RefineryID + "' AND (s.PeriodStart BETWEEN '" + Convert.ToDateTime(chartOptions["sd"]).ToShortDateString() +
             "' AND '" + Convert.ToDateTime(chartOptions["ed"]).ToShortDateString() + "') AND " +
             "RTRIM(s.DataSet) = '" + chartOptions["ds"] + "'  AND  ChartTitle='" + chartOptions["cn"].Trim() + "' ";
            if (chartOptions["sn"] != null && chartOptions["sn"].Trim().Length > 0)
            {
                if (chartOptions["sn"] != "BASE")
                    scenario = chartOptions["sn"];
            }
            
            //string factorSet = svc.FactorSet;
            if (chartOptions["table"].ToUpper() == "GENSUM")
            {
                query += " AND Currency='" + chartOptions["currency"] + "' AND m.FactorSet = '" + chartOptions["yr"] + "' AND m.UOM='" + chartOptions["UOM"] + "' AND m.Scenario='" + scenario + "' ";
            }
            else if (chartOptions["table"].ToUpper() == "MAINTAVAILCALC")
            {
                query += " AND (FactorSet ='" + chartOptions["yr"] + "' ) ";
            }
            else if (chartOptions["table"].ToUpper() == "MAINTINDEX")
            {
                query += " AND Currency='" + chartOptions["currency"] + "' AND m.FactorSet = '" + chartOptions["yr"] + "' ";
            }

            query += "ORDER BY s.PeriodStart";

            chrtData = DbHelper.QueryDb(query);
        }

        private string CreateHTMLErrorMessage(string msg)
        {
            return "<table width=672 height=368 border=0 align=center cellspacing=0 bordercolor=#76777C>" +
                                  " <tr>" +
                                      "<td height=35 align=center valign=top><p class=style1>&nbsp;</p>" +
                                      "<p class=noData>&nbsp;</p>" +
                                      "<p class=noData>&nbsp;</p>" +
                                      "<p class=noData>&nbsp;</p>" +
                                      "<p class=noData>" + msg + "</p></td>" +
                                  "</tr>" +
                                  "</table>";
        }


    }
}