﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Solomon.ProfileII.Contracts;
using Solomon.ProfileII.Services;
using System.Configuration;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Net;
using System.IO;
using  System.Web.Services.Protocols;
using Solomon.ProfileII.Logger;


namespace Solomon.ProfileII.WCFService
{

	[ServiceBehavior(InstanceContextMode=InstanceContextMode.PerCall)]
	public class ProfileService : IProfileII
	{
	
		//only use this with instancing mode Per Call
		//string _refineryId = string.Empty;

		private string _db12Conx = string.Empty;
		
		public ProfileService()
		{
			_db12Conx = ConfigurationManager.ConnectionStrings["SqlConnectionString"].ToString();
		}
			


            /// <summary>
            /// Returns rows from various tables such as Absence, Crude, Chart_LU, Opex
            /// </summary>
            /// <param name="companyID">Company ID, req'd for legacy procs</param>
            /// <param name="cert">Certificate from client for validation</param>
            /// <returns>DataSet</returns>
		public DataSet GetLookups( string companyID, byte[] cert)
		{
			try
			{
				string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
				{
					ProfileLogManager.LogException(new Exception("Invalid client key."));
					throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
				}
                if(!GoodCert(clientKey,cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
				return svc.GetLookups(refNum, companyID);
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
		}

        /// <summary>
        /// Legacy method, purpose unclear
        /// </summary>
        /// <param name="ReportCode">Report name</param>
        /// <param name="ds">Flag for testing or not. 'ACTUAL' = not testing</param>
        /// <param name="scenario">Methodology</param>
        /// <param name="currency">Currency the results will be in</param>
        /// <param name="startDate">Date results start from</param>
        /// <param name="UOM">Unit of Measure</param>
        /// <param name="studyYear">Study year</param>
        /// <param name="includeTarget">Bool for targeting results</param>
        /// <param name="includeYTD">Bool for YTD results</param>
        /// <param name="includeAVG">Bool for averges results (aka study equivalent)</param>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns>DataSet</returns>
		public DataSet GetDataDump(string ReportCode, string ds, string scenario, string currency, DateTime startDate, 
			string UOM, int studyYear, bool includeTarget, bool includeYTD, bool includeAVG, byte[] cert)
		{
			try { 
			    string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    return svc.GetDataDump(ReportCode, "Actual", scenario, currency, startDate, UOM, studyYear, includeTarget, includeYTD, 
				    includeAVG, refNum);

		    }
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
        }

        /// <summary>
        /// Returns rows from various tables such as Config, MaintTA, ProcessData, 
        /// </summary>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns>DataSet</returns>
		public DataSet GetReferences(byte[] cert)
		{
			try
			{

				string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    return svc.GetReferences(refNum);
		    }
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
}

        /// <summary>
        /// Tests connection to WCF service and database
        /// </summary>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns>String</returns>
		public string CheckService( byte[] cert)
		{
			try
			{
				string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }	
			    if(!GoodCert(clientKey,cert))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid certificate passed in from client"));
				    throw new SoapException("Invalid certificate.", new System.Xml.XmlQualifiedName());
			    }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    return svc.CheckService();
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
		}

        /// <summary>
        /// Returns template text file for Profile 'Tables'
        /// </summary>
        /// <param name="fileinfo">Template file requested</param>
        /// <param name="appVersion">Version of client-used to determine which template file repository to use</param>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns>byte array</returns>
		public byte[] DownloadTplFile(string fileinfo, string appVersion,  byte[] cert)
		{
			try
			{

				string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    return svc.DownloadTplFile(fileinfo, appVersion, refNum);
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
		}

        /// <summary>
        /// Saves client file to our server as a backup
        /// </summary>
        /// <param name="fileName">Name file will be saved as</param>
        /// <param name="bytes">bytes to create file from</param>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns></returns>
		public string UploadFile(string fileName, byte[] bytes,   byte[] cert)
		{
			try
			{
			    string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    return svc.UploadFile(fileName, bytes,refNum);
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
		}

        /// <summary>
        /// Legacy method, purpose unclear
        /// </summary>
        /// <param name="periodStart">Start date of data to return</param>
        /// <param name="periodEnd">End date of data to return</param>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns>DataSet</returns>
		public DataSet GetDataByPeriod(DateTime periodStart, DateTime periodEnd, byte[] cert)
		{
			try
			{

				string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    return svc.GetDataByPeriod(periodStart, periodEnd, refNum);
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
		}

        /// <summary>
        /// Legacy method, purpose unclear
        /// </summary>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns>DataSet</returns>
		public DataSet GetInputData(byte[] cert)
		{
			try
			{
				string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    return svc.GetInputData(refNum);
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
		}

        /// <summary>
        /// Upload client data for a period to our Submissions table
        /// </summary>
        /// <param name="ds">Flag for testing or not. 'ACTUAL' = not testing</param>
        /// <param name="cert">Certificate from client for validation</param>
		public void SubmitRefineryData(DataSet ds,  byte[] cert)
		{
			try
			{

    		    string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
			    svc.SubmitRefineryData(ds, refNum);
			}
			catch (Exception ex)
			{
                if (!ex.Message.StartsWith("Error 50001"))
                {
                    throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
                }
                
			}
		}


        /// <summary>
        /// Writes tracing and errors to Log4Net table
        /// </summary>
        /// <param name="applicationName">Application Name</param>
        /// <param name="methodology">Methodology</param>
        /// <param name="CallerIP">IP of client</param>
        /// <param name="UserID">User id of user.</param>
        /// <param name="ComputerName">Computer name of client</param>
        /// <param name="service">Service name</param>
        /// <param name="Method">Method that calls this method</param>
        /// <param name="EntityName">DB entity</param>
        /// <param name="PeriodStart">Start date of data</param>
        /// <param name="PeriodEnd">End date of data</param>
        /// <param name="status">Status (Success, Error, etc).</param>
        /// <param name="version">Version of app calling this method</param>
        /// <param name="errorMessages">Error message</param>
        /// <param name="localDomainName">Domain user has logged in to</param>
        /// <param name="osVersion">Operating System of client computer</param>
        /// <param name="browserVersion">Version of client browser</param>
        /// <param name="officeVersion">Version of client MS Office install</param>
        /// <param name="dataImportedFromBridgeFile">unclear</param>
        /// <param name="cert">Certificate from client for validation</param>
		public void WriteActivityLogExtended(string applicationName, string methodology,  string CallerIP, string UserID, 
			string ComputerName, string service, string Method, string EntityName, DateTime? PeriodStart, DateTime? PeriodEnd, 
			string status, string version, string errorMessages, string localDomainName, string osVersion, string browserVersion, 
			string officeVersion, string dataImportedFromBridgeFile, byte[] cert)
		{
            string refNum = string.Empty;
            string clientKey = string.Empty;
            ComputerName = "";
            ProfileDataManager svc = new Solomon.ProfileII.Services.ProfileDataManager(_db12Conx, GetUserHostAddress());
            try
			{	         
                
                if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key."));
				    throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                
			    svc.WriteActivityLogExtended(applicationName, methodology, refNum, CallerIP, UserID, ComputerName, service, Method, EntityName, PeriodStart, PeriodEnd, status, version, errorMessages, localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile);
                versionCheck(version); //pops error if not using supported version

            }
			catch (Exception ex)
			{
                try
                {
                    svc.WriteActivityLogExtended(applicationName, methodology, refNum, CallerIP, UserID, ComputerName, service, "WriteActivityLogExtended", EntityName, PeriodStart, PeriodEnd, "ERROR", version, ex.Message, localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile);
                }
                catch{ Exception ex2; } //do nothing
                if (ex.Message.StartsWith("Unsupported Version"))
                    throw ex;
            }
		}

        private void versionCheck(string paramVersion)
        {
            //pops error if not using supported version
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\SupportedClientVersions.txt";
                IList<string> supportedVersions = new List<string>();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        while (!sr.EndOfStream)
                        {
                            supportedVersions.Add(sr.ReadLine());
                        }
                    }
                    foreach (string ver in supportedVersions)
                    {
                        if (paramVersion.Substring(0, 1) == ver.Substring(0, 1))
                        {
                            string paramver2 = paramVersion.Replace(".", "");
                            string ver2 = ver.Replace(".", "");
                            if (Int32.Parse(paramver2) < Int32.Parse(ver2))
                            {
                                Exception oldVer = new Exception("Unsupported Version", 
                                    new Exception("You are using version " + paramVersion + 
                                    ", which is no longer supported. Please ask to be upgraded to the newest suppoted version."));
                                throw oldVer;
                            }
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                if (ex.Message.StartsWith("Unsupported Version"))
                    throw ex;
            }

        }

        /// <summary>
        /// Gets Profile Report as HTML string 
        /// </summary>
        /// <param name="queryString">List of arguments and parameters, legacy from when this was in aspx</param>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns>String</returns>
		public string GetReportHtml(string queryString, byte[] cert)
		{
			try
			{

			    string refNum = string.Empty;
			    string clientKey = string.Empty;
			    if (!GoodKey(ref refNum, ref clientKey))
			    {
				    ProfileLogManager.LogException(new Exception("Invalid client key: " + clientKey));
				    return "<HTML>ERROR</HTML>";
			    }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                // the below didn't work
                //  'In order to do Richard's 'Export to Excel' from browser control's context menu, must keep key in conx string.
                //   -we will copy from web browser and paste into xls instead, so remove clientkey
                //  queryString = queryString + "&WsP=" + clientKey; 

                ProfileReportsManager svc = new Solomon.ProfileII.Services.ProfileReportsManager(refNum);

			    return svc.GetReportHtml(queryString,refNum);
			}
			catch (Exception ex)
			{
				throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
			}
		}

        /// <summary>
        /// Gets the Refinery Scorecard report
        /// </summary>
        /// <param name="queryString">List of params and values</param>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns></returns>
        public string GetRefineryScorecardReport(string queryString, byte[] cert) //I'm using the queryString because it is needed for the dashboard in ProfileII
        {
            try
            {

                string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
                {
                    ProfileLogManager.LogException(new Exception("Invalid client key: " + clientKey));
                    return "<HTML>ERROR</HTML>";
                }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                // the below didn't work
                //  'In order to do Richard's 'Export to Excel' from browser control's context menu, must keep key in conx string.
                //   -we will copy from web browser and paste into xls instead, so remove clientkey
                //  queryString = queryString + "&WsP=" + clientKey; 

                ProfileReportsManager svc = new Solomon.ProfileII.Services.ProfileReportsManager(refNum);

                return svc.GetRefineryScorecardReport(refNum, queryString);
            }
            catch (Exception ex)
            {
                throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
            }
        }

        /// <summary>
        /// Gets the Refinery Trends report
        /// </summary>
        /// <param name="queryString">List of params and values</param>
        /// <param name="cert">Certificate from client for validation</param>
        /// <returns></returns>
        public string GetRefineryTrendsReport(string queryString, byte[] cert)//I'm using the queryString because it is needed for the dashboard in ProfileII
        {           
            try
            {

                string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
                {
                    ProfileLogManager.LogException(new Exception("Invalid client key: " + clientKey));
                    return "<HTML>ERROR</HTML>";
                }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                // the below didn't work
                //  'In order to do Richard's 'Export to Excel' from browser control's context menu, must keep key in conx string.
                //   -we will copy from web browser and paste into xls instead, so remove clientkey
                //  queryString = queryString + "&WsP=" + clientKey; 

                ProfileReportsManager svc = new Solomon.ProfileII.Services.ProfileReportsManager(refNum);

                return svc.GetRefineryTrendsReport(refNum, queryString);
            }
            catch (Exception ex)
            {
                throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
            }
        }

        public string GetTargetingReport(string queryString, byte[] cert)
        {
            try
            {
                string refNum = string.Empty;
                string clientKey = string.Empty;
                if (!GoodKey(ref refNum, ref clientKey))
                {
                    ProfileLogManager.LogException(new Exception("Invalid client key: " + clientKey));
                    return "<HTML>ERROR</HTML>";
                }
                if (!GoodCert(clientKey, cert))
                {
                    ProfileLogManager.LogException(new Exception("Invalid Certificate for client key: " + clientKey));
                    throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
                }
                // the below didn't work
                //  'In order to do Richard's 'Export to Excel' from browser control's context menu, must keep key in conx string.
                //   -we will copy from web browser and paste into xls instead, so remove clientkey
                //  queryString = queryString + "&WsP=" + clientKey; 

                ProfileReportsManager svc = new Solomon.ProfileII.Services.ProfileReportsManager(refNum);

                return svc.GetTargetingReport(queryString, refNum);
            }
            catch (Exception ex)
            {
                throw new SoapException(ex.Message, new System.Xml.XmlQualifiedName());
            }
        }

        private bool GoodKey(ref string refNum, ref string clientKey )
		{
            foreach (var hdr in OperationContext.Current.IncomingMessageHeaders)
			{
                System.ServiceModel.Channels.MessageHeaders headers = OperationContext.Current.IncomingMessageHeaders;
                if (hdr.Name.ToUpper().Trim() == "WSP")
				{                    
                    clientKey = headers.GetHeader<string>("WsP", string.Empty);// hdr.Namespace;
				}						
			}
			if (!Utilities.ValidateKey(clientKey, ref refNum))
			{
				ProfileLogManager.LogException(new Exception("Invalid client key: " + clientKey));
				throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			}
			else
			{
				return true;
			}
		}


		private bool GoodCert(string clientKey, byte[] cert)
		{
            string errorResponse = string.Empty;
			if (!Utilities.AuthenticateByCertificateAndClientId(ref errorResponse, cert, clientKey,false))
			{
				ProfileLogManager.LogException(new Exception("Invalid certificate for client key: " + clientKey));
				throw new SoapException("Invalid Certificate.", new System.Xml.XmlQualifiedName());
			}
			else
			{
				return true;
			}
			
			return false;
		}

		private string GetRefNum(string clientKey)
		{
			try
			{
				char[] delim = { '$' };
				int refLocation = Cryptographer.Decrypt(clientKey).Split(delim).Length - 1;
				string refId = Cryptographer.Decrypt(clientKey).Split(delim)[refLocation];
				return refId;
			}
			catch (Exception ex)
			{
				ProfileLogManager.LogException(new Exception("Invalid client key:" + clientKey));
				throw new SoapException("Invalid client key.", new System.Xml.XmlQualifiedName());
			}
		}

		private string GetUserHostAddress()
		{
			try
			{
				return HttpContext.Current.Request.UserHostAddress;
			}
			catch
			{
				return String.Empty;
			}
		}


    }
}
