﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChartPage.aspx.cs" Inherits="Solomon.ProfileII.WCFService.ChartPage" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<HTML lang="en">
	<HEAD>
		<title></title>
		<META http-equiv="PRAGMA" content="NO-CACHE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style type="text/css">
		  #nav { Z-INDEX: 2 }
	      #chart { }
		</style>
		<style type="text/css">
			.noData { FONT-WEIGHT: bold; FONT-SIZE: 18px; COLOR: #646567; FONT-FAMILY: Tahoma }
			.unitsheader { FONT-SIZE: 10pt; FONT-FAMILY: Tahoma }
			.showPrint { DISPLAY: none }
			.style4 { FONT-WEIGHT: bold; FONT-SIZE: 12pt; FONT-FAMILY: Tahoma }
			.style5 { FONT-SIZE: 10pt; FONT-FAMILY: Tahoma }
		</style>
		<LINK media="print" href="styles/print.css" type="text/css" rel="StyleSheet">
	</HEAD>
	<body color="#FFFFFF" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table>
				<tr>
					<td height="20%">
						<table class="small" cellPadding="0" width="100%" border="0">
							<tr>
								<td valign="top" width="50"><IMG alt="" src="ChartLogo.png"></td>
								<td style="COLOR: #666;FONT-FAMILY: Tahoma ; FONT-SIZE: 10pt;" width="220" valign="top" align="center">
								    <span style="POSITION:relative;TOP:11px"><IMG alt="" src="Profile.png"></span>
									<span style="POSITION:relative;TOP:13px">
										<br>
										A Software Service for Improving<br>
										Refinery Performance </span>
								</td>
								<td width="80"></td>
								<td style="COLOR: #666;FONT-FAMILY: Tahoma ; FONT-SIZE: 10pt;" width="300" align="center" valign="middle" style="COLOR: #666;FONT-FAMILY: Tahoma">
									<strong>
                                        
                                        
										<%=  Request.QueryString["cn"]%>
									</strong>
									<br>
									<%=  Request.QueryString["yr"] + " Methodology" %>
									<br>
									<%=  DateTime.Today.ToLongDateString()%>
								</td>
								<td></td>
							</tr>
						</table>
						<hr width=100% color=#000000 SIZE=1>
						<asp:panel id="PanelUnits" runat="server" Visible="False" CssClass="noshow">
							<DIV class="unitsheader"><BR>
								Select
								<asp:DropDownList id="ListUnits" runat="server" AutoPostBack="true" Width="300px" Height="338px" font="Tahoma"
									ForeColor="DarkBlue" Font-Bold="True" Font-Size="7pt" Font-Names="Tahoma"></asp:DropDownList>

							</DIV>
						</asp:panel>
					</td>
				</tr>
				<tr>
                    <td align="center" height="352">
                        <asp:Panel ID="Panel1" Height="352" runat="server">

                            <asp:Chart ID="Chart1" runat="server"  Height="352px" Width="640px">
                            </asp:Chart>
                        <br />
                            <asp:Label ID="lblWarnings" runat="server" text=""></asp:Label>
                        </asp:Panel>
                        <asp:Literal ID="ltMessage" runat="server"></asp:Literal>
                        


                    </td>
				</tr>
				<tr>
					<td align="center" height="20%">
						<%GetChartData();%>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>