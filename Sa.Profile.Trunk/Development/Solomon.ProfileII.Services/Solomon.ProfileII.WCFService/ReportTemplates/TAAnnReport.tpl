<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small border=0>
  <tr>
    <td colspan=9 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
  <tr>
    <td width=200></td>
    <td width=80></td>
    <td width=80></td>
    <td width=60></td>
    <td width=100></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
  </tr>
  <tr>
    <td valign=bottom><strong>Process Unit Name</strong></td>
    <td align=right valign=bottom><strong>Interval</br>(Days)&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Interval</br>(Years)&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Hours</br>Down</strong></td>
    <td align=right valign=bottom><strong>Unavailability</br>Due to T/A, %</strong></td>
    <td align=right valign=bottom><strong>T/A Cost</strong></td>
    <td align=right valign=bottom><strong>Annualized</br>T/A Cost&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Total&nbsp;&nbsp;&nbsp;&nbsp;</br>Work Hours</strong></td>
    <td align=right valign=bottom><strong>Annualized</br>Work Hours</strong></td>
  </tr>

  SECTION(Turnarounds,, SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;@UnitName</td>
    <td align=right>Format(TAIntDays,'#,##0')</td>
    <td align=right>Format(TAIntYrs,'#,##0.00')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>
    <td align=right>Format(MechUnavailTA_Ann,'#,##0.00')</td>
    <td align=right>Format(TACost,'#,##0')</td>
    <td align=right>Format(AnnTACost,'#,##0')</td>
    <td align=right>Format(TAEffort,'#,##0')</td>
    <td align=right>Format(AnnTAEffort,'#,##0')</td>
  </tr>
  END

  SECTION(TATotals,,)
  BEGIN
 			

  <tr></tr>
  <tr>
    <td colspan=4><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Refinery</strong></td>
    <td align=right><strong>Format(MechUnavailTA_Ann,'#,##0.00')</strong></td>
    <td></td>
    <td align=right><strong>Format(TotAnnTACost,'#,##0')</strong></td>
    <td align=right><strong>Format(TotTAEffort,'#,##0')</strong></td>
    <td align=right><strong>Format(TotAnnTAEffort,'#,##0')</strong></td>
  </tr>

  <tr></tr>
  <tr>
    <td colspan=4><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Allocation for Month</strong></td>
    <td align=right><strong>Format(MechUnavailTA_Ann,'#,##0.00')</strong></td>
    <td></td>
    <td align=right><strong>Format(MonthTACost,'#,##0')</strong></td>
    <td></td>
    <td align=right><strong>Format(MonthTAEffort,'#,##0')</strong></td>
  </tr>
  END

</table>

<!-- template-end -->
