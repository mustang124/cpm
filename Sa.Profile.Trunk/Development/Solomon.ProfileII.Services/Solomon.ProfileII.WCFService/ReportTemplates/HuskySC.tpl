<!-- config-start -->

<!-- config-end -->
<!-- template-start -->


<table class=small border=0 cellspacing=0>
  <tr>
    <td width=270></td>
    <td width=60></td>
    <td width=20></td>
    <td width=60></td>
    <td width=20></td>
    <td width=60></td>
    <td width=20></td>
    <td width=60></td>
    <td width=20></td>
    <td width=60></td>
    <td width=20></td>
  </tr>

  <tr>
    <td Colspan=11 align=left><img src="https://webservices.solomononline.com/RefineryReports/Templates/Husky.png"></td>
  </tr>


  <tr>
    <td colspan=11 align=Left valign=bottom><strong>Management Scorecard</strong></td>
  </tr>
  <tr>
    <td colspan=11 align=Left valign=bottom><strong>ReportPeriod</strong></td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td Colspan=2 align=center valign=bottom><strong>Current</strong></td>
    <td Colspan=2 align=center valign=bottom><strong>Study</strong></td>
    <td Colspan=4 align=center valign=bottom><strong>Previous Study Results</strong></td>
    <td Colspan=2 align=center valign=bottom><strong>Performance</strong></td>
  </tr>

  <tr>
    <td valign=bottom></td>
    <td Colspan=2 align=center valign=bottom><strong>Month</strong></td>
    <td Colspan=2 align=center valign=bottom><strong>Equivalent&sup1;</strong></td>
    <td Colspan=2 align=center valign=bottom><strong>-- 2010 --</strong></td>
    <td Colspan=2 align=center valign=bottom><strong>-- 2008 --</strong></td>
    <td Colspan=2 align=center valign=bottom><strong>Target</strong></td>
  </tr>

  SECTION(HuskySC,,)  
  BEGIN

  <tr>
    <td valign=bottom height=40><strong>&nbsp;&nbsp;&nbsp;Energy Intensity Index&reg; (EII&reg;)</strong></span></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@EII,0))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@EII_AVG,0))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@EII_Study,0))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@EII_StudyP1,0))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@EII_Target,0))</strong></td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Energy Usage, MBtu/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyUseDay,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyUseDay_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyUseDay_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyUseDay_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyUseDay_Target,0))</td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Standard Energy, MBtu/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotStdEnergy,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotStdEnergy_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotStdEnergy_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotStdEnergy_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotStdEnergy_Target,0))</td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom height=40><strong>&nbsp;&nbsp;&nbsp;Refinery Utilization, %</strong></span></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@UtilPcnt,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@UtilPcnt_AVG,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@UtilPcnt_Study,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@UtilPcnt_StudyP1,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@UtilPcnt_Target,1))</strong></td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UEDC&sup2;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@UtilUEDC,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UtilUEDC_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UtilUEDC_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UtilUEDC_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UtilUEDC_Target,0))</td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDC, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@EDC,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EDC_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EDC_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EDC_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EDC_Target,0))</td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom height=40><strong>&nbsp;&nbsp;&nbsp;Mechanical Availability, %</strong></span></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MechAvail,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MechAvail_AVG,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MechAvail_Study,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MechAvail_StudyP1,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MechAvail_Target,1))</strong></td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Annualized Turnaround Unavailability, %</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@MechUnavailTA,2))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@MechUnavailTA_AVG,2))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@MechUnavailTA_Study,2))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@MechUnavailTA_StudyP1,2))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@MechUnavailTA_Target,2))</td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround Unavailability, %</span></td>
    <td align=right valign=bottom>Format(@NonTAUnavail,2)</td>
    <td></td>
    <td align=right valign=bottom>Format(@NonTAUnavail_AVG,2)</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@NonTAUnavail_Study,2))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@NonTAUnavail_StudyP1,2))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@NonTAUnavail_Target,2))</td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom height=40><strong>&nbsp;&nbsp;&nbsp;Maintenance Index&sup3;, US $/EDC</strong></span></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MaintIndex,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MaintIndex_AVG,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MaintIndex_Study,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MaintIndex_StudyP1,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@MaintIndex_Target,1))</strong></td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Annualized Turnaround Cost, US $k/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@TAAdj,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TAAdj_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TAAdj_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TAAdj_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TAAdj_Target,0))</td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Turnaround Cost, US $k/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@RoutCost,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@RoutCost_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@RoutCost_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@RoutCost_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@RoutCost_Target,0))</td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom height=40><strong>&nbsp;&nbsp;&nbsp;Personnel Index, work hours/100 EDC</strong></span></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotWHrEDC,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotWHrEDC_AVG,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotWHrEDC_Study,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotWHrEDC_StudyP1,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotWHrEDC_Target,1))</strong></td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company, work hours/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@CompWHr,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@CompWHr_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@CompWHr_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@CompWHr_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@CompWHr_Target,0))</td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contractor, work hours/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@ContWhr,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@ContWhr_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@ContWhr_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@ContWhr_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@ContWhr_Target,0))</td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom height=40><strong>&nbsp;&nbsp;&nbsp;Non-Energy Operating Expenses, US cents/UEDC</strong></span></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@NEOpexUEDC,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@NEOpexUEDC_AVG,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@NEOpexUEDC_Study,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@NEOpexUEDC_StudyP1,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@NEOpexUEDC_Target,1))</strong></td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non-Energy Operating Costs, US $k/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@NEOpex,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@NEOpex_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@NEOpex_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@NEOpex_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@NEOpex_Target,0))</td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UEDC&sup2;, k</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@UEDC,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UEDC_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UEDC_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UEDC_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@UEDC_Target,0))</td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom height=40><strong>&nbsp;&nbsp;&nbsp;Total Cash Operating Expenses, US cents/UEDC</strong></span></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotCashOpexUEDC,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotCashOpexUEDC_AVG,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotCashOpexUEDC_Study,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotCashOpexUEDC_StudyP1,1))</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>NoShowZero(Format(@TotCashOpexUEDC_Target,1))</strong></td>
    <td></td>
  </tr>


  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Energy Costs, US $k/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyCost,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyCost_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyCost_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyCost_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@EnergyCost_Target,0))</td>
    <td></td>
  </tr>

  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Operating Costs, US $k/d</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpex,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpex_AVG,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpex_Study,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpex_StudyP1,0))</td>
    <td></td>
    <td align=right valign=bottom>NoShowZero(Format(@TotCashOpex_Target,0))</td>
    <td></td>
  </tr>

  END  

  <tr>
    <td height=80 Colspan=11 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&sup1; 12-month average for non-maintenance indicators and 24-month average for maintnenance indicators</td>
  </tr>

  <tr>
    <td Colspan=11 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&sup2; excludes credit for energy distribution in utilization and includes the credit for operating expenses</td>
  </tr>

  <tr>
    <td Colspan=11 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&sup3; each column includes turnaround costs on an annualized basis</td>
  </tr>

</table>
<!-- template-end -->
