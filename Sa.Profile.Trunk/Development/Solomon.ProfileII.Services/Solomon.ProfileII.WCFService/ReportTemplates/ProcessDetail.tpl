<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small border=0>
  <tr>
    <td colspan=20 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
  <tr>
    <td width=200></td>
    <td width=60></td>
    <td width=80></td>
    <td width=60></td>
    <td width=60></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
  </tr>
  <tr>
    <td valign=bottom><strong>Process Unit Name</strong></td>
    <td align=left valign=bottom><strong>Process</br>&nbsp;&nbsp;&nbsp;&nbsp;ID</strong></td>
    <td align=right valign=bottom><strong>Capacity</strong></td>
    <td align=right valign=bottom><strong>EDC, k</strong></td>
    <td align=right valign=bottom><strong>UEDC, k</strong></td>
    <td align=right valign=bottom><strong>Estimated</br>Gain&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Standard</br>Energy&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Standard</br>MEI, k&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Standard</br>PEI, k&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Standard</br>NEI, k&nbsp;&nbsp;</strong></td>
  </tr>

  SECTION(ProcessUnits,ProcessID<>'STEAMGEN' AND ProcessID<>'ELECGEN'AND ProcessID<>'TNK+BLND'AND ProcessID<>'OFFCOKE'AND ProcessID<>'ELECDIST', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;@UnitName</td>
    <td>Format(ProcessID,'#,##0')</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
  </tr>
  END

  <tr>
    <td colspan=10 height=30 valign=bottom><strong>Utilities and Off-sites</strong></td>
  </tr>

  SECTION(ProcessUnits,ProcessID='STEAMGEN' OR ProcessID='ELECGEN' OR ProcessID='TNK+BLND' OR ProcessID='OFFCOKE' OR ProcessID='ELECDIST', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;@UnitName</td>
    <td>Format(ProcessID,'#,##0')</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td align=right>Format(MaintEffDiv,'#,##0')</td>
    <td align=right>Format(PersEffDiv,'#,##0')</td>
    <td align=right>Format(NEOpexEffDiv,'#,##0')</td>
  </tr>
  END


  SECTION(UnitsOffsites,,)
  BEGIN
 
  <tr>
    <td colspan=2>&nbsp;&nbsp;&nbsp;Standard Tankage</td>
    <td align=right>Format(TnkStdCap,'#,##0')</td>
    <td align=right>Format(TnkStdEDC,'#,##0')</td>
    <td align=right>Format(TnkStdEDC,'#,##0')</td>
    <td colspan=6></td>
  </tr>

  <tr>
    <td colspan=4>&nbsp;&nbsp;&nbsp;Purchased Utility UEDC Credit</td>
    <td align=right>Format(PurchasedUtilityUEDC,'#,##0')</td>
    <td colspan=6></td>
  </tr>

  <tr>
    <td colspan=3>&nbsp;&nbsp;&nbsp;Receipts and Shipments</td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td colspan=2></td>
    <td align=right>Format(TotRSMaintEffDiv,'#,##0')</td>
    <td align=right>Format(TotRSPersEffDiv,'#,##0')</td>
    <td align=right>Format(TotRSNEOpexEffDiv,'#,##0')</td>
  </tr>

  <tr>
    <td colspan=6>&nbsp;&nbsp;&nbsp;Sensible Heat</td>
    <td align=right>Format(SensHeatStdEnergy,'#,##0')</td>
    <td colspan=3></td>
  </tr>
  <tr>
    <td colspan=6>&nbsp;&nbsp;&nbsp;Asphalt</td>
    <td align=right>Format(AspStdEnergy,'#,##0')</td>
    <td colspan=3></td>
  </tr>
  <tr>
    <td colspan=6>&nbsp;&nbsp;&nbsp;All Other</td>
    <td align=right>Format(OffsitesStdEnergy,'#,##0')</td>
    <td colspan=3></td>
  </tr>
  <tr>
    <td height=30 valign=bottom colspan=3><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Refinery</strong></td>
    <td align=right valign=bottom><strong>Format(TotEDC,'#,##0')</strong></td>
    <td align=right valign=bottom><strong>Format(TotUEDC,'#,##0')</strong></td>
    <td align=right valign=bottom><strong>Format(EstGain,'#,##0')</strong></td>
    <td align=right valign=bottom><strong>Format(TotStdEnergy,'#,##0')</strong></td>
    <td align=right valign=bottom><strong>Format(MaintEffDiv,'#,##0')</strong></td>
    <td align=right valign=bottom><strong>Format(PersEffDiv,'#,##0')</strong></td>
    <td align=right valign=bottom><strong>Format(NEOpexEffDiv,'#,##0')</strong></td>
  </tr>
  END
</table>

<!-- template-end -->