<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class='small' border=0 width=100%>
	<tr>
		<td width=475 colspan=3>&nbsp;</td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Top 2</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Break 1/2</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Break 2/3</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Break 3/4</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Bottom 2</b></td>
		<td>&nbsp;</td>
	</tr>
	SECTION(Table1,,)
      BEGIN
	 Header('<tr><td>&nbsp;</td></tr><tr><td><b>RefList Name:</b></td>  <td><b>Rank Break:</b></td>  <td><b>Break Value:</b></td> </tr>')
	<tr>
		<td>@RefListName</td>
		<td>@RankBreak</td>
		<td align="left">@BreakValue</td>
		<td>&nbsp;</td>
		<td align="left">Format(@Top2,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile1Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile2Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Tile3Break,'#,##0')</td>
		<td>&nbsp;</td>
		<td align="center">Format(@Btm2,'#,##0')</td>
		<td>&nbsp;</td>
	</tr>
      END
</table>
<!-- template-end -->