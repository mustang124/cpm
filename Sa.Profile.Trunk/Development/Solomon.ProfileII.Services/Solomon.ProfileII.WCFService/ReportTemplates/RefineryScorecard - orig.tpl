<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)
ShowIf(Ytd,IfYtdOn)
ShowIf(Avg,IfAvgOn)

<table class=small border=0>
  <tr>
    <td width=300></td>
    <td width=100></td>
    <td width=100></td>
    <td width=100></td>
    <td width=100></td>
  </tr>

  <tr>
    <td></td>
    <td align=right valign=bottom><strong>ReportPeriod</strong></td>
    <td align=right valign=bottom><strong>Tag(Target,Refinery<br/>Target&nbsp;</strong>)</td>
    <td align=right valign=bottom><strong>Tag(Avg,Rolling&nbsp;</br>Average</strong>)</td>
    <td align=right valign=bottom><strong>Tag(Ytd,YTD&nbsp;&nbsp;&nbsp;&nbsp;</br>Average</strong>)</td>
  </tr>
  
  SECTION(Chart_LU,SortKey < 800 AND SectionHeader <>'By Process Unit' , SortKey ASC)
  INCLUDETABLE(MAINTAVAILCALC)
  INCLUDETABLE(GENSUM)
  INCLUDETABLE(MAINTENANCE)
  BEGIN
  Header('
  <tr>
    <td colspan=5 height=25 valign=bottom><strong>@SectionHeader</strong></td>
  </tr>')
  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;@ChartTitle<span align=left>USEUNIT(AxisLabelUS,AxisLabelMetric)</span></td>
    <td align=right valign=bottom>NoShowZero(Format(@@ValueField1,@DecPlaces))</td>
    <td align=right>TAG(Target,NoShowZero(Format(@@TargetField,@DecPlaces)))</td>
    <td align=right valign=bottom>Tag(Avg,NoShowZero(Format(@@AvgField,@DecPlaces)))</td>
    <td align=right>Tag(Ytd,NoShowZero(Format(@@YTDField,@DecPlaces)))</td>
  </tr>
  END
  
 SECTION(UserDefined,,HeaderText ASC)
  BEGIN
  HEADER('<tr>
    <td colspan=5 height=25 valign=bottom><strong>@HeaderText</strong></td>
  </tr>')
  <tr>
    <td valign=bottom>&nbsp;&nbsp;&nbsp;@VariableDesc</td>
    <td align=right valign=bottom>NoShowZero(Format(@RptValue,@DecPlaces))</td>
    <td align=right valign=bottom>Tag(Target, NoShowZero(Format(@RptValue_Target,@DecPlaces)))</td>
    <td align=right valign=bottom>Tag(Avg,NoShowZero(Format(@RptValue_Avg,@DecPlaces)))</td>
    <td align=right valign=bottom>Tag(Ytd,NoShowZero(Format(@RptValue_YTD,@DecPlaces)))</td>
  </tr>
  END
</table>
<!-- template-end -->
