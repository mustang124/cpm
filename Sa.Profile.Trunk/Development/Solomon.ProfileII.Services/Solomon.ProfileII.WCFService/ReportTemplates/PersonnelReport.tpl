  <!-- config-start -->
  <!-- config-end -->
  <!-- template-start -->
  <style>
    .Indent2{padding-left:10px;              vertical-align:text-bottom;}
    .Indent3{padding-left:20px;              vertical-align:text-bottom;}

    .Header0{padding-left: 0px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .Header1{padding-left:0px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .Header2{padding-left:10px; Height:10px; vertical-align:text-bottom; font-weight:700;}
    .Header3{padding-left:20px; Height:10px; vertical-align:text-bottom; font-weight:700;}
    .SubTot3{padding-left:30px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .SubTot4{padding-left:40px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .SubTot5{padding-left:50px; Height:25px; vertical-align:text-bottom; font-weight:700;}
    .hvy    {                                                            font-weight:700;}
  </style>

  <table class=small border=0>

    <tr>
      <td width=200></td>
      <td width=80></td>
      <td width=70></td>
      <td width=70></td>
      <td width=60></td>
      <td width=80></td>
      <td width=70></td>
      <td width=100></td>
      <td width=70></td>
      <td width=60></td>
    </tr>
   
    <tr>
      <td></td>
      <td colspan=6 align=center valign=bottom><strong>------------------ Reported work hours for ReportPeriod -------------------</strong></td>
      <td colspan=3 align=right valign=bottom><strong>Personnel Index, wk hrs/100 EDC</strong></td>
    </tr>

    <tr>
      <td></td>
      <td align=right valign=bottom><strong>Straight-Time</strong></td>
      <td align=right valign=bottom><strong>Overtime</strong></td>
      <td align=right valign=bottom><strong>Contract</strong></td>
      <td align=right valign=bottom><strong>G&A</strong></td>
      <td align=right valign=bottom><strong>Absences</strong></td>
      <td align=right valign=bottom><strong>Total</strong></td>
      <td align=right valign=bottom><strong>Company</strong></td>
      <td align=right valign=bottom><strong>Contract</strong></td>
      <td align=right valign=bottom><strong>Total&nbsp;&nbsp;</strong></td>
    </tr>
  
    <tr>
      <td class="Header1">Operator, Craft & Clerical</td>
    </tr>
    
    SECTION(Personnel,SectionId='OO' AND PersID = 'OO',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Process Operations</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='OM' AND PersID = 'OCCMA',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Maintenance Activities</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='OM' AND PersID = 'OCCTAADJ',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent3">Adjusted T/A Maintenance</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END
	
	SECTION(Personnel,SectionId='OT' AND PersID = 'OCCTS',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Technical Support</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END
    
    SECTION(Personnel,SectionId='OA' AND PersID = 'OA',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Administrative Services</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='TO' AND PersID = 'TO',SortKey ASC)
    BEGIN
    <tr class="hvy">
      <td class="Header3">Subtotal O,C&C</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    <tr>
      <td class="Header1">Management, Professional & Staff</td>
    </tr>

    SECTION(Personnel,SectionId='MO' AND PersID = 'MO',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Process Operations</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='MM' AND PersID = 'MPSMA',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Maintenance Activities</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='MM' AND PersID = 'MPSTAADJ',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent3">Adjusted T/A Maintenance</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END
    
    SECTION(Personnel,SectionId='MT' AND PersID = 'MT',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Technical Support</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='MA' AND PersID = 'MA',SortKey ASC)
    BEGIN
    <tr>
      <td class="Indent2">Administrative Services</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='TM' AND PersID = 'TM',SortKey ASC)
    BEGIN
    <tr class="hvy">
      <td class="Header3">Subtotal M,P&S</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END

    SECTION(Personnel,SectionId='TP' AND PersID = 'TP',SortKey ASC)
    BEGIN
    <tr class="hvy">
      <td class="SubTot4">Total Company</td>
      <td align="right">NoShowZero(Format(STH,'#,##0'))</td>
      <td align="right">NoShowZero(Format(OVTHours,'#,##0'))</td>
      <td align="right">NoShowZero(Format(Contract,'#,##0'))</td>
      <td align="right">NoShowZero(Format(GA,'#,##0'))</td>
      <td align="right">NoShowZero(Format(AbsHrs,'#,##0'))</td>
      <td align="right">NoShowZero(Format(TotWhr,'#,##0'))</td>
      <td align="right">NoShowZero(Format(CompWhrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(ContWHrEDC,'#,##0.0'))</td>
      <td align="right">NoShowZero(Format(TotWHrEDC,'#,##0.0'))</td>
    </tr>
    END
     
  </table>

  <!-- template-end -->