﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text;

namespace Solomon.Profile.ChartService
{
    internal static class SqlHelper
    {
        
        internal static string BuildDataTable1SqlWithTheseFields(string refineryID, string periodStart,
            string periodEnd, string ChartTitle, string currency, string factorSet,
            string uom, string scenario, string tableName, string ytdField, string targetField, string aveField)
        //periodStart and periodEnd expect format '6/1/2015'
        {
            StringBuilder builder = new System.Text.StringBuilder("SELECT CAST(YEAR(s.PeriodStart) AS varchar(4)) as PeriodYear, ");
            builder.Append("CAST(MONTH(s.PeriodStart) AS varchar(2)) As PeriodMonth, ");
            builder.Append("(CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodDisplay,  ");
            builder.Append("AxisLabelUS , AxisLabelMetric, ");
            builder.Append("CASE(Decplaces) WHEN 0 then '{0:#,##0}' WHEN 1 then '{0:#,##0.0}' WHEN 2 then '{0:N}'  END AS DecFormat , ");
            builder.Append("'' As Description, ");
            if (ytdField != null && ytdField.Length > 0)
            {
                builder.Append("ISNULL(" + ytdField + ",0) AS 'YearToDate' , ");
            }
            else
            {
                builder.Append("0 AS 'YearToDate' , ");
            }

            if (targetField != null && targetField.Length > 0)
            {
                builder.Append("ISNULL(" + targetField + ",0) AS 'Target' , ");
            }
            else
            {
                builder.Append("0 AS 'Target' , ");
            }

            if (aveField != null && aveField.Length > 0)
            {
                builder.Append("ISNULL(" + aveField + ",0) AS 'RollingAverage'  ");
            }
            else
            {
                builder.Append("0 AS 'RollingAverage'   ");
            }

            string tableToUse = "GenSum";
            if (tableName!=null && tableName.Length>1)
            {
                tableToUse=tableName;
            }
            
            builder.Append("FROM Chart_LU, " + tableToUse + " m, Submissions s   ");
            builder.Append("WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID =  ");
            builder.Append("'" + refineryID + "' ");
            builder.Append("AND (s.PeriodStart BETWEEN '" + periodStart + "' AND '" + periodEnd + "') ");
            builder.Append("AND UPPER(RTRIM(s.DataSet)) = 'ACTUAL' ");
            builder.Append("AND  ChartTitle='" + ChartTitle + "' ");
            if (tableToUse.ToUpper() == "MAINTAVAILCALC")
            {
                builder.Append(" AND FactorSet ='" + factorSet + "' ");
            }
            else if (tableToUse.ToUpper() == "MAINTINDEX")
            {
                builder.Append("AND Currency='" + currency + "' AND m.FactorSet = '" + factorSet + "' ");
            }
            else  //"GENSUM")
            {
                builder.Append("AND Currency='" + currency + "' AND m.UOM='" + uom + "' AND RTRIM(m.Scenario)='" + scenario + "' ");
            }
                    
            builder.Append("ORDER BY s.PeriodStart; ");
            return builder.ToString();
        }

        internal static string BuildPlantInfoSql(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, //string factorSet,
            string uom, string scenario)
        {
            string[] parts = field1.Replace(" AS ", "^").Split('^');

            StringBuilder builder = new System.Text.StringBuilder("SELECT CAST(YEAR(s.PeriodStart) AS varchar(4)) as PeriodYear, ");
            builder.Append("CAST(MONTH(s.PeriodStart) AS varchar(2)) As PeriodMonth, ");
            builder.Append("(CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodDisplay,  ");
            builder.Append("AxisLabelUS , AxisLabelMetric, ");
            builder.Append("CASE(Decplaces) WHEN 0 then '{0:#,##0}' WHEN 1 then '{0:#,##0.0}' WHEN 2 then '{0:N}'  END AS DecFormat , ");
            builder.Append(parts[1].Trim() + " As Description, ");
            builder.Append(parts[0].Trim() + " AS CurrentMonth  ");
            builder.Append("FROM Chart_LU,GenSum m, Submissions s   ");
            builder.Append("WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + refineryId + "' ");
            builder.Append("AND (s.PeriodStart BETWEEN '" + periodStart + "' AND '" + periodEnd + "') ");
            builder.Append("AND UPPER(RTRIM(s.DataSet)) = 'ACTUAL' ");
            builder.Append("AND  ChartTitle='" + ChartTitle + "' ");
            builder.Append("AND Currency='" + currency + "' AND m.UOM='" + uom + "' AND RTRIM(m.Scenario)='" + scenario + "' ");
            builder.Append("ORDER BY s.PeriodStart; ");
            return builder.ToString();
        }

        internal static string BuildPlantMechAvailInfoSql(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, //string factorSet,
            string uom, string scenario)
        {
            string[] parts = field1.Replace(" AS ", "^").Split('^');

            StringBuilder builder = new System.Text.StringBuilder("SELECT CAST(YEAR(s.PeriodStart) AS varchar(4)) as PeriodYear, ");
            builder.Append("CAST(MONTH(s.PeriodStart) AS varchar(2)) As PeriodMonth, ");
            builder.Append("(CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodDisplay,  ");
            builder.Append("AxisLabelUS , AxisLabelMetric, ");
            builder.Append("CASE(Decplaces) WHEN 0 then '{0:#,##0}' WHEN 1 then '{0:#,##0.0}' WHEN 2 then '{0:N}'  END AS DecFormat , ");
            builder.Append(parts[1].Trim() + " As Description, ");
            builder.Append(parts[0].Trim() + " AS CurrentMonth  ");
            builder.Append(" FROM Chart_LU, MaintAvailCalc m, Submissions s   ");
            builder.Append(" WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + refineryId + "' ");
            builder.Append(" AND (s.PeriodStart BETWEEN '" + periodStart + "' AND '" + periodEnd + "') ");
            builder.Append(" AND UPPER(RTRIM(s.DataSet)) = 'ACTUAL' ");
            builder.Append(" AND  ChartTitle='" + ChartTitle + "' ");
            builder.Append(" AND RTRIM(m.Factorset)='" + scenario + "' ");
            builder.Append(" ORDER BY s.PeriodStart; ");
            return builder.ToString();
        }

        internal static string BuildPlantMaintIndexInfoSql(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, //string factorSet,
            string uom, string scenario)
        {
            string[] parts = field1.Replace(" AS ", "^").Split('^');

            StringBuilder builder = new System.Text.StringBuilder("SELECT CAST(YEAR(s.PeriodStart) AS varchar(4)) as PeriodYear, ");
            builder.Append("CAST(MONTH(s.PeriodStart) AS varchar(2)) As PeriodMonth, ");
            builder.Append("(CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodDisplay,  ");
            builder.Append("AxisLabelUS , AxisLabelMetric, ");
            builder.Append("CASE(Decplaces) WHEN 0 then '{0:#,##0}' WHEN 1 then '{0:#,##0.0}' WHEN 2 then '{0:N}'  END AS DecFormat , ");
            builder.Append(parts[1].Trim() + " As Description, ");
            builder.Append(parts[0].Trim() + " AS CurrentMonth  ");
            builder.Append(" FROM Chart_LU, MaintIndex m, Submissions s   ");
            builder.Append(" WHERE s.SubmissionID=m.SubmissionID AND s.RefineryID = '" + refineryId + "' ");
            builder.Append(" AND (s.PeriodStart BETWEEN '" + periodStart + "' AND '" + periodEnd + "') ");
            builder.Append(" AND UPPER(RTRIM(s.DataSet)) = 'ACTUAL' ");
            builder.Append(" AND  ChartTitle='" + ChartTitle + "' ");
            builder.Append(" AND Currency='" + currency + "'"); 
            builder.Append(" AND RTRIM(m.Factorset)='" + scenario + "' ");
            builder.Append(" ORDER BY s.PeriodStart; ");
            return builder.ToString();
        }

        internal static string BuildRankVariableQuerySql(List<int> kpiIds)
        {
            string sql = "select LongText from resultsDB.dbo.VarDef where VKey in(";
            foreach (int i in kpiIds)
            {
                sql = sql + i.ToString() + ",";
            }
            sql = sql.Remove(sql.Length - 1) + ");";
            return sql;
        }

        internal static string BuildReflistQuerySql(int peerGroupId)
        {
            return "select * from resultsDB.dbo.PeerGroupDef where PGKey=" + peerGroupId.ToString();
        }

        internal static string GetKpiNamesFromChartLUSql(List<int> vkeys)
        {
            string vkeysString = string.Empty;
            foreach (int key in vkeys)
            {
                vkeysString += key.ToString() + ",";
            }
            vkeysString = vkeysString.Remove(vkeysString.Length - 1); //trim trailing comma
            string sql = "SELECT DataTable, ValueField1 from dbo.Chart_LU where vkey in(" + vkeysString + ");";
            return sql;
        }


        internal static string BuildQuartileDataTable1Sql(string peerGroupId, List<int> rankVariables)
        {
            //periodStart and periodEnd expect format '6/1/2015'
            StringBuilder rankVariablesString = new System.Text.StringBuilder();

            foreach (int item in rankVariables)
            {
                rankVariablesString.Append("'" + item.ToString() + "',");
            }
            rankVariablesString.Remove(rankVariablesString.ToString().Length - 1, 1); //remove trailing comma
            StringBuilder builder = new System.Text.StringBuilder("SELECT 0 as Sort, 1900 as PeriodYear, 1 as PeriodMonth, ");
            builder.Append("'1/1900' as PeriodDisplay, "); //'DESC' as [Description], Top2 as QTop2,Tile1Break as Q1to2, ");
            builder.Append(" r.[Description] as [Description],  q.Top2 as QTop2, q.Tile1Break as Q1to2,  q.Tile2Break as Q2to3, q.Tile3Break as Q3to4, q.Bottom2 as QBottom2 ");
            builder.Append(" from ProfileFuels12.profile.ReportLayout_LU r ");
            builder.Append(" left outer join ProfileFuels12.profile.TargetingVariablesAvailable a on r.VKey=a.VKey ");
            builder.Append(" left outer join ResultsDB.dbo.VarDef v on r.VKey=v.VKey ");
            builder.Append(" left outer join ResultsDB.dbo.Quartiles q   on q.VKey=r.VKey and q.PGKey=" + peerGroupId);
            builder.Append(" where ");
            builder.Append(" (IsHeader>0 or DataCount Is Not Null)");
            builder.Append(" and v.vkey in(" + rankVariablesString + ")");
            builder.Append(" and r.Active=1");
            builder.Append(" order by r.[Description] asc;");
            return builder.ToString();
        }

        internal static List<string> BuildAvgYtdTargetDataTable1Sql(List<string> kpiNames, string breakValue,  string refId,
             string currency, string uom, string factorSet, string periodStart, string scenario)
        {
            if(uom=="Metric")
                uom="MET";
            List<string> selects = new List<string>();
            string select = string.Empty;
            int i = 0;
            foreach (string item in kpiNames)
            {
                select = BuildIndividualAvgYtdTargetDataTable1Sql(item, breakValue, i, refId, currency, uom, factorSet, periodStart, scenario);
                selects.Add(select);
                i++;
            }
            return selects;
        }

        internal static string GetFactorset(string refineryId)
        {
            return "SELECT ReportFactorSet from dbo.TSort where RefineryID='" + refineryId + "'";
        }

        private static string BuildIndividualAvgYtdTargetDataTable1Sql(string kpiName, string breakValue,  int sort,
            string refId, string currency, string uom, string factorSet, string periodStart, string scenario)
        {
            string[] parts = kpiName.Split('.');
            //assume no tablename
            string tableName = "dbo.GenSum";
            string fieldName = parts[0];
            //but if tablename, then:
            if (parts.Length > 1)
            {
                tableName ="dbo." + parts[0];
                fieldName = parts[1];
            }
            
            StringBuilder builder = new System.Text.StringBuilder("SELECT " + sort.ToString() + " as Sort, 0 as PeriodYear,0 as PeriodMonth, ");
            builder.Append("'' as PeriodDisplay, "); // '' as [Description],  '' as QTop2, '' as Q1to2, ");
            builder.Append("'" + fieldName + "' as [Description], ");
            builder.Append(" 0 as QTop2, 0 as Q1to2,   0 as Q2to3,  0 as Q3to4,  0 as QBottom2, ");
            builder.Append(fieldName + "_YTD as Ytd, " + fieldName + "_Avg   as Avg,  " + fieldName + "_Target  as Target ");
            builder.Append("from " + tableName + " ");
            builder.Append(" where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '" + refId + "' and PeriodStart = '" + periodStart + "' and DataSet = 'Actual' and Scenario = '" + scenario + "') ");
            builder.Append("and factorset = '" + factorSet + "' ");
            builder.Append("and currency = '" + currency + "' ");
            builder.Append("and UOM = '" + uom + "';");
            return builder.ToString();
        }

        internal static string BuildUnitsListSql(string refineryId, string periodStart, string periodEnd)
        {
            StringBuilder builder = new System.Text.StringBuilder("SELECT DISTINCT c.UnitID, RTRIM(c.ProcessID) +': '+ RTRIM(c.UnitName) AS UnitName,p.SortKey ");
            builder.Append(" FROM Config c,ProcessID_LU p ");
            builder.Append(" WHERE p.ProcessID=c.ProcessID AND p.MaintDetails='Y'  ");
            builder.Append(" AND c.SubmissionID IN ");
            builder.Append(" (SELECT SubmissionID FROM Submissions WHERE RefineryID='" + refineryId + "' AND PeriodStart BETWEEN '" + periodStart + "' AND '" + periodEnd + "') ");
            builder.Append(" ORDER BY p.SortKey");
            return builder.ToString();
        }

        internal static string BuildDataTable1UnitsSql(List<string> fields, string refineryID, string periodStart,
            string periodEnd, string ChartTitle, string currency, string unitId)
        //periodStart and periodEnd expect format '6/1/2015'
        {
            StringBuilder builder = new System.Text.StringBuilder();
            foreach (string field in fields)
            {
                string[] parts = field.Replace(" AS ", "^").Split('^');
                builder.Append("SELECT CAST(YEAR(s.PeriodStart) AS varchar(4)) as PeriodYear, ");
                builder.Append("CAST(MONTH(s.PeriodStart) AS varchar(2)) As PeriodMonth, ");
                builder.Append("(CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodDisplay,  ");
                builder.Append("AxisLabelUS , AxisLabelMetric, ");
                builder.Append("CASE(Decplaces) WHEN 0 then '{0:#,##0}' WHEN 1 then '{0:#,##0.0}' WHEN 2 then '{0:N}'  END AS DecFormat , ");
                builder.Append("'' As Description, ");

                for (int i = 0; i < parts.Length; i++)
                {
                    string fieldName = string.Empty;
                    switch (i)
                    {
                        case 0:
                            fieldName = "Target";
                            break;
                        case 1:
                            fieldName = "Ytd";
                            break;
                        default:
                            fieldName = "Avg"; //looks like this should never be used for Units....
                            break;
                    }
                    builder.Append(" ISNULL(" + parts[i] + ",0) AS '" + fieldName + "' , ");
                }
                builder.Remove(builder.Length - 2, 2);
                builder.Append(" FROM  Chart_LU, UnitIndicators mc ,Submissions s ");
                builder.Append("WHERE s.SubmissionID=mc.SubmissionID AND s.RefineryID =  ");
                builder.Append("'" + refineryID + "' ");
                builder.Append(" AND (s.PeriodStart BETWEEN '" + periodStart + "' AND '" + periodEnd + "') ");
                builder.Append(" AND UPPER(RTRIM(s.DataSet)) = 'ACTUAL' ");
                builder.Append(" AND  ChartTitle='" + ChartTitle + "' ");
                builder.Append(" AND Currency='" + currency + "' AND mc.UnitId = '" + unitId + "'");
                builder.Append(" ORDER BY s.PeriodStart; ");
            }
            return builder.ToString();
        }

        internal static string BuildUnitCurrentMonthsSql(string fields, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, string unitId)
        {
            StringBuilder builder = new System.Text.StringBuilder();

            string[] parts = fields.Replace(" AS ", "^").Split('^');

            builder.Append("SELECT CAST(YEAR(s.PeriodStart) AS varchar(4)) as PeriodYear, ");
            builder.Append("CAST(MONTH(s.PeriodStart) AS varchar(2)) As PeriodMonth, ");
            builder.Append("(CAST(MONTH(s.PeriodStart) AS varchar(2)) + '/' + CAST(YEAR(s.PeriodStart) AS varchar(4))) AS PeriodDisplay,  ");
            builder.Append("AxisLabelUS , AxisLabelMetric, ");
            builder.Append("CASE(Decplaces) WHEN 0 then '{0:#,##0}' WHEN 1 then '{0:#,##0.0}' WHEN 2 then '{0:N}'  END AS DecFormat , ");
            builder.Append(parts[1].Trim() + " As Description, ");
            builder.Append(parts[0].Trim() + " AS CurrentMonth  ");
            builder.Append(" FROM Chart_LU, UnitIndicators mc, Submissions s   ");
            builder.Append(" WHERE mc.SubmissionID=s.SubmissionID  AND (s.RefineryID = '" + refineryId + "' ");
            builder.Append(" AND s.PeriodStart BETWEEN '" + periodStart + "' AND '" + periodEnd + "') ");
            builder.Append(" AND UPPER(RTRIM(s.DataSet)) = 'ACTUAL' ");
            builder.Append(" AND  ChartTitle='" + ChartTitle + "' ");
            builder.Append(" AND mc.UnitID='" + unitId + "'");
            builder.Append(" AND mc.Currency='" + currency + "'");
            builder.Append(" ORDER BY s.PeriodStart; ");

            return builder.ToString();
        }

        
        //=========================
        private static string GensumColumnMatch(string rankVariableToMatch)
        {
            switch (rankVariableToMatch.ToUpper())
            {
                case "CASHOPEXBBL":
                    return "TotCashOpexBbl";
                    break;
                case "MAINTEFFINDEX":
                    return "MEI";
                    break;
                case "MAINTPERSEFFINDEX":
                    return "MaintPEI";
                    break;
                case "NONMAINTPERSEFFINDEX":
                    return "NonMaintPEI";
                    break;
                case "OPEXUEDC":
                    return "TotCashOpexUEDC";
                    break;
                case "REFUTILPCNT":
                    return "UtilPcnt";
                    break;
                default:
                    return rankVariableToMatch;
                    break;
            }
        }



    }
}
