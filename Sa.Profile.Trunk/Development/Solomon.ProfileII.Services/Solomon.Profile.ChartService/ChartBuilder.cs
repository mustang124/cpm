﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Data;

namespace Solomon.Profile.ChartService
{
    internal class ChartBuilder
    {
        public Chart BuildChart(ChartInfo chartInfo, DataSet dataSet, ref Chart chartTemp) 
        {
            ChartArea area = new ChartArea();
            area.Name = "ChartArea1";
            area.BorderWidth = 0;
            chartTemp.ChartAreas.Add(area);

            for (int counter = 0; counter < chartInfo.SeriesList.Count; counter++)
            {
                ChartPageSeries thisSeries = chartInfo.SeriesList[counter];

                thisSeries.Legend.DockingPosition = Docking.Bottom;
                thisSeries.Legend.Alignment = System.Drawing.StringAlignment.Center;

                Series newSeries = new Series(thisSeries.NameToUse);
                newSeries.ChartType = thisSeries.ChartType;
                
                if (thisSeries.Legend != null)
                {
                    Legend legend = new Legend();
                    legend.Docking = thisSeries.Legend.DockingPosition;
                    legend.Alignment = thisSeries.Legend.Alignment;
                    chartTemp.Legends.Add(legend);
                }
                
                newSeries.Color = thisSeries.Color;
                
                if (thisSeries.XAxisAngle != 0)
                    chartTemp.ChartAreas[counter].AxisX.LabelStyle.Angle = thisSeries.XAxisAngle;
                
                ChartPageSeriesPoints points = thisSeries.Points;
                newSeries.MarkerSize = points.Size;
                newSeries.MarkerStyle = points.Shape;
                newSeries.MarkerColor = points.Color;

                if (thisSeries.ChartType.ToString().Contains("Polar") || thisSeries.ChartType.ToString().Contains("Radar"))
                {
                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable);
                    List<double> y = PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    newSeries.Points.DataBindXY(x, y);
                }
                else
                {
                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable);
                    List<double> y =PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    newSeries.Points.DataBindXY(x,y);
                }
                chartTemp.Series.Add(newSeries);
            }                                   
            //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = True;
            if (chartTemp.Legends.Count > 0)
                chartTemp.Legends[0].Enabled = true;
            /*
            if (chartTemp.Series[0].ChartType.ToString().Contains("Polar") || chartTemp.Series[0].ChartType.ToString().ToUpper().Contains("RADAR"))
            {
                chartTemp.Series[0]["RadarDrawingStyle"] = "Line";
                chartTemp.Series[0].BorderDashStyle = ChartDashStyle.Dash;
                chartTemp.Series[0]["AreaDrawingStyle"] = "Polygon";
                chartTemp.Series[0]["CircularLabelsStyle"] = "Horizontal";
            }
            */
            
            return chartTemp;
        }

        public void BuildRadarChart(RadarChartInfo chartInfo,
            DataSet dataSet, ref Chart radarChart) 
        {
            ChartArea area = new ChartArea();
            area.Name = "ChartArea1";
            area.BorderWidth = 0;
            radarChart.ChartAreas.Add(area);

            for (int counter = 0; counter < chartInfo.SeriesList.Count; counter++)
            {
                RadarChartPageSeries thisSeries = chartInfo.SeriesList[counter];
                Series newSeries = new Series(thisSeries.NameToUse);
                newSeries.ChartType = thisSeries.ChartType;
                newSeries.Name = thisSeries.NameToUse;
                if (thisSeries.Legend != null)
                {
                    Legend legend = new Legend();
                    legend.Docking = thisSeries.Legend.DockingPosition;
                    legend.Alignment = thisSeries.Legend.Alignment;
                    radarChart.Legends.Add(legend);
                }

                newSeries.Color = thisSeries.Color;

                if (thisSeries.XAxisAngle != 0)
                    radarChart.ChartAreas[counter].AxisX.LabelStyle.Angle = thisSeries.XAxisAngle;

                RadarChartPageSeriesPoints points = thisSeries.Points;
                newSeries.MarkerSize = points.Size;
                newSeries.MarkerStyle = points.Shape;
                newSeries.MarkerColor = points.Color;

                if (thisSeries.ChartType.ToString().ToUpper().Contains("RADAR"))
                {
                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable, true);
                    List<double> y = null;
                    if (thisSeries.FinalYValues == null)
                    {
                        y = PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    }
                    else
                    {
                        y = thisSeries.FinalYValues; 
                        //chg from 0 to 8 for higher visibility on chart
                        for (int i=0; i<y.Count; i++)
                        {
                            if (y[i]< 8)
                                y[i] = 8;
                        }                         
                    }

                    //"Enumeration has either not started or has already finished."
                    try
                    {
                        //System.Diagnostics.Debug.Print(newSeries.Name);
                        newSeries.Points.DataBindXY(x, y);
                    }catch(Exception problem)
                    {
                        if (x.Count != y.Count)
                        {
                            string breakPoint = string.Empty;
                        }
                        string msg = problem.Message;
                    }
                }
                else
                {
                    List<string> x = PointsToBindString(thisSeries.XColumnNumber, thisSeries.XColumnDataTable);
                    List<double> y = PointsToBind(thisSeries.YColumnName, thisSeries.YColumnDataTable);
                    newSeries.Points.DataBindXY(x, y);
                }
                if (thisSeries.SeriesRadarBorderDashStyle != ChartDashStyle.NotSet)
                {
                    newSeries["RadarDrawingStyle"] = "Line";
                    newSeries.BorderWidth = 2;  //line width
                    //newSeries.BorderDashStyle = thisSeries.SeriesRadarBorderDashStyle;
                    
                }
                radarChart.Series.Add(newSeries);
            }
            /*
            radarChart.Series[0]["RadarDrawingStyle"] = "Line";
            radarChart.Series[0].BorderDashStyle = ChartDashStyle.Dash;
            radarChart.Series[0]["AreaDrawingStyle"] = "Polygon";
            radarChart.Series[0]["CircularLabelsStyle"] = "Horizontal";
            */
            radarChart.ChartAreas[0].AxisY.MajorGrid.Enabled = false; //removes the target-style circles
            //radarChart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            //radarChart.ChartAreas[0].AxisY.LabelStyle.Format = "C";
            radarChart.ChartAreas[0].AxisY.LabelStyle.Enabled = false;
            
            //no effect on visible part.  radarChart.Height = 800;
            

            //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = True;
            if (radarChart.Legends.Count > 0)
                radarChart.Legends[0].Enabled = true;
        }
        
        public List<double> PointsToBind(string YColumnName, System.Data.DataTable dataTable)
        {
            List<double> result = new List<double>();
            for (int counter = 0; counter < dataTable.Rows.Count; counter++)
            {
                if (dataTable.Rows[counter][YColumnName] != DBNull.Value)
                {
                    result.Add(double.Parse(dataTable.Rows[counter][YColumnName].ToString()));

                }
                else
                {
                    result.Add(0);
                }
            }
            return result;            
        }

        public List<string> PointsToBindString(int columnPosition, System.Data.DataTable dataTable, bool isRadarChart=false)
        {
            List<string> result = new List<string>();
            for (int counter = 0; counter < dataTable.Rows.Count; counter++)
            {
                if (dataTable.Rows[counter][columnPosition] != DBNull.Value)
                {
                    if (!isRadarChart)
                    {
                        result.Add(dataTable.Rows[counter][columnPosition].ToString());
                    }
                    else
                    {
                        //if text too long, it shrinks chart so you can't see it. Fix this later.
                        //result.Add(((string)dataTable.Rows[counter][columnPosition].ToString()).Substring(0, 1));
                        result.Add((string)dataTable.Rows[counter][columnPosition].ToString());
                    }
                }
                else
                {
                    result.Add(string.Empty);
                }
            }
            return result;
        }


        public List<List<double>> HighToLow(List<List<double>> listsIn)
        {
            int quartileListsCount = listsIn.Count;
            List<List<double>> listsOut = new List<List<double>>();
            for(int i=0;i< quartileListsCount;i++)
            {
                listsOut.Add(new List<double>());
            }
            int kpiCount = listsIn[0].Count;
            //loop thru each kpi column of all lists, 
            //collect the #s in a new list
            for (int kpi = 0; kpi < kpiCount; kpi++)
            {
                List<double> lst = new List<double>();
                //loop thru each list
                //for (int i = 0; i < 5; i++)
                for (int i = 0; i < listsIn.Count; i++)
                {
                    lst.Add(listsIn[i][kpi]);
                }
                lst.Sort();
                //add column back in to outgoing lists
                int counter = 0;
                for (int i= listsIn.Count - 1; i >=0; i--)
                {
                    double d = lst[i];
                    listsOut[counter].Add(d);
                    counter++;
                }
            }
            return listsOut;
        }


        //this changes a range of #s to 10-0
        public List<double> QuartileAvgMoYtdZeroTo100(List<double> listIn)
        {
            List<double> listOut = new List<double>();
            int listCount = listIn.Count;
            //make temp list to sort high to low
            List<double> sorts = new List<double>();
            foreach (double d in listIn)
            {
                sorts.Add(d);
            }
            sorts.Sort();  //sorts low to high
            //calc spread between low and high
            double spread = sorts[sorts.Count - 1] - sorts[0];
            //if low < 0, 
            double bumpUp = 0;
            //if (sorts[0] < 0)
            //{

                bumpUp = 0 - sorts[0];
            //}
            //else
            //{
            //    bumpUp = 0 - sorts[0];
            //}
            foreach (double d in listIn)
            {
                //if low < 0,
                //-make new list and add a # to each in list so low is now 0
                //divide each # in list by the spread, add to listOut
                listOut.Add(((d + bumpUp) / spread) * 100);
            }
            return listOut;
        }




    }    

    public class ChartInfo
    {
        public List<ChartPageSeries> SeriesList { get; set; }
    }
    public class ChartPageSeries
    {
        public int SeriesNumber {get;set;}
        public DataTable XColumnDataTable { get; set; }
        public DataTable YColumnDataTable {get;set;}
        public ChartPageLegend Legend {get;set;}
        public SeriesChartType ChartType { get; set; }
        public int XColumnNumber { get; set; }
        public string YColumnName { get; set; }
        public System.Drawing.Color Color { get; set; }
        public int XAxisAngle { get; set; }
        public string NameToUse { get; set; }
        public ChartPageSeriesPoints Points {get;set;}  
    }

    public class ChartPageSeriesPoints
    {
        public MarkerStyle Shape  { get; set; }
        public System.Drawing.Color Color  { get; set; }
        public int Size  { get; set; }
    }
    public class ChartPageLegend
    {
        public Docking DockingPosition {get;set;}
        public System.Drawing.StringAlignment Alignment  {get;set;}
    }    
}
