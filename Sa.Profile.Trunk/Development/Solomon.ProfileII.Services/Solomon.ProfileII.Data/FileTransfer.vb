﻿Imports System.Reflection
Imports System.IO
Imports System.Web.Services.Protocols
Imports System.Configuration

Public Class FileTransfer
    Private _refNum As String = String.Empty
    Private _activityLog As ActivityLog
    Private _db12Conx As String = String.Empty
    Public Sub New(db12Conx As String)
        _db12Conx = db12Conx
        _activityLog = New ActivityLog(_db12Conx)
    End Sub

    Public Function CheckService(userHostAddress As String) As String
        Try 'running test here
            Return "Check Service: Success"
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "CheckService", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Service Error.", New System.Xml.XmlQualifiedName())
        End Try
    End Function


    Public Function DownloadTplFile(fileinfo As String, appVersion As String, userHostAddress As String) As Byte()
        Dim fileStream As System.IO.FileStream = Nothing

        Dim versionDirectories As New List(Of String)
        Dim dirs As New DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory.ToString())
        For Each d In dirs.GetDirectories()
            If d.Name.Contains(".") Then
                versionDirectories.Add(d.Name)
            End If
        Next

        Dim foundDirectory As String = VersionMatcher(appVersion, versionDirectories)

        'Dim filePath As String = AppDomain.CurrentDomain.BaseDirectory.ToString() + appVersion + "\" + fileinfo '+ ".tpl"
        Dim filePath As String = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\" + foundDirectory + "\" + fileinfo '+ ".tpl"

        If Not File.Exists(filePath) Then
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "DownloadTplFile", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), "Can't find tpl file at '" + filePath + "'. ", String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Server error, unable to find one of the files for download.", System.Xml.XmlQualifiedName.Empty)
        End If
        Try
            fileStream = System.IO.File.Open(filePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim byteStream As Byte() = New Byte(fileStream.Length) {}
            fileStream.Read(byteStream, 0, fileStream.Length)
            fileStream.Close()
            Return byteStream
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "DownloadTplFile", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Server error, unable to download one of your files.", System.Xml.XmlQualifiedName.Empty)
        End Try
        _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "GetReferences", String.Empty, Nothing, Nothing, "Success", My.Application.Info.Version.ToString(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
    End Function

    Private Function VersionMatcher(fullAppVersion As String, versions As List(Of String)) As String
        Dim fullVer3 As String = String.Empty
        Dim versionsSort As New List(Of String)
        Dim fullVerParts() As String = fullAppVersion.Split(".")
        For i As Integer = 0 To 2
            fullVer3 = fullVer3 & RightPad3(fullVerParts(i))
        Next
        Try
            '//expect fullAppVersion to be #.#.# format with 1 2, Or 3 numbers in the "#".
            For Each version As String In versions
                Dim verParts As String() = version.Split(".")
                Dim verPadded As String = String.Empty
                For j As Int32 = 0 To 2
                    verPadded = verPadded & RightPad3(verParts(j))
                Next
                versionsSort.Add(verPadded)
            Next
            versionsSort.Sort()
            Dim versionFound As String = String.Empty
            For Each ver As String In versionsSort
                If Int32.Parse(ver) < Int32.Parse(fullVer3) Then
                    versionFound = ver
                ElseIf Int32.Parse(ver) = Int32.Parse(fullVer3) Then
                    versionFound = ver
                    Exit For
                End If
            Next
            Dim versionFoundDepadded As String = String.Empty
            For k As Int16 = 0 To 8 Step 3
                Dim depad As String = versionFound.Substring(k, 3)
                versionFoundDepadded = versionFoundDepadded & Int32.Parse(depad) & "."
            Next
            versionFoundDepadded = versionFoundDepadded.Remove(versionFoundDepadded.Length - 1, 1)
            Return versionFoundDepadded
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function

    'Private String VersionMatcher(String fullAppVersion, List<String> versions)
    '    {
    '        String fullVer3 = String.Empty;
    '        String[] fullVerParts = fullAppVersion.Split('.');
    '        For (int i = 0; i <3; i++)
    '            fullVer3 += RightPad3(fullVerParts[i]);
    '        List<string> versionsSort = New List<string>();
    '        Try
    '        {
    '            //expect fullAppVersion to be #.#.# format with 1 2, Or 3 numbers in the "#".

    '            foreach(string version in versions)
    '            {
    '                String[] verParts = version.Split('.');
    '                String verPadded = String.Empty;
    '                For (int i = 0; i < 3; i++)
    '                    verPadded += RightPad3(verParts[i]);
    '                versionsSort.Add(verPadded);
    '            }
    '            versionsSort.Sort();
    '            String versionFound = String.Empty;
    '            foreach(string ver in versionsSort)
    '            {
    '                If (Int32.Parse(ver) < Int32.Parse(fullVer3))
    '                {
    '                    versionFound = ver;
    '                }
    '                ElseIf (Int32.Parse(fullVer3) == Int32.Parse(ver))
    '                {
    '                    versionFound = ver;
    '                    break;
    '                }
    '            }
    '            String versionFoundDepadded = String.Empty;
    '            For (int i= 0;i<9;i+=3)
    '            {
    '                String depad = versionFound.Substring(i, 3);
    '                versionFoundDepadded +=  Int32.Parse(depad) + ".";
    '            }
    '            versionFoundDepadded = versionFoundDepadded.Remove(versionFoundDepadded.Length - 1,1);
    '            Return versionFoundDepadded;
    '        }
    '        Catch (Exception ex)
    '        {
    '            Return String.Empty;
    '        }
    '    }

    Private Function RightPad3(whatToPad As String) As String
        While whatToPad.Length < 3
            whatToPad = "0" & whatToPad
        End While
        Return whatToPad
    End Function
    '    {
    '        While (whatToPad.Length < 3)
    '        {
    '            whatToPad = "0" + whatToPad;
    '        }
    '        Return whatToPad;
    '    }



    Public Function UploadFile(fileName As String, bytes As Byte(), refnum As String, userHostAddress As String) As String
        Try
            Dim dateStamp As String = refnum & "_" & DateTime.Today.Year.ToString() & "-" & DateTime.Today.Month.ToString.PadLeft(2, "0") & "-" & DateTime.Today.Day.ToString().PadLeft(2, "0")
            Dim folder As String = AppDomain.CurrentDomain.BaseDirectory.ToString() & "Uploads\" & dateStamp
            If Not Directory.Exists(folder) Then
                Directory.CreateDirectory(folder)
            End If
            Using fileStream As New FileStream(folder + "\" + fileName, FileMode.Create)
                fileStream.Write(bytes, 0, bytes.Length)
            End Using
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "UploadFile", String.Empty, Nothing, Nothing, "Error", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw New SoapException("Server error, unable to upload your file.", System.Xml.XmlQualifiedName.Empty)
        End Try
        _activityLog.LogExtended("ProfileII", "", _refNum, userHostAddress, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "UploadFile", String.Empty, Nothing, Nothing, "Success", My.Application.Info.Version.ToString(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
        Return String.Empty
    End Function

    Private Function ValidateKey(clientKey As String, ByRef refNum As String) As Boolean
        'done at svc level
        Return True

        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(clientKey) Then
                company = Decrypt(clientKey).Split("$".ToCharArray)(0)

                locIndex = Decrypt(clientKey).Split("$".ToCharArray).Length - 2
                location = Decrypt(clientKey).Split("$".ToCharArray)(locIndex)
                Dim array As String() = Decrypt(clientKey).Split("$".ToCharArray)
                refNum = array(array.Length - 1) 'last position
                _refNum = refNum

                Dim clientKeysFolder As String = String.Empty
                Try
                    clientKeysFolder = ConfigurationManager.AppSettings("ClientKeysFolder").ToString()
                Catch
                End Try

                If clientKeysFolder.Length > 0 Then
                    If File.Exists(clientKeysFolder & company & "_" & location & ".key") Then
                        path = clientKeysFolder & company & "_" & location & ".key"
                    End If
                Else
                    If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                        path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                    ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                        path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                    Else
                        Return False
                    End If
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = clientKey.Trim)
                End If
            Else
                Return False
            End If
            Return False
        Catch ex As Exception
            _activityLog.LogExtended("ProfileII", "", _refNum, String.Empty, "", Environment.MachineName.ToString(), "Solomon.ProfileII.Data", "ValidateKey", String.Empty, Nothing, Nothing, "Error,", My.Application.Info.Version.ToString(), ex.Message, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
            Throw ex
        End Try
    End Function
End Class

