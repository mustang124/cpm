Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports System.IO


Public Class ReportControl
    Inherits System.Web.UI.UserControl
    'Private Const C_HTTP_HEADER_CONTENT As String = "Content-Disposition"
    'Private Const C_HTTP_ATTACHMENT As String = "attachment;filename="
    'Private Const C_HTTP_INLINE As String = "inline;filename="
    'Private Const C_HTTP_CONTENT_TYPE_OCTET As String = "application/octet-stream"
    'Private Const C_HTTP_CONTENT_TYPE_EXCEL As String = "application/ms-excel"
    'Private Const C_HTTP_CONTENT_LENGTH As String = "Content-Length"
    'Private Const C_QUERY_PARAM_CRITERIA As String = "Criteria"
    'Protected WithEvents C1SubMenu1 As C1.Web.C1Command.C1SubMenu
    'Private Const C_ERROR_NO_RESULT As String = "Data not found."
    'Protected rptData As New DataSet

    Protected WithEvents rptContent As System.Web.UI.WebControls.Literal
    Protected WithEvents ltContent As System.Web.UI.WebControls.Literal

#Region "Properties and Variables"

    Protected _studyYear As Integer
    Protected _refineries As New ArrayList
    Protected _reportName As String
    Protected _startDate As Date
    Protected _company As String
    Protected _unit As String
    Protected _scenario As String
    Protected _currency As String
    Protected _dataset As String
    Protected _page As String
    Protected _total As String
    Protected _current As Boolean
    Protected _includeYTD As Boolean
    Protected _includeAvg As Boolean
    Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Protected WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Protected WithEvents sdCompanyLU As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel
    Protected WithEvents ltPage As System.Web.UI.WebControls.Literal
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents ltTotal As System.Web.UI.WebControls.Literal

   

    Public Property TotalPages() As Integer
        Get
            Return _total
        End Get
        Set(ByVal Value As Integer)
            _total = Value
        End Set
    End Property


    Public Property PageNumber() As Integer
        Get
            Return _page
        End Get
        Set(ByVal Value As Integer)
            _page = Value
        End Set
    End Property

    Public Property Scenario() As String
        Get
            Return _scenario
        End Get
        Set(ByVal Value As String)
            _scenario = Value
        End Set
    End Property

    Public Property Refineries() As ArrayList
        Get
            Return _refineries
        End Get
        Set(ByVal Value As ArrayList)
            _refineries = Value
        End Set
    End Property

    Public Property ReportName()
        Get
            Return _reportName
        End Get
        Set(ByVal Value)
            _reportName = Value
        End Set
    End Property
    Public Property Data()
        Get
            Return _dataset
        End Get
        Set(ByVal Value)
            _dataset = Value
        End Set
    End Property

    Public Property StartDate()
        Get
            Return _startDate
        End Get
        Set(ByVal Value)
            _startDate = Value
        End Set
    End Property


    Public Property Company()
        Get
            Return _company
        End Get
        Set(ByVal Value)
            _company = Value
        End Set
    End Property


    Public Property UnitsOfMeasure()
        Get
            Return _unit
        End Get
        Set(ByVal Value)
            _unit = Value
        End Set
    End Property

    Public Property Currency()
        Get
            Return _currency
        End Get
        Set(ByVal Value)
            _currency = Value
        End Set
    End Property

    Public Property StudyYear()
        Get
            Return _studyYear
        End Get
        Set(ByVal Value)
            _studyYear = Value
        End Set
    End Property
    Public Property CurrentMonth()
        Get
            Return _current
        End Get
        Set(ByVal Value)
            _current = Value
        End Set
    End Property
    Public Property RollingAverage()
        Get
            Return _includeAvg
        End Get
        Set(ByVal Value)
            _includeAvg = Value
        End Set
    End Property

    Public Property YTDAverage()
        Get
            Return _includeYTD
        End Get
        Set(ByVal Value)
            _includeYTD = Value
        End Set
    End Property
#End Region


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.sdCompanyLU = New System.Data.SqlClient.SqlDataAdapter
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT CompanyID, CompanyName, CompanyLogin, CompanyPwd, PwdSaltKey FROM dbo.Comp" & _
        "any_LU"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=UNIT702;packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
        "1.7"";persist security info=False;initial catalog=ProfileFuels"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO dbo.Company_LU(CompanyID, CompanyName, CompanyLogin, CompanyPwd, PwdS" & _
        "altKey) VALUES (@CompanyID, @CompanyName, @CompanyLogin, @CompanyPwd, @PwdSaltKe" & _
        "y); SELECT CompanyID, CompanyName, CompanyLogin, CompanyPwd, PwdSaltKey FROM dbo" & _
        ".Company_LU WHERE (CompanyID = @CompanyID)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.VarChar, 10, "CompanyID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyName", System.Data.SqlDbType.VarChar, 50, "CompanyName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyLogin", System.Data.SqlDbType.VarChar, 50, "CompanyLogin"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyPwd", System.Data.SqlDbType.VarChar, 50, "CompanyPwd"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdSaltKey", System.Data.SqlDbType.VarChar, 50, "PwdSaltKey"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE dbo.Company_LU SET CompanyID = @CompanyID, CompanyName = @CompanyName, Com" & _
        "panyLogin = @CompanyLogin, CompanyPwd = @CompanyPwd, PwdSaltKey = @PwdSaltKey WH" & _
        "ERE (CompanyID = @Original_CompanyID) AND (CompanyLogin = @Original_CompanyLogin" & _
        ") AND (CompanyName = @Original_CompanyName) AND (CompanyPwd = @Original_CompanyP" & _
        "wd) AND (PwdSaltKey = @Original_PwdSaltKey); SELECT CompanyID, CompanyName, Comp" & _
        "anyLogin, CompanyPwd, PwdSaltKey FROM dbo.Company_LU WHERE (CompanyID = @Company" & _
        "ID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.VarChar, 10, "CompanyID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyName", System.Data.SqlDbType.VarChar, 50, "CompanyName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyLogin", System.Data.SqlDbType.VarChar, 50, "CompanyLogin"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompanyPwd", System.Data.SqlDbType.VarChar, 50, "CompanyPwd"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdSaltKey", System.Data.SqlDbType.VarChar, 50, "PwdSaltKey"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyLogin", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyLogin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyPwd", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyPwd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PwdSaltKey", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PwdSaltKey", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM dbo.Company_LU WHERE (CompanyID = @Original_CompanyID) AND (CompanyLo" & _
        "gin = @Original_CompanyLogin) AND (CompanyName = @Original_CompanyName) AND (Com" & _
        "panyPwd = @Original_CompanyPwd) AND (PwdSaltKey = @Original_PwdSaltKey)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyLogin", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyLogin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompanyPwd", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompanyPwd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PwdSaltKey", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PwdSaltKey", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdCompanyLU
        '
        Me.sdCompanyLU.DeleteCommand = Me.SqlDeleteCommand1
        Me.sdCompanyLU.InsertCommand = Me.SqlInsertCommand1
        Me.sdCompanyLU.SelectCommand = Me.SqlSelectCommand1
        Me.sdCompanyLU.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Company_LU", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CompanyID", "CompanyID"), New System.Data.Common.DataColumnMapping("CompanyName", "CompanyName"), New System.Data.Common.DataColumnMapping("CompanyLogin", "CompanyLogin"), New System.Data.Common.DataColumnMapping("CompanyPwd", "CompanyPwd"), New System.Data.Common.DataColumnMapping("PwdSaltKey", "PwdSaltKey")})})
        Me.sdCompanyLU.UpdateCommand = Me.SqlUpdateCommand1

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ' System.Net.ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy
        ltContent.Text = String.Empty
        GetReport()


    End Sub

    Public Sub GetReport()
        BuildRefineryScorecardReport()
    End Sub

#Region "Get Refinery Scorecard Data "
    Public Sub SendDataDump()
        Dim results As New DataSet

        results = CreateRSDataDump(ReportName, Company, UnitsOfMeasure, Currency, StudyYear, Scenario, False, True, True)
        results.DataSetName = "RefineryScrorecard"

        WebExportDatasetToExcel.Export.DataSetToExcel(results)
    End Sub

    Private Function CreateRSDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal studyYear As String, ByVal scenario As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean) As DataSet
        Dim StartSQL As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd, g.DaysInPeriod,g.Currency,g.UOM,EDC,UEDC"
        Dim EndSQL As String = " FROM Tsort t, Gensum g, MaintAvailCalc m, Submissions s WHERE t.CompanyID = '" & Company & _
                                     "' AND t.RefineryID = g.RefineryID AND g.SubmissionID = m.SubmissionID AND s.SubmissionID = m.SubmissionID AND " & _
                                    "g.Scenario='" + scenario + "' AND " + _
                                    "m.FactorSet=g.FactorSet AND g.FactorSet='" + studyYear + _
                                     "' AND g.UOM = '" & UOM & "' AND g.Currency = '" & CurrencyCode & _
                                     "' AND s.CalcsNeeded IS NULL ORDER BY s.PeriodStart DESC,t.Location"

        If Refineries.Count > 0 Then
            Dim refLst As String = "'" + String.Join("','", Refineries.ToArray(GetType(String))) + "'"
            EndSQL = " FROM Tsort t, Gensum g, MaintAvailCalc m, Submissions s WHERE t.RefineryID IN( " & refLst & _
                                                 ") AND t.RefineryID = g.RefineryID AND g.SubmissionID = m.SubmissionID AND s.SubmissionID = m.SubmissionID AND " & _
                                                 "m.FactorSet=g.FactorSet AND g.FactorSet='" + studyYear + _
                                                 "' AND g.UOM = '" & UOM & "' AND g.Currency = '" & CurrencyCode & _
                                                 "' AND s.CalcsNeeded IS NULL ORDER BY s.PeriodStart DESC,t.Location"
        End If


        Dim dsChart_LU = QueryDb("Select * from Chart_LU order by SortKey")

        Dim ds As New DataSet
        ds.DataSetName = ReportName.Replace(" ", "")
        Dim i As Integer
        Dim MiddleSQL As String
        Dim TabName As String
        Dim dvChart_LU As New DataView(dsChart_LU.Tables(0)) 'pull in chart lu order by sort key

        'Average Data
        dvChart_LU.RowFilter = "(SortKey >= 100) AND (SortKey < 200)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim AvgSQL As String = StartSQL & MiddleSQL & EndSQL
        Dim dsTemp As DataSet = QueryDb(AvgSQL)
        Dim AVGdt As DataTable = dsTemp.Tables(0).Copy
        AVGdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(AVGdt)


        'Performance Indicators
        dvChart_LU.RowFilter = "(SortKey >= 200) AND (SortKey < 300)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim PISQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = QueryDb(PISQL)
        Dim PIdt As DataTable = dsTemp.Tables(0).Copy()
        PIdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(PIdt)


        'Energy
        dvChart_LU.RowFilter = "(SortKey >= 300) AND (SortKey < 400)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim ESQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = QueryDb(ESQL)
        Dim Edt As DataTable = dsTemp.Tables(0).Copy()
        Edt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Edt)

        'Maintenance
        dvChart_LU.RowFilter = "(SortKey >= 400) AND (SortKey < 500)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim MSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = QueryDb(MSQL)
        Dim Mdt As DataTable = dsTemp.Tables(0).Copy()
        Mdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Mdt)

        'Operating Expenses
        dvChart_LU.RowFilter = "(SortKey >= 500) AND (SortKey < 600)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim OSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = QueryDb(OSQL)
        Dim Odt As DataTable = dsTemp.Tables(0).Copy()
        Odt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Odt)

        'Personnel
        dvChart_LU.RowFilter = "(SortKey >= 600) AND (SortKey < 700)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim PSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = QueryDb(PSQL)
        Dim Pdt As DataTable = dsTemp.Tables(0).Copy()
        Pdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Pdt)

        'Yields and Margins
        dvChart_LU.RowFilter = "(SortKey >= 700) AND (SortKey < 800)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim YSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = QueryDb(YSQL)
        Dim Ydt As DataTable = dsTemp.Tables(0).Copy()
        Ydt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Ydt)



        Dim userDefinedSQL As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
                    "s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "' AS UOM ," + _
                    "u.* FROM UserDefined u, Submissions s,Tsort t WHERE u.SubmissionID" + _
                    "= s.SubmissionID  and t.CompanyID = '" + Company + "' AND t.RefineryID = s.RefineryID AND " + _
                    "  s.CalcsNeeded IS NULL ORDER BY s.PeriodStart DESC,t.Location"

        If Refineries.Count > 0 Then
            Dim refLst As String = "'" + String.Join("','", Refineries.ToArray(GetType(String))) + "'"
            userDefinedSQL = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
                            "s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "' AS UOM ," + _
                            "u.* FROM UserDefined u, Submissions s,Tsort t WHERE u.SubmissionID" + _
                            "= s.SubmissionID  and t.RefineryID IN (" + refLst + ") AND t.RefineryID = s.RefineryID AND " + _
                            " s.CalcsNeeded IS NULL ORDER BY s.PeriodStart DESC,t.Location"
        End If

        dsTemp = QueryDb(userDefinedSQL)
        Dim dtUserDefined As DataTable = dsTemp.Tables(0).Copy()
        dtUserDefined.TableName = "User_Defined"
        ds.Tables.Add(dtUserDefined)

        Return ds
    End Function

    Private Function CreateMiddleSQL(ByVal dv As DataView, ByVal includeTarget As Boolean, ByVal includeYTD As Boolean, ByVal includeAvg As Boolean) As String
        Dim tmpstr As String
        Dim dbTable As String
        Dim tmpField, tmpCol, tmpAlias As String
        Dim i As Integer
        For i = 0 To dv.Count - 1
            dbTable = dv.Item(i)!DataTable
            If dbTable = "MaintAvailCalc" Then tmpAlias = "m." Else tmpAlias = "g."
            'tmpCol = dv.Item(i)!TargetField
            tmpCol = dv.Item(i)!TotField
            'Select Case tmpCol
            '    Case "EDC", "UEDC"
            '    Case Else
            tmpField = tmpAlias & tmpCol
            'tmpField = tmpField & tmpCol
            'tmpField = tmpField.Replace("_Target", "")
            tmpstr = tmpstr & ", " & tmpField

            'If includeTarget Then tmpstr = tmpstr & ", " & tmpField & "_Target"
            'If includeYTD Then tmpstr = tmpstr & ", " & tmpField & "_YTD"
            'If includeAvg Then tmpstr = tmpstr & ", " & tmpField & "_Avg"
            If includeTarget And dv.Item(i)!TargetField.ToString().Length > 0 Then tmpstr = tmpstr & ", " & tmpAlias & dv.Item(i)!TargetField
            If includeYTD And dv.Item(i)!YTDField.ToString().Length > 0 Then tmpstr = tmpstr & ", " & tmpAlias & dv.Item(i)!YTDField
            If includeAvg And dv.Item(i)!AvgField.ToString().Length > 0 Then tmpstr = tmpstr & ", " & tmpAlias & dv.Item(i)!AvgField
            'End Select
        Next
        Return tmpstr
    End Function


#End Region

#Region "Build Refinery Scorecard "
    Public Sub BuildRefineryScorecardReport()
        Dim strRow, query, UOM, axisFld, text As String
        '  Dim i, y, m, t As Integer
        Dim dsChartLU As DataSet
        Dim stmtIDs, stmtIDCnt As String

        dsChartLU = QueryDb("SELECT *, " + _
                            "CASE(Decplaces)" + _
                            " WHEN 0 then '{0:#,##0}'" + _
                            " WHEN 1 then '{0:#,##0.0}'" + _
                            " WHEN 2 then '{0:N}' " + _
                            " END AS DecFormat " + _
                            " FROM Chart_LU where SortKey < 800 and " + _
                            "SectionHeader <>'By Process Unit' ORDER BY SortKey")

        UOM = UnitsOfMeasure
        If UOM.StartsWith("US") Then
            axisFld = "AxisLabelUS"
        Else
            axisFld = "AxisLabelMetric"
        End If

        'Return submission IDs during that month
        If Refineries.Count = 0 Then
            stmtIDs = "SELECT  s.SubmissionID,t.RefineryID,s.Location FROM Submissions s,TSort t  WHERE  s.DataSet='" + Data + _
                        "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                        Year(StartDate).ToString + _
                        " AND s.RefineryID=t.RefineryID AND t.CompanyID='" + Company + "' AND s.CalcsNeeded IS NULL ORDER BY s.Location"

            stmtIDCnt = "SELECT  Count(*) FROM Submissions s,TSort t WHERE  s.DataSet='" + Data + _
                        "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                        Year(StartDate).ToString + _
                        " AND s.RefineryID=t.RefineryID AND t.CompanyID='" + Company + "' AND s.CalcsNeeded IS NULL"
        Else

            Dim refLst As String = "'" + String.Join("','", Refineries.ToArray(GetType(String))) + "'"
            stmtIDs = "SELECT  s.SubmissionID,t.RefineryID,s.Location FROM Submissions s , TSort t WHERE  s.DataSet='" + Data + _
                        "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                        Year(StartDate).ToString + " AND s.RefineryID=t.RefineryID AND t.RefineryID IN (" + refLst + ")" + _
                       " AND s.CalcsNeeded IS NULL ORDER BY s.Location"

            stmtIDCnt = "SELECT  Count(*) FROM Submissions s, TSort t  WHERE  s.DataSet='" + Data + _
                        "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                        Year(StartDate).ToString + " AND s.RefineryID=t.RefineryID AND t.RefineryID IN (" + refLst + ") AND s.CalcsNeeded IS NULL"
        End If

        'Calcutale pages and total pages 
        Dim totalRecords As Integer = CInt(ExecuteScalar(stmtIDCnt))
        TotalPages = Math.Ceiling(totalRecords / 4)
        Dim doNotShowAll As Boolean = IsNothing(Request.Form("__EVENTTARGET"))
        Dim pgNum As Integer = 0
        Dim max As Integer
        Dim objectCalled As String

        If Not doNotShowAll Then
            objectCalled = Request.Form("__EVENTTARGET")
        End If

       
        If Not IsNothing(Request.QueryString("page")) And doNotShowAll Then
            pgNum = (CInt(Request.QueryString("page")) - 1) * 4
        End If

        Dim dsIDs As DataSet
        'C1Menuitem11 is the excel menu button
            If objectCalled = "C1MenuItem11" Then
                dsIDs = QueryDb(stmtIDs, pgNum, totalRecords, "Submissions")
            Else
                dsIDs = QueryDb(stmtIDs, pgNum, , "Submissions")
            End If


        'Get the number of different tables
        Dim dt As DataTable = SelectDistinct("Chart_LU", dsChartLU.Tables(0), "DataTable")

        Dim rowTableName As DataRow
        For Each rowTableName In dt.Rows

            Dim drs As DataSet
            Dim stmt As String

            'If certain refineries were not selected , then use the first select string
            If Refineries.Count = 0 Then
                stmt = "SELECT * FROM " + rowTableName("DataTable") + " WHERE SubmissionID IN " + _
                              "(SELECT Distinct SubmissionID FROM Submissions s,TSort t WHERE  s.DataSet='" + Data + _
                              "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                              Year(StartDate).ToString + " AND s.RefineryID=t.RefineryID AND t.CompanyID='" + Company + "' AND  s.CalcsNeeded IS NULL ) "
            Else
                Dim refLst As String = "'" + String.Join("','", Refineries.ToArray(GetType(String))) + "'"
                stmt = "SELECT * FROM " + rowTableName("DataTable") + " WHERE SubmissionID IN " + _
                                              "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Data + _
                                              "' AND Month(PeriodStart)=" + Month(StartDate).ToString + " AND Year(PeriodStart)=" + _
                                               Year(StartDate).ToString + " AND RefineryID IN(" + refLst + ") AND CalcsNeeded IS NULL) "
            End If


            If rowTableName("DataTable").ToUpper = "MAINTAVAILCALC" Then
                stmt += " AND FactorSet='" + StudyYear.ToString + "'"
            End If

            If rowTableName("DataTable").ToUpper = "GENSUM" Then
                stmt += "AND UOM='" + UOM + "' AND Currency='" + Currency + "' AND FactorSet='" + StudyYear.ToString + "' AND Scenario='" + Scenario.ToString + "' "
            End If

            drs = QueryDb(stmt)
            drs.Tables(0).TableName = rowTableName("DataTable")
            If Not IsNothing(drs) Then
                dsChartLU.Tables.Add(drs.Tables(0).Copy)
            End If
        Next

        'Print table for each section 
        Dim dtSec As DataTable = SelectDistinct("Chart_LU", dsChartLU.Tables(0), "SectionHeader")
        Dim sectionName As String
        Dim sections() As String = {"Average Data", "Performance Indicators", "Energy", "Maintenance", "Personnel", "Operating Expenses", "Yields and Margins"}

        ltContent.Text += "<table class='small' border=0 bordercolor='darkcyan' width=100% cellspacing=0 cellpadding=2>"

        'Build headers
        ltContent.Text += "<tr><th align=left width=45%>" + Request.QueryString("rptOptions").ToString.ToUpper + "</th>"

        Dim rowRefinery As DataRow
        Dim colwidth As Integer = 8 - dsIDs.Tables(0).Rows.Count * 2 + 2
        Dim sectionwidth = 1 + dsIDs.Tables(0).Rows.Count * 2

        If colwidth <= 0 Then
            colwidth = 2
            sectionwidth = 2
        End If

        For Each rowRefinery In dsIDs.Tables(0).Rows
            ltContent.Text += "<th  align=left width=12% colspan='" + colwidth.ToString().Trim + "'> " + rowRefinery("Location").Trim.ToString.ToUpper() + "</th>"
        Next
        ltContent.Text += "</tr>"
        'Build sections and data
        For Each sectionName In sections
            'insert page break before personnel
            'If sectionName = "Personnel" Then
            '    ltContent.Text += "<P style=""page-break-before: always"">"
            'End If
            ltContent.Text += "<tr><td colspan=" + sectionwidth.ToString + "><strong>" + sectionName + "</strong></td></tr>"

            Dim row As DataRow

            For Each row In dsChartLU.Tables(0).Select("SectionHeader='" + sectionName + "'")

                Dim fieldName As String = row("TotField") 'row("TargetField").ToString.Replace("_Target", "")
                Dim chartTitle As String = row("ChartTitle")
                If row("ChartTitle".Trim).ToString.IndexOf("EDC") = -1 Then
                    chartTitle += ", " & row(axisFld)
                End If

                chartTitle = chartTitle.Replace("CurrencyCode", Currency)

                ltContent.Text += "<tr><td nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + chartTitle + "</td>"

                If fieldName.Length > 0 Then
                    If dsChartLU.Tables(row("DataTable")).Rows.Count > 0 Then
                        Dim fieldFormat As String = row("DecFormat")
                        Dim idRow As DataRow
                        For Each idRow In dsIDs.Tables(0).Rows
                            Dim id As Integer = idRow("SubmissionID")
                            Dim refRow() As DataRow = dsChartLU.Tables(row("DataTable")).Select("SubmissionID='" + id.ToString + "'")

                            If (CurrentMonth) Then
                                If dsChartLU.Tables(row("DataTable")).Columns.Contains((fieldName)) And _
                                   (refRow.Length > 0) Then
                                    ltContent.Text += "<td nowrap align=right colspan=" + (colwidth - 1).ToString() + ">" + String.Format(fieldFormat, refRow(0)(fieldName)) + "</td><td>&nbsp;&nbsp;&nbsp;</td>"
                                Else
                                    ltContent.Text += "<td colspan=" + (colwidth - 1).ToString() + ">&nbsp</td><td>&nbsp</td>"
                                End If
                            End If


                            If (RollingAverage) Then
                                ' If row("ChartTitle".Trim) <> "EDC" And row("ChartTitle".Trim) <> "UEDC" And _
                                If dsChartLU.Tables(row("DataTable")).Columns.Contains((row("AvgField").ToString)) And _
                                 (refRow.Length > 0) Then
                                    ltContent.Text += "<td nowrap align=right colspan=" + (colwidth - 1).ToString() + ">" + String.Format(fieldFormat, refRow(0)(row("AvgField"))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>"
                                Else
                                    ltContent.Text += "<td colspan=" + (colwidth - 1).ToString() + ">&nbsp</td><td>&nbsp</td>"
                                End If
                            End If


                            If (YTDAverage) Then
                                'If row("ChartTitle".Trim) <> "EDC" And row("ChartTitle".Trim) <> "UEDC" And _
                                If dsChartLU.Tables(row("DataTable")).Columns.Contains((row("YTDField").ToString)) And _
                                 (refRow.Length > 0) Then
                                    ltContent.Text += "<td nowrap align=right colspan=" + (colwidth - 1).ToString() + ">" + String.Format(fieldFormat, refRow(0)(row("YTDField"))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>"
                                Else
                                    ltContent.Text += "<td colspan=" + (colwidth - 1).ToString() + ">&nbsp</td><td>&nbsp</td>"
                                End If
                            End If
                        Next
                    Else
                        ltContent.Text += "<td colspan=" + sectionwidth.ToString + ">&nbsp</td>"
                    End If

                    ltContent.Text += "</tr>"

                End If
            Next
            ltContent.Text += "<td colspan=" + sectionwidth.ToString + ">&nbsp</td>"
        Next


        UserDefined(dsIDs)
        ltContent.Text += "</table>"
    End Sub

   
    'Find similar words at the beginning sub heads the second string
    Private Function SubHead(ByVal rt As String, ByVal lt As String) As String
        Dim rArray() As String = rt.Split
        Dim lArray() As String = lt.Split
        Dim u As Integer
        Dim stopAt As Integer = rArray.Length - 1
        Dim foundMatch As Boolean = False

        If lArray.Length - 1 < stopAt Then
            stopAt = lArray.Length - 1
        End If

        For u = 0 To stopAt
            If rArray(u) = lArray(u) Then
                If u > 0 Then
                    If rArray(u - 1) <> lArray(u - 1) Then
                        Exit For
                    End If
                End If
                lt = lt.Replace(rArray(u), String.Empty)
                foundMatch = True
            End If
        Next
        If foundMatch Then
            Return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + _
                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + lt
        Else
            Return lt
        End If

    End Function
#End Region

    Private Sub UserDefined(ByVal dsIDs As DataSet)
        Dim userStmt, headerInfo As String
        Dim drs, dsHead As DataSet
        Dim colwidth As Integer = 8 - dsIDs.Tables(0).Rows.Count * 2 + 2

        If Refineries.Count = 0 Then
            userStmt = "SELECT *, CASE(Decplaces)" + _
                        " WHEN 0 then '{0:#,##0}'" + _
                        " WHEN 1 then '{0:#,##0.0}'" + _
                        " WHEN 2 then '{0:N}' " + _
                        " END AS DecFormat  FROM UserDefined WHERE SubmissionID IN " + _
                       "(SELECT Distinct SubmissionID FROM Submissions s,TSort t WHERE  s.DataSet='" + Data + _
                       "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                       Year(StartDate).ToString + " AND s.RefineryID=t.RefineryID AND t.CompanyID='" + Company + "' AND  s.CalcsNeeded IS NULL ) "

            headerInfo = "SELECT headertext, Count(headertext)/Count(Distinct submissionid) AS HeaderOccurance from UserDefined " + _
                         " WHERE SubmissionID IN " + _
                         "(SELECT Distinct SubmissionID FROM Submissions s,TSort t WHERE  s.DataSet='" + Data + _
                         "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                         Year(StartDate).ToString + " AND s.RefineryID=t.RefineryID AND t.CompanyID='" + Company + _
                         "' AND  s.CalcsNeeded IS NULL ) " + _
                         " GROUP BY headertext,submissionid"
        Else
            Dim refLst As String = "'" + String.Join("','", Refineries.ToArray(GetType(String))) + "'"
            userStmt = "SELECT * , CASE(Decplaces)" + _
                        " WHEN 0 then '{0:#,##0}'" + _
                        " WHEN 1 then '{0:#,##0.0}'" + _
                        " WHEN 2 then '{0:N}' " + _
                        " END AS DecFormat FROM UserDefined WHERE SubmissionID IN " + _
                        "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Data + _
                        "' AND Month(PeriodStart)=" + Month(StartDate).ToString + " AND Year(PeriodStart)=" + _
                        Year(StartDate).ToString + " AND RefineryID IN(" + refLst + ") AND CalcsNeeded IS NULL) "


            headerInfo = "SELECT headertext, Count(headertext)/Count(Distinct submissionid) AS HeaderOccurance from UserDefined " + _
                         " WHERE SubmissionID IN " + _
                         "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Data + _
                         "' AND Month(PeriodStart)=" + Month(StartDate).ToString + " AND Year(PeriodStart)=" + _
                         Year(StartDate).ToString + " AND RefineryID IN(" + refLst + ") AND CalcsNeeded IS NULL) " + _
                         " GROUP BY headertext,submissionid"
        End If


        dsHead = QueryDb(headerInfo)
        drs = QueryDb(userStmt)

        If drs.Tables(0).Rows.Count > 0 Then
            Dim dtSec As DataTable = SelectDistinct(drs.Tables(0).TableName, drs.Tables(0), "HeaderText")
            Dim headRow, idRow As DataRow


            For Each headRow In dtSec.Rows
                Dim header As String = headRow("HeaderText")
                Dim headCnt As Integer = dsHead.Tables(0).Select("HeaderText='" + header + "'")(0)("HeaderOccurance")
                Dim index As Integer = 0
                ltContent.Text += "<tr><td><strong>" + header + "</strong></td>" + _
                            "<td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>" + _
                            "<td >&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>" + _
                            "<td >&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>" + _
                            "<td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr>"

                'If location has more entries in one category, repeat header
                '   For index = 0 To headCnt - 1

                Dim erSql As String = "SELECT distinct VariableDesc FROM UserDefined WHERE HeaderText='" + header + "'"
                If Refineries.Count = 0 Then
                    erSql = "SELECT distinct VariableDesc FROM UserDefined WHERE HeaderText='" + header + "'" + _
                            " AND SubmissionID IN " + _
                            "(SELECT Distinct SubmissionID FROM Submissions s,TSort t WHERE  s.DataSet='" + Data + _
                            "' AND Month(s.PeriodStart)=" + Month(StartDate).ToString + " AND Year(s.PeriodStart)=" + _
                            Year(StartDate).ToString + " AND s.RefineryID=t.RefineryID AND t.CompanyID='" + Company + _
                            "' AND  s.CalcsNeeded IS NULL ) "
                Else
                    Dim refLst As String = "'" + String.Join("','", Refineries.ToArray(GetType(String))) + "'"
                    erSql = "SELECT distinct VariableDesc FROM UserDefined WHERE HeaderText='" + header + "'" + _
                            " AND SubmissionID IN " + _
                            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Data + _
                            "' AND Month(PeriodStart)=" + Month(StartDate).ToString + " AND Year(PeriodStart)=" + _
                            Year(StartDate).ToString + " AND RefineryID IN(" + refLst + ") AND CalcsNeeded IS NULL) "
                End If

                Dim dsVarDesc As DataSet = QueryDb(erSql)
                Dim variRow As DataRow
                For Each variRow In dsVarDesc.Tables(0).Rows

                    Dim variDesc As String = variRow("VariableDesc")
                    'VariableDesc
                    ltContent.Text += "<tr><td nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + variDesc + "</td>"

                    'Handle CurrentMonth , YTD, and Avg base on HeaderText and VariableDesc
                    For Each idRow In dsIDs.Tables(0).Rows
                        Dim id As Integer = idRow("SubmissionID")
                        Dim refRow() As DataRow = drs.Tables(0).Select("SubmissionID='" + id.ToString + "' AND  HeaderText='" + header + "' AND VariableDesc='" + variDesc + "'")
                        Dim fieldFormat As String
                        Dim length As Integer = refRow.Length

                        If index < refRow.Length Then
                            fieldFormat = refRow(index)("DecFormat")
                            variDesc = refRow(index)("VariableDesc")
                        Else
                            fieldFormat = refRow(0)("DecFormat")
                            variDesc = refRow(0)("VariableDesc")
                        End If


                        'VariableDesc
                        'ltContent.Text += "<tr><td nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + variDesc + "</td>"


                        If (CurrentMonth) Then
                            If refRow.Length > 0 And index < refRow.Length Then
                                ltContent.Text += "<td nowrap align=right colspan=" + (colwidth - 1).ToString() + ">" + String.Format(fieldFormat, refRow(index)("RptValue")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>"
                            Else
                                ltContent.Text += "<td colspan=" + (colwidth - 1).ToString() + ">&nbsp</td><td>&nbsp</td>"
                            End If
                        End If

                        If (RollingAverage) Then
                            If refRow.Length > 0 And index < refRow.Length Then
                                ltContent.Text += "<td nowrap align=right colspan=" + (colwidth - 1).ToString() + ">" + String.Format(fieldFormat, refRow(index)("RptValue_Avg")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>"
                            Else
                                ltContent.Text += "<td colspan=" + (colwidth - 1).ToString() + ">&nbsp</td><td>&nbsp</td>"
                            End If
                        End If

                        If (YTDAverage) Then
                            If refRow.Length > 0 And index < refRow.Length Then
                                ltContent.Text += "<td nowrap align=right colspan=" + (colwidth - 1).ToString() + ">" + String.Format(fieldFormat, refRow(index)("RptValue_YTD")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>"
                            Else
                                ltContent.Text += "<td colspan=" + (colwidth - 1).ToString() + ">&nbsp</td><td>&nbsp</td>"
                            End If
                        End If

                    Next 'Next IDs
                    ltContent.Text += "</tr>"
                Next 'Next VariableDesc
                ltContent.Text += "<tr><td colspan=10> &nbsp;</td></tr>"
            Next 'Next HeaderText
        End If



    End Sub



    Private Sub TransmitFile(ByVal [output] As String, ByVal extension As String)
        Dim filename As String = Page.MapPath("data\report." & extension)
        'Dim c1Report As New C1.Web.C1WebReport.C1WebReport
        ' Delete old data for precaution 
        'If System.IO.File.Exists(filename) Then
        'System.IO.File.Delete(filename)
        'End If

        'c1Report.Report.RenderToFile(filename, GetFileType([output]))
        'Response.WriteFile(filename)
        'Dim file As System.IO.FileInfo = New System.IO.FileInfo(filename)

        'Response.Clear()
        ' Reponse.ClearHeaders()
        ' Response.AddHeader(C_HTTP_HEADER_CONTENT, C_HTTP_ATTACHMENT & "report." & extension)
        ' Response.ContentType = "application/octet-stream"
        'Response.AddHeader(C_HTTP_CONTENT_LENGTH, File.Length())
        ' Response.TransmitFile(filename)
        ' Response.Flush()

    End Sub

    'Private Function GetFileType(ByVal [output] As String) As C1.Win.C1Report.FileFormatEnum
    '    Dim ct As C1.Win.C1Report.FileFormatEnum = C1.Win.C1Report.FileFormatEnum.Text
    '    Try
    '        ct = CType([Enum].Parse(GetType(C1.Win.C1Report.FileFormatEnum), [output]), C1.Win.C1Report.FileFormatEnum)
    '    Catch
    '    End Try
    '    Return ct
    'End Function 'getChartType

    'Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    ' C1WebReport1.ShowPDF()
    'End Sub

    'Private Sub WordMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    'Private Sub RTFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("RTF", "rtf")
    'End Sub

    'Private Sub PDFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("PDF", "pdf")
    'End Sub

    'Private Sub ExcelMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub

    'Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub

    'Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
    '    GetReport()
    'End Sub


End Class
