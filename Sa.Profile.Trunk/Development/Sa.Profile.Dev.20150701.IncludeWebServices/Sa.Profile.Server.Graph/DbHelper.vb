Imports System.Data.SqlClient

Module DbHelper

    Const CONNECTIONSTR As String = "packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
        "1.13"";persist security info=True;initial catalog=ProfileFuels;password=ProfileFu" & _
        "els"
    Public Function QueryDb(ByVal sqlString As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim SqlConnection1 As New System.Data.SqlClient.SqlConnection

        '
        'SqlConnection1
        '
        SqlConnection1.ConnectionString = CONNECTIONSTR

        'Reads only the first sql statement. 
        'It's to gaurd against attacks on the data
        Try
            If sqlString.Trim().StartsWith("SELECT") Then
                Dim da As SqlDataAdapter = New SqlDataAdapter(sqlString, SqlConnection1)
                da.Fill(ds)
            End If
        Catch ex As Exception
            Throw New Exception("An database error just occurred." + sqlString)
        Finally
            SqlConnection1.Close()
        End Try

        Return ds

    End Function

End Module
