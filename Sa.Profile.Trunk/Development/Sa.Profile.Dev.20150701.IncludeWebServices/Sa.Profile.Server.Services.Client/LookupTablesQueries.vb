

Public Class LookupTablesQueries
    Inherits DataServiceExtension


#Region "Reference Queries"

    Public Shadows Function GetLookups() As DataSet
        If AppCertificate.IsCertificateInstall() Then

            If Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings("testing")) Then
                Me.Url = "https://webservices.solomononline.com/ReportServicesTestSite/WebServices/DataServices.asmx"
            End If
            Dim ds As DataSet = MyBase.GetLookups()
            Return ds
        Else
            Throw New Exception("Remote service call can not be envoked because Solomon Associates' certificate is not installed on your machine.")
        End If
    End Function

#End Region

#Region "Test Code"
    Public Shared Sub Main()
        Dim service As New LookupTablesQueries
        Dim lookups As DataSet = service.GetLookups()

        Dim y As Integer
        For y = 0 To lookups.Tables.Count - 1
            Console.WriteLine(lookups.Tables(y).TableName + "," + lookups.Tables(y).Rows.Count.ToString)
        Next
    End Sub
#End Region


End Class
