Public Class Table4Report
    Inherits ReportBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        BuildReport()
    End Sub


    Public Sub BuildReport()
        Dim capColName As String = IIf(Request.QueryString("UOM").StartsWith("US"), "Cap", "RptCap")
        Dim processRows() As String = {"", "Salaries & Wages", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Operator,Craft & Clerical", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Management,Professional & Staff", "", _
                                    "Employee Benefits", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Operator,Craft & Clerical", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Management,Professional & Staff", "", _
                                    "Maintenance Materials", "Contract Maintenance Labor Expense", "Other Contract Services", _
                                    "Turnaround Adjustment", "Environmental Expenses", "Other Non-Volume-Related Expenses", _
                                    "Utilized G&A Personnel Cost", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Subtotal Non-Volume-Related Expenses</strong>", "", "Chemicals", _
                                    "Catalysts", "Purchased Energy", "Produced Energy", _
                                    "Purchased Energy Other than Electric and Steam", "Other Volume-Related Expenses", _
                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Subtotal Volume-Related Expenses</strong>", "", _
                                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Total Cash Operating Expenses</strong>"}

        Dim columns() As String = {"", "", "OCCSal", "MPSSal", "", _
                                   "", "OCCBen", "MPSBen", "", _
                                   "MaintMatl", "ContMaintLabor", "OthCont", _
                                   "TAAdj", "Envir", "OthNonVol", _
                                   "GAPers", "STNonVol", "", "Chemicals", _
                                   "Catalysts", "PurchasedEnergy", "ProducedEnergy", _
                                   "PurOth", "OthVol", "STVol", "", "TotCashOpEx"}

        Dim decPlaces() As Integer = {0, 0, 1, 1, 0, _
                                      0, 1, 1, 0, _
                                      1, 1, 1, _
                                      1, 1, 1, _
                                      1, 1, 0, 1, _
                                      1, 1, 1, _
                                      1, 1, 1, 0, 1}
        Dim start As Date = Date.Now

        Dim opexQry As String = "SELECT *,(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS 'PurchasedEnergy', " + _
                      "(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS 'ProducedEnergy' " + _
                       "FROM Opex WHERE DataType='ADJ' AND Currency='@Currency' AND SubmissionID IN " + _
                       "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='@Dataset' AND Month(PeriodStart)=@Month " + _
                       "AND Year(PeriodStart)=@Year " + _
                       "AND RefineryID='@RefineryID')"
        opexQry = opexQry.Replace("@Currency", Request.QueryString("currency").ToString)
        opexQry = opexQry.Replace("@Dataset", Request.QueryString("ds").ToString)
        opexQry = opexQry.Replace("@Month", Month(CDate(Request.QueryString("sd").ToString)).ToString)
        opexQry = opexQry.Replace("@Year", Year(CDate(Request.QueryString("sd").ToString)).ToString)
        opexQry = opexQry.Replace("@RefineryID", RefineryID)


        Dim opexCalcQry As String = "SELECT *,(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS 'PurchasedEnergy', " + _
                      "(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS 'ProducedEnergy' " + _
                       "FROM OpexCalc WHERE DataType IN('C/BBL','UEDC') AND FactorSet=@FactorSet AND Currency='@Currency' AND SubmissionID IN " + _
                       "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='@Dataset' AND Month(PeriodStart)=@Month " + _
                       "AND Year(PeriodStart)=@Year " + _
                       "AND RefineryID='@RefineryID')"
        opexCalcQry = opexCalcQry.Replace("@Currency", Request.QueryString("currency").ToString)
        opexCalcQry = opexCalcQry.Replace("@Dataset", Request.QueryString("ds").ToString)
        opexCalcQry = opexCalcQry.Replace("@Month", Month(CDate(Request.QueryString("sd").ToString)).ToString)
        opexCalcQry = opexCalcQry.Replace("@Year", Year(CDate(Request.QueryString("sd").ToString)).ToString)
        opexCalcQry = opexCalcQry.Replace("@FactorSet", Request.QueryString("yr").ToString)
        opexCalcQry = opexCalcQry.Replace("@RefineryID", RefineryID)


        'Dim opexQry As String
        'opexQry = "SELECT *,(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS 'PurchasedEnergy', " + _
        '                     "(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS 'ProducedEnergy' " + _
        '                      "FROM Opex WHERE DataType='ADJ' AND Currency='@Currency' AND SubmissionID IN " + _
        '                      "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='@Dataset' AND Month(PeriodStart)='" + Month(CDate(Request.QueryString("sd").ToString)).ToString + "' " + _
        '                      "AND Year(PeriodStart)='" + Year(CDate(Request.QueryString("sd").ToString)).ToString + "' " + _
        '                      "AND RefineryID='" + RefineryID + "')"
        'Response.Write(Date.Now + " -" + start)
        'Exit Sub
        Dim ds As DataSet = QueryDb(opexQry)
        Dim ds2 As DataSet = QueryDb(opexCalcQry)
        ds.Merge(ds2)
        Dim fieldFormat As String


        Response.Write("<br>")
        Response.Write(" <table  class='small'  border=0 bordercolor='darkcyan' width=98% cellspacing=0 cellpadding=2>")
        Response.Write("<tr><td valign=bottom width=65%>&nbsp;</td>")
        Response.Write("<td width=45% align=center colspan=6><strong>---------Fuels Refinery Costs---------" + _
                       " </td></tr>")
        Response.Write("<tr><td><strong>" + Format(CDate(Request.QueryString("sd").ToString()), "MMMM yyyy") + "</strong></td>")
        Response.Write("<td nowrap align=center colspan=2>(" + Request.QueryString("currency") + "/1000)</td>")
        Response.Write("<td nowrap  align=center colspan=2>(100 " + Request.QueryString("currency") + "/bbl) </td>")
        Response.Write("<td nowrap  align=center colspan=2>(100 " + Request.QueryString("currency") + "/UEDC) </td></tr>")

        Dim m As Integer
        For m = 0 To processRows.Length - 1

            'Set number format
            Select Case decPlaces(m)
                Case 0
                    fieldFormat = "{0:#,##0}"
                Case 1
                    fieldFormat = "{0:#,##0.0}"
                Case 2
                    fieldFormat = "{0:N}"
            End Select


            Response.Write("<tr>")
            If processRows(m) = "" Then
                Response.Write("<td>&nbsp</td>")
            Else
                'Description column
                Response.Write("<td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + processRows(m) + "&nbsp</td>")
            End If


            If columns(m) <> "" Then
                'Current Month column
                If ds.Tables(0).Columns.Contains(columns(m)) Then
                    Dim dv As DataView = ds.Tables(0).DefaultView
                    dv.RowFilter = "DataType='ADJ'"
                    Response.Write("<td nowrap align=right>" + String.Format(fieldFormat, dv(0)(columns(m))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                    dv.RowFilter = "DataType='C/BBL'"

                    If dv.Count > 0 Then
                        Response.Write("<td nowrap align=right>" + String.Format(fieldFormat, dv(0)(columns(m))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                    Else
                        Response.Write("<td nowrap align=right>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>")
                    End If

                    dv.RowFilter = "DataType='UEDC'"
                    If dv.Count > 0 Then
                        Response.Write("<td nowrap align=right>" + String.Format(fieldFormat, dv(0)(columns(m))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                    Else
                        Response.Write("<td nowrap align=right>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td>")
                    End If
                Else
                    Response.Write("<td colspan=6>&nbsp</td>")
                End If
            Else
                Response.Write("<td colspan=6>&nbsp</td>")
            End If

            Response.Write("</tr>")
        Next


        Response.Write("</table>")
    End Sub
End Class
