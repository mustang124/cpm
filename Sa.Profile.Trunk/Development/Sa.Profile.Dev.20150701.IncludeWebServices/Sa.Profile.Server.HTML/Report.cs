using System;
using System.Data;
using System.Collections;

namespace GenrateInTableHtml
{
	/// <summary>
	/// Summary description for Table4.
	/// </summary>
	public class Report:IReport
	{
		System.Text.StringBuilder builder;
		string reportFileName;
		public Report(string filename)
		{
			//
			// TODO: Add constructor logic here
			//
			builder = new System.Text.StringBuilder();
            reportFileName=filename;
		}

		public string getReport(DataSet ds,string UOM,string currencyCode,Hashtable replacementValues)
		{
			TemplateEngine engine = new TemplateEngine();
			if (UOM.StartsWith("US"))
				engine.UseUSUnits=true;
			else
				engine.UseUSUnits=false;

			string text = engine.RenderOutput(ds,reportFileName,replacementValues);
			return text;

		}

		public string getReport(DataSet ds,string company, string location,string UOM,string currencyCode,int month,int year,Hashtable replacementValues)
		{
			TemplateEngine engine = new TemplateEngine();
			if (UOM.StartsWith("US"))
				engine.UseUSUnits=true;
			else
				engine.UseUSUnits=false;

			string text = engine.RenderOutput(ds,reportFileName,replacementValues);
			return text;
		}
	}
}
