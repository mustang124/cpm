

Imports System.Web.Services
Imports System.Collections
Imports System.Collections.Specialized
Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens

Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Enum RSCRUDE
    RAIL = 100001
    TT = 100002
    TB = 100003
    OMB = 100004
    BB = 100005
    PL = 100006
End Enum


Enum RSPROD
    RAIL = 100010
    TT = 100011
    TT0 = 100012
    TB = 100013
    OMB = 100014
    BB = 100015
    PL = 100016
End Enum

<System.Web.Services.WebService(Namespace:="http://solomononline.com/FuelProfileWebServices/SubmitServices")> _
Public Class SubmitServices
    Inherits System.Web.Services.WebService

    Private classErr As String

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call
    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents sdSubmissions As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdConfig As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdInventory As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdProcessData As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdOpex As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdPers As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdAbsence As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdCrude As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdYield As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdUnitTargets As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdRefTargets As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdMaintRoutHist As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdMaintRout As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdEnergy As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdElectric As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdConfigRS As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdLoadTA As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents scCompleteSubmission As System.Data.SqlClient.SqlCommand
    Friend WithEvents sdMaterialCategory_LU As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsMaintCat1 As FuelProfileWebServices.dsMaintCat
    Friend WithEvents sdLoadRoutHist As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents scStartUpload As System.Data.SqlClient.SqlCommand
    'Friend WithEvents scClearUploading As System.Data.SqlClient.SqlCommand
    Friend WithEvents scClearUploading As System.Data.SqlClient.SqlCommand
    Friend WithEvents scSubmissionID As System.Data.SqlClient.SqlCommand
    Friend WithEvents sdUserDefined As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdOpexAdd As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand21 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand21 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand20 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand20 As System.Data.SqlClient.SqlCommand
    Friend WithEvents sdUnitTargetsNew As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents sdLoadEDCStabilizers As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdOpexAll1 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sdOpexAll2 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents sqlLoadSubmission As System.Data.SqlClient.SqlCommand
    Friend WithEvents scPrepareForUpload As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand20 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand20 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand19 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand19 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand19 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand19 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand18 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand18 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand16 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand16 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand23 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand23 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand22 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand22 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand18 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand18 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand25 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand24 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand17 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand17 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand17 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand17 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand22 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand22 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand21 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand21 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand24 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand24 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand23 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand23 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand25 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand25 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand24 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand25 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand16 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand16 As System.Data.SqlClient.SqlCommand

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.sdSubmissions = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.sdConfig = New System.Data.SqlClient.SqlDataAdapter
        Me.sdInventory = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.sdProcessData = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.sdOpex = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.sdPers = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.sdAbsence = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand
        Me.sdCrude = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand9 = New System.Data.SqlClient.SqlCommand
        Me.sdYield = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand
        Me.sdUnitTargets = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand13 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand14 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand14 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand13 = New System.Data.SqlClient.SqlCommand
        Me.sdRefTargets = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand14 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand15 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand15 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand14 = New System.Data.SqlClient.SqlCommand
        Me.sdMaintRoutHist = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand15 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand16 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand16 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand15 = New System.Data.SqlClient.SqlCommand
        Me.sdLoadTA = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand17 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand17 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand17 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand17 = New System.Data.SqlClient.SqlCommand
        Me.sdMaintRout = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand
        Me.sdEnergy = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand12 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand12 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand11 = New System.Data.SqlClient.SqlCommand
        Me.sdElectric = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand12 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand13 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand13 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand12 = New System.Data.SqlClient.SqlCommand
        Me.sdConfigRS = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.scCompleteSubmission = New System.Data.SqlClient.SqlCommand
        Me.sdMaterialCategory_LU = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand24 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand18 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand18 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand25 = New System.Data.SqlClient.SqlCommand
        Me.DsMaintCat1 = New FuelProfileWebServices.dsMaintCat
        Me.sdLoadRoutHist = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand18 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand19 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand19 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand18 = New System.Data.SqlClient.SqlCommand
        Me.scStartUpload = New System.Data.SqlClient.SqlCommand
        Me.scClearUploading = New System.Data.SqlClient.SqlCommand
        Me.scSubmissionID = New System.Data.SqlClient.SqlCommand
        Me.sdUserDefined = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand19 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand20 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand20 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand19 = New System.Data.SqlClient.SqlCommand
        Me.sdOpexAdd = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand20 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand21 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand21 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand20 = New System.Data.SqlClient.SqlCommand
        Me.sdUnitTargetsNew = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand21 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand22 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand22 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand21 = New System.Data.SqlClient.SqlCommand
        Me.sdLoadEDCStabilizers = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand22 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand23 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand23 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand22 = New System.Data.SqlClient.SqlCommand
        Me.sdOpexAll1 = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand23 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand24 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand24 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand23 = New System.Data.SqlClient.SqlCommand
        Me.sdOpexAll2 = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand25 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand25 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand25 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand24 = New System.Data.SqlClient.SqlCommand
        Me.sqlLoadSubmission = New System.Data.SqlClient.SqlCommand
        Me.scPrepareForUpload = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand16 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand16 = New System.Data.SqlClient.SqlCommand
        CType(Me.DsMaintCat1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "packet size=4096;user id=ProfileFuels;password=ProfileFuels;data source=""10.10.41" & _
        ".13"";persist security info=False;initial catalog=ProfileFuels"
        '
        'sdSubmissions
        '
        Me.sdSubmissions.DeleteCommand = Me.SqlDeleteCommand1
        Me.sdSubmissions.InsertCommand = Me.SqlInsertCommand1
        Me.sdSubmissions.SelectCommand = Me.SqlSelectCommand1
        Me.sdSubmissions.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Submissions", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("RefineryID", "RefineryID"), New System.Data.Common.DataColumnMapping("DataSet", "DataSet"), New System.Data.Common.DataColumnMapping("PeriodStart", "PeriodStart"), New System.Data.Common.DataColumnMapping("PeriodEnd", "PeriodEnd"), New System.Data.Common.DataColumnMapping("PeriodYear", "PeriodYear"), New System.Data.Common.DataColumnMapping("PeriodMonth", "PeriodMonth"), New System.Data.Common.DataColumnMapping("RptCurrency", "RptCurrency"), New System.Data.Common.DataColumnMapping("RptCurrencyT15", "RptCurrencyT15"), New System.Data.Common.DataColumnMapping("UOM", "UOM"), New System.Data.Common.DataColumnMapping("Company", "Company"), New System.Data.Common.DataColumnMapping("Location", "Location"), New System.Data.Common.DataColumnMapping("CoordName", "CoordName"), New System.Data.Common.DataColumnMapping("CoordTitle", "CoordTitle"), New System.Data.Common.DataColumnMapping("CoordPhone", "CoordPhone"), New System.Data.Common.DataColumnMapping("CoordEMail", "CoordEMail"), New System.Data.Common.DataColumnMapping("BridgeVersion", "BridgeVersion"), New System.Data.Common.DataColumnMapping("ClientVersion", "ClientVersion")})})
        Me.sdSubmissions.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM dbo.Submissions WHERE (SubmissionID = @Original_SubmissionID) AND (Br" & _
        "idgeVersion = @Original_BridgeVersion OR @Original_BridgeVersion IS NULL AND Bri" & _
        "dgeVersion IS NULL) AND (ClientVersion = @Original_ClientVersion OR @Original_Cl" & _
        "ientVersion IS NULL AND ClientVersion IS NULL) AND (Company = @Original_Company " & _
        "OR @Original_Company IS NULL AND Company IS NULL) AND (CoordEMail = @Original_Co" & _
        "ordEMail OR @Original_CoordEMail IS NULL AND CoordEMail IS NULL) AND (CoordName " & _
        "= @Original_CoordName OR @Original_CoordName IS NULL AND CoordName IS NULL) AND " & _
        "(CoordPhone = @Original_CoordPhone OR @Original_CoordPhone IS NULL AND CoordPhon" & _
        "e IS NULL) AND (CoordTitle = @Original_CoordTitle OR @Original_CoordTitle IS NUL" & _
        "L AND CoordTitle IS NULL) AND (DataSet = @Original_DataSet) AND (Location = @Ori" & _
        "ginal_Location OR @Original_Location IS NULL AND Location IS NULL) AND (PeriodEn" & _
        "d = @Original_PeriodEnd OR @Original_PeriodEnd IS NULL AND PeriodEnd IS NULL) AN" & _
        "D (PeriodMonth = @Original_PeriodMonth OR @Original_PeriodMonth IS NULL AND Peri" & _
        "odMonth IS NULL) AND (PeriodStart = @Original_PeriodStart OR @Original_PeriodSta" & _
        "rt IS NULL AND PeriodStart IS NULL) AND (PeriodYear = @Original_PeriodYear OR @O" & _
        "riginal_PeriodYear IS NULL AND PeriodYear IS NULL) AND (RefineryID = @Original_R" & _
        "efineryID) AND (RptCurrency = @Original_RptCurrency) AND (RptCurrencyT15 = @Orig" & _
        "inal_RptCurrencyT15 OR @Original_RptCurrencyT15 IS NULL AND RptCurrencyT15 IS NU" & _
        "LL) AND (UOM = @Original_UOM)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BridgeVersion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BridgeVersion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ClientVersion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ClientVersion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Company", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Company", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordEMail", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordEMail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordPhone", System.Data.SqlDbType.VarChar, 40, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordPhone", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordTitle", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordTitle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Location", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Location", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodEnd", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodMonth", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodMonth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodStart", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodStart", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodYear", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodYear", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCurrency", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCurrency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCurrencyT15", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCurrencyT15", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOM", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOM", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO dbo.Submissions(RefineryID, DataSet, PeriodStart, PeriodEnd, PeriodYe" & _
        "ar, PeriodMonth, RptCurrency, RptCurrencyT15, UOM, Company, Location, CoordName," & _
        " CoordTitle, CoordPhone, CoordEMail, BridgeVersion, ClientVersion) VALUES (@Refi" & _
        "neryID, @DataSet, @PeriodStart, @PeriodEnd, @PeriodYear, @PeriodMonth, @RptCurre" & _
        "ncy, @RptCurrencyT15, @UOM, @Company, @Location, @CoordName, @CoordTitle, @Coord" & _
        "Phone, @CoordEMail, @BridgeVersion, @ClientVersion); SELECT SubmissionID, Refine" & _
        "ryID, DataSet, PeriodStart, PeriodEnd, PeriodYear, PeriodMonth, RptCurrency, Rpt" & _
        "CurrencyT15, UOM, Company, Location, CoordName, CoordTitle, CoordPhone, CoordEMa" & _
        "il, BridgeVersion, ClientVersion FROM dbo.Submissions WHERE (SubmissionID = @@ID" & _
        "ENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodStart", System.Data.SqlDbType.DateTime, 4, "PeriodStart"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodEnd", System.Data.SqlDbType.DateTime, 4, "PeriodEnd"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodYear", System.Data.SqlDbType.SmallInt, 2, "PeriodYear"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodMonth", System.Data.SqlDbType.TinyInt, 1, "PeriodMonth"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCurrency", System.Data.SqlDbType.VarChar, 5, "RptCurrency"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCurrencyT15", System.Data.SqlDbType.VarChar, 5, "RptCurrencyT15"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOM", System.Data.SqlDbType.VarChar, 5, "UOM"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Company", System.Data.SqlDbType.VarChar, 30, "Company"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Location", System.Data.SqlDbType.VarChar, 30, "Location"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordName", System.Data.SqlDbType.VarChar, 50, "CoordName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordTitle", System.Data.SqlDbType.VarChar, 50, "CoordTitle"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordPhone", System.Data.SqlDbType.VarChar, 40, "CoordPhone"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordEMail", System.Data.SqlDbType.VarChar, 255, "CoordEMail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BridgeVersion", System.Data.SqlDbType.VarChar, 10, "BridgeVersion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ClientVersion", System.Data.SqlDbType.VarChar, 10, "ClientVersion"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT SubmissionID, RefineryID, DataSet, PeriodStart, PeriodEnd, PeriodYear, Per" & _
        "iodMonth, RptCurrency, RptCurrencyT15, UOM, Company, Location, CoordName, CoordT" & _
        "itle, CoordPhone, CoordEMail, BridgeVersion, ClientVersion FROM dbo.Submissions"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE dbo.Submissions SET RefineryID = @RefineryID, DataSet = @DataSet, PeriodSt" & _
        "art = @PeriodStart, PeriodEnd = @PeriodEnd, PeriodYear = @PeriodYear, PeriodMont" & _
        "h = @PeriodMonth, RptCurrency = @RptCurrency, RptCurrencyT15 = @RptCurrencyT15, " & _
        "UOM = @UOM, Company = @Company, Location = @Location, CoordName = @CoordName, Co" & _
        "ordTitle = @CoordTitle, CoordPhone = @CoordPhone, CoordEMail = @CoordEMail, Brid" & _
        "geVersion = @BridgeVersion, ClientVersion = @ClientVersion WHERE (SubmissionID =" & _
        " @Original_SubmissionID) AND (BridgeVersion = @Original_BridgeVersion OR @Origin" & _
        "al_BridgeVersion IS NULL AND BridgeVersion IS NULL) AND (ClientVersion = @Origin" & _
        "al_ClientVersion OR @Original_ClientVersion IS NULL AND ClientVersion IS NULL) A" & _
        "ND (Company = @Original_Company OR @Original_Company IS NULL AND Company IS NULL" & _
        ") AND (CoordEMail = @Original_CoordEMail OR @Original_CoordEMail IS NULL AND Coo" & _
        "rdEMail IS NULL) AND (CoordName = @Original_CoordName OR @Original_CoordName IS " & _
        "NULL AND CoordName IS NULL) AND (CoordPhone = @Original_CoordPhone OR @Original_" & _
        "CoordPhone IS NULL AND CoordPhone IS NULL) AND (CoordTitle = @Original_CoordTitl" & _
        "e OR @Original_CoordTitle IS NULL AND CoordTitle IS NULL) AND (DataSet = @Origin" & _
        "al_DataSet) AND (Location = @Original_Location OR @Original_Location IS NULL AND" & _
        " Location IS NULL) AND (PeriodEnd = @Original_PeriodEnd OR @Original_PeriodEnd I" & _
        "S NULL AND PeriodEnd IS NULL) AND (PeriodMonth = @Original_PeriodMonth OR @Origi" & _
        "nal_PeriodMonth IS NULL AND PeriodMonth IS NULL) AND (PeriodStart = @Original_Pe" & _
        "riodStart OR @Original_PeriodStart IS NULL AND PeriodStart IS NULL) AND (PeriodY" & _
        "ear = @Original_PeriodYear OR @Original_PeriodYear IS NULL AND PeriodYear IS NUL" & _
        "L) AND (RefineryID = @Original_RefineryID) AND (RptCurrency = @Original_RptCurre" & _
        "ncy) AND (RptCurrencyT15 = @Original_RptCurrencyT15 OR @Original_RptCurrencyT15 " & _
        "IS NULL AND RptCurrencyT15 IS NULL) AND (UOM = @Original_UOM); SELECT Submission" & _
        "ID, RefineryID, DataSet, PeriodStart, PeriodEnd, PeriodYear, PeriodMonth, RptCur" & _
        "rency, RptCurrencyT15, UOM, Company, Location, CoordName, CoordTitle, CoordPhone" & _
        ", CoordEMail, BridgeVersion, ClientVersion FROM dbo.Submissions WHERE (Submissio" & _
        "nID = @SubmissionID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodStart", System.Data.SqlDbType.DateTime, 4, "PeriodStart"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodEnd", System.Data.SqlDbType.DateTime, 4, "PeriodEnd"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodYear", System.Data.SqlDbType.SmallInt, 2, "PeriodYear"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodMonth", System.Data.SqlDbType.TinyInt, 1, "PeriodMonth"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCurrency", System.Data.SqlDbType.VarChar, 5, "RptCurrency"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCurrencyT15", System.Data.SqlDbType.VarChar, 5, "RptCurrencyT15"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOM", System.Data.SqlDbType.VarChar, 5, "UOM"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Company", System.Data.SqlDbType.VarChar, 30, "Company"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Location", System.Data.SqlDbType.VarChar, 30, "Location"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordName", System.Data.SqlDbType.VarChar, 50, "CoordName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordTitle", System.Data.SqlDbType.VarChar, 50, "CoordTitle"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordPhone", System.Data.SqlDbType.VarChar, 40, "CoordPhone"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordEMail", System.Data.SqlDbType.VarChar, 255, "CoordEMail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BridgeVersion", System.Data.SqlDbType.VarChar, 10, "BridgeVersion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ClientVersion", System.Data.SqlDbType.VarChar, 10, "ClientVersion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BridgeVersion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BridgeVersion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ClientVersion", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ClientVersion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Company", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Company", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordEMail", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordEMail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordPhone", System.Data.SqlDbType.VarChar, 40, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordPhone", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CoordTitle", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CoordTitle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Location", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Location", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodEnd", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodMonth", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodMonth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodStart", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodStart", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodYear", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodYear", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCurrency", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCurrency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCurrencyT15", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCurrencyT15", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOM", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        '
        'sdConfig
        '
        Me.sdConfig.DeleteCommand = Me.SqlDeleteCommand16
        Me.sdConfig.InsertCommand = Me.SqlInsertCommand2
        Me.sdConfig.SelectCommand = Me.SqlSelectCommand2
        Me.sdConfig.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Config", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("UnitID", "UnitID"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("ProcessID", "ProcessID"), New System.Data.Common.DataColumnMapping("UnitName", "UnitName"), New System.Data.Common.DataColumnMapping("ProcessType", "ProcessType"), New System.Data.Common.DataColumnMapping("Cap", "Cap"), New System.Data.Common.DataColumnMapping("UtilPcnt", "UtilPcnt"), New System.Data.Common.DataColumnMapping("StmCap", "StmCap"), New System.Data.Common.DataColumnMapping("StmUtilPcnt", "StmUtilPcnt"), New System.Data.Common.DataColumnMapping("InServicePcnt", "InServicePcnt"), New System.Data.Common.DataColumnMapping("YearsOper", "YearsOper"), New System.Data.Common.DataColumnMapping("MHPerWeek", "MHPerWeek"), New System.Data.Common.DataColumnMapping("PostPerShift", "PostPerShift"), New System.Data.Common.DataColumnMapping("BlockOp", "BlockOp"), New System.Data.Common.DataColumnMapping("ControlType", "ControlType"), New System.Data.Common.DataColumnMapping("CapClass", "CapClass"), New System.Data.Common.DataColumnMapping("UtilCap", "UtilCap"), New System.Data.Common.DataColumnMapping("StmUtilCap", "StmUtilCap"), New System.Data.Common.DataColumnMapping("AllocPcntOfTime", "AllocPcntOfTime"), New System.Data.Common.DataColumnMapping("AllocPcntOfCap", "AllocPcntOfCap"), New System.Data.Common.DataColumnMapping("AllocUtilPcnt", "AllocUtilPcnt"), New System.Data.Common.DataColumnMapping("ActualCap", "ActualCap"), New System.Data.Common.DataColumnMapping("ActualStmCap", "ActualStmCap"), New System.Data.Common.DataColumnMapping("InserviceCap", "InserviceCap"), New System.Data.Common.DataColumnMapping("InserviceStmCap", "InserviceStmCap"), New System.Data.Common.DataColumnMapping("ModePcnt", "ModePcnt"), New System.Data.Common.DataColumnMapping("UnusedCapPcnt", "UnusedCapPcnt"), New System.Data.Common.DataColumnMapping("RptCap", "RptCap"), New System.Data.Common.DataColumnMapping("RptCapUOM", "RptCapUOM"), New System.Data.Common.DataColumnMapping("RptStmCap", "RptStmCap"), New System.Data.Common.DataColumnMapping("RptStmCapUOM", "RptStmCapUOM"), New System.Data.Common.DataColumnMapping("DesignFeedSulfur", "DesignFeedSulfur"), New System.Data.Common.DataColumnMapping("EnergyPcnt", "EnergyPcnt")})})
        Me.sdConfig.UpdateCommand = Me.SqlUpdateCommand16
        '
        'sdInventory
        '
        Me.sdInventory.DeleteCommand = Me.SqlDeleteCommand3
        Me.sdInventory.InsertCommand = Me.SqlInsertCommand4
        Me.sdInventory.SelectCommand = Me.SqlSelectCommand4
        Me.sdInventory.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Inventory", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("TankType", "TankType"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("TotStorage", "TotStorage"), New System.Data.Common.DataColumnMapping("MktgStorage", "MktgStorage"), New System.Data.Common.DataColumnMapping("MandStorage", "MandStorage"), New System.Data.Common.DataColumnMapping("FuelsStorage", "FuelsStorage"), New System.Data.Common.DataColumnMapping("NumTank", "NumTank"), New System.Data.Common.DataColumnMapping("LeasedPcnt", "LeasedPcnt"), New System.Data.Common.DataColumnMapping("AvgLevel", "AvgLevel"), New System.Data.Common.DataColumnMapping("MandLevel", "MandLevel"), New System.Data.Common.DataColumnMapping("Inven", "Inven")})})
        Me.sdInventory.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM dbo.Inventory WHERE (SubmissionID = @Original_SubmissionID) AND (Tank" & _
        "Type = @Original_TankType) AND (AvgLevel = @Original_AvgLevel OR @Original_AvgLe" & _
        "vel IS NULL AND AvgLevel IS NULL) AND (FuelsStorage = @Original_FuelsStorage OR " & _
        "@Original_FuelsStorage IS NULL AND FuelsStorage IS NULL) AND (Inven = @Original_" & _
        "Inven OR @Original_Inven IS NULL AND Inven IS NULL) AND (LeasedPcnt = @Original_" & _
        "LeasedPcnt OR @Original_LeasedPcnt IS NULL AND LeasedPcnt IS NULL) AND (MandLeve" & _
        "l = @Original_MandLevel OR @Original_MandLevel IS NULL AND MandLevel IS NULL) AN" & _
        "D (MandStorage = @Original_MandStorage OR @Original_MandStorage IS NULL AND Mand" & _
        "Storage IS NULL) AND (MktgStorage = @Original_MktgStorage OR @Original_MktgStora" & _
        "ge IS NULL AND MktgStorage IS NULL) AND (NumTank = @Original_NumTank OR @Origina" & _
        "l_NumTank IS NULL AND NumTank IS NULL) AND (SortKey = @Original_SortKey) AND (To" & _
        "tStorage = @Original_TotStorage OR @Original_TotStorage IS NULL AND TotStorage I" & _
        "S NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TankType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TankType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgLevel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgLevel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FuelsStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FuelsStorage", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inven", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inven", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LeasedPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LeasedPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MandLevel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MandLevel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MandStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MandStorage", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MktgStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MktgStorage", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumTank", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumTank", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotStorage", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO dbo.Inventory(SubmissionID, TankType, SortKey, TotStorage, MktgStorag" & _
        "e, MandStorage, FuelsStorage, NumTank, LeasedPcnt, AvgLevel, MandLevel, Inven) V" & _
        "ALUES (@SubmissionID, @TankType, @SortKey, @TotStorage, @MktgStorage, @MandStora" & _
        "ge, @FuelsStorage, @NumTank, @LeasedPcnt, @AvgLevel, @MandLevel, @Inven)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection1
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TankType", System.Data.SqlDbType.VarChar, 3, "TankType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.TinyInt, 1, "SortKey"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotStorage", System.Data.SqlDbType.Float, 8, "TotStorage"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MktgStorage", System.Data.SqlDbType.Float, 8, "MktgStorage"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MandStorage", System.Data.SqlDbType.Float, 8, "MandStorage"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FuelsStorage", System.Data.SqlDbType.Float, 8, "FuelsStorage"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumTank", System.Data.SqlDbType.SmallInt, 2, "NumTank"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LeasedPcnt", System.Data.SqlDbType.Real, 4, "LeasedPcnt"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgLevel", System.Data.SqlDbType.Real, 4, "AvgLevel"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MandLevel", System.Data.SqlDbType.Real, 4, "MandLevel"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inven", System.Data.SqlDbType.Real, 4, "Inven"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT SubmissionID, TankType, SortKey, TotStorage, MktgStorage, MandStorage, Fue" & _
        "lsStorage, NumTank, LeasedPcnt, AvgLevel, MandLevel, Inven FROM dbo.Inventory"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE dbo.Inventory SET SubmissionID = @SubmissionID, TankType = @TankType, Sort" & _
        "Key = @SortKey, TotStorage = @TotStorage, MktgStorage = @MktgStorage, MandStorag" & _
        "e = @MandStorage, FuelsStorage = @FuelsStorage, NumTank = @NumTank, LeasedPcnt =" & _
        " @LeasedPcnt, AvgLevel = @AvgLevel, MandLevel = @MandLevel, Inven = @Inven WHERE" & _
        " (SubmissionID = @Original_SubmissionID) AND (TankType = @Original_TankType) AND" & _
        " (AvgLevel = @Original_AvgLevel OR @Original_AvgLevel IS NULL AND AvgLevel IS NU" & _
        "LL) AND (FuelsStorage = @Original_FuelsStorage OR @Original_FuelsStorage IS NULL" & _
        " AND FuelsStorage IS NULL) AND (Inven = @Original_Inven OR @Original_Inven IS NU" & _
        "LL AND Inven IS NULL) AND (LeasedPcnt = @Original_LeasedPcnt OR @Original_Leased" & _
        "Pcnt IS NULL AND LeasedPcnt IS NULL) AND (MandLevel = @Original_MandLevel OR @Or" & _
        "iginal_MandLevel IS NULL AND MandLevel IS NULL) AND (MandStorage = @Original_Man" & _
        "dStorage OR @Original_MandStorage IS NULL AND MandStorage IS NULL) AND (MktgStor" & _
        "age = @Original_MktgStorage OR @Original_MktgStorage IS NULL AND MktgStorage IS " & _
        "NULL) AND (NumTank = @Original_NumTank OR @Original_NumTank IS NULL AND NumTank " & _
        "IS NULL) AND (SortKey = @Original_SortKey) AND (TotStorage = @Original_TotStorag" & _
        "e OR @Original_TotStorage IS NULL AND TotStorage IS NULL)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TankType", System.Data.SqlDbType.VarChar, 3, "TankType"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.TinyInt, 1, "SortKey"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotStorage", System.Data.SqlDbType.Float, 8, "TotStorage"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MktgStorage", System.Data.SqlDbType.Float, 8, "MktgStorage"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MandStorage", System.Data.SqlDbType.Float, 8, "MandStorage"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FuelsStorage", System.Data.SqlDbType.Float, 8, "FuelsStorage"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumTank", System.Data.SqlDbType.SmallInt, 2, "NumTank"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LeasedPcnt", System.Data.SqlDbType.Real, 4, "LeasedPcnt"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgLevel", System.Data.SqlDbType.Real, 4, "AvgLevel"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MandLevel", System.Data.SqlDbType.Real, 4, "MandLevel"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inven", System.Data.SqlDbType.Real, 4, "Inven"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TankType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TankType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgLevel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgLevel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FuelsStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FuelsStorage", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inven", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inven", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LeasedPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LeasedPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MandLevel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MandLevel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MandStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MandStorage", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MktgStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MktgStorage", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumTank", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumTank", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotStorage", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotStorage", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdProcessData
        '
        Me.sdProcessData.DeleteCommand = Me.SqlDeleteCommand4
        Me.sdProcessData.InsertCommand = Me.SqlInsertCommand5
        Me.sdProcessData.SelectCommand = Me.SqlSelectCommand5
        Me.sdProcessData.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ProcessData", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("UnitID", "UnitID"), New System.Data.Common.DataColumnMapping("Property", "Property"), New System.Data.Common.DataColumnMapping("RptValue", "RptValue"), New System.Data.Common.DataColumnMapping("RptUOM", "RptUOM"), New System.Data.Common.DataColumnMapping("SAValue", "SAValue")})})
        Me.sdProcessData.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM dbo.ProcessData WHERE (Property = @Original_Property) AND (Submission" & _
        "ID = @Original_SubmissionID) AND (UnitID = @Original_UnitID) AND (RptUOM = @Orig" & _
        "inal_RptUOM OR @Original_RptUOM IS NULL AND RptUOM IS NULL) AND (RptValue = @Ori" & _
        "ginal_RptValue OR @Original_RptValue IS NULL AND RptValue IS NULL) AND (SAValue " & _
        "= @Original_SAValue OR @Original_SAValue IS NULL AND SAValue IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Property", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Property", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptUOM", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAValue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAValue", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO dbo.ProcessData(SubmissionID, UnitID, Property, RptValue, RptUOM, SAV" & _
        "alue) VALUES (@SubmissionID, @UnitID, @Property, @RptValue, @RptUOM, @SAValue)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection1
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Property", System.Data.SqlDbType.VarChar, 30, "Property"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue", System.Data.SqlDbType.Real, 4, "RptValue"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptUOM", System.Data.SqlDbType.VarChar, 20, "RptUOM"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAValue", System.Data.SqlDbType.Real, 4, "SAValue"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT SubmissionID, UnitID, Property, RptValue, RptUOM, SAValue FROM dbo.Process" & _
        "Data"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE dbo.ProcessData SET SubmissionID = @SubmissionID, UnitID = @UnitID, Proper" & _
        "ty = @Property, RptValue = @RptValue, RptUOM = @RptUOM, SAValue = @SAValue WHERE" & _
        " (Property = @Original_Property) AND (SubmissionID = @Original_SubmissionID) AND" & _
        " (UnitID = @Original_UnitID) AND (RptUOM = @Original_RptUOM OR @Original_RptUOM " & _
        "IS NULL AND RptUOM IS NULL) AND (RptValue = @Original_RptValue OR @Original_RptV" & _
        "alue IS NULL AND RptValue IS NULL) AND (SAValue = @Original_SAValue OR @Original" & _
        "_SAValue IS NULL AND SAValue IS NULL)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Property", System.Data.SqlDbType.VarChar, 30, "Property"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue", System.Data.SqlDbType.Real, 4, "RptValue"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptUOM", System.Data.SqlDbType.VarChar, 20, "RptUOM"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAValue", System.Data.SqlDbType.Real, 4, "SAValue"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Property", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Property", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptUOM", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAValue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAValue", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdOpex
        '
        Me.sdOpex.DeleteCommand = Me.SqlDeleteCommand5
        Me.sdOpex.InsertCommand = Me.SqlInsertCommand6
        Me.sdOpex.SelectCommand = Me.SqlSelectCommand6
        Me.sdOpex.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpEx", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("Currency", "Currency"), New System.Data.Common.DataColumnMapping("Scenario", "Scenario"), New System.Data.Common.DataColumnMapping("DataType", "DataType"), New System.Data.Common.DataColumnMapping("OCCSal", "OCCSal"), New System.Data.Common.DataColumnMapping("MPSSal", "MPSSal"), New System.Data.Common.DataColumnMapping("OCCBen", "OCCBen"), New System.Data.Common.DataColumnMapping("MPSBen", "MPSBen"), New System.Data.Common.DataColumnMapping("MaintMatl", "MaintMatl"), New System.Data.Common.DataColumnMapping("ContMaintLabor", "ContMaintLabor"), New System.Data.Common.DataColumnMapping("ContMaintMatl", "ContMaintMatl"), New System.Data.Common.DataColumnMapping("OthCont", "OthCont"), New System.Data.Common.DataColumnMapping("Equip", "Equip"), New System.Data.Common.DataColumnMapping("Tax", "Tax"), New System.Data.Common.DataColumnMapping("Insur", "Insur"), New System.Data.Common.DataColumnMapping("TAAdj", "TAAdj"), New System.Data.Common.DataColumnMapping("Envir", "Envir"), New System.Data.Common.DataColumnMapping("OthNonVol", "OthNonVol"), New System.Data.Common.DataColumnMapping("GAPers", "GAPers"), New System.Data.Common.DataColumnMapping("STNonVol", "STNonVol"), New System.Data.Common.DataColumnMapping("Antiknock", "Antiknock"), New System.Data.Common.DataColumnMapping("Chemicals", "Chemicals"), New System.Data.Common.DataColumnMapping("Catalysts", "Catalysts"), New System.Data.Common.DataColumnMapping("Royalties", "Royalties"), New System.Data.Common.DataColumnMapping("PurElec", "PurElec"), New System.Data.Common.DataColumnMapping("PurSteam", "PurSteam"), New System.Data.Common.DataColumnMapping("PurOth", "PurOth"), New System.Data.Common.DataColumnMapping("PurFG", "PurFG"), New System.Data.Common.DataColumnMapping("PurLiquid", "PurLiquid"), New System.Data.Common.DataColumnMapping("PurSolid", "PurSolid"), New System.Data.Common.DataColumnMapping("RefProdFG", "RefProdFG"), New System.Data.Common.DataColumnMapping("RefProdOth", "RefProdOth"), New System.Data.Common.DataColumnMapping("EmissionsPurch", "EmissionsPurch"), New System.Data.Common.DataColumnMapping("EmissionsCredits", "EmissionsCredits"), New System.Data.Common.DataColumnMapping("EmissionsTaxes", "EmissionsTaxes"), New System.Data.Common.DataColumnMapping("OthVol", "OthVol"), New System.Data.Common.DataColumnMapping("STVol", "STVol"), New System.Data.Common.DataColumnMapping("TotCashOpEx", "TotCashOpEx"), New System.Data.Common.DataColumnMapping("GANonPers", "GANonPers"), New System.Data.Common.DataColumnMapping("InvenCarry", "InvenCarry"), New System.Data.Common.DataColumnMapping("Depreciation", "Depreciation"), New System.Data.Common.DataColumnMapping("Interest", "Interest"), New System.Data.Common.DataColumnMapping("STNonCash", "STNonCash"), New System.Data.Common.DataColumnMapping("TotRefExp", "TotRefExp"), New System.Data.Common.DataColumnMapping("Cogen", "Cogen"), New System.Data.Common.DataColumnMapping("OthRevenue", "OthRevenue"), New System.Data.Common.DataColumnMapping("ThirdPartyTerminalRM", "ThirdPartyTerminalRM"), New System.Data.Common.DataColumnMapping("ThirdPartyTerminalProd", "ThirdPartyTerminalProd"), New System.Data.Common.DataColumnMapping("POXO2", "POXO2"), New System.Data.Common.DataColumnMapping("PMAA", "PMAA"), New System.Data.Common.DataColumnMapping("FireSafetyLoss", "FireSafetyLoss"), New System.Data.Common.DataColumnMapping("EnvirFines", "EnvirFines"), New System.Data.Common.DataColumnMapping("ExclOth", "ExclOth"), New System.Data.Common.DataColumnMapping("TotExpExcl", "TotExpExcl"), New System.Data.Common.DataColumnMapping("STSal", "STSal"), New System.Data.Common.DataColumnMapping("STBen", "STBen"), New System.Data.Common.DataColumnMapping("PersCostExclTA", "PersCostExclTA"), New System.Data.Common.DataColumnMapping("PersCost", "PersCost"), New System.Data.Common.DataColumnMapping("EnergyCost", "EnergyCost"), New System.Data.Common.DataColumnMapping("NEOpex", "NEOpex")})})
        Me.sdOpex.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM dbo.OpEx WHERE (Currency = @Original_Currency) AND (DataType = @Origi" & _
        "nal_DataType) AND (Scenario = @Original_Scenario) AND (SubmissionID = @Original_" & _
        "SubmissionID) AND (Antiknock = @Original_Antiknock OR @Original_Antiknock IS NUL" & _
        "L AND Antiknock IS NULL) AND (Catalysts = @Original_Catalysts OR @Original_Catal" & _
        "ysts IS NULL AND Catalysts IS NULL) AND (Chemicals = @Original_Chemicals OR @Ori" & _
        "ginal_Chemicals IS NULL AND Chemicals IS NULL) AND (Cogen = @Original_Cogen OR @" & _
        "Original_Cogen IS NULL AND Cogen IS NULL) AND (ContMaintLabor = @Original_ContMa" & _
        "intLabor OR @Original_ContMaintLabor IS NULL AND ContMaintLabor IS NULL) AND (Co" & _
        "ntMaintMatl = @Original_ContMaintMatl OR @Original_ContMaintMatl IS NULL AND Con" & _
        "tMaintMatl IS NULL) AND (Depreciation = @Original_Depreciation OR @Original_Depr" & _
        "eciation IS NULL AND Depreciation IS NULL) AND (EmissionsCredits = @Original_Emi" & _
        "ssionsCredits OR @Original_EmissionsCredits IS NULL AND EmissionsCredits IS NULL" & _
        ") AND (EmissionsPurch = @Original_EmissionsPurch OR @Original_EmissionsPurch IS " & _
        "NULL AND EmissionsPurch IS NULL) AND (EmissionsTaxes = @Original_EmissionsTaxes " & _
        "OR @Original_EmissionsTaxes IS NULL AND EmissionsTaxes IS NULL) AND (EnergyCost " & _
        "= @Original_EnergyCost OR @Original_EnergyCost IS NULL AND EnergyCost IS NULL) A" & _
        "ND (Envir = @Original_Envir OR @Original_Envir IS NULL AND Envir IS NULL) AND (E" & _
        "nvirFines = @Original_EnvirFines OR @Original_EnvirFines IS NULL AND EnvirFines " & _
        "IS NULL) AND (Equip = @Original_Equip OR @Original_Equip IS NULL AND Equip IS NU" & _
        "LL) AND (ExclOth = @Original_ExclOth OR @Original_ExclOth IS NULL AND ExclOth IS" & _
        " NULL) AND (FireSafetyLoss = @Original_FireSafetyLoss OR @Original_FireSafetyLos" & _
        "s IS NULL AND FireSafetyLoss IS NULL) AND (GANonPers = @Original_GANonPers OR @O" & _
        "riginal_GANonPers IS NULL AND GANonPers IS NULL) AND (GAPers = @Original_GAPers " & _
        "OR @Original_GAPers IS NULL AND GAPers IS NULL) AND (Insur = @Original_Insur OR " & _
        "@Original_Insur IS NULL AND Insur IS NULL) AND (Interest = @Original_Interest OR" & _
        " @Original_Interest IS NULL AND Interest IS NULL) AND (InvenCarry = @Original_In" & _
        "venCarry OR @Original_InvenCarry IS NULL AND InvenCarry IS NULL) AND (MPSBen = @" & _
        "Original_MPSBen OR @Original_MPSBen IS NULL AND MPSBen IS NULL) AND (MPSSal = @O" & _
        "riginal_MPSSal OR @Original_MPSSal IS NULL AND MPSSal IS NULL) AND (MaintMatl = " & _
        "@Original_MaintMatl OR @Original_MaintMatl IS NULL AND MaintMatl IS NULL) AND (N" & _
        "EOpex = @Original_NEOpex OR @Original_NEOpex IS NULL AND NEOpex IS NULL) AND (OC" & _
        "CBen = @Original_OCCBen OR @Original_OCCBen IS NULL AND OCCBen IS NULL) AND (OCC" & _
        "Sal = @Original_OCCSal OR @Original_OCCSal IS NULL AND OCCSal IS NULL) AND (OthC" & _
        "ont = @Original_OthCont OR @Original_OthCont IS NULL AND OthCont IS NULL) AND (O" & _
        "thNonVol = @Original_OthNonVol OR @Original_OthNonVol IS NULL AND OthNonVol IS N" & _
        "ULL) AND (OthRevenue = @Original_OthRevenue OR @Original_OthRevenue IS NULL AND " & _
        "OthRevenue IS NULL) AND (OthVol = @Original_OthVol OR @Original_OthVol IS NULL A" & _
        "ND OthVol IS NULL) AND (PMAA = @Original_PMAA OR @Original_PMAA IS NULL AND PMAA" & _
        " IS NULL) AND (POXO2 = @Original_POXO2 OR @Original_POXO2 IS NULL AND POXO2 IS N" & _
        "ULL) AND (PersCost = @Original_PersCost OR @Original_PersCost IS NULL AND PersCo" & _
        "st IS NULL) AND (PersCostExclTA = @Original_PersCostExclTA OR @Original_PersCost" & _
        "ExclTA IS NULL AND PersCostExclTA IS NULL) AND (PurElec = @Original_PurElec OR @" & _
        "Original_PurElec IS NULL AND PurElec IS NULL) AND (PurFG = @Original_PurFG OR @O" & _
        "riginal_PurFG IS NULL AND PurFG IS NULL) AND (PurLiquid = @Original_PurLiquid OR" & _
        " @Original_PurLiquid IS NULL AND PurLiquid IS NULL) AND (PurOth = @Original_PurO" & _
        "th OR @Original_PurOth IS NULL AND PurOth IS NULL) AND (PurSolid = @Original_Pur" & _
        "Solid OR @Original_PurSolid IS NULL AND PurSolid IS NULL) AND (PurSteam = @Origi" & _
        "nal_PurSteam OR @Original_PurSteam IS NULL AND PurSteam IS NULL) AND (RefProdFG " & _
        "= @Original_RefProdFG OR @Original_RefProdFG IS NULL AND RefProdFG IS NULL) AND " & _
        "(RefProdOth = @Original_RefProdOth OR @Original_RefProdOth IS NULL AND RefProdOt" & _
        "h IS NULL) AND (Royalties = @Original_Royalties OR @Original_Royalties IS NULL A" & _
        "ND Royalties IS NULL) AND (STBen = @Original_STBen OR @Original_STBen IS NULL AN" & _
        "D STBen IS NULL) AND (STNonCash = @Original_STNonCash OR @Original_STNonCash IS " & _
        "NULL AND STNonCash IS NULL) AND (STNonVol = @Original_STNonVol OR @Original_STNo" & _
        "nVol IS NULL AND STNonVol IS NULL) AND (STSal = @Original_STSal OR @Original_STS" & _
        "al IS NULL AND STSal IS NULL) AND (STVol = @Original_STVol OR @Original_STVol IS" & _
        " NULL AND STVol IS NULL) AND (TAAdj = @Original_TAAdj OR @Original_TAAdj IS NULL" & _
        " AND TAAdj IS NULL) AND (Tax = @Original_Tax OR @Original_Tax IS NULL AND Tax IS" & _
        " NULL) AND (ThirdPartyTerminalProd = @Original_ThirdPartyTerminalProd OR @Origin" & _
        "al_ThirdPartyTerminalProd IS NULL AND ThirdPartyTerminalProd IS NULL) AND (Third" & _
        "PartyTerminalRM = @Original_ThirdPartyTerminalRM OR @Original_ThirdPartyTerminal" & _
        "RM IS NULL AND ThirdPartyTerminalRM IS NULL) AND (TotCashOpEx = @Original_TotCas" & _
        "hOpEx OR @Original_TotCashOpEx IS NULL AND TotCashOpEx IS NULL) AND (TotExpExcl " & _
        "= @Original_TotExpExcl OR @Original_TotExpExcl IS NULL AND TotExpExcl IS NULL) A" & _
        "ND (TotRefExp = @Original_TotRefExp OR @Original_TotRefExp IS NULL AND TotRefExp" & _
        " IS NULL)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Scenario", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Scenario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Antiknock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Antiknock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Catalysts", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Catalysts", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Chemicals", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Chemicals", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cogen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cogen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintLabor", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintLabor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Depreciation", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Depreciation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsCredits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsCredits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsPurch", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsPurch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsTaxes", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsTaxes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Envir", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Envir", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Equip", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equip", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FireSafetyLoss", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FireSafetyLoss", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GANonPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GANonPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Insur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Insur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Interest", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Interest", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InvenCarry", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InvenCarry", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NEOpex", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NEOpex", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthCont", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthCont", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthRevenue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthRevenue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_POXO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "POXO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCostExclTA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCostExclTA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurElec", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurElec", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurFG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurFG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurLiquid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurLiquid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurSolid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurSolid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurSteam", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurSteam", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefProdFG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefProdFG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefProdOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefProdOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Royalties", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Royalties", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonCash", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonCash", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAAdj", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAAdj", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tax", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalRM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotCashOpEx", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotCashOpEx", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotExpExcl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotExpExcl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotRefExp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotRefExp", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO dbo.OpEx(SubmissionID, Currency, Scenario, DataType, OCCSal, MPSSal, " & _
        "OCCBen, MPSBen, MaintMatl, ContMaintLabor, ContMaintMatl, OthCont, Equip, Tax, I" & _
        "nsur, TAAdj, Envir, OthNonVol, GAPers, STNonVol, Antiknock, Chemicals, Catalysts" & _
        ", Royalties, PurElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, R" & _
        "efProdOth, EmissionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol, TotC" & _
        "ashOpEx, GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Co" & _
        "gen, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, Fire" & _
        "SafetyLoss, EnvirFines, ExclOth, TotExpExcl, STSal, STBen, PersCostExclTA, PersC" & _
        "ost, EnergyCost, NEOpex) VALUES (@SubmissionID, @Currency, @Scenario, @DataType," & _
        " @OCCSal, @MPSSal, @OCCBen, @MPSBen, @MaintMatl, @ContMaintLabor, @ContMaintMatl" & _
        ", @OthCont, @Equip, @Tax, @Insur, @TAAdj, @Envir, @OthNonVol, @GAPers, @STNonVol" & _
        ", @Antiknock, @Chemicals, @Catalysts, @Royalties, @PurElec, @PurSteam, @PurOth, " & _
        "@PurFG, @PurLiquid, @PurSolid, @RefProdFG, @RefProdOth, @EmissionsPurch, @Emissi" & _
        "onsCredits, @EmissionsTaxes, @OthVol, @STVol, @TotCashOpEx, @GANonPers, @InvenCa" & _
        "rry, @Depreciation, @Interest, @STNonCash, @TotRefExp, @Cogen, @OthRevenue, @Thi" & _
        "rdPartyTerminalRM, @ThirdPartyTerminalProd, @POXO2, @PMAA, @FireSafetyLoss, @Env" & _
        "irFines, @ExclOth, @TotExpExcl, @STSal, @STBen, @PersCostExclTA, @PersCost, @Ene" & _
        "rgyCost, @NEOpex)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection1
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Scenario", System.Data.SqlDbType.VarChar, 8, "Scenario"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCSal", System.Data.SqlDbType.Real, 4, "OCCSal"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSSal", System.Data.SqlDbType.Real, 4, "MPSSal"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBen", System.Data.SqlDbType.Real, 4, "OCCBen"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBen", System.Data.SqlDbType.Real, 4, "MPSBen"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatl", System.Data.SqlDbType.Real, 4, "MaintMatl"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintLabor", System.Data.SqlDbType.Real, 4, "ContMaintLabor"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintMatl", System.Data.SqlDbType.Real, 4, "ContMaintMatl"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthCont", System.Data.SqlDbType.Real, 4, "OthCont"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Equip", System.Data.SqlDbType.Real, 4, "Equip"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tax", System.Data.SqlDbType.Real, 4, "Tax"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Insur", System.Data.SqlDbType.Real, 4, "Insur"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAAdj", System.Data.SqlDbType.Real, 4, "TAAdj"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Envir", System.Data.SqlDbType.Real, 4, "Envir"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVol", System.Data.SqlDbType.Real, 4, "OthNonVol"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAPers", System.Data.SqlDbType.Real, 4, "GAPers"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonVol", System.Data.SqlDbType.Real, 4, "STNonVol"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Antiknock", System.Data.SqlDbType.Real, 4, "Antiknock"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Chemicals", System.Data.SqlDbType.Real, 4, "Chemicals"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Catalysts", System.Data.SqlDbType.Real, 4, "Catalysts"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Royalties", System.Data.SqlDbType.Real, 4, "Royalties"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurElec", System.Data.SqlDbType.Real, 4, "PurElec"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurSteam", System.Data.SqlDbType.Real, 4, "PurSteam"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOth", System.Data.SqlDbType.Real, 4, "PurOth"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurFG", System.Data.SqlDbType.Real, 4, "PurFG"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurLiquid", System.Data.SqlDbType.Real, 4, "PurLiquid"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurSolid", System.Data.SqlDbType.Real, 4, "PurSolid"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefProdFG", System.Data.SqlDbType.Real, 4, "RefProdFG"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefProdOth", System.Data.SqlDbType.Real, 4, "RefProdOth"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsPurch", System.Data.SqlDbType.Real, 4, "EmissionsPurch"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsCredits", System.Data.SqlDbType.Real, 4, "EmissionsCredits"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsTaxes", System.Data.SqlDbType.Real, 4, "EmissionsTaxes"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVol", System.Data.SqlDbType.Real, 4, "OthVol"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STVol", System.Data.SqlDbType.Real, 4, "STVol"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotCashOpEx", System.Data.SqlDbType.Real, 4, "TotCashOpEx"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GANonPers", System.Data.SqlDbType.Real, 4, "GANonPers"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InvenCarry", System.Data.SqlDbType.Real, 4, "InvenCarry"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Depreciation", System.Data.SqlDbType.Real, 4, "Depreciation"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Interest", System.Data.SqlDbType.Real, 4, "Interest"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonCash", System.Data.SqlDbType.Real, 4, "STNonCash"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotRefExp", System.Data.SqlDbType.Real, 4, "TotRefExp"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cogen", System.Data.SqlDbType.Real, 4, "Cogen"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthRevenue", System.Data.SqlDbType.Real, 4, "OthRevenue"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalRM"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalProd"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@POXO2", System.Data.SqlDbType.Real, 4, "POXO2"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAA", System.Data.SqlDbType.Real, 4, "PMAA"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FireSafetyLoss", System.Data.SqlDbType.Real, 4, "FireSafetyLoss"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirFines", System.Data.SqlDbType.Real, 4, "EnvirFines"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclOth", System.Data.SqlDbType.Real, 4, "ExclOth"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotExpExcl", System.Data.SqlDbType.Real, 4, "TotExpExcl"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STSal", System.Data.SqlDbType.Real, 4, "STSal"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STBen", System.Data.SqlDbType.Real, 4, "STBen"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCostExclTA", System.Data.SqlDbType.Real, 4, "PersCostExclTA"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCost", System.Data.SqlDbType.Real, 4, "PersCost"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyCost", System.Data.SqlDbType.Real, 4, "EnergyCost"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NEOpex", System.Data.SqlDbType.Real, 4, "NEOpex"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT SubmissionID, Currency, Scenario, DataType, OCCSal, MPSSal, OCCBen, MPSBen" & _
        ", MaintMatl, ContMaintLabor, ContMaintMatl, OthCont, Equip, Tax, Insur, TAAdj, E" & _
        "nvir, OthNonVol, GAPers, STNonVol, Antiknock, Chemicals, Catalysts, Royalties, P" & _
        "urElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth, Emi" & _
        "ssionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol, TotCashOpEx, GANon" & _
        "Pers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, OthRevenu" & _
        "e, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, FireSafetyLoss, En" & _
        "virFines, ExclOth, TotExpExcl, STSal, STBen, PersCostExclTA, PersCost, EnergyCos" & _
        "t, NEOpex FROM dbo.OpEx"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE dbo.OpEx SET SubmissionID = @SubmissionID, Currency = @Currency, Scenario " & _
        "= @Scenario, DataType = @DataType, OCCSal = @OCCSal, MPSSal = @MPSSal, OCCBen = " & _
        "@OCCBen, MPSBen = @MPSBen, MaintMatl = @MaintMatl, ContMaintLabor = @ContMaintLa" & _
        "bor, ContMaintMatl = @ContMaintMatl, OthCont = @OthCont, Equip = @Equip, Tax = @" & _
        "Tax, Insur = @Insur, TAAdj = @TAAdj, Envir = @Envir, OthNonVol = @OthNonVol, GAP" & _
        "ers = @GAPers, STNonVol = @STNonVol, Antiknock = @Antiknock, Chemicals = @Chemic" & _
        "als, Catalysts = @Catalysts, Royalties = @Royalties, PurElec = @PurElec, PurStea" & _
        "m = @PurSteam, PurOth = @PurOth, PurFG = @PurFG, PurLiquid = @PurLiquid, PurSoli" & _
        "d = @PurSolid, RefProdFG = @RefProdFG, RefProdOth = @RefProdOth, EmissionsPurch " & _
        "= @EmissionsPurch, EmissionsCredits = @EmissionsCredits, EmissionsTaxes = @Emiss" & _
        "ionsTaxes, OthVol = @OthVol, STVol = @STVol, TotCashOpEx = @TotCashOpEx, GANonPe" & _
        "rs = @GANonPers, InvenCarry = @InvenCarry, Depreciation = @Depreciation, Interes" & _
        "t = @Interest, STNonCash = @STNonCash, TotRefExp = @TotRefExp, Cogen = @Cogen, O" & _
        "thRevenue = @OthRevenue, ThirdPartyTerminalRM = @ThirdPartyTerminalRM, ThirdPart" & _
        "yTerminalProd = @ThirdPartyTerminalProd, POXO2 = @POXO2, PMAA = @PMAA, FireSafet" & _
        "yLoss = @FireSafetyLoss, EnvirFines = @EnvirFines, ExclOth = @ExclOth, TotExpExc" & _
        "l = @TotExpExcl, STSal = @STSal, STBen = @STBen, PersCostExclTA = @PersCostExclT" & _
        "A, PersCost = @PersCost, EnergyCost = @EnergyCost, NEOpex = @NEOpex WHERE (Curre" & _
        "ncy = @Original_Currency) AND (DataType = @Original_DataType) AND (Scenario = @O" & _
        "riginal_Scenario) AND (SubmissionID = @Original_SubmissionID) AND (Antiknock = @" & _
        "Original_Antiknock OR @Original_Antiknock IS NULL AND Antiknock IS NULL) AND (Ca" & _
        "talysts = @Original_Catalysts OR @Original_Catalysts IS NULL AND Catalysts IS NU" & _
        "LL) AND (Chemicals = @Original_Chemicals OR @Original_Chemicals IS NULL AND Chem" & _
        "icals IS NULL) AND (Cogen = @Original_Cogen OR @Original_Cogen IS NULL AND Cogen" & _
        " IS NULL) AND (ContMaintLabor = @Original_ContMaintLabor OR @Original_ContMaintL" & _
        "abor IS NULL AND ContMaintLabor IS NULL) AND (ContMaintMatl = @Original_ContMain" & _
        "tMatl OR @Original_ContMaintMatl IS NULL AND ContMaintMatl IS NULL) AND (Depreci" & _
        "ation = @Original_Depreciation OR @Original_Depreciation IS NULL AND Depreciatio" & _
        "n IS NULL) AND (EmissionsCredits = @Original_EmissionsCredits OR @Original_Emiss" & _
        "ionsCredits IS NULL AND EmissionsCredits IS NULL) AND (EmissionsPurch = @Origina" & _
        "l_EmissionsPurch OR @Original_EmissionsPurch IS NULL AND EmissionsPurch IS NULL)" & _
        " AND (EmissionsTaxes = @Original_EmissionsTaxes OR @Original_EmissionsTaxes IS N" & _
        "ULL AND EmissionsTaxes IS NULL) AND (EnergyCost = @Original_EnergyCost OR @Origi" & _
        "nal_EnergyCost IS NULL AND EnergyCost IS NULL) AND (Envir = @Original_Envir OR @" & _
        "Original_Envir IS NULL AND Envir IS NULL) AND (EnvirFines = @Original_EnvirFines" & _
        " OR @Original_EnvirFines IS NULL AND EnvirFines IS NULL) AND (Equip = @Original_" & _
        "Equip OR @Original_Equip IS NULL AND Equip IS NULL) AND (ExclOth = @Original_Exc" & _
        "lOth OR @Original_ExclOth IS NULL AND ExclOth IS NULL) AND (FireSafetyLoss = @Or" & _
        "iginal_FireSafetyLoss OR @Original_FireSafetyLoss IS NULL AND FireSafetyLoss IS " & _
        "NULL) AND (GANonPers = @Original_GANonPers OR @Original_GANonPers IS NULL AND GA" & _
        "NonPers IS NULL) AND (GAPers = @Original_GAPers OR @Original_GAPers IS NULL AND " & _
        "GAPers IS NULL) AND (Insur = @Original_Insur OR @Original_Insur IS NULL AND Insu" & _
        "r IS NULL) AND (Interest = @Original_Interest OR @Original_Interest IS NULL AND " & _
        "Interest IS NULL) AND (InvenCarry = @Original_InvenCarry OR @Original_InvenCarry" & _
        " IS NULL AND InvenCarry IS NULL) AND (MPSBen = @Original_MPSBen OR @Original_MPS" & _
        "Ben IS NULL AND MPSBen IS NULL) AND (MPSSal = @Original_MPSSal OR @Original_MPSS" & _
        "al IS NULL AND MPSSal IS NULL) AND (MaintMatl = @Original_MaintMatl OR @Original" & _
        "_MaintMatl IS NULL AND MaintMatl IS NULL) AND (NEOpex = @Original_NEOpex OR @Ori" & _
        "ginal_NEOpex IS NULL AND NEOpex IS NULL) AND (OCCBen = @Original_OCCBen OR @Orig" & _
        "inal_OCCBen IS NULL AND OCCBen IS NULL) AND (OCCSal = @Original_OCCSal OR @Origi" & _
        "nal_OCCSal IS NULL AND OCCSal IS NULL) AND (OthCont = @Original_OthCont OR @Orig" & _
        "inal_OthCont IS NULL AND OthCont IS NULL) AND (OthNonVol = @Original_OthNonVol O" & _
        "R @Original_OthNonVol IS NULL AND OthNonVol IS NULL) AND (OthRevenue = @Original" & _
        "_OthRevenue OR @Original_OthRevenue IS NULL AND OthRevenue IS NULL) AND (OthVol " & _
        "= @Original_OthVol OR @Original_OthVol IS NULL AND OthVol IS NULL) AND (PMAA = @" & _
        "Original_PMAA OR @Original_PMAA IS NULL AND PMAA IS NULL) AND (POXO2 = @Original" & _
        "_POXO2 OR @Original_POXO2 IS NULL AND POXO2 IS NULL) AND (PersCost = @Original_P" & _
        "ersCost OR @Original_PersCost IS NULL AND PersCost IS NULL) AND (PersCostExclTA " & _
        "= @Original_PersCostExclTA OR @Original_PersCostExclTA IS NULL AND PersCostExclT" & _
        "A IS NULL) AND (PurElec = @Original_PurElec OR @Original_PurElec IS NULL AND Pur" & _
        "Elec IS NULL) AND (PurFG = @Original_PurFG OR @Original_PurFG IS NULL AND PurFG " & _
        "IS NULL) AND (PurLiquid = @Original_PurLiquid OR @Original_PurLiquid IS NULL AND" & _
        " PurLiquid IS NULL) AND (PurOth = @Original_PurOth OR @Original_PurOth IS NULL A" & _
        "ND PurOth IS NULL) AND (PurSolid = @Original_PurSolid OR @Original_PurSolid IS N" & _
        "ULL AND PurSolid IS NULL) AND (PurSteam = @Original_PurSteam OR @Original_PurSte" & _
        "am IS NULL AND PurSteam IS NULL) AND (RefProdFG = @Original_RefProdFG OR @Origin" & _
        "al_RefProdFG IS NULL AND RefProdFG IS NULL) AND (RefProdOth = @Original_RefProdO" & _
        "th OR @Original_RefProdOth IS NULL AND RefProdOth IS NULL) AND (Royalties = @Ori" & _
        "ginal_Royalties OR @Original_Royalties IS NULL AND Royalties IS NULL) AND (STBen" & _
        " = @Original_STBen OR @Original_STBen IS NULL AND STBen IS NULL) AND (STNonCash " & _
        "= @Original_STNonCash OR @Original_STNonCash IS NULL AND STNonCash IS NULL) AND " & _
        "(STNonVol = @Original_STNonVol OR @Original_STNonVol IS NULL AND STNonVol IS NUL" & _
        "L) AND (STSal = @Original_STSal OR @Original_STSal IS NULL AND STSal IS NULL) AN" & _
        "D (STVol = @Original_STVol OR @Original_STVol IS NULL AND STVol IS NULL) AND (TA" & _
        "Adj = @Original_TAAdj OR @Original_TAAdj IS NULL AND TAAdj IS NULL) AND (Tax = @" & _
        "Original_Tax OR @Original_Tax IS NULL AND Tax IS NULL) AND (ThirdPartyTerminalPr" & _
        "od = @Original_ThirdPartyTerminalProd OR @Original_ThirdPartyTerminalProd IS NUL" & _
        "L AND ThirdPartyTerminalProd IS NULL) AND (ThirdPartyTerminalRM = @Original_Thir" & _
        "dPartyTerminalRM OR @Original_ThirdPartyTerminalRM IS NULL AND ThirdPartyTermina" & _
        "lRM IS NULL) AND (TotCashOpEx = @Original_TotCashOpEx OR @Original_TotCashOpEx I" & _
        "S NULL AND TotCashOpEx IS NULL) AND (TotExpExcl = @Original_TotExpExcl OR @Origi" & _
        "nal_TotExpExcl IS NULL AND TotExpExcl IS NULL) AND (TotRefExp = @Original_TotRef" & _
        "Exp OR @Original_TotRefExp IS NULL AND TotRefExp IS NULL)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Scenario", System.Data.SqlDbType.VarChar, 8, "Scenario"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCSal", System.Data.SqlDbType.Real, 4, "OCCSal"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSSal", System.Data.SqlDbType.Real, 4, "MPSSal"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBen", System.Data.SqlDbType.Real, 4, "OCCBen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBen", System.Data.SqlDbType.Real, 4, "MPSBen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatl", System.Data.SqlDbType.Real, 4, "MaintMatl"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintLabor", System.Data.SqlDbType.Real, 4, "ContMaintLabor"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintMatl", System.Data.SqlDbType.Real, 4, "ContMaintMatl"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthCont", System.Data.SqlDbType.Real, 4, "OthCont"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Equip", System.Data.SqlDbType.Real, 4, "Equip"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tax", System.Data.SqlDbType.Real, 4, "Tax"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Insur", System.Data.SqlDbType.Real, 4, "Insur"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAAdj", System.Data.SqlDbType.Real, 4, "TAAdj"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Envir", System.Data.SqlDbType.Real, 4, "Envir"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVol", System.Data.SqlDbType.Real, 4, "OthNonVol"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAPers", System.Data.SqlDbType.Real, 4, "GAPers"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonVol", System.Data.SqlDbType.Real, 4, "STNonVol"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Antiknock", System.Data.SqlDbType.Real, 4, "Antiknock"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Chemicals", System.Data.SqlDbType.Real, 4, "Chemicals"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Catalysts", System.Data.SqlDbType.Real, 4, "Catalysts"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Royalties", System.Data.SqlDbType.Real, 4, "Royalties"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurElec", System.Data.SqlDbType.Real, 4, "PurElec"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurSteam", System.Data.SqlDbType.Real, 4, "PurSteam"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOth", System.Data.SqlDbType.Real, 4, "PurOth"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurFG", System.Data.SqlDbType.Real, 4, "PurFG"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurLiquid", System.Data.SqlDbType.Real, 4, "PurLiquid"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurSolid", System.Data.SqlDbType.Real, 4, "PurSolid"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefProdFG", System.Data.SqlDbType.Real, 4, "RefProdFG"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefProdOth", System.Data.SqlDbType.Real, 4, "RefProdOth"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsPurch", System.Data.SqlDbType.Real, 4, "EmissionsPurch"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsCredits", System.Data.SqlDbType.Real, 4, "EmissionsCredits"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsTaxes", System.Data.SqlDbType.Real, 4, "EmissionsTaxes"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVol", System.Data.SqlDbType.Real, 4, "OthVol"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STVol", System.Data.SqlDbType.Real, 4, "STVol"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotCashOpEx", System.Data.SqlDbType.Real, 4, "TotCashOpEx"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GANonPers", System.Data.SqlDbType.Real, 4, "GANonPers"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InvenCarry", System.Data.SqlDbType.Real, 4, "InvenCarry"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Depreciation", System.Data.SqlDbType.Real, 4, "Depreciation"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Interest", System.Data.SqlDbType.Real, 4, "Interest"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonCash", System.Data.SqlDbType.Real, 4, "STNonCash"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotRefExp", System.Data.SqlDbType.Real, 4, "TotRefExp"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cogen", System.Data.SqlDbType.Real, 4, "Cogen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthRevenue", System.Data.SqlDbType.Real, 4, "OthRevenue"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalRM"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalProd"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@POXO2", System.Data.SqlDbType.Real, 4, "POXO2"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAA", System.Data.SqlDbType.Real, 4, "PMAA"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FireSafetyLoss", System.Data.SqlDbType.Real, 4, "FireSafetyLoss"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirFines", System.Data.SqlDbType.Real, 4, "EnvirFines"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclOth", System.Data.SqlDbType.Real, 4, "ExclOth"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotExpExcl", System.Data.SqlDbType.Real, 4, "TotExpExcl"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STSal", System.Data.SqlDbType.Real, 4, "STSal"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STBen", System.Data.SqlDbType.Real, 4, "STBen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCostExclTA", System.Data.SqlDbType.Real, 4, "PersCostExclTA"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCost", System.Data.SqlDbType.Real, 4, "PersCost"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyCost", System.Data.SqlDbType.Real, 4, "EnergyCost"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NEOpex", System.Data.SqlDbType.Real, 4, "NEOpex"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Scenario", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Scenario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Antiknock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Antiknock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Catalysts", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Catalysts", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Chemicals", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Chemicals", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cogen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cogen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintLabor", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintLabor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Depreciation", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Depreciation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsCredits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsCredits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsPurch", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsPurch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsTaxes", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsTaxes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Envir", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Envir", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Equip", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Equip", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FireSafetyLoss", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FireSafetyLoss", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GANonPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GANonPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Insur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Insur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Interest", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Interest", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InvenCarry", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InvenCarry", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NEOpex", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NEOpex", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthCont", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthCont", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthRevenue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthRevenue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_POXO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "POXO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCostExclTA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCostExclTA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurElec", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurElec", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurFG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurFG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurLiquid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurLiquid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurSolid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurSolid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurSteam", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurSteam", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefProdFG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefProdFG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefProdOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefProdOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Royalties", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Royalties", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonCash", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonCash", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAAdj", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAAdj", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tax", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalRM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotCashOpEx", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotCashOpEx", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotExpExcl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotExpExcl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotRefExp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotRefExp", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdPers
        '
        Me.sdPers.DeleteCommand = Me.SqlDeleteCommand6
        Me.sdPers.InsertCommand = Me.SqlInsertCommand7
        Me.sdPers.SelectCommand = Me.SqlSelectCommand7
        Me.sdPers.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Pers", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("PersID", "PersID"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("SectionID", "SectionID"), New System.Data.Common.DataColumnMapping("NumPers", "NumPers"), New System.Data.Common.DataColumnMapping("STH", "STH"), New System.Data.Common.DataColumnMapping("OVTHours", "OVTHours"), New System.Data.Common.DataColumnMapping("OVTPcnt", "OVTPcnt"), New System.Data.Common.DataColumnMapping("Contract", "Contract"), New System.Data.Common.DataColumnMapping("GA", "GA"), New System.Data.Common.DataColumnMapping("AbsHrs", "AbsHrs"), New System.Data.Common.DataColumnMapping("MaintPcnt", "MaintPcnt"), New System.Data.Common.DataColumnMapping("CompEqP", "CompEqP"), New System.Data.Common.DataColumnMapping("ContEqP", "ContEqP"), New System.Data.Common.DataColumnMapping("GAEqP", "GAEqP"), New System.Data.Common.DataColumnMapping("TotEqP", "TotEqP"), New System.Data.Common.DataColumnMapping("CompWHr", "CompWHr"), New System.Data.Common.DataColumnMapping("ContWHr", "ContWHr"), New System.Data.Common.DataColumnMapping("GAWHr", "GAWHr"), New System.Data.Common.DataColumnMapping("TotWHr", "TotWHr")})})
        Me.sdPers.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM dbo.Pers WHERE (PersID = @Original_PersID) AND (SubmissionID = @Origi" & _
        "nal_SubmissionID) AND (AbsHrs = @Original_AbsHrs OR @Original_AbsHrs IS NULL AND" & _
        " AbsHrs IS NULL) AND (CompEqP = @Original_CompEqP OR @Original_CompEqP IS NULL A" & _
        "ND CompEqP IS NULL) AND (CompWHr = @Original_CompWHr OR @Original_CompWHr IS NUL" & _
        "L AND CompWHr IS NULL) AND (ContEqP = @Original_ContEqP OR @Original_ContEqP IS " & _
        "NULL AND ContEqP IS NULL) AND (ContWHr = @Original_ContWHr OR @Original_ContWHr " & _
        "IS NULL AND ContWHr IS NULL) AND (Contract = @Original_Contract OR @Original_Con" & _
        "tract IS NULL AND Contract IS NULL) AND (GA = @Original_GA OR @Original_GA IS NU" & _
        "LL AND GA IS NULL) AND (GAEqP = @Original_GAEqP OR @Original_GAEqP IS NULL AND G" & _
        "AEqP IS NULL) AND (GAWHr = @Original_GAWHr OR @Original_GAWHr IS NULL AND GAWHr " & _
        "IS NULL) AND (MaintPcnt = @Original_MaintPcnt OR @Original_MaintPcnt IS NULL AND" & _
        " MaintPcnt IS NULL) AND (NumPers = @Original_NumPers OR @Original_NumPers IS NUL" & _
        "L AND NumPers IS NULL) AND (OVTHours = @Original_OVTHours OR @Original_OVTHours " & _
        "IS NULL AND OVTHours IS NULL) AND (OVTPcnt = @Original_OVTPcnt OR @Original_OVTP" & _
        "cnt IS NULL AND OVTPcnt IS NULL) AND (STH = @Original_STH OR @Original_STH IS NU" & _
        "LL AND STH IS NULL) AND (SectionID = @Original_SectionID OR @Original_SectionID " & _
        "IS NULL AND SectionID IS NULL) AND (SortKey = @Original_SortKey OR @Original_Sor" & _
        "tKey IS NULL AND SortKey IS NULL) AND (TotEqP = @Original_TotEqP OR @Original_To" & _
        "tEqP IS NULL AND TotEqP IS NULL) AND (TotWHr = @Original_TotWHr OR @Original_Tot" & _
        "WHr IS NULL AND TotWHr IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersID", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AbsHrs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AbsHrs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompWHr", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContWHr", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contract", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contract", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAWHr", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OVTHours", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OVTHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OVTPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OVTPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SectionID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SectionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotWHr", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO dbo.Pers(SubmissionID, PersID, SortKey, SectionID, NumPers, STH, OVTH" & _
        "ours, OVTPcnt, Contract, GA, AbsHrs, MaintPcnt, CompEqP, ContEqP, GAEqP, TotEqP," & _
        " CompWHr, ContWHr, GAWHr, TotWHr) VALUES (@SubmissionID, @PersID, @SortKey, @Sec" & _
        "tionID, @NumPers, @STH, @OVTHours, @OVTPcnt, @Contract, @GA, @AbsHrs, @MaintPcnt" & _
        ", @CompEqP, @ContEqP, @GAEqP, @TotEqP, @CompWHr, @ContWHr, @GAWHr, @TotWHr)"
        Me.SqlInsertCommand7.Connection = Me.SqlConnection1
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersID", System.Data.SqlDbType.VarChar, 10, "PersID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SectionID", System.Data.SqlDbType.VarChar, 2, "SectionID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumPers", System.Data.SqlDbType.Real, 4, "NumPers"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STH", System.Data.SqlDbType.Real, 4, "STH"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OVTHours", System.Data.SqlDbType.Real, 4, "OVTHours"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OVTPcnt", System.Data.SqlDbType.Real, 4, "OVTPcnt"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contract", System.Data.SqlDbType.Real, 4, "Contract"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GA", System.Data.SqlDbType.Real, 4, "GA"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AbsHrs", System.Data.SqlDbType.Real, 4, "AbsHrs"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintPcnt", System.Data.SqlDbType.Real, 4, "MaintPcnt"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompEqP", System.Data.SqlDbType.Real, 4, "CompEqP"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContEqP", System.Data.SqlDbType.Real, 4, "ContEqP"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAEqP", System.Data.SqlDbType.Real, 4, "GAEqP"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotEqP", System.Data.SqlDbType.Real, 4, "TotEqP"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompWHr", System.Data.SqlDbType.Real, 4, "CompWHr"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContWHr", System.Data.SqlDbType.Real, 4, "ContWHr"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAWHr", System.Data.SqlDbType.Real, 4, "GAWHr"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotWHr", System.Data.SqlDbType.Real, 4, "TotWHr"))
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT SubmissionID, PersID, SortKey, SectionID, NumPers, STH, OVTHours, OVTPcnt," & _
        " Contract, GA, AbsHrs, MaintPcnt, CompEqP, ContEqP, GAEqP, TotEqP, CompWHr, Cont" & _
        "WHr, GAWHr, TotWHr FROM dbo.Pers"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE dbo.Pers SET SubmissionID = @SubmissionID, PersID = @PersID, SortKey = @So" & _
        "rtKey, SectionID = @SectionID, NumPers = @NumPers, STH = @STH, OVTHours = @OVTHo" & _
        "urs, OVTPcnt = @OVTPcnt, Contract = @Contract, GA = @GA, AbsHrs = @AbsHrs, Maint" & _
        "Pcnt = @MaintPcnt, CompEqP = @CompEqP, ContEqP = @ContEqP, GAEqP = @GAEqP, TotEq" & _
        "P = @TotEqP, CompWHr = @CompWHr, ContWHr = @ContWHr, GAWHr = @GAWHr, TotWHr = @T" & _
        "otWHr WHERE (PersID = @Original_PersID) AND (SubmissionID = @Original_Submission" & _
        "ID) AND (AbsHrs = @Original_AbsHrs OR @Original_AbsHrs IS NULL AND AbsHrs IS NUL" & _
        "L) AND (CompEqP = @Original_CompEqP OR @Original_CompEqP IS NULL AND CompEqP IS " & _
        "NULL) AND (CompWHr = @Original_CompWHr OR @Original_CompWHr IS NULL AND CompWHr " & _
        "IS NULL) AND (ContEqP = @Original_ContEqP OR @Original_ContEqP IS NULL AND ContE" & _
        "qP IS NULL) AND (ContWHr = @Original_ContWHr OR @Original_ContWHr IS NULL AND Co" & _
        "ntWHr IS NULL) AND (Contract = @Original_Contract OR @Original_Contract IS NULL " & _
        "AND Contract IS NULL) AND (GA = @Original_GA OR @Original_GA IS NULL AND GA IS N" & _
        "ULL) AND (GAEqP = @Original_GAEqP OR @Original_GAEqP IS NULL AND GAEqP IS NULL) " & _
        "AND (GAWHr = @Original_GAWHr OR @Original_GAWHr IS NULL AND GAWHr IS NULL) AND (" & _
        "MaintPcnt = @Original_MaintPcnt OR @Original_MaintPcnt IS NULL AND MaintPcnt IS " & _
        "NULL) AND (NumPers = @Original_NumPers OR @Original_NumPers IS NULL AND NumPers " & _
        "IS NULL) AND (OVTHours = @Original_OVTHours OR @Original_OVTHours IS NULL AND OV" & _
        "THours IS NULL) AND (OVTPcnt = @Original_OVTPcnt OR @Original_OVTPcnt IS NULL AN" & _
        "D OVTPcnt IS NULL) AND (STH = @Original_STH OR @Original_STH IS NULL AND STH IS " & _
        "NULL) AND (SectionID = @Original_SectionID OR @Original_SectionID IS NULL AND Se" & _
        "ctionID IS NULL) AND (SortKey = @Original_SortKey OR @Original_SortKey IS NULL A" & _
        "ND SortKey IS NULL) AND (TotEqP = @Original_TotEqP OR @Original_TotEqP IS NULL A" & _
        "ND TotEqP IS NULL) AND (TotWHr = @Original_TotWHr OR @Original_TotWHr IS NULL AN" & _
        "D TotWHr IS NULL)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersID", System.Data.SqlDbType.VarChar, 10, "PersID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SectionID", System.Data.SqlDbType.VarChar, 2, "SectionID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumPers", System.Data.SqlDbType.Real, 4, "NumPers"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STH", System.Data.SqlDbType.Real, 4, "STH"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OVTHours", System.Data.SqlDbType.Real, 4, "OVTHours"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OVTPcnt", System.Data.SqlDbType.Real, 4, "OVTPcnt"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Contract", System.Data.SqlDbType.Real, 4, "Contract"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GA", System.Data.SqlDbType.Real, 4, "GA"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AbsHrs", System.Data.SqlDbType.Real, 4, "AbsHrs"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintPcnt", System.Data.SqlDbType.Real, 4, "MaintPcnt"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompEqP", System.Data.SqlDbType.Real, 4, "CompEqP"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContEqP", System.Data.SqlDbType.Real, 4, "ContEqP"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAEqP", System.Data.SqlDbType.Real, 4, "GAEqP"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotEqP", System.Data.SqlDbType.Real, 4, "TotEqP"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CompWHr", System.Data.SqlDbType.Real, 4, "CompWHr"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContWHr", System.Data.SqlDbType.Real, 4, "ContWHr"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAWHr", System.Data.SqlDbType.Real, 4, "GAWHr"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotWHr", System.Data.SqlDbType.Real, 4, "TotWHr"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersID", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AbsHrs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AbsHrs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CompWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CompWHr", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContWHr", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Contract", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contract", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAWHr", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OVTHours", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OVTHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OVTPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OVTPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SectionID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SectionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotEqP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotEqP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotWHr", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotWHr", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdAbsence
        '
        Me.sdAbsence.DeleteCommand = Me.SqlDeleteCommand7
        Me.sdAbsence.InsertCommand = Me.SqlInsertCommand8
        Me.sdAbsence.SelectCommand = Me.SqlSelectCommand8
        Me.sdAbsence.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Absence", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("CategoryID", "CategoryID"), New System.Data.Common.DataColumnMapping("OCCAbs", "OCCAbs"), New System.Data.Common.DataColumnMapping("MPSAbs", "MPSAbs"), New System.Data.Common.DataColumnMapping("TotAbs", "TotAbs"), New System.Data.Common.DataColumnMapping("OCCPcnt", "OCCPcnt"), New System.Data.Common.DataColumnMapping("MPSPcnt", "MPSPcnt"), New System.Data.Common.DataColumnMapping("TotPcnt", "TotPcnt")})})
        Me.sdAbsence.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = "DELETE FROM dbo.Absence WHERE (CategoryID = @Original_CategoryID) AND (Submission" & _
        "ID = @Original_SubmissionID) AND (MPSAbs = @Original_MPSAbs OR @Original_MPSAbs " & _
        "IS NULL AND MPSAbs IS NULL) AND (MPSPcnt = @Original_MPSPcnt OR @Original_MPSPcn" & _
        "t IS NULL AND MPSPcnt IS NULL) AND (OCCAbs = @Original_OCCAbs OR @Original_OCCAb" & _
        "s IS NULL AND OCCAbs IS NULL) AND (OCCPcnt = @Original_OCCPcnt OR @Original_OCCP" & _
        "cnt IS NULL AND OCCPcnt IS NULL) AND (SortKey = @Original_SortKey) AND (TotAbs =" & _
        " @Original_TotAbs OR @Original_TotAbs IS NULL AND TotAbs IS NULL) AND (TotPcnt =" & _
        " @Original_TotPcnt OR @Original_TotPcnt IS NULL AND TotPcnt IS NULL)"
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CategoryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CategoryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotPcnt", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO dbo.Absence(SubmissionID, SortKey, CategoryID, OCCAbs, MPSAbs, TotAbs" & _
        ", OCCPcnt, MPSPcnt, TotPcnt) VALUES (@SubmissionID, @SortKey, @CategoryID, @OCCA" & _
        "bs, @MPSAbs, @TotAbs, @OCCPcnt, @MPSPcnt, @TotPcnt)"
        Me.SqlInsertCommand8.Connection = Me.SqlConnection1
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CategoryID", System.Data.SqlDbType.VarChar, 6, "CategoryID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCAbs", System.Data.SqlDbType.Real, 4, "OCCAbs"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSAbs", System.Data.SqlDbType.Real, 4, "MPSAbs"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotAbs", System.Data.SqlDbType.Real, 4, "TotAbs"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCPcnt", System.Data.SqlDbType.Real, 4, "OCCPcnt"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSPcnt", System.Data.SqlDbType.Real, 4, "MPSPcnt"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotPcnt", System.Data.SqlDbType.Real, 4, "TotPcnt"))
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT SubmissionID, SortKey, CategoryID, OCCAbs, MPSAbs, TotAbs, OCCPcnt, MPSPcn" & _
        "t, TotPcnt FROM dbo.Absence"
        Me.SqlSelectCommand8.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = "UPDATE dbo.Absence SET SubmissionID = @SubmissionID, SortKey = @SortKey, Category" & _
        "ID = @CategoryID, OCCAbs = @OCCAbs, MPSAbs = @MPSAbs, TotAbs = @TotAbs, OCCPcnt " & _
        "= @OCCPcnt, MPSPcnt = @MPSPcnt, TotPcnt = @TotPcnt WHERE (CategoryID = @Original" & _
        "_CategoryID) AND (SubmissionID = @Original_SubmissionID) AND (MPSAbs = @Original" & _
        "_MPSAbs OR @Original_MPSAbs IS NULL AND MPSAbs IS NULL) AND (MPSPcnt = @Original" & _
        "_MPSPcnt OR @Original_MPSPcnt IS NULL AND MPSPcnt IS NULL) AND (OCCAbs = @Origin" & _
        "al_OCCAbs OR @Original_OCCAbs IS NULL AND OCCAbs IS NULL) AND (OCCPcnt = @Origin" & _
        "al_OCCPcnt OR @Original_OCCPcnt IS NULL AND OCCPcnt IS NULL) AND (SortKey = @Ori" & _
        "ginal_SortKey) AND (TotAbs = @Original_TotAbs OR @Original_TotAbs IS NULL AND To" & _
        "tAbs IS NULL) AND (TotPcnt = @Original_TotPcnt OR @Original_TotPcnt IS NULL AND " & _
        "TotPcnt IS NULL)"
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CategoryID", System.Data.SqlDbType.VarChar, 6, "CategoryID"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCAbs", System.Data.SqlDbType.Real, 4, "OCCAbs"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSAbs", System.Data.SqlDbType.Real, 4, "MPSAbs"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotAbs", System.Data.SqlDbType.Real, 4, "TotAbs"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCPcnt", System.Data.SqlDbType.Real, 4, "OCCPcnt"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSPcnt", System.Data.SqlDbType.Real, 4, "MPSPcnt"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotPcnt", System.Data.SqlDbType.Real, 4, "TotPcnt"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CategoryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CategoryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotPcnt", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdCrude
        '
        Me.sdCrude.DeleteCommand = Me.SqlDeleteCommand9
        Me.sdCrude.InsertCommand = Me.SqlInsertCommand10
        Me.sdCrude.SelectCommand = Me.SqlSelectCommand10
        Me.sdCrude.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Crude", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("CrudeID", "CrudeID"), New System.Data.Common.DataColumnMapping("CNum", "CNum"), New System.Data.Common.DataColumnMapping("CrudeName", "CrudeName"), New System.Data.Common.DataColumnMapping("BBL", "BBL"), New System.Data.Common.DataColumnMapping("Gravity", "Gravity"), New System.Data.Common.DataColumnMapping("Sulfur", "Sulfur"), New System.Data.Common.DataColumnMapping("CostPerBBL", "CostPerBBL"), New System.Data.Common.DataColumnMapping("Density", "Density"), New System.Data.Common.DataColumnMapping("MT", "MT")})})
        Me.sdCrude.UpdateCommand = Me.SqlUpdateCommand9
        '
        'SqlDeleteCommand9
        '
        Me.SqlDeleteCommand9.CommandText = "DELETE FROM dbo.Crude WHERE (CrudeID = @Original_CrudeID) AND (SubmissionID = @Or" & _
        "iginal_SubmissionID) AND (BBL = @Original_BBL) AND (CNum = @Original_CNum) AND (" & _
        "CostPerBBL = @Original_CostPerBBL) AND (CrudeName = @Original_CrudeName) AND (De" & _
        "nsity = @Original_Density OR @Original_Density IS NULL AND Density IS NULL) AND " & _
        "(Gravity = @Original_Gravity OR @Original_Gravity IS NULL AND Gravity IS NULL) A" & _
        "ND (MT = @Original_MT OR @Original_MT IS NULL AND MT IS NULL) AND (Sulfur = @Ori" & _
        "ginal_Sulfur OR @Original_Sulfur IS NULL AND Sulfur IS NULL)"
        Me.SqlDeleteCommand9.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CrudeID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CrudeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BBL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BBL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CNum", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostPerBBL", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostPerBBL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CrudeName", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CrudeName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Density", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Density", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Gravity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Gravity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Sulfur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Sulfur", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = "INSERT INTO dbo.Crude(SubmissionID, CrudeID, CNum, CrudeName, BBL, Gravity, Sulfu" & _
        "r, CostPerBBL, Density, MT) VALUES (@SubmissionID, @CrudeID, @CNum, @CrudeName, " & _
        "@BBL, @Gravity, @Sulfur, @CostPerBBL, @Density, @MT)"
        Me.SqlInsertCommand10.Connection = Me.SqlConnection1
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CrudeID", System.Data.SqlDbType.SmallInt, 2, "CrudeID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CNum", System.Data.SqlDbType.VarChar, 5, "CNum"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CrudeName", System.Data.SqlDbType.VarChar, 30, "CrudeName"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BBL", System.Data.SqlDbType.Float, 8, "BBL"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Gravity", System.Data.SqlDbType.Real, 4, "Gravity"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Sulfur", System.Data.SqlDbType.Real, 4, "Sulfur"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostPerBBL", System.Data.SqlDbType.Real, 4, "CostPerBBL"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Density", System.Data.SqlDbType.Real, 4, "Density"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MT", System.Data.SqlDbType.Float, 8, "MT"))
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT SubmissionID, CrudeID, CNum, CrudeName, BBL, Gravity, Sulfur, CostPerBBL, " & _
        "Density, MT FROM dbo.Crude"
        Me.SqlSelectCommand10.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand9
        '
        Me.SqlUpdateCommand9.CommandText = "UPDATE dbo.Crude SET SubmissionID = @SubmissionID, CrudeID = @CrudeID, CNum = @CN" & _
        "um, CrudeName = @CrudeName, BBL = @BBL, Gravity = @Gravity, Sulfur = @Sulfur, Co" & _
        "stPerBBL = @CostPerBBL, Density = @Density, MT = @MT WHERE (CrudeID = @Original_" & _
        "CrudeID) AND (SubmissionID = @Original_SubmissionID) AND (BBL = @Original_BBL) A" & _
        "ND (CNum = @Original_CNum) AND (CostPerBBL = @Original_CostPerBBL) AND (CrudeNam" & _
        "e = @Original_CrudeName) AND (Density = @Original_Density OR @Original_Density I" & _
        "S NULL AND Density IS NULL) AND (Gravity = @Original_Gravity OR @Original_Gravit" & _
        "y IS NULL AND Gravity IS NULL) AND (MT = @Original_MT OR @Original_MT IS NULL AN" & _
        "D MT IS NULL) AND (Sulfur = @Original_Sulfur OR @Original_Sulfur IS NULL AND Sul" & _
        "fur IS NULL)"
        Me.SqlUpdateCommand9.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CrudeID", System.Data.SqlDbType.SmallInt, 2, "CrudeID"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CNum", System.Data.SqlDbType.VarChar, 5, "CNum"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CrudeName", System.Data.SqlDbType.VarChar, 30, "CrudeName"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BBL", System.Data.SqlDbType.Float, 8, "BBL"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Gravity", System.Data.SqlDbType.Real, 4, "Gravity"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Sulfur", System.Data.SqlDbType.Real, 4, "Sulfur"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CostPerBBL", System.Data.SqlDbType.Real, 4, "CostPerBBL"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Density", System.Data.SqlDbType.Real, 4, "Density"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MT", System.Data.SqlDbType.Float, 8, "MT"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CrudeID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CrudeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BBL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BBL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CNum", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CostPerBBL", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CostPerBBL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CrudeName", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CrudeName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Density", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Density", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Gravity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Gravity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Sulfur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Sulfur", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdYield
        '
        Me.sdYield.DeleteCommand = Me.SqlDeleteCommand10
        Me.sdYield.InsertCommand = Me.SqlInsertCommand11
        Me.sdYield.SelectCommand = Me.SqlSelectCommand11
        Me.sdYield.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Yield", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("Category", "Category"), New System.Data.Common.DataColumnMapping("MaterialID", "MaterialID"), New System.Data.Common.DataColumnMapping("MaterialName", "MaterialName"), New System.Data.Common.DataColumnMapping("BBL", "BBL"), New System.Data.Common.DataColumnMapping("Gravity", "Gravity"), New System.Data.Common.DataColumnMapping("Density", "Density"), New System.Data.Common.DataColumnMapping("MT", "MT"), New System.Data.Common.DataColumnMapping("PriceLocal", "PriceLocal"), New System.Data.Common.DataColumnMapping("PriceUS", "PriceUS")})})
        Me.sdYield.UpdateCommand = Me.SqlUpdateCommand10
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = "DELETE FROM dbo.Yield WHERE (SortKey = @Original_SortKey) AND (SubmissionID = @Or" & _
        "iginal_SubmissionID) AND (BBL = @Original_BBL) AND (Category = @Original_Categor" & _
        "y) AND (Density = @Original_Density OR @Original_Density IS NULL AND Density IS " & _
        "NULL) AND (Gravity = @Original_Gravity OR @Original_Gravity IS NULL AND Gravity " & _
        "IS NULL) AND (MT = @Original_MT OR @Original_MT IS NULL AND MT IS NULL) AND (Mat" & _
        "erialID = @Original_MaterialID) AND (MaterialName = @Original_MaterialName) AND " & _
        "(PriceLocal = @Original_PriceLocal) AND (PriceUS = @Original_PriceUS OR @Origina" & _
        "l_PriceUS IS NULL AND PriceUS IS NULL)"
        Me.SqlDeleteCommand10.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BBL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BBL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Category", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Category", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Density", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Density", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Gravity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Gravity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaterialID", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaterialID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaterialName", System.Data.SqlDbType.VarChar, 35, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaterialName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceUS", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand11
        '
        Me.SqlInsertCommand11.CommandText = "INSERT INTO dbo.Yield(SubmissionID, SortKey, Category, MaterialID, MaterialName, " & _
        "BBL, Gravity, Density, MT, PriceLocal, PriceUS) VALUES (@SubmissionID, @SortKey," & _
        " @Category, @MaterialID, @MaterialName, @BBL, @Gravity, @Density, @MT, @PriceLoc" & _
        "al, @PriceUS)"
        Me.SqlInsertCommand11.Connection = Me.SqlConnection1
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.Int, 4, "SortKey"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Category", System.Data.SqlDbType.VarChar, 5, "Category"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaterialID", System.Data.SqlDbType.VarChar, 5, "MaterialID"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaterialName", System.Data.SqlDbType.VarChar, 35, "MaterialName"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BBL", System.Data.SqlDbType.Float, 8, "BBL"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Gravity", System.Data.SqlDbType.Real, 4, "Gravity"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Density", System.Data.SqlDbType.Real, 4, "Density"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MT", System.Data.SqlDbType.Float, 8, "MT"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceLocal", System.Data.SqlDbType.Real, 4, "PriceLocal"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceUS", System.Data.SqlDbType.Real, 4, "PriceUS"))
        '
        'SqlSelectCommand11
        '
        Me.SqlSelectCommand11.CommandText = "SELECT SubmissionID, SortKey, Category, MaterialID, MaterialName, BBL, Gravity, D" & _
        "ensity, MT, PriceLocal, PriceUS FROM dbo.Yield"
        Me.SqlSelectCommand11.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = "UPDATE dbo.Yield SET SubmissionID = @SubmissionID, SortKey = @SortKey, Category =" & _
        " @Category, MaterialID = @MaterialID, MaterialName = @MaterialName, BBL = @BBL, " & _
        "Gravity = @Gravity, Density = @Density, MT = @MT, PriceLocal = @PriceLocal, Pric" & _
        "eUS = @PriceUS WHERE (SortKey = @Original_SortKey) AND (SubmissionID = @Original" & _
        "_SubmissionID) AND (BBL = @Original_BBL) AND (Category = @Original_Category) AND" & _
        " (Density = @Original_Density OR @Original_Density IS NULL AND Density IS NULL) " & _
        "AND (Gravity = @Original_Gravity OR @Original_Gravity IS NULL AND Gravity IS NUL" & _
        "L) AND (MT = @Original_MT OR @Original_MT IS NULL AND MT IS NULL) AND (MaterialI" & _
        "D = @Original_MaterialID) AND (MaterialName = @Original_MaterialName) AND (Price" & _
        "Local = @Original_PriceLocal) AND (PriceUS = @Original_PriceUS OR @Original_Pric" & _
        "eUS IS NULL AND PriceUS IS NULL)"
        Me.SqlUpdateCommand10.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.Int, 4, "SortKey"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Category", System.Data.SqlDbType.VarChar, 5, "Category"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaterialID", System.Data.SqlDbType.VarChar, 5, "MaterialID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaterialName", System.Data.SqlDbType.VarChar, 35, "MaterialName"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BBL", System.Data.SqlDbType.Float, 8, "BBL"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Gravity", System.Data.SqlDbType.Real, 4, "Gravity"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Density", System.Data.SqlDbType.Real, 4, "Density"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MT", System.Data.SqlDbType.Float, 8, "MT"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceLocal", System.Data.SqlDbType.Real, 4, "PriceLocal"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceUS", System.Data.SqlDbType.Real, 4, "PriceUS"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BBL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BBL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Category", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Category", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Density", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Density", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Gravity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Gravity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaterialID", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaterialID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaterialName", System.Data.SqlDbType.VarChar, 35, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaterialName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceUS", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdUnitTargets
        '
        Me.sdUnitTargets.DeleteCommand = Me.SqlDeleteCommand13
        Me.sdUnitTargets.InsertCommand = Me.SqlInsertCommand14
        Me.sdUnitTargets.SelectCommand = Me.SqlSelectCommand14
        Me.sdUnitTargets.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "UnitTargets", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("UnitID", "UnitID"), New System.Data.Common.DataColumnMapping("MechAvail", "MechAvail"), New System.Data.Common.DataColumnMapping("OpAvail", "OpAvail"), New System.Data.Common.DataColumnMapping("OnStream", "OnStream"), New System.Data.Common.DataColumnMapping("UtilPcnt", "UtilPcnt"), New System.Data.Common.DataColumnMapping("RoutCost", "RoutCost"), New System.Data.Common.DataColumnMapping("TACost", "TACost"), New System.Data.Common.DataColumnMapping("CurrencyCode", "CurrencyCode")})})
        Me.sdUnitTargets.UpdateCommand = Me.SqlUpdateCommand13
        '
        'SqlDeleteCommand13
        '
        Me.SqlDeleteCommand13.CommandText = "DELETE FROM dbo.UnitTargets WHERE (SubmissionID = @Original_SubmissionID) AND (Un" & _
        "itID = @Original_UnitID) AND (CurrencyCode = @Original_CurrencyCode OR @Original" & _
        "_CurrencyCode IS NULL AND CurrencyCode IS NULL) AND (MechAvail = @Original_MechA" & _
        "vail OR @Original_MechAvail IS NULL AND MechAvail IS NULL) AND (OnStream = @Orig" & _
        "inal_OnStream OR @Original_OnStream IS NULL AND OnStream IS NULL) AND (OpAvail =" & _
        " @Original_OpAvail OR @Original_OpAvail IS NULL AND OpAvail IS NULL) AND (RoutCo" & _
        "st = @Original_RoutCost OR @Original_RoutCost IS NULL AND RoutCost IS NULL) AND " & _
        "(TACost = @Original_TACost OR @Original_TACost IS NULL AND TACost IS NULL) AND (" & _
        "UtilPcnt = @Original_UtilPcnt OR @Original_UtilPcnt IS NULL AND UtilPcnt IS NULL" & _
        ")"
        Me.SqlDeleteCommand13.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CurrencyCode", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CurrencyCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MechAvail", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MechAvail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OnStream", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OnStream", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OpAvail", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OpAvail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TACost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TACost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UtilPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand14
        '
        Me.SqlInsertCommand14.CommandText = "INSERT INTO dbo.UnitTargets(SubmissionID, UnitID, MechAvail, OpAvail, OnStream, U" & _
        "tilPcnt, RoutCost, TACost, CurrencyCode) VALUES (@SubmissionID, @UnitID, @MechAv" & _
        "ail, @OpAvail, @OnStream, @UtilPcnt, @RoutCost, @TACost, @CurrencyCode)"
        Me.SqlInsertCommand14.Connection = Me.SqlConnection1
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MechAvail", System.Data.SqlDbType.Real, 4, "MechAvail"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OpAvail", System.Data.SqlDbType.Real, 4, "OpAvail"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OnStream", System.Data.SqlDbType.Real, 4, "OnStream"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UtilPcnt", System.Data.SqlDbType.Real, 4, "UtilPcnt"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCost", System.Data.SqlDbType.Real, 4, "RoutCost"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TACost", System.Data.SqlDbType.Real, 4, "TACost"))
        Me.SqlInsertCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CurrencyCode", System.Data.SqlDbType.VarChar, 4, "CurrencyCode"))
        '
        'SqlSelectCommand14
        '
        Me.SqlSelectCommand14.CommandText = "SELECT SubmissionID, UnitID, MechAvail, OpAvail, OnStream, UtilPcnt, RoutCost, TA" & _
        "Cost, CurrencyCode FROM dbo.UnitTargets"
        Me.SqlSelectCommand14.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand13
        '
        Me.SqlUpdateCommand13.CommandText = "UPDATE dbo.UnitTargets SET SubmissionID = @SubmissionID, UnitID = @UnitID, MechAv" & _
        "ail = @MechAvail, OpAvail = @OpAvail, OnStream = @OnStream, UtilPcnt = @UtilPcnt" & _
        ", RoutCost = @RoutCost, TACost = @TACost, CurrencyCode = @CurrencyCode WHERE (Su" & _
        "bmissionID = @Original_SubmissionID) AND (UnitID = @Original_UnitID) AND (Curren" & _
        "cyCode = @Original_CurrencyCode OR @Original_CurrencyCode IS NULL AND CurrencyCo" & _
        "de IS NULL) AND (MechAvail = @Original_MechAvail OR @Original_MechAvail IS NULL " & _
        "AND MechAvail IS NULL) AND (OnStream = @Original_OnStream OR @Original_OnStream " & _
        "IS NULL AND OnStream IS NULL) AND (OpAvail = @Original_OpAvail OR @Original_OpAv" & _
        "ail IS NULL AND OpAvail IS NULL) AND (RoutCost = @Original_RoutCost OR @Original" & _
        "_RoutCost IS NULL AND RoutCost IS NULL) AND (TACost = @Original_TACost OR @Origi" & _
        "nal_TACost IS NULL AND TACost IS NULL) AND (UtilPcnt = @Original_UtilPcnt OR @Or" & _
        "iginal_UtilPcnt IS NULL AND UtilPcnt IS NULL)"
        Me.SqlUpdateCommand13.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MechAvail", System.Data.SqlDbType.Real, 4, "MechAvail"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OpAvail", System.Data.SqlDbType.Real, 4, "OpAvail"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OnStream", System.Data.SqlDbType.Real, 4, "OnStream"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UtilPcnt", System.Data.SqlDbType.Real, 4, "UtilPcnt"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCost", System.Data.SqlDbType.Real, 4, "RoutCost"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TACost", System.Data.SqlDbType.Real, 4, "TACost"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CurrencyCode", System.Data.SqlDbType.VarChar, 4, "CurrencyCode"))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CurrencyCode", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CurrencyCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MechAvail", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MechAvail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OnStream", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OnStream", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OpAvail", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OpAvail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TACost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TACost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UtilPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdRefTargets
        '
        Me.sdRefTargets.DeleteCommand = Me.SqlDeleteCommand14
        Me.sdRefTargets.InsertCommand = Me.SqlInsertCommand15
        Me.sdRefTargets.SelectCommand = Me.SqlSelectCommand15
        Me.sdRefTargets.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "RefTargets", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("Property", "Property"), New System.Data.Common.DataColumnMapping("Target", "Target"), New System.Data.Common.DataColumnMapping("CurrencyCode", "CurrencyCode")})})
        Me.sdRefTargets.UpdateCommand = Me.SqlUpdateCommand14
        '
        'SqlDeleteCommand14
        '
        Me.SqlDeleteCommand14.CommandText = "DELETE FROM dbo.RefTargets WHERE (Property = @Original_Property) AND (SubmissionI" & _
        "D = @Original_SubmissionID) AND (CurrencyCode = @Original_CurrencyCode OR @Origi" & _
        "nal_CurrencyCode IS NULL AND CurrencyCode IS NULL) AND (Target = @Original_Targe" & _
        "t OR @Original_Target IS NULL AND Target IS NULL)"
        Me.SqlDeleteCommand14.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Property", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Property", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CurrencyCode", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CurrencyCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Target", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Target", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand15
        '
        Me.SqlInsertCommand15.CommandText = "INSERT INTO dbo.RefTargets(SubmissionID, Property, Target, CurrencyCode) VALUES (" & _
        "@SubmissionID, @Property, @Target, @CurrencyCode)"
        Me.SqlInsertCommand15.Connection = Me.SqlConnection1
        Me.SqlInsertCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Property", System.Data.SqlDbType.VarChar, 50, "Property"))
        Me.SqlInsertCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Target", System.Data.SqlDbType.Real, 4, "Target"))
        Me.SqlInsertCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CurrencyCode", System.Data.SqlDbType.VarChar, 4, "CurrencyCode"))
        '
        'SqlSelectCommand15
        '
        Me.SqlSelectCommand15.CommandText = "SELECT SubmissionID, Property, Target, CurrencyCode FROM dbo.RefTargets"
        Me.SqlSelectCommand15.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand14
        '
        Me.SqlUpdateCommand14.CommandText = "UPDATE dbo.RefTargets SET SubmissionID = @SubmissionID, Property = @Property, Tar" & _
        "get = @Target, CurrencyCode = @CurrencyCode WHERE (Property = @Original_Property" & _
        ") AND (SubmissionID = @Original_SubmissionID) AND (CurrencyCode = @Original_Curr" & _
        "encyCode OR @Original_CurrencyCode IS NULL AND CurrencyCode IS NULL) AND (Target" & _
        " = @Original_Target OR @Original_Target IS NULL AND Target IS NULL)"
        Me.SqlUpdateCommand14.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Property", System.Data.SqlDbType.VarChar, 50, "Property"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Target", System.Data.SqlDbType.Real, 4, "Target"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CurrencyCode", System.Data.SqlDbType.VarChar, 4, "CurrencyCode"))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Property", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Property", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CurrencyCode", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CurrencyCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Target", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Target", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdMaintRoutHist
        '
        Me.sdMaintRoutHist.DeleteCommand = Me.SqlDeleteCommand15
        Me.sdMaintRoutHist.InsertCommand = Me.SqlInsertCommand16
        Me.sdMaintRoutHist.SelectCommand = Me.SqlSelectCommand16
        Me.sdMaintRoutHist.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MaintRoutHist", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefineryID", "RefineryID"), New System.Data.Common.DataColumnMapping("DataSet", "DataSet"), New System.Data.Common.DataColumnMapping("PeriodStart", "PeriodStart"), New System.Data.Common.DataColumnMapping("PeriodEnd", "PeriodEnd"), New System.Data.Common.DataColumnMapping("Currency", "Currency"), New System.Data.Common.DataColumnMapping("RoutCost", "RoutCost"), New System.Data.Common.DataColumnMapping("RoutMatl", "RoutMatl"), New System.Data.Common.DataColumnMapping("Reported", "Reported")})})
        Me.sdMaintRoutHist.UpdateCommand = Me.SqlUpdateCommand15
        '
        'SqlDeleteCommand15
        '
        Me.SqlDeleteCommand15.CommandText = "DELETE FROM dbo.MaintRoutHist WHERE (Currency = @Original_Currency) AND (DataSet " & _
        "= @Original_DataSet) AND (PeriodStart = @Original_PeriodStart) AND (RefineryID =" & _
        " @Original_RefineryID) AND (PeriodEnd = @Original_PeriodEnd) AND (Reported = @Or" & _
        "iginal_Reported) AND (RoutCost = @Original_RoutCost OR @Original_RoutCost IS NUL" & _
        "L AND RoutCost IS NULL) AND (RoutMatl = @Original_RoutMatl OR @Original_RoutMatl" & _
        " IS NULL AND RoutMatl IS NULL)"
        Me.SqlDeleteCommand15.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodStart", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodStart", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodEnd", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Reported", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Reported", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatl", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand16
        '
        Me.SqlInsertCommand16.CommandText = "INSERT INTO dbo.MaintRoutHist(RefineryID, DataSet, PeriodStart, PeriodEnd, Curren" & _
        "cy, RoutCost, RoutMatl, Reported) VALUES (@RefineryID, @DataSet, @PeriodStart, @" & _
        "PeriodEnd, @Currency, @RoutCost, @RoutMatl, @Reported)"
        Me.SqlInsertCommand16.Connection = Me.SqlConnection1
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodStart", System.Data.SqlDbType.DateTime, 4, "PeriodStart"))
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodEnd", System.Data.SqlDbType.DateTime, 4, "PeriodEnd"))
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCost", System.Data.SqlDbType.Real, 4, "RoutCost"))
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatl", System.Data.SqlDbType.Real, 4, "RoutMatl"))
        Me.SqlInsertCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Reported", System.Data.SqlDbType.Bit, 1, "Reported"))
        '
        'SqlSelectCommand16
        '
        Me.SqlSelectCommand16.CommandText = "SELECT RefineryID, DataSet, PeriodStart, PeriodEnd, Currency, RoutCost, RoutMatl," & _
        " Reported FROM dbo.MaintRoutHist"
        Me.SqlSelectCommand16.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand15
        '
        Me.SqlUpdateCommand15.CommandText = "UPDATE dbo.MaintRoutHist SET RefineryID = @RefineryID, DataSet = @DataSet, Period" & _
        "Start = @PeriodStart, PeriodEnd = @PeriodEnd, Currency = @Currency, RoutCost = @" & _
        "RoutCost, RoutMatl = @RoutMatl, Reported = @Reported WHERE (Currency = @Original" & _
        "_Currency) AND (DataSet = @Original_DataSet) AND (PeriodStart = @Original_Period" & _
        "Start) AND (RefineryID = @Original_RefineryID) AND (PeriodEnd = @Original_Period" & _
        "End) AND (Reported = @Original_Reported) AND (RoutCost = @Original_RoutCost OR @" & _
        "Original_RoutCost IS NULL AND RoutCost IS NULL) AND (RoutMatl = @Original_RoutMa" & _
        "tl OR @Original_RoutMatl IS NULL AND RoutMatl IS NULL)"
        Me.SqlUpdateCommand15.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodStart", System.Data.SqlDbType.DateTime, 4, "PeriodStart"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodEnd", System.Data.SqlDbType.DateTime, 4, "PeriodEnd"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCost", System.Data.SqlDbType.Real, 4, "RoutCost"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatl", System.Data.SqlDbType.Real, 4, "RoutMatl"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Reported", System.Data.SqlDbType.Bit, 1, "Reported"))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodStart", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodStart", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodEnd", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Reported", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Reported", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatl", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdLoadTA
        '
        Me.sdLoadTA.DeleteCommand = Me.SqlDeleteCommand17
        Me.sdLoadTA.InsertCommand = Me.SqlInsertCommand17
        Me.sdLoadTA.SelectCommand = Me.SqlSelectCommand17
        Me.sdLoadTA.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "LoadTA", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefineryID", "RefineryID"), New System.Data.Common.DataColumnMapping("DataSet", "DataSet"), New System.Data.Common.DataColumnMapping("UnitID", "UnitID"), New System.Data.Common.DataColumnMapping("TAID", "TAID"), New System.Data.Common.DataColumnMapping("SchedTAInt", "SchedTAInt"), New System.Data.Common.DataColumnMapping("TADate", "TADate"), New System.Data.Common.DataColumnMapping("TAHrsDown", "TAHrsDown"), New System.Data.Common.DataColumnMapping("TACostLocal", "TACostLocal"), New System.Data.Common.DataColumnMapping("TAMatlLocal", "TAMatlLocal"), New System.Data.Common.DataColumnMapping("TAOCCSTH", "TAOCCSTH"), New System.Data.Common.DataColumnMapping("TAOCCOVT", "TAOCCOVT"), New System.Data.Common.DataColumnMapping("TAMPSSTH", "TAMPSSTH"), New System.Data.Common.DataColumnMapping("TAMPSOVTPcnt", "TAMPSOVTPcnt"), New System.Data.Common.DataColumnMapping("TAContOCC", "TAContOCC"), New System.Data.Common.DataColumnMapping("TAContMPS", "TAContMPS"), New System.Data.Common.DataColumnMapping("PrevTADate", "PrevTADate"), New System.Data.Common.DataColumnMapping("TAExceptions", "TAExceptions"), New System.Data.Common.DataColumnMapping("TACurrency", "TACurrency"), New System.Data.Common.DataColumnMapping("ProcessID", "ProcessID")})})
        Me.sdLoadTA.UpdateCommand = Me.SqlUpdateCommand17
        '
        'SqlDeleteCommand17
        '
        Me.SqlDeleteCommand17.CommandText = "DELETE FROM dbo.LoadTA WHERE (DataSet = @Original_DataSet) AND (RefineryID = @Ori" & _
        "ginal_RefineryID) AND (TAID = @Original_TAID) AND (UnitID = @Original_UnitID) AN" & _
        "D (PrevTADate = @Original_PrevTADate OR @Original_PrevTADate IS NULL AND PrevTAD" & _
        "ate IS NULL) AND (ProcessID = @Original_ProcessID OR @Original_ProcessID IS NULL" & _
        " AND ProcessID IS NULL) AND (SchedTAInt = @Original_SchedTAInt OR @Original_Sche" & _
        "dTAInt IS NULL AND SchedTAInt IS NULL) AND (TAContMPS = @Original_TAContMPS OR @" & _
        "Original_TAContMPS IS NULL AND TAContMPS IS NULL) AND (TAContOCC = @Original_TAC" & _
        "ontOCC OR @Original_TAContOCC IS NULL AND TAContOCC IS NULL) AND (TACostLocal = " & _
        "@Original_TACostLocal OR @Original_TACostLocal IS NULL AND TACostLocal IS NULL) " & _
        "AND (TACurrency = @Original_TACurrency OR @Original_TACurrency IS NULL AND TACur" & _
        "rency IS NULL) AND (TADate = @Original_TADate OR @Original_TADate IS NULL AND TA" & _
        "Date IS NULL) AND (TAExceptions = @Original_TAExceptions OR @Original_TAExceptio" & _
        "ns IS NULL AND TAExceptions IS NULL) AND (TAHrsDown = @Original_TAHrsDown OR @Or" & _
        "iginal_TAHrsDown IS NULL AND TAHrsDown IS NULL) AND (TAMPSOVTPcnt = @Original_TA" & _
        "MPSOVTPcnt OR @Original_TAMPSOVTPcnt IS NULL AND TAMPSOVTPcnt IS NULL) AND (TAMP" & _
        "SSTH = @Original_TAMPSSTH OR @Original_TAMPSSTH IS NULL AND TAMPSSTH IS NULL) AN" & _
        "D (TAMatlLocal = @Original_TAMatlLocal OR @Original_TAMatlLocal IS NULL AND TAMa" & _
        "tlLocal IS NULL) AND (TAOCCOVT = @Original_TAOCCOVT OR @Original_TAOCCOVT IS NUL" & _
        "L AND TAOCCOVT IS NULL) AND (TAOCCSTH = @Original_TAOCCSTH OR @Original_TAOCCSTH" & _
        " IS NULL AND TAOCCSTH IS NULL)"
        Me.SqlDeleteCommand17.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrevTADate", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrevTADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SchedTAInt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SchedTAInt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAContMPS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAContMPS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAContOCC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAContOCC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TACostLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TACostLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TACurrency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TACurrency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TADate", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAExceptions", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAExceptions", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAHrsDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAHrsDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMPSOVTPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMPSOVTPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMPSSTH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMPSSTH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMatlLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMatlLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAOCCOVT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAOCCOVT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAOCCSTH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAOCCSTH", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand17
        '
        Me.SqlInsertCommand17.CommandText = "INSERT INTO dbo.LoadTA(RefineryID, DataSet, UnitID, TAID, SchedTAInt, TADate, TAH" & _
        "rsDown, TACostLocal, TAMatlLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TA" & _
        "ContOCC, TAContMPS, PrevTADate, TAExceptions, TACurrency, ProcessID) VALUES (@Re" & _
        "fineryID, @DataSet, @UnitID, @TAID, @SchedTAInt, @TADate, @TAHrsDown, @TACostLoc" & _
        "al, @TAMatlLocal, @TAOCCSTH, @TAOCCOVT, @TAMPSSTH, @TAMPSOVTPcnt, @TAContOCC, @T" & _
        "AContMPS, @PrevTADate, @TAExceptions, @TACurrency, @ProcessID)"
        Me.SqlInsertCommand17.Connection = Me.SqlConnection1
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SchedTAInt", System.Data.SqlDbType.Real, 4, "SchedTAInt"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TADate", System.Data.SqlDbType.DateTime, 4, "TADate"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAHrsDown", System.Data.SqlDbType.Real, 4, "TAHrsDown"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TACostLocal", System.Data.SqlDbType.Real, 4, "TACostLocal"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMatlLocal", System.Data.SqlDbType.Real, 4, "TAMatlLocal"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAOCCSTH", System.Data.SqlDbType.Real, 4, "TAOCCSTH"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAOCCOVT", System.Data.SqlDbType.Real, 4, "TAOCCOVT"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMPSSTH", System.Data.SqlDbType.Real, 4, "TAMPSSTH"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMPSOVTPcnt", System.Data.SqlDbType.Real, 4, "TAMPSOVTPcnt"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAContOCC", System.Data.SqlDbType.Real, 4, "TAContOCC"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAContMPS", System.Data.SqlDbType.Real, 4, "TAContMPS"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PrevTADate", System.Data.SqlDbType.DateTime, 4, "PrevTADate"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAExceptions", System.Data.SqlDbType.SmallInt, 2, "TAExceptions"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TACurrency", System.Data.SqlDbType.VarChar, 4, "TACurrency"))
        Me.SqlInsertCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        '
        'SqlSelectCommand17
        '
        Me.SqlSelectCommand17.CommandText = "SELECT RefineryID, DataSet, UnitID, TAID, SchedTAInt, TADate, TAHrsDown, TACostLo" & _
        "cal, TAMatlLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContM" & _
        "PS, PrevTADate, TAExceptions, TACurrency, ProcessID FROM dbo.LoadTA"
        Me.SqlSelectCommand17.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand17
        '
        Me.SqlUpdateCommand17.CommandText = "UPDATE dbo.LoadTA SET RefineryID = @RefineryID, DataSet = @DataSet, UnitID = @Uni" & _
        "tID, TAID = @TAID, SchedTAInt = @SchedTAInt, TADate = @TADate, TAHrsDown = @TAHr" & _
        "sDown, TACostLocal = @TACostLocal, TAMatlLocal = @TAMatlLocal, TAOCCSTH = @TAOCC" & _
        "STH, TAOCCOVT = @TAOCCOVT, TAMPSSTH = @TAMPSSTH, TAMPSOVTPcnt = @TAMPSOVTPcnt, T" & _
        "AContOCC = @TAContOCC, TAContMPS = @TAContMPS, PrevTADate = @PrevTADate, TAExcep" & _
        "tions = @TAExceptions, TACurrency = @TACurrency, ProcessID = @ProcessID WHERE (D" & _
        "ataSet = @Original_DataSet) AND (RefineryID = @Original_RefineryID) AND (TAID = " & _
        "@Original_TAID) AND (UnitID = @Original_UnitID) AND (PrevTADate = @Original_Prev" & _
        "TADate OR @Original_PrevTADate IS NULL AND PrevTADate IS NULL) AND (ProcessID = " & _
        "@Original_ProcessID OR @Original_ProcessID IS NULL AND ProcessID IS NULL) AND (S" & _
        "chedTAInt = @Original_SchedTAInt OR @Original_SchedTAInt IS NULL AND SchedTAInt " & _
        "IS NULL) AND (TAContMPS = @Original_TAContMPS OR @Original_TAContMPS IS NULL AND" & _
        " TAContMPS IS NULL) AND (TAContOCC = @Original_TAContOCC OR @Original_TAContOCC " & _
        "IS NULL AND TAContOCC IS NULL) AND (TACostLocal = @Original_TACostLocal OR @Orig" & _
        "inal_TACostLocal IS NULL AND TACostLocal IS NULL) AND (TACurrency = @Original_TA" & _
        "Currency OR @Original_TACurrency IS NULL AND TACurrency IS NULL) AND (TADate = @" & _
        "Original_TADate OR @Original_TADate IS NULL AND TADate IS NULL) AND (TAException" & _
        "s = @Original_TAExceptions OR @Original_TAExceptions IS NULL AND TAExceptions IS" & _
        " NULL) AND (TAHrsDown = @Original_TAHrsDown OR @Original_TAHrsDown IS NULL AND T" & _
        "AHrsDown IS NULL) AND (TAMPSOVTPcnt = @Original_TAMPSOVTPcnt OR @Original_TAMPSO" & _
        "VTPcnt IS NULL AND TAMPSOVTPcnt IS NULL) AND (TAMPSSTH = @Original_TAMPSSTH OR @" & _
        "Original_TAMPSSTH IS NULL AND TAMPSSTH IS NULL) AND (TAMatlLocal = @Original_TAM" & _
        "atlLocal OR @Original_TAMatlLocal IS NULL AND TAMatlLocal IS NULL) AND (TAOCCOVT" & _
        " = @Original_TAOCCOVT OR @Original_TAOCCOVT IS NULL AND TAOCCOVT IS NULL) AND (T" & _
        "AOCCSTH = @Original_TAOCCSTH OR @Original_TAOCCSTH IS NULL AND TAOCCSTH IS NULL)" & _
        ""
        Me.SqlUpdateCommand17.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SchedTAInt", System.Data.SqlDbType.Real, 4, "SchedTAInt"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TADate", System.Data.SqlDbType.DateTime, 4, "TADate"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAHrsDown", System.Data.SqlDbType.Real, 4, "TAHrsDown"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TACostLocal", System.Data.SqlDbType.Real, 4, "TACostLocal"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMatlLocal", System.Data.SqlDbType.Real, 4, "TAMatlLocal"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAOCCSTH", System.Data.SqlDbType.Real, 4, "TAOCCSTH"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAOCCOVT", System.Data.SqlDbType.Real, 4, "TAOCCOVT"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMPSSTH", System.Data.SqlDbType.Real, 4, "TAMPSSTH"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMPSOVTPcnt", System.Data.SqlDbType.Real, 4, "TAMPSOVTPcnt"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAContOCC", System.Data.SqlDbType.Real, 4, "TAContOCC"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAContMPS", System.Data.SqlDbType.Real, 4, "TAContMPS"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PrevTADate", System.Data.SqlDbType.DateTime, 4, "PrevTADate"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAExceptions", System.Data.SqlDbType.SmallInt, 2, "TAExceptions"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TACurrency", System.Data.SqlDbType.VarChar, 4, "TACurrency"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PrevTADate", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrevTADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SchedTAInt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SchedTAInt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAContMPS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAContMPS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAContOCC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAContOCC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TACostLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TACostLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TACurrency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TACurrency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TADate", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAExceptions", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAExceptions", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAHrsDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAHrsDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMPSOVTPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMPSOVTPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMPSSTH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMPSSTH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMatlLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMatlLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAOCCOVT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAOCCOVT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand17.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAOCCSTH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAOCCSTH", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdMaintRout
        '
        Me.sdMaintRout.DeleteCommand = Me.SqlDeleteCommand8
        Me.sdMaintRout.InsertCommand = Me.SqlInsertCommand9
        Me.sdMaintRout.SelectCommand = Me.SqlSelectCommand9
        Me.sdMaintRout.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MaintRout", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("UnitID", "UnitID"), New System.Data.Common.DataColumnMapping("ProcessID", "ProcessID"), New System.Data.Common.DataColumnMapping("RegNum", "RegNum"), New System.Data.Common.DataColumnMapping("RegDown", "RegDown"), New System.Data.Common.DataColumnMapping("RegSlow", "RegSlow"), New System.Data.Common.DataColumnMapping("MaintNum", "MaintNum"), New System.Data.Common.DataColumnMapping("MaintDown", "MaintDown"), New System.Data.Common.DataColumnMapping("MaintSlow", "MaintSlow"), New System.Data.Common.DataColumnMapping("OthNum", "OthNum"), New System.Data.Common.DataColumnMapping("OthDown", "OthDown"), New System.Data.Common.DataColumnMapping("OthSlow", "OthSlow"), New System.Data.Common.DataColumnMapping("RoutCostLocal", "RoutCostLocal"), New System.Data.Common.DataColumnMapping("RoutMatlLocal", "RoutMatlLocal"), New System.Data.Common.DataColumnMapping("RoutCostUS", "RoutCostUS"), New System.Data.Common.DataColumnMapping("RoutMatlUS", "RoutMatlUS"), New System.Data.Common.DataColumnMapping("RoutMatlPcnt", "RoutMatlPcnt"), New System.Data.Common.DataColumnMapping("OthDownEconomic", "OthDownEconomic"), New System.Data.Common.DataColumnMapping("OthDownExternal", "OthDownExternal"), New System.Data.Common.DataColumnMapping("OthDownUnitUpsets", "OthDownUnitUpsets"), New System.Data.Common.DataColumnMapping("OthDownOffsiteUpsets", "OthDownOffsiteUpsets"), New System.Data.Common.DataColumnMapping("OthDownOther", "OthDownOther"), New System.Data.Common.DataColumnMapping("UnpRegNum", "UnpRegNum"), New System.Data.Common.DataColumnMapping("UnpRegDown", "UnpRegDown"), New System.Data.Common.DataColumnMapping("UnpMaintNum", "UnpMaintNum"), New System.Data.Common.DataColumnMapping("UnpMaintDown", "UnpMaintDown"), New System.Data.Common.DataColumnMapping("UnpOthNum", "UnpOthNum"), New System.Data.Common.DataColumnMapping("UnpOthDown", "UnpOthDown"), New System.Data.Common.DataColumnMapping("UnpDown", "UnpDown"), New System.Data.Common.DataColumnMapping("PlanRegDown", "PlanRegDown"), New System.Data.Common.DataColumnMapping("PlanMaintDown", "PlanMaintDown"), New System.Data.Common.DataColumnMapping("PlanOthDown", "PlanOthDown"), New System.Data.Common.DataColumnMapping("PlanDown", "PlanDown"), New System.Data.Common.DataColumnMapping("UseData", "UseData"), New System.Data.Common.DataColumnMapping("UsePcntOfCost", "UsePcntOfCost"), New System.Data.Common.DataColumnMapping("PeriodHrs", "PeriodHrs")})})
        Me.sdMaintRout.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = "DELETE FROM dbo.MaintRout WHERE (SubmissionID = @Original_SubmissionID) AND (Unit" & _
        "ID = @Original_UnitID) AND (MaintDown = @Original_MaintDown OR @Original_MaintDo" & _
        "wn IS NULL AND MaintDown IS NULL) AND (MaintNum = @Original_MaintNum OR @Origina" & _
        "l_MaintNum IS NULL AND MaintNum IS NULL) AND (MaintSlow = @Original_MaintSlow OR" & _
        " @Original_MaintSlow IS NULL AND MaintSlow IS NULL) AND (OthDown = @Original_Oth" & _
        "Down OR @Original_OthDown IS NULL AND OthDown IS NULL) AND (OthDownEconomic = @O" & _
        "riginal_OthDownEconomic OR @Original_OthDownEconomic IS NULL AND OthDownEconomic" & _
        " IS NULL) AND (OthDownExternal = @Original_OthDownExternal OR @Original_OthDownE" & _
        "xternal IS NULL AND OthDownExternal IS NULL) AND (OthDownOffsiteUpsets = @Origin" & _
        "al_OthDownOffsiteUpsets OR @Original_OthDownOffsiteUpsets IS NULL AND OthDownOff" & _
        "siteUpsets IS NULL) AND (OthDownOther = @Original_OthDownOther OR @Original_OthD" & _
        "ownOther IS NULL AND OthDownOther IS NULL) AND (OthDownUnitUpsets = @Original_Ot" & _
        "hDownUnitUpsets OR @Original_OthDownUnitUpsets IS NULL AND OthDownUnitUpsets IS " & _
        "NULL) AND (OthNum = @Original_OthNum OR @Original_OthNum IS NULL AND OthNum IS N" & _
        "ULL) AND (OthSlow = @Original_OthSlow OR @Original_OthSlow IS NULL AND OthSlow I" & _
        "S NULL) AND (PeriodHrs = @Original_PeriodHrs OR @Original_PeriodHrs IS NULL AND " & _
        "PeriodHrs IS NULL) AND (PlanDown = @Original_PlanDown OR @Original_PlanDown IS N" & _
        "ULL AND PlanDown IS NULL) AND (PlanMaintDown = @Original_PlanMaintDown OR @Origi" & _
        "nal_PlanMaintDown IS NULL AND PlanMaintDown IS NULL) AND (PlanOthDown = @Origina" & _
        "l_PlanOthDown OR @Original_PlanOthDown IS NULL AND PlanOthDown IS NULL) AND (Pla" & _
        "nRegDown = @Original_PlanRegDown OR @Original_PlanRegDown IS NULL AND PlanRegDow" & _
        "n IS NULL) AND (ProcessID = @Original_ProcessID OR @Original_ProcessID IS NULL A" & _
        "ND ProcessID IS NULL) AND (RegDown = @Original_RegDown OR @Original_RegDown IS N" & _
        "ULL AND RegDown IS NULL) AND (RegNum = @Original_RegNum OR @Original_RegNum IS N" & _
        "ULL AND RegNum IS NULL) AND (RegSlow = @Original_RegSlow OR @Original_RegSlow IS" & _
        " NULL AND RegSlow IS NULL) AND (RoutCostLocal = @Original_RoutCostLocal OR @Orig" & _
        "inal_RoutCostLocal IS NULL AND RoutCostLocal IS NULL) AND (RoutCostUS = @Origina" & _
        "l_RoutCostUS OR @Original_RoutCostUS IS NULL AND RoutCostUS IS NULL) AND (RoutMa" & _
        "tlLocal = @Original_RoutMatlLocal OR @Original_RoutMatlLocal IS NULL AND RoutMat" & _
        "lLocal IS NULL) AND (RoutMatlPcnt = @Original_RoutMatlPcnt OR @Original_RoutMatl" & _
        "Pcnt IS NULL AND RoutMatlPcnt IS NULL) AND (RoutMatlUS = @Original_RoutMatlUS OR" & _
        " @Original_RoutMatlUS IS NULL AND RoutMatlUS IS NULL) AND (UnpDown = @Original_U" & _
        "npDown OR @Original_UnpDown IS NULL AND UnpDown IS NULL) AND (UnpMaintDown = @Or" & _
        "iginal_UnpMaintDown OR @Original_UnpMaintDown IS NULL AND UnpMaintDown IS NULL) " & _
        "AND (UnpMaintNum = @Original_UnpMaintNum OR @Original_UnpMaintNum IS NULL AND Un" & _
        "pMaintNum IS NULL) AND (UnpOthDown = @Original_UnpOthDown OR @Original_UnpOthDow" & _
        "n IS NULL AND UnpOthDown IS NULL) AND (UnpOthNum = @Original_UnpOthNum OR @Origi" & _
        "nal_UnpOthNum IS NULL AND UnpOthNum IS NULL) AND (UnpRegDown = @Original_UnpRegD" & _
        "own OR @Original_UnpRegDown IS NULL AND UnpRegDown IS NULL) AND (UnpRegNum = @Or" & _
        "iginal_UnpRegNum OR @Original_UnpRegNum IS NULL AND UnpRegNum IS NULL) AND (UseD" & _
        "ata = @Original_UseData OR @Original_UseData IS NULL AND UseData IS NULL) AND (U" & _
        "sePcntOfCost = @Original_UsePcntOfCost OR @Original_UsePcntOfCost IS NULL AND Us" & _
        "ePcntOfCost IS NULL)"
        Me.SqlDeleteCommand8.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintSlow", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintSlow", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownEconomic", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownEconomic", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownExternal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownExternal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownOffsiteUpsets", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownOffsiteUpsets", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownOther", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownOther", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownUnitUpsets", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownUnitUpsets", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthSlow", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthSlow", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodHrs", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodHrs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanMaintDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanMaintDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanOthDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanOthDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanRegDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanRegDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RegDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RegDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RegNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RegNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RegSlow", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RegSlow", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCostLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCostLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCostUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCostUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpMaintDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpMaintDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpMaintNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpMaintNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpOthDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpOthDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpOthNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpOthNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpRegDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpRegDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpRegNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpRegNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UseData", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UseData", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UsePcntOfCost", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UsePcntOfCost", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand9
        '
        Me.SqlInsertCommand9.CommandText = "INSERT INTO dbo.MaintRout(SubmissionID, UnitID, ProcessID, RegNum, RegDown, RegSl" & _
        "ow, MaintNum, MaintDown, MaintSlow, OthNum, OthDown, OthSlow, RoutCostLocal, Rou" & _
        "tMatlLocal, RoutCostUS, RoutMatlUS, RoutMatlPcnt, OthDownEconomic, OthDownExtern" & _
        "al, OthDownUnitUpsets, OthDownOffsiteUpsets, OthDownOther, UnpRegNum, UnpRegDown" & _
        ", UnpMaintNum, UnpMaintDown, UnpOthNum, UnpOthDown, UnpDown, PlanRegDown, PlanMa" & _
        "intDown, PlanOthDown, PlanDown, UseData, UsePcntOfCost, PeriodHrs) VALUES (@Subm" & _
        "issionID, @UnitID, @ProcessID, @RegNum, @RegDown, @RegSlow, @MaintNum, @MaintDow" & _
        "n, @MaintSlow, @OthNum, @OthDown, @OthSlow, @RoutCostLocal, @RoutMatlLocal, @Rou" & _
        "tCostUS, @RoutMatlUS, @RoutMatlPcnt, @OthDownEconomic, @OthDownExternal, @OthDow" & _
        "nUnitUpsets, @OthDownOffsiteUpsets, @OthDownOther, @UnpRegNum, @UnpRegDown, @Unp" & _
        "MaintNum, @UnpMaintDown, @UnpOthNum, @UnpOthDown, @UnpDown, @PlanRegDown, @PlanM" & _
        "aintDown, @PlanOthDown, @PlanDown, @UseData, @UsePcntOfCost, @PeriodHrs)"
        Me.SqlInsertCommand9.Connection = Me.SqlConnection1
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RegNum", System.Data.SqlDbType.SmallInt, 2, "RegNum"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RegDown", System.Data.SqlDbType.Real, 4, "RegDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RegSlow", System.Data.SqlDbType.Real, 4, "RegSlow"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintNum", System.Data.SqlDbType.SmallInt, 2, "MaintNum"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintDown", System.Data.SqlDbType.Real, 4, "MaintDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintSlow", System.Data.SqlDbType.Real, 4, "MaintSlow"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNum", System.Data.SqlDbType.SmallInt, 2, "OthNum"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDown", System.Data.SqlDbType.Real, 4, "OthDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthSlow", System.Data.SqlDbType.Real, 4, "OthSlow"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCostLocal", System.Data.SqlDbType.Real, 4, "RoutCostLocal"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlLocal", System.Data.SqlDbType.Real, 4, "RoutMatlLocal"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCostUS", System.Data.SqlDbType.Real, 4, "RoutCostUS"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlUS", System.Data.SqlDbType.Real, 4, "RoutMatlUS"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlPcnt", System.Data.SqlDbType.Real, 4, "RoutMatlPcnt"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownEconomic", System.Data.SqlDbType.Real, 4, "OthDownEconomic"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownExternal", System.Data.SqlDbType.Real, 4, "OthDownExternal"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownUnitUpsets", System.Data.SqlDbType.Real, 4, "OthDownUnitUpsets"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownOffsiteUpsets", System.Data.SqlDbType.Real, 4, "OthDownOffsiteUpsets"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownOther", System.Data.SqlDbType.Real, 4, "OthDownOther"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpRegNum", System.Data.SqlDbType.SmallInt, 2, "UnpRegNum"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpRegDown", System.Data.SqlDbType.Real, 4, "UnpRegDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpMaintNum", System.Data.SqlDbType.SmallInt, 2, "UnpMaintNum"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpMaintDown", System.Data.SqlDbType.Real, 4, "UnpMaintDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpOthNum", System.Data.SqlDbType.SmallInt, 2, "UnpOthNum"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpOthDown", System.Data.SqlDbType.Real, 4, "UnpOthDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpDown", System.Data.SqlDbType.Real, 4, "UnpDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanRegDown", System.Data.SqlDbType.Real, 4, "PlanRegDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanMaintDown", System.Data.SqlDbType.Real, 4, "PlanMaintDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanOthDown", System.Data.SqlDbType.Real, 4, "PlanOthDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanDown", System.Data.SqlDbType.Real, 4, "PlanDown"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UseData", System.Data.SqlDbType.TinyInt, 1, "UseData"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UsePcntOfCost", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UsePcntOfCost", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodHrs", System.Data.SqlDbType.SmallInt, 2, "PeriodHrs"))
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT SubmissionID, UnitID, ProcessID, RegNum, RegDown, RegSlow, MaintNum, Maint" & _
        "Down, MaintSlow, OthNum, OthDown, OthSlow, RoutCostLocal, RoutMatlLocal, RoutCos" & _
        "tUS, RoutMatlUS, RoutMatlPcnt, OthDownEconomic, OthDownExternal, OthDownUnitUpse" & _
        "ts, OthDownOffsiteUpsets, OthDownOther, UnpRegNum, UnpRegDown, UnpMaintNum, UnpM" & _
        "aintDown, UnpOthNum, UnpOthDown, UnpDown, PlanRegDown, PlanMaintDown, PlanOthDow" & _
        "n, PlanDown, UseData, UsePcntOfCost, PeriodHrs FROM dbo.MaintRout"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = "UPDATE dbo.MaintRout SET SubmissionID = @SubmissionID, UnitID = @UnitID, ProcessI" & _
        "D = @ProcessID, RegNum = @RegNum, RegDown = @RegDown, RegSlow = @RegSlow, MaintN" & _
        "um = @MaintNum, MaintDown = @MaintDown, MaintSlow = @MaintSlow, OthNum = @OthNum" & _
        ", OthDown = @OthDown, OthSlow = @OthSlow, RoutCostLocal = @RoutCostLocal, RoutMa" & _
        "tlLocal = @RoutMatlLocal, RoutCostUS = @RoutCostUS, RoutMatlUS = @RoutMatlUS, Ro" & _
        "utMatlPcnt = @RoutMatlPcnt, OthDownEconomic = @OthDownEconomic, OthDownExternal " & _
        "= @OthDownExternal, OthDownUnitUpsets = @OthDownUnitUpsets, OthDownOffsiteUpsets" & _
        " = @OthDownOffsiteUpsets, OthDownOther = @OthDownOther, UnpRegNum = @UnpRegNum, " & _
        "UnpRegDown = @UnpRegDown, UnpMaintNum = @UnpMaintNum, UnpMaintDown = @UnpMaintDo" & _
        "wn, UnpOthNum = @UnpOthNum, UnpOthDown = @UnpOthDown, UnpDown = @UnpDown, PlanRe" & _
        "gDown = @PlanRegDown, PlanMaintDown = @PlanMaintDown, PlanOthDown = @PlanOthDown" & _
        ", PlanDown = @PlanDown, UseData = @UseData, UsePcntOfCost = @UsePcntOfCost, Peri" & _
        "odHrs = @PeriodHrs WHERE (SubmissionID = @Original_SubmissionID) AND (UnitID = @" & _
        "Original_UnitID) AND (MaintDown = @Original_MaintDown OR @Original_MaintDown IS " & _
        "NULL AND MaintDown IS NULL) AND (MaintNum = @Original_MaintNum OR @Original_Main" & _
        "tNum IS NULL AND MaintNum IS NULL) AND (MaintSlow = @Original_MaintSlow OR @Orig" & _
        "inal_MaintSlow IS NULL AND MaintSlow IS NULL) AND (OthDown = @Original_OthDown O" & _
        "R @Original_OthDown IS NULL AND OthDown IS NULL) AND (OthDownEconomic = @Origina" & _
        "l_OthDownEconomic OR @Original_OthDownEconomic IS NULL AND OthDownEconomic IS NU" & _
        "LL) AND (OthDownExternal = @Original_OthDownExternal OR @Original_OthDownExterna" & _
        "l IS NULL AND OthDownExternal IS NULL) AND (OthDownOffsiteUpsets = @Original_Oth" & _
        "DownOffsiteUpsets OR @Original_OthDownOffsiteUpsets IS NULL AND OthDownOffsiteUp" & _
        "sets IS NULL) AND (OthDownOther = @Original_OthDownOther OR @Original_OthDownOth" & _
        "er IS NULL AND OthDownOther IS NULL) AND (OthDownUnitUpsets = @Original_OthDownU" & _
        "nitUpsets OR @Original_OthDownUnitUpsets IS NULL AND OthDownUnitUpsets IS NULL) " & _
        "AND (OthNum = @Original_OthNum OR @Original_OthNum IS NULL AND OthNum IS NULL) A" & _
        "ND (OthSlow = @Original_OthSlow OR @Original_OthSlow IS NULL AND OthSlow IS NULL" & _
        ") AND (PeriodHrs = @Original_PeriodHrs OR @Original_PeriodHrs IS NULL AND Period" & _
        "Hrs IS NULL) AND (PlanDown = @Original_PlanDown OR @Original_PlanDown IS NULL AN" & _
        "D PlanDown IS NULL) AND (PlanMaintDown = @Original_PlanMaintDown OR @Original_Pl" & _
        "anMaintDown IS NULL AND PlanMaintDown IS NULL) AND (PlanOthDown = @Original_Plan" & _
        "OthDown OR @Original_PlanOthDown IS NULL AND PlanOthDown IS NULL) AND (PlanRegDo" & _
        "wn = @Original_PlanRegDown OR @Original_PlanRegDown IS NULL AND PlanRegDown IS N" & _
        "ULL) AND (ProcessID = @Original_ProcessID OR @Original_ProcessID IS NULL AND Pro" & _
        "cessID IS NULL) AND (RegDown = @Original_RegDown OR @Original_RegDown IS NULL AN" & _
        "D RegDown IS NULL) AND (RegNum = @Original_RegNum OR @Original_RegNum IS NULL AN" & _
        "D RegNum IS NULL) AND (RegSlow = @Original_RegSlow OR @Original_RegSlow IS NULL " & _
        "AND RegSlow IS NULL) AND (RoutCostLocal = @Original_RoutCostLocal OR @Original_R" & _
        "outCostLocal IS NULL AND RoutCostLocal IS NULL) AND (RoutCostUS = @Original_Rout" & _
        "CostUS OR @Original_RoutCostUS IS NULL AND RoutCostUS IS NULL) AND (RoutMatlLoca" & _
        "l = @Original_RoutMatlLocal OR @Original_RoutMatlLocal IS NULL AND RoutMatlLocal" & _
        " IS NULL) AND (RoutMatlPcnt = @Original_RoutMatlPcnt OR @Original_RoutMatlPcnt I" & _
        "S NULL AND RoutMatlPcnt IS NULL) AND (RoutMatlUS = @Original_RoutMatlUS OR @Orig" & _
        "inal_RoutMatlUS IS NULL AND RoutMatlUS IS NULL) AND (UnpDown = @Original_UnpDown" & _
        " OR @Original_UnpDown IS NULL AND UnpDown IS NULL) AND (UnpMaintDown = @Original" & _
        "_UnpMaintDown OR @Original_UnpMaintDown IS NULL AND UnpMaintDown IS NULL) AND (U" & _
        "npMaintNum = @Original_UnpMaintNum OR @Original_UnpMaintNum IS NULL AND UnpMaint" & _
        "Num IS NULL) AND (UnpOthDown = @Original_UnpOthDown OR @Original_UnpOthDown IS N" & _
        "ULL AND UnpOthDown IS NULL) AND (UnpOthNum = @Original_UnpOthNum OR @Original_Un" & _
        "pOthNum IS NULL AND UnpOthNum IS NULL) AND (UnpRegDown = @Original_UnpRegDown OR" & _
        " @Original_UnpRegDown IS NULL AND UnpRegDown IS NULL) AND (UnpRegNum = @Original" & _
        "_UnpRegNum OR @Original_UnpRegNum IS NULL AND UnpRegNum IS NULL) AND (UseData = " & _
        "@Original_UseData OR @Original_UseData IS NULL AND UseData IS NULL) AND (UsePcnt" & _
        "OfCost = @Original_UsePcntOfCost OR @Original_UsePcntOfCost IS NULL AND UsePcntO" & _
        "fCost IS NULL)"
        Me.SqlUpdateCommand8.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RegNum", System.Data.SqlDbType.SmallInt, 2, "RegNum"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RegDown", System.Data.SqlDbType.Real, 4, "RegDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RegSlow", System.Data.SqlDbType.Real, 4, "RegSlow"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintNum", System.Data.SqlDbType.SmallInt, 2, "MaintNum"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintDown", System.Data.SqlDbType.Real, 4, "MaintDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintSlow", System.Data.SqlDbType.Real, 4, "MaintSlow"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNum", System.Data.SqlDbType.SmallInt, 2, "OthNum"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDown", System.Data.SqlDbType.Real, 4, "OthDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthSlow", System.Data.SqlDbType.Real, 4, "OthSlow"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCostLocal", System.Data.SqlDbType.Real, 4, "RoutCostLocal"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlLocal", System.Data.SqlDbType.Real, 4, "RoutMatlLocal"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCostUS", System.Data.SqlDbType.Real, 4, "RoutCostUS"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlUS", System.Data.SqlDbType.Real, 4, "RoutMatlUS"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlPcnt", System.Data.SqlDbType.Real, 4, "RoutMatlPcnt"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownEconomic", System.Data.SqlDbType.Real, 4, "OthDownEconomic"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownExternal", System.Data.SqlDbType.Real, 4, "OthDownExternal"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownUnitUpsets", System.Data.SqlDbType.Real, 4, "OthDownUnitUpsets"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownOffsiteUpsets", System.Data.SqlDbType.Real, 4, "OthDownOffsiteUpsets"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthDownOther", System.Data.SqlDbType.Real, 4, "OthDownOther"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpRegNum", System.Data.SqlDbType.SmallInt, 2, "UnpRegNum"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpRegDown", System.Data.SqlDbType.Real, 4, "UnpRegDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpMaintNum", System.Data.SqlDbType.SmallInt, 2, "UnpMaintNum"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpMaintDown", System.Data.SqlDbType.Real, 4, "UnpMaintDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpOthNum", System.Data.SqlDbType.SmallInt, 2, "UnpOthNum"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpOthDown", System.Data.SqlDbType.Real, 4, "UnpOthDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnpDown", System.Data.SqlDbType.Real, 4, "UnpDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanRegDown", System.Data.SqlDbType.Real, 4, "PlanRegDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanMaintDown", System.Data.SqlDbType.Real, 4, "PlanMaintDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanOthDown", System.Data.SqlDbType.Real, 4, "PlanOthDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PlanDown", System.Data.SqlDbType.Real, 4, "PlanDown"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UseData", System.Data.SqlDbType.TinyInt, 1, "UseData"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UsePcntOfCost", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UsePcntOfCost", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodHrs", System.Data.SqlDbType.SmallInt, 2, "PeriodHrs"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintSlow", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintSlow", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownEconomic", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownEconomic", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownExternal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownExternal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownOffsiteUpsets", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownOffsiteUpsets", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownOther", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownOther", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthDownUnitUpsets", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthDownUnitUpsets", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthSlow", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthSlow", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodHrs", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodHrs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanMaintDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanMaintDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanOthDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanOthDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PlanRegDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PlanRegDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RegDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RegDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RegNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RegNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RegSlow", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RegSlow", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCostLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCostLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCostUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCostUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpMaintDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpMaintDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpMaintNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpMaintNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpOthDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpOthDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpOthNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpOthNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpRegDown", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpRegDown", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnpRegNum", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnpRegNum", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UseData", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UseData", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UsePcntOfCost", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UsePcntOfCost", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdEnergy
        '
        Me.sdEnergy.DeleteCommand = Me.SqlDeleteCommand11
        Me.sdEnergy.InsertCommand = Me.SqlInsertCommand12
        Me.sdEnergy.SelectCommand = Me.SqlSelectCommand12
        Me.sdEnergy.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Energy", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("TransCode", "TransCode"), New System.Data.Common.DataColumnMapping("EnergyType", "EnergyType"), New System.Data.Common.DataColumnMapping("TransType", "TransType"), New System.Data.Common.DataColumnMapping("TransferTo", "TransferTo"), New System.Data.Common.DataColumnMapping("SourceMBTU", "SourceMBTU"), New System.Data.Common.DataColumnMapping("UsageMBTU", "UsageMBTU"), New System.Data.Common.DataColumnMapping("PriceMBTUUS", "PriceMBTUUS"), New System.Data.Common.DataColumnMapping("PriceMBTULocal", "PriceMBTULocal"), New System.Data.Common.DataColumnMapping("RptSource", "RptSource"), New System.Data.Common.DataColumnMapping("RptPriceLocal", "RptPriceLocal"), New System.Data.Common.DataColumnMapping("Hydrogen", "Hydrogen"), New System.Data.Common.DataColumnMapping("Methane", "Methane"), New System.Data.Common.DataColumnMapping("Ethane", "Ethane"), New System.Data.Common.DataColumnMapping("Ethylene", "Ethylene"), New System.Data.Common.DataColumnMapping("Propane", "Propane"), New System.Data.Common.DataColumnMapping("Propylene", "Propylene"), New System.Data.Common.DataColumnMapping("Butane", "Butane"), New System.Data.Common.DataColumnMapping("Isobutane", "Isobutane"), New System.Data.Common.DataColumnMapping("C5Plus", "C5Plus"), New System.Data.Common.DataColumnMapping("CO", "CO"), New System.Data.Common.DataColumnMapping("CO2", "CO2"), New System.Data.Common.DataColumnMapping("N2", "N2"), New System.Data.Common.DataColumnMapping("Inerts", "Inerts"), New System.Data.Common.DataColumnMapping("Total", "Total")})})
        Me.sdEnergy.UpdateCommand = Me.SqlUpdateCommand11
        '
        'SqlDeleteCommand11
        '
        Me.SqlDeleteCommand11.CommandText = "DELETE FROM dbo.Energy WHERE (SubmissionID = @Original_SubmissionID) AND (TransCo" & _
        "de = @Original_TransCode) AND (Butane = @Original_Butane OR @Original_Butane IS " & _
        "NULL AND Butane IS NULL) AND (C5Plus = @Original_C5Plus OR @Original_C5Plus IS N" & _
        "ULL AND C5Plus IS NULL) AND (CO = @Original_CO OR @Original_CO IS NULL AND CO IS" & _
        " NULL) AND (CO2 = @Original_CO2 OR @Original_CO2 IS NULL AND CO2 IS NULL) AND (E" & _
        "nergyType = @Original_EnergyType) AND (Ethane = @Original_Ethane OR @Original_Et" & _
        "hane IS NULL AND Ethane IS NULL) AND (Ethylene = @Original_Ethylene OR @Original" & _
        "_Ethylene IS NULL AND Ethylene IS NULL) AND (Hydrogen = @Original_Hydrogen OR @O" & _
        "riginal_Hydrogen IS NULL AND Hydrogen IS NULL) AND (Inerts = @Original_Inerts OR" & _
        " @Original_Inerts IS NULL AND Inerts IS NULL) AND (Isobutane = @Original_Isobuta" & _
        "ne OR @Original_Isobutane IS NULL AND Isobutane IS NULL) AND (Methane = @Origina" & _
        "l_Methane OR @Original_Methane IS NULL AND Methane IS NULL) AND (N2 = @Original_" & _
        "N2 OR @Original_N2 IS NULL AND N2 IS NULL) AND (PriceMBTULocal = @Original_Price" & _
        "MBTULocal OR @Original_PriceMBTULocal IS NULL AND PriceMBTULocal IS NULL) AND (P" & _
        "riceMBTUUS = @Original_PriceMBTUUS OR @Original_PriceMBTUUS IS NULL AND PriceMBT" & _
        "UUS IS NULL) AND (Propane = @Original_Propane OR @Original_Propane IS NULL AND P" & _
        "ropane IS NULL) AND (Propylene = @Original_Propylene OR @Original_Propylene IS N" & _
        "ULL AND Propylene IS NULL) AND (RptPriceLocal = @Original_RptPriceLocal) AND (Rp" & _
        "tSource = @Original_RptSource) AND (SourceMBTU = @Original_SourceMBTU OR @Origin" & _
        "al_SourceMBTU IS NULL AND SourceMBTU IS NULL) AND (Total = @Original_Total OR @O" & _
        "riginal_Total IS NULL AND Total IS NULL) AND (TransType = @Original_TransType) A" & _
        "ND (TransferTo = @Original_TransferTo OR @Original_TransferTo IS NULL AND Transf" & _
        "erTo IS NULL) AND (UsageMBTU = @Original_UsageMBTU OR @Original_UsageMBTU IS NUL" & _
        "L AND UsageMBTU IS NULL)"
        Me.SqlDeleteCommand11.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransCode", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Butane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Butane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_C5Plus", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "C5Plus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CO", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CO", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ethane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ethane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ethylene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ethylene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Hydrogen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Hydrogen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inerts", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inerts", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Isobutane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Isobutane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Methane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Methane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_N2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "N2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceMBTULocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceMBTULocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceMBTUUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceMBTUUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Propane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Propane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Propylene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Propylene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptPriceLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptPriceLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptSource", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptSource", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SourceMBTU", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SourceMBTU", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransferTo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransferTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UsageMBTU", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UsageMBTU", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand12
        '
        Me.SqlInsertCommand12.CommandText = "INSERT INTO dbo.Energy(SubmissionID, TransCode, EnergyType, TransType, TransferTo" & _
        ", SourceMBTU, UsageMBTU, PriceMBTUUS, PriceMBTULocal, RptSource, RptPriceLocal, " & _
        "Hydrogen, Methane, Ethane, Ethylene, Propane, Propylene, Butane, Isobutane, C5Pl" & _
        "us, CO, CO2, N2, Inerts, Total) VALUES (@SubmissionID, @TransCode, @EnergyType, " & _
        "@TransType, @TransferTo, @SourceMBTU, @UsageMBTU, @PriceMBTUUS, @PriceMBTULocal," & _
        " @RptSource, @RptPriceLocal, @Hydrogen, @Methane, @Ethane, @Ethylene, @Propane, " & _
        "@Propylene, @Butane, @Isobutane, @C5Plus, @CO, @CO2, @N2, @Inerts, @Total)"
        Me.SqlInsertCommand12.Connection = Me.SqlConnection1
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransCode", System.Data.SqlDbType.SmallInt, 2, "TransCode"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyType", System.Data.SqlDbType.VarChar, 3, "EnergyType"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransType", System.Data.SqlDbType.VarChar, 3, "TransType"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransferTo", System.Data.SqlDbType.VarChar, 3, "TransferTo"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SourceMBTU", System.Data.SqlDbType.Float, 8, "SourceMBTU"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UsageMBTU", System.Data.SqlDbType.Float, 8, "UsageMBTU"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceMBTUUS", System.Data.SqlDbType.Real, 4, "PriceMBTUUS"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceMBTULocal", System.Data.SqlDbType.Real, 4, "PriceMBTULocal"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptSource", System.Data.SqlDbType.Float, 8, "RptSource"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptPriceLocal", System.Data.SqlDbType.Real, 4, "RptPriceLocal"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Hydrogen", System.Data.SqlDbType.Real, 4, "Hydrogen"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Methane", System.Data.SqlDbType.Real, 4, "Methane"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ethane", System.Data.SqlDbType.Real, 4, "Ethane"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ethylene", System.Data.SqlDbType.Real, 4, "Ethylene"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Propane", System.Data.SqlDbType.Real, 4, "Propane"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Propylene", System.Data.SqlDbType.Real, 4, "Propylene"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Butane", System.Data.SqlDbType.Real, 4, "Butane"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Isobutane", System.Data.SqlDbType.Real, 4, "Isobutane"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@C5Plus", System.Data.SqlDbType.Real, 4, "C5Plus"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CO", System.Data.SqlDbType.Real, 4, "CO"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CO2", System.Data.SqlDbType.Real, 4, "CO2"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@N2", System.Data.SqlDbType.Real, 4, "N2"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inerts", System.Data.SqlDbType.Real, 4, "Inerts"))
        Me.SqlInsertCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Real, 4, "Total"))
        '
        'SqlSelectCommand12
        '
        Me.SqlSelectCommand12.CommandText = "SELECT SubmissionID, TransCode, EnergyType, TransType, TransferTo, SourceMBTU, Us" & _
        "ageMBTU, PriceMBTUUS, PriceMBTULocal, RptSource, RptPriceLocal, Hydrogen, Methan" & _
        "e, Ethane, Ethylene, Propane, Propylene, Butane, Isobutane, C5Plus, CO, CO2, N2," & _
        " Inerts, Total FROM dbo.Energy"
        Me.SqlSelectCommand12.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand11
        '
        Me.SqlUpdateCommand11.CommandText = "UPDATE dbo.Energy SET SubmissionID = @SubmissionID, TransCode = @TransCode, Energ" & _
        "yType = @EnergyType, TransType = @TransType, TransferTo = @TransferTo, SourceMBT" & _
        "U = @SourceMBTU, UsageMBTU = @UsageMBTU, PriceMBTUUS = @PriceMBTUUS, PriceMBTULo" & _
        "cal = @PriceMBTULocal, RptSource = @RptSource, RptPriceLocal = @RptPriceLocal, H" & _
        "ydrogen = @Hydrogen, Methane = @Methane, Ethane = @Ethane, Ethylene = @Ethylene," & _
        " Propane = @Propane, Propylene = @Propylene, Butane = @Butane, Isobutane = @Isob" & _
        "utane, C5Plus = @C5Plus, CO = @CO, CO2 = @CO2, N2 = @N2, Inerts = @Inerts, Total" & _
        " = @Total WHERE (SubmissionID = @Original_SubmissionID) AND (TransCode = @Origin" & _
        "al_TransCode) AND (Butane = @Original_Butane OR @Original_Butane IS NULL AND But" & _
        "ane IS NULL) AND (C5Plus = @Original_C5Plus OR @Original_C5Plus IS NULL AND C5Pl" & _
        "us IS NULL) AND (CO = @Original_CO OR @Original_CO IS NULL AND CO IS NULL) AND (" & _
        "CO2 = @Original_CO2 OR @Original_CO2 IS NULL AND CO2 IS NULL) AND (EnergyType = " & _
        "@Original_EnergyType) AND (Ethane = @Original_Ethane OR @Original_Ethane IS NULL" & _
        " AND Ethane IS NULL) AND (Ethylene = @Original_Ethylene OR @Original_Ethylene IS" & _
        " NULL AND Ethylene IS NULL) AND (Hydrogen = @Original_Hydrogen OR @Original_Hydr" & _
        "ogen IS NULL AND Hydrogen IS NULL) AND (Inerts = @Original_Inerts OR @Original_I" & _
        "nerts IS NULL AND Inerts IS NULL) AND (Isobutane = @Original_Isobutane OR @Origi" & _
        "nal_Isobutane IS NULL AND Isobutane IS NULL) AND (Methane = @Original_Methane OR" & _
        " @Original_Methane IS NULL AND Methane IS NULL) AND (N2 = @Original_N2 OR @Origi" & _
        "nal_N2 IS NULL AND N2 IS NULL) AND (PriceMBTULocal = @Original_PriceMBTULocal OR" & _
        " @Original_PriceMBTULocal IS NULL AND PriceMBTULocal IS NULL) AND (PriceMBTUUS =" & _
        " @Original_PriceMBTUUS OR @Original_PriceMBTUUS IS NULL AND PriceMBTUUS IS NULL)" & _
        " AND (Propane = @Original_Propane OR @Original_Propane IS NULL AND Propane IS NU" & _
        "LL) AND (Propylene = @Original_Propylene OR @Original_Propylene IS NULL AND Prop" & _
        "ylene IS NULL) AND (RptPriceLocal = @Original_RptPriceLocal) AND (RptSource = @O" & _
        "riginal_RptSource) AND (SourceMBTU = @Original_SourceMBTU OR @Original_SourceMBT" & _
        "U IS NULL AND SourceMBTU IS NULL) AND (Total = @Original_Total OR @Original_Tota" & _
        "l IS NULL AND Total IS NULL) AND (TransType = @Original_TransType) AND (Transfer" & _
        "To = @Original_TransferTo OR @Original_TransferTo IS NULL AND TransferTo IS NULL" & _
        ") AND (UsageMBTU = @Original_UsageMBTU OR @Original_UsageMBTU IS NULL AND UsageM" & _
        "BTU IS NULL)"
        Me.SqlUpdateCommand11.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransCode", System.Data.SqlDbType.SmallInt, 2, "TransCode"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyType", System.Data.SqlDbType.VarChar, 3, "EnergyType"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransType", System.Data.SqlDbType.VarChar, 3, "TransType"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransferTo", System.Data.SqlDbType.VarChar, 3, "TransferTo"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SourceMBTU", System.Data.SqlDbType.Float, 8, "SourceMBTU"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UsageMBTU", System.Data.SqlDbType.Float, 8, "UsageMBTU"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceMBTUUS", System.Data.SqlDbType.Real, 4, "PriceMBTUUS"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceMBTULocal", System.Data.SqlDbType.Real, 4, "PriceMBTULocal"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptSource", System.Data.SqlDbType.Float, 8, "RptSource"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptPriceLocal", System.Data.SqlDbType.Real, 4, "RptPriceLocal"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Hydrogen", System.Data.SqlDbType.Real, 4, "Hydrogen"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Methane", System.Data.SqlDbType.Real, 4, "Methane"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ethane", System.Data.SqlDbType.Real, 4, "Ethane"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ethylene", System.Data.SqlDbType.Real, 4, "Ethylene"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Propane", System.Data.SqlDbType.Real, 4, "Propane"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Propylene", System.Data.SqlDbType.Real, 4, "Propylene"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Butane", System.Data.SqlDbType.Real, 4, "Butane"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Isobutane", System.Data.SqlDbType.Real, 4, "Isobutane"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@C5Plus", System.Data.SqlDbType.Real, 4, "C5Plus"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CO", System.Data.SqlDbType.Real, 4, "CO"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CO2", System.Data.SqlDbType.Real, 4, "CO2"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@N2", System.Data.SqlDbType.Real, 4, "N2"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Inerts", System.Data.SqlDbType.Real, 4, "Inerts"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Real, 4, "Total"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransCode", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Butane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Butane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_C5Plus", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "C5Plus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CO", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CO", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ethane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ethane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Ethylene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ethylene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Hydrogen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Hydrogen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Inerts", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Inerts", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Isobutane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Isobutane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Methane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Methane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_N2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "N2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceMBTULocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceMBTULocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceMBTUUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceMBTUUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Propane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Propane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Propylene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Propylene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptPriceLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptPriceLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptSource", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptSource", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SourceMBTU", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SourceMBTU", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransferTo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransferTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UsageMBTU", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UsageMBTU", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdElectric
        '
        Me.sdElectric.DeleteCommand = Me.SqlDeleteCommand12
        Me.sdElectric.InsertCommand = Me.SqlInsertCommand13
        Me.sdElectric.SelectCommand = Me.SqlSelectCommand13
        Me.sdElectric.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Electric", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("TransCode", "TransCode"), New System.Data.Common.DataColumnMapping("EnergyType", "EnergyType"), New System.Data.Common.DataColumnMapping("TransType", "TransType"), New System.Data.Common.DataColumnMapping("TransferTo", "TransferTo"), New System.Data.Common.DataColumnMapping("RptMWH", "RptMWH"), New System.Data.Common.DataColumnMapping("SourceMWH", "SourceMWH"), New System.Data.Common.DataColumnMapping("UsageMWH", "UsageMWH"), New System.Data.Common.DataColumnMapping("PriceUS", "PriceUS"), New System.Data.Common.DataColumnMapping("GenEff", "GenEff"), New System.Data.Common.DataColumnMapping("PriceLocal", "PriceLocal"), New System.Data.Common.DataColumnMapping("RptGenEff", "RptGenEff")})})
        Me.sdElectric.UpdateCommand = Me.SqlUpdateCommand12
        '
        'SqlDeleteCommand12
        '
        Me.SqlDeleteCommand12.CommandText = "DELETE FROM dbo.Electric WHERE (SubmissionID = @Original_SubmissionID) AND (Trans" & _
        "Code = @Original_TransCode) AND (EnergyType = @Original_EnergyType) AND (GenEff " & _
        "= @Original_GenEff OR @Original_GenEff IS NULL AND GenEff IS NULL) AND (PriceLoc" & _
        "al = @Original_PriceLocal) AND (PriceUS = @Original_PriceUS OR @Original_PriceUS" & _
        " IS NULL AND PriceUS IS NULL) AND (RptGenEff = @Original_RptGenEff OR @Original_" & _
        "RptGenEff IS NULL AND RptGenEff IS NULL) AND (RptMWH = @Original_RptMWH) AND (So" & _
        "urceMWH = @Original_SourceMWH OR @Original_SourceMWH IS NULL AND SourceMWH IS NU" & _
        "LL) AND (TransType = @Original_TransType) AND (TransferTo = @Original_TransferTo" & _
        " OR @Original_TransferTo IS NULL AND TransferTo IS NULL) AND (UsageMWH = @Origin" & _
        "al_UsageMWH OR @Original_UsageMWH IS NULL AND UsageMWH IS NULL)"
        Me.SqlDeleteCommand12.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransCode", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GenEff", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GenEff", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptGenEff", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptGenEff", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptMWH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptMWH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SourceMWH", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SourceMWH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransferTo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransferTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UsageMWH", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UsageMWH", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand13
        '
        Me.SqlInsertCommand13.CommandText = "INSERT INTO dbo.Electric(SubmissionID, TransCode, EnergyType, TransType, Transfer" & _
        "To, RptMWH, SourceMWH, UsageMWH, PriceUS, GenEff, PriceLocal, RptGenEff) VALUES " & _
        "(@SubmissionID, @TransCode, @EnergyType, @TransType, @TransferTo, @RptMWH, @Sour" & _
        "ceMWH, @UsageMWH, @PriceUS, @GenEff, @PriceLocal, @RptGenEff)"
        Me.SqlInsertCommand13.Connection = Me.SqlConnection1
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransCode", System.Data.SqlDbType.SmallInt, 2, "TransCode"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyType", System.Data.SqlDbType.VarChar, 3, "EnergyType"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransType", System.Data.SqlDbType.VarChar, 3, "TransType"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransferTo", System.Data.SqlDbType.VarChar, 3, "TransferTo"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptMWH", System.Data.SqlDbType.Real, 4, "RptMWH"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SourceMWH", System.Data.SqlDbType.Float, 8, "SourceMWH"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UsageMWH", System.Data.SqlDbType.Float, 8, "UsageMWH"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceUS", System.Data.SqlDbType.Real, 4, "PriceUS"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GenEff", System.Data.SqlDbType.Real, 4, "GenEff"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceLocal", System.Data.SqlDbType.Real, 4, "PriceLocal"))
        Me.SqlInsertCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptGenEff", System.Data.SqlDbType.Real, 4, "RptGenEff"))
        '
        'SqlSelectCommand13
        '
        Me.SqlSelectCommand13.CommandText = "SELECT SubmissionID, TransCode, EnergyType, TransType, TransferTo, RptMWH, Source" & _
        "MWH, UsageMWH, PriceUS, GenEff, PriceLocal, RptGenEff FROM dbo.Electric"
        Me.SqlSelectCommand13.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand12
        '
        Me.SqlUpdateCommand12.CommandText = "UPDATE dbo.Electric SET SubmissionID = @SubmissionID, TransCode = @TransCode, Ene" & _
        "rgyType = @EnergyType, TransType = @TransType, TransferTo = @TransferTo, RptMWH " & _
        "= @RptMWH, SourceMWH = @SourceMWH, UsageMWH = @UsageMWH, PriceUS = @PriceUS, Gen" & _
        "Eff = @GenEff, PriceLocal = @PriceLocal, RptGenEff = @RptGenEff WHERE (Submissio" & _
        "nID = @Original_SubmissionID) AND (TransCode = @Original_TransCode) AND (EnergyT" & _
        "ype = @Original_EnergyType) AND (GenEff = @Original_GenEff OR @Original_GenEff I" & _
        "S NULL AND GenEff IS NULL) AND (PriceLocal = @Original_PriceLocal) AND (PriceUS " & _
        "= @Original_PriceUS OR @Original_PriceUS IS NULL AND PriceUS IS NULL) AND (RptGe" & _
        "nEff = @Original_RptGenEff OR @Original_RptGenEff IS NULL AND RptGenEff IS NULL)" & _
        " AND (RptMWH = @Original_RptMWH) AND (SourceMWH = @Original_SourceMWH OR @Origin" & _
        "al_SourceMWH IS NULL AND SourceMWH IS NULL) AND (TransType = @Original_TransType" & _
        ") AND (TransferTo = @Original_TransferTo OR @Original_TransferTo IS NULL AND Tra" & _
        "nsferTo IS NULL) AND (UsageMWH = @Original_UsageMWH OR @Original_UsageMWH IS NUL" & _
        "L AND UsageMWH IS NULL)"
        Me.SqlUpdateCommand12.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransCode", System.Data.SqlDbType.SmallInt, 2, "TransCode"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyType", System.Data.SqlDbType.VarChar, 3, "EnergyType"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransType", System.Data.SqlDbType.VarChar, 3, "TransType"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TransferTo", System.Data.SqlDbType.VarChar, 3, "TransferTo"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptMWH", System.Data.SqlDbType.Real, 4, "RptMWH"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SourceMWH", System.Data.SqlDbType.Float, 8, "SourceMWH"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UsageMWH", System.Data.SqlDbType.Float, 8, "UsageMWH"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceUS", System.Data.SqlDbType.Real, 4, "PriceUS"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GenEff", System.Data.SqlDbType.Real, 4, "GenEff"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PriceLocal", System.Data.SqlDbType.Real, 4, "PriceLocal"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptGenEff", System.Data.SqlDbType.Real, 4, "RptGenEff"))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransCode", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GenEff", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GenEff", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PriceUS", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PriceUS", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptGenEff", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptGenEff", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptMWH", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptMWH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SourceMWH", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SourceMWH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TransferTo", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TransferTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UsageMWH", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UsageMWH", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdConfigRS
        '
        Me.sdConfigRS.DeleteCommand = Me.SqlDeleteCommand2
        Me.sdConfigRS.InsertCommand = Me.SqlInsertCommand3
        Me.sdConfigRS.SelectCommand = Me.SqlSelectCommand3
        Me.sdConfigRS.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ConfigRS", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("UnitID", "UnitID"), New System.Data.Common.DataColumnMapping("ProcessID", "ProcessID"), New System.Data.Common.DataColumnMapping("ProcessType", "ProcessType"), New System.Data.Common.DataColumnMapping("AvgSize", "AvgSize"), New System.Data.Common.DataColumnMapping("Throughput", "Throughput"), New System.Data.Common.DataColumnMapping("Vessels", "Vessels"), New System.Data.Common.DataColumnMapping("BPD", "BPD"), New System.Data.Common.DataColumnMapping("PcntOwnership", "PcntOwnership")})})
        Me.sdConfigRS.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM dbo.ConfigRS WHERE (SubmissionID = @Original_SubmissionID) AND (UnitI" & _
        "D = @Original_UnitID) AND (AvgSize = @Original_AvgSize OR @Original_AvgSize IS N" & _
        "ULL AND AvgSize IS NULL) AND (BPD = @Original_BPD OR @Original_BPD IS NULL AND B" & _
        "PD IS NULL) AND (PcntOwnership = @Original_PcntOwnership OR @Original_PcntOwners" & _
        "hip IS NULL AND PcntOwnership IS NULL) AND (ProcessID = @Original_ProcessID) AND" & _
        " (ProcessType = @Original_ProcessType) AND (Throughput = @Original_Throughput OR" & _
        " @Original_Throughput IS NULL AND Throughput IS NULL) AND (Vessels = @Original_V" & _
        "essels OR @Original_Vessels IS NULL AND Vessels IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgSize", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgSize", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BPD", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BPD", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PcntOwnership", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "PcntOwnership", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessType", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Throughput", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Throughput", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Vessels", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Vessels", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO dbo.ConfigRS(SubmissionID, UnitID, ProcessID, ProcessType, AvgSize, T" & _
        "hroughput, Vessels, BPD, PcntOwnership) VALUES (@SubmissionID, @UnitID, @Process" & _
        "ID, @ProcessType, @AvgSize, @Throughput, @Vessels, @BPD, @PcntOwnership)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessType", System.Data.SqlDbType.VarChar, 4, "ProcessType"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgSize", System.Data.SqlDbType.Real, 4, "AvgSize"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Throughput", System.Data.SqlDbType.Real, 4, "Throughput"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Vessels", System.Data.SqlDbType.Real, 4, "Vessels"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BPD", System.Data.SqlDbType.Real, 4, "BPD"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PcntOwnership", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "PcntOwnership", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT SubmissionID, UnitID, ProcessID, ProcessType, AvgSize, Throughput, Vessels" & _
        ", BPD, PcntOwnership FROM dbo.ConfigRS"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE dbo.ConfigRS SET SubmissionID = @SubmissionID, UnitID = @UnitID, ProcessID" & _
        " = @ProcessID, ProcessType = @ProcessType, AvgSize = @AvgSize, Throughput = @Thr" & _
        "oughput, Vessels = @Vessels, BPD = @BPD, PcntOwnership = @PcntOwnership WHERE (S" & _
        "ubmissionID = @Original_SubmissionID) AND (UnitID = @Original_UnitID) AND (AvgSi" & _
        "ze = @Original_AvgSize OR @Original_AvgSize IS NULL AND AvgSize IS NULL) AND (BP" & _
        "D = @Original_BPD OR @Original_BPD IS NULL AND BPD IS NULL) AND (PcntOwnership =" & _
        " @Original_PcntOwnership OR @Original_PcntOwnership IS NULL AND PcntOwnership IS" & _
        " NULL) AND (ProcessID = @Original_ProcessID) AND (ProcessType = @Original_Proces" & _
        "sType) AND (Throughput = @Original_Throughput OR @Original_Throughput IS NULL AN" & _
        "D Throughput IS NULL) AND (Vessels = @Original_Vessels OR @Original_Vessels IS N" & _
        "ULL AND Vessels IS NULL)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessType", System.Data.SqlDbType.VarChar, 4, "ProcessType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvgSize", System.Data.SqlDbType.Real, 4, "AvgSize"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Throughput", System.Data.SqlDbType.Real, 4, "Throughput"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Vessels", System.Data.SqlDbType.Real, 4, "Vessels"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BPD", System.Data.SqlDbType.Real, 4, "BPD"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PcntOwnership", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "PcntOwnership", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvgSize", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvgSize", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BPD", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BPD", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PcntOwnership", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "PcntOwnership", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessType", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Throughput", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Throughput", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Vessels", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Vessels", System.Data.DataRowVersion.Original, Nothing))
        '
        'scCompleteSubmission
        '
        Me.scCompleteSubmission.CommandText = "dbo.[spCompleteSubmission]"
        Me.scCompleteSubmission.CommandType = System.Data.CommandType.StoredProcedure
        Me.scCompleteSubmission.Connection = Me.SqlConnection1
        Me.scCompleteSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.scCompleteSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4))
        '
        'sdMaterialCategory_LU
        '
        Me.sdMaterialCategory_LU.DeleteCommand = Me.SqlDeleteCommand24
        Me.sdMaterialCategory_LU.InsertCommand = Me.SqlInsertCommand18
        Me.sdMaterialCategory_LU.SelectCommand = Me.SqlSelectCommand18
        Me.sdMaterialCategory_LU.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MaterialCategory_LU", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Category", "Category"), New System.Data.Common.DataColumnMapping("CategoryName", "CategoryName"), New System.Data.Common.DataColumnMapping("GrossGroup", "GrossGroup"), New System.Data.Common.DataColumnMapping("SortKey", "SortKey"), New System.Data.Common.DataColumnMapping("NetGroup", "NetGroup")})})
        Me.sdMaterialCategory_LU.UpdateCommand = Me.SqlUpdateCommand25
        '
        'SqlDeleteCommand24
        '
        Me.SqlDeleteCommand24.CommandText = "DELETE FROM dbo.MaterialCategory_LU WHERE (Category = @Original_Category) AND (Ca" & _
        "tegoryName = @Original_CategoryName OR @Original_CategoryName IS NULL AND Catego" & _
        "ryName IS NULL) AND (GrossGroup = @Original_GrossGroup OR @Original_GrossGroup I" & _
        "S NULL AND GrossGroup IS NULL) AND (NetGroup = @Original_NetGroup OR @Original_N" & _
        "etGroup IS NULL AND NetGroup IS NULL)"
        Me.SqlDeleteCommand24.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Category", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Category", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CategoryName", System.Data.SqlDbType.VarChar, 40, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CategoryName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GrossGroup", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrossGroup", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NetGroup", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NetGroup", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand18
        '
        Me.SqlInsertCommand18.CommandText = "INSERT INTO dbo.MaterialCategory_LU(Category, CategoryName, GrossGroup, NetGroup)" & _
        " VALUES (@Category, @CategoryName, @GrossGroup, @NetGroup)"
        Me.SqlInsertCommand18.Connection = Me.SqlConnection1
        Me.SqlInsertCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Category", System.Data.SqlDbType.VarChar, 5, "Category"))
        Me.SqlInsertCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CategoryName", System.Data.SqlDbType.VarChar, 40, "CategoryName"))
        Me.SqlInsertCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GrossGroup", System.Data.SqlDbType.VarChar, 2, "GrossGroup"))
        Me.SqlInsertCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NetGroup", System.Data.SqlDbType.VarChar, 2, "NetGroup"))
        '
        'SqlSelectCommand18
        '
        Me.SqlSelectCommand18.CommandText = "SELECT Category, CategoryName, GrossGroup, SortKey * 100 AS SortKey, NetGroup FRO" & _
        "M dbo.MaterialCategory_LU ORDER BY SortKey"
        Me.SqlSelectCommand18.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand25
        '
        Me.SqlUpdateCommand25.CommandText = "UPDATE dbo.MaterialCategory_LU SET Category = @Category, CategoryName = @Category" & _
        "Name, GrossGroup = @GrossGroup, NetGroup = @NetGroup WHERE (Category = @Original" & _
        "_Category) AND (CategoryName = @Original_CategoryName OR @Original_CategoryName " & _
        "IS NULL AND CategoryName IS NULL) AND (GrossGroup = @Original_GrossGroup OR @Ori" & _
        "ginal_GrossGroup IS NULL AND GrossGroup IS NULL) AND (NetGroup = @Original_NetGr" & _
        "oup OR @Original_NetGroup IS NULL AND NetGroup IS NULL)"
        Me.SqlUpdateCommand25.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Category", System.Data.SqlDbType.VarChar, 5, "Category"))
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CategoryName", System.Data.SqlDbType.VarChar, 40, "CategoryName"))
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GrossGroup", System.Data.SqlDbType.VarChar, 2, "GrossGroup"))
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NetGroup", System.Data.SqlDbType.VarChar, 2, "NetGroup"))
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Category", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Category", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CategoryName", System.Data.SqlDbType.VarChar, 40, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CategoryName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GrossGroup", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GrossGroup", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NetGroup", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NetGroup", System.Data.DataRowVersion.Original, Nothing))
        '
        'DsMaintCat1
        '
        Me.DsMaintCat1.DataSetName = "dsMaintCat"
        Me.DsMaintCat1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'sdLoadRoutHist
        '
        Me.sdLoadRoutHist.DeleteCommand = Me.SqlDeleteCommand18
        Me.sdLoadRoutHist.InsertCommand = Me.SqlInsertCommand19
        Me.sdLoadRoutHist.SelectCommand = Me.SqlSelectCommand19
        Me.sdLoadRoutHist.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "LoadRoutHist", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefineryID", "RefineryID"), New System.Data.Common.DataColumnMapping("DataSet", "DataSet"), New System.Data.Common.DataColumnMapping("PeriodStart", "PeriodStart"), New System.Data.Common.DataColumnMapping("RoutCostLocal", "RoutCostLocal"), New System.Data.Common.DataColumnMapping("RoutMatlLocal", "RoutMatlLocal")})})
        Me.sdLoadRoutHist.UpdateCommand = Me.SqlUpdateCommand18
        '
        'SqlDeleteCommand18
        '
        Me.SqlDeleteCommand18.CommandText = "DELETE FROM dbo.LoadRoutHist WHERE (DataSet = @Original_DataSet) AND (PeriodStart" & _
        " = @Original_PeriodStart) AND (RefineryID = @Original_RefineryID) AND (RoutCostL" & _
        "ocal = @Original_RoutCostLocal OR @Original_RoutCostLocal IS NULL AND RoutCostLo" & _
        "cal IS NULL) AND (RoutMatlLocal = @Original_RoutMatlLocal OR @Original_RoutMatlL" & _
        "ocal IS NULL AND RoutMatlLocal IS NULL)"
        Me.SqlDeleteCommand18.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodStart", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodStart", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCostLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCostLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlLocal", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand19
        '
        Me.SqlInsertCommand19.CommandText = "INSERT INTO dbo.LoadRoutHist(RefineryID, DataSet, PeriodStart, RoutCostLocal, Rou" & _
        "tMatlLocal) VALUES (@RefineryID, @DataSet, @PeriodStart, @RoutCostLocal, @RoutMa" & _
        "tlLocal)"
        Me.SqlInsertCommand19.Connection = Me.SqlConnection1
        Me.SqlInsertCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlInsertCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlInsertCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodStart", System.Data.SqlDbType.DateTime, 4, "PeriodStart"))
        Me.SqlInsertCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCostLocal", System.Data.SqlDbType.Real, 4, "RoutCostLocal"))
        Me.SqlInsertCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlLocal", System.Data.SqlDbType.Real, 4, "RoutMatlLocal"))
        '
        'SqlSelectCommand19
        '
        Me.SqlSelectCommand19.CommandText = "SELECT RefineryID, DataSet, PeriodStart, RoutCostLocal, RoutMatlLocal FROM dbo.Lo" & _
        "adRoutHist"
        Me.SqlSelectCommand19.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand18
        '
        Me.SqlUpdateCommand18.CommandText = "UPDATE dbo.LoadRoutHist SET RefineryID = @RefineryID, DataSet = @DataSet, PeriodS" & _
        "tart = @PeriodStart, RoutCostLocal = @RoutCostLocal, RoutMatlLocal = @RoutMatlLo" & _
        "cal WHERE (DataSet = @Original_DataSet) AND (PeriodStart = @Original_PeriodStart" & _
        ") AND (RefineryID = @Original_RefineryID) AND (RoutCostLocal = @Original_RoutCos" & _
        "tLocal OR @Original_RoutCostLocal IS NULL AND RoutCostLocal IS NULL) AND (RoutMa" & _
        "tlLocal = @Original_RoutMatlLocal OR @Original_RoutMatlLocal IS NULL AND RoutMat" & _
        "lLocal IS NULL)"
        Me.SqlUpdateCommand18.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodStart", System.Data.SqlDbType.DateTime, 4, "PeriodStart"))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutCostLocal", System.Data.SqlDbType.Real, 4, "RoutCostLocal"))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RoutMatlLocal", System.Data.SqlDbType.Real, 4, "RoutMatlLocal"))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PeriodStart", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PeriodStart", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutCostLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutCostLocal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand18.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RoutMatlLocal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RoutMatlLocal", System.Data.DataRowVersion.Original, Nothing))
        '
        'scStartUpload
        '
        Me.scStartUpload.CommandText = "dbo.[StartUpload]"
        Me.scStartUpload.CommandType = System.Data.CommandType.StoredProcedure
        Me.scStartUpload.Connection = Me.SqlConnection1
        Me.scStartUpload.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.scStartUpload.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6))
        Me.scStartUpload.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15))
        Me.scStartUpload.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OK", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Output, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        '
        'scClearUploading
        '
        Me.scClearUploading.CommandText = "dbo.[spClearUploading]"
        Me.scClearUploading.CommandType = System.Data.CommandType.StoredProcedure
        Me.scClearUploading.Connection = Me.SqlConnection1
        Me.scClearUploading.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.scClearUploading.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4))
        '
        'scSubmissionID
        '
        Me.scSubmissionID.CommandText = "dbo.[spSubmissionID]"
        Me.scSubmissionID.CommandType = System.Data.CommandType.StoredProcedure
        Me.scSubmissionID.Connection = Me.SqlConnection1
        Me.scSubmissionID.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.scSubmissionID.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6))
        Me.scSubmissionID.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Dataset", System.Data.SqlDbType.VarChar, 15))
        Me.scSubmissionID.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodStart", System.Data.SqlDbType.DateTime, 8))
        '
        'sdUserDefined
        '
        Me.sdUserDefined.DeleteCommand = Me.SqlDeleteCommand19
        Me.sdUserDefined.InsertCommand = Me.SqlInsertCommand20
        Me.sdUserDefined.SelectCommand = Me.SqlSelectCommand20
        Me.sdUserDefined.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "UserDefined", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("HeaderText", "HeaderText"), New System.Data.Common.DataColumnMapping("VariableDesc", "VariableDesc"), New System.Data.Common.DataColumnMapping("RptValue", "RptValue"), New System.Data.Common.DataColumnMapping("RptValue_Target", "RptValue_Target"), New System.Data.Common.DataColumnMapping("RptValue_Avg", "RptValue_Avg"), New System.Data.Common.DataColumnMapping("RptValue_YTD", "RptValue_YTD"), New System.Data.Common.DataColumnMapping("DecPlaces", "DecPlaces")})})
        Me.sdUserDefined.UpdateCommand = Me.SqlUpdateCommand19
        '
        'SqlDeleteCommand19
        '
        Me.SqlDeleteCommand19.CommandText = "DELETE FROM dbo.UserDefined WHERE (HeaderText = @Original_HeaderText) AND (Submis" & _
        "sionID = @Original_SubmissionID) AND (VariableDesc = @Original_VariableDesc) AND" & _
        " (DecPlaces = @Original_DecPlaces OR @Original_DecPlaces IS NULL AND DecPlaces I" & _
        "S NULL) AND (RptValue = @Original_RptValue OR @Original_RptValue IS NULL AND Rpt" & _
        "Value IS NULL) AND (RptValue_Avg = @Original_RptValue_Avg OR @Original_RptValue_" & _
        "Avg IS NULL AND RptValue_Avg IS NULL) AND (RptValue_Target = @Original_RptValue_" & _
        "Target OR @Original_RptValue_Target IS NULL AND RptValue_Target IS NULL) AND (Rp" & _
        "tValue_YTD = @Original_RptValue_YTD OR @Original_RptValue_YTD IS NULL AND RptVal" & _
        "ue_YTD IS NULL)"
        Me.SqlDeleteCommand19.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_HeaderText", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "HeaderText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VariableDesc", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VariableDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DecPlaces", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DecPlaces", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue_Avg", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue_Avg", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue_Target", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue_Target", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue_YTD", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue_YTD", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand20
        '
        Me.SqlInsertCommand20.CommandText = "INSERT INTO dbo.UserDefined(SubmissionID, HeaderText, VariableDesc, RptValue, Rpt" & _
        "Value_Target, RptValue_Avg, RptValue_YTD, DecPlaces) VALUES (@SubmissionID, @Hea" & _
        "derText, @VariableDesc, @RptValue, @RptValue_Target, @RptValue_Avg, @RptValue_YT" & _
        "D, @DecPlaces)"
        Me.SqlInsertCommand20.Connection = Me.SqlConnection1
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HeaderText", System.Data.SqlDbType.VarChar, 50, "HeaderText"))
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VariableDesc", System.Data.SqlDbType.VarChar, 100, "VariableDesc"))
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue", System.Data.SqlDbType.Float, 8, "RptValue"))
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue_Target", System.Data.SqlDbType.Float, 8, "RptValue_Target"))
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue_Avg", System.Data.SqlDbType.Float, 8, "RptValue_Avg"))
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue_YTD", System.Data.SqlDbType.Float, 8, "RptValue_YTD"))
        Me.SqlInsertCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DecPlaces", System.Data.SqlDbType.TinyInt, 1, "DecPlaces"))
        '
        'SqlSelectCommand20
        '
        Me.SqlSelectCommand20.CommandText = "SELECT SubmissionID, HeaderText, VariableDesc, RptValue, RptValue_Target, RptValu" & _
        "e_Avg, RptValue_YTD, DecPlaces FROM dbo.UserDefined"
        Me.SqlSelectCommand20.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand19
        '
        Me.SqlUpdateCommand19.CommandText = "UPDATE dbo.UserDefined SET SubmissionID = @SubmissionID, HeaderText = @HeaderText" & _
        ", VariableDesc = @VariableDesc, RptValue = @RptValue, RptValue_Target = @RptValu" & _
        "e_Target, RptValue_Avg = @RptValue_Avg, RptValue_YTD = @RptValue_YTD, DecPlaces " & _
        "= @DecPlaces WHERE (HeaderText = @Original_HeaderText) AND (SubmissionID = @Orig" & _
        "inal_SubmissionID) AND (VariableDesc = @Original_VariableDesc) AND (DecPlaces = " & _
        "@Original_DecPlaces OR @Original_DecPlaces IS NULL AND DecPlaces IS NULL) AND (R" & _
        "ptValue = @Original_RptValue OR @Original_RptValue IS NULL AND RptValue IS NULL)" & _
        " AND (RptValue_Avg = @Original_RptValue_Avg OR @Original_RptValue_Avg IS NULL AN" & _
        "D RptValue_Avg IS NULL) AND (RptValue_Target = @Original_RptValue_Target OR @Ori" & _
        "ginal_RptValue_Target IS NULL AND RptValue_Target IS NULL) AND (RptValue_YTD = @" & _
        "Original_RptValue_YTD OR @Original_RptValue_YTD IS NULL AND RptValue_YTD IS NULL" & _
        ")"
        Me.SqlUpdateCommand19.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HeaderText", System.Data.SqlDbType.VarChar, 50, "HeaderText"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VariableDesc", System.Data.SqlDbType.VarChar, 100, "VariableDesc"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue", System.Data.SqlDbType.Float, 8, "RptValue"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue_Target", System.Data.SqlDbType.Float, 8, "RptValue_Target"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue_Avg", System.Data.SqlDbType.Float, 8, "RptValue_Avg"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptValue_YTD", System.Data.SqlDbType.Float, 8, "RptValue_YTD"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DecPlaces", System.Data.SqlDbType.TinyInt, 1, "DecPlaces"))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_HeaderText", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "HeaderText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VariableDesc", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VariableDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DecPlaces", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DecPlaces", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue_Avg", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue_Avg", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue_Target", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue_Target", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand19.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptValue_YTD", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptValue_YTD", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdOpexAdd
        '
        Me.sdOpexAdd.DeleteCommand = Me.SqlDeleteCommand20
        Me.sdOpexAdd.InsertCommand = Me.SqlInsertCommand21
        Me.sdOpexAdd.SelectCommand = Me.SqlSelectCommand21
        Me.sdOpexAdd.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpexAdd", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("OCCBenAbs", "OCCBenAbs"), New System.Data.Common.DataColumnMapping("OCCBenInsur", "OCCBenInsur"), New System.Data.Common.DataColumnMapping("OCCBenPension", "OCCBenPension"), New System.Data.Common.DataColumnMapping("OCCBenSub", "OCCBenSub"), New System.Data.Common.DataColumnMapping("OCCBenStock", "OCCBenStock"), New System.Data.Common.DataColumnMapping("OCCBenTaxPen", "OCCBenTaxPen"), New System.Data.Common.DataColumnMapping("OCCBenTaxMed", "OCCBenTaxMed"), New System.Data.Common.DataColumnMapping("OCCBenTaxOth", "OCCBenTaxOth"), New System.Data.Common.DataColumnMapping("MPSBenAbs", "MPSBenAbs"), New System.Data.Common.DataColumnMapping("MPSBenInsur", "MPSBenInsur"), New System.Data.Common.DataColumnMapping("MPSBenPension", "MPSBenPension"), New System.Data.Common.DataColumnMapping("MPSBenSub", "MPSBenSub"), New System.Data.Common.DataColumnMapping("MPSBenStock", "MPSBenStock"), New System.Data.Common.DataColumnMapping("MPSBenTaxPen", "MPSBenTaxPen"), New System.Data.Common.DataColumnMapping("MPSBenTaxMed", "MPSBenTaxMed"), New System.Data.Common.DataColumnMapping("MPSBenTaxOth", "MPSBenTaxOth"), New System.Data.Common.DataColumnMapping("MaintMatl", "MaintMatl"), New System.Data.Common.DataColumnMapping("ContMaintMatl", "ContMaintMatl"), New System.Data.Common.DataColumnMapping("EquipMaint", "EquipMaint"), New System.Data.Common.DataColumnMapping("OthContProcOp", "OthContProcOp"), New System.Data.Common.DataColumnMapping("OthContTransOp", "OthContTransOp"), New System.Data.Common.DataColumnMapping("OthContFire", "OthContFire"), New System.Data.Common.DataColumnMapping("OthContVacTrucks", "OthContVacTrucks"), New System.Data.Common.DataColumnMapping("OthContConsult", "OthContConsult"), New System.Data.Common.DataColumnMapping("OthContInsp", "OthContInsp"), New System.Data.Common.DataColumnMapping("OthContSecurity", "OthContSecurity"), New System.Data.Common.DataColumnMapping("OthContComputing", "OthContComputing"), New System.Data.Common.DataColumnMapping("OthContJan", "OthContJan"), New System.Data.Common.DataColumnMapping("OthContLab", "OthContLab"), New System.Data.Common.DataColumnMapping("OthContFoodSvc", "OthContFoodSvc"), New System.Data.Common.DataColumnMapping("OthContAdmin", "OthContAdmin"), New System.Data.Common.DataColumnMapping("OthContLegal", "OthContLegal"), New System.Data.Common.DataColumnMapping("OthContOth", "OthContOth"), New System.Data.Common.DataColumnMapping("EnvirDisp", "EnvirDisp"), New System.Data.Common.DataColumnMapping("EnvirPermits", "EnvirPermits"), New System.Data.Common.DataColumnMapping("EnvirFines", "EnvirFines"), New System.Data.Common.DataColumnMapping("EnvirSpill", "EnvirSpill"), New System.Data.Common.DataColumnMapping("EnvirLab", "EnvirLab"), New System.Data.Common.DataColumnMapping("EnvirEng", "EnvirEng"), New System.Data.Common.DataColumnMapping("EnvirOth", "EnvirOth"), New System.Data.Common.DataColumnMapping("EquipNonMaint", "EquipNonMaint"), New System.Data.Common.DataColumnMapping("Tax", "Tax"), New System.Data.Common.DataColumnMapping("Insur", "Insur"), New System.Data.Common.DataColumnMapping("OthNonVolSupply", "OthNonVolSupply"), New System.Data.Common.DataColumnMapping("OthNonVolSafety", "OthNonVolSafety"), New System.Data.Common.DataColumnMapping("OthNonVolComm", "OthNonVolComm"), New System.Data.Common.DataColumnMapping("OthNonVolDonations", "OthNonVolDonations"), New System.Data.Common.DataColumnMapping("OthNonVolDues", "OthNonVolDues"), New System.Data.Common.DataColumnMapping("OthNonVolTravel", "OthNonVolTravel"), New System.Data.Common.DataColumnMapping("OthNonVolTrain", "OthNonVolTrain"), New System.Data.Common.DataColumnMapping("OthNonVolComputer", "OthNonVolComputer"), New System.Data.Common.DataColumnMapping("OthNonVolTanks", "OthNonVolTanks"), New System.Data.Common.DataColumnMapping("OthNonVolOth", "OthNonVolOth"), New System.Data.Common.DataColumnMapping("ChemicalsAlkyAcid", "ChemicalsAlkyAcid"), New System.Data.Common.DataColumnMapping("ChemicalsLube", "ChemicalsLube"), New System.Data.Common.DataColumnMapping("ChemicalsH2OTreat", "ChemicalsH2OTreat"), New System.Data.Common.DataColumnMapping("ChemicalsProcess", "ChemicalsProcess"), New System.Data.Common.DataColumnMapping("ChemicalsOthAcid", "ChemicalsOthAcid"), New System.Data.Common.DataColumnMapping("ChemicalsGasAdd", "ChemicalsGasAdd"), New System.Data.Common.DataColumnMapping("ChemicalsDieselAdd", "ChemicalsDieselAdd"), New System.Data.Common.DataColumnMapping("ChemicalsOthAdd", "ChemicalsOthAdd"), New System.Data.Common.DataColumnMapping("ChemicalsO2", "ChemicalsO2"), New System.Data.Common.DataColumnMapping("ChemicalsClay", "ChemicalsClay"), New System.Data.Common.DataColumnMapping("ChemicalsAmines", "ChemicalsAmines"), New System.Data.Common.DataColumnMapping("ChemicalsASESolv", "ChemicalsASESolv"), New System.Data.Common.DataColumnMapping("ChemicalsWasteH2O", "ChemicalsWasteH2O"), New System.Data.Common.DataColumnMapping("ChemicalsNMP", "ChemicalsNMP"), New System.Data.Common.DataColumnMapping("ChemicalsFurfural", "ChemicalsFurfural"), New System.Data.Common.DataColumnMapping("ChemicalsMIBK", "ChemicalsMIBK"), New System.Data.Common.DataColumnMapping("ChemicalsMEK", "ChemicalsMEK"), New System.Data.Common.DataColumnMapping("ChemicalsToluene", "ChemicalsToluene"), New System.Data.Common.DataColumnMapping("ChemicalsPropane", "ChemicalsPropane"), New System.Data.Common.DataColumnMapping("ChemicalsOthSolv", "ChemicalsOthSolv"), New System.Data.Common.DataColumnMapping("ChemicalsDewaxAids", "ChemicalsDewaxAids"), New System.Data.Common.DataColumnMapping("ChemicalsOth", "ChemicalsOth"), New System.Data.Common.DataColumnMapping("CatalystsFCC", "CatalystsFCC"), New System.Data.Common.DataColumnMapping("CatalystsHYC", "CatalystsHYC"), New System.Data.Common.DataColumnMapping("CatalystsNKSHYT", "CatalystsNKSHYT"), New System.Data.Common.DataColumnMapping("CatalystsDHYT", "CatalystsDHYT"), New System.Data.Common.DataColumnMapping("CatalystsVHYT", "CatalystsVHYT"), New System.Data.Common.DataColumnMapping("CatalystsRHYT", "CatalystsRHYT"), New System.Data.Common.DataColumnMapping("CatalystsHYFT", "CatalystsHYFT"), New System.Data.Common.DataColumnMapping("CatalystsCDWAX", "CatalystsCDWAX"), New System.Data.Common.DataColumnMapping("CatalystsREF", "CatalystsREF"), New System.Data.Common.DataColumnMapping("CatalystsHYG", "CatalystsHYG"), New System.Data.Common.DataColumnMapping("CatalystsS2Plant", "CatalystsS2Plant"), New System.Data.Common.DataColumnMapping("CatalystsPetChem", "CatalystsPetChem"), New System.Data.Common.DataColumnMapping("CatalystsOth", "CatalystsOth"), New System.Data.Common.DataColumnMapping("PurOthN2", "PurOthN2"), New System.Data.Common.DataColumnMapping("PurOthH2O", "PurOthH2O"), New System.Data.Common.DataColumnMapping("PurOthOth", "PurOthOth"), New System.Data.Common.DataColumnMapping("Royalties", "Royalties"), New System.Data.Common.DataColumnMapping("OthVolDemCrude", "OthVolDemCrude"), New System.Data.Common.DataColumnMapping("OthVolDemLightering", "OthVolDemLightering"), New System.Data.Common.DataColumnMapping("OthVolDemProd", "OthVolDemProd"), New System.Data.Common.DataColumnMapping("EmissionsTaxes", "EmissionsTaxes"), New System.Data.Common.DataColumnMapping("EmissionsPurch", "EmissionsPurch"), New System.Data.Common.DataColumnMapping("EmissionsCredits", "EmissionsCredits"), New System.Data.Common.DataColumnMapping("OthVolOth", "OthVolOth"), New System.Data.Common.DataColumnMapping("Currency", "Currency"), New System.Data.Common.DataColumnMapping("DataType", "DataType"), New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID")})})
        Me.sdOpexAdd.UpdateCommand = Me.SqlUpdateCommand20
        '
        'SqlDeleteCommand20
        '
        Me.SqlDeleteCommand20.CommandText = "DELETE FROM dbo.OpexAdd WHERE (Currency = @Original_Currency) AND (DataType = @Or" & _
        "iginal_DataType) AND (SubmissionID = @Original_SubmissionID) AND (CatalystsCDWAX" & _
        " = @Original_CatalystsCDWAX OR @Original_CatalystsCDWAX IS NULL AND CatalystsCDW" & _
        "AX IS NULL) AND (CatalystsDHYT = @Original_CatalystsDHYT OR @Original_CatalystsD" & _
        "HYT IS NULL AND CatalystsDHYT IS NULL) AND (CatalystsFCC = @Original_CatalystsFC" & _
        "C OR @Original_CatalystsFCC IS NULL AND CatalystsFCC IS NULL) AND (CatalystsHYC " & _
        "= @Original_CatalystsHYC OR @Original_CatalystsHYC IS NULL AND CatalystsHYC IS N" & _
        "ULL) AND (CatalystsHYFT = @Original_CatalystsHYFT OR @Original_CatalystsHYFT IS " & _
        "NULL AND CatalystsHYFT IS NULL) AND (CatalystsHYG = @Original_CatalystsHYG OR @O" & _
        "riginal_CatalystsHYG IS NULL AND CatalystsHYG IS NULL) AND (CatalystsNKSHYT = @O" & _
        "riginal_CatalystsNKSHYT OR @Original_CatalystsNKSHYT IS NULL AND CatalystsNKSHYT" & _
        " IS NULL) AND (CatalystsOth = @Original_CatalystsOth OR @Original_CatalystsOth I" & _
        "S NULL AND CatalystsOth IS NULL) AND (CatalystsPetChem = @Original_CatalystsPetC" & _
        "hem OR @Original_CatalystsPetChem IS NULL AND CatalystsPetChem IS NULL) AND (Cat" & _
        "alystsREF = @Original_CatalystsREF OR @Original_CatalystsREF IS NULL AND Catalys" & _
        "tsREF IS NULL) AND (CatalystsRHYT = @Original_CatalystsRHYT OR @Original_Catalys" & _
        "tsRHYT IS NULL AND CatalystsRHYT IS NULL) AND (CatalystsS2Plant = @Original_Cata" & _
        "lystsS2Plant OR @Original_CatalystsS2Plant IS NULL AND CatalystsS2Plant IS NULL)" & _
        " AND (CatalystsVHYT = @Original_CatalystsVHYT OR @Original_CatalystsVHYT IS NULL" & _
        " AND CatalystsVHYT IS NULL) AND (ChemicalsASESolv = @Original_ChemicalsASESolv O" & _
        "R @Original_ChemicalsASESolv IS NULL AND ChemicalsASESolv IS NULL) AND (Chemical" & _
        "sAlkyAcid = @Original_ChemicalsAlkyAcid OR @Original_ChemicalsAlkyAcid IS NULL A" & _
        "ND ChemicalsAlkyAcid IS NULL) AND (ChemicalsAmines = @Original_ChemicalsAmines O" & _
        "R @Original_ChemicalsAmines IS NULL AND ChemicalsAmines IS NULL) AND (ChemicalsC" & _
        "lay = @Original_ChemicalsClay OR @Original_ChemicalsClay IS NULL AND ChemicalsCl" & _
        "ay IS NULL) AND (ChemicalsDewaxAids = @Original_ChemicalsDewaxAids OR @Original_" & _
        "ChemicalsDewaxAids IS NULL AND ChemicalsDewaxAids IS NULL) AND (ChemicalsDieselA" & _
        "dd = @Original_ChemicalsDieselAdd OR @Original_ChemicalsDieselAdd IS NULL AND Ch" & _
        "emicalsDieselAdd IS NULL) AND (ChemicalsFurfural = @Original_ChemicalsFurfural O" & _
        "R @Original_ChemicalsFurfural IS NULL AND ChemicalsFurfural IS NULL) AND (Chemic" & _
        "alsGasAdd = @Original_ChemicalsGasAdd OR @Original_ChemicalsGasAdd IS NULL AND C" & _
        "hemicalsGasAdd IS NULL) AND (ChemicalsH2OTreat = @Original_ChemicalsH2OTreat OR " & _
        "@Original_ChemicalsH2OTreat IS NULL AND ChemicalsH2OTreat IS NULL) AND (Chemical" & _
        "sLube = @Original_ChemicalsLube OR @Original_ChemicalsLube IS NULL AND Chemicals" & _
        "Lube IS NULL) AND (ChemicalsMEK = @Original_ChemicalsMEK OR @Original_ChemicalsM" & _
        "EK IS NULL AND ChemicalsMEK IS NULL) AND (ChemicalsMIBK = @Original_ChemicalsMIB" & _
        "K OR @Original_ChemicalsMIBK IS NULL AND ChemicalsMIBK IS NULL) AND (ChemicalsNM" & _
        "P = @Original_ChemicalsNMP OR @Original_ChemicalsNMP IS NULL AND ChemicalsNMP IS" & _
        " NULL) AND (ChemicalsO2 = @Original_ChemicalsO2 OR @Original_ChemicalsO2 IS NULL" & _
        " AND ChemicalsO2 IS NULL) AND (ChemicalsOth = @Original_ChemicalsOth OR @Origina" & _
        "l_ChemicalsOth IS NULL AND ChemicalsOth IS NULL) AND (ChemicalsOthAcid = @Origin" & _
        "al_ChemicalsOthAcid OR @Original_ChemicalsOthAcid IS NULL AND ChemicalsOthAcid I" & _
        "S NULL) AND (ChemicalsOthAdd = @Original_ChemicalsOthAdd OR @Original_ChemicalsO" & _
        "thAdd IS NULL AND ChemicalsOthAdd IS NULL) AND (ChemicalsOthSolv = @Original_Che" & _
        "micalsOthSolv OR @Original_ChemicalsOthSolv IS NULL AND ChemicalsOthSolv IS NULL" & _
        ") AND (ChemicalsProcess = @Original_ChemicalsProcess OR @Original_ChemicalsProce" & _
        "ss IS NULL AND ChemicalsProcess IS NULL) AND (ChemicalsPropane = @Original_Chemi" & _
        "calsPropane OR @Original_ChemicalsPropane IS NULL AND ChemicalsPropane IS NULL) " & _
        "AND (ChemicalsToluene = @Original_ChemicalsToluene OR @Original_ChemicalsToluene" & _
        " IS NULL AND ChemicalsToluene IS NULL) AND (ChemicalsWasteH2O = @Original_Chemic" & _
        "alsWasteH2O OR @Original_ChemicalsWasteH2O IS NULL AND ChemicalsWasteH2O IS NULL" & _
        ") AND (ContMaintMatl = @Original_ContMaintMatl OR @Original_ContMaintMatl IS NUL" & _
        "L AND ContMaintMatl IS NULL) AND (EmissionsCredits = @Original_EmissionsCredits " & _
        "OR @Original_EmissionsCredits IS NULL AND EmissionsCredits IS NULL) AND (Emissio" & _
        "nsPurch = @Original_EmissionsPurch OR @Original_EmissionsPurch IS NULL AND Emiss" & _
        "ionsPurch IS NULL) AND (EmissionsTaxes = @Original_EmissionsTaxes OR @Original_E" & _
        "missionsTaxes IS NULL AND EmissionsTaxes IS NULL) AND (EnvirDisp = @Original_Env" & _
        "irDisp OR @Original_EnvirDisp IS NULL AND EnvirDisp IS NULL) AND (EnvirEng = @Or" & _
        "iginal_EnvirEng OR @Original_EnvirEng IS NULL AND EnvirEng IS NULL) AND (EnvirFi" & _
        "nes = @Original_EnvirFines OR @Original_EnvirFines IS NULL AND EnvirFines IS NUL" & _
        "L) AND (EnvirLab = @Original_EnvirLab OR @Original_EnvirLab IS NULL AND EnvirLab" & _
        " IS NULL) AND (EnvirOth = @Original_EnvirOth OR @Original_EnvirOth IS NULL AND E" & _
        "nvirOth IS NULL) AND (EnvirPermits = @Original_EnvirPermits OR @Original_EnvirPe" & _
        "rmits IS NULL AND EnvirPermits IS NULL) AND (EnvirSpill = @Original_EnvirSpill O" & _
        "R @Original_EnvirSpill IS NULL AND EnvirSpill IS NULL) AND (EquipMaint = @Origin" & _
        "al_EquipMaint OR @Original_EquipMaint IS NULL AND EquipMaint IS NULL) AND (Equip" & _
        "NonMaint = @Original_EquipNonMaint OR @Original_EquipNonMaint IS NULL AND EquipN" & _
        "onMaint IS NULL) AND (Insur = @Original_Insur OR @Original_Insur IS NULL AND Ins" & _
        "ur IS NULL) AND (MPSBenAbs = @Original_MPSBenAbs OR @Original_MPSBenAbs IS NULL " & _
        "AND MPSBenAbs IS NULL) AND (MPSBenInsur = @Original_MPSBenInsur OR @Original_MPS" & _
        "BenInsur IS NULL AND MPSBenInsur IS NULL) AND (MPSBenPension = @Original_MPSBenP" & _
        "ension OR @Original_MPSBenPension IS NULL AND MPSBenPension IS NULL) AND (MPSBen" & _
        "Stock = @Original_MPSBenStock OR @Original_MPSBenStock IS NULL AND MPSBenStock I" & _
        "S NULL) AND (MPSBenSub = @Original_MPSBenSub OR @Original_MPSBenSub IS NULL AND " & _
        "MPSBenSub IS NULL) AND (MPSBenTaxMed = @Original_MPSBenTaxMed OR @Original_MPSBe" & _
        "nTaxMed IS NULL AND MPSBenTaxMed IS NULL) AND (MPSBenTaxOth = @Original_MPSBenTa" & _
        "xOth OR @Original_MPSBenTaxOth IS NULL AND MPSBenTaxOth IS NULL) AND (MPSBenTaxP" & _
        "en = @Original_MPSBenTaxPen OR @Original_MPSBenTaxPen IS NULL AND MPSBenTaxPen I" & _
        "S NULL) AND (MaintMatl = @Original_MaintMatl OR @Original_MaintMatl IS NULL AND " & _
        "MaintMatl IS NULL) AND (OCCBenAbs = @Original_OCCBenAbs OR @Original_OCCBenAbs I" & _
        "S NULL AND OCCBenAbs IS NULL) AND (OCCBenInsur = @Original_OCCBenInsur OR @Origi" & _
        "nal_OCCBenInsur IS NULL AND OCCBenInsur IS NULL) AND (OCCBenPension = @Original_" & _
        "OCCBenPension OR @Original_OCCBenPension IS NULL AND OCCBenPension IS NULL) AND " & _
        "(OCCBenStock = @Original_OCCBenStock OR @Original_OCCBenStock IS NULL AND OCCBen" & _
        "Stock IS NULL) AND (OCCBenSub = @Original_OCCBenSub OR @Original_OCCBenSub IS NU" & _
        "LL AND OCCBenSub IS NULL) AND (OCCBenTaxMed = @Original_OCCBenTaxMed OR @Origina" & _
        "l_OCCBenTaxMed IS NULL AND OCCBenTaxMed IS NULL) AND (OCCBenTaxOth = @Original_O" & _
        "CCBenTaxOth OR @Original_OCCBenTaxOth IS NULL AND OCCBenTaxOth IS NULL) AND (OCC" & _
        "BenTaxPen = @Original_OCCBenTaxPen OR @Original_OCCBenTaxPen IS NULL AND OCCBenT" & _
        "axPen IS NULL) AND (OthContAdmin = @Original_OthContAdmin OR @Original_OthContAd" & _
        "min IS NULL AND OthContAdmin IS NULL) AND (OthContComputing = @Original_OthContC" & _
        "omputing OR @Original_OthContComputing IS NULL AND OthContComputing IS NULL) AND" & _
        " (OthContConsult = @Original_OthContConsult OR @Original_OthContConsult IS NULL " & _
        "AND OthContConsult IS NULL) AND (OthContFire = @Original_OthContFire OR @Origina" & _
        "l_OthContFire IS NULL AND OthContFire IS NULL) AND (OthContFoodSvc = @Original_O" & _
        "thContFoodSvc OR @Original_OthContFoodSvc IS NULL AND OthContFoodSvc IS NULL) AN" & _
        "D (OthContInsp = @Original_OthContInsp OR @Original_OthContInsp IS NULL AND OthC" & _
        "ontInsp IS NULL) AND (OthContJan = @Original_OthContJan OR @Original_OthContJan " & _
        "IS NULL AND OthContJan IS NULL) AND (OthContLab = @Original_OthContLab OR @Origi" & _
        "nal_OthContLab IS NULL AND OthContLab IS NULL) AND (OthContLegal = @Original_Oth" & _
        "ContLegal OR @Original_OthContLegal IS NULL AND OthContLegal IS NULL) AND (OthCo" & _
        "ntOth = @Original_OthContOth OR @Original_OthContOth IS NULL AND OthContOth IS N" & _
        "ULL) AND (OthContProcOp = @Original_OthContProcOp OR @Original_OthContProcOp IS " & _
        "NULL AND OthContProcOp IS NULL) AND (OthContSecurity = @Original_OthContSecurity" & _
        " OR @Original_OthContSecurity IS NULL AND OthContSecurity IS NULL) AND (OthContT" & _
        "ransOp = @Original_OthContTransOp OR @Original_OthContTransOp IS NULL AND OthCon" & _
        "tTransOp IS NULL) AND (OthContVacTrucks = @Original_OthContVacTrucks OR @Origina" & _
        "l_OthContVacTrucks IS NULL AND OthContVacTrucks IS NULL) AND (OthNonVolComm = @O" & _
        "riginal_OthNonVolComm OR @Original_OthNonVolComm IS NULL AND OthNonVolComm IS NU" & _
        "LL) AND (OthNonVolComputer = @Original_OthNonVolComputer OR @Original_OthNonVolC" & _
        "omputer IS NULL AND OthNonVolComputer IS NULL) AND (OthNonVolDonations = @Origin" & _
        "al_OthNonVolDonations OR @Original_OthNonVolDonations IS NULL AND OthNonVolDonat" & _
        "ions IS NULL) AND (OthNonVolDues = @Original_OthNonVolDues OR @Original_OthNonVo" & _
        "lDues IS NULL AND OthNonVolDues IS NULL) AND (OthNonVolOth = @Original_OthNonVol" & _
        "Oth OR @Original_OthNonVolOth IS NULL AND OthNonVolOth IS NULL) AND (OthNonVolSa" & _
        "fety = @Original_OthNonVolSafety OR @Original_OthNonVolSafety IS NULL AND OthNon" & _
        "VolSafety IS NULL) AND (OthNonVolSupply = @Original_OthNonVolSupply OR @Original" & _
        "_OthNonVolSupply IS NULL AND OthNonVolSupply IS NULL) AND (OthNonVolTanks = @Ori" & _
        "ginal_OthNonVolTanks OR @Original_OthNonVolTanks IS NULL AND OthNonVolTanks IS N" & _
        "ULL) AND (OthNonVolTrain = @Original_OthNonVolTrain OR @Original_OthNonVolTrain " & _
        "IS NULL AND OthNonVolTrain IS NULL) AND (OthNonVolTravel = @Original_OthNonVolTr" & _
        "avel OR @Original_OthNonVolTravel IS NULL AND OthNonVolTravel IS NULL) AND (OthV" & _
        "olDemCrude = @Original_OthVolDemCrude OR @Original_OthVolDemCrude IS NULL AND Ot" & _
        "hVolDemCrude IS NULL) AND (OthVolDemLightering = @Original_OthVolDemLightering O" & _
        "R @Original_OthVolDemLightering IS NULL AND OthVolDemLightering IS NULL) AND (Ot" & _
        "hVolDemProd = @Original_OthVolDemProd OR @Original_OthVolDemProd IS NULL AND Oth" & _
        "VolDemProd IS NULL) AND (OthVolOth = @Original_OthVolOth OR @Original_OthVolOth " & _
        "IS NULL AND OthVolOth IS NULL) AND (PurOthH2O = @Original_PurOthH2O OR @Original" & _
        "_PurOthH2O IS NULL AND PurOthH2O IS NULL) AND (PurOthN2 = @Original_PurOthN2 OR " & _
        "@Original_PurOthN2 IS NULL AND PurOthN2 IS NULL) AND (PurOthOth = @Original_PurO" & _
        "thOth OR @Original_PurOthOth IS NULL AND PurOthOth IS NULL) AND (Royalties = @Or" & _
        "iginal_Royalties OR @Original_Royalties IS NULL AND Royalties IS NULL) AND (Tax " & _
        "= @Original_Tax OR @Original_Tax IS NULL AND Tax IS NULL)"
        Me.SqlDeleteCommand20.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsCDWAX", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsCDWAX", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsDHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsDHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsFCC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsFCC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYFT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYFT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsNKSHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsPetChem", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsPetChem", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsREF", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsREF", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsRHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsRHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsS2Plant", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsS2Plant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsVHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsVHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsASESolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsASESolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAlkyAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAmines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAmines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsClay", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsClay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDewaxAids", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDieselAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsFurfural", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsFurfural", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsGasAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsH2OTreat", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsLube", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsLube", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMEK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMEK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMIBK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMIBK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsNMP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsNMP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthSolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsProcess", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsProcess", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsPropane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsPropane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsToluene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsToluene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsWasteH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsCredits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsCredits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsPurch", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsPurch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsTaxes", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsTaxes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirDisp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirDisp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirEng", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirEng", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirPermits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirPermits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirSpill", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirSpill", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipNonMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipNonMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Insur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Insur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContAdmin", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContAdmin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContComputing", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContComputing", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContConsult", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContConsult", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFire", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFire", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFoodSvc", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFoodSvc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContInsp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContInsp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContJan", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContJan", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLegal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLegal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContProcOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContProcOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContSecurity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContSecurity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContTransOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContTransOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContVacTrucks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContVacTrucks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComm", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComm", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComputer", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComputer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDonations", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDonations", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDues", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDues", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSupply", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSupply", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTanks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTanks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTrain", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTrain", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTravel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTravel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemCrude", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemCrude", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemLightering", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemLightering", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthN2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthN2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Royalties", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Royalties", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tax", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand21
        '
        Me.SqlInsertCommand21.CommandText = "INSERT INTO dbo.OpexAdd(OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, OCCBenS" & _
        "tock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, MPSBenAbs, MPSBenInsur, MPSBenPe" & _
        "nsion, MPSBenSub, MPSBenStock, MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MaintMa" & _
        "tl, ContMaintMatl, EquipMaint, OthContProcOp, OthContTransOp, OthContFire, OthCo" & _
        "ntVacTrucks, OthContConsult, OthContInsp, OthContSecurity, OthContComputing, Oth" & _
        "ContJan, OthContLab, OthContFoodSvc, OthContAdmin, OthContLegal, OthContOth, Env" & _
        "irDisp, EnvirPermits, EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, Equi" & _
        "pNonMaint, Tax, Insur, OthNonVolSupply, OthNonVolSafety, OthNonVolComm, OthNonVo" & _
        "lDonations, OthNonVolDues, OthNonVolTravel, OthNonVolTrain, OthNonVolComputer, O" & _
        "thNonVolTanks, OthNonVolOth, ChemicalsAlkyAcid, ChemicalsLube, ChemicalsH2OTreat" & _
        ", ChemicalsProcess, ChemicalsOthAcid, ChemicalsGasAdd, ChemicalsDieselAdd, Chemi" & _
        "calsOthAdd, ChemicalsO2, ChemicalsClay, ChemicalsAmines, ChemicalsASESolv, Chemi" & _
        "calsWasteH2O, ChemicalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, Chem" & _
        "icalsToluene, ChemicalsPropane, ChemicalsOthSolv, ChemicalsDewaxAids, ChemicalsO" & _
        "th, CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, C" & _
        "atalystsRHYT, CatalystsHYFT, CatalystsCDWAX, CatalystsREF, CatalystsHYG, Catalys" & _
        "tsS2Plant, CatalystsPetChem, CatalystsOth, PurOthN2, PurOthH2O, PurOthOth, Royal" & _
        "ties, OthVolDemCrude, OthVolDemLightering, OthVolDemProd, EmissionsTaxes, Emissi" & _
        "onsPurch, EmissionsCredits, OthVolOth, Currency, DataType, SubmissionID) VALUES " & _
        "(@OCCBenAbs, @OCCBenInsur, @OCCBenPension, @OCCBenSub, @OCCBenStock, @OCCBenTaxP" & _
        "en, @OCCBenTaxMed, @OCCBenTaxOth, @MPSBenAbs, @MPSBenInsur, @MPSBenPension, @MPS" & _
        "BenSub, @MPSBenStock, @MPSBenTaxPen, @MPSBenTaxMed, @MPSBenTaxOth, @MaintMatl, @" & _
        "ContMaintMatl, @EquipMaint, @OthContProcOp, @OthContTransOp, @OthContFire, @OthC" & _
        "ontVacTrucks, @OthContConsult, @OthContInsp, @OthContSecurity, @OthContComputing" & _
        ", @OthContJan, @OthContLab, @OthContFoodSvc, @OthContAdmin, @OthContLegal, @OthC" & _
        "ontOth, @EnvirDisp, @EnvirPermits, @EnvirFines, @EnvirSpill, @EnvirLab, @EnvirEn" & _
        "g, @EnvirOth, @EquipNonMaint, @Tax, @Insur, @OthNonVolSupply, @OthNonVolSafety, " & _
        "@OthNonVolComm, @OthNonVolDonations, @OthNonVolDues, @OthNonVolTravel, @OthNonVo" & _
        "lTrain, @OthNonVolComputer, @OthNonVolTanks, @OthNonVolOth, @ChemicalsAlkyAcid, " & _
        "@ChemicalsLube, @ChemicalsH2OTreat, @ChemicalsProcess, @ChemicalsOthAcid, @Chemi" & _
        "calsGasAdd, @ChemicalsDieselAdd, @ChemicalsOthAdd, @ChemicalsO2, @ChemicalsClay," & _
        " @ChemicalsAmines, @ChemicalsASESolv, @ChemicalsWasteH2O, @ChemicalsNMP, @Chemic" & _
        "alsFurfural, @ChemicalsMIBK, @ChemicalsMEK, @ChemicalsToluene, @ChemicalsPropane" & _
        ", @ChemicalsOthSolv, @ChemicalsDewaxAids, @ChemicalsOth, @CatalystsFCC, @Catalys" & _
        "tsHYC, @CatalystsNKSHYT, @CatalystsDHYT, @CatalystsVHYT, @CatalystsRHYT, @Cataly" & _
        "stsHYFT, @CatalystsCDWAX, @CatalystsREF, @CatalystsHYG, @CatalystsS2Plant, @Cata" & _
        "lystsPetChem, @CatalystsOth, @PurOthN2, @PurOthH2O, @PurOthOth, @Royalties, @Oth" & _
        "VolDemCrude, @OthVolDemLightering, @OthVolDemProd, @EmissionsTaxes, @EmissionsPu" & _
        "rch, @EmissionsCredits, @OthVolOth, @Currency, @DataType, @SubmissionID); SELECT" & _
        " OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, OCCBenStock, OCCBenTaxPen, OC" & _
        "CBenTaxMed, OCCBenTaxOth, MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSB" & _
        "enStock, MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MaintMatl, ContMaintMatl, Equ" & _
        "ipMaint, OthContProcOp, OthContTransOp, OthContFire, OthContVacTrucks, OthContCo" & _
        "nsult, OthContInsp, OthContSecurity, OthContComputing, OthContJan, OthContLab, O" & _
        "thContFoodSvc, OthContAdmin, OthContLegal, OthContOth, EnvirDisp, EnvirPermits, " & _
        "EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, EquipNonMaint, Tax, Insur," & _
        " OthNonVolSupply, OthNonVolSafety, OthNonVolComm, OthNonVolDonations, OthNonVolD" & _
        "ues, OthNonVolTravel, OthNonVolTrain, OthNonVolComputer, OthNonVolTanks, OthNonV" & _
        "olOth, ChemicalsAlkyAcid, ChemicalsLube, ChemicalsH2OTreat, ChemicalsProcess, Ch" & _
        "emicalsOthAcid, ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO" & _
        "2, ChemicalsClay, ChemicalsAmines, ChemicalsASESolv, ChemicalsWasteH2O, Chemical" & _
        "sNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene, Chemical" & _
        "sPropane, ChemicalsOthSolv, ChemicalsDewaxAids, ChemicalsOth, CatalystsFCC, Cata" & _
        "lystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, CatalystsRHYT, Catalyst" & _
        "sHYFT, CatalystsCDWAX, CatalystsREF, CatalystsHYG, CatalystsS2Plant, CatalystsPe" & _
        "tChem, CatalystsOth, PurOthN2, PurOthH2O, PurOthOth, Royalties, OthVolDemCrude, " & _
        "OthVolDemLightering, OthVolDemProd, EmissionsTaxes, EmissionsPurch, EmissionsCre" & _
        "dits, OthVolOth, Currency, DataType, SubmissionID FROM dbo.OpexAdd WHERE (Curren" & _
        "cy = @Currency) AND (DataType = @DataType) AND (SubmissionID = @SubmissionID)"
        Me.SqlInsertCommand21.Connection = Me.SqlConnection1
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenAbs", System.Data.SqlDbType.Real, 4, "OCCBenAbs"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenInsur", System.Data.SqlDbType.Real, 4, "OCCBenInsur"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenPension", System.Data.SqlDbType.Real, 4, "OCCBenPension"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenSub", System.Data.SqlDbType.Real, 4, "OCCBenSub"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenStock", System.Data.SqlDbType.Real, 4, "OCCBenStock"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxPen", System.Data.SqlDbType.Real, 4, "OCCBenTaxPen"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxMed", System.Data.SqlDbType.Real, 4, "OCCBenTaxMed"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxOth", System.Data.SqlDbType.Real, 4, "OCCBenTaxOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenAbs", System.Data.SqlDbType.Real, 4, "MPSBenAbs"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenInsur", System.Data.SqlDbType.Real, 4, "MPSBenInsur"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenPension", System.Data.SqlDbType.Real, 4, "MPSBenPension"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenSub", System.Data.SqlDbType.Real, 4, "MPSBenSub"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenStock", System.Data.SqlDbType.Real, 4, "MPSBenStock"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxPen", System.Data.SqlDbType.Real, 4, "MPSBenTaxPen"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxMed", System.Data.SqlDbType.Real, 4, "MPSBenTaxMed"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxOth", System.Data.SqlDbType.Real, 4, "MPSBenTaxOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatl", System.Data.SqlDbType.Real, 4, "MaintMatl"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintMatl", System.Data.SqlDbType.Real, 4, "ContMaintMatl"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipMaint", System.Data.SqlDbType.Real, 4, "EquipMaint"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContProcOp", System.Data.SqlDbType.Real, 4, "OthContProcOp"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContTransOp", System.Data.SqlDbType.Real, 4, "OthContTransOp"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFire", System.Data.SqlDbType.Real, 4, "OthContFire"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContVacTrucks", System.Data.SqlDbType.Real, 4, "OthContVacTrucks"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContConsult", System.Data.SqlDbType.Real, 4, "OthContConsult"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContInsp", System.Data.SqlDbType.Real, 4, "OthContInsp"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContSecurity", System.Data.SqlDbType.Real, 4, "OthContSecurity"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContComputing", System.Data.SqlDbType.Real, 4, "OthContComputing"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContJan", System.Data.SqlDbType.Real, 4, "OthContJan"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLab", System.Data.SqlDbType.Real, 4, "OthContLab"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFoodSvc", System.Data.SqlDbType.Real, 4, "OthContFoodSvc"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContAdmin", System.Data.SqlDbType.Real, 4, "OthContAdmin"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLegal", System.Data.SqlDbType.Real, 4, "OthContLegal"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContOth", System.Data.SqlDbType.Real, 4, "OthContOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirDisp", System.Data.SqlDbType.Real, 4, "EnvirDisp"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirPermits", System.Data.SqlDbType.Real, 4, "EnvirPermits"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirFines", System.Data.SqlDbType.Real, 4, "EnvirFines"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirSpill", System.Data.SqlDbType.Real, 4, "EnvirSpill"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirLab", System.Data.SqlDbType.Real, 4, "EnvirLab"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirEng", System.Data.SqlDbType.Real, 4, "EnvirEng"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirOth", System.Data.SqlDbType.Real, 4, "EnvirOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipNonMaint", System.Data.SqlDbType.Real, 4, "EquipNonMaint"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tax", System.Data.SqlDbType.Real, 4, "Tax"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Insur", System.Data.SqlDbType.Real, 4, "Insur"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSupply", System.Data.SqlDbType.Real, 4, "OthNonVolSupply"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSafety", System.Data.SqlDbType.Real, 4, "OthNonVolSafety"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComm", System.Data.SqlDbType.Real, 4, "OthNonVolComm"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDonations", System.Data.SqlDbType.Real, 4, "OthNonVolDonations"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDues", System.Data.SqlDbType.Real, 4, "OthNonVolDues"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTravel", System.Data.SqlDbType.Real, 4, "OthNonVolTravel"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTrain", System.Data.SqlDbType.Real, 4, "OthNonVolTrain"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComputer", System.Data.SqlDbType.Real, 4, "OthNonVolComputer"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTanks", System.Data.SqlDbType.Real, 4, "OthNonVolTanks"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolOth", System.Data.SqlDbType.Real, 4, "OthNonVolOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, "ChemicalsAlkyAcid"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsLube", System.Data.SqlDbType.Real, 4, "ChemicalsLube"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, "ChemicalsH2OTreat"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsProcess", System.Data.SqlDbType.Real, 4, "ChemicalsProcess"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, "ChemicalsOthAcid"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, "ChemicalsGasAdd"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, "ChemicalsDieselAdd"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, "ChemicalsOthAdd"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsO2", System.Data.SqlDbType.Real, 4, "ChemicalsO2"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsClay", System.Data.SqlDbType.Real, 4, "ChemicalsClay"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAmines", System.Data.SqlDbType.Real, 4, "ChemicalsAmines"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsASESolv", System.Data.SqlDbType.Real, 4, "ChemicalsASESolv"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, "ChemicalsWasteH2O"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsNMP", System.Data.SqlDbType.Real, 4, "ChemicalsNMP"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsFurfural", System.Data.SqlDbType.Real, 4, "ChemicalsFurfural"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMIBK", System.Data.SqlDbType.Real, 4, "ChemicalsMIBK"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMEK", System.Data.SqlDbType.Real, 4, "ChemicalsMEK"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsToluene", System.Data.SqlDbType.Real, 4, "ChemicalsToluene"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsPropane", System.Data.SqlDbType.Real, 4, "ChemicalsPropane"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, "ChemicalsOthSolv"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, "ChemicalsDewaxAids"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOth", System.Data.SqlDbType.Real, 4, "ChemicalsOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsFCC", System.Data.SqlDbType.Real, 4, "CatalystsFCC"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYC", System.Data.SqlDbType.Real, 4, "CatalystsHYC"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, "CatalystsNKSHYT"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsDHYT", System.Data.SqlDbType.Real, 4, "CatalystsDHYT"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsVHYT", System.Data.SqlDbType.Real, 4, "CatalystsVHYT"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsRHYT", System.Data.SqlDbType.Real, 4, "CatalystsRHYT"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYFT", System.Data.SqlDbType.Real, 4, "CatalystsHYFT"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsCDWAX", System.Data.SqlDbType.Real, 4, "CatalystsCDWAX"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsREF", System.Data.SqlDbType.Real, 4, "CatalystsREF"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYG", System.Data.SqlDbType.Real, 4, "CatalystsHYG"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsS2Plant", System.Data.SqlDbType.Real, 4, "CatalystsS2Plant"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsPetChem", System.Data.SqlDbType.Real, 4, "CatalystsPetChem"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsOth", System.Data.SqlDbType.Real, 4, "CatalystsOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthN2", System.Data.SqlDbType.Real, 4, "PurOthN2"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthH2O", System.Data.SqlDbType.Real, 4, "PurOthH2O"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthOth", System.Data.SqlDbType.Real, 4, "PurOthOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Royalties", System.Data.SqlDbType.Real, 4, "Royalties"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemCrude", System.Data.SqlDbType.Real, 4, "OthVolDemCrude"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemLightering", System.Data.SqlDbType.Real, 4, "OthVolDemLightering"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemProd", System.Data.SqlDbType.Real, 4, "OthVolDemProd"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsTaxes", System.Data.SqlDbType.Real, 4, "EmissionsTaxes"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsPurch", System.Data.SqlDbType.Real, 4, "EmissionsPurch"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsCredits", System.Data.SqlDbType.Real, 4, "EmissionsCredits"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolOth", System.Data.SqlDbType.Real, 4, "OthVolOth"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlInsertCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        '
        'SqlSelectCommand21
        '
        Me.SqlSelectCommand21.CommandText = "SELECT OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, OCCBenStock, OCCBenTaxPe" & _
        "n, OCCBenTaxMed, OCCBenTaxOth, MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub," & _
        " MPSBenStock, MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MaintMatl, ContMaintMatl" & _
        ", EquipMaint, OthContProcOp, OthContTransOp, OthContFire, OthContVacTrucks, OthC" & _
        "ontConsult, OthContInsp, OthContSecurity, OthContComputing, OthContJan, OthContL" & _
        "ab, OthContFoodSvc, OthContAdmin, OthContLegal, OthContOth, EnvirDisp, EnvirPerm" & _
        "its, EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, EquipNonMaint, Tax, I" & _
        "nsur, OthNonVolSupply, OthNonVolSafety, OthNonVolComm, OthNonVolDonations, OthNo" & _
        "nVolDues, OthNonVolTravel, OthNonVolTrain, OthNonVolComputer, OthNonVolTanks, Ot" & _
        "hNonVolOth, ChemicalsAlkyAcid, ChemicalsLube, ChemicalsH2OTreat, ChemicalsProces" & _
        "s, ChemicalsOthAcid, ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, Chemi" & _
        "calsO2, ChemicalsClay, ChemicalsAmines, ChemicalsASESolv, ChemicalsWasteH2O, Che" & _
        "micalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene, Che" & _
        "micalsPropane, ChemicalsOthSolv, ChemicalsDewaxAids, ChemicalsOth, CatalystsFCC," & _
        " CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, CatalystsRHYT, Cat" & _
        "alystsHYFT, CatalystsCDWAX, CatalystsREF, CatalystsHYG, CatalystsS2Plant, Cataly" & _
        "stsPetChem, CatalystsOth, PurOthN2, PurOthH2O, PurOthOth, Royalties, OthVolDemCr" & _
        "ude, OthVolDemLightering, OthVolDemProd, EmissionsTaxes, EmissionsPurch, Emissio" & _
        "nsCredits, OthVolOth, Currency, DataType, SubmissionID FROM dbo.OpexAdd"
        Me.SqlSelectCommand21.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand20
        '
        Me.SqlUpdateCommand20.CommandText = "UPDATE dbo.OpexAdd SET OCCBenAbs = @OCCBenAbs, OCCBenInsur = @OCCBenInsur, OCCBen" & _
        "Pension = @OCCBenPension, OCCBenSub = @OCCBenSub, OCCBenStock = @OCCBenStock, OC" & _
        "CBenTaxPen = @OCCBenTaxPen, OCCBenTaxMed = @OCCBenTaxMed, OCCBenTaxOth = @OCCBen" & _
        "TaxOth, MPSBenAbs = @MPSBenAbs, MPSBenInsur = @MPSBenInsur, MPSBenPension = @MPS" & _
        "BenPension, MPSBenSub = @MPSBenSub, MPSBenStock = @MPSBenStock, MPSBenTaxPen = @" & _
        "MPSBenTaxPen, MPSBenTaxMed = @MPSBenTaxMed, MPSBenTaxOth = @MPSBenTaxOth, MaintM" & _
        "atl = @MaintMatl, ContMaintMatl = @ContMaintMatl, EquipMaint = @EquipMaint, OthC" & _
        "ontProcOp = @OthContProcOp, OthContTransOp = @OthContTransOp, OthContFire = @Oth" & _
        "ContFire, OthContVacTrucks = @OthContVacTrucks, OthContConsult = @OthContConsult" & _
        ", OthContInsp = @OthContInsp, OthContSecurity = @OthContSecurity, OthContComputi" & _
        "ng = @OthContComputing, OthContJan = @OthContJan, OthContLab = @OthContLab, OthC" & _
        "ontFoodSvc = @OthContFoodSvc, OthContAdmin = @OthContAdmin, OthContLegal = @OthC" & _
        "ontLegal, OthContOth = @OthContOth, EnvirDisp = @EnvirDisp, EnvirPermits = @Envi" & _
        "rPermits, EnvirFines = @EnvirFines, EnvirSpill = @EnvirSpill, EnvirLab = @EnvirL" & _
        "ab, EnvirEng = @EnvirEng, EnvirOth = @EnvirOth, EquipNonMaint = @EquipNonMaint, " & _
        "Tax = @Tax, Insur = @Insur, OthNonVolSupply = @OthNonVolSupply, OthNonVolSafety " & _
        "= @OthNonVolSafety, OthNonVolComm = @OthNonVolComm, OthNonVolDonations = @OthNon" & _
        "VolDonations, OthNonVolDues = @OthNonVolDues, OthNonVolTravel = @OthNonVolTravel" & _
        ", OthNonVolTrain = @OthNonVolTrain, OthNonVolComputer = @OthNonVolComputer, OthN" & _
        "onVolTanks = @OthNonVolTanks, OthNonVolOth = @OthNonVolOth, ChemicalsAlkyAcid = " & _
        "@ChemicalsAlkyAcid, ChemicalsLube = @ChemicalsLube, ChemicalsH2OTreat = @Chemica" & _
        "lsH2OTreat, ChemicalsProcess = @ChemicalsProcess, ChemicalsOthAcid = @ChemicalsO" & _
        "thAcid, ChemicalsGasAdd = @ChemicalsGasAdd, ChemicalsDieselAdd = @ChemicalsDiese" & _
        "lAdd, ChemicalsOthAdd = @ChemicalsOthAdd, ChemicalsO2 = @ChemicalsO2, ChemicalsC" & _
        "lay = @ChemicalsClay, ChemicalsAmines = @ChemicalsAmines, ChemicalsASESolv = @Ch" & _
        "emicalsASESolv, ChemicalsWasteH2O = @ChemicalsWasteH2O, ChemicalsNMP = @Chemical" & _
        "sNMP, ChemicalsFurfural = @ChemicalsFurfural, ChemicalsMIBK = @ChemicalsMIBK, Ch" & _
        "emicalsMEK = @ChemicalsMEK, ChemicalsToluene = @ChemicalsToluene, ChemicalsPropa" & _
        "ne = @ChemicalsPropane, ChemicalsOthSolv = @ChemicalsOthSolv, ChemicalsDewaxAids" & _
        " = @ChemicalsDewaxAids, ChemicalsOth = @ChemicalsOth, CatalystsFCC = @CatalystsF" & _
        "CC, CatalystsHYC = @CatalystsHYC, CatalystsNKSHYT = @CatalystsNKSHYT, CatalystsD" & _
        "HYT = @CatalystsDHYT, CatalystsVHYT = @CatalystsVHYT, CatalystsRHYT = @Catalysts" & _
        "RHYT, CatalystsHYFT = @CatalystsHYFT, CatalystsCDWAX = @CatalystsCDWAX, Catalyst" & _
        "sREF = @CatalystsREF, CatalystsHYG = @CatalystsHYG, CatalystsS2Plant = @Catalyst" & _
        "sS2Plant, CatalystsPetChem = @CatalystsPetChem, CatalystsOth = @CatalystsOth, Pu" & _
        "rOthN2 = @PurOthN2, PurOthH2O = @PurOthH2O, PurOthOth = @PurOthOth, Royalties = " & _
        "@Royalties, OthVolDemCrude = @OthVolDemCrude, OthVolDemLightering = @OthVolDemLi" & _
        "ghtering, OthVolDemProd = @OthVolDemProd, EmissionsTaxes = @EmissionsTaxes, Emis" & _
        "sionsPurch = @EmissionsPurch, EmissionsCredits = @EmissionsCredits, OthVolOth = " & _
        "@OthVolOth, Currency = @Currency, DataType = @DataType, SubmissionID = @Submissi" & _
        "onID WHERE (Currency = @Original_Currency) AND (DataType = @Original_DataType) A" & _
        "ND (SubmissionID = @Original_SubmissionID) AND (CatalystsCDWAX = @Original_Catal" & _
        "ystsCDWAX OR @Original_CatalystsCDWAX IS NULL AND CatalystsCDWAX IS NULL) AND (C" & _
        "atalystsDHYT = @Original_CatalystsDHYT OR @Original_CatalystsDHYT IS NULL AND Ca" & _
        "talystsDHYT IS NULL) AND (CatalystsFCC = @Original_CatalystsFCC OR @Original_Cat" & _
        "alystsFCC IS NULL AND CatalystsFCC IS NULL) AND (CatalystsHYC = @Original_Cataly" & _
        "stsHYC OR @Original_CatalystsHYC IS NULL AND CatalystsHYC IS NULL) AND (Catalyst" & _
        "sHYFT = @Original_CatalystsHYFT OR @Original_CatalystsHYFT IS NULL AND Catalysts" & _
        "HYFT IS NULL) AND (CatalystsHYG = @Original_CatalystsHYG OR @Original_CatalystsH" & _
        "YG IS NULL AND CatalystsHYG IS NULL) AND (CatalystsNKSHYT = @Original_CatalystsN" & _
        "KSHYT OR @Original_CatalystsNKSHYT IS NULL AND CatalystsNKSHYT IS NULL) AND (Cat" & _
        "alystsOth = @Original_CatalystsOth OR @Original_CatalystsOth IS NULL AND Catalys" & _
        "tsOth IS NULL) AND (CatalystsPetChem = @Original_CatalystsPetChem OR @Original_C" & _
        "atalystsPetChem IS NULL AND CatalystsPetChem IS NULL) AND (CatalystsREF = @Origi" & _
        "nal_CatalystsREF OR @Original_CatalystsREF IS NULL AND CatalystsREF IS NULL) AND" & _
        " (CatalystsRHYT = @Original_CatalystsRHYT OR @Original_CatalystsRHYT IS NULL AND" & _
        " CatalystsRHYT IS NULL) AND (CatalystsS2Plant = @Original_CatalystsS2Plant OR @O" & _
        "riginal_CatalystsS2Plant IS NULL AND CatalystsS2Plant IS NULL) AND (CatalystsVHY" & _
        "T = @Original_CatalystsVHYT OR @Original_CatalystsVHYT IS NULL AND CatalystsVHYT" & _
        " IS NULL) AND (ChemicalsASESolv = @Original_ChemicalsASESolv OR @Original_Chemic" & _
        "alsASESolv IS NULL AND ChemicalsASESolv IS NULL) AND (ChemicalsAlkyAcid = @Origi" & _
        "nal_ChemicalsAlkyAcid OR @Original_ChemicalsAlkyAcid IS NULL AND ChemicalsAlkyAc" & _
        "id IS NULL) AND (ChemicalsAmines = @Original_ChemicalsAmines OR @Original_Chemic" & _
        "alsAmines IS NULL AND ChemicalsAmines IS NULL) AND (ChemicalsClay = @Original_Ch" & _
        "emicalsClay OR @Original_ChemicalsClay IS NULL AND ChemicalsClay IS NULL) AND (C" & _
        "hemicalsDewaxAids = @Original_ChemicalsDewaxAids OR @Original_ChemicalsDewaxAids" & _
        " IS NULL AND ChemicalsDewaxAids IS NULL) AND (ChemicalsDieselAdd = @Original_Che" & _
        "micalsDieselAdd OR @Original_ChemicalsDieselAdd IS NULL AND ChemicalsDieselAdd I" & _
        "S NULL) AND (ChemicalsFurfural = @Original_ChemicalsFurfural OR @Original_Chemic" & _
        "alsFurfural IS NULL AND ChemicalsFurfural IS NULL) AND (ChemicalsGasAdd = @Origi" & _
        "nal_ChemicalsGasAdd OR @Original_ChemicalsGasAdd IS NULL AND ChemicalsGasAdd IS " & _
        "NULL) AND (ChemicalsH2OTreat = @Original_ChemicalsH2OTreat OR @Original_Chemical" & _
        "sH2OTreat IS NULL AND ChemicalsH2OTreat IS NULL) AND (ChemicalsLube = @Original_" & _
        "ChemicalsLube OR @Original_ChemicalsLube IS NULL AND ChemicalsLube IS NULL) AND " & _
        "(ChemicalsMEK = @Original_ChemicalsMEK OR @Original_ChemicalsMEK IS NULL AND Che" & _
        "micalsMEK IS NULL) AND (ChemicalsMIBK = @Original_ChemicalsMIBK OR @Original_Che" & _
        "micalsMIBK IS NULL AND ChemicalsMIBK IS NULL) AND (ChemicalsNMP = @Original_Chem" & _
        "icalsNMP OR @Original_ChemicalsNMP IS NULL AND ChemicalsNMP IS NULL) AND (Chemic" & _
        "alsO2 = @Original_ChemicalsO2 OR @Original_ChemicalsO2 IS NULL AND ChemicalsO2 I" & _
        "S NULL) AND (ChemicalsOth = @Original_ChemicalsOth OR @Original_ChemicalsOth IS " & _
        "NULL AND ChemicalsOth IS NULL) AND (ChemicalsOthAcid = @Original_ChemicalsOthAci" & _
        "d OR @Original_ChemicalsOthAcid IS NULL AND ChemicalsOthAcid IS NULL) AND (Chemi" & _
        "calsOthAdd = @Original_ChemicalsOthAdd OR @Original_ChemicalsOthAdd IS NULL AND " & _
        "ChemicalsOthAdd IS NULL) AND (ChemicalsOthSolv = @Original_ChemicalsOthSolv OR @" & _
        "Original_ChemicalsOthSolv IS NULL AND ChemicalsOthSolv IS NULL) AND (ChemicalsPr" & _
        "ocess = @Original_ChemicalsProcess OR @Original_ChemicalsProcess IS NULL AND Che" & _
        "micalsProcess IS NULL) AND (ChemicalsPropane = @Original_ChemicalsPropane OR @Or" & _
        "iginal_ChemicalsPropane IS NULL AND ChemicalsPropane IS NULL) AND (ChemicalsTolu" & _
        "ene = @Original_ChemicalsToluene OR @Original_ChemicalsToluene IS NULL AND Chemi" & _
        "calsToluene IS NULL) AND (ChemicalsWasteH2O = @Original_ChemicalsWasteH2O OR @Or" & _
        "iginal_ChemicalsWasteH2O IS NULL AND ChemicalsWasteH2O IS NULL) AND (ContMaintMa" & _
        "tl = @Original_ContMaintMatl OR @Original_ContMaintMatl IS NULL AND ContMaintMat" & _
        "l IS NULL) AND (EmissionsCredits = @Original_EmissionsCredits OR @Original_Emiss" & _
        "ionsCredits IS NULL AND EmissionsCredits IS NULL) AND (EmissionsPurch = @Origina" & _
        "l_EmissionsPurch OR @Original_EmissionsPurch IS NULL AND EmissionsPurch IS NULL)" & _
        " AND (EmissionsTaxes = @Original_EmissionsTaxes OR @Original_EmissionsTaxes IS N" & _
        "ULL AND EmissionsTaxes IS NULL) AND (EnvirDisp = @Original_EnvirDisp OR @Origina" & _
        "l_EnvirDisp IS NULL AND EnvirDisp IS NULL) AND (EnvirEng = @Original_EnvirEng OR" & _
        " @Original_EnvirEng IS NULL AND EnvirEng IS NULL) AND (EnvirFines = @Original_En" & _
        "virFines OR @Original_EnvirFines IS NULL AND EnvirFines IS NULL) AND (EnvirLab =" & _
        " @Original_EnvirLab OR @Original_EnvirLab IS NULL AND EnvirLab IS NULL) AND (Env" & _
        "irOth = @Original_EnvirOth OR @Original_EnvirOth IS NULL AND EnvirOth IS NULL) A" & _
        "ND (EnvirPermits = @Original_EnvirPermits OR @Original_EnvirPermits IS NULL AND " & _
        "EnvirPermits IS NULL) AND (EnvirSpill = @Original_EnvirSpill OR @Original_EnvirS" & _
        "pill IS NULL AND EnvirSpill IS NULL) AND (EquipMaint = @Original_EquipMaint OR @" & _
        "Original_EquipMaint IS NULL AND EquipMaint IS NULL) AND (EquipNonMaint = @Origin" & _
        "al_EquipNonMaint OR @Original_EquipNonMaint IS NULL AND EquipNonMaint IS NULL) A" & _
        "ND (Insur = @Original_Insur OR @Original_Insur IS NULL AND Insur IS NULL) AND (M" & _
        "PSBenAbs = @Original_MPSBenAbs OR @Original_MPSBenAbs IS NULL AND MPSBenAbs IS N" & _
        "ULL) AND (MPSBenInsur = @Original_MPSBenInsur OR @Original_MPSBenInsur IS NULL A" & _
        "ND MPSBenInsur IS NULL) AND (MPSBenPension = @Original_MPSBenPension OR @Origina" & _
        "l_MPSBenPension IS NULL AND MPSBenPension IS NULL) AND (MPSBenStock = @Original_" & _
        "MPSBenStock OR @Original_MPSBenStock IS NULL AND MPSBenStock IS NULL) AND (MPSBe" & _
        "nSub = @Original_MPSBenSub OR @Original_MPSBenSub IS NULL AND MPSBenSub IS NULL)" & _
        " AND (MPSBenTaxMed = @Original_MPSBenTaxMed OR @Original_MPSBenTaxMed IS NULL AN" & _
        "D MPSBenTaxMed IS NULL) AND (MPSBenTaxOth = @Original_MPSBenTaxOth OR @Original_" & _
        "MPSBenTaxOth IS NULL AND MPSBenTaxOth IS NULL) AND (MPSBenTaxPen = @Original_MPS" & _
        "BenTaxPen OR @Original_MPSBenTaxPen IS NULL AND MPSBenTaxPen IS NULL) AND (Maint" & _
        "Matl = @Original_MaintMatl OR @Original_MaintMatl IS NULL AND MaintMatl IS NULL)" & _
        " AND (OCCBenAbs = @Original_OCCBenAbs OR @Original_OCCBenAbs IS NULL AND OCCBenA" & _
        "bs IS NULL) AND (OCCBenInsur = @Original_OCCBenInsur OR @Original_OCCBenInsur IS" & _
        " NULL AND OCCBenInsur IS NULL) AND (OCCBenPension = @Original_OCCBenPension OR @" & _
        "Original_OCCBenPension IS NULL AND OCCBenPension IS NULL) AND (OCCBenStock = @Or" & _
        "iginal_OCCBenStock OR @Original_OCCBenStock IS NULL AND OCCBenStock IS NULL) AND" & _
        " (OCCBenSub = @Original_OCCBenSub OR @Original_OCCBenSub IS NULL AND OCCBenSub I" & _
        "S NULL) AND (OCCBenTaxMed = @Original_OCCBenTaxMed OR @Original_OCCBenTaxMed IS " & _
        "NULL AND OCCBenTaxMed IS NULL) AND (OCCBenTaxOth = @Original_OCCBenTaxOth OR @Or" & _
        "iginal_OCCBenTaxOth IS NULL AND OCCBenTaxOth IS NULL) AND (OCCBenTaxPen = @Origi" & _
        "nal_OCCBenTaxPen OR @Original_OCCBenTaxPen IS NULL AND OCCBenTaxPen IS NULL) AND" & _
        " (OthContAdmin = @Original_OthContAdmin OR @Original_OthContAdmin IS NULL AND Ot" & _
        "hContAdmin IS NULL) AND (OthContComputing = @Original_OthContComputing OR @Origi" & _
        "nal_OthContComputing IS NULL AND OthContComputing IS NULL) AND (OthContConsult =" & _
        " @Original_OthContConsult OR @Original_OthContConsult IS NULL AND OthContConsult" & _
        " IS NULL) AND (OthContFire = @Original_OthContFire OR @Original_OthContFire IS N" & _
        "ULL AND OthContFire IS NULL) AND (OthContFoodSvc = @Original_OthContFoodSvc OR @" & _
        "Original_OthContFoodSvc IS NULL AND OthContFoodSvc IS NULL) AND (OthContInsp = @" & _
        "Original_OthContInsp OR @Original_OthContInsp IS NULL AND OthContInsp IS NULL) A" & _
        "ND (OthContJan = @Original_OthContJan OR @Original_OthContJan IS NULL AND OthCon" & _
        "tJan IS NULL) AND (OthContLab = @Original_OthContLab OR @Original_OthContLab IS " & _
        "NULL AND OthContLab IS NULL) AND (OthContLegal = @Original_OthContLegal OR @Orig" & _
        "inal_OthContLegal IS NULL AND OthContLegal IS NULL) AND (OthContOth = @Original_" & _
        "OthContOth OR @Original_OthContOth IS NULL AND OthContOth IS NULL) AND (OthContP" & _
        "rocOp = @Original_OthContProcOp OR @Original_OthContProcOp IS NULL AND OthContPr" & _
        "ocOp IS NULL) AND (OthContSecurity = @Original_OthContSecurity OR @Original_OthC" & _
        "ontSecurity IS NULL AND OthContSecurity IS NULL) AND (OthContTransOp = @Original" & _
        "_OthContTransOp OR @Original_OthContTransOp IS NULL AND OthContTransOp IS NULL) " & _
        "AND (OthContVacTrucks = @Original_OthContVacTrucks OR @Original_OthContVacTrucks" & _
        " IS NULL AND OthContVacTrucks IS NULL) AND (OthNonVolComm = @Original_OthNonVolC" & _
        "omm OR @Original_OthNonVolComm IS NULL AND OthNonVolComm IS NULL) AND (OthNonVol" & _
        "Computer = @Original_OthNonVolComputer OR @Original_OthNonVolComputer IS NULL AN" & _
        "D OthNonVolComputer IS NULL) AND (OthNonVolDonations = @Original_OthNonVolDonati" & _
        "ons OR @Original_OthNonVolDonations IS NULL AND OthNonVolDonations IS NULL) AND " & _
        "(OthNonVolDues = @Original_OthNonVolDues OR @Original_OthNonVolDues IS NULL AND " & _
        "OthNonVolDues IS NULL) AND (OthNonVolOth = @Original_OthNonVolOth OR @Original_O" & _
        "thNonVolOth IS NULL AND OthNonVolOth IS NULL) AND (OthNonVolSafety = @Original_O" & _
        "thNonVolSafety OR @Original_OthNonVolSafety IS NULL AND OthNonVolSafety IS NULL)" & _
        " AND (OthNonVolSupply = @Original_OthNonVolSupply OR @Original_OthNonVolSupply I" & _
        "S NULL AND OthNonVolSupply IS NULL) AND (OthNonVolTanks = @Original_OthNonVolTan" & _
        "ks OR @Original_OthNonVolTanks IS NULL AND OthNonVolTanks IS NULL) AND (OthNonVo" & _
        "lTrain = @Original_OthNonVolTrain OR @Original_OthNonVolTrain IS NULL AND OthNon" & _
        "VolTrain IS NULL) AND (OthNonVolTravel = @Original_OthNonVolTravel OR @Original_" & _
        "OthNonVolTravel IS NULL AND OthNonVolTravel IS NULL) AND (OthVolDemCrude = @Orig" & _
        "inal_OthVolDemCrude OR @Original_OthVolDemCrude IS NULL AND OthVolDemCrude IS NU" & _
        "LL) AND (OthVolDemLightering = @Original_OthVolDemLightering OR @Original_OthVol" & _
        "DemLightering IS NULL AND OthVolDemLightering IS NULL) AND (OthVolDemProd = @Ori" & _
        "ginal_OthVolDemProd OR @Original_OthVolDemProd IS NULL AND OthVolDemProd IS NULL" & _
        ") AND (OthVolOth = @Original_OthVolOth OR @Original_OthVolOth IS NULL AND OthVol" & _
        "Oth IS NULL) AND (PurOthH2O = @Original_PurOthH2O OR @Original_PurOthH2O IS NULL" & _
        " AND PurOthH2O IS NULL) AND (PurOthN2 = @Original_PurOthN2 OR @Original_PurOthN2" & _
        " IS NULL AND PurOthN2 IS NULL) AND (PurOthOth = @Original_PurOthOth OR @Original" & _
        "_PurOthOth IS NULL AND PurOthOth IS NULL) AND (Royalties = @Original_Royalties O" & _
        "R @Original_Royalties IS NULL AND Royalties IS NULL) AND (Tax = @Original_Tax OR" & _
        " @Original_Tax IS NULL AND Tax IS NULL); SELECT OCCBenAbs, OCCBenInsur, OCCBenPe" & _
        "nsion, OCCBenSub, OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, MPSBenA" & _
        "bs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock, MPSBenTaxPen, MPSBenTaxM" & _
        "ed, MPSBenTaxOth, MaintMatl, ContMaintMatl, EquipMaint, OthContProcOp, OthContTr" & _
        "ansOp, OthContFire, OthContVacTrucks, OthContConsult, OthContInsp, OthContSecuri" & _
        "ty, OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin, OthC" & _
        "ontLegal, OthContOth, EnvirDisp, EnvirPermits, EnvirFines, EnvirSpill, EnvirLab," & _
        " EnvirEng, EnvirOth, EquipNonMaint, Tax, Insur, OthNonVolSupply, OthNonVolSafety" & _
        ", OthNonVolComm, OthNonVolDonations, OthNonVolDues, OthNonVolTravel, OthNonVolTr" & _
        "ain, OthNonVolComputer, OthNonVolTanks, OthNonVolOth, ChemicalsAlkyAcid, Chemica" & _
        "lsLube, ChemicalsH2OTreat, ChemicalsProcess, ChemicalsOthAcid, ChemicalsGasAdd, " & _
        "ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, ChemicalsClay, ChemicalsAmines" & _
        ", ChemicalsASESolv, ChemicalsWasteH2O, ChemicalsNMP, ChemicalsFurfural, Chemical" & _
        "sMIBK, ChemicalsMEK, ChemicalsToluene, ChemicalsPropane, ChemicalsOthSolv, Chemi" & _
        "calsDewaxAids, ChemicalsOth, CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, Cataly" & _
        "stsDHYT, CatalystsVHYT, CatalystsRHYT, CatalystsHYFT, CatalystsCDWAX, CatalystsR" & _
        "EF, CatalystsHYG, CatalystsS2Plant, CatalystsPetChem, CatalystsOth, PurOthN2, Pu" & _
        "rOthH2O, PurOthOth, Royalties, OthVolDemCrude, OthVolDemLightering, OthVolDemPro" & _
        "d, EmissionsTaxes, EmissionsPurch, EmissionsCredits, OthVolOth, Currency, DataTy" & _
        "pe, SubmissionID FROM dbo.OpexAdd WHERE (Currency = @Currency) AND (DataType = @" & _
        "DataType) AND (SubmissionID = @SubmissionID)"
        Me.SqlUpdateCommand20.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenAbs", System.Data.SqlDbType.Real, 4, "OCCBenAbs"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenInsur", System.Data.SqlDbType.Real, 4, "OCCBenInsur"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenPension", System.Data.SqlDbType.Real, 4, "OCCBenPension"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenSub", System.Data.SqlDbType.Real, 4, "OCCBenSub"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenStock", System.Data.SqlDbType.Real, 4, "OCCBenStock"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxPen", System.Data.SqlDbType.Real, 4, "OCCBenTaxPen"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxMed", System.Data.SqlDbType.Real, 4, "OCCBenTaxMed"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxOth", System.Data.SqlDbType.Real, 4, "OCCBenTaxOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenAbs", System.Data.SqlDbType.Real, 4, "MPSBenAbs"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenInsur", System.Data.SqlDbType.Real, 4, "MPSBenInsur"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenPension", System.Data.SqlDbType.Real, 4, "MPSBenPension"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenSub", System.Data.SqlDbType.Real, 4, "MPSBenSub"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenStock", System.Data.SqlDbType.Real, 4, "MPSBenStock"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxPen", System.Data.SqlDbType.Real, 4, "MPSBenTaxPen"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxMed", System.Data.SqlDbType.Real, 4, "MPSBenTaxMed"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxOth", System.Data.SqlDbType.Real, 4, "MPSBenTaxOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatl", System.Data.SqlDbType.Real, 4, "MaintMatl"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintMatl", System.Data.SqlDbType.Real, 4, "ContMaintMatl"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipMaint", System.Data.SqlDbType.Real, 4, "EquipMaint"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContProcOp", System.Data.SqlDbType.Real, 4, "OthContProcOp"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContTransOp", System.Data.SqlDbType.Real, 4, "OthContTransOp"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFire", System.Data.SqlDbType.Real, 4, "OthContFire"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContVacTrucks", System.Data.SqlDbType.Real, 4, "OthContVacTrucks"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContConsult", System.Data.SqlDbType.Real, 4, "OthContConsult"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContInsp", System.Data.SqlDbType.Real, 4, "OthContInsp"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContSecurity", System.Data.SqlDbType.Real, 4, "OthContSecurity"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContComputing", System.Data.SqlDbType.Real, 4, "OthContComputing"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContJan", System.Data.SqlDbType.Real, 4, "OthContJan"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLab", System.Data.SqlDbType.Real, 4, "OthContLab"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFoodSvc", System.Data.SqlDbType.Real, 4, "OthContFoodSvc"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContAdmin", System.Data.SqlDbType.Real, 4, "OthContAdmin"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLegal", System.Data.SqlDbType.Real, 4, "OthContLegal"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContOth", System.Data.SqlDbType.Real, 4, "OthContOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirDisp", System.Data.SqlDbType.Real, 4, "EnvirDisp"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirPermits", System.Data.SqlDbType.Real, 4, "EnvirPermits"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirFines", System.Data.SqlDbType.Real, 4, "EnvirFines"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirSpill", System.Data.SqlDbType.Real, 4, "EnvirSpill"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirLab", System.Data.SqlDbType.Real, 4, "EnvirLab"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirEng", System.Data.SqlDbType.Real, 4, "EnvirEng"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirOth", System.Data.SqlDbType.Real, 4, "EnvirOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipNonMaint", System.Data.SqlDbType.Real, 4, "EquipNonMaint"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tax", System.Data.SqlDbType.Real, 4, "Tax"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Insur", System.Data.SqlDbType.Real, 4, "Insur"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSupply", System.Data.SqlDbType.Real, 4, "OthNonVolSupply"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSafety", System.Data.SqlDbType.Real, 4, "OthNonVolSafety"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComm", System.Data.SqlDbType.Real, 4, "OthNonVolComm"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDonations", System.Data.SqlDbType.Real, 4, "OthNonVolDonations"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDues", System.Data.SqlDbType.Real, 4, "OthNonVolDues"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTravel", System.Data.SqlDbType.Real, 4, "OthNonVolTravel"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTrain", System.Data.SqlDbType.Real, 4, "OthNonVolTrain"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComputer", System.Data.SqlDbType.Real, 4, "OthNonVolComputer"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTanks", System.Data.SqlDbType.Real, 4, "OthNonVolTanks"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolOth", System.Data.SqlDbType.Real, 4, "OthNonVolOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, "ChemicalsAlkyAcid"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsLube", System.Data.SqlDbType.Real, 4, "ChemicalsLube"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, "ChemicalsH2OTreat"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsProcess", System.Data.SqlDbType.Real, 4, "ChemicalsProcess"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, "ChemicalsOthAcid"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, "ChemicalsGasAdd"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, "ChemicalsDieselAdd"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, "ChemicalsOthAdd"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsO2", System.Data.SqlDbType.Real, 4, "ChemicalsO2"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsClay", System.Data.SqlDbType.Real, 4, "ChemicalsClay"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAmines", System.Data.SqlDbType.Real, 4, "ChemicalsAmines"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsASESolv", System.Data.SqlDbType.Real, 4, "ChemicalsASESolv"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, "ChemicalsWasteH2O"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsNMP", System.Data.SqlDbType.Real, 4, "ChemicalsNMP"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsFurfural", System.Data.SqlDbType.Real, 4, "ChemicalsFurfural"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMIBK", System.Data.SqlDbType.Real, 4, "ChemicalsMIBK"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMEK", System.Data.SqlDbType.Real, 4, "ChemicalsMEK"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsToluene", System.Data.SqlDbType.Real, 4, "ChemicalsToluene"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsPropane", System.Data.SqlDbType.Real, 4, "ChemicalsPropane"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, "ChemicalsOthSolv"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, "ChemicalsDewaxAids"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOth", System.Data.SqlDbType.Real, 4, "ChemicalsOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsFCC", System.Data.SqlDbType.Real, 4, "CatalystsFCC"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYC", System.Data.SqlDbType.Real, 4, "CatalystsHYC"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, "CatalystsNKSHYT"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsDHYT", System.Data.SqlDbType.Real, 4, "CatalystsDHYT"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsVHYT", System.Data.SqlDbType.Real, 4, "CatalystsVHYT"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsRHYT", System.Data.SqlDbType.Real, 4, "CatalystsRHYT"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYFT", System.Data.SqlDbType.Real, 4, "CatalystsHYFT"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsCDWAX", System.Data.SqlDbType.Real, 4, "CatalystsCDWAX"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsREF", System.Data.SqlDbType.Real, 4, "CatalystsREF"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYG", System.Data.SqlDbType.Real, 4, "CatalystsHYG"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsS2Plant", System.Data.SqlDbType.Real, 4, "CatalystsS2Plant"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsPetChem", System.Data.SqlDbType.Real, 4, "CatalystsPetChem"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsOth", System.Data.SqlDbType.Real, 4, "CatalystsOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthN2", System.Data.SqlDbType.Real, 4, "PurOthN2"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthH2O", System.Data.SqlDbType.Real, 4, "PurOthH2O"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthOth", System.Data.SqlDbType.Real, 4, "PurOthOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Royalties", System.Data.SqlDbType.Real, 4, "Royalties"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemCrude", System.Data.SqlDbType.Real, 4, "OthVolDemCrude"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemLightering", System.Data.SqlDbType.Real, 4, "OthVolDemLightering"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemProd", System.Data.SqlDbType.Real, 4, "OthVolDemProd"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsTaxes", System.Data.SqlDbType.Real, 4, "EmissionsTaxes"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsPurch", System.Data.SqlDbType.Real, 4, "EmissionsPurch"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsCredits", System.Data.SqlDbType.Real, 4, "EmissionsCredits"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolOth", System.Data.SqlDbType.Real, 4, "OthVolOth"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsCDWAX", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsCDWAX", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsDHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsDHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsFCC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsFCC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYFT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYFT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsNKSHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsPetChem", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsPetChem", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsREF", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsREF", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsRHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsRHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsS2Plant", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsS2Plant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsVHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsVHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsASESolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsASESolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAlkyAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAmines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAmines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsClay", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsClay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDewaxAids", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDieselAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsFurfural", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsFurfural", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsGasAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsH2OTreat", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsLube", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsLube", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMEK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMEK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMIBK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMIBK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsNMP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsNMP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthSolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsProcess", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsProcess", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsPropane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsPropane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsToluene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsToluene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsWasteH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsCredits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsCredits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsPurch", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsPurch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsTaxes", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsTaxes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirDisp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirDisp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirEng", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirEng", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirPermits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirPermits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirSpill", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirSpill", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipNonMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipNonMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Insur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Insur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContAdmin", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContAdmin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContComputing", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContComputing", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContConsult", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContConsult", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFire", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFire", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFoodSvc", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFoodSvc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContInsp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContInsp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContJan", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContJan", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLegal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLegal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContProcOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContProcOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContSecurity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContSecurity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContTransOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContTransOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContVacTrucks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContVacTrucks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComm", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComm", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComputer", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComputer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDonations", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDonations", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDues", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDues", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSupply", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSupply", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTanks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTanks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTrain", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTrain", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTravel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTravel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemCrude", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemCrude", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemLightering", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemLightering", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthN2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthN2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Royalties", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Royalties", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand20.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tax", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdUnitTargetsNew
        '
        Me.sdUnitTargetsNew.DeleteCommand = Me.SqlDeleteCommand21
        Me.sdUnitTargetsNew.InsertCommand = Me.SqlInsertCommand22
        Me.sdUnitTargetsNew.SelectCommand = Me.SqlSelectCommand22
        Me.sdUnitTargetsNew.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "UnitTargetsNew", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("UnitID", "UnitID"), New System.Data.Common.DataColumnMapping("Property", "Property"), New System.Data.Common.DataColumnMapping("Target", "Target"), New System.Data.Common.DataColumnMapping("CurrencyCode", "CurrencyCode")})})
        Me.sdUnitTargetsNew.UpdateCommand = Me.SqlUpdateCommand21
        '
        'SqlDeleteCommand21
        '
        Me.SqlDeleteCommand21.CommandText = "DELETE FROM dbo.UnitTargetsNew WHERE (Property = @Original_Property) AND (Submiss" & _
        "ionID = @Original_SubmissionID) AND (UnitID = @Original_UnitID) AND (CurrencyCod" & _
        "e = @Original_CurrencyCode OR @Original_CurrencyCode IS NULL AND CurrencyCode IS" & _
        " NULL) AND (Target = @Original_Target OR @Original_Target IS NULL AND Target IS " & _
        "NULL)"
        Me.SqlDeleteCommand21.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Property", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Property", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CurrencyCode", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CurrencyCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Target", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Target", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand22
        '
        Me.SqlInsertCommand22.CommandText = "INSERT INTO dbo.UnitTargetsNew(SubmissionID, UnitID, Property, Target, CurrencyCo" & _
        "de) VALUES (@SubmissionID, @UnitID, @Property, @Target, @CurrencyCode)"
        Me.SqlInsertCommand22.Connection = Me.SqlConnection1
        Me.SqlInsertCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlInsertCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Property", System.Data.SqlDbType.VarChar, 50, "Property"))
        Me.SqlInsertCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Target", System.Data.SqlDbType.Real, 4, "Target"))
        Me.SqlInsertCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CurrencyCode", System.Data.SqlDbType.VarChar, 4, "CurrencyCode"))
        '
        'SqlSelectCommand22
        '
        Me.SqlSelectCommand22.CommandText = "SELECT SubmissionID, UnitID, Property, Target, CurrencyCode FROM dbo.UnitTargetsN" & _
        "ew"
        Me.SqlSelectCommand22.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand21
        '
        Me.SqlUpdateCommand21.CommandText = "UPDATE dbo.UnitTargetsNew SET SubmissionID = @SubmissionID, UnitID = @UnitID, Pro" & _
        "perty = @Property, Target = @Target, CurrencyCode = @CurrencyCode WHERE (Propert" & _
        "y = @Original_Property) AND (SubmissionID = @Original_SubmissionID) AND (UnitID " & _
        "= @Original_UnitID) AND (CurrencyCode = @Original_CurrencyCode OR @Original_Curr" & _
        "encyCode IS NULL AND CurrencyCode IS NULL) AND (Target = @Original_Target OR @Or" & _
        "iginal_Target IS NULL AND Target IS NULL)"
        Me.SqlUpdateCommand21.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Property", System.Data.SqlDbType.VarChar, 50, "Property"))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Target", System.Data.SqlDbType.Real, 4, "Target"))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CurrencyCode", System.Data.SqlDbType.VarChar, 4, "CurrencyCode"))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Property", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Property", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CurrencyCode", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CurrencyCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand21.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Target", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Target", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdLoadEDCStabilizers
        '
        Me.sdLoadEDCStabilizers.DeleteCommand = Me.SqlDeleteCommand22
        Me.sdLoadEDCStabilizers.InsertCommand = Me.SqlInsertCommand23
        Me.sdLoadEDCStabilizers.SelectCommand = Me.SqlSelectCommand23
        Me.sdLoadEDCStabilizers.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "LoadEDCStabilizers", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RefineryID", "RefineryID"), New System.Data.Common.DataColumnMapping("DataSet", "DataSet"), New System.Data.Common.DataColumnMapping("EffDate", "EffDate"), New System.Data.Common.DataColumnMapping("AnnInputBbl", "AnnInputBbl"), New System.Data.Common.DataColumnMapping("AnnCokeBbl", "AnnCokeBbl"), New System.Data.Common.DataColumnMapping("AnnElecConsMWH", "AnnElecConsMWH"), New System.Data.Common.DataColumnMapping("AnnRSCRUDE_RAIL", "AnnRSCRUDE_RAIL"), New System.Data.Common.DataColumnMapping("AnnRSCRUDE_TT", "AnnRSCRUDE_TT"), New System.Data.Common.DataColumnMapping("AnnRSCRUDE_TB", "AnnRSCRUDE_TB"), New System.Data.Common.DataColumnMapping("AnnRSCRUDE_OMB", "AnnRSCRUDE_OMB"), New System.Data.Common.DataColumnMapping("AnnRSCRUDE_BB", "AnnRSCRUDE_BB"), New System.Data.Common.DataColumnMapping("AnnRSPROD_RAIL", "AnnRSPROD_RAIL"), New System.Data.Common.DataColumnMapping("AnnRSPROD_TT", "AnnRSPROD_TT"), New System.Data.Common.DataColumnMapping("AnnRSPROD_TB", "AnnRSPROD_TB"), New System.Data.Common.DataColumnMapping("AnnRSPROD_OMB", "AnnRSPROD_OMB"), New System.Data.Common.DataColumnMapping("AnnRSPROD_BB", "AnnRSPROD_BB")})})
        Me.sdLoadEDCStabilizers.UpdateCommand = Me.SqlUpdateCommand22
        '
        'SqlDeleteCommand22
        '
        Me.SqlDeleteCommand22.CommandText = "DELETE FROM dbo.LoadEDCStabilizers WHERE (DataSet = @Original_DataSet) AND (EffDa" & _
        "te = @Original_EffDate) AND (RefineryID = @Original_RefineryID) AND (AnnCokeBbl " & _
        "= @Original_AnnCokeBbl OR @Original_AnnCokeBbl IS NULL AND AnnCokeBbl IS NULL) A" & _
        "ND (AnnElecConsMWH = @Original_AnnElecConsMWH OR @Original_AnnElecConsMWH IS NUL" & _
        "L AND AnnElecConsMWH IS NULL) AND (AnnInputBbl = @Original_AnnInputBbl OR @Origi" & _
        "nal_AnnInputBbl IS NULL AND AnnInputBbl IS NULL) AND (AnnRSCRUDE_BB = @Original_" & _
        "AnnRSCRUDE_BB OR @Original_AnnRSCRUDE_BB IS NULL AND AnnRSCRUDE_BB IS NULL) AND " & _
        "(AnnRSCRUDE_OMB = @Original_AnnRSCRUDE_OMB OR @Original_AnnRSCRUDE_OMB IS NULL A" & _
        "ND AnnRSCRUDE_OMB IS NULL) AND (AnnRSCRUDE_RAIL = @Original_AnnRSCRUDE_RAIL OR @" & _
        "Original_AnnRSCRUDE_RAIL IS NULL AND AnnRSCRUDE_RAIL IS NULL) AND (AnnRSCRUDE_TB" & _
        " = @Original_AnnRSCRUDE_TB OR @Original_AnnRSCRUDE_TB IS NULL AND AnnRSCRUDE_TB " & _
        "IS NULL) AND (AnnRSCRUDE_TT = @Original_AnnRSCRUDE_TT OR @Original_AnnRSCRUDE_TT" & _
        " IS NULL AND AnnRSCRUDE_TT IS NULL) AND (AnnRSPROD_BB = @Original_AnnRSPROD_BB O" & _
        "R @Original_AnnRSPROD_BB IS NULL AND AnnRSPROD_BB IS NULL) AND (AnnRSPROD_OMB = " & _
        "@Original_AnnRSPROD_OMB OR @Original_AnnRSPROD_OMB IS NULL AND AnnRSPROD_OMB IS " & _
        "NULL) AND (AnnRSPROD_RAIL = @Original_AnnRSPROD_RAIL OR @Original_AnnRSPROD_RAIL" & _
        " IS NULL AND AnnRSPROD_RAIL IS NULL) AND (AnnRSPROD_TB = @Original_AnnRSPROD_TB " & _
        "OR @Original_AnnRSPROD_TB IS NULL AND AnnRSPROD_TB IS NULL) AND (AnnRSPROD_TT = " & _
        "@Original_AnnRSPROD_TT OR @Original_AnnRSPROD_TT IS NULL AND AnnRSPROD_TT IS NUL" & _
        "L)"
        Me.SqlDeleteCommand22.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EffDate", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EffDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnCokeBbl", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnCokeBbl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnElecConsMWH", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnElecConsMWH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnInputBbl", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnInputBbl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_BB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_BB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_OMB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_OMB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_RAIL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_RAIL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_TB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_TB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_TT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_TT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_BB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_BB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_OMB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_OMB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_RAIL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_RAIL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_TB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_TB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_TT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_TT", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand23
        '
        Me.SqlInsertCommand23.CommandText = "INSERT INTO dbo.LoadEDCStabilizers(RefineryID, DataSet, EffDate, AnnInputBbl, Ann" & _
        "CokeBbl, AnnElecConsMWH, AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRU" & _
        "DE_OMB, AnnRSCRUDE_BB, AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB" & _
        ", AnnRSPROD_BB) VALUES (@RefineryID, @DataSet, @EffDate, @AnnInputBbl, @AnnCokeB" & _
        "bl, @AnnElecConsMWH, @AnnRSCRUDE_RAIL, @AnnRSCRUDE_TT, @AnnRSCRUDE_TB, @AnnRSCRU" & _
        "DE_OMB, @AnnRSCRUDE_BB, @AnnRSPROD_RAIL, @AnnRSPROD_TT, @AnnRSPROD_TB, @AnnRSPRO" & _
        "D_OMB, @AnnRSPROD_BB)"
        Me.SqlInsertCommand23.Connection = Me.SqlConnection1
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EffDate", System.Data.SqlDbType.DateTime, 4, "EffDate"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnInputBbl", System.Data.SqlDbType.Float, 8, "AnnInputBbl"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnCokeBbl", System.Data.SqlDbType.Float, 8, "AnnCokeBbl"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnElecConsMWH", System.Data.SqlDbType.Float, 8, "AnnElecConsMWH"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_RAIL", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_RAIL"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_TT", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_TT"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_TB", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_TB"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_OMB", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_OMB"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_BB", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_BB"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_RAIL", System.Data.SqlDbType.Float, 8, "AnnRSPROD_RAIL"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_TT", System.Data.SqlDbType.Float, 8, "AnnRSPROD_TT"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_TB", System.Data.SqlDbType.Float, 8, "AnnRSPROD_TB"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_OMB", System.Data.SqlDbType.Float, 8, "AnnRSPROD_OMB"))
        Me.SqlInsertCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_BB", System.Data.SqlDbType.Float, 8, "AnnRSPROD_BB"))
        '
        'SqlSelectCommand23
        '
        Me.SqlSelectCommand23.CommandText = "SELECT RefineryID, DataSet, EffDate, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, Ann" & _
        "RSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, AnnRS" & _
        "PROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB FROM dbo.Load" & _
        "EDCStabilizers"
        Me.SqlSelectCommand23.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand22
        '
        Me.SqlUpdateCommand22.CommandText = "UPDATE dbo.LoadEDCStabilizers SET RefineryID = @RefineryID, DataSet = @DataSet, E" & _
        "ffDate = @EffDate, AnnInputBbl = @AnnInputBbl, AnnCokeBbl = @AnnCokeBbl, AnnElec" & _
        "ConsMWH = @AnnElecConsMWH, AnnRSCRUDE_RAIL = @AnnRSCRUDE_RAIL, AnnRSCRUDE_TT = @" & _
        "AnnRSCRUDE_TT, AnnRSCRUDE_TB = @AnnRSCRUDE_TB, AnnRSCRUDE_OMB = @AnnRSCRUDE_OMB," & _
        " AnnRSCRUDE_BB = @AnnRSCRUDE_BB, AnnRSPROD_RAIL = @AnnRSPROD_RAIL, AnnRSPROD_TT " & _
        "= @AnnRSPROD_TT, AnnRSPROD_TB = @AnnRSPROD_TB, AnnRSPROD_OMB = @AnnRSPROD_OMB, A" & _
        "nnRSPROD_BB = @AnnRSPROD_BB WHERE (DataSet = @Original_DataSet) AND (EffDate = @" & _
        "Original_EffDate) AND (RefineryID = @Original_RefineryID) AND (AnnCokeBbl = @Ori" & _
        "ginal_AnnCokeBbl OR @Original_AnnCokeBbl IS NULL AND AnnCokeBbl IS NULL) AND (An" & _
        "nElecConsMWH = @Original_AnnElecConsMWH OR @Original_AnnElecConsMWH IS NULL AND " & _
        "AnnElecConsMWH IS NULL) AND (AnnInputBbl = @Original_AnnInputBbl OR @Original_An" & _
        "nInputBbl IS NULL AND AnnInputBbl IS NULL) AND (AnnRSCRUDE_BB = @Original_AnnRSC" & _
        "RUDE_BB OR @Original_AnnRSCRUDE_BB IS NULL AND AnnRSCRUDE_BB IS NULL) AND (AnnRS" & _
        "CRUDE_OMB = @Original_AnnRSCRUDE_OMB OR @Original_AnnRSCRUDE_OMB IS NULL AND Ann" & _
        "RSCRUDE_OMB IS NULL) AND (AnnRSCRUDE_RAIL = @Original_AnnRSCRUDE_RAIL OR @Origin" & _
        "al_AnnRSCRUDE_RAIL IS NULL AND AnnRSCRUDE_RAIL IS NULL) AND (AnnRSCRUDE_TB = @Or" & _
        "iginal_AnnRSCRUDE_TB OR @Original_AnnRSCRUDE_TB IS NULL AND AnnRSCRUDE_TB IS NUL" & _
        "L) AND (AnnRSCRUDE_TT = @Original_AnnRSCRUDE_TT OR @Original_AnnRSCRUDE_TT IS NU" & _
        "LL AND AnnRSCRUDE_TT IS NULL) AND (AnnRSPROD_BB = @Original_AnnRSPROD_BB OR @Ori" & _
        "ginal_AnnRSPROD_BB IS NULL AND AnnRSPROD_BB IS NULL) AND (AnnRSPROD_OMB = @Origi" & _
        "nal_AnnRSPROD_OMB OR @Original_AnnRSPROD_OMB IS NULL AND AnnRSPROD_OMB IS NULL) " & _
        "AND (AnnRSPROD_RAIL = @Original_AnnRSPROD_RAIL OR @Original_AnnRSPROD_RAIL IS NU" & _
        "LL AND AnnRSPROD_RAIL IS NULL) AND (AnnRSPROD_TB = @Original_AnnRSPROD_TB OR @Or" & _
        "iginal_AnnRSPROD_TB IS NULL AND AnnRSPROD_TB IS NULL) AND (AnnRSPROD_TT = @Origi" & _
        "nal_AnnRSPROD_TT OR @Original_AnnRSPROD_TT IS NULL AND AnnRSPROD_TT IS NULL)"
        Me.SqlUpdateCommand22.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6, "RefineryID"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15, "DataSet"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EffDate", System.Data.SqlDbType.DateTime, 4, "EffDate"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnInputBbl", System.Data.SqlDbType.Float, 8, "AnnInputBbl"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnCokeBbl", System.Data.SqlDbType.Float, 8, "AnnCokeBbl"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnElecConsMWH", System.Data.SqlDbType.Float, 8, "AnnElecConsMWH"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_RAIL", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_RAIL"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_TT", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_TT"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_TB", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_TB"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_OMB", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_OMB"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSCRUDE_BB", System.Data.SqlDbType.Float, 8, "AnnRSCRUDE_BB"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_RAIL", System.Data.SqlDbType.Float, 8, "AnnRSPROD_RAIL"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_TT", System.Data.SqlDbType.Float, 8, "AnnRSPROD_TT"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_TB", System.Data.SqlDbType.Float, 8, "AnnRSPROD_TB"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_OMB", System.Data.SqlDbType.Float, 8, "AnnRSPROD_OMB"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AnnRSPROD_BB", System.Data.SqlDbType.Float, 8, "AnnRSPROD_BB"))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataSet", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataSet", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EffDate", System.Data.SqlDbType.DateTime, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EffDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RefineryID", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RefineryID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnCokeBbl", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnCokeBbl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnElecConsMWH", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnElecConsMWH", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnInputBbl", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnInputBbl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_BB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_BB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_OMB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_OMB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_RAIL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_RAIL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_TB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_TB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSCRUDE_TT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSCRUDE_TT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_BB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_BB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_OMB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_OMB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_RAIL", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_RAIL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_TB", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_TB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand22.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AnnRSPROD_TT", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AnnRSPROD_TT", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdOpexAll1
        '
        Me.sdOpexAll1.DeleteCommand = Me.SqlDeleteCommand23
        Me.sdOpexAll1.InsertCommand = Me.SqlInsertCommand24
        Me.sdOpexAll1.SelectCommand = Me.SqlSelectCommand24
        Me.sdOpexAll1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpExAll", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID"), New System.Data.Common.DataColumnMapping("Currency", "Currency"), New System.Data.Common.DataColumnMapping("Scenario", "Scenario"), New System.Data.Common.DataColumnMapping("DataType", "DataType"), New System.Data.Common.DataColumnMapping("OCCSal", "OCCSal"), New System.Data.Common.DataColumnMapping("MPSSal", "MPSSal"), New System.Data.Common.DataColumnMapping("OCCBenAbs", "OCCBenAbs"), New System.Data.Common.DataColumnMapping("OCCBenInsur", "OCCBenInsur"), New System.Data.Common.DataColumnMapping("OCCBenPension", "OCCBenPension"), New System.Data.Common.DataColumnMapping("OCCBenSub", "OCCBenSub"), New System.Data.Common.DataColumnMapping("OCCBenStock", "OCCBenStock"), New System.Data.Common.DataColumnMapping("OCCBenTaxPen", "OCCBenTaxPen"), New System.Data.Common.DataColumnMapping("OCCBenTaxMed", "OCCBenTaxMed"), New System.Data.Common.DataColumnMapping("OCCBenTaxOth", "OCCBenTaxOth"), New System.Data.Common.DataColumnMapping("OCCBen", "OCCBen"), New System.Data.Common.DataColumnMapping("MPSBenAbs", "MPSBenAbs"), New System.Data.Common.DataColumnMapping("MPSBenInsur", "MPSBenInsur"), New System.Data.Common.DataColumnMapping("MPSBenPension", "MPSBenPension"), New System.Data.Common.DataColumnMapping("MPSBenSub", "MPSBenSub"), New System.Data.Common.DataColumnMapping("MPSBenStock", "MPSBenStock"), New System.Data.Common.DataColumnMapping("MPSBenTaxPen", "MPSBenTaxPen"), New System.Data.Common.DataColumnMapping("MPSBenTaxMed", "MPSBenTaxMed"), New System.Data.Common.DataColumnMapping("MPSBenTaxOth", "MPSBenTaxOth"), New System.Data.Common.DataColumnMapping("MPSBen", "MPSBen"), New System.Data.Common.DataColumnMapping("MaintMatl", "MaintMatl"), New System.Data.Common.DataColumnMapping("ContMaintMatl", "ContMaintMatl"), New System.Data.Common.DataColumnMapping("EquipMaint", "EquipMaint"), New System.Data.Common.DataColumnMapping("MaintMatlST", "MaintMatlST"), New System.Data.Common.DataColumnMapping("ContMaintLabor", "ContMaintLabor"), New System.Data.Common.DataColumnMapping("ContMaintInspect", "ContMaintInspect"), New System.Data.Common.DataColumnMapping("ContMaintLaborST", "ContMaintLaborST"), New System.Data.Common.DataColumnMapping("OthContProcOp", "OthContProcOp"), New System.Data.Common.DataColumnMapping("OthContTransOp", "OthContTransOp"), New System.Data.Common.DataColumnMapping("OthContVacTrucks", "OthContVacTrucks"), New System.Data.Common.DataColumnMapping("OthContFire", "OthContFire"), New System.Data.Common.DataColumnMapping("OthContConsult", "OthContConsult"), New System.Data.Common.DataColumnMapping("OthContSecurity", "OthContSecurity"), New System.Data.Common.DataColumnMapping("OthContComputing", "OthContComputing"), New System.Data.Common.DataColumnMapping("OthContJan", "OthContJan"), New System.Data.Common.DataColumnMapping("OthContLab", "OthContLab"), New System.Data.Common.DataColumnMapping("OthContFoodSvc", "OthContFoodSvc"), New System.Data.Common.DataColumnMapping("OthContAdmin", "OthContAdmin"), New System.Data.Common.DataColumnMapping("OthContLegal", "OthContLegal"), New System.Data.Common.DataColumnMapping("OthContOth", "OthContOth"), New System.Data.Common.DataColumnMapping("OthCont", "OthCont"), New System.Data.Common.DataColumnMapping("TAAdj", "TAAdj"), New System.Data.Common.DataColumnMapping("EnvirDisp", "EnvirDisp"), New System.Data.Common.DataColumnMapping("EnvirPermits", "EnvirPermits"), New System.Data.Common.DataColumnMapping("EnvirFines", "EnvirFines"), New System.Data.Common.DataColumnMapping("EnvirSpill", "EnvirSpill"), New System.Data.Common.DataColumnMapping("EnvirLab", "EnvirLab"), New System.Data.Common.DataColumnMapping("EnvirEng", "EnvirEng"), New System.Data.Common.DataColumnMapping("EnvirOth", "EnvirOth"), New System.Data.Common.DataColumnMapping("Envir", "Envir"), New System.Data.Common.DataColumnMapping("EquipNonMaint", "EquipNonMaint"), New System.Data.Common.DataColumnMapping("Tax", "Tax"), New System.Data.Common.DataColumnMapping("InsurBI", "InsurBI"), New System.Data.Common.DataColumnMapping("InsurPC", "InsurPC"), New System.Data.Common.DataColumnMapping("InsurOth", "InsurOth"), New System.Data.Common.DataColumnMapping("OthNonVolSupply", "OthNonVolSupply"), New System.Data.Common.DataColumnMapping("OthNonVolSafety", "OthNonVolSafety"), New System.Data.Common.DataColumnMapping("OthNonVolNonPersSafety", "OthNonVolNonPersSafety"), New System.Data.Common.DataColumnMapping("OthNonVolComm", "OthNonVolComm"), New System.Data.Common.DataColumnMapping("OthNonVolDonations", "OthNonVolDonations"), New System.Data.Common.DataColumnMapping("OthNonVolNonContribPers", "OthNonVolNonContribPers"), New System.Data.Common.DataColumnMapping("OthNonVolDues", "OthNonVolDues"), New System.Data.Common.DataColumnMapping("OthNonVolTravel", "OthNonVolTravel"), New System.Data.Common.DataColumnMapping("OthNonVolTrain", "OthNonVolTrain"), New System.Data.Common.DataColumnMapping("OthNonVolComputer", "OthNonVolComputer"), New System.Data.Common.DataColumnMapping("OthNonVolTanks", "OthNonVolTanks"), New System.Data.Common.DataColumnMapping("OthNonVolExtraExpat", "OthNonVolExtraExpat"), New System.Data.Common.DataColumnMapping("OthNonVolOth", "OthNonVolOth"), New System.Data.Common.DataColumnMapping("OthNonVol", "OthNonVol"), New System.Data.Common.DataColumnMapping("STNonVol", "STNonVol"), New System.Data.Common.DataColumnMapping("GAPers", "GAPers"), New System.Data.Common.DataColumnMapping("ChemicalsAntiknock", "ChemicalsAntiknock"), New System.Data.Common.DataColumnMapping("ChemicalsAlkyAcid", "ChemicalsAlkyAcid"), New System.Data.Common.DataColumnMapping("ChemicalsLube", "ChemicalsLube"), New System.Data.Common.DataColumnMapping("ChemicalsH2OTreat", "ChemicalsH2OTreat"), New System.Data.Common.DataColumnMapping("ChemicalsOthAcid", "ChemicalsOthAcid"), New System.Data.Common.DataColumnMapping("ChemicalsProcess", "ChemicalsProcess"), New System.Data.Common.DataColumnMapping("ChemicalsGasAdd", "ChemicalsGasAdd"), New System.Data.Common.DataColumnMapping("ChemicalsDieselAdd", "ChemicalsDieselAdd"), New System.Data.Common.DataColumnMapping("ChemicalsOthAdd", "ChemicalsOthAdd"), New System.Data.Common.DataColumnMapping("ChemicalsO2", "ChemicalsO2"), New System.Data.Common.DataColumnMapping("ChemicalsClay", "ChemicalsClay"), New System.Data.Common.DataColumnMapping("ChemicalsAmines", "ChemicalsAmines"), New System.Data.Common.DataColumnMapping("ChemicalsASESolv", "ChemicalsASESolv"), New System.Data.Common.DataColumnMapping("ChemicalsWasteH2O", "ChemicalsWasteH2O"), New System.Data.Common.DataColumnMapping("ChemicalsNMP", "ChemicalsNMP"), New System.Data.Common.DataColumnMapping("ChemicalsFurfural", "ChemicalsFurfural"), New System.Data.Common.DataColumnMapping("ChemicalsMIBK", "ChemicalsMIBK"), New System.Data.Common.DataColumnMapping("ChemicalsMEK", "ChemicalsMEK"), New System.Data.Common.DataColumnMapping("ChemicalsToluene", "ChemicalsToluene"), New System.Data.Common.DataColumnMapping("ChemicalsPropane", "ChemicalsPropane"), New System.Data.Common.DataColumnMapping("ChemicalsOthSolv", "ChemicalsOthSolv"), New System.Data.Common.DataColumnMapping("ChemicalsAcid", "ChemicalsAcid"), New System.Data.Common.DataColumnMapping("ChemicalsDewaxAids", "ChemicalsDewaxAids"), New System.Data.Common.DataColumnMapping("ChemicalsOth", "ChemicalsOth"), New System.Data.Common.DataColumnMapping("Chemicals", "Chemicals")})})
        Me.sdOpexAll1.UpdateCommand = Me.SqlUpdateCommand23
        '
        'SqlDeleteCommand23
        '
        Me.SqlDeleteCommand23.CommandText = "DELETE FROM dbo.OpExAll WHERE (Currency = @Original_Currency) AND (DataType = @Or" & _
        "iginal_DataType) AND (Scenario = @Original_Scenario) AND (SubmissionID = @Origin" & _
        "al_SubmissionID) AND (Chemicals = @Original_Chemicals OR @Original_Chemicals IS " & _
        "NULL AND Chemicals IS NULL) AND (ChemicalsASESolv = @Original_ChemicalsASESolv O" & _
        "R @Original_ChemicalsASESolv IS NULL AND ChemicalsASESolv IS NULL) AND (Chemical" & _
        "sAcid = @Original_ChemicalsAcid OR @Original_ChemicalsAcid IS NULL AND Chemicals" & _
        "Acid IS NULL) AND (ChemicalsAlkyAcid = @Original_ChemicalsAlkyAcid OR @Original_" & _
        "ChemicalsAlkyAcid IS NULL AND ChemicalsAlkyAcid IS NULL) AND (ChemicalsAmines = " & _
        "@Original_ChemicalsAmines OR @Original_ChemicalsAmines IS NULL AND ChemicalsAmin" & _
        "es IS NULL) AND (ChemicalsAntiknock = @Original_ChemicalsAntiknock OR @Original_" & _
        "ChemicalsAntiknock IS NULL AND ChemicalsAntiknock IS NULL) AND (ChemicalsClay = " & _
        "@Original_ChemicalsClay OR @Original_ChemicalsClay IS NULL AND ChemicalsClay IS " & _
        "NULL) AND (ChemicalsDewaxAids = @Original_ChemicalsDewaxAids OR @Original_Chemic" & _
        "alsDewaxAids IS NULL AND ChemicalsDewaxAids IS NULL) AND (ChemicalsDieselAdd = @" & _
        "Original_ChemicalsDieselAdd OR @Original_ChemicalsDieselAdd IS NULL AND Chemical" & _
        "sDieselAdd IS NULL) AND (ChemicalsFurfural = @Original_ChemicalsFurfural OR @Ori" & _
        "ginal_ChemicalsFurfural IS NULL AND ChemicalsFurfural IS NULL) AND (ChemicalsGas" & _
        "Add = @Original_ChemicalsGasAdd OR @Original_ChemicalsGasAdd IS NULL AND Chemica" & _
        "lsGasAdd IS NULL) AND (ChemicalsH2OTreat = @Original_ChemicalsH2OTreat OR @Origi" & _
        "nal_ChemicalsH2OTreat IS NULL AND ChemicalsH2OTreat IS NULL) AND (ChemicalsLube " & _
        "= @Original_ChemicalsLube OR @Original_ChemicalsLube IS NULL AND ChemicalsLube I" & _
        "S NULL) AND (ChemicalsMEK = @Original_ChemicalsMEK OR @Original_ChemicalsMEK IS " & _
        "NULL AND ChemicalsMEK IS NULL) AND (ChemicalsMIBK = @Original_ChemicalsMIBK OR @" & _
        "Original_ChemicalsMIBK IS NULL AND ChemicalsMIBK IS NULL) AND (ChemicalsNMP = @O" & _
        "riginal_ChemicalsNMP OR @Original_ChemicalsNMP IS NULL AND ChemicalsNMP IS NULL)" & _
        " AND (ChemicalsO2 = @Original_ChemicalsO2 OR @Original_ChemicalsO2 IS NULL AND C" & _
        "hemicalsO2 IS NULL) AND (ChemicalsOth = @Original_ChemicalsOth OR @Original_Chem" & _
        "icalsOth IS NULL AND ChemicalsOth IS NULL) AND (ChemicalsOthAcid = @Original_Che" & _
        "micalsOthAcid OR @Original_ChemicalsOthAcid IS NULL AND ChemicalsOthAcid IS NULL" & _
        ") AND (ChemicalsOthAdd = @Original_ChemicalsOthAdd OR @Original_ChemicalsOthAdd " & _
        "IS NULL AND ChemicalsOthAdd IS NULL) AND (ChemicalsOthSolv = @Original_Chemicals" & _
        "OthSolv OR @Original_ChemicalsOthSolv IS NULL AND ChemicalsOthSolv IS NULL) AND " & _
        "(ChemicalsProcess = @Original_ChemicalsProcess OR @Original_ChemicalsProcess IS " & _
        "NULL AND ChemicalsProcess IS NULL) AND (ChemicalsPropane = @Original_ChemicalsPr" & _
        "opane OR @Original_ChemicalsPropane IS NULL AND ChemicalsPropane IS NULL) AND (C" & _
        "hemicalsToluene = @Original_ChemicalsToluene OR @Original_ChemicalsToluene IS NU" & _
        "LL AND ChemicalsToluene IS NULL) AND (ChemicalsWasteH2O = @Original_ChemicalsWas" & _
        "teH2O OR @Original_ChemicalsWasteH2O IS NULL AND ChemicalsWasteH2O IS NULL) AND " & _
        "(ContMaintInspect = @Original_ContMaintInspect OR @Original_ContMaintInspect IS " & _
        "NULL AND ContMaintInspect IS NULL) AND (ContMaintLabor = @Original_ContMaintLabo" & _
        "r OR @Original_ContMaintLabor IS NULL AND ContMaintLabor IS NULL) AND (ContMaint" & _
        "LaborST = @Original_ContMaintLaborST OR @Original_ContMaintLaborST IS NULL AND C" & _
        "ontMaintLaborST IS NULL) AND (ContMaintMatl = @Original_ContMaintMatl OR @Origin" & _
        "al_ContMaintMatl IS NULL AND ContMaintMatl IS NULL) AND (Envir = @Original_Envir" & _
        " OR @Original_Envir IS NULL AND Envir IS NULL) AND (EnvirDisp = @Original_EnvirD" & _
        "isp OR @Original_EnvirDisp IS NULL AND EnvirDisp IS NULL) AND (EnvirEng = @Origi" & _
        "nal_EnvirEng OR @Original_EnvirEng IS NULL AND EnvirEng IS NULL) AND (EnvirFines" & _
        " = @Original_EnvirFines OR @Original_EnvirFines IS NULL AND EnvirFines IS NULL) " & _
        "AND (EnvirLab = @Original_EnvirLab OR @Original_EnvirLab IS NULL AND EnvirLab IS" & _
        " NULL) AND (EnvirOth = @Original_EnvirOth OR @Original_EnvirOth IS NULL AND Envi" & _
        "rOth IS NULL) AND (EnvirPermits = @Original_EnvirPermits OR @Original_EnvirPermi" & _
        "ts IS NULL AND EnvirPermits IS NULL) AND (EnvirSpill = @Original_EnvirSpill OR @" & _
        "Original_EnvirSpill IS NULL AND EnvirSpill IS NULL) AND (EquipMaint = @Original_" & _
        "EquipMaint OR @Original_EquipMaint IS NULL AND EquipMaint IS NULL) AND (EquipNon" & _
        "Maint = @Original_EquipNonMaint OR @Original_EquipNonMaint IS NULL AND EquipNonM" & _
        "aint IS NULL) AND (GAPers = @Original_GAPers OR @Original_GAPers IS NULL AND GAP" & _
        "ers IS NULL) AND (InsurBI = @Original_InsurBI OR @Original_InsurBI IS NULL AND I" & _
        "nsurBI IS NULL) AND (InsurOth = @Original_InsurOth OR @Original_InsurOth IS NULL" & _
        " AND InsurOth IS NULL) AND (InsurPC = @Original_InsurPC OR @Original_InsurPC IS " & _
        "NULL AND InsurPC IS NULL) AND (MPSBen = @Original_MPSBen OR @Original_MPSBen IS " & _
        "NULL AND MPSBen IS NULL) AND (MPSBenAbs = @Original_MPSBenAbs OR @Original_MPSBe" & _
        "nAbs IS NULL AND MPSBenAbs IS NULL) AND (MPSBenInsur = @Original_MPSBenInsur OR " & _
        "@Original_MPSBenInsur IS NULL AND MPSBenInsur IS NULL) AND (MPSBenPension = @Ori" & _
        "ginal_MPSBenPension OR @Original_MPSBenPension IS NULL AND MPSBenPension IS NULL" & _
        ") AND (MPSBenStock = @Original_MPSBenStock OR @Original_MPSBenStock IS NULL AND " & _
        "MPSBenStock IS NULL) AND (MPSBenSub = @Original_MPSBenSub OR @Original_MPSBenSub" & _
        " IS NULL AND MPSBenSub IS NULL) AND (MPSBenTaxMed = @Original_MPSBenTaxMed OR @O" & _
        "riginal_MPSBenTaxMed IS NULL AND MPSBenTaxMed IS NULL) AND (MPSBenTaxOth = @Orig" & _
        "inal_MPSBenTaxOth OR @Original_MPSBenTaxOth IS NULL AND MPSBenTaxOth IS NULL) AN" & _
        "D (MPSBenTaxPen = @Original_MPSBenTaxPen OR @Original_MPSBenTaxPen IS NULL AND M" & _
        "PSBenTaxPen IS NULL) AND (MPSSal = @Original_MPSSal OR @Original_MPSSal IS NULL " & _
        "AND MPSSal IS NULL) AND (MaintMatl = @Original_MaintMatl OR @Original_MaintMatl " & _
        "IS NULL AND MaintMatl IS NULL) AND (MaintMatlST = @Original_MaintMatlST OR @Orig" & _
        "inal_MaintMatlST IS NULL AND MaintMatlST IS NULL) AND (OCCBen = @Original_OCCBen" & _
        " OR @Original_OCCBen IS NULL AND OCCBen IS NULL) AND (OCCBenAbs = @Original_OCCB" & _
        "enAbs OR @Original_OCCBenAbs IS NULL AND OCCBenAbs IS NULL) AND (OCCBenInsur = @" & _
        "Original_OCCBenInsur OR @Original_OCCBenInsur IS NULL AND OCCBenInsur IS NULL) A" & _
        "ND (OCCBenPension = @Original_OCCBenPension OR @Original_OCCBenPension IS NULL A" & _
        "ND OCCBenPension IS NULL) AND (OCCBenStock = @Original_OCCBenStock OR @Original_" & _
        "OCCBenStock IS NULL AND OCCBenStock IS NULL) AND (OCCBenSub = @Original_OCCBenSu" & _
        "b OR @Original_OCCBenSub IS NULL AND OCCBenSub IS NULL) AND (OCCBenTaxMed = @Ori" & _
        "ginal_OCCBenTaxMed OR @Original_OCCBenTaxMed IS NULL AND OCCBenTaxMed IS NULL) A" & _
        "ND (OCCBenTaxOth = @Original_OCCBenTaxOth OR @Original_OCCBenTaxOth IS NULL AND " & _
        "OCCBenTaxOth IS NULL) AND (OCCBenTaxPen = @Original_OCCBenTaxPen OR @Original_OC" & _
        "CBenTaxPen IS NULL AND OCCBenTaxPen IS NULL) AND (OCCSal = @Original_OCCSal OR @" & _
        "Original_OCCSal IS NULL AND OCCSal IS NULL) AND (OthCont = @Original_OthCont OR " & _
        "@Original_OthCont IS NULL AND OthCont IS NULL) AND (OthContAdmin = @Original_Oth" & _
        "ContAdmin OR @Original_OthContAdmin IS NULL AND OthContAdmin IS NULL) AND (OthCo" & _
        "ntComputing = @Original_OthContComputing OR @Original_OthContComputing IS NULL A" & _
        "ND OthContComputing IS NULL) AND (OthContConsult = @Original_OthContConsult OR @" & _
        "Original_OthContConsult IS NULL AND OthContConsult IS NULL) AND (OthContFire = @" & _
        "Original_OthContFire OR @Original_OthContFire IS NULL AND OthContFire IS NULL) A" & _
        "ND (OthContFoodSvc = @Original_OthContFoodSvc OR @Original_OthContFoodSvc IS NUL" & _
        "L AND OthContFoodSvc IS NULL) AND (OthContJan = @Original_OthContJan OR @Origina" & _
        "l_OthContJan IS NULL AND OthContJan IS NULL) AND (OthContLab = @Original_OthCont" & _
        "Lab OR @Original_OthContLab IS NULL AND OthContLab IS NULL) AND (OthContLegal = " & _
        "@Original_OthContLegal OR @Original_OthContLegal IS NULL AND OthContLegal IS NUL" & _
        "L) AND (OthContOth = @Original_OthContOth OR @Original_OthContOth IS NULL AND Ot" & _
        "hContOth IS NULL) AND (OthContProcOp = @Original_OthContProcOp OR @Original_OthC" & _
        "ontProcOp IS NULL AND OthContProcOp IS NULL) AND (OthContSecurity = @Original_Ot" & _
        "hContSecurity OR @Original_OthContSecurity IS NULL AND OthContSecurity IS NULL) " & _
        "AND (OthContTransOp = @Original_OthContTransOp OR @Original_OthContTransOp IS NU" & _
        "LL AND OthContTransOp IS NULL) AND (OthContVacTrucks = @Original_OthContVacTruck" & _
        "s OR @Original_OthContVacTrucks IS NULL AND OthContVacTrucks IS NULL) AND (OthNo" & _
        "nVol = @Original_OthNonVol OR @Original_OthNonVol IS NULL AND OthNonVol IS NULL)" & _
        " AND (OthNonVolComm = @Original_OthNonVolComm OR @Original_OthNonVolComm IS NULL" & _
        " AND OthNonVolComm IS NULL) AND (OthNonVolComputer = @Original_OthNonVolComputer" & _
        " OR @Original_OthNonVolComputer IS NULL AND OthNonVolComputer IS NULL) AND (OthN" & _
        "onVolDonations = @Original_OthNonVolDonations OR @Original_OthNonVolDonations IS" & _
        " NULL AND OthNonVolDonations IS NULL) AND (OthNonVolDues = @Original_OthNonVolDu" & _
        "es OR @Original_OthNonVolDues IS NULL AND OthNonVolDues IS NULL) AND (OthNonVolE" & _
        "xtraExpat = @Original_OthNonVolExtraExpat OR @Original_OthNonVolExtraExpat IS NU" & _
        "LL AND OthNonVolExtraExpat IS NULL) AND (OthNonVolNonContribPers = @Original_Oth" & _
        "NonVolNonContribPers OR @Original_OthNonVolNonContribPers IS NULL AND OthNonVolN" & _
        "onContribPers IS NULL) AND (OthNonVolNonPersSafety = @Original_OthNonVolNonPersS" & _
        "afety OR @Original_OthNonVolNonPersSafety IS NULL AND OthNonVolNonPersSafety IS " & _
        "NULL) AND (OthNonVolOth = @Original_OthNonVolOth OR @Original_OthNonVolOth IS NU" & _
        "LL AND OthNonVolOth IS NULL) AND (OthNonVolSafety = @Original_OthNonVolSafety OR" & _
        " @Original_OthNonVolSafety IS NULL AND OthNonVolSafety IS NULL) AND (OthNonVolSu" & _
        "pply = @Original_OthNonVolSupply OR @Original_OthNonVolSupply IS NULL AND OthNon" & _
        "VolSupply IS NULL) AND (OthNonVolTanks = @Original_OthNonVolTanks OR @Original_O" & _
        "thNonVolTanks IS NULL AND OthNonVolTanks IS NULL) AND (OthNonVolTrain = @Origina" & _
        "l_OthNonVolTrain OR @Original_OthNonVolTrain IS NULL AND OthNonVolTrain IS NULL)" & _
        " AND (OthNonVolTravel = @Original_OthNonVolTravel OR @Original_OthNonVolTravel I" & _
        "S NULL AND OthNonVolTravel IS NULL) AND (STNonVol = @Original_STNonVol OR @Origi" & _
        "nal_STNonVol IS NULL AND STNonVol IS NULL) AND (TAAdj = @Original_TAAdj OR @Orig" & _
        "inal_TAAdj IS NULL AND TAAdj IS NULL) AND (Tax = @Original_Tax OR @Original_Tax " & _
        "IS NULL AND Tax IS NULL)"
        Me.SqlDeleteCommand23.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Scenario", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Scenario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Chemicals", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Chemicals", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsASESolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsASESolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAlkyAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAmines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAmines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAntiknock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAntiknock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsClay", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsClay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDewaxAids", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDieselAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsFurfural", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsFurfural", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsGasAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsH2OTreat", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsLube", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsLube", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMEK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMEK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMIBK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMIBK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsNMP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsNMP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthSolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsProcess", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsProcess", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsPropane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsPropane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsToluene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsToluene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsWasteH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintInspect", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintInspect", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintLabor", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintLabor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintLaborST", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintLaborST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Envir", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Envir", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirDisp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirDisp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirEng", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirEng", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirPermits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirPermits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirSpill", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirSpill", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipNonMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipNonMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InsurBI", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InsurBI", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InsurOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InsurOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InsurPC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InsurPC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatlST", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatlST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthCont", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthCont", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContAdmin", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContAdmin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContComputing", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContComputing", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContConsult", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContConsult", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFire", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFire", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFoodSvc", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFoodSvc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContJan", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContJan", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLegal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLegal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContProcOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContProcOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContSecurity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContSecurity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContTransOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContTransOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContVacTrucks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContVacTrucks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComm", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComm", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComputer", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComputer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDonations", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDonations", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDues", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDues", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolExtraExpat", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolExtraExpat", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolNonContribPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolNonContribPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolNonPersSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolNonPersSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSupply", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSupply", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTanks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTanks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTrain", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTrain", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTravel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTravel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAAdj", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAAdj", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tax", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand24
        '
        Me.SqlInsertCommand24.CommandText = "INSERT INTO dbo.OpExAll(SubmissionID, Currency, Scenario, DataType, OCCSal, MPSSa" & _
        "l, OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, OCCBenStock, OCCBenTaxPen, " & _
        "OCCBenTaxMed, OCCBenTaxOth, OCCBen, MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBe" & _
        "nSub, MPSBenStock, MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, MaintMatl, " & _
        "ContMaintMatl, EquipMaint, MaintMatlST, ContMaintLabor, ContMaintInspect, ContMa" & _
        "intLaborST, OthContProcOp, OthContTransOp, OthContVacTrucks, OthContFire, OthCon" & _
        "tConsult, OthContSecurity, OthContComputing, OthContJan, OthContLab, OthContFood" & _
        "Svc, OthContAdmin, OthContLegal, OthContOth, OthCont, TAAdj, EnvirDisp, EnvirPer" & _
        "mits, EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, Envir, EquipNonMaint" & _
        ", Tax, InsurBI, InsurPC, InsurOth, OthNonVolSupply, OthNonVolSafety, OthNonVolNo" & _
        "nPersSafety, OthNonVolComm, OthNonVolDonations, OthNonVolNonContribPers, OthNonV" & _
        "olDues, OthNonVolTravel, OthNonVolTrain, OthNonVolComputer, OthNonVolTanks, OthN" & _
        "onVolExtraExpat, OthNonVolOth, OthNonVol, STNonVol, GAPers, ChemicalsAntiknock, " & _
        "ChemicalsAlkyAcid, ChemicalsLube, ChemicalsH2OTreat, ChemicalsOthAcid, Chemicals" & _
        "Process, ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, Chem" & _
        "icalsClay, ChemicalsAmines, ChemicalsASESolv, ChemicalsWasteH2O, ChemicalsNMP, C" & _
        "hemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene, ChemicalsPropan" & _
        "e, ChemicalsOthSolv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth, Chemicals)" & _
        " VALUES (@SubmissionID, @Currency, @Scenario, @DataType, @OCCSal, @MPSSal, @OCCB" & _
        "enAbs, @OCCBenInsur, @OCCBenPension, @OCCBenSub, @OCCBenStock, @OCCBenTaxPen, @O" & _
        "CCBenTaxMed, @OCCBenTaxOth, @OCCBen, @MPSBenAbs, @MPSBenInsur, @MPSBenPension, @" & _
        "MPSBenSub, @MPSBenStock, @MPSBenTaxPen, @MPSBenTaxMed, @MPSBenTaxOth, @MPSBen, @" & _
        "MaintMatl, @ContMaintMatl, @EquipMaint, @MaintMatlST, @ContMaintLabor, @ContMain" & _
        "tInspect, @ContMaintLaborST, @OthContProcOp, @OthContTransOp, @OthContVacTrucks," & _
        " @OthContFire, @OthContConsult, @OthContSecurity, @OthContComputing, @OthContJan" & _
        ", @OthContLab, @OthContFoodSvc, @OthContAdmin, @OthContLegal, @OthContOth, @OthC" & _
        "ont, @TAAdj, @EnvirDisp, @EnvirPermits, @EnvirFines, @EnvirSpill, @EnvirLab, @En" & _
        "virEng, @EnvirOth, @Envir, @EquipNonMaint, @Tax, @InsurBI, @InsurPC, @InsurOth, " & _
        "@OthNonVolSupply, @OthNonVolSafety, @OthNonVolNonPersSafety, @OthNonVolComm, @Ot" & _
        "hNonVolDonations, @OthNonVolNonContribPers, @OthNonVolDues, @OthNonVolTravel, @O" & _
        "thNonVolTrain, @OthNonVolComputer, @OthNonVolTanks, @OthNonVolExtraExpat, @OthNo" & _
        "nVolOth, @OthNonVol, @STNonVol, @GAPers, @ChemicalsAntiknock, @ChemicalsAlkyAcid" & _
        ", @ChemicalsLube, @ChemicalsH2OTreat, @ChemicalsOthAcid, @ChemicalsProcess, @Che" & _
        "micalsGasAdd, @ChemicalsDieselAdd, @ChemicalsOthAdd, @ChemicalsO2, @ChemicalsCla" & _
        "y, @ChemicalsAmines, @ChemicalsASESolv, @ChemicalsWasteH2O, @ChemicalsNMP, @Chem" & _
        "icalsFurfural, @ChemicalsMIBK, @ChemicalsMEK, @ChemicalsToluene, @ChemicalsPropa" & _
        "ne, @ChemicalsOthSolv, @ChemicalsAcid, @ChemicalsDewaxAids, @ChemicalsOth, @Chem" & _
        "icals)"
        Me.SqlInsertCommand24.Connection = Me.SqlConnection1
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Scenario", System.Data.SqlDbType.VarChar, 8, "Scenario"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCSal", System.Data.SqlDbType.Real, 4, "OCCSal"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSSal", System.Data.SqlDbType.Real, 4, "MPSSal"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenAbs", System.Data.SqlDbType.Real, 4, "OCCBenAbs"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenInsur", System.Data.SqlDbType.Real, 4, "OCCBenInsur"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenPension", System.Data.SqlDbType.Real, 4, "OCCBenPension"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenSub", System.Data.SqlDbType.Real, 4, "OCCBenSub"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenStock", System.Data.SqlDbType.Real, 4, "OCCBenStock"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxPen", System.Data.SqlDbType.Real, 4, "OCCBenTaxPen"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxMed", System.Data.SqlDbType.Real, 4, "OCCBenTaxMed"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxOth", System.Data.SqlDbType.Real, 4, "OCCBenTaxOth"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBen", System.Data.SqlDbType.Real, 4, "OCCBen"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenAbs", System.Data.SqlDbType.Real, 4, "MPSBenAbs"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenInsur", System.Data.SqlDbType.Real, 4, "MPSBenInsur"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenPension", System.Data.SqlDbType.Real, 4, "MPSBenPension"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenSub", System.Data.SqlDbType.Real, 4, "MPSBenSub"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenStock", System.Data.SqlDbType.Real, 4, "MPSBenStock"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxPen", System.Data.SqlDbType.Real, 4, "MPSBenTaxPen"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxMed", System.Data.SqlDbType.Real, 4, "MPSBenTaxMed"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxOth", System.Data.SqlDbType.Real, 4, "MPSBenTaxOth"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBen", System.Data.SqlDbType.Real, 4, "MPSBen"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatl", System.Data.SqlDbType.Real, 4, "MaintMatl"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintMatl", System.Data.SqlDbType.Real, 4, "ContMaintMatl"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipMaint", System.Data.SqlDbType.Real, 4, "EquipMaint"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatlST", System.Data.SqlDbType.Real, 4, "MaintMatlST"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintLabor", System.Data.SqlDbType.Real, 4, "ContMaintLabor"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintInspect", System.Data.SqlDbType.Real, 4, "ContMaintInspect"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintLaborST", System.Data.SqlDbType.Real, 4, "ContMaintLaborST"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContProcOp", System.Data.SqlDbType.Real, 4, "OthContProcOp"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContTransOp", System.Data.SqlDbType.Real, 4, "OthContTransOp"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContVacTrucks", System.Data.SqlDbType.Real, 4, "OthContVacTrucks"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFire", System.Data.SqlDbType.Real, 4, "OthContFire"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContConsult", System.Data.SqlDbType.Real, 4, "OthContConsult"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContSecurity", System.Data.SqlDbType.Real, 4, "OthContSecurity"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContComputing", System.Data.SqlDbType.Real, 4, "OthContComputing"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContJan", System.Data.SqlDbType.Real, 4, "OthContJan"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLab", System.Data.SqlDbType.Real, 4, "OthContLab"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFoodSvc", System.Data.SqlDbType.Real, 4, "OthContFoodSvc"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContAdmin", System.Data.SqlDbType.Real, 4, "OthContAdmin"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLegal", System.Data.SqlDbType.Real, 4, "OthContLegal"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContOth", System.Data.SqlDbType.Real, 4, "OthContOth"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthCont", System.Data.SqlDbType.Real, 4, "OthCont"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAAdj", System.Data.SqlDbType.Real, 4, "TAAdj"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirDisp", System.Data.SqlDbType.Real, 4, "EnvirDisp"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirPermits", System.Data.SqlDbType.Real, 4, "EnvirPermits"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirFines", System.Data.SqlDbType.Real, 4, "EnvirFines"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirSpill", System.Data.SqlDbType.Real, 4, "EnvirSpill"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirLab", System.Data.SqlDbType.Real, 4, "EnvirLab"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirEng", System.Data.SqlDbType.Real, 4, "EnvirEng"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirOth", System.Data.SqlDbType.Real, 4, "EnvirOth"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Envir", System.Data.SqlDbType.Real, 4, "Envir"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipNonMaint", System.Data.SqlDbType.Real, 4, "EquipNonMaint"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tax", System.Data.SqlDbType.Real, 4, "Tax"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InsurBI", System.Data.SqlDbType.Real, 4, "InsurBI"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InsurPC", System.Data.SqlDbType.Real, 4, "InsurPC"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InsurOth", System.Data.SqlDbType.Real, 4, "InsurOth"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSupply", System.Data.SqlDbType.Real, 4, "OthNonVolSupply"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSafety", System.Data.SqlDbType.Real, 4, "OthNonVolSafety"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolNonPersSafety", System.Data.SqlDbType.Real, 4, "OthNonVolNonPersSafety"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComm", System.Data.SqlDbType.Real, 4, "OthNonVolComm"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDonations", System.Data.SqlDbType.Real, 4, "OthNonVolDonations"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolNonContribPers", System.Data.SqlDbType.Real, 4, "OthNonVolNonContribPers"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDues", System.Data.SqlDbType.Real, 4, "OthNonVolDues"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTravel", System.Data.SqlDbType.Real, 4, "OthNonVolTravel"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTrain", System.Data.SqlDbType.Real, 4, "OthNonVolTrain"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComputer", System.Data.SqlDbType.Real, 4, "OthNonVolComputer"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTanks", System.Data.SqlDbType.Real, 4, "OthNonVolTanks"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolExtraExpat", System.Data.SqlDbType.Real, 4, "OthNonVolExtraExpat"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolOth", System.Data.SqlDbType.Real, 4, "OthNonVolOth"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVol", System.Data.SqlDbType.Real, 4, "OthNonVol"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonVol", System.Data.SqlDbType.Real, 4, "STNonVol"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAPers", System.Data.SqlDbType.Real, 4, "GAPers"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAntiknock", System.Data.SqlDbType.Real, 4, "ChemicalsAntiknock"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, "ChemicalsAlkyAcid"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsLube", System.Data.SqlDbType.Real, 4, "ChemicalsLube"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, "ChemicalsH2OTreat"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, "ChemicalsOthAcid"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsProcess", System.Data.SqlDbType.Real, 4, "ChemicalsProcess"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, "ChemicalsGasAdd"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, "ChemicalsDieselAdd"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, "ChemicalsOthAdd"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsO2", System.Data.SqlDbType.Real, 4, "ChemicalsO2"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsClay", System.Data.SqlDbType.Real, 4, "ChemicalsClay"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAmines", System.Data.SqlDbType.Real, 4, "ChemicalsAmines"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsASESolv", System.Data.SqlDbType.Real, 4, "ChemicalsASESolv"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, "ChemicalsWasteH2O"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsNMP", System.Data.SqlDbType.Real, 4, "ChemicalsNMP"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsFurfural", System.Data.SqlDbType.Real, 4, "ChemicalsFurfural"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMIBK", System.Data.SqlDbType.Real, 4, "ChemicalsMIBK"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMEK", System.Data.SqlDbType.Real, 4, "ChemicalsMEK"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsToluene", System.Data.SqlDbType.Real, 4, "ChemicalsToluene"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsPropane", System.Data.SqlDbType.Real, 4, "ChemicalsPropane"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, "ChemicalsOthSolv"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAcid", System.Data.SqlDbType.Real, 4, "ChemicalsAcid"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, "ChemicalsDewaxAids"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOth", System.Data.SqlDbType.Real, 4, "ChemicalsOth"))
        Me.SqlInsertCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Chemicals", System.Data.SqlDbType.Real, 4, "Chemicals"))
        '
        'SqlSelectCommand24
        '
        Me.SqlSelectCommand24.CommandText = "SELECT SubmissionID, Currency, Scenario, DataType, OCCSal, MPSSal, OCCBenAbs, OCC" & _
        "BenInsur, OCCBenPension, OCCBenSub, OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCC" & _
        "BenTaxOth, OCCBen, MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock" & _
        ", MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, MaintMatl, ContMaintMatl, Eq" & _
        "uipMaint, MaintMatlST, ContMaintLabor, ContMaintInspect, ContMaintLaborST, OthCo" & _
        "ntProcOp, OthContTransOp, OthContVacTrucks, OthContFire, OthContConsult, OthCont" & _
        "Security, OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin" & _
        ", OthContLegal, OthContOth, OthCont, TAAdj, EnvirDisp, EnvirPermits, EnvirFines," & _
        " EnvirSpill, EnvirLab, EnvirEng, EnvirOth, Envir, EquipNonMaint, Tax, InsurBI, I" & _
        "nsurPC, InsurOth, OthNonVolSupply, OthNonVolSafety, OthNonVolNonPersSafety, OthN" & _
        "onVolComm, OthNonVolDonations, OthNonVolNonContribPers, OthNonVolDues, OthNonVol" & _
        "Travel, OthNonVolTrain, OthNonVolComputer, OthNonVolTanks, OthNonVolExtraExpat, " & _
        "OthNonVolOth, OthNonVol, STNonVol, GAPers, ChemicalsAntiknock, ChemicalsAlkyAcid" & _
        ", ChemicalsLube, ChemicalsH2OTreat, ChemicalsOthAcid, ChemicalsProcess, Chemical" & _
        "sGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, ChemicalsClay, Chemic" & _
        "alsAmines, ChemicalsASESolv, ChemicalsWasteH2O, ChemicalsNMP, ChemicalsFurfural," & _
        " ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene, ChemicalsPropane, ChemicalsOthSo" & _
        "lv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth, Chemicals FROM dbo.OpExAll"
        Me.SqlSelectCommand24.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand23
        '
        Me.SqlUpdateCommand23.CommandText = "UPDATE dbo.OpExAll SET SubmissionID = @SubmissionID, Currency = @Currency, Scenar" & _
        "io = @Scenario, DataType = @DataType, OCCSal = @OCCSal, MPSSal = @MPSSal, OCCBen" & _
        "Abs = @OCCBenAbs, OCCBenInsur = @OCCBenInsur, OCCBenPension = @OCCBenPension, OC" & _
        "CBenSub = @OCCBenSub, OCCBenStock = @OCCBenStock, OCCBenTaxPen = @OCCBenTaxPen, " & _
        "OCCBenTaxMed = @OCCBenTaxMed, OCCBenTaxOth = @OCCBenTaxOth, OCCBen = @OCCBen, MP" & _
        "SBenAbs = @MPSBenAbs, MPSBenInsur = @MPSBenInsur, MPSBenPension = @MPSBenPension" & _
        ", MPSBenSub = @MPSBenSub, MPSBenStock = @MPSBenStock, MPSBenTaxPen = @MPSBenTaxP" & _
        "en, MPSBenTaxMed = @MPSBenTaxMed, MPSBenTaxOth = @MPSBenTaxOth, MPSBen = @MPSBen" & _
        ", MaintMatl = @MaintMatl, ContMaintMatl = @ContMaintMatl, EquipMaint = @EquipMai" & _
        "nt, MaintMatlST = @MaintMatlST, ContMaintLabor = @ContMaintLabor, ContMaintInspe" & _
        "ct = @ContMaintInspect, ContMaintLaborST = @ContMaintLaborST, OthContProcOp = @O" & _
        "thContProcOp, OthContTransOp = @OthContTransOp, OthContVacTrucks = @OthContVacTr" & _
        "ucks, OthContFire = @OthContFire, OthContConsult = @OthContConsult, OthContSecur" & _
        "ity = @OthContSecurity, OthContComputing = @OthContComputing, OthContJan = @OthC" & _
        "ontJan, OthContLab = @OthContLab, OthContFoodSvc = @OthContFoodSvc, OthContAdmin" & _
        " = @OthContAdmin, OthContLegal = @OthContLegal, OthContOth = @OthContOth, OthCon" & _
        "t = @OthCont, TAAdj = @TAAdj, EnvirDisp = @EnvirDisp, EnvirPermits = @EnvirPermi" & _
        "ts, EnvirFines = @EnvirFines, EnvirSpill = @EnvirSpill, EnvirLab = @EnvirLab, En" & _
        "virEng = @EnvirEng, EnvirOth = @EnvirOth, Envir = @Envir, EquipNonMaint = @Equip" & _
        "NonMaint, Tax = @Tax, InsurBI = @InsurBI, InsurPC = @InsurPC, InsurOth = @InsurO" & _
        "th, OthNonVolSupply = @OthNonVolSupply, OthNonVolSafety = @OthNonVolSafety, OthN" & _
        "onVolNonPersSafety = @OthNonVolNonPersSafety, OthNonVolComm = @OthNonVolComm, Ot" & _
        "hNonVolDonations = @OthNonVolDonations, OthNonVolNonContribPers = @OthNonVolNonC" & _
        "ontribPers, OthNonVolDues = @OthNonVolDues, OthNonVolTravel = @OthNonVolTravel, " & _
        "OthNonVolTrain = @OthNonVolTrain, OthNonVolComputer = @OthNonVolComputer, OthNon" & _
        "VolTanks = @OthNonVolTanks, OthNonVolExtraExpat = @OthNonVolExtraExpat, OthNonVo" & _
        "lOth = @OthNonVolOth, OthNonVol = @OthNonVol, STNonVol = @STNonVol, GAPers = @GA" & _
        "Pers, ChemicalsAntiknock = @ChemicalsAntiknock, ChemicalsAlkyAcid = @ChemicalsAl" & _
        "kyAcid, ChemicalsLube = @ChemicalsLube, ChemicalsH2OTreat = @ChemicalsH2OTreat, " & _
        "ChemicalsOthAcid = @ChemicalsOthAcid, ChemicalsProcess = @ChemicalsProcess, Chem" & _
        "icalsGasAdd = @ChemicalsGasAdd, ChemicalsDieselAdd = @ChemicalsDieselAdd, Chemic" & _
        "alsOthAdd = @ChemicalsOthAdd, ChemicalsO2 = @ChemicalsO2, ChemicalsClay = @Chemi" & _
        "calsClay, ChemicalsAmines = @ChemicalsAmines, ChemicalsASESolv = @ChemicalsASESo" & _
        "lv, ChemicalsWasteH2O = @ChemicalsWasteH2O, ChemicalsNMP = @ChemicalsNMP, Chemic" & _
        "alsFurfural = @ChemicalsFurfural, ChemicalsMIBK = @ChemicalsMIBK, ChemicalsMEK =" & _
        " @ChemicalsMEK, ChemicalsToluene = @ChemicalsToluene, ChemicalsPropane = @Chemic" & _
        "alsPropane, ChemicalsOthSolv = @ChemicalsOthSolv, ChemicalsAcid = @ChemicalsAcid" & _
        ", ChemicalsDewaxAids = @ChemicalsDewaxAids, ChemicalsOth = @ChemicalsOth, Chemic" & _
        "als = @Chemicals WHERE (Currency = @Original_Currency) AND (DataType = @Original" & _
        "_DataType) AND (Scenario = @Original_Scenario) AND (SubmissionID = @Original_Sub" & _
        "missionID) AND (Chemicals = @Original_Chemicals OR @Original_Chemicals IS NULL A" & _
        "ND Chemicals IS NULL) AND (ChemicalsASESolv = @Original_ChemicalsASESolv OR @Ori" & _
        "ginal_ChemicalsASESolv IS NULL AND ChemicalsASESolv IS NULL) AND (ChemicalsAcid " & _
        "= @Original_ChemicalsAcid OR @Original_ChemicalsAcid IS NULL AND ChemicalsAcid I" & _
        "S NULL) AND (ChemicalsAlkyAcid = @Original_ChemicalsAlkyAcid OR @Original_Chemic" & _
        "alsAlkyAcid IS NULL AND ChemicalsAlkyAcid IS NULL) AND (ChemicalsAmines = @Origi" & _
        "nal_ChemicalsAmines OR @Original_ChemicalsAmines IS NULL AND ChemicalsAmines IS " & _
        "NULL) AND (ChemicalsAntiknock = @Original_ChemicalsAntiknock OR @Original_Chemic" & _
        "alsAntiknock IS NULL AND ChemicalsAntiknock IS NULL) AND (ChemicalsClay = @Origi" & _
        "nal_ChemicalsClay OR @Original_ChemicalsClay IS NULL AND ChemicalsClay IS NULL) " & _
        "AND (ChemicalsDewaxAids = @Original_ChemicalsDewaxAids OR @Original_ChemicalsDew" & _
        "axAids IS NULL AND ChemicalsDewaxAids IS NULL) AND (ChemicalsDieselAdd = @Origin" & _
        "al_ChemicalsDieselAdd OR @Original_ChemicalsDieselAdd IS NULL AND ChemicalsDiese" & _
        "lAdd IS NULL) AND (ChemicalsFurfural = @Original_ChemicalsFurfural OR @Original_" & _
        "ChemicalsFurfural IS NULL AND ChemicalsFurfural IS NULL) AND (ChemicalsGasAdd = " & _
        "@Original_ChemicalsGasAdd OR @Original_ChemicalsGasAdd IS NULL AND ChemicalsGasA" & _
        "dd IS NULL) AND (ChemicalsH2OTreat = @Original_ChemicalsH2OTreat OR @Original_Ch" & _
        "emicalsH2OTreat IS NULL AND ChemicalsH2OTreat IS NULL) AND (ChemicalsLube = @Ori" & _
        "ginal_ChemicalsLube OR @Original_ChemicalsLube IS NULL AND ChemicalsLube IS NULL" & _
        ") AND (ChemicalsMEK = @Original_ChemicalsMEK OR @Original_ChemicalsMEK IS NULL A" & _
        "ND ChemicalsMEK IS NULL) AND (ChemicalsMIBK = @Original_ChemicalsMIBK OR @Origin" & _
        "al_ChemicalsMIBK IS NULL AND ChemicalsMIBK IS NULL) AND (ChemicalsNMP = @Origina" & _
        "l_ChemicalsNMP OR @Original_ChemicalsNMP IS NULL AND ChemicalsNMP IS NULL) AND (" & _
        "ChemicalsO2 = @Original_ChemicalsO2 OR @Original_ChemicalsO2 IS NULL AND Chemica" & _
        "lsO2 IS NULL) AND (ChemicalsOth = @Original_ChemicalsOth OR @Original_ChemicalsO" & _
        "th IS NULL AND ChemicalsOth IS NULL) AND (ChemicalsOthAcid = @Original_Chemicals" & _
        "OthAcid OR @Original_ChemicalsOthAcid IS NULL AND ChemicalsOthAcid IS NULL) AND " & _
        "(ChemicalsOthAdd = @Original_ChemicalsOthAdd OR @Original_ChemicalsOthAdd IS NUL" & _
        "L AND ChemicalsOthAdd IS NULL) AND (ChemicalsOthSolv = @Original_ChemicalsOthSol" & _
        "v OR @Original_ChemicalsOthSolv IS NULL AND ChemicalsOthSolv IS NULL) AND (Chemi" & _
        "calsProcess = @Original_ChemicalsProcess OR @Original_ChemicalsProcess IS NULL A" & _
        "ND ChemicalsProcess IS NULL) AND (ChemicalsPropane = @Original_ChemicalsPropane " & _
        "OR @Original_ChemicalsPropane IS NULL AND ChemicalsPropane IS NULL) AND (Chemica" & _
        "lsToluene = @Original_ChemicalsToluene OR @Original_ChemicalsToluene IS NULL AND" & _
        " ChemicalsToluene IS NULL) AND (ChemicalsWasteH2O = @Original_ChemicalsWasteH2O " & _
        "OR @Original_ChemicalsWasteH2O IS NULL AND ChemicalsWasteH2O IS NULL) AND (ContM" & _
        "aintInspect = @Original_ContMaintInspect OR @Original_ContMaintInspect IS NULL A" & _
        "ND ContMaintInspect IS NULL) AND (ContMaintLabor = @Original_ContMaintLabor OR @" & _
        "Original_ContMaintLabor IS NULL AND ContMaintLabor IS NULL) AND (ContMaintLaborS" & _
        "T = @Original_ContMaintLaborST OR @Original_ContMaintLaborST IS NULL AND ContMai" & _
        "ntLaborST IS NULL) AND (ContMaintMatl = @Original_ContMaintMatl OR @Original_Con" & _
        "tMaintMatl IS NULL AND ContMaintMatl IS NULL) AND (Envir = @Original_Envir OR @O" & _
        "riginal_Envir IS NULL AND Envir IS NULL) AND (EnvirDisp = @Original_EnvirDisp OR" & _
        " @Original_EnvirDisp IS NULL AND EnvirDisp IS NULL) AND (EnvirEng = @Original_En" & _
        "virEng OR @Original_EnvirEng IS NULL AND EnvirEng IS NULL) AND (EnvirFines = @Or" & _
        "iginal_EnvirFines OR @Original_EnvirFines IS NULL AND EnvirFines IS NULL) AND (E" & _
        "nvirLab = @Original_EnvirLab OR @Original_EnvirLab IS NULL AND EnvirLab IS NULL)" & _
        " AND (EnvirOth = @Original_EnvirOth OR @Original_EnvirOth IS NULL AND EnvirOth I" & _
        "S NULL) AND (EnvirPermits = @Original_EnvirPermits OR @Original_EnvirPermits IS " & _
        "NULL AND EnvirPermits IS NULL) AND (EnvirSpill = @Original_EnvirSpill OR @Origin" & _
        "al_EnvirSpill IS NULL AND EnvirSpill IS NULL) AND (EquipMaint = @Original_EquipM" & _
        "aint OR @Original_EquipMaint IS NULL AND EquipMaint IS NULL) AND (EquipNonMaint " & _
        "= @Original_EquipNonMaint OR @Original_EquipNonMaint IS NULL AND EquipNonMaint I" & _
        "S NULL) AND (GAPers = @Original_GAPers OR @Original_GAPers IS NULL AND GAPers IS" & _
        " NULL) AND (InsurBI = @Original_InsurBI OR @Original_InsurBI IS NULL AND InsurBI" & _
        " IS NULL) AND (InsurOth = @Original_InsurOth OR @Original_InsurOth IS NULL AND I" & _
        "nsurOth IS NULL) AND (InsurPC = @Original_InsurPC OR @Original_InsurPC IS NULL A" & _
        "ND InsurPC IS NULL) AND (MPSBen = @Original_MPSBen OR @Original_MPSBen IS NULL A" & _
        "ND MPSBen IS NULL) AND (MPSBenAbs = @Original_MPSBenAbs OR @Original_MPSBenAbs I" & _
        "S NULL AND MPSBenAbs IS NULL) AND (MPSBenInsur = @Original_MPSBenInsur OR @Origi" & _
        "nal_MPSBenInsur IS NULL AND MPSBenInsur IS NULL) AND (MPSBenPension = @Original_" & _
        "MPSBenPension OR @Original_MPSBenPension IS NULL AND MPSBenPension IS NULL) AND " & _
        "(MPSBenStock = @Original_MPSBenStock OR @Original_MPSBenStock IS NULL AND MPSBen" & _
        "Stock IS NULL) AND (MPSBenSub = @Original_MPSBenSub OR @Original_MPSBenSub IS NU" & _
        "LL AND MPSBenSub IS NULL) AND (MPSBenTaxMed = @Original_MPSBenTaxMed OR @Origina" & _
        "l_MPSBenTaxMed IS NULL AND MPSBenTaxMed IS NULL) AND (MPSBenTaxOth = @Original_M" & _
        "PSBenTaxOth OR @Original_MPSBenTaxOth IS NULL AND MPSBenTaxOth IS NULL) AND (MPS" & _
        "BenTaxPen = @Original_MPSBenTaxPen OR @Original_MPSBenTaxPen IS NULL AND MPSBenT" & _
        "axPen IS NULL) AND (MPSSal = @Original_MPSSal OR @Original_MPSSal IS NULL AND MP" & _
        "SSal IS NULL) AND (MaintMatl = @Original_MaintMatl OR @Original_MaintMatl IS NUL" & _
        "L AND MaintMatl IS NULL) AND (MaintMatlST = @Original_MaintMatlST OR @Original_M" & _
        "aintMatlST IS NULL AND MaintMatlST IS NULL) AND (OCCBen = @Original_OCCBen OR @O" & _
        "riginal_OCCBen IS NULL AND OCCBen IS NULL) AND (OCCBenAbs = @Original_OCCBenAbs " & _
        "OR @Original_OCCBenAbs IS NULL AND OCCBenAbs IS NULL) AND (OCCBenInsur = @Origin" & _
        "al_OCCBenInsur OR @Original_OCCBenInsur IS NULL AND OCCBenInsur IS NULL) AND (OC" & _
        "CBenPension = @Original_OCCBenPension OR @Original_OCCBenPension IS NULL AND OCC" & _
        "BenPension IS NULL) AND (OCCBenStock = @Original_OCCBenStock OR @Original_OCCBen" & _
        "Stock IS NULL AND OCCBenStock IS NULL) AND (OCCBenSub = @Original_OCCBenSub OR @" & _
        "Original_OCCBenSub IS NULL AND OCCBenSub IS NULL) AND (OCCBenTaxMed = @Original_" & _
        "OCCBenTaxMed OR @Original_OCCBenTaxMed IS NULL AND OCCBenTaxMed IS NULL) AND (OC" & _
        "CBenTaxOth = @Original_OCCBenTaxOth OR @Original_OCCBenTaxOth IS NULL AND OCCBen" & _
        "TaxOth IS NULL) AND (OCCBenTaxPen = @Original_OCCBenTaxPen OR @Original_OCCBenTa" & _
        "xPen IS NULL AND OCCBenTaxPen IS NULL) AND (OCCSal = @Original_OCCSal OR @Origin" & _
        "al_OCCSal IS NULL AND OCCSal IS NULL) AND (OthCont = @Original_OthCont OR @Origi" & _
        "nal_OthCont IS NULL AND OthCont IS NULL) AND (OthContAdmin = @Original_OthContAd" & _
        "min OR @Original_OthContAdmin IS NULL AND OthContAdmin IS NULL) AND (OthContComp" & _
        "uting = @Original_OthContComputing OR @Original_OthContComputing IS NULL AND Oth" & _
        "ContComputing IS NULL) AND (OthContConsult = @Original_OthContConsult OR @Origin" & _
        "al_OthContConsult IS NULL AND OthContConsult IS NULL) AND (OthContFire = @Origin" & _
        "al_OthContFire OR @Original_OthContFire IS NULL AND OthContFire IS NULL) AND (Ot" & _
        "hContFoodSvc = @Original_OthContFoodSvc OR @Original_OthContFoodSvc IS NULL AND " & _
        "OthContFoodSvc IS NULL) AND (OthContJan = @Original_OthContJan OR @Original_OthC" & _
        "ontJan IS NULL AND OthContJan IS NULL) AND (OthContLab = @Original_OthContLab OR" & _
        " @Original_OthContLab IS NULL AND OthContLab IS NULL) AND (OthContLegal = @Origi" & _
        "nal_OthContLegal OR @Original_OthContLegal IS NULL AND OthContLegal IS NULL) AND" & _
        " (OthContOth = @Original_OthContOth OR @Original_OthContOth IS NULL AND OthContO" & _
        "th IS NULL) AND (OthContProcOp = @Original_OthContProcOp OR @Original_OthContPro" & _
        "cOp IS NULL AND OthContProcOp IS NULL) AND (OthContSecurity = @Original_OthContS" & _
        "ecurity OR @Original_OthContSecurity IS NULL AND OthContSecurity IS NULL) AND (O" & _
        "thContTransOp = @Original_OthContTransOp OR @Original_OthContTransOp IS NULL AND" & _
        " OthContTransOp IS NULL) AND (OthContVacTrucks = @Original_OthContVacTrucks OR @" & _
        "Original_OthContVacTrucks IS NULL AND OthContVacTrucks IS NULL) AND (OthNonVol =" & _
        " @Original_OthNonVol OR @Original_OthNonVol IS NULL AND OthNonVol IS NULL) AND (" & _
        "OthNonVolComm = @Original_OthNonVolComm OR @Original_OthNonVolComm IS NULL AND O" & _
        "thNonVolComm IS NULL) AND (OthNonVolComputer = @Original_OthNonVolComputer OR @O" & _
        "riginal_OthNonVolComputer IS NULL AND OthNonVolComputer IS NULL) AND (OthNonVolD" & _
        "onations = @Original_OthNonVolDonations OR @Original_OthNonVolDonations IS NULL " & _
        "AND OthNonVolDonations IS NULL) AND (OthNonVolDues = @Original_OthNonVolDues OR " & _
        "@Original_OthNonVolDues IS NULL AND OthNonVolDues IS NULL) AND (OthNonVolExtraEx" & _
        "pat = @Original_OthNonVolExtraExpat OR @Original_OthNonVolExtraExpat IS NULL AND" & _
        " OthNonVolExtraExpat IS NULL) AND (OthNonVolNonContribPers = @Original_OthNonVol" & _
        "NonContribPers OR @Original_OthNonVolNonContribPers IS NULL AND OthNonVolNonCont" & _
        "ribPers IS NULL) AND (OthNonVolNonPersSafety = @Original_OthNonVolNonPersSafety " & _
        "OR @Original_OthNonVolNonPersSafety IS NULL AND OthNonVolNonPersSafety IS NULL) " & _
        "AND (OthNonVolOth = @Original_OthNonVolOth OR @Original_OthNonVolOth IS NULL AND" & _
        " OthNonVolOth IS NULL) AND (OthNonVolSafety = @Original_OthNonVolSafety OR @Orig" & _
        "inal_OthNonVolSafety IS NULL AND OthNonVolSafety IS NULL) AND (OthNonVolSupply =" & _
        " @Original_OthNonVolSupply OR @Original_OthNonVolSupply IS NULL AND OthNonVolSup" & _
        "ply IS NULL) AND (OthNonVolTanks = @Original_OthNonVolTanks OR @Original_OthNonV" & _
        "olTanks IS NULL AND OthNonVolTanks IS NULL) AND (OthNonVolTrain = @Original_OthN" & _
        "onVolTrain OR @Original_OthNonVolTrain IS NULL AND OthNonVolTrain IS NULL) AND (" & _
        "OthNonVolTravel = @Original_OthNonVolTravel OR @Original_OthNonVolTravel IS NULL" & _
        " AND OthNonVolTravel IS NULL) AND (STNonVol = @Original_STNonVol OR @Original_ST" & _
        "NonVol IS NULL AND STNonVol IS NULL) AND (TAAdj = @Original_TAAdj OR @Original_T" & _
        "AAdj IS NULL AND TAAdj IS NULL) AND (Tax = @Original_Tax OR @Original_Tax IS NUL" & _
        "L AND Tax IS NULL)"
        Me.SqlUpdateCommand23.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Scenario", System.Data.SqlDbType.VarChar, 8, "Scenario"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCSal", System.Data.SqlDbType.Real, 4, "OCCSal"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSSal", System.Data.SqlDbType.Real, 4, "MPSSal"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenAbs", System.Data.SqlDbType.Real, 4, "OCCBenAbs"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenInsur", System.Data.SqlDbType.Real, 4, "OCCBenInsur"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenPension", System.Data.SqlDbType.Real, 4, "OCCBenPension"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenSub", System.Data.SqlDbType.Real, 4, "OCCBenSub"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenStock", System.Data.SqlDbType.Real, 4, "OCCBenStock"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxPen", System.Data.SqlDbType.Real, 4, "OCCBenTaxPen"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxMed", System.Data.SqlDbType.Real, 4, "OCCBenTaxMed"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBenTaxOth", System.Data.SqlDbType.Real, 4, "OCCBenTaxOth"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OCCBen", System.Data.SqlDbType.Real, 4, "OCCBen"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenAbs", System.Data.SqlDbType.Real, 4, "MPSBenAbs"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenInsur", System.Data.SqlDbType.Real, 4, "MPSBenInsur"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenPension", System.Data.SqlDbType.Real, 4, "MPSBenPension"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenSub", System.Data.SqlDbType.Real, 4, "MPSBenSub"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenStock", System.Data.SqlDbType.Real, 4, "MPSBenStock"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxPen", System.Data.SqlDbType.Real, 4, "MPSBenTaxPen"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxMed", System.Data.SqlDbType.Real, 4, "MPSBenTaxMed"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBenTaxOth", System.Data.SqlDbType.Real, 4, "MPSBenTaxOth"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MPSBen", System.Data.SqlDbType.Real, 4, "MPSBen"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatl", System.Data.SqlDbType.Real, 4, "MaintMatl"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintMatl", System.Data.SqlDbType.Real, 4, "ContMaintMatl"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipMaint", System.Data.SqlDbType.Real, 4, "EquipMaint"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MaintMatlST", System.Data.SqlDbType.Real, 4, "MaintMatlST"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintLabor", System.Data.SqlDbType.Real, 4, "ContMaintLabor"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintInspect", System.Data.SqlDbType.Real, 4, "ContMaintInspect"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ContMaintLaborST", System.Data.SqlDbType.Real, 4, "ContMaintLaborST"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContProcOp", System.Data.SqlDbType.Real, 4, "OthContProcOp"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContTransOp", System.Data.SqlDbType.Real, 4, "OthContTransOp"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContVacTrucks", System.Data.SqlDbType.Real, 4, "OthContVacTrucks"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFire", System.Data.SqlDbType.Real, 4, "OthContFire"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContConsult", System.Data.SqlDbType.Real, 4, "OthContConsult"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContSecurity", System.Data.SqlDbType.Real, 4, "OthContSecurity"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContComputing", System.Data.SqlDbType.Real, 4, "OthContComputing"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContJan", System.Data.SqlDbType.Real, 4, "OthContJan"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLab", System.Data.SqlDbType.Real, 4, "OthContLab"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContFoodSvc", System.Data.SqlDbType.Real, 4, "OthContFoodSvc"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContAdmin", System.Data.SqlDbType.Real, 4, "OthContAdmin"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContLegal", System.Data.SqlDbType.Real, 4, "OthContLegal"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthContOth", System.Data.SqlDbType.Real, 4, "OthContOth"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthCont", System.Data.SqlDbType.Real, 4, "OthCont"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAAdj", System.Data.SqlDbType.Real, 4, "TAAdj"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirDisp", System.Data.SqlDbType.Real, 4, "EnvirDisp"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirPermits", System.Data.SqlDbType.Real, 4, "EnvirPermits"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirFines", System.Data.SqlDbType.Real, 4, "EnvirFines"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirSpill", System.Data.SqlDbType.Real, 4, "EnvirSpill"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirLab", System.Data.SqlDbType.Real, 4, "EnvirLab"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirEng", System.Data.SqlDbType.Real, 4, "EnvirEng"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnvirOth", System.Data.SqlDbType.Real, 4, "EnvirOth"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Envir", System.Data.SqlDbType.Real, 4, "Envir"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EquipNonMaint", System.Data.SqlDbType.Real, 4, "EquipNonMaint"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Tax", System.Data.SqlDbType.Real, 4, "Tax"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InsurBI", System.Data.SqlDbType.Real, 4, "InsurBI"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InsurPC", System.Data.SqlDbType.Real, 4, "InsurPC"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InsurOth", System.Data.SqlDbType.Real, 4, "InsurOth"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSupply", System.Data.SqlDbType.Real, 4, "OthNonVolSupply"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolSafety", System.Data.SqlDbType.Real, 4, "OthNonVolSafety"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolNonPersSafety", System.Data.SqlDbType.Real, 4, "OthNonVolNonPersSafety"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComm", System.Data.SqlDbType.Real, 4, "OthNonVolComm"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDonations", System.Data.SqlDbType.Real, 4, "OthNonVolDonations"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolNonContribPers", System.Data.SqlDbType.Real, 4, "OthNonVolNonContribPers"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolDues", System.Data.SqlDbType.Real, 4, "OthNonVolDues"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTravel", System.Data.SqlDbType.Real, 4, "OthNonVolTravel"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTrain", System.Data.SqlDbType.Real, 4, "OthNonVolTrain"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolComputer", System.Data.SqlDbType.Real, 4, "OthNonVolComputer"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolTanks", System.Data.SqlDbType.Real, 4, "OthNonVolTanks"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolExtraExpat", System.Data.SqlDbType.Real, 4, "OthNonVolExtraExpat"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVolOth", System.Data.SqlDbType.Real, 4, "OthNonVolOth"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthNonVol", System.Data.SqlDbType.Real, 4, "OthNonVol"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonVol", System.Data.SqlDbType.Real, 4, "STNonVol"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GAPers", System.Data.SqlDbType.Real, 4, "GAPers"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAntiknock", System.Data.SqlDbType.Real, 4, "ChemicalsAntiknock"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, "ChemicalsAlkyAcid"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsLube", System.Data.SqlDbType.Real, 4, "ChemicalsLube"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, "ChemicalsH2OTreat"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, "ChemicalsOthAcid"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsProcess", System.Data.SqlDbType.Real, 4, "ChemicalsProcess"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, "ChemicalsGasAdd"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, "ChemicalsDieselAdd"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, "ChemicalsOthAdd"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsO2", System.Data.SqlDbType.Real, 4, "ChemicalsO2"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsClay", System.Data.SqlDbType.Real, 4, "ChemicalsClay"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAmines", System.Data.SqlDbType.Real, 4, "ChemicalsAmines"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsASESolv", System.Data.SqlDbType.Real, 4, "ChemicalsASESolv"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, "ChemicalsWasteH2O"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsNMP", System.Data.SqlDbType.Real, 4, "ChemicalsNMP"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsFurfural", System.Data.SqlDbType.Real, 4, "ChemicalsFurfural"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMIBK", System.Data.SqlDbType.Real, 4, "ChemicalsMIBK"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsMEK", System.Data.SqlDbType.Real, 4, "ChemicalsMEK"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsToluene", System.Data.SqlDbType.Real, 4, "ChemicalsToluene"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsPropane", System.Data.SqlDbType.Real, 4, "ChemicalsPropane"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, "ChemicalsOthSolv"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsAcid", System.Data.SqlDbType.Real, 4, "ChemicalsAcid"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, "ChemicalsDewaxAids"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChemicalsOth", System.Data.SqlDbType.Real, 4, "ChemicalsOth"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Chemicals", System.Data.SqlDbType.Real, 4, "Chemicals"))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Scenario", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Scenario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Chemicals", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Chemicals", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsASESolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsASESolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAlkyAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAlkyAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAmines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAmines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsAntiknock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsAntiknock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsClay", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsClay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDewaxAids", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDewaxAids", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsDieselAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsDieselAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsFurfural", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsFurfural", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsGasAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsGasAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsH2OTreat", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsH2OTreat", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsLube", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsLube", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMEK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMEK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsMIBK", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsMIBK", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsNMP", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsNMP", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAcid", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAcid", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthAdd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthAdd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsOthSolv", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsOthSolv", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsProcess", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsProcess", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsPropane", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsPropane", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsToluene", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsToluene", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ChemicalsWasteH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ChemicalsWasteH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintInspect", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintInspect", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintLabor", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintLabor", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintLaborST", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintLaborST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ContMaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ContMaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Envir", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Envir", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirDisp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirDisp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirEng", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirEng", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirPermits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirPermits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnvirSpill", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnvirSpill", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EquipNonMaint", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EquipNonMaint", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GAPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GAPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InsurBI", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InsurBI", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InsurOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InsurOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InsurPC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InsurPC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MPSSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MPSSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MaintMatlST", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MaintMatlST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenAbs", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenAbs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenInsur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenInsur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenPension", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenPension", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenStock", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenStock", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenSub", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenSub", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxMed", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxMed", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCBenTaxPen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCBenTaxPen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OCCSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OCCSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthCont", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthCont", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContAdmin", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContAdmin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContComputing", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContComputing", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContConsult", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContConsult", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFire", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFire", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContFoodSvc", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContFoodSvc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContJan", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContJan", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLab", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLab", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContLegal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContLegal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContProcOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContProcOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContSecurity", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContSecurity", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContTransOp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContTransOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthContVacTrucks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthContVacTrucks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComm", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComm", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolComputer", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolComputer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDonations", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDonations", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolDues", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolDues", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolExtraExpat", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolExtraExpat", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolNonContribPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolNonContribPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolNonPersSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolNonPersSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolSupply", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolSupply", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTanks", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTanks", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTrain", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTrain", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthNonVolTravel", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthNonVolTravel", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAAdj", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAAdj", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand23.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Tax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tax", System.Data.DataRowVersion.Original, Nothing))
        '
        'sdOpexAll2
        '
        Me.sdOpexAll2.DeleteCommand = Me.SqlDeleteCommand25
        Me.sdOpexAll2.InsertCommand = Me.SqlInsertCommand25
        Me.sdOpexAll2.SelectCommand = Me.SqlSelectCommand25
        Me.sdOpexAll2.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "OpExAll", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CatalystsFCC", "CatalystsFCC"), New System.Data.Common.DataColumnMapping("CatalystsHYC", "CatalystsHYC"), New System.Data.Common.DataColumnMapping("CatalystsNKSHYT", "CatalystsNKSHYT"), New System.Data.Common.DataColumnMapping("CatalystsDHYT", "CatalystsDHYT"), New System.Data.Common.DataColumnMapping("CatalystsVHYT", "CatalystsVHYT"), New System.Data.Common.DataColumnMapping("CatalystsRHYT", "CatalystsRHYT"), New System.Data.Common.DataColumnMapping("CatalystsHYFT", "CatalystsHYFT"), New System.Data.Common.DataColumnMapping("CatalystsCDWax", "CatalystsCDWax"), New System.Data.Common.DataColumnMapping("CatalystsREF", "CatalystsREF"), New System.Data.Common.DataColumnMapping("CatalystsHYG", "CatalystsHYG"), New System.Data.Common.DataColumnMapping("CatalystsS2Plant", "CatalystsS2Plant"), New System.Data.Common.DataColumnMapping("CatalystsPetChem", "CatalystsPetChem"), New System.Data.Common.DataColumnMapping("Catalysts", "Catalysts"), New System.Data.Common.DataColumnMapping("CatalystsOth", "CatalystsOth"), New System.Data.Common.DataColumnMapping("PurOthN2", "PurOthN2"), New System.Data.Common.DataColumnMapping("PurOthH2O", "PurOthH2O"), New System.Data.Common.DataColumnMapping("PurOthOth", "PurOthOth"), New System.Data.Common.DataColumnMapping("PurOth", "PurOth"), New System.Data.Common.DataColumnMapping("Royalties", "Royalties"), New System.Data.Common.DataColumnMapping("EmissionsTaxes", "EmissionsTaxes"), New System.Data.Common.DataColumnMapping("EmissionsPurch", "EmissionsPurch"), New System.Data.Common.DataColumnMapping("EmissionsCredits", "EmissionsCredits"), New System.Data.Common.DataColumnMapping("OthVolOth", "OthVolOth"), New System.Data.Common.DataColumnMapping("OthVol", "OthVol"), New System.Data.Common.DataColumnMapping("GANonPers", "GANonPers"), New System.Data.Common.DataColumnMapping("InvenCarry", "InvenCarry"), New System.Data.Common.DataColumnMapping("Depreciation", "Depreciation"), New System.Data.Common.DataColumnMapping("Interest", "Interest"), New System.Data.Common.DataColumnMapping("STNonCash", "STNonCash"), New System.Data.Common.DataColumnMapping("TotRefExp", "TotRefExp"), New System.Data.Common.DataColumnMapping("Cogen", "Cogen"), New System.Data.Common.DataColumnMapping("OthRevenue", "OthRevenue"), New System.Data.Common.DataColumnMapping("ThirdPartyTerminalRM", "ThirdPartyTerminalRM"), New System.Data.Common.DataColumnMapping("ThirdPartyTerminalProd", "ThirdPartyTerminalProd"), New System.Data.Common.DataColumnMapping("POXO2", "POXO2"), New System.Data.Common.DataColumnMapping("PMAA", "PMAA"), New System.Data.Common.DataColumnMapping("OthVolDemCrude", "OthVolDemCrude"), New System.Data.Common.DataColumnMapping("OthVolDemLightering", "OthVolDemLightering"), New System.Data.Common.DataColumnMapping("OthVolDemProd", "OthVolDemProd"), New System.Data.Common.DataColumnMapping("STVol", "STVol"), New System.Data.Common.DataColumnMapping("TotCashOpEx", "TotCashOpEx"), New System.Data.Common.DataColumnMapping("ExclFireSafety", "ExclFireSafety"), New System.Data.Common.DataColumnMapping("ExclEnvirFines", "ExclEnvirFines"), New System.Data.Common.DataColumnMapping("ExclOth", "ExclOth"), New System.Data.Common.DataColumnMapping("TotExpExcl", "TotExpExcl"), New System.Data.Common.DataColumnMapping("STSal", "STSal"), New System.Data.Common.DataColumnMapping("STBen", "STBen"), New System.Data.Common.DataColumnMapping("PersCostExclTA", "PersCostExclTA"), New System.Data.Common.DataColumnMapping("PersCost", "PersCost"), New System.Data.Common.DataColumnMapping("EnergyCost", "EnergyCost"), New System.Data.Common.DataColumnMapping("NEOpex", "NEOpex"), New System.Data.Common.DataColumnMapping("Currency", "Currency"), New System.Data.Common.DataColumnMapping("DataType", "DataType"), New System.Data.Common.DataColumnMapping("Scenario", "Scenario"), New System.Data.Common.DataColumnMapping("SubmissionID", "SubmissionID")})})
        Me.sdOpexAll2.UpdateCommand = Me.SqlUpdateCommand24
        '
        'SqlDeleteCommand25
        '
        Me.SqlDeleteCommand25.CommandText = "DELETE FROM dbo.OpExAll WHERE (Currency = @Original_Currency) AND (DataType = @Or" & _
        "iginal_DataType) AND (Scenario = @Original_Scenario) AND (SubmissionID = @Origin" & _
        "al_SubmissionID) AND (Catalysts = @Original_Catalysts OR @Original_Catalysts IS " & _
        "NULL AND Catalysts IS NULL) AND (CatalystsCDWax = @Original_CatalystsCDWax OR @O" & _
        "riginal_CatalystsCDWax IS NULL AND CatalystsCDWax IS NULL) AND (CatalystsDHYT = " & _
        "@Original_CatalystsDHYT OR @Original_CatalystsDHYT IS NULL AND CatalystsDHYT IS " & _
        "NULL) AND (CatalystsFCC = @Original_CatalystsFCC OR @Original_CatalystsFCC IS NU" & _
        "LL AND CatalystsFCC IS NULL) AND (CatalystsHYC = @Original_CatalystsHYC OR @Orig" & _
        "inal_CatalystsHYC IS NULL AND CatalystsHYC IS NULL) AND (CatalystsHYFT = @Origin" & _
        "al_CatalystsHYFT OR @Original_CatalystsHYFT IS NULL AND CatalystsHYFT IS NULL) A" & _
        "ND (CatalystsHYG = @Original_CatalystsHYG OR @Original_CatalystsHYG IS NULL AND " & _
        "CatalystsHYG IS NULL) AND (CatalystsNKSHYT = @Original_CatalystsNKSHYT OR @Origi" & _
        "nal_CatalystsNKSHYT IS NULL AND CatalystsNKSHYT IS NULL) AND (CatalystsOth = @Or" & _
        "iginal_CatalystsOth OR @Original_CatalystsOth IS NULL AND CatalystsOth IS NULL) " & _
        "AND (CatalystsPetChem = @Original_CatalystsPetChem OR @Original_CatalystsPetChem" & _
        " IS NULL AND CatalystsPetChem IS NULL) AND (CatalystsREF = @Original_CatalystsRE" & _
        "F OR @Original_CatalystsREF IS NULL AND CatalystsREF IS NULL) AND (CatalystsRHYT" & _
        " = @Original_CatalystsRHYT OR @Original_CatalystsRHYT IS NULL AND CatalystsRHYT " & _
        "IS NULL) AND (CatalystsS2Plant = @Original_CatalystsS2Plant OR @Original_Catalys" & _
        "tsS2Plant IS NULL AND CatalystsS2Plant IS NULL) AND (CatalystsVHYT = @Original_C" & _
        "atalystsVHYT OR @Original_CatalystsVHYT IS NULL AND CatalystsVHYT IS NULL) AND (" & _
        "Cogen = @Original_Cogen OR @Original_Cogen IS NULL AND Cogen IS NULL) AND (Depre" & _
        "ciation = @Original_Depreciation OR @Original_Depreciation IS NULL AND Depreciat" & _
        "ion IS NULL) AND (EmissionsCredits = @Original_EmissionsCredits OR @Original_Emi" & _
        "ssionsCredits IS NULL AND EmissionsCredits IS NULL) AND (EmissionsPurch = @Origi" & _
        "nal_EmissionsPurch OR @Original_EmissionsPurch IS NULL AND EmissionsPurch IS NUL" & _
        "L) AND (EmissionsTaxes = @Original_EmissionsTaxes OR @Original_EmissionsTaxes IS" & _
        " NULL AND EmissionsTaxes IS NULL) AND (EnergyCost = @Original_EnergyCost OR @Ori" & _
        "ginal_EnergyCost IS NULL AND EnergyCost IS NULL) AND (ExclEnvirFines = @Original" & _
        "_ExclEnvirFines OR @Original_ExclEnvirFines IS NULL AND ExclEnvirFines IS NULL) " & _
        "AND (ExclFireSafety = @Original_ExclFireSafety OR @Original_ExclFireSafety IS NU" & _
        "LL AND ExclFireSafety IS NULL) AND (ExclOth = @Original_ExclOth OR @Original_Exc" & _
        "lOth IS NULL AND ExclOth IS NULL) AND (GANonPers = @Original_GANonPers OR @Origi" & _
        "nal_GANonPers IS NULL AND GANonPers IS NULL) AND (Interest = @Original_Interest " & _
        "OR @Original_Interest IS NULL AND Interest IS NULL) AND (InvenCarry = @Original_" & _
        "InvenCarry OR @Original_InvenCarry IS NULL AND InvenCarry IS NULL) AND (NEOpex =" & _
        " @Original_NEOpex OR @Original_NEOpex IS NULL AND NEOpex IS NULL) AND (OthRevenu" & _
        "e = @Original_OthRevenue OR @Original_OthRevenue IS NULL AND OthRevenue IS NULL)" & _
        " AND (OthVol = @Original_OthVol OR @Original_OthVol IS NULL AND OthVol IS NULL) " & _
        "AND (OthVolDemCrude = @Original_OthVolDemCrude OR @Original_OthVolDemCrude IS NU" & _
        "LL AND OthVolDemCrude IS NULL) AND (OthVolDemLightering = @Original_OthVolDemLig" & _
        "htering OR @Original_OthVolDemLightering IS NULL AND OthVolDemLightering IS NULL" & _
        ") AND (OthVolDemProd = @Original_OthVolDemProd OR @Original_OthVolDemProd IS NUL" & _
        "L AND OthVolDemProd IS NULL) AND (OthVolOth = @Original_OthVolOth OR @Original_O" & _
        "thVolOth IS NULL AND OthVolOth IS NULL) AND (PMAA = @Original_PMAA OR @Original_" & _
        "PMAA IS NULL AND PMAA IS NULL) AND (POXO2 = @Original_POXO2 OR @Original_POXO2 I" & _
        "S NULL AND POXO2 IS NULL) AND (PersCost = @Original_PersCost OR @Original_PersCo" & _
        "st IS NULL AND PersCost IS NULL) AND (PersCostExclTA = @Original_PersCostExclTA " & _
        "OR @Original_PersCostExclTA IS NULL AND PersCostExclTA IS NULL) AND (PurOth = @O" & _
        "riginal_PurOth OR @Original_PurOth IS NULL AND PurOth IS NULL) AND (PurOthH2O = " & _
        "@Original_PurOthH2O OR @Original_PurOthH2O IS NULL AND PurOthH2O IS NULL) AND (P" & _
        "urOthN2 = @Original_PurOthN2 OR @Original_PurOthN2 IS NULL AND PurOthN2 IS NULL)" & _
        " AND (PurOthOth = @Original_PurOthOth OR @Original_PurOthOth IS NULL AND PurOthO" & _
        "th IS NULL) AND (Royalties = @Original_Royalties OR @Original_Royalties IS NULL " & _
        "AND Royalties IS NULL) AND (STBen = @Original_STBen OR @Original_STBen IS NULL A" & _
        "ND STBen IS NULL) AND (STNonCash = @Original_STNonCash OR @Original_STNonCash IS" & _
        " NULL AND STNonCash IS NULL) AND (STSal = @Original_STSal OR @Original_STSal IS " & _
        "NULL AND STSal IS NULL) AND (STVol = @Original_STVol OR @Original_STVol IS NULL " & _
        "AND STVol IS NULL) AND (ThirdPartyTerminalProd = @Original_ThirdPartyTerminalPro" & _
        "d OR @Original_ThirdPartyTerminalProd IS NULL AND ThirdPartyTerminalProd IS NULL" & _
        ") AND (ThirdPartyTerminalRM = @Original_ThirdPartyTerminalRM OR @Original_ThirdP" & _
        "artyTerminalRM IS NULL AND ThirdPartyTerminalRM IS NULL) AND (TotCashOpEx = @Ori" & _
        "ginal_TotCashOpEx OR @Original_TotCashOpEx IS NULL AND TotCashOpEx IS NULL) AND " & _
        "(TotExpExcl = @Original_TotExpExcl OR @Original_TotExpExcl IS NULL AND TotExpExc" & _
        "l IS NULL) AND (TotRefExp = @Original_TotRefExp OR @Original_TotRefExp IS NULL A" & _
        "ND TotRefExp IS NULL)"
        Me.SqlDeleteCommand25.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Scenario", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Scenario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Catalysts", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Catalysts", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsCDWax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsCDWax", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsDHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsDHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsFCC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsFCC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYFT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYFT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsNKSHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsPetChem", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsPetChem", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsREF", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsREF", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsRHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsRHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsS2Plant", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsS2Plant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsVHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsVHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cogen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cogen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Depreciation", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Depreciation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsCredits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsCredits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsPurch", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsPurch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsTaxes", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsTaxes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclEnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclEnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclFireSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclFireSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GANonPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GANonPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Interest", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Interest", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InvenCarry", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InvenCarry", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NEOpex", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NEOpex", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthRevenue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthRevenue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemCrude", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemCrude", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemLightering", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemLightering", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_POXO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "POXO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCostExclTA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCostExclTA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthN2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthN2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Royalties", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Royalties", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonCash", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonCash", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalRM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotCashOpEx", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotCashOpEx", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotExpExcl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotExpExcl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotRefExp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotRefExp", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand25
        '
        Me.SqlInsertCommand25.CommandText = "INSERT INTO dbo.OpExAll(CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, CatalystsDHY" & _
        "T, CatalystsVHYT, CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, Ca" & _
        "talystsHYG, CatalystsS2Plant, CatalystsPetChem, Catalysts, CatalystsOth, PurOthN" & _
        "2, PurOthH2O, PurOthOth, PurOth, Royalties, EmissionsTaxes, EmissionsPurch, Emis" & _
        "sionsCredits, OthVolOth, OthVol, GANonPers, InvenCarry, Depreciation, Interest, " & _
        "STNonCash, TotRefExp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTermina" & _
        "lProd, POXO2, PMAA, OthVolDemCrude, OthVolDemLightering, OthVolDemProd, STVol, T" & _
        "otCashOpEx, ExclFireSafety, ExclEnvirFines, ExclOth, TotExpExcl, STSal, STBen, P" & _
        "ersCostExclTA, PersCost, EnergyCost, NEOpex, Currency, DataType, Scenario, Submi" & _
        "ssionID) VALUES (@CatalystsFCC, @CatalystsHYC, @CatalystsNKSHYT, @CatalystsDHYT," & _
        " @CatalystsVHYT, @CatalystsRHYT, @CatalystsHYFT, @CatalystsCDWax, @CatalystsREF," & _
        " @CatalystsHYG, @CatalystsS2Plant, @CatalystsPetChem, @Catalysts, @CatalystsOth," & _
        " @PurOthN2, @PurOthH2O, @PurOthOth, @PurOth, @Royalties, @EmissionsTaxes, @Emiss" & _
        "ionsPurch, @EmissionsCredits, @OthVolOth, @OthVol, @GANonPers, @InvenCarry, @Dep" & _
        "reciation, @Interest, @STNonCash, @TotRefExp, @Cogen, @OthRevenue, @ThirdPartyTe" & _
        "rminalRM, @ThirdPartyTerminalProd, @POXO2, @PMAA, @OthVolDemCrude, @OthVolDemLig" & _
        "htering, @OthVolDemProd, @STVol, @TotCashOpEx, @ExclFireSafety, @ExclEnvirFines," & _
        " @ExclOth, @TotExpExcl, @STSal, @STBen, @PersCostExclTA, @PersCost, @EnergyCost," & _
        " @NEOpex, @Currency, @DataType, @Scenario, @SubmissionID)"
        Me.SqlInsertCommand25.Connection = Me.SqlConnection1
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsFCC", System.Data.SqlDbType.Real, 4, "CatalystsFCC"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYC", System.Data.SqlDbType.Real, 4, "CatalystsHYC"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, "CatalystsNKSHYT"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsDHYT", System.Data.SqlDbType.Real, 4, "CatalystsDHYT"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsVHYT", System.Data.SqlDbType.Real, 4, "CatalystsVHYT"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsRHYT", System.Data.SqlDbType.Real, 4, "CatalystsRHYT"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYFT", System.Data.SqlDbType.Real, 4, "CatalystsHYFT"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsCDWax", System.Data.SqlDbType.Real, 4, "CatalystsCDWax"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsREF", System.Data.SqlDbType.Real, 4, "CatalystsREF"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYG", System.Data.SqlDbType.Real, 4, "CatalystsHYG"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsS2Plant", System.Data.SqlDbType.Real, 4, "CatalystsS2Plant"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsPetChem", System.Data.SqlDbType.Real, 4, "CatalystsPetChem"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Catalysts", System.Data.SqlDbType.Real, 4, "Catalysts"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsOth", System.Data.SqlDbType.Real, 4, "CatalystsOth"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthN2", System.Data.SqlDbType.Real, 4, "PurOthN2"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthH2O", System.Data.SqlDbType.Real, 4, "PurOthH2O"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthOth", System.Data.SqlDbType.Real, 4, "PurOthOth"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOth", System.Data.SqlDbType.Real, 4, "PurOth"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Royalties", System.Data.SqlDbType.Real, 4, "Royalties"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsTaxes", System.Data.SqlDbType.Real, 4, "EmissionsTaxes"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsPurch", System.Data.SqlDbType.Real, 4, "EmissionsPurch"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsCredits", System.Data.SqlDbType.Real, 4, "EmissionsCredits"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolOth", System.Data.SqlDbType.Real, 4, "OthVolOth"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVol", System.Data.SqlDbType.Real, 4, "OthVol"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GANonPers", System.Data.SqlDbType.Real, 4, "GANonPers"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InvenCarry", System.Data.SqlDbType.Real, 4, "InvenCarry"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Depreciation", System.Data.SqlDbType.Real, 4, "Depreciation"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Interest", System.Data.SqlDbType.Real, 4, "Interest"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonCash", System.Data.SqlDbType.Real, 4, "STNonCash"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotRefExp", System.Data.SqlDbType.Real, 4, "TotRefExp"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cogen", System.Data.SqlDbType.Real, 4, "Cogen"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthRevenue", System.Data.SqlDbType.Real, 4, "OthRevenue"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalRM"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalProd"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@POXO2", System.Data.SqlDbType.Real, 4, "POXO2"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAA", System.Data.SqlDbType.Real, 4, "PMAA"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemCrude", System.Data.SqlDbType.Real, 4, "OthVolDemCrude"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemLightering", System.Data.SqlDbType.Real, 4, "OthVolDemLightering"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemProd", System.Data.SqlDbType.Real, 4, "OthVolDemProd"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STVol", System.Data.SqlDbType.Real, 4, "STVol"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotCashOpEx", System.Data.SqlDbType.Real, 4, "TotCashOpEx"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclFireSafety", System.Data.SqlDbType.Real, 4, "ExclFireSafety"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclEnvirFines", System.Data.SqlDbType.Real, 4, "ExclEnvirFines"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclOth", System.Data.SqlDbType.Real, 4, "ExclOth"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotExpExcl", System.Data.SqlDbType.Real, 4, "TotExpExcl"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STSal", System.Data.SqlDbType.Real, 4, "STSal"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STBen", System.Data.SqlDbType.Real, 4, "STBen"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCostExclTA", System.Data.SqlDbType.Real, 4, "PersCostExclTA"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCost", System.Data.SqlDbType.Real, 4, "PersCost"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyCost", System.Data.SqlDbType.Real, 4, "EnergyCost"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NEOpex", System.Data.SqlDbType.Real, 4, "NEOpex"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Scenario", System.Data.SqlDbType.VarChar, 8, "Scenario"))
        Me.SqlInsertCommand25.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        '
        'SqlSelectCommand25
        '
        Me.SqlSelectCommand25.CommandText = "SELECT CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT," & _
        " CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, CatalystsHYG, Catal" & _
        "ystsS2Plant, CatalystsPetChem, Catalysts, CatalystsOth, PurOthN2, PurOthH2O, Pur" & _
        "OthOth, PurOth, Royalties, EmissionsTaxes, EmissionsPurch, EmissionsCredits, Oth" & _
        "VolOth, OthVol, GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRef" & _
        "Exp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMA" & _
        "A, OthVolDemCrude, OthVolDemLightering, OthVolDemProd, STVol, TotCashOpEx, ExclF" & _
        "ireSafety, ExclEnvirFines, ExclOth, TotExpExcl, STSal, STBen, PersCostExclTA, Pe" & _
        "rsCost, EnergyCost, NEOpex, Currency, DataType, Scenario, SubmissionID FROM dbo." & _
        "OpExAll"
        Me.SqlSelectCommand25.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand24
        '
        Me.SqlUpdateCommand24.CommandText = "UPDATE dbo.OpExAll SET CatalystsFCC = @CatalystsFCC, CatalystsHYC = @CatalystsHYC" & _
        ", CatalystsNKSHYT = @CatalystsNKSHYT, CatalystsDHYT = @CatalystsDHYT, CatalystsV" & _
        "HYT = @CatalystsVHYT, CatalystsRHYT = @CatalystsRHYT, CatalystsHYFT = @Catalysts" & _
        "HYFT, CatalystsCDWax = @CatalystsCDWax, CatalystsREF = @CatalystsREF, CatalystsH" & _
        "YG = @CatalystsHYG, CatalystsS2Plant = @CatalystsS2Plant, CatalystsPetChem = @Ca" & _
        "talystsPetChem, Catalysts = @Catalysts, CatalystsOth = @CatalystsOth, PurOthN2 =" & _
        " @PurOthN2, PurOthH2O = @PurOthH2O, PurOthOth = @PurOthOth, PurOth = @PurOth, Ro" & _
        "yalties = @Royalties, EmissionsTaxes = @EmissionsTaxes, EmissionsPurch = @Emissi" & _
        "onsPurch, EmissionsCredits = @EmissionsCredits, OthVolOth = @OthVolOth, OthVol =" & _
        " @OthVol, GANonPers = @GANonPers, InvenCarry = @InvenCarry, Depreciation = @Depr" & _
        "eciation, Interest = @Interest, STNonCash = @STNonCash, TotRefExp = @TotRefExp, " & _
        "Cogen = @Cogen, OthRevenue = @OthRevenue, ThirdPartyTerminalRM = @ThirdPartyTerm" & _
        "inalRM, ThirdPartyTerminalProd = @ThirdPartyTerminalProd, POXO2 = @POXO2, PMAA =" & _
        " @PMAA, OthVolDemCrude = @OthVolDemCrude, OthVolDemLightering = @OthVolDemLighte" & _
        "ring, OthVolDemProd = @OthVolDemProd, STVol = @STVol, TotCashOpEx = @TotCashOpEx" & _
        ", ExclFireSafety = @ExclFireSafety, ExclEnvirFines = @ExclEnvirFines, ExclOth = " & _
        "@ExclOth, TotExpExcl = @TotExpExcl, STSal = @STSal, STBen = @STBen, PersCostExcl" & _
        "TA = @PersCostExclTA, PersCost = @PersCost, EnergyCost = @EnergyCost, NEOpex = @" & _
        "NEOpex, Currency = @Currency, DataType = @DataType, Scenario = @Scenario, Submis" & _
        "sionID = @SubmissionID WHERE (Currency = @Original_Currency) AND (DataType = @Or" & _
        "iginal_DataType) AND (Scenario = @Original_Scenario) AND (SubmissionID = @Origin" & _
        "al_SubmissionID) AND (Catalysts = @Original_Catalysts OR @Original_Catalysts IS " & _
        "NULL AND Catalysts IS NULL) AND (CatalystsCDWax = @Original_CatalystsCDWax OR @O" & _
        "riginal_CatalystsCDWax IS NULL AND CatalystsCDWax IS NULL) AND (CatalystsDHYT = " & _
        "@Original_CatalystsDHYT OR @Original_CatalystsDHYT IS NULL AND CatalystsDHYT IS " & _
        "NULL) AND (CatalystsFCC = @Original_CatalystsFCC OR @Original_CatalystsFCC IS NU" & _
        "LL AND CatalystsFCC IS NULL) AND (CatalystsHYC = @Original_CatalystsHYC OR @Orig" & _
        "inal_CatalystsHYC IS NULL AND CatalystsHYC IS NULL) AND (CatalystsHYFT = @Origin" & _
        "al_CatalystsHYFT OR @Original_CatalystsHYFT IS NULL AND CatalystsHYFT IS NULL) A" & _
        "ND (CatalystsHYG = @Original_CatalystsHYG OR @Original_CatalystsHYG IS NULL AND " & _
        "CatalystsHYG IS NULL) AND (CatalystsNKSHYT = @Original_CatalystsNKSHYT OR @Origi" & _
        "nal_CatalystsNKSHYT IS NULL AND CatalystsNKSHYT IS NULL) AND (CatalystsOth = @Or" & _
        "iginal_CatalystsOth OR @Original_CatalystsOth IS NULL AND CatalystsOth IS NULL) " & _
        "AND (CatalystsPetChem = @Original_CatalystsPetChem OR @Original_CatalystsPetChem" & _
        " IS NULL AND CatalystsPetChem IS NULL) AND (CatalystsREF = @Original_CatalystsRE" & _
        "F OR @Original_CatalystsREF IS NULL AND CatalystsREF IS NULL) AND (CatalystsRHYT" & _
        " = @Original_CatalystsRHYT OR @Original_CatalystsRHYT IS NULL AND CatalystsRHYT " & _
        "IS NULL) AND (CatalystsS2Plant = @Original_CatalystsS2Plant OR @Original_Catalys" & _
        "tsS2Plant IS NULL AND CatalystsS2Plant IS NULL) AND (CatalystsVHYT = @Original_C" & _
        "atalystsVHYT OR @Original_CatalystsVHYT IS NULL AND CatalystsVHYT IS NULL) AND (" & _
        "Cogen = @Original_Cogen OR @Original_Cogen IS NULL AND Cogen IS NULL) AND (Depre" & _
        "ciation = @Original_Depreciation OR @Original_Depreciation IS NULL AND Depreciat" & _
        "ion IS NULL) AND (EmissionsCredits = @Original_EmissionsCredits OR @Original_Emi" & _
        "ssionsCredits IS NULL AND EmissionsCredits IS NULL) AND (EmissionsPurch = @Origi" & _
        "nal_EmissionsPurch OR @Original_EmissionsPurch IS NULL AND EmissionsPurch IS NUL" & _
        "L) AND (EmissionsTaxes = @Original_EmissionsTaxes OR @Original_EmissionsTaxes IS" & _
        " NULL AND EmissionsTaxes IS NULL) AND (EnergyCost = @Original_EnergyCost OR @Ori" & _
        "ginal_EnergyCost IS NULL AND EnergyCost IS NULL) AND (ExclEnvirFines = @Original" & _
        "_ExclEnvirFines OR @Original_ExclEnvirFines IS NULL AND ExclEnvirFines IS NULL) " & _
        "AND (ExclFireSafety = @Original_ExclFireSafety OR @Original_ExclFireSafety IS NU" & _
        "LL AND ExclFireSafety IS NULL) AND (ExclOth = @Original_ExclOth OR @Original_Exc" & _
        "lOth IS NULL AND ExclOth IS NULL) AND (GANonPers = @Original_GANonPers OR @Origi" & _
        "nal_GANonPers IS NULL AND GANonPers IS NULL) AND (Interest = @Original_Interest " & _
        "OR @Original_Interest IS NULL AND Interest IS NULL) AND (InvenCarry = @Original_" & _
        "InvenCarry OR @Original_InvenCarry IS NULL AND InvenCarry IS NULL) AND (NEOpex =" & _
        " @Original_NEOpex OR @Original_NEOpex IS NULL AND NEOpex IS NULL) AND (OthRevenu" & _
        "e = @Original_OthRevenue OR @Original_OthRevenue IS NULL AND OthRevenue IS NULL)" & _
        " AND (OthVol = @Original_OthVol OR @Original_OthVol IS NULL AND OthVol IS NULL) " & _
        "AND (OthVolDemCrude = @Original_OthVolDemCrude OR @Original_OthVolDemCrude IS NU" & _
        "LL AND OthVolDemCrude IS NULL) AND (OthVolDemLightering = @Original_OthVolDemLig" & _
        "htering OR @Original_OthVolDemLightering IS NULL AND OthVolDemLightering IS NULL" & _
        ") AND (OthVolDemProd = @Original_OthVolDemProd OR @Original_OthVolDemProd IS NUL" & _
        "L AND OthVolDemProd IS NULL) AND (OthVolOth = @Original_OthVolOth OR @Original_O" & _
        "thVolOth IS NULL AND OthVolOth IS NULL) AND (PMAA = @Original_PMAA OR @Original_" & _
        "PMAA IS NULL AND PMAA IS NULL) AND (POXO2 = @Original_POXO2 OR @Original_POXO2 I" & _
        "S NULL AND POXO2 IS NULL) AND (PersCost = @Original_PersCost OR @Original_PersCo" & _
        "st IS NULL AND PersCost IS NULL) AND (PersCostExclTA = @Original_PersCostExclTA " & _
        "OR @Original_PersCostExclTA IS NULL AND PersCostExclTA IS NULL) AND (PurOth = @O" & _
        "riginal_PurOth OR @Original_PurOth IS NULL AND PurOth IS NULL) AND (PurOthH2O = " & _
        "@Original_PurOthH2O OR @Original_PurOthH2O IS NULL AND PurOthH2O IS NULL) AND (P" & _
        "urOthN2 = @Original_PurOthN2 OR @Original_PurOthN2 IS NULL AND PurOthN2 IS NULL)" & _
        " AND (PurOthOth = @Original_PurOthOth OR @Original_PurOthOth IS NULL AND PurOthO" & _
        "th IS NULL) AND (Royalties = @Original_Royalties OR @Original_Royalties IS NULL " & _
        "AND Royalties IS NULL) AND (STBen = @Original_STBen OR @Original_STBen IS NULL A" & _
        "ND STBen IS NULL) AND (STNonCash = @Original_STNonCash OR @Original_STNonCash IS" & _
        " NULL AND STNonCash IS NULL) AND (STSal = @Original_STSal OR @Original_STSal IS " & _
        "NULL AND STSal IS NULL) AND (STVol = @Original_STVol OR @Original_STVol IS NULL " & _
        "AND STVol IS NULL) AND (ThirdPartyTerminalProd = @Original_ThirdPartyTerminalPro" & _
        "d OR @Original_ThirdPartyTerminalProd IS NULL AND ThirdPartyTerminalProd IS NULL" & _
        ") AND (ThirdPartyTerminalRM = @Original_ThirdPartyTerminalRM OR @Original_ThirdP" & _
        "artyTerminalRM IS NULL AND ThirdPartyTerminalRM IS NULL) AND (TotCashOpEx = @Ori" & _
        "ginal_TotCashOpEx OR @Original_TotCashOpEx IS NULL AND TotCashOpEx IS NULL) AND " & _
        "(TotExpExcl = @Original_TotExpExcl OR @Original_TotExpExcl IS NULL AND TotExpExc" & _
        "l IS NULL) AND (TotRefExp = @Original_TotRefExp OR @Original_TotRefExp IS NULL A" & _
        "ND TotRefExp IS NULL)"
        Me.SqlUpdateCommand24.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsFCC", System.Data.SqlDbType.Real, 4, "CatalystsFCC"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYC", System.Data.SqlDbType.Real, 4, "CatalystsHYC"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, "CatalystsNKSHYT"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsDHYT", System.Data.SqlDbType.Real, 4, "CatalystsDHYT"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsVHYT", System.Data.SqlDbType.Real, 4, "CatalystsVHYT"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsRHYT", System.Data.SqlDbType.Real, 4, "CatalystsRHYT"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYFT", System.Data.SqlDbType.Real, 4, "CatalystsHYFT"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsCDWax", System.Data.SqlDbType.Real, 4, "CatalystsCDWax"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsREF", System.Data.SqlDbType.Real, 4, "CatalystsREF"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsHYG", System.Data.SqlDbType.Real, 4, "CatalystsHYG"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsS2Plant", System.Data.SqlDbType.Real, 4, "CatalystsS2Plant"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsPetChem", System.Data.SqlDbType.Real, 4, "CatalystsPetChem"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Catalysts", System.Data.SqlDbType.Real, 4, "Catalysts"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CatalystsOth", System.Data.SqlDbType.Real, 4, "CatalystsOth"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthN2", System.Data.SqlDbType.Real, 4, "PurOthN2"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthH2O", System.Data.SqlDbType.Real, 4, "PurOthH2O"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOthOth", System.Data.SqlDbType.Real, 4, "PurOthOth"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PurOth", System.Data.SqlDbType.Real, 4, "PurOth"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Royalties", System.Data.SqlDbType.Real, 4, "Royalties"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsTaxes", System.Data.SqlDbType.Real, 4, "EmissionsTaxes"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsPurch", System.Data.SqlDbType.Real, 4, "EmissionsPurch"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EmissionsCredits", System.Data.SqlDbType.Real, 4, "EmissionsCredits"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolOth", System.Data.SqlDbType.Real, 4, "OthVolOth"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVol", System.Data.SqlDbType.Real, 4, "OthVol"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GANonPers", System.Data.SqlDbType.Real, 4, "GANonPers"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InvenCarry", System.Data.SqlDbType.Real, 4, "InvenCarry"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Depreciation", System.Data.SqlDbType.Real, 4, "Depreciation"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Interest", System.Data.SqlDbType.Real, 4, "Interest"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STNonCash", System.Data.SqlDbType.Real, 4, "STNonCash"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotRefExp", System.Data.SqlDbType.Real, 4, "TotRefExp"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cogen", System.Data.SqlDbType.Real, 4, "Cogen"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthRevenue", System.Data.SqlDbType.Real, 4, "OthRevenue"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalRM"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, "ThirdPartyTerminalProd"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@POXO2", System.Data.SqlDbType.Real, 4, "POXO2"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAA", System.Data.SqlDbType.Real, 4, "PMAA"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemCrude", System.Data.SqlDbType.Real, 4, "OthVolDemCrude"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemLightering", System.Data.SqlDbType.Real, 4, "OthVolDemLightering"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OthVolDemProd", System.Data.SqlDbType.Real, 4, "OthVolDemProd"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STVol", System.Data.SqlDbType.Real, 4, "STVol"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotCashOpEx", System.Data.SqlDbType.Real, 4, "TotCashOpEx"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclFireSafety", System.Data.SqlDbType.Real, 4, "ExclFireSafety"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclEnvirFines", System.Data.SqlDbType.Real, 4, "ExclEnvirFines"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExclOth", System.Data.SqlDbType.Real, 4, "ExclOth"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TotExpExcl", System.Data.SqlDbType.Real, 4, "TotExpExcl"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STSal", System.Data.SqlDbType.Real, 4, "STSal"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STBen", System.Data.SqlDbType.Real, 4, "STBen"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCostExclTA", System.Data.SqlDbType.Real, 4, "PersCostExclTA"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PersCost", System.Data.SqlDbType.Real, 4, "PersCost"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyCost", System.Data.SqlDbType.Real, 4, "EnergyCost"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NEOpex", System.Data.SqlDbType.Real, 4, "NEOpex"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Currency", System.Data.SqlDbType.VarChar, 4, "Currency"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataType", System.Data.SqlDbType.VarChar, 6, "DataType"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Scenario", System.Data.SqlDbType.VarChar, 8, "Scenario"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Currency", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Currency", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DataType", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DataType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Scenario", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Scenario", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Catalysts", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Catalysts", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsCDWax", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsCDWax", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsDHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsDHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsFCC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsFCC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYC", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYFT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYFT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsHYG", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsHYG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsNKSHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsNKSHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsPetChem", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsPetChem", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsREF", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsREF", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsRHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsRHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsS2Plant", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsS2Plant", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CatalystsVHYT", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CatalystsVHYT", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cogen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cogen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Depreciation", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Depreciation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsCredits", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsCredits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsPurch", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsPurch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EmissionsTaxes", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EmissionsTaxes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclEnvirFines", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclEnvirFines", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclFireSafety", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclFireSafety", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ExclOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ExclOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_GANonPers", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "GANonPers", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Interest", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Interest", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InvenCarry", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InvenCarry", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NEOpex", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NEOpex", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthRevenue", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthRevenue", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemCrude", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemCrude", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemLightering", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemLightering", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolDemProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolDemProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OthVolOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OthVolOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_POXO2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "POXO2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCost", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PersCostExclTA", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PersCostExclTA", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthH2O", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthH2O", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthN2", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthN2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PurOthOth", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PurOthOth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Royalties", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Royalties", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STBen", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STBen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STNonCash", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STNonCash", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STSal", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STSal", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STVol", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STVol", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalProd", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalProd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ThirdPartyTerminalRM", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ThirdPartyTerminalRM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotCashOpEx", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotCashOpEx", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotExpExcl", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotExpExcl", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand24.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TotRefExp", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TotRefExp", System.Data.DataRowVersion.Original, Nothing))
        '
        'sqlLoadSubmission
        '
        Me.sqlLoadSubmission.CommandText = "dbo.[LoadSubmission]"
        Me.sqlLoadSubmission.CommandType = System.Data.CommandType.StoredProcedure
        Me.sqlLoadSubmission.Connection = Me.SqlConnection1
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RefineryID", System.Data.SqlDbType.VarChar, 6))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodYear", System.Data.SqlDbType.SmallInt, 2))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PeriodMonth", System.Data.SqlDbType.TinyInt, 1))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DataSet", System.Data.SqlDbType.VarChar, 15))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Output, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCurrency", System.Data.SqlDbType.VarChar, 5))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCurrencyT15", System.Data.SqlDbType.VarChar, 5))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOM", System.Data.SqlDbType.VarChar, 5))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Company", System.Data.SqlDbType.VarChar, 30))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Location", System.Data.SqlDbType.VarChar, 30))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordName", System.Data.SqlDbType.VarChar, 50))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordTitle", System.Data.SqlDbType.VarChar, 50))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordPhone", System.Data.SqlDbType.VarChar, 40))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CoordEMail", System.Data.SqlDbType.VarChar, 255))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BridgeVersion", System.Data.SqlDbType.VarChar, 10))
        Me.sqlLoadSubmission.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ClientVersion", System.Data.SqlDbType.VarChar, 10))
        '
        'scPrepareForUpload
        '
        Me.scPrepareForUpload.CommandText = "dbo.[spPrepareForUpload]"
        Me.scPrepareForUpload.CommandType = System.Data.CommandType.StoredProcedure
        Me.scPrepareForUpload.Connection = Me.SqlConnection1
        Me.scPrepareForUpload.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        Me.scPrepareForUpload.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT SubmissionID, UnitID, SortKey, ProcessID, UnitName, ProcessType, Cap, Util" & _
        "Pcnt, StmCap, StmUtilPcnt, InServicePcnt, YearsOper, MHPerWeek, PostPerShift, Bl" & _
        "ockOp, ControlType, CapClass, UtilCap, StmUtilCap, AllocPcntOfTime, AllocPcntOfC" & _
        "ap, AllocUtilPcnt, ActualCap, ActualStmCap, InserviceCap, InserviceStmCap, ModeP" & _
        "cnt, UnusedCapPcnt, RptCap, RptCapUOM, RptStmCap, RptStmCapUOM, DesignFeedSulfur" & _
        ", EnergyPcnt FROM dbo.Config"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO dbo.Config(SubmissionID, UnitID, SortKey, ProcessID, UnitName, Proces" & _
        "sType, Cap, UtilPcnt, StmCap, StmUtilPcnt, InServicePcnt, YearsOper, MHPerWeek, " & _
        "PostPerShift, BlockOp, ControlType, CapClass, UtilCap, StmUtilCap, AllocPcntOfTi" & _
        "me, AllocPcntOfCap, AllocUtilPcnt, ActualCap, ActualStmCap, InserviceCap, Inserv" & _
        "iceStmCap, ModePcnt, UnusedCapPcnt, RptCap, RptCapUOM, RptStmCap, RptStmCapUOM, " & _
        "DesignFeedSulfur, EnergyPcnt) VALUES (@SubmissionID, @UnitID, @SortKey, @Process" & _
        "ID, @UnitName, @ProcessType, @Cap, @UtilPcnt, @StmCap, @StmUtilPcnt, @InServiceP" & _
        "cnt, @YearsOper, @MHPerWeek, @PostPerShift, @BlockOp, @ControlType, @CapClass, @" & _
        "UtilCap, @StmUtilCap, @AllocPcntOfTime, @AllocPcntOfCap, @AllocUtilPcnt, @Actual" & _
        "Cap, @ActualStmCap, @InserviceCap, @InserviceStmCap, @ModePcnt, @UnusedCapPcnt, " & _
        "@RptCap, @RptCapUOM, @RptStmCap, @RptStmCapUOM, @DesignFeedSulfur, @EnergyPcnt);" & _
        " SELECT SubmissionID, UnitID, SortKey, ProcessID, UnitName, ProcessType, Cap, Ut" & _
        "ilPcnt, StmCap, StmUtilPcnt, InServicePcnt, YearsOper, MHPerWeek, PostPerShift, " & _
        "BlockOp, ControlType, CapClass, UtilCap, StmUtilCap, AllocPcntOfTime, AllocPcntO" & _
        "fCap, AllocUtilPcnt, ActualCap, ActualStmCap, InserviceCap, InserviceStmCap, Mod" & _
        "ePcnt, UnusedCapPcnt, RptCap, RptCapUOM, RptStmCap, RptStmCapUOM, DesignFeedSulf" & _
        "ur, EnergyPcnt FROM dbo.Config WHERE (SubmissionID = @SubmissionID) AND (UnitID " & _
        "= @UnitID)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitName", System.Data.SqlDbType.VarChar, 30, "UnitName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessType", System.Data.SqlDbType.VarChar, 4, "ProcessType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cap", System.Data.SqlDbType.Real, 4, "Cap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UtilPcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StmCap", System.Data.SqlDbType.Real, 4, "StmCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StmUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "StmUtilPcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InServicePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "InServicePcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@YearsOper", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "YearsOper", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MHPerWeek", System.Data.SqlDbType.Real, 4, "MHPerWeek"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PostPerShift", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(9, Byte), CType(3, Byte), "PostPerShift", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BlockOp", System.Data.SqlDbType.VarChar, 6, "BlockOp"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ControlType", System.Data.SqlDbType.VarChar, 4, "ControlType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CapClass", System.Data.SqlDbType.TinyInt, 1, "CapClass"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UtilCap", System.Data.SqlDbType.Real, 4, "UtilCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StmUtilCap", System.Data.SqlDbType.Real, 4, "StmUtilCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AllocPcntOfTime", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfTime", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AllocPcntOfCap", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfCap", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AllocUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocUtilPcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ActualCap", System.Data.SqlDbType.Real, 4, "ActualCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ActualStmCap", System.Data.SqlDbType.Real, 4, "ActualStmCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InserviceCap", System.Data.SqlDbType.Real, 4, "InserviceCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InserviceStmCap", System.Data.SqlDbType.Real, 4, "InserviceStmCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ModePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "ModePcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnusedCapPcnt", System.Data.SqlDbType.Real, 4, "UnusedCapPcnt"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCap", System.Data.SqlDbType.Real, 4, "RptCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCapUOM", System.Data.SqlDbType.VarChar, 20, "RptCapUOM"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptStmCap", System.Data.SqlDbType.Real, 4, "RptStmCap"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptStmCapUOM", System.Data.SqlDbType.VarChar, 20, "RptStmCapUOM"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DesignFeedSulfur", System.Data.SqlDbType.Real, 4, "DesignFeedSulfur"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyPcnt", System.Data.SqlDbType.Real, 4, "EnergyPcnt"))
        '
        'SqlUpdateCommand16
        '
        Me.SqlUpdateCommand16.CommandText = "UPDATE dbo.Config SET SubmissionID = @SubmissionID, UnitID = @UnitID, SortKey = @" & _
        "SortKey, ProcessID = @ProcessID, UnitName = @UnitName, ProcessType = @ProcessTyp" & _
        "e, Cap = @Cap, UtilPcnt = @UtilPcnt, StmCap = @StmCap, StmUtilPcnt = @StmUtilPcn" & _
        "t, InServicePcnt = @InServicePcnt, YearsOper = @YearsOper, MHPerWeek = @MHPerWee" & _
        "k, PostPerShift = @PostPerShift, BlockOp = @BlockOp, ControlType = @ControlType," & _
        " CapClass = @CapClass, UtilCap = @UtilCap, StmUtilCap = @StmUtilCap, AllocPcntOf" & _
        "Time = @AllocPcntOfTime, AllocPcntOfCap = @AllocPcntOfCap, AllocUtilPcnt = @Allo" & _
        "cUtilPcnt, ActualCap = @ActualCap, ActualStmCap = @ActualStmCap, InserviceCap = " & _
        "@InserviceCap, InserviceStmCap = @InserviceStmCap, ModePcnt = @ModePcnt, UnusedC" & _
        "apPcnt = @UnusedCapPcnt, RptCap = @RptCap, RptCapUOM = @RptCapUOM, RptStmCap = @" & _
        "RptStmCap, RptStmCapUOM = @RptStmCapUOM, DesignFeedSulfur = @DesignFeedSulfur, E" & _
        "nergyPcnt = @EnergyPcnt WHERE (SubmissionID = @Original_SubmissionID) AND (UnitI" & _
        "D = @Original_UnitID) AND (ActualCap = @Original_ActualCap OR @Original_ActualCa" & _
        "p IS NULL AND ActualCap IS NULL) AND (ActualStmCap = @Original_ActualStmCap OR @" & _
        "Original_ActualStmCap IS NULL AND ActualStmCap IS NULL) AND (AllocPcntOfCap = @O" & _
        "riginal_AllocPcntOfCap OR @Original_AllocPcntOfCap IS NULL AND AllocPcntOfCap IS" & _
        " NULL) AND (AllocPcntOfTime = @Original_AllocPcntOfTime OR @Original_AllocPcntOf" & _
        "Time IS NULL AND AllocPcntOfTime IS NULL) AND (AllocUtilPcnt = @Original_AllocUt" & _
        "ilPcnt OR @Original_AllocUtilPcnt IS NULL AND AllocUtilPcnt IS NULL) AND (BlockO" & _
        "p = @Original_BlockOp OR @Original_BlockOp IS NULL AND BlockOp IS NULL) AND (Cap" & _
        " = @Original_Cap OR @Original_Cap IS NULL AND Cap IS NULL) AND (CapClass = @Orig" & _
        "inal_CapClass OR @Original_CapClass IS NULL AND CapClass IS NULL) AND (ControlTy" & _
        "pe = @Original_ControlType OR @Original_ControlType IS NULL AND ControlType IS N" & _
        "ULL) AND (DesignFeedSulfur = @Original_DesignFeedSulfur OR @Original_DesignFeedS" & _
        "ulfur IS NULL AND DesignFeedSulfur IS NULL) AND (EnergyPcnt = @Original_EnergyPc" & _
        "nt OR @Original_EnergyPcnt IS NULL AND EnergyPcnt IS NULL) AND (InServicePcnt = " & _
        "@Original_InServicePcnt OR @Original_InServicePcnt IS NULL AND InServicePcnt IS " & _
        "NULL) AND (InserviceCap = @Original_InserviceCap OR @Original_InserviceCap IS NU" & _
        "LL AND InserviceCap IS NULL) AND (InserviceStmCap = @Original_InserviceStmCap OR" & _
        " @Original_InserviceStmCap IS NULL AND InserviceStmCap IS NULL) AND (MHPerWeek =" & _
        " @Original_MHPerWeek OR @Original_MHPerWeek IS NULL AND MHPerWeek IS NULL) AND (" & _
        "ModePcnt = @Original_ModePcnt OR @Original_ModePcnt IS NULL AND ModePcnt IS NULL" & _
        ") AND (PostPerShift = @Original_PostPerShift OR @Original_PostPerShift IS NULL A" & _
        "ND PostPerShift IS NULL) AND (ProcessID = @Original_ProcessID) AND (ProcessType " & _
        "= @Original_ProcessType) AND (RptCap = @Original_RptCap OR @Original_RptCap IS N" & _
        "ULL AND RptCap IS NULL) AND (RptCapUOM = @Original_RptCapUOM OR @Original_RptCap" & _
        "UOM IS NULL AND RptCapUOM IS NULL) AND (RptStmCap = @Original_RptStmCap OR @Orig" & _
        "inal_RptStmCap IS NULL AND RptStmCap IS NULL) AND (RptStmCapUOM = @Original_RptS" & _
        "tmCapUOM OR @Original_RptStmCapUOM IS NULL AND RptStmCapUOM IS NULL) AND (SortKe" & _
        "y = @Original_SortKey) AND (StmCap = @Original_StmCap OR @Original_StmCap IS NUL" & _
        "L AND StmCap IS NULL) AND (StmUtilCap = @Original_StmUtilCap OR @Original_StmUti" & _
        "lCap IS NULL AND StmUtilCap IS NULL) AND (StmUtilPcnt = @Original_StmUtilPcnt OR" & _
        " @Original_StmUtilPcnt IS NULL AND StmUtilPcnt IS NULL) AND (UnitName = @Origina" & _
        "l_UnitName) AND (UnusedCapPcnt = @Original_UnusedCapPcnt OR @Original_UnusedCapP" & _
        "cnt IS NULL AND UnusedCapPcnt IS NULL) AND (UtilCap = @Original_UtilCap OR @Orig" & _
        "inal_UtilCap IS NULL AND UtilCap IS NULL) AND (UtilPcnt = @Original_UtilPcnt OR " & _
        "@Original_UtilPcnt IS NULL AND UtilPcnt IS NULL) AND (YearsOper = @Original_Year" & _
        "sOper OR @Original_YearsOper IS NULL AND YearsOper IS NULL); SELECT SubmissionID" & _
        ", UnitID, SortKey, ProcessID, UnitName, ProcessType, Cap, UtilPcnt, StmCap, StmU" & _
        "tilPcnt, InServicePcnt, YearsOper, MHPerWeek, PostPerShift, BlockOp, ControlType" & _
        ", CapClass, UtilCap, StmUtilCap, AllocPcntOfTime, AllocPcntOfCap, AllocUtilPcnt," & _
        " ActualCap, ActualStmCap, InserviceCap, InserviceStmCap, ModePcnt, UnusedCapPcnt" & _
        ", RptCap, RptCapUOM, RptStmCap, RptStmCapUOM, DesignFeedSulfur, EnergyPcnt FROM " & _
        "dbo.Config WHERE (SubmissionID = @SubmissionID) AND (UnitID = @UnitID)"
        Me.SqlUpdateCommand16.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubmissionID", System.Data.SqlDbType.Int, 4, "SubmissionID"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitID", System.Data.SqlDbType.Int, 4, "UnitID"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SortKey", System.Data.SqlDbType.SmallInt, 2, "SortKey"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessID", System.Data.SqlDbType.VarChar, 8, "ProcessID"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnitName", System.Data.SqlDbType.VarChar, 30, "UnitName"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProcessType", System.Data.SqlDbType.VarChar, 4, "ProcessType"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Cap", System.Data.SqlDbType.Real, 4, "Cap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UtilPcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StmCap", System.Data.SqlDbType.Real, 4, "StmCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StmUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "StmUtilPcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InServicePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "InServicePcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@YearsOper", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "YearsOper", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MHPerWeek", System.Data.SqlDbType.Real, 4, "MHPerWeek"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PostPerShift", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(9, Byte), CType(3, Byte), "PostPerShift", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BlockOp", System.Data.SqlDbType.VarChar, 6, "BlockOp"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ControlType", System.Data.SqlDbType.VarChar, 4, "ControlType"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CapClass", System.Data.SqlDbType.TinyInt, 1, "CapClass"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UtilCap", System.Data.SqlDbType.Real, 4, "UtilCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StmUtilCap", System.Data.SqlDbType.Real, 4, "StmUtilCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AllocPcntOfTime", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfTime", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AllocPcntOfCap", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfCap", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AllocUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocUtilPcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ActualCap", System.Data.SqlDbType.Real, 4, "ActualCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ActualStmCap", System.Data.SqlDbType.Real, 4, "ActualStmCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InserviceCap", System.Data.SqlDbType.Real, 4, "InserviceCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InserviceStmCap", System.Data.SqlDbType.Real, 4, "InserviceStmCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ModePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "ModePcnt", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UnusedCapPcnt", System.Data.SqlDbType.Real, 4, "UnusedCapPcnt"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCap", System.Data.SqlDbType.Real, 4, "RptCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptCapUOM", System.Data.SqlDbType.VarChar, 20, "RptCapUOM"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptStmCap", System.Data.SqlDbType.Real, 4, "RptStmCap"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RptStmCapUOM", System.Data.SqlDbType.VarChar, 20, "RptStmCapUOM"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DesignFeedSulfur", System.Data.SqlDbType.Real, 4, "DesignFeedSulfur"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EnergyPcnt", System.Data.SqlDbType.Real, 4, "EnergyPcnt"))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ActualCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ActualCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ActualStmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ActualStmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AllocPcntOfCap", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AllocPcntOfTime", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfTime", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AllocUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocUtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BlockOp", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BlockOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CapClass", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CapClass", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ControlType", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ControlType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DesignFeedSulfur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DesignFeedSulfur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InServicePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "InServicePcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InserviceCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InserviceCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InserviceStmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InserviceStmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MHPerWeek", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MHPerWeek", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ModePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "ModePcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PostPerShift", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(9, Byte), CType(3, Byte), "PostPerShift", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessType", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCapUOM", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCapUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptStmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptStmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptStmCapUOM", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptStmCapUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StmUtilCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StmUtilCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StmUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "StmUtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitName", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnusedCapPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnusedCapPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UtilCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UtilCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_YearsOper", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "YearsOper", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand16
        '
        Me.SqlDeleteCommand16.CommandText = "DELETE FROM dbo.Config WHERE (SubmissionID = @Original_SubmissionID) AND (UnitID " & _
        "= @Original_UnitID) AND (ActualCap = @Original_ActualCap OR @Original_ActualCap " & _
        "IS NULL AND ActualCap IS NULL) AND (ActualStmCap = @Original_ActualStmCap OR @Or" & _
        "iginal_ActualStmCap IS NULL AND ActualStmCap IS NULL) AND (AllocPcntOfCap = @Ori" & _
        "ginal_AllocPcntOfCap OR @Original_AllocPcntOfCap IS NULL AND AllocPcntOfCap IS N" & _
        "ULL) AND (AllocPcntOfTime = @Original_AllocPcntOfTime OR @Original_AllocPcntOfTi" & _
        "me IS NULL AND AllocPcntOfTime IS NULL) AND (AllocUtilPcnt = @Original_AllocUtil" & _
        "Pcnt OR @Original_AllocUtilPcnt IS NULL AND AllocUtilPcnt IS NULL) AND (BlockOp " & _
        "= @Original_BlockOp OR @Original_BlockOp IS NULL AND BlockOp IS NULL) AND (Cap =" & _
        " @Original_Cap OR @Original_Cap IS NULL AND Cap IS NULL) AND (CapClass = @Origin" & _
        "al_CapClass OR @Original_CapClass IS NULL AND CapClass IS NULL) AND (ControlType" & _
        " = @Original_ControlType OR @Original_ControlType IS NULL AND ControlType IS NUL" & _
        "L) AND (DesignFeedSulfur = @Original_DesignFeedSulfur OR @Original_DesignFeedSul" & _
        "fur IS NULL AND DesignFeedSulfur IS NULL) AND (EnergyPcnt = @Original_EnergyPcnt" & _
        " OR @Original_EnergyPcnt IS NULL AND EnergyPcnt IS NULL) AND (InServicePcnt = @O" & _
        "riginal_InServicePcnt OR @Original_InServicePcnt IS NULL AND InServicePcnt IS NU" & _
        "LL) AND (InserviceCap = @Original_InserviceCap OR @Original_InserviceCap IS NULL" & _
        " AND InserviceCap IS NULL) AND (InserviceStmCap = @Original_InserviceStmCap OR @" & _
        "Original_InserviceStmCap IS NULL AND InserviceStmCap IS NULL) AND (MHPerWeek = @" & _
        "Original_MHPerWeek OR @Original_MHPerWeek IS NULL AND MHPerWeek IS NULL) AND (Mo" & _
        "dePcnt = @Original_ModePcnt OR @Original_ModePcnt IS NULL AND ModePcnt IS NULL) " & _
        "AND (PostPerShift = @Original_PostPerShift OR @Original_PostPerShift IS NULL AND" & _
        " PostPerShift IS NULL) AND (ProcessID = @Original_ProcessID) AND (ProcessType = " & _
        "@Original_ProcessType) AND (RptCap = @Original_RptCap OR @Original_RptCap IS NUL" & _
        "L AND RptCap IS NULL) AND (RptCapUOM = @Original_RptCapUOM OR @Original_RptCapUO" & _
        "M IS NULL AND RptCapUOM IS NULL) AND (RptStmCap = @Original_RptStmCap OR @Origin" & _
        "al_RptStmCap IS NULL AND RptStmCap IS NULL) AND (RptStmCapUOM = @Original_RptStm" & _
        "CapUOM OR @Original_RptStmCapUOM IS NULL AND RptStmCapUOM IS NULL) AND (SortKey " & _
        "= @Original_SortKey) AND (StmCap = @Original_StmCap OR @Original_StmCap IS NULL " & _
        "AND StmCap IS NULL) AND (StmUtilCap = @Original_StmUtilCap OR @Original_StmUtilC" & _
        "ap IS NULL AND StmUtilCap IS NULL) AND (StmUtilPcnt = @Original_StmUtilPcnt OR @" & _
        "Original_StmUtilPcnt IS NULL AND StmUtilPcnt IS NULL) AND (UnitName = @Original_" & _
        "UnitName) AND (UnusedCapPcnt = @Original_UnusedCapPcnt OR @Original_UnusedCapPcn" & _
        "t IS NULL AND UnusedCapPcnt IS NULL) AND (UtilCap = @Original_UtilCap OR @Origin" & _
        "al_UtilCap IS NULL AND UtilCap IS NULL) AND (UtilPcnt = @Original_UtilPcnt OR @O" & _
        "riginal_UtilPcnt IS NULL AND UtilPcnt IS NULL) AND (YearsOper = @Original_YearsO" & _
        "per OR @Original_YearsOper IS NULL AND YearsOper IS NULL)"
        Me.SqlDeleteCommand16.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubmissionID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubmissionID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ActualCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ActualCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ActualStmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ActualStmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AllocPcntOfCap", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AllocPcntOfTime", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocPcntOfTime", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AllocUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "AllocUtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BlockOp", System.Data.SqlDbType.VarChar, 6, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BlockOp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Cap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CapClass", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CapClass", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ControlType", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ControlType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_DesignFeedSulfur", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DesignFeedSulfur", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EnergyPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EnergyPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InServicePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "InServicePcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InserviceCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InserviceCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_InserviceStmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "InserviceStmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MHPerWeek", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MHPerWeek", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ModePcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "ModePcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PostPerShift", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(9, Byte), CType(3, Byte), "PostPerShift", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessID", System.Data.SqlDbType.VarChar, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProcessType", System.Data.SqlDbType.VarChar, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProcessType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptCapUOM", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptCapUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptStmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptStmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RptStmCapUOM", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RptStmCapUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SortKey", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SortKey", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StmCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StmCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StmUtilCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StmUtilCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StmUtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "StmUtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnitName", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnitName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UnusedCapPcnt", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UnusedCapPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UtilCap", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UtilCap", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UtilPcnt", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "UtilPcnt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_YearsOper", System.Data.SqlDbType.Decimal, 5, System.Data.ParameterDirection.Input, False, CType(5, Byte), CType(2, Byte), "YearsOper", System.Data.DataRowVersion.Original, Nothing))
        CType(Me.DsMaintCat1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region



#Region "Help Functions"
    'Private Function QueryDb(ByVal sqlString As String) As DataSet
    '    Dim ds As DataSet = New DataSet
    '    Dim sqlstmt As String
    '    ' Try
    '    sqlstmt = sqlString

    '    Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, SqlConnection1)
    '    da.Fill(ds)

    '    Return ds
    '    ' Catch ex As Exception

    '    'Throw New Exception("There was a database exception")
    '    ' End Try

    'End Function


    Sub changeOpexall2()

        Me.SqlUpdateCommand24.CommandText = "UPDATE dbo.OpExAll SET CatalystsFCC = @CatalystsFCC, CatalystsHYC = @CatalystsHYC" & _
        ", CatalystsNKSHYT = @CatalystsNKSHYT, CatalystsDHYT = @CatalystsDHYT, CatalystsV" & _
        "HYT = @CatalystsVHYT, CatalystsRHYT = @CatalystsRHYT, CatalystsHYFT = @Catalysts" & _
        "HYFT, CatalystsCDWax = @CatalystsCDWax, CatalystsREF = @CatalystsREF, CatalystsH" & _
        "YG = @CatalystsHYG, CatalystsS2Plant = @CatalystsS2Plant, CatalystsPetChem = @Ca" & _
        "talystsPetChem, Catalysts = @Catalysts, CatalystsOth = @CatalystsOth, PurOthN2 =" & _
        " @PurOthN2, PurOthH2O = @PurOthH2O, PurOthOth = @PurOthOth, PurOth = @PurOth, Ro" & _
        "yalties = @Royalties, EmissionsTaxes = @EmissionsTaxes, EmissionsPurch = @Emissi" & _
        "onsPurch, EmissionsCredits = @EmissionsCredits, OthVolOth = @OthVolOth, OthVol =" & _
        " @OthVol, GANonPers = @GANonPers, InvenCarry = @InvenCarry, Depreciation = @Depr" & _
        "eciation, Interest = @Interest, STNonCash = @STNonCash, TotRefExp = @TotRefExp, " & _
        "Cogen = @Cogen, OthRevenue = @OthRevenue, ThirdPartyTerminalRM = @ThirdPartyTerm" & _
        "inalRM, ThirdPartyTerminalProd = @ThirdPartyTerminalProd, POXO2 = @POXO2, PMAA =" & _
        " @PMAA, OthVolDemCrude = @OthVolDemCrude, OthVolDemLightering = @OthVolDemLighte" & _
        "ring, OthVolDemProd = @OthVolDemProd, STVol = @STVol, TotCashOpEx = @TotCashOpEx" & _
        ", ExclFireSafety = @ExclFireSafety, ExclEnvirFines = @ExclEnvirFines, ExclOth = " & _
        "@ExclOth, TotExpExcl = @TotExpExcl, STSal = @STSal, STBen = @STBen, PersCostExcl" & _
        "TA = @PersCostExclTA, PersCost = @PersCost, EnergyCost = @EnergyCost, NEOpex = @" & _
        "NEOpex WHERE  (SubmissionID = @SubmissionID);"



        Me.SqlUpdateCommand24.CommandText += "SELECT CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, Catal" & _
        "ystsDHYT, CatalystsVHYT, CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, Catalysts" & _
        "REF, CatalystsHYG, CatalystsS2Plant, CatalystsPetChem, Catalysts, CatalystsOth, " & _
        "PurOthN2, PurOthH2O, PurOthOth, PurOth, Royalties, EmissionsTaxes, EmissionsPurc" & _
        "h, EmissionsCredits, OthVolOth, OthVol, GANonPers, InvenCarry, Depreciation, Int" & _
        "erest, STNonCash, TotRefExp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdParty" & _
        "TerminalProd, POXO2, PMAA, OthVolDemCrude, OthVolDemLightering, OthVolDemProd, S" & _
        "TVol, TotCashOpEx, ExclFireSafety, ExclEnvirFines, ExclOth, TotExpExcl, STSal, S" & _
        "TBen, PersCostExclTA, PersCost, EnergyCost, NEOpex, Currency, DataType, Scenario" & _
        ", SubmissionID FROM dbo.OpExAll WHERE (SubmissionID = @SubmissionID);"
    End Sub
#End Region

#Region "SubmitRefineryData"
    '<summary>
    '   Submit dataset into the database
    '</summary>
    ' <param name="ds">Dataset to submit</param>
    <WebMethod()> _
    Public Sub SubmitRefineryData(ByVal ds As DataSet)


        ''
        '' Retrieve the response's soap context
        ''
        Dim responseContext As SoapContext = ResponseSoapContext.Current

        ''
        '' Disable timestamp
        ''
        ' RequestSoapContext.Current.Security.Timestamp.TtlInSeconds = -1
        responseContext.Security.Timestamp.TtlInSeconds = -1

        Dim refineryId As String = GetRefineryID(RequestSoapContext.Current)

        Dim errpoint As String
        Dim selectRow As DataRow
        Dim t, g, v As Integer
        Dim aMod As Boolean = False
        Dim id As Integer
        Dim currency, scenario, aDataset As String
        Dim dsProfile As New ProfileFuel
        Dim dcol As DataColumn
        'Get data from database
        'sdSubmissions.Fill(dsProfile)
        Dim sd As DateTime
        Dim rows() As DataRow
        'Dim rows() As DataRow = dsProfile.Submissions.Select("PeriodYear='" + ds.Tables("Settings").Rows(0)("PeriodYear").ToString + _
        '                                             "' AND PeriodMonth='" + ds.Tables("Settings").Rows(0)("PeriodMonth").ToString + _
        '                                             "' AND RefineryID='" + refineryId.Trim + "'")


        'If rows.Length > 0 Then
        '    id = rows(0)("SubmissionID")
        '    rows(0)("RefineryID") = refineryId
        '    If IsDBNull(ds.Tables("Settings").Rows(0)("DataSet")) Then
        '        rows(0)("DataSet") = "ACTUAL"
        '    Else
        '        If DeNull(ds.Tables("Settings").Rows(0), "DataSet").ToString.Trim.Length > 0 Then
        '            rows(0)("DataSet") = DeNull(ds.Tables("Settings").Rows(0), "DataSet")
        '        Else
        '            rows(0)("DataSet") = "ACTUAL"
        '        End If

        '    End If

        '    aDataset = rows(0)("Dataset")
        '    rows(0)("RptCurrency") = DeNull(ds.Tables("Settings").Rows(0), "RptCurrency")
        '    currency = DeNull(ds.Tables("Settings").Rows(0), "RptCurrency")

        '    If Not IsDBNull(ds.Tables("Settings").Rows(0)("RptCurrencyT15")) Then
        '        'Throw New Exception("Not null" + ds.Tables("Settings").Rows(0)("RptCurrencyT15").ToString())
        '        rows(0)("RptCurrencyT15") = ds.Tables("Settings").Rows(0)("RptCurrencyT15").ToString()
        '    Else
        '        'Throw New Exception("Yes I am null" + ds.Tables("Settings").Rows(0)("RptCurrencyT15").ToString())
        '        rows(0)("RptCurrencyT15") = currency
        '    End If

        '    rows(0)("PeriodYear") = DeNull(ds.Tables("Settings").Rows(0), "PeriodYear")
        '    rows(0)("PeriodMonth") = DeNull(ds.Tables("Settings").Rows(0), "PeriodMonth")
        '    rows(0)("Company") = DeNull(ds.Tables("Settings").Rows(0), "Company")
        '    rows(0)("Location") = DeNull(ds.Tables("Settings").Rows(0), "Location")
        '    rows(0)("CoordName") = DeNull(ds.Tables("Settings").Rows(0), "CoordName")
        '    rows(0)("CoordTitle") = DeNull(ds.Tables("Settings").Rows(0), "CoordTitle")
        '    rows(0)("CoordPhone") = DeNull(ds.Tables("Settings").Rows(0), "CoordPhone")
        '    rows(0)("CoordEMail") = DeNull(ds.Tables("Settings").Rows(0), "CoordEmail")

        '    If ds.Tables("Settings").Columns.Contains("BridgeVersion") Then
        '        rows(0)("BridgeVersion") = DeNull(ds.Tables("Settings").Rows(0), "BridgeVersion")
        '    End If

        '    If ds.Tables("Settings").Columns.Contains("ClientVersion") Then
        '        rows(0)("ClientVersion") = DeNull(ds.Tables("Settings").Rows(0), "ClientVersion")
        '    End If

        '    If ds.Tables("Settings").Rows(0)("UOM").StartsWith("US") Then
        '        rows(0)("UOM") = "US"
        '    Else
        '        rows(0)("UOM") = "MET"
        '    End If


        'Else

        '    Dim row As ProfileFuel.SubmissionsRow = dsProfile.Submissions.NewSubmissionsRow
        '    row.RefineryID = refineryId
        '    If IsDBNull(ds.Tables("Settings").Rows(0)("DataSet")) Then
        '        row.DataSet = "ACTUAL"
        '    Else
        '        If DeNull(ds.Tables("Settings").Rows(0), "DataSet").ToString.Trim.Length > 0 Then
        '            row.DataSet = DeNull(ds.Tables("Settings").Rows(0), "DataSet")
        '        Else
        '            row.DataSet = "ACTUAL"
        '        End If

        '    End If
        '    aDataset = row.DataSet
        '    row.RptCurrency = DeNull(ds.Tables("Settings").Rows(0), "RptCurrency")
        '    currency = CType(row.RptCurrency, String)

        '    If Not IsDBNull(ds.Tables("Settings").Rows(0)("RptCurrencyT15")) Then
        '        row.RptCurrencyT15 = ds.Tables("Settings").Rows(0)("RptCurrencyT15")
        '    Else
        '        row.RptCurrencyT15 = currency
        '    End If


        '    row.PeriodYear = DeNull(ds.Tables("Settings").Rows(0), "PeriodYear")
        '    row.PeriodMonth = DeNull(ds.Tables("Settings").Rows(0), "PeriodMonth")
        '    row.Company = DeNull(ds.Tables("Settings").Rows(0), "Company")
        '    row.Location = DeNull(ds.Tables("Settings").Rows(0), "Location")
        '    row.CoordName = DeNull(ds.Tables("Settings").Rows(0), "CoordName")
        '    row.CoordTitle = DeNull(ds.Tables("Settings").Rows(0), "CoordTitle")
        '    row.CoordPhone = DeNull(ds.Tables("Settings").Rows(0), "CoordPhone")
        '    row.CoordEMail = DeNull(ds.Tables("Settings").Rows(0), "CoordEmail")

        '    If ds.Tables("Settings").Columns.Contains("BridgeVersion") Then
        '        row.BridgeVersion = DeNull(ds.Tables("Settings").Rows(0), "BridgeVersion")
        '    End If

        '    If ds.Tables("Settings").Columns.Contains("ClientVersion") Then
        '        row.ClientVersion = DeNull(ds.Tables("Settings").Rows(0), "ClientVersion")
        '    End If

        '    If ds.Tables("Settings").Rows(0)("UOM").StartsWith("US") Then
        '        row.UOM = "US"
        '    Else
        '        row.UOM = "MET"
        '    End If


        '    dsProfile.Submissions.AddSubmissionsRow(row)
        'End If

        'sdSubmissions.Update(dsProfile)


        'If id = 0 Then
        '    sdSubmissions.Fill(dsProfile, "Submissions")
        '    id = dsProfile.Submissions(dsProfile.Submissions.Count - 1)("SubmissionId")
        'End If

        Dim a As Integer
        'Get or insert submission id

        sqlLoadSubmission.Connection.Open()
        sqlLoadSubmission.Parameters("@RefineryID").Value = refineryId
        sqlLoadSubmission.Parameters("@PeriodYear").Value = ds.Tables("Settings").Rows(0)("PeriodYear").ToString()
        sqlLoadSubmission.Parameters("@PeriodMonth").Value = ds.Tables("Settings").Rows(0)("PeriodMonth").ToString()
        sqlLoadSubmission.Parameters("@Company").Value = DeNull(ds.Tables("Settings").Rows(0), "Company")

        sqlLoadSubmission.Parameters("@Location").Value = DeNull(ds.Tables("Settings").Rows(0), "Location")
        sqlLoadSubmission.Parameters("@CoordName").Value = DeNull(ds.Tables("Settings").Rows(0), "CoordName")
        sqlLoadSubmission.Parameters("@CoordTitle").Value = DeNull(ds.Tables("Settings").Rows(0), "CoordTitle")
        sqlLoadSubmission.Parameters("@CoordPhone").Value = DeNull(ds.Tables("Settings").Rows(0), "CoordPhone")
        sqlLoadSubmission.Parameters("@CoordEMail").Value = DeNull(ds.Tables("Settings").Rows(0), "CoordEmail")

        If ds.Tables("Settings").Columns.Contains("ClientVersion") Then
            sqlLoadSubmission.Parameters("@ClientVersion").Value = DeNull(ds.Tables("Settings").Rows(0), "ClientVersion")
        End If

        If ds.Tables("Settings").Columns.Contains("BridgeVersion") Then
            sqlLoadSubmission.Parameters("@BridgeVersion").Value = DeNull(ds.Tables("Settings").Rows(0), "BridgeVersion")
        End If

        If ds.Tables("Settings").Rows(0)("UOM").StartsWith("US") Then
            sqlLoadSubmission.Parameters("@UOM").Value = "US"
        Else
            sqlLoadSubmission.Parameters("@UOM").Value = "MET"
        End If


        'If IsDBNull(ds.Tables("Settings").Rows(0)("DataSet")) Then
        '    sqlLoadSubmission.Parameters("@DataSet").Value = "ACTUAL"
        'Else
        '    If DeNull(ds.Tables("Settings").Rows(0), "DataSet").ToString.Trim.Length > 0 Then
        '        sqlLoadSubmission.Parameters("@DataSet").Value = DeNull(ds.Tables("Settings").Rows(0), "DataSet")
        '    Else
        '        sqlLoadSubmission.Parameters("@DataSet").Value = "ACTUAL"
        '    End If

        'End If

        sqlLoadSubmission.Parameters("@DataSet").Value = DeNull(ds.Tables("Settings").Rows(0), "DataSet")
        aDataset = sqlLoadSubmission.Parameters("@DataSet").Value

        sqlLoadSubmission.Parameters("@RptCurrency").Value = DeNull(ds.Tables("Settings").Rows(0), "RptCurrency")
        currency = CType(sqlLoadSubmission.Parameters("@RptCurrency").Value, String)

        sqlLoadSubmission.Parameters("@RptCurrencyT15").Value = ds.Tables("Settings").Rows(0)("RptCurrencyT15")



        sqlLoadSubmission.ExecuteNonQuery()
        id = sqlLoadSubmission.Parameters("@SubmissionID").Value
        sqlLoadSubmission.Connection.Close()
        errpoint = "Passed LoadSubmission"

        'Check to see if calc is running
        Dim isOK As Boolean
        scStartUpload.Connection.Open()
        scStartUpload.Parameters("@RefineryID").Value = refineryId
        scStartUpload.Parameters("@DataSet").Value = aDataset
        scStartUpload.ExecuteNonQuery()
        isOK = scStartUpload.Parameters("@OK").Value
        scStartUpload.Connection.Close()

        If Not isOK Then
            Throw New ProfileExceptions.CalcsAlreadyRunningException("Cannot upload month because results are currently being " + _
                                                                     "calculated for your refinery.  Please wait a few minutes " + _
                                                                     "and try again.")
        End If

        'Throw New Exception("Passed Config. Submission ID is " + id)
        'Delete rows with given submission id
        scPrepareForUpload.Connection.Open()
        scPrepareForUpload.Parameters("@SubmissionID").Value = id
        scPrepareForUpload.ExecuteNonQuery()
        scPrepareForUpload.Connection.Close()
        errpoint = "Passed scPrepareForUpload"

        Try

            'LoadData(sdConfig, dsProfile, "Config", id.ToString, refineryId)


            For t = 0 To ds.Tables("Config").Rows.Count - 1
                Dim newrow As ProfileFuel.ConfigRow = dsProfile.Config.NewConfigRow
                AssignData(newrow, ds.Tables("Config").Rows(t))

                newrow.SubmissionID = id

                dsProfile.Config.AddConfigRow(newrow)
            Next

            sdConfig.Update(dsProfile)




            Dim errmsg As String
            'Get data from database

            'LoadData(sdConfigRS, dsProfile, "ConfigRS", id.ToString, refineryId)

            For t = 0 To ds.Tables("ConfigRS").Rows.Count - 1
                For g = 0 To ds.Tables("ConfigRS").Columns.Count - 1

                    If ds.Tables("ConfigRS").Columns(g).ColumnName.ToUpper.Trim <> "PROCESSID" Then
                        Dim newrow As ProfileFuel.ConfigRSRow = dsProfile.ConfigRS.NewConfigRSRow
                        Dim unitEnum As Object
                        newrow.SubmissionID = id
                        newrow.ProcessID = DeNull(ds.Tables("ConfigRS").Rows(t), "ProcessID")
                        newrow.UnitID = 0
                        If newrow.ProcessID.ToUpper.TRIM = "RSCRUDE" Then
                            Dim crude As RSCRUDE
                            unitEnum = crude
                        Else
                            Dim prod As RSPROD
                            unitEnum = prod
                        End If

                        Select Case (ds.Tables("ConfigRS").Columns(g).ColumnName.ToUpper.Trim)
                            Case "RAILCARBBL"
                                newrow.ProcessType = "RAIL"
                                newrow.Throughput = DeNull(ds.Tables("ConfigRS").Rows(t), "RailcarBBL")
                                newrow.UnitID = unitEnum.RAIL
                            Case "TANKTRUCKBBL"
                                newrow.ProcessType = "TT"
                                newrow.Throughput = DeNull(ds.Tables("ConfigRS").Rows(t), "TankTruckBBL")
                                newrow.UnitID = unitEnum.TT
                            Case "TANKERBERTHBBL"
                                newrow.ProcessType = "TB"
                                newrow.Throughput = DeNull(ds.Tables("ConfigRS").Rows(t), "TankerBerthBBL")
                                newrow.UnitID = unitEnum.TB
                            Case "OFFSHOREBUOYBBL"
                                newrow.ProcessType = "OMB"
                                newrow.Throughput = DeNull(ds.Tables("ConfigRS").Rows(t), "OffShoreBuoyBBL")
                                newrow.UnitID = unitEnum.OMB
                            Case "BARGEBERTHBBL"
                                newrow.ProcessType = "BB"
                                newrow.Throughput = DeNull(ds.Tables("ConfigRS").Rows(t), "BargeBerthBBL")
                                newrow.UnitID = unitEnum.BB
                            Case "PIPELINEBBL"
                                newrow.ProcessType = "PL"
                                newrow.Throughput = DeNull(ds.Tables("ConfigRS").Rows(t), "PipelineBBL")
                                newrow.UnitID = unitEnum.PL
                        End Select

                        If newrow.UnitID <> 0 Then
                            dsProfile.ConfigRS.AddConfigRSRow(newrow)
                        Else
                            newrow = Nothing

                        End If

                    End If
                Next
            Next
            sdConfigRS.Update(dsProfile)


            'LoadData(sdInventory, dsProfile, "Inventory", id.ToString, refineryId)
            errpoint = "Passed sdConfigRS"

            If ds.Tables.Contains("Inventory") Then
                For t = 0 To ds.Tables("Inventory").Rows.Count - 1
                    Dim newrow As ProfileFuel.InventoryRow = dsProfile.Inventory.NewInventoryRow
                    Dim sortKey As Integer
                    AssignData(newrow, ds.Tables("Inventory").Rows(t))

                    newrow.SubmissionID = id

                    Select Case newrow.TankType.ToUpper.Trim
                        Case "CRD"
                            sortKey = 1
                        Case "DST"
                            sortKey = 5
                        Case "FIN"
                            sortKey = 3
                        Case "GAS"
                            sortKey = 4
                        Case "INT"
                            sortKey = 2
                        Case "NHC"
                            sortKey = 10
                        Case "OFP"
                            sortKey = 6
                    End Select

                    newrow.SortKey = sortKey

                    dsProfile.Inventory.AddInventoryRow(newrow)
                Next
                sdInventory.Update(dsProfile)
                errpoint = "Passed sdInventory"
            End If
            'LoadData(sdProcessData, dsProfile, "ProcessData", id.ToString, refineryId)

            For t = 0 To ds.Tables("ProcessData").Rows.Count - 1
                Dim newrow As ProfileFuel.ProcessDataRow = dsProfile.ProcessData.NewProcessDataRow
                If (Not ds.Tables("ProcessData").Rows(t).IsNull("Property") And _
                    Not ds.Tables("ProcessData").Rows(t).IsNull("UnitID")) Then

                    AssignData(newrow, ds.Tables("ProcessData").Rows(t))


                    newrow.SubmissionID = id
                    newrow._Property = DeNull(ds.Tables("ProcessData").Rows(t), "Property")
                    dsProfile.ProcessData.AddProcessDataRow(newrow)
                End If
            Next
            sdProcessData.Update(dsProfile)
            errpoint = "Passed sdProcessData"

            If ds.Tables.Contains("Opex") Then
                'LoadData(sdOpex, dsProfile, "OpEx", id.ToString, refineryId)

                For t = 0 To ds.Tables("OpEx").Rows.Count - 1
                    Dim newrow As ProfileFuel.OpExRow = dsProfile.OpEx.NewOpExRow
                    AssignData(newrow, ds.Tables("OpEx").Rows(t))

                    newrow.SubmissionID = id
                    newrow.DataType = "RPT"
                    newrow.Currency = currency
                    newrow.Scenario = ""

                    dsProfile.OpEx.AddOpExRow(newrow)
                Next


                sdOpex.Update(dsProfile)
            End If

            errpoint = "Passed sdOpex"
            'LoadData(sdPers, dsProfile, "Pers", id.ToString, refineryId)

            For t = 0 To ds.Tables("Pers").Rows.Count - 1
                Dim newrow As ProfileFuel.PersRow = dsProfile.Pers.NewPersRow
                AssignData(newrow, ds.Tables("Pers").Rows(t))

                newrow.SubmissionID = id

                dsProfile.Pers.AddPersRow(newrow)
            Next
            sdPers.Update(dsProfile)
            errpoint = "Passed sdPers"

            'LoadData(sdAbsence, dsProfile, "Absence", id.ToString, refineryId)
            If (ds.Tables.Contains("Absence")) Then
                For t = 0 To ds.Tables("Absence").Rows.Count - 1
                    Dim newrow As ProfileFuel.AbsenceRow = dsProfile.Absence.NewAbsenceRow
                    Dim sortKey As Integer
                    AssignData(newrow, ds.Tables("Absence").Rows(t))

                    newrow.SubmissionID = id

                    Select Case newrow.CategoryID.ToUpper.Trim
                        Case "DISC"
                            sortKey = 6
                        Case "EXC"
                            sortKey = 9
                        Case "HOL"
                            sortKey = 8
                        Case "JURY"
                            sortKey = 4
                        Case "MIL"
                            sortKey = 3
                        Case "ONJOB"
                            sortKey = 1
                        Case "OTH"
                            sortKey = 11
                        Case "SICK"
                            sortKey = 2
                        Case "UNEXC"
                            sortKey = 10
                        Case "UNION"
                            sortKey = 5
                        Case "VAC"
                            sortKey = 7
                    End Select

                    newrow.SortKey = sortKey

                    dsProfile.Absence.AddAbsenceRow(newrow)
                Next
                sdAbsence.Update(dsProfile)
                errpoint = "Passed sdAbsence"
            End If

            sdLoadTA.Fill(dsProfile)
            For t = 0 To ds.Tables("MaintTA").Rows.Count - 1

                If IsDBNull(ds.Tables("MaintTA").Rows(t)("TAID")) Then
                    Select Case (ds.Tables("MaintTA").Rows(t)("UnitID"))
                        Case 90051 To 90058
                            ds.Tables("MaintTA").Rows(t)("TAID") = 0
                    End Select
                End If

                If Not IsDBNull(ds.Tables("MaintTA").Rows(t)("TAID")) Then
                    rows = dsProfile.LoadTA.Select("TAID='" + ds.Tables("MaintTA").Rows(t)("TAID").ToString + "' AND " + _
                                                    "UnitID='" + ds.Tables("MaintTA").Rows(t)("UnitID").ToString + "' AND " + _
                                                    "RefineryID='" + refineryId + "' AND " + _
                                                    "DataSet='" + aDataset + "'")

                    'if the submission id exists delete it
                    If rows.Length > 0 Then
                        rows(0).Delete()
                    End If

                    Dim newrow As ProfileFuel.LoadTARow = dsProfile.LoadTA.NewLoadTARow
                    AssignData(newrow, ds.Tables("MaintTA").Rows(t))

                    newrow.RefineryID = refineryId


                    newrow.DataSet = aDataset
                    If Not IsDBNull(ds.Tables("MaintTA").Rows(t)!TADate) Then
                        newrow.TADate = ds.Tables("MaintTA").Rows(t)!TADate
                    End If

                    If Not IsDBNull(ds.Tables("MaintTA").Rows(t)!PrevTADate) Then
                        newrow.PrevTADate = ds.Tables("MaintTA").Rows(t)!PrevTADate
                    End If


                    If IsDBNull(ds.Tables("MaintTA").Rows(t)("TAID")) And (newrow.UnitID > 90050 And newrow.UnitID < 90059) Then
                        newrow.TAID = 0
                    End If
                    dsProfile.LoadTA.AddLoadTARow(newrow)

                End If

            Next
            sdLoadTA.Update(dsProfile)
            errpoint = "Passed sdLoadTA"
            'LoadData(sdMaintRout, dsProfile, "MaintRout", id.ToString, refineryId)
            For t = 0 To ds.Tables("MaintRout").Rows.Count - 1
                Dim newrow As ProfileFuel.MaintRoutRow = dsProfile.MaintRout.NewMaintRoutRow

                AssignData(newrow, ds.Tables("MaintRout").Rows(t))
                newrow.SubmissionID = id
                dsProfile.MaintRout.AddMaintRoutRow(newrow)
            Next
            sdMaintRout.Update(dsProfile)
            errpoint = "Passed sdMaintRout"


            'LoadData(sdCrude, dsProfile, "Crude", id.ToString, refineryId)
            For t = 0 To ds.Tables("Crude").Rows.Count - 1
                Dim newrow As ProfileFuel.CrudeRow = dsProfile.Crude.NewCrudeRow
                AssignData(newrow, ds.Tables("Crude").Rows(t))
                newrow.SubmissionID = id
                newrow.CrudeID = t + 1
                dsProfile.Crude.AddCrudeRow(newrow)
            Next
            sdCrude.Update(dsProfile)
            errpoint = "Passed sdCrude"


            Dim d As Integer
            sdMaterialCategory_LU.Fill(DsMaintCat1)

            Dim matCatDict As New HybridDictionary
            For t = 0 To DsMaintCat1.Tables(0).Rows.Count - 1
                matCatDict.Add(DsMaintCat1.Tables(0).Rows(t)("Category"), 100 * (t + 1))
            Next


            'LoadData(sdYield, dsProfile, "Yield", id.ToString, refineryId)
            For t = 0 To ds.Tables("Yield_RM").Rows.Count - 1
                Dim category As String = DeNull(ds.Tables("Yield_RM").Rows(t), "Category")
                Dim sortkey As Integer = matCatDict(category.Trim)
                matCatDict(category.Trim) = sortkey + 1

                Dim newrow As ProfileFuel.YieldRow = dsProfile.Yield.NewYieldRow
                AssignData(newrow, ds.Tables("Yield_RM").Rows(t))

                newrow.SubmissionID = id
                newrow.SortKey = sortkey
                newrow.Category = category
                dsProfile.Yield.AddYieldRow(newrow)
            Next

            matCatDict.Clear()
            For t = 0 To DsMaintCat1.Tables(0).Rows.Count - 1
                matCatDict.Add(DsMaintCat1.Tables(0).Rows(t)("Category").Trim, 100 * (t + 1))
            Next


            For t = 0 To ds.Tables("Yield_Prod").Rows.Count - 1
                Dim category As String = DeNull(ds.Tables("Yield_Prod").Rows(t), "Category")
                Dim sortkey As Integer = matCatDict(category.Trim)
                matCatDict(category.Trim) = sortkey + 1

                Dim newrow As ProfileFuel.YieldRow = dsProfile.Yield.NewYieldRow
                AssignData(newrow, ds.Tables("Yield_Prod").Rows(t))

                newrow.SubmissionID = id
                newrow.SortKey = sortkey
                newrow.Category = category
                dsProfile.Yield.AddYieldRow(newrow)
            Next
            sdYield.Update(dsProfile)
            errpoint = "Passed sdYield"


            'LoadData(sdEnergy, dsProfile, "Energy", id.ToString, refineryId)
            For t = 0 To ds.Tables("Energy").Rows.Count - 1
                Dim newrow As ProfileFuel.EnergyRow = dsProfile.Energy.NewEnergyRow
                AssignData(newrow, ds.Tables("Energy").Rows(t))
                newrow.SubmissionID = id
                newrow.TransCode = t + 1
                dsProfile.Energy.AddEnergyRow(newrow)
            Next
            sdEnergy.Update(dsProfile)
            errpoint = "Passed sdEnergy"

            'LoadData(sdElectric, dsProfile, "Electric", id.ToString, refineryId)
            For t = 0 To ds.Tables("Electric").Rows.Count - 1
                Dim newrow As ProfileFuel.ElectricRow = dsProfile.Electric.NewElectricRow
                AssignData(newrow, ds.Tables("Electric").Rows(t))
                newrow.SubmissionID = id
                newrow.TransCode = t + 1
                dsProfile.Electric.AddElectricRow(newrow)
            Next
            sdElectric.Update(dsProfile)
            errpoint = "Passed sdElectric"

            If ds.Tables.Contains("RefTargets") Then
                'LoadData(sdRefTargets, dsProfile, "RefTargets", id.ToString, refineryId)
                For t = 0 To ds.Tables("RefTargets").Rows.Count - 1
                    If (Not ds.Tables("RefTargets").Rows(t).IsNull("Property")) Then
                        Dim newrow As ProfileFuel.RefTargetsRow = dsProfile.RefTargets.NewRefTargetsRow
                        AssignData(newrow, ds.Tables("RefTargets").Rows(t))

                        newrow.SubmissionID = id
                        newrow._Property = DeNull(ds.Tables("RefTargets").Rows(t), "Property")

                        If newrow.CurrencyCode = String.Empty Then
                            newrow.CurrencyCode = "USD"
                        End If
                        dsProfile.RefTargets.AddRefTargetsRow(newrow)
                    End If
                Next
                sdRefTargets.Update(dsProfile)
                errpoint = "Passed sdRefTargets"
            End If
            'USE WITH NEW SK CORP AND NEW CLIENTS AFTER 12/5/2007
            If (ds.Tables.Contains("UnitTargetsNew")) Then
                'LoadData(sdUnitTargetsNew, dsProfile, "UnitTargetsNew", id.ToString, refineryId)


                For t = 0 To ds.Tables("UnitTargetsNew").Rows.Count - 1
                    Dim newrow As ProfileFuel.UnitTargetsNewRow = dsProfile.UnitTargetsNew.NewUnitTargetsNewRow
                    Dim checkRows() As DataRow = dsProfile.UnitTargetsNew.Select("Property='" + ds.Tables("UnitTargetsNew").Rows(t)("Property").ToString + "' AND UnitID='" + ds.Tables("UnitTargetsNew").Rows(t)("UnitID").ToString + "' AND SubmissionId='" + id.ToString + "'")
                    If checkRows.Length = 0 Then
                        If Not IsDBNull(ds.Tables("UnitTargetsNew").Rows(t)("Target")) Then

                            AssignData(newrow, ds.Tables("UnitTargetsNew").Rows(t))

                            newrow.SubmissionID = id
                            newrow._Property = DeNull(ds.Tables("UnitTargetsNew").Rows(t), "Property")



                            If newrow.CurrencyCode = String.Empty Then
                                newrow.CurrencyCode = "USD"
                            End If
                            dsProfile.UnitTargetsNew.AddUnitTargetsNewRow(newrow)
                        End If
                    End If
                Next
                sdUnitTargetsNew.Update(dsProfile)
            End If
            errpoint = "Passed sdUnitTargetsNew"


            If (ds.Tables.Contains("UnitTargets")) Then

                'LoadData(sdUnitTargets, dsProfile, "UnitTargets", id.ToString, refineryId)
                For t = 0 To ds.Tables("UnitTargets").Rows.Count - 1
                    Dim newrow As ProfileFuel.UnitTargetsRow = dsProfile.UnitTargets.NewUnitTargetsRow
                    Dim checkRows() As DataRow = dsProfile.UnitTargets.Select("UnitID='" + ds.Tables("UnitTargets").Rows(t)("UnitID").ToString + "' AND SubmissionId='" + id.ToString + "'")
                    If checkRows.Length = 0 Then
                        AssignData(newrow, ds.Tables("UnitTargets").Rows(t))

                        newrow.SubmissionID = id

                        If newrow.CurrencyCode = String.Empty Then
                            newrow.CurrencyCode = "USD"
                        End If
                        dsProfile.UnitTargets.AddUnitTargetsRow(newrow)
                    End If
                Next
                sdUnitTargets.Update(dsProfile)
            End If
            errpoint = "Passed sdUnitTargets"

            Dim periodStart As Date
            Dim dSet As String
            Dim k As Integer

            sdLoadRoutHist.Fill(dsProfile)

            currency = DeNull(ds.Tables("Settings").Rows(0), "RptCurrency")

            If ds.Tables.Contains("MaintRoutHist") Then
                For t = 0 To ds.Tables("MaintRoutHist").Rows.Count - 1

                    If Not IsDBNull(ds.Tables("MaintRoutHist").Rows(t)!PeriodStart) Then
                        periodStart = ds.Tables("MaintRoutHist").Rows(t)!PeriodStart

                        'if the submission id exists delete it
                        rows = dsProfile.LoadRoutHist.Select("RefineryID='" + refineryId + _
                                                              "' AND PeriodStart='" + periodStart + _
                                                              "' AND DataSet='" + aDataset + "'")

                        For k = 0 To rows.Length - 1
                            rows(k).Delete()
                        Next


                        Dim newrow As ProfileFuel.LoadRoutHistRow = dsProfile.LoadRoutHist.NewLoadRoutHistRow
                        AssignData(newrow, ds.Tables("MaintRoutHist").Rows(t))

                        newrow.RefineryID = refineryId
                        newrow.DataSet = aDataset
                        newrow.PeriodStart = periodStart


                        dsProfile.LoadRoutHist.AddLoadRoutHistRow(newrow)
                    End If
                Next
                sdLoadRoutHist.Update(dsProfile)
                errpoint = "Passed sdLoadRoutHist"
            End If
            'UserDefined
            If (ds.Tables.Contains("UserDefined")) Then

                'LoadData(sdUserDefined, dsProfile, "UserDefined", id.ToString, refineryId)

                For t = 0 To ds.Tables("UserDefined").Rows.Count - 1
                    Dim newrow As ProfileFuel.UserDefinedRow = dsProfile.UserDefined.NewUserDefinedRow

                    AssignData(newrow, ds.Tables("UserDefined").Rows(t))


                    newrow.SubmissionID = id

                    dsProfile.UserDefined.AddUserDefinedRow(newrow)
                Next
                sdUserDefined.Update(dsProfile)
            End If
            errpoint = "Passed sdUserDefined"

            'OpexAdd
            If (ds.Tables.Contains("OpexAdd")) Then

                'LoadData(sdOpexAdd, dsProfile, "OpexAdd", id.ToString, refineryId)


                For t = 0 To ds.Tables("OpexAdd").Rows.Count - 1
                    Dim newrow As ProfileFuel.OpexAddRow = dsProfile.OpexAdd.NewOpexAddRow
                    AssignData(newrow, ds.Tables("OpexAdd").Rows(t))


                    newrow.SubmissionID = id
                    newrow.Currency = currency
                    newrow.DataType = "RPT"
                    dsProfile.OpexAdd.AddOpexAddRow(newrow)
                Next
                sdOpexAdd.Update(dsProfile)
            End If

            errpoint = "Passed sdOpexAdd"


            Try
                'OpexAll1
                If (ds.Tables.Contains("OpexAll")) Then

                    'Delete rows with the given SubmissionId
                    'LoadData(sdOpexAll1, dsProfile, "OpexAll", id.ToString, refineryId)
                    'sdOpexAll2.Fill(dsProfile)
                    'LoadData(sdOpexAll2, dsProfile, "OpexAll", id.ToString, refineryId)
                    'Opex and OpexAdd share the same submissionid so just deleting 
                    ' rows according toOpex
                    ' LoadData(sdOpex, dsProfile, "Opex", id.ToString, refineryId)
                    'Throw New Exception("Inside OpexAll. The row count is " + ds.Tables("OpExAll").Rows.Count.ToString)

                    For t = 0 To ds.Tables("OpexAll").Rows.Count - 1
                        Dim newrow As ProfileFuel.OpExAllRow = dsProfile.OpExAll.NewOpExAllRow
                        AssignData(newrow, ds.Tables("OpexAll").Rows(t))

                        newrow.SubmissionID = id
                        newrow.Currency = currency
                        newrow.DataType = "RPT"
                        newrow.Scenario = String.Empty

                        dsProfile.OpExAll.AddOpExAllRow(newrow)
                    Next
                Else
                    'Delete old rows in OpexAll
                    'LoadData(sdOpexAll1, dsProfile, "OpexAll", id.ToString, refineryId)
                    'sdOpexAll2.Fill(dsProfile)
                    'Map Opex and OpexAdd into OpexAll to support legacy clients
                    For t = 0 To ds.Tables("Opex").Rows.Count - 1
                        Dim newrow As ProfileFuel.OpExAllRow = dsProfile.OpExAll.NewOpExAllRow

                        'If table OpexAdd exist assign fields in OpexAll also
                        If (ds.Tables.Contains("OpexAdd")) Then
                            'Number of OpexAdd should be one
                            If t < ds.Tables("OpexAdd").Rows.Count Then
                                AssignData(newrow, ds.Tables("OpexAdd").Rows(t))
                                newrow.InsurOth = DeNull(ds.Tables("OpexAdd").Rows(t), "Insur")
                            End If
                        End If

                        'Assign Opex fields in OpexAll
                        AssignData(newrow, ds.Tables("Opex").Rows(t))

                        newrow.SubmissionID = id
                        newrow.Currency = currency
                        newrow.DataType = "RPT"
                        newrow.ContMaintLaborST = DeNull(ds.Tables("Opex").Rows(t), "ContMaintLabor")
                        newrow.Scenario = String.Empty
                        newrow.MaintMatlST = DeNull(ds.Tables("Opex").Rows(t), "MaintMatl")


                        dsProfile.OpExAll.AddOpExAllRow(newrow)
                    Next
                End If
                'VS Studio 2003 could not handle the number of columns in 
                'OpexAll so they are split up into SqlDataAdapters: sdOpexAll1 
                'and sdOpexAll2

                sdOpexAll1.Update(dsProfile)



            Catch ex As Exception
                Throw New Exception("OPEXALL: " + ex.Message)
            End Try
            errpoint = "Passed sdOpexAll1"


            Try
                'OpexAll2
                If (ds.Tables.Contains("OpexAll")) Then
                    sdOpexAll2.Fill(dsProfile)

                    Dim dRows() As DataRow = dsProfile.OpExAll.Select("SubmissionID='" + id.ToString + "' and DataType='RPT' and Currency='" + currency + "'")

                    If (dRows.Length > 0) Then
                        For t = 0 To ds.Tables("OpexAll").Rows.Count - 1
                            AssignData(dRows(t), ds.Tables("OpexAll").Rows(t))

                        Next
                        changeOpexall2()
                        sdOpexAll2.Update(dRows)

                    End If

                Else 'Handle legacy app
                    sdOpexAll2.Fill(dsProfile)
                    Dim dRows() As DataRow = dsProfile.OpExAll.Select("SubmissionID='" + id.ToString + "' and DataType='RPT' and Currency='" + currency + "'")
                    If dRows.Length > 0 Then

                        'Map Opex and OpexAdd into OpexAll to support legacy clients
                        For t = 0 To ds.Tables("Opex").Rows.Count - 1
                            'If table OpexAdd exist assign fields in OpexAll also
                            If (ds.Tables.Contains("OpexAdd")) Then
                                'OpexAdd row count should be one
                                If t < ds.Tables("OpexAdd").Rows.Count Then
                                    AssignData(dRows(t), ds.Tables("OpexAdd").Rows(t))
                                End If
                            End If
                            'Assign Opex fields in OpexAll
                            AssignData(dRows(t), ds.Tables("Opex").Rows(t))
                        Next
                        changeOpexall2()
                        sdOpexAll2.Update(dRows)
                    End If
                End If


            Catch ex As Exception
                Throw New Exception("OPEXALL2: " + ex.Message)
            End Try
            errpoint = "Passed sdOpexAll2"



            'LoadEDCStabilizers
            If (ds.Tables.Contains("EDCStabilizers")) Then

                'LoadData(sdLoadEDCStabilizers, dsProfile, "LoadEDCStabilizers", id.ToString, refineryId)

                For t = 0 To ds.Tables("EDCStabilizers").Rows.Count - 1


                    Dim newrow As ProfileFuel.LoadEDCStabilizersRow = dsProfile.LoadEDCStabilizers.NewLoadEDCStabilizersRow
                    AssignData(newrow, ds.Tables("EDCStabilizers").Rows(t))

                    newrow.RefineryID = refineryId

                    'if (newrow.DataSet Is DBNull.Value) OrElse (newrow.DataSet = String.Empty) Then
                    If (Not ds.Tables("EDCStabilizers").Columns.Contains("DataSet")) Then
                        newrow.DataSet = "ACTUAL"
                    Else
                        If newrow.DataSet.ToUpper() = "CLIENT" Then
                            newrow.DataSet = "ACTUAL"
                        End If
                    End If


                    dsProfile.LoadEDCStabilizers.AddLoadEDCStabilizersRow(newrow)

                Next
                sdLoadEDCStabilizers.Update(dsProfile)
            End If
            errpoint = "Passed sdLoadEDCStabilizers"

        Catch exOutRange As ArgumentOutOfRangeException
            Throw New Exception(exOutRange.ActualValue & "is out of range and not valid. Please change this value and try again.")
        Catch exRunning As ProfileExceptions.CalcsAlreadyRunningException
            Throw exRunning
        Catch ex As Exception

            Dim sw As StreamWriter = New StreamWriter("C:\Inetpub\wwwroot\RefineryWS\Log\Log_" + refineryId + "_" + DateTime.Now.ToString("dd-MMM-yyyy h mm tt") + ".txt")
            ' Add some text to the file.

            sw.WriteLine("RefineryID: " + refineryId)
            sw.WriteLine(ex.ToString())
            sw.WriteLine(errpoint)
            sw.WriteLine(classErr)
            sw.WriteLine("-------------------")
            ' Arbitrary objects can also be written to the file.

            sw.Close()
            ds.WriteXml("C:\Inetpub\wwwroot\RefineryWS\Log\" & refineryId & "_input.txt")

            Throw New Exception(ex.Message)
        Finally
            'Change the recalc flag in the Submission table
            scCompleteSubmission.Connection.Open()
            scCompleteSubmission.Parameters("@SubmissionID").Value = id
            scCompleteSubmission.ExecuteNonQuery()
            scCompleteSubmission.Connection.Close()

        End Try

    End Sub
#End Region

#Region "GetDataByPeriod"


    '<summary>
    '   Returns refinery data for the given period
    '</summary>
    ' <param name="periodStart">Starting period range</param>
    ' <param name="periodEnd">Ending period range</param>
    '<returns>Dataset for the period given</returns>
    <WebMethod()> _
    Public Function GetDataByPeriod(ByVal periodStart As Date, ByVal periodEnd As Date) As DataSet
        '
        ' Retrieve the response's soap context
        '
        Dim responseContext As SoapContext = ResponseSoapContext.Current

        '
        ' Disable timestamp
        '
        responseContext.Security.Timestamp.TtlInSeconds = -1

        Dim refineryId As String = GetRefineryID(RequestSoapContext.Current)

        Dim ds As New DataSet

        Dim settings As New StringBuilder
        With settings
            .Append(" SELECT RTRIM(s.Company) AS Company,RTRIM(s.Location) AS Location, ")
            .Append(" RTRIM(CoordName) AS CoordName,RTRIM(CoordTitle) AS CoordTitle, ")
            .Append(" RTRIM(CoordPhone) AS CoordPhone,RTRIM(CoordEmail) AS CoordEmail, ")
            .Append(" RTRIM(RptCurrency) AS RptCurrency,RTRIM(RptCurrencyT15) AS RptCurrencyT15,(CASE RTRIM(UOM) ")
            .Append(" WHEN 'US' THEN 'US Units' WHEN 'MET' THEN 'Metric' END) AS UOM, ")
            .Append(" ' ' AS BridgeLocation,FuelsLubesCombo, ")
            .Append(" PeriodMonth,PeriodYear, ")
            .Append(" RTRIM(DataSet) AS DataSet, BridgeVersion, ClientVersion ")
            .Append(" FROM dbo.Submissions s,TSort t")
            .Append(" WHERE s.RefineryID='" + refineryId + "' AND s.RefineryID=t.RefineryID ")
            .Append(" AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '" + DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')")
        End With

        'Dim config As New StringBuilder
        'With config
        '    .Append("SELECT c.UnitID,RTRIM(c.ProcessID) as ProcessID,c.SortKey,RTRIM(c.UnitName) AS UnitName,RTRIM(c.ProcessType) AS ProcessType,c.RptCap, RTRIM(p.ProcessGroup) AS ProcessGroup,")
        '    .Append("c.UtilPcnt,c.RptStmCap,c.StmUtilPcnt,c.InServicePcnt,c.DesignFeedSulfur ")
        '    .Append(" FROM dbo.Config c,ProcessID_LU p WHERE  c.ProcessID=p.ProcessID AND  ")
        '    .Append(" p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND ")
        '    .Append("SubmissionID IN (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
        '    .Append(" WHERE RefineryID='" + refineryId + "' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '" + DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))")
        'End With

        Dim config As New StringBuilder
        With config
            .Append("SELECT c.UnitID,RTRIM(c.ProcessID) as ProcessID,c.SortKey,RTRIM(c.UnitName) AS UnitName,RTRIM(c.ProcessType) AS ProcessType,c.RptCap, RTRIM(p.ProcessGroup) AS ProcessGroup,")
            .Append("c.UtilPcnt,c.RptStmCap,c.StmUtilPcnt,c.InServicePcnt,c.DesignFeedSulfur,c.EnergyPcnt ")
            .Append(" FROM dbo.Config c,ProcessID_LU p WHERE  c.ProcessID=p.ProcessID AND  ")
            .Append(" p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND ")
            .Append("SubmissionID IN (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId + "' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '" + DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))")
        End With


        Dim configRS As New StringBuilder
        With configRS
            .Append("SELECT RTRIM(ProcessID) AS ProcessID,")
            .Append("SUM(CASE WHEN ProcessType = 'RAIL' THEN ISNULL(Throughput,0) END) AS RailcarBBL,")
            .Append("SUM(CASE WHEN ProcessType IN ('TT', 'TTGD', 'TTO') THEN ISNULL(Throughput,0) END) AS TankTruckBBL,")
            .Append("SUM(CASE WHEN ProcessType = 'TB' THEN ISNULL(Throughput,0) END) AS TankerBerthBBL,")
            .Append("SUM(CASE WHEN ProcessType = 'OMB' THEN ISNULL(Throughput,0) END) AS OffshoreBuoyBBL,")
            .Append("SUM(CASE WHEN ProcessType = 'BB' THEN ISNULL(Throughput,0) END) AS BargeBerthBBL,")
            .Append("SUM(CASE WHEN ProcessType = 'PL' THEN ISNULL(Throughput,0) END) AS PipelineBBL ")
            .Append("FROM ConfigRS ")
            .Append("WHERE SubmissionID IN ")
            .Append("(SELECT  SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')) ")
            .Append("GROUP BY ProcessID")
        End With


        Dim inventory As New StringBuilder
        With inventory
            .Append("SELECT TankType,NumTank,FuelsStorage,AvgLevel ")
            .Append(" FROM dbo.Inventory WHERE  SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))  ")
        End With

        Dim processData As New StringBuilder
        With processData
            .Append("SELECT pd.UnitID,pd.Property,pd.RptValue,t.SortKey AS SortKey, RTRIM(cfg.ProcessID) AS ProcessID, RTRIM(cfg.UnitName) AS UnitName")
            .Append(" FROM dbo.ProcessData pd, Config cfg, Table2_LU  t   WHERE   cfg.UnitID=pd.UnitID AND ")
            .Append(" cfg.SubmissionID = pd.SubmissionID AND t.ProcessID=cfg.ProcessID AND t.Property=pd.Property AND (pd.SubmissionID IN ")
            .Append(" (SELECT SubmissionID FROM dbo.Submissions ")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')))  ")
        End With

        Dim opEx As New StringBuilder
        With opEx
            .Append("SELECT ThirdPartyTerminalProd,ThirdPartyTerminalRM,OthRevenue,")
            .Append("OthNonVol,OthVol,PurOth,Catalysts,Chemicals,GAPers,Envir,OthCont,")
            .Append("ContMaintLabor,MaintMatl,MPSBen,OCCBen,MPSSal,OCCSal,POXO2")
            .Append(" FROM dbo.OpEx WHERE DataType='RPT' AND (SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')))  ")
        End With

        Dim pers As New StringBuilder
        With pers
            .Append("SELECT RTRIM(PersID) AS PersID,NumPers,AbsHrs,STH,OVTHours,OVTPcnt,Contract,GA,MaintPcnt")
            .Append(" FROM dbo.Pers WHERE  (SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND PersID NOT IN ('OCCTAADJ','MPSTAADJ','OCCTAEXCL','MPSTAEXCL','OCCTAEXC','MPSTAEXC') AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')))  ")
        End With


        Dim absence As New StringBuilder
        With absence
            .Append("SELECT OCCAbs,MPSAbs,RTRIM(CategoryID) As CategoryID ")
            .Append(" FROM dbo.Absence WHERE  (SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')))  ")
        End With

        Dim maintTA_Process As New StringBuilder
        With maintTA_Process
            .Append("SELECT mt.TAID,mt.UnitID,RTRIM(cfg.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown,")
            .Append("mt.TACostLocal,mt.TAMatlLocal,mt.TAOCCSTH,mt.TAOCCOVT,mt.TAMPSSTH,mt.TAMPSOVTPcnt,")
            .Append("cfg.SortKey,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate,")
            .Append("mt.TAExceptions,RTRIM(cfg.UnitName) AS UnitName ")
            .Append("FROM dbo.MaintTA mt, Config cfg WHERE  mt.RefineryId='" + refineryId + "'")
            .Append(" AND mt.UnitId = cfg.UnitId AND cfg.SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))  ")
        End With

        Dim maintTA_Other As New StringBuilder
        With maintTA_Other
            .Append("SELECT mt.TAID,mt.UnitID,P.SortKey,RTRIM(P.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown,")
            .Append("mt.TACostLocal,mt.TAMatlLocal,mt.TAOCCSTH,mt.TAOCCOVT,mt.TAMPSSTH,mt.TAMPSOVTPcnt,")
            .Append("RTRIM(P.Description) as UnitName,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate,")
            .Append("mt.TAExceptions ")
            .Append("FROM dbo.MaintTA mt, ProcessID_LU P WHERE  mt.RefineryId='" + refineryId + "' AND ")
            ' .Append(" P.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND ")
            .Append(" mt.ProcessID = P.ProcessID AND ProfileProcFacility = 'N' AND MaintDetails = 'Y' ")
        End With

        Dim maintRout As New StringBuilder
        With maintRout

            .Append("SELECT mr.UnitID,RTRIM(mr.ProcessID) AS ProcessID,mr.RoutCostLocal,mr.RoutMatlLocal,")
            .Append("mr.RegNum,mr.MaintNum,mr.OthNum,mr.OthDownEconomic,mr.OthDownExternal,")
            .Append("mr.OthDownUnitUpsets,mr.OthDownOffsiteUpsets,mr.OthDownOther,")
            .Append("mr.UnpRegNum,mr.UnpRegDown,mr.UnpMaintNum,mr.UnpMaintDown,")
            .Append("mr.UnpOthNum,mr.UnpOthDown,")
            .Append("mr.RegDown,mr.RegSlow,mr.MaintDown,mr.MaintSlow,mr.OthDown,mr.OthSlow,")
            .Append("cfg.SortKey,ISNULL(RTRIM(cfg.UnitName), RTRIM(lu.Description)) AS UnitName")
            .Append(" FROM dbo.MaintRout mr LEFT JOIN dbo.Config cfg ON mr.SubmissionID = cfg.SubmissionID AND mr.UNITID=cfg.UNITID ")
            .Append(" LEFT JOIN dbo.ProcessID_LU lu ON lu.ProcessID = mr.ProcessID ")
            ' .Append(" WHERE  lu.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND ")
            .Append("WHERE  mr.SubmissionID IN (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))  ")
        End With



        Dim crude As New StringBuilder
        With crude
            .Append("SELECT RTRIM(CNum) as CNum,RTRIM(CrudeName) AS CrudeName,Gravity,Sulfur,BBL,CostPerBBL")
            .Append(" FROM dbo.Crude WHERE  (SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')))  ")
        End With

        Dim yieldRM As New StringBuilder
        With yieldRM
            .Append("SELECT y.Category,y.MaterialID,y.MaterialName,y.BBL,y.PriceLocal,m.Sortkey ")
            .Append(" FROM dbo.Yield y, Material_LU m WHERE  m.MaterialID=y.MaterialID AND  y.SubmissionID IN ")
            .Append(" (SELECT SubmissionID FROM dbo.Submissions ")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND Category IN ('OTHRM','RCHEM','RLUBE') AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))  ")
        End With

        Dim yieldProd As New StringBuilder
        With yieldProd
            .Append("SELECT y.Category,y.MaterialID,y.MaterialName,y.BBL,y.PriceLocal,m.Sortkey")
            .Append(" FROM dbo.Yield y, Material_LU m WHERE m.MaterialID=y.MaterialID AND  y.SubmissionID IN ")
            .Append(" (SELECT SubmissionID FROM dbo.Submissions ")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND Category NOT IN ('OTHRM','RCHEM','RLUBE','RMI','RPF') AND y.MaterialID <> 'GAIN' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))")

        End With


        Dim energy As New StringBuilder
        With energy
            .Append("SELECT RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) As TransferTo, RTRIM(e.EnergyType) as EnergyType,")
            .Append("e.TransCode,e.RptSource,e.RptPriceLocal,elu.SortKey,e.Hydrogen,e.Methane,")
            .Append("e.Ethane,e.Ethylene,e.Propane,e.Propylene,e.Butane,e.Isobutane,e.C5Plus,e.CO,e.CO2,e.N2,e.Inerts ")
            .Append(" FROM dbo.Energy e ,Energy_LU elu WHERE ")
            .Append("elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND  elu.EnergyType=e.EnergyType AND elu.SortKey < 100 AND ")
            .Append("  (SubmissionID IN ")
            .Append(" (SELECT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))) ")

        End With



        Dim electric As New StringBuilder
        With electric
            .Append("SELECT RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) as TransferTo, RTRIM(e.EnergyType) as EnergyType,e.TransCode,e.RptGenEff,e.RptMWH,e.PriceLocal,elu.SortKey ")
            .Append(" FROM dbo.Electric e ,Energy_LU elu WHERE ")
            .Append("elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND elu.EnergyType=e.EnergyType AND elu.SortKey > 100 AND ")
            .Append("  (SubmissionID IN ")
            .Append(" (SELECT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))) ")
        End With



        Dim refTargets As New StringBuilder
        With refTargets
            .Append("SELECT ISNULL(RTRIM(r.CurrencyCode),'USD') as CurrencyCode,r.Target,RTRIM(r.Property) As Property,RTRIM(c.SectionHeader) AS SectionHeader,c.SortKey ")
            .Append(" FROM dbo.RefTargets r ,Chart_LU c WHERE c.TargetField=r.Property AND c.Sortkey<800 ")
            .Append("  AND  (SubmissionID IN ")
            .Append(" (SELECT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')))  ")
        End With


        Dim unitTargets As New StringBuilder
        With unitTargets
            .Append("SELECT u.UnitID,u.MechAvail,u.OpAvail,u.OnStream,u.UtilPcnt,")
            .Append(" u.RoutCost,u.TACost,ISNULL(RTRIM(u.CurrencyCode),'USD') as CurrencyCode,")
            .Append(" RTRIM(cfg.ProcessID)AS ProcessID,cfg.SortKey,RTRIM(cfg.UnitName) AS UnitName")
            .Append(" FROM dbo.UnitTargets u , Config cfg WHERE  ")
            .Append(" u.UnitId = cfg.UnitId AND u.SubmissionID=cfg.SubmissionID AND u.SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId)
            .Append("' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "'))  ")
        End With

        Dim maintRoutHist As String = "EXEC GetMaintRoutHist '" + refineryId + "', 'ACTUAL'"

        Dim userDefined As New StringBuilder
        With userDefined
            .Append(" SELECT u.HeaderText,u.VariableDesc,CAST(u.RptValue AS real) AS RptValue,CAST(u.RptValue_Target AS real) AS RptValue_Target,CAST(u.RptValue_Avg AS real) AS RptValue_Avg,CAST(u.RptValue_YTD AS real) AS RptValue_YTD,CAST( u.DecPlaces AS int) AS DecPlaces")
            .Append(" FROM dbo.UserDefined u  WHERE  ")
            .Append(" u.SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId + "' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')) ")

        End With

        Dim opexAdd As New StringBuilder
        With opexAdd
            .Append("SELECT OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock,")
            .Append("OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,MPSBenAbs,MPSBenInsur,")
            .Append("MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth,")
            .Append("MaintMatl,ContMaintMatl,EquipMaint,OthContProcOp,OthContTransOp,")
            .Append("OthContFire,OthContVacTrucks,OthContConsult,OthContInsp,OthContSecurity,")
            .Append("OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin,")
            .Append("OthContLegal,OthContOth,EnvirDisp,EnvirPermits,EnvirFines,")
            .Append("EnvirSpill,EnvirLab,EnvirEng,EnvirOth,EquipNonMaint,Tax,Insur,")
            .Append("OthNonVolSupply,OthNonVolSafety,OthNonVolComm,OthNonVolDonations,OthNonVolDues,")
            .Append("OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks,")
            .Append("OthNonVolOth,ChemicalsAlkyAcid,ChemicalsLube,ChemicalsH2OTreat,")
            .Append("ChemicalsProcess,ChemicalsOthAcid,ChemicalsGasAdd,ChemicalsDieselAdd,")
            .Append("ChemicalsOthAdd,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv,")
            .Append("ChemicalsWasteH2O,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK,")
            .Append("ChemicalsMEK,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv,")
            .Append("ChemicalsDewaxAids,ChemicalsOth,CatalystsFCC,CatalystsHYC,")
            .Append("CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT,")
            .Append("CatalystsHYFT,CatalystsCDWax,CatalystsREF,CatalystsHYG,")
            .Append("CatalystsS2Plant,CatalystsPetChem,CatalystsOth,PurOthN2,PurOthH2O,")
            .Append("PurOthOth,Royalties,OthVolDemCrude,OthVolDemLightering,OthVolDemProd,")
            .Append("EmissionsTaxes,EmissionsPurch,EmissionsCredits,OthVolOth ")
            .Append("FROM OpexAdd WHERE DataType='RPT' AND SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId + "' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')) ")
        End With

        Dim unitTargetsNew As New StringBuilder
        With unitTargetsNew
            'Get UnitTargetsNew
            .Append("SELECT ut.UnitID,c.UnitName,c.ProcessID,ut.Property,ut.Target,ut.CurrencyCode,")
            .Append(" ul.USDescription, ul.MetDescription, ul.USDecPlaces, ul.MetDecPlaces,ul.SortKey ")
            .Append(" FROM UnitTargetsNew ut,UnitTargets_LU ul, Config c ")
            .Append(" WHERE ut.SubmissionID = c.SubmissionID AND ut.UnitID = c.UnitID AND ul.Property = ut.Property ")
            .Append(" AND (ul.ProcessID='ALL' OR ul.ProcessID=c.ProcessID) ")
            .Append(" AND c.SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId + "' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')) ")
            .Append(" ORDER BY c.SortKey")
        End With


        Dim EDCStabilizers As New StringBuilder
        With EDCStabilizers
            .Append("SELECT RefineryID,EffDate,AnnInputBbl,")
            .Append("AnnCokeBbl,AnnElecConsMWH,AnnRSCRUDE_RAIL,AnnRSCRUDE_TT,")
            .Append("AnnRSCRUDE_TB,AnnRSCRUDE_OMB,AnnRSCRUDE_BB,AnnRSPROD_RAIL,")
            .Append("AnnRSCRUDE_TT,AnnRSCRUDE_TB,AnnRSCRUDE_OMB,AnnRSPROD_BB")
            .Append(" FROM dbo.LoadEDCStabilizers ")
            .Append(" WHERE RefineryID='" + refineryId + "'")
        End With

        Dim OpexAll As New StringBuilder
        With OpexAll
            .Append("SELECT OCCSal,MPSSal, OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub,")
            .Append("OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, OCCBen, ")
            .Append("MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock, ")
            .Append("MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, MaintMatl, ")
            .Append("ContMaintMatl, EquipMaint, MaintMatlST, ContMaintLabor, ")
            .Append("ContMaintInspect, ContMaintLaborST, OthContProcOp, OthContTransOp,")
            .Append("OthContFire, OthContVacTrucks, OthContConsult, OthContSecurity,")
            .Append("OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin,")
            .Append("OthContLegal, OthContOth, OthCont, TAAdj, EnvirDisp, EnvirPermits, ")
            .Append("EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, Envir, EquipNonMaint,")
            .Append("Tax, InsurBI, InsurPC, InsurOth, OthNonVolSupply, OthNonVolSafety, ")
            .Append("OthNonVolNonPersSafety, OthNonVolComm, OthNonVolDonations, ")
            .Append("OthNonVolNonContribPers, OthNonVolDues, OthNonVolTravel, OthNonVolTrain,")
            .Append("OthNonVolComputer, OthNonVolTanks, OthNonVolExtraExpat, OthNonVolOth, ")
            .Append("OthNonVol, STNonVol, GAPers, ChemicalsAntiknock, ChemicalsAlkyAcid, ")
            .Append("ChemicalsLube, ChemicalsH2OTreat, ChemicalsProcess, ChemicalsOthAcid,")
            .Append("ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, ")
            .Append("ChemicalsClay, ChemicalsAmines, ChemicalsASESolv, ChemicalsWasteH2O, ")
            .Append("ChemicalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene,")
            .Append("ChemicalsPropane, ChemicalsOthSolv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth, ")
            .Append("Chemicals, CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, ")
            .Append("CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, CatalystsHYG, CatalystsS2Plant,")
            .Append("CatalystsPetChem, CatalystsOth, Catalysts, PurOthN2, PurOthH2O, PurOthOth, PurOth,")
            .Append("Royalties, EmissionsTaxes, EmissionsPurch, EmissionsCredits, OthVolOth, OthVol, ")
            .Append("GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, ")
            .Append("OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, OthVolDemCrude, ")
            .Append("OthVolDemLightering, OthVolDemProd, STVol, TotCashOpEx, ExclFireSafety, ExclEnvirFines, ")
            .Append("ExclOth, TotExpExcl, STSal, STBen, PersCostExclTA, PersCost, EnergyCost, NEOpex ")
            .Append("FROM OpExAll WHERE DataType='RPT' AND SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId + "' AND (PeriodStart BETWEEN '" + periodStart.ToShortDateString + "' AND '")
            .Append(DateAdd(DateInterval.Day, -1, periodEnd).ToShortDateString + "')) ")

        End With

        'build query
        Dim allTables As New StringBuilder
        With allTables
            .Append(settings.ToString + ";" + config.ToString + ";" + configRS.ToString + ";")
            .Append(inventory.ToString + ";" + processData.ToString + ";" + opEx.ToString + ";" + opexAdd.ToString + ";")
            .Append(pers.ToString + ";" + absence.ToString + ";" + maintTA_Process.ToString + ";")
            .Append(maintTA_Other.ToString + ";" + maintRout.ToString + ";" + crude.ToString + ";" + yieldRM.ToString + ";")
            .Append(yieldProd.ToString + ";" + energy.ToString + ";" + electric.ToString + ";" + refTargets.ToString + ";")
            .Append(unitTargets.ToString + ";" + maintRoutHist + ";" + userDefined.ToString + ";" + unitTargetsNew.ToString + ";")
            .Append(EDCStabilizers.ToString + ";" + OpexAll.ToString + ";")
        End With


        'Query database
        ds = QueryDb(allTables.ToString)

        Dim tablenames() As String = {"Settings", "Config", "ConfigRS", "Inventory", "ProcessData", _
                                      "Opex", "OpexAdd", "Pers", "Absence", "MaintTA_Process", "MaintTA_Other", _
                                      "MaintRout", "Crude", "Yield_RM", "Yield_Prod", "Energy", _
                                      "Electric", "RefTargets", "UnitTargets", "MaintRoutHist", "UserDefined", _
                                      "UnitTargetsNew", "EDCStabilizers", "OpexAll"}

        'Changed defualt table names to assigned table names 
        Dim y As Integer

        For y = 0 To ds.Tables.Count - 1
            If (ds.Tables(y).TableName.ToUpper <> tablenames(y).ToUpper) Then
                ds.Tables(y).TableName = tablenames(y)
            End If
        Next

        Return ds
    End Function

#End Region

#Region "GetInputData"

    '<summary>
    '   Returns all refinery data
    '</summary>
    '<returns>Dataset</returns>
    <WebMethod()> _
    Public Function GetInputData() As DataSet
        '
        ' Retrieve the response's soap context
        '
        Dim responseContext As SoapContext = ResponseSoapContext.Current

        '
        ' Disable timestamp
        '
        responseContext.Security.Timestamp.TtlInSeconds = -1

        Dim refineryId As String = GetRefineryID(RequestSoapContext.Current)

        Dim ds As New DataSet

        Dim settings As New StringBuilder
        With settings '1
            .Append(" SELECT  ")
            .Append(" s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append(" RTRIM(s.Company) AS Company,RTRIM(s.Location) AS Location, ")
            .Append(" RTRIM(CoordName) AS CoordName,RTRIM(CoordTitle) AS CoordTitle, ")
            .Append(" RTRIM(CoordPhone) AS CoordPhone,RTRIM(CoordEmail) AS CoordEmail, ")
            .Append(" RTRIM(RptCurrency) AS RptCurrency,RTRIM(RptCurrencyT15) AS RptCurrencyT15,(CASE RTRIM(UOM) ")
            .Append(" WHEN 'US' THEN 'US Units' WHEN 'MET' THEN 'Metric' END) AS UOM, ")
            .Append(" ' ' AS BridgeLocation,FuelsLubesCombo, ")
            .Append(" PeriodMonth,PeriodYear, ")
            .Append(" RTRIM(DataSet) AS DataSet, BridgeVersion,ClientVersion ")
            .Append(" FROM dbo.Submissions s,TSort t ")
            .Append(" WHERE s.RefineryID='" + refineryId + "' AND s.RefineryID=t.RefineryID ")
        End With

        Dim config As New StringBuilder
        With config '2
            .Append("SELECT ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("c.UnitID,RTRIM(c.ProcessID) as ProcessID,c.SortKey,RTRIM(c.UnitName) AS UnitName,RTRIM(c.ProcessType) AS ProcessType,c.RptCap, RTRIM(p.ProcessGroup) AS ProcessGroup, ")
            .Append("c.UtilPcnt,c.RptStmCap,c.StmUtilPcnt,c.InServicePcnt,c.DesignFeedSulfur ")
            .Append("FROM dbo.Config c,ProcessID_LU p ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("c.SubmissionID = s.SubmissionID AND ")
            .Append("c.ProcessID=p.ProcessID AND   ")
            .Append("p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND  ")
            .Append("c.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID  ")
            .Append("FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
        End With

        Dim configRS As New StringBuilder
        With configRS '3
            .Append("SELECT  ")
            .Append("MIN(s.SubmissionID), MIN(s.PeriodStart) as PeriodStart, MIN(s.PeriodEnd) as PeriodEnd, ")
            .Append("RTRIM(ProcessID) AS ProcessID, ")
            .Append("SUM(CASE WHEN ProcessType = 'RAIL' THEN ISNULL(Throughput,0) END) AS RailcarBBL, ")
            .Append("SUM(CASE WHEN ProcessType IN ('TT', 'TTGD', 'TTO') THEN ISNULL(Throughput,0) END) AS TankTruckBBL, ")
            .Append("SUM(CASE WHEN ProcessType = 'TB' THEN ISNULL(Throughput,0) END) AS TankerBerthBBL, ")
            .Append("SUM(CASE WHEN ProcessType = 'OMB' THEN ISNULL(Throughput,0) END) AS OffshoreBuoyBBL, ")
            .Append("SUM(CASE WHEN ProcessType = 'BB' THEN ISNULL(Throughput,0) END) AS BargeBerthBBL, ")
            .Append("SUM(CASE WHEN ProcessType = 'PL' THEN ISNULL(Throughput,0) END) AS PipelineBBL  ")
            .Append("FROM ConfigRS c ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE  ")
            .Append("c.SubmissionID = s.SubmissionID AND ")
            .Append("c.SubmissionID IN  ")
            .Append("(SELECT  SubmissionID FROM dbo.Submissions  ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
            .Append("GROUP BY ProcessID ")
        End With

        Dim inventory As New StringBuilder
        With inventory '4
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("TankType,NumTank,FuelsStorage,AvgLevel  ")
            .Append("FROM dbo.Inventory i ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("i.SubmissionID = s.SubmissionID AND ")
            .Append("i.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions  ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
        End With

        Dim processData As New StringBuilder
        With processData '5
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("pd.UnitID,pd.Property,pd.RptValue,t.SortKey AS SortKey, RTRIM(cfg.ProcessID) AS ProcessID, RTRIM(cfg.UnitName) AS UnitName ")
            .Append("FROM dbo.ProcessData pd, Config cfg, Table2_LU  t    ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE    ")
            .Append("pd.SubmissionID = s.SubmissionID AND ")
            .Append("cfg.UnitID=pd.UnitID AND  ")
            .Append("cfg.SubmissionID = pd.SubmissionID AND t.ProcessID=cfg.ProcessID AND t.Property=pd.Property AND (pd.SubmissionID IN  ")
            .Append("(SELECT SubmissionID FROM dbo.Submissions  ")
            .Append("WHERE RefineryID='" + refineryId + "')) ")
        End With

        Dim opEx As New StringBuilder
        With opEx '6
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("ThirdPartyTerminalProd,ThirdPartyTerminalRM,OthRevenue, ")
            .Append("OthNonVol,OthVol,PurOth,Catalysts,Chemicals,GAPers,Envir,OthCont, ")
            .Append("ContMaintLabor,MaintMatl,MPSBen,OCCBen,MPSSal,OCCSal,POXO2 ")
            .Append("FROM  ")
            .Append("dbo.OpEx o ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE  ")
            .Append("o.SubmissionID = s.SubmissionID AND ")
            .Append("DataType='RPT' AND (o.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "')) ")
        End With

        Dim pers As New StringBuilder
        With pers '7
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("RTRIM(PersID) AS PersID,NumPers,AbsHrs,STH,OVTHours,OVTPcnt,Contract,GA,MaintPcnt ")
            .Append("FROM  ")
            .Append("dbo.Pers p ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("p.SubmissionID = s.SubmissionID AND ")
            .Append("(p.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
            .Append("AND PersID NOT IN ('OCCTAADJ','MPSTAADJ','OCCTAEXCL','MPSTAEXCL','OCCTAEXC','MPSTAEXC')) ")
        End With

        Dim absence As New StringBuilder
        With absence '8
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("OCCAbs,MPSAbs,RTRIM(CategoryID) As CategoryID  ")
            .Append("FROM  ")
            .Append("dbo.Absence a ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("a.SubmissionID = s.SubmissionID AND ")
            .Append("(a.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "')) ")
        End With

        Dim maintTA_Process As New StringBuilder
        With maintTA_Process '9
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("mt.TAID,mt.UnitID,RTRIM(cfg.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown, ")
            .Append("mt.TACostLocal,mt.TAMatlLocal,mt.TAOCCSTH,mt.TAOCCOVT,mt.TAMPSSTH,mt.TAMPSOVTPcnt, ")
            .Append("cfg.SortKey,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate, ")
            .Append("mt.TAExceptions,RTRIM(cfg.UnitName) AS UnitName  ")
            .Append("FROM  ")
            .Append("dbo.MaintTA mt, Config cfg  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("cfg.SubmissionID = s.SubmissionID AND ")
            .Append("mt.RefineryId='" + refineryId + "' ")
            .Append("AND mt.UnitId = cfg.UnitId AND cfg.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
        End With

        Dim maintTA_Other As New StringBuilder
        With maintTA_Other '10
            .Append("SELECT mt.TAID,mt.UnitID,P.SortKey,RTRIM(P.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown, ")
            .Append("mt.TACostLocal,mt.TAMatlLocal,mt.TAOCCSTH,mt.TAOCCOVT,mt.TAMPSSTH,mt.TAMPSOVTPcnt, ")
            .Append("RTRIM(P.Description) as UnitName,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate, ")
            .Append("mt.TAExceptions  ")
            .Append("FROM dbo.MaintTA mt, ProcessID_LU P WHERE  mt.RefineryId='" + refineryId + "' AND  ")
            .Append("mt.ProcessID = P.ProcessID AND ProfileProcFacility = 'N' AND MaintDetails = 'Y'  ")
        End With

        Dim maintRout As New StringBuilder
        With maintRout '11
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("mr.UnitID,RTRIM(mr.ProcessID) AS ProcessID,mr.RoutCostLocal,mr.RoutMatlLocal, ")
            .Append("mr.RegNum,mr.MaintNum,mr.OthNum,mr.OthDownEconomic,mr.OthDownExternal, ")
            .Append("mr.OthDownUnitUpsets,mr.OthDownOffsiteUpsets,mr.OthDownOther, ")
            .Append("mr.UnpRegNum,mr.UnpRegDown,mr.UnpMaintNum,mr.UnpMaintDown, ")
            .Append("mr.UnpOthNum,mr.UnpOthDown, ")
            .Append("mr.RegDown,mr.RegSlow,mr.MaintDown,mr.MaintSlow,mr.OthDown,mr.OthSlow, ")
            .Append("cfg.SortKey,ISNULL(RTRIM(cfg.UnitName), RTRIM(lu.Description)) AS UnitName ")
            .Append("FROM  ")
            .Append("dbo.MaintRout mr LEFT JOIN dbo.Config cfg ON mr.SubmissionID = cfg.SubmissionID AND mr.UNITID=cfg.UNITID  ")
            .Append("LEFT JOIN dbo.ProcessID_LU lu ON lu.ProcessID = mr.ProcessID  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("mr.SubmissionID = s.SubmissionID AND ")
            .Append("mr.SubmissionID IN (SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
        End With

        Dim crude As New StringBuilder
        With crude '12
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("RTRIM(CNum) as CNum,RTRIM(CrudeName) AS CrudeName,Gravity,Sulfur,BBL,CostPerBBL ")
            .Append("FROM  ")
            .Append("dbo.Crude c ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("c.SubmissionID = s.SubmissionID AND ")
            .Append("(c.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions  ")
            .Append("WHERE RefineryID='" + refineryId + "')) ")
        End With

        Dim yieldRM As New StringBuilder
        With yieldRM '13
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("y.Category,y.MaterialID,y.MaterialName,y.BBL,y.PriceLocal,m.Sortkey  ")
            .Append("FROM  ")
            .Append("dbo.Yield y, Material_LU m  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("y.SubmissionID = s.SubmissionID AND ")
            .Append("m.MaterialID=y.MaterialID AND  y.SubmissionID IN  ")
            .Append("(SELECT SubmissionID FROM dbo.Submissions  ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
            .Append("AND Category IN ('OTHRM','RCHEM','RLUBE') ")
        End With

        Dim yieldProd As New StringBuilder
        With yieldProd '14
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("y.Category,y.MaterialID,y.MaterialName,y.BBL,y.PriceLocal,m.Sortkey ")
            .Append("FROM  ")
            .Append("dbo.Yield y, Material_LU m  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE  ")
            .Append("y.SubmissionID = s.SubmissionID AND ")
            .Append("m.MaterialID=y.MaterialID AND  y.SubmissionID IN  ")
            .Append("(SELECT SubmissionID FROM dbo.Submissions  ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
            .Append("AND Category NOT IN ('OTHRM','RCHEM','RLUBE','RMI','RPF') AND y.MaterialID <> 'GAIN' ")
        End With

        Dim energy As New StringBuilder
        With energy '15
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) As TransferTo, RTRIM(e.EnergyType) as EnergyType, ")
            .Append("e.TransCode,e.RptSource,e.RptPriceLocal,elu.SortKey,e.Hydrogen,e.Methane,e.Ethane,e.Ethylene,")
            .Append("e.Propane,e.Propylene,e.Butane,e.Isobutane,e.C5Plus,e.CO,e.CO2,e.N2,e.Inerts ")
            .Append("FROM  ")
            .Append("dbo.Energy e ,Energy_LU elu  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE  ")
            .Append("e.SubmissionID = s.SubmissionID AND ")
            .Append("elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND  elu.EnergyType=e.EnergyType AND elu.SortKey < 100 AND  ")
            .Append("(e.SubmissionID IN  ")
            .Append("(SELECT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "')) ")
        End With

        Dim electric As New StringBuilder
        With electric '16
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) as TransferTo, RTRIM(e.EnergyType) as EnergyType,e.TransCode,e.RptGenEff,e.RptMWH,e.PriceLocal,elu.SortKey  ")
            .Append("FROM  ")
            .Append("dbo.Electric e ,Energy_LU elu  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE  ")
            .Append("e.SubmissionID = s.SubmissionID AND ")
            .Append("elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND elu.EnergyType=e.EnergyType AND elu.SortKey > 100 AND  ")
            .Append("(e.SubmissionID IN  ")
            .Append("(SELECT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "')) ")
        End With

        Dim refTargets As New StringBuilder
        With refTargets '17
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("ISNULL(RTRIM(r.CurrencyCode),'USD') as CurrencyCode,r.Target,RTRIM(r.Property) As Property,RTRIM(c.SectionHeader) AS SectionHeader,c.SortKey  ")
            .Append("FROM  ")
            .Append("dbo.RefTargets r ,Chart_LU c  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE  ")
            .Append("r.SubmissionID = s.SubmissionID AND ")
            .Append("c.TargetField=r.Property AND c.Sortkey<800  ")
            .Append("AND  (r.SubmissionID IN  ")
            .Append("(SELECT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "')) ")
        End With

        Dim unitTargets As New StringBuilder
        With unitTargets '18
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("u.UnitID,u.MechAvail,u.OpAvail,u.OnStream,u.UtilPcnt, ")
            .Append("u.RoutCost,u.TACost,ISNULL(RTRIM(u.CurrencyCode),'USD') as CurrencyCode, ")
            .Append("RTRIM(cfg.ProcessID)AS ProcessID,cfg.SortKey,RTRIM(cfg.UnitName) AS UnitName ")
            .Append("FROM  ")
            .Append("dbo.UnitTargets u , Config cfg  ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("u.SubmissionID = s.SubmissionID AND ")
            .Append("u.UnitId = cfg.UnitId AND u.SubmissionID=cfg.SubmissionID AND u.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
        End With

        Dim maintRoutHist As String = "EXEC GetMaintRoutHist '" + refineryId + "', 'ACTUAL'" '21

        Dim userDefined As New StringBuilder
        With userDefined '19
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("u.HeaderText,u.VariableDesc,CAST(u.RptValue AS real) AS RptValue,CAST(u.RptValue_Target AS real) AS RptValue_Target,CAST(u.RptValue_Avg AS real) AS RptValue_Avg,CAST(u.RptValue_YTD AS real) AS RptValue_YTD,CAST( u.DecPlaces AS int) AS DecPlaces ")
            .Append("FROM  ")
            .Append("dbo.UserDefined u   ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE   ")
            .Append("u.SubmissionID = s.SubmissionID AND ")
            .Append("u.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "')  ")
        End With

        Dim opexAdd As New StringBuilder
        With opexAdd '20
            .Append("SELECT  ")
            .Append("s.SubmissionID, s.PeriodStart, s.PeriodEnd, ")
            .Append("OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock, ")
            .Append("OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,MPSBenAbs,MPSBenInsur, ")
            .Append("MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth, ")
            .Append("MaintMatl,ContMaintMatl,EquipMaint,OthContProcOp,OthContTransOp, ")
            .Append("OthContFire,OthContVacTrucks,OthContConsult,OthContInsp,OthContSecurity, ")
            .Append("OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin, ")
            .Append("OthContLegal,OthContOth,EnvirDisp,EnvirPermits,EnvirFines, ")
            .Append("EnvirSpill,EnvirLab,EnvirEng,EnvirOth,EquipNonMaint,Tax,Insur, ")
            .Append("OthNonVolSupply,OthNonVolSafety,OthNonVolComm,OthNonVolDonations,OthNonVolDues, ")
            .Append("OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks, ")
            .Append("OthNonVolOth,ChemicalsAlkyAcid,ChemicalsLube,ChemicalsH2OTreat, ")
            .Append("ChemicalsProcess,ChemicalsOthAcid,ChemicalsGasAdd,ChemicalsDieselAdd, ")
            .Append("ChemicalsOthAdd,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv, ")
            .Append("ChemicalsWasteH2O,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK, ")
            .Append("ChemicalsMEK,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv, ")
            .Append("ChemicalsDewaxAids,ChemicalsOth,CatalystsFCC,CatalystsHYC, ")
            .Append("CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT, ")
            .Append("CatalystsHYFT,CatalystsCDWax,CatalystsREF,CatalystsHYG, ")
            .Append("CatalystsS2Plant,CatalystsPetChem,CatalystsOth,PurOthN2,PurOthH2O, ")
            .Append("PurOthOth,Royalties,OthVolDemCrude,OthVolDemLightering,OthVolDemProd, ")
            .Append("EmissionsTaxes,EmissionsPurch,EmissionsCredits,OthVolOth  ")
            .Append("FROM  ")
            .Append("OpexAdd o ")
            .Append(",dbo.Submissions s  ")
            .Append("WHERE  o.DataType='RPT' AND")
            .Append("o.SubmissionID = s.SubmissionID AND ")
            .Append("o.SubmissionID IN  ")
            .Append("(SELECT DISTINCT SubmissionID FROM dbo.Submissions ")
            .Append("WHERE RefineryID='" + refineryId + "') ")
        End With

        Dim EDCStabilizers As New StringBuilder
        With EDCStabilizers
            .Append("SELECT RefineryID,EffDate,AnnInputBbl,")
            .Append("AnnCokeBbl,AnnElecConsMWH,AnnRSCRUDE_RAIL,AnnRSCRUDE_TT,")
            .Append("AnnRSCRUDE_TB,AnnRSCRUDE_OMB,AnnRSCRUDE_BB,AnnRSPROD_RAIL,")
            .Append("AnnRSCRUDE_TT,AnnRSCRUDE_TB,AnnRSCRUDE_OMB,AnnRSPROD_BB")
            .Append(" FROM dbo.LoadEDCStabilizers ")
            .Append(" WHERE RefineryID='" + refineryId + "'")
        End With

        Dim opexAll As New StringBuilder
        With opexAll
            .Append("SELECT s.SubmissionID, s.PeriodStart,s.PeriodEnd, OCCSal,")
            .Append("MPSSal, OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, ")
            .Append("OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, OCCBen, ")
            .Append("MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock, ")
            .Append("MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, MaintMatl, ")
            .Append("ContMaintMatl, EquipMaint, MaintMatlST, ContMaintLabor, ")
            .Append("ContMaintInspect, ContMaintLaborST, OthContProcOp, OthContTransOp,")
            .Append("OthContFire, OthContVacTrucks, OthContConsult, OthContSecurity,")
            .Append("OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin,")
            .Append("OthContLegal, OthContOth, OthCont, TAAdj, EnvirDisp, EnvirPermits, ")
            .Append("EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirOth, Envir, EquipNonMaint,")
            .Append("Tax, InsurBI, InsurPC, InsurOth, OthNonVolSupply, OthNonVolSafety, ")
            .Append("OthNonVolNonPersSafety, OthNonVolComm, OthNonVolDonations, ")
            .Append("OthNonVolNonContribPers, OthNonVolDues, OthNonVolTravel, OthNonVolTrain,")
            .Append("OthNonVolComputer, OthNonVolTanks, OthNonVolExtraExpat, OthNonVolOth, ")
            .Append("OthNonVol, STNonVol, GAPers, ChemicalsAntiknock, ChemicalsAlkyAcid, ")
            .Append("ChemicalsLube, ChemicalsH2OTreat, ChemicalsProcess, ChemicalsOthAcid,")
            .Append("ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, ")
            .Append("ChemicalsClay, ChemicalsAmines, ChemicalsASESolv, ChemicalsWasteH2O, ")
            .Append("ChemicalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene,")
            .Append("ChemicalsPropane, ChemicalsOthSolv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth, ")
            .Append("Chemicals, CatalystsFCC, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, ")
            .Append("CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, CatalystsHYG, CatalystsS2Plant,")
            .Append("CatalystsPetChem, CatalystsOth, Catalysts, PurOthN2, PurOthH2O, PurOthOth, PurOth,")
            .Append("Royalties, EmissionsTaxes, EmissionsPurch, EmissionsCredits, OthVolOth, OthVol, ")
            .Append("GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, ")
            .Append("OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, OthVolDemCrude, ")
            .Append("OthVolDemLightering, OthVolDemProd, STVol, TotCashOpEx, ExclFireSafety, ExclEnvirFines, ")
            .Append("ExclOth, TotExpExcl, STSal, STBen, PersCostExclTA, PersCost, EnergyCost, NEOpex ")
            .Append("FROM dbo.OpExAll op, dbo.Submissions s ")
            .Append(" WHERE op.SubmissionID=s.SubmissionID AND op.DataType='RPT' AND s.SubmissionID IN ")
            .Append(" (SELECT DISTINCT SubmissionID FROM dbo.Submissions")
            .Append(" WHERE RefineryID='" + refineryId + "') ")

        End With


        'build query
        Dim allTables As New StringBuilder
        With allTables
            .Append(settings.ToString + ";") '1
            .Append(config.ToString + ";") '2
            .Append(configRS.ToString + ";") '3
            .Append(inventory.ToString + ";") '4
            .Append(processData.ToString + ";") '5
            .Append(opEx.ToString + ";") '6
            .Append(opexAdd.ToString + ";") '7
            .Append(opexAll.ToString + ";") '7
            .Append(pers.ToString + ";") '8
            .Append(absence.ToString + ";") '9
            .Append(maintTA_Process.ToString + ";") ' 10
            .Append(maintTA_Other.ToString + ";") '11
            .Append(maintRout.ToString + ";") '12
            .Append(crude.ToString + ";") '13
            .Append(yieldRM.ToString + ";") '14 
            .Append(yieldProd.ToString + ";") '15
            .Append(energy.ToString + ";") '16
            .Append(electric.ToString + ";") '17 
            .Append(refTargets.ToString + ";") '18
            .Append(unitTargets.ToString + ";") '19
            .Append(maintRoutHist + ";") '20
            .Append(userDefined.ToString + ";") '21
            .Append(EDCStabilizers.ToString + ";") '22
        End With




        'Query database
        ds = QueryDb(allTables.ToString)

        Dim tablenames() As String = {"Settings", "Config", "ConfigRS", "Inventory", "ProcessData", _
                                      "Opex", "OpexAdd", "OpexAll", "Pers", "Absence", "MaintTA_Process", "MaintTA_Other", _
                                      "MaintRout", "Crude", "Yield_RM", "Yield_Prod", "Energy", _
                                      "Electric", "RefTargets", "UnitTargets", "MaintRoutHist", "UserDefined", "EDCStabilizers"}

        'Changed defualt table names to assigned table names 
        Dim y As Integer

        For y = 0 To ds.Tables.Count - 1
            If (ds.Tables(y).TableName.ToUpper <> tablenames(y).ToUpper) Then
                ds.Tables(y).TableName = tablenames(y)
            End If
        Next

        Return ds
    End Function

#End Region



    Private Sub LoadData(ByVal sda As SqlDataAdapter, ByVal ds As DataSet, ByVal tablename As String, ByVal submissionId As String, ByVal refineryId As String)
        Dim t As Integer
        Dim rows() As DataRow

      
        sda.Fill(ds)

        If ds.Tables(tablename).Columns.Contains("RefineryID") Then
            rows = ds.Tables(tablename).Select("RefineryID='" + refineryId.ToString + "'")
        ElseIf ds.Tables(tablename).Columns.Contains("SubmissionID") Then
            rows = ds.Tables(tablename).Select("SubmissionID='" + submissionId.ToString + "'")
        End If


        For t = 0 To rows.Length - 1
            rows(t).Delete()
        Next
    End Sub

    'Generically assigns values from one row to another row in a different table or dataset
    'by ColumnName
    Sub AssignData(ByVal newRow As DataRow, ByVal dataRow As DataRow)

        Dim dcol As DataColumn
        'TODO: check if column types match before merging
        For Each dcol In dataRow.Table.Columns
            If newRow.Table.Columns.Contains(dcol.ColumnName) And Not dcol.ColumnName.StartsWith("Submission") Then
                classErr = "Table: " + newRow.Table.TableName + " Column: " + dcol.ColumnName + " ColumnType: " + dcol.DataType.ToString()
                newRow(dcol.ColumnName) = DeNull(dataRow, dcol.ColumnName)
            End If
        Next
        classErr = ""
    End Sub

    Private Sub sdSubmissions_RowUpdated(ByVal sender As System.Object, ByVal e As System.Data.SqlClient.SqlRowUpdatedEventArgs) Handles sdSubmissions.RowUpdated

    End Sub


    Private Sub SqlDataAdapter1_RowUpdated(ByVal sender As System.Object, ByVal e As System.Data.SqlClient.SqlRowUpdatedEventArgs) Handles sdLoadEDCStabilizers.RowUpdated

    End Sub

    Private Sub sdEnergy_RowUpdated(ByVal sender As System.Object, ByVal e As System.Data.SqlClient.SqlRowUpdatedEventArgs) Handles sdEnergy.RowUpdated

    End Sub

    Private Sub sdOpexAll2_RowUpdated(ByVal sender As System.Object, ByVal e As System.Data.SqlClient.SqlRowUpdatedEventArgs) Handles sdOpexAll2.RowUpdated

    End Sub
End Class
