Option Explicit On

Imports System
Imports System.Data
Imports System.Collections

Friend Class IReport
    Overridable Function getReport(ByVal ds As DataSet, ByVal UOM As String, ByVal currencyCode As String, ByVal replacementValues As Hashtable) As String
        Return ""
    End Function
    Overridable Function getReport(ByVal ds As DataSet, ByVal company As String, ByVal location As String, ByVal UOM As String, ByVal currencyCode As String, ByVal month As Integer, _
                       ByVal year As Integer, ByVal replacementValues As Hashtable) As String
        Return ""
    End Function
End Class
