﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.ProfileII.Contracts
{
    [DataContract]
    public class ChartData
    {
        [DataMember]
        public object Unit { get; set; }
    }
}
