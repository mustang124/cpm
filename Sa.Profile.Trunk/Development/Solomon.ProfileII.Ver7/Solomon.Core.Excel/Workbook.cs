﻿using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	public partial class XL
	{
		public static bool WorksheetExists(Excel.Workbook wkb, string name)
		{
			foreach (Excel.Worksheet wks in wkb.Worksheets)
			{
				if (wks.Name == name)
				{
					return true;
				};
			}
			return false;
		}

		public static bool WorksheetExists(Excel.Workbook wkb, string name, out Excel.Worksheet worksheet)
		{
			worksheet = null;

			foreach (Excel.Worksheet wks in wkb.Worksheets)
			{
				if (wks.Name == name)
				{
					worksheet = wks;
					return true;
				};
			}
			return false;
		}

		public static void CopyWorkbookValues(Excel.Workbook wkbSource, Excel.Workbook wkbTarget)
		{
			Excel.Worksheet wksTarget;
			Excel.Range rngTarget;

			if (wkbSource != wkbTarget)
			{
				foreach (Excel.Worksheet wksSource in wkbSource.Worksheets)
				{
					if (XL.WorksheetExists(wkbTarget, wksSource.Name, out wksTarget))
					{
						if (wksSource.ProtectContents || wksSource.ProtectDrawingObjects || wksSource.ProtectScenarios)
						{
							foreach (Excel.Range rngSource in wksSource.UsedRange)
							{
								if (!rngSource.Locked)
								{
									rngTarget = wksTarget.Range[rngSource.AddressLocal];
									rngTarget.Value = rngSource.Value;
								}
							}
						}
						else
						{
							try
							{
								foreach (Excel.Range rngSource in wksSource.UsedRange.SpecialCells(Excel.XlCellType.xlCellTypeConstants))
								{
									if (!rngSource.Locked)// && rngTarget.MergeArea.Value == null && rngSource.MergeArea.Value != null)
									{
										rngTarget = wksTarget.Range[rngSource.AddressLocal];
										rngTarget.Value = rngSource.Value;
									}
								}
							}
							catch
							{
							}

							try
							{
								foreach (Excel.Range rngSource in wksSource.UsedRange.SpecialCells(Excel.XlCellType.xlCellTypeFormulas))
								{
									if (!rngSource.Locked)// && rngTarget.MergeArea.Value == null && rngSource.MergeArea.Value != null)
									{
										rngTarget = wksTarget.Range[rngSource.AddressLocal];
										rngTarget.Value = rngSource.Value;
									}
								}
							}
							catch
							{
							}
						}
					}
				}
				wkbTarget.ForceFullCalculation = true;
				wkbTarget.Parent.CalculateFullRebuild();
			}
		}
	}
}
