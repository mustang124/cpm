using System;
using System.Data;
using System.Collections;
namespace GenrateInTableHtml
{
	/// <summary>
	/// Summary description for IReport.
	/// </summary>
	public interface IReport
	{ 
		 string getReport(DataSet ds,string UOM,string currencyCode,Hashtable replacementValues);
		 string getReport(DataSet ds,string company, string location,string UOM,string currencyCode,int month, int year,Hashtable replacementValues);
	}
}
