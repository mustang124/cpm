﻿Imports System.Text.RegularExpressions

Public Class CreateUserControl
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Unnamed_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub FormCreateUser_Click(sender As Object, e As EventArgs)
        Dim users As New UserData
        Dim userData As CreateUserModel = New CreateUserModel
        Dim inputFirstName As String = Request.Form("inputfirstname")
        Dim inputlastname As String = Request.Form("inputlastname")
        Dim inputUsername As String = Request.Form("inputUsername")
        Dim inputEmail As String = Request.Form("inputEmail")
        Dim inputPassword As String = Request.Form("inputPassword")
        Dim isAdmin As String = Request.Form("gridCheck1")
        Dim isEmailValid As Boolean = False
        Dim isUserNameValid As Boolean = True
        Dim isUserNameExists As Boolean = False

        If String.IsNullOrEmpty(inputFirstName) Or String.IsNullOrEmpty(inputlastname) Or
           String.IsNullOrEmpty(inputEmail) Or String.IsNullOrEmpty(inputPassword) Then
            ltCreateUserError.Text = "<font size=1px style=""COLOR:RED""> Please enter the required fields. </font>"
        Else

            Try
                Dim a = New System.Net.Mail.MailAddress(inputEmail)
                isEmailValid = True
            Catch ex As FormatException
                ltCreateUserError.Text = "<font size=1px style=""COLOR:RED""> " + ex.Message.ToString() + " </font>"
            End Try

            If isEmailValid Then
                Dim rgx As New Regex("[A-Za-z][A-Za-z0-9._]{2,20}")
                If Not rgx.IsMatch(inputUsername) Then
                    ltCreateUserError.Text = "<font size=1px style=""COLOR:RED""> UserName should be between 2 to 20 characters in Length. No Special characters or spaces are allowed. </font>"
                    isUserNameValid = False
                End If

                If isUserNameValid Then
                    If users.DoesUserExists(inputUsername) Then
                        ltCreateUserError.Text = "<font size=1px style=""COLOR:RED""> UserName already exists, choose different one. </font>"
                        isUserNameExists = True
                    End If
                End If
            End If


            If isEmailValid And isUserNameValid And Not isUserNameExists Then
                userData.FirstName = inputFirstName
                userData.Lastname = inputlastname
                userData.Email = inputEmail
                userData.UserName = inputUsername
                userData.IsAdmin = If(isAdmin = "on", True, False)
                userData.Company = Session("AppUser")
                userData.Password = inputPassword

                If users.CreateUser(userData) Then
                    ltCreateUserError.Text = "<font size=1px style=""COLOR:GREEN""> User Created Successfully. </font>"
                End If
            End If

        End If

    End Sub
End Class