﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CreateUserControl.ascx.vb" Inherits="Solomon.Profile.Corporate.CreateUserControl" %>
<div class="row" id="main-work">
    <br />
    <br />
    <br />
    <br />
    <br />

    <form id="frmcreateuser" name="frmcreateuser" runat="server" method="post">
        <div class="well">
            <div class="passwordBlock">

                <div class="form-group row">
                    <label for="inputfirstname" class="col-sm-2 col-form-label margin-top-10">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="inputfirstname" name="inputfirstname" placeholder="FirstName">
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="inputlastname" name="inputlastname" placeholder="LastName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-2 col-form-label margin-top-10">UserName</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputUsername" name="inputUsername" placeholder="UserName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label margin-top-10">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email">
                    </div>
                </div>
                <%--<asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="inputEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>--%>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label margin-top-10">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gridCheck" class="col-sm-2 col-form-label margin-top-5">Administrator</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck1" name="gridCheck1">
                            <label class="form-check-label" for="gridCheck1">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputCompany" class="col-sm-2 col-form-label margin-top-10">Company</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail39" value="Delek" disabled="disabled">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 margin-top-10">
                        <asp:Button type="submit" ID="FormCreateUser" OnClick="FormCreateUser_Click" runat="server" class="btn btn-primary" Text="Create User"></asp:Button>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <asp:Literal ID="ltCreateUserError" runat="server"></asp:Literal>
                    </div>
                </div>

            </div>
        </div>
   
    </form>

</div>
