﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ManageUsersControl.ascx.vb" Inherits="Solomon.Profile.Corporate.ManageUsersControl" %>

<div class="row" id="main-work">
    <br />
    <br />
    <br />
    <br />
    <br />

    <form id="frmMain" name="frmMain" runat="server">
        <div class="well">
            <%--  <asp:GridView ID="GridView1" runat="server">
                    <FooterStyle CssClass="GridViewFooterStyle" />
                    <RowStyle CssClass="GridViewRowStyle" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <PagerStyle CssClass="GridViewPagerStyle" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                </asp:GridView>--%>

            <asp:GridView ID="GridView1" CssClass="table table-striped table-bordered table-condensed" DataKeyNames="UserId" OnRowCommand="GridView1_RowCommand" runat="server" PageSize="10" AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="UserName" HeaderText="User Name"/>
                    <asp:BoundField DataField="Email" HeaderText="Email"/>
                    <asp:BoundField DataField="Company" HeaderText="Company"/>
                    <asp:TemplateField  HeaderText="Actions">
                        <ItemTemplate>
                            <asp:Button ID="Refineries" CommandName="Refineries" CssClass="btn btn-primary" Text="View Refineries" runat="server" />
                            <asp:Button ID="ResetPassword" CommandName="ResetPass" CssClass="btn btn-primary" Text="Reset Password" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderText="">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="DeleteUser" CommandArgument='<%#Eval("UserId")%>'  ID="DeleteUser" CssClass="btn btn-default" ToolTip="Delete">
                                <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <!-- Bootstrap Modal Dialog -->
            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">
                                        <asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                                </div>
                                <div class="modal-body">
                                    <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" runat="server" onserverclick="delete_ServerClick" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="modal fade" id="myModal1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">
                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label></h4>
                                </div>
                                <div class="modal-body">
                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" runat="server" onserverclick="Reset_ServerClick" data-dismiss="modal" class="btn btn-primary" id="Button1">Yes</button>
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <%--         <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False"
    DataSourceID="XmlDataSource1"
    CssClass="">
    <Columns>
        <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
        <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
        <asp:BoundField DataField="phone" HeaderText="phone" SortExpression="phone" />
    </Columns>
</asp:GridView>--%>
        </div>

                      
		<asp:Literal ID="ltManageUsers" runat="server"></asp:Literal>
    </form>

</div>
