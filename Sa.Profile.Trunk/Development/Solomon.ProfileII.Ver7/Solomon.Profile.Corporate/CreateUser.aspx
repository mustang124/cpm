﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master"  CodeBehind="CreateUser.aspx.vb" Inherits="Solomon.Profile.Corporate.CreateUser" %>
<%@ Register TagPrefix="cu1" TagName="CreateUserControl" Src="CreateUserControl.ascx" %>

<asp:Content ID="CreateUser_head" runat="server" ContentPlaceHolderID="PH_Head">
</asp:Content>

<asp:Content ID="CreateUser_Sidebar" runat="server" ContentPlaceHolderID="PH_Sidebar">
    <asp:PlaceHolder ID="PlaceHolder4" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="CreateUser_body" runat="server" ContentPlaceHolderID="PH_Work">

    <cu1:CreateUserControl id="cuControl1" runat="server"></cu1:CreateUserControl>

    <script type="text/javascript">
        $(function () {
            $('#summary').html('Create User');

            //settings menu highlight active item
            $('.settingsNav li').removeClass('settingsActive');
            $('.settingsNav li:contains("Create User")').addClass('settingsActive');

            $('.navigationStandard,.navigationHome').show();
        });
        Cookies.set('solomon_test', 'solomon_test', { expires: 1 });
        var cookieVal = Cookies.get('solomon_test');

        if (cookieVal == null) {
            document.writeln("<center><span style=\"COLOR:RED;\">Cookies must be enabled to use this site.</span></center>");
        }
        else {
            Cookies.remove('solomon_test');
        }
    </script>
</asp:Content>
