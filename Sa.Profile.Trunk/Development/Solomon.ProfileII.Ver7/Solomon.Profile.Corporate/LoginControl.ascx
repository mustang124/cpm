<%@ Control Language="VB" AutoEventWireup="false" CodeBehind="LoginControl.ascx.vb" Inherits="Solomon.Profile.Corporate.LoginControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<link media="screen" href="styles/rounded-div.css" type="text/css" rel="stylesheet" />

<div class="row">
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<form id="frmSignIn" name="frmSignIn" onsubmit="return(document.frmSignIn.securedusername.value!='' &amp;&amp; document.frmSignIn.securedpassword.value!='');" method="post" runat="server">
		
			<div class="col-xs-12 col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Please sign in</span>
					</div>
					<div class="panel-body">
						<input type="text" id="frmSignInUName" name="frmSignInUName" class="form-control" placeholder=" Username ..." />								
						<input type="password" id="frmSignInPWord" name="frmSignInPWord" class="form-control" placeholder=" Password ..." />								
						<asp:Button type="submit" ID="frmSignInSignIn" CssClass="btn btn-primary btn-block" OnClick="frmSignInSignIn_Click" runat="server" Text="Sign in" />
					</div>
				</div>
			</div>
		
	</form>
</div>

<p><asp:Literal ID="ltStatus" runat="server"></asp:Literal></p>

<script type="text/javascript">
	// FOCUS ON USERNAME TEXTBOX
	$('#frmSignInUName').focus();
</script>
