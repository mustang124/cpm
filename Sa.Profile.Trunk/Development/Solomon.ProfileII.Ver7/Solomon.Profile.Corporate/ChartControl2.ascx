<%@ Control Language="vb" ClassName="ChartControl2" CodeBehind="ChartControl2.ascx.vb" AutoEventWireup="false" Inherits="Solomon.Profile.Corporate.ChartControl2" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<style type="text/css">
		#nav {
			z-index: 2;
		}
	</style>
	<style type="text/css">
		.noData {
			font-weight: bold;
			font-size: 18px;
			color: #646567;
			font-family: Tahoma;
		}
		.unitsheader {
			font-size: 10pt;
			font-family: Tahoma;
		}
		.showPrint {
			display: none;
		}
		.style4 {
			font-weight: bold;
			font-size: 12pt;
			font-family: Tahoma;
		}
		.style5 {
			font-size: 10pt;
			font-family: Tahoma;
		}
	</style>
	<link media="print" href="styles/print.css" type="text/css" rel="StyleSheet" />
</head>
<body>
	<table border="0" height="100%" width="650">
		<tr>
			<td height="20%">
				<table style="Z-INDEX: 120" height="85" cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr>
						<td width="314" background="images/ReportBGLeft.gif" bgcolor="dimgray">
							<div id="LabelName" align="center">
								<span style="color:#fff;font-weight:bold;display:block;margin-left:5px;width:68%">
									<%= ChartName %>
								</span>
							</div>
							<br>
							<div id="Methodology" align="center">
								<font size="2" color="white">
										<%=  StudyYear.ToString + " Methodology" %>
									</font>
							</div>
						</td>
						<td align="right" width="32%" colspan="5">
							<!--div align="right"-->
							<img height="50" alt="" src="https://webservices.solomononline.com/ProfileII/images/SA logo RECT.jpg"
								width="300"><br>
							<!--/div-->
							<!--DIV id="LabelMonth" align="right"-->
							<font size="2">
									<%=  Date.Today.ToLongDateString()%>
								</font>
							<!--/DIV-->
						</td>
					</tr>
				</table>
				<asp:Panel ID="PanelUnits" Style="Z-INDEX: 116" Width="368px" CssClass="noshow" Visible="False"
					runat="server">
					<div class="unitsheader">
						<br>
						Select
							<asp:DropDownList ID="ListUnits" runat="server" Width="155px" Font-Names="Tahoma" Font-Size="7pt"
								Font-Bold="True" ForeColor="DarkBlue" font="Tahoma" Height="338px" AutoPostBack="true">
							</asp:DropDownList>
					</div>
				</asp:Panel>
				<hr style="width:100%;color:#000;height:1px;" />
				<h5>
					<center><%= IIf(Request.QueryString("rptOptions").ToString() <> "Current", Request.QueryString("rptOptions").ToString(), String.Empty)%></center>
				</h5>
			</td>
		</tr>
		<tr>
			<td align="center" height="352">

				<asp:Chart ID="Chart1" runat="server" Width="594px" Height="352px" RenderType="ImageTag">
					<Series>
						<asp:Series Name="Series1" ChartType="Column">
						</asp:Series>
					</Series>
					<ChartAreas>
						<asp:ChartArea Name="ChartArea1">
						</asp:ChartArea>
					</ChartAreas>
				</asp:Chart>

				<asp:Literal ID="ltMessage" runat="server"></asp:Literal>

			</td>
		</tr>
		<tr>
			<td align="center" height="20%">
				<%=GetChartData()%>
			</td>
		</tr>
	</table>
</body>
</html>
