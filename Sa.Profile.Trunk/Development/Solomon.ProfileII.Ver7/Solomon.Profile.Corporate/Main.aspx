<%@ OutputCache Location="None" %>
<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="Navigator.aspx.vb" Inherits="Solomon.Profile.Corporate.navigator" %>

<asp:Content ID="main_head" runat="server" ContentPlaceHolderID="PH_Head">
	<style type="text/css">
		.style1 {
			font-size: 15pt;
			text-transform: uppercase;
			color: #ffffff;
		}

		.style2 {
			font-size: 10px;
		}

		.optstyle {
			font-size: 7pt;
			font-family: tahoma;
		}

		.vButton {
			font-size: 7pt;
			color: white;
			font-family: tahoma;
			background-color: #5f6a7f;
		}

		#lbRefinery {
			width: 100px;
			height: 100px;
		}

		.style3 {
			color: #f8f8f8;
		}

		.style4 {
			font-weight: bold;
			color: #ffffff;
		}

		
	</style>
</asp:Content>

<asp:Content ID="main_Sidebar" runat="server" ContentPlaceHolderID="PH_Sidebar">
	<div class="label-default analysis">Analysis</div>
	<asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
	<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="main_Work" runat="server" ContentPlaceHolderID="PH_Work">
	
	<div class="row" id="main-work">
		<br />
		<br />
		<br />
		<br />
		<br />

		<form id="frmMain" name="frmMain" runat="server">		

			<div class="form-control period-selection-bar row">
				<label for="ddlStart" class="col-sm-2 control-label">Starting: </label>
				<div class="col-sm-4">
					<asp:DropDownList CssClass="form-control" ID="ddlStart" ClientIDMode="Static" TabIndex="2" runat="server"></asp:DropDownList>
				</div>
					
				<label for="ddlEnd" id="labelFor_ddlEnd" class="col-sm-2 control-label">Through: </label>
				<div class="col-sm-4">
					<asp:DropDownList CssClass="form-control" ID="ddlEnd" ClientIDMode="Static" TabIndex="2" runat="server"></asp:DropDownList>
				</div>
			</div>
            
			
				
			<div class="well">
				<div class="form-group">
					<table class="table-responsive">
						<thead></thead>
						<tbody>			
							<tr>
								<td style="width: 38%;"><strong>Currency</strong></td>
								<td style="width: 62%;">
									<asp:DropDownList ID="ddlCurrency" TabIndex="2" runat="server" CssClass="form-control"></asp:DropDownList>
								</td>
							</tr>		
							<tr>
								<td><strong>Methodology</strong></td>
								<td>
									<asp:DropDownList ID="ddlMethodology" TabIndex="2" runat="server" CssClass="form-control"></asp:DropDownList>
								</td>
							</tr>
							<tr>
					
								<td><strong>Units of Measure</strong></td>
								<td>
									<asp:DropDownList ID="ddlUOM" TabIndex="3" runat="server" CssClass="form-control radio-inline">
										<asp:ListItem Value="US" Selected="True">US Units</asp:ListItem>
										<asp:ListItem Value="MET">Metric</asp:ListItem>
									</asp:DropDownList>
								</td>					
							</tr>
							<tr>					
								<td><strong>Pricing Basis</strong></td>
								<td>
									<asp:RadioButtonList ID="rbScenario" runat="server" CssClass="form-control rpt-opt-radio" CellSpacing="1" RepeatLayout="Flow" RepeatDirection="Horizontal">
										<asp:ListItem Value="Client" Selected="True">Client</asp:ListItem>
										<asp:ListItem Value="Study">Study</asp:ListItem>
									</asp:RadioButtonList>
								</td>
							</tr>
							<tr>
								<td><strong>Reporting Options</strong></td>
								<td>
									<asp:RadioButtonList ID="rdoReportOptions" TabIndex="5" runat="server" CssClass="form-control rpt-opt-radio" CellSpacing="1" RepeatLayout="Flow" RepeatDirection="Horizontal">
										<asp:ListItem Value="Current Month" Selected="True">Current Month</asp:ListItem>
										<asp:ListItem Value="Rolling Average">Rolling Average</asp:ListItem>
										<asp:ListItem Value="YTD Average">YTD Average</asp:ListItem>
									</asp:RadioButtonList>
								</td>
							</tr>
							<tr>
								<td class="rpt-opt-refinery-left"><strong>Refinery </strong>(Optional)
									<br />
									<span class="style2">If you 
										do not select any refineries, all refineries will be shown when you 
										click View Results. To select multiple refineries, use the Shift key 
										and your mouse for contiguous selection of refineries. Use the Ctrl 
										key and your mouse for noncontiguous selection and 
										deselection.</span>
								</td>
								<td class="rpt-opt-refinery-right">
									<asp:ListBox ID="lbRefinery" TabIndex="6" runat="server" CssClass="form-control" SelectionMode="Multiple" DataValueField="RefineryID" DataTextField="Location" DataMember="TSort" DataSource="<%# DsTSort1%>"></asp:ListBox>
								</td>					
							</tr>
							
						</tbody>
					</table>
					
					<div class="row col-centered">
						<div class="col-md-12">
							<br />
						</div>
					</div>

					<div class="row col-centered">
						<div class="col-md-12">
							<asp:Button ID="btnViewReports" ClientIDMode="Static" runat="server" Text="View Results" CssClass="btn btn-default center-block"></asp:Button>
						</div>
					</div>
								
				</div>

			</div>

              
		<asp:Literal ID="ltJsScript" runat="server"></asp:Literal>

	</form>

</div>

	<script type="text/javascript">
		restoreChartState();

		//Displays Selected Report on the Screen
		function SummaryInfo(info) {
			if (info) {
				if (info.length > 48) {
					$('#summary').html('<span style="font-size:13pt; font-weight:bold;">' + info + '</span>');
				}
				else {
					$('#summary').html(info);
				}
			}
		}

		function NodePicked(section) {
			if (section == 'CHARTS') {
				//document.getElementById('endMth').style.visibility = "visible";
				//document.getElementById('ddlEnd').style.visibility = "visible";
				//document.getElementById('ddlEnd').disabled = false;

				$('#endMth').show();
				$('#labelFor_ddlEnd').show();
				$('#ddlEnd').show().prop('disabled', false);;

			}
			else {
				//document.getElementById('endMth').style.visibility = "hidden";
				//document.getElementById('ddlEnd').style.visibility = "hidden";
				//document.getElementById('ddlEnd').disabled = true;
				
				$('#endMth').hide();
				$('#labelFor_ddlEnd').hide();
				$('#ddlEnd').hide().prop('disabled', true);
			}
		}

		$(function() {
			$('.dropdown-toggle').dropdown();
			$('#summary').html('Main');
			$('.trigger').addClass('btn').addClass('restyle-trigger-btn');
			$('.branch').addClass('restyle-branch-btn');
			$('.branch span').addClass('btn').addClass('restyle-branch-span-btn');

			$('.navigationStandard').show();
			//$('#navbtnLogout').show();
		});
	</script>

</asp:Content>


