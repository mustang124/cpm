﻿Imports System.Data.SqlClient
Imports System.Text

Public Structure CreateUserModel
    Dim FirstName As String
    Dim Lastname As String
    Dim UserName As String
    Dim Email As String
    Dim Password As String
    Dim IsAdmin As Boolean
    Dim Company As String
End Structure

Public Class UserData

#Region "Private Variables"
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("SqlConnectionString").ToString()
#End Region

    Public Function CreateUser(ByVal user As CreateUserModel) As Boolean
        Dim rowsAffected As Integer
        Dim query As String = String.Empty
        Dim errorMsg As String = String.Empty
        Dim saltKey As String = String.Empty
        Dim tempPassword As String = String.Empty

        'Dim r As Random = New Random()
        'Dim s As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        'Dim sb As New StringBuilder
        'Dim cnt As Integer = r.Next(15, 33)
        'For i As Integer = 1 To cnt
        '    Dim idx As Integer = r.Next(0, s.Length)
        '    sb.Append(s.Substring(idx, 1))
        'Next
        'saltKey = sb.ToString()

        tempPassword = Encrypt(user.Password, "mustard")


        query &= "INSERT INTO Users (UserName, Password, FirstName , LastName, Email, Company, FirstLogin, PwdSaltKey, Administrator, TempPassword)  "
        query &= "VALUES (@1, @2, @3, @4, @5, @6, @7, @8, @9, @10)"

        Using conn As New SqlConnection(_connectionString)
            Using comm As New SqlCommand()
                With comm
                    .Connection = conn
                    .CommandType = CommandType.Text
                    .CommandText = query
                    .Parameters.AddWithValue("@1", user.UserName)
                    .Parameters.AddWithValue("@2", tempPassword)
                    .Parameters.AddWithValue("@3", user.FirstName)
                    .Parameters.AddWithValue("@4", user.Lastname)
                    .Parameters.AddWithValue("@5", user.Email)
                    .Parameters.AddWithValue("@6", user.Company)
                    .Parameters.AddWithValue("@7", 1)
                    .Parameters.AddWithValue("@8", "mustard")
                    .Parameters.AddWithValue("@9", user.IsAdmin)
                    .Parameters.AddWithValue("@10", tempPassword)
                End With
                Try
                    conn.Open()
                    rowsAffected = comm.ExecuteNonQuery()
                Catch ex As SqlException
                    errorMsg = ex.Message.ToString()
                End Try
            End Using
        End Using

        Return True
    End Function



    Public Function DoesUserExists(ByVal userName As String) As Boolean
        Dim reader As SqlDataReader
        SqlConnection1 = New System.Data.SqlClient.SqlConnection
        SqlConnection1.ConnectionString = _connectionString

        Dim cmd As New SqlCommand("SELECT * FROM Users WHERE UserName=@user", SqlConnection1)
        cmd.Parameters.Add("@user", userName)
        cmd.Connection.Open()
        reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Return reader.HasRows
    End Function
End Class
