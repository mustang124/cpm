<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ReportTree.ascx.vb" Inherits="Solomon.Profile.Corporate.ReportTree" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style>
	.rTrigger {
		font: 10/14px geneva, arial, verdana, sans-serif;
	}
	.rBranch {
		display: block;
		font-weight: bold;
		margin-left: 5px;
	}
	.rBranch a:link {
		font-weight: bold;
		margin-left: 5px;
		color: #3f4755;
		text-decoration: none;
	}
	.rBranch a:visited {
		font-weight: bold;
		margin-left: 5px;
		color: #3f4755;
		text-decoration: none;
	}
	.rBranch a:hover {
		font-weight: bold;
		border-top: white thin solid;
		border-bottom: gray thin solid;
		border-right: gray thin solid;
		margin-left: 5px;
		color: darkcyan;
		text-decoration: none;
		width: 90%;
	}
	.rBranch a:active {
		font-weight: bold;
		border-top: white thin solid;
		border-bottom: gray thin solid;
		border-right: gray thin solid; /*border-right: black thin solid;border-top: black thin solid;*/
		margin: 4pt; /*border-left: black thin solid;border-bottom: black thin solid;*/
		background-color: #cccccc;
		width: 90%;
	}
</style>

<script type="text/javascript">
	function NodePicked(section) { }
	function SummaryInfo(info) { }

	function setReportValue(node) {
		$('#SelectedNode').value = node;
		$('#Section').value = 'REPORTS';

		Cookies.set('pickednode', node, { expires: 1 });
		Cookies.set('pickedsection', 'REPORTS', { expires: 1 });
		Cookies.set('openedNodes', 'reportBranch', { expires: 1 });

		SummaryInfo(node);
		NodePicked('REPORTS');
	}
</script>

<div class="rBranch"></div>

<ul class="nav nav-list">
  <li onclick="javascript:setReportValue('Refinery Scorecard');"><label class="tree-toggle nav-header">Refinery Scorecard</label></li>  
</ul>

<input type="hidden" id="ReportSection" name="ReportSection" />
<input type="hidden" id="ReportSelectedNode" name="ReportSelectedNode" />
<input type="hidden" id="ReportOpendedBranches" name="ReportOpenedBranches" />

