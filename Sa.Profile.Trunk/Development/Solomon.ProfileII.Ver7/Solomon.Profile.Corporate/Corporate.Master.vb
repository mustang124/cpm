﻿Public Class Corporate
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Session("AppUser")) Then
            UserNameLabel.Text = "Welcome, " + Session("AppUser") + "!"
        End If
    End Sub

    Protected Sub navbtnHome_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("main.aspx")
    End Sub

    Protected Sub navbtnChangePassword_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("createuser.aspx")
    End Sub

    Protected Sub navbtnLogout_ServerClick(sender As Object, e As EventArgs)
        Session.Abandon()
        Response.Redirect("index.aspx")
    End Sub
End Class