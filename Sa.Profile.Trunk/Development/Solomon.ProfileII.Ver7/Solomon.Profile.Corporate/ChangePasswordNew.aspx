﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="ChangePasswordNew.aspx.vb" Inherits="Solomon.Profile.Corporate.ChangePasswordNew" %>
<%@ Register TagPrefix="cp1" TagName="ChangePasswordNewControl" Src="ChangePasswordNewCtrl.ascx" %>

<asp:Content ID="changepasswordnew_head" runat="server" ContentPlaceHolderID="PH_Head">
</asp:Content>

<asp:Content ID="changePassword_Sidebar" runat="server" ContentPlaceHolderID="PH_Sidebar">
    <asp:PlaceHolder ID="PlaceHolder3" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="changepasswordnew_body" runat="server" ContentPlaceHolderID="PH_Work">

    <cp1:ChangePasswordNewControl id="cpControl1" runat="server"></cp1:ChangePasswordNewControl>

    <script type="text/javascript">
        $(function () {
            $('#summary').html('Change Password');
            $('.navigationStandard,.navigationHome').show();
            //settings menu highlight active item
            $('.settingsNav li').removeClass('settingsActive');
            $('.settingsNav li:contains("Change Password")').addClass('settingsActive');
        });
        Cookies.set('solomon_test', 'solomon_test', { expires: 1 });
        var cookieVal = Cookies.get('solomon_test');

        if (cookieVal == null) {
            document.writeln("<center><span style=\"COLOR:RED;\">Cookies must be enabled to use this site.</span></center>");
        }
        else {
            Cookies.remove('solomon_test');
        }
    </script>
</asp:Content>
