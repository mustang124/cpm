<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
ShowIf(Target,IfTargetOn)

<table class='small' border="0">
  <tr>
    <td width=120></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=20></td>
    <td width=70></td>
    <td width=70></td>
    <td width=60></td>
    <td width=60></td>
    <td width=60></td>
  <tr>
    <td align=left valign=bottom><strong>ReportPeriod</strong></td>
    <td align=left valign=bottom><strong>Process</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID</strong></td>
    <td align=left valign=bottom><strong>Process</br>&nbsp;&nbsp;Type</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Capacity</br></strong></td>
    <td align=right valign=bottom><strong>EDC&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>UEDC&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Util.</br>%&nbsp;</strong></td>
    <td align=right valign=bottom><strong>Oper.</br>Avail.</br>%&nbsp;&nbsp;</strong></td>
    <td align=right valign=bottom><strong>T/A&nbsp;</br>CurrencyCode</br>/bbl</strong></td>
  </tr>
  SECTION(ProcessUnits,,)
  BEGIN
  Header('<tr>
    <td colspan=10 height=30 valign=bottom><strong>&nbsp;&nbsp;&nbsp;@Description</strong></td>
  </tr>')
  <tr>
    <td valign=bottom height=30 align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@UnitName</td>
    <td valign=bottom align=left>@ProcessID</td>
    <td valign=bottom aling=left>@ProcessType</td>
    <td valign=bottom align=right>Format(Cap,'#,##0')</td>
    <td valign=bottom align=left>@CapUnits</td>
    <td valign=bottom align=right>Format(EDC,'#,##0')</td>
    <td valign=bottom align=right>Format(UEDC,'#,##0')</td>
    <td valign=bottom align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td valign=bottom align=right>Format(OpAvail,'#,##0.0')</td>
    <td valign=bottom align=right>NoShowZero(Format(TACost,'N'))</td>
  </tr>
<!--
  <tr>
    <td valign=bottom align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tag(Target, - Target)</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(UtilPcnt_Target,'#,##0.0')))</td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(OpAvail_Target,'#,##0.0')))</td>
    <td valign=bottom align=right>Tag(Target,NoShowZero(Format(TACost_Target,'N')))</td>
  </tr>
-->
 END

  <tr>
    <td valign=bottom colspan=10 height=50><strong>&nbsp;&nbsp;&nbsp;Utilities and Off-Sites</strong></td>
  </tr>

  SECTION(Generated, ProcessID='STEAMGEN' ,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=10 height=30 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Steam Generation</td>
  </tr>
  ')
  END

  SECTION(Generated,ProcessID='STEAMGEN' AND ProcessType='SFB',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solid Fired Boilers</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>@CapUnits</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated,ProcessID='STEAMGEN' AND ProcessType='LGFB',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Liquid/Gas Fired Boilers</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>@CapUnits</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated, ProcessID='ELECGEN' ,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=10 height=30 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Electric Power Generation</td>
  </tr>
  ')
  END

  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='SFB',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solid Fired Boilers</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>kw</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='LGFB',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Liquid/Gas Fired Boilers</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>kw</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='DSL',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diesel Engine Driver</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>kw</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='FT',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fired Turbine Driver</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>kw</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='EXP',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expander Turbine Driver</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>kw</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='STT',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Steam Topping Turbine Driver</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>kw</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated,ProcessID='ELECGEN' AND ProcessType='EXTR',)
   BEGIN 
  <tr>
    <td colspan=3 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Extraction Turbine Driver</td>
    <td align=right valign=bottom>Format(Cap,'#,##0')</td>
    <td align=left valign=bottom>kw</td>
    <td align=right valign=bottom>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
   END

  SECTION(Generated, ProcessID='FCCPOWER' ,)
  BEGIN
  HEADER('
  <tr>
    <td colspan=3 height=30 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FCC Power Recovery Train</td>
    <td valign=bottom align=right valign=bottom height = 30>Format(Cap,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bhp</td>
    <td align=right valign=bottom height = 30>Format(EDC,'#,##0')</td>
    <td align=right valign=bottom height = 30>Format(UEDC,'#,##0')</td>
    <td align=right valign=bottom>Format(UtilPcnt,'#,##0.0')</td>
    <td colspan=2></td>
  </tr>
  ')
  END

  <tr>
    <td valign=bottom colspan=10 height=50><strong>&nbsp;&nbsp;&nbsp;Receipts and Shipments</strong></td>
  </tr>

  <tr>
    <td valign=bottom height=30 colspan=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Raw Material Receipts</td>
  </tr>

  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='RAIL',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Railcar</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='TT',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tank Truck</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='TB',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanker Berth</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='OMB',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Offshore Buoy</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='BB',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Barge Berth</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSCRUDE' AND ProcessType='PL',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipeline</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td colspan=5></td>
  </tr>
  END

  <tr>
    <td valign=bottom height=30 colspan=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Shipments</td>
  </tr>

  SECTION(RawMaterial,ProcessID='RSPROD' AND ProcessType='RAIL',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Railcar</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSPROD' AND ProcessType='TT',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tank Truck</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSPROD' AND ProcessType='TB',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanker Berth</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSPROD' AND ProcessType='OMB',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Offshore Buoy</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSPROD' AND ProcessType='BB',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Barge Berth</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td align=right>NoShowZero(Format(EDC,'#,##0'))</td>
    <td colspan=4></td>
  </tr>
  END

  SECTION(RawMaterial,ProcessID='RSPROD' AND ProcessType='PL',)
  BEGIN
  <tr>
    <td valign=bottom colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipeline</td>
    <td valign=bottom align=right>Format(BPD,'#,##0')</td>
    <td valign=bottom align=left valign=bottom>bbl</td>
    <td colspan=5></td>
  </tr>
  END

</table>
<!-- template-end -->