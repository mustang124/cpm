<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small width=900 border=0>
  <tr>
    <td colspan=20 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
  <tr>
    <td width=5></td>
    <td width=200></td>
    <td width=10></td>
    <td width=70></td>
    <td width=60></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
  </tr>
  <tr>
    <td colspan=6></td>
    <td colspan=4 align=center valign=bottom><strong>Downtime Hours</strong></td>
    <td colspan=8></td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Process Unit Name</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Process ID</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Average</br>EDC</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Process/</br>Regulatory</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Mechanical</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Period</br>Hours</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Unavailability</br>Due to T/A</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Mechanical</br>Availability</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Operational</br>Availability</strong></td>
  </tr>

  SECTION(Unit,,)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td align=right>Format(AvgEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(RegDown,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(MaintDown,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(PeriodHrs,'#,##0')</td>
    <td></td>
    <td align=right>Format(MechUnavailTA,'#,##0.00')</td>
    <td></td>
    <td align=right>Format(MechAvail,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(OpAvail,'#,##0.0')</td>
    <td></td>
  </tr>
  END

  SECTION(Total,,)
  BEGIN
  <tr>
    <td colspan=12><strong>Process Unit Total</strong></td>
    <td align=right valign=bottom><strong>Format(MechUnavailTA,'#,##0.00')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(MechAvail,'#,##0.0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(OpAvail,'#,##0.0')</strong></td>
    <td></td>
  </tr>
  END

</table>

<!-- template-end -->