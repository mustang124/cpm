Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Solomon Profile� II")> 
<Assembly: AssemblyDescription("Monitoring Refinery Improvement Progress")> 
<Assembly: AssemblyCompany("HSB Solomon Associates LLC")> 
<Assembly: AssemblyProduct("Solomon Profile� II")> 
<Assembly: AssemblyCopyright("� 2021 HSB Solomon Associates LLC")> 
<Assembly: AssemblyTrademark("Solomon Profile� is a Registered Trademark of HSB Solomon Associates LLC")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("26AFB954-80A3-40B7-BF4A-9B30689E3F33")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("7.0.0.0")> 

<Assembly: AssemblyDelaySign(False)> 
<Assembly: AssemblyKeyFile("..\..\SolomonProfileII.snk")> 
<Assembly: AssemblyKeyName("")> 