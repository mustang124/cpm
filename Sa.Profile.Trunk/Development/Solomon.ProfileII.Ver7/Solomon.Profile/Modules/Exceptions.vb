'********************************************************************************
'*  TODO: Comments go here...                                                   *
'********************************************************************************
Option Explicit On

Imports System
Imports System.Text
Imports System.IO
Imports Solomon.ProfileII.Contracts
Imports Solomon.ProfileII.Proxies
Imports System.Configuration


Public Class Exceptions


    Shared Sub Display(ByVal MessageType As String, ByVal ex As System.Exception, WhereFrom As String)
        'Dim ALog As New RemoteSubmitServices
        'Dim myFile As Stream = File.Create("Profile.log")
        'Dim myTextListener As New TextWriterTraceListener(myFile)

        'Trace.Listeners.Add(myTextListener)
        System.Diagnostics.Debug.WriteLine("Module: " & WhereFrom & vbCrLf & "Error: " & ex.Message, MessageType)
        MessageBox.Show("Module: " & WhereFrom & vbCrLf & "Error: " & ex.Message, MessageType)
        'Trace.WriteLine(vbCrLf & "Date: " & Now.ToString & vbCrLf & "Module: " & WhereFrom & vbCrLf & "Error: " & ex.Message & vbCrLf)
        'Trace.Flush()
        'ALog.SubmitActivityLog("", "", "", Environment.UserName, Environment.MachineName.ToString(), WhereFrom, "", "", "", "", ex.Message, "ERROR")


        If Util.UseNonWCFWebService Then
            'ALog.SubmitActivityLog(WhereFrom, Nothing, Nothing, Nothing, "ERROR", ex, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            ''Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
            Try
                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, WhereFrom, String.Empty, Nothing, Nothing, "ERROR", version, ex.Message,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
            Catch
            Finally
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
            End Try
        End If
        System.Environment.ExitCode = 0
        'myFile.Close()
    End Sub

   
End Class
'TODO: Add Serializable attributes...
Friend Class InvalidKeyLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid key values.

#Region " Constructors "
    Friend Sub New()
        'Default constructor.
    End Sub

    Friend Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Friend Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

'TODO: Add Serializable attributes...
Friend Class InvalidIVLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid IV values.

#Region " Constructors "
    Friend Sub New()
        'Default constructor.
    End Sub

    Friend Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Friend Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

'TODO: Add additional custom exception classes...
