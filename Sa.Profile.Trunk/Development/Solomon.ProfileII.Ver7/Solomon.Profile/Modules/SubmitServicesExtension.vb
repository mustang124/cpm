Option Explicit On

Imports System
Imports System.IO
Imports System.Net
Imports System.Reflection
Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Security.Policy
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens

Friend Class SubmitServicesExtension
    Inherits Solomon.Submit12.SubmitServices2

	Dim UToken As UsernameToken

    Friend Sub New()

        MyBase.New()
        Try
            T("SubmitServicesExtension.New())", True)
            ConfigureProxy()
            ' 20081001 RRH Path - SecureWebServiceRequest(frmMain.AppPath & "\_CERT\policy.ct")
            SecureWebServiceRequest(pathCert & Policy.GetFileName())
            T("SubmitServicesExtension.New-End")
        Catch ex As Exception
            If ex.Message.Contains("Microsoft.Web.Services2, Version=2.0.3.0") AndAlso ex.Message.Contains("The system cannot find the file specified.") Then
                MsgBox("Profile needs the 'Microsoft Web Services Enhancements 2.0 Service Pack 3' to be installed. Please contact Solomon.")
            Else
                MsgBox("Error at SubmitServicesExtension.New(): " & ex.Message)
            End If

        End Try
    End Sub

	Friend Sub New(ByVal keypath As String)
        MyBase.New()
        T("SubmitServicesExtension.New(keypath))")
        SecureWebServiceRequest(keypath)
        T("SubmitServicesExtension.New(keypath)-End)")
	End Sub

	Private Sub ConfigureProxy()
        T("SubmitServicesExtension.ConfigureProxy()")
        Me.PreAuthenticate = True
        T("SubmitServicesExtension.ConfigureProxy.PreAutheenticate")
        Me.Credentials = CredentialCache.DefaultCredentials
        T("SubmitServicesExtension.ConfigureProxy.me.Proxy")
		Me.Proxy = WebRequest.GetSystemWebProxy
        'Me.Proxy = GlobalProxySelection.Select
        T("SubmitServicesExtension.ConfigureProxy-proxy.credentials")
        Me.Proxy.Credentials = CredentialCache.DefaultCredentials
        T("SubmitServicesExtension.ConfigureProxy-IWebProxy")
		Dim wp As IWebProxy = WebRequest.GetSystemWebProxy
        'Dim wp As System.Net.WebProxy = GlobalProxySelection.Select
        T("SubmitServicesExtension.ConfigureProxy-wp.credentials")
		wp.Credentials = CredentialCache.DefaultCredentials

        Dim username As String = String.Empty
        Try
            username = My.Settings.ProxyUsername 'System.Configuration.ConfigurationManager.AppSettings("ProxyUsername")
        Catch
        End Try

        T("SubmitServicesExtension.ConfigureProxy-username")
        If Not IsNothing(username) AndAlso _
          username.Trim().Length > 0 Then
            T("SubmitServicesExtension.ConfigureProxy-username=" & username)
            Dim password As String = My.Settings.ProxyPassword 'System.Configuration.ConfigurationManager.AppSettings("ProxyPassword")
            T("SubmitServicesExtension.ConfigureProxy-password")
            T("SubmitServicesExtension.ConfigureProxy-password=" & password)

            T("SubmitServicesExtension.ConfigureProxy-cr1")
            Dim cr As New System.Net.NetworkCredential(username, password)
            T("SubmitServicesExtension.ConfigureProxy-cr2")
            Me.Proxy.Credentials = cr
            T("SubmitServicesExtension.ConfigureProxy-cr3")
            Me.Credentials = cr
            T("SubmitServicesExtension.ConfigureProxy-cr4")
            wp.Credentials = cr
            T("SubmitServicesExtension.ConfigureProxy-cr5")
        End If
        T("SubmitServicesExtension.ConfigureProxy-End")
	End Sub

    Private Sub SecureWebServiceRequest(ByVal keyPath As String)
        Try
            T("SubmitServicesExtension.SecureWebServiceRequest()")
            If Not File.Exists(keyPath) Then
                Throw New Exception(Directory.GetCurrentDirectory() + vbCrLf + "Key was not found. If you have a valid key, restart your application. Otherwise, please register your application " + _
                                    "before you submit or download data.")
            End If

            Dim key As String

            Dim reader As New StreamReader(keyPath)
            Dim password As String

            If reader.Peek <> -1 Then
                key = reader.ReadLine
                reader.Close()
                password = CalcMiniKey(key)
                UToken = New UsernameToken(key, password, PasswordOption.SendHashed)
                'ConfigureProxy(nproxy, token)
            Else
                Throw New Exception("A key was not found")
            End If
            T("SubmitServicesExtension.SecureWebServiceRequest-End")
        Catch ex As Exception
            MsgBox("Error at SubmitServicesExtension.SecureWebServiceRequest" & ex.Message)
        End Try
    End Sub

    Private Sub ConfigureProxy(ByVal proxy As  _
        WebServicesClientProtocol, ByVal token As UsernameToken)
        T("SubmitServicesExtension.ConfigureProxy()")
        proxy.RequestSoapContext.Security.Timestamp.TtlInSeconds = -1
        proxy.RequestSoapContext.Security.Tokens.Add(token)
        Dim dk As New DerivedKeyToken(token)
        proxy.RequestSoapContext.Security.Tokens.Add(dk)
        proxy.RequestSoapContext.Security.Elements.Add( _
           New MessageSignature(dk))
        proxy.RequestSoapContext.Security.Elements.Add( _
         New EncryptedData(dk))
        T("SubmitServicesExtension.ConfigureProxy-End")
    End Sub 'ConfigureProxy

	Private Function CalcMiniKey(ByVal key As String) As String
		Dim lastpos As Integer = key.Length - 1
		Dim firstPart As String = key.Substring(0, CInt(Math.Round(lastpos * (3 / 8))))
		Dim secondPart As String = key.Substring(CInt(Math.Round(lastpos * 0.75)), CInt(lastpos - (lastpos * 0.75) + 1))

		Return secondPart + firstPart
	End Function

	'Resolves the 'underlying connection is closed' error
	Protected Overrides Function GetWebRequest(ByVal uri As Uri) As System.Net.WebRequest
        T("SubmitServicesExtension.GetWebRequest()")
        ConfigureProxy()

		Dim requestPropertyInfo As PropertyInfo = Nothing
		'Dim request As WebRequest = WebRequest.Create(uri)

		Dim request As WebRequest = MyBase.GetWebRequest(uri)
		request.Credentials = CredentialCache.DefaultCredentials

		If IsNothing(requestPropertyInfo) Then
			requestPropertyInfo = request.GetType().GetProperty("Request", BindingFlags.Default)
		End If

		' Retrieve underlying web request 
		Dim webRequest As HttpWebRequest = CType(requestPropertyInfo.GetValue(request, Nothing), HttpWebRequest)

		' Setting KeepAlive 
		webRequest.KeepAlive = False
        T("SubmitServicesExtension.GetWebRequest-End")
		Return request

	End Function

End Class
