Friend Module Policy

    Friend Function GetFileName() As String
        T("GetFileName()")
        GetFileName = Nothing
        'MessageBox.Show(modAppDeclarations.pathCert, "*.ct")
        T("GetFileName-pathCert=" & modAppDeclarations.pathCert)
        For Each f As String In IO.Directory.GetFiles(modAppDeclarations.pathCert, "*.ct")
            T("GetFileName-File: " & f)

            If IO.File.Exists(f) Then
                Dim result As String = System.IO.Path.GetFileName(f)
                T("GetFileName-FileToUse: " & result)
                Return result
            End If


        Next f
        T("GetFileName-End")
    End Function

End Module
