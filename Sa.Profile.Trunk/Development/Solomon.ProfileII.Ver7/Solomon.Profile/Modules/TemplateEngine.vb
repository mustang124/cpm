Option Explicit On

Imports System
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Data
Imports System.Collections

Friend Class TemplateEngine
    Private _useUsUnits As Boolean = True
    ' 
    ' TODO: Add constructor logic here 
    ' 
    'Friend Sub New()
    'End Sub


    ''' <summary> 
    ''' Flag used thats indicates is US or Metric 
    ''' descriptions should be used. Should set before RenderOutput 
    ''' is called. 
    ''' </summary> 
    Friend Property UseUSUnits() As Boolean
        Get
            Return _useUsUnits
        End Get
        Set(ByVal value As Boolean)
            _useUsUnits = value
        End Set
    End Property

    ' <summary> 
    ' Reads a template renders the output 
    ' </summary> 
    ' <param name="tableName"></param> 
    ' <param name="xmlfile"></param> 
    ' <param name="templatefile"></param> 
    ' <returns></returns> 
    Friend Function RenderOutput(ByVal data As DataSet, ByVal templatefile As String, ByVal replacementValues As Hashtable) As String
        Dim tableName As String = ""
        Dim xmlfile As String = ""
        Dim text As String = ReadFile(templatefile)
        Dim body As String = ""
        Dim ds As DataSet

        If text.Length = 0 Then
            Return "Tempate file was not found."
        End If
        Dim fieldAttribute As New Hashtable()

        If data Is Nothing Then
            ds = New DataSet()
        Else
            ds = data
        End If

        Try
            'Read Config section 
            If data Is Nothing Then
                'Read header 
                Dim config As Match = Regex.Match(text, "(?:\s*<!--\s*config-start\s*-->\s*)([\s\w\d\W]*)(?:\s*<!--\s*config-end\s*-->\s*)", RegexOptions.IgnoreCase)
                If config.Success Then

                    Dim cString As String = config.Groups(1).Value
                    'Matches key-value pairs 
                    For Each m As Match In Regex.Matches(cString, "\w+\s*=\s*[^,;\r\n]+")
                        Dim str As String = m.Value
                        Dim key As String = str.Split("=".ToCharArray())(0)
                        Dim val As String = str.Split("=".ToCharArray())(1)

                        If key.StartsWith("DATASET") Then
                            tableName = val
                        ElseIf key.StartsWith("FILE") Then
                            xmlfile = val
                            If File.Exists(xmlfile) Then
                                ds.ReadXml(xmlfile)
                            End If
                        Else
                            fieldAttribute.Add(key, val)
                        End If
                    Next
                End If
            End If

            ' Read template section 
            Dim template As Match = Regex.Match(text, "(?:\s*<!--\s*template-start\s*-->\s*)([\s\w\d\W]*)(?:\s*<!--\s*template-end\s*-->\s*)", RegexOptions.IgnoreCase)
            If template.Success Then
                body = template.Groups(1).Value
                body = RenderSection(ds, body)
                body = CalculateFormula(ds, body)
                body = HandleColumnNameFormatting(body)
                body = HandleNoShowZero(body)
                body = HandleTag(body)
                body = HandleReplacementValues(body, replacementValues)
                body = HandleShowIf(body)
            End If
        Finally
            ds.Dispose()
            fieldAttribute = Nothing
        End Try

        Return body
    End Function


    ''' <summary> 
    ''' Parses templates return html text 
    ''' </summary> 
    ''' <param name="body"></param> 
    ''' <returns></returns> 
    Private Function RenderSection(ByVal ds As DataSet, ByVal body As String) As String
        For Each m As Match In Regex.Matches(body, "\bSECTION\s*\((.[^,]*),(\w*[^,]*),(\w*[^\)]*)\)[\w\W]*?BEGIN([\w\W]*?)\bEND\b\s*", RegexOptions.IgnoreCase)
            ds.Relations.Clear()

            'Get Section parameters 
            Dim tableName As String = m.Groups(1).Value
            Dim filter As String = m.Groups(2).Value
            Dim sort As String = m.Groups(3).Value

            'Get Template body 
            Dim sectionBody As String = m.Groups(4).Value
            Dim secResults As String = ""


            'Establish tables' relationships if they exist 
            RelateTable(ds, m.Value)

            'Start rendering output
            Try
                Debug.Print(tableName)
                Debug.Print(ds.Tables(tableName).TableName)
            Catch
            End Try

            If ds.Tables(tableName).[Select](filter).Length > 0 Then
                For Each row As DataRow In ds.Tables(tableName).[Select](filter, sort)
                    Dim secRows As String = sectionBody

                    'Handle first table 
                    For Each column As DataColumn In ds.Tables(tableName).Columns
                        Dim columnName As String = column.ColumnName
                        secRows = HandleFormatting(secRows, columnName, row(columnName))
                    Next

                    'Handles the parentTable columns in table relationships. It does the following 
                    ' parentData 
                    ' childData1 
                    ' childData2 

                    Dim selectStmt As String = ""
                    Dim count As Integer = 0
                    Dim totalRelationships As Integer = ds.Tables(tableName).ParentRelations.Count

                    'Build select string column1='xxx' AND column2='dxxx' 
                    'rrh 20110607 RRH
                    For Each relation As DataRelation In ds.Tables(tableName).ParentRelations
                        Dim relateCol As String = relation.ParentColumns(0).ColumnName
                        selectStmt += relateCol + "='" + row(relateCol).ToString() + "'"
                        If count < totalRelationships - 1 Then
                            selectStmt += " AND "
                            count += 1
                        End If
                    Next

                    If ds.Tables(tableName).ParentRelations.Count > 0 Then
                        Dim parentTable As String = ds.Tables(tableName).ParentRelations(0).ParentTable.TableName
                        For Each parentRow As DataRow In ds.Tables(parentTable).Select(selectStmt)
                            For Each column1 As DataColumn In parentRow.Table.Columns
                                Dim columnName As String = column1.ColumnName
                                secRows = HandleFormatting(secRows, columnName, parentRow(columnName))
                            Next
                        Next
                    End If

                    secResults += secRows
                    secResults = HandleHeaders(secResults)
                Next
            Else
                ' If there no rows returned ,replace each column name 
                ' with empty strings 
                ' string secRows=sectionBody; 
                ' foreach (DataColumn column in ds.Tables[tableName].Columns ) 
                ' { 
                ' string columnName =column.ColumnName; 
                ' secRows = HandleFormatting(secRows,columnName,String.Empty); 
                ' } 
                ' 
                ' foreach ( DataRelation relation in ds.Tables[tableName].ParentRelations) 
                ' { 
                ' DataTable parentTable = relation.ParentTable ; 
                ' foreach (DataColumn column in parentTable.Columns ) 
                ' { 
                ' string columnName =column.ColumnName; 
                ' secRows = HandleFormatting(secRows,columnName,String.Empty); 
                ' } 
                ' } 
                ' secResults+=secRows; 
                ' secResults = HandleHeaders(secResults); 

                'If no rows in the datatable, remove the section 
                secResults = ""
            End If


            secResults = HandleIncludeTable(ds, m.Value, secResults)

            body = body.Replace(m.Value, secResults)
        Next

        Return body
    End Function


    Friend Function HandleSubSections(ByVal ds As DataSet, ByVal body As String) As String

        For Each m As Match In Regex.Matches(body, "\bSUBSECTION\s*\((.[^,]*),(\w*[^,]*),(\w*[^\)]*)\)[\w\W]*?BEGIN([\w\W]*?)\bEND SUBSECTION\b\s*", RegexOptions.IgnoreCase)
            ds.Relations.Clear()

            'Get Section parameters 
            Dim tableName As String = m.Groups(1).Value
            Dim filter As String = m.Groups(2).Value
            Dim sort As String = m.Groups(3).Value
            'Get Template body 
            Dim sectionBody As String = m.Groups(4).Value
            Dim secResults As String = ""


            'Establish tables' relationships if they exist 
            RelateTable(ds, m.Value)

            'Start rendering output 
            If ds.Tables(tableName).[Select](filter).Length > 0 Then
                For Each row As DataRow In ds.Tables(tableName).[Select](filter, sort)
                    Dim secRows As String = sectionBody

                    'Handle first table 
                    For Each column As DataColumn In ds.Tables(tableName).Columns
                        Dim columnName As String = column.ColumnName

                        secRows = HandleFormatting(secRows, columnName, row(columnName))
                    Next

                    'Handles the parentTable columns in table relationships. It does the following 
                    ' parentData 
                    ' childData1 
                    ' childData2 

                    Dim selectStmt As String = ""
                    Dim count As Integer = 0
                    Dim totalRelationships As Integer = ds.Tables(tableName).ParentRelations.Count

                    'Build select string column1='xxx' AND column2='dxxx' 
                    For Each relation As DataRelation In ds.Tables(tableName).ParentRelations
                        Dim relateCol As String = relation.ParentColumns(0).ColumnName
                        selectStmt += relateCol + "='" + row(relateCol).ToString() + "'"
                        If count < totalRelationships - 1 Then
                            selectStmt += " AND "
                            count += 1
                        End If
                    Next

                    If ds.Tables(tableName).ParentRelations.Count > 0 Then
                        Dim parentTable As String = ds.Tables(tableName).ParentRelations(0).ParentTable.TableName
                        For Each parentRow As DataRow In ds.Tables(parentTable).[Select](selectStmt)
                            For Each column1 As DataColumn In parentRow.Table.Columns
                                Dim columnName As String = column1.ColumnName
                                secRows = HandleFormatting(secRows, columnName, parentRow(columnName))
                            Next
                        Next
                    End If


                    secResults += secRows
                    secResults = HandleHeaders(secResults)
                Next
            Else
                ' If there no rows returned ,replace each column name 
                ' with empty strings 
                Dim secRows As String = sectionBody
                For Each column As DataColumn In ds.Tables(tableName).Columns
                    Dim columnName As String = column.ColumnName
                    secRows = HandleFormatting(secRows, columnName, [String].Empty)
                Next

                For Each relation As DataRelation In ds.Tables(tableName).ParentRelations
                    Dim parentTable As DataTable = relation.ParentTable
                    For Each column As DataColumn In parentTable.Columns
                        Dim columnName As String = column.ColumnName
                        secRows = HandleFormatting(secRows, columnName, [String].Empty)
                    Next
                Next
                secResults += secRows
                secResults = HandleHeaders(secResults)
            End If


            secResults = HandleIncludeTable(ds, m.Value, secResults)

            body = body.Replace(m.Value, secResults)
        Next

        Return body
    End Function


    ''' <summary> 
    ''' Handles fields that dependent on UOM setting 
    ''' </summary> 
    ''' <param name="body"></param> 
    ''' <returns></returns> 
    Friend Function HandleUnits(ByVal body As String) As String
        For Each m As Match In Regex.Matches(body, "\b[Uu]seUnit\((.[^,]*),(.[^,)]*)??\)", RegexOptions.IgnoreCase)
            Dim USDecription As String = m.Groups(1).Value
            Dim MetricDescription As String = m.Groups(2).Value
            Dim replacement As String = ""
            If UseUSUnits Then
                replacement = USDecription
            Else
                replacement = MetricDescription
            End If

            'Check if the replacement value is a string or columnname. 
            'Anything begining with a ' or " means it is not a column name 
            If replacement.StartsWith("'") OrElse replacement.StartsWith("""") Then
                replacement = replacement.Split("'""".ToCharArray())(1)
            Else
                replacement = "@" + replacement
            End If

            body = body.Replace(m.Value, replacement)
        Next
        Return body
    End Function

    ''' <summary> 
    ''' Doesn't show zeros 
    ''' </summary> 
    ''' <param name="body"></param> 
    ''' <returns></returns> 
    Friend Function HandleNoShowZero(ByVal body As String) As String
        For Each m As Match In Regex.Matches(body, "\b[Nn]oShowZero\(\s*([\s\w\W]*?)\s*\)", RegexOptions.IgnoreCase)
            Dim replacement As String = m.Groups(1).Value.Trim()

            If Regex.Match(replacement, "\d").Success Then
                If Convert.ToDouble(replacement) = 0 Then
                    replacement = [String].Empty
                End If
            ElseIf replacement.Equals([String].Empty) Then
                replacement = [String].Empty
            End If


            body = body.Replace(m.Value, replacement)
        Next
        Return body
    End Function


    ''' <summary> 
    ''' Remove duplicate headers 
    ''' </summary> 
    ''' <param name="body"></param> 
    ''' <returns></returns> 
    Friend Function HandleHeaders(ByVal body As String) As String
        For Each m As Match In Regex.Matches(body, "\b[Hh]eader\(\'\s*([\s\w\W]*?)\s*\'\)", RegexOptions.IgnoreCase)
            Dim replacement As String = m.Groups(1).Value
            If body.IndexOf(replacement) <> 0 AndAlso (body.IndexOf(replacement) <> body.LastIndexOf(replacement)) Then
                replacement = ""
            End If
            ' MatchCollection mc = Regex.Matches(body,replacement,RegexOptions.IgnoreCase ); 
            ' int occurences = mc.Count; 
            ' if (occurences > 1) 
            ' replacement=""; 

            body = body.Replace(m.Value, replacement)
        Next

        Return body
    End Function


    ' Return included tables 
    Friend Function HandleIncludeTable(ByVal ds As DataSet, ByVal section As String, ByVal body As String) As String
        Dim includedTable As String

        For Each m As Match In Regex.Matches(section, "\b[Ii]ncludeTable\((.[^,\)]*)\)", RegexOptions.IgnoreCase)
            includedTable = m.Groups(1).Value

            'Search for column names in the included tables 
            For Each parentRow As DataRow In ds.Tables(includedTable).Rows
                For Each column1 As DataColumn In parentRow.Table.Columns
                    Dim columnName As String = column1.ColumnName
                    body = HandleFormatting(body, columnName, parentRow(columnName))

                Next
            Next
        Next

        Return body
    End Function

    ''' <summary> 
    ''' Create relationships between tables 
    ''' </summary> 
    Friend Sub RelateTable(ByVal ds As DataSet, ByVal body As String)
        Dim parentTableName As String
        Dim childTableName As String
        Dim field1 As String
        Dim field2 As String

        For Each m As Match In Regex.Matches(body, "\b[Rr]elate\((.[^,]*),(.[^,]*),(.[^,]*),(.[^,)]*)?\)", RegexOptions.IgnoreCase)
            parentTableName = m.Groups(1).Value
            childTableName = m.Groups(2).Value
            field1 = m.Groups(3).Value
            field2 = m.Groups(4).Value

            Dim parentColumn As DataColumn = ds.Tables(parentTableName).Columns(field1)
            Dim childColumn As DataColumn = ds.Tables(childTableName).Columns(field2)
            ds.Relations.Add(New DataRelation(field1, parentColumn, childColumn, False))
        Next

    End Sub

    ''' <summary> 
    ''' Handles format that is given from the database 
    ''' </summary> 
    ''' <param name="body"></param> 
    ''' <returns></returns> 
    Private Function HandleColumnNameFormatting(ByVal body As String) As String

        For Each smc As Match In Regex.Matches(body, "\b[Ff]ormat\(\s*(\w*[^,a-dg-zA-DG-Z]*)\s*,\s*([\w\W]*?)\s*\){1}", RegexOptions.IgnoreCase)
            Dim format As String = ""
            Dim columnValue As String = smc.Groups(1).Value
            Dim val As Double
            If columnValue IsNot Nothing AndAlso Not columnValue.StartsWith("@") Then
                If columnValue.Trim().Length > 0 Then
                    val = [Double].Parse(columnValue)
                Else
                    val = 0
                End If


                Select Case smc.Groups(2).Value.Trim()
                    Case "0"
                        format = "{0:#,##0}"
                        Exit Select
                    Case "1"
                        format = "{0:#,##0.0}"
                        Exit Select
                    Case "2"
                        format = "{0:N}"
                        Exit Select
                    Case Else

                        If smc.Groups(2).Value.Trim().StartsWith("'") Then
                            Dim a As String() = smc.Groups(2).Value.Trim().Split("'".ToCharArray())
                            If a.Length > 2 Then
                                format = "{0:" + a(1) + "}"

                            End If
                        End If
                        Exit Select
                End Select


                If format.Trim().Length > 0 Then
                    body = Regex.Replace(body, "\b[Ff]ormat\(\s*" + columnValue + "\s*,\s*" + smc.Groups(2).Value.Trim() + "\s*?\)", [String].Format(format, val).Trim(), RegexOptions.IgnoreCase)
                Else
                    body = Regex.Replace(body, "\b[Ff]ormat\(\s*" + columnValue + "\s*,\s*" + smc.Groups(2).Value.Trim() + "\s*?\)", [String].Empty, RegexOptions.IgnoreCase)
                End If
            Else
                body = Regex.Replace(body, "\b[Ff]ormat\(\s*" + columnValue + "\s*,\s*" + smc.Groups(2).Value.Trim() + "\s*?\)", [String].Empty, RegexOptions.IgnoreCase)
            End If
        Next

        Return body
    End Function

    ''' <summary> 
    ''' Handles number formatting and colume name subsitution 
    ''' </summary> 
    ''' <param name="secRow"></param> 
    ''' <param name="columnName"></param> 
    ''' <param name="columnValue"></param> 
    ''' <returns></returns> 
    Private Function HandleFormatting(ByVal secRow As String, ByVal columnName As String, ByVal columnValue As Object) As String
        Dim sm As Match = Regex.Match(secRow, "\b[Ff]ormat\(\s*(" + columnName + ")\s*\,\'(.[^']*)\'?\)", RegexOptions.IgnoreCase)
        If columnValue IsNot Nothing Then
            If sm.Success Then
                'Added for mixed type columns like varchar 
                If (TypeOf columnValue Is String) AndAlso (Regex.Match(columnValue.ToString(), "^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$").Success) Then
                    columnValue = Convert.ToDouble(columnValue)
                End If

                Dim format As String = sm.Groups(2).Value
                'Replace format clause 
                'secRow = Regex.Replace(secRow,sm.Groups[0].Value ,String.Format("{0:"+format+"}",columnValue).Trim(),RegexOptions.IgnoreCase ); 
                secRow = Regex.Replace(secRow, "\b[Ff]ormat\(\s*" + columnName + "\s*\,\s*\'" + format + "\'\s*?\)", [String].Format("{0:" + format + "}", columnValue).Trim(), RegexOptions.IgnoreCase)
            Else
                'Replace column name with value 
                secRow = Regex.Replace(secRow, "@\b" + columnName + "\b", columnValue.ToString().Trim(), RegexOptions.IgnoreCase)
            End If
        Else
            secRow = Regex.Replace(secRow, "@\b" + columnName + "\b", [String].Empty, RegexOptions.IgnoreCase)
        End If
        Return secRow
    End Function

    ' <summary> 
    ' Convert TAGs to a cascade stylesheet class 
    ' Can use cascade stylesheets to make elements hidden and more. 
    ' </summary> 
    ' <param name="secRow"></param> 
    ' <param name="columnName"></param> 
    ' <param name="columnValue"></param> 
    ' <returns></returns> 
    Private Function HandleTag(ByVal body As String) As String
        For Each sm As Match In Regex.Matches(body, "\b[Tt]ag\(\s*(\w*)\s*,([\w\W]*?)\){1}", RegexOptions.IgnoreCase)
            Dim cssClassName As String = sm.Groups(1).Value
            Dim txt As String = sm.Groups(2).Value

            'Replace the TAG clause 
            body = Regex.Replace(body, "\b[Tt]ag\(\s*" + cssClassName + "\s*\,\s*" + txt + "\s*?\)", "<span class='" + cssClassName + "'>" + txt + "</span>", RegexOptions.IgnoreCase)
        Next
        Return body
    End Function

    ' <summary> 
    ' Convert TAGs to a cascade stylesheet class 
    ' Can use cascade stylesheets to make elements hidden and more. 
    ' </summary> 
    ' <param name="secRow"></param> 
    ' <param name="columnName"></param> 
    ' <param name="columnValue"></param> 
    ' <returns></returns> 
    Private Function HandleShowIf(ByVal body As String) As String
        For Each sm As Match In Regex.Matches(body, "\b[Ss]howIf\(\s*(\w*)\s*,([\w\W]*?)\){1}", RegexOptions.IgnoreCase)
            Dim cssClassName As String = sm.Groups(1).Value
            Dim test As String = sm.Groups(2).Value
            Dim hiddenStyle As String = "<style>." + cssClassName + "{display:none;} </style>"
            'Replace the TAG clause 

            If [Boolean].Parse(test) Then
                body = Regex.Replace(body, "\b[Ss]howIf\(\s*" + cssClassName + "\s*\,([\w\W]*?)\){1}", [String].Empty, RegexOptions.IgnoreCase)
            Else
                body = Regex.Replace(body, "\b[Ss]howIf\(\s*" + cssClassName + "\s*\,([\w\W]*?)\){1}", hiddenStyle, RegexOptions.IgnoreCase)
            End If
        Next
        Return body
    End Function
    ' <summary> 
    ' Handles that Formula Tag. Computes equation given in the parameter 
    ' </summary> 
    ' <param name="table"></param> 
    ' <param name="formula"></param> 
    ' <param name="filter"></param> 
    ' <returns></returns> 
    Private Function CalculateFormula(ByVal ds As DataSet, ByVal body As String) As String
        Dim formula As String = ""
        Dim filter As String = ""
        Dim format As String = ""
        Dim tableName As String = ""

        For Each m As Match In Regex.Matches(body, "\b[Ff]ormula\((.[^,]*),(.[^,]*),(.[^,]*)?,\'(.[^']*)\'?\)")
            Dim str As String = m.Value

            'Get parameters 

            tableName = m.Groups(1).Value
            formula = m.Groups(2).Value
            filter = m.Groups(3).Value
            format = m.Groups(4).Value

            Dim answer As Object
            answer = ds.Tables(tableName).Compute(formula, filter)

            body = body.Replace(str, [String].Format("{0:" + format + "}", answer))
        Next
        Return body
    End Function

    ' <summary> 
    ' Replace custom tags in the template string with the value given. 
    ' This is used with SHOWIF and any other future condition statements. 
    ' </summary> 
    ' <param name="body"></param> 
    ' <param name="tags"></param> 
    ' <returns></returns> 
    Private Function HandleReplacementValues(ByVal body As String, ByVal replacementValues As Hashtable) As String

        Dim en As IDictionaryEnumerator = replacementValues.GetEnumerator()
        While en.MoveNext()

            body = body.Replace(DirectCast(en.Key, String), DirectCast(en.Value, String))
        End While

        Return body
    End Function

    ''' <summary> 
    ''' Reads the file and return the content 
    ''' </summary> 
    ''' <param name="filename"></param> 
    ''' <returns></returns> 
    Friend Function ReadFile(ByVal filename As String) As String
        Dim buf As String = ""
        If File.Exists(filename) Then
            Dim reader As StreamReader = File.OpenText(filename)
            Try
                buf = reader.ReadToEnd()
                buf = HandleUnits(buf)
            Finally
                reader.Close()
            End Try
        End If
        Return buf
    End Function
End Class

