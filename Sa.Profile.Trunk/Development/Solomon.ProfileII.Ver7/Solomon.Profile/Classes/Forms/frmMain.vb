Option Explicit On

Imports System
Imports System.IO
Imports Solomon.Profile.Solomon.Submit12
Imports System.Configuration
Imports System.Collections.Generic
Imports Solomon.ProfileII.Contracts
Imports Solomon.ProfileII.Proxies

Friend Class frmMain
    Inherits System.Windows.Forms.Form



#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()
        T("=================================================")
        T("frmMain.New-Start: " & DateTime.Now().ToShortDateString() & " " & DateTime.Now().ToShortTimeString())
        Application.EnableVisualStyles()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        T("frmMain.New-End")
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents pnlProgress As System.Windows.Forms.StatusBarPanel
    Friend WithEvents pnlCurrency As System.Windows.Forms.StatusBarPanel
    Friend WithEvents pnlUOM As System.Windows.Forms.StatusBarPanel
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuFile As System.Windows.Forms.MenuItem
    Friend WithEvents MenuNew As System.Windows.Forms.MenuItem
    Friend WithEvents MenuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents MenuSave As System.Windows.Forms.MenuItem
    Friend WithEvents MenuPrint As System.Windows.Forms.MenuItem
    Friend WithEvents MenuClose As System.Windows.Forms.MenuItem
    Friend WithEvents MenuExit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuEdit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuConf As System.Windows.Forms.MenuItem
    Friend WithEvents MenuData As System.Windows.Forms.MenuItem
    Friend WithEvents MenuConfRefInfo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuConfProcess As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuViewTargets As System.Windows.Forms.MenuItem
    Friend WithEvents MenuViewHist As System.Windows.Forms.MenuItem
    Friend WithEvents MenuViewRefTarget As System.Windows.Forms.MenuItem
    Friend WithEvents MenuViewUnitTarget As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView16Therm As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView16Elec As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView15Raw As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView15Yield As System.Windows.Forms.MenuItem
    Friend WithEvents MenuViewUser As System.Windows.Forms.MenuItem
    Friend WithEvents MenuTools As System.Windows.Forms.MenuItem
    Friend WithEvents MenuToolsAdmin As System.Windows.Forms.MenuItem
    Friend WithEvents MenuToolsImport As System.Windows.Forms.MenuItem
    Friend WithEvents MenuToolsUpload As System.Windows.Forms.MenuItem
    Friend WithEvents MenuWindow As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataProc As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataInv As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataOpex As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataPers As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataCrude As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataMatl As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataEnergy As System.Windows.Forms.MenuItem
    Friend WithEvents MenuDataUser As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView1Proc As System.Windows.Forms.MenuItem
    Friend WithEvents MenuView1Inv As System.Windows.Forms.MenuItem
    Friend WithEvents MenuHelp As System.Windows.Forms.MenuItem
    Friend WithEvents MenuHelpHelp As System.Windows.Forms.MenuItem
    Friend WithEvents MenuHelpAbout As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDashboardProfileChoice As System.Windows.Forms.MenuItem
    Friend WithEvents MenuEDCStable As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.StatusBar1 = New System.Windows.Forms.StatusBar()
        Me.pnlProgress = New System.Windows.Forms.StatusBarPanel()
        Me.pnlCurrency = New System.Windows.Forms.StatusBarPanel()
        Me.pnlUOM = New System.Windows.Forms.StatusBarPanel()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuFile = New System.Windows.Forms.MenuItem()
        Me.MenuNew = New System.Windows.Forms.MenuItem()
        Me.MenuOpen = New System.Windows.Forms.MenuItem()
        Me.MenuSave = New System.Windows.Forms.MenuItem()
        Me.MenuPrint = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuClose = New System.Windows.Forms.MenuItem()
        Me.MenuExit = New System.Windows.Forms.MenuItem()
        Me.MenuEdit = New System.Windows.Forms.MenuItem()
        Me.MenuConf = New System.Windows.Forms.MenuItem()
        Me.MenuConfRefInfo = New System.Windows.Forms.MenuItem()
        Me.MenuConfProcess = New System.Windows.Forms.MenuItem()
        Me.MenuData = New System.Windows.Forms.MenuItem()
        Me.MenuDataProc = New System.Windows.Forms.MenuItem()
        Me.MenuDataInv = New System.Windows.Forms.MenuItem()
        Me.MenuDataOpex = New System.Windows.Forms.MenuItem()
        Me.MenuDataPers = New System.Windows.Forms.MenuItem()
        Me.MenuDataCrude = New System.Windows.Forms.MenuItem()
        Me.MenuDataMatl = New System.Windows.Forms.MenuItem()
        Me.MenuDataEnergy = New System.Windows.Forms.MenuItem()
        Me.MenuDataUser = New System.Windows.Forms.MenuItem()
        Me.MenuView = New System.Windows.Forms.MenuItem()
        Me.MenuView1 = New System.Windows.Forms.MenuItem()
        Me.MenuView1Proc = New System.Windows.Forms.MenuItem()
        Me.MenuView1Inv = New System.Windows.Forms.MenuItem()
        Me.MenuView2 = New System.Windows.Forms.MenuItem()
        Me.MenuView4 = New System.Windows.Forms.MenuItem()
        Me.MenuView5 = New System.Windows.Forms.MenuItem()
        Me.MenuView6 = New System.Windows.Forms.MenuItem()
        Me.MenuView7 = New System.Windows.Forms.MenuItem()
        Me.MenuView9 = New System.Windows.Forms.MenuItem()
        Me.MenuView10 = New System.Windows.Forms.MenuItem()
        Me.MenuView14 = New System.Windows.Forms.MenuItem()
        Me.MenuView15 = New System.Windows.Forms.MenuItem()
        Me.MenuView15Raw = New System.Windows.Forms.MenuItem()
        Me.MenuView15Yield = New System.Windows.Forms.MenuItem()
        Me.MenuView16 = New System.Windows.Forms.MenuItem()
        Me.MenuView16Therm = New System.Windows.Forms.MenuItem()
        Me.MenuView16Elec = New System.Windows.Forms.MenuItem()
        Me.MenuViewUser = New System.Windows.Forms.MenuItem()
        Me.MenuViewTargets = New System.Windows.Forms.MenuItem()
        Me.MenuViewRefTarget = New System.Windows.Forms.MenuItem()
        Me.MenuViewUnitTarget = New System.Windows.Forms.MenuItem()
        Me.MenuEDCStable = New System.Windows.Forms.MenuItem()
        Me.MenuViewHist = New System.Windows.Forms.MenuItem()
        Me.MenuTools = New System.Windows.Forms.MenuItem()
        Me.MenuToolsAdmin = New System.Windows.Forms.MenuItem()
        Me.MenuToolsImport = New System.Windows.Forms.MenuItem()
        Me.MenuToolsUpload = New System.Windows.Forms.MenuItem()
        Me.mnuDashboardProfileChoice = New System.Windows.Forms.MenuItem()
        Me.MenuWindow = New System.Windows.Forms.MenuItem()
        Me.MenuHelp = New System.Windows.Forms.MenuItem()
        Me.MenuHelpHelp = New System.Windows.Forms.MenuItem()
        Me.MenuHelpAbout = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        CType(Me.pnlProgress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, -24)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.pnlProgress, Me.pnlCurrency, Me.pnlUOM})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(480, 24)
        Me.StatusBar1.TabIndex = 1
        Me.StatusBar1.Text = "StatusBar1"
        '
        'pnlProgress
        '
        Me.pnlProgress.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.pnlProgress.Name = "pnlProgress"
        Me.pnlProgress.Width = 63
        '
        'pnlCurrency
        '
        Me.pnlCurrency.Name = "pnlCurrency"
        Me.pnlCurrency.Width = 200
        '
        'pnlUOM
        '
        Me.pnlUOM.Name = "pnlUOM"
        Me.pnlUOM.Width = 200
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuFile, Me.MenuEdit, Me.MenuView, Me.MenuTools, Me.mnuDashboardProfileChoice, Me.MenuWindow, Me.MenuHelp})
        '
        'MenuFile
        '
        Me.MenuFile.Index = 0
        Me.MenuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuNew, Me.MenuOpen, Me.MenuSave, Me.MenuPrint, Me.MenuItem2, Me.MenuClose, Me.MenuExit})
        Me.MenuFile.Text = "&File"
        '
        'MenuNew
        '
        Me.MenuNew.Index = 0
        Me.MenuNew.Text = "&New Month"
        '
        'MenuOpen
        '
        Me.MenuOpen.Index = 1
        Me.MenuOpen.Text = "&Open Month"
        '
        'MenuSave
        '
        Me.MenuSave.Index = 2
        Me.MenuSave.Text = "&Save All"
        '
        'MenuPrint
        '
        Me.MenuPrint.Index = 3
        Me.MenuPrint.Text = "&Print"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 4
        Me.MenuItem2.Text = "-"
        '
        'MenuClose
        '
        Me.MenuClose.Index = 5
        Me.MenuClose.Text = "&Close Month"
        '
        'MenuExit
        '
        Me.MenuExit.Index = 6
        Me.MenuExit.Text = "&Exit"
        '
        'MenuEdit
        '
        Me.MenuEdit.Index = 1
        Me.MenuEdit.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuConf, Me.MenuData})
        Me.MenuEdit.Text = "&Edit"
        Me.MenuEdit.Visible = False
        '
        'MenuConf
        '
        Me.MenuConf.Index = 0
        Me.MenuConf.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuConfRefInfo, Me.MenuConfProcess})
        Me.MenuConf.Text = "&Configuration"
        '
        'MenuConfRefInfo
        '
        Me.MenuConfRefInfo.Index = 0
        Me.MenuConfRefInfo.Text = "&Refinery Information"
        '
        'MenuConfProcess
        '
        Me.MenuConfProcess.Index = 1
        Me.MenuConfProcess.Text = "&Process Facilities"
        '
        'MenuData
        '
        Me.MenuData.Index = 1
        Me.MenuData.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuDataProc, Me.MenuDataInv, Me.MenuDataOpex, Me.MenuDataPers, Me.MenuDataCrude, Me.MenuDataMatl, Me.MenuDataEnergy, Me.MenuDataUser})
        Me.MenuData.Text = "&Data Entry"
        '
        'MenuDataProc
        '
        Me.MenuDataProc.Index = 0
        Me.MenuDataProc.Text = "Process &Facilities"
        '
        'MenuDataInv
        '
        Me.MenuDataInv.Index = 1
        Me.MenuDataInv.Text = "&Inventory"
        '
        'MenuDataOpex
        '
        Me.MenuDataOpex.Index = 2
        Me.MenuDataOpex.Text = "&Operating Expenses"
        '
        'MenuDataPers
        '
        Me.MenuDataPers.Index = 3
        Me.MenuDataPers.Text = "&Personnel"
        '
        'MenuDataCrude
        '
        Me.MenuDataCrude.Index = 4
        Me.MenuDataCrude.Text = "&Crude Charge Detail"
        '
        'MenuDataMatl
        '
        Me.MenuDataMatl.Index = 5
        Me.MenuDataMatl.Text = "&Material Balance"
        '
        'MenuDataEnergy
        '
        Me.MenuDataEnergy.Index = 6
        Me.MenuDataEnergy.Text = "&Energy"
        '
        'MenuDataUser
        '
        Me.MenuDataUser.Index = 7
        Me.MenuDataUser.Text = "&User-Defined Inputs/Ouputs"
        '
        'MenuView
        '
        Me.MenuView.Index = 2
        Me.MenuView.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuView1, Me.MenuView2, Me.MenuView4, Me.MenuView5, Me.MenuView6, Me.MenuView7, Me.MenuView9, Me.MenuView10, Me.MenuView14, Me.MenuView15, Me.MenuView16, Me.MenuViewUser, Me.MenuViewTargets, Me.MenuEDCStable, Me.MenuViewHist})
        Me.MenuView.Text = "&View"
        Me.MenuView.Visible = False
        '
        'MenuView1
        '
        Me.MenuView1.Index = 0
        Me.MenuView1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuView1Proc, Me.MenuView1Inv})
        Me.MenuView1.Text = "Table &1"
        '
        'MenuView1Proc
        '
        Me.MenuView1Proc.Index = 0
        Me.MenuView1Proc.Text = "&Process Facilities"
        '
        'MenuView1Inv
        '
        Me.MenuView1Inv.Index = 1
        Me.MenuView1Inv.Text = "&Inventory"
        '
        'MenuView2
        '
        Me.MenuView2.Index = 1
        Me.MenuView2.Text = "Table &2"
        '
        'MenuView4
        '
        Me.MenuView4.Index = 2
        Me.MenuView4.Text = "Table &4"
        '
        'MenuView5
        '
        Me.MenuView5.Index = 3
        Me.MenuView5.Text = "Table &5"
        '
        'MenuView6
        '
        Me.MenuView6.Index = 4
        Me.MenuView6.Text = "Table &6"
        '
        'MenuView7
        '
        Me.MenuView7.Index = 5
        Me.MenuView7.Text = "Table &7"
        '
        'MenuView9
        '
        Me.MenuView9.Index = 6
        Me.MenuView9.Text = "Table &9"
        '
        'MenuView10
        '
        Me.MenuView10.Index = 7
        Me.MenuView10.Text = "Table 1&0"
        '
        'MenuView14
        '
        Me.MenuView14.Index = 8
        Me.MenuView14.Text = "&Table 14"
        '
        'MenuView15
        '
        Me.MenuView15.Index = 9
        Me.MenuView15.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuView15Raw, Me.MenuView15Yield})
        Me.MenuView15.Text = "T&able 15"
        '
        'MenuView15Raw
        '
        Me.MenuView15Raw.Index = 0
        Me.MenuView15Raw.Text = "&Raw Material Inputs"
        '
        'MenuView15Yield
        '
        Me.MenuView15Yield.Index = 1
        Me.MenuView15Yield.Text = "Product &Yields"
        '
        'MenuView16
        '
        Me.MenuView16.Index = 10
        Me.MenuView16.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuView16Therm, Me.MenuView16Elec})
        Me.MenuView16.Text = "Ta&ble 16"
        '
        'MenuView16Therm
        '
        Me.MenuView16Therm.Index = 0
        Me.MenuView16Therm.Text = "Thermal &Energy"
        '
        'MenuView16Elec
        '
        Me.MenuView16Elec.Index = 1
        Me.MenuView16Elec.Text = "E&lectricity"
        '
        'MenuViewUser
        '
        Me.MenuViewUser.Index = 11
        Me.MenuViewUser.Text = "&User-Defined Inputs/Outputs"
        '
        'MenuViewTargets
        '
        Me.MenuViewTargets.Index = 12
        Me.MenuViewTargets.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuViewRefTarget, Me.MenuViewUnitTarget})
        Me.MenuViewTargets.Text = "&Performance Targets"
        '
        'MenuViewRefTarget
        '
        Me.MenuViewRefTarget.Index = 0
        Me.MenuViewRefTarget.Text = "&Refinery Level"
        '
        'MenuViewUnitTarget
        '
        Me.MenuViewUnitTarget.Index = 1
        Me.MenuViewUnitTarget.Text = "Process &Unit Level"
        '
        'MenuEDCStable
        '
        Me.MenuEDCStable.Index = 13
        Me.MenuEDCStable.Text = "&EDC Stabilizers"
        '
        'MenuViewHist
        '
        Me.MenuViewHist.Index = 14
        Me.MenuViewHist.Text = "&Historical Maintenance Costs"
        '
        'MenuTools
        '
        Me.MenuTools.Index = 3
        Me.MenuTools.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuToolsAdmin, Me.MenuToolsImport, Me.MenuToolsUpload})
        Me.MenuTools.Text = "&Tools"
        '
        'MenuToolsAdmin
        '
        Me.MenuToolsAdmin.Index = 0
        Me.MenuToolsAdmin.Text = "&Administrative Console"
        '
        'MenuToolsImport
        '
        Me.MenuToolsImport.Index = 1
        Me.MenuToolsImport.Text = "&Import All Input Forms"
        '
        'MenuToolsUpload
        '
        Me.MenuToolsUpload.Index = 2
        Me.MenuToolsUpload.Text = "&Upload Month to Solomon"
        '
        'mnuDashboardProfileChoice
        '
        Me.mnuDashboardProfileChoice.Index = 4
        Me.mnuDashboardProfileChoice.Text = "&Dashboard"
        '
        'MenuWindow
        '
        Me.MenuWindow.Index = 5
        Me.MenuWindow.Text = "&Window"
        '
        'MenuHelp
        '
        Me.MenuHelp.Index = 6
        Me.MenuHelp.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuHelpHelp, Me.MenuHelpAbout, Me.MenuItem1})
        Me.MenuHelp.Text = "&Help"
        '
        'MenuHelpHelp
        '
        Me.MenuHelpHelp.Index = 0
        Me.MenuHelpHelp.Text = "H&elp"
        '
        'MenuHelpAbout
        '
        Me.MenuHelpAbout.Index = 1
        Me.MenuHelpAbout.Text = "&About Solomon Profile� II"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 2
        Me.MenuItem1.Text = "Diagnostics and Connectivity"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(480, 0)
        Me.Controls.Add(Me.StatusBar1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Menu = Me.MainMenu1
        Me.Name = "frmMain"
        Me.Text = "Solomon Profile� II"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.pnlProgress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlUOM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    'http://labs.developerfusion.co.uk/convert/csharp-to-vb.aspx

    Friend ActiveMainForm As Main
    'Friend LoginString
    Friend RefineryID, CompanyID, RefLocation As String

    Friend PeriodMonth, PeriodYear, CurrentMonth As Integer
    Friend PeriodDisplay As String
    Friend PeriodStart As Date

    Private YearLimit As Integer
    Friend RefineryInformationRights, ConfigRights, ProcessRights As Boolean
    Friend InvenRights, OpexRights, PersRights, CrudeRights As Boolean
    Friend MatlRights, EnergyRights, UserDefinedRights, ResultsRights As Boolean
    'Friend DBVersion As String
    Friend IsLocal As Boolean
    Private BridgeLocation As String
    Friend UserName, UserPassword As String
    Private ds_Conf As New DataSet
    Friend ds_Ref As New DataSet
    'Dim al As New RemoteSubmitServices
    'Dim ds As New DataServiceExtension


    Private _dashboard As New Dashboard
    Private _delim As Char = Chr(14)
    Friend CurrencySetting As String = String.Empty
    Friend CurrencyT15Setting As String = String.Empty
    Friend UomSetting As String = String.Empty
    Friend MethodologySetting As String = String.Empty

    'Private _dashboardToAppendTo As String = String.Empty


    ' 20081001 RRH Path - Application paths are in modAppPaths.vb
    'Public AppPath As String
    'Private RefPath As String = Application.StartupPath & "\_REF\"
    'Private ConfigPath As String = Application.StartupPath & "\_CONFIG\"
    'Private AdminPath As String = Application.StartupPath & "\_ADMIN\"

#Region " MDI FormCalls "

    ' Used for ClickOnce Installations to select the data directory
    'Private Sub selectDirectory()
    '    ' 20080818 RRH - the "Try" needs to be moved to the Key.GetRefineryID function
    '    ' 
    '    Try

    '        RefineryID = Trim(Key.GetRefineryID)

    '    Catch ex As Exception
    '        ' if GetRefineryID fails then ask to repeat slecting a directory 
    '        If MessageBox.Show("Cannot find a valid key. Do you want to select another directory?", My.Application.Info.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

    '            pathData = verEndSlash(changePathData(pathData, pathData))
    '            refreshPathData()
    '            selectDirectory() ' set the RefineryID; if fails then ask again.

    '        Else : Exit Sub

    '        End If

    '    End Try

    'End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Used for ClickOnce Installations to select the data directory
        'selectDirectory()
        T("frmMain.frmMain_Load()")
        ' 20081001 RRH Path (Removed) - AppPath = Application.StartupPath
        Try
            RefineryID = Trim(Key.GetRefineryID)
            CompanyID = Trim(Key.GetCompanyID)
            T("frmMain-DataServicesUrl")

            If Util.UseNonWCFWebService Then
                'ds.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
                'T("frmMain-ds.RefineryIs2012")
                'DBVersion = IIf(ds.RefineryIs2012(GetClientKey()), "2012", "2010")
            Else
                Dim testSvc As String = String.Empty
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                Try
                    testSvc = proxy.CheckService(Nothing)
                    If Not testSvc.Contains("Success") Then
                        MsgBox("Error connecting to Solomon Website!")
                    End If
                Catch ex As Exception
                    Dim errMsg As String = "Error connecting to Solomon Website: " & ex.Message
                    If Not IsNothing(ex.InnerException) Then
                        errMsg = errMsg & " | " & ex.InnerException.Message
                    End If
                    MsgBox(errMsg)
                End Try
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()

                'Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                'Try
                '    DBVersion = IIf(proxy.RefineryIs2012(GetClientKey()), "2012", "2010")
                'Catch ex As Exception
                '    MessageBox.Show("Error at 'proxy.RefineryIs2012()' in frmMain.Load(): " & ex.Message)
                '    Return
                'End Try
                'If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                'Dim dataSvc As DataWebSvc.DataService = Util.DataServicesObject()
                'Dim test As String = dataSvc.CheckService(GetClientKey())


                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                ''Dim proxy As New ProfileDataClient(endpointName)
                ''Dim svc As New WcfService.ProfileDataServiceClient() ' ProfileDataClient("WSHttpBinding_IProfileDataService1") '"WSHttpBinding_IProfileDataService") 'BasicHttpBinding_IProfileDataService
                'Try
                '    DBVersion = IIf(proxy.RefineryIs2012(GetClientKey()), "2012", "2010")
                'Catch ex As Exception
                '    T("RefineryIs2012() error: " & ex.Message)
                '    MsgBox("Error in frmMain_Load(): " & ex.Message)
                'Finally
                '    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                'End Try
            End If

            Dim MyLogin As New Login(Me)
            Dim result As DialogResult

            Me.Opacity = 0
            T("frmMain-MyLogin.ShowDialog")
            result = MyLogin.ShowDialog(Me)
            Select Case result
                Case Windows.Forms.DialogResult.OK
                    UserName = MyLogin.tbUsername.Text
                    UserPassword = MyLogin.tbPassword.Text
                    MyLogin.Dispose()

                    Dim permissions As ArrayList = New ArrayList
                    'IsLocal = MyLogin.chkLocalInstall.Checked
                    permissions = GetPermissions(UserName)
                    YearLimit = GetYearLimit(UserName)
                    SetUserRights(permissions)
                    SetMenus()
                    'LoginString = "user id=" & UserName & ";data source=DBS7;persist security info=False;initial catalog=Fuels;password=" & UserPassword
                    'LoginString = "user id=" & UserName & ";data source=" + ConfigurationManager.AppSettings("DatabaseName").ToString() + _
                    '    ";persist security info=False;initial catalog=" + ConfigurationManager.AppSettings("DatabaseInitialCatalog").ToString() + ";password=" & UserPassword
                    Me.Opacity = 100
                    OpenGettingStarted(False)
                Case Else
                    MyLogin.Dispose()
                    Me.Close()
            End Select
        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "frmMain_Load")
            'ProfileMsgBox("CRIT", "KEY", "")
            Me.Close()
            Exit Sub
        End Try
        T("frmMain.frmMain_Load()-End")
    End Sub

    Private Sub frmMain_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Try
            ActiveMainForm.RemoveUserFromActivity()
        Catch ex As Exception
        End Try
    End Sub

#End Region

#Region " Windows Menu Click "
    Private Sub MenuWindow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim thismain As Main
        thismain = sender.tag()
        thismain.WindowState = FormWindowState.Maximized
    End Sub

    Friend Sub AddWindowItem(ByVal main As Main)
        Dim mi As MenuItem = MenuWindow.MenuItems.Add(Format(main.PeriodStart, "y"))
        mi.Tag = main
        AddHandler mi.Click, AddressOf MenuWindow_Click
        SetMenus()
    End Sub

    Friend Sub RemoveWindowItem(ByVal dt As Date)
        Dim mi As MenuItem
        For Each mi In MenuWindow.MenuItems
            If mi.Text = Format(dt, "y") Then
                MenuWindow.MenuItems.Remove(mi)
                Exit For
            End If
        Next
        SetMenus()
    End Sub

#End Region

#Region " View Menu Items "
    Private Sub MenuView1Proc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView1Proc.Click
        ActiveMainForm.PrintPreviews("Table 1", "Process Facilities")
    End Sub

    Private Sub MenuView1Inv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView1Inv.Click
        ActiveMainForm.PrintPreviews("Table 1", "Inventory")
    End Sub

    Private Sub MenuView2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView2.Click
        ActiveMainForm.PrintPreviews("Table 2", Nothing)
    End Sub

    Private Sub MenuView4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView4.Click
        ActiveMainForm.PrintPreviews("Table 4", Nothing)
    End Sub

    Private Sub MenuView5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView5.Click
        ActiveMainForm.PrintPreviews("Table 5", Nothing)
    End Sub

    Private Sub MenuView6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView6.Click
        ActiveMainForm.PrintPreviews("Table 6", Nothing)
    End Sub

    Private Sub MenuView7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView7.Click
        ActiveMainForm.PrintPreviews("Table 7", Nothing)
    End Sub

    Private Sub MenuView9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView9.Click
        ActiveMainForm.PrintPreviews("Table 9", Nothing)
    End Sub

    Private Sub MenuView10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView10.Click
        ActiveMainForm.PrintPreviews("Table 10", Nothing)
    End Sub

    Private Sub MenuView14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView14.Click
        ActiveMainForm.PrintPreviews("Table 14", Nothing)
    End Sub

    Private Sub MenuView15Raw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView15Raw.Click
        ActiveMainForm.PrintPreviews("Table 15", "Raw Material Inputs")
    End Sub

    Private Sub MenuView15Yield_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView15Yield.Click
        ActiveMainForm.PrintPreviews("Table 15", "Product Yields")
    End Sub

    Private Sub MenuView16Therm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView16Therm.Click
        ActiveMainForm.PrintPreviews("Table 16", "Thermal Energy")
    End Sub

    Private Sub MenuView16Elec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuView16Elec.Click
        ActiveMainForm.PrintPreviews("Table 16", "Electricity")
    End Sub

    Private Sub MenuViewUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuViewUser.Click
        ActiveMainForm.PrintPreviews("User-Defined Inputs/Outputs", Nothing)
    End Sub

    Private Sub MenuViewRefTarget_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuViewRefTarget.Click
        ActiveMainForm.PrintPreviews("Performance Targets", "Refinery Level")
    End Sub

    Private Sub MenuViewUnitTarget_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuViewUnitTarget.Click
        ActiveMainForm.PrintPreviews("Performance Targets", "Process Unit Level")
    End Sub

    Private Sub MenuEDCStable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuEDCStable.Click
        ActiveMainForm.PrintPreviews("EDC Stabilizers", Nothing)
    End Sub

    Private Sub MenuViewHist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuViewHist.Click
        ActiveMainForm.PrintPreviews("Historical Maintenance Costs", Nothing)
    End Sub
#End Region

#Region " Edit Data Entry Menu Items "
    Private Sub MenuDataProc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataProc.Click
        ActiveMainForm.EditMonthlyData("Process Facilities")
    End Sub

    Private Sub MenuDataInv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataInv.Click
        ActiveMainForm.EditMonthlyData("Inventory")
    End Sub

    Private Sub MenuDataOpex_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataOpex.Click
        ActiveMainForm.EditMonthlyData("Operating Expenses")
    End Sub

    Private Sub MenuDataPers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataPers.Click
        ActiveMainForm.EditMonthlyData("Personnel")
    End Sub

    Private Sub MenuDataCrude_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataCrude.Click
        ActiveMainForm.EditMonthlyData("Crude Charge Detail")
    End Sub

    Private Sub MenuDataMatl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataMatl.Click
        ActiveMainForm.EditMonthlyData("Material Balance")
    End Sub

    Private Sub MenuDataEnergy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataEnergy.Click
        ActiveMainForm.EditMonthlyData("Energy")
    End Sub

    Private Sub MenuDataUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuDataUser.Click
        ActiveMainForm.EditMonthlyData("User-Defined Inputs/Outputs")
    End Sub
#End Region

#Region " Edit Configuration Menu Items "
    Private Sub MenuConfRefInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuConfRefInfo.Click
        ActiveMainForm.EditConfiguration("Refinery Information")
    End Sub

    Private Sub MenuConfProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuConfProcess.Click
        ActiveMainForm.EditConfiguration("Process Facilities")
    End Sub
#End Region

#Region " Tools Menu Items "

    Private Sub MenuToolsAdmin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuToolsAdmin.Click
        ActiveMainForm.SA_Admin1.Visible = True
        ActiveMainForm.SA_Admin1.BringToFront()
    End Sub

    Private Sub MenuToolsImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuToolsImport.Click
        ActiveMainForm.ImportAll()
    End Sub

    Private Sub MenuToolsRef_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim result As DialogResult = ProfileMsgBox("YN", "WAIT", "")
        If result = Windows.Forms.DialogResult.Yes Then Set_REF()
    End Sub

    Private Sub MenuToolsUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuToolsUpload.Click
        ActiveMainForm.CallUploadDataset()
    End Sub
#End Region

#Region " Privileges "
    Private Sub SetUserRights(ByVal RightsList As ArrayList)
        Dim i As Integer
        Dim intLeft As Integer = 5

        Dim Privilege As String
        For i = 0 To RightsList.Count - 1
            Privilege = RightsList(i).ToString
            Select Case Strings.Left(Privilege, intLeft)

                Case Strings.Left(strRightsAdmin, intLeft) ' 20081212 RRH use constant string rather than "Administrator - Full Access"
                    modGeneral.AdminRights = True
                    RefineryInformationRights = True 'Refinery Information
                    ConfigRights = True 'Configuration
                    ProcessRights = True 'Process Facilities
                    InvenRights = True 'Inventory
                    OpexRights = True 'Opex
                    PersRights = True 'Personnel
                    CrudeRights = True 'Crude
                    MatlRights = True 'Yield
                    EnergyRights = True 'Energy
                    UserDefinedRights = True 'User-defined
                    ResultsRights = True 'Results
                    Exit Select

                Case Strings.Left(strRightsRefinery, intLeft) : RefineryInformationRights = True : Exit Select 'Refinery Information
                Case Strings.Left(strRightsConfiguration, intLeft) : ConfigRights = True : Exit Select
                Case Strings.Left(strRightsProcess, intLeft) : ProcessRights = True : Exit Select
                Case Strings.Left(strRightsInventory, intLeft) : InvenRights = True : Exit Select
                Case Strings.Left(strRightsOpEx, intLeft) : OpexRights = True : Exit Select
                Case Strings.Left(strRightsPersonnel, intLeft) : PersRights = True : Exit Select
                Case Strings.Left(strRightsCrude, intLeft) : CrudeRights = True : Exit Select
                Case Strings.Left(strRightsMaterial, intLeft) : MatlRights = True : Exit Select
                Case Strings.Left(strRightsEnergy, intLeft) : EnergyRights = True : Exit Select
                Case Strings.Left(strRightsUser, intLeft) : UserDefinedRights = True : Exit Select
                Case Strings.Left(strRightsView, intLeft) : ResultsRights = True : Exit Select

            End Select
        Next
    End Sub

    Friend Sub SetMenus()
        Dim mi As MenuItem
        Dim HasChildren As Boolean
        For Each mi In MenuWindow.MenuItems
            HasChildren = True
            Exit For
        Next

        MenuEdit.Visible = HasChildren
        MenuView.Visible = HasChildren
        MenuHelp.Visible = HasChildren
        MenuSave.Visible = HasChildren
        MenuPrint.Visible = HasChildren
        MenuToolsImport.Visible = HasChildren
        MenuToolsUpload.Visible = HasChildren
        MenuToolsAdmin.Visible = HasChildren
        MenuClose.Visible = HasChildren
        If Not HasChildren Then
            pnlCurrency.Text = ""
            pnlUOM.Text = ""
            pnlProgress.Text = ""
        End If

        MenuNew.Enabled = AdminRights
        MenuWindow.Visible = HasChildren
        If AdminRights = False Or HasChildren = False Then
            MenuTools.Visible = False
        Else
            MenuTools.Visible = True
        End If
    End Sub


#End Region

#Region " LookupData - Reference "
    Private Sub Get_REF()

        ' 20081001 RRH Path - Dim fld As New System.IO.DirectoryInfo(RefPath)
        Dim fld As New System.IO.DirectoryInfo(pathRef)
        Dim fil As System.IO.FileInfo

        If fld.Exists = False Then fld.Create()
        ds_Ref.Clear()

        Try
            ' 20081001 RRH Path - ds_Ref.ReadXml(RefPath & "Reference.xml", XmlReadMode.ReadSchema)
            ds_Ref.ReadXml(pathRef & "Reference.xml", XmlReadMode.ReadSchema)
        Catch ex As Exception
            For Each fil In fld.GetFiles
                fil.Delete()
            Next
            Set_REF()
        End Try
    End Sub

    'Private Sub W1(msg As String)
    '    Try
    '        Using writer As StreamWriter = New StreamWriter("C:\Temp\templog.txt", True)
    '            writer.WriteLine(msg)
    '        End Using
    '    Catch
    '    End Try
    'End Sub

    Friend Sub Set_REF()
        T("Entering Set_REf")
        Dim Lookup As New LookupTablesQueries
        ' Lookup.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
        T("Dimmed lookup")
        InstalledCertificate()
        T("After installedcert")
        HasNecessaryDownloadCredentials()
        T("aftet HasNeceessare")
        ds_Ref.Clear()
        T("after dsref.Clear")
        Try
            pnlProgress.Text = "Downloading reference data"

            If Util.UseNonWCFWebService Then
                'ds_Ref = Lookup.GetLookups(GetClientKey(), CompanyID)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                Dim result As String = String.Empty
                Try
                    T("Before GetLookups")
                    ds_Ref = proxy.GetLookups(CompanyID, Nothing)
                    'ds_Ref = svc.GetLookups(RefineryID, CompanyID)
                    'ds_Ref.WriteXml("C:\temp\ds_Ref.xml")
                    T("After GetLookups")
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                End Try
            End If


            ' 20081001 RRH Path - ds_Ref.WriteXml(RefPath & "Reference.xml", XmlWriteMode.WriteSchema)
            'W("BEfore file.delete")
            'W(pathRef)
            File.Delete(pathRef & "Reference.xml")
            'W("Before writexml")
            'W(ds_Ref.Tables.Count.ToString())
            ds_Ref.WriteXml(pathRef & "Reference.xml", XmlWriteMode.WriteSchema)

            'UpdateActivity("Downloaded Solomon reference data")
            pnlProgress.Text = "Ready"

            ''DWW 20111115 for checking
            'Dim rowIdx As Integer = 0
            'For Each dt As DataTable In ds_Ref.Tables
            '    Console.Write("Table " & Convert.ToString(rowIdx) & " :Name = " & dt.TableName & ": Row count = " & dt.Rows.Count)
            '    Console.Write(Environment.NewLine)
            '    rowIdx = rowIdx + 1
            'Next
            Me.Cursor = Cursors.Default

        Catch ex As Exception

            'this is probably because ds_Ref is Nothing

            Exceptions.Display("Critical Error", ex, "Set_REF")
            Me.Close()
            Exit Sub
        End Try
    End Sub

    Friend Sub HasNecessaryDownloadCredentials()
        If AdminRights = False Or IsLocal Then
            Dim ex As New Exception("INVALID_CREDENTIALS")
            Throw ex
        End If
    End Sub

    Friend Sub InstalledCertificate()
        If AppCertificate.IsCertificateInstall = False Then
            Dim ex As New Exception("NO_CERTIFICATE")
            Throw ex
        End If
    End Sub

    Friend Sub CallExceptionManager(ByVal ex As Exception)
        Me.ControlBox = True
        If TypeOf ex Is System.Net.WebException Then
            ProfileMsgBox("CRIT", "CONN", "")
        Else
            Select Case ex.Message
                Case "INVALID_CREDENTIALS", "NO_CERTIFICATE"
                    ProfileMsgBox("CRIT", ex.Message, "")
                Case Else
                    ProfileMsgBox("CRIT", "", ex.Message)
            End Select
        End If
        Me.Cursor = Cursors.Default
        pnlProgress.Text = "Ready"
    End Sub
#End Region

#Region " LookupData - Configuration"
    Private Sub Get_Conf()
        ' 20081001 RRH Path - Dim fld As New System.IO.DirectoryInfo(ConfigPath)
        Dim fld As New System.IO.DirectoryInfo(pathConfig)
        Dim fil As System.IO.FileInfo
        Dim i As Integer
        Try
            If fld.Exists = False Then fld.Create()
            ds_Conf.Clear()

            For Each fil In fld.GetFiles
                If fil.Extension = ".xml" And fil.Name <> "Diagnostics.xml" Then
                    ds_Conf.ReadXml(fil.FullName, XmlReadMode.ReadSchema)
                    i = i + 1
                End If
            Next

            If i = 0 Then Set_Conf()
        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "Get_Conf")
            Me.Close()
            Exit Sub
        End Try
    End Sub
    Private Sub WriteLog(st As String)
        'This doesn't really help
        'Using sw As StreamWriter = New StreamWriter("ws.log", True)
        '        sw.WriteLine(Now.ToString & ": " & st)
        'sw.Flush()
        'End Using
    End Sub
    Friend Sub Set_Conf()

        If Util.UseNonWCFWebService Then
            Dim Lookup As New RefTablesQueries
            'Lookup.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
            Try
                InstalledCertificate()
                HasNecessaryDownloadCredentials()
                Me.Cursor = Cursors.WaitCursor
                pnlProgress.Text = "Downloading refinery configuration data"
                ds_Conf.Clear()
                WriteLog("Set_Conf")
                ds_Conf = Lookup.GetReferences(GetClientKey())

                'ds_Conf.WriteXml("C:\ds_Conf.xml")


                SaveConfiguration()
                'UpdateActivity("Downloaded refinery configuration data")

                Dim rowIdx As Integer = 0
                For Each dt As DataTable In ds_Conf.Tables
                    Console.Write("Table " & Convert.ToString(rowIdx) & " :Name = " & dt.TableName & ": Row count = " & dt.Rows.Count)
                    Console.Write(Environment.NewLine)
                    rowIdx = rowIdx + 1
                Next

                pnlProgress.Text = "Ready"
                Me.Cursor = Cursors.Default
            Catch ex As Exception
                Exceptions.Display("Critical Error", ex, "Set_Conf")
                Me.Close()
                Exit Sub
            End Try
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()

            Dim result As String = String.Empty
            Dim erred As Boolean = False
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
            Try
                'ds = proxy.GetDataByPeriod(sdt, edt, RefineryID)
                Me.Cursor = Cursors.WaitCursor
                pnlProgress.Text = "Downloading refinery configuration data"
                ds_Conf.Clear()
                WriteLog("Set_Conf")
                ds_Conf = proxy.GetReferences(Nothing)
                'ds_Conf = svc.GetReferences(RefineryID)

                'ds_Conf.WriteXml("C:\ds_Conf.xml")


                SaveConfiguration()
                'UpdateActivity("Downloaded refinery configuration data")

                Dim rowIdx As Integer = 0
                For Each dt As DataTable In ds_Conf.Tables
                    Console.Write("Table " & Convert.ToString(rowIdx) & " :Name = " & dt.TableName & ": Row count = " & dt.Rows.Count)
                    Console.Write(Environment.NewLine)
                    rowIdx = rowIdx + 1
                Next

                pnlProgress.Text = "Ready"
                Me.Cursor = Cursors.Default

            Catch ex As Exception
                erred = True
                Exceptions.Display("Critical Error", ex, "Set_Conf")
            Finally
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                If erred Then
                    Me.Close()
                End If
            End Try
        End If

    End Sub

    Friend Sub UpdateTA_XmlOnly()
        'process.xml ??? and UnitList.xml???


        Dim Lookup As New RefTablesQueries
        Dim dsTemp As DataSet
        If Util.UseNonWCFWebService Then
            'Lookup.Url = ConfigurationManager.AppSettings("DataServicesUrl").ToString()
        End If
        Try
            If Util.UseNonWCFWebService Then
                InstalledCertificate()
                HasNecessaryDownloadCredentials()
            End If
            Me.Cursor = Cursors.WaitCursor
            pnlProgress.Text = "Downloading refinery configuration data"
            'dsTemp.Clear()
            WriteLog("Set_Conf")

            If Util.UseNonWCFWebService Then
                dsTemp = Lookup.GetReferences(GetClientKey())
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim result As String = String.Empty
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                Try
                    'Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                    dsTemp = proxy.GetReferences(Nothing)
                    'dsTemp = svc.GetReferences(RefineryID)
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                End Try
            End If
            Dim dsProcess As New DataSet
            dsProcess.ReadXml(pathConfig & "Process.xml")
            dsProcess.Tables.Remove("MaintTA")
            dsProcess.Tables.Add(dsTemp.Tables("MaintTA_Process").Copy)
            dsProcess.Tables("MaintTA_Process").TableName = "MaintTA"

            If ds_Conf.Tables.Contains("MaintTA_Other") Then
                ds_Conf.Tables.Remove("MaintTA_Other")
            End If
            ds_Conf.Tables.Add(dsTemp.Tables("MaintTA_Other").Copy)
            'ds_Conf.Tables("MaintTA").TableName = "MaintTA"
            dsProcess.Merge(ds_Conf.Tables("MaintTA_Other"))
            File.Delete(pathConfig & "Process.xml")
            dsProcess.WriteXml(pathConfig & "Process.xml", XmlWriteMode.WriteSchema)

            Dim dsUnitList As New DataSet
            dsUnitList.Tables.Add(dsTemp.Tables("UnitList").Copy)
            ds_Conf.Tables.Remove("UnitList")
            ds_Conf.Tables.Add(dsTemp.Tables("UnitList").Copy)
            File.Delete(pathConfig & "UnitList.xml")
            dsUnitList.WriteXml(pathConfig & "UnitList.xml", XmlWriteMode.WriteSchema)

            Dim rowIdx As Integer = 0
            For Each dt As DataTable In ds_Conf.Tables
                Console.Write("Table " & Convert.ToString(rowIdx) & " :Name = " & dt.TableName & ": Row count = " & dt.Rows.Count)
                Console.Write(Environment.NewLine)
                rowIdx = rowIdx + 1
            Next

            pnlProgress.Text = "Ready"
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "UpdateTA_XmlOnly")
            Me.Close()
            Exit Sub
        End Try
    End Sub


    Friend Sub SaveConfiguration()

        'Process includes: Configuration, Process Data, Turnaround Maintenance, Non-Turnaround Maintenance and Performance Indicators
        Dim dsProcess As New DataSet
        Try
            dsProcess.Tables.Add(ds_Conf.Tables("Config").Copy)
            dsProcess.Tables.Add(ds_Conf.Tables("ProcessData").Copy)

            dsProcess.Tables.Add(ds_Conf.Tables("MaintTA_Process").Copy)
            dsProcess.Tables("MaintTA_Process").TableName = "MaintTA"
            ds_Conf.Tables("MaintTA_Other").TableName = "MaintTA"
            dsProcess.Merge(ds_Conf.Tables("MaintTA"))
            dsProcess.Tables.Add(ds_Conf.Tables("MaintRout").Copy)
            dsProcess.Tables.Add(ds_Conf.Tables("UnitTargetsNew").Copy)

            Dim dtrow As DataRow
            Dim tmpstr As String
            Dim cmonth, cyear As Integer

            ds_Conf.Tables("LoadedMonths").Columns.Add("PeriodStart", GetType(Date))
            For Each dtrow In ds_Conf.Tables("LoadedMonths").Rows
                cmonth = CInt(dtrow!PeriodMonth)
                cyear = CInt(dtrow!PeriodYear)
                Dim sdt As New Date(cyear, cmonth, 1)
                tmpstr = Format(sdt, "MMMM yyyy")
                dtrow!PeriodStart = sdt
            Next

            Dim dsLoadedMonths As New DataSet
            dsLoadedMonths.Tables.Add(ds_Conf.Tables("LoadedMonths").Copy)

            Dim dsUnitList As New DataSet
            dsUnitList.Tables.Add(ds_Conf.Tables("UnitList").Copy)

            Dim dsSettings As New DataSet
            dsSettings.Tables.Add(ds_Conf.Tables("Settings").Copy)
            dsSettings.Tables(0).Rows(0)!BridgeLocation = BridgeLocation

            'Refinery Information includes: Performance Targets, Inventory, EDCStabilizers, and Historical Maintenance Costs
            Dim dsRefineryInformation As New DataSet
            dsRefineryInformation.Tables.Add(ds_Conf.Tables("RefTargets").Copy)
            dsRefineryInformation.Tables.Add(ds_Conf.Tables("Inventory").Copy)
            dsRefineryInformation.Tables.Add(ds_Conf.Tables("ConfigRS").Copy)
            dsRefineryInformation.Tables.Add(ds_Conf.Tables("EDCStabilizers").Copy)
            dsRefineryInformation.Tables.Add(ds_Conf.Tables("MaintRoutHist").Copy)

            'Inventory
            Dim row, newrow As DataRow
            Dim dv As New DataView(dsRefineryInformation.Tables("Inventory"))
            Dim TankType As String
            For Each row In ds_Ref.Tables("TankType_LU").Rows
                TankType = row!TankType.ToString
                dv.RowFilter = "TankType = '" & TankType & "'"
                If dv.Count = 0 Then
                    newrow = dsRefineryInformation.Tables("Inventory").NewRow
                    newrow!TankType = TankType
                    dsRefineryInformation.Tables("Inventory").Rows.Add(newrow)
                End If
            Next

            dv.Table = dsRefineryInformation.Tables("ConfigRS")
            dv.RowFilter = "ProcessID = 'RSCRUDE'"
            If dv.Count = 0 Then
                newrow = dsRefineryInformation.Tables("ConfigRS").NewRow
                newrow!ProcessID = "RSCRUDE"
                dsRefineryInformation.Tables("ConfigRS").Rows.Add(newrow)
            End If
            dv.RowFilter = "ProcessID = 'RSPROD'"
            If dv.Count = 0 Then
                newrow = dsRefineryInformation.Tables("ConfigRS").NewRow
                newrow!ProcessID = "RSPROD"
                dsRefineryInformation.Tables("ConfigRS").Rows.Add(newrow)
            End If

            'Opex
            Dim dsOpex As New DataSet
            dsOpex.Tables.Add(ds_Conf.Tables("OpExAll").Copy)

            'Personnel
            Dim dsPersonnel As New DataSet
            dsPersonnel.Tables.Add(ds_Conf.Tables("Pers").Copy)
            dsPersonnel.Tables.Add(ds_Conf.Tables("Absence").Copy)

            dv.Table = dsPersonnel.Tables("Pers")
            Dim PersID As String
            For Each row In ds_Ref.Tables("Pers_LU").Rows
                PersID = row!PersID.ToString
                dv.RowFilter = "PersID = '" & PersID & "'"
                If dv.Count = 0 Then
                    newrow = dsPersonnel.Tables("Pers").NewRow
                    newrow!PersID = PersID
                    dsPersonnel.Tables("Pers").Rows.Add(newrow)
                End If
            Next

            dv.Table = dsPersonnel.Tables("Absence")
            Dim CategoryID As String
            For Each row In ds_Ref.Tables("Absence_LU").Rows
                CategoryID = row!CategoryID.ToString
                dv.RowFilter = "CategoryID = '" & CategoryID & "'"
                If dv.Count = 0 Then
                    newrow = dsPersonnel.Tables("Absence").NewRow
                    newrow!CategoryID = CategoryID
                    dsPersonnel.Tables("Absence").Rows.Add(newrow)
                End If
            Next

            'Crude
            Dim dsCrude As New DataSet
            dsCrude.Tables.Add(ds_Conf.Tables("Crude").Copy)

            'Yield
            Dim dsYield As New DataSet
            dsYield.Tables.Add(ds_Conf.Tables("Yield_RM").Copy)
            dsYield.Tables.Add(ds_Conf.Tables("Yield_RMB").Copy)
            dsYield.Tables.Add(ds_Conf.Tables("Yield_Prod").Copy)

            'Energy
            Dim dsEnergy As New DataSet
            dsEnergy.Tables.Add(ds_Conf.Tables("Energy").Copy)
            dsEnergy.Tables.Add(ds_Conf.Tables("Electric").Copy)

            'UserDefined
            Dim dsUserDefined As New DataSet
            dsUserDefined.Tables.Add(ds_Conf.Tables("UserDefined").Copy)

            dsProcess.WriteXml(pathConfig & "Process.xml", XmlWriteMode.WriteSchema)
            dsLoadedMonths.WriteXml(pathConfig & "LoadedMonths.xml", XmlWriteMode.WriteSchema)
            dsUnitList.WriteXml(pathConfig & "UnitList.xml", XmlWriteMode.WriteSchema)
            dsSettings.WriteXml(pathConfig & "Settings.xml", XmlWriteMode.WriteSchema)
            dsRefineryInformation.WriteXml(pathConfig & "RefineryInformation.xml", XmlWriteMode.WriteSchema)
            dsOpex.WriteXml(pathConfig & "Opex.xml", XmlWriteMode.WriteSchema)
            dsPersonnel.WriteXml(pathConfig & "Personnel.xml", XmlWriteMode.WriteSchema)
            dsCrude.WriteXml(pathConfig & "Crude.xml", XmlWriteMode.WriteSchema)
            dsYield.WriteXml(pathConfig & "Yield.xml", XmlWriteMode.WriteSchema)
            dsEnergy.WriteXml(pathConfig & "Energy.xml", XmlWriteMode.WriteSchema)
            dsUserDefined.WriteXml(pathConfig & "UserDefined.xml", XmlWriteMode.WriteSchema)


            Try
                CurrencySetting = dsSettings.Tables(0).Rows(0)("RptCurrency").ToString()
                CurrencyT15Setting = dsSettings.Tables(0).Rows(0)("RptCurrencyT15").ToString()
                UomSetting = dsSettings.Tables(0).Rows(0)("UOM").ToString()
                MethodologySetting = dsSettings.Tables(0).Rows(0)("ReportFactorSet").ToString()
            Catch
            End Try

            pnlProgress.Text = "Ready"
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "SaveConfiguration")
            'Me.Close()
            Exit Sub
        End Try
    End Sub
#End Region

#Region " LookupData - Monthly Data "
    Private Sub Get_Month(ByVal sdt As Date, clientKey As String, currency As String)
        ' 20081001 RRH Path - Dim fld As New System.IO.DirectoryInfo(AppPath & "\" & Format(sdt, "yyyyMM"))
        Dim fld As New System.IO.DirectoryInfo(pathMonth & "\" & Format(sdt, "yyyyMM"))
        Dim fil As System.IO.FileInfo
        Dim i As Integer
        Try
            If fld.Exists = False Then
                Set_Month(sdt, clientKey, currency)
            Else
                For Each fil In fld.GetFiles
                    i = i + 1
                Next
                If i = 0 Then Set_Month(sdt, clientKey, currency)
            End If
        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "Get_Month")
            Me.Close()
            Exit Sub
        End Try
    End Sub

    Friend Sub Set_Month(ByVal sdt As Date, clientKey As String, currency As String)
        T("ENtering Set_Month()")
        HasNecessaryDownloadCredentials()
        T("  Set_Month() 01")
        If Util.UseNonWCFWebService Then
            'Dim Lookup As New SubmitServices2

        Else
            Dim ds As New DataSet
            T("  Set_Month() 02")
            Dim edt As Date = DateAdd(DateInterval.Month, 1, sdt)
            Try
                T("  Set_Month() 03")
                pnlProgress.Text = "Downloading monthly data: " & Format(sdt, "y")
                'ds = Lookup.GetDataByPeriod(sdt, edt, clientKey, currency)
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim result As String = String.Empty
                T("  Set_Month() 04")
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                Try
                    'Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                    T("  Set_Month() 05")
                    ds = proxy.GetDataByPeriod(sdt, edt, Nothing)
                    'ds.WriteXml("C:\Temp\GetMonth.xml")
                    'ds = svc.GetDataByPeriod(sdt, edt, RefineryID)
                Catch ex As Exception
                    Throw ex
                Finally
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                End Try
                'ds.WriteXml("C:\temp\GetdataByPeriod.xml")
                T("  Set_Month() 06")
                SaveMonth(ds, sdt)
                T("  Set_Month() 07")
                pnlProgress.Text = "Ready"
                Me.Cursor = Cursors.Default
            Catch ex As Exception
                Exceptions.Display("Critical Error", ex, "Set_Month")
                Me.Close()
                Exit Sub
            End Try

        End If



        T("Exiting Set_Month() ")
    End Sub

    Friend Sub SaveMonth(ByVal ds As DataSet, ByVal sdt As Date)
        T("Entering SaveMonth()")
        Dim edt As Date = DateAdd(DateInterval.Month, 1, sdt)

        ' 20081001 RRH Path - Dim MonthPath As String = AppPath & "\" & Format(sdt, "yyyyMM")
        Dim MonthPath As String = pathMonth & Format(sdt, "yyyyMM")
        Dim fld As New System.IO.DirectoryInfo(MonthPath)

        Try
            If fld.Exists = False Then fld.Create()

            MonthPath = MonthPath & "\"

            Dim tmpds As New DataSet

            'dsSettings
            tmpds.Tables.Add(ds.Tables("Settings").Copy)

            If ds.Tables("Settings").Rows.Count < 1 Then
                'this probably means the submission which data was requested for was damaged...
                Exceptions.Display("Error", New System.Exception("Settings table is empty. Profile will try to continue."), "Save_Month")
            End If
            Try
                tmpds.Tables(0).Rows(0)!BridgeLocation = BridgeLocation
            Catch exBridgeFile As Exception
                Dim err As String = "Error in SaveMonth: "
                If BridgeLocation Is Nothing Then
                    err = err & "BridgeLocation is Nothing"
                Else
                    err = err & exBridgeFile.Message
                End If
                err = err & ". Profile will now try to continue."
                Exceptions.Display("Error", New System.Exception(err), "Save_Month")
            End Try
            tmpds.WriteXml(MonthPath & "Settings.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsProcess
            Dim dsProcess As New DataSet
            tmpds.Tables.Add(ds.Tables("Config").Copy)
            tmpds.Tables.Add(ds.Tables("ProcessData").Copy)
            tmpds.Tables.Add(ds.Tables("MaintTA_Process").Copy)
            tmpds.Tables("MaintTA_Process").TableName = "MaintTA"
            ds.Tables("MaintTA_Other").TableName = "MaintTA"
            tmpds.Merge(ds.Tables("MaintTA"))
            tmpds.Tables.Add(ds.Tables("MaintRout").Copy)
            tmpds.Tables.Add(ds.Tables("UnitTargetsNew").Copy)
            tmpds.WriteXml(MonthPath & "Process.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsRefineryInformation
            tmpds.Tables.Add(ds.Tables("Inventory").Copy)
            tmpds.Tables.Add(ds.Tables("ConfigRS").Copy)
            tmpds.Tables.Add(ds.Tables("EDCStabilizers").Copy)
            tmpds.Tables.Add(ds.Tables("RefTargets").Copy)
            tmpds.Tables.Add(ds.Tables("MaintRoutHist").Copy)
            tmpds.WriteXml(MonthPath & "RefineryInformation.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsOpEx
            tmpds.Tables.Add(ds.Tables("OpExAll").Copy)
            tmpds.WriteXml(MonthPath & "OpEx.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsPersonnel
            tmpds.Tables.Add(ds.Tables("Pers").Copy)
            tmpds.Tables.Add(ds.Tables("Absence").Copy)
            tmpds.WriteXml(MonthPath & "Personnel.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsCrude
            tmpds.Tables.Add(ds.Tables("Crude").Copy)
            tmpds.WriteXml(MonthPath & "Crude.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsYield
            tmpds.Tables.Add(ds.Tables("Yield_RM").Copy)
            tmpds.Tables.Add(ds.Tables("Yield_RMB").Copy)
            tmpds.Tables.Add(ds.Tables("Yield_Prod").Copy)
            tmpds.WriteXml(MonthPath & "Yield.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsEnergy
            tmpds.Tables.Add(ds.Tables("Energy").Copy)
            tmpds.Tables.Add(ds.Tables("Electric").Copy)
            tmpds.WriteXml(MonthPath & "Energy.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()

            'dsUserDefined
            tmpds.Tables.Add(ds.Tables("UserDefined").Copy)
            tmpds.WriteXml(MonthPath & "UserDefined.xml", XmlWriteMode.WriteSchema)
            tmpds.Tables.Clear()
            T("Finished SaveMonth()")
        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "Save_Month")
            Me.Close()
            Exit Sub
        End Try
        T("Exiting SaveMonth()")
    End Sub

    Friend Function SubmitMonthlyData(ByVal ds As DataSet) As Boolean

        If Util.UseNonWCFWebService Then
            'Try
            '    Dim Lookup As New SubmitServices2
            '    Lookup.Url = ConfigurationManager.AppSettings("SubmitServicesUrl").ToString()
            '    Lookup.SubmitRefineryData(ds, GetClientKey())
            '    Return True
            'Catch ex As Exception
            '    Exceptions.Display("Critical Error", ex, "SubmitMonthlyData")
            '    'Me.Close()
            '    Exit Function
            'End Try
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName) '  ServiceReference1.ProfileReportServiceClient()
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim result As String = String.Empty
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
            Try
                'Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                proxy.SubmitRefineryData(ds, Nothing)
                'svc.SubmitRefineryData(ds, RefineryID)
                Return True
            Catch ex As Exception
                Exceptions.Display("Critical Error", ex, "SubmitMonthlyData")
                'Me.Close()
                Exit Function
            Finally
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
            End Try
        End If
    End Function
#End Region

#Region " Help Menu "
    Private Sub MenuHelpAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuHelpAbout.Click
        ActiveMainForm.gbAbout.Visible = True
        ActiveMainForm.pnlBlank.BringToFront()
    End Sub

    Private Sub MenuHelpHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuHelpHelp.Click
        ActiveMainForm.CallHelp()
    End Sub
#End Region

#Region " File Menu Items "

    Private Sub MenuNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuNew.Click
        CreateNewMonth()
    End Sub

    Private Sub MenuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuOpen.Click
        OpenGettingStarted(True)
    End Sub

    Private Sub MenuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuSave.Click
        ActiveMainForm.SaveAll()
    End Sub

    Private Sub MenuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuPrint.Click
        ActiveMainForm.SetPrintControl()
    End Sub

    Private Sub MenuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuClose.Click
        ActiveMainForm.Close()
    End Sub

    Private Sub MenuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuExit.Click
        Me.Close()
    End Sub

    Private Sub CreateNewMonth(Optional NewDate As Date = Nothing)

        Try

            Dim dt As Date

            If NewDate = Nothing Then
                Dim dv As New DataView(ds_Conf.Tables("LoadedMonths"))
                dv.Sort = "PeriodStart DESC"

                ' 20100414D00 RRH added Strings.Left(*, Strings.InStr(dv.Item(0)!PeriodStart.ToString, " ", CompareMethod.Binary) - 1) to code to retrieve only date part
                dt = CDate(Strings.Left(dv.Item(0)!PeriodStart.ToString, Strings.InStr(dv.Item(0)!PeriodStart.ToString, " ", CompareMethod.Binary) - 1))
                dt = DateAdd(DateInterval.Month, 1, dt)
            Else
                'Create Gap Date
                dt = NewDate
            End If

            Dim row As DataRow = ds_Conf.Tables("LoadedMonths").NewRow
            row!PeriodMonth = Month(dt)
            row!PeriodYear = Year(dt)
            row!PeriodStart = dt
            ds_Conf.Tables("LoadedMonths").Rows.Add(row)
            Dim tmpds As New DataSet
            tmpds.Tables.Add(ds_Conf.Tables("LoadedMonths").Copy)

            ' 20081001 RRH Path - tmpds.WriteXml(ConfigPath & "LoadedMonths.xml", XmlWriteMode.WriteSchema)
            tmpds.WriteXml(pathConfig & "LoadedMonths.xml", XmlWriteMode.WriteSchema)

            PeriodMonth = Month(dt)
            PeriodYear = Year(dt)
            PeriodStart = dt
            CurrentMonth = CInt(Format(dt, "yyyyMM"))

            Dim tempto, tempfrom As String
            ' 20081001 RRH Path - tempto = AppPath & "\" & Format(dt, "yyyyMM") & "\"
            tempto = pathMonth & Format(dt, "yyyyMM") & "\"
            Dim cDirTo As DirectoryInfo = New DirectoryInfo(tempto)
            If cDirTo.Exists = False Then cDirTo.Create()

            ' 20081001 RRH Path - tempfrom = AppPath & "\_CONFIG\"
            tempfrom = pathConfig
            Dim cDir As DirectoryInfo = New DirectoryInfo(tempfrom)
            Dim cfile As FileInfo

            For Each cfile In cDir.GetFiles
                cfile.CopyTo(Path.Combine(tempto, cfile.Name))
            Next

            Dim MyMain As New Main(Me)
            ActiveMainForm = MyMain

            'Dim key As New Key
            MyMain.RefineryID = Key.GetRefineryID()

            AddWindowItem(MyMain)
            SetMenus()
            MyMain.Show()
            MyMain.HideWSCtrls()
            ProfileMsgBox("INFO", "NEW", Format(dt, "y"))
            MyMain.SaveInfo()

        Catch ex As Exception

            Exceptions.Display("Critical Error", ex, "CreateNewMonth")
            Me.Close()
            Exit Sub

        End Try

    End Sub

    Public Function SubscriptionIsActive() As Boolean
        Dim ds As New DataSet
        Try
            Dim active As Nullable(Of Integer)
            ds.ReadXml(pathConfig & "Settings.xml")
            active = ds.Tables(0).Rows(0)("Active")
            If IsNull(active) OrElse active <> 1 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub SetBridgeLocation()
        Dim ds As New DataSet
        Try
            ' 20081001 RRH Path - ds.ReadXml(ConfigPath & "Settings.xml")
            ds.ReadXml(pathConfig & "Settings.xml")
            BridgeLocation = ds.Tables(0).Rows(0)!BridgeLocation.ToString
        Catch ex As Exception
            BridgeLocation = ""
        End Try
    End Sub

    Private Function GetCurrencyFromSettings() As String
        Dim ds As New DataSet
        Try
            ds.ReadXml(pathConfig & "Settings.xml")
            Return ds.Tables(0).Rows(0)!RptCurrency.ToString
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Sub RefreshLocalFiles()
        Me.Cursor = Cursors.WaitCursor
        Try
            ' 20081001 RRH Path - Dim Dir As String() = Directory.GetDirectories(AppPath)
            Dim Dir As String() = Directory.GetDirectories(pathStartUp)
            Dim TmpDir As String
            Dim i As Integer
            For i = 0 To Dir.Length - 1
                ' 20081001 RRH Path - TmpDir = Dir(i).Replace(AppPath & "\", "")
                TmpDir = Dir(i).Replace(pathStartUp, "")
                If IsNumeric(TmpDir) Then Directory.Delete(Dir(i), True)
            Next

            'DWW 20111115 - Removed legacy code
            CallReference()
            CallConfig()


        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "RefreshLocalFiles")
            Me.Close()
            Exit Sub
        End Try
    End Sub

    Friend Sub RemoveLocalXmlFiles()
        'CallReference()
        ds_Ref.Clear()

        'CallConfig()
        File.Delete(pathConfig & "Settings.xml")
        ds_Conf.Clear()

        File.Delete(pathConfig & "LoadedMonths.xml")
        File.Delete(pathConfig & "UnitList.xml")
        File.Delete(pathConfig & "Settings.xml")
        File.Delete(pathConfig & "RefineryInformation.xml")
        File.Delete(pathConfig & "Opex.xml")
        File.Delete(pathConfig & "Personnel.xml")
        File.Delete(pathConfig & "Crude.xml")
        File.Delete(pathConfig & "Yield.xml")
        File.Delete(pathConfig & "Energy.xml")
        File.Delete(pathConfig & "UserDefined.xml")

        File.Delete(pathConfig & "Process.xml")
    End Sub

    Friend Sub DeleteFoldersWithDateNames()
        Dim Dir As String() = Directory.GetDirectories(pathStartUp)
        Dim TmpDir As String
        Dim i As Integer
        For i = 0 To Dir.Length - 1
            Try
                TmpDir = Dir(i).Replace(pathStartUp, "")
                If IsNumeric(TmpDir) Then Directory.Delete(Dir(i), True)
            Catch ex As Exception

            End Try
        Next
    End Sub


    Private Sub GetLookupData()
        Try
            ''Dim Lookup As New LookupTablesQueries
            Dim profileManager As New ProfileBusinessManager.Import()

            ds_Ref.Clear()

            pnlProgress.Text = "Downloading reference data"
            ds_Ref = profileManager.GetAllData()
            ' 20081001 RRH Path - ds_Ref.WriteXml(RefPath & "Reference.xml", XmlWriteMode.WriteSchema)
            ds_Ref.WriteXml(pathRef & "Reference.xml", XmlWriteMode.WriteSchema)
            'UpdateActivity("Downloaded Solomon reference data")
            pnlProgress.Text = "Ready"
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "GetLookupData")
            Me.Close()
            Exit Sub
        End Try
    End Sub

    Private Sub CallReference()
        Set_REF()
    End Sub

    Private Sub UpdateActivity(ByVal txt As String)
        'Dim dsActivity As New DataSet
        'Try

        '    If File.Exists(pathAdmin & "Activity.xml") Then
        '        dsActivity.ReadXml(pathAdmin & "Activity.xml")
        '        Dim row As DataRow = dsActivity.Tables("Events").NewRow
        '        row!EventDate = Now
        '        row!EventDescription = txt
        '        row!EventUser = UCase(UserName)
        '        dsActivity.Tables("Events").Rows.Add(row)
        '        dsActivity.WriteXml(pathAdmin & "Activity.xml", XmlWriteMode.WriteSchema)
        '    Else
        '        File.Open(pathAdmin & "Activity.xml", FileMode.CreateNew, FileAccess.Write)
        '    End If
        'Catch ex As Exception
        '    Exceptions.Display("Critical Error", ex, "UpdateActivity")
        '    Me.Close()
        '    Exit Sub
        'End Try
    End Sub

    Private Sub CallConfig()
        SetBridgeLocation()
        Set_Conf()
    End Sub

    Private Sub OpenGettingStarted(ByVal IsOpen As Boolean)
        Dim MyMonth As New OpenMonth(Me)
        Dim WasDownloaded As Boolean

        AddHandler MyMonth.RefreshLocalFiles, AddressOf RefreshLocalFiles

        'Testing SubmitServices.GetInputData
        'Dim dd As New Solomon.SubmitDev.SubmitServices
        'Dim dds As DataSet = dd.GetInputData(RefineryID)

        Try

            Get_REF()
            Get_Conf()

            Dim dv As New DataView(ds_Conf.Tables("LoadedMonths"))
            Dim tmpStr As String = ""
            If YearLimit > 0 Then tmpStr = "PeriodStart > =  #1/1/" & YearLimit & "#"

            Dim mi As MenuItem
            For Each mi In MenuWindow.MenuItems
                If tmpStr <> "" Then tmpStr = tmpStr & " AND "
                tmpStr = tmpStr & "Periodstart <> #" & CType(mi.Text, Date) & "#"
            Next

            dv.RowFilter = tmpStr
            dv.Sort = "PeriodStart DESC"

            Dim result As DialogResult
            If MenuWindow.MenuItems.Count > 0 Then
                'MyMonth.IsOpen = True
                MyMonth.Text = "   Open Month"
                MyMonth.rbtnNew.Enabled = False
                MyMonth.llRefresh.Enabled = False
            Else
                'MyMonth.IsOpen = False
                MyMonth.Text = "   Getting Started"
                MyMonth.rbtnNew.Enabled = AdminRights
                MyMonth.llRefresh.Enabled = AdminRights
            End If

            MyMonth.PopulateListBox(dv, AdminRights)
            result = MyMonth.ShowDialog(Me)

            Select Case result
                Case Windows.Forms.DialogResult.OK

                    Dim cindex As Integer = MyMonth.MonthList.SelectedIndex

                    Dim sv As String = String.Empty

                    If (MyMonth.MonthList.SelectedItems.Count > 0) Then
                        sv = MyMonth.MonthList.SelectedItem.ToString()
                    End If

                    If MyMonth.rbtnNew.Checked Then

                        If ((sv <> String.Empty) AndAlso (sv.Substring(0, 2) = "**")) Then
                            sv = sv.Substring(2, sv.Length - 2)
                            CreateNewMonth(CDate(sv))
                        Else
                            CreateNewMonth()
                        End If

                    Else

                        Me.Cursor = Cursors.WaitCursor

                        'Gap Date

                        PeriodMonth = Month(CDate(sv))
                        PeriodYear = Year(CDate(sv))
                        PeriodDisplay = Format(sv, "y")
                        PeriodStart = CDate(sv)
                        CurrentMonth = CInt(Format(PeriodStart, "yyyyMM"))
                        MyMonth.Close()

                        Try
                            Get_Month(PeriodStart, GetClientKey(), GetCurrencyFromSettings())
                        Catch ex As Exception
                            CallExceptionManager(ex)
                            Exit Sub
                        End Try

                        'MMMMM BREAK OUT WITH NEW
                        Try
                            Dim MyMain As New Main(Me)
                            ActiveMainForm = MyMain

                            If WasDownloaded Then MyMain.CreateEvent(Now(), "Downloaded '" & PeriodDisplay & "' from Solomon")
                            MyMain.RefineryID = Key.GetRefineryID

                            'HandleMainWindows(PeriodDisplay)
                            SetMenus()
                            Me.Cursor = Cursors.Default
                            AddWindowItem(MyMain)
                            'MyMain.pnlDockTable.Visible = False
                            'MyMain.pnlDockMaxButtons.Visible = False
                            'MyMain.pnlWorkSpace.Visible = True
                            'MyMain.pnlWorkSpace.BringToFront()
                            MyMain.Show()
                            MyMain.UpdateStatusBar()
                            MyMain.IsLoading = False
                        Catch ex As Exception
                            CallExceptionManager(ex)
                            Exit Sub
                        End Try
                    End If

                Case Else

                    SetMenus()
                    MyMonth.Close()

            End Select

        Catch ex As Exception

            Exceptions.Display("Critical Error", ex, "OpenGettingStarted")
            Me.Close()
            Exit Sub

        End Try

    End Sub

#End Region

    Friend Function GetDataDump(ByVal ReportCode As String, ByVal ds As String, ByVal Scenario As String, ByVal CurrencyCode As String, ByVal PeriodStart As Date, ByVal UOM As String, ByVal StudyYear As Integer, ByVal includeTarget As Boolean, ByVal includeYTD As Boolean, ByVal includeAvg As Boolean, refineryID As String) As DataSet
        '20120110 RRH Removing certificate validation
        'If AppCertificate.IsCertificateInstall = False Then
        '    Dim result1 As DialogResult = ProfileMsgBox("YN", "NOCERT", "")
        '    If result1 = Windows.Forms.DialogResult.Yes Then AppCertificate.InstallCertficate()
        '    Me.Cursor = Cursors.Default
        '    ActiveMainForm.LoadingReferenceData("Ready", False)
        '    Return Nothing
        '    Exit Function
        'End If
        Try
            Dim ws As New RefTablesQueries
            Return ws.GetRefineryData(ReportCode, "ACTUAL", Scenario, CurrencyCode, PeriodStart, UOM, StudyYear, includeTarget, includeYTD, includeAvg, refineryID)
        Catch ex As Exception
            Exceptions.Display("Critical Error", ex, "GetDataDump")
            Me.Close()
        End Try
        Return Nothing
    End Function

    Private Sub MenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles MenuItem1.Click
        modGeneral.RunDiagnostics()
        'Process.Start(Application.StartupPath & "\NetworkMonitor.exe")
    End Sub

    Friend Sub PrepActivityLogArguments(ByRef refineryID As String, ByRef CallerIP As String, ByRef userid As String, ByRef computername As String, ByRef service As String)
        refineryID = Me.RefineryID
        CallerIP = GetClientIpV4()
        If userid.Length < 1 Then
            userid = My.User.Name
        End If
        computername = Environment.MachineName.ToString()
        service = "Profile II v. " + Application.ProductVersion.ToString()
    End Sub
    Friend Sub PrepActivityLogArguments(ByRef applicationName As String, ByRef Methodology As String, ByRef refineryID As String, ByRef CallerIP As String,
            ByRef userid As String, ByRef computername As String, ByRef version As String, ByVal err As Exception,
            ByRef errorMessages As String, ByRef localDomainName As String, ByRef osVersion As String, ByRef officeVersion As String)
        applicationName = "Profile II"
        Try
            Methodology = modGeneral.Methodology
        Catch
            Methodology = "0000"
        End Try

        refineryID = Me.RefineryID
        CallerIP = GetClientIpV4()
        If IsNothing(userid) OrElse userid.Length < 1 Then
            userid = My.User.Name
        End If

        If IsNothing(userid) OrElse userid.Length < 1 Then
            If Environment.UserName.Length > 0 Then
                userid = Environment.UserName
            Else
                userid = "UNKNOWN"
            End If
        End If
        computername = Environment.MachineName.ToString()
        version = Application.ProductVersion.ToString()
        If Not IsNothing(err) Then
            errorMessages = err.Message.Substring(0, 998)
        Else
            errorMessages = String.Empty
        End If

        If My.User.Name.Contains("\") Then
            Dim parts() As String = My.User.Name.Split("\")
            localDomainName = parts(0)
        Else
            If Environment.UserDomainName.Length > 0 Then
                localDomainName = Environment.UserDomainName
            Else
                localDomainName = String.Empty
            End If

        End If

        osVersion = modGeneral.OsVersion()

        officeVersion = String.Empty 'haven't found a reliable way of doing this yet.  No-one seems to be able to get this post to work all the time:  http://stackoverflow.com/questions/2203980/detect-whether-office-is-32bit-or-64bit-via-the-registry
    End Sub

    Private Function GetClientIpV4() As String
        Try
            Dim hostName As String = System.Net.Dns.GetHostName()
            Dim ipEntry As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(hostName)
            Dim addresses As System.Net.IPAddress() = ipEntry.AddressList
            Return addresses(addresses.Length - 1).ToString()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Sub mnuDashboardProfileChoice_Click(sender As Object, e As EventArgs) Handles mnuDashboardProfileChoice.Click
        If mnuDashboardProfileChoice.Text = "&Dashboard" Then
            mnuDashboardProfileChoice.Text = "&Profile"
            _dashboard.MdiParent = Me
            'test.IsMdiChild = True
            _dashboard.Visible = True
            _dashboard.SetParentForm(Me)
            _dashboard.Show()
        Else
            mnuDashboardProfileChoice.Text = "&Dashboard"
            If _dashboard.Visible = True Then _dashboard.Hide()
            ActiveMainForm.Show()
        End If
    End Sub

    Public Function GetReportFromTitleAndDesc(title As String, desc As String, ByRef browser As SA_Browser) As String

        Return ActiveMainForm.SelectTableView(title, desc, Nothing, Nothing)

    End Function



End Class
