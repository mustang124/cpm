
Option Explicit On
Imports System.Configuration
Imports Solomon.ProfileII.Contracts
Imports Solomon.ProfileII.Proxies

Friend Class Login
    Inherits System.Windows.Forms.Form

    Friend WithEvents llForgotPassword As System.Windows.Forms.LinkLabel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnCancel02 As System.Windows.Forms.Button
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
	Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblVersion As System.Windows.Forms.Label
	Friend WithEvents Panel2 As System.Windows.Forms.Panel

#Region " Windows Form Designer generated code "

    Friend Sub New(ByVal _MyParent As frmMain)

        MyBase.New()
        'This call is required by the Windows Form Designer.
        T("Login.New")
        InitializeComponent()

        lblVersion.Text = "Version " & Application.ProductVersion '& " DB: " & _MyParent.DBVersion

        'Add any initialization after the InitializeComponent() call
        'For testing
        '#If DEBUG Then
        '        tbUsername.Text = "solomon"
        '        tbPassword.Text = "profile"
        '#Else
        tbUsername.Text = String.Empty
        tbPassword.Text = String.Empty
        '#End If
        T("Login.New-End")
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbNew As System.Windows.Forms.TextBox
    Friend WithEvents tbConfirm As System.Windows.Forms.TextBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel01 As System.Windows.Forms.Button
    Friend WithEvents gbChangePassword As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbPassword As System.Windows.Forms.TextBox
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents llChangePassword As System.Windows.Forms.LinkLabel
    Friend WithEvents tbUsername As System.Windows.Forms.TextBox
    Friend WithEvents gbEMailPassword As System.Windows.Forms.GroupBox
    Friend WithEvents txtSolIDeMail As System.Windows.Forms.TextBox
    Friend WithEvents gbLogin As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbNew = New System.Windows.Forms.TextBox()
        Me.tbConfirm = New System.Windows.Forms.TextBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel01 = New System.Windows.Forms.Button()
        Me.gbChangePassword = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbPassword = New System.Windows.Forms.TextBox()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.llChangePassword = New System.Windows.Forms.LinkLabel()
        Me.tbUsername = New System.Windows.Forms.TextBox()
        Me.gbLogin = New System.Windows.Forms.GroupBox()
        Me.llForgotPassword = New System.Windows.Forms.LinkLabel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.gbEMailPassword = New System.Windows.Forms.GroupBox()
        Me.btnCancel02 = New System.Windows.Forms.Button()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtSolIDeMail = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.gbChangePassword.SuspendLayout()
        Me.gbLogin.SuspendLayout()
        Me.gbEMailPassword.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(19, 19)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(81, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "New Password:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(3, 44)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(97, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Confirm Password:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbNew
        '
        Me.tbNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNew.Location = New System.Drawing.Point(109, 19)
        Me.tbNew.Name = "tbNew"
        Me.tbNew.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.tbNew.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbNew.Size = New System.Drawing.Size(203, 21)
        Me.tbNew.TabIndex = 1
        '
        'tbConfirm
        '
        Me.tbConfirm.AcceptsReturn = True
        Me.tbConfirm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbConfirm.Location = New System.Drawing.Point(109, 44)
        Me.tbConfirm.Name = "tbConfirm"
        Me.tbConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.tbConfirm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbConfirm.Size = New System.Drawing.Size(203, 21)
        Me.tbConfirm.TabIndex = 2
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.Image = CType(resources.GetObject("btnOK.Image"), System.Drawing.Image)
        Me.btnOK.Location = New System.Drawing.Point(109, 71)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnOK.Size = New System.Drawing.Size(64, 28)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnCancel01
        '
        Me.btnCancel01.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnCancel01.FlatAppearance.BorderSize = 0
        Me.btnCancel01.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel01.ForeColor = System.Drawing.Color.Black
        Me.btnCancel01.Image = CType(resources.GetObject("btnCancel01.Image"), System.Drawing.Image)
        Me.btnCancel01.Location = New System.Drawing.Point(179, 71)
        Me.btnCancel01.Name = "btnCancel01"
        Me.btnCancel01.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnCancel01.Size = New System.Drawing.Size(64, 28)
        Me.btnCancel01.TabIndex = 4
        Me.btnCancel01.Text = "Cancel"
        Me.btnCancel01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel01.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnCancel01.UseVisualStyleBackColor = False
        '
        'gbChangePassword
        '
        Me.gbChangePassword.Controls.Add(Me.tbConfirm)
        Me.gbChangePassword.Controls.Add(Me.btnOK)
        Me.gbChangePassword.Controls.Add(Me.tbNew)
        Me.gbChangePassword.Controls.Add(Me.Label5)
        Me.gbChangePassword.Controls.Add(Me.Label9)
        Me.gbChangePassword.Controls.Add(Me.btnCancel01)
        Me.gbChangePassword.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbChangePassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbChangePassword.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.gbChangePassword.Location = New System.Drawing.Point(6, 206)
        Me.gbChangePassword.Name = "gbChangePassword"
        Me.gbChangePassword.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbChangePassword.Size = New System.Drawing.Size(319, 112)
        Me.gbChangePassword.TabIndex = 7
        Me.gbChangePassword.TabStop = False
        Me.gbChangePassword.Text = "Change your password"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(12, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Enter Username:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(43, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Password:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbPassword
        '
        Me.tbPassword.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.tbPassword.Location = New System.Drawing.Point(109, 44)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.tbPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbPassword.ShortcutsEnabled = False
        Me.tbPassword.Size = New System.Drawing.Size(203, 21)
        Me.tbPassword.TabIndex = 2
        '
        'btnLogin
        '
        Me.btnLogin.BackColor = System.Drawing.Color.YellowGreen
        Me.btnLogin.FlatAppearance.BorderSize = 0
        Me.btnLogin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogin.ForeColor = System.Drawing.Color.Black
        Me.btnLogin.Image = CType(resources.GetObject("btnLogin.Image"), System.Drawing.Image)
        Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.TopRight
        Me.btnLogin.Location = New System.Drawing.Point(109, 72)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnLogin.Size = New System.Drawing.Size(64, 28)
        Me.btnLogin.TabIndex = 3
        Me.btnLogin.Text = "Sign In"
        Me.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnLogin.UseVisualStyleBackColor = False
        '
        'llChangePassword
        '
        Me.llChangePassword.AutoSize = True
        Me.llChangePassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llChangePassword.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.llChangePassword.LinkColor = System.Drawing.SystemColors.ActiveCaption
        Me.llChangePassword.Location = New System.Drawing.Point(189, 72)
        Me.llChangePassword.Name = "llChangePassword"
        Me.llChangePassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.llChangePassword.Size = New System.Drawing.Size(44, 13)
        Me.llChangePassword.TabIndex = 3
        Me.llChangePassword.TabStop = True
        Me.llChangePassword.Text = "Change"
        Me.llChangePassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbUsername
        '
        Me.tbUsername.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.tbUsername.Location = New System.Drawing.Point(109, 19)
        Me.tbUsername.Name = "tbUsername"
        Me.tbUsername.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbUsername.ShortcutsEnabled = False
        Me.tbUsername.Size = New System.Drawing.Size(203, 21)
        Me.tbUsername.TabIndex = 1
        '
        'gbLogin
        '
        Me.gbLogin.Controls.Add(Me.llForgotPassword)
        Me.gbLogin.Controls.Add(Me.tbUsername)
        Me.gbLogin.Controls.Add(Me.tbPassword)
        Me.gbLogin.Controls.Add(Me.llChangePassword)
        Me.gbLogin.Controls.Add(Me.Label3)
        Me.gbLogin.Controls.Add(Me.Label2)
        Me.gbLogin.Controls.Add(Me.Label19)
        Me.gbLogin.Controls.Add(Me.Label18)
        Me.gbLogin.Controls.Add(Me.btnLogin)
        Me.gbLogin.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.gbLogin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLogin.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.gbLogin.Location = New System.Drawing.Point(6, 94)
        Me.gbLogin.Name = "gbLogin"
        Me.gbLogin.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbLogin.Size = New System.Drawing.Size(319, 112)
        Me.gbLogin.TabIndex = 19
        Me.gbLogin.TabStop = False
        Me.gbLogin.Text = "Welcome"
        '
        'llForgotPassword
        '
        Me.llForgotPassword.AutoSize = True
        Me.llForgotPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llForgotPassword.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.llForgotPassword.LinkColor = System.Drawing.SystemColors.ActiveCaption
        Me.llForgotPassword.Location = New System.Drawing.Point(251, 86)
        Me.llForgotPassword.Name = "llForgotPassword"
        Me.llForgotPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.llForgotPassword.Size = New System.Drawing.Size(58, 13)
        Me.llForgotPassword.TabIndex = 21
        Me.llForgotPassword.TabStop = True
        Me.llForgotPassword.Text = "password?"
        Me.llForgotPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(230, 72)
        Me.Label19.Name = "Label19"
        Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label19.Size = New System.Drawing.Size(82, 13)
        Me.Label19.TabIndex = 22
        Me.Label19.Text = "your password."
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(189, 86)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label18.Size = New System.Drawing.Size(64, 13)
        Me.Label18.TabIndex = 20
        Me.Label18.Text = "Forgot your"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gbEMailPassword
        '
        Me.gbEMailPassword.Controls.Add(Me.btnCancel02)
        Me.gbEMailPassword.Controls.Add(Me.btnSend)
        Me.gbEMailPassword.Controls.Add(Me.Label8)
        Me.gbEMailPassword.Controls.Add(Me.txtSolIDeMail)
        Me.gbEMailPassword.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbEMailPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.gbEMailPassword.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.gbEMailPassword.Location = New System.Drawing.Point(6, 318)
        Me.gbEMailPassword.Name = "gbEMailPassword"
        Me.gbEMailPassword.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbEMailPassword.Size = New System.Drawing.Size(319, 112)
        Me.gbEMailPassword.TabIndex = 20
        Me.gbEMailPassword.TabStop = False
        Me.gbEMailPassword.Text = "Forgot your password?"
        '
        'btnCancel02
        '
        Me.btnCancel02.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnCancel02.FlatAppearance.BorderSize = 0
        Me.btnCancel02.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel02.ForeColor = System.Drawing.Color.Black
        Me.btnCancel02.Image = CType(resources.GetObject("btnCancel02.Image"), System.Drawing.Image)
        Me.btnCancel02.Location = New System.Drawing.Point(179, 71)
        Me.btnCancel02.Name = "btnCancel02"
        Me.btnCancel02.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnCancel02.Size = New System.Drawing.Size(64, 28)
        Me.btnCancel02.TabIndex = 4
        Me.btnCancel02.Text = "Cancel"
        Me.btnCancel02.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel02.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnCancel02.UseVisualStyleBackColor = False
        '
        'btnSend
        '
        Me.btnSend.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnSend.FlatAppearance.BorderSize = 0
        Me.btnSend.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSend.ForeColor = System.Drawing.Color.Black
        Me.btnSend.Image = CType(resources.GetObject("btnSend.Image"), System.Drawing.Image)
        Me.btnSend.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSend.Location = New System.Drawing.Point(109, 71)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(64, 28)
        Me.btnSend.TabIndex = 3
        Me.btnSend.Text = "Send"
        Me.btnSend.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSend.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSend.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(12, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(88, 13)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Enter Username:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSolIDeMail
        '
        Me.txtSolIDeMail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSolIDeMail.Location = New System.Drawing.Point(109, 19)
        Me.txtSolIDeMail.Name = "txtSolIDeMail"
        Me.txtSolIDeMail.Size = New System.Drawing.Size(203, 21)
        Me.txtSolIDeMail.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(6, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(81, 76)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(87, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(226, 38)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 16
        Me.PictureBox2.TabStop = False
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Gray
        Me.Label6.Location = New System.Drawing.Point(87, 50)
        Me.Label6.Name = "Label6"
        Me.Label6.Padding = New System.Windows.Forms.Padding(10, 0, 10, 0)
        Me.Label6.Size = New System.Drawing.Size(226, 38)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "A Software Service for Improving Refinery Performance"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(6, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(6, 12, 6, 6)
        Me.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel1.Size = New System.Drawing.Size(319, 94)
        Me.Panel1.TabIndex = 18
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Image = Global.Solomon.Profile.My.Resources.Resources.error_1
        Me.Button1.Location = New System.Drawing.Point(311, -2)
        Me.Button1.Margin = New System.Windows.Forms.Padding(0)
        Me.Button1.Name = "Button1"
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(21, 21)
        Me.Button1.TabIndex = 23
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.Control
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.lblVersion)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.gbEMailPassword)
        Me.Panel2.Controls.Add(Me.gbChangePassword)
        Me.Panel2.Controls.Add(Me.gbLogin)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(2, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.Panel2.Size = New System.Drawing.Size(333, 233)
        Me.Panel2.TabIndex = 24
        '
        'lblVersion
        '
        Me.lblVersion.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblVersion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblVersion.Location = New System.Drawing.Point(6, 208)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(319, 17)
        Me.lblVersion.TabIndex = 35
        Me.lblVersion.Text = "Version"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.Panel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Padding = New System.Windows.Forms.Padding(2)
        Me.Panel3.Size = New System.Drawing.Size(337, 237)
        Me.Panel3.TabIndex = 25
        '
        'Login
        '
        Me.AcceptButton = Me.btnLogin
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(345, 245)
        Me.Controls.Add(Me.Panel3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Login"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "  Welcome"
        Me.gbChangePassword.ResumeLayout(False)
        Me.gbChangePassword.PerformLayout()
        Me.gbLogin.ResumeLayout(False)
        Me.gbLogin.PerformLayout()
        Me.gbEMailPassword.ResumeLayout(False)
        Me.gbEMailPassword.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim IsChangingPassword As Boolean
    Dim IsLoggingOn As Boolean
    Dim MyParent As frmMain
    'Dim al As New RemoteSubmitServices

    Private Sub viewGroupBox(ByVal gbBoxName As GroupBox)

		For Each ctrl As Control In Panel2.Controls
			If (TypeOf (ctrl) Is GroupBox) Then
				If (ctrl.Name = gbBoxName.Name) Then
					ctrl.Visible = True
				Else
					ctrl.Visible = False
				End If
			End If
		Next ctrl

    End Sub

    Private Sub Login()
        T("Login.Login()")
        If ValidLogin(tbUsername.Text.Trim(), tbPassword.Text.Trim()) Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            LoggedInUser = tbUsername.Text.Trim()
            SetEnvironmentCultureAndRegionalSettings()

            If Util.UseNonWCFWebService Then
                'al.SubmitActivityLog("LOGIN", "", Nothing, Nothing, "Success", Nothing, String.Empty)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()

                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try
                    Dim applicationName As String = String.Empty
                    Dim Methodology As String = String.Empty
                    Dim localRefineryID As String = String.Empty
                    Dim CallerIP As String = String.Empty
                    Dim userid As String = String.Empty
                    Dim computername As String = String.Empty
                    Dim version As String = String.Empty
                    Dim err As Object = Nothing
                    Dim errorMessages As String = String.Empty
                    Dim localDomainName As String = String.Empty
                    Dim osVersion As String = String.Empty
                    Dim officeVersion As String = String.Empty
                    frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                    userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                    'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                    proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, "LOGIN", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                Catch
                Finally
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                End Try
            End If
        Else

            If Util.UseNonWCFWebService Then
                'al.SubmitActivityLog("LOGIN", "", Nothing, Nothing, "Failed", Nothing, String.Empty)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try
                    Dim applicationName As String = String.Empty
                    Dim Methodology As String = String.Empty
                    Dim localRefineryID As String = String.Empty
                    Dim CallerIP As String = String.Empty
                    Dim userid As String = String.Empty
                    Dim computername As String = String.Empty
                    Dim version As String = String.Empty
                    Dim err As Object = Nothing
                    Dim errorMessages As String = String.Empty
                    Dim localDomainName As String = String.Empty
                    Dim osVersion As String = String.Empty
                    Dim officeVersion As String = String.Empty
                    frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                    userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                    proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                        userid, computername, String.Empty, "LOGIN", String.Empty, Nothing, Nothing, "ERROR", version, "Login failed",
                        localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                    'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                    '    userid, computername, String.Empty, "LOGIN", String.Empty, Nothing, Nothing, "ERROR", version, "Login failed",
                    '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
                Catch
                Finally
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                End Try
            End If
            ProfileMsgBox("CRIT", "PASSWORD", "")
            ' 2008.08.07 RRH ui - Better UI when incorrect PW
            Me.tbPassword.Text = ""
            Me.tbPassword.Focus()
        End If
        T("Login.Login-End")
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        T("Login.btnLogin_Click()")
        ' 2008.08.07 RRH ui - Hold Shift, Ctrl, and Alt keys when clicking login to change administrator's password

        'If ((My.Computer.Keyboard.ShiftKeyDown) And (My.Computer.Keyboard.CtrlKeyDown) And (My.Computer.Keyboard.AltKeyDown)) Then
        '    viewGroupBox(gbRenewPassword)  ' 2008.08.07 RRH New procedure to change GroupBox visibility
        '    Me.txtSolID.Select()
        'Else
        Login()
        'End If
        T("Login.btnLogin_Click-End")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel01.Click
        tbConfirm.Text = ""
        tbNew.Text = ""
        viewGroupBox(gbLogin)              ' 2008.08.07 RRH New procedure to change GroupBox visibility
    End Sub

    Private Sub Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        T("Login.Login_Load()")
        MyParent = CType(Me.ParentForm, frmMain)
        viewGroupBox(gbLogin)              ' 2008.08.07 RRH New procedure to change GroupBox visibility
        'If System.IO.Directory.Exists("C:\Users\sfb") Then
        '    If System.IO.File.Exists("C:\Users\sfb\DevConfigHelper.txt") Then
        '        tbUsername.Text = "solomon"
        '        tbPassword.Text = "profile"
        '        Login()
        '    End If
        'End If
        T("Login.Login_Load-End")
    End Sub

    Private Sub llChangePassword_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles llChangePassword.LinkClicked
        If ValidLogin(tbUsername.Text, tbPassword.Text) Then
            viewGroupBox(gbChangePassword) ' 2008.08.07 RRH New procedure to change GroupBox visibility
            tbNew.Focus()
            'al.SubmitActivityLog("", "", "", tbUsername.Text.Trim(), "", Environment.MachineName.ToString(), "CHANGE PASSWORD", "", "", "", "", "Success")


            If Util.UseNonWCFWebService Then
                'al.SubmitActivityLog("llChangePassword_LinkClicked", Nothing, Nothing, Nothing, "Success", Nothing, Nothing)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try
                    Dim applicationName As String = String.Empty
                    Dim Methodology As String = String.Empty
                    Dim localRefineryID As String = String.Empty
                    Dim CallerIP As String = String.Empty
                    Dim userid As String = String.Empty
                    Dim computername As String = String.Empty
                    Dim version As String = String.Empty
                    Dim err As Object = Nothing
                    Dim errorMessages As String = String.Empty
                    Dim localDomainName As String = String.Empty
                    Dim osVersion As String = String.Empty
                    Dim officeVersion As String = String.Empty
                    frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                    userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                    proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                        userid, computername, String.Empty, "llChangePassword_LinkClicked",
                        String.Empty, Nothing, Nothing, "Success", version, String.Empty,
                        localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                    'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                    '    userid, computername, String.Empty, "llChangePassword_LinkClicked",
                    '    String.Empty, Nothing, Nothing, "Success", version, String.Empty,
                    '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
                Catch
                Finally
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close()
                End Try
            End If
        Else
            ProfileMsgBox("CRIT", "PASSWORD", "")
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If tbConfirm.Text = tbNew.Text And tbNew.Text <> "" Then
            Try
                ChangePassword(tbUsername.Text, tbPassword.Text, tbConfirm.Text)
            Catch ex As Exception
                ProfileMsgBox("CRIT", "CHANGEPW", "")
            End Try
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            tbConfirm.Text = ""
            tbNew.Text = ""
            tbNew.Focus()
        End If
    End Sub

#Region " eMail Password "

    Private Sub btnCancelEMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel02.Click
        clearEMail()
        viewGroupBox(gbLogin)
    End Sub

    Private Sub clearEMail()
        ' 2008.08.19 RRH - Added to clear renewing password text boxes
        Me.txtSolIDeMail.Text = ""

    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        ' verify user's e-mail address
        Dim users As DataSet = New DataSet
        ReadEncrpytedXML("usersXP.xml", users)
		users.Tables(0).CaseSensitive = False

        Dim rows() As DataRow = users.Tables(0).Select("LoginID='" & Me.txtSolIDeMail.Text & "'")

        If (rows.GetUpperBound(0) > -1) Then

            Dim txtAdmineMail As String = rows(0)("Email_Address").ToString

			'If (txtAdmineMail <> "") Then

			Dim strMessage As String = "Your password is: " & rows(0)("Password").ToString

			Me.Cursor = Cursors.WaitCursor

            'Dim mc As WCFEMailServices.MessageClient = New WCFEMailServices.MessageClient()

            'Dim strResult As String = mc.SendFromUnmonitored(ConfigurationManager.AppSettings("SolomonEmail"), "Profile (Solomon Associates)", txtAdmineMail, "Solomon Profile� II Password", strMessage, "en-US")
            'ProfileMsgBox("INFO", "", strResult)

			Me.Cursor = Cursors.Arrow
			clearEMail()
			viewGroupBox(gbLogin)

			'Else

			'	If ProfileMsgBox("YN", "", "The e-mail address entered does not match the one in Solomon Profile� II." & vbCrLf & "Do you wish to try again?") = Windows.Forms.DialogResult.Yes Then
			'		Me.lblEmailAddress.Text = ""
			'		Me.lblEmailAddress.Select()
			'	Else
			'		clearEMail()
			'		viewGroupBox(gbLogin)
			'	End If

			'End If

		Else

			If ProfileMsgBox("YN", "", "The user id entered is not in Solomon Profile� II." & vbCrLf & "Do you wish to try again?") = Windows.Forms.DialogResult.Yes Then
				Me.txtSolIDeMail.Text = ""
				Me.txtSolIDeMail.Select()
			Else
				clearEMail()
				viewGroupBox(gbLogin)
			End If

		End If

	End Sub

    Private Sub llForgotPassword_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles llForgotPassword.LinkClicked
        viewGroupBox(gbEMailPassword)
    End Sub

#End Region ' eMail Password

    Private Sub Login_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        T("Login.Login_Shown()")
        tbUsername.Focus()
        T("Login.Login_Shown-End")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub SetEnvironmentCultureAndRegionalSettings()
        T("Login.SetEnvironmentCultureAndRegionalSettings()")
        On Error GoTo EnvironmentSettingsError

        Dim culture As New ProfileEnvironment.Culture()
        Dim regionalCurrency As New ProfileEnvironment.RegionalCurrency()
        Dim regionalNumeric As New ProfileEnvironment.RegionalNumeric()

        'Open, "EnvironmentCulture.xml" and save settings for the environment culture
        SaveUserEnvironmentCulture(culture, regionalNumeric, regionalCurrency)

        T("Login.SetEnvironmentCultureAndRegionalSettings-End")
        Exit Sub

EnvironmentSettingsError:
        MsgBox("Error loading environment settings for " & Me.Text, MsgBoxStyle.Critical, "Profile II")

    End Sub

    Public Sub SaveUserEnvironmentCulture(ByVal culture As ProfileEnvironment.Culture, ByVal regionalNumeric As ProfileEnvironment.RegionalNumeric, ByVal regionalCurrency As ProfileEnvironment.RegionalCurrency)
        T("Login.SaveUserEnvironmentCulture()")
        Try

            'Open, "EnvironmentCulture.xml" and save settings
            Dim dsEnvironmentCulture As New DataSet
            Dim tmp_dsEnvironmentCulture As New DataSet
            Dim userName As String = LoggedInUser

            dsEnvironmentCulture.ReadXml(pathEnvironment & "EnvironmentCulture.xml")

            'Populate the Data rows with the properties from the custom objects properties. Add the datatables and write xml

            Dim cultureTable As DataTable = dsEnvironmentCulture.Tables("CultureTable")

            If cultureTable.Rows.Count = 0 Then

                Dim newCultureRow As DataRow = cultureTable.NewRow()

                Dim cultureInfo As New ProfileEnvironment.Culture()
                cultureInfo = culture.GetCulture()

                cultureInfo.UserId = userName

                newCultureRow("UserId") = cultureInfo.UserId
                newCultureRow("CultureName") = cultureInfo.CultureName
                newCultureRow("TwoLetterISOLanguageName") = cultureInfo.TwoLetterISOLanguageName
                newCultureRow("ThreeLetterISOLanguageName") = cultureInfo.ThreeLetterISOLanguageName
                newCultureRow("ThreeLetterWindowsLanguageName") = cultureInfo.ThreeLetterWindowsLanguageName
                newCultureRow("DisplayName") = cultureInfo.DisplayName
                newCultureRow("EnglishName") = cultureInfo.EnglishName

                cultureTable.Rows.Add(newCultureRow)

                'Populate the Data rows with the properties from the custom objects properties. Add the datatables and write xml
                Dim regionalCurrTable As DataTable = dsEnvironmentCulture.Tables("RegionalCurrencyTable")
                Dim newCurrencyRow As DataRow = regionalCurrTable.NewRow()

                regionalCurrency.UserId = userName

                newCurrencyRow("UserId") = regionalCurrency.UserId
                newCurrencyRow("CurrencySymbol") = regionalCurrency.CurrencySymbol
                newCurrencyRow("PositiveCurrencyFormat") = regionalCurrency.PositiveCurrencyFormat
                newCurrencyRow("NegativeCurrencyFormat") = regionalCurrency.NegativeCurrencyFormat
                newCurrencyRow("DecimalSymbol") = regionalCurrency.DecimalSymbol
                newCurrencyRow("NumberOfDigitsAfterDecimal") = regionalCurrency.NumberOfDigitsAfterDecimal
                newCurrencyRow("DigitGroupingSymbol") = regionalCurrency.DigitGroupingSymbol

                regionalCurrTable.Rows.Add(newCurrencyRow)


                Dim regionalNumTable As DataTable = dsEnvironmentCulture.Tables("RegionalNumericTable")
                Dim newNumericRow As DataRow = regionalNumTable.NewRow()

                regionalNumeric.UserId = userName

                newNumericRow("UserId") = regionalNumeric.UserId
                newNumericRow("DecimalSymbol") = regionalNumeric.DecimalSymbol
                newNumericRow("NumberOfDigitsAfterDecimal") = regionalNumeric.NumberOfDigitsAfterDecimal
                newNumericRow("DigitGroupingSymbol") = regionalNumeric.DigitGroupingSymbol
                newNumericRow("NegativeSignSymbol") = regionalNumeric.NegativeSignSymbol
                newNumericRow("NegativeNumberFormat") = regionalNumeric.NegativeNumberFormat

                regionalNumTable.Rows.Add(newNumericRow)

                dsEnvironmentCulture.WriteXml(pathEnvironment & "EnvironmentCulture.xml")
            Else
                'Row items exist. Need to check if user is within environment setting xml via the UserId. Just check whether or not the culture table has the userId, if it doesn't 
                'then we need to add the user environment settings to the file. Also, if we have a match get the saved environment culture. This will be handled then in the "else".
                Dim userExists As Boolean = False
                Dim rowIndex As Integer = 0
                Dim userEnvironmentCulture As String = String.Empty
                
                For Each row As DataRow In cultureTable.Rows
                    
                    If row.Item(0).ToString() = LoggedInUser Then
                        'Get previuosly saved environment.
                        userEnvironmentCulture = row.Item(1).ToString()
                        userExists = True
                        Exit For
                    End If

                    rowIndex = rowIndex + 1

                Next

                'Add user to table
                If userExists = False Then

                    Dim newCultureRow As DataRow = cultureTable.NewRow()

                    Dim cultureInfo As New ProfileEnvironment.Culture()
                    cultureInfo = culture.GetCulture()

                    cultureInfo.UserId = userName

                    newCultureRow("UserId") = cultureInfo.UserId
                    newCultureRow("CultureName") = cultureInfo.CultureName
                    newCultureRow("TwoLetterISOLanguageName") = cultureInfo.TwoLetterISOLanguageName
                    newCultureRow("ThreeLetterISOLanguageName") = cultureInfo.ThreeLetterISOLanguageName
                    newCultureRow("ThreeLetterWindowsLanguageName") = cultureInfo.ThreeLetterWindowsLanguageName
                    newCultureRow("DisplayName") = cultureInfo.DisplayName
                    newCultureRow("EnglishName") = cultureInfo.EnglishName

                    cultureTable.Rows.Add(newCultureRow)

                    'Populate the Data rows with the properties from the custom objects properties. Add the datatables and write xml
                    Dim regionalCurrTable As DataTable = dsEnvironmentCulture.Tables("RegionalCurrencyTable")
                    Dim newCurrencyRow As DataRow = regionalCurrTable.NewRow()

                    regionalCurrency.UserId = userName

                    newCurrencyRow("UserId") = regionalCurrency.UserId
                    newCurrencyRow("CurrencySymbol") = regionalCurrency.CurrencySymbol
                    newCurrencyRow("PositiveCurrencyFormat") = regionalCurrency.PositiveCurrencyFormat
                    newCurrencyRow("NegativeCurrencyFormat") = regionalCurrency.NegativeCurrencyFormat
                    newCurrencyRow("DecimalSymbol") = regionalCurrency.DecimalSymbol
                    newCurrencyRow("NumberOfDigitsAfterDecimal") = regionalCurrency.NumberOfDigitsAfterDecimal
                    newCurrencyRow("DigitGroupingSymbol") = regionalCurrency.DigitGroupingSymbol

                    regionalCurrTable.Rows.Add(newCurrencyRow)


                    Dim regionalNumTable As DataTable = dsEnvironmentCulture.Tables("RegionalNumericTable")
                    Dim newNumericRow As DataRow = regionalNumTable.NewRow()

                    regionalNumeric.UserId = userName

                    newNumericRow("UserId") = regionalNumeric.UserId
                    newNumericRow("DecimalSymbol") = regionalNumeric.DecimalSymbol
                    newNumericRow("NumberOfDigitsAfterDecimal") = regionalNumeric.NumberOfDigitsAfterDecimal
                    newNumericRow("DigitGroupingSymbol") = regionalNumeric.DigitGroupingSymbol
                    newNumericRow("NegativeSignSymbol") = regionalNumeric.NegativeSignSymbol
                    newNumericRow("NegativeNumberFormat") = regionalNumeric.NegativeNumberFormat

                    regionalNumTable.Rows.Add(newNumericRow)

                    dsEnvironmentCulture.WriteXml(pathEnvironment & "EnvironmentCulture.xml")
                Else
                    'Edit user to table if environment has changed
                    Dim cultureInfo As New ProfileEnvironment.Culture()
                    cultureInfo = culture.GetCulture()

                    'Open xml, fill dataset and check environment
                    cultureInfo.UserId = LoggedInUser

                    If userEnvironmentCulture <> cultureInfo.CultureName Then

                        Dim regionalCurrTable As DataTable = dsEnvironmentCulture.Tables("RegionalCurrencyTable")
                        Dim regionalNumTable As DataTable = dsEnvironmentCulture.Tables("RegionalNumericTable")

                        'Update culture table
                        cultureTable.Rows(rowIndex)("UserId") = cultureInfo.UserId
                        cultureTable.Rows(rowIndex)("CultureName") = cultureInfo.CultureName
                        cultureTable.Rows(rowIndex)("TwoLetterISOLanguageName") = cultureInfo.TwoLetterISOLanguageName
                        cultureTable.Rows(rowIndex)("ThreeLetterISOLanguageName") = cultureInfo.ThreeLetterISOLanguageName
                        cultureTable.Rows(rowIndex)("ThreeLetterWindowsLanguageName") = cultureInfo.ThreeLetterWindowsLanguageName
                        cultureTable.Rows(rowIndex)("DisplayName") = cultureInfo.DisplayName
                        cultureTable.Rows(rowIndex)("EnglishName") = cultureInfo.EnglishName

                        'Update regional currency table
                        regionalCurrTable.Rows(rowIndex)("UserId") = cultureInfo.UserId
                        regionalCurrTable.Rows(rowIndex)("CurrencySymbol") = regionalCurrency.CurrencySymbol
                        regionalCurrTable.Rows(rowIndex)("PositiveCurrencyFormat") = regionalCurrency.PositiveCurrencyFormat
                        regionalCurrTable.Rows(rowIndex)("NegativeCurrencyFormat") = regionalCurrency.NegativeCurrencyFormat
                        regionalCurrTable.Rows(rowIndex)("DecimalSymbol") = regionalCurrency.DecimalSymbol
                        regionalCurrTable.Rows(rowIndex)("NumberOfDigitsAfterDecimal") = regionalCurrency.NumberOfDigitsAfterDecimal
                        regionalCurrTable.Rows(rowIndex)("DigitGroupingSymbol") = regionalCurrency.DigitGroupingSymbol

                        'Update regional number table
                        regionalNumTable.Rows(rowIndex)("UserId") = cultureInfo.UserId
                        regionalNumTable.Rows(rowIndex)("DecimalSymbol") = regionalNumeric.DecimalSymbol
                        regionalNumTable.Rows(rowIndex)("NumberOfDigitsAfterDecimal") = regionalNumeric.NumberOfDigitsAfterDecimal
                        regionalNumTable.Rows(rowIndex)("DigitGroupingSymbol") = regionalNumeric.DigitGroupingSymbol
                        regionalNumTable.Rows(rowIndex)("NegativeSignSymbol") = regionalNumeric.NegativeSignSymbol
                        regionalNumTable.Rows(rowIndex)("NegativeNumberFormat") = regionalNumeric.NegativeNumberFormat

                        dsEnvironmentCulture.WriteXml(pathEnvironment & "EnvironmentCulture.xml")

                    End If
                End If
            End If

        Catch ex As Exception

        End Try
        T("Login.SaveUserEnvironmentCulture-End")
    End Sub

	Private Sub gbEMailPassword_Enter(sender As Object, e As EventArgs) Handles gbEMailPassword.VisibleChanged

		If gbEMailPassword.Visible = True Then
			txtSolIDeMail.Select()
		End If

	End Sub
End Class
