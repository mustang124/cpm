Option Explicit On

Imports System
Imports System.IO
Imports System.Drawing.Printing
Imports System.Globalization
Imports System.Configuration
Imports System.Collections.Generic
Imports Solomon.ProfileII.Contracts
Imports Solomon.ProfileII.Proxies

Friend Class Main
	Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    'Dim al As New RemoteSubmitServices

	Public Sub New(ByVal _MyParent As frmMain)
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
       If IsNothing(SA_Process1) Then
            SA_Process1 = New SA_Process()
            Me.Controls.Add(SA_Process1)
        End If
        If IsNothing(SA_OperatingExpenses1) Then
            SA_OperatingExpenses1 = New SA_OperatingExpenses()
            Me.Controls.Add(SA_OperatingExpenses1)
        End If
        If IsNothing(SA_Configuration1) Then
            SA_Configuration1 = New SA_Configuration()
            Me.Controls.Add(SA_Configuration1)
        End If
        If IsNothing(SA_Energy1) Then
            SA_Energy1 = New SA_Energy()
            Me.Controls.Add(SA_Energy1)
        End If
        If IsNothing(SA_MaterialBalance1) Then
            SA_MaterialBalance1 = New SA_MaterialBalance()
            Me.Controls.Add(SA_MaterialBalance1)
        End If
        If IsNothing(SA_RefineryInfo1) Then
            SA_RefineryInfo1 = New SA_RefineryInfo()
            Me.Controls.Add(SA_RefineryInfo1)
        End If
        If IsNothing(SA_Browser1) Then
            SA_Browser1 = New SA_Browser()
            Me.Controls.Add(SA_Browser1)
        End If
        If IsNothing(SA_Admin1) Then
            SA_Admin1 = New SA_Admin()
            Me.Controls.Add(SA_Admin1)
        End If
        If IsNothing(SA_Inventory1) Then
            SA_Inventory1 = New SA_Inventory()
            Me.Controls.Add(SA_Inventory1)
        End If
        If IsNothing(SA_Personnel1) Then
            SA_Personnel1 = New SA_Personnel()
            Me.Controls.Add(SA_Personnel1)
        End If
		IsLoading = True
		SetMdiParent(_MyParent)

		Me.MdiParent = _MyParent
		PeriodMonth = _MyParent.PeriodMonth
		PeriodYear = _MyParent.PeriodYear
		CurrentMonth = _MyParent.CurrentMonth.ToString
		'.Text = PeriodDisplay
		PeriodStart = _MyParent.PeriodStart
		SetPaths()

        btnSave.Enabled = modGeneral.AdminRights
		'btnRefresh.Enabled = MyParent.AdminRights
        btnClear.Enabled = modGeneral.AdminRights
        btnImport.Enabled = modGeneral.AdminRights
        btnUpload.Enabled = modGeneral.AdminRights
		btnChtCreate.Enabled = MyParent.ResultsRights
		btnRptCreate.Enabled = MyParent.ResultsRights
		'btnRptDump.Enabled = MyParent.AdminRights
        btnAdmin.Enabled = modGeneral.AdminRights

		tcControlPanel.SelectedTab = tpSettings

		GetIsCheckedTable()
		NameDatasets()
		PopulateMonth()
		PopulateReferenceData()

		Dim appEnvmt As AppEnvironment = New AppEnvironment

		appEnvmt.TestEnvironment()

		''Load the workstation environment culture and region settings
		'LoadEnvironmentCultureAndRegionalSettings()
		'LoadingReferenceData("Loading Environment Culture as Regional Settings...", True)

		LoadingReferenceData("Loading Info...", True)
		LoadInfo()

		GetConfiguration()
		' 2009.06.04 RRH
		PopulateLoadedMonths()

		StartMonitoring(UCase(_MyParent.UserName))

		SA_Admin1.LoadAdminCtl()

		LoadingReferenceData("Loading Refinery Settings and Configuration...", True)
		LoadRefineryInformation()

		LoadingReferenceData("Loading Inventory...", True)
		LoadInventory()

		LoadingReferenceData("Loading Operating Expenses...", True)
		LoadOperatingExpenses()

		LoadingReferenceData("Loading Personnel...", True)
		LoadPersonnel()

		LoadingReferenceData("Loading Crude Charge Detail...", True)
		LoadCrudes()

		LoadingReferenceData("Loading Material Balance...", True)
		LoadMaterialBalance()

		LoadingReferenceData("Loading Energy...", True)
		LoadEnergy()

		LoadingReferenceData("Loading User-Defined Inputs/Outputs...", True)
		LoadUserDefined()

		'LoadingReferenceData("Loading Performance Targets...", True)
		'LoadTargets()

		LoadingReferenceData("Loading Maintenance History...", True)
		LoadRoutineHistory()

		PopulateTableViewsTree()
		PopulateChartTree()
		PopulateReportTree()
		PopulateMethodology()

		AddDataEntryControlHandlers()

		treeTableViews.ExpandAll()
		treeConfiguration.Nodes(0).Expand()
		treeConfiguration.Nodes(1).Expand()

        'cmbRptCurrencyCode.SelectedIndex = 0
        'cmbChtCurrencyCode.SelectedIndex = 0
        Try
            frmMain.CurrencySetting = dsSettings.Tables(0).Rows(0)("RptCurrency").ToString()
            frmMain.CurrencyT15Setting = dsSettings.Tables(0).Rows(0)("RptCurrencyT15").ToString()
            frmMain.UomSetting = dsSettings.Tables(0).Rows(0)("UOM").ToString()

            Dim configPath As String = Application.StartupPath & "\_CONFIG"
            If Directory.Exists(configPath) Then
                Dim fileName As String = Dir(configPath + "\Settings.xml")
                If fileName.Length > 0 Then
                    Dim ds As New DataSet()
                    ds.ReadXml(configPath + "\" + fileName)
                    frmMain.MethodologySetting = ds.Tables(0).Rows(0)("ReportFactorSet").ToString()
                End If
            End If


            'dsSettings.Tables(0).WriteXml("C:\temp\Settings.xml")
        Catch
        End Try
        PresetCbo(cmbRptCurrency, frmMain.CurrencySetting)
        PresetCboMakesSense(cmbRptCurrencyCode, frmMain.CurrencySetting)
        PresetCboMakesSense(cmbChtCurrencyCode, frmMain.CurrencySetting)

        PresetCbo(cmbUOM, frmMain.UomSetting)
        PresetCbo(cmbRptUOM, frmMain.UomSetting)
        PresetCbo(cmbChtUOM, frmMain.UomSetting)

        PresetCbo(cmbRptCurrencyT15, frmMain.CurrencyT15Setting)

        PresetCbo(cmbRptMeth, frmMain.MethodologySetting)
        PresetCbo(cmbChtMeth, frmMain.MethodologySetting)

		tcControlPanel.SelectedTab = tpSettings
        lblVersion.Text = "Version " & Application.ProductVersion '& " DB: " & _MyParent.DBVersion

        cbStudyPriceRpt.Checked = My.Settings.viewStudyPricingRpt
		cbStudyPriceCht.Checked = My.Settings.viewStudyPricingCht

		EnablingWatchers()
        LoadingReferenceData("Ready", False)
        lblCopyright.Text = "� " + DateTime.Today.Year.ToString() + " HSB Solomon Associates LLC"
        cbStudyPriceCht.Checked = True
        'cbStudyPriceCht.Visible = False
        cbStudyPriceRpt.Checked = True
        'cbStudyPriceRpt.Visible = False
	End Sub

    Private Sub PresetCbo(ByRef cbo As Windows.Forms.ComboBox, valueToSelect As String)
        Try
            For i As Integer = 0 To cbo.Items.Count - 1 ' Each itm As String In cbo.Items
                ' Debug.Print(cbo.Items(i).ToString())
                If cbo.Items(i)(0).ToString().Trim().ToUpper() = valueToSelect.Trim().ToUpper() Then
                    cbo.SelectedItem = cbo.Items(i)
                    Return
                End If
            Next
        Catch
        End Try
    End Sub

    Private Sub PresetCboMakesSense(ByRef cbo As Windows.Forms.ComboBox, valueToSelect As String)
        'the other PresetCbo doesn't make sense in that it needs the (i)(0) to work.
        Try
            For i As Integer = 0 To cbo.Items.Count - 1 ' Each itm As String In cbo.Items
                'Debug.Print(cbo.Items(i).ToString())
                If cbo.Items(i).ToString().Trim().ToUpper() = valueToSelect.Trim().ToUpper() Then
                    cbo.SelectedItem = cbo.Items(i)
                    Return
                End If
            Next
        Catch
        End Try
    End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents treeCharts As System.Windows.Forms.TreeView
	Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
	Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
	Friend WithEvents Label23 As System.Windows.Forms.Label
	Friend WithEvents Label27 As System.Windows.Forms.Label
	Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
	Friend WithEvents Label29 As System.Windows.Forms.Label
	Friend WithEvents btnUpload As System.Windows.Forms.Button
	Friend WithEvents btnImport As System.Windows.Forms.Button
	Friend WithEvents Label35 As System.Windows.Forms.Label
	Friend WithEvents cmbChtStart As System.Windows.Forms.ComboBox
	Friend WithEvents cmbChtEnd As System.Windows.Forms.ComboBox
	Friend WithEvents cmbChtCurrencyCode As System.Windows.Forms.ComboBox
	Friend WithEvents cmbChtUOM As System.Windows.Forms.ComboBox
	Friend WithEvents chkChtTarget As System.Windows.Forms.CheckBox
	Friend WithEvents chkChtAVG As System.Windows.Forms.CheckBox
	Friend WithEvents chkChtYTD As System.Windows.Forms.CheckBox
	Friend WithEvents btnChtCreate As System.Windows.Forms.Button
	Friend WithEvents cmbRptMonth As System.Windows.Forms.ComboBox
	Friend WithEvents chkRptYTD As System.Windows.Forms.CheckBox
	Friend WithEvents chkRptAVG As System.Windows.Forms.CheckBox
	Friend WithEvents chkRptTarget As System.Windows.Forms.CheckBox
	Friend WithEvents cmbRptCurrencyCode As System.Windows.Forms.ComboBox
	Friend WithEvents cmbRptUOM As System.Windows.Forms.ComboBox
	Friend WithEvents cmbChtMeth As System.Windows.Forms.ComboBox
	Friend WithEvents cmbRptMeth As System.Windows.Forms.ComboBox
	Friend WithEvents cbStudyPriceRpt As System.Windows.Forms.CheckBox
	Friend WithEvents cbStudyPriceCht As System.Windows.Forms.CheckBox
	Friend WithEvents tcControlPanel As System.Windows.Forms.TabControl
	Friend WithEvents tpConfig As System.Windows.Forms.TabPage
	Friend WithEvents tpDataEntry As System.Windows.Forms.TabPage
	Friend WithEvents tpTables As System.Windows.Forms.TabPage
	Friend WithEvents tpReports As System.Windows.Forms.TabPage
	Friend WithEvents tpCharts As System.Windows.Forms.TabPage
	Friend WithEvents pnlControlPanel As System.Windows.Forms.Panel
    Friend WithEvents pnlWorkSpace As System.Windows.Forms.Panel
    Friend WithEvents SA_OperatingExpenses1 As SA_OperatingExpenses
    Friend WithEvents SA_MaterialBalance1 As SA_MaterialBalance
    Friend WithEvents SA_Personnel1 As SA_Personnel
    Friend WithEvents SA_Configuration1 As SA_Configuration
    Friend WithEvents SA_Energy1 As SA_Energy
    Friend WithEvents SA_Inventory1 As SA_Inventory
    Friend WithEvents SA_CrudeCharge1 As SA_CrudeCharge
    Friend WithEvents SA_UserDefined1 As SA_UserDefined
	Friend WithEvents MonthWatcher As System.IO.FileSystemWatcher
	Friend WithEvents ConfigWatcher As System.IO.FileSystemWatcher
	Friend WithEvents ReferenceWatcher As System.IO.FileSystemWatcher
	Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents SA_RefineryInfo1 As SA_RefineryInfo
	Friend WithEvents tpSettings As System.Windows.Forms.TabPage
	Friend WithEvents pnlSettings As System.Windows.Forms.Panel
	Friend WithEvents cmbRptCurrencyT15 As System.Windows.Forms.ComboBox
	Friend WithEvents Label30 As System.Windows.Forms.Label
	Friend WithEvents cmbRptCurrency As System.Windows.Forms.ComboBox
	Friend WithEvents cmbUOM As System.Windows.Forms.ComboBox
	Friend WithEvents txtCoordPhone As System.Windows.Forms.TextBox
	Friend WithEvents txtCoordTitle As System.Windows.Forms.TextBox
	Friend WithEvents txtCoordName As System.Windows.Forms.TextBox
	Friend WithEvents txtLocation As System.Windows.Forms.TextBox
	Friend WithEvents txtCompany As System.Windows.Forms.TextBox
	Friend WithEvents txtCoordEMail As System.Windows.Forms.TextBox
	Friend WithEvents Label21 As System.Windows.Forms.Label
	Friend WithEvents Label20 As System.Windows.Forms.Label
	Friend WithEvents Label19 As System.Windows.Forms.Label
	Friend WithEvents Label18 As System.Windows.Forms.Label
	Friend WithEvents Label17 As System.Windows.Forms.Label
	Friend WithEvents Label16 As System.Windows.Forms.Label
	Friend WithEvents Label15 As System.Windows.Forms.Label
	Friend WithEvents Label14 As System.Windows.Forms.Label
	Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
	Friend WithEvents btnAdmin As System.Windows.Forms.Button
    Friend WithEvents SA_Admin1 As SA_Admin
    Friend WithEvents SA_Process1 As SA_Process
    Friend WithEvents SA_Browser1 As SA_Browser
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
	Friend WithEvents lvUsers As System.Windows.Forms.ListView
	Friend WithEvents lvDataEntry As System.Windows.Forms.ListView
	Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
	Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
	Friend WithEvents dgvEvents As System.Windows.Forms.DataGridView
	Friend WithEvents btnClearLog As System.Windows.Forms.Button
	Friend WithEvents FileSystemWatcher1 As System.IO.FileSystemWatcher
	Friend WithEvents lvReports As System.Windows.Forms.ListView
	Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
	Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
	Friend WithEvents Label73 As System.Windows.Forms.Label
	Friend WithEvents Label77 As System.Windows.Forms.Label
	Friend WithEvents btnClear As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
	Friend WithEvents btnClose As System.Windows.Forms.Button
	Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
	Friend WithEvents Label37 As System.Windows.Forms.Label
	Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents SA_Import1 As SA_Import
	Friend WithEvents pnlBlank As System.Windows.Forms.Panel
	Friend WithEvents gbAbout As System.Windows.Forms.GroupBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents btnCP As System.Windows.Forms.Button
	Friend WithEvents Panel3 As System.Windows.Forms.Panel
	Friend WithEvents btnWorkspace As System.Windows.Forms.Button
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents Panel10 As System.Windows.Forms.Panel
	Friend WithEvents btnRptCreate As System.Windows.Forms.Button
	Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents Label7 As System.Windows.Forms.Label
	Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
	Friend WithEvents Button1 As System.Windows.Forms.Button
	Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
	Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
	Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
	Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
	Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
	Friend WithEvents Label8 As System.Windows.Forms.Label
	Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
	Friend WithEvents Label22 As System.Windows.Forms.Label
	Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
	Friend WithEvents EventDate As System.Windows.Forms.DataGridViewTextBoxColumn
	Friend WithEvents EventDescription As System.Windows.Forms.DataGridViewTextBoxColumn
	Friend WithEvents EventUser As System.Windows.Forms.DataGridViewTextBoxColumn
	Friend WithEvents treeConfiguration As System.Windows.Forms.TreeView
	Friend WithEvents ListView1 As System.Windows.Forms.ListView
	Friend WithEvents ListView2 As System.Windows.Forms.ListView
    Friend WithEvents lblCopyright As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Panel17 As System.Windows.Forms.Panel
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents treeTableViews As System.Windows.Forms.TreeView
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents cboChartType As System.Windows.Forms.ComboBox
    Friend WithEvents cboChartColor As System.Windows.Forms.ComboBox
    Friend WithEvents cboChartYtdColor As System.Windows.Forms.ComboBox
    Friend WithEvents cboChartAvgColor As System.Windows.Forms.ComboBox
    Friend WithEvents cboChartTargetColor As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowChartDataTable As System.Windows.Forms.CheckBox
	Friend WithEvents btnRadarPlot As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("TWS", 0)
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Process Facilities")
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Inventory")
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Operating Expenses")
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Personnel")
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Crude Charge Detail")
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Material Balance")
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Energy")
        Dim ListViewItem9 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("User-Defined Inputs/Outputs")
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Process Facilities")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Inventory")
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 1", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2})
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 2")
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 4")
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 5")
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 6")
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 7")
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 9")
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 10")
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 14")
        Dim TreeNode12 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Raw Material Inputs")
        Dim TreeNode13 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Product Yields")
        Dim TreeNode14 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 15", New System.Windows.Forms.TreeNode() {TreeNode12, TreeNode13})
        Dim TreeNode15 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Thermal Energy")
        Dim TreeNode16 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Electricity")
        Dim TreeNode17 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table 16", New System.Windows.Forms.TreeNode() {TreeNode15, TreeNode16})
        Dim TreeNode18 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("User-Defined Inputs/Outputs")
        Dim TreeNode19 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Refinery Level")
        Dim TreeNode20 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Process Unit Level")
        Dim TreeNode21 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Performance Targets", New System.Windows.Forms.TreeNode() {TreeNode19, TreeNode20})
        Dim TreeNode22 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Historical Maintenance Costs")
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnlControlPanel = New System.Windows.Forms.Panel()
        Me.tcControlPanel = New System.Windows.Forms.TabControl()
        Me.tpSettings = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.lvUsers = New System.Windows.Forms.ListView()
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnAdmin = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.pnlSettings = New System.Windows.Forms.Panel()
        Me.cmbRptCurrencyT15 = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.cmbRptCurrency = New System.Windows.Forms.ComboBox()
        Me.cmbUOM = New System.Windows.Forms.ComboBox()
        Me.txtCoordPhone = New System.Windows.Forms.TextBox()
        Me.txtCoordTitle = New System.Windows.Forms.TextBox()
        Me.txtCoordName = New System.Windows.Forms.TextBox()
        Me.txtLocation = New System.Windows.Forms.TextBox()
        Me.txtCompany = New System.Windows.Forms.TextBox()
        Me.txtCoordEMail = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.tpConfig = New System.Windows.Forms.TabPage()
        Me.treeConfiguration = New System.Windows.Forms.TreeView()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.tpDataEntry = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.dgvEvents = New System.Windows.Forms.DataGridView()
        Me.EventDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EventDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EventUser = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.btnClearLog = New System.Windows.Forms.Button()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lvDataEntry = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.tpTables = New System.Windows.Forms.TabPage()
        Me.treeTableViews = New System.Windows.Forms.TreeView()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.tpReports = New System.Windows.Forms.TabPage()
        Me.lvReports = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label49 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmbRptMonth = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.cmbRptMeth = New System.Windows.Forms.ComboBox()
        Me.cmbRptCurrencyCode = New System.Windows.Forms.ComboBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.cmbRptUOM = New System.Windows.Forms.ComboBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.chkRptTarget = New System.Windows.Forms.CheckBox()
        Me.chkRptAVG = New System.Windows.Forms.CheckBox()
        Me.chkRptYTD = New System.Windows.Forms.CheckBox()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.cbStudyPriceRpt = New System.Windows.Forms.CheckBox()
        Me.btnRptCreate = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.tpCharts = New System.Windows.Forms.TabPage()
        Me.treeCharts = New System.Windows.Forms.TreeView()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboChartYtdColor = New System.Windows.Forms.ComboBox()
        Me.cboChartAvgColor = New System.Windows.Forms.ComboBox()
        Me.cboChartTargetColor = New System.Windows.Forms.ComboBox()
        Me.cboChartColor = New System.Windows.Forms.ComboBox()
        Me.cmbChtStart = New System.Windows.Forms.ComboBox()
        Me.cmbChtEnd = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbChtMeth = New System.Windows.Forms.ComboBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.cmbChtCurrencyCode = New System.Windows.Forms.ComboBox()
        Me.cmbChtUOM = New System.Windows.Forms.ComboBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.chkChtTarget = New System.Windows.Forms.CheckBox()
        Me.chkChtAVG = New System.Windows.Forms.CheckBox()
        Me.chkChtYTD = New System.Windows.Forms.CheckBox()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.cbStudyPriceCht = New System.Windows.Forms.CheckBox()
        Me.btnChtCreate = New System.Windows.Forms.Button()
        Me.cboChartType = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCP = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.pnlWorkSpace = New System.Windows.Forms.Panel()
        Me.pnlBlank = New System.Windows.Forms.Panel()
        Me.gbAbout = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCopyright = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnWorkspace = New System.Windows.Forms.Button()
        Me.SA_Import1 = New SA_Import()
        Me.SA_CrudeCharge1 = New SA_CrudeCharge()
        Me.SA_Personnel1 = New SA_Personnel()
        Me.SA_OperatingExpenses1 = New SA_OperatingExpenses()
        Me.SA_UserDefined1 = New SA_UserDefined()
        Me.SA_Configuration1 = New SA_Configuration()
        Me.SA_Energy1 = New SA_Energy()
        Me.SA_Inventory1 = New SA_Inventory()
        Me.SA_MaterialBalance1 = New SA_MaterialBalance()
        Me.SA_RefineryInfo1 = New SA_RefineryInfo()
        Me.SA_Process1 = New SA_Process()
        Me.SA_Admin1 = New SA_Admin()
        Me.SA_Browser1 = New SA_Browser()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.MonthWatcher = New System.IO.FileSystemWatcher()
        Me.ConfigWatcher = New System.IO.FileSystemWatcher()
        Me.ReferenceWatcher = New System.IO.FileSystemWatcher()
        Me.FileSystemWatcher1 = New System.IO.FileSystemWatcher()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.chkShowChartDataTable = New System.Windows.Forms.CheckBox()
        Me.btnRadarPlot = New System.Windows.Forms.Button()
        Me.pnlControlPanel.SuspendLayout()
        Me.tcControlPanel.SuspendLayout()
        Me.tpSettings.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.pnlSettings.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpConfig.SuspendLayout()
        Me.Panel18.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpDataEntry.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel12.SuspendLayout()
        CType(Me.dgvEvents, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel13.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpTables.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpReports.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpCharts.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlWorkSpace.SuspendLayout()
        Me.pnlBlank.SuspendLayout()
        Me.gbAbout.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.MonthWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConfigWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReferenceWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(5, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(257, 25)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Refinery Configuration"
        '
        'pnlControlPanel
        '
        Me.pnlControlPanel.Controls.Add(Me.tcControlPanel)
        Me.pnlControlPanel.Controls.Add(Me.Panel1)
        Me.pnlControlPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlControlPanel.Location = New System.Drawing.Point(0, 0)
        Me.pnlControlPanel.Name = "pnlControlPanel"
        Me.pnlControlPanel.Size = New System.Drawing.Size(316, 674)
        Me.pnlControlPanel.TabIndex = 1
        '
        'tcControlPanel
        '
        Me.tcControlPanel.Controls.Add(Me.tpSettings)
        Me.tcControlPanel.Controls.Add(Me.tpConfig)
        Me.tcControlPanel.Controls.Add(Me.tpDataEntry)
        Me.tcControlPanel.Controls.Add(Me.tpTables)
        Me.tcControlPanel.Controls.Add(Me.tpReports)
        Me.tcControlPanel.Controls.Add(Me.tpCharts)
        Me.tcControlPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcControlPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcControlPanel.Location = New System.Drawing.Point(0, 24)
        Me.tcControlPanel.Margin = New System.Windows.Forms.Padding(4)
        Me.tcControlPanel.Multiline = True
        Me.tcControlPanel.Name = "tcControlPanel"
        Me.tcControlPanel.SelectedIndex = 0
        Me.tcControlPanel.Size = New System.Drawing.Size(316, 650)
        Me.tcControlPanel.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.tcControlPanel.TabIndex = 7
        Me.tcControlPanel.TabStop = False
        '
        'tpSettings
        '
        Me.tpSettings.Controls.Add(Me.GroupBox5)
        Me.tpSettings.Controls.Add(Me.pnlSettings)
        Me.tpSettings.Controls.Add(Me.Panel2)
        Me.tpSettings.Location = New System.Drawing.Point(4, 40)
        Me.tpSettings.Name = "tpSettings"
        Me.tpSettings.Padding = New System.Windows.Forms.Padding(3)
        Me.tpSettings.Size = New System.Drawing.Size(308, 606)
        Me.tpSettings.TabIndex = 6
        Me.tpSettings.Text = "Settings"
        Me.tpSettings.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Panel10)
        Me.GroupBox5.Controls.Add(Me.Panel5)
        Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox5.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.GroupBox5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox5.Location = New System.Drawing.Point(3, 375)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox5.Size = New System.Drawing.Size(302, 228)
        Me.GroupBox5.TabIndex = 14
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Settings Tools"
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Panel10.Controls.Add(Me.lvUsers)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel10.Location = New System.Drawing.Point(0, 14)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Padding = New System.Windows.Forms.Padding(12, 6, 12, 12)
        Me.Panel10.Size = New System.Drawing.Size(302, 178)
        Me.Panel10.TabIndex = 228
        '
        'lvUsers
        '
        Me.lvUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvUsers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvUsers.ForeColor = System.Drawing.Color.MediumBlue
        Me.lvUsers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem1.StateImageIndex = 0
        Me.lvUsers.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})
        Me.lvUsers.LargeImageList = Me.ImageList2
        Me.lvUsers.Location = New System.Drawing.Point(12, 6)
        Me.lvUsers.Margin = New System.Windows.Forms.Padding(0)
        Me.lvUsers.MultiSelect = False
        Me.lvUsers.Name = "lvUsers"
        Me.lvUsers.ShowGroups = False
        Me.lvUsers.Size = New System.Drawing.Size(278, 160)
        Me.lvUsers.SmallImageList = Me.ImageList2
        Me.lvUsers.TabIndex = 216
        Me.lvUsers.TabStop = False
        Me.lvUsers.TileSize = New System.Drawing.Size(24, 24)
        Me.ToolTip1.SetToolTip(Me.lvUsers, "Refinery users that are currently signed in")
        Me.lvUsers.UseCompatibleStateImageBehavior = False
        '
        'SA_Browser1
        '
        Me.SA_Browser1.BackColor = System.Drawing.SystemColors.Control
        Me.SA_Browser1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Browser1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Browser1.Name = "SA_Browser1"
        Me.SA_Browser1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Browser1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Browser1.TabIndex = 72
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "users.png")
        Me.ImageList2.Images.SetKeyName(1, "users.png")
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gray
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.btnSave)
        Me.Panel5.Controls.Add(Me.btnClear)
        Me.Panel5.Controls.Add(Me.btnAdmin)
        Me.Panel5.Controls.Add(Me.btnClose)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 192)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(302, 36)
        Me.Panel5.TabIndex = 229
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.Gray
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(0, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnSave.Size = New System.Drawing.Size(75, 34)
        Me.btnSave.TabIndex = 222
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save the settings above")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.Gray
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.Color.White
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(75, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnClear.Size = New System.Drawing.Size(75, 34)
        Me.btnClear.TabIndex = 223
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clear the settings above")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnAdmin
        '
        Me.btnAdmin.BackColor = System.Drawing.Color.Gray
        Me.btnAdmin.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnAdmin.FlatAppearance.BorderSize = 0
        Me.btnAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdmin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdmin.ForeColor = System.Drawing.Color.White
        Me.btnAdmin.Image = CType(resources.GetObject("btnAdmin.Image"), System.Drawing.Image)
        Me.btnAdmin.Location = New System.Drawing.Point(150, 0)
        Me.btnAdmin.Name = "btnAdmin"
        Me.btnAdmin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnAdmin.Size = New System.Drawing.Size(75, 34)
        Me.btnAdmin.TabIndex = 213
        Me.btnAdmin.TabStop = False
        Me.btnAdmin.Text = "Admin"
        Me.btnAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdmin.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnAdmin, "Open Administrative Console")
        Me.btnAdmin.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.Gray
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(225, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnClose.Size = New System.Drawing.Size(75, 34)
        Me.btnClose.TabIndex = 221
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close current month")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'pnlSettings
        '
        Me.pnlSettings.AutoScroll = True
        Me.pnlSettings.Controls.Add(Me.cmbRptCurrencyT15)
        Me.pnlSettings.Controls.Add(Me.Label30)
        Me.pnlSettings.Controls.Add(Me.cmbRptCurrency)
        Me.pnlSettings.Controls.Add(Me.cmbUOM)
        Me.pnlSettings.Controls.Add(Me.txtCoordPhone)
        Me.pnlSettings.Controls.Add(Me.txtCoordTitle)
        Me.pnlSettings.Controls.Add(Me.txtCoordName)
        Me.pnlSettings.Controls.Add(Me.txtLocation)
        Me.pnlSettings.Controls.Add(Me.txtCompany)
        Me.pnlSettings.Controls.Add(Me.txtCoordEMail)
        Me.pnlSettings.Controls.Add(Me.Label21)
        Me.pnlSettings.Controls.Add(Me.Label20)
        Me.pnlSettings.Controls.Add(Me.Label19)
        Me.pnlSettings.Controls.Add(Me.Label18)
        Me.pnlSettings.Controls.Add(Me.Label17)
        Me.pnlSettings.Controls.Add(Me.Label16)
        Me.pnlSettings.Controls.Add(Me.Label15)
        Me.pnlSettings.Controls.Add(Me.Label14)
        Me.pnlSettings.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlSettings.Location = New System.Drawing.Point(3, 88)
        Me.pnlSettings.Name = "pnlSettings"
        Me.pnlSettings.Size = New System.Drawing.Size(302, 287)
        Me.pnlSettings.TabIndex = 216
        '
        'cmbRptCurrencyT15
        '
        Me.cmbRptCurrencyT15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbRptCurrencyT15.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbRptCurrencyT15.DisplayMember = "1"
        Me.cmbRptCurrencyT15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRptCurrencyT15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRptCurrencyT15.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbRptCurrencyT15.ItemHeight = 13
        Me.cmbRptCurrencyT15.Location = New System.Drawing.Point(184, 248)
        Me.cmbRptCurrencyT15.MaxDropDownItems = 10
        Me.cmbRptCurrencyT15.Name = "cmbRptCurrencyT15"
        Me.cmbRptCurrencyT15.Size = New System.Drawing.Size(104, 21)
        Me.cmbRptCurrencyT15.TabIndex = 9
        Me.cmbRptCurrencyT15.ValueMember = "1"
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label30.Location = New System.Drawing.Point(16, 248)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(152, 23)
        Me.Label30.TabIndex = 204
        Me.Label30.Text = "Table 15 Reported Currency"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbRptCurrency
        '
        Me.cmbRptCurrency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbRptCurrency.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbRptCurrency.DisplayMember = "1"
        Me.cmbRptCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRptCurrency.DropDownWidth = 200
        Me.cmbRptCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRptCurrency.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbRptCurrency.ItemHeight = 13
        Me.cmbRptCurrency.Location = New System.Drawing.Point(120, 221)
        Me.cmbRptCurrency.MaxDropDownItems = 10
        Me.cmbRptCurrency.Name = "cmbRptCurrency"
        Me.cmbRptCurrency.Size = New System.Drawing.Size(168, 21)
        Me.cmbRptCurrency.TabIndex = 8
        Me.cmbRptCurrency.ValueMember = "1"
        '
        'cmbUOM
        '
        Me.cmbUOM.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbUOM.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbUOM.DisplayMember = "1"
        Me.cmbUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbUOM.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbUOM.ItemHeight = 13
        Me.cmbUOM.Items.AddRange(New Object() {"US Units", "Metric"})
        Me.cmbUOM.Location = New System.Drawing.Point(120, 194)
        Me.cmbUOM.MaxDropDownItems = 2
        Me.cmbUOM.Name = "cmbUOM"
        Me.cmbUOM.Size = New System.Drawing.Size(168, 21)
        Me.cmbUOM.TabIndex = 7
        Me.cmbUOM.ValueMember = "1"
        '
        'txtCoordPhone
        '
        Me.txtCoordPhone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCoordPhone.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCoordPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoordPhone.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtCoordPhone.Location = New System.Drawing.Point(120, 121)
        Me.txtCoordPhone.Name = "txtCoordPhone"
        Me.txtCoordPhone.Size = New System.Drawing.Size(168, 21)
        Me.txtCoordPhone.TabIndex = 5
        '
        'txtCoordTitle
        '
        Me.txtCoordTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCoordTitle.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCoordTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoordTitle.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtCoordTitle.Location = New System.Drawing.Point(120, 94)
        Me.txtCoordTitle.Name = "txtCoordTitle"
        Me.txtCoordTitle.Size = New System.Drawing.Size(168, 21)
        Me.txtCoordTitle.TabIndex = 4
        '
        'txtCoordName
        '
        Me.txtCoordName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCoordName.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCoordName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoordName.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtCoordName.Location = New System.Drawing.Point(120, 67)
        Me.txtCoordName.Name = "txtCoordName"
        Me.txtCoordName.Size = New System.Drawing.Size(168, 21)
        Me.txtCoordName.TabIndex = 3
        '
        'txtLocation
        '
        Me.txtLocation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLocation.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtLocation.Location = New System.Drawing.Point(120, 40)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(168, 21)
        Me.txtLocation.TabIndex = 2
        '
        'txtCompany
        '
        Me.txtCompany.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCompany.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompany.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtCompany.Location = New System.Drawing.Point(120, 13)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(168, 21)
        Me.txtCompany.TabIndex = 1
        '
        'txtCoordEMail
        '
        Me.txtCoordEMail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCoordEMail.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCoordEMail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoordEMail.ForeColor = System.Drawing.Color.MediumBlue
        Me.txtCoordEMail.Location = New System.Drawing.Point(120, 147)
        Me.txtCoordEMail.Name = "txtCoordEMail"
        Me.txtCoordEMail.Size = New System.Drawing.Size(168, 21)
        Me.txtCoordEMail.TabIndex = 6
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(16, 221)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(104, 23)
        Me.Label21.TabIndex = 185
        Me.Label21.Text = "Currency"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(16, 194)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(104, 23)
        Me.Label20.TabIndex = 184
        Me.Label20.Text = "Units of Measure"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(16, 147)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(104, 23)
        Me.Label19.TabIndex = 183
        Me.Label19.Text = "E-Mail"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(16, 121)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(104, 23)
        Me.Label18.TabIndex = 182
        Me.Label18.Text = "Phone"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(16, 94)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(104, 23)
        Me.Label17.TabIndex = 181
        Me.Label17.Text = "Contact Title"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(16, 67)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(104, 23)
        Me.Label16.TabIndex = 180
        Me.Label16.Text = "Contact Name"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(16, 40)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(104, 23)
        Me.Label15.TabIndex = 179
        Me.Label15.Text = "Refinery Location"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(16, 13)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(104, 23)
        Me.Label14.TabIndex = 178
        Me.Label14.Text = "Company Name"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(6, 3, 12, 0)
        Me.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel2.Size = New System.Drawing.Size(302, 85)
        Me.Panel2.TabIndex = 217
        '
        'PictureBox2
        '
        Me.PictureBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(87, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(203, 44)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 16
        Me.PictureBox2.TabStop = False
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Gray
        Me.Label10.Location = New System.Drawing.Point(87, 47)
        Me.Label10.Name = "Label10"
        Me.Label10.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label10.Size = New System.Drawing.Size(203, 38)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Refinery Settings and Preferences"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = Global.Solomon.Profile.My.Resources.Resources.Logo
        Me.PictureBox1.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(81, 82)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox1, "Click to go www.SolomonOnline.com")
        '
        'tpConfig
        '
        Me.tpConfig.BackColor = System.Drawing.Color.Transparent
        Me.tpConfig.Controls.Add(Me.treeConfiguration)
        Me.tpConfig.Controls.Add(Me.ListView1)
        Me.tpConfig.Controls.Add(Me.Label32)
        Me.tpConfig.Controls.Add(Me.Panel18)
        Me.tpConfig.Location = New System.Drawing.Point(4, 40)
        Me.tpConfig.Name = "tpConfig"
        Me.tpConfig.Padding = New System.Windows.Forms.Padding(3)
        Me.tpConfig.Size = New System.Drawing.Size(308, 606)
        Me.tpConfig.TabIndex = 1
        Me.tpConfig.Text = "Configuration"
        Me.tpConfig.UseVisualStyleBackColor = True
        Me.tpConfig.Visible = False
        '
        'treeConfiguration
        '
        Me.treeConfiguration.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.treeConfiguration.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.treeConfiguration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeConfiguration.ForeColor = System.Drawing.Color.MediumBlue
        Me.treeConfiguration.HotTracking = True
        Me.treeConfiguration.Location = New System.Drawing.Point(6, 103)
        Me.treeConfiguration.Name = "treeConfiguration"
        Me.treeConfiguration.ShowNodeToolTips = True
        Me.treeConfiguration.ShowRootLines = False
        Me.treeConfiguration.Size = New System.Drawing.Size(296, 495)
        Me.treeConfiguration.TabIndex = 9
        '
        'ListView1
        '
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.Location = New System.Drawing.Point(3, 100)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(302, 503)
        Me.ListView1.TabIndex = 226
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'Label32
        '
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label32.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.Gray
        Me.Label32.Location = New System.Drawing.Point(3, 88)
        Me.Label32.Name = "Label32"
        Me.Label32.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label32.Size = New System.Drawing.Size(302, 12)
        Me.Label32.TabIndex = 232
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel18
        '
        Me.Panel18.Controls.Add(Me.PictureBox8)
        Me.Panel18.Controls.Add(Me.Label11)
        Me.Panel18.Controls.Add(Me.PictureBox9)
        Me.Panel18.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel18.Location = New System.Drawing.Point(3, 3)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Padding = New System.Windows.Forms.Padding(6, 3, 12, 0)
        Me.Panel18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel18.Size = New System.Drawing.Size(302, 85)
        Me.Panel18.TabIndex = 227
        '
        'PictureBox8
        '
        Me.PictureBox8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(87, 3)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(203, 44)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox8.TabIndex = 16
        Me.PictureBox8.TabStop = False
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Gray
        Me.Label11.Location = New System.Drawing.Point(87, 47)
        Me.Label11.Name = "Label11"
        Me.Label11.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label11.Size = New System.Drawing.Size(203, 38)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Refinery Configuration, Turnarounds, and Targets"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox9.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox9.Image = Global.Solomon.Profile.My.Resources.Resources.Logo
        Me.PictureBox9.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(81, 82)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox9.TabIndex = 9
        Me.PictureBox9.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox9, "Click to go www.SolomonOnline.com")
        '
        'tpDataEntry
        '
        Me.tpDataEntry.BackColor = System.Drawing.Color.Transparent
        Me.tpDataEntry.Controls.Add(Me.GroupBox1)
        Me.tpDataEntry.Controls.Add(Me.Label9)
        Me.tpDataEntry.Controls.Add(Me.lvDataEntry)
        Me.tpDataEntry.Controls.Add(Me.Label31)
        Me.tpDataEntry.Controls.Add(Me.Panel4)
        Me.tpDataEntry.Location = New System.Drawing.Point(4, 40)
        Me.tpDataEntry.Name = "tpDataEntry"
        Me.tpDataEntry.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDataEntry.Size = New System.Drawing.Size(308, 606)
        Me.tpDataEntry.TabIndex = 2
        Me.tpDataEntry.Text = "Data Entry"
        Me.tpDataEntry.UseVisualStyleBackColor = True
        Me.tpDataEntry.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel12)
        Me.GroupBox1.Controls.Add(Me.Panel13)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(3, 267)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.GroupBox1.Size = New System.Drawing.Size(302, 336)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Entry Tools"
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.dgvEvents)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel12.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Panel12.Location = New System.Drawing.Point(0, 17)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Padding = New System.Windows.Forms.Padding(12, 6, 12, 12)
        Me.Panel12.Size = New System.Drawing.Size(302, 283)
        Me.Panel12.TabIndex = 951
        Me.ToolTip1.SetToolTip(Me.Panel12, "Communication with Solomon server")
        '
        'dgvEvents
        '
        Me.dgvEvents.AllowUserToAddRows = False
        Me.dgvEvents.AllowUserToDeleteRows = False
        Me.dgvEvents.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvEvents.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEvents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvEvents.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvEvents.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEvents.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEvents.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EventDate, Me.EventDescription, Me.EventUser})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEvents.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvEvents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEvents.Location = New System.Drawing.Point(12, 6)
        Me.dgvEvents.MultiSelect = False
        Me.dgvEvents.Name = "dgvEvents"
        Me.dgvEvents.ReadOnly = True
        Me.dgvEvents.RowHeadersVisible = False
        Me.dgvEvents.RowHeadersWidth = 25
        Me.dgvEvents.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEvents.Size = New System.Drawing.Size(278, 265)
        Me.dgvEvents.TabIndex = 948
        '
        'EventDate
        '
        Me.EventDate.DataPropertyName = "EventDate"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.EventDate.DefaultCellStyle = DataGridViewCellStyle3
        Me.EventDate.FillWeight = 75.0!
        Me.EventDate.Frozen = True
        Me.EventDate.HeaderText = "Date"
        Me.EventDate.Name = "EventDate"
        Me.EventDate.ReadOnly = True
        Me.EventDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.EventDate.Width = 36
        '
        'EventDescription
        '
        Me.EventDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.EventDescription.DataPropertyName = "EventDescription"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.EventDescription.DefaultCellStyle = DataGridViewCellStyle4
        Me.EventDescription.FillWeight = 150.0!
        Me.EventDescription.HeaderText = "Description of Activity"
        Me.EventDescription.Name = "EventDescription"
        Me.EventDescription.ReadOnly = True
        Me.EventDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.EventDescription.Width = 74
        '
        'EventUser
        '
        Me.EventUser.DataPropertyName = "EventUser"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.EventUser.DefaultCellStyle = DataGridViewCellStyle5
        Me.EventUser.FillWeight = 60.0!
        Me.EventUser.HeaderText = "User"
        Me.EventUser.Name = "EventUser"
        Me.EventUser.ReadOnly = True
        Me.EventUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.EventUser.Width = 35
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.Gray
        Me.Panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel13.Controls.Add(Me.btnClearLog)
        Me.Panel13.Controls.Add(Me.btnUpload)
        Me.Panel13.Controls.Add(Me.btnImport)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel13.Location = New System.Drawing.Point(0, 300)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(302, 36)
        Me.Panel13.TabIndex = 952
        '
        'btnClearLog
        '
        Me.btnClearLog.BackColor = System.Drawing.Color.Gray
        Me.btnClearLog.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClearLog.FlatAppearance.BorderSize = 0
        Me.btnClearLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearLog.ForeColor = System.Drawing.Color.White
        Me.btnClearLog.Image = CType(resources.GetObject("btnClearLog.Image"), System.Drawing.Image)
        Me.btnClearLog.Location = New System.Drawing.Point(225, 0)
        Me.btnClearLog.Name = "btnClearLog"
        Me.btnClearLog.Size = New System.Drawing.Size(75, 34)
        Me.btnClearLog.TabIndex = 949
        Me.btnClearLog.TabStop = False
        Me.btnClearLog.Text = "Clear"
        Me.btnClearLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClearLog.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClearLog, "Clear event log below")
        Me.btnClearLog.UseVisualStyleBackColor = False
        '
        'btnUpload
        '
        Me.btnUpload.BackColor = System.Drawing.Color.Gray
        Me.btnUpload.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnUpload.FlatAppearance.BorderSize = 0
        Me.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpload.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpload.ForeColor = System.Drawing.Color.White
        Me.btnUpload.Image = Global.Solomon.Profile.My.Resources.Resources.completeTest
        Me.btnUpload.Location = New System.Drawing.Point(75, 0)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(75, 34)
        Me.btnUpload.TabIndex = 13
        Me.btnUpload.TabStop = False
        Me.btnUpload.Text = "Upload"
        Me.btnUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnUpload, "Upload data to Solomon")
        Me.btnUpload.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.Color.Gray
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.White
        Me.btnImport.Image = Global.Solomon.Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(0, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(75, 34)
        Me.btnImport.TabIndex = 12
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Import all input forms from bridge file(s)")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Gray
        Me.Label9.Location = New System.Drawing.Point(3, 255)
        Me.Label9.Name = "Label9"
        Me.Label9.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label9.Size = New System.Drawing.Size(302, 12)
        Me.Label9.TabIndex = 231
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lvDataEntry
        '
        Me.lvDataEntry.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvDataEntry.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvDataEntry.Dock = System.Windows.Forms.DockStyle.Top
        Me.lvDataEntry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDataEntry.ForeColor = System.Drawing.Color.MediumBlue
        Me.lvDataEntry.FullRowSelect = True
        Me.lvDataEntry.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem2.UseItemStyleForSubItems = False
        Me.lvDataEntry.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5, ListViewItem6, ListViewItem7, ListViewItem8, ListViewItem9})
        Me.lvDataEntry.LabelWrap = False
        Me.lvDataEntry.LargeImageList = Me.ImageList2
        Me.lvDataEntry.Location = New System.Drawing.Point(3, 100)
        Me.lvDataEntry.Margin = New System.Windows.Forms.Padding(8)
        Me.lvDataEntry.MultiSelect = False
        Me.lvDataEntry.Name = "lvDataEntry"
        Me.lvDataEntry.Scrollable = False
        Me.lvDataEntry.Size = New System.Drawing.Size(302, 155)
        Me.lvDataEntry.TabIndex = 229
        Me.lvDataEntry.TabStop = False
        Me.lvDataEntry.UseCompatibleStateImageBehavior = False
        Me.lvDataEntry.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Input Form"
        Me.ColumnHeader1.Width = 240
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Status"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.Gray
        Me.Label31.Location = New System.Drawing.Point(3, 88)
        Me.Label31.Name = "Label31"
        Me.Label31.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label31.Size = New System.Drawing.Size(302, 12)
        Me.Label31.TabIndex = 232
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.PictureBox3)
        Me.Panel4.Controls.Add(Me.Label24)
        Me.Panel4.Controls.Add(Me.PictureBox10)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(3, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Padding = New System.Windows.Forms.Padding(6, 3, 12, 0)
        Me.Panel4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel4.Size = New System.Drawing.Size(302, 85)
        Me.Panel4.TabIndex = 230
        '
        'PictureBox3
        '
        Me.PictureBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(87, 3)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(203, 44)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox3.TabIndex = 16
        Me.PictureBox3.TabStop = False
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Gray
        Me.Label24.Location = New System.Drawing.Point(87, 47)
        Me.Label24.Name = "Label24"
        Me.Label24.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label24.Size = New System.Drawing.Size(203, 38)
        Me.Label24.TabIndex = 15
        Me.Label24.Text = "Monthly Data to be Submitted for Calculations"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox10.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox10.Image = Global.Solomon.Profile.My.Resources.Resources.Logo
        Me.PictureBox10.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(81, 82)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox10.TabIndex = 9
        Me.PictureBox10.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox10, "Click to go www.SolomonOnline.com")
        '
        'tpTables
        '
        Me.tpTables.BackColor = System.Drawing.Color.Transparent
        Me.tpTables.Controls.Add(Me.treeTableViews)
        Me.tpTables.Controls.Add(Me.ListView2)
        Me.tpTables.Controls.Add(Me.Label4)
        Me.tpTables.Controls.Add(Me.Label73)
        Me.tpTables.Controls.Add(Me.Label36)
        Me.tpTables.Controls.Add(Me.Panel6)
        Me.tpTables.Location = New System.Drawing.Point(4, 40)
        Me.tpTables.Name = "tpTables"
        Me.tpTables.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTables.Size = New System.Drawing.Size(308, 606)
        Me.tpTables.TabIndex = 3
        Me.tpTables.Text = "Table Views"
        Me.tpTables.UseVisualStyleBackColor = True
        Me.tpTables.Visible = False
        '
        'treeTableViews
        '
        Me.treeTableViews.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.treeTableViews.BackColor = System.Drawing.Color.White
        Me.treeTableViews.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.treeTableViews.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeTableViews.ForeColor = System.Drawing.Color.MediumBlue
        Me.treeTableViews.HotTracking = True
        Me.treeTableViews.Location = New System.Drawing.Point(5, 103)
        Me.treeTableViews.Name = "treeTableViews"
        TreeNode1.Name = ""
        TreeNode1.Text = "Process Facilities"
        TreeNode2.Name = ""
        TreeNode2.Text = "Inventory"
        TreeNode3.Name = ""
        TreeNode3.Text = "Table 1"
        TreeNode4.Name = ""
        TreeNode4.Text = "Table 2"
        TreeNode5.Name = ""
        TreeNode5.Text = "Table 4"
        TreeNode6.Name = ""
        TreeNode6.Text = "Table 5"
        TreeNode7.Name = ""
        TreeNode7.Text = "Table 6"
        TreeNode8.Name = ""
        TreeNode8.Text = "Table 7"
        TreeNode9.Name = ""
        TreeNode9.Text = "Table 9"
        TreeNode10.Name = ""
        TreeNode10.Text = "Table 10"
        TreeNode11.Name = ""
        TreeNode11.Text = "Table 14"
        TreeNode12.Name = ""
        TreeNode12.Text = "Raw Material Inputs"
        TreeNode13.Name = ""
        TreeNode13.Text = "Product Yields"
        TreeNode14.Name = ""
        TreeNode14.Text = "Table 15"
        TreeNode15.Name = ""
        TreeNode15.Text = "Thermal Energy"
        TreeNode16.Name = ""
        TreeNode16.Text = "Electricity"
        TreeNode17.Name = ""
        TreeNode17.Text = "Table 16"
        TreeNode18.Name = ""
        TreeNode18.Text = "User-Defined Inputs/Outputs"
        TreeNode19.Name = ""
        TreeNode19.Text = "Refinery Level"
        TreeNode20.Name = ""
        TreeNode20.Text = "Process Unit Level"
        TreeNode21.Name = ""
        TreeNode21.Text = "Performance Targets"
        TreeNode22.Name = ""
        TreeNode22.Text = "Historical Maintenance Costs"
        Me.treeTableViews.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode3, TreeNode4, TreeNode5, TreeNode6, TreeNode7, TreeNode8, TreeNode9, TreeNode10, TreeNode11, TreeNode14, TreeNode17, TreeNode18, TreeNode21, TreeNode22})
        Me.treeTableViews.Scrollable = False
        Me.treeTableViews.Size = New System.Drawing.Size(298, 497)
        Me.treeTableViews.TabIndex = 12
        '
        'ListView2
        '
        Me.ListView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView2.Location = New System.Drawing.Point(3, 100)
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(302, 501)
        Me.ListView2.TabIndex = 232
        Me.ListView2.UseCompatibleStateImageBehavior = False
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(3, 601)
        Me.Label4.Name = "Label4"
        Me.Label4.Padding = New System.Windows.Forms.Padding(0, 0, 8, 0)
        Me.Label4.Size = New System.Drawing.Size(302, 1)
        Me.Label4.TabIndex = 231
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label73
        '
        Me.Label73.BackColor = System.Drawing.SystemColors.Control
        Me.Label73.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label73.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label73.Location = New System.Drawing.Point(3, 602)
        Me.Label73.Name = "Label73"
        Me.Label73.Padding = New System.Windows.Forms.Padding(0, 0, 8, 0)
        Me.Label73.Size = New System.Drawing.Size(302, 1)
        Me.Label73.TabIndex = 230
        Me.Label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.Gray
        Me.Label36.Location = New System.Drawing.Point(3, 88)
        Me.Label36.Name = "Label36"
        Me.Label36.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label36.Size = New System.Drawing.Size(302, 12)
        Me.Label36.TabIndex = 234
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.PictureBox4)
        Me.Panel6.Controls.Add(Me.Label43)
        Me.Panel6.Controls.Add(Me.PictureBox11)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(3, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Padding = New System.Windows.Forms.Padding(6, 3, 12, 0)
        Me.Panel6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel6.Size = New System.Drawing.Size(302, 85)
        Me.Panel6.TabIndex = 233
        '
        'PictureBox4
        '
        Me.PictureBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(87, 3)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(203, 44)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox4.TabIndex = 16
        Me.PictureBox4.TabStop = False
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.Transparent
        Me.Label43.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.Gray
        Me.Label43.Location = New System.Drawing.Point(87, 47)
        Me.Label43.Name = "Label43"
        Me.Label43.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label43.Size = New System.Drawing.Size(203, 38)
        Me.Label43.TabIndex = 15
        Me.Label43.Text = "Traditional Solomon Table Views for Printing"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox11.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox11.Image = Global.Solomon.Profile.My.Resources.Resources.Logo
        Me.PictureBox11.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(81, 82)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox11.TabIndex = 9
        Me.PictureBox11.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox11, "Click to go www.SolomonOnline.com")
        '
        'tpReports
        '
        Me.tpReports.BackColor = System.Drawing.Color.Transparent
        Me.tpReports.Controls.Add(Me.lvReports)
        Me.tpReports.Controls.Add(Me.Label49)
        Me.tpReports.Controls.Add(Me.GroupBox3)
        Me.tpReports.Controls.Add(Me.Label44)
        Me.tpReports.Controls.Add(Me.Panel7)
        Me.tpReports.Location = New System.Drawing.Point(4, 40)
        Me.tpReports.Name = "tpReports"
        Me.tpReports.Padding = New System.Windows.Forms.Padding(3)
        Me.tpReports.Size = New System.Drawing.Size(308, 606)
        Me.tpReports.TabIndex = 4
        Me.tpReports.Text = "Reports"
        Me.tpReports.UseVisualStyleBackColor = True
        '
        'lvReports
        '
        Me.lvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lvReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvReports.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvReports.ForeColor = System.Drawing.Color.MediumBlue
        Me.lvReports.FullRowSelect = True
        Me.lvReports.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvReports.LabelWrap = False
        Me.lvReports.LargeImageList = Me.ImageList2
        Me.lvReports.Location = New System.Drawing.Point(3, 100)
        Me.lvReports.Margin = New System.Windows.Forms.Padding(8)
        Me.lvReports.MultiSelect = False
        Me.lvReports.Name = "lvReports"
        Me.lvReports.Scrollable = False
        Me.lvReports.Size = New System.Drawing.Size(302, 298)
        Me.lvReports.TabIndex = 233
        Me.lvReports.TabStop = False
        Me.ToolTip1.SetToolTip(Me.lvReports, "Select a report to create")
        Me.lvReports.UseCompatibleStateImageBehavior = False
        Me.lvReports.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Form"
        Me.ColumnHeader3.Width = 240
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Status"
        '
        'Label49
        '
        Me.Label49.BackColor = System.Drawing.Color.Transparent
        Me.Label49.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label49.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.Gray
        Me.Label49.Location = New System.Drawing.Point(3, 398)
        Me.Label49.Name = "Label49"
        Me.Label49.Padding = New System.Windows.Forms.Padding(6)
        Me.Label49.Size = New System.Drawing.Size(302, 12)
        Me.Label49.TabIndex = 237
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmbRptMonth)
        Me.GroupBox3.Controls.Add(Me.Label29)
        Me.GroupBox3.Controls.Add(Me.Label37)
        Me.GroupBox3.Controls.Add(Me.cmbRptMeth)
        Me.GroupBox3.Controls.Add(Me.cmbRptCurrencyCode)
        Me.GroupBox3.Controls.Add(Me.Label35)
        Me.GroupBox3.Controls.Add(Me.cmbRptUOM)
        Me.GroupBox3.Controls.Add(Me.Label39)
        Me.GroupBox3.Controls.Add(Me.chkRptTarget)
        Me.GroupBox3.Controls.Add(Me.chkRptAVG)
        Me.GroupBox3.Controls.Add(Me.chkRptYTD)
        Me.GroupBox3.Controls.Add(Me.Panel15)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.GroupBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox3.Location = New System.Drawing.Point(3, 410)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox3.Size = New System.Drawing.Size(302, 193)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Reporting Tools"
        '
        'cmbRptMonth
        '
        Me.cmbRptMonth.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbRptMonth.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbRptMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRptMonth.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbRptMonth.FormatString = "y"
        Me.cmbRptMonth.FormattingEnabled = True
        Me.cmbRptMonth.Location = New System.Drawing.Point(107, 29)
        Me.cmbRptMonth.Name = "cmbRptMonth"
        Me.cmbRptMonth.Size = New System.Drawing.Size(190, 21)
        Me.cmbRptMonth.TabIndex = 15
        '
        'Label29
        '
        Me.Label29.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label29.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label29.Location = New System.Drawing.Point(0, 32)
        Me.Label29.Name = "Label29"
        Me.Label29.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Label29.Size = New System.Drawing.Size(302, 26)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Month of Report"
        '
        'Label37
        '
        Me.Label37.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label37.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label37.Location = New System.Drawing.Point(175, 106)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(122, 13)
        Me.Label37.TabIndex = 43
        Me.Label37.Text = "Methodology   Currency"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbRptMeth
        '
        Me.cmbRptMeth.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbRptMeth.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbRptMeth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRptMeth.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbRptMeth.Location = New System.Drawing.Point(180, 122)
        Me.cmbRptMeth.Name = "cmbRptMeth"
        Me.cmbRptMeth.Size = New System.Drawing.Size(55, 21)
        Me.cmbRptMeth.TabIndex = 37
        Me.cmbRptMeth.TabStop = False
        '
        'cmbRptCurrencyCode
        '
        Me.cmbRptCurrencyCode.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbRptCurrencyCode.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbRptCurrencyCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRptCurrencyCode.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbRptCurrencyCode.Items.AddRange(New Object() {"USD"})
        Me.cmbRptCurrencyCode.Location = New System.Drawing.Point(241, 122)
        Me.cmbRptCurrencyCode.Name = "cmbRptCurrencyCode"
        Me.cmbRptCurrencyCode.Size = New System.Drawing.Size(55, 21)
        Me.cmbRptCurrencyCode.TabIndex = 27
        '
        'Label35
        '
        Me.Label35.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label35.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label35.Location = New System.Drawing.Point(193, 63)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(88, 13)
        Me.Label35.TabIndex = 28
        Me.Label35.Text = "Units of Measure"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbRptUOM
        '
        Me.cmbRptUOM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbRptUOM.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbRptUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRptUOM.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbRptUOM.Items.AddRange(New Object() {"US Units", "Metric"})
        Me.cmbRptUOM.Location = New System.Drawing.Point(180, 79)
        Me.cmbRptUOM.Name = "cmbRptUOM"
        Me.cmbRptUOM.Size = New System.Drawing.Size(116, 21)
        Me.cmbRptUOM.TabIndex = 26
        '
        'Label39
        '
        Me.Label39.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label39.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label39.Location = New System.Drawing.Point(0, 58)
        Me.Label39.Name = "Label39"
        Me.Label39.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Label39.Size = New System.Drawing.Size(302, 23)
        Me.Label39.TabIndex = 47
        Me.Label39.Text = "When applicable:"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkRptTarget
        '
        Me.chkRptTarget.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkRptTarget.Checked = True
        Me.chkRptTarget.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRptTarget.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkRptTarget.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.chkRptTarget.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkRptTarget.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkRptTarget.Location = New System.Drawing.Point(0, 81)
        Me.chkRptTarget.Name = "chkRptTarget"
        Me.chkRptTarget.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.chkRptTarget.Size = New System.Drawing.Size(302, 23)
        Me.chkRptTarget.TabIndex = 30
        Me.chkRptTarget.Text = "Show Targets"
        Me.chkRptTarget.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        'chkRptAVG
        '
        Me.chkRptAVG.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkRptAVG.Checked = True
        Me.chkRptAVG.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRptAVG.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkRptAVG.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.chkRptAVG.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkRptAVG.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkRptAVG.Location = New System.Drawing.Point(0, 104)
        Me.chkRptAVG.Name = "chkRptAVG"
        Me.chkRptAVG.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.chkRptAVG.Size = New System.Drawing.Size(302, 23)
        Me.chkRptAVG.TabIndex = 31
        Me.chkRptAVG.Text = "Show Averages"
        Me.chkRptAVG.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        'chkRptYTD
        '
        Me.chkRptYTD.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkRptYTD.Checked = True
        Me.chkRptYTD.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRptYTD.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkRptYTD.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.chkRptYTD.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkRptYTD.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkRptYTD.Location = New System.Drawing.Point(0, 127)
        Me.chkRptYTD.Name = "chkRptYTD"
        Me.chkRptYTD.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.chkRptYTD.Size = New System.Drawing.Size(302, 30)
        Me.chkRptYTD.TabIndex = 32
        Me.chkRptYTD.Text = "Show YTD Averages"
        Me.chkRptYTD.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Gray
        Me.Panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel15.Controls.Add(Me.cbStudyPriceRpt)
        Me.Panel15.Controls.Add(Me.btnRptCreate)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel15.Location = New System.Drawing.Point(0, 157)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Padding = New System.Windows.Forms.Padding(6, 0, 1, 0)
        Me.Panel15.Size = New System.Drawing.Size(302, 36)
        Me.Panel15.TabIndex = 44
        '
        'cbStudyPriceRpt
        '
        Me.cbStudyPriceRpt.AutoSize = True
        Me.cbStudyPriceRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cbStudyPriceRpt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cbStudyPriceRpt.ForeColor = System.Drawing.Color.White
        Me.cbStudyPriceRpt.Location = New System.Drawing.Point(6, 0)
        Me.cbStudyPriceRpt.Name = "cbStudyPriceRpt"
        Me.cbStudyPriceRpt.Size = New System.Drawing.Size(218, 34)
        Me.cbStudyPriceRpt.TabIndex = 41
        Me.cbStudyPriceRpt.Text = "Use Previous Study Pricing"
        '
        'btnRptCreate
        '
        Me.btnRptCreate.BackColor = System.Drawing.Color.Gray
        Me.btnRptCreate.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnRptCreate.FlatAppearance.BorderSize = 0
        Me.btnRptCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRptCreate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRptCreate.ForeColor = System.Drawing.Color.White
        Me.btnRptCreate.Image = CType(resources.GetObject("btnRptCreate.Image"), System.Drawing.Image)
        Me.btnRptCreate.Location = New System.Drawing.Point(224, 0)
        Me.btnRptCreate.Name = "btnRptCreate"
        Me.btnRptCreate.Size = New System.Drawing.Size(75, 34)
        Me.btnRptCreate.TabIndex = 33
        Me.btnRptCreate.TabStop = False
        Me.btnRptCreate.Text = "Create"
        Me.btnRptCreate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRptCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnRptCreate, "Create selected report")
        Me.btnRptCreate.UseVisualStyleBackColor = False
        '
        'Label44
        '
        Me.Label44.BackColor = System.Drawing.Color.Transparent
        Me.Label44.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label44.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.Gray
        Me.Label44.Location = New System.Drawing.Point(3, 88)
        Me.Label44.Name = "Label44"
        Me.Label44.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label44.Size = New System.Drawing.Size(302, 12)
        Me.Label44.TabIndex = 236
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.PictureBox5)
        Me.Panel7.Controls.Add(Me.Label45)
        Me.Panel7.Controls.Add(Me.PictureBox12)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(3, 3)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Padding = New System.Windows.Forms.Padding(6, 3, 12, 0)
        Me.Panel7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel7.Size = New System.Drawing.Size(302, 85)
        Me.Panel7.TabIndex = 235
        '
        'PictureBox5
        '
        Me.PictureBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(87, 3)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(203, 44)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox5.TabIndex = 16
        Me.PictureBox5.TabStop = False
        '
        'Label45
        '
        Me.Label45.BackColor = System.Drawing.Color.Transparent
        Me.Label45.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label45.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.Gray
        Me.Label45.Location = New System.Drawing.Point(87, 47)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(203, 38)
        Me.Label45.TabIndex = 15
        Me.Label45.Text = "Reports of Results at the Refinery and Process Unit Levels"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox12.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox12.Image = Global.Solomon.Profile.My.Resources.Resources.Logo
        Me.PictureBox12.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(81, 82)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox12.TabIndex = 9
        Me.PictureBox12.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox12, "Click to go www.SolomonOnline.com")
        '
        'tpCharts
        '
        Me.tpCharts.BackColor = System.Drawing.Color.Transparent
        Me.tpCharts.Controls.Add(Me.treeCharts)
        Me.tpCharts.Controls.Add(Me.Label77)
        Me.tpCharts.Controls.Add(Me.Label46)
        Me.tpCharts.Controls.Add(Me.Panel8)
        Me.tpCharts.Controls.Add(Me.Label48)
        Me.tpCharts.Controls.Add(Me.GroupBox2)
        Me.tpCharts.Location = New System.Drawing.Point(4, 40)
        Me.tpCharts.Name = "tpCharts"
        Me.tpCharts.Padding = New System.Windows.Forms.Padding(3)
        Me.tpCharts.Size = New System.Drawing.Size(308, 606)
        Me.tpCharts.TabIndex = 5
        Me.tpCharts.Text = "Charts"
        Me.tpCharts.UseVisualStyleBackColor = True
        '
        'treeCharts
        '
        Me.treeCharts.BackColor = System.Drawing.Color.White
        Me.treeCharts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeCharts.ForeColor = System.Drawing.Color.MediumBlue
        Me.treeCharts.HotTracking = True
        Me.treeCharts.Location = New System.Drawing.Point(3, 100)
        Me.treeCharts.Name = "treeCharts"
        Me.treeCharts.Size = New System.Drawing.Size(302, 257)
        Me.treeCharts.TabIndex = 14
        '
        'Label77
        '
        Me.Label77.BackColor = System.Drawing.SystemColors.Control
        Me.Label77.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label77.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label77.Location = New System.Drawing.Point(3, 344)
        Me.Label77.Name = "Label77"
        Me.Label77.Padding = New System.Windows.Forms.Padding(0, 0, 8, 0)
        Me.Label77.Size = New System.Drawing.Size(302, 1)
        Me.Label77.TabIndex = 232
        Me.Label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label46
        '
        Me.Label46.BackColor = System.Drawing.Color.Transparent
        Me.Label46.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label46.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.Color.Gray
        Me.Label46.Location = New System.Drawing.Point(3, 88)
        Me.Label46.Name = "Label46"
        Me.Label46.Padding = New System.Windows.Forms.Padding(12, 0, 12, 0)
        Me.Label46.Size = New System.Drawing.Size(302, 12)
        Me.Label46.TabIndex = 238
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.PictureBox6)
        Me.Panel8.Controls.Add(Me.Label47)
        Me.Panel8.Controls.Add(Me.PictureBox13)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(3, 3)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Padding = New System.Windows.Forms.Padding(6, 3, 12, 0)
        Me.Panel8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel8.Size = New System.Drawing.Size(302, 85)
        Me.Panel8.TabIndex = 237
        '
        'PictureBox6
        '
        Me.PictureBox6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(87, 3)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(203, 44)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox6.TabIndex = 16
        Me.PictureBox6.TabStop = False
        '
        'Label47
        '
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        Me.Label47.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label47.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.Gray
        Me.Label47.Location = New System.Drawing.Point(87, 47)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(203, 38)
        Me.Label47.TabIndex = 15
        Me.Label47.Text = "Charts of Results at the Refinery and Process Unit Levels"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox13
        '
        Me.PictureBox13.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox13.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox13.Image = Global.Solomon.Profile.My.Resources.Resources.Logo
        Me.PictureBox13.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(81, 82)
        Me.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox13.TabIndex = 9
        Me.PictureBox13.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox13, "Click to go www.SolomonOnline.com")
        '
        'Label48
        '
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label48.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.Gray
        Me.Label48.Location = New System.Drawing.Point(3, 345)
        Me.Label48.Name = "Label48"
        Me.Label48.Padding = New System.Windows.Forms.Padding(6)
        Me.Label48.Size = New System.Drawing.Size(302, 12)
        Me.Label48.TabIndex = 239
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkShowChartDataTable)
        Me.GroupBox2.Controls.Add(Me.cboChartYtdColor)
        Me.GroupBox2.Controls.Add(Me.cboChartAvgColor)
        Me.GroupBox2.Controls.Add(Me.cboChartTargetColor)
        Me.GroupBox2.Controls.Add(Me.cboChartColor)
        Me.GroupBox2.Controls.Add(Me.cmbChtStart)
        Me.GroupBox2.Controls.Add(Me.cmbChtEnd)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.cmbChtMeth)
        Me.GroupBox2.Controls.Add(Me.Label42)
        Me.GroupBox2.Controls.Add(Me.cmbChtCurrencyCode)
        Me.GroupBox2.Controls.Add(Me.cmbChtUOM)
        Me.GroupBox2.Controls.Add(Me.Label41)
        Me.GroupBox2.Controls.Add(Me.chkChtTarget)
        Me.GroupBox2.Controls.Add(Me.chkChtAVG)
        Me.GroupBox2.Controls.Add(Me.chkChtYTD)
        Me.GroupBox2.Controls.Add(Me.Panel17)
        Me.GroupBox2.Controls.Add(Me.cboChartType)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.GroupBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox2.Location = New System.Drawing.Point(3, 357)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox2.Size = New System.Drawing.Size(302, 246)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Charting Tools"
        Me.GroupBox2.Controls.Add(Me.btnRadarPlot)
        '
        'cboChartYtdColor
        '
        Me.cboChartYtdColor.FormattingEnabled = True
        Me.cboChartYtdColor.Items.AddRange(New Object() {"Blue", "Yellow", "Green", "LightBlue", "LightGreen", "Black", "Gray", "Red"})
        Me.cboChartYtdColor.Location = New System.Drawing.Point(135, 186)
        Me.cboChartYtdColor.Name = "cboChartYtdColor"
        Me.cboChartYtdColor.Size = New System.Drawing.Size(39, 21)
        Me.cboChartYtdColor.TabIndex = 57
        '
        'cboChartAvgColor
        '
        Me.cboChartAvgColor.FormattingEnabled = True
        Me.cboChartAvgColor.Items.AddRange(New Object() {"Blue", "Yellow", "Green", "LightBlue", "LightGreen", "Black", "Gray", "Red"})
        Me.cboChartAvgColor.Location = New System.Drawing.Point(135, 160)
        Me.cboChartAvgColor.Name = "cboChartAvgColor"
        Me.cboChartAvgColor.Size = New System.Drawing.Size(39, 21)
        Me.cboChartAvgColor.TabIndex = 56
        '
        'cboChartTargetColor
        '
        Me.cboChartTargetColor.FormattingEnabled = True
        Me.cboChartTargetColor.Items.AddRange(New Object() {"Blue", "Yellow", "Green", "LightBlue", "LightGreen", "Black", "Gray", "Red"})
        Me.cboChartTargetColor.Location = New System.Drawing.Point(135, 133)
        Me.cboChartTargetColor.Name = "cboChartTargetColor"
        Me.cboChartTargetColor.Size = New System.Drawing.Size(39, 21)
        Me.cboChartTargetColor.TabIndex = 55
        '
        'cboChartColor
        '
        Me.cboChartColor.FormattingEnabled = True
        Me.cboChartColor.Items.AddRange(New Object() {"Blue", "Yellow", "Green", "LightBlue", "LightGreen", "Black", "Gray", "Red"})
        Me.cboChartColor.Location = New System.Drawing.Point(122, 20)
        Me.cboChartColor.Name = "cboChartColor"
        Me.cboChartColor.Size = New System.Drawing.Size(82, 21)
        Me.cboChartColor.TabIndex = 54
        Me.cboChartColor.Text = "Blue"
        '
        'cmbChtStart
        '
        Me.cmbChtStart.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbChtStart.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbChtStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbChtStart.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbChtStart.FormatString = "y"
        Me.cmbChtStart.FormattingEnabled = True
        Me.cmbChtStart.Location = New System.Drawing.Point(106, 51)
        Me.cmbChtStart.Name = "cmbChtStart"
        Me.cmbChtStart.Size = New System.Drawing.Size(190, 21)
        Me.cmbChtStart.TabIndex = 14
        '
        'cmbChtEnd
        '
        Me.cmbChtEnd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbChtEnd.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbChtEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbChtEnd.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbChtEnd.FormatString = "y"
        Me.cmbChtEnd.FormattingEnabled = True
        Me.cmbChtEnd.Location = New System.Drawing.Point(106, 82)
        Me.cmbChtEnd.Name = "cmbChtEnd"
        Me.cmbChtEnd.Size = New System.Drawing.Size(190, 21)
        Me.cmbChtEnd.TabIndex = 15
        '
        'Label23
        '
        Me.Label23.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label23.Location = New System.Drawing.Point(0, 55)
        Me.Label23.Name = "Label23"
        Me.Label23.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Label23.Size = New System.Drawing.Size(302, 30)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Starting Month"
        '
        'Label27
        '
        Me.Label27.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label27.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label27.Location = New System.Drawing.Point(0, 85)
        Me.Label27.Name = "Label27"
        Me.Label27.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Label27.Size = New System.Drawing.Size(302, 26)
        Me.Label27.TabIndex = 2
        Me.Label27.Text = "Ending Month"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(193, 116)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 13)
        Me.Label13.TabIndex = 47
        Me.Label13.Text = "Units of Measure"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbChtMeth
        '
        Me.cmbChtMeth.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbChtMeth.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbChtMeth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbChtMeth.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbChtMeth.Location = New System.Drawing.Point(180, 175)
        Me.cmbChtMeth.Name = "cmbChtMeth"
        Me.cmbChtMeth.Size = New System.Drawing.Size(55, 21)
        Me.cmbChtMeth.TabIndex = 25
        '
        'Label42
        '
        Me.Label42.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label42.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label42.Location = New System.Drawing.Point(175, 159)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(122, 13)
        Me.Label42.TabIndex = 52
        Me.Label42.Text = "Methodology   Currency"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbChtCurrencyCode
        '
        Me.cmbChtCurrencyCode.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbChtCurrencyCode.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbChtCurrencyCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbChtCurrencyCode.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbChtCurrencyCode.Items.AddRange(New Object() {"USD"})
        Me.cmbChtCurrencyCode.Location = New System.Drawing.Point(241, 175)
        Me.cmbChtCurrencyCode.Name = "cmbChtCurrencyCode"
        Me.cmbChtCurrencyCode.Size = New System.Drawing.Size(55, 21)
        Me.cmbChtCurrencyCode.TabIndex = 17
        '
        'cmbChtUOM
        '
        Me.cmbChtUOM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbChtUOM.BackColor = System.Drawing.Color.LemonChiffon
        Me.cmbChtUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbChtUOM.ForeColor = System.Drawing.Color.MediumBlue
        Me.cmbChtUOM.Items.AddRange(New Object() {"US Units", "Metric"})
        Me.cmbChtUOM.Location = New System.Drawing.Point(180, 132)
        Me.cmbChtUOM.Name = "cmbChtUOM"
        Me.cmbChtUOM.Size = New System.Drawing.Size(116, 21)
        Me.cmbChtUOM.TabIndex = 16
        '
        'Label41
        '
        Me.Label41.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label41.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label41.Location = New System.Drawing.Point(0, 111)
        Me.Label41.Name = "Label41"
        Me.Label41.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.Label41.Size = New System.Drawing.Size(302, 23)
        Me.Label41.TabIndex = 51
        Me.Label41.Text = "When applicable:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkChtTarget
        '
        Me.chkChtTarget.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkChtTarget.Checked = True
        Me.chkChtTarget.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkChtTarget.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkChtTarget.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.chkChtTarget.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkChtTarget.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkChtTarget.Location = New System.Drawing.Point(0, 134)
        Me.chkChtTarget.Name = "chkChtTarget"
        Me.chkChtTarget.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.chkChtTarget.Size = New System.Drawing.Size(302, 23)
        Me.chkChtTarget.TabIndex = 20
        Me.chkChtTarget.Text = "Show Targets"
        Me.chkChtTarget.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        'chkChtAVG
        '
        Me.chkChtAVG.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkChtAVG.Checked = True
        Me.chkChtAVG.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkChtAVG.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkChtAVG.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.chkChtAVG.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkChtAVG.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkChtAVG.Location = New System.Drawing.Point(0, 157)
        Me.chkChtAVG.Name = "chkChtAVG"
        Me.chkChtAVG.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.chkChtAVG.Size = New System.Drawing.Size(302, 23)
        Me.chkChtAVG.TabIndex = 21
        Me.chkChtAVG.Text = "Show Averages"
        Me.chkChtAVG.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        'chkChtYTD
        '
        Me.chkChtYTD.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkChtYTD.Checked = True
        Me.chkChtYTD.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkChtYTD.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkChtYTD.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.chkChtYTD.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkChtYTD.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkChtYTD.Location = New System.Drawing.Point(0, 180)
        Me.chkChtYTD.Name = "chkChtYTD"
        Me.chkChtYTD.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.chkChtYTD.Size = New System.Drawing.Size(302, 30)
        Me.chkChtYTD.TabIndex = 22
        Me.chkChtYTD.Text = "Show YTD Averages"
        Me.chkChtYTD.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.Gray
        Me.Panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel17.Controls.Add(Me.cbStudyPriceCht)
        Me.Panel17.Controls.Add(Me.btnChtCreate)
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel17.Location = New System.Drawing.Point(0, 210)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Padding = New System.Windows.Forms.Padding(0, 0, 1, 0)
        Me.Panel17.Size = New System.Drawing.Size(302, 36)
        Me.Panel17.TabIndex = 48
        '
        'cbStudyPriceCht
        '
        Me.cbStudyPriceCht.AutoSize = True
        Me.cbStudyPriceCht.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cbStudyPriceCht.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.cbStudyPriceCht.ForeColor = System.Drawing.Color.White
        Me.cbStudyPriceCht.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cbStudyPriceCht.Location = New System.Drawing.Point(0, 0)
        Me.cbStudyPriceCht.Name = "cbStudyPriceCht"
        Me.cbStudyPriceCht.Padding = New System.Windows.Forms.Padding(6, 0, 0, 0)
        Me.cbStudyPriceCht.Size = New System.Drawing.Size(224, 34)
        Me.cbStudyPriceCht.TabIndex = 42
        Me.cbStudyPriceCht.Text = "Use Previous Study Pricing"
        '
        'btnChtCreate
        '
        Me.btnChtCreate.BackColor = System.Drawing.Color.Gray
        Me.btnChtCreate.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnChtCreate.FlatAppearance.BorderSize = 0
        Me.btnChtCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChtCreate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChtCreate.ForeColor = System.Drawing.Color.White
        Me.btnChtCreate.Image = CType(resources.GetObject("btnChtCreate.Image"), System.Drawing.Image)
        Me.btnChtCreate.Location = New System.Drawing.Point(224, 0)
        Me.btnChtCreate.Name = "btnChtCreate"
        Me.btnChtCreate.Size = New System.Drawing.Size(75, 34)
        Me.btnChtCreate.TabIndex = 23
        Me.btnChtCreate.TabStop = False
        Me.btnChtCreate.Text = "Create"
        Me.btnChtCreate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnChtCreate.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnChtCreate, "Create selected chart")
        Me.btnChtCreate.UseVisualStyleBackColor = False
        '
        'cboChartType
        '
        Me.cboChartType.FormattingEnabled = True
        Me.cboChartType.Items.AddRange(New Object() {"Column", "StackedColumn"})
        Me.cboChartType.Location = New System.Drawing.Point(5, 20)
        Me.cboChartType.Name = "cboChartType"
        Me.cboChartType.Size = New System.Drawing.Size(110, 21)
        Me.cboChartType.TabIndex = 53
        Me.cboChartType.Text = "Column"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Panel1.Controls.Add(Me.btnCP)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(0, 0, 1, 1)
        Me.Panel1.Size = New System.Drawing.Size(316, 24)
        Me.Panel1.TabIndex = 180
        '
        'btnCP
        '
        Me.btnCP.BackColor = System.Drawing.Color.Gray
        Me.btnCP.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnCP.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnCP.FlatAppearance.BorderSize = 0
        Me.btnCP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCP.ForeColor = System.Drawing.Color.White
        Me.btnCP.Image = CType(resources.GetObject("btnCP.Image"), System.Drawing.Image)
        Me.btnCP.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCP.Location = New System.Drawing.Point(0, 0)
        Me.btnCP.Name = "btnCP"
        Me.btnCP.Size = New System.Drawing.Size(315, 23)
        Me.btnCP.TabIndex = 14
        Me.btnCP.Text = "Control Panel"
        Me.btnCP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.btnCP, "Click to minimize/maximize")
        Me.btnCP.UseVisualStyleBackColor = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "na.png")
        Me.ImageList1.Images.SetKeyName(1, "information-1.png")
        '
        'pnlWorkSpace
        '
        Me.pnlWorkSpace.BackColor = System.Drawing.SystemColors.Control
        Me.pnlWorkSpace.Controls.Add(Me.pnlBlank)
        Me.pnlWorkSpace.Controls.Add(Me.Panel3)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Import1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_CrudeCharge1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Personnel1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_OperatingExpenses1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_UserDefined1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Configuration1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Energy1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Inventory1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_MaterialBalance1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_RefineryInfo1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Process1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Admin1)
        Me.pnlWorkSpace.Controls.Add(Me.SA_Browser1)
        Me.pnlWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlWorkSpace.Location = New System.Drawing.Point(316, 0)
        Me.pnlWorkSpace.Name = "pnlWorkSpace"
        Me.pnlWorkSpace.Size = New System.Drawing.Size(700, 674)
        Me.pnlWorkSpace.TabIndex = 6
        '
        'pnlBlank
        '
        Me.pnlBlank.Controls.Add(Me.gbAbout)
        Me.pnlBlank.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBlank.Location = New System.Drawing.Point(0, 24)
        Me.pnlBlank.Name = "pnlBlank"
        Me.pnlBlank.Padding = New System.Windows.Forms.Padding(100)
        Me.pnlBlank.Size = New System.Drawing.Size(700, 650)
        Me.pnlBlank.TabIndex = 954
        '
        'gbAbout
        '
        Me.gbAbout.Controls.Add(Me.Label1)
        Me.gbAbout.Controls.Add(Me.lblCopyright)
        Me.gbAbout.Controls.Add(Me.Label26)
        Me.gbAbout.Controls.Add(Me.lblVersion)
        Me.gbAbout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAbout.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAbout.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.gbAbout.Location = New System.Drawing.Point(100, 100)
        Me.gbAbout.Name = "gbAbout"
        Me.gbAbout.Padding = New System.Windows.Forms.Padding(12)
        Me.gbAbout.Size = New System.Drawing.Size(500, 450)
        Me.gbAbout.TabIndex = 0
        Me.gbAbout.TabStop = False
        Me.gbAbout.Text = "About Solomon Profile� II"
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(12, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(476, 319)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'lblCopyright
        '
        Me.lblCopyright.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblCopyright.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCopyright.Location = New System.Drawing.Point(12, 345)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.Size = New System.Drawing.Size(476, 17)
        Me.lblCopyright.TabIndex = 36
        Me.lblCopyright.Text = "� 2014 HSB Solomon Associates LLC"
        Me.lblCopyright.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label26
        '
        Me.Label26.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label26.Location = New System.Drawing.Point(12, 362)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(476, 59)
        Me.Label26.TabIndex = 35
        Me.Label26.Text = resources.GetString("Label26.Text")
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVersion
        '
        Me.lblVersion.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblVersion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblVersion.Location = New System.Drawing.Point(12, 421)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(476, 17)
        Me.lblVersion.TabIndex = 34
        Me.lblVersion.Text = "Version"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Panel3.Controls.Add(Me.btnWorkspace)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Padding = New System.Windows.Forms.Padding(0, 0, 1, 1)
        Me.Panel3.Size = New System.Drawing.Size(700, 24)
        Me.Panel3.TabIndex = 956
        '
        'btnWorkspace
        '
        Me.btnWorkspace.BackColor = System.Drawing.Color.Gray
        Me.btnWorkspace.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnWorkspace.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnWorkspace.FlatAppearance.BorderSize = 0
        Me.btnWorkspace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWorkspace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWorkspace.ForeColor = System.Drawing.Color.White
        Me.btnWorkspace.Image = Global.Solomon.Profile.My.Resources.Resources.help
        Me.btnWorkspace.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnWorkspace.Location = New System.Drawing.Point(0, 0)
        Me.btnWorkspace.Name = "btnWorkspace"
        Me.btnWorkspace.Size = New System.Drawing.Size(699, 23)
        Me.btnWorkspace.TabIndex = 14
        Me.btnWorkspace.Text = "Workspace"
        Me.btnWorkspace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.btnWorkspace, "Click for help")
        Me.btnWorkspace.UseVisualStyleBackColor = False
        '
        'SA_Import1
        '
        Me.SA_Import1.BackColor = System.Drawing.SystemColors.Control
        Me.SA_Import1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Import1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_Import1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Import1.Name = "SA_Import1"
        Me.SA_Import1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Import1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Import1.TabIndex = 953
        Me.SA_Import1.Visible = False
        '
        'SA_CrudeCharge1
        '
        Me.SA_CrudeCharge1.BackColor = System.Drawing.Color.White
        Me.SA_CrudeCharge1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_CrudeCharge1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_CrudeCharge1.ForeColor = System.Drawing.Color.MediumBlue
        Me.SA_CrudeCharge1.Location = New System.Drawing.Point(0, 0)
        Me.SA_CrudeCharge1.Name = "SA_CrudeCharge1"
        Me.SA_CrudeCharge1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_CrudeCharge1.Size = New System.Drawing.Size(700, 674)
        Me.SA_CrudeCharge1.TabIndex = 70
        Me.SA_CrudeCharge1.Visible = False
        '
        'SA_Personnel1
        '
        Me.SA_Personnel1.BackColor = System.Drawing.Color.White
        Me.SA_Personnel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Personnel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_Personnel1.ForeColor = System.Drawing.Color.MediumBlue
        Me.SA_Personnel1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Personnel1.Name = "SA_Personnel1"
        Me.SA_Personnel1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Personnel1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Personnel1.TabIndex = 63
        Me.SA_Personnel1.Visible = False
        '
        'SA_OperatingExpenses1
        '
        Me.SA_OperatingExpenses1.BackColor = System.Drawing.Color.White
        Me.SA_OperatingExpenses1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_OperatingExpenses1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_OperatingExpenses1.ForeColor = System.Drawing.Color.MediumBlue
        Me.SA_OperatingExpenses1.Location = New System.Drawing.Point(0, 0)
        Me.SA_OperatingExpenses1.Name = "SA_OperatingExpenses1"
        Me.SA_OperatingExpenses1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_OperatingExpenses1.Size = New System.Drawing.Size(700, 674)
        Me.SA_OperatingExpenses1.TabIndex = 61
        Me.SA_OperatingExpenses1.Visible = False
        '
        'SA_UserDefined1
        '
        Me.SA_UserDefined1.BackColor = System.Drawing.Color.White
        Me.SA_UserDefined1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_UserDefined1.Location = New System.Drawing.Point(0, 0)
        Me.SA_UserDefined1.Name = "SA_UserDefined1"
        Me.SA_UserDefined1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_UserDefined1.Size = New System.Drawing.Size(700, 674)
        Me.SA_UserDefined1.TabIndex = 71
        Me.SA_UserDefined1.Visible = False
        '
        'SA_Configuration1
        '
        Me.SA_Configuration1.BackColor = System.Drawing.Color.White
        Me.SA_Configuration1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Configuration1.ForeColor = System.Drawing.Color.MediumBlue
        Me.SA_Configuration1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Configuration1.Name = "SA_Configuration1"
        Me.SA_Configuration1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Configuration1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Configuration1.TabIndex = 64
        Me.SA_Configuration1.Visible = False
        '
        'SA_Energy1
        '
        Me.SA_Energy1.BackColor = System.Drawing.Color.White
        Me.SA_Energy1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Energy1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_Energy1.ForeColor = System.Drawing.Color.MediumBlue
        Me.SA_Energy1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Energy1.Name = "SA_Energy1"
        Me.SA_Energy1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Energy1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Energy1.TabIndex = 65
        Me.SA_Energy1.Visible = False
        '
        'SA_Inventory1
        '
        Me.SA_Inventory1.BackColor = System.Drawing.Color.White
        Me.SA_Inventory1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Inventory1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_Inventory1.ForeColor = System.Drawing.Color.MediumBlue
        Me.SA_Inventory1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Inventory1.Name = "SA_Inventory1"
        Me.SA_Inventory1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Inventory1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Inventory1.TabIndex = 67
        Me.SA_Inventory1.Visible = False
        '
        'SA_MaterialBalance1
        '
        Me.SA_MaterialBalance1.BackColor = System.Drawing.Color.White
        Me.SA_MaterialBalance1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_MaterialBalance1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_MaterialBalance1.ForeColor = System.Drawing.Color.MediumBlue
        Me.SA_MaterialBalance1.Location = New System.Drawing.Point(0, 0)
        Me.SA_MaterialBalance1.Name = "SA_MaterialBalance1"
        Me.SA_MaterialBalance1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_MaterialBalance1.Size = New System.Drawing.Size(700, 674)
        Me.SA_MaterialBalance1.TabIndex = 62
        Me.SA_MaterialBalance1.Visible = False
        '
        'SA_RefineryInfo1
        '
        Me.SA_RefineryInfo1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_RefineryInfo1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_RefineryInfo1.Location = New System.Drawing.Point(0, 0)
        Me.SA_RefineryInfo1.Name = "SA_RefineryInfo1"
        Me.SA_RefineryInfo1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_RefineryInfo1.Size = New System.Drawing.Size(700, 674)
        Me.SA_RefineryInfo1.TabIndex = 73
        Me.SA_RefineryInfo1.Visible = False
        '
        'SA_Process1
        '
        Me.SA_Process1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Process1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Process1.Name = "SA_Process1"
        Me.SA_Process1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Process1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Process1.TabIndex = 74
        Me.SA_Process1.Visible = False
        '
        'SA_Admin1
        '
        Me.SA_Admin1.BackColor = System.Drawing.SystemColors.Control
        Me.SA_Admin1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SA_Admin1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SA_Admin1.Location = New System.Drawing.Point(0, 0)
        Me.SA_Admin1.Name = "SA_Admin1"
        Me.SA_Admin1.Padding = New System.Windows.Forms.Padding(4)
        Me.SA_Admin1.Size = New System.Drawing.Size(700, 674)
        Me.SA_Admin1.TabIndex = 952
        '



        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Excel Files (*.xls)|*.xls"
        Me.OpenFileDialog1.Title = "Select Solomon Profile� II Bridge File"
        '
        'MonthWatcher
        '
        Me.MonthWatcher.EnableRaisingEvents = True
        Me.MonthWatcher.Filter = "*.xml"
        Me.MonthWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite
        Me.MonthWatcher.SynchronizingObject = Me
        '
        'ConfigWatcher
        '
        Me.ConfigWatcher.EnableRaisingEvents = True
        Me.ConfigWatcher.Filter = "*.xml"
        Me.ConfigWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite
        Me.ConfigWatcher.SynchronizingObject = Me
        '
        'ReferenceWatcher
        '
        Me.ReferenceWatcher.EnableRaisingEvents = True
        Me.ReferenceWatcher.Filter = "*.xml"
        Me.ReferenceWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite
        Me.ReferenceWatcher.SynchronizingObject = Me
        '
        'FileSystemWatcher1
        '
        Me.FileSystemWatcher1.EnableRaisingEvents = True
        Me.FileSystemWatcher1.NotifyFilter = System.IO.NotifyFilters.LastWrite
        Me.FileSystemWatcher1.SynchronizingObject = Me
        '
        'ToolTip1
        '
        Me.ToolTip1.ShowAlways = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(232, 159)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 28)
        Me.Button1.TabIndex = 33
        Me.Button1.TabStop = False
        Me.Button1.Text = "Create"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.Button1, "Create selected report")
        Me.Button1.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.ComboBox1)
        Me.GroupBox4.Controls.Add(Me.Button1)
        Me.GroupBox4.Controls.Add(Me.CheckBox1)
        Me.GroupBox4.Controls.Add(Me.CheckBox2)
        Me.GroupBox4.Controls.Add(Me.CheckBox3)
        Me.GroupBox4.Controls.Add(Me.ComboBox2)
        Me.GroupBox4.Controls.Add(Me.ComboBox3)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.ComboBox4)
        Me.GroupBox4.Controls.Add(Me.Label22)
        Me.GroupBox4.Controls.Add(Me.CheckBox4)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.GroupBox4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(302, 201)
        Me.GroupBox4.TabIndex = 14
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Reporting Tools"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(180, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 13)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "Solomon Methodology"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(114, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 42
        Me.Label7.Text = "Currency"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ComboBox1
        '
        Me.ComboBox1.BackColor = System.Drawing.Color.LemonChiffon
        Me.ComboBox1.ForeColor = System.Drawing.Color.MediumBlue
        Me.ComboBox1.Location = New System.Drawing.Point(175, 64)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 37
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.CheckBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CheckBox1.Location = New System.Drawing.Point(11, 147)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(123, 17)
        Me.CheckBox1.TabIndex = 32
        Me.CheckBox1.Text = "Show YTD Averages"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Checked = True
        Me.CheckBox2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox2.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.CheckBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CheckBox2.Location = New System.Drawing.Point(11, 124)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(101, 17)
        Me.CheckBox2.TabIndex = 31
        Me.CheckBox2.Text = "Show Averages"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Checked = True
        Me.CheckBox3.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox3.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.CheckBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CheckBox3.Location = New System.Drawing.Point(11, 99)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(92, 17)
        Me.CheckBox3.TabIndex = 30
        Me.CheckBox3.Text = "Show Targets"
        '
        'ComboBox2
        '
        Me.ComboBox2.BackColor = System.Drawing.Color.LemonChiffon
        Me.ComboBox2.ForeColor = System.Drawing.Color.MediumBlue
        Me.ComboBox2.Items.AddRange(New Object() {"USD"})
        Me.ComboBox2.Location = New System.Drawing.Point(106, 64)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(63, 21)
        Me.ComboBox2.TabIndex = 27
        '
        'ComboBox3
        '
        Me.ComboBox3.BackColor = System.Drawing.Color.LemonChiffon
        Me.ComboBox3.ForeColor = System.Drawing.Color.MediumBlue
        Me.ComboBox3.Items.AddRange(New Object() {"US Units", "Metric"})
        Me.ComboBox3.Location = New System.Drawing.Point(6, 64)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(94, 21)
        Me.ComboBox3.TabIndex = 26
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(8, 48)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 13)
        Me.Label8.TabIndex = 28
        Me.Label8.Text = "Units of Measure"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ComboBox4
        '
        Me.ComboBox4.BackColor = System.Drawing.Color.LemonChiffon
        Me.ComboBox4.ForeColor = System.Drawing.Color.MediumBlue
        Me.ComboBox4.FormatString = "y"
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(106, 18)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(190, 21)
        Me.ComboBox4.TabIndex = 15
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(8, 21)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(86, 13)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Month of Report"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.CheckBox4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CheckBox4.Location = New System.Drawing.Point(11, 170)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(140, 17)
        Me.CheckBox4.TabIndex = 41
        Me.CheckBox4.Text = "Use Study Study Pricing"
        '
        'btnRadarPlot
        '
        Me.btnRadarPlot.Location = New System.Drawing.Point(25, 42)
        Me.btnRadarPlot.Name = "btnRadarPlot"
        Me.btnRadarPlot.Size = New System.Drawing.Size(75, 23)
        Me.btnRadarPlot.TabIndex = 59
        Me.btnRadarPlot.Text = "Radar Plot"
        Me.btnRadarPlot.UseVisualStyleBackColor = True
        '
        'chkShowChartDataTable
        '
        Me.chkShowChartDataTable.AutoSize = True
        Me.chkShowChartDataTable.Checked = True
        Me.chkShowChartDataTable.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowChartDataTable.Location = New System.Drawing.Point(215, 21)
        Me.chkShowChartDataTable.Name = "chkShowChartDataTable"
        Me.chkShowChartDataTable.Size = New System.Drawing.Size(86, 17)
        Me.chkShowChartDataTable.TabIndex = 58
        Me.chkShowChartDataTable.Text = "Show Data"
        Me.chkShowChartDataTable.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1016, 674)
        Me.Controls.Add(Me.pnlWorkSpace)
        Me.Controls.Add(Me.pnlControlPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Main"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlControlPanel.ResumeLayout(False)
        Me.tcControlPanel.ResumeLayout(False)
        Me.tpSettings.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.pnlSettings.ResumeLayout(False)
        Me.pnlSettings.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpConfig.ResumeLayout(False)
        Me.Panel18.ResumeLayout(False)
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpDataEntry.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel12.ResumeLayout(False)
        CType(Me.dgvEvents, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel13.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpTables.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpReports.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpCharts.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.pnlWorkSpace.ResumeLayout(False)
        Me.pnlBlank.ResumeLayout(False)
        Me.gbAbout.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.MonthWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConfigWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReferenceWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Variables
    ' 20081001 RRH Path - Private ConfigPath As String  ' Changed to pathRef
    ' 20081001 RRH Path - Private RefPath As String     ' Not used in Main.vb

    'Datasets - Configuration and Data Entry

    'Variables
    Friend CurrentMonth As String
    Friend PeriodMonth, PeriodYear As Integer
    Friend PeriodStart As Date
    Friend RefineryID As String
    Private InfoNotSaved, HasTableBeenViewed As Boolean
    Private PrintControl, PrevCtl As Control
    Friend KCurrency, UOM, CurrencyCode, CurrencyT15 As String
    Private DisplayUnitCol As String
    Friend MonthPath As String
    Friend IsLoading As Boolean
    Friend MyParent As frmMain
    Friend ErrorHandler As New Exceptions

    'Datasets - Configuration and Data Entry
    Friend dsSettings, dsUnitList As New DataSet
    Friend dsRefineryInformation, dsProcess As New DataSet
    Friend dsOperatingExpenses, dsPersonnel, dsCrude As New DataSet
    Friend dsYield, dsEnergy, dsUserDefined, dsMappings As New DataSet
    Friend dsLoadedMonths, dsIsChecked As New DataSet
    Private BLACK As Color = Color.Black
    Private BTNFACE As Color = SystemColors.ButtonFace

    Private _appSettingsKeys() As String = ConfigurationManager.AppSettings.AllKeys()
    Private _delim As Char = Chr(14)
    Friend RadarChtVariables As RadarChartVariables

#Region " Main Form Procedures "

    Friend Sub WriteToActivityLog(applicationName As String,
            methodology As String,
            refID As String,
            callerIP As String,
            userid As String,
            computername As String,
            version As String,
            errorMessages As String,
            localDomainName As String,
            os_Version As String,
            officeVersion As String,
            service As String,
            method As String,
            enitityName As String,
            periodStart As Date?,
            periodEnd As Date?,
            status As String,
            err As Object,
            osVersion As String, browserVersion As String, dataImportedFromBridgeFile As String)
        Try
            Dim origErrMsg As String = errorMessages
            Dim proxy As New ProfileClient(Util.EndpointNameToUse())
            Try

                frmMain.PrepActivityLogArguments(applicationName, methodology, refID, callerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                If (errorMessages.Length < 2 And origErrMsg.Length > 2) Then
                    errorMessages = origErrMsg '.Substring(0, 998) 'ensure at least one errorMessage gets written; a populated error object makes errorMessages null.
                End If
                If Not IsDate(periodStart) Then periodStart = Nothing
                If Not IsDate(periodEnd) Then periodEnd = Nothing
                proxy.WriteActivityLogExtended(applicationName, methodology, callerIP,
                    userid, computername, service, method, enitityName, periodStart,
                    periodEnd, status, version, errorMessages,
                    localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile, Nothing)
            Catch
            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then
                    Try
                        proxy.Close()
                    Catch
                    End Try
                End If
            End Try
        Catch
        End Try

    End Sub

    Private Sub SetPaths()
        ' 20081001 RRH Path - Only MonthPath needs to be updated.
        'MonthPath = MyParent.AppPath & "\" & CurrentMonth & "\"
        'ConfigPath = MyParent.AppPath & "\_CONFIG\" - Removed, use pathConfig
        'RefPath = MyParent.AppPath & "\_REF\" - Removed, not used

        MonthPath = pathMonth & CurrentMonth & "\"

    End Sub

    Private Sub SetMdiParent(ByRef tmpfrm As frmMain)
        MyParent = tmpfrm
    End Sub

    Private Sub Main_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim y As Integer = CInt((MyParent.Height - 130) / 6)
        Dim sz As Size
        sz.Width = y
        tcControlPanel.ItemSize = sz
        'UpdateStatusBar()

        'new Radar Plot button
        Dim slide As Integer = 25
        GroupBox2.Height = 246 + slide
        GroupBox2.Top = 357 - slide


        'Dim treeChartsHeight As Integer = GroupBox2.Top - treeCharts.Top - 15
        'treeCharts.Height = treeChartsHeight ' 345 ' 257 - slide


        btnRadarPlot.Top = 42 + 8
        btnRadarPlot.Left = 25 - 20
        cboChartTargetColor.Top = chkChtTarget.Top
        cboChartAvgColor.Top = chkChtAVG.Top
        cboChartYtdColor.Top = chkChtYTD.Top

        MyParent.ActiveMainForm = Me

    End Sub

    Private Sub AddDataEntryControlHandlers()
        ' 20081001 RRH Path - MonthWatcher.Path = MyParent.AppPath & "\" & CurrentMonth
        MonthWatcher.Path = pathMonth & CurrentMonth & "\"
        MonthWatcher.Filter = "*.xml"
        MonthWatcher.EnableRaisingEvents = True
        AddHandler MonthWatcher.Changed, AddressOf MonthOnChanged

        ' 20081001 RRH Path - ConfigWatcher.Path = MyParent.AppPath & "\_CONFIG"
        ConfigWatcher.Path = pathConfig
        ConfigWatcher.Filter = "*.xml"
        ConfigWatcher.EnableRaisingEvents = True
        AddHandler ConfigWatcher.Changed, AddressOf ConfigOnChanged

        ' 20081001 RRH Path - ReferenceWatcher.Path = MyParent.AppPath & "\_REF"
        ReferenceWatcher.Path = pathRef
        ReferenceWatcher.Filter = "*.xml"
        ReferenceWatcher.EnableRaisingEvents = True
        AddHandler ConfigWatcher.Changed, AddressOf RefOnChanged
    End Sub

    Private Sub tcControlPanel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tcControlPanel.SelectedIndexChanged
        Dim tab As TabPage = tcControlPanel.SelectedTab
        Select Case tab.Name
            Case "tpSettings", "tpConfig", "tpDataEntry"
                SA_Browser1.Visible = False
                PrintControl = Nothing
                MyParent.MenuPrint.Enabled = False
            Case Else
                MyParent.MenuPrint.Enabled = True
                PrintControl = SA_Browser1
                SA_Browser1.Visible = True
                SA_Browser1.BringToFront()
                SA_Browser1.wbWebBrowser.Visible = False
                PrevCtl = SA_Browser1
                SA_Browser1.PleaseWait()
                Select Case tab.Name
                    Case tpTables.Name
                        If Not treeTableViews.SelectedNode Is Nothing Then SelectTableView("", "", SA_Browser1, treeTableViews.SelectedNode)
                        If HasTableBeenViewed Then SA_Browser1.wbWebBrowser.Visible = True Else SA_Browser1.wbWebBrowser.Visible = False
                        SA_Browser1.lblHeader.Text = "Table Views"
                        SA_Browser1.btnDump.Visible = False
                        SA_Browser1.btnDataEntry.Visible = True
                        SA_Browser1.btnAddToDashboard.Visible = False
                    Case tpReports.Name
                        SA_Browser1.wbWebBrowser.Visible = False
                        SA_Browser1.lblHeader.Text = "Reports of Results"
                        'SA_Browser1.btnDump.Visible = True
                        SA_Browser1.btnDataEntry.Visible = False

                        If modGeneral.AdminRights Then
                            SA_Browser1.btnAddToDashboard.Visible = True
                        End If
                    Case tpCharts.Name
                        SA_Browser1.wbWebBrowser.Visible = False
                        SA_Browser1.lblHeader.Text = "Charts of Results"
                        'SA_Browser1.btnDump.Visible = False
                        SA_Browser1.btnDataEntry.Visible = False
                        If modGeneral.AdminRights Then
                            SA_Browser1.btnAddToDashboard.Visible = True
                        End If

                End Select
        End Select
    End Sub

    Private Sub Main_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Try

            'Refinery Info
            If InfoNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Settings")
                    Case DialogResult.Yes : SaveInfo()
                    Case DialogResult.No
                        dsSettings.RejectChanges()
                        btnSave.ForeColor = BLACK
                        btnSave.BackColor = BTNFACE
                        InfoNotSaved = False
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Configuration
            If SA_Configuration1.ConfigNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Configuration")
                    Case DialogResult.Yes
                        SaveConfiguration()
                    Case DialogResult.No
                        dsProcess.RejectChanges()
                        SaveMade(SA_Configuration1.btnSave, SA_Configuration1.ConfigNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Refinery Information
            If SA_RefineryInfo1.RefineryInformationNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Refinery Information")
                    Case DialogResult.Yes
                        SaveRefineryInformation()
                    Case DialogResult.No
                        dsProcess.RejectChanges()
                        SaveMade(SA_RefineryInfo1.btnSave, SA_RefineryInfo1.RefineryInformationNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Process Facilities
            If SA_Process1.ProcessNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Process Facilities")
                    Case DialogResult.Yes
                        SaveProcess()
                    Case DialogResult.No
                        dsProcess.RejectChanges()
                        dsMappings.Tables("Config").RejectChanges()
                        dsMappings.Tables("MaintRout").RejectChanges()
                        dsMappings.Tables("ProcessData").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_Process1.btnSave, SA_Process1.ProcessNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Inventory
            If SA_Inventory1.InventoryNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Inventory")
                    Case DialogResult.Yes
                        SaveInventory()
                    Case DialogResult.No
                        dsRefineryInformation.RejectChanges()
                        dsMappings.Tables("Inventory").RejectChanges()
                        dsMappings.Tables("ConfigRS").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_Inventory1.btnSave, SA_Inventory1.InventoryNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Operating Expenses
            If SA_OperatingExpenses1.OpexNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Operating Expenses")
                    Case DialogResult.Yes
                        SaveOpex()
                    Case DialogResult.No
                        SA_OperatingExpenses1.tblFriendOpEx.RejectChanges()
                        dsMappings.Tables("OpEx").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_OperatingExpenses1.btnSave, SA_OperatingExpenses1.OpexNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Personnel
            If SA_Personnel1.PersonnelNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Personnel")
                    Case DialogResult.Yes
                        SavePersonnel()
                    Case DialogResult.No
                        dsPersonnel.RejectChanges()
                        dsMappings.Tables("Personnel").RejectChanges()
                        dsMappings.Tables("Absence").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_Personnel1.btnSave, SA_Personnel1.PersonnelNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Crude Crude Charge
            If SA_CrudeCharge1.CrudeNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Crude Charge Detail")
                    Case DialogResult.Yes
                        SaveCrude()
                    Case DialogResult.No
                        dsCrude.RejectChanges()
                        dsMappings.Tables("Crude").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_CrudeCharge1.btnSave, SA_CrudeCharge1.CrudeNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Material Balance
            If SA_MaterialBalance1.MaterialNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Material Balance")
                    Case DialogResult.Yes
                        SaveMaterialBalance()
                    Case DialogResult.No
                        dsYield.RejectChanges()
                        dsMappings.Tables("Yield_RM").RejectChanges()
                        dsMappings.Tables("Yield_RMB").RejectChanges()
                        dsMappings.Tables("Yield_Prod").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_MaterialBalance1.btnSave, SA_MaterialBalance1.MaterialNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'Energy
            If SA_Energy1.EnergyNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "Energy")
                    Case DialogResult.Yes
                        SaveEnergy()
                    Case DialogResult.No
                        dsEnergy.RejectChanges()
                        dsMappings.Tables("Energy").RejectChanges()
                        dsMappings.Tables("Electric").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_Energy1.btnSave, SA_Energy1.EnergyNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            'UserDefined
            If SA_UserDefined1.UserDefinedNotSaved Then
                Select Case ProfileMsgBox("YNC", "UNSAVED", "User-Defined Inputs/Outputs")
                    Case DialogResult.Yes
                        SaveUserDefined()
                    Case DialogResult.No
                        dsUserDefined.RejectChanges()
                        dsMappings.Tables("UserDefined").RejectChanges()
                        SaveMappings()
                        SaveMade(SA_UserDefined1.btnSave, SA_UserDefined1.UserDefinedNotSaved)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Exit Sub
                End Select
            End If

            MyParent.MenuFile.Enabled = True
            MyParent.MenuEdit.Enabled = True
            MyParent.MenuView.Enabled = True
            MyParent.MenuTools.Enabled = True
            MyParent.MenuWindow.Enabled = True
            MyParent.MenuHelp.Enabled = True

            DisablingWatchers()
            SA_Admin1.FileSystemWatcher1.EnableRaisingEvents = False
            My.Settings.Save()

            MyParent.RemoveWindowItem(PeriodStart)
        Catch ex As Exception
            ErrorHandler.Display("Error", ex, "MainClosing")
        End Try
    End Sub

    Private Sub SelectConfiguration(ByVal Node As TreeNode)
        If Node.Tag Is Nothing Then Exit Sub
        If IsNumeric(Node.Tag) Then
            Select Case Node.Parent.Text
                Case "Process Unit Level"
                    'SA_Targets1.ProcessLevel(Node.Tag, Trim(Node.Text))
                    'SA_Targets1.Visible = True
                    'SA_Targets1.BringToFront()
                    'SA_Targets1.BackColor = System.Drawing.SystemColors.Control ' 20081027 RRH Color
                Case Else
                    SA_Configuration1.SelectedUnitID(CInt(Node.Tag), Trim(Node.Text))
                    SA_Configuration1.Visible = True
                    SA_Configuration1.BringToFront()
                    SA_Configuration1.BackColor = System.Drawing.SystemColors.Control ' 20081027 RRH Color
            End Select
        Else
            Dim ctl As Control
            ctl = Node.Tag
            ctl.BackColor = System.Drawing.SystemColors.Control ' 20081027 RRH Color
            Select Case ctl.Name
                Case "SA_Inventory1"
                    Node.Tag.Visible = True
                    Node.Tag.BringToFront()
                Case "SA_RefineryInfo1"
                    Node.Tag.Visible = True
                    Node.Tag.BringToFront()
                    'SA_RefineryInfo1.LoadingForm()
                Case "SA_RoutineHistory1"
                    Node.Tag.Visible = True
                    Node.Tag.BringToFront()
            End Select
            PrevCtl = Node.Tag
        End If
        tpConfig.Tag = Node
    End Sub

    Private Sub treeConfiguration_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles treeConfiguration.AfterSelect
        SelectConfiguration(e.Node)
    End Sub

    Private Sub treeConfiguration_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles treeConfiguration.NodeMouseClick
        SelectConfiguration(e.Node)
    End Sub

    Private Sub SelectDataEntry()
        If lvDataEntry.SelectedItems.Count = 0 Then Exit Sub
        Dim tmpstr As String = lvDataEntry.SelectedItems(0).Text
        Dim ctl As Control

        Select Case tmpstr
            Case "Process Facilities" : ctl = SA_Process1
            Case "Inventory" : ctl = SA_Inventory1
            Case "Operating Expenses" : ctl = SA_OperatingExpenses1
            Case "Personnel" : ctl = SA_Personnel1
            Case "Crude Charge Detail" : ctl = SA_CrudeCharge1
            Case "Material Balance"
                ctl = SA_MaterialBalance1
                SA_MaterialBalance1.PopulateSummary()
            Case "Energy" : ctl = SA_Energy1
            Case "User-Defined Inputs/Outputs" : ctl = SA_UserDefined1
            Case Else
                ctl = Nothing
                Exit Sub
        End Select
        'al.SubmitActivityLog("", RefineryID, "", UserName, Environment.MachineName.ToString(), "", "SelectDataEntry", tmpstr, "", "", "", "Success")



        If Util.UseNonWCFWebService Then
            'al.SubmitActivityLog("SelectDataEntry", Nothing, Nothing, Nothing, "Success", Nothing, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            Try
                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, "SelectDataEntry", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                '    userid, computername, String.Empty, "SelectDataEntry", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
            Catch ex As Exception

            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If
        ctl.Visible = True
        ctl.BackColor = System.Drawing.SystemColors.Control
        ctl.BringToFront()
        tpDataEntry.Tag = ctl

    End Sub

    Public Function PrinterIsInstalled() As Boolean
        If PrinterSettings.InstalledPrinters.Count > 0 Then
            Return True
        Else
            HideWSCtrls()
            MsgBox("You do not have a printer set up on this machine.", MsgBoxStyle.Critical, "No Printer Installed")
            PrintControl = Nothing
            MyParent.MenuPrint.Enabled = False
        End If
    End Function

    Private Sub SelectTableViewOrig(ByVal Node As TreeNode)
        Throw New Exception("SelectTableViewOrig is not used")
        MyParent.MenuPrint.Enabled = True
        Dim ds As New DataSet
        Dim tmpCurr As String = CurrencyCode
        Dim loc As String = GetLocation()
        Dim co As String = GetCompany()
        Dim Title, Desc As String

        'SA_Browser1.GoBlank()
        SA_Browser1.Visible = True
        SA_Browser1.BringToFront()
        If Node.Parent Is Nothing Then

            Title = Node.Text
            Desc = CStr(Node.Tag)

            Select Case Title
                Case "Table 2", "Table 9", "Table 10"
                    ds.Tables.Add(dsProcess.Tables("ProcessData").Copy)
                    ds.Tables.Add(dsProcess.Tables("MaintTA").Copy)
                    ds.Tables.Add(dsProcess.Tables("MaintRout").Copy)
                    ds.Tables.Add(MyParent.ds_Ref.Tables("Table2_LU").Copy)
                    ds.Tables.Add(MyParent.ds_Ref.Tables("ProcessID_LU").Copy)
                Case "Table 4"
                    ds.Tables.Add(SA_OperatingExpenses1.tblFriendOpEx.Copy)
                    Dim studyDetail As Boolean = My.Settings.viewDetailOpex
                    ds.Tables(0).TableName = "OpEx"
                    For i As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
                        Dim row As DataRow = ds.Tables(0).Rows(i)
                        If studyDetail Then
                            If IsDBNull(row!detailStudy) Then row.Delete()
                        Else
                            If IsDBNull(row!detailProfile) Then row.Delete()
                        End If
                    Next
                Case "Table 5", "Table 6"
                    ds.Tables.Add(dsPersonnel.Tables("Pers").Copy)
                    Dim studyDetail As Boolean = My.Settings.viewDetailPersonnel
                    For i As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
                        Dim row As DataRow = ds.Tables(0).Rows(i)
                        If studyDetail Then
                            If IsDBNull(row!detailStudy) Then row.Delete()
                        Else
                            If IsDBNull(row!detailProfile) Then row.Delete()
                        End If
                    Next
                Case "Table 7"
                    ds.Tables.Add(dsPersonnel.Tables("Absence").Copy)
                    Dim studyDetail As Boolean = My.Settings.viewDetailPersonnel
                    For i As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
                        Dim row As DataRow = ds.Tables(0).Rows(i)
                        If studyDetail Then
                            If IsDBNull(row!detailStudy) Then row.Delete()
                        Else
                            If IsDBNull(row!detailProfile) Then row.Delete()
                        End If
                    Next
                Case "Table 14" : ds.Tables.Add(dsCrude.Tables("Crude").Copy)
                Case "User-Defined Inputs/Outputs" : ds.Tables.Add(dsUserDefined.Tables("UserDefined").Copy)
                Case "Historical Maintenance Costs" : ds.Tables.Add(dsRefineryInformation.Tables("MaintRoutHist").Copy)
                Case "EDC Stabilizers" : ds.Tables.Add(dsRefineryInformation.Tables("EDCStabilizers").Copy)
                Case Else : Exit Sub
            End Select
        Else

            Title = Node.Parent.Text
            Desc = Node.Text

            Select Case Title
                Case "Performance Targets"
                    ds.Tables.Add(dsRefineryInformation.Tables("RefTargets").Copy)
                    ds.Tables.Add(dsProcess.Tables("UnitTargetsNew").Copy)
                    ds.Tables.Add(MyParent.ds_Ref.Tables("Chart_LU").Copy)
                    ds.Tables.Add(MyParent.ds_Ref.Tables("ProcessID_LU").Copy)
                    Dim row As DataRow
                    Dim tfield, NewDesc As String
                    Dim dv As New DataView(dsRefineryInformation.Tables("RefTargets"))
                    For Each row In ds.Tables("Chart_LU").Rows
                        tfield = row!TargetField.ToString & ""
                        If tfield.EndsWith("_Target") Then
                            dv.RowFilter = "Property = '" & tfield & "'"
                            If dv.Count = 1 Then
                                NewDesc = Trim(dv.Item(0).Item("ChartTitle").ToString)
                                NewDesc = Replace(NewDesc, ",", "")
                                dv.Item(0).Item("ChartTitle") = NewDesc & ","
                            End If
                        End If
                    Next
                    tmpCurr = "Currency"
                Case "Table 1"
                    ds.Tables.Add(dsRefineryInformation.Tables("Inventory").Copy)
                    ds.Tables.Add(dsRefineryInformation.Tables("ConfigRS").Copy)
                    ds.Tables.Add(dsProcess.Tables("Config").Copy)
                    ds.Tables.Add(MyParent.ds_Ref.Tables("ProcessID_LU").Copy)
                Case "Table 15"
                    tmpCurr = dsSettings.Tables(0).Rows(0)!RptCurrencyT15.ToString
                    ds.Tables.Add(dsYield.Tables("Yield_RM").Copy)
                    ds.Tables.Add(dsYield.Tables("Yield_RMB").Copy)
                    ds.Tables.Add(dsYield.Tables("Yield_Prod").Copy)
                Case "Table 16"
                    ds.Tables.Add(dsEnergy.Tables("Energy").Copy)
                    ds.Tables.Add(dsEnergy.Tables("Electric").Copy)
                    ds.Tables.Add(MyParent.ds_Ref.Tables("Energy_LU").Copy)
                Case Else : Exit Sub
            End Select
        End If
        'al.SubmitActivityLog("", "", "", UserName, Environment.MachineName.ToString(), "Main", "SelectTableView", Title, "", "", "", "Success")


        If Util.UseNonWCFWebService Then
            'al.SubmitActivityLog("SelectTableView", Nothing, Nothing, Nothing, "Success", Nothing, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            'Dim svc As New WcfService.ProfileDataServiceClient()
            Try
                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                userid, computername, String.Empty, "SelectTableViewOrig", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                'userid, computername, String.Empty, "SelectTableViewOrig", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                'localDomainName, osVersion, Nothing, officeVersion, Nothing)
            Catch
            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If
        Try
            Dim ht As New Hashtable
            ' 20081001 RRH Path - ht.Add("HeaderImageDir", MyParent.AppPath & "\_TEMP\Templates")
            ht.Add("HeaderImageDir", pathTemp & "Templates")
            SA_Browser1.DisplayInputReport(ds, Title, GetCompany, GetLocation, Desc, UOM, tmpCurr, PeriodStart, ht)
            SA_Browser1.lblHeader.Text = "Table Views"
            SA_Browser1.wbWebBrowser.Visible = True
            HasTableBeenViewed = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        tpTables.Tag = Node
        SA_Browser1.Ordinal = 0
        SA_Browser1.Url = Title + Chr(14) + Desc
    End Sub

    Public Function SelectTableView(title As String, desc As String,
             ByRef browserControl As SA_Browser, Optional ByVal Node As TreeNode = Nothing) As String
        Dim returnValue As String = String.Empty
        MyParent.MenuPrint.Enabled = True
        Dim ds As New DataSet
        Dim tmpCurr As String = CurrencyCode
        Dim loc As String = GetLocation()
        Dim co As String = GetCompany()
        If Not IsNothing(Node) Then
            If Node.Parent Is Nothing Then
                title = Node.Text
                desc = CStr(Node.Tag)
            Else
                title = Node.Parent.Text
                desc = Node.Text
            End If
        End If

        'title = Node.Text
        ' desc = CStr(Node.Tag)

        Select Case title
            Case "Table 2", "Table 9", "Table 10"
                ds.Tables.Add(dsProcess.Tables("ProcessData").Copy)
                ds.Tables.Add(dsProcess.Tables("MaintTA").Copy)
                ds.Tables.Add(dsProcess.Tables("MaintRout").Copy)
                ds.Tables.Add(MyParent.ds_Ref.Tables("Table2_LU").Copy)
                ds.Tables.Add(MyParent.ds_Ref.Tables("ProcessID_LU").Copy)
            Case "Table 4"
                'this is dummy code to get an "Under Construction";
                'Product owner told me that Table 4 NEVER worked.
                ds.Tables.Add(dsPersonnel.Tables("Absence").Copy)
                Dim studyDetail As Boolean = My.Settings.viewDetailPersonnel
                For i As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
                    Dim row As DataRow = ds.Tables(0).Rows(i)
                    If studyDetail Then
                        If IsDBNull(row!detailStudy) Then row.Delete()
                    Else
                        If IsDBNull(row!detailProfile) Then row.Delete()
                    End If
                Next

                'orig code below:
                'ds.Tables.Add(SA_OperatingExpenses1.tblFriendOpEx.Copy)
                'Dim studyDetail As Boolean = My.Settings.viewDetailOpex
                'ds.Tables(0).TableName = "OpEx"

                'Dim col As New DataColumn("DataType", GetType(String))
                'ds.Tables(0).Columns.Add(col)
                ''                ds.Tables.
                'For Each r As DataRow In ds.Tables(0).Rows
                '    r("DataType") = "A"
                '    If IsDBNull(r("costsRefinery")) Then
                '        r("costsRefinery") = 0
                '    End If
                'Next

                'For i As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
                '    Dim row As DataRow = ds.Tables(0).Rows(i)
                '    If studyDetail Then
                '        If IsDBNull(row!detailStudy) Then row.Delete()
                '    Else
                '        If IsDBNull(row!detailProfile) Then row.Delete()
                '    End If
                'Next
            Case "Table 5", "Table 6"
                ds.Tables.Add(dsPersonnel.Tables("Pers").Copy)
                Dim studyDetail As Boolean = My.Settings.viewDetailPersonnel
                For i As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
                    Dim row As DataRow = ds.Tables(0).Rows(i)
                    If studyDetail Then
                        If IsDBNull(row!detailStudy) Then row.Delete()
                    Else
                        If IsDBNull(row!detailProfile) Then row.Delete()
                    End If
                Next
            Case "Table 7"
                ds.Tables.Add(dsPersonnel.Tables("Absence").Copy)
                Dim studyDetail As Boolean = My.Settings.viewDetailPersonnel
                For i As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
                    Dim row As DataRow = ds.Tables(0).Rows(i)
                    If studyDetail Then
                        If IsDBNull(row!detailStudy) Then row.Delete()
                    Else
                        If IsDBNull(row!detailProfile) Then row.Delete()
                    End If
                Next
            Case "Table 14" : ds.Tables.Add(dsCrude.Tables("Crude").Copy)
            Case "User-Defined Inputs/Outputs" : ds.Tables.Add(dsUserDefined.Tables("UserDefined").Copy)
            Case "Historical Maintenance Costs" : ds.Tables.Add(dsRefineryInformation.Tables("MaintRoutHist").Copy)
            Case "EDC Stabilizers" : ds.Tables.Add(dsRefineryInformation.Tables("EDCStabilizers").Copy)
            Case "Performance Targets"
                ds.Tables.Add(dsRefineryInformation.Tables("RefTargets").Copy)
                ds.Tables.Add(dsProcess.Tables("UnitTargetsNew").Copy)
                ds.Tables.Add(MyParent.ds_Ref.Tables("Chart_LU").Copy)
                ds.Tables.Add(MyParent.ds_Ref.Tables("ProcessID_LU").Copy)
                Dim row As DataRow
                Dim tfield, NewDesc As String
                Dim dv As New DataView(dsRefineryInformation.Tables("RefTargets"))
                For Each row In ds.Tables("Chart_LU").Rows
                    tfield = row!TargetField.ToString & ""
                    If tfield.EndsWith("_Target") Then
                        dv.RowFilter = "Property = '" & tfield & "'"
                        If dv.Count = 1 Then
                            NewDesc = Trim(dv.Item(0).Item("ChartTitle").ToString)
                            NewDesc = Replace(NewDesc, ",", "")
                            dv.Item(0).Item("ChartTitle") = NewDesc & ","
                        End If
                    End If
                Next
                tmpCurr = "Currency"
            Case "Table 1"
                ds.Tables.Add(dsRefineryInformation.Tables("Inventory").Copy)
                ds.Tables.Add(dsRefineryInformation.Tables("ConfigRS").Copy)
                ds.Tables.Add(dsProcess.Tables("Config").Copy)
                ds.Tables.Add(MyParent.ds_Ref.Tables("ProcessID_LU").Copy)
            Case "Table 15"
                tmpCurr = dsSettings.Tables(0).Rows(0)!RptCurrencyT15.ToString
                ds.Tables.Add(dsYield.Tables("Yield_RM").Copy)
                ds.Tables.Add(dsYield.Tables("Yield_RMB").Copy)
                ds.Tables.Add(dsYield.Tables("Yield_Prod").Copy)
            Case "Table 16"
                ds.Tables.Add(dsEnergy.Tables("Energy").Copy)
                ds.Tables.Add(dsEnergy.Tables("Electric").Copy)
                ds.Tables.Add(MyParent.ds_Ref.Tables("Energy_LU").Copy)
            Case Else : Exit Function
        End Select


        Try
            'ds.WriteXml("C:\temp\Table4b.xml")
        Catch
        End Try

        'al.SubmitActivityLog("", "", "", UserName, Environment.MachineName.ToString(), "Main", "SelectTableView", Title, "", "", "", "Success")


        If Util.UseNonWCFWebService Then
            'al.SubmitActivityLog("SelectTableView", Nothing, Nothing, Nothing, "Success", Nothing, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            Try



                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, "SelectTableView", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                '    userid, computername, String.Empty, "SelectTableView", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
            Catch ex As Exception
                Dim msg As String = ex.Message
            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If
        Dim ht As New Hashtable
        Try
            ht.Add("HeaderImageDir", pathTemp & "Templates") ' 20081001 RRH Path - ht.Add("HeaderImageDir", MyParent.AppPath & "\_TEMP\Templates")
            If Not IsNothing(browserControl) Then
                'SA_Browser1.GoBlank()
                browserControl.Visible = True
                browserControl.BringToFront()
                browserControl.DisplayInputReport(ds, title, GetCompany, GetLocation, desc, UOM, tmpCurr, PeriodStart, ht)
                returnValue = browserControl.wbWebBrowser.DocumentText
                browserControl.lblHeader.Text = "Table Views"
                browserControl.wbWebBrowser.Visible = True
                HasTableBeenViewed = True
                browserControl.Ordinal = 0
                browserControl.Url = title + _delim + desc
            Else
                'called from Dashboard
                Dim pg As New Page
                returnValue = pg.createPage(ds, title, GetCompany, GetLocation, desc, UOM, tmpCurr, PeriodStart.Month, PeriodStart.Year, PeriodStart, ht)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        tpTables.Tag = Node

        Return returnValue
    End Function

    Private Sub treeTableViews_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles treeTableViews.NodeMouseClick
        SelectTableView("", "", SA_Browser1, e.Node)
    End Sub

    Public Sub SaveAll()
        If SA_Configuration1.ConfigNotSaved Then SaveConfiguration()
        If SA_Inventory1.InventoryNotSaved Then SaveInventory()
        If SA_RefineryInfo1.RefineryInformationNotSaved Then SaveRefineryInformation()
        If SA_Process1.ProcessNotSaved Then SaveProcess()
        If SA_OperatingExpenses1.OpexNotSaved Then SaveOpex()
        If SA_Personnel1.PersonnelNotSaved Then SavePersonnel()
        If SA_CrudeCharge1.CrudeNotSaved Then SaveCrude()
        If SA_MaterialBalance1.MaterialNotSaved Then SaveMaterialBalance()
        If SA_Energy1.EnergyNotSaved Then SaveEnergy()
        If SA_UserDefined1.UserDefinedNotSaved Then SaveUserDefined()
    End Sub

    Public Sub PrintPreviews(ByVal node1 As String, ByVal node2 As String)
        tcControlPanel.SelectedTab = tpTables
        Dim cnode, pnode As TreeNode
        For Each pnode In treeTableViews.Nodes
            If pnode.Text = node1 And node2 Is Nothing Then
                treeTableViews.SelectedNode = pnode
                Exit Sub
            Else
                For Each cnode In pnode.Nodes
                    If node2 = cnode.Text And node1 = pnode.Text Then
                        treeTableViews.SelectedNode = cnode
                        Exit Sub
                    End If
                Next
            End If
        Next
    End Sub

    Public Sub EditMonthlyData(ByVal Area As String)
        tcControlPanel.SelectedTab = tpDataEntry
        For Each i As ListViewItem In lvDataEntry.Items
            If i.SubItems(0).Text = Area Then i.Selected = True Else i.Selected = False
        Next
    End Sub

    Public Sub EditConfiguration(ByVal node1 As String)
        tcControlPanel.SelectedTab = tpConfig
        treeConfiguration.SelectedNode = Nothing
        Select Case node1
            Case "Refinery Information"
                treeConfiguration.SelectedNode = treeConfiguration.Nodes(0)
            Case "Process Facilities"
                treeConfiguration.SelectedNode = treeConfiguration.Nodes(1).FirstNode.FirstNode.FirstNode
        End Select
    End Sub

    Public Sub SelectTabPage(ByVal tabCtl As TabControl, ByVal tabPg As TabPage)
        tabCtl.SelectedTab = tabPg
    End Sub

    Public Sub HideWSCtrls()
        pnlBlank.Visible = True
        pnlBlank.BringToFront()
    End Sub

    Public Sub UpdateStatusBar()
        MyParent.pnlCurrency.Text = "Currency: " & CurrencyCode
        MyParent.pnlUOM.Text = "Units of Measure: " & UOM
    End Sub

#End Region

#Region " Refinery Settings and Confuration "
    Public Sub PopulateLoadedMonths()
        Dim dt As DataTable = dsLoadedMonths.Tables(0)
        dt.Columns.Add("MonthDisplay")

        Dim dvRpt As New DataView(dt)
        dvRpt.Sort = "PeriodStart DESC"
        With cmbRptMonth
            .DisplayMember = "PeriodStart"
            .ValueMember = "PeriodStart"
            .DataSource = dvRpt
            .SelectedIndex = 0
            '.SelectedValue = CDate(CurrentMonth.Substring(4, 2) & "/1/" & CurrentMonth.Substring(0, 4))
            Dim dtmTemp As Date = New Date(PeriodYear, PeriodMonth, 1)
            .SelectedValue = dtmTemp ' CDate(PeriodMonth & "/1/" & CurrentMonth.Substring(0, 4))
        End With

        Dim dvChtStart As New DataView(dt)
        dvChtStart.Sort = "PeriodStart DESC"
        Dim dtmChtTemp As Date = New Date(PeriodYear, PeriodMonth, 1)
        With cmbChtStart
            .DisplayMember = "PeriodStart"
            .ValueMember = "PeriodStart"
            .DataSource = dvChtStart
            '.SelectedValue = DateAdd("M", -12, CDate(CurrentMonth.Substring(4, 2) & "/1/" & CurrentMonth.Substring(0, 4)))
            'If dvChtStart.Count > 11 Then .SelectedIndex = 11 Else .SelectedIndex = dvChtStart.Count - 1

            .SelectedValue = DateAdd("M", -12, dtmChtTemp)
        End With

        Dim dvChtEnd As New DataView(dt)
        dvChtEnd.Sort = "PeriodStart DESC"
        With cmbChtEnd
            .DisplayMember = "PeriodStart"
            .ValueMember = "PeriodStart"
            .DataSource = dvChtEnd
            .SelectedIndex = 0
            '.SelectedValue = CDate(CurrentMonth.Substring(4, 2) & "/1/" & CurrentMonth.Substring(0, 4))

            .SelectedValue = dtmChtTemp ' CDate(PeriodMonth & "/1/" & CurrentMonth.Substring(0, 4))
        End With


        cmbRptUOM.SelectedIndex = 0
        cmbChtUOM.SelectedIndex = 0
    End Sub

    Friend Function GetBridgeVersion(tmpWB As String) As String
        Dim rtn As String = ""
        Dim MyExcel As Object = OpenExcel()
        Dim MyWB As Object = OpenWorkbook(MyExcel, tmpWB)


        Dim Version As String = MyWB.Names("VERSION").value.ToString()
        rtn = Version

        If MyExcel Is Nothing Or
            MyWB Is Nothing Then
            rtn = ""
        End If
        Return rtn
        Exit Function

    End Function
    Private Sub LoadInfo()
        IsLoading = True

        Try

            CurrencyCode = dsSettings.Tables(0).Rows(0)!RptCurrency.ToString
            KCurrency = CurrencyCode & "/1000"
            UOM = dsSettings.Tables(0).Rows(0)!UOM.ToString

            dsSettings.Tables(0).Rows(0)!PeriodMonth = PeriodMonth
            dsSettings.Tables(0).Rows(0)!PeriodYear = PeriodYear
            dsSettings.Tables(0).Rows(0)!ClientVersion = Application.ProductVersion

            dsSettings.AcceptChanges()

            With cmbRptCurrency
                .DisplayMember = "Description"
                .ValueMember() = "CurrencyCode"
                .DataSource = MyParent.ds_Ref.Tables("Currency_LU")
            End With


            Dim b As Binding
            txtCompany.DataBindings.Add("Text", dsSettings.Tables(0), "Company")
            txtLocation.DataBindings.Add("Text", dsSettings.Tables(0), "Location")
            txtCoordName.DataBindings.Add("Text", dsSettings.Tables(0), "CoordName")
            txtCoordTitle.DataBindings.Add("Text", dsSettings.Tables(0), "CoordTitle")
            txtCoordPhone.DataBindings.Add("Text", dsSettings.Tables(0), "CoordPhone")
            txtCoordEMail.DataBindings.Add("Text", dsSettings.Tables(0), "CoordEMail")
            cmbUOM.DataBindings.Add("SelectedItem", dsSettings.Tables(0), "UOM")
            cmbRptCurrency.DataBindings.Add("SelectedValue", dsSettings.Tables(0), "RptCurrency")
            cmbRptCurrencyT15.DataBindings.Add("SelectedItem", dsSettings.Tables(0), "RptCurrencyT15")

            'IsLoading = False
            MyParent.RefLocation = Trim(dsSettings.Tables(0).Rows(0)!Location.ToString)
            Me.Text = Format(PeriodStart, "y") & " - " & Trim(dsSettings.Tables(0).Rows(0)!Location.ToString)
            PopulateMethodology()
            PopulateSettings()


        Catch ex As Exception
            MsgBox("Error loading refinery settings for " & Me.Text & ".  Please exit Profile II and upon re-entry, refresh your local months through the administrative tools." & vbCrLf & ex.Message, MsgBoxStyle.Critical, "Profile II")
            LoadingReferenceData("Ready", False)
        End Try
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        If ProfileMsgBox("YN", "CLEAR", "Refinery Settings") <> Windows.Forms.DialogResult.Yes Then Exit Sub
        Dim row As DataRow = dsSettings.Tables(0).Rows(0)
        row!Company = DBNull.Value
        row!Location = DBNull.Value
        row!CoordName = DBNull.Value
        row!CoordTitle = DBNull.Value
        row!CoordPhone = DBNull.Value
        row!CoordEmail = DBNull.Value
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveInfo()
    End Sub

    Public Sub SaveInfo()
        MonthWatcher.EnableRaisingEvents = False
        ConfigWatcher.EnableRaisingEvents = False
        dsSettings.AcceptChanges()
        dsSettings.WriteXml(MonthPath & "Settings.xml", XmlWriteMode.WriteSchema)
        btnSave.ForeColor = Color.White
        btnSave.BackColor = Color.Gray
        InfoNotSaved = False
        MonthWatcher.EnableRaisingEvents = True
        ConfigWatcher.EnableRaisingEvents = True
    End Sub

    Private Sub MonthOnChanged(ByVal source As Object, ByVal e As FileSystemEventArgs)
        MonthWatcher.EnableRaisingEvents = False
        While (True)
            Try
                Dim XMLFileName As String = e.Name
                Select Case XMLFileName
                    Case "Settings.xml"
                        HandleMonthUpdate(dsSettings, btnSave, InfoNotSaved, "Refinery Settings", XMLFileName)
                    Case "Process.xml"
                        HandleMonthUpdate(dsProcess, SA_Process1.btnSave, SA_Process1.ProcessNotSaved, "Process Facilities", XMLFileName)
                        UpdateTable2()
                    Case "RefineryInformation.xml"
                        HandleMonthUpdate(dsRefineryInformation, SA_RefineryInfo1.btnSave, SA_Inventory1.InventoryNotSaved, "Refinery Information", XMLFileName)
                    Case "OpEx.xml"
                        HandleMonthUpdate(dsOperatingExpenses, SA_OperatingExpenses1.btnSave, SA_OperatingExpenses1.OpexNotSaved, "Operating Expenses", XMLFileName)
                    Case "Personnel.xml"
                        HandleMonthUpdate(dsPersonnel, SA_Personnel1.btnSave, SA_Personnel1.PersonnelNotSaved, "Personnel", XMLFileName)
                    Case "Crude.xml"
                        HandleMonthUpdate(dsCrude, SA_CrudeCharge1.btnSave, SA_CrudeCharge1.CrudeNotSaved, "Crude Charge Detail", XMLFileName)
                    Case "Yield.xml"
                        HandleMonthUpdate(dsYield, SA_MaterialBalance1.btnSave, SA_MaterialBalance1.MaterialNotSaved, "Material Balance", XMLFileName)
                    Case "Energy.xml"
                        HandleMonthUpdate(dsEnergy, SA_Energy1.btnSave, SA_Energy1.EnergyNotSaved, "Energy", XMLFileName)
                    Case "UserDefined.xml"
                        HandleMonthUpdate(dsUserDefined, SA_UserDefined1.btnSave, SA_UserDefined1.UserDefinedNotSaved, "User Defined Inputs/Outputs", XMLFileName)
                    Case "IsChecked.xml"
                        HandleMonthUpdate(dsIsChecked, Nothing, False, "User Defined Inputs/Outputs", XMLFileName)
                End Select
                Exit While
            Catch ex As Exception
                'keep looping until free
                If ex.GetType() Is Type.GetType("IOException") Then
                    Throw ex
                End If
            End Try
        End While
        MonthWatcher.EnableRaisingEvents = True
    End Sub

    Private Sub HandleMonthUpdate(ByVal ds As DataSet, ByVal btn As Button, ByRef IsUnsaved As Boolean, ByVal tmpstr As String, ByVal XMLFileName As String)
        ' 20081216 RRH Removed MsgBox and simplified code
        'Dim result As DialogResult
        'If IsUnsaved Then
        '    result = MsgBox("Someone has made changes to " & tmpstr & ". Do you want to accept these changes? If so, you will lose the unsaved changes you have made.", MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo, "Profile II")
        '    If result = Windows.Forms.DialogResult.No Then Exit Sub
        'End If

        If IsUnsaved AndAlso ProfileMsgBox("YN", "", "Someone has made changes to " & tmpstr & ". Do you want to accept these changes? If so, you will lose the unsaved changes you have made.") = Windows.Forms.DialogResult.No Then Exit Sub

        ds.Clear()
        ' 20081001 RRH Path - Dim cpath As String = MyParent.AppPath & "\" & CurrentMonth & "\"
        Dim cpath As String = pathMonth & CurrentMonth & "\"
        ds.ReadXml(cpath & XMLFileName)
        If Not btn Is Nothing Then
            btn.BackColor = BTNFACE
            btn.ForeColor = BLACK
        End If
        IsUnsaved = False
    End Sub

    Private Sub ConfigOnChanged(ByVal source As Object, ByVal e As FileSystemEventArgs)
        ConfigWatcher.EnableRaisingEvents = False
        ' 20081001 RRH Path - Dim cpath As String = MyParent.AppPath & "\_CONFIG\"
        Dim cpath As String = pathConfig
        'Dim result As DialogResult
        While (True)
            Try
                Dim XMLFileName As String = e.Name
                Select Case XMLFileName
                    Case "UnitList.xml"
                        dsUnitList.Clear()
                        dsUnitList.ReadXml(cpath & "UnitList.xml")
                    Case "Process.xml"


                        'If SA_Configuration1.ConfigNotSaved Then
                        '    result = MsgBox("Someone has made changes to Refinery Configuration. Do you want to accept these changes? If so, you will lose the unsaved changes you have made.", MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo, "Profile II")
                        '    If result = Windows.Forms.DialogResult.No Then Exit Sub
                        'End If

                        If SA_Configuration1.ConfigNotSaved AndAlso ProfileMsgBox("YN", "", "Someone has made changes to Refinery Configuration. Do you want to accept these changes? If so, you will lose the unsaved changes you have made.") = Windows.Forms.DialogResult.No Then Exit Sub

                        Dim tmpds As New DataSet
                        Dim tmpdt, dt As DataTable
                        dsProcess.Tables.Remove("MaintTA")
                        tmpds.ReadXml(cpath & "Process.xml")
                        tmpdt = tmpds.Tables("MaintTA")
                        dt = tmpdt.Copy
                        dsProcess.Tables.Add(dt)
                    Case "LoadedMonths.xml"
                        dsLoadedMonths.Clear()
                        dsLoadedMonths.ReadXml(cpath & "LoadedMonths.xml")
                End Select
                Exit While
            Catch ex As Exception
                'keep looping until free
                If ex.GetType() Is Type.GetType("IOException") Then
                    Throw ex
                End If
            End Try
        End While
        MonthWatcher.EnableRaisingEvents = True
    End Sub

    Private Sub RefOnChanged(ByVal source As Object, ByVal e As FileSystemEventArgs)
        While (True)
            Try
                PopulateReferenceData()
                Exit While
            Catch ex As Exception
                'keep looping until free
                If ex.GetType() Is Type.GetType("IOException") Then
                    Throw ex
                End If
            End Try
        End While
    End Sub

    Private Sub cmbUOM_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbUOM.SelectionChangeCommitted
        PopulateSettings()
    End Sub

    Private Sub cmbCurrency_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRptCurrency.SelectionChangeCommitted
        'If IsLoading Then Exit Sub
        'If dsCurrency_LU.Tables.Count = 0 Then Exit Sub
        If sender.Text.ToString = "" Then Exit Sub
        PopulateSettings()
        UpdateStatusBar()
    End Sub

    Private Sub cmbRptCurrencyT15_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRptCurrencyT15.SelectionChangeCommitted
        'If IsLoading Then Exit Sub
        'If dsCurrency_LU.Tables.Count = 0 Then Exit Sub
        If sender.Text.ToString = "" Then Exit Sub
        InfoChanged()
    End Sub

    Private Sub txtCompany_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCompany.TextChanged
        InfoChanged()
    End Sub

    Private Sub txtLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLocation.TextChanged
        InfoChanged()
    End Sub

    Private Sub txtCoordName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCoordName.TextChanged
        InfoChanged()
    End Sub

    Private Sub txtCoordEmail_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCoordEMail.TextChanged
        InfoChanged()
    End Sub

    Private Sub txtCoordPhone_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCoordPhone.TextChanged
        InfoChanged()
    End Sub

    Private Sub txtCoordTitle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCoordTitle.TextChanged
        InfoChanged()
    End Sub

    Private Sub InfoChanged()
        If IsLoading Then Exit Sub
        If modGeneral.AdminRights = False Then Exit Sub
        If InfoNotSaved Then Exit Sub
        btnSave.ForeColor = Color.White
        btnSave.BackColor = Color.Red
        InfoNotSaved = True
    End Sub

    Private Sub PopulateSettings()

        If IsLoading = False Then
            CurrencyCode = Trim(cmbRptCurrency.Text.Substring(cmbRptCurrency.Text.Length - 4))
            UOM = cmbUOM.Text
        End If

        KCurrency = CurrencyCode & "/1000"
        CurrencyT15 = dsSettings.Tables(0).Rows(0)!RptCurrencyT15.ToString

        With cmbRptCurrencyT15.Items
            .Clear()
            .Add("USD")
            If CurrencyCode <> "USD" Then .Add(CurrencyCode)
            'cmbRptCurrencyCode.SelectedIndex = 0
        End With

        With cmbRptCurrencyCode.Items
            .Clear()
            .Add("USD")
            If CurrencyCode <> "USD" Then .Add(CurrencyCode)
            'cmbRptCurrencyCode.SelectedIndex = 0
        End With

        With cmbChtCurrencyCode.Items
            .Clear()
            .Add("USD")
            If CurrencyCode <> "USD" Then .Add(CurrencyCode)
            'cmbChtCurrencyCode.SelectedIndex = 0
        End With

        cmbRptUOM.SelectedValue = UOM
        cmbChtUOM.SelectedValue = UOM

        If UOM = "US Units" Then DisplayUnitCol = "DisplayTextUS" Else DisplayUnitCol = "DisplayTextMet"

        If IsNothing(SA_Process1) Then SA_Process1 = New SA_Process()
        SA_Process1.SetCurrencyAndUOM()
        SA_OperatingExpenses1.SetCurrencyAndUOM()
        SA_Configuration1.SetUOMandCurrency(UOM, CurrencyCode, KCurrency)

        Try
            SA_Energy1.SetCurrencyAndUOM(CurrencyCode, UOM)
        Catch ex As Exception
        End Try
        SA_MaterialBalance1.SetCurrencyAndUOM(CurrencyCode)
        If IsNothing(SA_RefineryInfo1) Then SA_RefineryInfo1 = New SA_RefineryInfo()
        SA_RefineryInfo1.SetUOMandCurrency(UOM, CurrencyCode)
        'SA_Targets1.SetUOM(UOM)
        UpdateStatusBar()
        InfoChanged()

    End Sub

    Private Function GetCompany() As String
        Dim dt As DataTable = dsSettings.Tables(0)
        Try
            Return dt.Rows(0)!Company.ToString
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function GetLocation() As String
        Dim dt As DataTable = dsSettings.Tables(0)
        Try
            Return dt.Rows(0)!Location.ToString
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

#Region " Populating TreeViews "

    Private Sub PopulateConfigTree(ByVal UnitID As Integer)

        Dim dvPID As New DataView(MyParent.ds_Ref.Tables("ProcessID_LU"))
        Dim dvPG As New DataView(MyParent.ds_Ref.Tables("ProcessGroup_LU"))
        Dim dvConf As New DataView(dsProcess.Tables(0))

        Dim Group, LastProcess, Process, UnitName As String
        Dim node1 As TreeNode = Nothing
        Dim node2 As TreeNode = Nothing
        Dim node3 As TreeNode = Nothing
        Dim node4 As TreeNode = Nothing
        Dim node5 As TreeNode = Nothing

        Dim cnode As TreeNode = Nothing

        Dim ProcessID, ProcessGroup As String
        Dim cUnitID As Integer

        Dim i, j As Integer

        treeConfiguration.Nodes.Clear()

        node5 = treeConfiguration.Nodes.Add("Refinery Information")
        node5.Tag = SA_RefineryInfo1
        node5.ToolTipText = "Performance Targets, Inventory, EDC Stabilizers & Historical Costs"

        node1 = treeConfiguration.Nodes.Add("Process Facilities")
        node1.ToolTipText = "Unit Configuration, Turnarounds & Performance Targets"
        dvPG.Sort = "SortKey ASC"

        For i = 0 To dvPG.Count - 1
            ProcessGroup = dvPG.Item(i)!ProcessGroup.ToString
            Group = dvPG.Item(i)!GroupDesc.ToString
            dvConf.RowFilter = "ProcessGroup = '" & ProcessGroup & "'"
            dvConf.Sort = "SortKey ASC"
            LastProcess = ""
            If dvConf.Count > 0 Then
                node2 = node1.Nodes.Add(Group)
                For j = 0 To dvConf.Count - 1
                    ProcessID = dvConf.Item(j)!Processid.ToString
                    dvPID.RowFilter = "ProcessID = '" & ProcessID & "'"
                    Process = Trim(dvPID.Item(0)!ProcessDesc.ToString)
                    If Process <> LastProcess Then
                        node3 = node2.Nodes.Add(Process)
                        LastProcess = Process
                    End If
                    UnitName = Trim(dvConf.Item(j)!UnitName.ToString)
                    cUnitID = CInt(Trim(dvConf.Item(j)!UnitID.ToString))
                    If cUnitID = UnitID Then
                        cnode = node3.Nodes.Add(UnitName)
                        cnode.Tag = cUnitID
                    Else
                        node3.Nodes.Add(UnitName).Tag = cUnitID
                    End If
                Next
                node2.Expand()
            End If
        Next

        node3 = node2.Nodes.Add("Utilities and Off-Sites Maintenance")
        node3.Nodes.Add("Off-Sites - Tankage").Tag = 90051
        node3.Nodes.Add("Off-Sites - Blending/Oil Movements").Tag = 90052
        node3.Nodes.Add("Off-Sites - Coke Handling/Loading").Tag = 90053
        node3.Nodes.Add("Off-Sites - Other").Tag = 90054
        node3.Nodes.Add("Marine").Tag = 90055
        node3.Nodes.Add("Utilities - Other Steam/Electrical").Tag = 90056
        node3.Nodes.Add("Utilities - Environmental").Tag = 90057
        node3.Nodes.Add("Utilities - Other").Tag = 90058

        For Each n As TreeNode In treeConfiguration.Nodes
            If Not n.Tag Is Nothing AndAlso n.Tag.ToString = UnitID.ToString Then cnode = n : Exit For
        Next n

        If Not cnode Is Nothing Then
            treeConfiguration.SelectedNode = cnode
        Else
            node1.Expand()
        End If

        'treeConfiguration.ExpandAll() ' functions

    End Sub
    Private Sub PopulateTableViewsTree()

        treeTableViews.Nodes.Clear()

        Dim i As Integer
        Dim pnode As TreeNode = Nothing
        Dim tmpStr, Title, Desc, prevTitle, postTitle As String

        Const TrailingAppSettingsKeys As Integer = 1



        ' Does not follow the pattern for all else . . 
        tmpStr = GetAppSettingKey("Table 1;Process Facilities")
        Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode = treeTableViews.Nodes.Add(Title)
        pnode.Nodes.Add(Desc)

        tmpStr = GetAppSettingKey("Table 1;Inventory").ToString()
        'Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        'pnode = treeTableViews.Nodes.Add(Title)
        pnode.Nodes.Add(Desc)

        'all else follow pattern
        tmpStr = GetAppSettingKey("Table 2;Process Data").ToString()
        PopulateTreeTableViewsNode(tmpStr)

        tmpStr = GetAppSettingKey("Table 4;Operating Expenses").ToString()
        PopulateTreeTableViewsNode(tmpStr)
        tmpStr = GetAppSettingKey("Table 5;Operator, Craft and Clerical").ToString()
        PopulateTreeTableViewsNode(tmpStr)
        tmpStr = GetAppSettingKey("Table 6;Management, Professional and Staff").ToString()
        PopulateTreeTableViewsNode(tmpStr)
        tmpStr = GetAppSettingKey("Table 7;Company Employee Absences").ToString()
        PopulateTreeTableViewsNode(tmpStr)
        tmpStr = GetAppSettingKey("Table 9;Turnaround Maintenance").ToString()
        PopulateTreeTableViewsNode(tmpStr)
        tmpStr = GetAppSettingKey("Table 10;Non-Turnaround Maintenance").ToString()
        PopulateTreeTableViewsNode(tmpStr)
        tmpStr = GetAppSettingKey("Table 14;Crude Charge Detail").ToString()
        PopulateTreeTableViewsNode(tmpStr)

        'does not follow pattern
        tmpStr = GetAppSettingKey("Table 15;Raw Material Inputs").ToString()
        Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode = treeTableViews.Nodes.Add(Title)
        pnode.Nodes.Add(Desc)

        tmpStr = GetAppSettingKey("Table 15;Raw Material Blended Inputs").ToString()
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode.Nodes.Add(Desc)

        tmpStr = GetAppSettingKey("Table 15;Product Yields").ToString()
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode.Nodes.Add(Desc)

        'not follow pattern
        tmpStr = GetAppSettingKey("Table 16;Thermal Energy").ToString()
        Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode = treeTableViews.Nodes.Add(Title)
        pnode.Nodes.Add(Desc)

        'If AddTitle Then  pnode.Tag = Desc  Else pnode.Nodes.Add(Desc)

        tmpStr = GetAppSettingKey("Table 16;Electricity").ToString()
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode.Nodes.Add(Desc)

        tmpStr = GetAppSettingKey("User-Defined Inputs/Outputs;Customized").ToString()
        PopulateTreeTableViewsNode(tmpStr)

        'not follow
        tmpStr = GetAppSettingKey("Performance Targets;Refinery Level").ToString()
        Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode = treeTableViews.Nodes.Add(Title)
        pnode.Nodes.Add(Desc)

        tmpStr = GetAppSettingKey("Performance Targets;Process Unit Level").ToString()
        Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
        pnode.Nodes.Add(Desc)

        tmpStr = GetAppSettingKey("EDC Stabilizers;EDC Stabilizers").ToString()
        PopulateTreeTableViewsNode(tmpStr)
        tmpStr = GetAppSettingKey("Historical Maintenance Costs;Non-Turnaround").ToString()
        PopulateTreeTableViewsNode(tmpStr)
    End Sub

    'Private Sub PopulateTableViewsTree_Legacy()

    '    treeTableViews.Nodes.Clear()

    '    Dim i As Integer
    '    Dim pnode As TreeNode = Nothing
    '    Dim tmpStr, Title, Desc, prevTitle, postTitle As String

    '    Const TrailingAppSettingsKeys As Integer = 1



    '    ' Does not follow the pattern for all else . . 
    '    tmpStr = GetAppSettingKey("Table 1;Process Facilities")
    '    Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode = treeTableViews.Nodes.Add(Title)
    '    pnode.Nodes.Add(Desc)

    '    tmpStr = GetAppSettingKey("Table 1;Inventory").ToString()
    '    'Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    'pnode = treeTableViews.Nodes.Add(Title)
    '    pnode.Nodes.Add(Desc)

    '    'all else follow pattern
    '    tmpStr = GetAppSettingKey("Table 2;Process Data").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)

    '    tmpStr = GetAppSettingKey("Table 4;Operating Expenses").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    '    tmpStr = GetAppSettingKey("Table 5;Operator, Craft and Clerical").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    '    tmpStr = GetAppSettingKey("Table 6;Management, Professional and Staff").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    '    tmpStr = GetAppSettingKey("Table 7;Company Employee Absences").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    '    tmpStr = GetAppSettingKey("Table 9;Turnaround Maintenance").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    '    tmpStr = GetAppSettingKey("Table 10;Non-Turnaround Maintenance").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    '    tmpStr = GetAppSettingKey("Table 14;Crude Charge Detail").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)

    '    'does not follow pattern
    '    tmpStr = GetAppSettingKey("Table 15;Raw Material Inputs").ToString()
    '    Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode = treeTableViews.Nodes.Add(Title)
    '    pnode.Nodes.Add(Desc)

    '    tmpStr = GetAppSettingKey("Table 15;Raw Material Blended Inputs").ToString()
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode.Nodes.Add(Desc)

    '    tmpStr = GetAppSettingKey("Table 15;Product Yields").ToString()
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode.Nodes.Add(Desc)

    '    'not follow pattern
    '    tmpStr = GetAppSettingKey("Table 16;Thermal Energy").ToString()
    '    Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode = treeTableViews.Nodes.Add(Title)
    '    pnode.Nodes.Add(Desc)

    '    'If AddTitle Then  pnode.Tag = Desc  Else pnode.Nodes.Add(Desc)

    '    tmpStr = GetAppSettingKey("Table 16;Electricity").ToString()
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode.Nodes.Add(Desc)

    '    tmpStr = GetAppSettingKey("User-Defined Inputs/Outputs;Customized").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)

    '    'not follow
    '    tmpStr = GetAppSettingKey("Performance Targets;Refinery Level").ToString()
    '    Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode = treeTableViews.Nodes.Add(Title)
    '    pnode.Nodes.Add(Desc)

    '    tmpStr = GetAppSettingKey("Performance Targets;Process Unit Level").ToString()
    '    Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)
    '    pnode.Nodes.Add(Desc)

    '    tmpStr = GetAppSettingKey("EDC Stabilizers;EDC Stabilizers").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    '    tmpStr = GetAppSettingKey("Historical Maintenance Costs;Non-Turnaround").ToString()
    '    PopulateTreeTableViewsNode(tmpStr)
    'End Sub

    Private Function GetAppSettingKey(key As String) As String
        For count As Integer = 0 To _appSettingsKeys.Length - 1
            If key = _appSettingsKeys(count) Then
                Return key
            End If
        Next
        Return String.Empty
    End Function


    Private Sub PopulateTreeTableViewsNode(titleAndDesc As String, Optional AddTitle As Boolean = True)
        Dim pnode As TreeNode
        Dim title As String = Mid(titleAndDesc, 1, InStr(titleAndDesc, ";") - 1)
        Dim desc As String = Mid(titleAndDesc, InStr(titleAndDesc, ";") + 1)
        If AddTitle Then
            pnode = treeTableViews.Nodes.Add(title)
            pnode.Tag = desc
        Else
            pnode.Nodes.Add(desc)
        End If
    End Sub

    'Legacy
    Private Sub PopulateTableViewsTreeLegacy()

        treeTableViews.Nodes.Clear()

        Dim i As Integer
        Dim pnode As TreeNode = Nothing
        Dim tmpStr, Title, Desc, prevTitle, postTitle As String

        Const TrailingAppSettingsKeys As Integer = 1

        For i = 2 To System.Configuration.ConfigurationManager.AppSettings.Count - TrailingAppSettingsKeys - 1

            tmpStr = System.Configuration.ConfigurationManager.AppSettings.AllKeys(i)

            Title = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
            Desc = Mid(tmpStr, InStr(tmpStr, ";") + 1)

            If i > 2 Then
                tmpStr = System.Configuration.ConfigurationManager.AppSettings.AllKeys(i - 1)
                prevTitle = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
            Else
                prevTitle = ""
            End If

            If i < System.Configuration.ConfigurationManager.AppSettings.Count - TrailingAppSettingsKeys - 1 Then
                'If i >= 21 Then Stop 'SB added temporarily
                tmpStr = System.Configuration.ConfigurationManager.AppSettings.AllKeys(i + 1)
                postTitle = Mid(tmpStr, 1, InStr(tmpStr, ";") - 1)
            Else
                postTitle = ""
            End If

            If Title <> prevTitle Then
                pnode = treeTableViews.Nodes.Add(Title)
                Debug.WriteLine("TITLE: " + tmpStr + "|" + Title)
            End If

            If Title <> postTitle And Title <> prevTitle Then
                pnode.Tag = Desc
                Debug.WriteLine("TAG: " + tmpStr + "|" + Desc)
            Else
                pnode.Nodes.Add(Desc)
                Debug.WriteLine(".Nodes.Add: " + tmpStr + "|" + Desc)
            End If

        Next i

    End Sub

    Private Sub PopulateChartTree()
        treeCharts.Nodes.Clear()
        btnChtCreate.Enabled = MyParent.ResultsRights

        Dim dt As DataTable = MyParent.ds_Ref.Tables("Chart_LU")
        Dim row As DataRow
        Dim SectionHeader, ChartTitle As String
        Dim LastSectionHeader As String = ""
        Dim PNode As TreeNode = Nothing
        For Each row In dt.Rows
            SectionHeader = row!SectionHeader.ToString
            ChartTitle = row!ChartTitle.ToString
            If LastSectionHeader <> SectionHeader Then
                PNode = treeCharts.Nodes.Add(SectionHeader)
            End If
            LastSectionHeader = SectionHeader
            PNode.Nodes.Add(ChartTitle).Tag = row
            'Dim breakPoint As String = ""
            'If ChartTitle.Contains("NTA") Then Stop
        Next
    End Sub

    Private Sub PopulateReportTree()
        lvReports.Items.Clear()

        btnRptCreate.Enabled = MyParent.ResultsRights
        If IsNothing(SA_Browser1) Then SA_Browser1 = New SA_Browser()
        SA_Browser1.btnDump.Enabled = MyParent.ResultsRights

        Dim dt As DataTable = MyParent.ds_Ref.Tables("Report_LU")
        Dim row As DataRow
        'Dim ReportName As String
        For Each row In dt.Rows
            'ReportName = row!ReportName.ToString
            Dim item As New ListViewItem
            item.Name = row!ReportCode
            If row!ReportName.ToString().Contains("EDC, Utilization, Standard Energy") Then
                item.Text = "EDC Calculation" ' row!ReportName.ToString()
            Else
                item.Text = row!ReportName.ToString()
            End If

            item.Tag = row!CustomGroup.ToString()
            lvReports.Items.Add(item)
            'lvReports.Items.Add(ReportName).Tag = row!CustomGroup.ToString()
        Next
    End Sub

    Private Sub PopulateMethodology()
        Dim dt As DataTable = MyParent.ds_Ref.Tables("Methodology")
        cmbRptMeth.DataSource = dt
        cmbRptMeth.DisplayMember = "FactorSet"
        cmbChtMeth.DataSource = dt
        cmbChtMeth.DisplayMember = "FactorSet"

        Try
            modGeneral.Methodology = dt.Rows(0)(0).ToString().Trim()
        Catch
        End Try

        cmbRptMeth.SelectedIndex = 0
        cmbChtMeth.SelectedIndex = 0
    End Sub

#End Region

#Region " Configuration "

    Public Sub GetConfiguration()
        MyParent = CType(Me.ParentForm, frmMain)
        LoadConfiguration()
    End Sub

    Public Sub SaveConfiguration()

        DisablingWatchers()
        dsProcess.AcceptChanges()
        dsUnitList.AcceptChanges()

        dsProcess.WriteXml(MonthPath & "Process.xml", XmlWriteMode.WriteSchema)
        dsProcess.WriteXml(pathConfig & "Process.xml", XmlWriteMode.WriteSchema)
        dsUnitList.WriteXml(MonthPath & "UnitList.xml", XmlWriteMode.WriteSchema)
        dsUnitList.WriteXml(pathConfig & "Unitlist.xml", XmlWriteMode.WriteSchema)

        UpdateTable2()
        UpdateProcessTargets()

        Dim UnitID As Integer
        Try
            UnitID = CInt(treeConfiguration.SelectedNode.Tag())
        Catch ex As Exception
            UnitID = 0
        End Try

        PopulateConfigTree(UnitID)

        SaveMade(SA_Configuration1.btnSave, SA_Configuration1.ConfigNotSaved)
        EnablingWatchers()
    End Sub

    Private Sub UpdateProcessTargets()
        Dim UnitName As String
        Dim dvItem As DataRow
        Dim ProcessID, TargetProperty As String
        Dim UnitID, SortKey, i, j As Integer
        Dim dvConfig As New DataView(dsProcess.Tables("Config"))
        Dim dvUnitTargetsNew As New DataView(dsProcess.Tables("UnitTargetsNew"))
        Dim dvUnitTargets_LU As New DataView(MyParent.ds_Ref.Tables("UnitTargets_LU"))
        dvConfig.Sort = "SortKey ASC"
        '-----UPDATE *****************************************
        For i = 0 To dvConfig.Count - 1
            UnitID = CInt(dvConfig.Item(i)!UnitID)
            ProcessID = dvConfig.Item(i)!ProcessID.ToString
            SortKey = CInt(dvConfig.Item(i)!SortKey)
            UnitName = dvConfig.Item(i)!UnitName.ToString
            If SortKey < 3000 Then
                dvUnitTargets_LU.RowFilter = "ProcessID = '" & ProcessID & "' or ProcessID = 'ALL'"
                If dvUnitTargets_LU.Count > 0 Then
                    For j = 0 To dvUnitTargets_LU.Count - 1
                        TargetProperty = dvUnitTargets_LU.Item(j)!Property.ToString
                        dvUnitTargetsNew.RowFilter = "UnitID = " & UnitID & " AND Property = '" & Trim(TargetProperty) & "'"
                        If dvUnitTargetsNew.Count = 0 Then
                            Dim newrow As DataRow = dsProcess.Tables("UnitTargetsNew").NewRow()
                            newrow!UnitID = UnitID
                            newrow!ProcessID = ProcessID
                            newrow!UnitName = UnitName
                            newrow!Property = TargetProperty
                            newrow!USDescription = dvUnitTargets_LU.Item(j)!USDescription
                            newrow!MetDescription = dvUnitTargets_LU.Item(j)!MetDescription
                            newrow!USDecPlaces = dvUnitTargets_LU.Item(j)!USDecPlaces
                            newrow!MetDecPlaces = dvUnitTargets_LU.Item(j)!MetDecPlaces
                            newrow!SortKey = dvUnitTargets_LU.Item(j)!SortKey
                            newrow!CurrencyCode = ""
                            dsProcess.Tables("UnitTargetsNew").Rows.Add(newrow)
                        Else
                            For Each dvItem In dsProcess.Tables("UnitTargetsNew").Rows
                                dvItem!CurrencyCode = Trim(dvItem!CurrencyCode.ToString)
                            Next
                        End If
                    Next
                End If
            End If
        Next
        dsProcess.Tables("UnitTargetsNew").AcceptChanges()
    End Sub

    Public Sub DeleteConfiguration(ByVal UnitID As Integer)
        Dim NewUnitID As Integer
        Dim i As Integer

        Dim dt As DataTable
        For Each dt In dsProcess.Tables
            Select Case dt.TableName
                Case "Config", "MaintRout"
                    For i = dt.Rows.Count - 1 To 0 Step -1
                        If CInt(dt.Rows(i)!UnitID) = UnitID Then
                            dt.Rows(i).Delete()
                            Exit For
                        End If
                    Next i
                Case Else
                    For i = dt.Rows.Count - 1 To 0 Step -1
                        If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete()
                    Next i
            End Select
        Next

        dsProcess.AcceptChanges()

        dt = dsMappings.Tables("Config")
        For i = dt.Rows.Count - 1 To 0 Step -1
            If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete() : Debug.Print(UnitID & "   If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete()")
        Next i

        dt = dsMappings.Tables("MaintRout")
        For i = dt.Rows.Count - 1 To 0 Step -1
            If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete() : Debug.Print(UnitID & "   If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete()")
        Next i

        dt = dsMappings.Tables("ProcessData")
        For i = dt.Rows.Count - 1 To 0 Step -1
            If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete() : Debug.Print(UnitID & "   If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete()")
        Next i

        ' 20090129 RRH Delete Target Information when a unit is deleted.
        dt = dsProcess.Tables("UnitTargetsNew")
        For i = dt.Rows.Count - 1 To 0 Step -1
            If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete() : Debug.Print(UnitID & "   If CInt(dt.Rows(i)!UnitID) = UnitID Then dt.Rows(i).Delete()")
        Next i

        dsMappings.AcceptChanges()
        SaveMappings()

        Dim dv As New DataView(dsProcess.Tables("Config"))
        dv.Sort = "SortKey, UnitID"
        NewUnitID = CInt(dv.Item(0)!UnitID)
        PopulateConfigTree(NewUnitID)
        SA_Configuration1.SelectedUnitID(NewUnitID, Nothing)
        SA_Configuration1.Visible = True

    End Sub

    Public Sub AddProcessUnit(ByVal UnitID As Integer)
        PopulateConfigTree(UnitID)
        UpdateTable2()
        UpdateTable10()
        UpdateProcessTargets()
    End Sub

    Private Sub LoadConfiguration()


        Try
            Dim tmpdt, dt As DataTable
            Dim tmpds As New DataSet

            LoadingReferenceData("Loading Process Data...", True)
            RemoveNonProcessFacilities()
            PopulateConfigTree(Nothing)
            AddOffsitesAndUtilities(dsProcess.Tables("MaintTA"))
            UpdateProcessTargets()
            SA_Configuration1.SetDataSet(dsProcess, MyParent.ds_Ref, dsUnitList)
            If MyParent.ConfigRights = False Then NoRights(SA_Configuration1.pnlHeader, Nothing)
            SA_Configuration1.SetUOMandCurrency(UOM, CurrencyCode, KCurrency)

            LoadProcessFacilities()
            LoadingReferenceData("Ready", False)
        Catch ex As Exception
            LoadingReferenceData("Ready", False)
        End Try


    End Sub

    Private Sub RemoveNonProcessFacilities()
        Dim ProcessID As String
        Dim ProcFac As String
        Dim dv As New DataView(MyParent.ds_Ref.Tables("ProcessID_LU"))
        Dim i As Integer
        For i = dsProcess.Tables("Config").Rows.Count - 1 To 0 Step -1
            With dsProcess.Tables("Config")
                ProcessID = .Rows(i)!ProcessID.ToString
                dv.RowFilter = "ProcessID = '" & ProcessID & "'"
                If dv.Count = 1 Then
                    ProcFac = dv.Item(0)!ProfileProcFacility.ToString
                    If ProcFac = "N" Then .Rows(i).Delete()
                End If
            End With
        Next
    End Sub

    Private Sub UpdateUtilsAndOffs()
        Dim UnitID As Integer
        Dim dt As DataTable = dsProcess.Tables("MaintTA")
        Dim dv As New DataView(dt)
        Dim row As DataRow
        For UnitID = 90051 To 90058
            dv.RowFilter = "UnitID = " & UnitID
            If dv.Count = 0 Then
                row = dt.NewRow
                Select Case UnitID
                    Case 90051
                        row!ProcessID = "OFFTANK"
                        row!UnitName = "Offsite - Tankage"
                        row!SortKey = 6000
                    Case 90052
                        row!ProcessID = "OFFBLEND"
                        row!UnitName = "Offsite - Coke Handling/Loading"
                        row!SortKey = 6005
                    Case 90053
                        row!ProcessID = "OFFCOKE"
                        row!UnitName = "Offsite - Coke Handling/Loading"
                        row!SortKey = 6010
                    Case 90054
                        row!ProcessID = "OFFOTH"
                        row!UnitName = "Offsite - Other"
                        row!SortKey = 6015
                    Case 90055
                        row!ProcessID = "MARINE"
                        row!UnitName = "Marine Facility"
                        row!SortKey = 6100
                    Case 90056
                        row!ProcessID = "UTILSTM"
                        row!UnitName = "Utilities - Steam/Electrical"
                        row!SortKey = 6200
                    Case 90057
                        row!ProcessID = "UTILENV"
                        row!UnitName = "Utilities - Environmental"
                        row!SortKey = 6205
                    Case 90058
                        row!ProcessID = "UTILOTH"
                        row!UnitName = "Utilities - Other"
                        row!SortKey = 6210
                End Select
                row!TAID = 1
                dt.Rows.Add(row)
            End If
        Next
        dt.AcceptChanges()
    End Sub

    Private Sub CleanUpProcess()
        Dim row As DataRow
        For Each row In dsProcess.Tables("Config").Rows
            If row!ProcessID.ToString <> "FTCOGEN" Then
                row!RptStmCap = DBNull.Value
                row!StmUtilPcnt = DBNull.Value
            End If
        Next
        For Each row In dsProcess.Tables("MaintTA").Rows
            If row!TAExceptions Is DBNull.Value Then
                row!TAExceptions = 0
            End If
        Next
    End Sub

#End Region

#Region " Workspace Handlers "
    Public Sub LoadingReferenceData(ByVal lbl As String, ByVal IsLoad As Boolean)
        MyParent.pnlProgress.Text = lbl
        If IsLoad Then
            MyParent.Cursor = Cursors.WaitCursor
        Else
            MyParent.Cursor = Cursors.Default
        End If
    End Sub
#End Region

#Region " Routine History "
    Private Sub LoadRoutineHistory()
        On Error GoTo RoutineHistoryError
        If dsRefineryInformation.Tables("MaintRoutHist").Rows.Count = 0 Then
            Dim dv As New DataView(dsLoadedMonths.Tables(0))
            dv.Sort = "PeriodStart ASC"
            Dim cmonth, cyear As Integer
            cmonth = CInt(dv.Item(0)!PeriodMonth)
            cyear = CInt(dv.Item(0)!PeriodYear)
            'Dim tmpdate As Date = PeriodStart
            Dim row As DataRow
            With dsRefineryInformation.Tables("MaintRoutHist")
                For i As Integer = 1 To 24
                    row = .NewRow
                    Select Case cmonth
                        Case 1 : cmonth -= cmonth
                        Case Else
                            cmonth = 12
                            cyear -= cyear
                    End Select
                    Dim sdt As New Date(cyear, cmonth, 1)
                    row!PeriodStart = sdt
                    .Rows.Add(row)
                Next
            End With
            SaveRefineryInformation()
        End If
        'SA_RoutineHistory1.SetDataSet()
        'SA_RoutineHistory1.HasRights(MyParent.AdminRights)
        Exit Sub
RoutineHistoryError:
        MsgBox("Error loading historical non-turnaround maintenance costs for " & Me.Text & ".  Please exit Profile II and upon re-entry, refresh your local months through the administrative tools.", MsgBoxStyle.Critical, "Profile II")
        LoadingReferenceData("Ready", False)
    End Sub

#End Region

#Region " Refinery Configuration "
    Private Sub LoadRefineryInformation()
        Try

            dsRefineryInformation.Tables("RefTargets").Columns.Add("ChartTitle")
            dsRefineryInformation.Tables("RefTargets").Columns.Add("AxisLabelUS")
            dsRefineryInformation.Tables("RefTargets").Columns.Add("AxisLabelMetric")
        Catch ex As Exception
            'GoTo RefineryInformationError
        End Try

        Dim row As DataRow
        Dim dv As New DataView(dsRefineryInformation.Tables("RefTargets"))
        Dim SortKey As Integer

        For Each row In MyParent.ds_Ref.Tables("Chart_LU").Rows
            SortKey = CInt(row!SortKey)
            If SortKey < 800 Then
                dv.RowFilter = "SortKey = " & SortKey
                If dv.Count = 0 Then
                    Dim newrow As DataRow = dsRefineryInformation.Tables("RefTargets").NewRow
                    newrow!SortKey = row!SortKey
                    newrow!SectionHeader = row!SectionHeader
                    newrow!Property = row!TargetField
                    newrow!ChartTitle = row!ChartTitle
                    newrow!AxisLabelUS = row!AxisLabelUS
                    newrow!AxisLabelMetric = row!AxisLabelMetric
                    If InStr(row!AxisLabelUS.ToString, "CurrencyCode", CompareMethod.Text) = 0 Then
                        newrow!CurrencyCode = ""
                    ElseIf CurrencyCode = "USD" Then
                        newrow!CurrencyCode = "USD"
                    End If

                    dsRefineryInformation.Tables("RefTargets").Rows.Add(newrow)
                Else
                    Select Case dv.Item(0)!Property.ToString
                        Case "EDC" : dv.Item(0).Delete()
                        Case "UEDC" : dv.Item(0).Delete()
                        Case Else
                            dv.Item(0)!SectionHeader = row!SectionHeader
                            dv.Item(0)!Property = row!TargetField
                            dv.Item(0)!ChartTitle = row!ChartTitle
                            dv.Item(0)!AxisLabelUS = row!AxisLabelUS
                            dv.Item(0)!AxisLabelMetric = row!AxisLabelMetric
                            If InStr(row!AxisLabelUS.ToString, "CurrencyCode", CompareMethod.Text) = 0 Then
                                dv.Item(0)!CurrencyCode = ""
                            ElseIf CurrencyCode = "USD" Then
                                dv.Item(0)!CurrencyCode = "USD"
                            End If
                    End Select
                End If
            End If
        Next
        dsRefineryInformation.AcceptChanges()

        Try
            SA_RefineryInfo1.SetDataSet()
            If MyParent.RefineryInformationRights = False Then NoRights(SA_RefineryInfo1.pnlHeader, Nothing)
            SA_RefineryInfo1.SetUOMandCurrency(UOM, CurrencyCode)
            Exit Sub
        Catch ex As Exception
            GoTo RefineryInformationError
        End Try
RefineryInformationError:
        'PUT IN CORRECT MSGBOX
        MsgBox("Error loading Refinery Information for " & Me.Text & ".  Please exit Profile II and upon re-entry, refresh your local months through the administrative tools.", MsgBoxStyle.Critical, "Profile II")
        LoadingReferenceData("Ready", False)
    End Sub

    Public Sub SaveRefineryInformation()
        DisablingWatchers()
        dsRefineryInformation.AcceptChanges()
        dsRefineryInformation.WriteXml(MonthPath & "RefineryInformation.xml", XmlWriteMode.WriteSchema)
        dsRefineryInformation.WriteXml(pathConfig & "RefineryInformation.xml", XmlWriteMode.WriteSchema)
        SaveMade(SA_RefineryInfo1.btnSave, SA_RefineryInfo1.RefineryInformationNotSaved)
        EnablingWatchers()
    End Sub

#End Region

#Region " Process Facilities "

    Private Sub UpdateConfigMappings()
        Dim row, mrow(0), xrow As DataRow
        Dim col As DataColumn
        If dsMappings.Tables("Config") Is Nothing Then
            Dim dt As New DataTable

            For Each col In dsProcess.Tables("Config").Columns
                Select Case col.ColumnName
                    Case "UnitID", "SortKey" : dt.Columns.Add(col.ColumnName, GetType(Integer))
                    Case Else : dt.Columns.Add(col.ColumnName, GetType(String))
                End Select
            Next

            For Each row In dsProcess.Tables("Config").Rows
                mrow(0) = dt.NewRow
                mrow(0)!ProcessID = row!ProcessID
                mrow(0)!UnitName = row!UnitName
                mrow(0)!UnitID = row!UnitID
                mrow(0)!SortKey = row!SortKey
                dt.Rows.Add(mrow(0))
            Next

            dt.TableName = "Config"

            dsMappings.Tables.Add(dt)

            '20090410 RRH dsMappings.WriteXml(pathStartUp & "Mappings.xml", XmlWriteMode.WriteSchema)
            XMLFile.WriteXMLFile(dsMappings, pathStartUp & "Mappings.xml")

        Else
            For Each row In dsProcess.Tables("Config").Rows
                mrow = dsMappings.Tables("Config").Select("UnitID=" & row!UnitID.ToString)
                If mrow.Length = 0 Then
                    xrow = dsMappings.Tables("Config").NewRow
                    xrow!ProcessID = row!ProcessID
                    xrow!UnitName = row!UnitName
                    xrow!UnitID = row!UnitID
                    xrow!SortKey = row!SortKey
                    dsMappings.Tables("Config").Rows.Add(xrow)
                Else
                    mrow(0)!UnitName = row!UnitName
                    mrow(0)!ProcessID = row!ProcessID
                End If
            Next

            '20090410 RRH dsMappings.WriteXml(pathStartUp & "Mappings.xml", XmlWriteMode.WriteSchema)
            XMLFile.WriteXMLFile(dsMappings, pathStartUp & "Mappings.xml")

        End If
        dsMappings.Tables("Config").AcceptChanges()
    End Sub

    Private Sub UpdateProcessDataMappings()
        Dim row, mrow(0), xrow As DataRow
        Dim col As DataColumn
        If dsMappings.Tables("ProcessData") Is Nothing Then
            Dim dt As New DataTable

            For Each col In dsProcess.Tables("ProcessData").Columns
                Select Case col.ColumnName
                    Case "UnitID", "SortKey" : dt.Columns.Add(col.ColumnName, GetType(Integer))
                    Case Else : dt.Columns.Add(col.ColumnName, GetType(String))
                End Select
            Next

            For Each row In dsProcess.Tables("ProcessData").Rows
                mrow(0) = dt.NewRow
                mrow(0)!ProcessID = row!ProcessID
                mrow(0)!UnitName = row!UnitName
                mrow(0)!UnitID = row!UnitID
                mrow(0)!Property = row!Property
                mrow(0)!SortKey = row!SortKey
                mrow(0)!USDescription = row!USDescription
                mrow(0)!MetDescription = row!MetDescription
                dt.Rows.Add(mrow(0))
            Next

            dt.TableName = "ProcessData"

            dsMappings.Tables.Add(dt)

            '20090410 RRH dsMappings.WriteXml(pathStartUp & "Mappings.xml", XmlWriteMode.WriteSchema)
            XMLFile.WriteXMLFile(dsMappings, pathStartUp & "Mappings.xml")

        Else
            For Each row In dsProcess.Tables("ProcessData").Rows
                mrow = dsMappings.Tables("ProcessData").Select("UnitID=" & row!UnitID.ToString & " and Property = '" & row!Property.ToString & "'")
                If mrow.Length = 0 Then
                    xrow = dsMappings.Tables("ProcessData").NewRow
                    xrow!ProcessID = row!ProcessID
                    xrow!UnitName = row!UnitName
                    xrow!UnitID = row!UnitID
                    xrow!SortKey = row!SortKey
                    xrow!Property = row!Property
                    xrow!USDescription = row!USDescription
                    xrow!MetDescription = row!MetDescription
                    dsMappings.Tables("ProcessData").Rows.Add(xrow)
                Else
                    mrow(0)!UnitName = row!UnitName
                    mrow(0)!ProcessID = row!ProcessID
                    mrow(0)!USDescription = row!USDescription
                    mrow(0)!MetDescription = row!MetDescription
                End If
            Next

            '20090410 RRH dsMappings.WriteXml(pathStartUp & "Mappings.xml", XmlWriteMode.WriteSchema)
            XMLFile.WriteXMLFile(dsMappings, pathStartUp & "Mappings.xml")

        End If
        dsMappings.Tables("ProcessData").AcceptChanges()
    End Sub

    Private Sub UpdateMaintRoutMappings()
        Dim row, mrow(0), xrow As DataRow
        Dim col As DataColumn
        If dsMappings.Tables("MaintRout") Is Nothing Then
            Dim dt As New DataTable

            For Each col In dsProcess.Tables("MaintRout").Columns
                Select Case col.ColumnName
                    Case "UnitID", "SortKey" : dt.Columns.Add(col.ColumnName, GetType(Integer))
                    Case Else : dt.Columns.Add(col.ColumnName, GetType(String))
                End Select
            Next

            For Each row In dsProcess.Tables("MaintRout").Rows
                mrow(0) = dt.NewRow
                mrow(0)!ProcessID = row!ProcessID
                mrow(0)!UnitName = row!UnitName
                mrow(0)!UnitID = row!UnitID
                mrow(0)!SortKey = row!SortKey
                dt.Rows.Add(mrow(0))
            Next

            dt.TableName = "MaintRout"

            dsMappings.Tables.Add(dt)

            '20090410 RRH dsMappings.WriteXml(pathStartUp & "Mappings.xml", XmlWriteMode.WriteSchema)
            XMLFile.WriteXMLFile(dsMappings, pathStartUp & "Mappings.xml")

        Else
            For Each row In dsProcess.Tables("MaintRout").Rows
                mrow = dsMappings.Tables("MaintRout").Select("UnitID=" & row!UnitID.ToString)
                If mrow.Length = 0 Then
                    xrow = dsMappings.Tables("MaintRout").NewRow
                    xrow!ProcessID = row!ProcessID
                    xrow!UnitName = row!UnitName
                    xrow!UnitID = row!UnitID
                    xrow!SortKey = row!SortKey
                    dsMappings.Tables("MaintRout").Rows.Add(xrow)
                Else
                    mrow(0)!UnitName = row!UnitName
                    mrow(0)!ProcessID = row!ProcessID
                End If
            Next

            '20090410 RRH dsMappings.WriteXml(pathStartUp & "Mappings.xml", XmlWriteMode.WriteSchema)
            XMLFile.WriteXMLFile(dsMappings, pathStartUp & "Mappings.xml")

        End If
        dsMappings.Tables("MaintRout").AcceptChanges()
    End Sub

    Public Sub SaveProcess()
        DisablingWatchers()
        SaveIsChecked()
        dsProcess.AcceptChanges()
        dsProcess.WriteXml(MonthPath & "Process.xml", XmlWriteMode.WriteSchema)
        SaveMade(SA_Process1.btnSave, SA_Process1.ProcessNotSaved)
        EnablingWatchers()
        SaveMappings()
    End Sub

    Private Sub LoadProcessFacilities()
        CleanUpProcess()
        UpdateConfigMappings()
        UpdateTable2()
        UpdateProcessDataMappings()
        UpdateTable10()
        UpdateMaintRoutMappings()
        SA_Process1.SetDataSet()
        SA_Process1.SetCurrencyAndUOM()
        If MyParent.ProcessRights = False Then NoRights(SA_Process1.pnlHeader, SA_Process1.gbTools)
    End Sub

    Private Sub UpdateTable2()

        Dim dt As DataTable = dsProcess.Tables("ProcessData")
        Try
            dt.Columns.Add("USDescription")
            dt.Columns.Add("MetDescription")
        Catch ex As Exception
        End Try

        Dim dv As New DataView(MyParent.ds_Ref.Tables("Table2_LU"))
        Dim dvT2 As New DataView(dt)

        Dim dtT2Map As DataTable = dsMappings.Tables("ProcessData")
        Dim dvT2Map As New DataView(dtT2Map)

        Dim UnitID As Integer
        Dim ProcessID, Prop, UnitName, USDesc, MetDesc As String
        Dim SortKey As Integer
        Dim i As Integer

        Try

            For Each row As DataRow In dsProcess.Tables(0).Rows

                UnitID = CInt(row!UnitID)
                ProcessID = row!ProcessID.ToString
                UnitName = row!UnitName.ToString
                dv.RowFilter = "ProcessID = '" & ProcessID & "'"

                For i = 0 To dv.Count - 1
                    Prop = dv.Item(i)!Property.ToString
                    'If Prop = "UOPK" Then Stop
                    SortKey = CInt(dv.Item(i)!SortKey)
                    USDesc = dv.Item(i)!USDescription.ToString
                    MetDesc = dv.Item(i)!MetDescription.ToString

                    dvT2.RowFilter = "UnitID = " & UnitID & " and Property = '" & Prop & "'"

                    If dvT2.Count = 0 Then
                        Dim newrow As DataRow = dt.NewRow
                        newrow!UnitID = UnitID
                        newrow!ProcessID = ProcessID
                        newrow!SortKey = SortKey
                        newrow!UnitName = UnitName
                        newrow!Property = Prop
                        newrow!USDescription = USDesc
                        newrow!MetDescription = MetDesc
                        dt.Rows.Add(newrow)
                    Else
                        dvT2.Item(0)!UnitID = UnitID
                        dvT2.Item(0)!ProcessID = ProcessID
                        dvT2.Item(0)!SortKey = SortKey
                        dvT2.Item(0)!UnitName = UnitName
                        dvT2.Item(0)!Property = Prop
                        dvT2.Item(0)!USDescription = USDesc
                        dvT2.Item(0)!MetDescription = MetDesc
                    End If

                    dvT2Map.RowFilter = "UnitID = " & UnitID & " and Property = '" & Prop & "'"

                    If dvT2Map.Count = 0 Then
                        Dim newrow As DataRow = dtT2Map.NewRow
                        newrow!UnitID = UnitID
                        newrow!ProcessID = ProcessID
                        newrow!SortKey = SortKey
                        newrow!UnitName = UnitName
                        newrow!Property = Prop
                        newrow!USDescription = USDesc
                        newrow!MetDescription = MetDesc
                        dtT2Map.Rows.Add(newrow)
                    Else
                        dvT2Map.Item(0)!UnitID = UnitID
                        dvT2Map.Item(0)!ProcessID = ProcessID
                        dvT2Map.Item(0)!SortKey = SortKey
                        dvT2Map.Item(0)!UnitName = UnitName
                        dvT2Map.Item(0)!Property = Prop
                        dvT2Map.Item(0)!USDescription = USDesc
                        dvT2Map.Item(0)!MetDescription = MetDesc
                    End If

                Next i

            Next row

            dt.AcceptChanges()
            dtT2Map.AcceptChanges()

        Catch ex As Exception
            MessageBox.Show(UnitID.ToString & ":" & Prop.ToString)
        End Try


    End Sub

    Private Sub UpdateTable10()
        Dim row As DataRow
        Dim ProcessID As String

        Dim dvm As New DataView(dsProcess.Tables("MaintRout"))


        Dim mt As New DataTable()
        Try
            mt = dvm.ToTable(False, "UnitID", "ProcessID", "RegNum", "RegDown", "MaintNum", "MaintDown", "OthNum", "OthDown", "RoutCostLocal", "OthDownEconomic", "OthDownExternal", "OthDownUnitUpsets", "OthDownOffsiteUpsets", "RoutExpLocal", "RoutCptlLocal", "RoutOvhdLocal", "SortKey", "UnitName")
            Dim dv As New DataView(mt)
            Dim dv_lu As New DataView(MyParent.ds_Ref.Tables("ProcessID_LU"))
            Dim dvT10Map1 As New DataView(dsMappings.Tables("MaintRout"))
            Dim mt2 As New DataTable()
            mt2 = dvT10Map1.ToTable(False, "UnitID", "ProcessID", "RegNum", "RegDown", "MaintNum", "MaintDown", "OthNum", "OthDown", "RoutCostLocal", "OthDownEconomic", "OthDownExternal", "OthDownUnitUpsets", "OthDownOffsiteUpsets", "RoutExpLocal", "RoutCptlLocal", "RoutOvhdLocal", "SortKey", "UnitName")
            Dim dvT10MapAs = New DataView(mt2)

            Dim dvT10Map As New DataView(dsMappings.Tables("MaintRout"))

            For Each row In dsProcess.Tables(0).Rows
                dv.RowFilter = "UnitID = " & row!UnitID.ToString
                ProcessID = row!ProcessID.ToString
                dv_lu.RowFilter = "ProcessID = '" & ProcessID & "'"

                If dv_lu.Item(0)!MaintDetails.ToString = "Y" Then
                    If dv.Count = 0 Then
                        Dim newrow As DataRow = dsProcess.Tables("MaintRout").NewRow
                        newrow!UnitID = row!UnitID
                        newrow!ProcessID = ProcessID
                        newrow!SortKey = row!SortKey
                        newrow!UnitName = row!UnitName
                        dsProcess.Tables("MaintRout").Rows.Add(newrow)
                    Else
                        dv.Item(0)!UnitID = row!UnitID
                        dv.Item(0)!ProcessID = ProcessID
                        dv.Item(0)!SortKey = row!SortKey
                        dv.Item(0)!UnitName = row!UnitName
                    End If
                End If

                dvT10Map.RowFilter = "UnitID = " & row!UnitID.ToString

                If dv_lu.Item(0)!MaintDetails.ToString = "Y" Then
                    If dvT10Map.Count = 0 Then
                        Dim newrow As DataRow = dsMappings.Tables("MaintRout").NewRow
                        newrow!UnitID = row!UnitID
                        newrow!ProcessID = ProcessID
                        newrow!SortKey = row!SortKey
                        newrow!UnitName = row!UnitName
                        dsMappings.Tables("MaintRout").Rows.Add(newrow)
                    Else
                        dvT10Map.Item(0)!UnitID = row!UnitID
                        dvT10Map.Item(0)!ProcessID = ProcessID
                        dvT10Map.Item(0)!SortKey = row!SortKey
                        dvT10Map.Item(0)!UnitName = row!UnitName
                    End If
                End If
            Next row

            AddOffsitesAndUtilities(dsProcess.Tables("MaintRout"))
            dsProcess.Tables("MaintRout").AcceptChanges()
        Catch Ex As Exception
            ErrorHandler.Display("Error", Ex, "Main - UpdateTable10")
        End Try

    End Sub

    Private Sub AddOffsitesAndUtilities(ByVal dt As DataTable)
        Dim row As DataRow
        Dim i, j As Integer
        Dim UnitName As String = ""
        Dim ProcessID As String = ""
        Dim SortKey As Integer
        For i = 90051 To 90058
            Select Case i
                Case 90051
                    ProcessID = "OFFTANK"
                    UnitName = "Offsite - Tankage"
                    SortKey = 6000
                Case 90052
                    ProcessID = "OFFBLEND"
                    UnitName = "Offsite - Blending/Oil Movements"
                    SortKey = 6005
                Case 90053
                    ProcessID = "OFFCOKE"
                    UnitName = "Offsite - Coke Handling/Loading"
                    SortKey = 6010
                Case 90054
                    ProcessID = "OFFOTH"
                    UnitName = "Offsite - Other"
                    SortKey = 6015
                Case 90055
                    ProcessID = "MARINE"
                    UnitName = "All Marine"
                    SortKey = 6100
                Case 90056
                    ProcessID = "UTILSTM"
                    UnitName = "Utilities - Steam/Electrical"
                    SortKey = 6200
                Case 90057
                    ProcessID = "UTILENV"
                    UnitName = "Utilities - Environmental"
                    SortKey = 6205
                Case 90058
                    ProcessID = "UTILOTH"
                    UnitName = "Utilities - Other"
                    SortKey = 6210
            End Select

            Dim dv As New DataView(dt)
            dv.RowFilter = "UnitID = " & i
            If dv.Count = 0 Then
                row = dt.NewRow
                If dt.TableName = "MaintTA" Then row!TAID = 0
                row!UnitID = i
                row!ProcessID = ProcessID
                row!UnitName = UnitName
                row!SortKey = SortKey
                dt.Rows.Add(row)
            Else
                For j = 0 To dv.Count - 1
                    With dv.Item(j)
                        !ProcessID = ProcessID
                        !UnitName = UnitName
                        !SortKey = SortKey
                    End With
                Next
            End If
        Next
    End Sub

#End Region

#Region " Operating Expenses "
    Public Sub SaveOpex()
        DisablingWatchers()
        ' SA_OperatingExpenses1.tblFriendOpEx.AcceptChanges()

        Dim dv As DataView = Me.SA_OperatingExpenses1.dgvOpEx.DataSource
        Dim dgv As DataGridView = Me.SA_OperatingExpenses1.dgvOpEx

        Dim culture As New ProfileEnvironment.RegionalNumeric()

        Dim idxID As Integer
        Dim idxValue As Integer
        For Each c As DataGridViewColumn In dgv.Columns
            If (c.DataPropertyName.ToString = "Opex_ID") Then idxID = c.Index
            If (c.DataPropertyName.ToString = "costsRefinery") Then idxValue = c.Index ' this field is created in pivotToDataTableOpex

        Next c


        'Create new data table to save to the xml file and load the data into it.
        'This is done because if the column data type is a Single data type it will not save correctly
        Dim tempDataSet As New DataSet()
        Dim tempTable As New DataTable()
        tempTable.TableName = "OpExAll"
        For Each column As System.Data.DataColumn In dsOperatingExpenses.Tables("OpExAll").Columns

            Dim newCol As New DataColumn()

            'If column.DataType Is GetType(Single) Then
            '    newCol.DataType = GetType(String)
            'Else
            '    newCol.DataType = column.DataType
            'End If

            newCol.DataType = GetType(Single)

            newCol.ColumnName = column.ColumnName
            tempTable.Columns.Add(newCol)

        Next column
        tempTable.NewRow()

        Dim newRow As DataRow = tempTable.NewRow
        tempTable.Rows.Add(newRow)


        tempDataSet.Tables.Add(tempTable)

        For Each r As DataRow In dv.Table.Rows
            If dsOperatingExpenses.Tables("OpExAll").Columns.Contains(r.Item(idxID).ToString()) Then
                If r.Item(idxValue).ToString() <> String.Empty Then

                    'Dim value As String = Convert.ToString(culture.StringToFloatFormat(r.Item(idxValue)))
                    Dim value As Single = r.Item(idxValue)

                    dsOperatingExpenses.Tables("OpExAll").Rows(0).Item(r.Item(idxID)) = value
                    tempDataSet.Tables("OpExAll").Rows(0).Item(r.Item(idxID)) = value

                End If
            End If
        Next r

        tempDataSet.AcceptChanges()
        tempDataSet.WriteXml(MonthPath & "OpEx.xml", XmlWriteMode.WriteSchema)

        SaveIsChecked()
        SaveMade(SA_OperatingExpenses1.btnSave, SA_OperatingExpenses1.OpexNotSaved)
        EnablingWatchers()
        SaveMappings()

    End Sub

    Private Sub LoadOperatingExpenses()

        Dim dvOpEx_LU As New DataView
        dvOpEx_LU.Table = MyParent.ds_Ref.Tables("OpEx_LU")

        Try
            dsMappings.Tables("OpEx").Columns.Add("Description", System.Type.GetType("System.String"))
            dsMappings.Tables("OpEx").Columns.Add("SortKey", System.Type.GetType("System.Int16"))
            dsMappings.Tables("OpEx").Columns.Add("Indent", System.Type.GetType("System.Int16"))
            dsMappings.Tables("OpEx").Columns.Add("ParentID", System.Type.GetType("System.String"))
            dsMappings.Tables("OpEx").Columns.Add("detailStudy", System.Type.GetType("System.String"))
            dsMappings.Tables("OpEx").Columns.Add("detailProfile", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        ' Add rows to OpEx Data if they do not exist.
        Dim drOpExLinks() As DataRow

        For Each r As DataRow In dvOpEx_LU.Table.Rows
            drOpExLinks = dsMappings.Tables("OpEx").Select("OpexID = '" & r.Item("OpexID").ToString() & "'")

            If (drOpExLinks.GetUpperBound(0) = -1) Then
                Dim newDRLinks As DataRow = dsMappings.Tables("OpEx").NewRow
                newDRLinks("OpexID") = r.Item("OpexID")
                newDRLinks("Description") = r.Item("Description")
                newDRLinks("SortKey") = r.Item("SortKey")
                newDRLinks("Indent") = r.Item("Indent")
                newDRLinks("ParentID") = r.Item("ParentID")
                newDRLinks("detailStudy") = r.Item("detailStudy")
                newDRLinks("detailProfile") = r.Item("detailProfile")
                dsMappings.Tables("OpEx").Rows.Add(newDRLinks)
            Else
                drOpExLinks(0).Item("Description") = r.Item("Description")
                drOpExLinks(0).Item("SortKey") = r.Item("SortKey")
                drOpExLinks(0).Item("Indent") = r.Item("Indent")
                drOpExLinks(0).Item("ParentID") = r.Item("ParentID")
                drOpExLinks(0).Item("detailStudy") = r.Item("detailStudy")
                drOpExLinks(0).Item("detailProfile") = r.Item("detailProfile")
            End If

        Next r

        SA_OperatingExpenses1.SetDataSet()
        If MyParent.OpexRights = False Then NoRights(SA_OperatingExpenses1.pnlHeader, SA_OperatingExpenses1.gbTools)

    End Sub
#End Region

#Region " Personnel "
    Public Sub SavePersonnel()
        DisablingWatchers()
        SaveIsChecked()
        dsPersonnel.AcceptChanges()
        dsPersonnel.WriteXml(MonthPath & "Personnel.xml", XmlWriteMode.WriteSchema)

        SaveMade(SA_Personnel1.btnSave, SA_Personnel1.PersonnelNotSaved)
        EnablingWatchers()

        SaveMappings()
    End Sub

    Friend Sub LoadPersonnel()
        Dim dvPers As New DataView
        Dim dvAbs As New DataView
        'Dim dr As DataRow

        dvPers.Table = MyParent.ds_Ref.Tables("Pers_LU")
        dvAbs.Table = MyParent.ds_Ref.Tables("Absence_LU")

        Try
            dsPersonnel.Tables("Pers").Columns.Add("Description", System.Type.GetType("System.String"))
            dsPersonnel.Tables("Pers").Columns.Add("SortKey", System.Type.GetType("System.Int16"))
            dsPersonnel.Tables("Pers").Columns.Add("Indent", System.Type.GetType("System.Int16"))
            dsPersonnel.Tables("Pers").Columns.Add("ParentID", System.Type.GetType("System.String"))
            dsPersonnel.Tables("Pers").Columns.Add("detailStudy", System.Type.GetType("System.String"))
            dsPersonnel.Tables("Pers").Columns.Add("detailProfile", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        Try
            dsMappings.Tables("Personnel").Columns.Add("Description", System.Type.GetType("System.String"))
            dsMappings.Tables("Personnel").Columns.Add("SortKey", System.Type.GetType("System.Int16"))
            dsMappings.Tables("Personnel").Columns.Add("Indent", System.Type.GetType("System.Int16"))
            dsMappings.Tables("Personnel").Columns.Add("ParentID", System.Type.GetType("System.String"))
            dsMappings.Tables("Personnel").Columns.Add("detailStudy", System.Type.GetType("System.String"))
            dsMappings.Tables("Personnel").Columns.Add("detailProfile", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        Try
            dsPersonnel.Tables("Absence").Columns.Add("Description", System.Type.GetType("System.String"))
            dsPersonnel.Tables("Absence").Columns.Add("SortKey", System.Type.GetType("System.Int16"))
            dsPersonnel.Tables("Absence").Columns.Add("Indent", System.Type.GetType("System.Int16"))
            dsPersonnel.Tables("Absence").Columns.Add("ParentID", System.Type.GetType("System.String"))
            dsPersonnel.Tables("Absence").Columns.Add("detailStudy", System.Type.GetType("System.String"))
            dsPersonnel.Tables("Absence").Columns.Add("detailProfile", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        Try
            dsMappings.Tables("Absence").Columns.Add("Description", System.Type.GetType("System.String"))
            dsMappings.Tables("Absence").Columns.Add("SortKey", System.Type.GetType("System.Int16"))
            dsMappings.Tables("Absence").Columns.Add("Indent", System.Type.GetType("System.Int16"))
            dsMappings.Tables("Absence").Columns.Add("ParentID", System.Type.GetType("System.String"))
            dsMappings.Tables("Absence").Columns.Add("detailStudy", System.Type.GetType("System.String"))
            dsMappings.Tables("Absence").Columns.Add("detailProfile", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        ' Add rows to Personnel Data they do not exist.
        Dim drPersonnel() As DataRow
        Dim drPersonnelLinks() As DataRow

        For Each r As DataRow In dvPers.Table.Rows

            drPersonnel = dsPersonnel.Tables("Pers").Select("PersID = '" & r.Item("PersID").ToString & "'")

            If (drPersonnel.GetUpperBound(0) = -1) Then
                Dim newDR As DataRow = dsPersonnel.Tables("Pers").NewRow
                newDR("PersID") = r.Item("PersID")
                newDR("Description") = r.Item("Description")
                newDR("SortKey") = r.Item("SortKey")
                newDR("Indent") = r.Item("Indent")
                newDR("ParentID") = r.Item("ParentID")
                newDR("detailStudy") = r.Item("detailStudy")
                newDR("detailProfile") = r.Item("detailProfile")
                dsPersonnel.Tables("Pers").Rows.Add(newDR)
            Else
                'dr = dsPersonnel.Tables("Pers").Rows.Find(r.Item("PersID").ToString())
                drPersonnel(0).Item("Description") = r.Item("Description")
                drPersonnel(0).Item("SortKey") = r.Item("SortKey")
                drPersonnel(0).Item("Indent") = r.Item("Indent")
                drPersonnel(0).Item("ParentID") = r.Item("ParentID")
                drPersonnel(0).Item("detailStudy") = r.Item("detailStudy")
                drPersonnel(0).Item("detailProfile") = r.Item("detailProfile")
            End If

            drPersonnelLinks = dsMappings.Tables("Personnel").Select("PersID = '" & r.Item("PersID").ToString & "'")

            If (drPersonnelLinks.GetUpperBound(0) = -1) Then
                Dim newDRLinks As DataRow = dsMappings.Tables("Personnel").NewRow
                newDRLinks("PersID") = r.Item("PersID")
                newDRLinks("Description") = r.Item("Description")
                newDRLinks("SortKey") = r.Item("SortKey")
                newDRLinks("Indent") = r.Item("Indent")
                newDRLinks("ParentID") = r.Item("ParentID")
                newDRLinks("detailStudy") = r.Item("detailStudy")
                newDRLinks("detailProfile") = r.Item("detailProfile")
                dsMappings.Tables("Personnel").Rows.Add(newDRLinks)
            Else
                drPersonnelLinks(0).Item("Description") = r.Item("Description")
                drPersonnelLinks(0).Item("SortKey") = r.Item("SortKey")
                drPersonnelLinks(0).Item("Indent") = r.Item("Indent")
                drPersonnelLinks(0).Item("ParentID") = r.Item("ParentID")
                drPersonnelLinks(0).Item("detailStudy") = r.Item("detailStudy")
                drPersonnelLinks(0).Item("detailProfile") = r.Item("detailProfile")
            End If

        Next r

        ' Add rows to Absence Data they do not exist.
        Dim drAbsences() As DataRow
        Dim drAbsencesLinks() As DataRow

        For Each r As DataRow In dvAbs.Table.Rows

            drAbsences = dsPersonnel.Tables("Absence").Select("CategoryID = '" & r.Item("CategoryID").ToString & "'")

            If (drAbsences.GetUpperBound(0) = -1) Then
                Dim newDR As DataRow = dsPersonnel.Tables("Absence").NewRow
                newDR("CategoryID") = r.Item("CategoryID")
                newDR("Description") = r.Item("Description")
                newDR("SortKey") = r.Item("SortKey")
                newDR("Indent") = r.Item("Indent")
                newDR("ParentID") = r.Item("ParentID")
                newDR("detailStudy") = r.Item("detailStudy")
                newDR("detailProfile") = r.Item("detailProfile")
                dsPersonnel.Tables("Absence").Rows.Add(newDR)
            Else
                drAbsences(0).Item("Description") = r.Item("Description")
                drAbsences(0).Item("SortKey") = r.Item("SortKey")
                drAbsences(0).Item("Indent") = r.Item("Indent")
                drAbsences(0).Item("ParentID") = r.Item("ParentID")
                drAbsences(0).Item("detailStudy") = r.Item("detailStudy")
                drAbsences(0).Item("detailProfile") = r.Item("detailProfile")
            End If

            drAbsencesLinks = dsMappings.Tables("Absence").Select("CategoryID = '" & r.Item("CategoryID").ToString & "'")

            If (drAbsencesLinks.GetUpperBound(0) = -1) Then
                Dim newDRLinks As DataRow = dsMappings.Tables("Absence").NewRow
                newDRLinks("CategoryID") = r.Item("CategoryID")
                newDRLinks("Description") = r.Item("Description")
                newDRLinks("SortKey") = r.Item("SortKey")
                newDRLinks("Indent") = r.Item("Indent")
                newDRLinks("ParentID") = r.Item("ParentID")
                newDRLinks("detailStudy") = r.Item("detailStudy")
                newDRLinks("detailProfile") = r.Item("detailProfile")
                dsMappings.Tables("Absence").Rows.Add(newDRLinks)
            Else
                drAbsencesLinks(0).Item("Description") = r.Item("Description")
                drAbsencesLinks(0).Item("SortKey") = r.Item("SortKey")
                drAbsencesLinks(0).Item("Indent") = r.Item("Indent")
                drAbsencesLinks(0).Item("ParentID") = r.Item("ParentID")
                drAbsencesLinks(0).Item("detailStudy") = r.Item("detailStudy")
                drAbsencesLinks(0).Item("detailProfile") = r.Item("detailProfile")
            End If

        Next r

        SA_Personnel1.SetDataSet()
        If MyParent.PersRights = False Then NoRights(SA_Personnel1.pnlHeader, SA_Personnel1.gbTools)

        'Exit Sub
        'PersonnelError:
        '        MsgBox("Error loading personnel for " & Me.Text & ".  Please exit Profile II and upon re-entry, refresh your local months through the administrative tools.", MsgBoxStyle.Critical, "Profile II")
        '        LoadingReferenceData("Ready", False)
    End Sub

#End Region

#Region " Crude Charge Detail "
    Private Sub LoadCrudes()
        On Error GoTo CrudeErrors

        verifyListMapping(dsMappings.Tables("Crude"))

        SA_CrudeCharge1.SetDataSet()
        If MyParent.CrudeRights = False Then NoRights(SA_CrudeCharge1.pnlHeader, SA_CrudeCharge1.gbTools)
        Exit Sub
CrudeErrors:
        ProfileMsgBox("CRIT", "LOADERR", "Crude Charge Detail")
        LoadingReferenceData("Ready", False)
    End Sub
    Public Sub SaveCrude()
        DisablingWatchers()
        SaveIsChecked()
        dsCrude.AcceptChanges()
        dsCrude.WriteXml(MonthPath & "Crude.xml", XmlWriteMode.WriteSchema)

        Dim ds As New DataSet
        Dim row As DataRow
        ds = dsCrude.Copy
        For Each row In ds.Tables("Crude").Rows
            row!BBL = 0
            row!CostPerBBL = 0
        Next

        ds.WriteXml(pathConfig & "Crude.xml", XmlWriteMode.WriteSchema)
        SaveMade(SA_CrudeCharge1.btnSave, SA_CrudeCharge1.CrudeNotSaved)
        EnablingWatchers()
        SaveMappings()
    End Sub
#End Region

#Region " User Defined Data"
    Private Sub LoadUserDefined()
        On Error GoTo UserDefinedErrors
        verifyListMapping(dsMappings.Tables("UserDefined"))

        SA_UserDefined1.SetDataSet()
        If MyParent.UserDefinedRights = False Then NoRights(SA_UserDefined1.pnlHeader, SA_UserDefined1.gbTools)
        Exit Sub
UserDefinedErrors:
        ProfileMsgBox("CRIT", "LOADERR", "User-Defined Inputs/Outputs")
        LoadingReferenceData("Ready", False)
    End Sub

    Public Sub SaveUserDefined()
        DisablingWatchers()
        SaveIsChecked()
        dsUserDefined.AcceptChanges()
        Dim ds As New DataSet
        Dim row As DataRow
        ds = dsUserDefined.Copy
        For Each row In ds.Tables("UserDefined").Rows
            row!RptValue = DBNull.Value
            row!RptValue_AVG = DBNull.Value
            row!RptValue_YTD = DBNull.Value
        Next
        ds.WriteXml(pathConfig & "UserDefined.xml", XmlWriteMode.WriteSchema)
        dsUserDefined.WriteXml(MonthPath & "UserDefined.xml", XmlWriteMode.WriteSchema)
        SaveMade(SA_UserDefined1.btnSave, SA_UserDefined1.UserDefinedNotSaved)
        EnablingWatchers()
        SaveMappings()
    End Sub

#End Region

#Region " Inventory "
    Public Sub SaveInventory()
        DisablingWatchers()
        SaveIsChecked()
        dsRefineryInformation.AcceptChanges()
        dsRefineryInformation.WriteXml(MonthPath & "RefineryInformation.xml", XmlWriteMode.WriteSchema)

        Dim ds As New DataSet
        Dim row As DataRow
        ds = dsRefineryInformation.Copy
        For Each row In ds.Tables("Inventory").Rows
            row!AvgLevel = DBNull.Value
        Next

        For Each row In ds.Tables("ConfigRS").Rows
            row!RailCarBBL = DBNull.Value
            row!TankTruckBBL = DBNull.Value
            row!TankerBerthBBL = DBNull.Value
            row!OffShoreBuoyBBL = DBNull.Value
            row!BargeBerthBBL = DBNull.Value
            row!PipelineBBL = DBNull.Value
        Next

        ds.WriteXml(pathConfig & "RefineryInformation.xml", XmlWriteMode.WriteSchema)
        SaveMade(SA_Inventory1.btnSave, SA_Inventory1.InventoryNotSaved)

        EnablingWatchers()
        SaveMappings()
    End Sub

    Private Sub LoadInventory()

        Dim drLinks() As DataRow

        Try
            dsRefineryInformation.Tables("Inventory").Columns.Add("Description")
            dsRefineryInformation.Tables("ConfigRS").Columns.Add("Description")
        Catch ex As Exception
        End Try

        For Each r As DataRow In dsRefineryInformation.Tables("Inventory").Rows

            drLinks = dsMappings.Tables("Inventory").Select("TankType = '" & r.Item("TankType").ToString & "'")

            If (drLinks.GetUpperBound(0) = -1) Then
                Dim newDRLinks As DataRow = dsMappings.Tables("Inventory").NewRow
                newDRLinks("Description") = r.Item("Description")
                newDRLinks("TankType") = r.Item("TankType")
                dsMappings.Tables("Inventory").Rows.Add(newDRLinks)
            End If

            Select Case Trim(r!TankType.ToString)
                Case "CRD" : r!Description = "Crude Oil & Condensate"
                Case "FIN" : r!Description = "Finished Products"
                Case "INT" : r!Description = "Intermediate Stocks, Unfinished Oils & Blendstocks"
            End Select
        Next r

        For Each r As DataRow In dsRefineryInformation.Tables("ConfigRS").Rows

            drLinks = dsMappings.Tables("ConfigRS").Select("ProcessID = '" & r.Item("ProcessID").ToString & "'")

            If (drLinks.GetUpperBound(0) = -1) Then
                Dim newDRLinks As DataRow = dsMappings.Tables("ConfigRS").NewRow
                newDRLinks("Description") = r.Item("Description")
                newDRLinks("ProcessID") = r.Item("ProcessID")
                dsMappings.Tables("ConfigRS").Rows.Add(newDRLinks)
            End If

            Select Case Trim(r!ProcessID.ToString)
                Case "RSCRUDE" : r!Description = "Raw Material Receipts"
                Case "RSPROD" : r!Description = "Product Shipments"
            End Select

        Next r

        SA_Inventory1.SetDataSets()
        If MyParent.InvenRights = False Then NoRights(SA_Inventory1.pnlHeader, SA_Inventory1.gbTools)
    End Sub
#End Region

#Region " Material Balance "
    Public Sub SaveMaterialBalance()
        DisablingWatchers()
        SaveIsChecked()
        dsYield.AcceptChanges()
        dsYield.WriteXml(MonthPath & "Yield.xml", XmlWriteMode.WriteSchema)
        Dim ds As New DataSet
        Dim row As DataRow
        ds = dsYield.Copy
        For Each row In ds.Tables(0).Rows
            row!BBL = DBNull.Value
            row!PriceLocal = DBNull.Value
        Next

        For Each row In ds.Tables(1).Rows
            row!BBL = DBNull.Value
            row!PriceLocal = DBNull.Value
        Next

        ds.WriteXml(pathConfig & "Yield.xml", XmlWriteMode.WriteSchema)
        SaveMade(SA_MaterialBalance1.btnSave, SA_MaterialBalance1.MaterialNotSaved)

        EnablingWatchers()
        SaveMappings()
    End Sub

    Private Sub LoadMaterialBalance()
        On Error GoTo MaterialErrors

        verifyListMapping(dsMappings.Tables("Yield_RM"))
        verifyListMapping(dsMappings.Tables("Yield_RMB"))
        verifyListMapping(dsMappings.Tables("Yield_Prod"))

        SA_MaterialBalance1.SetDataSet()
        If MyParent.MatlRights = False Then NoRights(SA_MaterialBalance1.pnlHeader, SA_MaterialBalance1.gbTools)
        SA_MaterialBalance1.SetCurrencyAndUOM(CurrencyCode)
        Exit Sub
MaterialErrors:
        ProfileMsgBox("CRIT", "LOADERR", "Material Balance")
        LoadingReferenceData("Ready", False)
    End Sub
#End Region

#Region " Energy "
    Public Sub SaveEnergy()
        DisablingWatchers()
        SaveIsChecked()
        dsEnergy.AcceptChanges()
        dsEnergy.WriteXml(MonthPath & "Energy.xml", XmlWriteMode.WriteSchema)

        Dim ds As New DataSet
        ds = dsEnergy.Copy

        For Each row As DataRow In ds.Tables("Energy").Rows
            row!RptSource = 0
            row!RptPriceLocal = 0
        Next row

        For Each row As DataRow In ds.Tables("Electric").Rows
            row!RptMWH = 0
            row!PriceLocal = 0
            row!RptGenEff = 0
        Next row

        ds.AcceptChanges()
        ds.WriteXml(pathConfig & "Energy.xml", XmlWriteMode.WriteSchema)

        SaveMade(SA_Energy1.btnSave, SA_Energy1.EnergyNotSaved)

        EnablingWatchers()
        SaveMappings()
    End Sub

    Private Sub LoadEnergy()

        'On Error GoTo EnergyErrors

        Dim dvEnergy_LU As New DataView
        dvEnergy_LU.Table = MyParent.ds_Ref.Tables("Energy_LU")

        Try
            dsEnergy.Tables("Energy").Columns.Add("Header", System.Type.GetType("System.String"))
            dsEnergy.Tables("Energy").Columns.Add("TransTypeDesc", System.Type.GetType("System.String"))
            dsEnergy.Tables("Energy").Columns.Add("EnergyTypeDesc", System.Type.GetType("System.String"))
            dsEnergy.Tables("Energy").Columns.Add("Composition", System.Type.GetType("System.Int16"))
            dsEnergy.Tables("Energy").Columns.Add("Total", System.Type.GetType("System.Double"))
        Catch ex As Exception
        End Try

        Try
            dsMappings.Tables("Energy").Columns.Add("Header", System.Type.GetType("System.String"))
            dsMappings.Tables("Energy").Columns.Add("TransTypeDesc", System.Type.GetType("System.String"))
            dsMappings.Tables("Energy").Columns.Add("EnergyTypeDesc", System.Type.GetType("System.String"))
            dsMappings.Tables("Energy").Columns.Add("Composition", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        Try
            dsEnergy.Tables("Electric").Columns.Add("Header", System.Type.GetType("System.String"))
            dsEnergy.Tables("Electric").Columns.Add("TransTypeDesc", System.Type.GetType("System.String"))
            dsEnergy.Tables("Electric").Columns.Add("EnergyTypeDesc", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        Try
            dsMappings.Tables("Electric").Columns.Add("Header", System.Type.GetType("System.String"))
            dsMappings.Tables("Electric").Columns.Add("TransTypeDesc", System.Type.GetType("System.String"))
            dsMappings.Tables("Electric").Columns.Add("EnergyTypeDesc", System.Type.GetType("System.String"))
        Catch ex As Exception
        End Try

        Dim drThermal() As DataRow
        Dim drElec() As DataRow

        Dim drThermalLinks() As DataRow
        Dim drElecLinks() As DataRow

        For Each r As DataRow In dvEnergy_LU.Table.Rows

            If CInt(r.Item("SortKey")) < 100 Then

                drThermal = dsEnergy.Tables("Energy").Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")

                If (drThermal.GetUpperBound(0) >= 0) Then
                    drThermal(0).Item("TransTypeDesc") = r.Item("TransTypeDesc")
                    drThermal(0).Item("EnergyTypeDesc") = r.Item("EnergyTypeDesc")
                    drThermal(0).Item("Composition") = r.Item("Composition")
                    drThermal(0).Item("SortKey") = r.Item("SortKey")
                End If

                drThermalLinks = dsMappings.Tables("Energy").Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")

                If (drThermalLinks.GetUpperBound(0) = -1) Then
                    Dim newDRLinks As DataRow = dsMappings.Tables("Energy").NewRow
                    newDRLinks("Header") = r.Item("Header")
                    newDRLinks("TransType") = r.Item("TransType")
                    newDRLinks("TransTypeDesc") = r.Item("TransTypeDesc")
                    newDRLinks("TransferTo") = r.Item("TransferTo")
                    newDRLinks("EnergyType") = r.Item("EnergyType")
                    newDRLinks("EnergyTypeDesc") = r.Item("EnergyTypeDesc")
                    newDRLinks("SortKey") = r.Item("SortKey")
                    newDRLinks("Composition") = r.Item("Composition")
                    dsMappings.Tables("Energy").Rows.Add(newDRLinks)
                Else
                    drThermalLinks(0).Item("Header") = r.Item("Header")
                    drThermalLinks(0).Item("TransTypeDesc") = r.Item("TransTypeDesc")
                    drThermalLinks(0).Item("EnergyTypeDesc") = r.Item("EnergyTypeDesc")
                    drThermalLinks(0).Item("Composition") = r.Item("Composition")
                    drThermalLinks(0).Item("SortKey") = r.Item("SortKey")
                End If

            End If

            If CInt(r.Item("SortKey")) > 100 Then

                drElec = dsEnergy.Tables("Electric").Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")

                If (drElec.GetUpperBound(0) >= 0) Then
                    drElec(0).Item("TransTypeDesc") = r.Item("TransTypeDesc")
                    drElec(0).Item("EnergyTypeDesc") = r.Item("EnergyTypeDesc")
                    drElec(0).Item("SortKey") = r.Item("SortKey")
                End If

                drElecLinks = dsMappings.Tables("Electric").Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")

                If (drElecLinks.GetUpperBound(0) = -1) Then
                    Dim newDRLinks As DataRow = dsMappings.Tables("Electric").NewRow
                    newDRLinks("Header") = r.Item("Header")
                    newDRLinks("TransType") = r.Item("TransType")
                    newDRLinks("TransTypeDesc") = r.Item("TransTypeDesc")
                    newDRLinks("TransferTo") = r.Item("TransferTo")
                    newDRLinks("EnergyType") = r.Item("EnergyType")
                    newDRLinks("EnergyTypeDesc") = r.Item("EnergyTypeDesc")
                    newDRLinks("SortKey") = r.Item("SortKey")
                    dsMappings.Tables("Electric").Rows.Add(newDRLinks)
                Else
                    drElecLinks(0).Item("Header") = r.Item("Header")
                    drElecLinks(0).Item("TransTypeDesc") = r.Item("TransTypeDesc")
                    drElecLinks(0).Item("EnergyTypeDesc") = r.Item("EnergyTypeDesc")
                    drElecLinks(0).Item("SortKey") = r.Item("SortKey")
                End If

            End If

        Next r



        Dim dr() As DataRow

        For Each r As DataRow In dsEnergy.Tables("Energy").Rows
            dr = dvEnergy_LU.Table.Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")
            If (dr.GetUpperBound(0) >= 0) Then
                r.Item("TransTypeDesc") = dr(0).Item("TransTypeDesc").ToString
                r.Item("EnergyTypeDesc") = dr(0).Item("EnergyTypeDesc").ToString '& " (" & r.Item("TransCode").ToString & ")"
                r.Item("Composition") = dr(0).Item("Composition").ToString
                r.Item("SortKey") = dr(0).Item("SortKey").ToString
            End If
        Next r

        For Each r As DataRow In dsEnergy.Tables("Electric").Rows
            dr = dvEnergy_LU.Table.Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")
            If (dr.GetUpperBound(0) >= 0) Then
                r.Item("TransTypeDesc") = dr(0).Item("TransTypeDesc").ToString
                r.Item("EnergyTypeDesc") = dr(0).Item("EnergyTypeDesc").ToString '& " (" & r.Item("TransCode").ToString & ")"
                r.Item("SortKey") = CInt(dr(0).Item("SortKey").ToString)
            End If
        Next r

        For Each r As DataRow In dsMappings.Tables("Energy").Rows
            dr = dvEnergy_LU.Table.Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")
            If (dr.GetUpperBound(0) >= 0) Then
                r.Item("TransTypeDesc") = dr(0).Item("TransTypeDesc").ToString
                r.Item("EnergyTypeDesc") = dr(0).Item("EnergyTypeDesc").ToString
                r.Item("Composition") = dr(0).Item("Composition").ToString
                r.Item("SortKey") = dr(0).Item("SortKey").ToString

                r.Item("Header") = dr(0).Item("Header").ToString
                r.Item("TransType") = dr(0).Item("TransType").ToString
                r.Item("TransferTo") = dr(0).Item("TransferTo").ToString
                r.Item("EnergyType") = dr(0).Item("EnergyType").ToString
            End If
        Next r

        For Each r As DataRow In dsMappings.Tables("Electric").Rows
            dr = dvEnergy_LU.Table.Select("TransType = '" & r.Item("TransType").ToString() & "' AND TransferTo = '" & r.Item("TransferTo").ToString() & "' AND EnergyType = '" & r.Item("EnergyType").ToString() & "'")
            If (dr.GetUpperBound(0) >= 0) Then
                r.Item("TransTypeDesc") = dr(0).Item("TransTypeDesc").ToString
                r.Item("EnergyTypeDesc") = dr(0).Item("EnergyTypeDesc").ToString
                r.Item("SortKey") = dr(0).Item("SortKey").ToString

                r.Item("Header") = dr(0).Item("Header").ToString
                r.Item("TransType") = dr(0).Item("TransType").ToString
                r.Item("TransferTo") = dr(0).Item("TransferTo").ToString
                r.Item("EnergyType") = dr(0).Item("EnergyType").ToString
            End If
        Next r

        SA_Energy1.SetDataSet()
        If MyParent.EnergyRights = False Then NoRights(SA_Energy1.pnlHeader, SA_Energy1.gbTools)
        SA_Energy1.SetCurrencyAndUOM(CurrencyCode, UOM)

    End Sub

#End Region

#Region " Mappings and Is Complete"
    Private Sub SaveIsChecked()
        dsIsChecked.AcceptChanges()
        dsIsChecked.WriteXml(MonthPath & "IsChecked.xml", XmlWriteMode.WriteSchema)
    End Sub

    Private Sub GetIsCheckedTable()
        Try
            dsIsChecked.ReadXml(MonthPath & "IsChecked.xml", XmlReadMode.ReadSchema)
        Catch ex As Exception
            Dim dt As New DataTable
            dt.Columns.Add("Description", GetType(String))
            dt.Columns.Add("Checked", GetType(Boolean))
            Dim row As DataRow
            For Each i As ListViewItem In lvDataEntry.Items
                row = dt.NewRow
                row!Description = i.Text
                row!Checked = False
                dt.Rows.Add(row)
            Next

            dsIsChecked.Tables.Add(dt)
            dsIsChecked.WriteXml(MonthPath & "IsChecked.xml", XmlWriteMode.WriteSchema)
        End Try

        UpdateIsChecked()
    End Sub

    Private Sub UpdateIsChecked()
        For Each i As ListViewItem In lvDataEntry.Items
            Dim Description As String = i.Text
            Dim row() As DataRow = dsIsChecked.Tables(0).Select("Description='" & Description & "'")
            If CBool(row(0)!Checked) Then
                i.SubItems.Add("Ready")
            Else
                i.SubItems.Add("")
            End If
        Next
        lvDataEntry.Refresh()
    End Sub

    Friend Sub SaveMappings()
        DisablingWatchers()
        dsMappings.AcceptChanges()

        '20090410 RRH dsMappings.WriteXml(pathStartUp & "Mappings.xml", XmlWriteMode.WriteSchema)
        XMLFile.WriteXMLFile(dsMappings, pathStartUp & "Mappings.xml")
        modUploadNotify.uploadNotify(Me.Label48, Me.Label49, False, dsSettings, MonthPath)

        EnablingWatchers()

    End Sub

    Public Sub DataEntryComplete(ByVal IsComplete As Boolean, ByVal tag As String)
        tcControlPanel.SelectedTab = tpDataEntry
        For Each i As ListViewItem In lvDataEntry.Items
            If i.Text = tag Then
                If IsComplete Then
                    i.SubItems(1).Text = "Ready"
                Else
                    i.SubItems(1).Text = ""
                End If
                Exit Sub
            End If
        Next
    End Sub

#End Region

#Region " Import Bridge and Upload Dataset "
    Public Sub CallUploadDataset()
        Dim applicationName As String = String.Empty
        Dim methodology As String = String.Empty
        Dim refID As String = String.Empty
        Dim callerIP As String = String.Empty
        Dim userid As String = String.Empty
        Dim computername As String = String.Empty
        Dim version As String = String.Empty
        Dim errorMessages As String = String.Empty
        Dim localDomainName As String = String.Empty
        Dim os_Version As String = String.Empty
        Dim officeVersion As String = String.Empty
        Dim service As String = String.Empty
        Try
            LoadingReferenceData("Preparing monthly data for upload...", True)
            HideWSCtrls()
            SaveAll()

            Dim ds As New DataSet
            ds.Tables.Add(dsSettings.Tables("Settings").Copy)
            ds.Tables.Add(dsRefineryInformation.Tables("EDCStabilizers").Copy)
            ds.Tables.Add(dsProcess.Tables("Config").Copy)
            ds.Tables.Add(dsRefineryInformation.Tables("ConfigRS").Copy)
            ds.Tables.Add(dsRefineryInformation.Tables("Inventory").Copy)
            ds.Tables.Add(dsProcess.Tables("ProcessData").Copy)
            ds.Tables.Add(dsOperatingExpenses.Tables("OpExAll").Copy)
            ds.Tables.Add(dsPersonnel.Tables("Pers").Copy)
            ds.Tables.Add(dsPersonnel.Tables("Absence").Copy)
            ds.Tables.Add(dsProcess.Tables("MaintTA").Copy)
            ds.Tables.Add(dsProcess.Tables("MaintRout").Copy)
            ds.Tables.Add(dsCrude.Tables("Crude").Copy)
            ds.Tables.Add(dsYield.Tables("Yield_RM").Copy)
            ds.Tables.Add(dsYield.Tables("Yield_RMB").Copy)
            ds.Tables.Add(dsYield.Tables("Yield_Prod").Copy)
            ds.Tables.Add(dsEnergy.Tables("Energy").Copy)
            ds.Tables.Add(dsEnergy.Tables("Electric").Copy)
            ds.Tables.Add(dsUserDefined.Tables("UserDefined").Copy)
            ds.Tables.Add(dsRefineryInformation.Tables("RefTargets").Copy)
            ds.Tables.Add(dsProcess.Tables("UnitTargetsNew").Copy)
            ds.Tables.Add(dsRefineryInformation.Tables("MaintRoutHist").Copy)

            'ds.Tables.Add(dsOperatingExpenses.Tables("OpexAdd").Copy)
            LoadingReferenceData("Uploading to Solomon...", True)
            'ds.WriteXml(MonthPath & "MonthlyUpload.xml", XmlWriteMode.WriteSchema)
            If MyParent.SubmitMonthlyData(ds) Then
                CreateEvent(Now(), "Uploaded '" & Format(PeriodStart, "y") & "' to Solomon")
                ProfileMsgBox("INFO", "UPYES", Me.Text)
            Else

                'Error message was already shown - by proxy(?)

                'MyParent.PostDialogBox("CONN", Nothing)
            End If
            LoadingReferenceData("Ready", False)




            frmMain.PrepActivityLogArguments(refID, callerIP, userid, computername, service)
            frmMain.PrepActivityLogArguments(applicationName, methodology, refID, callerIP,
            userid, computername, version, Nothing,
            errorMessages, localDomainName, os_Version, officeVersion)


            If Util.UseNonWCFWebService Then
                'al.WriteActivityLogExtended(applicationName, methodology, GetClientKey(), callerIP, userid, computername, service, "CallUploadDataset", "",
                '                            PeriodStart.ToString("mm-dd-yy"), "", "Success", version, String.Empty,
                '                            localDomainName, os_Version, String.Empty, officeVersion, String.Empty)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try

                    'Dim applicationName As String = String.Empty
                    'Dim Methodology As String = String.Empty
                    Dim localRefineryID As String = String.Empty
                    'Dim CallerIP As String = String.Empty
                    'Dim userid As String = String.Empty
                    'Dim computername As String = String.Empty
                    'Dim version As String = String.Empty
                    Dim err As Object = Nothing
                    'Dim errorMessages As String = String.Empty
                    'Dim localDomainName As String = String.Empty
                    Dim osVersion As String = String.Empty
                    'Dim officeVersion As String = String.Empty
                    frmMain.PrepActivityLogArguments(applicationName, methodology, localRefineryID, callerIP,
                    userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                    proxy.WriteActivityLogExtended(applicationName, methodology, callerIP,
                        userid, computername, String.Empty, "CallUploadDataset", String.Empty, PeriodStart.ToString("mm-dd-yy"), Nothing, "Success", version, Nothing,
                        localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                    'svc.WriteActivityLogExtended(applicationName, methodology, GetClientKey(), callerIP,
                    '    userid, computername, String.Empty, "CallUploadDataset", String.Empty, PeriodStart.ToString("mm-dd-yy"), Nothing, "Success", version, Nothing,
                    '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
                Catch
                Finally
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then
                        Try
                            proxy.Close()
                        Catch
                        End Try
                    End If
                End Try
            End If

        Catch ex As Exception
            MsgBox("An error occurred while uploading your data")

            If Util.UseNonWCFWebService Then
                'al.WriteActivityLogExtended(applicationName, methodology, GetClientKey(), callerIP, userid, computername, service, "CallUploadDataset", "",
                '                            PeriodStart.ToString("mm-dd-yy"), "", "ERROR", version, ex.Message,
                '                            localDomainName, os_Version, String.Empty, officeVersion, String.Empty)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try

                    'Dim applicationName As String = String.Empty
                    'Dim Methodology As String = String.Empty
                    Dim localRefineryID As String = String.Empty
                    'Dim CallerIP As String = String.Empty
                    'Dim userid As String = String.Empty
                    'Dim computername As String = String.Empty
                    'Dim version As String = String.Empty
                    Dim err As Object = Nothing
                    'Dim errorMessages As String = String.Empty
                    'Dim localDomainName As String = String.Empty
                    Dim osVersion As String = String.Empty
                    'Dim officeVersion As String = String.Empty
                    frmMain.PrepActivityLogArguments(applicationName, methodology, localRefineryID, callerIP,
                    userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                    proxy.WriteActivityLogExtended(applicationName, methodology, callerIP,
                    userid, computername, String.Empty, "CallUploadDataset", String.Empty, PeriodStart.ToString("mm-dd-yy"), Nothing, "ERROR", version, ex.Message,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                    'svc.WriteActivityLogExtended(applicationName, methodology, GetClientKey(), callerIP,
                    '    userid, computername, String.Empty, "CallUploadDataset", String.Empty, PeriodStart.ToString("mm-dd-yy"), Nothing, "ERROR", version, ex.Message,
                    '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
                Catch
                Finally
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                End Try
            End If
        End Try
    End Sub

    Private Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        CallUploadDataset()
        modUploadNotify.uploadNotify(Me.Label48, Me.Label49, True, dsSettings, MonthPath)
    End Sub
#End Region

#Region " Printing Calls "
    Public Sub SetPrintControl()
        If PrintControl Is Nothing Then Exit Sub
        If PrinterIsInstalled() = False Then Exit Sub
        Try
            SA_Browser1.PrintPage()
        Catch ex As Exception
            MsgBox("You must be viewing a report or chart before printing.", MsgBoxStyle.Critical, "No Report or Chart")
        End Try
    End Sub
#End Region

#Region " Charts and Reports "
    Private Sub btnChtCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChtCreate.Click
        ChtCreateRoutine()
    End Sub

    Private Sub ChtCreateRoutine()
        LoadingReferenceData("Creating Chart...", True)
        ' 20120110 RRH - Removing Certificate Validation
        'If MyParent.IsLocal = False Then
        '    If MyParent.ResultsRights Then
        '        SA_Browser1.InstallCertificate()
        '    End If
        'End If

        ' 20081215 RRH changed verification analysis
        If Not (Not treeCharts.SelectedNode Is Nothing AndAlso Not treeCharts.SelectedNode.Tag Is Nothing) Then
            ProfileMsgBox("CRIT", "RESSEL", "chart")
            LoadingReferenceData("Ready", False)
            Exit Sub
        End If

        Dim usingByProcessUnit As Boolean = treeCharts.SelectedNode.FullPath.StartsWith("By Process Unit\")


        If MyParent.IsLocal Then Exit Sub
        If MyParent.ResultsRights = False Then Exit Sub

        Dim row As DataRow = CType(treeCharts.SelectedNode.Tag, DataRow)
        Dim AvgField As String = ""
        Dim YTDField As String = ""
        Dim TargetField As String = ""

        If chkChtAVG.Checked Then
            Try
                AvgField = row!AvgField.ToString
            Catch ex As Exception
                AvgField = ""
            End Try
        End If

        If chkChtYTD.Checked Then
            Try
                YTDField = row!YTDField.ToString
            Catch ex As Exception
                YTDField = ""
            End Try
        End If

        If chkChtTarget.Checked Then
            Try
                TargetField = row!TargetField.ToString
            Catch ex As Exception
                TargetField = ""
            End Try
        End If


        Dim UOM As String = cmbChtUOM.Text
        If UOM = "US Units" Then UOM = "US" Else UOM = "MET"
        Dim StudyYear As String = CStr(cmbChtMeth.Text)
        Dim TmpCurrencyCode As String = cmbChtCurrencyCode.SelectedItem.ToString
        Dim PeriodStart As String = cmbChtStart.Text ' = cmbChtStart.SelectedValue
        Dim PeriodEnd As String = cmbChtEnd.Text  'cmbChtEnd.SelectedValue

        ' 20081215 RRH changed verification analysis
        If (PeriodStart = "" Or PeriodEnd = "") OrElse CDate(PeriodStart) > CDate(PeriodEnd) Then
            ProfileMsgBox("CRIT", "CHTDATE", Nothing)
            LoadingReferenceData("Ready", False)
            Exit Sub
        End If

        Dim location As String
        If txtLocation.Text = "" Then location = "ND" Else location = Trim(txtLocation.Text)

        Dim scenario As String
        If cbStudyPriceCht.Checked Then scenario = cmbChtMeth.Text.Trim() Else scenario = "CLIENT"

        Dim targetSeriesColor As String = cboChartTargetColor.Text
        If Not chkChtTarget.Checked Then
            targetSeriesColor = String.Empty
        End If
        Dim avgSeriesColor As String = cboChartAvgColor.Text
        If Not chkChtAVG.Checked Then
            avgSeriesColor = String.Empty
        End If
        Dim ytdSeriesColor As String = cboChartYtdColor.Text
        If Not chkChtYTD.Checked Then
            ytdSeriesColor = String.Empty
        End If
        SA_Browser1.DisplayRefnumChart(MyParent.RefineryID, location, "ACTUAL", _
                                         scenario, CDate(PeriodStart), CDate(PeriodEnd), _
                                         row, UOM, TmpCurrencyCode, StudyYear, TargetField, AvgField, YTDField, cboChartType.Text,
                                         cboChartColor.Text, avgSeriesColor, ytdSeriesColor, targetSeriesColor, chkShowChartDataTable.Checked)
        'al.SubmitActivityLog(cmbRptMeth.Text, "", "", UserName, Environment.MachineName.ToString(), "Main", "btnChtCreate", treeCharts.SelectedNode.ToString(), PeriodStart, PeriodEnd, "", "Success")

        If usingByProcessUnit Then
            SA_Browser1.btnAddToDashboard.Visible = False
        Else
            If modGeneral.AdminRights Then
                SA_Browser1.btnAddToDashboard.Visible = True
            End If
        End If

        Dim startDate As DateTime? = Nothing
        Dim endDate As DateTime? = Nothing
        Try
            startDate = DateTime.Parse(PeriodStart)
        Catch
        End Try
        Try
            endDate = DateTime.Parse(PeriodEnd)
        Catch
        End Try


        If Util.UseNonWCFWebService Then
            'al.SubmitActivityLog("btnChtCreate_Click", Nothing, startDate, endDate, "Success", Nothing, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            Try
                'proxy.

                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, "btnChtCreate_Click", Nothing, startDate, endDate, "Success", version, Nothing,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                '    userid, computername, String.Empty, "btnChtCreate_Click", String.Empty, startDate, endDate, "Success", version, Nothing,
                '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
            Catch ex As Exception
                Dim errMsg As String = ex.Message
            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() '
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If

        SA_Browser1.lblHeader.Text = "Charts of Results"
        SA_Browser1.Visible = True
        SA_Browser1.BringToFront()
        SA_Browser1.wbWebBrowser.Visible = True
        LoadingReferenceData("Ready", False)

    End Sub

    Friend Sub GetDataDump()
        Dim tmpPath As String
        LoadingReferenceData("Creating data dump into Excel...", True)

        If lvReports.SelectedItems.Count = 0 Then
            ProfileMsgBox("CRIT", "RESSEL", "report")
            LoadingReferenceData("Ready", False)
            Exit Sub
        End If
        Dim ReportCode As String = lvReports.SelectedItems(0).Name.ToString
        ' 20081001 RRH Path - tmpPath = MyParent.AppPath & "\_TEMP"
        tmpPath = pathTemp
        If Directory.Exists(tmpPath) = False Then Directory.CreateDirectory(tmpPath)
        Dim wb As New PVExportToExcel.pvWorkBook
        Dim ds As New DataSet
        Dim tmpUOM As String
        If cmbRptUOM.Text = "US Units" Then tmpUOM = "US" Else tmpUOM = "MET"
        Dim scenario As String
        If cbStudyPriceRpt.Checked Then scenario = cmbRptMeth.Text Else scenario = "CLIENT"

        Try
            ds = MyParent.GetDataDump(ReportCode, "ACTUAL", scenario, cmbRptCurrencyCode.Text, CDate(cmbRptMonth.SelectedValue), tmpUOM, CInt(cmbRptMeth.Text), chkRptTarget.Checked, chkRptYTD.Checked, chkRptAVG.Checked, RefineryID)
            If ds.Tables.Count = 0 Then
                LoadingReferenceData("Ready", False)
                Exit Sub
            End If
        Catch ex As Exception
            ProfileMsgBox("CRIT", "CONN", Nothing)
            LoadingReferenceData("Ready", False)
            Exit Sub
        End Try

        ds.DataSetName = lvReports.SelectedItems(0).Text
        wb.datasource = ds
        wb.workpath = tmpPath
        wb.renderWorkbook(ds)

        ' 20081216 RRH Dialog box
        'Dim result As DialogResult
        'result = MsgBox("The historical data for the report requested has been saved as " & tmpPath & ". Due to the different versions of Excel, Profile II may not be able to open the file directly. If you would like to attempt to open the file, click 'Yes', otherwise click 'No' and manually open the file in a separate instance of Excel.", MsgBoxStyle.Information Or MsgBoxStyle.YesNo, "Profile II")
        tmpPath = tmpPath & ds.DataSetName & ".xls"

        If ProfileMsgBox("YN", "", "The historical data for the report requested has been saved as " & tmpPath & ". Due to the different versions of Excel, Profile II may not be able to open the file directly. If you would like to attempt to open the file, click 'Yes', otherwise click 'No' and manually open the file in a separate instance of Excel.") = Windows.Forms.DialogResult.Yes Then

            Dim MyXL, MyWB As Object
            Try
                MyXL = CreateObject("Excel.Application")
            Catch ex1 As Exception
                MyXL = Nothing
                ProfileMsgBox("CRIT", "NOXL", Nothing)
                LoadingReferenceData("Ready", False)
                Exit Sub
            End Try

            MyXL.Visible = True

            Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")

            MyWB = MyXL.workbooks.open(tmpPath)

            System.Threading.Thread.CurrentThread.CurrentCulture = oldCI

            'MyWB.Parent.Windows(1).Visible = True

            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyWB)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(MyXL)
            MyWB = Nothing
            MyXL = Nothing

        End If


        'al.SubmitActivityLog(cmbRptMeth.Text, RefineryID, "", UserName, Environment.MachineName.ToString(), "Main", "GetDataDump", ReportCode, PeriodStart, "", "", "Success")


        If Util.UseNonWCFWebService Then
            'al.SubmitActivityLog("GetDataDump", Nothing, Nothing, Nothing, "Success", Nothing, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            Try
                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, "GetDataDump", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                '    userid, computername, String.Empty, "GetDataDump", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
            Catch ex As Exception

            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If
        LoadingReferenceData("Ready", False)
    End Sub

    Private Sub btnRptCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptCreate.Click
        If lvReports.SelectedItems.Count < 1 Then Return
        Dim ReportCode As String = lvReports.SelectedItems(0).Name.ToString
        Select Case ReportCode.ToUpper().Trim()
            Case "RS", "TRE"
                modGeneral.MainForm = Me
                GetListOfRowsWanted(lvReports.SelectedItems(0).ToString())
            Case "TG"
                'show SA_Targeting
                Dim filter As New SA_Targeting()
                'pnlWorkSpace.Controls(0).Visible = False
                SA_Browser1.Visible = False
                pnlWorkSpace.Controls.Add(filter)
                filter.Visible = True
                filter.BringToFront()
                filter.Dock = DockStyle.Fill
            Case Else
                btnRptCreateRoutine()
        End Select
        'If ReportCode <> "RS" And ReportCode <> "TRE" Then
        '    btnRptCreateRoutine()
        'Else
        '    modGeneral.MainForm = Me
        '    GetListOfRowsWanted(lvReports.SelectedItems(0).ToString())
        'End If
    End Sub

    Private Function GetListOfRowsWanted(reportName As String) As String
        'add this list into querystring at &rows=
        'SA_ReportRowsFilter

        Dim filter As New SA_ReportRowsFilter() 'GetStoredProcTableName(reportName))
        filter.BuildFilter(GetStoredProcTableName(reportName))
        'Dim filter As SA_ReportRowsFilter




        'pnlWorkSpace.Controls(0).Visible = False
        SA_Browser1.Visible = False
        pnlWorkSpace.Controls.Add(filter)
        filter.Visible = True
        filter.BringToFront()
        filter.Dock = DockStyle.Fill

    End Function

    Private Function GetStoredProcTableName(reportName As String) As String
        If reportName.Contains("Refinery Scorecard") Then
            Return "ReportLayout_LU"
        ElseIf reportName.Contains("Refinery Trends Report") Then
            Return "ReportLayout_LU"
        Else
            Return String.Empty
        End If
    End Function

    Friend Sub btnRptCreateRoutine(Optional rowsOrdinalsToInclude As String = "", Optional peersForTargeting As List(Of String) = Nothing,
                                   Optional kpisForTargeting As List(Of String) = Nothing)
        SA_Browser1.PleaseWait()

        LoadingReferenceData("Creating Report...", True)
        'If MyParent.ResultsRights Then
        '    SA_Browser1.InstallCertificate()
        'End If

        If lvReports.SelectedItems.Count = 0 Then
            ProfileMsgBox("CRIT", "RESSEL", "report")
            LoadingReferenceData("Ready", False)
            Exit Sub
        End If

        Dim ReportCode As String = lvReports.SelectedItems(0).Name.ToString
        Dim tmpUOM As String
        If CBool(cmbRptUOM.SelectedItem.Startswith("US")) Then tmpUOM = "US" Else tmpUOM = "MET"
        Dim location As String
        If txtLocation.Text = "" Then location = "ND" Else location = Trim(txtLocation.Text)
        Dim scenario As String
        If cbStudyPriceRpt.Checked Then scenario = cmbRptMeth.Text.Trim() Else scenario = "CLIENT"
        Dim lvReportsSelectedItems0Text As String = lvReports.SelectedItems(0).Text
        If lvReportsSelectedItems0Text = "EDC Calculation" Then
            lvReportsSelectedItems0Text = "EDC, Utilization, Standard Energy and Gain"
        End If
        If lvReports.SelectedItems(0).Tag.ToUpper() = "0" Then

            SA_Browser1.DisplayReport(RefineryID, location, lvReportsSelectedItems0Text, CDate(cmbRptMonth.Text), tmpUOM, _
                                   cmbRptCurrencyCode.SelectedItem.ToString, cmbRptMeth.Text.Trim, "ACTUAL", scenario, _
                                   chkRptTarget.Checked, chkRptAVG.Checked, chkRptYTD.Checked, rowsOrdinalsToInclude)
        Else
            SA_Browser1.DisplayCustomReport(RefineryID, location, lvReportsSelectedItems0Text, CDate(cmbRptMonth.Text), tmpUOM, _
                                   cmbRptCurrencyCode.SelectedItem.ToString, cmbRptMeth.Text.Trim, "ACTUAL", scenario, _
                                   chkRptTarget.Checked, chkRptAVG.Checked, chkRptYTD.Checked, peersForTargeting, kpisForTargeting,
                                   rowsOrdinalsToInclude)


        End If
        SA_Browser1.lblHeader.Text = "Reports of Results"
        SA_Browser1.Visible = True
        SA_Browser1.BringToFront()
        'SA_Browser1.btnDump.Visible = True
        SA_Browser1.wbWebBrowser.Visible = True
        LoadingReferenceData("Ready", False)

        Dim startDate As DateTime? = Nothing
        Dim endDate As DateTime? = Nothing
        Try
            startDate = DateTime.Parse(PeriodStart)
        Catch
        End Try
        'Try
        '    endDate = DateTime.Parse(PeriodEnd)
        'Catch
        'End Try
        'al.SubmitActivityLog(cmbRptMeth.Text, RefineryID, "", UserName, Environment.MachineName.ToString(), "Main", "btnRptCreate", ReportCode.ToString(), cmbRptMonth.Text, "", "", "Success")


        If Util.UseNonWCFWebService Then
            'al.SubmitActivityLog("btnRptCreateRoutine", Nothing, startDate, endDate, "Success", Nothing, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            Try

                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, "btnRptCreateRoutine", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                '    userid, computername, String.Empty, "btnRptCreateRoutine", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
            Catch ex As Exception

            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If
    End Sub


#End Region

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Friend Sub GoToDataEntry()
        SA_Browser1.Visible = False
        Dim str As String
        Try
            str = treeTableViews.SelectedNode.Text
        Catch ex As Exception
            str = ""
        End Try

        Select Case str
            Case "Table 1", "Process Facilities", "Table 2", "Table 10"
                EditMonthlyData("Process Facilities")
            Case "Inventory"
                EditMonthlyData("Inventory")
            Case "Table 4"
                EditMonthlyData("Operating Expenses")
            Case "Table 5", "Table 6", "Table 7"
                EditMonthlyData("Personnel")
            Case "Table 9"
                EditConfiguration("Process Facilities")
            Case "Table 14"
                EditMonthlyData("Crude Charge Detail")
            Case "Table 15", "Raw Material Inputs", "Product Yields"
                EditMonthlyData("Material Balance")
            Case "Table 16", "Thermal Energy", "Electricity"
                EditMonthlyData("Energy")
            Case "User-Defined Inputs/Outputs"
                EditMonthlyData(str)
            Case "Refinery Level", "Process Unit Level", "Historical Maintenance Costs", "EDC Stabilizers"
                EditConfiguration(str)
        End Select
        'al.SubmitActivityLog("", RefineryID, "", UserName, Environment.MachineName.ToString(), "Main", "GoToDataEntry", str, "", "", "", "Success")


        If Util.UseNonWCFWebService Then
            'al.SubmitActivityLog("GoToDataEntry", Nothing, Nothing, Nothing, "Success", Nothing, String.Empty)
        Else
            'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
            'Dim proxy As New ProfileDataClient(endpointName)
            'Dim svc As New WcfService.ProfileDataServiceClient()
            Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

            Try
                Dim applicationName As String = String.Empty
                Dim Methodology As String = String.Empty
                Dim localRefineryID As String = String.Empty
                Dim CallerIP As String = String.Empty
                Dim userid As String = String.Empty
                Dim computername As String = String.Empty
                Dim version As String = String.Empty
                Dim err As Object = Nothing
                Dim errorMessages As String = String.Empty
                Dim localDomainName As String = String.Empty
                Dim osVersion As String = String.Empty
                Dim officeVersion As String = String.Empty
                frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                    userid, computername, String.Empty, "GoToDataEntry", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                    localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                '    userid, computername, String.Empty, "GoToDataEntry", String.Empty, Nothing, Nothing, "Success", version, Nothing,
                '    localDomainName, osVersion, Nothing, officeVersion, Nothing)
            Catch ex As Exception
                Dim msg As String = ex.Message
            Finally
                'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End Try
        End If

    End Sub

#Region " Import Utility "

    Private Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        'need file "Solomon Profile� II Bridge File.xlsx" in "C:\Program Files (x86)\HSB Solomon Associates LLC\Solomon Profile II\_BRIDGE"
        ImportAll()
    End Sub

    Public Sub ImportAll()
        SA_Import1.SetDataSet()
        SA_Import1.Visible = True
        SA_Import1.BringToFront()
        tcControlPanel.Enabled = False
        MyParent.MenuFile.Enabled = False
        MyParent.MenuEdit.Enabled = False
        MyParent.MenuView.Enabled = False
        MyParent.MenuTools.Enabled = False
        MyParent.MenuWindow.Enabled = False
        MyParent.MenuHelp.Enabled = False
        btnWorkspace.Enabled = False
    End Sub
#End Region

    Private Sub UpdateLinkVisited()
        'lblHome6.LinkVisited = True
        SA_Browser1.GoHome()
        SA_Browser1.Visible = True
        SA_Browser1.BringToFront()
        SA_Browser1.btnDump.Visible = False
    End Sub

    Friend Sub CallHelp()
        SA_Browser1.GoHelp()
        SA_Browser1.Visible = True
        SA_Browser1.BringToFront()
        SA_Browser1.btnDump.Visible = False
    End Sub

    Friend Sub NameDatasets()
        dsSettings.DataSetName = "Settings"
        dsRefineryInformation.DataSetName = "RefineryInformation"
        dsProcess.DataSetName = "Process"
        ' 20081217 RRH OpEx
        dsOperatingExpenses.DataSetName = "OpEx"
        dsPersonnel.DataSetName = "Personnel"
        dsCrude.DataSetName = "Crude"
        dsYield.DataSetName = "Yield"
        dsEnergy.DataSetName = "Energy"
        dsUserDefined.DataSetName = "UserDefined"
        dsMappings.DataSetName = "Mappings"
    End Sub

    Friend Sub PopulateMonth()
        ClearMonth()
        dsSettings.ReadXml(MonthPath & "Settings.xml")

        If Not dsSettings.Tables("Settings").Columns.Contains("Uploaded") Then
            dsSettings.Tables("Settings").Columns.Add("Uploaded", System.Type.GetType("System.Boolean"))
            dsSettings.Tables(0).Rows(0)!Uploaded = modUploadNotify.filesNotModified(MonthPath)
        End If

        modUploadNotify.uploadNotify(Me.Label48, Me.Label49, CBool(dsSettings.Tables(0).Rows(0)!Uploaded.ToString), dsSettings, MonthPath)

        dsProcess.ReadXml(MonthPath & "Process.xml")
        dsOperatingExpenses.ReadXml(MonthPath & "OpEx.xml")
        dsPersonnel.ReadXml(MonthPath & "Personnel.xml")
        dsCrude.ReadXml(MonthPath & "Crude.xml")
        dsYield.ReadXml(MonthPath & "Yield.xml")
        dsEnergy.ReadXml(MonthPath & "Energy.xml")
        dsUserDefined.ReadXml(MonthPath & "UserDefined.xml")
        dsRefineryInformation.ReadXml(MonthPath & "RefineryInformation.xml")

        Dim tmp_dsProcessds As New DataSet
        dsProcess.Tables.Remove("MaintTA")
        tmp_dsProcessds.ReadXml(pathConfig & "Process.xml")
        dsProcess.Tables.Add(tmp_dsProcessds.Tables("MaintTA").Copy)

        Dim tmp_dsRefineryInformation As New DataSet
        dsRefineryInformation.Tables.Remove("EDCStabilizers")
        dsRefineryInformation.Tables.Remove("MaintRoutHist")
        tmp_dsRefineryInformation.ReadXml(pathConfig & "RefineryInformation.xml")
        dsRefineryInformation.Tables.Add(tmp_dsRefineryInformation.Tables("EDCStabilizers").Copy)
        dsRefineryInformation.Tables.Add(tmp_dsRefineryInformation.Tables("MaintRoutHist").Copy)

        dsUnitList.ReadXml(pathConfig & "UnitList.xml")
        dsLoadedMonths.ReadXml(pathConfig & "LoadedMonths.xml")

        '20090410 RRH dsMappings.ReadXml(pathStartUp & "Mappings.xml")
        XMLFile.ReadXMLFile(dsMappings, pathStartUp & "Mappings.xml")

        ' Create the Excel workbook reference table in dsMappings

        If Not dsMappings.Tables.Contains("wkbReference") Then _
                    dsMappings.Tables.Add("wkbReference")
        If Not dsMappings.Tables("wkbReference").Columns.Contains("frmName") Then _
                    dsMappings.Tables("wkbReference").Columns.Add("frmName", System.Type.GetType("System.String"))
        If Not dsMappings.Tables("wkbReference").Columns.Contains("tbFilePath") Then _
                    dsMappings.Tables("wkbReference").Columns.Add("tbFilePath", System.Type.GetType("System.String"))
        If Not dsMappings.Tables("wkbReference").Columns.Contains("cbSheets") Then _
                    dsMappings.Tables("wkbReference").Columns.Add("cbSheets", System.Type.GetType("System.String"))
        If Not dsMappings.Tables("wkbReference").Columns.Contains("cbSheets1") Then _
                    dsMappings.Tables("wkbReference").Columns.Add("cbSheets1", System.Type.GetType("System.String"))

        dsSettings.AcceptChanges()
        dsProcess.AcceptChanges()
        dsOperatingExpenses.AcceptChanges()
        dsPersonnel.AcceptChanges()
        dsCrude.AcceptChanges()
        dsYield.AcceptChanges()
        dsEnergy.AcceptChanges()
        dsUserDefined.AcceptChanges()
        dsRefineryInformation.AcceptChanges()
        dsUnitList.AcceptChanges()
        dsLoadedMonths.AcceptChanges()
        dsMappings.AcceptChanges()

        NameDatasets()
    End Sub

    Friend Sub PopulateReferenceData()
        PopulateReportTree()
        PopulateChartTree()
    End Sub

    Friend Sub ClearMonth()
        Try
            dsSettings.Tables("Settings").Clear()
            dsProcess.Tables("Config").Clear()
            dsProcess.Tables("ProcessData").Clear()
            dsProcess.Tables("MaintTA").Clear()
            dsProcess.Tables("MaintRout").Clear()
            dsProcess.Tables("UnitTargetsNew").Clear()
            dsRefineryInformation.Tables("RefTargets").Clear()
            dsRefineryInformation.Tables("Inventory").Clear()
            dsRefineryInformation.Tables("ConfigRS").Clear()
            dsRefineryInformation.Tables("EDCStabilizers").Clear()
            dsRefineryInformation.Tables("MaintRoutHist").Clear()
            dsOperatingExpenses.Tables("OpExAll").Clear()
            'dsOperatingExpenses.Tables("OpexAdd").Clear()
            dsPersonnel.Tables("Pers").Clear()
            dsPersonnel.Tables("Absence").Clear()
            dsCrude.Tables("Crude").Clear()
            dsYield.Tables("Yield_RM").Clear()
            dsYield.Tables("Yield_RMB").Clear()
            dsYield.Tables("Yield_Prod").Clear()
            dsEnergy.Tables("Energy").Clear()
            dsEnergy.Tables("Electric").Clear()
            dsUserDefined.Tables(0).Clear()
            dsUnitList.Tables(0).Clear()
        Catch ex As Exception

        End Try
    End Sub

    Friend Sub EnablingWatchers()
        MonthWatcher.EnableRaisingEvents = True
        ConfigWatcher.EnableRaisingEvents = True
        ReferenceWatcher.EnableRaisingEvents = True
    End Sub

    Friend Sub DisablingWatchers()
        MonthWatcher.EnableRaisingEvents = False
        ConfigWatcher.EnableRaisingEvents = False
        ReferenceWatcher.EnableRaisingEvents = False
    End Sub

    Private Sub btnAdmin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdmin.Click
        SA_Admin1.Visible = True
        SA_Admin1.BringToFront()
        tcControlPanel.Enabled = False
        MyParent.MenuFile.Enabled = False
        MyParent.MenuEdit.Enabled = False
        MyParent.MenuView.Enabled = False
        MyParent.MenuTools.Enabled = False
        MyParent.MenuWindow.Enabled = False
        MyParent.MenuHelp.Enabled = False
        btnWorkspace.Enabled = False
        btnCP.Enabled = False
    End Sub

    Friend Sub HideTopLayer(ByVal ctl As Control)
        ctl.Visible = False
    End Sub

    'Private Sub LinkLabel1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    CallHelp()
    'End Sub

    'Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim result As DialogResult = ProfileMsgBox("YN", "WAIT", "")
    '    If result = Windows.Forms.DialogResult.Yes Then MyParent.Set_REF()
    'End Sub

    Private Sub lvDataEntry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDataEntry.Click
        SelectDataEntry()
    End Sub

    Private Sub lvDataEntry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDataEntry.SelectedIndexChanged
        SelectDataEntry()
    End Sub

#Region " Activity Monitor "
    Dim UserName As String
    Dim ds As New DataSet
    Dim dtEvents As New DataTable
    Dim dtLoggedOn As New DataTable
    Dim startTime As String = Date.Now.ToShortDateString
    'Starts the monitoring the login  and event files. 
    'Needed because there was some problems with the IDE and Filewatcher at 
    'IDE designer
    Friend Sub StartMonitoring(ByVal User As String)
        Dim userimages As ImageList = New ImageList
        MyParent = CType(Me.ParentForm, frmMain)
        UserName = User

        ' 20081001 RRH Path
        'If Not Directory.Exists(myparent.MyParent.AppPath & "\_ADMIN") Then
        '    Directory.CreateDirectory(myparent.MyParent.AppPath & "\_ADMIN")
        If Not Directory.Exists(pathAdmin) Then
            Directory.CreateDirectory(pathAdmin)
        End If

        ' 20081001 RRH Path - FileSystemWatcher1.Path = myparent.MyParent.AppPath & "\_ADMIN"
        FileSystemWatcher1.Path = pathAdmin
        FileSystemWatcher1.Filter = "Activity.xml"

        Try
            ' 20081001 RRH Path - ds.ReadXml(myparent.MyParent.AppPath & "/_ADMIN/Activity.xml")
            ds.ReadXml(pathAdmin & "Activity.xml")
            dtEvents = ds.Tables("Events")
            dtLoggedOn = ds.Tables("LoggedOn")
        Catch ex As Exception
            dtEvents.TableName = "Events"
            With dtEvents.Columns
                .Add("EventDate", GetType(Date))
                .Add("EventDescription", GetType(String))
                .Add("EventUser", GetType(String))
            End With
            dtLoggedOn.TableName = "LoggedOn"
            With dtLoggedOn.Columns
                .Add("User", GetType(String))
            End With
            ds.Tables.Add(dtEvents)
            ds.Tables.Add(dtLoggedOn)
        End Try

        Dim row As DataRow
        Dim IsThere As Boolean
        If dtLoggedOn.Rows.Count > 0 Then
            For Each row In dtLoggedOn.Rows
                If row!User.ToString = UserName Then IsThere = True
            Next
        End If
        If IsThere = False Then
            row = dtLoggedOn.NewRow
            row!User = UserName
            dtLoggedOn.Rows.Add(row)
        End If

        ' 20081001 RRH Path - ds.WriteXml(myparent.MyParent.AppPath & "/_ADMIN/Activity.xml", XmlWriteMode.WriteSchema)
        ds.WriteXml(pathAdmin & "Activity.xml", XmlWriteMode.WriteSchema)

        Dim dv As New DataView(ds.Tables("Events"))
        dv.Sort = "EventDate DESC"
        dgvEvents.DataSource = dv

        AddUsers()

        FileSystemWatcher1.EnableRaisingEvents = True
        AddHandler FileSystemWatcher1.Changed, AddressOf OnChanged
    End Sub

    Private Sub AddUsers()
        lvUsers.Items.Clear()
        lvUsers.Items.Add(Trim(UserName)).ImageIndex = 0

        Dim row As DataRow
        For Each row In dtLoggedOn.Rows
            If row!User.ToString <> UserName Then
                lvUsers.Items.Add(Trim(row!User.ToString)).ImageIndex = 0
            End If
        Next
    End Sub

    Friend Sub CreateEvent(ByVal dt As Date, ByVal desc As String)
        Dim row As DataRow = dtEvents.NewRow
        row!EventDate = dt
        row!EventDescription = Trim(desc)
        row!EventUser = UserName
        dtEvents.Rows.Add(row)
        FileSystemWatcher1.EnableRaisingEvents = False

        ' 20081001 RRH Path - ds.WriteXml(myparent.MyParent.AppPath & "/_ADMIN/Activity.xml", XmlWriteMode.WriteSchema)
        ds.WriteXml(pathAdmin & "Activity.xml", XmlWriteMode.WriteSchema)

        FileSystemWatcher1.EnableRaisingEvents = True
    End Sub

    Private Sub OnChanged(ByVal source As Object, ByVal e As FileSystemEventArgs)
        dtEvents.Clear()
        dtLoggedOn.Clear()

        While (True)
            Try
                ' 20081001 RRH Path - ds.ReadXml(myparent.MyParent.AppPath & "/_ADMIN/Activity.xml")
                ds.ReadXml(pathAdmin & "Activity.xml")
                Exit While
            Catch ex As Exception
                'keep looping until free
                If ex.GetType() Is Type.GetType("IOException") Then
                    Throw ex
                End If
            End Try
        End While

        AddUsers()
    End Sub

    Friend Sub RemoveUserFromActivity()
        FileSystemWatcher1.EnableRaisingEvents = False
        Dim i As Integer
        For i = dtLoggedOn.Rows.Count - 1 To 0 Step -1
            With dtLoggedOn.Rows(i)
                If !User.ToString = UserName Then
                    .Delete()
                End If
            End With
        Next

        ' 20081001 RRH Path - ds.WriteXml(myparent.MyParent.AppPath & "/_ADMIN/Activity.xml", XmlWriteMode.WriteSchema)
        ds.WriteXml(pathAdmin & "Activity.xml", XmlWriteMode.WriteSchema)

    End Sub

    Private Sub DeleteEvents()
        Dim result As DialogResult = ProfileMsgBox("YN", "DELETEAM", "")
        If result = Windows.Forms.DialogResult.Yes Then
            dtEvents.Clear()
            FileSystemWatcher1.EnableRaisingEvents = False

            ' 20081001 RRH Path - ds.WriteXml(myparent.MyParent.AppPath & "/_ADMIN/Activity.xml", XmlWriteMode.WriteSchema)
            ds.WriteXml(pathAdmin & "Activity.xml", XmlWriteMode.WriteSchema)

            FileSystemWatcher1.EnableRaisingEvents = True
        End If
    End Sub

    Private Sub btnClearLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLog.Click
        DeleteEvents()
    End Sub
#End Region

    'Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    MyParent.Close()
    'End Sub

    Private Sub btnCP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCP.Click
        If pnlControlPanel.Width <> 26 Then
            tcControlPanel.Visible = False
            pnlControlPanel.Width = 26
            btnCP.Text = ""
            btnCP.Image = ImageList1.Images(1)
        Else
            pnlControlPanel.Width = 316
            btnCP.Image = ImageList1.Images(0)
            btnCP.Text = "Control Panel"
            tcControlPanel.Visible = True
        End If
    End Sub

    Private Sub btnWorkspace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWorkspace.Click
        CallHelp()
    End Sub

    Private Sub treeTableViews_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles treeTableViews.AfterSelect
        SelectTableView("", "", SA_Browser1, e.Node)
    End Sub

    Private Sub cbStudyPriceRpt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbStudyPriceRpt.CheckedChanged
        My.Settings.viewStudyPricingRpt = CBool(cbStudyPriceRpt.Checked)
        My.Settings.Save()
    End Sub

    Private Sub cbStudyPriceCht_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbStudyPriceCht.CheckedChanged
        My.Settings.viewStudyPricingCht = CBool(cbStudyPriceCht.Checked)
        My.Settings.Save()
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        UpdateLinkVisited()
    End Sub

    Private Sub PictureBox9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox9.Click
        UpdateLinkVisited()
    End Sub

    Private Sub PictureBox10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox10.Click
        UpdateLinkVisited()
    End Sub

    Private Sub PictureBox11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox11.Click
        UpdateLinkVisited()
    End Sub

    Private Sub PictureBox12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox12.Click
        UpdateLinkVisited()
    End Sub

    Private Sub PictureBox13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox13.Click
        UpdateLinkVisited()
    End Sub

    Private Sub ShowRadarChart()
        'MsgBox("Show radar form, get variables from it, store at project level, close radar form, fire method here")
        Dim radarChoice As New RadarChartChoices()
        Dim chartName As String = String.Empty
        radarChoice.ShowDialog()
        If Not IsNothing(radarChoice) Then
            radarChoice.Dispose()
        End If
        If Not IsNothing(RadarChtVariables) Then 'user made some choices and didn't cancel out
            'call button click
            ' ChtCreateRoutine()

            Dim dt As Date = DateTime.Now
            Dim page As String = ConfigurationManager.AppSettings("ChartControlUrl").ToString()

            Dim url As String = String.Empty
            '
            'If Not IsNothing(RadarChtVariables) Then '  ChartType.ToUpper = "RADAR" Then
            'Dim breakValue As String = RadarChtVariables.BreakValue
            'RadarChtVariables.RankBreak = ????
            'Dim refListName As String = RadarChtVariables.PeerGroup ' "NSA" 'aka study
            Dim refListNames As List(Of Integer)
            If Not IsNothing(RadarChtVariables) Then
                refListNames = New List(Of Integer)
                refListNames.Add(RadarChtVariables.PeerGroup)
            End If
            'Dim fields As New List(Of String)
            'If field1.Length > 0 Then fields.Add(field1)
            'If field2.Length > 0 Then fields.Add(field2)
            'If field3.Length > 0 Then fields.Add(field3)
            'If field4.Length > 0 Then fields.Add(field4)
            'If field5.Length > 0 Then fields.Add(field5)
            Dim rankVariables As List(Of Integer) = RadarChtVariables.RankVariables

            'Dim group As String = RadarChtVariables.Group
            'Dim rankVariable As String = RadarChtVariables.RankVariable
            Dim monthColor As String = String.Empty
            If RadarChtVariables.GetMonth Then
                monthColor = RadarChtVariables.MonthColor
            End If
            Dim ytdColor As String = String.Empty
            If RadarChtVariables.GetYtd Then
                ytdColor = RadarChtVariables.YtdColor
            End If
            Dim avgColor As String = String.Empty
            If RadarChtVariables.GetStudyEquivalentAkaRollingAve Then
                avgColor = RadarChtVariables.StudyEquivalentAkaRollingAveColor
            End If


            'we aren't using Target
            Dim targetColor As String = String.Empty


            Dim currency As String = cmbChtCurrencyCode.SelectedItem.ToString
            Dim startDate As String = cmbChtStart.Text ' = cmbChtStart.SelectedValue
            Dim endDate As String = cmbChtEnd.Text  'cmbChtEnd.SelectedValue

            ' 20081215 RRH changed verification analysis
            'If (PeriodStart = "" Or startDate = "") OrElse CDate(PeriodStart) > CDate(endDate) Then
            If (IsNothing(PeriodStart) Or IsNothing(startDate)) OrElse PeriodStart > CDate(endDate) Then
                ProfileMsgBox("CRIT", "CHTDATE", Nothing)
                LoadingReferenceData("Ready", False)
                Exit Sub
            End If
            Dim scenario As String = String.Empty
            If cbStudyPriceCht.Checked Then scenario = cmbChtMeth.Text.Trim() Else scenario = "CLIENT"
            Dim rankBreak As String = RadarChtVariables.RankBreak
            RadarChtVariables = Nothing
            'refListName = refListName.Replace("    ", "$$")

            'MsgBox("need these querystrings:")
            'url = RadarChartUrl(fields, chartName, startDate, endDate, currency, UOM, senario,
            'ChartColor, AvgColor, YtdColor, TargetColor, study, breakValue, kpiNames)
            Dim chartColor As String = "" 'this is not something that we want to let them change
            Dim localUom As String = UOM
            If localUom = "US Units" Then
                localUom = "US"
            ElseIf localUom = "Metric" Then
                localUom = "MET"
            End If


            url = RadarChartUrl(chartName, startDate, endDate, currency, localUom, scenario,
                   chartColor, monthColor, avgColor, ytdColor, refListNames, rankVariables)
            'End If

            'wbWebBrowser.Navigate(url, "", Nothing, header)
            '_url = url
            ' _title = chartName
            Dim header As String = "WsP: " & GetClientKey() + vbCrLf

            SA_Browser1.Navigate(url, "", Nothing, header, chartName)
            Dim startDateLog As DateTime? = Nothing
            Dim endDateLog As DateTime? = Nothing
            Try
                startDateLog = DateTime.Parse(PeriodStart)
            Catch
            End Try
            'Try
            '    endDateLog = DateTime.Parse()
            'Catch
            'End Try


            If Util.UseNonWCFWebService Then
                'al.SubmitActivityLog("btnRadarChart_Click", Nothing, startDateLog.ToString(), endDateLog, "Success", Nothing, String.Empty)
            Else
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfDataEndpointName")
                'Dim proxy As New ProfileDataClient(endpointName)
                'Dim svc As New WcfService.ProfileDataServiceClient()
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Try
                    Dim applicationName As String = String.Empty
                    Dim Methodology As String = String.Empty
                    Dim localRefineryID As String = String.Empty
                    Dim CallerIP As String = String.Empty
                    Dim userid As String = String.Empty
                    Dim computername As String = String.Empty
                    Dim version As String = String.Empty
                    Dim err As Object = Nothing
                    Dim errorMessages As String = String.Empty
                    Dim localDomainName As String = String.Empty
                    Dim osVersion As String = String.Empty
                    Dim officeVersion As String = String.Empty
                    frmMain.PrepActivityLogArguments(applicationName, Methodology, localRefineryID, CallerIP,
                    userid, computername, version, err, errorMessages, localDomainName, osVersion, officeVersion)
                    proxy.WriteActivityLogExtended(applicationName, Methodology, CallerIP,
                       userid, computername, String.Empty, "btnRadarChart_Click", String.Empty, startDateLog.ToString(), endDateLog, "Success", version, Nothing,
                       localDomainName, osVersion, Nothing, officeVersion, Nothing, Nothing)
                    'svc.WriteActivityLogExtended(applicationName, Methodology, GetClientKey(), CallerIP,
                    '  userid, computername, String.Empty, "btnRadarChart_Click", String.Empty, startDateLog.ToString(), endDateLog, "Success", version, Nothing,
                    'localDomainName, osVersion, Nothing, officeVersion, Nothing)
                Catch ex As Exception
                    Dim errMsg As String = ex.Message
                Finally
                    'If Not svc.State = ServiceModel.CommunicationState.Faulted Then svc.Close() ' 
                    If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
                End Try
            End If
            SA_Browser1.lblHeader.Text = "Charts of Results"
            SA_Browser1.Visible = True
            SA_Browser1.BringToFront()
            SA_Browser1.wbWebBrowser.Visible = True


        End If
    End Sub

    'Private Function RadarChartUrl(chartName As String,
    '                           startDate As Date, endDate As Date, currency As String,
    '                           uom As String, scenarioYear As String, chartColor As String,
    '                           monthColor As String, avgColor As String, ytdColor As String,
    '                           refListName As String, rankBreak As String, breakValue As String,
    '                           rankVariables As List(Of Integer)) As String
    '    Dim page As String = ConfigurationManager.AppSettings("ChartControlUrl").ToString()
    '    ' Chart radarChart = factory.GetRadarChartTest2(fields, "118NSA", "6/1/2015", "6/1/2016", "TEST NAME", 
    '    '    "USD", "2014", "US", "2014", "chratInfoRequested",
    '    '   "RADAR", "Red", "Blue", "Black", "White", "USA", "NSA", kpiNames);

    '    Dim url As String = page & "?cn=" & chartName & "&sd=" & startDate.ToString("M/d/yyyy") & "&ed=" & endDate.ToString("M/d/yyyy")
    '    url += "&UOM=" + uom + "&currency=" + currency
    '    url += "&sn=" + scenarioYear.Trim()

    '    'url+="&chartType=" + ChartType - is added below
    '    'Dim fieldCount As Integer = 1
    '    'For itemCount As Integer = 0 To fields.Count - 1
    '    '    If fields(itemCount).Length > 0 Then
    '    '        url += "&field" + fieldCount.ToString() + "=" + fields(itemCount)
    '    '        fieldCount += 1
    '    '    End If
    '    'Next

    '    Dim chartInfo As String = "&chart=RADAR|" + chartColor + "|" + avgColor + "|" + ytdColor + "|" + monthColor
    '    url += chartInfo
    '    url += "&refListName=" + refListName + "&rankBreak=" + rankBreak + "&breakValue=" + breakValue

    '    If rankVariables.Count > 0 Then
    '        url += "&rankVariables="
    '        For Each item As String In rankVariables
    '            url += item + "|"
    '        Next
    '        url = url.Remove(url.Length - 1) 'remvoe trailing "|"
    '    End If


    '    url += "&WsP=" + GetClientKeyString()
    '    'espect this:
    '    'http://localhost:2153/ChartPage.aspx?cn=&sd=6/1/2015&ed=6/1/2016&UOM=Metric&currency=USD&sn=2014&chart=RADAR|Black|DarkGreen|Chocolate|&refListName=NSA14&rankBreak=Area&breakValue=USA&rankVariables=EII|OpAvail|MechAvail|PersIndex|PEI|MaintPersEffIndex|MaintIndex|MaintEffIndex|NEOpexEffIndex&WsP=X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=
    '    Return url

    'End Function

    Private Function RadarChartUrl(chartName As String,
                               startDate As Date, endDate As Date, currency As String,
                               uom As String, scenarioYear As String, chartColor As String,
                               monthColor As String, avgColor As String, ytdColor As String,
                               PeerGroupIds As List(Of Integer),
                               rankVariables As List(Of Integer)) As String
        Dim page As String = ConfigurationManager.AppSettings("ChartControlUrl").ToString()
        ' Chart radarChart = factory.GetRadarChartTest2(fields, "118NSA", "6/1/2015", "6/1/2016", "TEST NAME", 
        '    "USD", "2014", "US", "2014", "chratInfoRequested",
        '   "RADAR", "Red", "Blue", "Black", "White", "USA", "NSA", kpiNames);

        Dim url As String = page & "?cn=" & chartName & "&sd=" & startDate.ToString("M/d/yyyy") & "&ed=" & endDate.ToString("M/d/yyyy")
        url += "&UOM=" + uom + "&currency=" + currency
        url += "&sn=" + scenarioYear.Trim()

        'url+="&chartType=" + ChartType - is added below
        'Dim fieldCount As Integer = 1
        'For itemCount As Integer = 0 To fields.Count - 1
        '    If fields(itemCount).Length > 0 Then
        '        url += "&field" + fieldCount.ToString() + "=" + fields(itemCount)
        '        fieldCount += 1
        '    End If
        'Next

        Dim chartInfo As String = "&chart=RADAR|" + chartColor + "|" + avgColor + "|" + ytdColor + "|" + monthColor
        url += chartInfo

        If Not IsNothing(PeerGroupIds) AndAlso PeerGroupIds.Count > 0 Then
            url += "&PeerGroups="
            For Each i As Integer In PeerGroupIds
                url += i.ToString() & "|"
            Next
            url = url.Remove(url.Length - 1) 'remove trailing comma
        End If
        'url += "&PeerGroups=" + refListName + "&rankBreak=" + rankBreak + "&breakValue=" + breakValue

        If Not IsNothing(rankVariables) AndAlso rankVariables.Count > 0 Then
            url += "&rankVariables="
            For Each item As String In rankVariables
                url += item + "|"
            Next
            url = url.Remove(url.Length - 1) 'remvoe trailing "|"
        End If


        'url += "&WsP=" + GetClientKeyString()
        'expect this:

        Return url

    End Function

    Private Function GetClientKeyString() As String
        Dim key As String = String.Empty
        Try
            If IO.File.Exists(modAppDeclarations.pathCert & Policy.GetFileName()) Then
                Dim reader As New IO.StreamReader(modAppDeclarations.pathCert & Policy.GetFileName())
                Dim password As String = ""

                If reader.Peek <> -1 Then
                    key = reader.ReadLine
                    'Console.WriteLine(strKey)
                    reader.Close()
                Else
                    Throw New Exception("A key was not found")
                End If
            End If
            Return key
        Catch ex As Exception
            MsgBox("Error reading ClientKeyString: " + ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Sub treeCharts_Click(sender As Object, e As EventArgs) Handles treeCharts.Click
        If Directory.Exists("C:\Users\sfb.DC1\Console") Then
            ChtCreateRoutine()
        End If
    End Sub
    Private Sub cboChartType_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboChartType.SelectionChangeCommitted
        'If cboChartType.SelectedItem.ToString().ToUpper() = "RADAR" Then
        '    ShowRadarChart()
        '    cboChartType.SelectedIndex = 0  'Once I click Radar, need to deselect Radar in cbo else will send bad url if they try for non-Radar chart

        'End If
    End Sub

    Private Sub btnRadarPlot_Click(sender As Object, e As EventArgs) Handles btnRadarPlot.Click
        ShowRadarChart()
    End Sub

    Private Sub Main_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        treeCharts.Height = tpCharts.Height - GroupBox2.Height - 120
        cboChartTargetColor.Top = chkChtTarget.Top
        cboChartAvgColor.Top = chkChtAVG.Top
        cboChartYtdColor.Top = chkChtYTD.Top
    End Sub
End Class
