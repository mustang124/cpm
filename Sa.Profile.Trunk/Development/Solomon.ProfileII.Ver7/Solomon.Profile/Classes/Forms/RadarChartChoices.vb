﻿Imports System.Configuration
Imports System.Collections.Generic
Imports System.Text


Public Class RadarChartChoices
    Implements IDisposable

    Private _peerGroups As List(Of TargetingItem)
    'Private _rankVariables As List(Of String)
    Private _kpiList As List(Of TargetingItem)
    'Private _groupsParts As String()
    Private _main As Main
    'Private _cboDelim As String = "    "

    'for dispose()
    Protected disposed As Boolean = False
    Private managedResource As System.ComponentModel.Component
    Private unmanagedResource As IntPtr

    'Public Sub New(mainForm As Main)
    '   _main = mainForm
    'End Sub

    Private Sub RadarChartChoices_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim peerChoices As DataTable = frmMain.ds_Ref.Tables("TargetingPeerGroups")
        _peerGroups = New List(Of TargetingItem)
        Dim count As Integer = 0
        For Each row As DataRow In peerChoices.Rows
            Dim reflistid As Integer = CInt(row("PGKey"))
            Dim value As String = row("LongText").ToString() ' .Replace("$$", _cboDelim)
            _peerGroups.Add(New TargetingItem(value, count, reflistid))
            count += 1
        Next
        cboPeerGroups.DataSource = New BindingSource(_peerGroups, Nothing)
        cboPeerGroups.DisplayMember = "Name"
        cboPeerGroups.ValueMember = "Value"

        'Per Richard 2017-07-11 "Yes the KPIs for the targeting report and Radar Plots should be the same - but note that the KPIs for the radar plot will need to have quartile breaks."
        'targeting report uses kpis listed in profile.ReportLayout_LU.VKey
        Dim kpiChoices As DataTable = frmMain.ds_Ref.Tables("Chart_LU") 'ReportLayout_LU")
        If Not IsNothing(kpiChoices) AndAlso kpiChoices.Rows.Count > 0 Then
            _kpiList = New List(Of TargetingItem)
            For Each row As DataRow In kpiChoices.Rows
                If Not IsDBNull(row("VKey")) Then 'And CInt(row("Active")) = 1 And CInt(row("IsHeader")) = 0 Then
                    '_kpiList.Add(New TargetingItem(row(0), row(1).ToString()))
                    _kpiList.Add(New TargetingItem(CInt(row("VKey")), row("ChartTitle").ToString())) '  "Description").ToString()))
                    chkBoxRankVariables.Items.Add(row("ChartTitle").ToString(), False)
                End If
            Next
        end if

        PopulateColors()
        cboAveColor.DataSource = PopulateColors()
        cboMonthColor.DataSource = PopulateColors()
        cboYtdColor.DataSource = PopulateColors()

        ' moight not need below ==========================================================================================
        '_rankVariables = ConfigurationManager.AppSettings("RadarRankVariables").Split(CChar(";"))
        'For i As Integer = 0 To _rankVariables.Length - 1
        '    'chkBoxKpis.Items.Add(_kpisParts(i), False)
        '    Dim checkForTesting As Boolean = False ' "EII;MaintEffIndex;MaintIndex;MaintPersEffIndex;MechAvail;NEOpexEffIndex;OpAvail;PEI;PersIndex".Contains(_rankVariables(i))
        '    Dim checksForTesting As String() = "EII;MaintEffIndex;MaintIndex;MaintPersEffIndex;MechAvail;NEOpexEffIndex;OpAvail;PEI;PersIndex".Split(CChar(";"))
        '    For j As Integer = 0 To checksForTesting.Length - 1
        '        If checksForTesting(j) = _rankVariables(i) Then
        '            checkForTesting = True
        '        End If
        '    Next

        '    chkBoxRankVariables.Items.Add(_rankVariables(i), checkForTesting)
        'Next

        ''_groupsParts = ConfigurationManager.AppSettings("RadarGroups").Split(CChar(";"))
        ''For i As Integer = 0 To _groupsParts.Length - 1
        ''    chkBoxGroups.Items.Add(_groupsParts(i), False)
        ''Next

        'cboPeerGroups = BuildRefListNames(cboPeerGroups, "RadarRefListNames1")
        'cboPeerGroups = BuildRefListNames(cboPeerGroups, "RadarRefListNames2")
        'cboPeerGroups = BuildRefListNames(cboPeerGroups, "RadarRefListNames3")
        'cboPeerGroups = BuildRefListNames(cboPeerGroups, "RadarRefListNames4")
        'cboPeerGroups = BuildRefListNames(cboPeerGroups, "RadarRefListNames5")

        'Dim rankBreaks As String() = ConfigurationManager.AppSettings("RadarRankBreaks").Split(CChar(";"))
        'For i As Integer = 0 To rankBreaks.Length - 1
        '    cboRankBreaks.Items.Add(rankBreaks(i))
        'Next

        'Dim breakValues As String() = ConfigurationManager.AppSettings("RadarBreakValues").Split(CChar(";"))
        'For i As Integer = 0 To breakValues.Length - 1
        '    cboBreakValues.Items.Add(breakValues(i))
        'Next

        ''Dim rankVariables As String() = ConfigurationManager.AppSettings("RadarRankVariables").Split(CChar(";"))
        ''For i As Integer = 0 To rankVariables.Length - 1
        ''    cboRankVariables.Items.Add(rankVariables(i))
        ''Next

        'PopulateColors()
        'cboAveColor.DataSource = PopulateColors()
        'cboMonthColor.DataSource = PopulateColors()
        'cboYtdColor.DataSource = PopulateColors()






        ''defaults for testing
        'cboPeerGroups.Text = "NSA14"
        'cboBreakValues.Text = "USA"
        'cboRankBreaks.Text = "Area"

        '' cboBreakValues.Text = "NSA"

    End Sub

    Private Sub chkBoxKpis_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkBoxRankVariables.SelectedIndexChanged
        'selectedindex is 2, 
        'selecteditem is string = "Maint INex"

        Dim cb As CheckedListBox = TryCast(sender, CheckedListBox)
        Dim isChecked As Boolean = chkBoxRankVariables.GetItemChecked(cb.SelectedIndex)
        'For i As Integer = 0 To chkBoxKpis.Items.Count - 1
        '    chkBoxKpis.SetItemChecked(i, False)
        'Next

        chkBoxRankVariables.SetItemChecked(cb.SelectedIndex, Not isChecked)

    End Sub


    'Private Sub chkBoxGroups_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim cb As CheckedListBox = TryCast(sender, CheckedListBox)
    '    Dim isChecked As Boolean = chkBoxGroups.GetItemChecked(cb.SelectedIndex)
    '    For i As Integer = 0 To chkBoxGroups.Items.Count - 1
    '        chkBoxGroups.SetItemChecked(i, False)
    '    Next

    '    chkBoxGroups.SetItemChecked(cb.SelectedIndex, Not isChecked)
    'End Sub

    'Private Function BuildRefListNames(ByRef cb As ComboBox, configKey As String) As ComboBox
    '    Dim names As String = ConfigurationManager.AppSettings(configKey).Replace("^", "and")
    '    Dim refListNames As String() = names.Split(CChar(";"))
    '    For i As Integer = 0 To refListNames.Length - 1
    '        cb.Items.Add(refListNames(i))
    '    Next
    '    Return cb
    'End Function


    Private Sub btnRadarChart_Click(sender As Object, e As EventArgs) Handles btnRadarChart.Click
        Dim keepOpen As Boolean = False
        Try
            Dim rankVariables As New List(Of Integer)
            For Each checkedItem As Object In chkBoxRankVariables.CheckedItems
                For Each targetingItem As TargetingItem In _kpiList
                    If targetingItem.Name = checkedItem Then
                        rankVariables.Add(targetingItem.KpiId) 'these will be the VKey
                        Exit For
                    End If
                Next
            Next
            If rankVariables.Count < 3 Then
                keepOpen = True
                MsgBox("Please select at least 3 Rank Variables and try again.")
                Return
            End If

            If rankVariables.Count > 10 Then
                keepOpen = True
                MsgBox("Please select no more than 10 Rank Variables and try again.")
                Return
            End If
            'For i As Integer = 0 To chkBoxRankVariables.Items.Count - 1
            '    Dim z = chkBoxRankVariables.Items
            '    If chkBoxRankVariables.GetItemChecked(i) Then
            '        rankVariables.Add(chkBoxRankVariables(i).Tag)
            '    End If
            'Next
            'Dim group As String = String.Empty
            'For i As Integer = 0 To chkBoxGroups.Items.Count - 1
            '    If chkBoxKpis.GetItemChecked(i) Then
            '        group = _groupsParts(i)
            '        Exit For
            '    End If
            'Next

            Dim forms As Form() = frmMain.MdiChildren()
            For Each frm As Form In forms
                If frm.Name = "Main" And frm.Visible = True Then 'ughhh
                    _main = frm
                    Exit For
                End If
            Next
            _main.RadarChtVariables = New RadarChartVariables()

            Dim item As TargetingItem = cboPeerGroups.SelectedItem
            'item.RefListId  '"NSA14    Area    USA"
            'Dim peerName As String = item.Name.Substring(0, InStr(item.Name, _cboDelim) - 1)

            _main.RadarChtVariables.PeerGroup = item.PeerGroupId '.Name '.Replace(peerName, item.PeerGroupId.ToString())
            _main.RadarChtVariables.RankVariables = rankVariables
            '_main.RadarChtVariables.Group = group
            '_main.RadarChtVariables.RankBreak = cboRankBreaks.Text
            '_main.RadarChtVariables.BreakValue = cboBreakValues.Text
            '_main.RadarChtVariables.RankVariable = cboRankVariables.Text
            _main.RadarChtVariables.GetMonth = chkMonth.Checked
            _main.RadarChtVariables.GetYtd = chkYTD.Checked
            _main.RadarChtVariables.GetStudyEquivalentAkaRollingAve = chkRollingAve.Checked
            _main.RadarChtVariables.MonthColor = cboMonthColor.Text
            _main.RadarChtVariables.YtdColor = cboYtdColor.Text
            _main.RadarChtVariables.StudyEquivalentAkaRollingAveColor = cboAveColor.Text


            '=======================DisplayRefnumChart()
            'Dim breakValue As String = "USA"
            'Dim study As String = "NSA"

            'Dim fields As New List(Of String)
            'If field1.Length > 0 Then fields.Add(field1)
            'If field2.Length > 0 Then fields.Add(field2)
            'If field3.Length > 0 Then fields.Add(field3)
            'If field4.Length > 0 Then fields.Add(field4)
            'If field5.Length > 0 Then fields.Add(field5)

            'Dim parts As String() = ConfigurationManager.AppSettings("RadarKpis").Split(CChar(";"))
            'Dim kpiNames As New List(Of String)
            'For Each element As String In parts
            '    kpiNames.Add(element)
            'Next

            'url = RadarChartUrl(fields, chartName, startDate, endDate, currency, UOM, senario, ChartColor, AvgColor, YtdColor, TargetColor, study, breakValue, kpiNames)


        Catch ex As Exception
            MsgBox("Error in RadarChartChoices: " + ex.Message)
        Finally
            If Not keepOpen Then
                Me.Visible = False
                Me.Close()
                Me.Dispose()
            End If
        End Try

    End Sub

    'duplicated in Designer.vb:
    'Protected Overridable Overloads Sub Dispose( _
    'ByVal disposing As Boolean)
    '    If Not Me.disposed Then
    '        If disposing Then
    '            managedResource.Dispose()
    '        End If
    '        ' Add code here to release the unmanaged resource.
    '        'I don't have any here:  unmanagedResource = IntPtr.Zero
    '        ' Note that this is not thread safe. 
    '    End If
    '    Me.disposed = True
    'End Sub

#Region " IDisposable Support " 
    ' Do not change or add Overridable to these methods. 
    ' Put cleanup code in Dispose(ByVal disposing As Boolean). 
    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub
#End Region
    Private Class TargetingItem
        Private _name As String = String.Empty
        Public Property Name As String
            Get
                Return _name
            End Get
            Set(value As String)
                _name = value
            End Set
        End Property

        Private _value As Integer
        Public Property Value As Integer
            Get
                Return _value
            End Get
            Set(value As Integer)
                _value = value
            End Set
        End Property

        Private _peerGroupId As Integer
        Public Property PeerGroupId As Integer
            Get
                Return _peerGroupId
            End Get
            Set(value As Integer)
                _peerGroupId = value
            End Set
        End Property

        Private _kpiId As Integer
        Public Property KpiId As Integer
            Get
                Return _kpiId
            End Get
            Set(value As Integer)
                _kpiId = value
            End Set
        End Property

        Public Sub New(name As String, value As Integer, peerGroupId As Integer)
            Me.Name = name
            Me.Value = value
            Me.PeerGroupId = PeerGroupId
        End Sub

        Public Sub New(kpiId As Integer, name As String)
            Me.KpiId = kpiId
            Me.Name = name
        End Sub

        Public Overrides Function ToString() As String
            '// Generates the text shown in the combo box
            Return Name
        End Function

    End Class
End Class