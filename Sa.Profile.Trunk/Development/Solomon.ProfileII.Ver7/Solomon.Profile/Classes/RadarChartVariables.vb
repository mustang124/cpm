﻿Imports System.Collections.Generic

Public Class RadarChartVariables
    Public PeerGroup As Integer
    'Public KPIs As List(Of String)
    'Public Group As String
    Public RankBreak As String
    Public BreakValue As String
    Public RankVariables As List(Of Integer)
    Public GetMonth As Boolean
    Public GetYtd As Boolean
    Public GetStudyEquivalentAkaRollingAve As Boolean
    Public MonthColor As String
    Public YtdColor As String
    Public StudyEquivalentAkaRollingAveColor As String
End Class
