Option Explicit On




Friend Class SA_UserDefined
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents dgUserDefined As System.Windows.Forms.DataGridView
    Friend WithEvents tcUserDefined As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents gbTools As System.Windows.Forms.GroupBox
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents rbLinks As System.Windows.Forms.RadioButton
    Friend WithEvents rbData As System.Windows.Forms.RadioButton
    Friend WithEvents lblWorkBook As System.Windows.Forms.Label
    Friend WithEvents cbSheets As System.Windows.Forms.ComboBox
    Friend WithEvents tbFilePath As System.Windows.Forms.TextBox
    Friend WithEvents lblWorkSheet As System.Windows.Forms.Label
    Friend WithEvents HeaderText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VariableDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DecPlaces As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RptValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RptValue_Target As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RptValue_Avg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RptValue_YTD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgUserDefined_Maps As System.Windows.Forms.DataGridView
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lkStartRow As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkEndRow As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkHeaderText As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkVariableDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkDecPlaces As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkRptValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkRptValue_Target As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkRptValue_Avg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkRptValue_YTD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblHeaderUser As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_UserDefined))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.gbTools = New System.Windows.Forms.GroupBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.lblView = New System.Windows.Forms.Label
        Me.btnEdit = New System.Windows.Forms.Button
        Me.rbLinks = New System.Windows.Forms.RadioButton
        Me.lblWorkBook = New System.Windows.Forms.Label
        Me.rbData = New System.Windows.Forms.RadioButton
        Me.cbSheets = New System.Windows.Forms.ComboBox
        Me.tbFilePath = New System.Windows.Forms.TextBox
        Me.lblWorkSheet = New System.Windows.Forms.Label
        Me.dgUserDefined = New System.Windows.Forms.DataGridView
        Me.HeaderText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.VariableDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DecPlaces = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RptValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RptValue_Target = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RptValue_Avg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RptValue_YTD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tcUserDefined = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.dgUserDefined_Maps = New System.Windows.Forms.DataGridView
        Me.lkStartRow = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkEndRow = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkHeaderText = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkVariableDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkDecPlaces = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkRptValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkRptValue_Target = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkRptValue_Avg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkRptValue_YTD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblHeaderUser = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.pnlTools.SuspendLayout()
        Me.gbTools.SuspendLayout()
        CType(Me.dgUserDefined, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcUserDefined.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgUserDefined_Maps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTools
        '
        Me.pnlTools.BackColor = System.Drawing.SystemColors.Control
        Me.pnlTools.Controls.Add(Me.gbTools)
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 105)
        Me.pnlTools.TabIndex = 883
        '
        'gbTools
        '
        Me.gbTools.BackColor = System.Drawing.SystemColors.Control
        Me.gbTools.Controls.Add(Me.btnBrowse)
        Me.gbTools.Controls.Add(Me.chkComplete)
        Me.gbTools.Controls.Add(Me.lblView)
        Me.gbTools.Controls.Add(Me.btnEdit)
        Me.gbTools.Controls.Add(Me.rbLinks)
        Me.gbTools.Controls.Add(Me.lblWorkBook)
        Me.gbTools.Controls.Add(Me.rbData)
        Me.gbTools.Controls.Add(Me.cbSheets)
        Me.gbTools.Controls.Add(Me.tbFilePath)
        Me.gbTools.Controls.Add(Me.lblWorkSheet)
        Me.gbTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTools.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbTools.Location = New System.Drawing.Point(0, 0)
        Me.gbTools.Name = "gbTools"
        Me.gbTools.Padding = New System.Windows.Forms.Padding(0, 4, 0, 8)
        Me.gbTools.Size = New System.Drawing.Size(742, 99)
        Me.gbTools.TabIndex = 2
        Me.gbTools.TabStop = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(711, 15)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(21, 21)
        Me.btnBrowse.TabIndex = 966
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for bridge workbook")
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'chkComplete
        '
        Me.chkComplete.AutoSize = True
        Me.chkComplete.BackColor = System.Drawing.Color.Transparent
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkComplete.Location = New System.Drawing.Point(9, 49)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(136, 17)
        Me.chkComplete.TabIndex = 965
        Me.chkComplete.Text = "Check when completed"
        Me.chkComplete.UseVisualStyleBackColor = False
        '
        'lblView
        '
        Me.lblView.AutoSize = True
        Me.lblView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblView.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblView.Location = New System.Drawing.Point(6, 72)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(33, 13)
        Me.lblView.TabIndex = 949
        Me.lblView.Text = "View"
        Me.lblView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.Image = Global.Solomon.Profile.My.Resources.Resources.log
        Me.btnEdit.Location = New System.Drawing.Point(6, 15)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(134, 28)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.TabStop = False
        Me.btnEdit.Text = "Remove Values Only"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnEdit, "Removes numerical values only")
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'rbLinks
        '
        Me.rbLinks.AutoSize = True
        Me.rbLinks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLinks.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbLinks.Location = New System.Drawing.Point(92, 70)
        Me.rbLinks.Name = "rbLinks"
        Me.rbLinks.Size = New System.Drawing.Size(53, 17)
        Me.rbLinks.TabIndex = 948
        Me.rbLinks.Text = "LINKS"
        Me.rbLinks.UseVisualStyleBackColor = True
        '
        'lblWorkBook
        '
        Me.lblWorkBook.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWorkBook.AutoSize = True
        Me.lblWorkBook.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkBook.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblWorkBook.Location = New System.Drawing.Point(389, 18)
        Me.lblWorkBook.Name = "lblWorkBook"
        Me.lblWorkBook.Size = New System.Drawing.Size(65, 13)
        Me.lblWorkBook.TabIndex = 945
        Me.lblWorkBook.Text = "Workbook"
        Me.lblWorkBook.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbData
        '
        Me.rbData.AutoSize = True
        Me.rbData.Checked = True
        Me.rbData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbData.Location = New System.Drawing.Point(41, 70)
        Me.rbData.Name = "rbData"
        Me.rbData.Size = New System.Drawing.Size(52, 17)
        Me.rbData.TabIndex = 947
        Me.rbData.TabStop = True
        Me.rbData.Text = "DATA"
        Me.rbData.UseVisualStyleBackColor = True
        '
        'cbSheets
        '
        Me.cbSheets.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets.FormattingEnabled = True
        Me.cbSheets.Location = New System.Drawing.Point(464, 42)
        Me.cbSheets.Name = "cbSheets"
        Me.cbSheets.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets.TabIndex = 8
        '
        'tbFilePath
        '
        Me.tbFilePath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFilePath.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbFilePath.Location = New System.Drawing.Point(464, 15)
        Me.tbFilePath.Name = "tbFilePath"
        Me.tbFilePath.ReadOnly = True
        Me.tbFilePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbFilePath.Size = New System.Drawing.Size(241, 21)
        Me.tbFilePath.TabIndex = 6
        '
        'lblWorkSheet
        '
        Me.lblWorkSheet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWorkSheet.AutoSize = True
        Me.lblWorkSheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkSheet.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblWorkSheet.Location = New System.Drawing.Point(389, 45)
        Me.lblWorkSheet.Name = "lblWorkSheet"
        Me.lblWorkSheet.Size = New System.Drawing.Size(69, 13)
        Me.lblWorkSheet.TabIndex = 946
        Me.lblWorkSheet.Text = "Worksheet"
        Me.lblWorkSheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgUserDefined
        '
        Me.dgUserDefined.AllowUserToResizeRows = False
        Me.dgUserDefined.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgUserDefined.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgUserDefined.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgUserDefined.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.HeaderText, Me.VariableDesc, Me.DecPlaces, Me.RptValue, Me.RptValue_Target, Me.RptValue_Avg, Me.RptValue_YTD})
        Me.dgUserDefined.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgUserDefined.Location = New System.Drawing.Point(6, 36)
        Me.dgUserDefined.MultiSelect = False
        Me.dgUserDefined.Name = "dgUserDefined"
        Me.dgUserDefined.RowHeadersWidth = 25
        Me.dgUserDefined.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.MediumBlue
        Me.dgUserDefined.RowsDefaultCellStyle = DataGridViewCellStyle9
        Me.dgUserDefined.Size = New System.Drawing.Size(722, 383)
        Me.dgUserDefined.TabIndex = 947
        '
        'HeaderText
        '
        Me.HeaderText.DataPropertyName = "HeaderText"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.HeaderText.DefaultCellStyle = DataGridViewCellStyle2
        Me.HeaderText.FillWeight = 150.0!
        Me.HeaderText.HeaderText = "Header Text"
        Me.HeaderText.Name = "HeaderText"
        Me.HeaderText.Width = 150
        '
        'VariableDesc
        '
        Me.VariableDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.VariableDesc.DataPropertyName = "VariableDesc"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.VariableDesc.DefaultCellStyle = DataGridViewCellStyle3
        Me.VariableDesc.FillWeight = 200.0!
        Me.VariableDesc.HeaderText = "Variable Description"
        Me.VariableDesc.Name = "VariableDesc"
        '
        'DecPlaces
        '
        Me.DecPlaces.DataPropertyName = "DecPlaces"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DecPlaces.DefaultCellStyle = DataGridViewCellStyle4
        Me.DecPlaces.FillWeight = 50.0!
        Me.DecPlaces.HeaderText = "Decimal Places"
        Me.DecPlaces.Name = "DecPlaces"
        Me.DecPlaces.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DecPlaces.Width = 50
        '
        'RptValue
        '
        Me.RptValue.DataPropertyName = "RptValue"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.RptValue.DefaultCellStyle = DataGridViewCellStyle5
        Me.RptValue.FillWeight = 65.0!
        Me.RptValue.HeaderText = "Current Month"
        Me.RptValue.Name = "RptValue"
        Me.RptValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RptValue.Width = 65
        '
        'RptValue_Target
        '
        Me.RptValue_Target.DataPropertyName = "RptValue_Target"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.RptValue_Target.DefaultCellStyle = DataGridViewCellStyle6
        Me.RptValue_Target.FillWeight = 65.0!
        Me.RptValue_Target.HeaderText = "Target Value"
        Me.RptValue_Target.Name = "RptValue_Target"
        Me.RptValue_Target.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RptValue_Target.Width = 65
        '
        'RptValue_Avg
        '
        Me.RptValue_Avg.DataPropertyName = "RptValue_Avg"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.RptValue_Avg.DefaultCellStyle = DataGridViewCellStyle7
        Me.RptValue_Avg.FillWeight = 65.0!
        Me.RptValue_Avg.HeaderText = "Rolling Average"
        Me.RptValue_Avg.Name = "RptValue_Avg"
        Me.RptValue_Avg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RptValue_Avg.Width = 65
        '
        'RptValue_YTD
        '
        Me.RptValue_YTD.DataPropertyName = "RptValue_YTD"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.RptValue_YTD.DefaultCellStyle = DataGridViewCellStyle8
        Me.RptValue_YTD.FillWeight = 65.0!
        Me.RptValue_YTD.HeaderText = "Year to Date"
        Me.RptValue_YTD.Name = "RptValue_YTD"
        Me.RptValue_YTD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RptValue_YTD.Width = 65
        '
        'tcUserDefined
        '
        Me.tcUserDefined.Controls.Add(Me.TabPage1)
        Me.tcUserDefined.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcUserDefined.ItemSize = New System.Drawing.Size(140, 18)
        Me.tcUserDefined.Location = New System.Drawing.Point(4, 145)
        Me.tcUserDefined.Name = "tcUserDefined"
        Me.tcUserDefined.SelectedIndex = 0
        Me.tcUserDefined.Size = New System.Drawing.Size(742, 451)
        Me.tcUserDefined.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcUserDefined.TabIndex = 958
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.dgUserDefined)
        Me.TabPage1.Controls.Add(Me.dgUserDefined_Maps)
        Me.TabPage1.Controls.Add(Me.lblHeaderUser)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.TabPage1.Size = New System.Drawing.Size(734, 425)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "User-Defined"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'dgUserDefined_Maps
        '
        Me.dgUserDefined_Maps.AllowUserToAddRows = False
        Me.dgUserDefined_Maps.AllowUserToDeleteRows = False
        Me.dgUserDefined_Maps.AllowUserToResizeColumns = False
        Me.dgUserDefined_Maps.AllowUserToResizeRows = False
        Me.dgUserDefined_Maps.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgUserDefined_Maps.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgUserDefined_Maps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgUserDefined_Maps.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.lkStartRow, Me.lkEndRow, Me.lkHeaderText, Me.lkVariableDesc, Me.lkDecPlaces, Me.lkRptValue, Me.lkRptValue_Target, Me.lkRptValue_Avg, Me.lkRptValue_YTD})
        Me.dgUserDefined_Maps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgUserDefined_Maps.Location = New System.Drawing.Point(6, 36)
        Me.dgUserDefined_Maps.MultiSelect = False
        Me.dgUserDefined_Maps.Name = "dgUserDefined_Maps"
        Me.dgUserDefined_Maps.RowHeadersVisible = False
        Me.dgUserDefined_Maps.RowHeadersWidth = 25
        Me.dgUserDefined_Maps.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.MediumBlue
        Me.dgUserDefined_Maps.RowsDefaultCellStyle = DataGridViewCellStyle20
        Me.dgUserDefined_Maps.Size = New System.Drawing.Size(722, 383)
        Me.dgUserDefined_Maps.TabIndex = 949
        Me.dgUserDefined_Maps.Visible = False
        '
        'lkStartRow
        '
        Me.lkStartRow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkStartRow.DataPropertyName = "lkStartRow"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkStartRow.DefaultCellStyle = DataGridViewCellStyle11
        Me.lkStartRow.HeaderText = "Starting Row (integer)"
        Me.lkStartRow.Name = "lkStartRow"
        Me.lkStartRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkEndRow
        '
        Me.lkEndRow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkEndRow.DataPropertyName = "lkEndRow"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkEndRow.DefaultCellStyle = DataGridViewCellStyle12
        Me.lkEndRow.HeaderText = "Ending Row (integer)"
        Me.lkEndRow.Name = "lkEndRow"
        Me.lkEndRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkHeaderText
        '
        Me.lkHeaderText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkHeaderText.DataPropertyName = "lkHeaderText"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkHeaderText.DefaultCellStyle = DataGridViewCellStyle13
        Me.lkHeaderText.HeaderText = "Header Text (letter)"
        Me.lkHeaderText.Name = "lkHeaderText"
        Me.lkHeaderText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkVariableDesc
        '
        Me.lkVariableDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkVariableDesc.DataPropertyName = "lkVariableDesc"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkVariableDesc.DefaultCellStyle = DataGridViewCellStyle14
        Me.lkVariableDesc.HeaderText = "Variable Description (letter)"
        Me.lkVariableDesc.Name = "lkVariableDesc"
        Me.lkVariableDesc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkDecPlaces
        '
        Me.lkDecPlaces.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkDecPlaces.DataPropertyName = "lkDecPlaces"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkDecPlaces.DefaultCellStyle = DataGridViewCellStyle15
        Me.lkDecPlaces.HeaderText = "Decimal Places (letter)"
        Me.lkDecPlaces.Name = "lkDecPlaces"
        Me.lkDecPlaces.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkRptValue
        '
        Me.lkRptValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkRptValue.DataPropertyName = "lkRptValue"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkRptValue.DefaultCellStyle = DataGridViewCellStyle16
        Me.lkRptValue.HeaderText = "Current Month (letter)"
        Me.lkRptValue.Name = "lkRptValue"
        Me.lkRptValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkRptValue_Target
        '
        Me.lkRptValue_Target.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkRptValue_Target.DataPropertyName = "lkRptValue_Target"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkRptValue_Target.DefaultCellStyle = DataGridViewCellStyle17
        Me.lkRptValue_Target.HeaderText = "Target Value (letter)"
        Me.lkRptValue_Target.Name = "lkRptValue_Target"
        Me.lkRptValue_Target.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkRptValue_Avg
        '
        Me.lkRptValue_Avg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkRptValue_Avg.DataPropertyName = "lkRptValue_Avg"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkRptValue_Avg.DefaultCellStyle = DataGridViewCellStyle18
        Me.lkRptValue_Avg.HeaderText = "Rolling Average (letter)"
        Me.lkRptValue_Avg.Name = "lkRptValue_Avg"
        Me.lkRptValue_Avg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkRptValue_YTD
        '
        Me.lkRptValue_YTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkRptValue_YTD.DataPropertyName = "lkRptValue_YTD"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.lkRptValue_YTD.DefaultCellStyle = DataGridViewCellStyle19
        Me.lkRptValue_YTD.HeaderText = "Year to Date (letter)"
        Me.lkRptValue_YTD.Name = "lkRptValue_YTD"
        Me.lkRptValue_YTD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lblHeaderUser
        '
        Me.lblHeaderUser.BackColor = System.Drawing.Color.Transparent
        Me.lblHeaderUser.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblHeaderUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderUser.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblHeaderUser.Location = New System.Drawing.Point(6, 0)
        Me.lblHeaderUser.Name = "lblHeaderUser"
        Me.lblHeaderUser.Size = New System.Drawing.Size(722, 36)
        Me.lblHeaderUser.TabIndex = 869
        Me.lblHeaderUser.Text = "User-Defined Inputs/Outputs"
        Me.lblHeaderUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon.Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon.Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(500, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 2
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Update input form with linked values")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1035
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(500, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "User-Defined Inputs/Outputs"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_UserDefined
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tcUserDefined)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Name = "SA_UserDefined"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.pnlTools.ResumeLayout(False)
        Me.gbTools.ResumeLayout(False)
        Me.gbTools.PerformLayout()
        CType(Me.dgUserDefined, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcUserDefined.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dgUserDefined_Maps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private MyParent As Main
    Private IsLoaded, HasRights As Boolean
    Friend UserDefinedNotSaved As Boolean
    Private dvWB As New DataView

    Friend Sub SetDataSet()
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.UserDefinedRights

        Dim dv1 As New DataView(MyParent.dsUserDefined.Tables("UserDefined"))
        'dv1.Sort = "SortKey ASC"
        dgUserDefined.DataSource = dv1 ' MyParent.dsUserDefined.Tables("UserDefined")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='User-Defined Inputs/Outputs'")
        chkComplete.Checked = CBool(row(0)!Checked)

        dgUserDefined_Maps.DataSource = MyParent.dsMappings.Tables("UserDefined")

        'tbFilePath.Text = My.Settings.workbookUserDefined
        'cbSheets.Text = My.Settings.worksheetUserDefined

        Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = 'User'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            If Not System.IO.File.Exists(dr(0).Item(tbFilePath.Name).ToString) Then
                'Throw New Exception("Mapping file problem: Cannot find file at " & dr(0).Item(tbFilePath.Name).ToString)
            End If
            tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
            cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
        End If

        IsLoaded = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveUserDefined()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim result As DialogResult = ProfileMsgBox("YN", "CLEAR", "user-defined values and averages")
        If result = vbNo Then Exit Sub
        Dim row As DataRow
        For Each row In MyParent.dsUserDefined.Tables("UserDefined").Rows
            row!RptValue = DBNull.Value
            row!RptValue_AVG = DBNull.Value
            row!RptValue_YTD = DBNull.Value
        Next
        ChangesMade(btnSave, chkComplete, UserDefinedNotSaved)
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim result As DialogResult
        If rbData.Checked Then
            result = ProfileMsgBox("YN", "CLEAR", "user-defined inputs/outputs data")
            If result = DialogResult.No Then Exit Sub
            ' 20090115 RRH - .Clear() also 'accepts changes'... MyParent.dsUserDefined.Tables("UserDefined").Clear()
            For Each row As DataRow In MyParent.dsUserDefined.Tables("UserDefined").Rows
                row.Delete()
            Next row

        Else
            result = ProfileMsgBox("YN", "CLEAR", "user-defined inputs/outputs links")
            If result = DialogResult.No Then Exit Sub
            With MyParent.dsMappings.Tables("UserDefined").Rows(0)
                !lkStartRow = DBNull.Value
                !lkEndRow = DBNull.Value
                !lkHeaderText = DBNull.Value
                !lkVariableDesc = DBNull.Value
                !lkRptValue = DBNull.Value
                !lkRptValue_Target = DBNull.Value
                !lkRptValue_Avg = DBNull.Value
                !lkRptValue_YTD = DBNull.Value
                !lkDecPlaces = DBNull.Value
            End With
        End If

        ChangesMade(btnSave, chkComplete, UserDefinedNotSaved)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        MyParent.PrintPreviews("User-Defined Inputs/Outputs", Nothing)
    End Sub

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        If IsLoaded = False Then Exit Sub
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "User-Defined Inputs/Outputs")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='User-Defined Inputs/Outputs'")
        row(0)!Checked = chk.Checked
        btnSave.ForeColor = Color.White
        btnSave.BackColor = Color.Red
        UserDefinedNotSaved = True
    End Sub

    Private Sub dgUserDefined_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgUserDefined.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, UserDefinedNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If UserDefinedNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "User-defined Inputs/Outputs")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveUserDefined()
                Case DialogResult.No
                    MyParent.dsUserDefined.RejectChanges()
                    MyParent.dsMappings.Tables("UserDefined").RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, UserDefinedNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub SA_UserDefined_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.btnSave.TabIndex = 0
        'Me.btnDelete.TabIndex = 1
        Me.btnEdit.TabIndex = 2
        Me.btnClear.TabIndex = 3

        Me.tbFilePath.TabIndex = 4
        Me.btnBrowse.TabIndex = 5
        Me.cbSheets.TabIndex = 6
        'Me.btnView.TabIndex = 7

        Me.dgUserDefined.TabIndex = 8
        Me.btnPreview.TabIndex = 9
        Me.chkComplete.TabIndex = 10

        Me.btnClose.TabIndex = 11

    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub viewLinksData()

        dgUserDefined.Visible = rbData.Checked
        dgUserDefined_Maps.Visible = Not rbData.Checked

        btnPreview.Enabled = rbData.Checked

        If rbData.Checked Then
            lblHeaderUser.Text = "User-Defined Inputs/Outputs:   DATA"
        Else
            lblHeaderUser.Text = "User-Defined Inputs/Outputs:   LINKS"
        End If

        If IsLoaded And HasRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)

    End Sub

    Friend Function ImportUserDefined() As Boolean
        Dim MyExcel As Object = OpenExcel()
        Dim MyWB As Object = OpenWorkbook(MyExcel, tbFilePath.Text)
        Dim MyWS As Object = OpenSheet(MyExcel, MyWB, cbSheets.Text)

        If MyExcel Is Nothing Then Exit Function
        If MyWB Is Nothing Then Exit Function
        If MyWS Is Nothing Then Exit Function

        Try
            Dim newDV_LU As New DataView
            Dim row As DataRow = MyParent.dsMappings.Tables("UserDefined").Rows(0)
            Dim newrow As DataRow
            Dim dt As DataTable = MyParent.dsUserDefined.Tables("UserDefined")
            dt.Clear()
            ChangesMade(btnSave, chkComplete, UserDefinedNotSaved)

            For i As Integer = CInt(row!lkStartRow) To CInt(row!lkEndRow)

                If Not IsDBNull(MyWS.Range(row!lkHeaderText.ToString & i.ToString).Value) AndAlso CStr(MyWS.Range(row!lkHeaderText.ToString & i.ToString).Value) <> "" Then

                    newrow = dt.NewRow
                    newrow!HeaderText = CStr(MyWS.Range(row!lkHeaderText.ToString & i.ToString).Value)
                    newrow!VariableDesc = CStr(MyWS.Range(row!lkVariableDesc.ToString & i.ToString).Value)
                    newrow!DecPlaces = CInt(CleanseNumericDataFromBridgeFile(MyWS.Range(row!lkDecPlaces.ToString & i.ToString).Value, cbSheets.Text, row!lkDecPlaces.ToString() & i.ToString()))
                    newrow!RptValue = CDbl(CleanseNumericDataFromBridgeFile(MyWS.Range(row!lkRptValue.ToString & i.ToString).Value, cbSheets.Text, row!lkRptValue.ToString() & i.ToString()))
                    newrow!RptValue_Target = CDbl(CleanseNumericDataFromBridgeFile(MyWS.Range(row!lkRptValue_Target.ToString & i.ToString).Value, cbSheets.Text, row!lkRptValue_Target.ToString() & i.ToString()))
                    newrow!RptValue_Avg = CDbl(CleanseNumericDataFromBridgeFile(MyWS.Range(row!lkRptValue_Avg.ToString & i.ToString).Value, cbSheets.Text, row!lkRptValue_Avg.ToString() & i.ToString()))
                    newrow!RptValue_YTD = CDbl(CleanseNumericDataFromBridgeFile(MyWS.Range(row!lkRptValue_YTD.ToString & i.ToString).Value, cbSheets.Text, row!lkRptValue_YTD.ToString() & i.ToString()))
                    dt.Rows.Add(newrow)

                End If

            Next i

            ImportUserDefined = True
        Catch ex As Exception
            ProfileMsgBox("CRIT", "MAPPING", "user-defined inputs/outputs")
        End Try
CleanupExcel:
        CloseExcel(MyExcel, MyWB, MyWS)
        Return ImportUserDefined
    End Function

    Private Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If ProfileMsgBox("YN", "IMPORT", "user-defined inputs/outputs") = DialogResult.Yes Then ImportUserDefined()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        selectWorkBook(Me.Cursor, tbFilePath, cbSheets, MyParent.MyParent.pnlProgress, dvWB)
    End Sub

    Private Sub tbFilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged

        saveWkbWksReferences(MyParent.dsMappings, "User", tbFilePath.Name, tbFilePath.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, UserDefinedNotSaved)

        My.Settings.workbookUserDefined = tbFilePath.Text
        My.Settings.Save()
    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged

        saveWkbWksReferences(MyParent.dsMappings, "User", cbSheets.Name, cbSheets.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, UserDefinedNotSaved)

        My.Settings.worksheetUserDefined = cbSheets.Text
        My.Settings.Save()
    End Sub

    Private Sub dgUserDefined_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgUserDefined.UserDeletingRow
        If HasRights Then
            ChangesMade(btnSave, chkComplete, UserDefinedNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgUserDefined_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgUserDefined.DataError
        dgvDataError(sender, e)
    End Sub
End Class
