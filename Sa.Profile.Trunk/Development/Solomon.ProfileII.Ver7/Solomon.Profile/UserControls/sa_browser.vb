Option Compare Binary
Option Explicit On
Option Strict On

Imports WSX509 = Microsoft.Web.Services2.Security.X509
Imports System.Configuration
Imports System.IO
Imports System.Collections.Generic

'####################################
'For some strange reason, these couldn't be referenced via a dll (from another project). Their projects had to be added 
' to this solution else you get this error:
'"Reference required to assembly" . . . "containing the base class" . . . "Add one to your project"	
Imports Solomon.ProfileII.Proxies
Imports Solomon.ProfileII.Contracts
'###################################


Friend Class SA_Browser
    Implements IDockable

    Private _clientKey As String = ""
    Private _title As String = String.Empty
    Private _delim As String = Chr(14)
    Private _graphicType As Integer = 0  '0=table, 1=report, 2=custom report, 3=column/stacked column chart, 4 = radar chart

    Private _ordinal As Integer = 0
    Public Property Ordinal As Integer Implements IDockable.Ordinal
        Get
            Return _ordinal
        End Get
        Set(value As Integer)
            _ordinal = value
            Me.Tag = _ordinal.ToString()
        End Set
    End Property

    Private _url As String = Nothing
    Public Property Url As String Implements IDockable.Url
        Get
            Return _url
        End Get
        Set(value As String)
            _url = value
        End Set
    End Property

    Private _callingPanel As IDocker
    Public Property CallingPanel As IDocker
        Get
            Return _callingPanel
        End Get
        Set(value As IDocker)
            _callingPanel = value
        End Set
    End Property
    Private _callingDashboard As Dashboard
    Public Property CallingDashboard As Dashboard
        Get
            Return _callingDashboard
        End Get
        Set(value As Dashboard)
            _callingDashboard = value
            btnAddToDashboard.Visible = False
        End Set
    End Property

    Private Enum Exec
        OLECMDID_OPTICAL_ZOOM = 63
    End Enum
    Private Enum ExecOpt
        OLECMDEXECOPT_DODEFAULT = 0
        OLECMDEXECOPT_PROMPTUSER = 1
        OLECMDEXECOPT_DONTPROMPTUSER = 2
        OLECMDEXECOPT_SHOWHELP = 3
    End Enum



    Friend Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        If Not modGeneral.AdminRights Then
            btnAddToDashboard.Visible = False
        End If

        '20101004 RRH: Changed _CERT Path If IO.File.Exists("_CERT\" & Policy.GetFileName()) Then
        '20101004 RRH: Changed _CERT Path Dim reader As New IO.StreamReader("_CERT\" & Policy.GetFileName())
        If IO.File.Exists(modAppDeclarations.pathCert & Policy.GetFileName()) Then
            Dim reader As New IO.StreamReader(modAppDeclarations.pathCert & Policy.GetFileName())
            Dim password As String = ""

            If reader.Peek <> -1 Then
                _clientKey = reader.ReadLine
                Console.WriteLine(_clientKey)
                reader.Close()
            Else
                Throw New Exception("A key was not found")
            End If

            wbWebBrowser.Navigate("about:blank")
        End If

    End Sub

    Private Sub StartUp()
        '20101004 RRH: Changed _CERT Path Dim reader As New IO.StreamReader("_CERT\" & Policy.GetFileName())
        Dim reader As New IO.StreamReader(modAppDeclarations.pathCert & Policy.GetFileName())
        Dim password As String = ""

        If reader.Peek <> -1 Then
            _clientKey = reader.ReadLine
            _clientKey = Replace(_clientKey, "+", "%2B")
            _clientKey = Replace(_clientKey, "/", "%2F")
            _clientKey = Replace(_clientKey, "\", "%5C")

            Console.WriteLine(_clientKey)
            reader.Close()
        Else
            Throw New Exception("A key was not found")
        End If
    End Sub

    '20120110 RRH Removing certificate validation
    'Friend Sub InstallCertificate()
    '    Dim store As WSX509.X509CertificateStore = WSX509.X509CertificateStore.CurrentUserStore(WSX509.X509CertificateStore.RootStore)
    '    Dim fopen As Boolean = store.OpenRead

    '    Dim cers As WSX509.X509CertificateCollection = store.FindCertificateBySubjectString("SolomonAssociates")

    '    'Install certificate if not available
    '    If (cers.Count = 0) Then

    '        Dim result As DialogResult = ProfileMsgBox("YN", "NOCERT", "")
    '        If result = DialogResult.Yes Then
    '            '20101004 RRH: Changed _CERT Path System.Diagnostics.Process.Start("_CERT/Solomon Profile� II.cer")
    '            System.Diagnostics.Process.Start(modAppDeclarations.pathCert + "Solomon Profile� II.cer")
    '        End If

    '    End If

    'End Sub




    Friend Sub DisplayRefnumChart(ByVal refineryID As String, _
                                   ByVal location As String, _
                                   ByVal dataset As String, _
                                   ByVal senario As String, _
                                   ByVal periodStart As Date, _
                                   ByVal periodEnd As Date, _
                                   ByVal chartRow As DataRow, _
                                   ByVal UOM As String, _
                                   ByVal currency As String, _
                                   ByVal studyYear As String, _
                                   ByVal TargetField As String, _
                                   ByVal AvgField As String, _
                                   ByVal YTDField As String,
                                   Optional ByVal ChartType As String = "",
                                   Optional ByVal ChartColor As String = "",
                                   Optional ByVal AvgColor As String = "",
                                   Optional ByVal YtdColor As String = "",
                                   Optional ByVal TargetColor As String = "",
                                   Optional ByVal showChartDataTable As Boolean = True)

        Dim v1, v2, v3, v4, v5 As String
        Dim u As Integer

        For u = 0 To chartRow.Table.Columns.Count - 1
            Dim t As String = chartRow.Table.Columns(u).DataType.ToString()
            If chartRow.Table.Columns(u).DataType = GetType(System.Int32) Then
                If IsDBNull(chartRow(u)) Then
                    chartRow(u) = DBNull.Value
                End If
            ElseIf chartRow.Table.Columns(u).DataType = GetType(System.String) Then
                If IsDBNull(chartRow(u)) Then
                    chartRow(u) = ""
                End If
            End If
        Next

        v1 = chartRow("ValueField1").ToString
        v2 = chartRow("ValueField2").ToString
        v3 = chartRow("ValueField3").ToString
        v4 = chartRow("ValueField4").ToString
        v5 = chartRow("ValueField5").ToString

        If Not IsDBNull(chartRow("Legend1")) Then
            If CStr(chartRow("Legend1")).Length > 0 Then
                v1 = "ISNULL(" + v1 + ",0) AS '" + chartRow("Legend1").ToString + "'"
            End If
        Else
            v1 = "ISNULL(" + v1 + ",0) AS '" + v1 + "'"
        End If

        If Not IsNothing(location.Trim) Then
            If location.Trim.Length > 0 Then
                v1 = v1.Replace("Location", location)
            End If
        End If

        If Not IsDBNull(chartRow("Legend2")) Then
            If CStr(chartRow("Legend2")).Length > 0 Then
                v2 = "ISNULL(" + v2 + ",0) AS '" + chartRow("Legend2").ToString + "'"
            End If
        Else
            v2 = "ISNULL(" + v2 + ",0) AS '" + v2 + "'"
        End If

        If Not IsDBNull(chartRow("Legend3")) Then
            If CStr(chartRow("Legend3")).Length > 0 Then
                v3 = "ISNULL(" + v3 + ",0) AS '" + chartRow("Legend3").ToString + "'"
            End If
        Else
            v1 = "ISNULL(" + v3 + ",0) AS '" + v3 + "'"
        End If

        If Not IsDBNull(chartRow("Legend4")) Then
            If CStr(chartRow("Legend4")).Length > 0 Then
                v4 = "ISNULL(" + v4 + ",0) AS '" + chartRow("Legend4").ToString + "'"
            End If
        Else
            v4 = "ISNULL(" + v4 + ",0) AS '" + v4 + "'"
        End If

        If Not IsDBNull(chartRow("Legend5")) Then
            If CStr(chartRow("Legend5")).Length > 0 Then
                v5 = "ISNULL(" + v5 + ",0) AS '" + chartRow("Legend5").ToString + "'"
            End If
        Else
            v1 = "ISNULL(" + v5 + ",0) AS '" + v5 + "'"
        End If

        DisplayRefnumChart(chartRow("ChartTitle").ToString, _
                           refineryID, _
                           location, _
                           dataset, _
                           senario, _
                           periodStart, _
                           periodEnd, _
                           studyYear, _
                           chartRow("DataTable").ToString, _
                           UOM, _
                           currency, _
                           v1, _
                           YTDField, _
                           AvgField, _
                           TargetField, _
                           v2, _
                           v3, _
                           v4, _
                           v5,
                           ChartType,
                           ChartColor,
                           AvgColor,
                           YtdColor,
                           TargetColor,
                           showChartDataTable)

    End Sub

    Friend Sub DisplayRefnumChart(ByVal chartName As String, _
                                  ByVal refineryID As String, _
                                  ByVal location As String, _
                                  ByVal dataset As String, _
                                  ByVal senario As String, _
                                  ByVal startDate As Date, _
                                  ByVal endDate As Date, _
                                  ByVal StudyYear As String, _
                                  ByVal table As String, _
                                  ByVal UOM As String, _
                                  ByVal currency As String, _
                                  ByVal field1 As String, _
                                  Optional ByVal YTD As String = "", _
                                  Optional ByVal twelveMonthAvg As String = "", _
                                  Optional ByVal target As String = "", _
                                  Optional ByVal field2 As String = "", _
                                  Optional ByVal field3 As String = "", _
                                  Optional ByVal field4 As String = "", _
                                  Optional ByVal field5 As String = "",
                                   Optional ByVal ChartType As String = "",
                                   Optional ByVal ChartColor As String = "",
                                   Optional ByVal AvgColor As String = "",
                                   Optional ByVal YtdColor As String = "",
                                   Optional ByVal TargetColor As String = "",
                                   Optional ByVal showChartDataTable As Boolean = True)




        Dim header As String = "WsP: " & _clientKey + vbCrLf

        chartName = System.Web.HttpUtility.UrlEncode(chartName)
        field1 = System.Web.HttpUtility.UrlEncode(field1)
        field2 = System.Web.HttpUtility.UrlEncode(field2)
        field3 = System.Web.HttpUtility.UrlEncode(field3)
        field4 = System.Web.HttpUtility.UrlEncode(field4)
        field5 = System.Web.HttpUtility.UrlEncode(field5)

        Dim dt As Date = DateTime.Now
        Dim page As String = ConfigurationManager.AppSettings("ChartControlUrl").ToString()

        If table = "UnitIndicators" Then
            page = page.Replace("ChartPage", "UnitIndicators")
        End If


        Dim url As String = String.Empty

        'chartName
        url = page + "?cn=" + chartName
        url += "&field1=" + field1 + "&field2=" + field2 + _
                           "&field3=" + field3 + "&field4=" + field4 + _
                           "&field5=" + field5
        url += "&sd=" + startDate.ToString("M/d/yyyy") + "&ed=" + endDate.ToString("M/d/yyyy")
        url += "&UOM=" + UOM + "&currency=" + currency

        'Methodology?
        url += "&sn=" + senario.Trim()


        url += "&YTD=" & YTD & "&TwelveMonthAvg=" & twelveMonthAvg & _
                         "&target=" & target

        url += "&tableName=" & table

        If Not showChartDataTable Then
            url += "&chrtData=N"
        End If

        Dim chartInfo As String = "&chart=" + ChartType + "|" + ChartColor + "|" + AvgColor + "|" + YtdColor + "|" + TargetColor
        url += chartInfo
        'url += "&WsP=" + _clientKey

        wbWebBrowser.Navigate(url, "", Nothing, header)
        _url = url
        _title = chartName
        _graphicType = 3

    End Sub

    Public Sub Navigate(url As String, targetFrameName As String, postData() As Byte, header As String, Optional chartName As String = "")
        wbWebBrowser.Navigate(url, targetFrameName, postData, header)
        _url = url
        _title = chartName
        _graphicType = 4
    End Sub

    Friend Sub DisplayReport(ByVal refineryID As String, _
                                 ByVal location As String, _
                                 ByVal reportCode As String, _
                                 ByVal periodStart As Date, _
                                 ByVal UOM As String, _
                                 ByVal currency As String, _
                                 ByVal studyYear As String, _
                                 ByVal dataSet As String, _
                                 ByVal Scenario As String, _
                                 ByVal includeTarget As Boolean, _
                                 ByVal includeAvg As Boolean, _
                                 ByVal includeYTD As Boolean, _
                                 Optional ByVal rowsOrdinalsToInclude As String = "")


        Dim header As String = "WsP: " & _clientKey + vbCrLf
        Dim localUrl As String = String.Empty
        If UOM.StartsWith("US") Then
            UOM = "US"
        Else
            UOM = "MET"
        End If

        Dim dt As Date = DateTime.Now
        If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            wbWebBrowser.Navigate(ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToShortDateString & _
                                                 "&ref=" & refineryID & "&ds=" & dataSet & _
                                                 "&UOM=" & UOM & "&currency=" & currency & _
                                                 "&yr=" & studyYear & _
                                                 "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                 "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt, "", Nothing, header)
        Else

            If Util.UseNonWCFWebService Then

                localUrl = ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToString("M/d/yyyy") & _
                                                       "&ds=" & dataSet & _
                                                      "&UOM=" & UOM & "&currency=" & currency & _
                                                      "&yr=" & studyYear & _
                                                      "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                      "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt

                If rowsOrdinalsToInclude.Length > 0 Then
                    localUrl += "&rows=" + rowsOrdinalsToInclude
                End If

                'In order to do Richard's 'Export to Excel' from browser control's context menu, must keep key in conx string.
                header = String.Empty
                'localUrl += "&WsP=" + _clientKey

                'Try
                '    Using sw As New StreamWriter("C:\temp\reportUrls.txt", True)
                '        sw.WriteLine(localUrl)
                '    End Using
                'Catch
                'End Try


                wbWebBrowser.Navigate(localUrl, "", Nothing, header)
            Else
                '=======================

                localUrl = "rn=" & reportCode & "&sd=" & periodStart.ToString("M/d/yyyy") & _
                                           "&ds=" & dataSet & _
                                          "&UOM=" & UOM & "&currency=" & currency & _
                                          "&yr=" & studyYear & _
                                          "&target=" & includeTarget & "&avg=" & includeAvg & _
                                          "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt

                If rowsOrdinalsToInclude.Length > 0 Then
                    localUrl += "&rows=" + rowsOrdinalsToInclude
                End If

                'In order to do Richard's 'Export to Excel' from browser control's context menu, must keep key in conx string.
                '-we will copy from web browser and paste into xls instead, so remove clientkey
                header = String.Empty
                'localUrl += "&WsP=" + _clientKey

                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfReportsEndpointName")
                ''Dim proxy As New ProfileReportsClient(endpointName) '  ServiceReference1.ProfileReportServiceClient()
                Dim result As String = String.Empty
                Dim heading As String = ""
                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()
                Try
                    'If reportCode = "Refinery Scorecard" Then
                    '    result = proxy.GetRefineryScorecardReport(localUrl, Nothing)
                    'ElseIf reportCode = "Refinery Trends Report" Then
                    '    result = proxy.GetRefineryTrendsReport(localUrl, Nothing)
                    'Else
                    result = proxy.GetReportHtml(localUrl, Nothing)
                    'End If



                    Dim inttest As Integer = result.IndexOf("runat=" & Chr(34) & "server" & Chr(34) & ">")
                    Dim myParent As Main = DirectCast(frmMain.ActiveMdiChild, Main)
                    heading = modGeneral.GetWcfReportHeading(reportCode, Scenario, myParent.txtCompany.Text.Trim(), location)
                    result = result.Replace("runat=" & Chr(34) & "server" & Chr(34) & ">", "runat=" & Chr(34) & "server" & Chr(34) & ">" & heading)
                    'localUrl = proxy.Endpoint.Address.ToString() & "?" & localUrl
                Catch ex As Exception
                    result = "<HTML>Error</HTML>"

                End Try
                wbWebBrowser.DocumentText = result
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()

                'localUrl = ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn="
                'Add http to front for dashboards if needed
                'localUrl =  localUrl &  "&WsP=" & _clientKey
            End If

        End If


        'Dim html As String = svc.GetReportHtml(localUrl)
        'wbWebBrowser.DocumentText = html

        _url = localUrl
        _title = reportCode
        _graphicType = 1
    End Sub


    Friend Sub DisplayCustomReport(ByVal refineryID As String, _
                                 ByVal location As String, _
                                 ByVal reportCode As String, _
                                 ByVal periodStart As Date, _
                                 ByVal UOM As String, _
                                 ByVal currency As String, _
                                 ByVal studyYear As String, _
                                 ByVal dataSet As String, _
                                 ByVal Scenario As String, _
                                 ByVal includeTarget As Boolean, _
                                 ByVal includeAvg As Boolean, _
                                 ByVal includeYTD As Boolean, _
                                 ByVal PeerGroupsChosen As List(Of String), _
                                 ByVal KpisChosen As List(Of String), _
                                 Optional ByVal rowsOrdinalsToInclude As String = "")


        Dim header As String = "WsP: " & _clientKey + vbCrLf

        If UOM.StartsWith("US") Then
            UOM = "US"
        Else
            UOM = "MET"
        End If

        Dim dt As Date = DateTime.Now
        If Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("testing")) Then
            wbWebBrowser.Navigate(ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToShortDateString & _
                                                 "&ref=" & refineryID & "&ds=" & dataSet & _
                                                 "&UOM=" & UOM & "&currency=" & currency & _
                                                 "&yr=" & studyYear & _
                                                 "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                 "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt, "", Nothing, header)
        Else
            Dim localUrl As String = String.Empty

            If Util.UseNonWCFWebService Then
                localUrl = ConfigurationManager.AppSettings("ReportControlUrl").ToString() + "?rn=" & reportCode & "&sd=" & periodStart.ToString("M/d/yyyy") & _
                                                       "&ds=" & dataSet & _
                                                      "&UOM=" & UOM & "&currency=" & currency & _
                                                      "&yr=" & studyYear & _
                                                      "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                      "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt
                If (Not IsNothing(rowsOrdinalsToInclude) AndAlso rowsOrdinalsToInclude.Length > 0) Then
                    localUrl += "&rows=" + rowsOrdinalsToInclude
                End If

                If PeerGroupsChosen.Count > 0 Then
                    localUrl += "&peers="
                    For Each peer As String In PeerGroupsChosen
                        localUrl += peer + _delim
                    Next
                    localUrl = localUrl.Remove(localUrl.Length - 1) 'remove trailing delim
                End If
                'If KpisChosen.Count > 0 Then
                '    url += "&kpis="
                '    For Each kpi As String In KpisChosen
                '        url += kpi + _delim
                '    Next
                '    url = url.Remove(url.Length - 1) 'remove trailing delim
                'End If

                'In order to do Richard's 'Export to Excel' from browser control's context menu, must keep key in conx string.
                '-copy and past into xls instead
                header = String.Empty
                'localUrl += "&WsP=" + _clientKey
                wbWebBrowser.Navigate(localUrl, "", Nothing, header)
            Else
                localUrl = "rn=" & reportCode & "&sd=" & periodStart.ToString("M/d/yyyy") & _
                                                       "&ds=" & dataSet & _
                                                      "&UOM=" & UOM & "&currency=" & currency & _
                                                      "&yr=" & studyYear & _
                                                      "&target=" & includeTarget & "&avg=" & includeAvg & _
                                                      "&ytd=" & includeYTD & "&SN=" & Scenario & "&TS=" & dt
                If (Not IsNothing(rowsOrdinalsToInclude) AndAlso rowsOrdinalsToInclude.Length > 0) Then
                    localUrl += "&rows=" + rowsOrdinalsToInclude
                End If

                If PeerGroupsChosen.Count > 0 Then
                    localUrl += "&peers="
                    For Each peer As String In PeerGroupsChosen
                        localUrl += peer + _delim
                    Next
                    localUrl = localUrl.Remove(localUrl.Length - 1) 'remove trailing delim
                End If
                header = String.Empty
                'localUrl += "&WsP=" + _clientKey
                'Dim endpointName As String = ConfigurationManager.AppSettings("WcfReportsEndpointName")
                'Dim proxy As New ProfileReportsClient(endpointName) '("WSHttpBinding_IProfileReportService")
                'Dim svc As ReportWebSvc.ReportService = Util.ReportServicesObject()

                Dim proxy As New ProfileClient(Util.EndpointNameToUse()) ' Dim svc As DataWebSvc.DataService = Util.DataServicesObject()

                Dim result As String = String.Empty
                Dim heading As String = ""

                heading = "<table cellSpacing=" & Chr(34) & "0" & Chr(34) & " cellPadding=" & Chr(34) & "0" & Chr(34) & " width=" & Chr(34) & "100%" & Chr(34) & " border=" & Chr(34) & "0" & Chr(34) & "><tr><td style=" & Chr(34) & "BACKGROUND-REPEAT: no-repeat" & Chr(34) & " width=" & Chr(34) & "350" & Chr(34) & " "
                heading = heading & " background = " & Chr(34) & My.Application.Info.DirectoryPath.ToString() & "\Resources\ReportBGLeft.gif" & Chr(34) & " "
                heading = heading & "bgColor=" & Chr(34) & "white" & Chr(34) & "><DIV id=" & Chr(34) & "LabelName" & Chr(34) & " style=" & Chr(34) & "DISPLAY: inline; FONT-WEIGHT: bold; FONT-SIZE: 12pt; Z-INDEX: 106; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & ">"
                heading = heading & reportCode 'Operating Expenses"
                heading = heading & "</DIV><br><DIV id=" & Chr(34) & "LabelMonth" & Chr(34) & " style=" & Chr(34) & "DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & ">"
                heading = heading & Scenario.Trim() & ": Methodology"
                heading = heading & "</DIV><br><DIV id=" & Chr(34) & "LabelDataFilter" & Chr(34) & " style=" & Chr(34) & "DISPLAY: inline; FONT-SIZE: 10pt; Z-INDEX: 105; LEFT: 1px; WIDTH: 250px; COLOR: white; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & ">"
                Dim myParent As Main = DirectCast(frmMain.ActiveMdiChild, Main)
                heading = heading & myParent.txtCompany.Text.Trim() & " - " & location
                heading = heading & "</DIV></td><TD></TD><td width=" & Chr(34) & "42%" & Chr(34) & "><div align=" & Chr(34) & "right" & Chr(34) & "><IMG height=" & Chr(34) & "50" & Chr(34) & " alt=" & Chr(34) & Chr(34) & " "
                heading = heading & "src=" & Chr(34) & My.Application.Info.DirectoryPath.ToString() & "\Resources\SA logo RECT.jpg" & Chr(34) & " "
                heading = heading & " width=" & Chr(34) & "300" & Chr(34) & "><br>"
                heading = heading & "</div><DIV id=" & Chr(34) & "DIV1" & Chr(34) & " style=" & Chr(34) & "FONT-SIZE: 10pt; FONT-FAMILY: Tahoma; HEIGHT: 16px" & Chr(34) & " align=" & Chr(34) & "right" & Chr(34) & ">"
                heading = heading & DateTime.Today.ToLongDateString()
                heading = heading & "</DIV></td></tr></table><hr width=" & Chr(34) & "100%" & Chr(34) & " color=" & Chr(34) & "#000000" & Chr(34) & " SIZE=" & Chr(34) & "1" & Chr(34) & ">"

                Try
                    If reportCode.ToUpper = "TARGETING" Then
                        'heading = heading & "<tr><td>###</td></tr>"
                        result = proxy.GetTargetingReport(localUrl, Nothing)
                    Else
                        result = proxy.GetReportHtml(localUrl, Nothing)
                    End If
                    Dim inttest As Integer = result.IndexOf("runat=" & Chr(34) & "server" & Chr(34) & ">")
                    result = result.Replace("runat=" & Chr(34) & "server" & Chr(34) & ">", "runat=" & Chr(34) & "server" & Chr(34) & ">" & heading)
                Catch ex As Exception
                    result = "<HTML>Error</HTML>"
                End Try

                wbWebBrowser.DocumentText = result
                If Not proxy.State = ServiceModel.CommunicationState.Faulted Then proxy.Close()
            End If
            _url = localUrl
            _title = reportCode
            _graphicType = 2
        End If
    End Sub



    Friend Sub DisplayInputReport(ByVal ds As DataSet, ByVal tableName As String, ByVal company As String,
                                  ByVal location As String, ByVal description As String, ByVal UOM As String,
                                  ByVal currency As String, ByVal period As Date, ByVal ht As Hashtable)
        Dim pg As New Page

        'wbWebBrowser.Navigate("about:blank")



        Dim html As String = pg.createPage(ds, tableName, _
                                                  company, _
                                                  location, _
                                                  description, _
                                                  UOM, currency, _
                                                  Month(period), Year(period), period, ht)
        wbWebBrowser.DocumentText = html
        'If IsNothing(wbWebBrowser.Document) Then
        '    wbWebBrowser.DocumentText = html
        'Else
        '    wbWebBrowser.Document.Body.InnerHtml = html
        'End If
        '_url = String.Empty 'this doesn't go to web page, it is built by Profile project using local xml
        _title = tableName
        _graphicType = 0
    End Sub

    Friend Sub PrintPage()
        wbWebBrowser.ShowPrintDialog()
    End Sub

    Friend Sub GoHome()
        lblHeader.Text = "www.SolomonOnline.com"
        wbWebBrowser.Visible = True
        wbWebBrowser.Navigate(ConfigurationManager.AppSettings("SolomonUrl").ToString())
    End Sub

    Friend Sub GoHelp()
        lblHeader.Text = "Online Help"
        wbWebBrowser.Visible = True
        wbWebBrowser.Navigate(ConfigurationManager.AppSettings("HelpUrl").ToString())
    End Sub

    Friend Sub PleaseWait()
        If IO.File.Exists(pathTemp & "Templates\PleaseWait.htm") Then _
            wbWebBrowser.Navigate(New Uri(pathTemp & "Templates\PleaseWait.htm"))
    End Sub

    Private Sub GoBlank()
        wbWebBrowser.Navigate("about:blank")
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If Not IsNothing(_callingPanel) Then
            If Not modGeneral.AdminRights Then
                MsgBox("You are not authorized to remove from dashboards")
                Return
            Else
                _callingDashboard.RemoveControl(Me.Ordinal, Me.Url)
                '_callingPanel.CloseThis(_ordinal)
            End If
        Else
            Dim myParent As Main = DirectCast(Me.ParentForm, Main)
            myParent.HideTopLayer(Me)
        End If

    End Sub

    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim myParent As Main = DirectCast(Me.ParentForm, Main)
        If myParent.PrinterIsInstalled() = False Then Exit Sub
        Try
            PrintPage()
        Catch ex As Exception
            MsgBox("You must be viewing a page before printing.", MsgBoxStyle.Critical, "No Chart")
        End Try
    End Sub

    Private Sub btnDump_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDump.Click
        'SB commented out; button visibility was set to False in Version 6
        'Dim myParent As Main = DirectCast(Me.ParentForm, Main)
        'myParent.GetDataDump()

    End Sub

    Private Sub btnDataEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDataEntry.Click
        Dim myParent As Main = DirectCast(Me.ParentForm, Main)
        myParent.GoToDataEntry()
    End Sub

    Private Sub tcBrowser_MouseClick(sender As Object, e As MouseEventArgs) Handles tcBrowser.MouseClick
        If Not IsNothing(_callingPanel) AndAlso TypeOf (_callingPanel) Is CascadeDock Then
            _callingPanel.MakeTopmost(_ordinal)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAddToDashboard.Click
        If Not modGeneral.AdminRights Then
            MsgBox("You are not authorized to add to dashboards")
            Return
        End If
        'MsgBox(_title + Environment.NewLine + _url)
        Dim dash As New DashboardsList
        dash.AssignParentForm(Me)
        dash.ShowDialog()
        Try
            dash.Close()
        Catch
        End Try

    End Sub
    Public Sub AppendUCToFile(control As SA_Browser, folderPath As String, dashboardName As String)
        'NOTE: I'm not using XML because I'm writing the URLs, which will have some
        ' characters that XML will need to transform back and forth, etc.

        If IsNothing(control) Then Exit Sub
        If dashboardName.Length < 1 Then Exit Sub
        If Not File.Exists(pathConfig + "Dashboard" + dashboardName + ".txt") Then
            MsgBox("Dashboard " & dashboardName & " has not been created. Please create the dashboard and try again.")
            Return
        End If
        Dim ord As Integer = 0
        Using reader As New StreamReader(pathConfig + "Dashboard" + dashboardName + ".txt", False)
            While Not reader.EndOfStream
                Dim line As String = reader.ReadLine()
                If line.Length > 0 Then
                    Dim parts() As String = line.Split(Chr(14))
                    ord = Int32.Parse(parts(0))
                End If
            End While
        End Using
        ord += 1
        Dim path As String = folderPath + "\Dashboard" + dashboardName + ".txt"
        Try
            Using writer As New StreamWriter(path, True)
                writer.WriteLine(ord.ToString() & _delim & control._graphicType.ToString() & _delim & control.Url)
                writer.Flush()
            End Using
        Catch ex As Exception
            MsgBox("Error in AppendUCToFile()" + ex.Message)
        End Try

    End Sub

    Public Sub GetDashboardName(name As String) 'called by Dashboards form
        '_dashboardToAppendTo = name
        AppendUCToFile(Me, pathConfig, name)
    End Sub

    Friend Sub DisplayRefnumChartOLD(ByVal chartName As String, _
                              ByVal refineryID As String, _
                              ByVal location As String, _
                              ByVal dataset As String, _
                              ByVal senario As String, _
                              ByVal startDate As Date, _
                              ByVal endDate As Date, _
                              ByVal StudyYear As String, _
                              ByVal table As String, _
                              ByVal UOM As String, _
                              ByVal currency As String, _
                              ByVal field1 As String, _
                              Optional ByVal YTD As String = "", _
                              Optional ByVal twelveMonthAvg As String = "", _
                              Optional ByVal target As String = "", _
                              Optional ByVal field2 As String = "", _
                              Optional ByVal field3 As String = "", _
                              Optional ByVal field4 As String = "", _
                              Optional ByVal field5 As String = "")




        Dim header As String = "WsP: " & _clientKey + vbCrLf

        chartName = System.Web.HttpUtility.UrlEncode(chartName)
        field1 = System.Web.HttpUtility.UrlEncode(field1)
        field2 = System.Web.HttpUtility.UrlEncode(field2)
        field3 = System.Web.HttpUtility.UrlEncode(field3)
        field4 = System.Web.HttpUtility.UrlEncode(field4)
        field5 = System.Web.HttpUtility.UrlEncode(field5)

        Dim dt As Date = DateTime.Now
        Dim page As String = ConfigurationManager.AppSettings("ChartControlUrl").ToString()
        page = page.Replace("ChartPage", "ChartControl")
        Dim stackedChart As Boolean = False
        Select Case chartName
            Case "Non-Volume-Related+Expenses"
                stackedChart = True
            Case "Net+Raw+Material+Input"
                stackedChart = True
            Case "Personnel+Index"
                stackedChart = True
            Case "Maintenance+Index"
                stackedChart = True
            Case "Maintenance+Efficiency+Index"
                stackedChart = True
            Case "Cash+Operating+Expenses+UEDC+Basis"
                stackedChart = True
            Case "Energy+Consumption+per+Barrel"
                stackedChart = True
            Case "Maintenance+Index,+Monthly+NTA+&+Annualized+TA"
                stackedChart = True
            Case "Volume-Related+Expenses"
                stackedChart = True
            Case "O,C&C+Personnel+Index"
                stackedChart = True
            Case "M,P&S+Personnel+Index"
                stackedChart = True
            Case "OcC&C+Total+Absences"
                stackedChart = True
            Case "McP&S+Total+Absences"
                stackedChart = True
            Case "Maintenance+Work+Force"
                stackedChart = True
        End Select
        If stackedChart Then
            page = page.Replace("ChartControl.aspx", "ChartControlStacked.aspx")
        End If
        Dim url As String = page + "?TEST=1&cn=" & chartName & "&sd=" & startDate.ToString("M/d/yyyy") & _
                               "&ed=" & endDate.ToString("M/d/yyyy") & _
                               "&ds=" & dataset & _
                               "&sn=" & senario & "&table=" & table & _
                               "&UOM=" & UOM & "&currency=" & currency & "&YR=" & StudyYear & _
                               "&YTD=" & YTD & "&TwelveMonthAvg=" & twelveMonthAvg & _
                               "&target=" & target & _
                               "&field1=" & field1 & "&field2=" & field2 & _
                               "&field3=" & field3 & "&field4=" & field4 & _
                               "&field5=" & field5 & "&TS=" & dt

        wbWebBrowser.Navigate(url, "", Nothing, header)

    End Sub

    Private Sub btnZoomUp_Click(sender As Object, e As EventArgs) Handles btnZoomUp.Click
        '"Option Strict On disallows late binding" "ExecWB"
        'Try
        '    Dim Res As Object = Nothing
        '    Dim MyWeb As Object
        '    MyWeb = Me.wbWebBrowser.ActiveXInstance
        '    MyWeb.ExecWB(Exec.OLECMDID_OPTICAL_ZOOM, _
        '          ExecOpt.OLECMDEXECOPT_DONTPROMPTUSER, 50, IntPtr.Zero)
        'Catch ex As Exception
        '    MsgBox("Error:" & ex.Message)
        'End Try


        'Neiher of these work
        '1
        Dim text As String = wbWebBrowser.DocumentText
        'text.Replace("<div id=""Panel1"" style=""height:352px;"">", "<div id=""Panel1"" style=""height:600px;"">")
        text.Replace("height:352px;", "height:600px;")
        wbWebBrowser.DocumentText = text
        'text.Replace("<div id=""Panel1"" style=""height:352px;">"

        '2
        text = wbWebBrowser.Document.Body.InnerHtml
        'text.Replace("<div id=""Panel1"" style=""height:352px;"">", "<div id=""Panel1"" style=""height:600px;"">")
        text.Replace("height:352px;", "height:600px;")
        wbWebBrowser.Document.Body.InnerHtml = text
        wbWebBrowser.Refresh()

        'wbWebBrowser.Controls("Panel1").Height = 600
    End Sub
End Class
