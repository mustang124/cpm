﻿Imports System.Collections.Generic

Public Class SA_Targeting

    Private _cboDelim As String = "    "
    Private _peerChoices As DataTable = Nothing
    'Private _regionCodes As DataTable = Nothing

    Private Sub SA_Targeting_Load(sender As Object, e As EventArgs) Handles Me.Load
        'fill cbos
        _peerChoices = frmMain.ds_Ref.Tables("TargetingPeerGroups")
        '_regionCodes = frmMain.ds_Ref.Tables("TargetingRegionCodes")

        Dim regionList As New List(Of String)
        'Dim tempRegionList As New List(Of String)
        Dim count As Integer = 0

        'For Each row As DataRow In _regionCodes.Rows
        '    Dim regionCode As String = row(4).ToString()
        '    'Dim value As String = row(0).ToString()
        '    regionList.Add(regionCode)
        '    count += 1
        'Next

        cboRegionCodes.DataSource = New BindingSource(regionList, Nothing)
        cboRegionCodes.DisplayMember = "Name"
        'cboPeerGroups.ValueMember = "Value"

        cboPeerGroups.DataSource = Nothing
        cboPeerGroups.Items.Clear()

        Dim peerList As New List(Of Item)
        count = 0
        For Each row As DataRow In _peerChoices.Rows
            'If CInt(row("StudyYear")) = 2014 Then
            Dim peerGroupId As Integer = CInt(row(0))
            Dim value As String = row(4).ToString() '4).ToString() '.Replace("$$", _cboDelim)
            peerList.Add(New Item(value, count, peerGroupId))
            count += 1
            'End If
        Next
        cboPeerGroups.DataSource = New BindingSource(peerList, Nothing)
        cboPeerGroups.DisplayMember = "Name"
        cboPeerGroups.ValueMember = "Value"


    End Sub

    Private Sub btnTargeting_Click(sender As Object, e As EventArgs) Handles btnTargeting.Click
        'Dim peerKey As Integer = DirectCast(cboPeerGroups.SelectedItem, KeyValuePair(Of Integer, String)).Name
        Dim selectedItem As Item = DirectCast(cboPeerGroups.SelectedItem, Item)
        Dim peerKey As Integer = selectedItem.PeerGroupId
        '        Dim parts As String() = selectedItem.Name.Split("$$")

        'Dim delim1 As Integer = selectedItem.Name.IndexOf(_cboDelim, 0)
        'Dim delim2 As Integer = selectedItem.Name.IndexOf(_cboDelim, delim1 + 4)
        ''Dim part1 As String = selectedItem.Name.Substring(0, delim1 - 1)
        'Dim part2 As String = selectedItem.Name.Substring(delim1 + 4, selectedItem.Name.Length - delim2 - 3)
        'Dim part3 As String = selectedItem.Name.Substring(delim2 + 4, selectedItem.Name.Length - delim2 - 4)

        'Dim value As String = DirectCast(cboPeerGroups.SelectedItem, KeyValuePair(Of Integer, String)).Value
        'Dim kpiKey As Integer = 1
        Dim peers As New List(Of String)
        peers.Add(peerKey.ToString) ' + "$$" + part2 + "$$" + part3)
        'Dim kpis As New List(Of String)
        'kpis.Add(cboKPIs.Text)
        Dim parent As Main = Me.ParentForm
        parent.btnRptCreateRoutine(Nothing, peers, Nothing) ' _kpiList)
        Me.Visible = False
    End Sub

    Private Class Item
        Private _name As String = String.Empty
        Public Property Name As String
            Get
                Return _name
            End Get
            Set(value As String)
                _name = value
            End Set
        End Property

        Private _value As Integer
        Public Property Value As Integer
            Get
                Return _value
            End Get
            Set(value As Integer)
                _value = value
            End Set
        End Property

        Private _peerGroupId As Integer
        Public Property PeerGroupId As Integer
            Get
                Return _peerGroupId
            End Get
            Set(value As Integer)
                _peerGroupId = value
            End Set
        End Property

        Public Sub New(name As String, value As Integer, peerGroupId As Integer)
            Me.Name = name
            Me.Value = value
            Me.PeerGroupId = peerGroupId
        End Sub

        Public Overrides Function ToString() As String
            '// Generates the text shown in the combo box
            Return Name
        End Function

    End Class


    Private Sub cboRegionCodes_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboRegionCodes.SelectionChangeCommitted
        cboPeerGroups.DataSource = Nothing
        cboPeerGroups.Items.Clear()
        Dim regionCode As String = cboRegionCodes.SelectedItem.ToString()
        If regionCode.Length > 0 Then
            Dim peerList As New List(Of Item)
            Dim count As Integer = 0
            For Each row As DataRow In _peerChoices.Rows
                If row("RegionCode") = regionCode And CInt(row("StudyYear")) = 2014 Then
                    Dim peerGroupId As Integer = row(0)
                    Dim value As String = row(4).ToString() '.Replace("$$", _cboDelim)
                    peerList.Add(New Item(value, count, peerGroupId))
                    count += 1
                End If
            Next
            cboPeerGroups.DataSource = New BindingSource(peerList, Nothing)
            cboPeerGroups.DisplayMember = "Name"
            cboPeerGroups.ValueMember = "Value"
        End If

    End Sub
End Class



