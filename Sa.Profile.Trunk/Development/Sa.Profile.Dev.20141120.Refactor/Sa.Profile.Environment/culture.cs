﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.ComponentModel;

namespace ProfileEnvironment
{
    /**
     * <summary>
     * This class provides regional currency information
     * based on the current user 
     * </summary>
     * <remarks>
     * </remarks>
     * <returns>
     * An object of values representing the regional currency properties
     * </returns>
     */
    [Serializable]
    public class Culture
    {
        public Culture()
        {
        }

        #region Public Properties

        [FieldNullable(IsNullable = false)]
        public string UserId { get; set; }

        [FieldNullable(IsNullable = false)]
        public string CultureName { get; set; }

        [FieldNullable(IsNullable = false)]
        public string TwoLetterISOLanguageName { get; set; }

        [FieldNullable(IsNullable = false)]
        public string ThreeLetterISOLanguageName { get; set; }

        [FieldNullable(IsNullable = false)]
        public string ThreeLetterWindowsLanguageName { get; set; }

        [FieldNullable(IsNullable = false)]
        public string DisplayName { get; set; }

        [FieldNullable(IsNullable = false)]
        public string EnglishName{ get; set; }


        #endregion Public Properties


        #region Public Methods

        public Culture GetCulture()
        {
            Culture culture = new Culture();

            culture.CultureName = GetCultureName();
            culture.TwoLetterISOLanguageName = GetTwoLetterISOLanguageName();
            culture.ThreeLetterISOLanguageName = GetThreeLetterISOLanguageName();
            culture.ThreeLetterWindowsLanguageName = GetThreeLetterWindowsLanguageName();
            culture.DisplayName = GetDisplayName();
            culture.EnglishName = GetEnglishName();

            return culture;

        }

        public List<Culture> GetCultures()
        {
            List<Culture> cultureList = new List<Culture>();


            foreach (CultureInfo cultureInfo in CultureInfo.GetCultures(CultureTypes.NeutralCultures))
            {
                Culture culture = new Culture();

                culture.CultureName = cultureInfo.Name;
                culture.TwoLetterISOLanguageName = cultureInfo.TwoLetterISOLanguageName;
                culture.ThreeLetterISOLanguageName = cultureInfo.ThreeLetterISOLanguageName;
                culture.ThreeLetterWindowsLanguageName = cultureInfo.ThreeLetterWindowsLanguageName;
                culture.DisplayName = cultureInfo.DisplayName;
                culture.EnglishName = cultureInfo.EnglishName;
            }

            return cultureList;
        }

        #endregion Public Methods


        #region Private Methods

        private string GetCultureName()
        {
            string retVal = System.Globalization.CultureInfo.CurrentCulture.Name.ToString();

            return retVal;
        }

        private string GetTwoLetterISOLanguageName()
        {
            string retVal = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToString();

            return retVal;
        }

        private string GetThreeLetterISOLanguageName()
        {
            string retVal = System.Globalization.CultureInfo.CurrentCulture.ThreeLetterISOLanguageName.ToString();

            return retVal;
        }

        private string GetThreeLetterWindowsLanguageName()
        {
            string retVal = System.Globalization.CultureInfo.CurrentCulture.ThreeLetterWindowsLanguageName.ToString();

            return retVal;
        }

        private string GetDisplayName()
        {
            string retVal = System.Globalization.CultureInfo.CurrentCulture.DisplayName.ToString();

            return retVal;
        }

        private string GetEnglishName()
        {
            string retVal = System.Globalization.CultureInfo.CurrentCulture.EnglishName.ToString();

            return retVal;
        }

        #endregion Private Methods
    }
}

