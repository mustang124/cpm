﻿
CREATE FUNCTION [dbo].[GetAvgEnergyConsKBTUPerBbl](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS real
AS
BEGIN
	DECLARE @KBTUPerBbl real
	SELECT @KBTUPerBbl = CASE WHEN SUM(m.NetInputBbl)>0 THEN SUM(e.TotEnergyConsMBTU)/SUM(m.NetInputBbl/1000) ELSE NULL END
	FROM MaterialTot m INNER JOIN EnergyTot e ON e.SubmissionID = m.SubmissionID
	INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND PeriodStart>=@StartDate AND PeriodStart < @EndDate

	RETURN @KBTUPerBbl
END
