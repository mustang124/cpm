﻿
CREATE FUNCTION [dbo].[GetDowntimes](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT r.UnitID, r.FactorSet, r.MaintDown, r.MaintSlow, r.RegDown, r.RegSlow, r.OthDown, r.OthSlow, r.PeriodHrs, r.PeriodEDC
		, r.OthDownEconomic, r.OthDownExternal, r.OthDownUnitUpsets, r.OthDownOffsiteUpsets, r.OthDownOther, r.UnpRegDown, r.UnpMaintDown, r.UnpOthDown
		, TADown = dbo.GetTADowntime(@RefineryID, @Dataset, @StartDate, @EndDate, r.UnitID)
		, MechUnavailTA = dbo.GetMechUnavailTA(@RefineryID, @Dataset, @StartDate, @EndDate, r.UnitID)
	FROM dbo.SumRoutDowntimes(@RefineryID, @Dataset, @StartDate, @EndDate) r
)



