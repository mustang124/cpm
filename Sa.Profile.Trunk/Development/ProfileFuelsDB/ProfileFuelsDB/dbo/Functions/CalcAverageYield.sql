﻿

CREATE FUNCTION [dbo].[CalcAverageYield](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime)
RETURNS TABLE 
AS
RETURN (
SELECT NetInputkBPD = CASE WHEN NumDays > 0 THEN NetInputBbl/1000/NumDays ELSE NULL END
	, GainPcnt = CASE WHEN NetInputBbl > 0 THEN 100*GainBbl/NetInputBbl ELSE NULL END
	, PVPPcnt = CASE WHEN NetInputBbl > 0 THEN 100*PVP/NetInputBbl ELSE NULL END
FROM dbo.SumMaterialTot(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd) m
)

