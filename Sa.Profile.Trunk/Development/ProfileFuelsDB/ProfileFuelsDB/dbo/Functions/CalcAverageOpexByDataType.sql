﻿
CREATE  FUNCTION [dbo].[CalcAverageOpexByDataType](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @DataType varchar(8))
RETURNS TABLE 
AS

	RETURN (
	 -- Using Client energy prices for all pricing scenarios
	SELECT o.FactorSet, o.Currency, Scenario = ISNULL(@Scenario, o.Scenario), o.DataType
		, TotCashOpex = GlobalDB.dbo.WtAvg(TotCashOpex, Divisor)
		, STVol = GlobalDB.dbo.WtAvg(STVol, Divisor)
		, STNonVol = GlobalDB.dbo.WtAvg(STNonVol, Divisor)
		, NEOpex = GlobalDB.dbo.WtAvg(NEOpex, Divisor)
	FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
	AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
	AND o.Scenario = 'CLIENT' AND o.FactorSet = ISNULL(@FactorSet, o.FactorSet) AND o.Currency = ISNULL(@Currency, o.Currency) AND o.DataType = ISNULL(@Datatype, o.DataType)
	GROUP BY o.FactorSet, o.Currency, o.Scenario, o.DataType
	)

