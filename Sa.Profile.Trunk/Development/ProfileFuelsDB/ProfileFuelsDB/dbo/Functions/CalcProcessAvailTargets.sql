﻿

CREATE FUNCTION [dbo].[CalcProcessAvailTargets](@SubmissionID int)
RETURNS TABLE
AS
RETURN (
	SELECT ProcessID = pg.ProcessGrouping, f.FactorSet, 
	MechAvail_Ann_Target=GlobalDB.dbo.WtAvg(m.MechAvail_Ann_Target,f.EDCNoMult), 
	MechAvailSlow_Ann_Target=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Ann_Target,f.EDCNoMult), 
	OpAvail_Ann_Target=GlobalDB.dbo.WtAvg(m.OpAvail_Ann_Target,f.EDCNoMult), 
	OpAvailSlow_Ann_Target=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Ann_Target,f.EDCNoMult), 
	OnStream_Ann_Target=GlobalDB.dbo.WtAvg(m.OnStream_Ann_Target,f.EDCNoMult), 
	OnStreamSlow_Ann_Target=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Ann_Target,f.EDCNoMult), 
	MechAvail_Act_Target=GlobalDB.dbo.WtAvg(m.MechAvail_Act_Target,f.EDCNoMult), 
	MechAvailSlow_Act_Target=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Act_Target,f.EDCNoMult), 
	OpAvail_Act_Target=GlobalDB.dbo.WtAvg(m.OpAvail_Act_Target,f.EDCNoMult), 
	OpAvailSlow_Act_Target=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Act_Target,f.EDCNoMult), 
	OnStream_Act_Target=GlobalDB.dbo.WtAvg(m.OnStream_Act_Target,f.EDCNoMult), 
	OnStreamSlow_Act_Target=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Act_Target,f.EDCNoMult)
	FROM MaintCalc m 
	INNER JOIN FactorCalc f ON f.SubmissionID = m.SubmissionID AND f.UnitID = m.UnitID
	INNER JOIN RefProcessGroupings pg ON pg.SubmissionID = m.SubmissionID AND pg.UnitID = m.UnitID
	WHERE m.SubmissionID = @SubmissionID AND f.EDCNoMult > 0
	GROUP BY pg.ProcessGrouping, f.FactorSet
)



