﻿CREATE FUNCTION [dbo].[CalcAverageFactors](@RefineryID varchar(6), @Dataset varchar(15), @FactorSet dbo.FactorSet, @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT a.FactorSet
		, AvgEDC = GlobalDB.dbo.WtAvg(a.EDC, s.NumDays)
		, AvgUEDC = GlobalDB.dbo.WtAvg(a.UEDC, s.NumDays)
		, SumUEDC = SUM(1.0*a.UEDC*s.NumDays)
		, UtilPcnt = GlobalDB.dbo.WtAvg(a.UtilPcnt, EDC*s.NumDays)
		, AvgProcessEDC = GlobalDB.dbo.WtAvg(a.TotProcessEDC, s.NumDays)
		, AvgProcessUEDC = GlobalDB.dbo.WtAvg(a.TotProcessUEDC, s.NumDays)
		, ProcessUtilPcnt = GlobalDB.dbo.WtAvg(a.TotProcessUtilPcnt, TotProcessEDC*s.NumDays)
		, UtilOSTA = GlobalDB.dbo.WtAvg(a.UtilOSTA, TotProcessEDC*s.NumDays)
		, EnergyUseDay = GlobalDB.dbo.WtAvg(a.EnergyUseDay, s.NumDays)
		, AvgStdEnergy = GlobalDB.dbo.WtAvg(a.TotStdEnergy, s.NumDays)
		, EII = GlobalDB.dbo.WtAvg(a.EII, a.TotStdEnergy*s.NumDays)
		, LossGainBpD = SUM(a.ReportLossGain)/SUM(s.NumDays*1.0)
		, AvgStdGainBpD = SUM(a.EstGain)/SUM(s.NumDays*1.0)
		, VEI = GlobalDB.dbo.WtAvgNN(a.VEI, EstGain)
		, Complexity = GlobalDB.dbo.WtAvg(a.Complexity, a.EDC*s.NumDays)
		, NonMaintPersEffDiv = SUM(a.NonMaintPersEffDiv*s.FractionOfYear)
		, MaintPersEffDiv = SUM(a.MaintPersEffDiv*s.FractionOfYear)
		, PersEffDiv = SUM(a.PersEffDiv*s.FractionOfYear)
		, MaintEffDiv = SUM(a.MaintEffDiv*s.FractionOfYear)
		, AvgAnnNonMaintPersEffDiv = GlobalDB.dbo.WtAvg(a.NonMaintPersEffDiv,s.NumDays)
		, AvgAnnMaintPersEffDiv = GlobalDB.dbo.WtAvg(a.MaintPersEffDiv,s.NumDays)
		, AvgAnnPersEffDiv = GlobalDB.dbo.WtAvg(a.PersEffDiv,s.NumDays)
		, AvgAnnMaintEffDiv = GlobalDB.dbo.WtAvg(a.MaintEffDiv,s.NumDays)
	FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
	AND (a.FactorSet = @FactorSet OR @FactorSet IS NULL)
	GROUP BY a.FactorSet
	)



