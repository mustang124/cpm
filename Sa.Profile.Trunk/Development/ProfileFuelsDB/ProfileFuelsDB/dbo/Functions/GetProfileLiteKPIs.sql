﻿CREATE FUNCTION [dbo].[GetProfileLiteKPIs](@RefineryID char(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5))
RETURNS @averages TABLE (
	RefUtilPcnt real NULL, 
	UtilOSTA real NULL, 
	EDC real NULL, 
	UtilUEDC real NULL,
	ProcessUtilPcnt real NULL, 
	TotProcessEDC real NULL, 
	TotProcessUEDC real NULL,
	MechAvail real NULL, 
	OpAvail real NULL, 
	MechUnavailRout real NULL, 
	MechUnavailTA real NULL, 
	RegUnavail real NULL, 
	EII real NULL, 
	EnergyUseDay real NULL, 
	TotStdEnergy real NULL,
	VEI real NULL, 
	ReportLossGain real NULL, 
	EstGain real NULL, 
	ProcessEffIndex real,
	GainPcnt real NULL, 
	RawMatlKBpD real NULL, 
	ProdYieldKBpD real NULL, 
	NetInputBPD real NULL,
	TotMaintForceWHrEDC real NULL, 
	MaintTAWHr real NULL, 
	MaintNonTAWHr real NULL,
	PersIndex real NULL, 
	NonMaintWHr real NULL, 
	TotMaintForceWHr real NULL, 
	MaintIndex real NULL,
	AnnTACost real NULL, 
	RoutCost real NULL,
	OpexUEDC real NULL, 
	NEOpexEDC real NULL,	
	NEOpex real NULL, 
	OpexEDC real NULL, 
	EnergyCost real NULL, 
	TotCashOpex real NULL,
	UEDC real NULL
	)
AS BEGIN

IF NOT EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND Dataset = @Dataset AND PeriodEnd = @EndDate)
	SELECT @EndDate = MAX(PeriodEnd) FROM Submissions WHERE RefineryID = @RefineryID AND Dataset = @Dataset AND PeriodEnd <= @EndDate
	
DECLARE @EII real, @VEI real, @RefUtilPcnt real, @UtilOSTA real, @EDC real, @UEDC real, @ProcessUtilPcnt real, @TotProcessEDC real, @TotProcessUEDC real
SELECT @EII = 100*CASE WHEN SUM(a.TotStdEnergy*s.NumDays) > 0 THEN SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) ELSE NULL END,
@VEI = 100*SUM(CASE WHEN VEI IS NOT NULL THEN ReportLossGain END)/SUM(CASE WHEN VEI IS NOT NULL THEN EstGain END),
@RefUtilPcnt = CASE WHEN SUM(EDC) > 0 THEN SUM(UtilPcnt*EDC*s.NumDays)/SUM(EDC*s.NumDays) ELSE NULL END,
@ProcessUtilPcnt = 100*CASE WHEN SUM(TotProcessEDC) > 0 THEN SUM(TotProcessUEDC*s.NumDays)/SUM(TotProcessEDC*s.NumDays) ELSE NULL END,
@UtilOSTA = CASE WHEN SUM(TotProcessEDC) > 0 THEN SUM(UtilOSTA*TotProcessEDC*s.NumDays)/SUM(TotProcessEDC*s.NumDays) ELSE NULL END,
@EDC = SUM(EDC*s.NumDays)/SUM(s.NumDays), @UEDC = SUM(UEDC*s.NumDays)/SUM(s.NumDays),
@TotProcessEDC = SUM(TotProcessEDC*s.NumDays)/SUM(s.NumDays), @TotProcessUEDC = SUM(TotProcessUEDC*s.NumDays)/SUM(s.NumDays)
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
AND a.FactorSet = @FactorSet

DECLARE @MechUnavailRout real, @MechUnavailTA real, @RegUnavail real, @MechAvail real, @OpAvail real, @EnergyUseDay real, @TotStdEnergy real
	, @ReportLossGain real, @EstGain real, @ProcessEffIndex real
SELECT @MechUnavailRout = GlobalDB.dbo.WtAvg(100-(MechAvail_Act+MechUnavailTA_Act), f.TotProcessEDC*s.FractionOfYear), 
	@MechUnavailTA = GlobalDB.dbo.WtAvg(MechUnavailTA_Ann, CASE WHEN s.PeriodEnd = @EndDate THEN 1 ELSE 0 END),
	@RegUnavail = GlobalDB.dbo.WtAvg(MechAvail_Act-OpAvail_Act, f.TotProcessEDC*s.FractionOfYear), 
/*	@TotProcessEDC = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
*/	@EnergyUseDay = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0)/1000.0, 
	@TotStdEnergy = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0)/1000.0,
	@ReportLossGain = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain = SUM(f.EstGain)/SUM(s.NumDays*1.0)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
AND m.FactorSet = @FactorSet

SELECT @MechAvail = 100 - @MechUnavailTA - @MechUnavailRout
SELECT @OpAvail = @MechAvail - @RegUnavail

SELECT @ProcessEffIndex = dbo.AvgProcessEffIndex(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet)

DECLARE @GainPcnt real, @RawMatlKBpD real, @ProdYieldKBpD real, @NetInputBPD real
SELECT @GainPcnt = SUM(GainBbl)/SUM(NetInputBbl)*100, @RawMatlKBpD = SUM(NetInputBbl)/SUM(s.NumDays*1000.0), @ProdYieldKBpD = SUM(NetInputBbl+GainBbl)/SUM(s.NumDays*1000.0),
	@NetInputBPD = SUM(NetInputBbl)/SUM(s.NumDays*1.0)
FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND m.NetInputBbl > 0

DECLARE @TotMaintForceWHrEDC real, @PersIndex real, @MaintIndex real
SELECT @TotMaintForceWHrEDC = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @PersIndex = GlobalDB.dbo.WtAvg(g.TotWHrEDC,p.WHrEDCDivisor)
, @MaintIndex = GlobalDB.dbo.WtAvg((g.RoutIndex + g.TAIndex_AVG),g.EDC)
FROM PersTotCalc p INNER JOIN Gensum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet AND g.UOM = @UOM
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

DECLARE @OpexUEDC real, @NEOpexEDC real
SELECT @OpexUEDC = GlobalDB.dbo.WtAvg(TotCashOpex,Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
AND o.DataType = 'UEDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NEOpexEDC = GlobalDB.dbo.WtAvg(NEOpex,Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
AND o.DataType = 'EDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

DECLARE @NonMaintWHr real, @TotMaintForceWHr real, @MaintTAWHr real, @MaintNonTAWHr real
SELECT @NonMaintWHr = SUM(NonMaintWHr/1000), @TotMaintForceWHr = SUM(TotMaintForceWHr/1000), @MaintTAWHr = SUM(TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr = SUM(TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate

DECLARE @AnnTACost real, @RoutCost real
SELECT @AnnTACost = SUM(AllocAnnTACost*s.FractionOfYear)/1000, @RoutCost = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND mtc.Currency = @Currency

DECLARE @NEOpex real, @OpexEDC real, @EnergyCost real, @TotCashOpex real
SELECT @NEOpex = SUM(NEOpex*Divisor/1000), @OpexEDC = SUM(TotCashOpex*Divisor)/SUM(Divisor), @EnergyCost = SUM(EnergyCost*Divisor/1000), @TotCashOpex = SUM(TotCashOpex*Divisor/1000)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

INSERT @averages (RefUtilPcnt, UtilOSTA, EDC, UEDC, ProcessUtilPcnt, TotProcessEDC, TotProcessUEDC
	, MechAvail, OpAvail, MechUnavailRout, MechUnavailTA, RegUnavail
	, EII, EnergyUseDay, TotStdEnergy
	, VEI, ProcessEffIndex, ReportLossGain, EstGain, GainPcnt, RawMatlKBpD, ProdYieldKBpD, NetInputBPD
	, TotMaintForceWHrEDC, MaintTAWHr, MaintNonTAWHr
	, PersIndex, NonMaintWHr, TotMaintForceWHr 
	, MaintIndex, AnnTACost, RoutCost
	, OpexUEDC, NEOpexEDC, NEOpex, OpexEDC, EnergyCost, TotCashOpex)
VALUES (@RefUtilPcnt, @UtilOSTA, @EDC, @UEDC, @ProcessUtilPcnt, @TotProcessEDC, @TotProcessUEDC
	, @MechAvail, @OpAvail, @MechUnavailRout, @MechUnavailTA, @RegUnavail
	, @EII, @EnergyUseDay, @TotStdEnergy
	, @VEI, @ProcessEffIndex, @ReportLossGain, @EstGain, @GainPcnt, @RawMatlKBpD, @ProdYieldKBpD, @NetInputBPD
	, @TotMaintForceWHrEDC, @MaintTAWHr, @MaintNonTAWHr
	, @PersIndex, @NonMaintWHr, @TotMaintForceWHr 
	, @MaintIndex, @AnnTACost, @RoutCost
	, @OpexUEDC, @NEOpexEDC, @NEOpex, @OpexEDC, @EnergyCost, @TotCashOpex)

IF @UOM = 'MET'
	UPDATE @averages
	SET EnergyUseDay = EnergyUseDay * 1.055, TotStdEnergy = TotStdEnergy * 1.055
IF @Currency = 'RUB'
	UPDATE @averages SET OpexUEDC = OpexUEDC/100

UPDATE @averages SET UtilUEDC = EDC*(RefUtilPcnt/100.0)

RETURN

END


