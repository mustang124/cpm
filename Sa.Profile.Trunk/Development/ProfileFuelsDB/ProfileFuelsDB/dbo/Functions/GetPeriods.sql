﻿CREATE FUNCTION dbo.GetPeriods(@SubmissionID int)
RETURNS TABLE
AS
RETURN (
	SELECT PeriodStart, PeriodEnd, Start3Mo = DATEADD(mm, -3, PeriodEnd), Start12Mo = DATEADD(mm, -12, PeriodEnd), StartYTD = dbo.BuildDate(DATEPART(yy, PeriodStart), 1, 1), Start24Mo = DATEADD(mm, -24, PeriodEnd)
	FROM Submissions
	WHERE SubmissionID = @SubmissionID 
	)
