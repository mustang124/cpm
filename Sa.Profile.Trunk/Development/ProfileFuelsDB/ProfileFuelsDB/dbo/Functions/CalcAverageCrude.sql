﻿

CREATE FUNCTION [dbo].[CalcAverageCrude](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT TotBbl = SUM(TotBbl), TotMT = SUM(TotMT)
		, Density = GlobalDB.dbo.WtAvg(AvgDensity, TotMT)
		, Gravity = dbo.KGM3toAPI(GlobalDB.dbo.WtAvg(AvgDensity, TotMT))
		, Sulfur = GlobalDB.dbo.WtAvgNZ(AvgSulfur,TotMT)
	FROM CrudeTot c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
	AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
)

