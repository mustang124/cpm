﻿CREATE FUNCTION dbo.AvgExchangeRate(@CurrencyCode CurrencyCode, @PeriodStart smalldatetime, @PeriodEnd smalldatetime)
RETURNS real
AS
BEGIN
DECLARE @Months TABLE(FirstOfMonth smalldatetime NOT NULL, DaysInMonth tinyint NULL, ExchRate real NULL)
DECLARE @AddDate smalldatetime
SELECT @AddDate = @PeriodStart
WHILE @AddDate < @PeriodEnd
BEGIN
	INSERT @Months (FirstOfMonth, DaysInMonth, ExchRate)
	SELECT @AddDate, DATEDIFF(d, @AddDate, DATEADD(m, 1, @AddDate)), dbo.ExchangeRate('USD', @CurrencyCode, @AddDate)
	SELECT @AddDate = DATEADD(m, 1, @AddDate)
END

DECLARE @AvgExchRate real
SELECT @AvgExchRate = GlobalDB.dbo.WtAvg(ExchRate, DaysInMonth) FROM @Months

RETURN @AvgExchRate

END

