﻿
CREATE FUNCTION [dbo].[CalcAverageProcessAvail](@SubmissionID int)
RETURNS TABLE
AS
RETURN (
	SELECT m.ProcessID, f.FactorSet, 
	MechAvail_Ann_Avg=GlobalDB.dbo.WtAvg(m.MechAvail_Ann_Avg,f.EDCNoMult), 
	MechAvailSlow_Ann_Avg=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Ann_Avg,f.EDCNoMult), 
	OpAvail_Ann_Avg=GlobalDB.dbo.WtAvg(m.OpAvail_Ann_Avg,f.EDCNoMult), 
	OpAvailSlow_Ann_Avg=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Ann_Avg,f.EDCNoMult), 
	OnStream_Ann_Avg=GlobalDB.dbo.WtAvg(m.OnStream_Ann_Avg,f.EDCNoMult), 
	OnStreamSlow_Ann_Avg=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Ann_Avg,f.EDCNoMult), 
	MechAvail_Act_Avg=GlobalDB.dbo.WtAvg(m.MechAvail_Act_Avg,f.EDCNoMult), 
	MechAvailSlow_Act_Avg=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Act_Avg,f.EDCNoMult), 
	OpAvail_Act_Avg=GlobalDB.dbo.WtAvg(m.OpAvail_Act_Avg,f.EDCNoMult), 
	OpAvailSlow_Act_Avg=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Act_Avg,f.EDCNoMult), 
	OnStream_Act_Avg=GlobalDB.dbo.WtAvg(m.OnStream_Act_Avg,f.EDCNoMult), 
	OnStreamSlow_Act_Avg=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Act_Avg,f.EDCNoMult), 
	MechAvail_Ann_YTD=GlobalDB.dbo.WtAvg(m.MechAvail_Ann_YTD,f.EDCNoMult), 
	MechAvailSlow_Ann_YTD=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Ann_YTD,f.EDCNoMult), 
	OpAvail_Ann_YTD=GlobalDB.dbo.WtAvg(m.OpAvail_Ann_YTD,f.EDCNoMult), 
	OpAvailSlow_Ann_YTD=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Ann_YTD,f.EDCNoMult), 
	OnStream_Ann_YTD=GlobalDB.dbo.WtAvg(m.OnStream_Ann_YTD,f.EDCNoMult), 
	OnStreamSlow_Ann_YTD=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Ann_YTD,f.EDCNoMult), 
	MechAvail_Act_YTD=GlobalDB.dbo.WtAvg(m.MechAvail_Act_YTD,f.EDCNoMult), 
	MechAvailSlow_Act_YTD=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Act_YTD,f.EDCNoMult), 
	OpAvail_Act_YTD=GlobalDB.dbo.WtAvg(m.OpAvail_Act_YTD,f.EDCNoMult), 
	OpAvailSlow_Act_YTD=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Act_YTD,f.EDCNoMult), 
	OnStream_Act_YTD=GlobalDB.dbo.WtAvg(m.OnStream_Act_YTD,f.EDCNoMult), 
	OnStreamSlow_Act_YTD=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Act_YTD,f.EDCNoMult), 
	MechAvailOSTA_Avg=GlobalDB.dbo.WtAvg(m.MechAvailOSTA_Avg,f.EDCNoMult), 
	MechAvailOSTA_YTD=GlobalDB.dbo.WtAvg(m.MechAvailOSTA_YTD,f.EDCNoMult), 
	MechAvail_Ann_Target=GlobalDB.dbo.WtAvg(m.MechAvail_Ann_Target,f.EDCNoMult), 
	MechAvailSlow_Ann_Target=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Ann_Target,f.EDCNoMult), 
	OpAvail_Ann_Target=GlobalDB.dbo.WtAvg(m.OpAvail_Ann_Target,f.EDCNoMult), 
	OpAvailSlow_Ann_Target=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Ann_Target,f.EDCNoMult), 
	OnStream_Ann_Target=GlobalDB.dbo.WtAvg(m.OnStream_Ann_Target,f.EDCNoMult), 
	OnStreamSlow_Ann_Target=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Ann_Target,f.EDCNoMult), 
	MechAvail_Act_Target=GlobalDB.dbo.WtAvg(m.MechAvail_Act_Target,f.EDCNoMult), 
	MechAvailSlow_Act_Target=GlobalDB.dbo.WtAvg(m.MechAvailSlow_Act_Target,f.EDCNoMult), 
	OpAvail_Act_Target=GlobalDB.dbo.WtAvg(m.OpAvail_Act_Target,f.EDCNoMult), 
	OpAvailSlow_Act_Target=GlobalDB.dbo.WtAvg(m.OpAvailSlow_Act_Target,f.EDCNoMult), 
	OnStream_Act_Target=GlobalDB.dbo.WtAvg(m.OnStream_Act_Target,f.EDCNoMult), 
	OnStreamSlow_Act_Target=GlobalDB.dbo.WtAvg(m.OnStreamSlow_Act_Target,f.EDCNoMult)
	FROM MaintCalc m INNER JOIN FactorCalc f 
	ON f.SubmissionID = m.SubmissionID AND f.UnitID = m.UnitID
	WHERE m.SubmissionID = @SubmissionID AND f.EDCNoMult > 0
	GROUP BY m.ProcessID, f.FactorSet
)


