﻿

CREATE   PROC spReportProcessDataSA (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT p.SubmissionID, p.UnitID, p.Property, p.SAValue, t.FormulaSymbol, t.USDescription, t.USDecPlaces
FROM ProcessData p INNER JOIN Config c ON c.submissionid = p.submissionid AND p.unitid = c.unitid 
INNER JOIN Table2_LU t ON p.property = t.property AND c.processid = t.processid
INNER JOIN Factors f ON f.processid = c.processid AND f.processtype = c.processtype AND f.factorset=@FactorSet AND f.CogenES IN ('E','')
WHERE c.SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID AND DataSet=@DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth)
AND t.FormulaSymbol IS NOT NULL AND CHARINDEX(t.FormulaSymbol, f.FormulaSymbols) > 0
ORDER BY c.UnitID, t.FormulaSymbol


