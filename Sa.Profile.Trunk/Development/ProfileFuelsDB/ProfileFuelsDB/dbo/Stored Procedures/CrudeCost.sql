﻿





CREATE     PROCEDURE CrudeCost
	(@StudyYear StudyYear, 
	@CNum varchar(5),
	@CrudeAPI real,
	@CrudeSulfur real,
	@CrudeDest PricingLocation,
	@BasePrice real OUTPUT, 
	@TransCost real OUTPUT, 
	@GravityAdj real OUTPUT, 
	@SulfurAdj real OUTPUT,
	@PricePerBbl real OUTPUT,
	@CrudeOrigin PricingLocation OUTPUT, 
	@Details bit = 0)
AS
SET NOCOUNT ON
DECLARE @PostedAPI real, @APIScale tinyint, @PostedSulfur real, @SulfurAdjSlope real
DECLARE @BblAdder real, @MTAdder real, @PcntAdj real

SELECT	@BasePrice = NULL, @TransCost = NULL, @GravityAdj = NULL, @SulfurAdj = NULL, @PricePerBbl = NULL, @CrudeOrigin = NULL

SELECT 	@BasePrice = P.BasePrice, @CrudeOrigin = P.CrudeOrigin, 
	@PostedAPI = P.PostedAPI, @APIScale = P.APIScale, 
	@PostedSulfur = P.PostedSulfur, @SulfurAdjSlope = P.SulfurAdjSlope
FROM CrudePrice P
WHERE P.StudyYear = @StudyYear AND P.CNum = @CNum

SET @SulfurAdj = (@PostedSulfur-@CrudeSulfur)*@SulfurAdjSlope

DECLARE @MTPerBbl float
EXEC dbo.BblMTGravity 1, @MTPerBbl OUTPUT, @CrudeAPI, NULL
EXEC TransCost @StudyYear = @StudyYear, 
	@CrudeOrigin = @CrudeOrigin, 
	@CrudeDest = @CrudeDest, 
	@BblAdder = @BblAdder OUTPUT,
	@MTAdder = @MTAdder OUTPUT,
	@PcntAdj = @PcntAdj OUTPUT, 
	@Details = @Details
	SELECT @TransCost = ISNULL(@BblAdder, 0)
		+ ISNULL(@MTAdder, 0) * @MTPerBbl
		+ ISNULL(@PcntAdj, 0) * @BasePrice/100

EXECUTE CrudeGravityAdj @StudyYear = @StudyYear, 
	@APIScale = @APIScale, @CrudeAPI = @CrudeAPI, 
	@PostedAPI = @PostedAPI, @APIAdj = @GravityAdj OUTPUT, @Details = @Details

SELECT @PricePerBbl = @BasePrice + ISNULL(@TransCost, 0) + ISNULL(@GravityAdj, 0) + ISNULL(@SulfurAdj, 0)

SET NOCOUNT OFF

IF @Details = 1
	SELECT 	BasePrice = @BasePrice, 
		TransCost = @TransCost, 
		GravityAdj = @GravityAdj,
		SulfurAdj = @SulfurAdj,
		PricePerBbl = @PricePerBbl,
		StudyYear = @StudyYear, 
		CNum = @CNum, CrudeAPI = @CrudeAPI,
		CrudeOrigin = @CrudeOrigin, CrudeDest = @CrudeDest, 
		BblAdder = ISNULL(@BblAdder, 0),
		MTAdder = ISNULL(@MTAdder, 0), MTPerBbl = @MTPerBbl,
		PcntAdj = ISNULL(@PcntAdj, 0)







