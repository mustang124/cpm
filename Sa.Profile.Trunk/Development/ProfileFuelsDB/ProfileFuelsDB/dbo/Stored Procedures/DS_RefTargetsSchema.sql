﻿CREATE PROCEDURE [dbo].[DS_RefTargetsSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(c.SectionHeader) AS SectionHeader,RTRIM(r.Property) AS Property, r.Target, RTRIM(r.CurrencyCode) AS CurrencyCode,c.SortKey FROM RefTargets r,Chart_LU c 
        WHERE c.TargetField=r.Property AND c.Sortkey<800 AND  r.SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID=@RefineryID)
END
