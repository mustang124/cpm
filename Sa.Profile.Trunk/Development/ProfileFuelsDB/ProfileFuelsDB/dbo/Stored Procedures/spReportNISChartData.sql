﻿



CREATE   PROC [dbo].[spReportNISChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2008', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	UtilPcnt real NULL, 
	VEI real NULL, 
	ProcessEffIndex real NULL, 
	OpAvail real NULL, 
	MaintIndex real NULL,
	PersIndex real NULL, 
	NEOpexEDC real NULL,
	OpexUEDC real NULL
)
DECLARE @msgString varchar(255)
SELECT @msgString = CAST(@PeriodEnd as varchar(20))

--- Everything Already Available in Gensum
INSERT INTO @data (PeriodStart, PeriodEnd, UtilPcnt, OpAvail, EII, VEI, ProcessEffIndex, PersIndex, MaintIndex, NEOpexEDC, OpexUEDC)
SELECT	PeriodStart, PeriodEnd, Gensum.UtilPcnt, OpAvail, Gensum.EII, Gensum.VEI, ProcessEffIndex = f.PEI, PersIndex = TotWHrEDC, MaintIndex = RoutIndex + TAIndex_AVG, NEOpexEDC, TotCashOpexUEDC
FROM Gensum LEFT JOIN FactorTotCalc f ON f.SubmissionID = GenSum.SubmissionID AND f.FactorSet = GenSum.FactorSet
WHERE Gensum.RefineryID = @RefineryID AND DataSet = @DataSet AND Gensum.FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario
AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd

IF (SELECT COUNT(*) FROM @data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
			INSERT @data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilPcnt, VEI, OpAvail, PersIndex, MaintIndex, NEOpexEDC, OpexUEDC, ProcessEffIndex
FROM @Data
ORDER BY PeriodStart ASC





