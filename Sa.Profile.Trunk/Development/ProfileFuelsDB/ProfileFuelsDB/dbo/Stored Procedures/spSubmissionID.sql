﻿

CREATE   PROCEDURE [dbo].[spSubmissionID] (@RefineryID char(6), @Dataset varchar(15), @PeriodYear smallint, @PeriodMonth tinyint)
AS
SET NOCOUNT ON
IF @PeriodYear < 1990 
	RETURN 1
IF @PeriodMonth NOT BETWEEN 1 AND 12
	RETURN 2
IF NOT EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND Dataset = @Dataset AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth)
	INSERT INTO Submissions(RefineryID, Dataset, PeriodYear, PeriodMonth, RptCurrency, UOM, PeriodStart, PeriodEnd)
	VALUES (@RefineryID, @Dataset, @PeriodYear, @PeriodMonth,  '', 'US', 
	CONVERT(smalldatetime, CONVERT(varchar(2), @PeriodMonth) + '/1/' + CONVERT(varchar(4), @PeriodYear)),
	DATEADD(m, 1, CONVERT(smalldatetime, CONVERT(varchar(2), @PeriodMonth) + '/1/' + CONVERT(varchar(4), @PeriodYear))))

SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND Dataset = @Dataset AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth
