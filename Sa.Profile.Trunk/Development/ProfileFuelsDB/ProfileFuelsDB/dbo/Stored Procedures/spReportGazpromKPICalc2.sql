﻿


CREATE   PROC [dbo].[spReportGazpromKPICalc2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15), 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5),
	@EII real = NULL OUTPUT, @EII_QTR real = NULL OUTPUT, @EII_AVG real = NULL OUTPUT, @EII_YTD real = NULL OUTPUT, 
	@EnergyUseDay real = NULL OUTPUT, @EnergyUseDay_QTR real = NULL OUTPUT, @EnergyUseDay_AVG real = NULL OUTPUT, @EnergyUseDay_YTD real = NULL OUTPUT, 
	@TotStdEnergy real = NULL OUTPUT, @TotStdEnergy_QTR real = NULL OUTPUT, @TotStdEnergy_AVG real = NULL OUTPUT, @TotStdEnergy_YTD real = NULL OUTPUT, 
	@RefUtilPcnt real = NULL OUTPUT, @RefUtilPcnt_QTR real = NULL OUTPUT, @RefUtilPcnt_AVG real = NULL OUTPUT, @RefUtilPcnt_YTD real = NULL OUTPUT, 
	@EDC real = NULL OUTPUT, @EDC_QTR real = NULL OUTPUT, @EDC_AVG real = NULL OUTPUT, @EDC_YTD real = NULL OUTPUT, 
	@UEDC real = NULL OUTPUT, @UEDC_QTR real = NULL OUTPUT, @UEDC_AVG real = NULL OUTPUT, @UEDC_YTD real = NULL OUTPUT, 
	@VEI real = NULL OUTPUT, @VEI_QTR real = NULL OUTPUT, @VEI_AVG real = NULL OUTPUT, @VEI_YTD real = NULL OUTPUT, 
	@ReportLossGain real = NULL OUTPUT, @ReportLossGain_QTR real = NULL OUTPUT, @ReportLossGain_AVG real = NULL OUTPUT, @ReportLossGain_YTD real = NULL OUTPUT, 
	@EstGain real = NULL OUTPUT, @EstGain_QTR real = NULL OUTPUT, @EstGain_AVG real = NULL OUTPUT, @EstGain_YTD real = NULL OUTPUT, 
	@OpAvail real = NULL OUTPUT, @OpAvail_QTR real = NULL OUTPUT, @OpAvail_AVG real = NULL OUTPUT, @OpAvail_YTD real = NULL OUTPUT, 
	@MechUnavailTA real = NULL OUTPUT, @MechUnavailTA_QTR real = NULL OUTPUT, @MechUnavailTA_AVG real = NULL OUTPUT, @MechUnavailTA_YTD real = NULL OUTPUT, 
	@NonTAUnavail real = NULL OUTPUT, @NonTAUnavail_QTR real = NULL OUTPUT, @NonTAUnavail_AVG real = NULL OUTPUT, @NonTAUnavail_YTD real = NULL OUTPUT, 
	@RoutIndex real = NULL OUTPUT, @RoutIndex_QTR real = NULL OUTPUT, @RoutIndex_AVG real = NULL OUTPUT, @RoutIndex_YTD real = NULL OUTPUT,
	@RoutCost real = NULL OUTPUT, @RoutCost_QTR real = NULL OUTPUT, @RoutCost_AVG real = NULL OUTPUT, @RoutCost_YTD real = NULL OUTPUT, 
	@PersIndex real = NULL OUTPUT, @PersIndex_QTR real = NULL OUTPUT, @PersIndex_AVG real = NULL OUTPUT, @PersIndex_YTD real = NULL OUTPUT, 
	@AnnTAWhr real = NULL OUTPUT, @AnnTAWhr_QTR real = NULL OUTPUT, @AnnTAWhr_AVG real = NULL OUTPUT, @AnnTAWhr_YTD real = NULL OUTPUT,
	@NonTAWHr real = NULL OUTPUT, @NonTAWHr_QTR real = NULL OUTPUT, @NonTAWHr_AVG real = NULL OUTPUT, @NonTAWHr_YTD real = NULL OUTPUT,
	@NEOpexEDC real = NULL OUTPUT, @NEOpexEDC_QTR real = NULL OUTPUT, @NEOpexEDC_AVG real = NULL OUTPUT, @NEOpexEDC_YTD real = NULL OUTPUT, 
	@NEOpex real = NULL OUTPUT, @NEOpex_QTR real = NULL OUTPUT, @NEOpex_AVG real = NULL OUTPUT, @NEOpex_YTD real = NULL OUTPUT, 
	@OpexUEDC real = NULL OUTPUT, @OpexUEDC_QTR real = NULL OUTPUT, @OpexUEDC_AVG real = NULL OUTPUT, @OpexUEDC_YTD real = NULL OUTPUT, 
	@TAAdj real = NULL OUTPUT, @TAAdj_QTR real = NULL OUTPUT, @TAAdj_AVG real = NULL OUTPUT, @TAAdj_YTD real = NULL OUTPUT,
	@EnergyCost real = NULL OUTPUT, @EnergyCost_QTR real = NULL OUTPUT, @EnergyCost_AVG real = NULL OUTPUT, @EnergyCost_YTD real = NULL OUTPUT, 
	@TotCashOpex real = NULL OUTPUT, @TotCashOpex_QTR real = NULL OUTPUT, @TotCashOpex_AVG real = NULL OUTPUT, @TotCashOpex_YTD real = NULL OUTPUT
	)
AS

SELECT @EII = NULL, @EII_QTR = NULL, @EII_AVG = NULL, @EII_YTD = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_QTR = NULL, @EnergyUseDay_AVG = NULL, @EnergyUseDay_YTD = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_QTR = NULL, @TotStdEnergy_AVG = NULL, @TotStdEnergy_YTD = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_QTR = NULL, @RefUtilPcnt_AVG = NULL, @RefUtilPcnt_YTD = NULL, 
	@EDC = NULL, @EDC_QTR = NULL, @EDC_AVG = NULL, @EDC_YTD = NULL, 
	@UEDC = NULL, @UEDC_QTR = NULL, @UEDC_AVG = NULL, @UEDC_YTD = NULL, 
	@VEI = NULL, @VEI_QTR = NULL, @VEI_AVG = NULL, @VEI_YTD = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_QTR = NULL, @ReportLossGain_AVG = NULL, @ReportLossGain_YTD = NULL,
	@EstGain = NULL, @EstGain_QTR = NULL, @EstGain_AVG = NULL, @EstGain_YTD = NULL, 
	@OpAvail = NULL, @OpAvail_QTR = NULL, @OpAvail_AVG = NULL, @OpAvail_YTD = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_QTR = NULL, @MechUnavailTA_AVG = NULL, @MechUnavailTA_YTD = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_QTR = NULL, @NonTAUnavail_AVG = NULL, @NonTAUnavail_YTD = NULL, 
	@RoutIndex = NULL, @RoutIndex_QTR = NULL, @RoutIndex_AVG = NULL, @RoutIndex_YTD = NULL,
	@RoutCost = NULL, @RoutCost_QTR = NULL, @RoutCost_AVG = NULL, @RoutCost_YTD = NULL, 
	@PersIndex = NULL, @PersIndex_QTR = NULL, @PersIndex_AVG = NULL, @PersIndex_YTD = NULL, 
	@AnnTAWhr = NULL, @AnnTAWhr_QTR = NULL, @AnnTAWhr_AVG = NULL, @AnnTAWhr_YTD = NULL,
	@NonTAWHr = NULL, @NonTAWHr_QTR = NULL, @NonTAWHr_AVG = NULL, @NonTAWHr_YTD = NULL,
	@NEOpexEDC = NULL, @NEOpexEDC_QTR = NULL, @NEOpexEDC_AVG = NULL, @NEOpexEDC_YTD = NULL, 
	@NEOpex = NULL, @NEOpex_QTR = NULL, @NEOpex_AVG = NULL, @NEOpex_YTD = NULL, 
	@OpexUEDC = NULL, @OpexUEDC_QTR = NULL, @OpexUEDC_AVG = NULL, @OpexUEDC_YTD = NULL, 
	@TAAdj = NULL, @TAAdj_QTR = NULL, @TAAdj_AVG = NULL, @TAAdj_YTD = NULL,
	@EnergyCost = NULL, @EnergyCost_QTR = NULL, @EnergyCost_AVG = NULL, @EnergyCost_YTD = NULL, 
	@TotCashOpex = NULL, @TotCashOpex_QTR = NULL, @TotCashOpex_AVG = NULL, @TotCashOpex_YTD = NULL

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)
IF DATEPART(yy, @Start3Mo) < 2010
	SET @Start3Mo = '12/31/2009'
IF DATEPART(yy, @Start12Mo) < 2010
	SET @Start12Mo = '12/31/2009'
DECLARE	
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_AVG real, @ProcessUtilPcnt_YTD real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_AVG real, @TotProcessEDC_YTD real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_AVG real, @TotProcessUEDC_YTD real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_AVG real, @TotMaintForceWHrEDC_YTD real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_AVG real, @MaintTAWHr_YTD real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_AVG real, @MaintNonTAWHr_YTD real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_AVG real, @MaintIndex_YTD real,
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_AVG real, @AnnTACost_YTD real


--- Everything Already Available in Gensum
SELECT	@RefUtilPcnt = UtilPcnt, @RefUtilPcnt_AVG = UtilPcnt_AVG, @RefUtilPcnt_YTD = UtilPcnt_YTD,
	@ProcessUtilPcnt = ProcessUtilPcnt, @ProcessUtilPcnt_AVG = ProcessUtilPcnt_AVG, @ProcessUtilPcnt_YTD = ProcessUtilPcnt_YTD,
	@OpAvail = OpAvail, @OpAvail_AVG = OpAvail_AVG, @OpAvail_YTD = OpAvail_YTD,
	@EII = EII, @EII_AVG = EII_AVG, @EII_YTD = EII_YTD,
	@VEI = VEI, @VEI_AVG = VEI_AVG, @VEI_YTD = VEI_YTD,
	@PersIndex = TotWHrEDC, @PersIndex_AVG = TotWHrEDC_AVG, @PersIndex_YTD = TotWHrEDC_YTD,
	@EDC = EDC/1000, @EDC_AVG = EDC_AVG/1000, @EDC_YTD = EDC_YTD/1000,
	@UEDC = UEDC/1000, @UEDC_AVG = UEDC_AVG/1000, @UEDC_YTD = UEDC_YTD/1000,
	@TotMaintForceWHrEDC = TotMaintForceWHrEDC, @TotMaintForceWHrEDC_AVG = TotMaintForceWHrEDC_Avg, @TotMaintForceWHrEDC_YTD = TotMaintForceWHrEDC_YTD,
	@MaintIndex = RoutIndex + TAIndex_AVG, @MaintIndex_AVG = MaintIndex_AVG, @MaintIndex_YTD = RoutIndex_YTD + TAIndex_AVG,
	@RoutIndex = RoutIndex, @RoutIndex_AVG = RoutIndex_AVG, @RoutIndex_YTD = RoutIndex_YTD,
	@NEOpexEDC = NEOpexEDC, @NEOpexEDC_AVG = NEOpexEDC_AVG, @NEOpexEDC_YTD = NEOpexEDC_YTD
FROM Gensum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_QTR = MechUnavailTA_Ann, @MechUnavailTA_AVG = MechUnavailTA_Ann, @MechUnavailTA_YTD = MechUnavailTA_Ann,
	@NonTAUnavail = 100 - OpAvail_Ann - MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @NonTAUnavail_QTR = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	--, @MechUnavailTA_QTR=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail_Avg = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	--, @MechUnavailTA_Avg=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail_YTD = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	--, @MechUnavailTA_Avg=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd
AND m.FactorSet = @FactorSet

IF @NonTAUnavail < 0.05
	SET @NonTAUnavail = 0
IF @NonTAUnavail_QTR < 0.05
	SET @NonTAUnavail_QTR = 0
IF @NonTAUnavail_AVG < 0.05
	SET @NonTAUnavail_AVG = 0
SELECT @OpAvail = 100 - @NonTAUnavail - @MechUnavailTA, 
		@OpAvail_QTR = 100 - @NonTAUnavail_QTR - @MechUnavailTA, 
		@OpAvail_AVG = 100 - @NonTAUnavail_Avg - @MechUnavailTA,
		@OpAvail_YTD = 100 - @NonTAUnavail_YTD - @MechUnavailTA
	
EXEC spAverageFactors @RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, 
		@EII = @EII_QTR OUTPUT, @VEI = @VEI_QTR OUTPUT, @UtilPcnt = @RefUtilPcnt_QTR OUTPUT, @UtilOSTA = NULL, @EDC = @EDC_QTR OUTPUT, @UEDC = @UEDC_QTR OUTPUT, 
		@ProcessUtilPcnt = @ProcessUtilPcnt_QTR OUTPUT, @TotProcessEDC = @TotProcessEDC_QTR OUTPUT, @TotProcessUEDC = @TotProcessUEDC_QTR OUTPUT

SELECT @TotProcessEDC = TotProcessEDC, @TotProcessUEDC = TotProcessUEDC,
	@EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_QTR = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_QTR = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_QTR = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_QTR = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_QTR = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_QTR = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_AVG = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_AVG = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_AVG = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_AVG = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_AVG = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_AVG = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd
AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_YTD = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_YTD = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_YTD = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_YTD = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_YTD = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_YTD = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd
AND f.FactorSet = @FactorSet

SELECT @TotMaintForceWHrEDC_QTR = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @PersIndex_QTR = SUM(g.TotWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @MaintIndex_QTR = SUM((g.RoutIndex + g.TAIndex_AVG)*g.EDC)/SUM(g.EDC)
, @RoutIndex_QTR = SUM(g.RoutIndex*g.EDC)/SUM(g.EDC)
, @NEOpexEDC_QTR = SUM(g.NEOpexEDC*g.EDC)/SUM(g.EDC)
FROM PersTotCalc p INNER JOIN Gensum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet AND g.UOM = @UOM
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

SELECT @AnnTAWHr = SUM(TotWHr)/1000 FROM Pers WHERE SubmissionID = @SubmissionID AND PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr = TotNonTAWHr/1000
FROM PersTot
WHERE SubmissionID = @SubmissionID

SELECT @AnnTAWHr_QTR = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_QTR = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd

SELECT @AnnTAWHr_AVG = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_AVG = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd

SELECT @AnnTAWhr_YTD = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_YTD = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd

SELECT @AnnTACost = AllocAnnTACost/1000, @RoutCost = CurrRoutCost/1000
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency

SELECT @AnnTACost_QTR = SUM(AllocAnnTACost)/1000, @RoutCost_QTR = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

SELECT @AnnTACost_AVG = SUM(AllocAnnTACost)/1000, @RoutCost_AVG = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

SELECT @AnnTACost_YTD = SUM(AllocAnnTACost)/1000, @RoutCost_YTD = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

SELECT @NEOpex = NEOpex*Divisor/1000, @EnergyCost = EnergyCost*Divisor/1000, @TotCashOpex = TotCashOpex*Divisor/1000, @TAAdj = TAAdj*Divisor/1000
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpexUEDC = TotCashOpex
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpex_QTR = SUM(NEOpex*Divisor)/1000, @EnergyCost_QTR = SUM(EnergyCost*Divisor)/1000
	, @TotCashOpex_QTR = SUM(TotCashOpex*Divisor)/1000, @TAAdj_QTR = SUM(TAAdj*Divisor)/1000
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

SELECT @OpexUEDC_QTR = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'UEDC'

SELECT @NEOpex_AVG = SUM(NEOpex*Divisor)/1000, @EnergyCost_AVG = SUM(EnergyCost*Divisor)/1000
	, @TotCashOpex_AVG = SUM(TotCashOpex*Divisor)/1000, @TAAdj_AVG = SUM(TAAdj*Divisor)/1000
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

SELECT @OpexUEDC_AVG = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'UEDC'

SELECT @NEOpex_YTD = SUM(NEOpex*Divisor)/1000, @EnergyCost_YTD = SUM(EnergyCost*Divisor)/1000
	, @TotCashOpex_YTD = SUM(TotCashOpex*Divisor)/1000, @TAAdj_YTD = SUM(TAAdj*Divisor)/1000
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

SELECT @OpexUEDC_YTD = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'UEDC'

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_QTR = @EnergyUseDay_QTR * 1.055, @EnergyUseDay_AVG = @EnergyUseDay_AVG * 1.055, @EnergyUseDay_YTD = @EnergyUseDay_YTD * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_QTR = @TotStdEnergy_QTR * 1.055, @TotStdEnergy_AVG = @TotStdEnergy_AVG * 1.055, @TotStdEnergy_YTD = @TotStdEnergy_YTD * 1.055

SELECT 
	@EnergyUseDay = @EnergyUseDay/1000, @EnergyUseDay_QTR = @EnergyUseDay_QTR/1000, @EnergyUseDay_AVG = @EnergyUseDay_AVG/1000, @EnergyUseDay_YTD = @EnergyUseDay_YTD/1000, 
	@TotStdEnergy = @TotStdEnergy/1000, @TotStdEnergy_QTR = @TotStdEnergy_QTR/1000, @TotStdEnergy_AVG = @TotStdEnergy_AVG/1000, @TotStdEnergy_YTD = @TotStdEnergy_YTD/1000, 

	@EDC_QTR = @EDC_QTR/1000, @UEDC_QTR = @UEDC_QTR/1000,
	@TotProcessEDC = @TotProcessEDC/1000, @TotProcessEDC_QTR = @TotProcessEDC_QTR/1000, @TotProcessEDC_AVG = @TotProcessEDC_AVG/1000, @TotProcessEDC_YTD = @TotProcessEDC_YTD/1000, 
	@TotProcessUEDC = @TotProcessUEDC/1000, @TotProcessUEDC_QTR = @TotProcessUEDC_QTR/1000, @TotProcessUEDC_AVG = @TotProcessUEDC_AVG/1000, @TotProcessUEDC_YTD = @TotProcessUEDC_YTD/1000
	






