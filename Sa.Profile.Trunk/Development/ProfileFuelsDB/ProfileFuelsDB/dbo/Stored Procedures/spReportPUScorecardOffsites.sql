﻿
CREATE  PROC spReportPUScorecardOffsites (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency= @Currency, UOM = @UOM, 
c.UnitID, c.ProcessID, c.ProcessType, Cap = CASE WHEN @UOM = 'US' THEN c.Cap ELSE c.RptCap END, 
CapUnits = CASE WHEN @UOM = 'US' THEN d.DisplayTextUS WHEN s.UOM = 'US' THEN d.DisplayTextUS ELSE d.DisplayTextMET END,
fc.UtilPcnt, ISNULL(EDCNoMult,0) AS EDC, ISNULL(UEDCNoMult,0) AS UEDC
FROM Submissions s INNER JOIN Config c ON c.SubmissionID = s.SubmissionID
INNER JOIN FactorCalc fc ON c.UnitID = fc.UnitID AND c.SubmissionID = fc.SubmissionID
INNER JOIN ProcessID_lu p ON c.ProcessID = p.ProcessID
INNER JOIN DisplayUnits_LU d ON p.DisplayUnits = d.DisplayUnits
WHERE c.ProcessID IN ('STEAMGEN', 'ELECGEN', 'FCCPOWER') AND fc.factorset=@FactorSet
AND s.RefineryID=@RefineryID AND s.DataSet = @DataSet AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear)
ORDER BY s.PeriodStart DESC, p.SortKey


