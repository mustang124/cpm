﻿CREATE PROCEDURE [dbo].[DUMP_GPV_PrimaryProductsRefinery]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@PriceCol real,
	@DataSetID nvarchar(10),
	@RefNum nvarchar(10)
	
	AS
	
	SELECT s.Location, 
                 s.PeriodStart, 
                 s.PeriodEnd,s.NumDays as DaysInPeriod, 
                 @CurrencyCode as Currency,@UOM as UOM,y.MaterialID,
                 SAIName as MaterialName, SUM(ISNULL(BBL,0)) AS BBL, 
                 CASE WHEN SUM(ISNULL(BBL,0))=0 THEN 0 ELSE CAST( SUM(ISNULL(BBL,0)*ISNULL(PriceLocal,0))/SUM(BBL) AS REAL) END AS PriceLocal, 
                 CASE WHEN SUM(ISNULL(BBL,0))=0 THEN 0 ELSE CAST( SUM(ISNULL(BBL,0)*ISNULL(PriceUS,0))/SUM(BBL) AS REAL) END AS PriceUS, 
                 SUM(ISNULL(BBL,0))*SUM(ISNULL( @PriceCol ,0))/1000 As GPV, 
                 Min(y.Sortkey) AS SortKey
                 FROM Yield y 
                 INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID 
                 INNER JOIN Submissions s  ON s.SubmissionID=y.SubmissionID 
                 WHERE  Category in ( 'Prod','RPF') And LubesOnly = 0 And y.SubmissionID IN 
                (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=@DataSetID
                 AND RefineryID=@RefNum
                 ) GROUP BY s.PeriodStart,s.PeriodEnd,s.Location,y.MaterialID,UOM,s.NumDays,SAIName  ORDER BY s.PeriodStart DESC,SortKey ASC
