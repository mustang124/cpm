﻿CREATE   PROC [dbo].[spReportGazpromCoKPI2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2008', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

IF @RefineryID NOT IN ('106FL','150FL','322EUR')
	RETURN 1

/*
106FL = O
150FL = Y
322EUR = M
*/
DECLARE	
	@M_EII real, @M_EII_QTR real, @M_EII_AVG real, @M_EII_YTD real, 
	@M_RefUtilPcnt real, @M_RefUtilPcnt_QTR real, @M_RefUtilPcnt_AVG real, @M_RefUtilPcnt_YTD real, 
	@M_VEI real, @M_VEI_QTR real, @M_VEI_AVG real, @M_VEI_YTD real, 
	@M_OpAvail real, @M_OpAvail_QTR real, @M_OpAvail_AVG real, @M_OpAvail_YTD real,
	@M_RoutIndex real, @M_RoutIndex_QTR real, @M_RoutIndex_AVG real, @M_RoutIndex_YTD real,
	@M_PersIndex real, @M_PersIndex_QTR real, @M_PersIndex_AVG real, @M_PersIndex_YTD real, 
	@M_NEOpexEDC real, @M_NEOpexEDC_QTR real, @M_NEOpexEDC_AVG real, @M_NEOpexEDC_YTD real, 
	@M_OpexUEDC real, @M_OpexUEDC_QTR real, @M_OpexUEDC_AVG real, @M_OpexUEDC_YTD real, 
	@O_EII real, @O_EII_QTR real, @O_EII_AVG real, @O_EII_YTD real, 
	@O_RefUtilPcnt real, @O_RefUtilPcnt_QTR real, @O_RefUtilPcnt_AVG real, @O_RefUtilPcnt_YTD real, 
	@O_VEI real, @O_VEI_QTR real, @O_VEI_AVG real, @O_VEI_YTD real, 
	@O_OpAvail real, @O_OpAvail_QTR real, @O_OpAvail_AVG real, @O_OpAvail_YTD real, 
	@O_RoutIndex real, @O_RoutIndex_QTR real, @O_RoutIndex_AVG real, @O_RoutIndex_YTD real,
	@O_PersIndex real, @O_PersIndex_QTR real, @O_PersIndex_AVG real, @O_PersIndex_YTD real, 
	@O_NEOpexEDC real, @O_NEOpexEDC_QTR real, @O_NEOpexEDC_AVG real, @O_NEOpexEDC_YTD real, 
	@O_OpexUEDC real, @O_OpexUEDC_QTR real, @O_OpexUEDC_AVG real, @O_OpexUEDC_YTD real, 
	@Y_EII real, @Y_EII_QTR real, @Y_EII_AVG real, @Y_EII_YTD real, 
	@Y_RefUtilPcnt real, @Y_RefUtilPcnt_QTR real, @Y_RefUtilPcnt_AVG real, @Y_RefUtilPcnt_YTD real, 
	@Y_VEI real, @Y_VEI_QTR real, @Y_VEI_AVG real, @Y_VEI_YTD real, 
	@Y_OpAvail real, @Y_OpAvail_QTR real, @Y_OpAvail_AVG real, @Y_OpAvail_YTD real,
	@Y_RoutIndex real, @Y_RoutIndex_QTR real, @Y_RoutIndex_AVG real, @Y_RoutIndex_YTD real,
	@Y_PersIndex real, @Y_PersIndex_QTR real, @Y_PersIndex_AVG real, @Y_PersIndex_YTD real, 
	@Y_NEOpexEDC real, @Y_NEOpexEDC_QTR real, @Y_NEOpexEDC_AVG real, @Y_NEOpexEDC_YTD real, 
	@Y_OpexUEDC real, @Y_OpexUEDC_QTR real, @Y_OpexUEDC_AVG real, @Y_OpexUEDC_YTD real  

DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc2] '322EUR', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @M_EII OUTPUT, @EII_QTR = @M_EII_QTR OUTPUT, @EII_AVG = @M_EII_AVG OUTPUT, @EII_YTD = @M_EII_YTD OUTPUT, 
	@RefUtilPcnt = @M_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @M_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_AVG = @M_RefUtilPcnt_AVG OUTPUT, @RefUtilPcnt_YTD = @M_RefUtilPcnt_YTD OUTPUT, 
	@VEI = @M_VEI OUTPUT, @VEI_QTR = @M_VEI_QTR OUTPUT, @VEI_AVG = @M_VEI_AVG OUTPUT, @VEI_YTD = @M_VEI_YTD OUTPUT, 
	@OpAvail = @M_OpAvail OUTPUT, @OpAvail_QTR = @M_OpAvail_QTR OUTPUT, @OpAvail_AVG = @M_OpAvail_AVG OUTPUT, @OpAvail_YTD = @M_OpAvail_YTD OUTPUT, 
	@RoutIndex = @M_RoutIndex OUTPUT, @RoutIndex_QTR = @M_RoutIndex_QTR OUTPUT, @RoutIndex_AVG = @M_RoutIndex_AVG OUTPUT, @RoutIndex_YTD = @M_RoutIndex_YTD OUTPUT,
	@PersIndex = @M_PersIndex OUTPUT, @PersIndex_QTR = @M_PersIndex_QTR OUTPUT, @PersIndex_AVG = @M_PersIndex_AVG OUTPUT, @PersIndex_YTD = @M_PersIndex_YTD OUTPUT, 
	@NEOpexEDC = @M_NEOpexEDC OUTPUT, @NEOpexEDC_QTR = @M_NEOpexEDC_QTR OUTPUT, @NEOpexEDC_AVG = @M_NEOpexEDC_AVG OUTPUT, @NEOpexEDC_YTD = @M_NEOpexEDC_YTD OUTPUT, 
	@OpexUEDC = @M_OpexUEDC OUTPUT, @OpexUEDC_QTR = @M_OpexUEDC_QTR OUTPUT, @OpexUEDC_AVG = @M_OpexUEDC_AVG OUTPUT, @OpexUEDC_YTD = @M_OpexUEDC_YTD OUTPUT

IF @spResult > 1	
	RETURN @spResult

EXEC @spResult = [dbo].[spReportGazpromKPICalc2] '106FL', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @O_EII OUTPUT, @EII_QTR = @O_EII_QTR OUTPUT, @EII_AVG = @O_EII_AVG OUTPUT, @EII_YTD = @O_EII_YTD OUTPUT, 
	@RefUtilPcnt = @O_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @O_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_AVG = @O_RefUtilPcnt_AVG OUTPUT, @RefUtilPcnt_YTD = @O_RefUtilPcnt_YTD OUTPUT, 
	@VEI = @O_VEI OUTPUT, @VEI_QTR = @O_VEI_QTR OUTPUT, @VEI_AVG = @O_VEI_AVG OUTPUT, @VEI_YTD = @O_VEI_YTD OUTPUT, 
	@OpAvail = @O_OpAvail OUTPUT, @OpAvail_QTR = @O_OpAvail_QTR OUTPUT, @OpAvail_AVG = @O_OpAvail_AVG OUTPUT, @OpAvail_YTD = @O_OpAvail_YTD OUTPUT, 
	@RoutIndex = @O_RoutIndex OUTPUT, @RoutIndex_QTR = @O_RoutIndex_QTR OUTPUT, @RoutIndex_AVG = @O_RoutIndex_AVG OUTPUT, @RoutIndex_YTD = @O_RoutIndex_YTD OUTPUT,
	@PersIndex = @O_PersIndex OUTPUT, @PersIndex_QTR = @O_PersIndex_QTR OUTPUT, @PersIndex_AVG = @O_PersIndex_AVG OUTPUT, @PersIndex_YTD = @O_PersIndex_YTD OUTPUT, 
	@NEOpexEDC = @O_NEOpexEDC OUTPUT, @NEOpexEDC_QTR = @O_NEOpexEDC_QTR OUTPUT, @NEOpexEDC_AVG = @O_NEOpexEDC_AVG OUTPUT, @NEOpexEDC_YTD = @O_NEOpexEDC_YTD OUTPUT, 
	@OpexUEDC = @O_OpexUEDC OUTPUT, @OpexUEDC_QTR = @O_OpexUEDC_QTR OUTPUT, @OpexUEDC_AVG = @O_OpexUEDC_AVG OUTPUT, @OpexUEDC_YTD = @O_OpexUEDC_YTD OUTPUT

IF @spResult > 1	
	RETURN @spResult

EXEC @spResult = [dbo].[spReportGazpromKPICalc2] '150FL', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @Y_EII OUTPUT, @EII_QTR = @Y_EII_QTR OUTPUT, @EII_AVG = @Y_EII_AVG OUTPUT, @EII_YTD = @Y_EII_YTD OUTPUT, 
	@RefUtilPcnt = @Y_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @Y_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_AVG = @Y_RefUtilPcnt_AVG OUTPUT, @RefUtilPcnt_YTD = @Y_RefUtilPcnt_YTD OUTPUT, 
	@VEI = @Y_VEI OUTPUT, @VEI_QTR = @Y_VEI_QTR OUTPUT, @VEI_AVG = @Y_VEI_AVG OUTPUT, @VEI_YTD = @Y_VEI_YTD OUTPUT, 
	@OpAvail = @Y_OpAvail OUTPUT, @OpAvail_QTR = @Y_OpAvail_QTR OUTPUT, @OpAvail_AVG = @Y_OpAvail_AVG OUTPUT, @OpAvail_YTD = @Y_OpAvail_YTD OUTPUT, 
	@RoutIndex = @Y_RoutIndex OUTPUT, @RoutIndex_QTR = @Y_RoutIndex_QTR OUTPUT, @RoutIndex_AVG = @Y_RoutIndex_AVG OUTPUT, @RoutIndex_YTD = @Y_RoutIndex_YTD OUTPUT,
	@PersIndex = @Y_PersIndex OUTPUT, @PersIndex_QTR = @Y_PersIndex_QTR OUTPUT, @PersIndex_AVG = @Y_PersIndex_AVG OUTPUT, @PersIndex_YTD = @Y_PersIndex_YTD OUTPUT, 
	@NEOpexEDC = @Y_NEOpexEDC OUTPUT, @NEOpexEDC_QTR = @Y_NEOpexEDC_QTR OUTPUT, @NEOpexEDC_AVG = @Y_NEOpexEDC_AVG OUTPUT, @NEOpexEDC_YTD = @Y_NEOpexEDC_YTD OUTPUT, 
	@OpexUEDC = @Y_OpexUEDC OUTPUT, @OpexUEDC_QTR = @Y_OpexUEDC_QTR OUTPUT, @OpexUEDC_AVG = @Y_OpexUEDC_AVG OUTPUT, @OpexUEDC_YTD = @Y_OpexUEDC_YTD OUTPUT

IF @spResult > 1	
	RETURN @spResult

SELECT 
	M_EII = @M_EII, M_EII_QTR = @M_EII_QTR, M_EII_AVG = @M_EII_AVG, M_EII_YTD = @M_EII_YTD, 
	M_UtilPcnt = @M_RefUtilPcnt, M_UtilPcnt_QTR = @M_RefUtilPcnt_QTR, M_UtilPcnt_AVG = @M_RefUtilPcnt_AVG, M_UtilPcnt_YTD = @M_RefUtilPcnt_YTD, 
	M_VEI = @M_VEI, M_VEI_QTR = @M_VEI_QTR, M_VEI_AVG = @M_VEI_AVG, M_VEI_YTD = @M_VEI_YTD, 
	M_OpAvail = @M_OpAvail, M_OpAvail_QTR = @M_OpAvail_QTR, M_OpAvail_AVG = @M_OpAvail_AVG, M_OpAvail_YTD = @M_OpAvail_YTD, 
	M_RoutIndex = @M_RoutIndex, M_RoutIndex_QTR = @M_RoutIndex_QTR, M_RoutIndex_AVG = @M_RoutIndex_AVG, M_RoutIndex_YTD = @M_RoutIndex_YTD, 
	M_TotWHrEDC = @M_PersIndex, M_TotWHrEDC_QTR = @M_PersIndex_QTR, M_TotWHrEDC_AVG = @M_PersIndex_AVG, M_TotWHrEDC_YTD = @M_PersIndex_YTD, 
	M_NEOpexEDC = @M_NEOpexEDC, M_NEOpexEDC_QTR = @M_NEOpexEDC_QTR, M_NEOpexEDC_AVG = @M_NEOpexEDC_AVG, M_NEOpexEDC_YTD = @M_NEOpexEDC_YTD, 
	M_TotCashOpexUEDC = @M_OpexUEDC, M_TotCashOpexUEDC_QTR = @M_OpexUEDC_QTR, M_TotCashOpexUEDC_AVG = @M_OpexUEDC_AVG, M_TotCashOpexUEDC_YTD = @M_OpexUEDC_YTD,

	O_EII = @O_EII, O_EII_QTR = @O_EII_QTR, O_EII_AVG = @O_EII_AVG, O_EII_YTD = @O_EII_YTD, 
	O_UtilPcnt = @O_RefUtilPcnt, O_UtilPcnt_QTR = @O_RefUtilPcnt_QTR, O_UtilPcnt_AVG = @O_RefUtilPcnt_AVG, O_UtilPcnt_YTD = @O_RefUtilPcnt_YTD, 
	O_VEI = @O_VEI, O_VEI_QTR = @O_VEI_QTR, O_VEI_AVG = @O_VEI_AVG, O_VEI_YTD = @O_VEI_YTD, 
	O_OpAvail = @O_OpAvail, O_OpAvail_QTR = @O_OpAvail_QTR, O_OpAvail_AVG = @O_OpAvail_AVG, O_OpAvail_YTD = @O_OpAvail_YTD, 
	O_RoutIndex = @O_RoutIndex, O_RoutIndex_QTR = @O_RoutIndex_QTR, O_RoutIndex_AVG = @O_RoutIndex_AVG, O_RoutIndex_YTD = @O_RoutIndex_YTD, 
	O_TotWHrEDC = @O_PersIndex, O_TotWHrEDC_QTR = @O_PersIndex_QTR, O_TotWHrEDC_AVG = @O_PersIndex_AVG, O_TotWHrEDC_YTD = @O_PersIndex_YTD, 
	O_NEOpexEDC = @O_NEOpexEDC, O_NEOpexEDC_QTR = @O_NEOpexEDC_QTR, O_NEOpexEDC_AVG = @O_NEOpexEDC_AVG, O_NEOpexEDC_YTD = @O_NEOpexEDC_YTD, 
	O_TotCashOpexUEDC = @O_OpexUEDC, O_TotCashOpexUEDC_QTR = @O_OpexUEDC_QTR, O_TotCashOpexUEDC_AVG = @O_OpexUEDC_AVG, O_TotCashOpexUEDC_YTD = @O_OpexUEDC_YTD,

	Y_EII = @Y_EII, Y_EII_QTR = @Y_EII_QTR, Y_EII_AVG = @Y_EII_AVG, Y_EII_YTD = @Y_EII_YTD, 
	Y_UtilPcnt = @Y_RefUtilPcnt, Y_UtilPcnt_QTR = @Y_RefUtilPcnt_QTR, Y_UtilPcnt_AVG = @Y_RefUtilPcnt_AVG, Y_UtilPcnt_YTD = @Y_RefUtilPcnt_YTD, 
	Y_VEI = @Y_VEI, Y_VEI_QTR = @Y_VEI_QTR, Y_VEI_AVG = @Y_VEI_AVG, Y_VEI_YTD = @Y_VEI_YTD, 
	Y_OpAvail = @Y_OpAvail, Y_OpAvail_QTR = @Y_OpAvail_QTR, Y_OpAvail_AVG = @Y_OpAvail_AVG, Y_OpAvail_YTD = @Y_OpAvail_YTD, 
	Y_RoutIndex = @Y_RoutIndex, Y_RoutIndex_QTR = @Y_RoutIndex_QTR, Y_RoutIndex_AVG = @Y_RoutIndex_AVG, Y_RoutIndex_YTD = @Y_RoutIndex_YTD, 
	Y_TotWHrEDC = @Y_PersIndex, Y_TotWHrEDC_QTR = @Y_PersIndex_QTR, Y_TotWHrEDC_AVG = @Y_PersIndex_AVG, Y_TotWHrEDC_YTD = @Y_PersIndex_YTD, 
	Y_NEOpexEDC = @Y_NEOpexEDC, Y_NEOpexEDC_QTR = @Y_NEOpexEDC_QTR, Y_NEOpexEDC_AVG = @Y_NEOpexEDC_AVG, Y_NEOpexEDC_YTD = @Y_NEOpexEDC_YTD, 
	Y_TotCashOpexUEDC = @Y_OpexUEDC, Y_TotCashOpexUEDC_QTR = @Y_OpexUEDC_QTR, Y_TotCashOpexUEDC_AVG = @Y_OpexUEDC_AVG, Y_TotCashOpexUEDC_YTD = @Y_OpexUEDC_YTD


