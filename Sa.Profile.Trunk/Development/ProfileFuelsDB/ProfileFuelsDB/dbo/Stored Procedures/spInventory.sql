﻿CREATE PROC spInventory(@SubmissionID int)
AS
SET NOCOUNT ON

DELETE FROM InventoryTot WHERE SubmissionID = @SubmissionID

UPDATE Inventory 
SET Inven = FuelsStorage*AvgLevel/100
WHERE SubmissionID = @SubmissionID

INSERT INTO InventoryTot(SubmissionID, FuelsStorage, NumTank, Inven, AvgLevel, AvgVesselSize)
SELECT @SubmissionID, SUM(FuelsStorage), SUM(NumTank), SUM(Inven), 
CASE WHEN SUM(FuelsStorage)>0 THEN SUM(Inven)/SUM(FuelsStorage) END,
CASE WHEN SUM(NumTank)>0 THEN SUM(FuelsStorage)/SUM(NumTank) END
FROM Inventory
WHERE SubmissionID = @SubmissionID AND TankType <> 'NHC'

