﻿CREATE PROCEDURE [dbo].[DS_EnergySchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(e.TransType) as TransType, RTRIM(e.TransferTo) as TransferTo, RTRIM(e.EnergyType) as EnergyType, e.TransCode,elu.SortKey, 0.0 AS RptSource, 0.0 AS RptPriceLocal,
                  e.Hydrogen,e.Methane,e.Ethane,e.Ethylene,e.Propane,e.Propylene,e.Butane,e.Isobutane,e.C5Plus,e.CO,e.CO2,e.N2,e.Inerts FROM Energy e , Energy_LU elu WHERE SubmissionID=(SELECT MAX(SubmissionID) FROM Submissions WHERE RefineryID=@RefineryID) AND e.TransferTo = elu.TransferTo AND e.TransType = elu.TransType AND e.EnergyType = elu.EnergyType
                  
END
