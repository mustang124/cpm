﻿




CREATE      PROC [dbo].[spInitialCalcs](@SubmissionID int)
AS

SET NOCOUNT ON

UPDATE Submissions
SET 	PeriodStart = dbo.BuildDate(PeriodYear, PeriodMonth, 1),
	PeriodEnd = DATEADD(mm, 1, dbo.BuildDate(PeriodYear, PeriodMonth, 1))
WHERE SubmissionID = @SubmissionID

UPDATE Submissions
SET 	NumDays = DATEDIFF(d, PeriodStart, PeriodEnd),
	FractionOfYear = DATEDIFF(d, PeriodStart, PeriodEnd)/365.0
WHERE SubmissionID = @SubmissionID

UPDATE Submissions
SET 	RptCurrencyT15 = RptCurrency
WHERE SubmissionID = @SubmissionID AND RptCurrencyT15 IS NULL

INSERT INTO CurrenciesToCalc (RefineryID, Currency)
SELECT RefineryID, RptCurrency 
FROM Submissions s
WHERE NOT EXISTS (SELECT * FROM CurrenciesToCalc c WHERE c.RefineryId = s.RefineryID AND c.Currency = s.RptCurrency)
AND SubmissionID = @SubmissionID

INSERT INTO CurrenciesToCalc (RefineryID, Currency)
SELECT RefineryID, RptCurrencyT15
FROM Submissions s
WHERE NOT EXISTS (SELECT * FROM CurrenciesToCalc c WHERE c.RefineryId = s.RefineryID AND c.Currency = s.RptCurrencyT15)
AND SubmissionID = @SubmissionID

UPDATE Config
SET SortKey = ProcessID_LU.SortKey
FROM Config INNER JOIN ProcessID_LU ON ProcessID_LU.ProcessID = Config.ProcessID
WHERE Config.SubmissionID = @SubmissionID

UPDATE Config
SET BlockOp = NULL
WHERE SubmissionID = @SubmissionID AND BlockOp = ''

EXEC spSetUOM @SubmissionID
EXEC spLoadFactorData @SubmissionID
EXEC spEDCStabilizers @SubmissionID




