﻿
CREATE           PROC [dbo].[spCompleteSubmission](@SubmissionID int)
AS

EXEC spProcessTAs @SubmissionID
EXEC spProcessRoutHist @SubmissionID
EXEC spLoadEDCStabilizers @SubmissionID
EXEC TransferUnitTargets @SubmissionID

IF EXISTS (SELECT * FROM Submissions WHERE SubmissionID = @SubmissionID AND ClientVersion LIKE 'ProfileLit%')
	UPDATE Yield
	SET MaterialID = 'M154'
	WHERE Category = 'OTHRM' AND SubmissionID = @SubmissionID AND MaterialID = 'M123'

IF EXISTS (SELECT * FROM ProcessData WHERE Property = 'FeedGasRate' AND SubmissionID = @SubmissionID)
	UPDATE ProcessData SET Property = 'FeedRateGas' WHERE Property = 'FeedGasRate' AND SubmissionID = @SubmissionID
	
UPDATE Submissions 
SET CalcsNeeded = 'F' , Submitted = getdate()
WHERE SubmissionID = @SubmissionID

EXEC spClearUploading @SubmissionID

RAISERROR (50001, 7, 1)

