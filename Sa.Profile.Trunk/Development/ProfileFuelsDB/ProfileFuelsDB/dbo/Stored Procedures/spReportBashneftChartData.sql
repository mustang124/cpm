﻿




CREATE   PROC [dbo].[spReportBashneftChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2008', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	ProcessUtilPcnt real NULL, 
	VEI real NULL, 
	OpAvail real NULL, 
	MaintEffIndex real NULL,
	nmPersEffIndex real NULL, 
	NEOpexEDC real NULL,
	OpexUEDC real NULL
)
DECLARE @msgString varchar(255)
SELECT @msgString = CAST(@PeriodEnd as varchar(20))

--- Everything Already Available in Gensum
INSERT INTO @data (PeriodStart, PeriodEnd, EII, ProcessUtilPcnt, VEI, OpAvail, MaintEffIndex, nmPersEffIndex, NEOpexEDC, OpexUEDC)
SELECT	g.PeriodStart, g.PeriodEnd, g.EII, g.ProcessUtilPcnt, g.VEI, g.OpAvail, MaintEffIndex = mi.RoutEffIndex + mi.TAEffIndex_Avg, nmPersEffIndex = g.NonMaintPEI, g.NEOpexEDC, g.TotCashOpexUEDC
FROM Gensum g LEFT JOIN MaintIndex mi ON mi.SubmissionID = g.SubmissionID AND mi.Currency = g.Currency AND mi.FactorSet = g.FactorSet
WHERE g.RefineryID = @RefineryID AND g.DataSet = @DataSet AND g.FactorSet = @FactorSet AND g.Currency = 'USD' AND UOM = @UOM AND g.Scenario = @Scenario
AND g.PeriodStart >= @PeriodStart12Mo AND g.PeriodStart < @PeriodEnd

IF (SELECT COUNT(*) FROM @data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
			INSERT @data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, ProcessUtilPcnt, VEI, OpAvail, MaintEffIndex, nmPersEffIndex, NEOpexEDC, OpexUEDC
FROM @Data
ORDER BY PeriodStart ASC









