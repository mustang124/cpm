﻿CREATE PROCEDURE [dbo].[DUMP_PSD_OFFSITE_RS]

	@RefNum nvarchar(10),
	@DataSetID nvarchar(10),
	@StudyYear nvarchar(10)
	
	AS
	
	
	SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,s.RptCurrency as Currency,s.UOM, 
                                    c.UnitID, c.ProcessID, c.ProcessType, ISNULL(c.Throughput,0) As BDP, ISNULL(fc.EDCNoMult,0) AS EDC,  
                                     ISNULL(fc.UEDCNoMult,0) AS UEDC  
                                     FROM ConfigRS c, FactorCalc fc,displayunits_lu d,processid_lu p,Submissions s    
                                     WHERE c.unitid = fc.unitid and c.submissionid = fc.submissionid  AND  
                                    c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits   
                                     AND p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD')  
                                     AND fc.factorset= @StudyYear  AND s.SubmissionID=c.SubmissionID AND c.SubmissionId IN  
                                    (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=@DataSetID 
                                     AND RefineryID=@RefNum 
                                     ) ORDER BY PeriodStart DESC
