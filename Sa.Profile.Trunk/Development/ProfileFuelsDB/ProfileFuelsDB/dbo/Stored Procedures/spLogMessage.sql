﻿CREATE PROC spLogMessage(@SubmissionID int, @Source varchar(30), @Severity char(1) = 'I', @MessageText varchar(255) = '')
AS
INSERT INTO dbo.MessageLog(SubmissionID, Source, Severity, MessageText, MessageTime)
VALUES(@SubmissionID, @Source, @Severity, @MessageText, GetDate())

