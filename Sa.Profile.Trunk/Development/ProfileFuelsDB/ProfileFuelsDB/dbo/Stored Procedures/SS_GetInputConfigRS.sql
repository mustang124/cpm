﻿CREATE PROC [dbo].[SS_GetInputConfigRS]
	@RefineryID nvarchar(10)
AS

SELECT  
            MIN(s.SubmissionID), MIN(s.PeriodStart) as PeriodStart, MIN(s.PeriodEnd) as PeriodEnd, 
            RTRIM(ProcessID) AS ProcessID, 
            SUM(CASE WHEN ProcessType = 'RAIL' THEN ISNULL(Throughput,0) END) AS RailcarBBL, 
            SUM(CASE WHEN ProcessType IN ('TT', 'TTGD', 'TTO') THEN ISNULL(Throughput,0) END) AS TankTruckBBL, 
            SUM(CASE WHEN ProcessType = 'TB' THEN ISNULL(Throughput,0) END) AS TankerBerthBBL, 
            SUM(CASE WHEN ProcessType = 'OMB' THEN ISNULL(Throughput,0) END) AS OffshoreBuoyBBL, 
            SUM(CASE WHEN ProcessType = 'BB' THEN ISNULL(Throughput,0) END) AS BargeBerthBBL, 
            SUM(CASE WHEN ProcessType = 'PL' THEN ISNULL(Throughput,0) END) AS PipelineBBL  
            FROM ConfigRS c 
            ,dbo.Submissions s  
            WHERE  
            c.SubmissionID = s.SubmissionID AND 
            c.SubmissionID IN  
            (SELECT  SubmissionID FROM dbo.Submissions  
            WHERE RefineryID=@RefineryID) 
            GROUP BY ProcessID
