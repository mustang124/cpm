﻿

CREATE   PROC [dbo].[spReportThaiOilKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2008', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)

DECLARE	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_AVG real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_AVG real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_AVG real, 
	
	@OpAvail real, @OpAvail_QTR real, @OpAvail_AVG real, 
	@MechUnavailTA real, @MechUnavailTA_QTR real, @MechUnavailTA_AVG real, 
	@NonTAUnavail real, @NonTAUnavail_QTR real, @NonTAUnavail_AVG real, 

	@EII real, @EII_QTR real, @EII_AVG real, 
	@EnergyUseDay real, @EnergyUseDay_QTR real, @EnergyUseDay_AVG real, 
	@TotStdEnergy real, @TotStdEnergy_QTR real, @TotStdEnergy_AVG real, 

	@VEI real, @VEI_QTR real, @VEI_AVG real, 
	@ReportLossGain real, @ReportLossGain_QTR real, @ReportLossGain_AVG real, 
	@EstGain real, @EstGain_QTR real, @EstGain_AVG real, 

	@Gain real, @Gain_QTR real, @Gain_AVG real, 
	@RawMatl real, @RawMatl_QTR real, @RawMatl_AVG real, 
	@ProdYield real, @ProdYield_QTR real, @ProdYield_AVG real, 

	@PersIndex real, @PersIndex_QTR real, @PersIndex_AVG real, 
	@NonMaintWHr real, @NonMaintWHr_QTR real, @NonMaintWHr_AVG real, 
	@TotMaintForceWHr real, @TotMaintForceWHr_QTR real, @TotMaintForceWHr_AVG real, 
	@EDC real, @EDC_QTR real, @EDC_AVG real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_AVG real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_AVG real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_AVG real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_AVG real, 
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_AVG real, 
	@RoutCost real, @RoutCost_QTR real, @RoutCost_AVG real, 

	@NEOpexEDC real, @NEOpexEDC_QTR real, @NEOpexEDC_AVG real, 
	@NEOpex real, @NEOpex_QTR real, @NEOpex_AVG real, 

	@OpexUEDC real, @OpexUEDC_QTR real, @OpexUEDC_AVG real, 
	@UEDC real, @UEDC_QTR real, @UEDC_AVG real, 

	@OpexEDC real, @OpexEDC_QTR real, @OpexEDC_AVG real, 
	@EnergyCost real, @EnergyCost_QTR real, @EnergyCost_AVG real, 
	@TotCashOpex real, @TotCashOpex_QTR real, @TotCashOpex_AVG real 

--- Everything Already Available in Gensum
SELECT	@ProcessUtilPcnt = ProcessUtilPcnt, @ProcessUtilPcnt_AVG = ProcessUtilPcnt_AVG,
	@OpAvail = OpAvail, @OpAvail_AVG = OpAvail_AVG,
	@EII = EII, @EII_AVG = EII_AVG,
	@VEI = VEI, @VEI_AVG = VEI_AVG,
	@Gain = GainPcnt, @Gain_AVG = GainPcnt_AVG,
	@PersIndex = TotWHrEDC, @PersIndex_AVG = TotWHrEDC_AVG,
	@EDC = EDC/1000, @EDC_AVG = EDC_AVG/1000,
	@UEDC = UEDC/1000, @UEDC_AVG = UEDC_AVG/1000,
	@TotMaintForceWHrEDC = TotMaintForceWHrEDC, @TotMaintForceWHrEDC_AVG = TotMaintForceWHrEDC_Avg,
	@MaintIndex = RoutIndex + TAIndex_AVG, @MaintIndex_AVG = MaintIndex_AVG,
	@NEOpexEDC = NEOpexEDC, @NEOpexEDC_AVG = NEOpexEDC_AVG,
	@OpexUEDC = TotCashOpexUEDC, @OpexUEDC_AVG = TotCashOpexUEDC_AVG
FROM Gensum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_QTR = MechUnavailTA_Ann, @MechUnavailTA_AVG = MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @OpAvail_QTR=SUM(OpAvail_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear), 
	@MechUnavailTA_QTR=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
AND m.FactorSet = @FactorSet

SELECT @MechUnavailTA_AVG=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail = 100 - @OpAvail - @MechUnavailTA, @NonTAUnavail_QTR = 100 - @OpAvail_QTR - @MechUnavailTA_QTR, @NonTAUnavail_AVG = 100 - @OpAvail_AVG - @MechUnavailTA_AVG

EXEC spAverageFactors @RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, 
		@EII = @EII_QTR OUTPUT, @VEI = @VEI_QTR OUTPUT, @UtilPcnt = NULL, @UtilOSTA = NULL, @EDC = @EDC_QTR OUTPUT, @UEDC = @UEDC_QTR OUTPUT, 
		@ProcessUtilPcnt = @ProcessUtilPcnt_QTR OUTPUT, @TotProcessEDC = @TotProcessEDC_QTR OUTPUT, @TotProcessUEDC = @TotProcessUEDC_QTR OUTPUT

SELECT @TotProcessEDC = TotProcessEDC, @TotProcessUEDC = TotProcessUEDC,
	@EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_QTR = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_QTR = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_QTR = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_QTR = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_QTR = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_QTR = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_AVG = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_AVG = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_AVG = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_AVG = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_AVG = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_AVG = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd
AND f.FactorSet = @FactorSet

SELECT @RawMatl = NetInputBbl/(s.NumDays*1000.0), @ProdYield = (NetInputBbl+GainBbl)/(s.NumDays*1000.0)
FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.SubmissionID = @SubmissionID

SELECT @Gain_QTR = SUM(GainBbl)/SUM(NetInputBbl)*100, @RawMatl_QTR = SUM(NetInputBbl)/SUM(s.NumDays*1000.0), @ProdYield_QTR = SUM(NetInputBbl+GainBbl)/SUM(s.NumDays*1000.0)
FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND m.NetInputBbl > 0

SELECT @Gain_AVG = SUM(GainBbl)/SUM(NetInputBbl)*100, @RawMatl_AVG = SUM(NetInputBbl)/SUM(s.NumDays*1000.0), @ProdYield_AVG = SUM(NetInputBbl+GainBbl)/SUM(s.NumDays*1000.0)
FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND m.NetInputBbl > 0

SELECT @TotMaintForceWHrEDC_QTR = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @PersIndex_QTR = SUM(g.TotWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @MaintIndex_QTR = SUM((g.RoutIndex + g.TAIndex_AVG)*g.EDC)/SUM(g.EDC)
FROM PersTotCalc p INNER JOIN Gensum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet AND g.UOM = @UOM
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

SELECT @OpexUEDC_QTR = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'UEDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NEOpexEDC_QTR = SUM(NEOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'EDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NonMaintWHr = NonMaintWHr/1000, @TotMaintForceWHr = TotMaintForceWHr/1000, @MaintTAWHr = (TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr = (TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
FROM PersTot
WHERE SubmissionID = @SubmissionID

SELECT @NonMaintWHr_QTR = SUM(NonMaintWHr/1000), @TotMaintForceWHr_QTR = SUM(TotMaintForceWHr/1000), @MaintTAWHr_QTR = SUM(TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr_QTR = SUM(TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd

SELECT @NonMaintWHr_AVG = SUM(NonMaintWHr/1000), @TotMaintForceWHr_AVG = SUM(TotMaintForceWHr/1000), @MaintTAWHr_AVG = SUM(TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr_AVG = SUM(TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd

SELECT @AnnTACost = AllocAnnTACost/1000, @RoutCost = CurrRoutCost/1000
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency

SELECT @AnnTACost_QTR = SUM(AllocAnnTACost)/1000, @RoutCost_QTR = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

SELECT @AnnTACost_AVG = SUM(AllocAnnTACost)/1000, @RoutCost_AVG = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

SELECT @NEOpex = NEOpex*Divisor/1000, @OpexEDC = TotCashOpex, @EnergyCost = EnergyCost*Divisor/1000, @TotCashOpex = TotCashOpex*Divisor/1000
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @NEOpex_QTR = SUM(NEOpex*Divisor/1000), @OpexEDC_QTR = SUM(TotCashOpex*Divisor)/SUM(Divisor), @EnergyCost_QTR = SUM(EnergyCost*Divisor/1000), @TotCashOpex_QTR = SUM(TotCashOpex*Divisor/1000)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

SELECT @NEOpex_AVG = SUM(NEOpex*Divisor/1000), @OpexEDC_AVG = SUM(TotCashOpex*Divisor)/SUM(Divisor), @EnergyCost_AVG = SUM(EnergyCost*Divisor/1000), @TotCashOpex_AVG = SUM(TotCashOpex*Divisor/1000)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_QTR = @EnergyUseDay_QTR * 1.055, @EnergyUseDay_AVG = @EnergyUseDay_AVG * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_QTR = @TotStdEnergy_QTR * 1.055, @TotStdEnergy_AVG = @TotStdEnergy_AVG * 1.055
	
SELECT ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, ProcessUtilPcnt_AVG = @ProcessUtilPcnt_AVG, 
	TotProcessEDC = @TotProcessEDC/1000, TotProcessEDC_QTR = @TotProcessEDC_QTR/1000, TotProcessEDC_AVG = @TotProcessEDC_AVG/1000, 
	TotProcessUEDC = @TotProcessUEDC/1000, TotProcessUEDC_QTR = @TotProcessUEDC_QTR/1000, TotProcessUEDC_AVG = @TotProcessUEDC_AVG/1000, 
	
	OpAvail = @OpAvail, OpAvail_QTR = @OpAvail_QTR, OpAvail_AVG = @OpAvail_AVG, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_QTR = @MechUnavailTA_QTR, MechUnavailTA_AVG = @MechUnavailTA_AVG, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_QTR = @NonTAUnavail_QTR, NonTAUnavail_AVG = @NonTAUnavail_AVG, 

	EII = @EII, EII_QTR = @EII_QTR, EII_AVG = @EII_AVG, 
	EnergyUseDay = @EnergyUseDay/1000, EnergyUseDay_QTR = @EnergyUseDay_QTR/1000, EnergyUseDay_AVG = @EnergyUseDay_AVG/1000, 
	TotStdEnergy = @TotStdEnergy/1000, TotStdEnergy_QTR = @TotStdEnergy_QTR/1000, TotStdEnergy_AVG = @TotStdEnergy_AVG/1000, 

	VEI = @VEI, VEI_QTR = @VEI_QTR, VEI_AVG = @VEI_AVG, 
	ReportLossGain = @ReportLossGain, ReportLossGain_QTR = @ReportLossGain_QTR, ReportLossGain_AVG = @ReportLossGain_AVG, 
	EstGain = @EstGain, EstGain_QTR = @EstGain_QTR, EstGain_AVG = @EstGain_AVG, 

	Gain = -@Gain, Gain_QTR = -@Gain_QTR, Gain_AVG = -@Gain_AVG, 
	RawMatl = @RawMatl, RawMatl_QTR = @RawMatl_QTR, RawMatl_AVG = @RawMatl_AVG, 
	ProdYield = @ProdYield, ProdYield_QTR = @ProdYield_QTR, ProdYield_AVG = @ProdYield_AVG, 

	PersIndex = @PersIndex, PersIndex_QTR = @PersIndex_QTR, PersIndex_AVG = @PersIndex_AVG, 
	NonMaintWHr = @NonMaintWHr, NonMaintWHr_QTR = @NonMaintWHr_QTR, NonMaintWHr_AVG = @NonMaintWHr_AVG, 
	TotMaintForceWHr = @TotMaintForceWHr, TotMaintForceWHr_QTR = @TotMaintForceWHr_QTR, TotMaintForceWHr_AVG = @TotMaintForceWHr_AVG, 
	EDC = @EDC, EDC_QTR = @EDC_QTR/1000, EDC_AVG = @EDC_AVG, 
	UEDC = @UEDC, UEDC_QTR = @UEDC_QTR/1000, UEDC_AVG = @UEDC_AVG, 

	TotMaintForceWHrEDC = @TotMaintForceWHrEDC, TotMaintForceWHrEDC_QTR = @TotMaintForceWHrEDC_QTR, TotMaintForceWHrEDC_AVG = @TotMaintForceWHrEDC_AVG, 
	MaintTAWHr = @MaintTAWHr, MaintTAWHr_QTR = @MaintTAWHr_QTR, MaintTAWHr_AVG = @MaintTAWHr_AVG, 
	MaintNonTAWHr = @MaintNonTAWHr, MaintNonTAWHr_QTR = @MaintNonTAWHr_QTR, MaintNonTAWHr_AVG = @MaintNonTAWHr_AVG, 

	MaintIndex = @MaintIndex, MaintIndex_QTR = @MaintIndex_QTR, MaintIndex_AVG = @MaintIndex_AVG, 
	AnnTACost = @AnnTACost, AnnTACost_QTR = @AnnTACost_QTR, AnnTACost_AVG = @AnnTACost_AVG, 
	RoutCost = @RoutCost, RoutCost_QTR = @RoutCost_QTR, RoutCost_AVG = @RoutCost_AVG, 

	NEOpexEDC = @NEOpexEDC, NEOpexEDC_QTR = @NEOpexEDC_QTR, NEOpexEDC_AVG = @NEOpexEDC_AVG, 
	NEOpex = @NEOpex, NEOpex_QTR = @NEOpex_QTR, NEOpex_AVG = @NEOpex_AVG, 

	OpexUEDC = @OpexUEDC, OpexUEDC_QTR = @OpexUEDC_QTR, OpexUEDC_AVG = @OpexUEDC_AVG, 
	OpexEDC = @OpexEDC, OpexEDC_QTR = @OpexEDC_QTR, OpexEDC_AVG = @OpexEDC_AVG, 
	EnergyCost = @EnergyCost, EnergyCost_QTR = @EnergyCost_QTR, EnergyCost_AVG = @EnergyCost_AVG, 
	TotCashOpex = @TotCashOpex, TotCashOpex_QTR = @TotCashOpex_QTR, TotCashOpex_AVG = @TotCashOpex_AVG 





