﻿CREATE PROC [dbo].[SS_GetConfigRS]
	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
AS

	SELECT RTRIM(ProcessID) AS ProcessID,
            SUM(CASE WHEN ProcessType = 'RAIL' THEN ISNULL(Throughput,0) END) AS RailcarBBL,
            SUM(CASE WHEN ProcessType IN ('TT', 'TTGD', 'TTO') THEN ISNULL(Throughput,0) END) AS TankTruckBBL,
            SUM(CASE WHEN ProcessType = 'TB' THEN ISNULL(Throughput,0) END) AS TankerBerthBBL,
            SUM(CASE WHEN ProcessType = 'OMB' THEN ISNULL(Throughput,0) END) AS OffshoreBuoyBBL,
            SUM(CASE WHEN ProcessType = 'BB' THEN ISNULL(Throughput,0) END) AS BargeBerthBBL,
            SUM(CASE WHEN ProcessType = 'PL' THEN ISNULL(Throughput,0) END) AS PipelineBBL 
            FROM ConfigRS 
            WHERE SubmissionID IN 
            (SELECT  SubmissionID FROM dbo.Submissions 
				WHERE RefineryID=@RefineryID  and DataSet = @Dataset and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND DATEADD(Day, -1, @PeriodEnd)))
            GROUP BY ProcessID
