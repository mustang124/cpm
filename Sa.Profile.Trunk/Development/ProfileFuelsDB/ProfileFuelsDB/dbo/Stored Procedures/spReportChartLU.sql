﻿

CREATE  PROC spReportChartLU (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT ChartTitle,SectionHeader,SortKey,
CASE WHEN LEN(AxisLabelUS) > 0 THEN ', ' + AxisLabelUS ELSE AxisLabelUS END as AxisLabelUS , 
CASE WHEN LEN(AxisLabelMetric) > 0 THEN ', ' + AxisLabelMetric  ELSE AxisLabelMetric END as AxisLabelMetric,DataTable, 
TotField, TargetField, YTDField, AVGField, DecPlaces 
FROM Chart_LU
WHERE CustomGroup = 0
OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CustomType = 'C' AND CompanyID IN (SELECT CompanyID FROM Submissions WHERE RefineryID = @RefineryID AND DataSet=@DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth))



