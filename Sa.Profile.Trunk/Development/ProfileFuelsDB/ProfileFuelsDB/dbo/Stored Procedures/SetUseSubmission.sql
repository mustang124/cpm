﻿
CREATE PROC [dbo].[SetUseSubmission](@SubmissionID int)
AS
BEGIN

SET NOCOUNT ON

IF EXISTS (SELECT * FROM Submissions WHERE SubmissionID = @SubmissionID AND UseSubmission = 0)
BEGIN
	DECLARE @RefineryID char(6), @Dataset varchar(15), @PeriodYear smallint, @PeriodMonth tinyint, @PeriodStart smalldatetime
	SELECT @RefineryID = RefineryID, @Dataset = DataSet, @PeriodYear = PeriodYear, @PeriodMonth = PeriodMonth , @PeriodStart = PeriodStart
	FROM Submissions WHERE SubmissionID = @SubmissionID

	UPDATE Submissions SET UseSubmission = 0 
	WHERE RefineryID = @RefineryID AND Dataset = @DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth
	AND SubmissionID <> @SubmissionID AND UseSubmission = 1
	
	UPDATE Submissions SET UseSubmission = 1 WHERE SubmissionID = @SubmissionID
	
	UPDATE Submissions SET CalcsNeeded = 'A' 
	WHERE RefineryID = @RefineryID AND Dataset = @DataSet AND PeriodStart >= @PeriodStart AND CalcsNeeded IS NULL
	
	EXEC StartCalcs
END

SET NOCOUNT OFF

END


