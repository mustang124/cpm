﻿





CREATE       PROC [dbo].[spCalcUnitAvail](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @avail_cursor CURSOR VARYING OUTPUT)
AS
SET NOCOUNT ON

SELECT UnitID, MaintDown = SUM(MaintDown), MaintSlow = SUM(MaintSlow), 
RegDown = SUM(RegDown), RegSlow = SUM(RegSlow), 
OthDown = SUM(OthDown), OthSlow = SUM(OthSlow), PeriodHrs = SUM(PeriodHrs),
OthDownEconomic = SUM(OthDownEconomic), OthDownExternal = SUM(OthDownExternal), OthDownUnitUpsets = SUM(OthDownUnitUpsets), OthDownOffsiteUpsets = SUM(OthDownOffsiteUpsets), OthDownOther = SUM(OthDownOther), 
UnpRegDown = SUM(UnpRegDown), UnpMaintDown = SUM(UnpMaintDown), UnpOthDown  = SUM(UnpOthDown),
TADown = CAST(ISNULL((SELECT SUM(CASE WHEN RestartDate > @PeriodStart AND TADate < @PeriodEnd THEN DATEDIFF(hh, CASE WHEN TADate < @PeriodStart THEN @PeriodStart ELSE TADate END, CASE WHEN RestartDate > @PeriodEnd THEN @PeriodEnd ELSE RestartDate END) ELSE 0 END)
	FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND ISNULL(TAExceptions, 0) = 0 AND TAID > 0 AND MaintTA.UnitID = MaintRout.UnitID), 0) as real),
MechUnavailTA = ISNULL((SELECT ta.MechUnavailTA FROM MaintTA ta WHERE ta.RefineryID = @RefineryID AND ta.DataSet = @DataSet AND ta.TAID > 0 AND ta.UnitID = MaintRout.UnitID
	AND (ta.NextTADate > @PeriodEnd OR ta.NextTADate IS NULL) AND ta.TADate < @PeriodEnd), 0)
INTO #Hours
FROM MaintRout
WHERE PeriodHrs > 0 AND SubmissionID IN (SELECT SubmissionID FROM Submissions 
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet 
		AND PeriodStart >= @PeriodStart AND PeriodStart < @PeriodEnd)
GROUP BY UnitID
SET @avail_cursor = CURSOR FORWARD_ONLY STATIC FOR
SELECT m.UnitID, m.PeriodHrs, PeriodHrsOSTA = CASE WHEN (m.PeriodHrs-m.TADown) > 0 THEN (m.PeriodHrs-m.TADown) ELSE 0 END,
MechUnavailTA_Act = ISNULL(m.TADown, 0)/m.PeriodHrs * 100, 
MechAvailOSTA = CASE WHEN (m.PeriodHrs-m.TADown) > 0 THEN (1 - ISNULL(m.MaintDown, 0)/(m.PeriodHrs-m.TADown)) * 100 END,
MechAvail_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs) * 100, 
MechAvailSlow_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.MaintSlow, 0))/m.PeriodHrs) * 100, 
OpAvail_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0))/m.PeriodHrs) * 100,
OpAvailSlow_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0))/m.PeriodHrs) * 100,
OnStream_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0))/m.PeriodHrs) * 100,
OnStreamSlow_Act = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0) + ISNULL(m.OthSlow, 0))/m.PeriodHrs) * 100,  
MechUnavailTA_Ann = ISNULL(m.MechUnavailTA, 0),
MechAvail_Ann = (1 - (ISNULL(m.MaintDown, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
MechAvailSlow_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.MaintSlow, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
OpAvail_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
OpAvailSlow_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
OnStream_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
OnStreamSlow_Ann = (1 - (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0) + ISNULL(m.MaintSlow, 0) + ISNULL(m.RegSlow, 0) + ISNULL(m.OthSlow, 0))/m.PeriodHrs) * 100 - ISNULL(m.MechUnavailTA, 0),
MechUnavailPlan = (ISNULL(m.MaintDown, 0) - ISNULL(m.UnpMaintDown, 0))/m.PeriodHrs * 100,
MechUnavailUnp = ISNULL(m.UnpMaintDown, 0)/m.PeriodHrs * 100,
MechUnavail_Ann = ISNULL(m.MaintDown, 0)/m.PeriodHrs*100 + ISNULL(m.MechUnavailTA, 0),
MechUnavail_Act = (ISNULL(m.MaintDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs * 100,  
RegUnavail = ISNULL(m.RegDown, 0)/m.PeriodHrs * 100,
RegUnavailPlan = (ISNULL(m.RegDown, 0) - ISNULL(m.UnpRegDown, 0))/m.PeriodHrs * 100,
RegUnavailUnp = ISNULL(m.UnpRegDown, 0)/m.PeriodHrs * 100,
OpUnavail_Ann = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0))/m.PeriodHrs*100 + ISNULL(m.MechUnavailTA, 0),
OpUnavail_Act = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs * 100, 
OthUnavailEconomic = ISNULL(m.OthDownEconomic,0)/m.PeriodHrs*100, 
OthUnavailExternal = ISNULL(m.OthDownExternal,0)/m.PeriodHrs*100, 
OthUnavailUnitUpsets = ISNULL(m.OthDownUnitUpsets,0)/m.PeriodHrs*100, 
OthUnavailOffsiteUpsets = ISNULL(m.OthDownOffsiteUpsets,0)/m.PeriodHrs*100, 
OthUnavailOther = ISNULL(m.OthDownOther,0)/m.PeriodHrs*100, 
OthUnavail = ISNULL(m.OthDown,0)/m.PeriodHrs*100, 
OthUnavailPlan = (ISNULL(m.OthDown, 0) - ISNULL(m.UnpOthDown, 0))/m.PeriodHrs * 100,
OthUnavailUnp = ISNULL(m.UnpOthDown, 0)/m.PeriodHrs * 100,
TotUnavail_Ann = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0))/m.PeriodHrs*100 + ISNULL(m.MechUnavailTA, 0),
TotUnavail_Act = (ISNULL(m.MaintDown, 0) + ISNULL(m.RegDown, 0) + ISNULL(m.OthDown, 0) + ISNULL(m.TADown, 0))/m.PeriodHrs * 100, 
TotUnavailUnp = (ISNULL(m.UnpMaintDown, 0) + ISNULL(m.UnpRegDown, 0) + ISNULL(m.UnpOthDown, 0))/m.PeriodHrs * 100
FROM #Hours m
OPEN @avail_cursor






