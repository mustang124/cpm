﻿


CREATE                       PROC [dbo].[spUnitCalcs](@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @NumDays int, @RefineryID char(6), @PeriodStart smalldatetime, @Dataset varchar(15)
SELECT @NumDays = NumDays, @RefineryID = RefineryID, @PeriodStart = PeriodStart, @Dataset = DataSet
FROM Submissions WHERE SubmissionID = @SubmissionID
--IF EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND BlockOp IS NOT NULL)
--BEGIN
	--- Update ModePcnt
--END
DECLARE @AnnInputBbl float, @AnnCokeBbl float, @AnnElecConsMWH float,
	@AnnRSCRUDE_RAIL float, @AnnRSCRUDE_TT float, @AnnRSCRUDE_TB float, @AnnRSCRUDE_OMB float, @AnnRSCRUDE_BB float, 
	@AnnRSPROD_RAIL float, @AnnRSPROD_TT float, @AnnRSPROD_TB float, @AnnRSPROD_OMB float, @AnnRSPROD_BB float
SELECT @AnnInputBbl = AnnInputBbl, @AnnCokeBbl = AnnCokeBbl, @AnnElecConsMWH = AnnElecConsMWH,
	@AnnRSCRUDE_RAIL = AnnRSCRUDE_RAIL, @AnnRSCRUDE_TT = AnnRSCRUDE_TT, @AnnRSCRUDE_TB = AnnRSCRUDE_TB, @AnnRSCRUDE_OMB = AnnRSCRUDE_OMB, @AnnRSCRUDE_BB = AnnRSCRUDE_BB, 
	@AnnRSPROD_RAIL = AnnRSPROD_RAIL, @AnnRSPROD_TT = AnnRSPROD_TT, @AnnRSPROD_TB = AnnRSPROD_TB, @AnnRSPROD_OMB = AnnRSPROD_OMB, @AnnRSPROD_BB = AnnRSPROD_BB
FROM EDCStabilizers
WHERE SubmissionID = @SubmissionID 

DECLARE @NewUnitID int

-- Handle MFCapacity
UPDATE Config
SET StmCap = Cap * d.Ratio
FROM Config INNER JOIN Submissions s ON s.SubmissionID = Config.SubmissionID 
INNER JOIN UnitDefaults d ON d.RefineryID = s.RefineryID AND d.UnitId = config.UnitID AND s.PeriodStart >= d.EffDate AND s.PeriodStart < d.EffUntil
WHERE Config.SubmissionID = @SubmissionID AND ISNULL(Config.StmCap, 0) = 0 AND Config.ProcessID IN ('SDWAX','SOLVEX','LHYC','CDWAX','WDOIL','LHYFT','WHYFT','LACID','WACID')
UPDATE Config
SET StmUtilPcnt = Cap*UtilPcnt/StmCap
FROM Config INNER JOIN Submissions s ON s.SubmissionID = Config.SubmissionID 
WHERE Config.SubmissionID = @SubmissionID AND Config.StmCap > 0 AND Config.ProcessID IN ('SDWAX','SOLVEX','LHYC','CDWAX','WDOIL','LHYFT','WHYFT','LACID','WACID')

-- Load special calculated data for factors
DELETE FROM Config
WHERE ProcessID IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND SubmissionID = @SubmissionID

DECLARE @NextUnitID UnitID, @MaxConfigUnitID UnitID, @MaxRoutUnitID UnitID
SELECT @MaxConfigUnitID = MAX(UnitID) FROM Config WHERE SubmissionID = @SubmissionID
SELECT @MaxRoutUnitID = MAX(UnitID) FROM MaintRout WHERE SubmissionID = @SubmissionID
IF @MaxConfigUnitID IS NULL
	RETURN 1
IF @MaxConfigUnitID >= ISNULL(@MaxRoutUnitID,0)
	SELECT @NextUnitID = @MaxConfigUnitID + 1
ELSE
	SELECT @NextUnitID = @MaxRoutUnitID + 1
IF @NextUnitID < 91000
	SET @NewUnitID = 91001
	
-- Tankange & Blending Capacity (Cap excludes leased capacity, StmCap includes leased)
DECLARE @TankCap real, @TankCapRefOwn real, @TankCnt real, @TankCntRefOwn real, @TotInputBbl float, @TankInputBPD real, @TankRefOwnFraction real
IF EXISTS (SELECT * FROM Inventory WHERE SubmissionID = @SubmissionID)
	SELECT @TankCap = SUM(FuelsStorage), @TankCapRefOwn = SUM(FuelsStorage*(100-isnull(LeasedPcnt,0))/100)
		, @TankCnt = SUM(NumTank), @TankCntRefOwn = SUM(NumTank*(100-isnull(LeasedPcnt,0))/100)
	FROM Inventory WHERE SubmissionID = @SubmissionID
ELSE
	SELECT @TankCap = SUM(FuelsStorage), @TankCapRefOwn = SUM(FuelsStorage)
		, @TankCnt = SUM(NumTank), @TankCntRefOwn = SUM(NumTank)
	FROM dbo.StudyTankage WHERE RefineryID = @RefineryID AND DataSet = @Dataset AND EffDate <= @PeriodStart AND EffUntil > @PeriodStart

IF @TankCap = 0
	SET @TankRefOwnFraction = 1
ELSE
	SELECT @TankRefOwnFraction = @TankCapRefOwn/@TankCap
SELECT @TotInputBbl = TotInputBbl FROM MaterialTot WHERE SubmissionID = @SubmissionID
IF @TotInputBbl/@TankCap > 100 
	SELECT @TankInputBPD = 30*@TankCap/365.0
ELSE
	SELECT @TankInputBPD = @TotInputBbl/@NumDays
	
-- Tankange & Blending Capacity (Cap excludes leased tanks, StmCap includes leased)
INSERT Config (SubmissionID, UnitID, ProcessID, ProcessType, SortKey, UnitName, Cap, StmCap, UtilPcnt, StmUtilPcnt, InservicePcnt, YearsOper, MHPerWeek)
SELECT @SubmissionID, @NextUnitID, ProcessID, 'TCAP', SortKey, 'Tankage Capacity', 
Cap = @TankCapRefOwn / 365.0, StmCap = @TankCap / 365.0, 
UtilPcnt = 100, 100, InservicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
FROM ProcessID_LU WHERE ProcessID = 'TNK+BLND'
SELECT @NextUnitID = @NextUnitID + 1
-- Tankange & Blending Number of Tanks (Cap excludes leased tanks, StmCap includes leased)
INSERT Config (SubmissionID, UnitID, ProcessID, ProcessType, SortKey, UnitName, Cap, StmCap, UtilPcnt, StmUtilPcnt, InservicePcnt, YearsOper, MHPerWeek)
SELECT @SubmissionID, @NextUnitID, ProcessID, 'TCNT', SortKey, 'Number of Tanks', 
Cap = @TankCntRefOwn, StmCap = @TankCnt, 
UtilPcnt = 100, 100, InservicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
FROM ProcessID_LU WHERE ProcessID = 'TNK+BLND'
SELECT @NextUnitID = @NextUnitID + 1
-- Tankage and Blending Throughput (Net Input Barrels/day) (Cap accounts for leased capacity, StmCap includes leased)
INSERT Config (SubmissionID, UnitID, ProcessID, ProcessType, SortKey, UnitName, Cap, StmCap, UtilPcnt, StmUtilPcnt, InservicePcnt, YearsOper, MHPerWeek)
SELECT @SubmissionID, @NextUnitID, ProcessID, 'TBPD', SortKey, 'Input Barrels/Day', 
Cap = @TankInputBPD*@TankRefOwnFraction, StmCap = @TankInputBPD, 
UtilPcnt = 100, 100, InservicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
FROM ProcessID_LU WHERE ProcessID = 'TNK+BLND'
SELECT @NextUnitID = @NextUnitID + 1

-- Coke handling
DECLARE @CokeBPD real
IF @AnnCokeBbl IS NOT NULL
	SET @CokeBPD = @AnnCokeBbl/365.0
ELSE 
	SELECT @CokeBPD = SUM(Bbl)/@NumDays FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'COKE'
IF @CokeBPD > 0
BEGIN
	SET @NewUnitID = NULL
	SELECT @NewUnitID = MIN(UnitID) FROM MaintRout WHERE SubmissionID = @SubmissionID AND ProcessID = 'OFFCOKE'
	IF @NewUnitID IS NULL
	BEGIN
		SELECT @NewUnitID = @NextUnitID
		SELECT @NextUnitID = @NextUnitID + 1
	END
	INSERT Config (SubmissionID, UnitID, ProcessID, SortKey, UnitName, Cap, UtilPcnt, InservicePcnt, YearsOper, MHPerWeek)
	SELECT @SubmissionID, @NewUnitID, ProcessID, SortKey, 'Coke Handling', Cap = @CokeBPD, UtilPcnt = 100, InservicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
	FROM ProcessID_LU
	WHERE ProcessID = 'OFFCOKE' 
END
-- Electrical distribution capacity
DECLARE @ElecDistCap real
IF @AnnElecConsMWH IS NOT NULL
	SET @ElecDistCap = @AnnElecConsMWH*1000/(365.0 * 24)
ELSE 
	SELECT @ElecDistCap = CASE WHEN TotPowerConsMWH > 0 THEN TotPowerConsMWH ELSE 0 END*1000/(@NumDays * 24) 
	FROM EnergyTot WHERE SubmissionID = @SubmissionID
IF @ElecDistCap > 0
BEGIN
	INSERT Config (SubmissionID, UnitID, ProcessID, SortKey, UnitName, Cap, UtilPcnt, InservicePcnt, YearsOper, MHPerWeek)
	SELECT @SubmissionID, @NextUnitID, ProcessID, SortKey, Description, Cap = @ElecDistCap, UtilPcnt = 100, InservicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
	FROM ProcessID_LU WHERE ProcessID = 'ELECDIST'
	SELECT @NextUnitID = @NextUnitID + 1
END

UPDATE Config
SET 	AllocPcntOfCap = ISNULL(AllocPcntOfCap, 100),
	AllocPcntOfTime = ISNULL(AllocPcntOfTime, 100),
	InserviceCap = Cap*ISNULL(InservicePcnt, 0)/100,
	UtilCap = Cap*ISNULL(InservicePcnt,0)*ISNULL(UtilPcnt,0)/10000,
	ActualCap = Cap/(CASE WHEN ISNULL(AllocPcntOfCap, 100) > 0 THEN ISNULL(AllocPcntOfCap, 100)/100 ELSE NULL END),
	InserviceStmCap = StmCap*ISNULL(InservicePcnt, 0)/100,
	StmUtilCap = StmCap*ISNULL(InservicePcnt,0)*ISNULL(StmUtilPcnt,0)/10000,
	ActualStmCap = StmCap/(CASE WHEN ISNULL(AllocPcntOfCap, 100) > 0 THEN ISNULL(AllocPcntOfCap,100)/100 ELSE NULL END),
	AllocUtilPcnt = UtilPcnt/(CASE WHEN ISNULL(AllocPcntOfTime, 100) > 0 THEN ISNULL(AllocPcntOfTime, 100)/100 ELSE NULL END),
	ModePcnt = ISNULL(ModePcnt, 100)
WHERE SubmissionID = @SubmissionID
UPDATE ConfigRS
SET 	Vessels = Throughput/CASE WHEN AvgSize > 0 THEN AvgSize ELSE NULL END,
	BPD = Throughput/@NumDays
WHERE SubmissionID = @SubmissionID
DELETE FROM ProcessTotCalc WHERE SubmissionID = @SubmissionID
INSERT INTO ProcessTotCalc(SubmissionID, ProcessID, NumUnits, Cap, UtilCap, StmCap, StmUtilCap, InServiceCap, InserviceStmCap, ActualCap, ActualStmCap, AllocPcntOfTime)
SELECT SubmissionID, ProcessID, COUNT(*), SUM(Cap), SUM(UtilCap), SUM(StmCap), SUM(StmUtilCap), SUM(InServiceCap), SUM(InserviceStmCap), SUM(ActualCap), SUM(ActualStmCap), SUM(AllocPcntOfTime*Cap)/SUM(Cap)
FROM Config
WHERE (BlockOp IS NULL OR BlockOp = 'BP') AND Cap > 0
AND SubmissionID = @SubmissionID
GROUP BY SubmissionID, ProcessID
DECLARE @RVLocFactor real, @InflFactor real
SELECT @RVLocFactor = RVLocFactor, @InflFactor = InflFactor
FROM Submissions s INNER JOIN RefineryFactors r ON r.RefineryID = s.RefineryID AND s.PeriodStart BETWEEN r.EffDate AND r.EffUntil
WHERE s.SubmissionID = @SubmissionID
DELETE FROM FactorCalc WHERE SubmissionID = @SubmissionID

/* Calculate percent of hydrogen in feed for hydrogen generation units for standard energy calculation */
/* Has to be here because spLoadFactorData that does the other updates to UnitFactorData happens before UtilCap is calculated */
UPDATE UnitFactorData
SET EIIFeedH2PcntProdH2 = CASE
			WHEN FeedRateGas > 0 THEN ISNULL(FeedH2,0)*FeedRateGas/CASE WHEN c.UtilCap > 0 THEN c.UtilCap END 
			ELSE ISNULL(FeedH2,0)/((100-ISNULL(FeedH2,0))*7.6 + ISNULL(FeedH2,0))*100
			END
FROM UnitFactorData INNER JOIN Config c ON c.SubmissionID = UnitFactorData.SubmissionID AND c.UnitID = UnitFactorData.UnitID AND c.ProcessID = 'HYG'
WHERE UnitFactorData.SubmissionID = @SubmissionID

DECLARE cFactorSets CURSOR LOCAL SCROLL
FOR
SELECT FactorSet, NEOpexEffMult, MaintEffMult, PersEffMult, MaintPersEffMult, NonMaintPersEffMult 
FROM FactorSets WHERE Calculate = 'Y' AND RefineryType = 'FUELS'

DECLARE @FactorSet FactorSet, @NEOpexEffMult real, @MaintEffMult real, @PersEffMult real, @MaintPersEffMult real, @NonMaintPersEffMult real
DECLARE @UnitID int, @ProcessID ProcessID, @ProcessType ProcessType, @Cap real, @UtilPcnt real, @StmCap real, @StmUtilPcnt real, @InservicePcnt real, @StmUtilCap real, @AllocPcntOfTime real, @AllocPcntOfCap real, @ActualCap real, @ActualStmCap real, @ModePcnt real, @DesignFeedSulfur real
DECLARE @StdMult real, @UtilMult real, @PlantMult real, @NPMult real, @NPPlantMult real
DECLARE @FullEDC real, @FullUEDC real, @FullNEOpexEffDiv real, @FullMaintEffDiv real, @FullPersEffDiv real, @FullMaintPersEffDiv real, @FullNonMaintPersEffDiv real, @FullRV real
DECLARE @FullEDC2 real, @FullNEOpexEffDiv2 real, @FullMaintEffDiv2 real, @FullPersEffDiv2 real, @FullMaintPersEffDiv2 real, @FullNonMaintPersEffDiv2 real, @FullRV2 real
DECLARE @EDCFactor real, @EDCFactor2 real, @EDCNoMult real, @UEDCNoMult real, 
	@NonProratedPlantEDC real, @NonProratedEDC real, @PlantEDC real, 
	@RVSulfurFactor real, @RV real, @NonProratedRV real, @PlantRV real, @NonProratedPlantRV real,
	@NEOpexEffFactor real, @NEOpexEffFactor2 real, @NEOpexEffDiv real, @NonProratedNEOpexEffDiv real, @PlantNEOpexEffDiv real, @NonProratedPlantNEOpexEffDiv real,
	@MaintEffFactor real, @MaintEffFactor2 real, @MaintEffDiv real, @NonProratedMaintEffDiv real, @PlantMaintEffDiv real, @NonProratedPlantMaintEffDiv real,
	@PersEffFactor real, @PersEffFactor2 real, @PersEffDiv real, @NonProratedPersEffDiv real, @PlantPersEffDiv real, @NonProratedPlantPersEffDiv real,
	@MaintPersEffFactor real, @MaintPersEffFactor2 real, @MaintPersEffDiv real, @NonProratedMaintPersEffDiv real, @PlantMaintPersEffDiv real, @NonProratedPlantMaintPersEffDiv real,
	@NonMaintPersEffFactor real, @NonMaintPersEffFactor2 real, @NonMaintPersEffDiv real, @NonProratedNonMaintPersEffDiv real, @PlantNonMaintPersEffDiv real, @NonProratedPlantNonMaintPersEffDiv real
DECLARE @EDCConstant real, @EDCExponent real, @EDCMinCap real, @EDCMaxCap real,
	@NEOpexEffConstant real, @NEOpexEffExponent real, @NEOpexEffMinCap real, @NEOpexEffMaxCap real,
	@MaintEffConstant real, @MaintEffExponent real, @MaintEffMinCap real, @MaintEffMaxCap real,
	@PersEffConstant real, @PersEffExponent real, @PersEffMinCap real, @PersEffMaxCap real,
	@MaintPersEffConstant real, @MaintPersEffExponent real, @MaintPersEffMinCap real, @MaintPersEffMaxCap real,
	@NonMaintPersEffConstant real, @NonMaintPersEffExponent real, @NonMaintPersEffMinCap real, @NonMaintPersEffMaxCap real,
	@RVConstant real, @RVExponent real, @RVNomCap real, @SulfurAdj char(1), @BaseSulfur real,
	@MultGroup ProcessID, @UnitConvPcntFormula varchar(255)
DECLARE cUnits CURSOR LOCAL FAST_FORWARD 
FOR
SELECT UnitID, ProcessID, ProcessType, Cap, UtilPcnt, StmCap, StmUtilPcnt, InservicePcnt, AllocPcntOfTime, AllocPcntOfCap, ActualCap, ActualStmCap, ModePcnt, DesignFeedSulfur
FROM Config WHERE SubmissionID = @SubmissionID
UNION
SELECT UnitID, ProcessID, ProcessType, Cap = ISNULL(BPD, 0), UtilPcnt = 100, StmCap = NULL, StmUtilPcnt = NULL, InservicePcnt = 100, AllocPcntOfTime = 100, AllocPcntOfCap = 100, ActualCap = ISNULL(BPD, 0), ActualStmCap = NULL, ModePcnt = 100, DesignFeedSulfur = NULL
FROM ConfigRS WHERE SubmissionID = @SubmissionID
OPEN cFactorSets
OPEN cUnits
FETCH NEXT FROM cUnits INTO @UnitID, @ProcessID, @ProcessType, @Cap, @UtilPcnt, @StmCap, @StmUtilPcnt, @InservicePcnt, @AllocPcntOfTime, @AllocPcntOfCap, @ActualCap, @ActualStmCap, @ModePcnt, @DesignFeedSulfur
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Override Receipt/shipment volumes if provided in EDCStabilizers
	IF @ProcessID = 'RSCRUDE'
	BEGIN
		IF @ProcessType = 'RAIL' AND @AnnRSCRUDE_RAIL IS NOT NULL
			SELECT @Cap = @AnnRSCRUDE_RAIL/365.0
		ELSE IF @ProcessType = 'TT' AND @AnnRSCRUDE_TT IS NOT NULL
			SELECT @Cap = @AnnRSCRUDE_TT/365.0
		ELSE IF @ProcessType = 'TB' AND @AnnRSCRUDE_TB IS NOT NULL
			SELECT @Cap = @AnnRSCRUDE_TB/365.0
		ELSE IF @ProcessType = 'OMB' AND @AnnRSCRUDE_OMB IS NOT NULL
			SELECT @Cap = @AnnRSCRUDE_OMB/365.0
		ELSE IF @ProcessType = 'BB' AND @AnnRSCRUDE_BB IS NOT NULL
			SELECT @Cap = @AnnRSCRUDE_BB/365.0
		SET @ActualCap = @Cap
	END
	ELSE IF @ProcessID = 'RSPROD' 
	BEGIN
		IF @ProcessType = 'RAIL' AND @AnnRSPROD_RAIL IS NOT NULL
			SELECT @Cap = @AnnRSPROD_RAIL/365.0
		ELSE IF @ProcessType = 'TT' AND @AnnRSPROD_TT IS NOT NULL
			SELECT @Cap = @AnnRSPROD_TT/365.0
		ELSE IF @ProcessType = 'TB' AND @AnnRSPROD_TB IS NOT NULL
			SELECT @Cap = @AnnRSPROD_TB/365.0
		ELSE IF @ProcessType = 'OMB' AND @AnnRSPROD_OMB IS NOT NULL
			SELECT @Cap = @AnnRSPROD_OMB/365.0
		ELSE IF @ProcessType = 'BB' AND @AnnRSPROD_BB IS NOT NULL
			SELECT @Cap = @AnnRSPROD_BB/365.0
		SET @ActualCap = @Cap
	END

	SELECT 	@NPPlantMult = (@AllocPcntOfCap/100)
	SELECT	@NPMult = @NPPlantMult * (@AllocPcntOfTime/100) 
	SELECT	@PlantMult = @NPPlantMult * (@InservicePcnt/100)
	SELECT	@StdMult = @NPMult * (@InservicePcnt/100) 
	SELECT	@UtilMult = @PlantMult * (@ModePcnt/100)
		
	FETCH FIRST FROM cFactorSets INTO @FactorSet, @NEOpexEffMult, @MaintEffMult, @PersEffMult, @MaintPersEffMult, @NonMaintPersEffMult
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT	@EDCConstant = NULL, @EDCExponent = NULL, @EDCMinCap = NULL, @EDCMaxCap = NULL, 
			@NEOpexEffConstant = NULL, @NEOpexEffExponent = NULL, @NEOpexEffMinCap = NULL, @NEOpexEffMaxCap = NULL,
			@MaintEffConstant = NULL, @MaintEffExponent = NULL, @MaintEffMinCap = NULL, @MaintEffMaxCap = NULL,
			@PersEffConstant = NULL, @PersEffExponent = NULL, @PersEffMinCap = NULL, @PersEffMaxCap = NULL,
			@MaintPersEffConstant = NULL, @MaintPersEffExponent = NULL, @MaintPersEffMinCap = NULL, @MaintPersEffMaxCap = NULL,
			@NonMaintPersEffConstant = NULL, @NonMaintPersEffExponent = NULL, @NonMaintPersEffMinCap = NULL, @NonMaintPersEffMaxCap = NULL,
			@RVConstant = NULL, @RVExponent = NULL, @RVNomCap = NULL, @SulfurAdj = NULL, @BaseSulfur = NULL,
			@MultGroup = NULL, @UnitConvPcntFormula = NULL
		SELECT	@EDCConstant = EDCConstant, @EDCExponent = EDCExponent, @EDCMinCap = EDCMinCap, @EDCMaxCap = EDCMaxCap, 
			@NEOpexEffConstant = NEOpexEffConstant, @NEOpexEffExponent = NEOpexEffExponent, @NEOpexEffMinCap = NEOpexEffMinCap, @NEOpexEffMaxCap = NEOpexEffMaxCap,
			@MaintEffConstant = MaintEffConstant, @MaintEffExponent = MaintEffExponent, @MaintEffMinCap = MaintEffMinCap, @MaintEffMaxCap = MaintEffMaxCap,
			@PersEffConstant = PersEffConstant, @PersEffExponent = PersEffExponent, @PersEffMinCap = PersEffMinCap, @PersEffMaxCap = PersEffMaxCap,
			@MaintPersEffConstant = MaintPersEffConstant, @MaintPersEffExponent = MaintPersEffExponent, @MaintPersEffMinCap = MaintPersEffMinCap, @MaintPersEffMaxCap = MaintPersEffMaxCap,
			@NonMaintPersEffConstant = NonMaintPersEffConstant, @NonMaintPersEffExponent = NonMaintPersEffExponent, @NonMaintPersEffMinCap = NonMaintPersEffMinCap, @NonMaintPersEffMaxCap = NonMaintPersEffMaxCap,
			@RVConstant = RVConstant, @RVExponent = RVExponent, @RVNomCap = RVNomCap, @SulfurAdj = SulfurAdj, @BaseSulfur = BaseSulfur,
			@MultGroup = MultGroup, @UnitConvPcntFormula = UnitConvPcntFormula
		FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = @ProcessID AND ProcessType = @ProcessType AND CogenES IN ('','E')
		/*
		IF @ProcessID = 'OFFCOKE' AND @ActualCap > 0
		BEGIN
			IF NOT EXISTS (SELECT * FROM MaintRout WHERE SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart BETWEEN DATEADD(mm, -24, @PeriodStart) AND @PeriodStart) AND ProcessID = 'OFFCOKE' AND RoutCostLocal > 0)
				AND NOT EXISTS (SELECT * FROM TAForSubmission WHERE SubmissionID = @SubmissionID AND ProcessID = 'OFFCOKE' AND TACostLocal > 0)
			BEGIN
				IF EXISTS (SELECT * FROM FactorSets WHERE FactorSet = @FactorSet AND OffCokeExclusion IN ('MaintEff', 'All'))
					SELECT @MaintEffConstant = CASE WHEN @MaintEffConstant IS NOT NULL THEN 0 END
						, @MaintPersEffConstant = CASE WHEN @MaintPersEffConstant IS NOT NULL THEN 0 END
						--, @MaintEffConstant2 = CASE WHEN @MaintEffConstant2 IS NOT NULL THEN 0 END
						--, @MaintPersEffConstant2 = CASE WHEN @MaintPersEffConstant2 IS NOT NULL THEN 0 END

				--IF NOT EXISTS (SELECT * FROM PStaff WHERE Refnum = @Refnum AND CokeHandle_MHWK > 0)
				IF EXISTS (SELECT * FROM FactorSets WHERE FactorSet = @FactorSet AND OffCokeExclusion IN ('All'))
					SELECT @NEOpexEffConstant = CASE WHEN @NEOpexEffConstant IS NOT NULL THEN 0 END
						, @NonMaintPersEffConstant = CASE WHEN @NonMaintPersEffConstant IS NOT NULL THEN 0 END
						, @PersEffConstant = CASE WHEN @PersEffConstant IS NOT NULL THEN 0 END
						, @EDCConstant = CASE WHEN @EDCConstant IS NOT NULL THEN 0 END
						--, @NEOpexEffConstant2 = CASE WHEN @NEOpexEffConstant2 IS NOT NULL THEN 0 END
						--, @NonMaintPersEffConstant2 = CASE WHEN @NonMaintPersEffConstant2 IS NOT NULL THEN 0 END
						--, @PersEffConstant2 = CASE WHEN @PersEffConstant2 IS NOT NULL THEN 0 END
						--, @EDCConstant2 = CASE WHEN @EDCConstant2 IS NOT NULL THEN 0 END
			END
		END
		*/
		EXEC spCalcFactor @ActualCap, @EDCConstant, @EDCExponent, @EDCMinCap, @EDCMaxCap, @EDCFactor OUTPUT, @FullEDC OUTPUT
		SELECT @FullUEDC = @FullEDC*@UtilPcnt/100
		EXEC spCalcFactor @ActualCap, @NEOpexEffConstant, @NEOpexEffExponent, @NEOpexEffMinCap, @NEOpexEffMaxCap, @NEOpexEffFactor OUTPUT, @FullNEOpexEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @MaintEffConstant, @MaintEffExponent, @MaintEffMinCap, @MaintEffMaxCap, @MaintEffFactor OUTPUT, @FullMaintEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @PersEffConstant, @PersEffExponent, @PersEffMinCap, @PersEffMaxCap, @PersEffFactor OUTPUT, @FullPersEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @MaintPersEffConstant, @MaintPersEffExponent, @MaintPersEffMinCap, @MaintPersEffMaxCap, @MaintPersEffFactor OUTPUT, @FullMaintPersEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @NonMaintPersEffConstant, @NonMaintPersEffExponent, @NonMaintPersEffMinCap, @NonMaintPersEffMaxCap, @NonMaintPersEffFactor OUTPUT, @FullNonMaintPersEffDiv OUTPUT
		SELECT @RVSulfurFactor = ISNULL(dbo.RVSulfurAdj(@DesignFeedSulfur, @BaseSulfur, @SulfurAdj), 1)
		SELECT @FullRV = @RVConstant * power(@ActualCap/@RVNomCap, @RVExponent)
		IF @ActualStmCap > 0
		BEGIN
			SELECT	@EDCConstant = NULL, @EDCExponent = NULL, @EDCMinCap = NULL, @EDCMaxCap = NULL, 
				@NEOpexEffConstant = NULL, @NEOpexEffExponent = NULL, @NEOpexEffMinCap = NULL, @NEOpexEffMaxCap = NULL,
				@MaintEffConstant = NULL, @MaintEffExponent = NULL, @MaintEffMinCap = NULL, @MaintEffMaxCap = NULL,
				@PersEffConstant = NULL, @PersEffExponent = NULL, @PersEffMinCap = NULL, @PersEffMaxCap = NULL,
				@MaintPersEffConstant = NULL, @MaintPersEffExponent = NULL, @MaintPersEffMinCap = NULL, @MaintPersEffMaxCap = NULL,
				@NonMaintPersEffConstant = NULL, @NonMaintPersEffExponent = NULL, @NonMaintPersEffMinCap = NULL, @NonMaintPersEffMaxCap = NULL,
				@RVConstant = NULL, @RVExponent = NULL, @RVNomCap = NULL, @SulfurAdj = NULL, @BaseSulfur = NULL,
				@MultGroup = NULL, @UnitConvPcntFormula = NULL
			SELECT	@EDCConstant = EDCConstant, @EDCExponent = EDCExponent, @EDCMinCap = EDCMinCap, @EDCMaxCap = EDCMaxCap, 
				@NEOpexEffConstant = NEOpexEffConstant, @NEOpexEffExponent = NEOpexEffExponent, @NEOpexEffMinCap = NEOpexEffMinCap, @NEOpexEffMaxCap = NEOpexEffMaxCap,
				@MaintEffConstant = MaintEffConstant, @MaintEffExponent = MaintEffExponent, @MaintEffMinCap = MaintEffMinCap, @MaintEffMaxCap = MaintEffMaxCap,
				@PersEffConstant = PersEffConstant, @PersEffExponent = PersEffExponent, @PersEffMinCap = PersEffMinCap, @PersEffMaxCap = PersEffMaxCap,
				@MaintPersEffConstant = MaintPersEffConstant, @MaintPersEffExponent = MaintPersEffExponent, @MaintPersEffMinCap = MaintPersEffMinCap, @MaintPersEffMaxCap = MaintPersEffMaxCap,
				@NonMaintPersEffConstant = NonMaintPersEffConstant, @NonMaintPersEffExponent = NonMaintPersEffExponent, @NonMaintPersEffMinCap = NonMaintPersEffMinCap, @NonMaintPersEffMaxCap = NonMaintPersEffMaxCap,
				@RVConstant = RVConstant, @RVExponent = RVExponent, @RVNomCap = RVNomCap, @SulfurAdj = SulfurAdj, @BaseSulfur = BaseSulfur,
				@MultGroup = MultGroup, @UnitConvPcntFormula = UnitConvPcntFormula
			FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = @ProcessID AND ProcessType = @ProcessType AND CogenES = 'S'
			EXEC spCalcFactor @ActualStmCap, @EDCConstant, @EDCExponent, @EDCMinCap, @EDCMaxCap, @EDCFactor2 OUTPUT, @FullEDC2 OUTPUT
			EXEC spCalcFactor @ActualStmCap, @NEOpexEffConstant, @NEOpexEffExponent, @NEOpexEffMinCap, @NEOpexEffMaxCap, @NEOpexEffFactor2 OUTPUT, @FullNEOpexEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualStmCap, @MaintEffConstant, @MaintEffExponent, @MaintEffMinCap, @MaintEffMaxCap, @MaintEffFactor2 OUTPUT, @FullMaintEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualStmCap, @PersEffConstant, @PersEffExponent, @PersEffMinCap, @PersEffMaxCap, @PersEffFactor2 OUTPUT, @FullPersEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualStmCap, @MaintPersEffConstant, @MaintPersEffExponent, @MaintPersEffMinCap, @MaintPersEffMaxCap, @MaintPersEffFactor2 OUTPUT, @FullMaintPersEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualStmCap, @NonMaintPersEffConstant, @NonMaintPersEffExponent, @NonMaintPersEffMinCap, @NonMaintPersEffMaxCap, @NonMaintPersEffFactor2 OUTPUT, @FullNonMaintPersEffDiv2 OUTPUT
			SELECT @FullRV2 = ISNULL(@RVConstant * power(@ActualStmCap/@RVNomCap, @RVExponent), 0)
			SELECT 	@FullEDC = @FullEDC + ISNULL(@FullEDC2, 0),
				@FullUEDC = @FullUEDC + ISNULL(@FullEDC2*@StmUtilPcnt/100, 0),
				@FullNEOpexEffDiv = @FullNEOpexEffDiv + ISNULL(@FullNEOpexEffDiv2, 0),
				@FullMaintEffDiv = @FullMaintEffDiv + ISNULL(@FullMaintEffDiv2, 0),
				@FullPersEffDiv = @FullPersEffDiv + ISNULL(@FullPersEffDiv2, 0),
				@FullMaintPersEffDiv = @FullMaintPersEffDiv + ISNULL(@FullMaintPersEffDiv2, 0),
				@FullNonMaintPersEffDiv = @FullNonMaintPersEffDiv + ISNULL(@FullNonMaintPersEffDiv2, 0),
				@FullRV = @FullRV + ISNULL(@FullRV2, 0)
		END
		ELSE BEGIN
			SELECT 	@EDCFactor2 = NULL, @NEOpexEffFactor2 = NULL, @MaintEffFactor2 = NULL, @PersEffFactor2 = NULL, @MaintPersEffFactor2 = NULL, @NonMaintPersEffFactor2 = NULL, 
				@FullEDC2 = NULL, @FullNEOpexEffDiv2 = NULL, @FullMaintEffDiv2 = NULL, @FullPersEffDiv2 = NULL, @FullMaintPersEffDiv2 = NULL, @FullNonMaintPersEffDiv2 = NULL, @FullRV2 = NULL
		END
		SELECT	@NEOpexEffFactor = @NEOpexEffFactor * @NEOpexEffMult, @NEOpexEffFactor2 = @NEOpexEffFactor2 * @NEOpexEffMult, @FullNEOpexEffDiv = @FullNEOpexEffDiv * @NEOpexEffMult
		SELECT	@MaintEffFactor = @MaintEffFactor * @MaintEffMult, @MaintEffFactor2 = @MaintEffFactor2 * @MaintEffMult, @FullMaintEffDiv = @FullMaintEffDiv * @MaintEffMult
		SELECT	@PersEffFactor = @PersEffFactor * @PersEffMult, @PersEffFactor2 = @PersEffFactor2 * @PersEffMult, @FullPersEffDiv = @FullPersEffDiv * @PersEffMult
		SELECT	@MaintPersEffFactor = @MaintPersEffFactor * @MaintPersEffMult, @MaintPersEffFactor2 = @MaintPersEffFactor2 * @MaintPersEffMult, @FullMaintPersEffDiv = @FullMaintPersEffDiv * @MaintPersEffMult
		SELECT	@NonMaintPersEffFactor = @NonMaintPersEffFactor * @NonMaintPersEffMult, @NonMaintPersEffFactor2 = @NonMaintPersEffFactor2 * @NonMaintPersEffMult, @FullNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @NonMaintPersEffMult
		SELECT 	@EDCNoMult = @FullEDC*@StdMult, 
			@UEDCNoMult = @FullUEDC*@UtilMult, 
			@NonProratedPlantEDC = @FullEDC*@NPPlantMult, 
			@NonProratedEDC = @FullEDC*@NPMult, 
			@PlantEDC = @FullEDC*@PlantMult
		SELECT 	@NEOpexEffDiv = @FullNEOpexEffDiv*@StdMult,
			@NonProratedPlantNEOpexEffDiv = @FullNEOpexEffDiv*@NPPlantMult,
			@NonProratedNEOpexEffDiv = @FullNEOpexEffDiv*@NPMult,
			@PlantNEOpexEffDiv = @FullNEOpexEffDiv*@PlantMult
		SELECT 	@MaintEffDiv = @FullMaintEffDiv*@StdMult,
			@NonProratedPlantMaintEffDiv = @FullMaintEffDiv*@NPPlantMult,
			@NonProratedMaintEffDiv = @FullMaintEffDiv*@NPMult,
			@PlantMaintEffDiv = @FullMaintEffDiv*@PlantMult
		SELECT 	@PersEffDiv = @FullPersEffDiv*@StdMult,
			@NonProratedPlantPersEffDiv = @FullPersEffDiv * @NPPlantMult,
			@NonProratedPersEffDiv = @FullPersEffDiv * @NPMult,
			@PlantPersEffDiv = @FullPersEffDiv * @PlantMult
		SELECT 	@MaintPersEffDiv = @FullMaintPersEffDiv*@StdMult,
			@NonProratedPlantMaintPersEffDiv = @FullMaintPersEffDiv * @NPPlantMult,
			@NonProratedMaintPersEffDiv = @FullMaintPersEffDiv * @NPMult,
			@PlantMaintPersEffDiv = @FullMaintPersEffDiv * @PlantMult
		SELECT 	@NonMaintPersEffDiv = @FullNonMaintPersEffDiv*@StdMult,
			@NonProratedPlantNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @NPPlantMult,
			@NonProratedNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @NPMult,
			@PlantNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @PlantMult
		SELECT 	@FullRV = @FullRV * @RVSulfurFactor * @RVLocFactor * @InflFactor
		SELECT 	@RV = @FullRV * @StdMult,
			@NonProratedRV = @FullRV * @NPMult,
			@PlantRV = @FullRV * @PlantMult,
			@NonProratedPlantRV = @FullRV * @NPPlantMult
		
		IF @FullPersEffDiv IS NULL AND @FullMaintPersEffDiv IS NOT NULL
			SELECT @FullPersEffDiv = @FullMaintPersEffDiv + @FullNonMaintPersEffDiv
				,	@PlantPersEffDiv = @PlantMaintPersEffDiv + @PlantNonMaintPersEffDiv
				,	@NonProratedPersEffDiv = @NonProratedMaintPersEffDiv + @NonProratedNonMaintPersEffDiv
				,	@NonProratedPlantPersEffDiv = @NonProratedPlantMaintPersEffDiv + @NonProratedPlantNonMaintPersEffDiv
				,	@PersEffDiv = @MaintPersEffDiv + @NonMaintPersEffDiv

		INSERT INTO FactorCalc (SubmissionID, FactorSet, UnitID, ProcessID, 
			EDCFactor, EDCFactor2, MultGroup, EDCNoMult, UEDCNoMult, UtilPcnt,
			NonProratedPlantEDC, NonProratedEDC, PlantEDC, 
			RVSulfurFactor, RV, NonProratedRV, PlantRV, NonProratedPlantRV,
			NEOpexEffFactor, NEOpexEffFactor2, NEOpexEffDiv, NonProratedNEOpexEffDiv, PlantNEOpexEffDiv, NonProratedPlantNEOpexEffDiv,
			MaintEffFactor, MaintEffFactor2, MaintEffDiv, NonProratedMaintEffDiv, PlantMaintEffDiv, NonProratedPlantMaintEffDiv,
			PersEffFactor, PersEffFactor2, PersEffDiv, NonProratedPersEffDiv, PlantPersEffDiv, NonProratedPlantPersEffDiv,
			MaintPersEffFactor, MaintPersEffFactor2, MaintPersEffDiv, NonProratedMaintPersEffDiv, PlantMaintPersEffDiv, NonProratedPlantMaintPersEffDiv,
			NonMaintPersEffFactor, NonMaintPersEffFactor2, NonMaintPersEffDiv, NonProratedNonMaintPersEffDiv, PlantNonMaintPersEffDiv, NonProratedPlantNonMaintPersEffDiv)
		SELECT	@SubmissionID, @FactorSet, @UnitID, @ProcessID, 
			@EDCFactor, @EDCFactor2, @MultGroup, @EDCNoMult, @UEDCNoMult, CASE WHEN @EDCNoMult = 0 THEN @UtilPcnt ELSE @UEDCNoMult/@EDCNoMult*100 END,
			@NonProratedPlantEDC, @NonProratedEDC, @PlantEDC,
			@RVSulfurFactor, @RV, @NonProratedRV, @PlantRV, @NonProratedPlantRV,
			@NEOpexEffFactor, @NEOpexEffFactor2, @NEOpexEffDiv, @NonProratedNEOpexEffDiv, @PlantNEOpexEffDiv, @NonProratedPlantNEOpexEffDiv,
			@MaintEffFactor, @MaintEffFactor2, @MaintEffDiv, @NonProratedMaintEffDiv, @PlantMaintEffDiv, @NonProratedPlantMaintEffDiv,
			@PersEffFactor, @PersEffFactor2, @PersEffDiv, @NonProratedPersEffDiv, @PlantPersEffDiv, @NonProratedPlantPersEffDiv,
			@MaintPersEffFactor, @MaintPersEffFactor2, @MaintPersEffDiv, @NonProratedMaintPersEffDiv, @PlantMaintPersEffDiv, @NonProratedPlantMaintPersEffDiv,
			@NonMaintPersEffFactor, @NonMaintPersEffFactor2, @NonMaintPersEffDiv, @NonProratedNonMaintPersEffDiv, @PlantNonMaintPersEffDiv, @NonProratedPlantNonMaintPersEffDiv
			
		FETCH NEXT FROM cFactorSets INTO @FactorSet, @NEOpexEffMult, @MaintEffMult, @PersEffMult, @MaintPersEffMult, @NonMaintPersEffMult
	END
	FETCH NEXT FROM cUnits INTO @UnitID, @ProcessID, @ProcessType, @Cap, @UtilPcnt, @StmCap, @StmUtilPcnt, @InservicePcnt, @AllocPcntOfTime, @AllocPcntOfCap, @ActualCap, @ActualStmCap, @ModePcnt, @DesignFeedSulfur
END
CLOSE cUnits
DEALLOCATE cUnits
CLOSE cFactorSets
DEALLOCATE cFactorSets
DECLARE @UtilCap real, @EIIFormula varchar(255), @VEIFormula varchar(255), @EIIFormula2 varchar(255), @VEIFormula2 varchar(255)
DECLARE @strUnitID varchar(20), @strSubmissionID varchar(20)
DECLARE cFactorCalc CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
	SELECT 	c.UnitID, FactorCalc.FactorSet, c.UtilCap, c.StmUtilCap,
	EIIFormula = CASE WHEN c.ProcessID = 'ASP' THEN 'NULL' ELSE f.EIIFormula END, f.VEIFormula,
	EIIFormula = CASE WHEN c.ProcessID = 'ASP' THEN 'NULL' ELSE fs.EIIFormula END, fs.VEIFormula
	FROM FactorCalc INNER JOIN Config c ON c.SubmissionID = FactorCalc.SubmissionID AND c.UnitID = FactorCalc.UnitID
	INNER JOIN Factors f ON f.FactorSet = FactorCalc.FactorSet AND f.ProcessID = c.ProcessID AND f.ProcessType = c.ProcessType AND f.CogenES IN ('','E')
	LEFT JOIN Factors fs ON fs.FactorSet = FactorCalc.FactorSet AND fs.ProcessID = c.ProcessID AND fs.ProcessType = c.ProcessType AND fs.CogenES = 'S'
	WHERE FactorCalc.SubmissionID = @SubmissionID
OPEN cFactorCalc
FETCH NEXT FROM cFactorCalc INTO @UnitID, @FactorSet, @UtilCap, @StmUtilCap, @EIIFormula, @VEIFormula, @EIIFormula2, @VEIFormula2
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT 	@strUnitID = CAST(@UnitID AS varchar(20)), 
		@strSubmissionID = CAST(@SubmissionID AS varchar(20))
	SELECT @EIIFormula = CASE WHEN ISNULL(@EIIFormula, '') = '' THEN 'NULL' WHEN ISNULL(@UtilCap, 0) <= 0 THEN 'NULL' ELSE CAST(@UtilCap AS varchar(20)) + '*(' + @EIIFormula + ')/1000' END
		, @EIIFormula2 = CASE WHEN ISNULL(@EIIFormula2, '') = '' THEN 'NULL' WHEN ISNULL(@StmUtilCap, 0) <= 0 THEN 'NULL' ELSE CAST(@StmUtilCap AS varchar(20)) + '*(' + @EIIFormula2 + ')/1000' END
		, @VEIFormula = CASE WHEN ISNULL(@VEIFormula, '') = '' THEN 'NULL' WHEN ISNULL(@UtilCap, 0) <= 0 THEN 'NULL' ELSE CAST(@UtilCap AS varchar(20)) + '*(' + @VEIFormula + ')' END
		, @VEIFormula2 = CASE WHEN ISNULL(@VEIFormula2, '') = '' THEN 'NULL' WHEN ISNULL(@StmUtilCap, 0) <= 0 THEN 'NULL' ELSE CAST(@StmUtilCap AS varchar(20)) + '*(' + @VEIFormula2 + ')' END
	EXEC ('UPDATE FactorCalc SET StdEnergy = ISNULL(' + @EIIFormula + ', 0) + ISNULL(' + @EIIFormula2 + ',0), '
		+ ' StdGain = ISNULL(' + @VEIFormula + ',0) + ISNULL(' + @VEIFormula2 + ', 0)'
		+ ' FROM FactorCalc LEFT JOIN UnitFactorData u ON u.SubmissionID = FactorCalc.SubmissionID AND u.UnitID = FactorCalc.UnitID '
		+ ' WHERE FactorCalc.SubmissionID = ' + @strSubmissionID + ' AND FactorCalc.FactorSet = ''' + @FactorSet + ''''
		+ ' AND FactorCalc.UnitID = ' + @strUnitID)
	FETCH NEXT FROM cFactorCalc INTO @UnitID, @FactorSet, @UtilCap, @StmUtilCap, @EIIFormula, @VEIFormula, @EIIFormula2, @VEIFormula2
END
CLOSE cFactorCalc
DEALLOCATE cFactorCalc
EXEC spFactorTotCalc @SubmissionID












