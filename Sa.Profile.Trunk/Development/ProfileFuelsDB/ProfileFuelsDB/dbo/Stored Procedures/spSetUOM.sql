﻿


CREATE               PROC [dbo].[spSetUOM](@SubmissionID int)
AS
SET NOCOUNT ON 
DECLARE @IsMetric bit, @RptCurrency CurrencyCode, @PeriodStart smalldatetime, @RptCurrencyT15 CurrencyCode, @H2ConsNM3PerM3 bit
SELECT @IsMetric = CASE WHEN UOM = 'MET' THEN 1 ELSE 0 END, @RptCurrency = RptCurrency, @RptCurrencyT15 = RptCurrencyT15, @PeriodStart = PeriodStart,
	@H2ConsNM3PerM3 = CASE WHEN UOM = 'MET' AND RefineryID IN ('150FL') THEN 1 ELSE 0 END
FROM Submissions
WHERE SubmissionID = @SubmissionID
UPDATE Config
SET RptCapUOM = CASE WHEN @IsMetric = 1 THEN ProcessID_LU.MetricCapUnits ELSE ProcessID_LU.USCapUnits END,
Cap = RptCap * CASE WHEN @IsMetric = 1 THEN ProcessID_LU.MetricToUSMult ELSE 1 END
FROM Config INNER JOIN ProcessID_LU ON ProcessID_LU.ProcessID = Config.ProcessID
WHERE Config.SubmissionID = @SubmissionID
UPDATE Config
SET RptStmCapUOM = CASE WHEN @IsMetric = 1 THEN 'TONNE/HR' ELSE 'KLB/HR' END,
StmCap = RptStmCap * CASE WHEN @IsMetric = 1 THEN 2.2046 ELSE 1 END
WHERE SubmissionID = @SubmissionID AND ProcessID IN ('COGEN','FTCOGEN')
UPDATE ProcessData
SET RptUOM = CASE WHEN @IsMetric = 1 THEN Table2_LU.MetricUOM ELSE Table2_LU.USUOM END,
SAValue = CASE WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'KGM3' THEN dbo.KGM3toAPI(RptValue)
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'BARG' THEN 14.504 * RptValue
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'NM3/B' THEN RptValue *1000 / 26.79
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'NM3/M3' THEN RptValue *1000 / 26.79/6.29
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'TC' THEN 1.8*RptValue+32
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'DTC' THEN 1.8*RptValue
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'GJ' THEN dbo.UnitsConv(RptValue, Table2_LU.MetricUOM, 'MBTU')
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'M2' THEN dbo.UnitsConv(RptValue, Table2_LU.MetricUOM, 'SQFT')
	WHEN @IsMetric = 1 AND Table2_LU.MetricUOM = 'KNM3' THEN dbo.UnitsConv(RptValue, Table2_LU.MetricUOM, 'KSCF')
	ELSE RptValue END
FROM ProcessData INNER JOIN Config ON Config.SubmissionID = ProcessData.SubmissionID AND Config.UnitID = ProcessData.UnitID
INNER JOIN Table2_LU ON Table2_LU.ProcessID = Config.ProcessID AND Table2_LU.Property = ProcessData.Property
WHERE ProcessData.SubmissionID = @SubmissionID

IF @H2ConsNM3PerM3 = 1
	UPDATE ProcessData
	SET RptUOM = 'NM3/M3', SAValue = RptValue *1000 / 26.79/6.29
	WHERE ProcessData.SubmissionID = @SubmissionID AND Property = 'H2Cons'

UPDATE Energy
SET SourceMBTU = CASE WHEN TransType IN ('PUR','PRO') OR (TransType = 'DST' AND TransferTo = 'REF') THEN ABS(RptSource) / CASE WHEN @IsMetric = 1 THEN 1.055 ELSE 1 END ELSE NULL END,
UsageMBTU = CASE WHEN TransType IN ('PUR','PRO') OR (TransType = 'DST' AND TransferTo = 'REF') THEN NULL ELSE ABS(RptSource) / CASE WHEN @IsMetric = 1 THEN 1.055 ELSE 1 END END,
PriceMBTULocal = RptPriceLocal * CASE WHEN @IsMetric = 1 THEN 1.055 ELSE 1 END,
PriceMBTUUS = RptPriceLocal * CASE WHEN @IsMetric = 1 THEN 1.055 ELSE 1 END*dbo.ExchangeRate(@RptCurrency, 'USD', @PeriodStart)
WHERE SubmissionID = @SubmissionID
UPDATE Electric
SET GenEff = RptGenEff / CASE WHEN @IsMetric = 1 THEN 1.055 ELSE 1 END,
SourceMWH = CASE WHEN TransType IN ('PRO', 'PUR') OR (TransType = 'DST' AND TransferTo = 'REF') THEN ABS(RptMWH) ELSE NULL END,
UsageMWH = CASE WHEN TransType IN ('PRO', 'PUR') OR (TransType = 'DST' AND TransferTo = 'REF') THEN NULL ELSE ABS(RptMWH) END,
PriceUS = PriceLocal * dbo.ExchangeRate(@RptCurrency, 'USD', @PeriodStart)
WHERE SubmissionID = @SubmissionID
UPDATE Yield
SET PriceUS = PriceLocal * dbo.ExchangeRate(@RptCurrencyT15, 'USD', @PeriodStart)
WHERE SubmissionID = @SubmissionID


