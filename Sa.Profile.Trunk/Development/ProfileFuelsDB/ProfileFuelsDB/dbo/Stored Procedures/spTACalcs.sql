﻿



CREATE       PROC spTACalcs(@RefineryID varchar(6), @Dataset varchar(15), @UnitID UnitID, @TAID int)
AS
SET NOCOUNT ON

IF @TAID = 0
	RETURN

DECLARE @TADate smalldatetime, @PrevTADate smalldatetime, @TAHrsDown real, 
	@TACostLocal real, @TAMatlLocal real, 
	@TAOCCSTH real, @TAOCCOVT real, @TAMPSSTH real, @TAMPSOVTPcnt real, @TAContOCC real, @TAContMPS real,
	@TAExceptions tinyint, @TACurrency CurrencyCode
SELECT 	@TADate = TADate, @PrevTADate = PrevTADate, @TAHrsDown = TAHrsDown, 
	@TACostLocal = TACostLocal, @TAMatlLocal = TAMatlLocal, @TACurrency = TACurrency, 
	@TAOCCSTH = TAOCCSTH, @TAOCCOVT = TAOCCOVT, @TAMPSSTH = TAMPSSTH, @TAMPSOVTPcnt = TAMPSOVTPcnt, 
	@TAContOCC = TAContOCC, @TAContMPS = TAContMPS, @TAExceptions = TAExceptions
FROM MaintTA
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
DECLARE @TACostUS real, @TAMatlUS real
IF @TACurrency IS NULL
	SELECT @TACurrency = RptCurrency FROM Submissions
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet
SELECT	@TACostUS = @TACostLocal * dbo.ExchangeRate(@TACurrency, 'USD', @TADate), @TAMatlUS = @TAMatlLocal * dbo.ExchangeRate(@TACurrency, 'USD', @TADate)
DECLARE @TAIntDays real, @TAIntYrs real, @RestartDate smalldatetime,
	@AnnTACostLocal real, @AnnTAMatlLocal real, @AnnTACost real, @AnnTAMatl real,
	@TAMPSOVT real, @AnnTAOCCSTH real, @AnnTAOCCOVT	real, @AnnTAMPSSTH real, @AnnTAMPSOVT real,
	@AnnTAContOCCWHr real, @AnnTAContMPSWHr real, @MechUnavailTA real, @TAMatlPcnt real
SELECT 	@TAIntDays = DATEDIFF(d, @PrevTADate, @TADate),
	@TAIntYrs = DATEDIFF(d, @PrevTADate, @TADate)/365.25,
	@RestartDate = DATEADD(hh, @TAHrsDown, @TADate),
	@TAMPSOVT = @TAMPSSTH * ISNULL(@TAMPSOVTPcnt, 0)/100,
	@TAMatlPcnt = CASE WHEN @TACostLocal > 0 THEN @TAMatlLocal / @TACostLocal * 100 END
IF @TAIntYrs > 0
	SELECT 	@AnnTACostLocal = @TACostLocal/@TAIntYrs, 
		@AnnTAMatlLocal = @TAMatlLocal/@TAIntYrs, 
		@AnnTACost = @TACostUS/@TAIntYrs, @AnnTAMatl = @TAMatlUS/@TAIntYrs, 
		@AnnTAOCCSTH = ISNULL(@TAOCCSTH, 0) / @TAIntYrs,
		@AnnTAOCCOVT = ISNULL(@TAOCCOVT, 0) / @TAIntYrs,
		@AnnTAMPSSTH = ISNULL(@TAMPSSTH, 0) / @TAIntYrs,
		@AnnTAMPSOVT = ISNULL(@TAMPSOVT, 0) / @TAIntYrs,
		@AnnTAContOCCWHr = ISNULL(@TAContOCC, 0) / @TAIntYrs,
		@AnnTAContMPSWHr = ISNULL(@TAContMPS, 0) / @TAIntYrs,
		@MechUnavailTA = @TAHrsDown/(@TAIntDays*24)*100
UPDATE MaintTA
SET 	TACostUS = @TACostUS, TAMatlUS = @TAMatlUS,
	TAIntDays = @TAIntDays, TAIntYrs = @TAIntYrs, RestartDate = @RestartDate,
	AnnTACostLocal = @AnnTACostLocal, AnnTAMatlLocal = @AnnTAMatlLocal, AnnTACost = @AnnTACost, AnnTAMatl = @AnnTAMatl, 
	TAMPSOVT = @TAMPSOVT, AnnTAOCCSTH = @AnnTAOCCSTH, AnnTAOCCOVT = @AnnTAOCCOVT, 
	AnnTAMPSSTH = @AnnTAMPSSTH, AnnTAMPSOVT = @AnnTAMPSOVT, 
	AnnTAContOCCWHr = @AnnTAContOCCWHr, AnnTAContMPSWHr = @AnnTAContMPSWHr,
	MechUnavailTA = @MechUnavailTA, TAMatlPcnt = @TAMatlPcnt, TACurrency = @TACurrency
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID

EXEC spTACosts @RefineryID, @Dataset, @UnitID, @TAID




