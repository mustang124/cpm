﻿



CREATE  PROC [dbo].[spAverageCEI](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @CEI real OUTPUT)
AS
SELECT @CEI = CASE WHEN SUM(SCE_Total) > 0 THEN SUM(CEI*SCE_Total*s.NumDays)/SUM(SCE_Total*s.NumDays) ELSE NULL END
FROM CEI2008 a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND a.FactorSet = @FactorSet AND a.CEI > 0

