﻿
CREATE         PROC [dbo].[spAverageMaint](@SubmissionID int) 
AS
SET NOCOUNT ON
EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverageMaint', @MessageText = 'Started'
DECLARE @RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime
SELECT @RefineryID = RefineryID, @DataSet = DataSet, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd
FROM Submissions WHERE SubmissionID = @SubmissionID
DECLARE @Start24Mo smalldatetime, @StartYTD smalldatetime
SELECT @Start24Mo = DATEADD(mm, -24, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1)
IF @RefineryID IN ('106FL','150FL','322EUR') AND DATEPART(yy, @Start24Mo) < 2011 AND DATEPART(yy, @PeriodStart) >= 2011
	SET @Start24Mo = '12/31/2010'

UPDATE MaintIndex
SET RoutIndex_Avg = c.RoutIndex, RoutMatlIndex_Avg = c.RoutMatlIndex
	, MaintIndex_Avg = c.MaintIndex, MaintMatlIndex_Avg = c.MaintMatlIndex
	, RoutEffIndex_Avg = c.RoutEffIndex, MaintEffIndex_Avg = c.MaintEffIndex
FROM MaintIndex LEFT JOIN dbo.CalcMaintIndex(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd, NULL, NULL) c ON c.FactorSet = MaintIndex.FactorSet AND c.Currency = MaintIndex.Currency
WHERE MaintIndex.SubmissionID = @SubmissionID

UPDATE GenSum
SET RoutIndex_Avg = m.RoutIndex_Avg, TAIndex_Avg = m.TAIndex_Avg, MaintIndex_Avg = m.MaintIndex_Avg
	, MEI_Rout_Avg = m.RoutEffIndex_Avg, MEI_TA_Avg = m.TAEffIndex_Avg, MEI_Avg = m.MaintEffIndex_Avg
FROM GenSum LEFT JOIN MaintIndex m ON m.SubmissionID = GenSum.SubmissionID AND m.FactorSet = GenSum.FactorSet AND m.Currency = GenSum.Currency

UPDATE GenSum
SET RoutIndex_YTD = c.RoutIndex, TAIndex_YTD = c.TAIndex, MaintIndex_YTD = c.MaintIndex
	, MEI_Rout_YTD = c.RoutEffIndex, MEI_TA_YTD = c.TAEffIndex, MEI_YTD = c.MaintEffIndex
FROM GenSum LEFT JOIN dbo.CalcMaintIndex(@RefineryID, @DataSet, @StartYTD, @PeriodEnd, NULL, NULL) c ON c.FactorSet = GenSum.FactorSet AND c.Currency = GenSum.Currency
WHERE GenSum.SubmissionID = @SubmissionID

DECLARE @UnitAnnRout TABLE(UnitID int NOT NULL, Currency varchar(4) NOT NULL, RoutCost real NULL, RoutMatl real NULL)
INSERT @UnitAnnRout(UnitID, Currency, RoutCost, RoutMatl)
SELECT m.UnitID, m.Currency, RoutCost = SUM(m.CurrRoutCost)/SUM(s.FractionOfYear), RoutMatl = SUM(m.CurrRoutMatl)/SUM(s.FractionOfYear)
FROM MaintCost m INNER JOIN dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd) s ON s.SubmissionID = m.SubmissionID
GROUP BY m.UnitID, m.Currency

UPDATE @UnitAnnRout SET RoutCost = 0 WHERE RoutCost < 0
UPDATE @UnitAnnRout SET RoutMatl = 0 WHERE RoutMatl < 0

UPDATE MaintCost
SET AnnRoutCost = ISNULL(r.RoutCost, 0), AnnRoutMatl = r.RoutMatl,
	AnnMaintCost = ISNULL(MaintCost.AnnTACost, 0) + ISNULL(r.RoutCost, 0),
	AnnMaintMatl = ISNULL(MaintCost.AnnTAMatl, 0) + ISNULL(r.RoutMatl, 0)
FROM MaintCost INNER JOIN @UnitAnnRout r ON r.UnitID = MaintCost.UnitID AND r.Currency = MaintCost.Currency
WHERE MaintCost.SubmissionID = @SubmissionID

UPDATE Submissions
SET CalcsNeeded = NULL
WHERE SubmissionID = @SubmissionID 
AND CalcsNeeded = 'M'

EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverageMaint', @MessageText = 'Completed'


