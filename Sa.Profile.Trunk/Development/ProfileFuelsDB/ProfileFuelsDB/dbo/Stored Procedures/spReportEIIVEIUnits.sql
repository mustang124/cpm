﻿




CREATE      PROC [dbo].[spReportEIIVEIUnits] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SELECT s.SubmissionID, s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, 
p.Description, p.SortKey, c.UnitName, c.UnitID, c.ProcessID, c.ProcessType, c.UtilCap, fc.StdEnergy, fc.StdGain, 
VEIFormulaForReport, EIIFormulaForReport, DisplayTextUS, fc.UnitEII
INTO #UnitData
FROM Submissions s 
INNER JOIN Config c ON c.SubmissionID = s.SubmissionID
INNER JOIN FactorCalc fc ON c.submissionId = fc.submissionid AND c.unitid = fc.unitid
INNER JOIN Factors f ON f.processid = c.processid AND f.processtype = c.processtype AND f.factorset=fc.factorset AND f.CogenES IN('E','')
INNER JOIN ProcessID_LU p ON c.processid = p.processid
INNER JOIN DisplayUnits_LU d ON p.displayunits = d.displayunits
WHERE f.factorset=@FactorSet AND c.PROCESSID NOT IN ('STEAMGEN','ELECGEN','FCCPOWER','FTCOGEN','BLENDING','TNK+BLND') 
And s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth)
ORDER BY s.PeriodStart DESC, p.SortKey

SELECT s1.SubmissionID, c.UnitID, NumDays_YTD = SUM(s2.NumDays), Throughput_YTD = SUM(c.UtilCap*s2.NumDays), 
EstMBTU_YTD = SUM(fc.StdEnergy*s2.NumDays), StdEnergy_YTD = SUM(fc.StdEnergy*s2.NumDays)/SUM(s2.NumDays),
EstGain_YTD = SUM(fc.StdGain*s2.NumDays), StdGain_YTD = SUM(fc.StdGain*s2.NumDays)/SUM(s2.NumDays),
UnitEII_YTD = SUM(CASE WHEN fc.UnitEII > 0 THEN fc.UnitEII*s2.NumDays*fc.StdEnergy END)/SUM(CASE WHEN fc.UnitEII > 0 THEN s2.NumDays*fc.StdEnergy END)
INTO #YTD
FROM Submissions s1 INNER JOIN Submissions s2 ON s2.RefineryID = s1.RefineryID AND s2.DataSet = s1.DataSet AND s2.PeriodYear = s1.PeriodYear AND s2.PeriodMonth <= s1.PeriodMonth
INNER JOIN Config c ON c.SubmissionID = s2.SubmissionID
INNER JOIN FactorCalc fc ON c.submissionId = fc.submissionid AND c.unitid = fc.unitid
WHERE fc.factorset=@FactorSet AND c.PROCESSID NOT IN ('STEAMGEN','ELECGEN','FCCPOWER','FTCOGEN','BLENDING','TNK+BLND') 
And s1.RefineryID = @RefineryID AND s1.DataSet=@DataSet AND s1.PeriodYear = ISNULL(@PeriodYear, s1.PeriodYear) AND s1.PeriodMonth = ISNULL(@PeriodMonth, s1.PeriodMonth)
GROUP BY s1.SubmissionID, c.UnitID

SET NOCOUNT ON

IF @PeriodMonth IS NULL
	SELECT u.Location, u.PeriodStart, u.PeriodEnd, u.DaysInPeriod, Currency = @Currency, UOM = @UOM, 
	u.ProcessID, u.UnitID, u.UnitName, u.Description, u.SortKey, u.ProcessType, u.UtilCap, u.StdEnergy, u.StdGain, 
	u.VEIFormulaForReport, u.EIIFormulaForReport, u.DisplayTextUS, 
	Throughput = u.UtilCap*u.DaysInPeriod, EstMBTU = u.StdEnergy*u.DaysInPeriod, EstGain = u.StdGain*u.DaysInPeriod,
	ytd.Throughput_YTD, ytd.StdEnergy_YTD, ytd.EstMBTU_YTD, ytd.StdGain_YTD, ytd.EstGain_YTD,
	u.UnitEII, ytd.UnitEII_YTD
	FROM #UnitData u INNER JOIN #YTD ytd ON ytd.SubmissionID = u.SubmissionID AND ytd.UnitID = u.UnitID
	ORDER BY u.PeriodStart DESC, u.SortKey, u.UnitID
ELSE
	SELECT u.Location, u.PeriodStart, u.PeriodEnd, u.DaysInPeriod, Currency = @Currency, UOM = @UOM, 
	u.Description, u.SortKey, u.UnitName, u.UnitID, u.ProcessID, u.ProcessType, u.UtilCap, u.StdEnergy, u.StdGain, 
	u.VEIFormulaForReport, u.EIIFormulaForReport, u.DisplayTextUS, Variables = CAST('' AS varchar(255)), 
	Throughput = u.UtilCap*u.DaysInPeriod, EstMBTU = u.StdEnergy*u.DaysInPeriod, EstGain = u.StdGain*u.DaysInPeriod,
	ytd.Throughput_YTD, ytd.StdEnergy_YTD, ytd.EstMBTU_YTD, ytd.StdGain_YTD, ytd.EstGain_YTD,
	u.UnitEII, ytd.UnitEII_YTD
	FROM #UnitData u INNER JOIN #YTD ytd ON ytd.SubmissionID = u.SubmissionID AND ytd.UnitID = u.UnitID
	ORDER BY u.PeriodStart DESC, u.SortKey, u.UnitID

DROP TABLE #UnitData
DROP TABLE #YTD




