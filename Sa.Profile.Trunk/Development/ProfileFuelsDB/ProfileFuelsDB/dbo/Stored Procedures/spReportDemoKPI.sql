﻿



CREATE   PROC [dbo].[spReportDemoKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2010', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE	
	@EII real, @EII_YTD real, @EII_AVG real, 
	@EnergyUseDay real, @EnergyUseDay_YTD real, @EnergyUseDay_AVG real, 
	@TotStdEnergy real, @TotStdEnergy_YTD real, @TotStdEnergy_AVG real, 

	@RefUtilPcnt real, @RefUtilPcnt_YTD real, @RefUtilPcnt_AVG real, 
	@EDC real, @EDC_YTD real, @EDC_AVG real, 

	@ProcessEffIndex real, @ProcessEffIndex_YTD real, @ProcessEffIndex_AVG real, 

	@VEI real, @VEI_YTD real, @VEI_AVG real, 
	@ReportLossGain real, @ReportLossGain_YTD real, @ReportLossGain_AVG real, 
	@EstGain real, @EstGain_YTD real, @EstGain_AVG real, 
	@NetInputBPD real, @NetInputBPD_YTD real, @NetInputBPD_AVG real,
	
	@OpAvail real, @OpAvail_YTD real, @OpAvail_AVG real, 
	@MechUnavailTA real, @MechUnavailTA_YTD real, @MechUnavailTA_AVG real, 
	@NonTAUnavail real, @NonTAUnavail_YTD real, @NonTAUnavail_AVG real, 

	@PersIndex real, @PersIndex_YTD real, @PersIndex_AVG real, 
	@AnnTAWhr real, @AnnTAWhr_YTD real, @AnnTAWhr_AVG real,
	@NonTAWHr real, @NonTAWHr_YTD real, @NonTAWHr_AVG real,

	@MaintIndex real, @MaintIndex_YTD real, @MaintIndex_AVG real, 
	@TAAdj real, @TAAdj_YTD real, @TAAdj_AVG real,
	@AnnTACost real, @AnnTACost_YTD real, @AnnTACost_AVG real, 
	@RoutCost real, @RoutCost_YTD real, @RoutCost_AVG real, 

	@NEOpexEDC real, @NEOpexEDC_YTD real, @NEOpexEDC_AVG real, 
	@NEOpex real, @NEOpex_YTD real, @NEOpex_AVG real, 

	@OpexUEDC real, @OpexUEDC_YTD real, @OpexUEDC_AVG real,
	@EnergyCost real, @EnergyCost_YTD real, @EnergyCost_AVG real, 
	@TotCashOpex real, @TotCashOpex_YTD real, @TotCashOpex_AVG real,
	@UEDC real, @UEDC_YTD real, @UEDC_AVG real
	
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)

SELECT @EII = NULL, @EII_YTD = NULL, @EII_AVG = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_YTD = NULL, @EnergyUseDay_AVG = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_YTD = NULL, @TotStdEnergy_AVG = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_YTD = NULL, @RefUtilPcnt_AVG = NULL, 
	@EDC = NULL, @EDC_YTD = NULL, @EDC_AVG = NULL, 
	@UEDC = NULL, @UEDC_YTD = NULL, @UEDC_AVG = NULL, 
	@VEI = NULL, @VEI_YTD = NULL, @VEI_AVG = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_YTD = NULL, @ReportLossGain_AVG = NULL, 
	@EstGain = NULL, @EstGain_YTD = NULL, @EstGain_AVG = NULL, 
	@OpAvail = NULL, @OpAvail_YTD = NULL, @OpAvail_AVG = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_YTD = NULL, @MechUnavailTA_AVG = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_YTD = NULL, @NonTAUnavail_AVG = NULL, 
	@RoutCost = NULL, @RoutCost_YTD = NULL, @RoutCost_AVG = NULL, 
	@PersIndex = NULL, @PersIndex_YTD = NULL, @PersIndex_AVG = NULL, 
	@AnnTAWhr = NULL, @AnnTAWhr_YTD = NULL, @AnnTAWhr_AVG = NULL,
	@NonTAWHr = NULL, @NonTAWHr_YTD = NULL, @NonTAWHr_AVG = NULL,
	@NEOpexEDC = NULL, @NEOpexEDC_YTD = NULL, @NEOpexEDC_AVG = NULL, 
	@NEOpex = NULL, @NEOpex_YTD = NULL, @NEOpex_AVG = NULL, 
	@OpexUEDC = NULL, @OpexUEDC_YTD = NULL, @OpexUEDC_AVG = NULL, 
	@TAAdj = NULL, @TAAdj_YTD = NULL, @TAAdj_AVG = NULL,
	@EnergyCost = NULL, @EnergyCost_YTD = NULL, @EnergyCost_AVG = NULL, 
	@TotCashOpex = NULL, @TotCashOpex_YTD = NULL, @TotCashOpex_AVG = NULL

SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)

--- Everything Already Available in Gensum
SELECT	@RefUtilPcnt = UtilPcnt, @RefUtilPcnt_YTD = UtilPcnt_YTD, @RefUtilPcnt_AVG = UtilPcnt_AVG,
	@OpAvail = OpAvail, @OpAvail_YTD = OpAvail_YTD, @OpAvail_AVG = OpAvail_AVG,
	@EII = EII, @EII_YTD = EII_YTD, @EII_AVG = EII_AVG,
	@VEI = VEI, @VEI_YTD = VEI_YTD, @VEI_AVG = VEI_AVG,
	@NetInputBPD = NetInputBPD*1000, @NetInputBPD_YTD = NetInputBPD_YTD*1000, @NetInputBPD_AVG = NetInputBPD_Avg*1000,
	@PersIndex = TotWHrEDC, @PersIndex_YTD = TotWHrEDC_YTD, @PersIndex_AVG = TotWHrEDC_AVG,
	@EDC = EDC/1000, @EDC_YTD = EDC_YTD/1000, @EDC_AVG = EDC_AVG/1000,
	@UEDC = UEDC/1000, @UEDC_YTD = UEDC_YTD/1000, @UEDC_AVG = UEDC_AVG/1000,
	@MaintIndex = RoutIndex + TAIndex_AVG, @MaintIndex_YTD = MaintIndex_YTD, @MaintIndex_AVG = MaintIndex_AVG,
	@NEOpexEDC = NEOpexEDC, @NEOpexEDC_YTD = NEOpexEDC_YTD, @NEOpexEDC_AVG = NEOpexEDC_AVG
FROM Gensum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_YTD = MechUnavailTA_Ann, @MechUnavailTA_AVG = MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @MechUnavailTA_YTD=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd
AND m.FactorSet = @FactorSet

SELECT @MechUnavailTA_AVG=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail = 100 - @OpAvail - @MechUnavailTA, @NonTAUnavail_YTD = 100 - @OpAvail_YTD - @MechUnavailTA_YTD, @NonTAUnavail_AVG = 100 - @OpAvail_AVG - @MechUnavailTA_AVG
IF @NonTAUnavail < 0.05
	SET @NonTAUnavail = 0
IF @NonTAUnavail_YTD < 0.05
	SET @NonTAUnavail_YTD = 0
IF @NonTAUnavail_AVG < 0.05
	SET @NonTAUnavail_AVG = 0
	
SELECT @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays,
	@ProcessEffIndex = f.PEI
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

SELECT @EnergyUseDay_YTD = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_YTD = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_YTD = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_YTD = SUM(EstGain)/SUM(s.NumDays*1.0),
	@ProcessEffIndex_YTD = dbo.AvgProcessEffIndex(@RefineryID, @DataSet, @StartYTD, @PeriodEnd, @FactorSet)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd
AND f.FactorSet = @FactorSet

SELECT @EnergyUseDay_AVG = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_AVG = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_AVG = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_AVG = SUM(EstGain)/SUM(s.NumDays*1.0),
	@ProcessEffIndex_AVG = dbo.AvgProcessEffIndex(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, @FactorSet)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd
AND f.FactorSet = @FactorSet

SELECT @AnnTAWHr = SUM(TotWHr)/1000 FROM Pers WHERE SubmissionID = @SubmissionID AND PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr = TotNonTAWHr/1000 FROM PersTot WHERE SubmissionID = @SubmissionID

SELECT @AnnTAWHr_YTD = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_YTD = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd

SELECT @AnnTAWHr_AVG = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_AVG = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd

SELECT @AnnTACost = AllocAnnTACost, @RoutCost = CurrRoutCost
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency

SELECT @AnnTACost_YTD = SUM(AllocAnnTACost), @RoutCost_YTD = SUM(CurrRoutCost)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

SELECT @AnnTACost_AVG = SUM(AllocAnnTACost), @RoutCost_AVG = SUM(CurrRoutCost)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

SELECT @NEOpex = NEOpex*Divisor, @EnergyCost = EnergyCost*Divisor, @TotCashOpex = TotCashOpex*Divisor, @TAAdj = TAAdj*Divisor
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpexUEDC = TotCashOpex
FROM OpexCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpex_YTD = SUM(NEOpex*Divisor), @EnergyCost_YTD = SUM(EnergyCost*Divisor)
	, @TotCashOpex_YTD = SUM(TotCashOpex*Divisor), @TAAdj_YTD = SUM(TAAdj*Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

SELECT @OpexUEDC_YTD = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'UEDC'

SELECT @NEOpex_AVG = SUM(NEOpex*Divisor), @EnergyCost_AVG = SUM(EnergyCost*Divisor)
	, @TotCashOpex_AVG = SUM(TotCashOpex*Divisor), @TAAdj_AVG = SUM(TAAdj*Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'EDC'

SELECT @OpexUEDC_AVG = SUM(TotCashOpex*Divisor)/SUM(Divisor)
FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
AND o.DataType = 'UEDC'

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_YTD = @EnergyUseDay_YTD * 1.055, @EnergyUseDay_AVG = @EnergyUseDay_AVG * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_YTD = @TotStdEnergy_YTD * 1.055, @TotStdEnergy_AVG = @TotStdEnergy_AVG * 1.055

SELECT 
	@EnergyUseDay = @EnergyUseDay, @EnergyUseDay_YTD = @EnergyUseDay_YTD, @EnergyUseDay_AVG = @EnergyUseDay_AVG, 
	@TotStdEnergy = @TotStdEnergy, @TotStdEnergy_YTD = @TotStdEnergy_YTD, @TotStdEnergy_AVG = @TotStdEnergy_AVG, 
	@EDC_YTD = @EDC_YTD, @UEDC_YTD = @UEDC_YTD
	
SELECT 
	EII = @EII, EII_YTD = @EII_YTD, EII_AVG = @EII_AVG, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_YTD = @EnergyUseDay_YTD, EnergyUseDay_AVG = @EnergyUseDay_AVG, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_YTD = @TotStdEnergy_YTD, TotStdEnergy_AVG = @TotStdEnergy_AVG, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_YTD = @RefUtilPcnt_YTD, UtilPcnt_AVG = @RefUtilPcnt_AVG, 
	EDC = @EDC, EDC_YTD = @EDC_YTD, EDC_AVG = @EDC_AVG, 
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_YTD = @EDC_YTD*@RefUtilPcnt_YTD/100, UtilUEDC_AVG = @EDC_AVG*@RefUtilPcnt_AVG/100, 
	
	ProcessEffIndex = @ProcessEffIndex, ProcessEffIndex_YTD = @ProcessEffIndex_YTD, ProcessEffIndex_AVG = @ProcessEffIndex_AVG, 
	VEI = @VEI, VEI_YTD = @VEI_YTD, VEI_AVG = @VEI_AVG, 
	NetInputBPD = @NetInputBPD, NetInputBPD_YTD = @NetInputBPD_YTD, NetInputBPD_AVG = @NetInputBPD_Avg,
	ReportLossGain = @ReportLossGain, ReportLossGain_YTD = @ReportLossGain_YTD, ReportLossGain_AVG = @ReportLossGain_AVG, 
	EstGain = @EstGain, EstGain_YTD = @EstGain_YTD, EstGain_AVG = @EstGain_AVG, 

	OpAvail = @OpAvail, OpAvail_YTD = @OpAvail_YTD, OpAvail_AVG = @OpAvail_AVG, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_YTD = @MechUnavailTA_YTD, MechUnavailTA_AVG = @MechUnavailTA_AVG, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_YTD = @NonTAUnavail_YTD, NonTAUnavail_AVG = @NonTAUnavail_AVG, 

	TotWHrEDC = @PersIndex, TotWHrEDC_YTD = @PersIndex_YTD, TotWHrEDC_AVG = @PersIndex_AVG, 
	AnnTAWhr = @AnnTAWhr, AnnTAWhr_YTD = @AnnTAWhr_YTD, AnnTAWhr_AVG = @AnnTAWhr_AVG,
	NonTAWHr = @NonTAWHr, NonTAWHr_YTD = @NonTAWHr_YTD, NonTAWHr_AVG = @NonTAWHr_AVG, 

	MaintIndex = @MaintIndex, MaintIndex_YTD = @MaintIndex_YTD, MaintIndex_AVG = @MaintIndex_AVG, 
	TAAdj = @TAAdj, TAAdj_YTD = @TAAdj_YTD, TAAdj_AVG = @TAAdj_AVG,
	RoutCost = @RoutCost, RoutCost_YTD = @RoutCost_YTD, RoutCost_AVG = @RoutCost_AVG, 

	NEOpexEDC = @NEOpexEDC, NEOpexEDC_YTD = @NEOpexEDC_YTD, NEOpexEDC_AVG = @NEOpexEDC_AVG, 
	NEOpex = @NEOpex, NEOpex_YTD = @NEOpex_YTD, NEOpex_AVG = @NEOpex_AVG, 

	TotCashOpexUEDC = @OpexUEDC, TotCashOpexUEDC_YTD = @OpexUEDC_YTD, TotCashOpexUEDC_AVG = @OpexUEDC_AVG, 
	EnergyCost = @EnergyCost, EnergyCost_YTD = @EnergyCost_YTD, EnergyCost_AVG = @EnergyCost_AVG, 
	TotCashOpex = @TotCashOpex, TotCashOpex_YTD = @TotCashOpex_YTD, TotCashOpex_AVG = @TotCashOpex_AVG, 
	UEDC = @UEDC, UEDC_YTD = @UEDC_YTD, UEDC_AVG = @UEDC_AVG






