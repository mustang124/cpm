﻿






CREATE PROC [dbo].[spReportTA] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2010', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SELECT ta.UnitID, UnitName = ISNULL(c.UnitName, pid.Description), c.ProcessID, pid.SortKey, ta.TADate, ta.PrevTADate, ta.TAHrsDown, ta.TAIntDays, ta.TAIntYrs, ta.TAHrsDown/(24.0*ta.TAIntDays)*100 AS MechUnavailTA_Ann
	, ta.TACostLocal, ta.TACurrency, ta.AnnTACostLocal, tac.TACost, tac.AnnTACost, TACompWHr = ISNULL(ta.TAOCCSTH,0) + ISNULL(ta.TAOCCOVT,0) + ISNULL(ta.TAMPSSTH*ISNULL(1+ta.tampsOvtPcnt/100,1),0)
	, TAContWHr = ISNULL(ta.TAContOCC,0)+ISNULL(ta.TAContMPS,0)
	, TAEffort = ISNULL(ta.TAOCCSTH,0) + ISNULL(ta.TAOCCOVT,0) + ISNULL(ta.TAMPSSTH*ISNULL(1+ta.tampsOvtPcnt/100,1),0) + ISNULL(ta.TAContOCC,0)+ISNULL(ta.TAContMPS,0)
	, AnnTAEffort = (ISNULL(ta.TAOCCSTH,0) + ISNULL(ta.TAOCCOVT,0) + ISNULL(ta.TAMPSSTH*ISNULL(1+ta.tampsOvtPcnt/100,1),0) + ISNULL(ta.TAContOCC,0)+ISNULL(ta.TAContMPS,0))/ta.TAIntYrs
	, tac.PeriodStart
FROM dbo.TAForSubmission ta INNER JOIN ProcessID_LU pid ON pid.ProcessID = ta.ProcessID
INNER JOIN dbo.TACostsForSubmission tac ON tac.SubmissionID = ta.SubmissionID AND tac.UnitID = ta.UnitID AND tac.Currency = @Currency
LEFT JOIN Config c ON c.SubmissionID = ta.SubmissionID AND c.UnitID = ta.UnitID AND pid.ProfileProcFacility = 'Y'
WHERE ta.SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @Dataset))
ORDER BY pid.SortKey, ta.UnitID




