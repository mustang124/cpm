﻿


CREATE   PROC [dbo].[spAverageAvail](@SubmissionID int)
AS

SET NOCOUNT ON
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @RefineryID varchar(6), @DataSet varchar(15)
SELECT @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @RefineryID = RefineryID, @DataSet = DataSet
FROM Submissions WHERE SubmissionID = @SubmissionID
DECLARE @Start24Mo smalldatetime, @StartYTD smalldatetime
SELECT 	@Start24Mo = DATEADD(yy, -2, @PeriodEnd),
	@StartYTD  = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1)

UPDATE MaintCalc
SET PeriodHrs_Avg = a.PeriodHrs, PeriodHrsOSTA_Avg = a.PeriodHrsOSTA,
	MechUnavailTA_Act_Avg = a.MechUnavailTA_Act, MechAvailOSTA_Avg = a.MechAvailOSTA, 
	MechAvail_Act_Avg = a.MechAvail_Act, MechAvailSlow_Act_Avg = a.MechAvailSlow_Act, 
	OpAvail_Act_Avg = a.OpAvail_Act, OpAvailSlow_Act_Avg = a.OpAvailSlow_Act, 
	OnStream_Act_Avg = a.OnStream_Act, OnStreamSlow_Act_Avg = a.OnStreamSlow_Act,
	MechAvail_Ann_Avg = a.MechAvail_Ann, MechAvailSlow_Ann_Avg = a.MechAvailSlow_Ann, 
	OpAvail_Ann_Avg = a.OpAvail_Ann, OpAvailSlow_Ann_Avg = a.OpAvailSlow_Ann, 
	OnStream_Ann_Avg = a.OnStream_Ann, OnStreamSlow_Ann_Avg = a.OnStreamSlow_Ann
FROM MaintCalc LEFT JOIN dbo.CalcAverageUnitAvail(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd) a ON a.UnitID = MaintCalc.UnitID
WHERE MaintCalc.SubmissionID = @SubmissionID

UPDATE MaintCalc
SET PeriodHrs_YTD = a.PeriodHrs, PeriodHrsOSTA_YTD = a.PeriodHrsOSTA,
	MechUnavailTA_Act_YTD = a.MechUnavailTA_Act, MechAvailOSTA_YTD = a.MechAvailOSTA, 
	MechAvail_Act_YTD = a.MechAvail_Act, MechAvailSlow_Act_YTD = a.MechAvailSlow_Act, 
	OpAvail_Act_YTD = a.OpAvail_Act, OpAvailSlow_Act_YTD = a.OpAvailSlow_Act, 
	OnStream_Act_YTD = a.OnStream_Act, OnStreamSlow_Act_YTD = a.OnStreamSlow_Act,
	MechAvail_Ann_YTD = a.MechAvail_Ann, MechAvailSlow_Ann_YTD = a.MechAvailSlow_Ann, 
	OpAvail_Ann_YTD = a.OpAvail_Ann, OpAvailSlow_Ann_YTD = a.OpAvailSlow_Ann, 
	OnStream_Ann_YTD = a.OnStream_Ann, OnStreamSlow_Ann_YTD = a.OnStreamSlow_Ann
FROM MaintCalc LEFT JOIN dbo.CalcAverageUnitAvail(@RefineryID, @Dataset, @StartYTD, @PeriodEnd) a ON a.UnitID = MaintCalc.UnitID
WHERE MaintCalc.SubmissionID = @SubmissionID

UPDATE MaintProcess
SET MechAvail_Ann_Avg=m.MechAvail_Ann_Avg, 
MechAvail_Act_Avg=m.MechAvail_Act_Avg, 
MechAvailSlow_Ann_Avg=m.MechAvailSlow_Ann_Avg, 
MechAvailSlow_Act_Avg=m.MechAvailSlow_Act_Avg, 
MechAvailOSTA_Avg=m.MechAvailOSTA_Avg, 
OpAvail_Ann_Avg=m.OpAvail_Ann_Avg, 
OpAvail_Act_Avg=m.OpAvail_Act_Avg, 
OpAvailSlow_Ann_Avg=m.OpAvailSlow_Ann_Avg, 
OpAvailSlow_Act_Avg=m.OpAvailSlow_Act_Avg, 
OnStream_Ann_Avg=m.OnStream_Ann_Avg, 
OnStream_Act_Avg=m.OnStream_Act_Avg, 
OnStreamSlow_Ann_Avg=m.OnStreamSlow_Ann_Avg, 
OnStreamSlow_Act_Avg=m.OnStreamSlow_Act_Avg, 
MechAvail_Ann_YTD=m.MechAvail_Ann_YTD, 
MechAvail_Act_YTD=m.MechAvail_Act_YTD, 
MechAvailSlow_Ann_YTD=m.MechAvailSlow_Ann_YTD, 
MechAvailSlow_Act_YTD=m.MechAvailSlow_Act_YTD, 
MechAvailOSTA_YTD=m.MechAvailOSTA_YTD, 
OpAvail_Ann_YTD=m.OpAvail_Ann_YTD, 
OpAvail_Act_YTD=m.OpAvail_Act_YTD, 
OpAvailSlow_Ann_YTD=m.OpAvailSlow_Ann_YTD, 
OpAvailSlow_Act_YTD=m.OpAvailSlow_Act_YTD, 
OnStream_Ann_YTD=m.OnStream_Ann_YTD, 
OnStream_Act_YTD=m.OnStream_Act_YTD, 
OnStreamSlow_Ann_YTD=m.OnStreamSlow_Ann_YTD, 
OnStreamSlow_Act_YTD=m.OnStreamSlow_Act_YTD, 
MechAvail_Ann_Target=m.MechAvail_Ann_Target, 
MechAvail_Act_Target=m.MechAvail_Act_Target, 
MechAvailSlow_Ann_Target=m.MechAvailSlow_Ann_Target, 
MechAvailSlow_Act_Target=m.MechAvailSlow_Act_Target, 
OpAvail_Ann_Target=m.OpAvail_Ann_Target, 
OpAvail_Act_Target=m.OpAvail_Act_Target, 
OpAvailSlow_Ann_Target=m.OpAvailSlow_Ann_Target, 
OpAvailSlow_Act_Target=m.OpAvailSlow_Act_Target, 
OnStream_Ann_Target=m.OnStream_Ann_Target, 
OnStream_Act_Target=m.OnStream_Act_Target, 
OnStreamSlow_Ann_Target=m.OnStreamSlow_Ann_Target, 
OnStreamSlow_Act_Target=m.OnStreamSlow_Act_Target
FROM MaintProcess INNER JOIN dbo.CalcAverageProcessAvail(@SubmissionID) m ON m.ProcessID = MaintProcess.ProcessID AND m.FactorSet = MaintProcess.FactorSet
WHERE MaintProcess.SubmissionID = @SubmissionID

UPDATE MaintAvailCalc
SET MechAvail_Ann_Avg=a.MechAvail_Ann,MechAvailSlow_Ann_Avg=a.MechAvailSlow_Ann, 
OpAvail_Ann_Avg=a.OpAvail_Ann, OpAvailSlow_Ann_Avg=a.OpAvailSlow_Ann, 
OnStream_Ann_Avg=a.OnStream_Ann, OnStreamSlow_Ann_Avg=a.OnStreamSlow_Ann, 
MechAvail_Act_Avg=a.MechAvail_Act, MechAvailSlow_Act_Avg=a.MechAvailSlow_Act, 
OpAvail_Act_Avg=a.OpAvail_Act, OpAvailSlow_Act_Avg=a.OpAvailSlow_Act, 
OnStream_Act_Avg=a.OnStream_Act, OnStreamSlow_Act_Avg=a.OnStreamSlow_Act, 
MechAvailOSTA_Avg=a.MechAvailOSTA
FROM MaintAvailCalc LEFT JOIN dbo.CalcAverageAvail(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd) a ON a.FactorSet = MaintAvailCalc.FactorSet
WHERE MaintAvailCalc.SubmissionID = @SubmissionID

UPDATE MaintAvailCalc
SET MechAvail_Ann_YTD=y.MechAvail_Ann, MechAvailSlow_Ann_YTD=y.MechAvailSlow_Ann, 
OpAvail_Ann_YTD=y.OpAvail_Ann, OpAvailSlow_Ann_YTD=y.OpAvailSlow_Ann, 
OnStream_Ann_YTD=y.OnStream_Ann, OnStreamSlow_Ann_YTD=y.OnStreamSlow_Ann, 
MechAvail_Act_YTD=y.MechAvail_Act, MechAvailSlow_Act_YTD=y.MechAvailSlow_Act, 
OpAvail_Act_YTD=y.OpAvail_Act, OpAvailSlow_Act_YTD=y.OpAvailSlow_Act, 
OnStream_Act_YTD=y.OnStream_Act, OnStreamSlow_Act_YTD=y.OnStreamSlow_Act, 
MechAvailOSTA_YTD=y.MechAvailOSTA
FROM MaintAvailCalc LEFT JOIN dbo.CalcAverageAvail(@RefineryID, @DataSet, @StartYTD, @PeriodEnd) y ON y.FactorSet = MaintAvailCalc.FactorSet
WHERE MaintAvailCalc.SubmissionID = @SubmissionID

UPDATE Gensum
SET MechAvail = m.MechAvail_Ann, MechAvail_Avg = m.MechAvail_Ann_Avg, MechAvail_YTD = m.MechAvail_Ann_YTD, 
MechAvailSlow = m.MechAvailSlow_Ann, MechAvailSlow_Avg = m.MechAvailSlow_Ann_Avg, MechAvailSlow_YTD = m.MechAvailSlow_Ann_YTD, 
OpAvail = m.OpAvail_Ann, OpAvail_Avg = m.OpAvail_Ann_Avg, OpAvail_YTD = m.OpAvail_Ann_YTD, 
OpAvailSlow = m.OpAvailSlow_Ann, OpAvailSlow_Avg = m.OpAvailSlow_Ann_Avg, OpAvailSlow_YTD = m.OpAvailSlow_Ann_YTD, 
OnStream = m.OnStream_Ann, OnStream_Avg = m.OnStream_Ann_Avg, OnStream_YTD = m.OnStream_Ann_YTD, 
OnStreamSlow = m.OnStreamSlow_Ann, OnStreamSlow_Avg = m.OnStreamSlow_Ann_Avg, OnStreamSlow_YTD = m.OnStreamSlow_Ann_YTD
FROM Gensum LEFT JOIN MaintAvailCalc m ON m.SubmissionID = Gensum.SubmissionID AND m.FactorSet = Gensum.FactorSet
WHERE Gensum.SubmissionID = @SubmissionID




