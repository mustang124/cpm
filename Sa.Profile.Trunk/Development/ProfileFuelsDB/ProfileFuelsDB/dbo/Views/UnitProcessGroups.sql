﻿
CREATE VIEW [dbo].[UnitProcessGroups]
AS
SELECT SubmissionID, FactorSet, UnitID, [OperProc], [IdleProc], [TotProc], [SpecFrac], [OthProc], [AncUnits], [MajConv], [TotUtly], [TotRS], [TotRef]
FROM (
SELECT SubmissionID, FactorSet, UnitID, ProcessGrouping, UnitCount = 1
FROM RefProcessGroupings
WHERE ProcessGrouping IN (SELECT ProcessGrouping FROM ProcessGrouping_LU)
) pg
PIVOT (SUM(UnitCount) FOR ProcessGrouping IN ([OperProc], [IdleProc], [TotProc], [SpecFrac], [OthProc], [AncUnits], [MajConv], [TotUtly], [TotRS], [TotRef])) pvt

