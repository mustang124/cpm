﻿CREATE TABLE [dbo].[DisplayUnits_LU] (
    [DisplayUnits]   [dbo].[UOM]  NOT NULL,
    [DisplayTextUS]  VARCHAR (20) NOT NULL,
    [DisplayTextMet] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DisplayUnits_LU] PRIMARY KEY CLUSTERED ([DisplayUnits] ASC) WITH (FILLFACTOR = 90)
);

