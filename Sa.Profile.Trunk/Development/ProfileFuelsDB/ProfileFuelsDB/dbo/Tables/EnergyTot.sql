﻿CREATE TABLE [dbo].[EnergyTot] (
    [SubmissionID]      INT        NOT NULL,
    [PurFGMBTU]         REAL       NULL,
    [PurLiquidMBTU]     REAL       NULL,
    [PurSolidMBTU]      REAL       NULL,
    [PurSteamMBTU]      REAL       NULL,
    [PurThermMBTU]      FLOAT (53) NULL,
    [PurPowerMBTU]      FLOAT (53) NULL,
    [PurTotMBTU]        REAL       NULL,
    [ProdFGMBTU]        FLOAT (53) NULL,
    [ProdOthMBTU]       FLOAT (53) NULL,
    [ProdPowerAdj]      FLOAT (53) NULL,
    [ProdTotMBTU]       REAL       NULL,
    [TotEnergyConsMBTU] FLOAT (53) NULL,
    [PurPowerConsMWH]   FLOAT (53) NULL,
    [ProdPowerConsMWH]  FLOAT (53) NULL,
    [TotPowerConsMWH]   REAL       NULL,
    CONSTRAINT [PK_EnergyTot] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);

