﻿CREATE TABLE [dbo].[LoadRoutHist] (
    [RefineryID]    CHAR (6)      NOT NULL,
    [DataSet]       VARCHAR (15)  NOT NULL,
    [PeriodStart]   SMALLDATETIME NOT NULL,
    [RoutCostLocal] REAL          NULL,
    [RoutMatlLocal] REAL          NULL,
    CONSTRAINT [PK_LoadRoutHist] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [DataSet] ASC, [PeriodStart] ASC) WITH (FILLFACTOR = 90)
);

