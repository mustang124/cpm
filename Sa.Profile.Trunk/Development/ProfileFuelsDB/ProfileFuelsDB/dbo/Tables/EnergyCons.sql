﻿CREATE TABLE [dbo].[EnergyCons] (
    [SubmissionID]   INT                     NOT NULL,
    [TransType]      [dbo].[EnergyTransType] NOT NULL,
    [EnergyType]     [dbo].[EnergyType]      NOT NULL,
    [MBTU]           FLOAT (53)              NULL,
    [PercentOfTotal] REAL                    NULL,
    [MBTUPerUEDC]    REAL                    NULL,
    [KBTUPerBbl]     REAL                    NULL,
    CONSTRAINT [PK_EnergyCons] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [TransType] ASC, [EnergyType] ASC) WITH (FILLFACTOR = 90)
);

