﻿CREATE DEFAULT [dbo].[defFactorSet]
    AS 'DEFAULT';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[MaintAvailCalc].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[MultLimits].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[MultFactors].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[GenSum].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[MaintIndex].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[FactorSets].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[NewFactors].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[CEI].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[CEI2008].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[ROICalc].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[Divisors].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[RefProcessGroupings].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[FactorProcessCalc].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[MaintProcess].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[Factors].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[FactorTotCalc].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[FactorCalc].[FactorSet]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defFactorSet]', @objname = N'[dbo].[FactorSet]';

