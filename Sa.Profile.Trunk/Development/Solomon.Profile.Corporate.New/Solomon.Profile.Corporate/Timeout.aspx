<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="Timeout.aspx.vb" Inherits="Solomon.Profile.Corporate.TimedOut" %>
<%@ Register TagPrefix="uc1" TagName="LoginControl" Src="LoginControl.ascx" %>

<asp:Content ID="timeout_head" runat="server" ContentPlaceHolderID="PH_Head">
	</asp:Content>

<asp:Content ID="timeout_body" runat="server" ContentPlaceHolderID="PH_Work">
	
	<div class="row">	
		<div class="col-sm-12 col-md-8 col-centered">
			<uc1:LoginControl id="LoginControl2" runat="server"></uc1:LoginControl>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-centered">
			<script type="text/javascript">

				Cookies.set('solomon_test', 'solomon_test', { expires: 1 });
				var cookieVal = Cookies.get('solomon_test');

				if (cookieVal == null) {
					document.writeln("<center><span style=\"COLOR:RED;\">Cookies must be enabled to use this site.</span></center>");
				}
				else {
					Cookies.remove('solomon_test');
				}

			</script>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			$('#summary').html('Session Timeout');
		});
	</script>

</asp:Content>
