Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Init1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Users", "FirstName", Function(c) c.String())
            AddColumn("dbo.Users", "LastName", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Users", "LastName")
            DropColumn("dbo.Users", "FirstName")
        End Sub
    End Class
End Namespace
