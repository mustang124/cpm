<%@ Page Language="VB" AutoEventWireup="false" AspCompat="true" CodeBehind="PopUpWin2.aspx.vb" Inherits="Solomon.Profile.Corporate.PopUpWin2" %>

<!DOCTYPE html>
<html>
<head>
	<title>
		<%= Request.QueryString("node")%>
	</title>
	<meta http-equiv="PRAGMA" content="NO-CACHE" />
	<meta content="JavaScript" name="vs_defaultClientScript" />
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
	<style type="text/css">
		#export_menu {
			right: 20px;
			bottom: 0px;
			position: absolute;
			top: expression(document.body.scrollTop+document.body.clientHeight-this.clientHeight);
		}
		#print-menu-bar {
			background-color: rgb(105, 105, 105);
			padding: 4px;
		}
	</style>	
	<script src="scripts/jquery/jquery-2.2.4.min.js"></script> <!-- (das) Bootstrap 3.3.6 requires previous version of jquery -->
	<script src="scripts/bootstrap/bootstrap-3.3.6.min.js"></script>
	<link href="styles/bootstrap/bootstrap-3.3.6.min.css" rel="stylesheet" />
	<script src="scripts/jquery/browser-fix.min.js"></script> <!-- (das) printElement.js requires previous functionality of jquery that checks browser type -->
	<script src="scripts/jquery/printElement.js"></script>
</head>
<body>

	<form id="frmPopUpWin2" method="post" runat="server">
		
		<input id="ltPage"
			type="hidden"
			value='<%= IIf(Not IsNothing(Request.QueryString("page")), Request.QueryString("page"), 1)%>'
			name="ltPage" />
		
		<input id="ltTotal" type="hidden"
			value='<%= IIf(Not IsNothing(Request.Form("ltTotal")), Request.Form("ltTotal"), 1)%>'
			name="ltTotal" />

		<div id="print-menu-bar">
			<button type="button" id="btnPrintPage" class="btn btn-default">&nbsp;&nbsp;Print&nbsp;&nbsp;</button>
		</div>
			<asp:Panel ID="pnlContent" Style="z-index: 103;"
			runat="server" Font-Names="Tahoma" Font-Size="XX-Small" Width="98%" Height="87.03%" CssClass="NPanel" Wrap="False">
			
				<p>
					<asp:PlaceHolder ID="phContent" runat="server"></asp:PlaceHolder>
				</p>
			</asp:Panel>

		<div id="export_menu" style="z-index: 104; visibility: hidden">
			<% If Request.QueryString("section") = "REPORTS" Then%>
				<asp:ImageButton ID="btnDataDump" Style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
					runat="server" ImageUrl="images/button_email_this.gif"></asp:ImageButton>
			<% End If%>
			<a href="javascript:window.print();">
				<img id="IMG2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
					src="images/button_print_this.gif" runat="server" />
			</a>
		</div>

		<script type="text/javascript">
			$(document).ready(function () {
				$("#btnPrintPage").click(function () {
					$("#pnlContent").printElement();
				});
			});
		</script>

	</form>

</body>
</html>
