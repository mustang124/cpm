<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ChartTree.ascx.vb" Inherits="Solomon.Profile.Corporate.ChartTree" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style>
	.trigger {
		font: 9/10px geneva, arial, verdana, sans-serif;
		cursor: pointer;
	}
	.branch {
		display: none;
		font: 9/10px geneva, arial, verdana, sans-serif;
		margin-left: 2px;
	}
	.branch a:link {
		font: 9/10px geneva, arial, verdana, sans-serif;
		margin-left: 5px;
		color: black;
		text-decoration: none;
		display: inline-block;
		width: 90%;
	}
	.branch a:visited {
		font: 9/10px geneva, arial, verdana, sans-serif;
		margin-left: 5px;
		color: black;
		text-decoration: none;
		display: inline-block;
		width: 90%;
	}
	.branch a:hover {
		font: 9/10px geneva, arial, verdana, sans-serif;
		border-top: white thin solid;
		border-bottom: gray thin solid;
		border-right: gray thin solid;
		margin-left: 5px;
		color: darkcyan;
		text-decoration: none;
		display: inline-block;
		width: 90%;
	}
	.branch a:active {
		width: 92%;
		border-top: white thin solid;
		border-bottom: gray thin solid;
		border-right: gray thin solid; /*border-right: black thin solid;border-top: black thin solid;margin-left: 5px;border-left: black thin solid;border-bottom: black thin solid;*/
		background-color: #cccccc;
	}
</style>

<script type="text/javascript">

	var openImg = new Image();
	openImg.src = "Styles\\TMenu\\minus.gif";
	var closedImg = new Image();
	closedImg.src = "Styles\\TMenu\\plusik.gif";

/*
	function showBranch(branch) {
		var objBranch = $(branch).style;
		if (objBranch.display == "block")
			objBranch.display = "none";
		else
			objBranch.display = "block";

		Cookies.set('openedNodes', branch, { expires: 1 });
	}
*/
/*
	function swapFolder(img) {
		objImg = document.getElementById(img);
		if (objImg.src.indexOf('plusik.gif') > -1)
			objImg.src = openImg.src;
		else
			objImg.src = closedImg.src;
	}
*/
	function setChartValue(node) {
		//document.forms[0]['SelectedNode'].value = node;
		//document.forms[0]['Section'].value = 'CHARTS';
		$('#SelectedNode').value = node;
		$('#Section').value = node;

		Cookies.set('pickednode', node, { expires: 1 });
		Cookies.set('pickedsection', 'CHARTS', { expires: 1 });

		SummaryInfo(node);
		NodePicked('CHARTS');
	}

	function getValue(theVal) {
		return theVal.replace(/[^\d\.]/g, "") * 1;
	}

	function restoreChartState() {
		var branch = Cookies.get('openedNodes');
		var section = Cookies.get('pickedsection');
		var node = Cookies.get('pickednode');

		if (section) {
			if (section == 'CHARTS') {
				if (branch) {
					//showBranch(branch);
					//swapFolder('folder' + getValue(branch));
				}
			}
			NodePicked(section);
		}
		SummaryInfo(node);
	}

	$(function () {
		$('.tree-toggle').click(function () {
			$(this).parent().children('ul.tree').toggle(600);
		});
	});
</script>

<%= BuildChartMenu() %>

<input id="Section" type="hidden" name="Section" />
<input id="SelectedNode" type="hidden" name="SelectedNode" />
