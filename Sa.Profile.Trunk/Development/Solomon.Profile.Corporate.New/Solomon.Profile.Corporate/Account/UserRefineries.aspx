﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="UserRefineries.aspx.vb" Inherits="Solomon.Profile.Corporate.UserRefineries" %>

<asp:Content ID="ManageUsers_head" runat="server" ContentPlaceHolderID="PH_Head">
</asp:Content>

<asp:Content ID="ManageUsers_Sidebar" runat="server" ContentPlaceHolderID="PH_Sidebar">
    <asp:PlaceHolder ID="PlaceHolder7" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="ManageUsers_body" runat="server" ContentPlaceHolderID="PH_Work">

    <div class="row" id="main-work">
        <br />
        <br />
        <br />
        <br />
        <br />

        <div class="well">
            <asp:HiddenField ID="HiddenField1" runat="server" /> 
            <div class="passwordBlock">
                <div class="form-group row">
                    <asp:Label runat="server" ID="SelectedUserLabel" Text="Selected User" CssClass="col-sm-4 col-form-label font-bold margin-top-10" />
                    <div class="col-sm-8">
                        <asp:TextBox ID="SelectedUser" runat="server" Text="Dummy User" Enabled="false" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 margin-top-10">
                        <asp:Label runat="server" ID="Label2" Text="Refinery Access" CssClass="col-form-label font-bold" />
                        <br />
                        <asp:Label runat="server" ID="Label1" Text="To select multiple refineries, use the Shift key 
										and your mouse for contiguous selection of refineries. Use the Ctrl 
										key and your mouse for noncontiguous selection and 
										deselection."
                            CssClass="col-form-label" />
                    </div>
                    <div class="col-sm-8">
                        <asp:ListBox ID="lbRefinery" runat="server" CssClass="form-control" SelectionMode="Multiple" DataValueField="RefineryID" DataTextField="Location" DataMember="TSort" DataSource="<%# DsTSort1%>"></asp:ListBox>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 col-form-label margin-top-10"></div>
                    <div class="col-sm-8">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Update Access" CssClass="btn btn-default profile-button-primary"></asp:Button>
                    </div>
                </div>
            </div>

            <asp:Literal ID="ltUserRefineries" runat="server"></asp:Literal>
        </div>

    </div>

    <script type="text/javascript">
        $(function () {
            $('#summary').html('User Refineries');
            $('.navigationStandard,.navigationHome').show();
            //settings menu highlight active item
            $('.settingsNav li').removeClass('settingsActive');
            $('.settingsNav li:contains("Manage Users")').addClass('settingsActive');
              var hv = $("#"+ '<%= HiddenField1.ClientID %>').val();
            $('#Cmpany').text(hv)
        });
    </script>
</asp:Content>
