﻿<%@ Page Title="Two-Factor Authentication" Language="vb" MasterPageFile="~/Corporate.Master" AutoEventWireup="false" CodeBehind="TwoFactorAuthenticationSignIn.aspx.vb" Inherits="Solomon.Profile.Corporate.TwoFactorAuthenticationSignIn" %>

<asp:Content ID="index_body" runat="server" ContentPlaceHolderID="PH_Work">

    <div class="col-sm-12 col-md-10 col-centered">
        <div class="row">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <asp:PlaceHolder runat="server" ID="PlaceHolder1" Visible="false">
                <p class="text-danger">
                    <asp:Literal runat="server" ID="Literal1" />
                </p>
            </asp:PlaceHolder>
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <h4>Enter verification code that is sent to your email address</h4>
                        <hr />
                        <asp:HiddenField ID="SelectedProvider" runat="server" />
                        <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                            <p class="text-danger">
                                <asp:Literal runat="server" ID="FailureText" />
                            </p>
                        </asp:PlaceHolder>
                        <div class="form-group">
                            <asp:Label Text="Code:" runat="server" AssociatedControlID="Code" CssClass="col-md-2 control-label" />
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Code" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <div class="checkbox twofactorcheckbox">
                                    <asp:CheckBox Text="" ID="RememberBrowser" runat="server" />Remember Browser
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button Text="Submit" ID="CodeSubmit" OnClick="CodeSubmit_Click" CssClass="btn btn-default profile-button-primary" runat="server" />
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>





    <script type="text/javascript">
        $(function () {
            $('#summary').html('Two-Factor Code');
            $('#Code').focus();
        });
    </script>

</asp:Content>
