﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin

Partial Public Class Register
    Inherits Page

    Private stree As Control

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         If Not Session("Company") Is Nothing Then
                    HiddenField1.Value = Session("Company")
        End If
        If Not (User.Identity.IsAuthenticated) Then
            Response.Redirect("~/Account/Login")
        End If

        inputEmail39.Value= Session("Company")
        stree = Page.LoadControl("~/SettingsControl.ascx")
        PlaceHolder4.Controls.Add(stree)

        Dim manager = HttpContext.Current.GetOwinContext()
        Dim roleStore = New RoleStore(Of IdentityRole)(manager.[Get](Of ApplicationDbContext)())
        Dim roleMngr = New RoleManager(Of IdentityRole)(roleStore)
        Dim roles = roleMngr.Roles.Where(Function(u) Not u.Name.Contains("SuperAdmin")).OrderBy(Function(r) r.Name).ToList().[Select](Function(rr) New ListItem With {
                    .Value = rr.Name,
                    .Text = rr.Name
                    }).ToArray()
        UserRole.Items.AddRange(roles)
    End Sub

    Protected Sub CreateUser_Click(sender As Object, e As EventArgs)
        Dim manager = Context.GetOwinContext().GetUserManager(Of ApplicationUserManager)()
        Dim signInManager = Context.GetOwinContext().Get(Of ApplicationSignInManager)()

        Dim inputFirstName As String = Request.Form("inputfirstname")
        Dim inputlastname As String = Request.Form("inputlastname")
        Dim inputUsername As String = Request.Form("inputUsername")
        Dim inputEmail As String = Request.Form("inputEmail")
        Dim inputPassword As String = Request.Form("inputPassword")

        Dim user = New ApplicationUser() With {.UserName = inputUsername, .Email = inputEmail, .FirstName = inputFirstName, .LastName = inputlastname, .Company =  Session("Company") }
        'Below Code triggers two factor Auth
        user.PhoneNumberConfirmed = True
        user.PhoneNumber = "9999999999"
        Dim result = manager.Create(user, inputPassword)

        If result.Succeeded Then
            'Enable Two Factor Auth manually setting the value and adding phone number(triggers two factor process)
            manager.SetTwoFactorEnabled(user.Id, True)
            ' For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            Dim code = manager.GenerateEmailConfirmationToken(user.Id)
            Dim callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request)
            Dim message As String = "Below are your temporary Login Credentials for Profile Corp application.<br /><br /><b> UserName: " & inputUsername & "</b><br /><b> Password: " + inputPassword & "</b><br /> <br /><br /> Please confirm your account by clicking <a href=""" + callbackUrl & """>here</a>. Make sure you change your login password after the first login."   
            manager.SendEmail(user.Id, "Confirm your account", message)

            'Assign Role to user Here   
            manager.AddToRole(user.Id, UserRole.SelectedValue)
            SuccessMessage.Visible = True
            SuccessMessage.InnerText = "User created successfully!"
            'signInManager.SignIn(user, isPersistent:=False, rememberBrowser:=False)
            'IdentityHelper.RedirectToReturnUrl(Request.QueryString("ReturnUrl"), Response)
        Else
            ErrorMessage.Visible = True
            ErrorMessage.InnerText = result.Errors.FirstOrDefault()
        End If
    End Sub

    'Protected Sub ClearForm_Click(sender As Object, e As EventArgs)
    '    inputfirstname.Text = ""
    'End Sub
End Class

