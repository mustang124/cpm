﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Corporate.Master" CodeBehind="ManageUsers.aspx.vb" Inherits="Solomon.Profile.Corporate.ManageUsers" %>

<asp:Content ID="ManageUsers_head" runat="server" ContentPlaceHolderID="PH_Head">
</asp:Content>

<asp:Content ID="ManageUsers_Sidebar" runat="server" ContentPlaceHolderID="PH_Sidebar">
    <asp:PlaceHolder ID="PlaceHolder5" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="ManageUsers_body" runat="server" ContentPlaceHolderID="PH_Work">

    <div class="row" id="main-work">
        <br />
        <br />
        <br />
        <br />
        <br />

        <div class="well">
             <div class="alert alert-success alert-dismissible" role="alert" runat="server" id="SuccessMessage" visible="false">>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="ErrorMessage" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <asp:HiddenField ID="HiddenField1" runat="server" /> 
            <%--  <asp:GridView ID="GridView1" runat="server">
                    <FooterStyle CssClass="GridViewFooterStyle" />
                    <RowStyle CssClass="GridViewRowStyle" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <PagerStyle CssClass="GridViewPagerStyle" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                </asp:GridView>--%>

            <asp:GridView ID="GridView1" CssClass="table table-striped table-bordered table-condensed" DataKeyNames="Id" 
                OnRowCommand="GridView1_RowCommand" runat="server" PageSize="4" >
                <%--AllowPaging="true" OnPageIndexChanging="GridView1_PageIndexChanging"--%>
                <Columns>
                    <asp:BoundField DataField="UserName" HeaderText="User Name" />
                    <asp:BoundField DataField="Email" HeaderText="Email" />
                    <asp:BoundField DataField="Company" HeaderText="Company" />
                    <asp:TemplateField HeaderText="Actions">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="Refineries" CommandName="Refineries" CommandArgument='<%#Eval("Id")%>' CssClass="btn btn-default profile-button-primary" Text="View Refineries" />
                            <asp:LinkButton runat="server" ID="ResetPassword" CommandName="ResetPass" CommandArgument='<%#Eval("Id")%>' CssClass="btn btn-default profile-button-primary" Text="Reset Password" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="DeleteUser" CommandArgument='<%#Eval("Id")%>' ID="DeleteUser" CssClass="btn btn-default" ToolTip="Delete">
                                <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <!-- Bootstrap Modal Dialog -->
            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">
                                        <asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                                </div>
                                <div class="modal-body">
                                    <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" runat="server" onserverclick="Action_ServerClick" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

           
            <%--         <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False"
    DataSourceID="XmlDataSource1"
    CssClass="">
    <Columns>
        <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
        <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
        <asp:BoundField DataField="phone" HeaderText="phone" SortExpression="phone" />
    </Columns>
</asp:GridView>--%>
            <asp:Literal ID="ltManageUsers" runat="server"></asp:Literal>
        </div>

    </div>

    <script type="text/javascript">
        $(function () {
            $(".alert").fadeTo(5000, 500).slideUp(500, function () {
                $(".alert").slideUp(500);
            });

            $('#summary').html('Manage Users');
            $('.navigationStandard,.navigationHome').show();
            //settings menu highlight active item
            $('.settingsNav li').removeClass('settingsActive');
            $('.settingsNav li:contains("Manage Users")').addClass('settingsActive');
            $('.wrap_side').css('height', '385px');
             var hv = $("#"+ '<%= HiddenField1.ClientID %>').val();
            $('#Cmpany').text(hv)
        });
    </script>
</asp:Content>
