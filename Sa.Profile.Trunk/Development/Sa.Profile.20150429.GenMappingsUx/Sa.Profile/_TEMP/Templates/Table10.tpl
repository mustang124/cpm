<!-- config-start -->
FILE=_CONFIG/Process.xml;
FILE=_REF/Process_LU.xml;

<!-- config-end -->
<!-- template-start -->
<table  class='small' width=100% border=0>

  <tr>
    <td width=5></td>
    <td></td>
    <td width=65></td>
    <td width=65></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
    <td width=50></td>
  </tr>

  <tr>
    <td colspan=2></td>
    <td colspan=5 align=center><strong>Non-Turnaround</strong></td>
    
  </tr>

  <tr>
    <td colspan=2></td>
    <td colspan=5 valign=bottom align=center><strong>Maint. Costs</strong></td>
    <td colspan=1 valign=bottom align=center><strong></strong></td>
    <td colspan=3 valign=bottom align=center></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=5 valign=bottom align=center><strong>-- (CurrencyCode/1000) --</strong></td>
	<td colspan=4 align=center><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Hours Down or Lost</strong></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td align=right colspan=2><strong>Expenses</strong></td>
	<td align=right><strong>Capital</strong></td>
	<td align=right><strong>Ovrhd</strong></td>
	<td align=right><strong>Total</strong></td>
	<td colspan=2 valign=bottom align=right><strong>Process</strong></td>
    <td colspan=1 valign=bottom align=right><strong>Maint</strong></td>
    <td colspan=1 valign=bottom align=right><strong>Other</strong></td>

   </tr>
  SECTION(MaintRout,UnitID<>'90051' AND UnitID<>'90052' AND UnitID <>'90053' AND UNitID<>'90054' AND UnitID <> '90055' AND UnitID <> '90056' AND UnitID <> '90057' AND UnitID <> '90058', SortKey)
  	RELATE(ProcessID_LU,MaintRout,ProcessID,ProcessID)
  BEGIN
  HEADER('<tr>
    <td colspan=10 height=30 valign=bottom><strong> @ProcessDesc </strong></td>
  </tr>')
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td align=right colspan=2>Format(RoutExpLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCptlLocal,'#,##0.0') </td>
	<td align=right>Format(RoutOvhdLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCostLocal,'#,##0.0') </td>
    <td align=right colspan=2>Format(RegDown,'#,##0') </td>
    <td align=right>Format(MaintDown,'#,##0') </td>
    <td align=right>Format(OthDown,'#,##0') </td>
    
  </tr>
  END

  <tr>
    <td colspan=10 height=30 valign=bottom><strong>Off-Sites</strong></td>
  </tr> 


SECTION(MaintRout,UnitID='90051' OR UnitID='90052' OR UnitID='90053' OR UNitID='90054',)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td align=right colspan=2>Format(RoutExpLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCptlLocal,'#,##0.0') </td>
	<td align=right>Format(RoutOvhdLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCostLocal,'#,##0.0') </td>
    <td align=right colspan=2>Format(RegDown,'#,##0') </td>
    <td align=right>Format(MaintDown,'#,##0') </td>
    <td align=right>Format(OthDown,'#,##0') </td>
  END

  <tr>
    <td colspan=10 height=30 valign=bottom><strong> Marine </strong></td>
  </tr> 

SECTION(MaintRout,UnitID='90055',)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td align=right colspan=2>Format(RoutExpLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCptlLocal,'#,##0.0') </td>
	<td align=right>Format(RoutOvhdLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCostLocal,'#,##0.0') </td>
    <td align=right colspan=2>Format(RegDown,'#,##0') </td>
    <td align=right>Format(MaintDown,'#,##0') </td>
    <td align=right>Format(OthDown,'#,##0') </td>
  </tr>
  END

  <tr>
    <td colspan=10 height=30 valign=bottom><strong>Utilities</strong></td>
  </tr> 

SECTION(MaintRout,UnitID='90056'OR UnitID='90057' OR UnitID='90058',)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td align=right colspan=2>Format(RoutExpLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCptlLocal,'#,##0.0') </td>
	<td align=right>Format(RoutOvhdLocal,'#,##0.0') </td>
	<td align=right>Format(RoutCostLocal,'#,##0.0') </td>
    <td align=right colspan=2>Format(RegDown,'#,##0') </td>
    <td align=right>Format(MaintDown,'#,##0') </td>
    <td align=right>Format(OthDown,'#,##0') </td>
  </tr>
  END
</table>
<!-- template-end -->
