﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sa.Profile.GenerateMappings.Ux
{
	public partial class GenerateMappings : Form
	{
		public GenerateMappings()
		{
			InitializeComponent();
		}

		private void GenerateMappings_Load(object sender, EventArgs e)
		{
			SetGenMappingsButton();
		}

		private string PathMapping = string.Empty;
		private string PathBridge = string.Empty;
		private string PathOutput = string.Empty;

		private void btnPathMapping_Click(object sender, EventArgs e)
		{
			this.PathMapping = SelectCurrentFile("Mapping Files (.xml)|*.xml");
			this.txtPathMapping.Text = this.PathMapping;
			SetGenMappingsButton();
		}

		private void btnPathBridge_Click(object sender, EventArgs e)
		{
			this.PathBridge = SelectCurrentFile("Bridge Files (.xlsx)|*.xlsx");
			this.txtPathBridge.Text = this.PathBridge;
			SetGenMappingsButton();
		}

		private void btnPathOutput_Click(object sender, EventArgs e)
		{
			this.PathOutput = CreateOutputFile("Bridge Files (.xml)|*.xml");
			this.txtPathOutput.Text = this.PathOutput;
			SetGenMappingsButton();
		}

		private string CreateOutputFile(string Filter)
		{
			using (SaveFileDialog pth = new SaveFileDialog())
			{
				pth.ShowHelp = false;
				pth.Filter = Filter;
				pth.FileName = this.PathMapping.Replace(".xml", " AutoMap.xml");
				pth.ShowDialog();
				return pth.FileName;
			}
		}

		private string SelectCurrentFile(string Filter)
		{
			using (OpenFileDialog ofd = new OpenFileDialog())
			{
				ofd.CheckFileExists = true;
				ofd.CheckPathExists = true;
				ofd.ShowHelp = false;
				ofd.Multiselect = false;
				ofd.Filter = Filter;
				ofd.ShowDialog();
				return ofd.FileName;
			}
		}

		private bool VerifyPaths()
		{
			return System.IO.File.Exists(this.PathMapping) & System.IO.File.Exists(this.PathBridge) & (this.PathOutput != string.Empty);
		}

		private void SetGenMappingsButton()
		{
			btnGenMappings.Enabled = VerifyPaths();
		}

		private void btnGenMappings_Click(object sender, EventArgs e)
		{
			Sa.Extensible.CreateMappings(this.PathMapping, this.PathBridge, this.PathOutput);
			MessageBox.Show("Complete!");
		}
	}
}
