﻿using System;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Xml;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	public partial class Extensible
	{
		public static void CreateMappings(string PathMap, string PathWorkbook, string PathMappingsXml)
		{
			System.Console.WriteLine("Opening Excel:          {0}", DateTime.Now.ToString());
			Excel.Application xla = Common.NewExcelApplication();

			System.Console.WriteLine("Opening Workbook:       {0}", DateTime.Now.ToString());
			Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, PathWorkbook);

			DateTime dt = DateTime.Now;
			Sa.Xsd.MappingsSchema map;

			System.Console.WriteLine("Reading Mapping:        {0}", dt.ToString());
			Extensible.LoadMappings(PathMap, out map);

			System.Console.WriteLine("Importing References:   {0}", dt.ToString());
			Sa.Extensible.ImportReferences(map, wkb);

			System.Console.WriteLine("Import Duration (s):    {0}", (DateTime.Now - dt).TotalSeconds.ToString());
			System.Console.WriteLine("Closing Excel:          {0}", DateTime.Now.ToString());
			Common.CloseExcel(ref xla, ref wkb);

			System.Console.WriteLine("Writing XML File:       {0}" + DateTime.Now.ToString());
			Sa.Extensible.WriteDocument(map, PathMappingsXml);
			map.Clear();
			map.Dispose();

			System.Console.WriteLine("Closing Application:    {0}" , DateTime.Now.ToString());
		}

		static void LoadMappings(string PathMap, out Sa.Xsd.MappingsSchema map)
		{
			map = new Sa.Xsd.MappingsSchema();
			map.EnforceConstraints = true;
			map.ReadXml(PathMap, XmlReadMode.Auto);
		}
			
		static void ImportReferences(Sa.Xsd.MappingsSchema ms, Excel.Workbook wkb)
		{
			//Load.CrudeCharge(dd, wkb);
			//Load.ProductYield(dd, wkb);
			//Load.RawMaterial(dd, wkb);

			Parallel.Invoke
			(
			    () => { Load.InventoryTankage(ms, wkb); },
			    () => { Load.InventoryReceipts(ms, wkb); },
			    () => { Load.Personnel(ms, wkb); },
			    () => { Load.PersonnelAbsences(ms, wkb); },
			    () => { Load.OpEx(ms, wkb); },
			    () => { Load.Energy(ms, wkb); },
			    () => { Load.EnergyElectric(ms, wkb); },
			    () => { Load.ProcessFacilities(ms, wkb); }
			);
		}

		static void WriteDocument(Sa.Xsd.MappingsSchema dd, string fileName)
		{
			Encoding e = new UTF8Encoding();

			using (XmlTextWriter tw = new XmlTextWriter(fileName, e))
			{
				tw.Formatting = Formatting.Indented;

				dd.WriteXml(tw, XmlWriteMode.WriteSchema);

				tw.Flush();
				tw.Close();
			}
		}
	}
}