﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	public partial class Extensible
	{
		internal partial class Load
		{
			internal static void InventoryTankage(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Inventory Tankage:    {0}", DateTime.Now.ToString());

				int row = 8;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Inventory"];
				Excel.Range rng = null;

				int r = row;
				int c = col;
				int colAvgLevel = 4;

				rng = wks.Cells[r, c];

				map.Inventory.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.MappingsSchema.InventoryRow dr = map.Inventory.FindByTankType(Common.ReturnString(rng));

					if (dr != null)
					{
						rng = wks.Cells[r, colAvgLevel];
						dr.AvgLevel = Common.ReturnAddress(rng);
					}
					else
					{
						dr = map.Inventory.NewInventoryRow();

						dr.TankType = Common.ReturnString(rng);

						rng = wks.Cells[r, ++c];
						dr.Description = Common.ReturnString(rng);

						rng = wks.Cells[r, ++c];
						dr.AvgLevel = Common.ReturnAddress(rng);

						map.Inventory.AddInventoryRow(dr);
					}
					c = col;
					rng = wks.Cells[++r, c];
				}
				map.Inventory.AcceptChanges();
				map.Inventory.EndLoadData();
			}

			internal static void InventoryReceipts(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Inventory Receipts:   {0}", DateTime.Now.ToString());

				int[] row = { 15, 23 };

				int colProcessId = 1;
				int colType = 2;
				int colDetail = 3;
				int colAddress = 4;

				Excel.Worksheet wks = wkb.Worksheets["Inventory"];
				Excel.Range rng = null;

				int r;
				
				map.ConfigRS.BeginLoadData();

				foreach (int i in row)
				{
					rng = wks.Cells[i, colProcessId];
					Xsd.MappingsSchema.ConfigRSRow dr = map.ConfigRS.FindByProcessID(Common.ReturnString(rng));

					if (dr != null)
					{
						r = i;

						rng = wks.Cells[r, colType];

						while (Common.RangeHasValue(rng))
						{
							dr[ProcessTypeColumn(Common.ReturnString(rng))] = Common.ReturnAddress(wks.Cells[r, colAddress]);

							r++;
							rng = wks.Cells[r, colType];
						}
					}
					else
					{
						Xsd.MappingsSchema.ConfigRSRow nr = map.ConfigRS.NewConfigRSRow();

						rng = wks.Cells[i - 1, colDetail];
						nr.Description = Common.ReturnString(rng);

						rng = wks.Cells[i, colProcessId];
						nr.ProcessID = Common.ReturnString(rng);

						r = i;

						rng = wks.Cells[r, colType];

						while (Common.RangeHasValue(rng))
						{
							dr[ProcessTypeColumn(Common.ReturnString(rng))] = Common.ReturnAddress(wks.Cells[r, colAddress]);

							r++;
							rng = wks.Cells[r, colType];
						}

						map.ConfigRS.AddConfigRSRow(nr);
					}
				}
				map.ConfigRS.AcceptChanges();
				map.ConfigRS.EndLoadData();
			}

			private static string ProcessTypeColumn(string ProcessTypeId)
			{
				string ColName = string.Empty;

				switch (ProcessTypeId)
				{
					case "RAIL":
						ColName = "RailcarBBL";
						break;
					case "TT":
						ColName = "TankTruckBBL";
						break;
					case "TB":
						ColName = "TankerBerthBBL";
						break;
					case "OMB":
						ColName = "OffshoreBuoyBBL";
						break;
					case "BB":
						ColName = "BargeBerthBBL";
						break;
					case "PL":
						ColName = "PipelineBBL";
						break;
					default:
						break;
				}
				return ColName;
			}
		}
	}
}