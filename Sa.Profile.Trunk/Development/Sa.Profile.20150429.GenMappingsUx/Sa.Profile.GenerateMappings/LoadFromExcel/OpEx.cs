﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	public partial class Extensible
	{
		internal partial class Load
		{
			internal static void OpEx(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  OpEx:                 {0}", DateTime.Now.ToString());

				int[] col = { 1, 1 };

				Excel.Worksheet wks = wkb.Worksheets["Operating Expenses"];
				Excel.Range rng = null;

				int colOffsetDescription = 1;
				int colOffsetData = 2;

				map.OpEx.BeginLoadData();

				foreach (int c in col)
				{
					for (int r = 7; r <= 155; r++)
					{
						rng = wks.Cells[r, c];

						if (Common.RangeHasValue(rng))
						{
							Xsd.MappingsSchema.OpExRow dr = map.OpEx.FindByOpexID(Common.ReturnString(rng));

							if (dr != null)
							{
								rng = wks.Cells[r, c + colOffsetData];
								dr.costsRefinery = Common.ReturnAddress(rng);
							}
							else
							{
								Xsd.MappingsSchema.OpExRow nr = map.OpEx.NewOpExRow();

								nr.OpexID = Common.ReturnString(rng);

								rng = wks.Cells[r, c + colOffsetDescription];
								nr.Description = Common.ReturnString(rng);

								rng = wks.Cells[r, c + colOffsetData];
								nr.costsRefinery = Common.ReturnAddress(rng);

								map.OpEx.AddOpExRow(nr);

								//  <SortKey></SortKey>
								//  <Indent></Indent>
								//  <ParentID></ParentID>
								//  <detailStudy></detailStudy>
								//  <detailProfile></detailProfile>
							}
						}
					}
				}
				map.OpEx.AcceptChanges();
				map.OpEx.EndLoadData();
			}
		}
	}
}