﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	public partial class Extensible
	{
		internal partial class Load
		{
			internal static void CrudeCharge(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Crude Charge Detail"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				map.Crude.BeginLoadData();

				//while (Common.RangeHasValue(rng))
				//{
				//}

				map.Crude.AcceptChanges();
				map.Crude.EndLoadData();
			}
		}
	}
}