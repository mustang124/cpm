﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.ProfileII.Contracts;
using System.Data;
using Solomon.ProfileII.Data;

namespace Solomon.ProfileII.Services
{
    public class ProfileDataManager //: IProfileDataService
    {        
        string _db12Conx = string.Empty;
        string _userHostAddress = string.Empty;

        public ProfileDataManager(string db12Conx, string userHostAddress)
        {
            _db12Conx = db12Conx;
            _userHostAddress = userHostAddress;

        }

        public string CheckService()
        {
            FileTransfer svc = new FileTransfer( _db12Conx);
            return svc.CheckService(_userHostAddress);
        }

        public byte[] DownloadTplFile(string fileinfo, string appVersion, string refNum)
        {
            FileTransfer svc = new FileTransfer(   _db12Conx);
            return svc.DownloadTplFile(fileinfo, appVersion,  _userHostAddress);
        }

        public DataSet GetDataByPeriod(DateTime periodStart, DateTime periodEnd, string refineryID)
        {
            SubmitServices svc = new SubmitServices(   _db12Conx);
            return svc.GetDataByPeriod(periodStart, periodEnd, refineryID);
        }

        public DataSet GetDataDump(string ReportCode, string ds, string scenario, string currency, 
            DateTime startDate, string UOM, int studyYear, bool includeTarget, bool includeYTD, 
            bool includeAVG, string refineryID)
        {
            DataServices svc = new DataServices(   _db12Conx);
            return svc.GetDataDump(ReportCode.Trim(), ds, scenario, currency, startDate,
                UOM, studyYear, includeTarget, includeYTD, includeAVG, refineryID, _userHostAddress);
        }

        public DataSet GetInputData(string refineryID)
        {
            SubmitServices svc = new SubmitServices( _db12Conx);
            return svc.GetInputData(refineryID);
        }

        public DataSet GetLookups(string refineryID, string companyID)
        {
            DataServices svc = new DataServices( _db12Conx);
            return svc.GetLookups(refineryID, companyID, _userHostAddress);
        }

        public DataSet GetReferences(string refineryID)
        {
            DataServices svc = new DataServices( _db12Conx);
            return svc.GetReferences(refineryID, _userHostAddress);
        }

        public bool RefineryIs2012(string RefineryID)
        {
            DataServices svc = new DataServices( _db12Conx);
            return svc.RefineryIs2012(RefineryID);
        }

        public void SubmitRefineryData(DataSet ds, string refineryID)
        {
            SubmitServices svc = new SubmitServices( _db12Conx);
            svc.SubmitRefineryData(ds, refineryID, _userHostAddress);
        }

        public string UploadFile(string fileName, byte[] bytes, string refNum)
        {
            FileTransfer svc = new FileTransfer( _db12Conx);
            return svc.UploadFile(fileName, bytes, refNum, _userHostAddress);
        }

        public void WriteActivityLogExtended(string applicationName, string methodology, string refNum, string CallerIP, string UserID, string ComputerName, string service, string Method, string EntityName, DateTime? PeriodStart, DateTime? PeriodEnd, string status, string version, string errorMessages, string localDomainName, string osVersion, string browserVersion, string officeVersion, string dataImportedFromBridgeFile)
        {
            DataServices svc = new DataServices( _db12Conx);
            //SubmitServices svc = new SubmitServices(_db10Conx, _db12Conx);
            //FileTransfer svc = new FileTransfer(_db10Conx, _db12Conx);
            svc.WriteActivityLogExtended(applicationName, methodology, refNum, CallerIP, UserID, ComputerName, service, Method, EntityName,  PeriodStart,  PeriodEnd, status, version, errorMessages, localDomainName, osVersion, browserVersion, officeVersion, dataImportedFromBridgeFile);

            //return svc.(RefineryID);
        }
    }
}
