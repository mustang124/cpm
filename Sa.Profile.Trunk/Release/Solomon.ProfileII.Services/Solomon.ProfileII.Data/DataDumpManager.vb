Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.IO

Public Class DataDumpManager
    Private db As DBHelper

    Public Sub New(db12Conx As String)
        db = New DBHelper(db12Conx)
    End Sub

    Public Function GetDataDump(ByVal reportCode As String, ByVal reportName As String, ByVal refineryId As String, ByVal ds As String, ByVal scenario As String, ByVal currency As String, ByVal startDate As Date, ByVal UOM As String, ByVal studyYear As Integer, ByVal includeTarget As Boolean, ByVal includeYTD As Boolean, ByVal includeAVG As Boolean) As DataSet

        If (scenario Is Nothing) Or (scenario = String.Empty) Or (scenario = "BASE") Then
            scenario = "CLIENT"
        End If

        Return GetDataSetForDataDump(refineryId, _
                                    reportName, _
                                    Year(startDate), _
                                    Month(startDate), _
                                    ds, _
                                    studyYear, _
                                    scenario, _
                                    currency, _
                                    UOM, _
                                    includeTarget, _
                                    includeYTD, _
                                    includeAVG)

        'Select Case reportCode.ToUpper.Trim
        '    Case "RS"
        '        Return CreateRSDataDump(reportName, refineryId, UOM, currency, studyYear, includeTarget, includeAVG, includeYTD, scenario)
        '    Case "PS"
        '        Return CreatePSDataDump(reportName, refineryId, UOM, currency, includeTarget, includeAVG, includeYTD, startDate, studyYear, ds)
        '    Case "PD"
        '        Return CreatePDDataDump(reportName, refineryId, UOM, currency, includeTarget, includeAVG, includeYTD, startDate, studyYear, ds)
        '    Case "GPV"
        '        Return CreateGPVDataDump(reportName, refineryId, UOM, currency, includeTarget, includeAVG, includeYTD, startDate, studyYear, ds, scenario)
        '    Case "EII"
        '        Return CreateEIIDataDump(reportName, refineryId, UOM, currency, includeTarget, includeAVG, includeYTD, startDate, studyYear, ds)
        '    Case "T4"
        '        Return CreateOpexDataDump(reportName, refineryId, UOM, currency, includeTarget, includeAVG, includeYTD, startDate, studyYear, ds)
        '        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '        ' This section added by GMO 12/6/2007
        '    Case "CDU", "FCC", "HYC", "REF", "PXYL"
        '        Return CreateProcessUnitDataDump(reportName, refineryId, UOM, currency, includeTarget, includeAVG, includeYTD, startDate, studyYear, ds)
        '        ' END GMO 12/6/2007
        '        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '    Case "SENS"
        '        Return CreateSensibleHeatDataDump(reportName, refineryId, UOM, currency, includeTarget, includeAVG, includeYTD, startDate, studyYear, ds, scenario)
        '    Case Else
        '        Return New DataSet
        'End Select

    End Function
    'SP
#Region "Get Process Unit Detail Data Dump "
    Private Function CreatePDDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean, ByVal startDate As Date, ByVal studyYear As Integer, ByVal dSet As String) As DataSet
        Dim dsOut As New DataSet

        Dim procesLevelStmt As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
            " s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "' AS UOM , " + _
            " description, c.unitname, f.processid,c.Cap,c.RptCap, " + _
            " stdenergy, stdgain, EDCNoMult*MultiFactor/1000 as kEDC, UEDCNoMult*MultiFactor/1000 as kUEDC " + _
            " FROM factorcalc f ,processid_lu g ,config c , Submissions s WHERE " + _
            " f.processid=g.processid AND " + _
            " f.factorset='" + studyYear.ToString() + "' AND " + _
            " c.submissionid=f.submissionid AND c.processid=f.processid AND f.unitid=c.unitid AND " + _
            " g.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND " + _
            " c.processid NOT IN ('BLENDING') AND " + _
            " f.submissionid = s.submissionid AND " + _
            " s.DataSet='" + dSet + "'  AND RefineryID='" + RefID + "'" + _
            " ORDER BY s.PeriodStart DESC, c.Sortkey ASC, c.UnitID ASC"



        Dim unitOffsitesStmt As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
            "s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "' AS UOM , " + _
            "EDC/1000 as TotEDC,UEDC/1000 as TotUEDC,TotRSEDC/1000 as kEDC, TotRSUEDC/1000 as kUEDC, SensHeatStdEnergy,AspStdEnergy, " + _
            " OffsitesStdEnergy,TotStdEnergy,EstGain/DateDiff(d,s.PeriodStart,DateAdd(m, 1, s.PeriodStart)) as EstGain " + _
            " FROM FactorTotCalc f , Submissions s WHERE " + _
            " f.factorset='" + studyYear.ToString() + "' AND " + _
            " f.submissionid=s.submissionid AND " + _
            " s.DataSet='" + dSet + "' AND " + _
            " s.RefineryID='" + RefID + "' ORDER BY s.PeriodStart DESC"
        db.RefineryID = RefID
        Dim dt As DataTable = db.QueryDb(procesLevelStmt).Tables(0)
        dt.TableName = "Process Units"
        dsOut.Tables.Add(dt.Copy())

        dt = db.QueryDb(unitOffsitesStmt).Tables(0)
        dt.TableName = "Units and Offsites"
        dsOut.Tables.Add(dt.Copy())

        Return dsOut
    End Function
#End Region
    'SP
#Region "Get EII Data Dump"
    Private Function CreateEIIDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean, ByVal startDate As Date, ByVal studyYear As Integer, ByVal dSet As String) As DataSet
        Dim refInfQry, funcQry As String
        Dim dsOut As New DataSet
        Dim dsRef As DataSet
        Dim f As Integer
        Dim currency As String
        Dim params As SqlParameterCollection

        If Not UOM.StartsWith("US") Then
            currency = CurrencyCode
        End If
        params.Add(New SqlParameter("@UOM", UOM))
        params.Add(New SqlParameter("@CurrencyCode", CurrencyCode))
        params.Add(New SqlParameter("@DataSetID", dSet))
        params.Add(New SqlParameter("@RefNum", RefID))
        params.Add(New SqlParameter("@StudyYear", studyYear.ToString()))

        refInfQry = "DS_EIIDump_RefInf"
        'refInfQry = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
        '             "s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "' AS UOM , " + _
        '            "EII,EnergyUseDay AS EIIDailyUsage,TotStdEnergy AS EIIStdEnergy," + _
        '            "VEI,EstGain AS VEIStdGain,ReportLossGain AS VEIActualGain," + _
        '            "SensHeatUtilCap AS SensGrossInput, SensHeatStdEnergy ," + _
        '            "'44-(0.23*CrudeGravity)' As SensHeatFormula,(44-(SensHeatStdEnergy/SensHeatUtilCap))/.23 AS CrudeGravity " + _
        '            "FROM FactorTotCalc f,Submissions s  WHERE factorSet='" + studyYear.ToString + "'" + _
        '            " AND  f.SubmissionID=s.SubmissionID AND f.SubmissionID IN " + _
        '            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet.ToString + _
        '            "' AND  RefineryID='" + RefID + "'" + _
        '            " ) ORDER BY PeriodStart DESC"
        db.RefineryID = RefID
        dsRef = db.StoredProcedure(refInfQry, params)
        dsOut.DataSetName = "EII"

        dsRef.Tables(0).TableName = "Summary"
        dsOut.Tables.Add(dsRef.Tables(0).Copy)
        dsOut.Tables(0).Columns.Remove("SensGrossInput")
        dsOut.Tables(0).Columns.Remove("SensHeatStdEnergy")
        dsOut.Tables(0).Columns.Remove("SensHeatFormula")
        dsOut.Tables(0).Columns.Remove("CrudeGravity")

        dsRef.Tables(0).TableName = "Sensible_Heat"
        dsOut.Tables.Add(dsRef.Tables(0).Copy)
        dsOut.Tables(1).Columns.Remove("EII")
        dsOut.Tables(1).Columns.Remove("EIIStdEnergy")
        dsOut.Tables(1).Columns.Remove("EIIDailyUsage")
        dsOut.Tables(1).Columns.Remove("VEI")
        dsOut.Tables(1).Columns.Remove("VEIStdGain")
        dsOut.Tables(1).Columns.Remove("VEIActualGain")


        funcQry = "DS_EIIDump_funcQry"
        'funcQry = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
        '          "s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "' as UOM , " + _
        '          " p.Description,c.SortKey,c.UnitName, c.unitid, c.ProcessID," + _
        '          " c.processType, c.utilcap, fc.stdenergy, fc.stdgain, " + _
        '          "VEIFormulaForReport, EIIFormulaForReport, displaytextUS" + _
        '          " FROM Config c, factorcalc fc, factors f, processid_lu p, displayunits_lu d , Submissions s " + _
        '          " WHERE c.unitid = fc.unitid AND c.submissionId = fc.submissionid   " + _
        '          " AND f.factorset=fc.factorset AND f.processid = c.processid AND f.processtype = c.processtype AND f.factorset='" + studyYear.ToString + "'" + _
        '          "AND c.processid = p.processid AND c.PROCESSID NOT IN ('STEAMGEN','ELECGEN','FCCPOWER','FTCOGEN','BLENDING','TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') and p.displayunits = d.displayunits" + _
        '          " AND s.SubmissionID=c.SubmissionID AND c.submissionid IN " + _
        '          "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
        '          "' AND  RefineryID='" + RefID + "'" + _
        '          " ) ORDER BY PeriodStart DESC, c.SortKey"

        dsRef = db.StoredProcedure(funcQry, params)


        For f = 0 To dsRef.Tables(0).Rows.Count - 1
            Dim tableName As String = dsRef.Tables(0).Rows(f)("ProcessID").Trim.ToString + "_" + dsRef.Tables(0).Rows(f)("UnitName").Trim.ToString
            tableName = Regex.Replace(tableName, "\W", "_")

            'Truncate tablename to 30 characters
            If tableName.Length > 30 Then
                tableName = tableName.Substring(0, 29)
            End If

            If Not dsOut.Tables.Contains(tableName) Then
                dsOut.Tables.Add(tableName)

                dsOut.Tables(tableName).Columns.Add("Location")
                dsOut.Tables(tableName).Columns.Add("PeriodStart")
                dsOut.Tables(tableName).Columns.Add("PeriodEnd")
                dsOut.Tables(tableName).Columns.Add("DaysInPeriod")
                dsOut.Tables(tableName).Columns.Add("Currency")
                dsOut.Tables(tableName).Columns.Add("UOM")
                dsOut.Tables(tableName).Columns.Add("ProcessID")
                dsOut.Tables(tableName).Columns.Add("UnitID")
                dsOut.Tables(tableName).Columns.Add("UnitName")
                dsOut.Tables(tableName).Columns.Add("Description")
                dsOut.Tables(tableName).Columns.Add("SortKey")
                dsOut.Tables(tableName).Columns.Add("ProcessType")
                dsOut.Tables(tableName).Columns.Add("UtilCap")
                dsOut.Tables(tableName).Columns.Add("StdEnergy")
                dsOut.Tables(tableName).Columns.Add("StdGain")
                dsOut.Tables(tableName).Columns.Add("VEIFormulaForReport")
                dsOut.Tables(tableName).Columns.Add("EIIFormulaForReport")
                dsOut.Tables(tableName).Columns.Add("DisplayTextUS")

            End If


            dsOut.Tables(tableName).ImportRow(dsRef.Tables(0).Rows(f))
        Next

        refInfQry = "DS_EIIDump_RefInfAsphalt"
        'refInfQry = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
        '            "s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "'  AS UOM , " + _
        '            " ASPUtilCap, ASPStdEnergy, 115 as ASPEIIFormula " + _
        '            " FROM FactorTotCalc f , Submissions s WHERE f.factorSet='" + studyYear.ToString + "'" + _
        '            " AND s.SubmissionID=f.SubmissionID AND f.SubmissionID IN " + _
        '            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
        '            "' AND  RefineryID='" + RefID + "'" + _
        '            " )  Order By PeriodStart DESC"
        dsRef = db.StoredProcedure(refInfQry, params)
        'dsRef = db.QueryDb(refInfQry)
        dsRef.Tables(0).TableName = "Asphalt"
        dsOut.Tables.Add(dsRef.Tables(0).Copy)

        refInfQry = "DS_EIIDump_RefInfOffsite"
        'refInfQry = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
        '            "s.NumDays as DaysInPeriod,'" + CurrencyCode + "' AS Currency,'" + UOM + "' as UOM , " + _
        '            " OffsitesUtilCap,OffsitesStdEnergy,'40+4*(A)' As OffsitesFormula, Complexity " + _
        '            " FROM FactorTotCalc f , Submissions s WHERE f.factorSet='" + studyYear.ToString + "'" + _
        '            " AND s.SubmissionID=f.SubmissionID AND f.SubmissionID IN " + _
        '            "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
        '            "' AND  RefineryID='" + RefID + "'" + _
        '            " ) Order By PeriodStart DESC"
        dsRef = db.StoredProcedure(refInfQry, params)
        'dsRef = db.QueryDb(refInfQry)
        dsRef.Tables(0).TableName = "Off-sites"
        dsOut.Tables.Add(dsRef.Tables(0).Copy)

        Return dsOut
    End Function
#End Region
    'BEING REWORKED
#Region "Get Gross Product Value Data Dump"
    Private Function CreateGPVDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean, ByVal startDate As Date, ByVal studyYear As Integer, ByVal dSet As String, ByVal scenario As String) As DataSet
        Dim priceCol As String = IIf(CurrencyCode.StartsWith("US"), "PriceUS", "PriceLocal")
        Dim columns() As String = {"MaterialName", "BBL", priceCol, "GPV"}
        Dim decPlaces() As Integer = {0, 0, 2, 1}
        Dim yldQry, rmiQry, othrmQry, yldLubeQry, catQry As String
        Dim dsYield As DataSet
        Dim dsYieldLube As DataSet
        Dim dsCategories As DataSet
        Dim dsRawMaterials As DataSet
        Dim dsOthRM As DataSet
        ' Dim m, t As Integer
        Dim currency As String

        If Not UOM.StartsWith("US") Then
            currency = CurrencyCode
        End If
        db.RefineryID = RefID
        yldQry = "SELECT s.Location, " + _
                " s.PeriodStart, " + _
                " s.PeriodEnd,s.NumDays as DaysInPeriod, " + _
                " '" + CurrencyCode + "' as Currency,'" + UOM + "' as UOM,y.MaterialID," + _
                " SAIName as MaterialName, SUM(ISNULL(BBL,0)) AS BBL, CASE WHEN SUM(ISNULL(BBL,0))=0 THEN 0 ELSE CAST( SUM(ISNULL(BBL,0)*ISNULL(PriceLocal,0))/SUM(BBL) AS REAL) END AS PriceLocal, " + _
                 "CASE WHEN SUM(ISNULL(BBL,0))=0 THEN 0 ELSE CAST( SUM(ISNULL(BBL,0)*ISNULL(PriceUS,0))/SUM(BBL) AS REAL) END AS PriceUS, SUM(ISNULL(BBL,0))*SUM(ISNULL(" + priceCol + ",0))/1000 As GPV, Min(y.Sortkey) AS SortKey" + _
                " FROM Yield y " + _
                " INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID " + _
                " INNER JOIN Submissions s  ON s.SubmissionID=y.SubmissionID " + _
                " WHERE  Category in ( 'Prod','RPF') And LubesOnly = 0 And y.SubmissionID IN " + _
                "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
                "' AND RefineryID='" + RefID + "'" + _
                " ) GROUP BY s.PeriodStart,s.PeriodEnd,s.Location,y.MaterialID,UOM,s.NumDays,SAIName  ORDER BY s.PeriodStart DESC,SortKey ASC "
        db.RefineryID = RefID
        dsYield = db.QueryDb(yldQry)
        dsYield.Tables(0).TableName = "Primary_Products_Refinery"


        yldLubeQry = "SELECT s.Location,s.PeriodStart, s.PeriodEnd, " + _
        "s.NumDays as DaysInPeriod, '" + CurrencyCode + "' as Currency,'" + UOM + "' as UOM,y.MaterialID, " + _
        "SAIName as MaterialName, ISNULL(SUM(BBL),0) AS BBL,  " + _
        " CASE WHEN  SUM(y.BBL)=0 THEN 0  " + _
        " ELSE  " + _
        " ISNULL(SUM(BBL * PriceLocal) / SUM(BBL), 0)" + _
        " END AS PriceLocal, " + _
        " CASE WHEN  SUM(y.BBL)=0 THEN 0  " + _
        " ELSE  " + _
        " ISNULL(SUM(BBL * PriceUS) / SUM(BBL), 0)" + _
        " END AS PriceUS,  " + _
        " ISNULL(SUM(BBL),0)*ISNULL(SUM(" + priceCol + " ),0)/1000 As GPV, " + _
         "MIN(m.SortKey) AS SortKey  " + _
        " FROM Yield y " + _
        " INNER JOIN Submissions s ON  y.SubmissionID=s.SubmissionID AND s.DataSet='" + dSet + "' AND  s.RefineryID='" + RefID + "'" + _
        " INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID  " + _
        " INNER JOIN TSort t ON t.RefineryID ='" + RefID + "' " + _
        "WHERE(AllowInPROD = 1 And LubesOnly = 1 And FuelsLubesCombo = 1) " + _
        " GROUP BY s.PeriodStart,s.PeriodEnd,s.Location,s.NumDays,y.MaterialID, SAIName ORDER BY s.PeriodStart DESC,SortKey ASC "

        dsYieldLube = db.QueryDb(yldLubeQry)
        dsYieldLube.Tables(0).TableName = "Primary_Products_Lubes"
        If dsYieldLube.Tables(0).Rows.Count > 0 Then
            dsYield.Tables.Add(dsYieldLube.Tables(0).Copy)
        End If


        catQry = "SELECT s.Location, " + _
                " s.PeriodStart, " + _
                " s.PeriodEnd,s.NumDays as DaysInPeriod, " + _
                " '" + CurrencyCode + "' as Currency,'" + UOM + "' as UOM," + _
                "(CASE Category " + _
                " WHEN 'MPROD' THEN 'Miscellaneous' " + _
                " WHEN 'ASP' THEN 'ASPHALT' " + _
                " WHEN 'SOLV' THEN 'Speciality Solvents' " + _
                " WHEN 'FCHEM' THEN 'Ref. Feedstocks To Chemical Plant' " + _
                " WHEN 'FLUBE' THEN 'Ref. Feedstocks To Lube Refining' " + _
                " WHEN 'COKE' THEN 'Saleable Petroleum Coke (FOE)' " + _
                " WHEN 'OTHRM' THEN 'Other Raw Materials' " + _
                "  END) AS MaterialName," + _
                " ISNULL(NetBBL,0) as BBL, ISNULL(NetValueMUS,0)*1000000/ISNULL(NetBBL,1)*dbo.ExchangeRate('USD','" + CurrencyCode + "','" + startDate.ToString + "') as PriceLocal, " + _
                " ISNULL(NetValueMUS,0)*1000000/ISNULL(NetBBL,1) as PriceUS, ISNULL(NetValueMUS,0)*1000 As GPVUS  ,ISNULL(NetValueMUS,0)*1000*dbo.ExchangeRate('USD','" + CurrencyCode + "','" + startDate.ToString + "') As GPVLocal " + _
                " FROM materialstcalc y,Submissions s  " + _
                "WHERE y.Scenario='" + scenario + "' AND s.SubmissionID=y.SubmissionID AND NetBBL > 0 AND Category in ('ASP', 'COKE', 'FCHEM', 'FLUBE', 'MPROD', 'SOLV') AND y.SubmissionID IN " + _
                "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
                "' AND RefineryID='" + RefID + "'" + _
                " )  ORDER BY s.PeriodStart DESC"


        dsCategories = db.QueryDb(catQry)
        dsYield.DataSetName = "Gross_Product_Value"
        dsYield.Tables.Add(dsCategories.Tables(0).Copy)
        dsYield.Tables(dsYield.Tables.Count - 1).TableName = "Secondary_Products"


        othrmQry = "SELECT s.Location, " + _
                " s.PeriodStart, " + _
                " s.PeriodEnd,s.NumDays as DaysInPeriod, " + _
                " '" + CurrencyCode + "' as Currency,'" + UOM + "' as UOM," + _
                "(CASE Category " + _
                " WHEN 'MPROD' THEN 'Miscellaneous' " + _
                " WHEN 'ASP' THEN 'ASPHALT' " + _
                " WHEN 'SOLV' THEN 'Speciality Solvents' " + _
                " WHEN 'FCHEM' THEN 'Ref. Feedstocks To Chemical Plant' " + _
                " WHEN 'FLUBE' THEN 'Ref. Feedstocks To Lube Refining' " + _
                " WHEN 'COKE' THEN 'Saleable Petroleum Coke (FOE)' " + _
                " WHEN 'OTHRM' THEN 'Other Raw Materials' " + _
                "  END) AS MaterialName," + _
                " ISNULL(NetBBL,0) as BBL, CAST( ISNULL(NetValueMUS,0)*1000000/ISNULL(NetBBL,1)*dbo.ExchangeRate('USD','" + CurrencyCode + "','" + startDate.ToString + "') AS REAL) as PriceLocal, " + _
                " CAST( ISNULL(NetValueMUS,0)*1000000/ISNULL(NetBBL,1) AS REAL) as PriceUS,CAST( ISNULL(NetValueMUS,0)*1000*dbo.ExchangeRate('USD','" + CurrencyCode + "','" + startDate.ToString + "') AS REAL) As GPV " + _
                " FROM materialstcalc y,Submissions s  " + _
                "WHERE y.Scenario='" + scenario + "' AND s.SubmissionID=y.SubmissionID  AND NetBBL > 0 AND Category in ('OTHRM') AND y.SubmissionID IN " + _
                "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
                "' AND RefineryID='" + RefID + "'" + _
                " )  ORDER BY s.PeriodStart DESC"
        dsOthRM = db.QueryDb(othrmQry)
        'dsOthRM.Tables(0).TableName = "Raw_Material_Details"

        rmiQry = "SELECT s.Location, " + _
                      " s.PeriodStart, " + _
                      " s.PeriodEnd,s.NumDays as DaysInPeriod, " + _
                      " '" + CurrencyCode + "' as Currency,'" + UOM + "' as UOM,y.MaterialID," + _
                      " SAIName as MaterialName, SUM(ISNULL(BBL,0)) AS BBL, CASE WHEN SUM(ISNULL(BBL,0))=0 THEN 0 ELSE CAST( SUM(ISNULL(BBL,0)*ISNULL(PriceLocal,0))/SUM(BBL) AS REAL) END AS PriceLocal, " + _
                       "CASE WHEN SUM(ISNULL(BBL,0))=0 THEN 0 ELSE CAST( SUM(ISNULL(BBL,0)*ISNULL(PriceUS,0))/SUM(BBL) AS REAL) END AS PriceUS,CAST( SUM(ISNULL(BBL,0))*SUM(ISNULL(" + priceCol + ",0))/1000  AS REAL) As GPV, Min(y.Sortkey) AS SortKey" + _
                      " FROM Yield y " + _
                      " INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID " + _
                      " INNER JOIN Submissions s  ON s.SubmissionID=y.SubmissionID " + _
                      " WHERE  Category in ('RMI') And LubesOnly = 0 And y.SubmissionID IN " + _
                      "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
                      "' AND RefineryID='" + RefID + "'" + _
                      " ) GROUP BY s.PeriodStart,s.PeriodEnd,s.Location,y.MaterialID,UOM,s.NumDays,SAIName  ORDER BY s.PeriodStart DESC,SortKey ASC "

        dsRawMaterials = db.QueryDb(rmiQry)
        'dsRawMaterials.Tables(0).TableName = "Raw_Material_Details"
        dsRawMaterials.Merge(dsOthRM)
        'Dim rows() As DataRow = dsRawMaterials.Tables(0).Select("1=1", "PeriodStart DESC")
        dsRawMaterials.Tables(0).DefaultView.Sort = "PeriodStart DESC"
        'Dim rows As DataRowView() = New DataRowView(dsRawMaterials.Tables(0).Rows.Count - 1) {}

        Dim tmpTable As New DataTable
        'Dim cols() As DataColumn
        'dsRawMaterials.Tables(0).Columns.CopyTo(cols, 0)
        'tmpTable.Columns.AddRange(cols)

        'dsRawMaterials.Tables(0).DefaultView.CopyTo(rows, 0)
        'dsRawMaterials.Tables(0).Clear()
        'dsRawMaterials.Merge(rows)
        'tmpTable.BeginLoadData()
        'tmpTable.LoadDataRow(rows, True)
        'tmpTable.EndLoadData()

        dsYield.Tables.Add(dsRawMaterials.Tables(0).Copy)
        dsYield.Tables(dsYield.Tables.Count - 1).TableName = "Raw_Material_Details"




        If CurrencyCode.StartsWith("US") Then
            dsYield.Tables("Primary_Products_Refinery").Columns.Remove("PriceLocal")
            If dsYield.Tables.Contains("Primary_Products_Lubes") Then
                dsYield.Tables("Primary_Products_Lubes").Columns.Remove("PriceLocal")
            End If
            dsYield.Tables("Secondary_Products").Columns.Remove("PriceLocal")
            dsYield.Tables("Secondary_Products").Columns.Remove("GPVLocal")
        Else
            dsYield.Tables("Primary_Products_Refinery").Columns.Remove("PriceUS")
            If dsYield.Tables.Contains("Primary_Products_Lubes") Then
                dsYield.Tables("Primary_Products_Lubes").Columns.Remove("PriceUS")
            End If
            dsYield.Tables("Secondary_Products").Columns.Remove("PriceUS")
            dsYield.Tables("Secondary_Products").Columns.Remove("GPVUS")

        End If


        Return dsYield
    End Function
#End Region
    'SP
#Region "Get Process Unit Scorecard Data Dump"
    Private Function CreatePSDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean, ByVal startDate As Date, ByVal studyYear As Integer, ByVal dSet As String) As DataSet
        Dim capColName As String = IIf(UOM.StartsWith("US"), "Cap", "RptCap")
        Dim processRows() As String = {"Unit Name", "Process ID", "Process Type", "Capacity,b/d", "EDC", "UEDC", "Mechanical Availability,%", "Operational Availability,%", "On-Strean Factor,%", "Process Utilization,%", "Non-Turnaround Expenses, CurrencyCode/bbl", "Turnaround Expenses, CurrencyCode/bbl"}
        Dim columns() As String = {"UnitName", "ProcessID", "ProcessType", capColName, "EDC", "UEDC", "MechAvail", "OpAvail", "OnStream", "", "RoutCost", "TACost"}
        Dim decPlaces() As Integer = {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2}
        Dim m, j As Integer

        Dim dsPLU As New DataSet
        Dim dsOut As New DataSet
        Dim pluQry, tabName As String
        db.RefineryID = RefID
        Dim dsUnits As DataSet = db.QueryDb("Select Distinct UnitID, SortKey from config ORDER BY SortKey")
        dsOut.DataSetName = ReportName.Replace(" ", "")

        pluQry = "SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod, c.UnitID, RTRIM(p.Description) As Description, RTRIM(c.UnitName) As UnitName," + _
                "RTRIM(c.ProcessID) As ProcessID, RTRIM(c.ProcessType) As ProcessType, " + capColName + ", d.DisplayTextUS, d.DisplayTextMet," + _
                "fc.EDCNoMult * fpc.MultiFactor as EDC, fc.UEDCNoMult * fpc.MultiFactor as UEDC, " + _
                "MechAvail_Ann as MechAvail, MechAvail as MechAvail_Target, " + _
                "OpAvail_Ann as OpAvail, OpAvail as OpAvail_Target," + _
                "OnStream_Ann as OnStream, OnStream as OnStream_Target," + _
                "ISNULL(AnnTACost/" + capColName + ",0) as TACost, TACost as TACost_Target, " + _
                "ISNULL(AnnRoutCost/" + capColName + ",0) as RoutCost, RoutCost as RoutCost_Target, u.CurrencyCode " + _
                "FROM Config c " + _
                "INNER JOIN ProcessID_LU p on c.ProcessID = p.ProcessID " + _
                "INNER JOIN FactorCalc fc on fc.SubmissionID = c.SubmissionID and c.UnitID = fc.UnitID " + _
                "INNER JOIN FactorProcessCalc fpc on fc.SubmissionID = fpc.SubmissionID and fpc.ProcessID = c.ProcessID " + _
                "INNER JOIN MaintCalc m on m.SubmissionID = c.SubmissionID and c.UnitID = m.UnitID " + _
                "INNER JOIN MaintCost mc on mc.SubmissionID = c.SubmissionID and c.UnitID = mc.UnitID " + _
                "INNER JOIN DisplayUnits_LU d on p.DisplayUnits = d.DisplayUnits " + _
                "INNER JOIN Submissions s on s.SubmissionID = c.SubmissionID " + _
                "LEFT JOIN UnitTargets u on c.SubmissionID = u.SubmissionID and c.UnitID = u.UnitID " + _
                "WHERE  fc.FactorSet=fpc.Factorset AND fc.FactorSet='" + studyYear.ToString + "' AND  mc.Currency ='" + CurrencyCode + "' " + _
                " AND c.CAP > 0 AND p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') " + _
                " AND  c.SubmissionID  IN  " + _
                    "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + _
                    "'  AND RefineryID='" + RefID + "'" + _
                    " ) ORDER BY s.PeriodStart DESC"

        dsPLU = db.QueryDb(pluQry)


        For m = 0 To dsUnits.Tables(0).Rows.Count - 1
            Dim dsTemp As DataSet = Nothing
            Dim pluRows() As DataRow = dsPLU.Tables(0).Select("UnitID='" + dsUnits.Tables(0).Rows(m)("UnitID").ToString + "'")
            If pluRows.Length > 0 Then
                dsTemp = db.QueryDb("SELECT ' ' as Location,Cast('1/1/1900' as datetime) As PeriodStart, " + _
                                "Cast('1/1/1900' as datetime) as PeriodEnd, 0 as DaysInPeriod, " + _
                                "' ' as Currency,' ' as UOM, ' ' As UnitName, ' ' As ProcessID, " + _
                                "' ' As ProcessType, 0 as " + capColName + ",'' as DisplayTextUS,'' as DisplayTextMet," + _
                                "0.0 as EDC, 0.0  as UEDC, " + _
                                "0.0 as MechAvail, 0.0 as MechAvail_Target, " + _
                                "0.0 as OpAvail, 0.0 as OpAvail_Target," + _
                                "0.0 as OnStream, 0.0 as OnStream_Target," + _
                                "0.0  as TACost, 0.0 as TACost_Target, " + _
                                "0.0 as RoutCost, 0.0 as RoutCost_Target")
            End If
            tabName = ""
            For j = 0 To pluRows.Length - 1
                tabName = pluRows(j)("ProcessID").Trim + "_" + pluRows(j)("UnitName").Trim
                tabName = Regex.Replace(tabName, "\W", "_")
                If tabName.Length > 30 Then
                    tabName = tabName.Substring(0, 29)
                End If

                If Not dsTemp.Tables.Contains(tabName) Then
                    dsTemp.Tables(0).Clear()
                    dsTemp.Tables(0).TableName = tabName
                End If

                Dim newrow As DataRow = dsTemp.Tables(tabName).NewRow

                newrow("Location") = pluRows(j)("Location")
                newrow("DaysInPeriod") = pluRows(j)("DaysInPeriod")
                newrow("UnitName") = pluRows(j)("UnitName")
                newrow("ProcessID") = pluRows(j)("ProcessID")
                newrow("ProcessType") = pluRows(j)("ProcessType")
                newrow("PeriodStart") = pluRows(j)("PeriodStart")
                newrow("PeriodEnd") = pluRows(j)("PeriodEnd")
                newrow(capColName) = pluRows(j)(capColName)
                newrow("DisplayTextUS") = pluRows(j)("DisplayTextUS")
                newrow("DisplayTextMet") = pluRows(j)("DisplayTextMet")
                newrow("UOM") = UOM
                newrow("Currency") = CurrencyCode
                newrow("EDC") = pluRows(j)("EDC")
                newrow("UEDC") = pluRows(j)("UEDC")
                newrow("MechAvail") = pluRows(j)("MechAvail")
                newrow("MechAvail_Target") = pluRows(j)("MechAvail_Target")
                newrow("OpAvail") = pluRows(j)("OpAvail")
                newrow("OpAvail_Target") = pluRows(j)("OpAvail_Target")
                newrow("OnStream") = pluRows(j)("OnStream")
                newrow("OnStream_Target") = pluRows(j)("OnStream_Target")
                newrow("TACost") = pluRows(j)("TACost")
                newrow("TACost_Target") = pluRows(j)("TACost_Target")
                newrow("RoutCost") = pluRows(j)("RoutCost")
                newrow("RoutCost_Target") = pluRows(j)("RoutCost_Target")
                dsTemp.Tables(tabName).Rows.Add(newrow)
            Next
            If pluRows.Length > 0 And Not dsOut.Tables.Contains(tabName) Then
                dsOut.Tables.Add(dsTemp.Tables(tabName).Copy)
            End If
        Next

        'Utilities and Offsites

        Dim offsitesQry As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,s.RptCurrency as Currency,s.UOM," + _
                                    "c.UnitID,c.UnitName, c.ProcessID, c.ProcessType, ISNULL(c." + capColName + ",0) AS " + capColName + ", ISNULL(c.UtilPcnt,0) AS UtilPcnt, " + _
                                    "ISNULL(EDCNoMult,0) AS EDC, ISNULL(UEDCNoMult,0) AS UEDC, d.DisplayTextUS, d.DisplayTextMET " + _
                                    "FROM config c,factorcalc fc, processid_lu p ,displayunits_lu d,Submissions s " + _
                                    " WHERE c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits AND " + _
                                    "c.unitid = fc.unitid AND c.submissionid = fc.submissionid AND " + _
                                    "c.ProcessID IN ('STEAMGEN', 'ELECGEN', 'FCCPOWER') " + _
                                    " AND fc.factorset=" + studyYear.ToString + " AND s.SubmissionID=c.SubmissionID AND c.SubmissionId IN " + _
                                    "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + "' AND " + _
                                    " RefineryID='" + RefID + "'" + _
                                    " ) ORDER BY PeriodStart DESC"


        Dim offsitesRSQry As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,s.RptCurrency as Currency,s.UOM," + _
                                    "c.UnitID, c.ProcessID, c.ProcessType, ISNULL(c.Throughput,0) As BDP, ISNULL(fc.EDCNoMult,0) AS EDC, " + _
                                    " ISNULL(fc.UEDCNoMult,0) AS UEDC " + _
                                    " FROM ConfigRS c, FactorCalc fc,displayunits_lu d,processid_lu p,Submissions s   " + _
                                    " WHERE c.unitid = fc.unitid and c.submissionid = fc.submissionid  AND " + _
                                    "c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits  " + _
                                    " AND p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') " + _
                                    " AND fc.factorset=" + studyYear.ToString + " AND s.SubmissionID=c.SubmissionID AND c.SubmissionId IN " + _
                                    "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + dSet + "' " + _
                                    " AND RefineryID='" + RefID + "'" + _
                                    " ) ORDER BY PeriodStart DESC"

        dsPLU = db.QueryDb(offsitesQry)
        For m = 0 To dsUnits.Tables(0).Rows.Count - 1
            Dim dsTemp As DataSet = Nothing
            Dim pluRows() As DataRow = dsPLU.Tables(0).Select("UnitID='" + dsUnits.Tables(0).Rows(m)("UnitID").ToString + "'")
            If pluRows.Length > 0 Then
                dsTemp = db.QueryDb("SELECT ' ' as Location,Cast('1/1/1900' as datetime) As PeriodStart, " + _
                                "Cast('1/1/1900' as datetime) as PeriodEnd, 0 as DaysInPeriod, " + _
                                "' ' as Currency,' ' as UOM, '' As UnitName, ' ' As ProcessID, " + _
                                "' ' As ProcessType, 0 as " + capColName + "," + _
                                "'' as DisplayTextUS, '' as DisplayTextMet,0.0 as EDC, 0.0  as UEDC")
            End If
            tabName = ""
            For j = 0 To pluRows.Length - 1
                tabName = pluRows(j)("ProcessID").Trim + "_" + pluRows(j)("UnitName").Trim

                tabName = Regex.Replace(tabName, "\W", "_")
                If tabName.Length > 30 Then
                    tabName = tabName.Substring(0, 29)
                End If

                If Not dsTemp.Tables.Contains(tabName) Then
                    dsTemp.Tables(0).Clear()
                    dsTemp.Tables(0).TableName = tabName
                End If

                Dim newrow As DataRow = dsTemp.Tables(tabName).NewRow

                newrow("Location") = pluRows(j)("Location")
                newrow("DaysInPeriod") = pluRows(j)("DaysInPeriod")
                newrow("UnitName") = pluRows(j)("UnitName")
                newrow("ProcessID") = pluRows(j)("ProcessID")
                newrow("ProcessType") = pluRows(j)("ProcessType")
                newrow("PeriodStart") = pluRows(j)("PeriodStart")
                newrow("PeriodEnd") = pluRows(j)("PeriodEnd")
                newrow(capColName) = pluRows(j)(capColName)
                newrow("DisplayTextUS") = pluRows(j)("DisplayTextUS")
                newrow("DisplayTextMet") = pluRows(j)("DisplayTextMet")
                newrow("UOM") = UOM
                newrow("Currency") = CurrencyCode
                newrow("EDC") = pluRows(j)("EDC")
                newrow("UEDC") = pluRows(j)("UEDC")
                dsTemp.Tables(tabName).Rows.Add(newrow)
            Next
            If pluRows.Length > 0 And Not dsOut.Tables.Contains(tabName) Then
                dsOut.Tables.Add(dsTemp.Tables(tabName).Copy)
            End If
        Next

        dsPLU = db.QueryDb(offsitesRSQry)
        dsPLU.Tables(0).TableName = "Utilities_Offsites"
        dsOut.Tables.Add(dsPLU.Tables(0).Copy)

        Return dsOut
    End Function
#End Region

#Region "Get Refinery Scorecard Data Dump"
    Private Function CreateRSDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal studyYear As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean, ByVal scenario As String) As DataSet
        Dim StartSQL As String = "SELECT Location, PeriodStart, PeriodEnd, DaysInPeriod, Currency, UOM, EDC, UEDC"
        Dim EndSQL As String = " FROM Tsort t, Gensum g, MaintAvailCalc m WHERE t.RefineryID = '" & RefID & _
                              "' AND t.RefineryID = g.RefineryID AND g.SubmissionID = m.SubmissionID AND " & _
                              "m.FactorSet=g.FactorSet AND g.FactorSet='" + studyYear + _
                              "' AND g.UOM = '" & UOM & "' AND g.Currency = '" & CurrencyCode & _
                              "' AND g.Scenario='" & scenario & "' ORDER BY PeriodStart DESC"
        db.RefineryID = RefID
        Dim dsChart_LU As DataSet = db.QueryDb("Select * from Chart_LU order by SortKey")

        Dim ds As New DataSet
        ds.DataSetName = ReportName.Replace(" ", "")

        Dim MiddleSQL As String
        Dim TabName As String
        Dim dvChart_LU As New DataView(dsChart_LU.Tables(0)) 'pull in chart lu order by sort key

        'Average Data
        dvChart_LU.RowFilter = "(SortKey >= 100) AND (SortKey < 200)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim AvgSQL As String = StartSQL & MiddleSQL & EndSQL
        Dim dsTemp As DataSet = db.QueryDb(AvgSQL)
        Dim AVGdt As DataTable = dsTemp.Tables(0).Copy
        AVGdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(AVGdt)


        'Performance Indicators
        dvChart_LU.RowFilter = "(SortKey >= 200) AND (SortKey < 300)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim PISQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = db.QueryDb(PISQL)
        Dim PIdt As DataTable = dsTemp.Tables(0).Copy()
        PIdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(PIdt)


        'Energy
        dvChart_LU.RowFilter = "(SortKey >= 300) AND (SortKey < 400)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim ESQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = db.QueryDb(ESQL)
        Dim Edt As DataTable = dsTemp.Tables(0).Copy()
        Edt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Edt)

        'Maintenance
        dvChart_LU.RowFilter = "(SortKey >= 400) AND (SortKey < 500)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim MSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = db.QueryDb(MSQL)
        Dim Mdt As DataTable = dsTemp.Tables(0).Copy()
        Mdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Mdt)

        'Operating Expenses
        dvChart_LU.RowFilter = "(SortKey >= 500) AND (SortKey < 600)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim OSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = db.QueryDb(OSQL)
        Dim Odt As DataTable = dsTemp.Tables(0).Copy()
        Odt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Odt)

        'Personnel
        dvChart_LU.RowFilter = "(SortKey >= 600) AND (SortKey < 700)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim PSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = db.QueryDb(PSQL)
        Dim Pdt As DataTable = dsTemp.Tables(0).Copy()
        Pdt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Pdt)

        'Yields and Margins
        dvChart_LU.RowFilter = "(SortKey >= 700) AND (SortKey < 800)"
        TabName = dvChart_LU.Item(0)!SectionHeader
        MiddleSQL = CreateMiddleSQL(dvChart_LU, includeTarget, includeYTD, includeAvg)

        Dim YSQL As String = StartSQL & MiddleSQL & EndSQL
        dsTemp = db.QueryDb(YSQL)
        Dim Ydt As DataTable = dsTemp.Tables(0).Copy()
        Ydt.TableName = TabName.Replace(" ", "_")
        'FILL TABLE & ADD TO DATASET
        ds.Tables.Add(Ydt)

        Dim userDefinedSQL As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd," + _
            "s.NumDays as DaysInPeriod," + _
            "u.* FROM UserDefined u, Submissions s WHERE u.SubmissionID" + _
            "= s.SubmissionID  and  s.RefineryID ='" + RefID + "'"
        dsTemp = db.QueryDb(userDefinedSQL)
        Dim dtUserDefined As DataTable = dsTemp.Tables(0).Copy()
        dtUserDefined.TableName = "User_Defined"
        ds.Tables.Add(dtUserDefined)


        Return ds
    End Function

    Private Function CreateMiddleSQL(ByVal dv As DataView, ByVal includeTarget As Boolean, ByVal includeYTD As Boolean, ByVal includeAvg As Boolean) As String
        Dim tmpstr As String = ""
        Dim dbTable As String = ""
        Dim tmpField As String = ""
        Dim tmpCol As String = ""
        Dim tmpAlias As String = ""
        Dim i As Integer
        For i = 0 To dv.Count - 1
            dbTable = dv.Item(i)!DataTable
            If dbTable = "MaintAvailCalc" Then tmpAlias = "m." Else tmpAlias = "g."
            'tmpCol = dv.Item(i)!TargetField
            tmpCol = dv.Item(i)!TotField
            ' Select Case tmpCol
            'Case "EDC", "UEDC"
            ' Case Else
            tmpField = tmpAlias & tmpCol
            'tmpField = tmpField.Replace("_Target", "")
            tmpstr = tmpstr & ", " & tmpField

            'If includeTarget Then tmpstr = tmpstr & ", " & tmpField & "_Target"
            'If includeYTD Then tmpstr = tmpstr & ", " & tmpField & "_YTD"
            'If includeAvg Then tmpstr = tmpstr & ", " & tmpField & "_Avg"

            If includeTarget And dv.Item(i)!TargetField.ToString().Length > 0 Then tmpstr = tmpstr & ", " & tmpAlias & dv.Item(i)!TargetField
            If includeYTD And dv.Item(i)!YTDField.ToString().Length > 0 Then tmpstr = tmpstr & ", " & tmpAlias & dv.Item(i)!YTDField
            If includeAvg And dv.Item(i)!AvgField.ToString().Length > 0 Then tmpstr = tmpstr & ", " & tmpAlias & dv.Item(i)!AvgField
            'End Select
        Next
        Return tmpstr
    End Function
#End Region

#Region "Get Operating Expenses Data Dump"
    Function CreateOpexDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean, ByVal startDate As Date, ByVal studyYear As Integer, ByVal dSet As String) As DataSet

        Dim worksheets() As String = {CurrencyCode + "_per_1000", "100" + CurrencyCode + "_per_bbl", "100" + CurrencyCode + "_per_UEDC"}
        Dim datatypes() As String = {"ADJ", "C/BBL", "UEDC"}
        Dim d As Integer
        Dim dsResults As DataSet = New DataSet
        dsResults.DataSetName = "Operating_Expenses"

        For d = 0 To 2

            Dim opexQry As String = "SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,o.Currency," + _
                                "OCCSal As 'OCCSalary', MPSSal as 'MPSSalary',OCCBen as 'OCCBenefits', MPSBen as 'MPSBenefits' ," + _
                                "MaintMatl , ContMaintLabor, OthCont," + _
                                "TAAdj, Envir, OthNonVol," + _
                                "GAPers, Chemicals,Catalysts,PurOth,OthVol, " + _
                                  "(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS 'PurchasedEnergy', " + _
                                 "(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS 'ProducedEnergy' " + _
                                  "FROM Opex o, Submissions s WHERE o.DataType='@DataType' AND o.Currency='@Currency' AND " + _
                                  " s.SubmissionID=o.SubmissionID AND o.SubmissionID IN " + _
                                  "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='@Dataset'  " + _
                                  "AND RefineryID='@RefineryID') ORDER BY s.PeriodStart DESC"

            If (d > 0) Then
                opexQry = "SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,s.RptCurrency AS Currency," + _
                          "OCCSal As 'OCCSalary', MPSSal as 'MPSSalary',OCCBen as 'OCCBenefits', MPSBen as 'MPSBenefits' ," + _
                          "MaintMatl , ContMaintLabor, OthCont," + _
                          "TAAdj, Envir, OthNonVol," + _
                          "GAPers, Chemicals,Catalysts,PurOth,OthVol, " + _
                     "(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS 'PurchasedEnergy', " + _
                     "(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS 'ProducedEnergy' " + _
                      "FROM OpexCalc o , Submissions s WHERE DataType IN('@DataType') AND FactorSet=@FactorSet AND Currency='@Currency' AND s.SubmissionID=o.SubmissionID AND s.SubmissionID IN " + _
                      "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='@Dataset' " + _
                      "AND RefineryID='@RefineryID') ORDER BY s.PeriodStart DESC"
            End If

            opexQry = opexQry.Replace("@Currency", CurrencyCode)
            opexQry = opexQry.Replace("@Dataset", dSet)
            opexQry = opexQry.Replace("@RefineryID", RefID)
            opexQry = opexQry.Replace("@FactorSet", studyYear)
            opexQry = opexQry.Replace("@DataType", datatypes(d))

            db.RefineryID = RefID
            Dim ds As DataSet = db.QueryDb(opexQry)
            ds.Tables(0).TableName = worksheets(d)
            ' ds.DataSetName = ReportName.Replace(" ", "")
            dsResults.Tables.Add(ds.Tables(0).Copy())
        Next



        Return dsResults
    End Function

#End Region

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' This section added by GMO 12/4/2007
    ' Change the columns by SAC 3/4/2008
#Region "Get Custom Process Unit Data Dump"
    Private Function CreateProcessUnitDataDump _
    ( _
    ByVal tReport As String, _
    ByVal tRefineryID As String, _
    ByVal tUOM As String, _
    ByVal tCurrency As String, _
    ByVal includeTarget As Boolean, _
    ByVal includeAvg As Boolean, _
    ByVal includeYTD As Boolean, _
    ByVal dStartDate As Date, _
    ByVal tFactorSet As Integer, _
    ByVal tDataSet As String _
    ) As DataSet

        Dim ds As New DataSet
        Dim dsOut As New DataSet

        Dim tQuery As String
        Dim tReportCode As String
        Dim tSubmissionID As String
        Dim tLocalField As String

        If tUOM.StartsWith("US") Then
            tLocalField = "curd.USDescription,curd.USValue,curd.USTarget "
        Else
            tLocalField = "curd.MetDescription,curd.MetValue,curd.MetTarget "
        End If

        ' Get report code from database
        tQuery = "SELECT ReportCode FROM Report_LU WHERE upper(rtrim(ReportName)) = '" & UCase(Trim(tReport)) & "'"
        db.RefineryID = tRefineryID
        Dim dtReportName As DataTable = db.QueryDb(tQuery).Tables(0)
        tReportCode = dtReportName.Rows(0).Item("ReportCode")

        ' Get submission ID from database
        tQuery = "SELECT SubmissionID FROM Submissions " _
        & "WHERE DataSet = '" + tDataSet & "' AND " _
        & "(" _
        & "Month(PeriodStart) = " & Month(dStartDate) & " AND Year(PeriodStart) = " & Year(dStartDate) _
        & ") " _
        & "AND RefineryID = '" + tRefineryID + "'"

        Dim dtSubmissionID As DataTable = db.QueryDb(tQuery).Tables(0)
        tSubmissionID = dtSubmissionID.Rows(0).Item("SubmissionID")

        ' Construct query with parameters
        tQuery = "SELECT " _
        & "s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod, " _
        & "curd.ProcessID,curd.UnitID,curd.Unitname,curd.Property," _
        & "curd.FactorSet, curd.Currency, " _
        & tLocalField _
        & "FROM " _
        & "CustomUnitReportData curd, Submissions s  " _
        & "WHERE " _
        & "curd.SubmissionID = " & tSubmissionID & " AND " _
        & "rtrim(curd.ProcessID) = '" & Trim(tReportCode) & "' AND " _
        & "(FactorSet = '" & tFactorSet & "' or FactorSet = 'N/A') AND " _
        & "(Currency = '" & tCurrency & "' or Currency = 'N/A') AND " _
        & "curd.SubmissionID = s.SubmissionID AND " _
        & "s.DataSet='" & tDataSet & "' AND " _
        & "s.RefineryID='" & tRefineryID & "' " _
        & "ORDER BY UnitID, SortKey"

        ' Get dataset
        dsOut = db.QueryDb(tQuery)
        dsOut.Tables(0).TableName = "CustomProcessUnitData"
        dsOut.DataSetName = Trim(tReportCode) & "Report"

        Return dsOut

    End Function
#End Region

#Region "Get Sensible Heat Data Dump"
    Private Function CreateSensibleHeatDataDump(ByVal ReportName As String, ByVal RefID As String, ByVal UOM As String, ByVal CurrencyCode As String, ByVal includeTarget As Boolean, ByVal includeAvg As Boolean, ByVal includeYTD As Boolean, ByVal startDate As Date, ByVal studyYear As Integer, ByVal dSet As String, ByVal scenario As String) As DataSet
        Dim ds As DataSet
        db.RefineryID = RefID
        Dim dt As DataTable = db.QueryDb("EXEC spReportSensibleHeat @RefineryID ='" + RefID + _
                                        "', @PeriodYear =NULL" + _
                                        ", @PeriodMonth =NULL" + _
                                        ", @FactorSet ='" + studyYear.ToString() + "',@TotBbl = NULL, @BPD =NULL").Tables(0)

        ' htmlText += "Number of Columns: " & dt.Columns(1).ColumnName
        dt.TableName = "SensibleHeat"
        ds = New DataSet("Sensible_Heat_Material")
        ds.Merge(dt)
        Return ds
    End Function
#End Region


    Private Function GetDataSetForDataDump(ByVal refineryID As String, _
                                            ByVal reportName As String, _
                                            ByVal reportYear As Integer, _
                                            ByVal reportMonth As Integer, _
                                            ByVal dataSet As String, _
                                            ByVal factorSet As String, _
                                            ByVal scenario As String, _
                                            ByVal currency As String, _
                                            ByVal UOM As String, _
                                            ByVal includeTarget As Boolean, _
                                            ByVal includeYTD As Boolean, _
                                            ByVal includeAvg As Boolean _
                                            ) As DataSet
        Dim ds As New DataSet
        db.RefineryID = refineryID
        Dim dtStProc As DataTable = db.QueryDb("exec spGetDataDumpProcs '" + refineryID + "','" + reportName + "'").Tables(0)
        Dim a As Integer
        Dim execString As String = ""
        Dim tablenames(dtStProc.Rows.Count) As String
        Dim procedureName As String
        If dtStProc.Rows.Count > 0 Then
            For a = 0 To dtStProc.Rows.Count - 1
                procedureName = dtStProc.Rows(a)("ProcName")
                tablenames(a) = dtStProc.Rows(a)("DataDumpTableName")

                execString += "exec " & procedureName & " '" & _
                                               refineryID + "'," & _
                                               "null," & _
                                               "null,'" & _
                                               dataSet & "','" & _
                                               factorSet & "','" & _
                                               scenario & "','" & _
                                               currency & "','" & _
                                               UOM & "'," & _
                                               Convert.ToByte(includeTarget) & "," & _
                                               Convert.ToByte(includeYTD) & "," & _
                                               Convert.ToByte(includeAvg) & ";"
            Next

            ds = db.QueryDb(execString)
            For a = 0 To ds.Tables.Count - 1
                If Not tablenames(a).StartsWith("[") Then
                    ds.Tables(a).TableName = tablenames(a)
                ElseIf tablenames(a) = "[ByUnit]" Then

                    Dim row As DataRow
                    For Each row In ds.Tables(a).Rows
                        Dim unitName As String = row("UnitName").Trim()

                        unitName = unitName.Replace("/", "_")
                        unitName = unitName.Replace("#", "No. ")
                        If Not ds.Tables.Contains(unitName) Then
                            ds.Tables.Add(unitName.Trim)
                            Dim col As DataColumn
                            For Each col In ds.Tables(a).Columns
                                ds.Tables(unitName).Columns.Add(col.ColumnName, col.DataType)
                            Next
                        End If
                        Dim dt As DataTable = ds.Tables(unitName)
                        dt.ImportRow(row)
                    Next

                ElseIf tablenames(a).Trim() = "[ByOffsite]" Then
                    Dim dt As DataTable
                    Dim row As DataRow
                    Dim col As DataColumn
                    For Each row In ds.Tables(a).Rows
                        Dim processType As String = row("ProcessType").Trim()
                        Dim processID As String = row("ProcessID").Trim()
                        If Not ds.Tables.Contains(processType & "_" & processID) Then
                            ds.Tables.Add(processType & "_" & processID)

                            For Each col In ds.Tables(a).Columns
                                ds.Tables(processType & "_" & processID).Columns.Add(col.ColumnName, col.DataType)
                            Next
                        End If
                        dt = ds.Tables(processType & "_" & processID)
                        dt.ImportRow(row)
                    Next

                ElseIf tablenames(a) = "[ByOpexDataType]" Then
                    Dim row As DataRow
                    Dim dt As DataTable

                    For Each row In ds.Tables(a).Rows
                        Dim dataType As String = row("DataType").Trim()
                        dataType = dataType.Replace("/", " _")
                        dataType = dataType.Replace("-", " _")
                        dataType = dataType.Replace("#", "No. ")
                        'If tablename not found build table 
                        If Not ds.Tables.Contains(dataType) Then

                            ds.Tables.Add(dataType)

                            Dim col As DataColumn
                            For Each col In ds.Tables(a).Columns
                                ds.Tables(dataType).Columns.Add(col.ColumnName, col.DataType)
                            Next
                        End If
                        dt = ds.Tables(dataType)
                        dt.ImportRow(row)
                    Next

                Else
                    ds.Tables(a).TableName = tablenames(a)
                End If

            Next


            Dim table As DataTable
            Dim dset As DataSet = ds.Clone
            For Each table In dset.Tables
                If table.TableName.StartsWith("Table") Then
                    ds.Tables.Remove(table.TableName)
                End If
            Next


        End If


        ds.DataSetName = reportName.Replace(" ", "_")
        Return ds
    End Function

End Class
