﻿Imports System.String
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Configuration




Public Class ActivityLog
    Private db As DBHelper
    Public Sub New(db12Conx As String)
        db = New DBHelper(db12Conx)
    End Sub



    'Friend Sub Log(Methodology As String, refineryID As String, CallerIP As String, UserID As String, ComputerName As String, Service As String, Method As String, EntityName As String, PeriodStart As String, PeriodEnd As String, Notes As String, status As String)

    '    db.RefineryID = refineryID
    '    db.QueryDb("spLogActivity 'Profile II','" & db.CleanText(Methodology) & "','" & db.CleanText(refineryID) & "','" & db.CleanText(CallerIP) & "','" & db.CleanText(UserID) & _
    '               "','" & db.CleanText(ComputerName) & _
    '               "', '" & db.CleanText(Service) & "','" & db.CleanText(Method) & "','" & db.CleanText(EntityName) & "','" & db.CleanText(PeriodStart) & _
    '               "','" & db.CleanText(PeriodEnd) & "','" & db.CleanText(Notes) & "','" & db.CleanText(status) & "'")
    '    db.RefineryID = Nothing
    'End Sub

    Friend Sub LogExtended(ApplicationName As String, methodology As String, refineryID As String, CallerIP As String,
                   UserID As String, ComputerName As String, service As String,
                   Method As String, EntityName As String, PeriodStart As Date?, PeriodEnd As Date?,
                   status As String,
                   version As String, errorMessages As String, localDomainName As String,
                   osVersion As String, browserVersion As String, officeVersion As String,
                   dataImportedFromBridgeFile As String)

        Try
            '    <add key="LogToActivityLog" value="F"/>
            Select Case ConfigurationManager.AppSettings("LogToActivityLog").ToUpper()
                Case "T", "TRUE", "Y", "YES"
                    'continue
                Case Else
                    Exit Sub
            End Select
        Catch
        End Try

        '@Application varchar(20),
        '@Methodology varchar(20),
        '@RefineryID varchar(6), 
        '@CallerIP varchar(20), 

        '@UserID varchar(20), 
        '@ComputerName varchar(50), 
        '@Service varchar(50),

        '@Method varchar(50), 
        '@EntityName varchar(50), 
        '@PeriodStart Varchar(20),
        '@PeriodEnd varchar(20),

        '@Notes varchar(max),
        '@Status varchar(50) 



        'USE [ProfileFuels12]
        'GO

        'DECLARE	@return_value int

        'EXEC	@return_value = [dbo].[spLogActivityExtended]
        '		@Application = N'Profile II',
        '		@Methodology = N'2012',
        '		@RefineryID = N'XXPAC',
        '		@CallerIP = N'192.168.27.84',
        '		@UserID = N'sfb',
        '		@ComputerName = N'THINK-S1F7B50',
        '		@Service =N'',
        '		@Method = N'LOGIN',
        '		@EntityName = N'',
        '		@PeriodStart = N'',

        '		@PeriodEnd = N'',
        '		@Notes = N'',
        '		@Status = N'Success',
        '		@AppVersion = N'6.0.0.0',
        '		@ErrorMessages = N'',
        '		@LocalDomainName = N'DC1',
        '		@OSVersion = N'6.1.7601.65536; 64 bit',
        '		@BrowserVersion = N'',
        '@OfficeVersion = N'',
        '@DataImportedFromBridgeFile  = N''

        'SELECT	'Return Value' = @return_value

        'GO



        Try

            'Dim methodology As String = String.Empty 'not needed per Richard but is needed per proc
            browserVersion = String.Empty 'this is for future use
            Dim notes As String = String.Empty 'Richard doesn't sseem to want to use this
            service = String.Empty 'will be using the App argument instead for this App

            db.RefineryID = refineryID  'Needed for Is2012() 
            db.QueryDb("spLogActivityExtended '" & db.CleanText(ApplicationName) & "','" & db.CleanText(methodology) & "','" &
                   db.CleanText(refineryID) & "','" & db.CleanText(CallerIP) & "','" & db.CleanText(UserID) & "','" &
                   db.CleanText(ComputerName) & "', '" & db.CleanText(service) & "','" & db.CleanText(Method) & "','" &
                   db.CleanText(EntityName) & "','" & PeriodStart & "','" & PeriodEnd & "','" &
                   db.CleanText(notes) & "','" & db.CleanText(status) & "', '" & db.CleanText(version) &
                    "', '" & db.CleanText(errorMessages) & "', '" & db.CleanText(localDomainName) &
                      "', '" & db.CleanText(osVersion) & "', '" & db.CleanText(browserVersion) &
                        "', '" & db.CleanText(officeVersion) & "', '" & db.CleanText(dataImportedFromBridgeFile) & "'")
            db.RefineryID = Nothing


        Catch ex As Exception
            Dim debutMessage As String = ex.Message
        End Try
    End Sub


End Class
