<!-- config-start -->

<!-- config-end -->
<!-- template-start -->

<table class='small' border=0 width=100%>
	<tr>
		<td width=475 colspan=2>&nbsp;</td>
		<td width=3% align="center"><b>ReportPeriod</b></td>
		<td>&nbsp;</td>
		<td width=3% align="center"><b>Target</b></td>
		<td>&nbsp;</td>
	</tr>
	SECTION(CustomUnitData,,)
      BEGIN
         Header('<tr><td>&nbsp;</td></tr><tr><td colspan=6><b>Unit Name: @UnitName</b></td></tr>')
	<tr>
		<td>&nbsp;</td>
		<td>USEUNIT(USDescription,MetDescription)</td>
		<td align="right">NoShowZero(Format(USEUNIT(USValue,MetValue),USEUNIT(USDecPlaces,MetDecPlaces)))</td>
		<td>&nbsp;</td>
		<td align="right">NoShowZero(Format(USEUNIT(USTarget,MetTarget),USEUNIT(USDecPlaces,MetDecPlaces)))</td>
		<td>&nbsp;</td>
	</tr>
      END
</table>
<!-- template-end -->