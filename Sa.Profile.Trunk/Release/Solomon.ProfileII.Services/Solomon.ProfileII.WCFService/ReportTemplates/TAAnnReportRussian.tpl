<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small border=0>
<!--
  <tr>
    <td colspan=9 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
-->
SECTION(Turnarounds,,)
BEGIN 
HEADER('
  <tr>
    <td colspan=9 valign=bottom><strong>Последний капитальный ремонта по состоянию на Format(PeriodStart,'MM.yyyy')</strong></td>
  </tr>
')
END
  <tr>
    <td width=200></td>
    <td width=80></td>
    <td width=80></td>
    <td width=60></td>
    <td width=100></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
    <td width=80></td>
  </tr>
  <tr>
    <td valign=bottom><strong>Название установки</strong></td>  <!-- Process Unit Name -->
    <td align=center valign=bottom><strong>Интервал между кап. ремонтами, дни</strong></td>  <!-- Interval (Days) -->
    <td align=center valign=bottom><strong>Интервал между кап. ремонтами, лет</strong></td>  <!-- Interval (Years) -->
    <td align=center valign=bottom><strong>Срок простоя установки в связи с кап. ремонтами,</br>часы</strong></td>  <!-- Hours Down -->
    <td align=center valign=bottom><strong>Потеря готовности из-за кап. ремонтов, %</strong></td>  <!-- Unavailability Due to T/A -->
    <td align=center valign=bottom><strong>Полные расходы</strong></td>  <!-- T/A Cost -->
    <td align=center valign=bottom><strong>Полные расходы, приведенные к году</strong></td>  <!-- Annualized T/A Cost -->
    <td align=center valign=bottom><strong>Все рабочие часы</strong></td>  <!-- Total Work Hours -->
    <td align=center valign=bottom><strong>Рабочие часы, приведенные в году</strong></td>  <!-- Annualized Work hours -->
  </tr>

  SECTION(Turnarounds,, SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td>&nbsp;&nbsp;&nbsp;@UnitName</td>
    <td align=right>Format(TAIntDays,'#,##0')</td>
    <td align=right>Format(TAIntYrs,'#,##0.00')</td>
    <td align=right>Format(TAHrsDown,'#,##0')</td>
    <td align=right>Format(MechUnavailTA_Ann,'#,##0.00')</td>
    <td align=right>Format(TACost,'#,##0')</td>
    <td align=right>Format(AnnTACost,'#,##0')</td>
    <td align=right>Format(TAEffort,'#,##0')</td>
    <td align=right>Format(AnnTAEffort,'#,##0')</td>
  </tr>
  END

  SECTION(TATotals,,)
  BEGIN
 			

  <tr></tr>
  <tr>
    <td colspan=4><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Все топливное производство</strong></td>  <!-- Total Refinery -->
    <td align=right><strong>Format(MechUnavailTA_Ann,'#,##0.00')</strong></td>
    <td></td>
    <td align=right><strong>Format(TotAnnTACost,'#,##0')</strong></td>
    <td align=right><strong>Format(TotTAEffort,'#,##0')</strong></td>
    <td align=right><strong>Format(TotAnnTAEffort,'#,##0')</strong></td>
  </tr>

  <tr></tr>
  <tr>
    <td colspan=4><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В пересчете на месяц</strong></td>  <!-- Allocation for Month -->
    <td align=right><strong>Format(MechUnavailTA_Ann,'#,##0.00')</strong></td>
    <td></td>
    <td align=right><strong>Format(MonthTACost,'#,##0')</strong></td>
    <td></td>
    <td align=right><strong>Format(MonthTAEffort,'#,##0')</strong></td>
  </tr>
  END

</table>

<!-- template-end -->
