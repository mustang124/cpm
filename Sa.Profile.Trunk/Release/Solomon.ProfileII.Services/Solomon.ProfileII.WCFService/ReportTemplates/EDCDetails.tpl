<!-- config-start -->

<!-- config-end -->
<!-- template-start -->
<table class=small width=900 border=0>
  <tr>
    <td colspan=20 valign=bottom><strong>ReportPeriod</strong></td>
  </tr>
  <tr>
    <td width=5></td>
    <td width=200></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=50></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
    <td width=50></td>
    <td width=5></td>
    <td width=50></td>
    <td width=5></td>
    <td width=70></td>
    <td width=10></td>
    <td width=70></td>
    <td width=10></td>
  </tr>
  <tr>
    <td colspan=2 valign=bottom><strong>Process Unit Name</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Process ID</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Process</br>Type</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Capacity</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Utilization, %</strong></td>
    <td colspan=2 align=center valign=bottom><strong>EDC</br>Constant</strong></td>
    <td colspan=2 align=center valign=bottom><strong>kEDC</strong></td>
    <td colspan=2 align=center valign=bottom><strong>kUEDC</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Standard</br>Energy</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Estimated</br>Gain</strong></td>
  </tr>

  SECTION(ProcessUnits, ProcessGrouping = 'Process', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td></td>
    <td>@ProcessType</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDCFactor,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
  </tr>
  END

  SECTION(UnitsOffsites,,)
  BEGIN
  <tr>
    <td colspan=8><strong>Process Unit Total</strong></td>
    <td align=right valign=bottom><strong>Format(ProcessUtilPcnt,'#,##0.0')</strong></td>
    <td colspan=3></td>
    <td align=right valign=bottom><strong>Format(ProcessEDC,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(ProcessUEDC,'#,##0')</strong></td>
    <td colspan=5></td>
  </tr>
  END

  <tr>
    <td colspan=20 height=30 valign=bottom><strong>Ancillary Units</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Ancillary', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td></td>
    <td>@ProcessType</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDCFactor,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 valign=bottom><strong>Idle Units</strong></td>
  </tr>
  SECTION(ProcessUnits, ProcessGrouping = 'Idle', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td></td>
    <td>@ProcessID</td>
    <td></td>
    <td>@ProcessType</td>
    <td align=right>Format(USEUNIT(Cap,RptCap),'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDCFactor,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
  </tr>
  END

  <tr>
    <td colspan=20 height=30 valign=bottom><strong>Utilities and Off-sites</strong></td>
  </tr>

  SECTION(ProcessUnits,ProcessGrouping = 'Utility' AND ProcessID <> 'ELECDIST' AND ProcessID <> 'TNK+BLND', SORTKEY ASC,UNITID ASC)
  BEGIN
  <tr>
    <td></td>
    <td>@UnitName</td>
    <td colspan=4></td>
    <td align=right>Format(Cap,'#,##0')</td>
    <td></td>
    <td align=right>Format(UtilPcnt,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(EDCFactor,'#,##0.0')</td>
    <td></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdEnergy,'#,##0')</td>
    <td></td>
    <td align=right>Format(StdGain,'#,##0')</td>
    <td></td>
  </tr>
  END


  SECTION(UnitsOffsites,,)
  BEGIN
 
  <tr>
    <td></td>
    <td colspan=5>Standard Tankage</td>
    <td align=right>Format(TnkStdCap,'#,##0')</td>
    <td colspan=5></td>
    <td align=right>Format(TnkStdEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(TnkStdEDC,'#,##0')</td>
    <td colspan=13></td>
  </tr>

  <tr>
    <td></td>
    <td colspan=13>Purchased Utility UEDC Credit</td>
    <td align=right>Format(PurchasedUtilityUEDC,'#,##0')</td>
    <td colspan=13></td>
  </tr>

  <tr>
    <td></td>
    <td colspan=3>Receipts and Shipments</td>
    <td colspan=8></td>
    <td align=right>Format(kEDC,'#,##0')</td>
    <td></td>
    <td align=right>Format(kUEDC,'#,##0')</td>
    <td colspan=5></td>
  </tr>

  <tr>
    <td></td>
    <td colspan=3>Sensible Heat</td>
    <td colspan=12></td>
    <td align=right>Format(SensHeatStdEnergy,'#,##0')</td>
    <td colspan=9></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=3>Asphalt</td>
    <td colspan=12></td>
    <td align=right>Format(AspStdEnergy,'#,##0')</td>
    <td colspan=9></td>
  </tr>
  <tr>
    <td></td>
    <td colspan=3>All Other</td>
    <td colspan=12></td>
    <td align=right>Format(OffsitesStdEnergy,'#,##0')</td>
    <td colspan=9></td>
  </tr>
  <tr>
    <td height=30 valign=bottom colspan=8><strong>Total Refinery</strong></td>
    <td align=right valign=bottom><strong>Format(RefUtilPcnt,'#,##0.0')</strong></td>
    <td colspan=3></td>
    <td align=right valign=bottom><strong>Format(TotEDC,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(TotUEDC,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(TotStdEnergy,'#,##0')</strong></td>
    <td></td>
    <td align=right valign=bottom><strong>Format(EstGain,'#,##0')</strong></td>
    <td></td>
  </tr>
  END
</table>

<!-- template-end -->