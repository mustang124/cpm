<!-- config-start -->
FILE=_CONFIG/RefineryInformation.xml;
<!-- config-end -->
<!-- template-start -->
<table class=small width=100% border=0>
  <tr>
    <td colspan=6></td>
    <td colspan=2 align=center valign=bottom><strong>Number<br>of Tanks</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Total Storage<br>Capacity, bbl</strong></td>
    <td colspan=2 align=center valign=bottom><strong>Average<br>Inventory<br>Level, %</strong></td>
  </tr>
  <tr>
    <td colspan=12 height=30 valign=bottom><strong>Inventory </strong></td>
  </tr>
  SECTION(Inventory,TankType='CRD',)
  BEGIN
  <tr>
    <td width=5></td>
    <td colspan=5 align=left>Crude Oil &amp; Condensate</td>
    <td width=50 align=right>Format(NumTank,'#,##0')</td>
    <td width=10></td>
    <td width=100 align=right>Format(FuelsStorage,'#,##0')</td>
    <td width=10></td>
    <td width=50 align=right>Format(AvgLevel,'#,##0.0')</td>
    <td width=10></td>
  </tr>
  END
  SECTION(Inventory,TankType='INT',)
  BEGIN
  <tr>
    <td></td>
    <td colspan=5 align=left>Intermediate Stocks, Unfinished Oils &amp; Blendstocks</td>
    <td align=right>Format(NumTank,'#,##0')</td>
    <td></td>
    <td align=right>Format(FuelsStorage,'#,##0')</td>
    <td></td>
    <td align=right>Format(AvgLevel,'#,##0.0')</td>
    <td></td>
  </tr>
  END
  SECTION(Inventory,TankType='FIN',)
  BEGIN
  <tr>
    <td></td>
    <td colspan=5 align=left>Finished Products</td>
    <td align=right>Format(NumTank,'#,##0')</td>
    <td></td>
    <td align=right>Format(FuelsStorage,'#,##0')</td>
    <td></td>
    <td align=right>Format(AvgLevel,'#,##0.0')</td>
    <td></td>
  </tr>
  END
  <tr>
    <td height=30 valign=bottom colspan=2></td>
    <td height=45 valign=bottom colspan=4 align=center><strong>--- Monthly Throughput,bbl ---</strong></td>
    <td colspan=6></td>
  </tr>
  <tr>
    <td colspan=2></td>
    <td colspan=2 align=center><strong>Raw Material<br>Receipts</strong></td>
    <td colspan=2 align=center><strong>Product<br>Shipments</strong></td>
    <td colspan=6></td>
  </tr>
  <tr>
    <td colspan=12 height=30 valign=bottom><strong>Inventory - Transport</strong></td>
  </tr>
  <tr>
    <td></td>
    <td width=150>Railcar</td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsCrude',) BEGIN Format(RailcarBBL,'#,##0') END </td>
    <td width=5></td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsProd',) BEGIN Format(RailcarBBL,'#,##0') END </td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td></td>
    <td width=150>Tank Truck</td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsCrude',) BEGIN Format(TankTruckBBL,'#,##0') END </td>
    <td width=5></td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsProd',) BEGIN Format(TankTruckBBL,'#,##0') END </td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td></td>
    <td width=150>Tanker Berth</td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsCrude',) BEGIN Format(TankerBerthBBL,'#,##0') END </td>
    <td width=5></td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsProd',) BEGIN Format(TankerBerthBBL,'#,##0') END </td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td></td>
    <td width=150>Offshore Buoy</td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsCrude',) BEGIN Format(OffshoreBuoyBBL,'#,##0') END </td>
    <td width=5></td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsProd',) BEGIN Format(OffshoreBuoyBBL,'#,##0') END </td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td></td>
    <td width=150>Barge Berth</td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsCrude',) BEGIN Format(BargeBerthBBL,'#,##0') END </td>
    <td width=5></td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsProd',) BEGIN Format(BargeBerthBBL,'#,##0') END </td>
    <td colspan=7></td>
  </tr>
  <tr>
    <td></td>
    <td width=150>Pipeline</td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsCrude',) BEGIN Format(PipelineBBL,'#,##0') END </td>
    <td width=5></td>
    <td width=80 align=right>SECTION(ConfigRS,ProcessID='RsProd',) BEGIN Format(PipelineBBL,'#,##0') END </td>
    <td colspan=7></td>
  </tr>
</table>

<!-- template-end -->
