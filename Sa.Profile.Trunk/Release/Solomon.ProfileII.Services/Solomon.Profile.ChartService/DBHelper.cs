﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Solomon.Profile.ChartService
{
    internal class DBHelper : IDisposable
    {
        private bool _disposing = false;
        private SqlConnection _conx = null;
        private string _connectionString = string.Empty;

        public string table { get; set; }


        internal DBHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        internal ChartsDataset TestGetData(string sql)
        {
            return null; // PopulateRadarChartsDataset();
        }

        internal ChartsDataset.PlantsInfoTemplateDataTable GetPlantColumnData(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, //string factorSet,
            string uom, string scenario)
        {
            string sql = SqlHelper.BuildPlantInfoSql(field1, refineryId, periodStart,
                periodEnd, ChartTitle, currency, uom, scenario);
            table = "GENSUM";
            using (_conx = new SqlConnection(_connectionString))
            {
                ChartsDataset.PlantsInfoTemplateDataTable dt = new ChartsDataset.PlantsInfoTemplateDataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
            /*
            DataSet dsCol = new DataSet();
            dsCol.ReadXml(@"C:\chrtDataColumn.xml");
            return dsCol;
            */
        }

        internal ChartsDataset.PlantsInfoTemplateDataTable GetMechAvailColumnData(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, //string factorSet,
            string uom, string scenario)
        {
            string sql = SqlHelper.BuildPlantMechAvailInfoSql(field1, refineryId, periodStart,
                periodEnd, ChartTitle, currency, uom, scenario);
            table = "MAINTAVAILCALC";
            using (_conx = new SqlConnection(_connectionString))
            {
                ChartsDataset.PlantsInfoTemplateDataTable dt = new ChartsDataset.PlantsInfoTemplateDataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
            /*
            DataSet dsCol = new DataSet();
            dsCol.ReadXml(@"C:\chrtDataColumn.xml");
            return dsCol;
            */
        }

        internal ChartsDataset.PlantsInfoTemplateDataTable GetMaintIndexColumnData(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, //string factorSet,
            string uom, string scenario)
        {
            string sql = SqlHelper.BuildPlantMaintIndexInfoSql(field1, refineryId, periodStart,
                periodEnd, ChartTitle, currency, uom, scenario);
            table = "MAINTINDEX";
            using (_conx = new SqlConnection(_connectionString))
            {
                ChartsDataset.PlantsInfoTemplateDataTable dt = new ChartsDataset.PlantsInfoTemplateDataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
            /*
            DataSet dsCol = new DataSet();
            dsCol.ReadXml(@"C:\chrtDataColumn.xml");
            return dsCol;
            */
        }

        //internal ChartsDataset.DataTable1DataTable GetDatatable1ColumnData(string field1, string refineryId, string periodStart,
        //string periodEnd, string ChartTitle, string currency, string factorSet,
        //string uom, string scenario)
        //{
        //    //study will be NSA, PAC, etc.

        //    string sql = SqlHelper.BuildDataTable1Sql(refineryId, periodStart,
        //    periodEnd, ChartTitle, currency, factorSet, uom, scenario);

        //    table = "GENSUM";

        //    using (_conx = new SqlConnection(_connectionString))
        //    {
        //        ChartsDataset.DataTable1DataTable dt = new ChartsDataset.DataTable1DataTable();
        //        SqlCommand cmd = new SqlCommand(sql, _conx);
        //        SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
        //        adp.Fill(dt);
        //        return dt;
        //    }
        //    /*
        //    DataSet dsCol = new DataSet();
        //    dsCol.ReadXml(@"C:\chrtDataColumn.xml");
        //    return dsCol;
        //    */
        //}

        //BuildDataTable1SqlWithTheseFields
        internal ChartsDataset.DataTable1DataTable GetDatatable1ColumnDataWithTheseFields(string field1, string refineryId, string periodStart,
        string periodEnd, string ChartTitle, string currency, string factorSet,
        string uom, string scenario, string tableName, string ytdField, string targetField, string aveField)
        {
            //study will be NSA, PAC, etc.

            string sql = SqlHelper.BuildDataTable1SqlWithTheseFields(refineryId, periodStart,
            periodEnd, ChartTitle, currency, factorSet, uom, scenario,tableName,ytdField,targetField,aveField);

            table = "GENSUM";

            using (_conx = new SqlConnection(_connectionString))
            {
                ChartsDataset.DataTable1DataTable dt = new ChartsDataset.DataTable1DataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
            /*
            DataSet dsCol = new DataSet();
            dsCol.ReadXml(@"C:\chrtDataColumn.xml");
            return dsCol;
            */
        }
        internal DataTable GetDatatable1QuartileData(int peerGroupId,  ref List<int> vkeyList, string refNum, DateTime? startDate,
             string scenario,  string currency, string uom)
        {
            List<int> foundVkeys = new List<int>();
            //this is taking too long to put in a proc.
            ChartsDataset ds = new ChartsDataset();
            int factorSet = -1;
            string sql = "select ReportFactorSet from dbo.TSort where RefineryID='" + refNum + "';"; 
            DataSet dsTemp = new DataSet();
            using (_conx = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dsTemp);
            }
            if (dsTemp != null && dsTemp.Tables.Count == 1 && dsTemp.Tables[0].Rows.Count > 0)
            {
                factorSet = Convert.ToInt32(dsTemp.Tables[0].Rows[0]["ReportFactorSet"]);
                string submissionUom = uom;
                if(submissionUom.ToUpper().Contains("METRIC"))
                {
                    submissionUom = "MET";
                }
                else if(submissionUom.ToUpper().Contains("US UNITS"))
                {
                    submissionUom = "US";
                }

                sql = "SELECT * from dbo.Submissions where RefineryID = '" + refNum + "' and PeriodStart = '" + startDate.ToString() +
                "'  AND RptCUrrency= '" + currency +
                "' and UOM = '" + submissionUom + "' and DataSet = 'Actual';";
                // int factorSet, string scenario,  string currency, string uom)
                //need to find submission id etc, use startDate and refnum.
                dsTemp = new DataSet();
                using (_conx = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand(sql, _conx);
                    SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                    adp.Fill(dsTemp);
                }
                int submissionID = -1;
                if (dsTemp != null && dsTemp.Tables.Count == 1 && dsTemp.Tables[0].Rows.Count > 0)
                {
                    submissionID = Convert.ToInt32(dsTemp.Tables[0].Rows[0]["SubmissionID"]);

                    //ChartsDataset.DataTable1DataTable table1 = new ChartsDataset.DataTable1DataTable();
                    //ds.Tables.Add(table1);
                    int tablesCount = ds.Tables.Count;

                    //get the mo/tgt/ave/ytd data

                    //-fields from chartlu, using vkeyList
                
                    foreach (int vkey in vkeyList)
                    {
                        StringBuilder builder = new StringBuilder();
                        


                        //NOTE:  v.vkey is NOT really the Sort. It is a way to give each record an ID because
                        //      it might need to be deleted later, and will need the remove that that kpi 
                        //      from the list of kpis passed in.
                        //      When get the ytd/ave/mo data, THEN will change the Sort to be the real Sort
                        //builder.Append("SELECT v.vkey as Sort,  ");  //
                        builder.Append("SELECT null as Sort,  ");



                        builder.Append(" (select YEAR(periodstart) from dbo.Submissions where submissionid =" + submissionID + ")  as PeriodYear, ");
                        builder.Append(" (select MONTH(periodstart) from dbo.Submissions where submissionid = " + submissionID + ")   as PeriodMonth,");
                        builder.Append(" (select STR(MONTH(periodstart)) from dbo.Submissions where submissionid = " + submissionID + ") +  '/'  + ");
                        builder.Append(" (select LTRIM(STR(YEAR(periodstart))) from dbo.Submissions where submissionid = " + submissionID + ") ");
                        builder.Append(" as PeriodDisplay,");
                        builder.Append(" c.ChartTitle as [Description], ");
                        builder.Append(" q.Top2 as QTop2, q.Tile1Break as Q1to2,  q.Tile2Break as Q2to3, q.Tile3Break as Q3to4, q.Bottom2 as QBottom2,");
                        builder.Append(" null as Ytd, null as [Avg], null as [Target], null as [Month]");
                        builder.Append(" from dbo.Chart_LU c");
                        builder.Append(" left outer join profile.TargetingVariablesAvailable a on c.VKey=a.VKey ");
                        builder.Append(" left outer join ResultsDB.dbo.VarDef v on c.VKey=v.VKey ");
                        builder.Append(" left outer join ResultsDB.dbo.Quartiles q on q.VKey=c.VKey and q.PGKey= " + peerGroupId.ToString() + "");
                        builder.Append(" where ");
                        builder.Append(" v.vkey = " + vkey.ToString() + ";");
                        sql = builder.ToString();

                        dsTemp = new DataSet();
                        using (_conx = new SqlConnection(_connectionString))
                        {
                            SqlCommand cmd = new SqlCommand(sql, _conx);
                            SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                            adp.Fill(dsTemp);
                        }
                        if (dsTemp != null && dsTemp.Tables.Count == 1 && dsTemp.Tables[0].Rows.Count == 1)
                        {
                            DataRow quartileRow = ds.DataTable1.NewRow();
                            bool allNull = true;
                            
                            for (int fieldCount = 0; fieldCount < 10; fieldCount++)
                            {
                                quartileRow[fieldCount] = dsTemp.Tables[0].Rows[0][fieldCount];
                                
                                if (fieldCount > 4 && fieldCount < 11)
                                {
                                    //System.Diagnostics.Debug.WriteLine(dsTemp.Tables[0].Columns[fieldCount].ColumnName);
                                    if (dsTemp.Tables[0].Rows[0][fieldCount] != DBNull.Value)
                                    { allNull = false; }
                                }
                                            
                            }
                            if (!allNull)
                            {
                                System.Diagnostics.Debug.WriteLine(dsTemp.Tables[0].Rows[0]["Description"].ToString());
                                foundVkeys.Add(vkey);
                                ds.DataTable1.Rows.Add(quartileRow);
                            }
                            

                            //WriteXml(ds.DataTable1, @"C:\quartileRow.xml");
                            if (!allNull)
                            {
                                dsTemp = new DataSet();
                                sql = BuildYtdAvgTgtMoSqlForRadar(vkey, submissionID, factorSet, scenario, currency, uom);
                                using (_conx = new SqlConnection(_connectionString))
                                {
                                    SqlCommand cmd = new SqlCommand(sql, _conx);
                                    SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                                    adp.Fill(dsTemp);
                                }

                                //WriteXml(dsTemp.Tables[0], @"C:\YtdAvgTgtMoRow.xml");
                                if (dsTemp != null && dsTemp.Tables.Count > 0 && dsTemp.Tables[0].Rows.Count > 0)
                                {
                                    for (int fieldCount = 0; fieldCount < 4; fieldCount++)
                                    {
                                        quartileRow[fieldCount + 10] = dsTemp.Tables[0].Rows[0][fieldCount];
                                    }
                                }
                            }
                            //WriteXml(ds.DataTable1, @"C:\FinalRadarRow.xml");
                        }


                    }
                }
            }
            vkeyList = foundVkeys;

            //--loop thru all fields and make calls 

            //get quartile data and put into table1

            //dynamically add mo/tgt/ave/ytd to table1 based on vkey which is not stored in the dataset!!!
            //WriteXml(ds.DataTable1, @"C:\FinalDataTable1.xml");
            return ds.DataTable1;
        }

        private string BuildYtdAvgTgtMoSqlForRadar( int vkey)
        {
            //List<string> fieldNamesToUse = new List<string>() {  "YTDField", "AvgField", "TargetField", "ValueField1" };
            string chartluFieldName = string.Empty;
            string tableName = string.Empty;
            string sql = "SELECT DataTable FROM dbo.Chart_LU WHERE VKey = " + vkey.ToString();
            DataSet dsTemp = new DataSet();
            using (_conx = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dsTemp);
                tableName = dsTemp.Tables[0].Rows[0]["DataTable"].ToString();
            }
            sql = "SELECT YTDField, AvgField, TargetField, ValueField1 FROM dbo.Chart_LU WHERE VKey = " + vkey.ToString();
            dsTemp = new DataSet();
            using (_conx = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                dsTemp = new DataSet();
                adp.Fill(dsTemp);
            }
            return "SELECT " + dsTemp.Tables[0].Rows[0]["YTDField"].ToString() + "," +
            dsTemp.Tables[0].Rows[0]["AvgField"].ToString() + "," +
            dsTemp.Tables[0].Rows[0]["TargetField"].ToString() + "," +
            dsTemp.Tables[0].Rows[0]["ValueField1"].ToString() + 
            " FROM dbo." + tableName +
            " WHERE ";
        }

        private string BuildYtdAvgTgtMoSqlForRadar(int vkey, int submissionId, int factorSet, string scenario, string currency, string uom)
        {
            //List<string> fieldNamesToUse = new List<string>() {  "YTDField", "AvgField", "TargetField", "ValueField1" };
            string chartluFieldName = string.Empty;
            string tableName = string.Empty;
            string sql = "SELECT DataTable FROM dbo.Chart_LU WHERE VKey = " + vkey.ToString();
            DataSet dsTemp = new DataSet();
            using (_conx = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dsTemp);
                tableName = dsTemp.Tables[0].Rows[0]["DataTable"].ToString();
            }
            sql = "SELECT YTDField, AvgField, TargetField, ValueField1 FROM dbo.Chart_LU WHERE VKey = " + vkey.ToString();
            dsTemp = new DataSet();
            using (_conx = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                dsTemp = new DataSet();
                adp.Fill(dsTemp);
            }
            if (uom.ToUpper().Contains("METRIC"))
            {
                uom = "MET";
            }
            else if(uom.ToUpper().Contains("US UNITS"))
            {
                uom = "US";
            }
            string returnSql = string.Empty;
            switch(tableName.ToUpper())
            {
                case "GENSUM":
                    returnSql = "SELECT " + dsTemp.Tables[0].Rows[0]["YTDField"].ToString() + "," +
                    dsTemp.Tables[0].Rows[0]["AvgField"].ToString() + "," +
                    " NULL, " + //quartiles will be the targets instead  dsTemp.Tables[0].Rows[0]["TargetField"].ToString() + "," +
                    dsTemp.Tables[0].Rows[0]["ValueField1"].ToString() +
                    " FROM dbo." + tableName +
                    " WHERE  Submissionid = " + submissionId.ToString() +
                    " and FactorSet = " + factorSet.ToString() +
                    " and Scenario = '" + scenario + "' " +
                    " and Currency = '" + currency + "' " +
                    " and UOM = '" + uom + "';";
                    break;
                case "MAINTAVAILCALC":
                    returnSql = "SELECT " + dsTemp.Tables[0].Rows[0]["YTDField"].ToString() + "," +
                    dsTemp.Tables[0].Rows[0]["AvgField"].ToString() + "," +
                   " NULL, " + //quartiles will be the targets instead   dsTemp.Tables[0].Rows[0]["TargetField"].ToString() + "," +
                    dsTemp.Tables[0].Rows[0]["ValueField1"].ToString() +
                    " FROM dbo." + tableName +
                    " WHERE  Submissionid = " + submissionId.ToString() +
                    " and FactorSet = " + factorSet.ToString() + ";";
                    break;
                case "MAINTINDEX":
                    returnSql = "SELECT " + dsTemp.Tables[0].Rows[0]["YTDField"].ToString() + "," +
                    dsTemp.Tables[0].Rows[0]["AvgField"].ToString() + "," +
                    " NULL, " + //quartiles will be the targets instead  dsTemp.Tables[0].Rows[0]["TargetField"].ToString() + "," +
                    dsTemp.Tables[0].Rows[0]["ValueField1"].ToString() +
                    " FROM dbo." + tableName +
                    " WHERE  Submissionid = " + submissionId.ToString() +
                    " and currency = '" + currency + "' " +
                    " and FactorSet = " + factorSet.ToString() + ";";
                    break;
                default:
                    throw new Exception("Unexpected tablename " + tableName);
                    break;
            }
            return returnSql;
        }

        private void WriteXml(DataTable table, string path)
        {
            try
            {
                table.WriteXml(path);
            }
            catch { }
        }

        public string GetFactorSet(string refId)
        {
            string factorSet = string.Empty;
            string factorSetSql = SqlHelper.GetFactorset(refId);
            using (_conx = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(factorSetSql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(factorSetSql, _conx);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                factorSet = ds.Tables[0].Rows[0][0].ToString().Trim();
            }
            return factorSet;
        }

        internal List<string> GetKpiFieldsFromChartLU(List<int> vkeys)
        {
            string sql = SqlHelper.GetKpiNamesFromChartLUSql(vkeys);
            DataSet ds = new DataSet();
            using (_conx = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(ds);
            }
            List<string> results = new List<string>();
            if(ds!=null && ds.Tables.Count==1 && ds.Tables[0].Rows.Count>0)
            {
                foreach(DataRow row in ds.Tables[0].Rows)
                {
                    string value = row["ValueField1"].ToString() ?? string.Empty;
                    if (value.Length > 0)
                    {
                        value = (row["DataTable"] == DBNull.Value ? string.Empty : row["DataTable"].ToString() + ".") + value;
                    }
                    results.Add(value);
                }
            }
            return results;
        }

        internal ChartsDataset.DataTable1DataTable GetAvgYtdTargetDatatableData(List<string> kpiFields, string rankBreak, string breakValue, string refId,
            string currency, string uom, string periodStart, string scenario)
        {
            string factorSet = GetFactorSet(refId);
            //study will be NSA, PAC, etc.
            List<string> lst = SqlHelper.BuildAvgYtdTargetDataTable1Sql(kpiFields, breakValue, refId, currency, uom, factorSet, periodStart, scenario);
            table = "GENSUM";
            /*
            lst = new List<string>();

            lst.Add("SELECT 0 as Sort, 'EII' as [Description], EII_YTD as Ytd, EII_Avg   as Avg,  EII_Target  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 1 as Sort, 'MaintEffIndex' as [Description], 100 as Ytd, 100   as Avg, 100  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 2 as Sort, 'MaintIndex' as [Description], MaintIndex_YTD as Ytd, MaintIndex_Avg   as Avg,  MaintIndex_Target  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 3 as Sort, 'MaintPersEffIndex' as [Description], 100 as Ytd, 100   as Avg,  100  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 4 as Sort, 'MechAvail' as [Description], MechAvail_YTD as Ytd, MechAvail_Avg   as Avg,  MechAvail_Target  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 5 as Sort, 'NEOpexEffIndex' as [Description], 150 as Ytd, 150   as Avg,  150  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 6 as Sort, 'OpAvail' as [Description], OpAvail_YTD as Ytd, OpAvail_Avg   as Avg,  OpAvail_Target  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 7 as Sort, 'PEI' as [Description], PEI_YTD as Ytd, PEI_Avg   as Avg,  PEI_Target  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            lst.Add("SELECT 8 as Sort, 'PersIndex' as [Description], 0 as Ytd, 0   as Avg,  0  as Target from dbo.GenSum where submissionid = (select SubmissionId from dbo.Submissions where RefineryId = '118NSA') and factorset = '2014' and currency = 'USD' and UOM = 'US';");
            */



            ChartsDataset.DataTable1DataTable first = null;
            ChartsDataset.DataTable1DataTable temp = new ChartsDataset.DataTable1DataTable(); // DataTable();
            temp.TableName = "AvgYtdTargetDatatableData";
            foreach (string item in lst)
            {
                using (_conx = new SqlConnection(_connectionString))
                {
                    //DataTable dt = new ChartsDataset.DataTable1DataTable();
                    SqlCommand cmd = new SqlCommand(item, _conx);
                    SqlDataAdapter adp = new SqlDataAdapter(item, _conx);
                    if (first == null)
                    {
                        first = new ChartsDataset.DataTable1DataTable();
                        first.TableName = "AvgYtdTargetDatatableData";
                        adp.Fill(first);
                        //DataRow dr = first.NewRow();
                        //dr[0] = 1;
                        //dr[10] = 10;
                        //dr[11] = 11;
                        //first.Rows.Add(dr);
                    }
                    else
                    {
                        adp.Fill(temp);
                        first.Merge(temp);
                        temp.Clear();
                    }
                }
                //if(System.IO.File.Exists(@"C:\first.xml"))
                //    System.IO.File.Delete(@"C:\first.xml");
                //first.WriteXml(@"C:\first.xml");
            }
            return first;
        }

        internal DataTable GetStackedColumnData(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, string factorSet,
            string uom, string scenario)
        {
            string sql = SqlHelper.BuildPlantInfoSql(field1, refineryId, periodStart,
                periodEnd, ChartTitle, currency, uom, scenario);
            table = "GENSUM";
            using (_conx = new SqlConnection(_connectionString))
            {
                DataTable dt = new ChartsDataset.PlantsInfoTemplateDataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
            /*
            DataSet dsCol = new DataSet();
            dsCol.ReadXml(@"C:\chrtDataStacked.xml");
            return dsCol;
            */
        }

        public DataTable GetUnitsList(string refineryId, string periodStart, string periodEnd)
        {
            string sql = SqlHelper.BuildUnitsListSql(refineryId, periodStart, periodEnd);
            table = string.Empty;
            using (_conx = new SqlConnection(_connectionString))
            {
                DataTable dt = new ChartsDataset.PlantsInfoTemplateDataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
        }

        internal ChartsDataset.PlantsInfoTemplateDataTable GetUnitCurrentMonths(string field1, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, string unitId)
        {
            //expecting field1 to be Desc and ^ and CurrentMonth column names
            string sql = SqlHelper.BuildUnitCurrentMonthsSql(field1, refineryId, periodStart,
                periodEnd, ChartTitle, currency, unitId);
            table = string.Empty;
            using (_conx = new SqlConnection(_connectionString))
            {
                ChartsDataset.PlantsInfoTemplateDataTable dt = new ChartsDataset.PlantsInfoTemplateDataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
        }

        internal ChartsDataset.DataTable1DataTable BuildDataTable1Units(List<string> fields, string refineryId, string periodStart,
            string periodEnd, string ChartTitle, string currency, string unitId)
        {
            //expecting fields to be list of 3 column names to use for YTD, then AVG, then Target
            string sql = SqlHelper.BuildDataTable1UnitsSql(fields, refineryId, periodStart,
                periodEnd, ChartTitle, currency, unitId);
            table = string.Empty;
            using (_conx = new SqlConnection(_connectionString))
            {
                ChartsDataset.DataTable1DataTable dt = new ChartsDataset.DataTable1DataTable();
                SqlCommand cmd = new SqlCommand(sql, _conx);
                SqlDataAdapter adp = new SqlDataAdapter(sql, _conx);
                adp.Fill(dt);
                return dt;
            }
        }

        /*
        private ChartsDataset PopulateRadarChartsDataset()
        {
            ChartsDataset ds = new ChartsDataset();

            DataTable dt = ds.Tables[0];
            DataRow dr = dt.NewRow();
            object[] rowArray = new object[] { 0, 2016, 1, "1/2016", "Desc1", 100, 80, 60, 40, 20, 10, 60, 55 };
            dr.ItemArray = rowArray; dt.Rows.Add(dr);
            dr = dt.NewRow();
            rowArray = new object[] { 1, 2016, 2, "2/2016", "Desc2", 100, 80, 60, 40, 20, 10, 60,65 };
            dr.ItemArray = rowArray; dt.Rows.Add(dr);
            dr = dt.NewRow();
            rowArray = new object[] { 2, 2016, 3, "3/2016", "Desc3", 100, 80, 60, 40, 20, 20, 40, 75 };
            dr.ItemArray = rowArray; dt.Rows.Add(dr);
            dr = dt.NewRow();
            rowArray = new object[] { 3, 2016, 4, "4/2016", "Desc4", 100, 80, 60, 40, 20, 10, 60, 45 };
            dr.ItemArray = rowArray; dt.Rows.Add(dr);

            DataTable dt2 = ds.Tables[1];
            DataTable dt3 = ds.Tables[1].Copy();

            dt2.TableName = "1";
            dr = dt2.NewRow();
            rowArray = new object[] { 0, 2016, 1, "1/2016", "Singapore", 50 };
            dr.ItemArray = rowArray; dt2.Rows.Add(dr);
            dr = dt2.NewRow();
            rowArray = new object[] { 1, 2016, 2, "2/2016", "Singapore", 30 };
            dr.ItemArray = rowArray; dt2.Rows.Add(dr);
            dr = dt2.NewRow();
            rowArray = new object[] { 2, 2016, 3, "3/2016", "Singapore", 40 };
            dr.ItemArray = rowArray; dt2.Rows.Add(dr);
            dr = dt2.NewRow();
            rowArray = new object[] { 3, 2016, 4, "4/2016", "Singapore", 50 };
            dr.ItemArray = rowArray; dt2.Rows.Add(dr);

            dr = dt3.NewRow();
            rowArray = new object[] { 0, 2016, 1, "1/2016", "Dallas", 40 };
            dr.ItemArray = rowArray; dt3.Rows.Add(dr);
            dr = dt3.NewRow();
            rowArray = new object[] { 1, 2016, 2, "2/2016", "Dallas", 30 };
            dr.ItemArray = rowArray; dt3.Rows.Add(dr);
            dr = dt3.NewRow();
            rowArray = new object[] { 2, 2016, 3, "3/2016", "Dallas", 40 };
            dr.ItemArray = rowArray; dt3.Rows.Add(dr);
            dr = dt3.NewRow();
            rowArray = new object[] { 3, 2016, 4, "4/2016", "Dallas", 20 };
            dr.ItemArray = rowArray; dt3.Rows.Add(dr);
            dt3.TableName = "2";
            ds.Tables.Add(dt3);

            return ds;
        }
        */
        //internal DataSet Test()
        //{
        //    return new DataSet();
        //}

        void IDisposable.Dispose()
        {
            if (_disposing)
                return;
            _disposing = true;
            if (_conx != null && _conx.State != ConnectionState.Closed)
            {
                try
                {
                    _conx.Close();
                    _conx.Dispose();
                }
                catch { }
            }
        }

    }
}
