﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Drawing;
using System.Configuration;


namespace Solomon.Profile.ChartService
{
    public class ChartFactory
    {
        private string _connectionString = string.Empty;

        private string _dbTable = string.Empty;
        public string dbtable
        {
            get { return _dbTable; }
            set { _dbTable = value; }
        }

        private string _factorSet = string.Empty;
        public string FactorSet
        {
            get { return _factorSet; }
        }

        public ChartFactory(string connectionString, string refineryId)
        {
            _connectionString = connectionString;
            DBHelper dbHelper = new DBHelper(_connectionString);
            _factorSet = dbHelper.GetFactorSet(refineryId);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="chartInfoRequested">chartInfoRequested is opex, energy, etc. describes the data to be returned.</param>
        /// <param name="chartType">The text description of the WIndows Chart type</param>
        /// <param name="chartColor">The text description of the color</param>
        /// <param name="avgColor">Color for Average line</param>
        /// <param name="ytdColor">Color for YTD line</param>
        /// <param name="targetColor">Color for Target line</param>
        /// <returns>Windows Chart</returns>
        //public Chart GetCommonChart( List<string> field, string refineryID, string periodStart,
        //    string periodEnd, string ChartTitle, string currency, ref string factorSet,
        //    string uom, string scenario,
        //    string chartInfoRequested, string chartType,
        //    string chartColor1, string avgColor, string ytdColor, string targetColor, string tableName = "GENSUM")
        //{
        //    Chart chart = null;
        //    int seriesColorCount = 0;
        //    try
        //    {
        //        DBHelper dbHelper = new DBHelper(_connectionString);
        //        ChartsDataset dataSet = new ChartsDataset();
        //        //string sql = SqlHelper.BuildDataTable1Sql(refineryID,periodStart,periodEnd,ChartTitle,currency,factorSet,uom,scenario);
        //        dbHelper.table = string.Empty;
        //        dbtable = dbHelper.table;
        //        factorSet = dbHelper.GetFactorSet(refineryID);
        //        ChartsDataset.DataTable1DataTable dt1 = dbHelper.GetDatatable1ColumnData(field[0], refineryID, periodStart, periodEnd, ChartTitle, currency, factorSet, uom, scenario);
        //        dataSet.Tables.Remove(dataSet.DataTable1);
        //        dt1.TableName = "DataTable1";
        //        dataSet.Tables.Add(dt1);

        //        //how will I know if there is another plant being requested???
        //        //first plant is field1 in conx string; other plants must be field2-field5
        //        //this will get the Plant info tables
        //        for (int i = 0; i < field.Count; i++)
        //        {
        //            if (field[i].Length > 0)
        //            {
        //                DataTable dtPlant = null;
        //                if (tableName.ToUpper() == "GENSUM")
        //                {
        //                    dtPlant = dbHelper.GetPlantColumnData(field[i], refineryID, periodStart, periodEnd, ChartTitle, currency, uom, scenario);
        //                }
        //                else if (tableName.ToUpper() == "MAINTAVAILCALC")
        //                {
        //                    dtPlant = dbHelper.GetMechAvailColumnData(field[i], refineryID, periodStart, periodEnd, ChartTitle, currency, uom, scenario);
        //                }
        //                else if (tableName == "MAINTINDEX")
        //                {
        //                    dtPlant = dbHelper.GetMaintIndexColumnData(field[i], refineryID, periodStart, periodEnd, ChartTitle, currency, uom, scenario);
        //                }
        //                dtPlant.TableName = "PlantInfo" + i.ToString();
        //                dataSet.Tables.Add(dtPlant);
        //            }
        //        }
        //        dbtable = dbHelper.table;
        //        //dbHelper.GetData(_connectionString, "");
        //        int seriesNum = 0;
        //        SeriesChartType seriesChartType = GetChartType(chartType);

        //        ChartBuilder builder = new ChartBuilder();
        //        ChartPageLegend legend = new ChartPageLegend();
        //        legend.Alignment = System.Drawing.StringAlignment.Center;
        //        legend.DockingPosition = Docking.Bottom;

        //        List<ChartPageSeries> seriesList = new List<ChartPageSeries>();
        //        List<Color> usedColors = new List<Color>();
        //        //always do this one first. other ones later.
        //        for (int i = 0; i < dataSet.Tables.Count; i++) //start at 1, table 0 will be handled later
        //        {
        //            foreach (DataTable table in dataSet.Tables)
        //            {
        //                if (table.TableName == "PlantInfo" + i.ToString())
        //                {
        //                    seriesList.Add(BuildChartPageSeries(table, table, "PeriodDisplay", "CurrentMonth", seriesNum, table.Rows[0]["Description"].ToString(),
        //                        GetChartType(chartType), GetNextSeriesColor(GetColor(chartColor1), ref usedColors), ref legend));
        //                    //GetChartType(chartType), GetColor(chartColor), ref legend));
        //                    seriesNum++;

        //                    //if (i == 0) chartColor = "Purple";
        //                    //chartColor = GetNextRadarColor(i + 2).ToString();
        //                    break;
        //                }
        //            }
        //        }

        //        //YTD, AVG, Targets last.
        //        //It looks like these are always going to be lines....
        //        SeriesChartType other3SeriesTypes = SeriesChartType.Line; //GetOther3ChartType(chartType);// 
        //        foreach (DataColumn col in dataSet.Tables["DataTable1"].Columns)
        //        {
        //            if ((col.ColumnName == "YearToDate" && ytdColor.Length > 0)
        //                || (col.ColumnName == "Target" && targetColor.Length > 0) ||
        //                (col.ColumnName == "RollingAverage" && avgColor.Length > 0))
        //            {
        //                string colorToUse = string.Empty;
        //                switch (col.ColumnName)
        //                {
        //                    case "YearToDate":
        //                        colorToUse = ytdColor;
        //                        break;
        //                    case "Target":
        //                        colorToUse = targetColor;
        //                        break;
        //                    case "RollingAverage":
        //                        colorToUse = avgColor;
        //                        break;
        //                }
        //                seriesList.Add(BuildChartPageSeries(dataSet.Tables["DataTable1"], dataSet.Tables["DataTable1"], "PeriodMonth", col.ColumnName,
        //                seriesNum, col.ColumnName, other3SeriesTypes, GetColor(colorToUse), ref legend));
        //                seriesNum++;
        //            }
        //        }

        //        ChartInfo info = new ChartInfo();
        //        info.SeriesList = seriesList;
        //        chart = new Chart();
        //        chart = builder.BuildChart(info, dataSet, ref chart);
        //        return chart;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public Chart GetCommonChartWithTheseFields(List<string> field, string refineryID, string periodStart,
            string periodEnd, string ChartTitle, string currency, ref string factorSet,
            string uom, string scenario,
            string chartInfoRequested, string chartType,
            string chartColor1, string avgColor, string ytdColor, string targetColor,
            string ytdField, string targetField, string aveField, string tableName = "GENSUM")
        {
            Chart chart = null;
            int seriesColorCount = 0;
            try
            {
                DBHelper dbHelper = new DBHelper(_connectionString);
                ChartsDataset dataSet = new ChartsDataset();
                //string sql = SqlHelper.BuildDataTable1Sql(refineryID,periodStart,periodEnd,ChartTitle,currency,factorSet,uom,scenario);
                dbHelper.table = string.Empty;
                dbtable = dbHelper.table;
                factorSet = dbHelper.GetFactorSet(refineryID);
                ChartsDataset.DataTable1DataTable dt1 = dbHelper.GetDatatable1ColumnDataWithTheseFields(
                    field[0], refineryID, periodStart, periodEnd, ChartTitle, currency, factorSet, uom, scenario,
                    tableName, ytdField, targetField, aveField);
                dataSet.Tables.Remove(dataSet.DataTable1);
                dt1.TableName = "DataTable1";
                dataSet.Tables.Add(dt1);
                if (dt1.Rows.Count > 0)
                {
                    //how will I know if there is another plant being requested???
                    //first plant is field1 in conx string; other plants must be field2-field5
                    //this will get the Plant info tables
                    for (int i = 0; i < field.Count; i++)
                    {
                        if (field[i].Length > 0)
                        {
                            DataTable dtPlant = null;
                            if (tableName.ToUpper() == "GENSUM")
                            {
                                dtPlant = dbHelper.GetPlantColumnData(field[i], refineryID, periodStart, periodEnd, ChartTitle, currency, uom, scenario);
                            }
                            else if (tableName.ToUpper() == "MAINTAVAILCALC")
                            {
                                dtPlant = dbHelper.GetMechAvailColumnData(field[i], refineryID, periodStart, periodEnd, ChartTitle, currency, uom, scenario);
                            }
                            else if (tableName == "MAINTINDEX")
                            {
                                dtPlant = dbHelper.GetMaintIndexColumnData(field[i], refineryID, periodStart, periodEnd, ChartTitle, currency, uom, scenario);
                            }
                            dtPlant.TableName = "PlantInfo" + i.ToString();
                            dataSet.Tables.Add(dtPlant);
                        }
                    }
                    dbtable = dbHelper.table;
                    //dbHelper.GetData(_connectionString, "");
                    int seriesNum = 0;
                    SeriesChartType seriesChartType = GetChartType(chartType);

                    ChartBuilder builder = new ChartBuilder();
                    ChartPageLegend legend = new ChartPageLegend();
                    legend.Alignment = System.Drawing.StringAlignment.Center;
                    legend.DockingPosition = Docking.Bottom;

                    List<ChartPageSeries> seriesList = new List<ChartPageSeries>();
                    List<Color> usedColors = new List<Color>();
                    //always do this one first. other ones later.
                    for (int i = 0; i < dataSet.Tables.Count; i++) //start at 1, table 0 will be handled later
                    {
                        foreach (DataTable table in dataSet.Tables)
                        {
                            if (table.TableName == "PlantInfo" + i.ToString())
                            {
                                seriesList.Add(BuildChartPageSeries(table, table, "PeriodDisplay", "CurrentMonth", seriesNum, table.Rows[0]["Description"].ToString(),
                                    GetChartType(chartType), GetNextSeriesColor(GetColor(chartColor1), ref usedColors), ref legend));
                                //GetChartType(chartType), GetColor(chartColor), ref legend));
                                seriesNum++;

                                //if (i == 0) chartColor = "Purple";
                                //chartColor = GetNextRadarColor(i + 2).ToString();
                                break;
                            }
                        }
                    }

                    //YTD, AVG, Targets last.
                    //It looks like these are always going to be lines....
                    SeriesChartType other3SeriesTypes = SeriesChartType.Line; //GetOther3ChartType(chartType);// 
                    foreach (DataColumn col in dataSet.Tables["DataTable1"].Columns)
                    {
                        if ((col.ColumnName == "YearToDate" && ytdColor.Length > 0)
                            || (col.ColumnName == "Target" && targetColor.Length > 0) ||
                            (col.ColumnName == "RollingAverage" && avgColor.Length > 0))
                        {
                            string colorToUse = string.Empty;
                            switch (col.ColumnName)
                            {
                                case "YearToDate":
                                    colorToUse = ytdColor;
                                    break;
                                case "Target":
                                    colorToUse = targetColor;
                                    break;
                                case "RollingAverage":
                                    colorToUse = avgColor;
                                    break;
                            }
                            seriesList.Add(BuildChartPageSeries(dataSet.Tables["DataTable1"], dataSet.Tables["DataTable1"], "PeriodMonth", col.ColumnName,
                            seriesNum, col.ColumnName, other3SeriesTypes, GetColor(colorToUse), ref legend));
                            seriesNum++;
                        }
                    }

                    ChartInfo info = new ChartInfo();
                    info.SeriesList = seriesList;
                    chart = new Chart();
                    chart = builder.BuildChart(info, dataSet, ref chart);
                    return chart;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private ChartPageSeries BuildChartPageSeries(DataTable xColDataTable, DataTable yColDataTable, string xColName, string yColName,
            int seriesNumber, string seriesName, SeriesChartType chartType, Color seriesColor, ref ChartPageLegend legend)
        {
            //yColName = yColName.Replace("_x0020_", " ");
            ChartPageSeriesPoints points2 = new ChartPageSeriesPoints();
            if (chartType == SeriesChartType.Line)
            {
                points2.Shape = MarkerStyle.Diamond;
                points2.Color = System.Drawing.Color.Black;
                points2.Size = 1;
            }
            ChartPageSeries second = new ChartPageSeries();
            second.XColumnDataTable = xColDataTable;
            second.YColumnDataTable = yColDataTable;
            second.SeriesNumber = seriesNumber;
            second.Legend = legend;
            second.ChartType = chartType;
            second.XColumnNumber = GetColumnNumber(xColDataTable, xColName);
            //second.YColumnNumber = GetColumnNumber(yColDataTable, yColName);
            second.YColumnName = yColName;
            second.Color = seriesColor;
            second.XAxisAngle = 0;
            second.NameToUse = seriesName;
            second.Points = points2;
            return second;
        }

        private SeriesChartType GetChartType(string chartType)
        {
            switch (chartType.ToUpper())
            {
                case "COLUMN":
                    return SeriesChartType.Column;
                    break;
                case "STACKEDCOLUMN":
                    return SeriesChartType.StackedColumn;
                    break;
                case "STACKEDBAR":
                    return SeriesChartType.StackedBar;
                    break;
                case "LINE":
                    return SeriesChartType.Line;
                    break;
                case "PIE":
                    return SeriesChartType.Pie;
                    break;
                case "POINT":
                    return SeriesChartType.Point;
                    break;
                default: //"POLAR","RADAR":
                    return SeriesChartType.Radar;
                    break;
            }
        }
        private System.Drawing.Color GetColor(string color)
        {
            System.Drawing.ColorConverter z = new System.Drawing.ColorConverter();
            return (System.Drawing.Color)z.ConvertFromString(color);
        }

        private int GetColumnNumber(DataTable dataTable, string columnName)
        {
            int value = -1;
            for (int i = 0; i < dataTable.Columns.Count; i++)   //(DataColumn col in dataTable.Columns)
            {
                if (dataTable.Columns[i].ColumnName.ToUpper() == columnName.ToUpper())
                {
                    value = i;
                    break;
                }
            }
            return value;
        }


        public Chart GetRadarChart(string refineryID, string periodStart,
            string periodEnd, string ChartTitle, string currency,//, string factorSet,
            string uom, string scenario, //string chartInfoRequested, string chartType,
            string chartColor, string avgColor, string ytdColor, string monthColor,
            List<int> peerGroups, List<int> rankVariables, ref string warnings)
        {
            Chart radarChart = null;
            List<int> foundKpis = new List<int>();
            try
            {
                //DBHelper localdbDbHelper = new DBHelper(ConfigurationManager.ConnectionStrings["Dbs1ConnectionString"].ToString());
                DBHelper dbHelper = new DBHelper(_connectionString);
                ChartsDataset dataSet = new ChartsDataset();// dbHelper.TestGetData("");
                dbHelper.table = string.Empty;
                dbtable = dbHelper.table;
                int rowCount = dataSet.Tables["DataTable1"].Rows.Count;
                string breakValue = string.Empty;
                DataTable dt1 = dbHelper.GetDatatable1QuartileData(peerGroups[0], ref  rankVariables, refineryID, Convert.ToDateTime(periodStart), scenario, currency, uom);
                foundKpis = rankVariables;
                dbtable = dbHelper.table;
                dt1.TableName = "DataTable1";
                dataSet.Tables["DataTable1"].Clear();
                dataSet.Tables["DataTable1"].Merge(dt1);
                int seriesNum = 0;
                SeriesChartType seriesChartType = GetChartType("RADAR");
                try
                {
                    dt1.WriteXml(@"C:\dt1.xml");
                }
                catch { }
                ChartBuilder builder = new ChartBuilder();

                RadarChartPageLegend legend = new RadarChartPageLegend();
                legend.Alignment = System.Drawing.StringAlignment.Center;
                legend.DockingPosition = Docking.Bottom;

                List<RadarChartPageSeries> seriesList = new List<RadarChartPageSeries>();

                //going BACKWARD thru columns
                //should sort 
                //should only be 1 row for each of 9 quartile columns (columns 10-12 are for Mo,Ytd,Avg, not used yet)
                //the sries are qtop2, q1to2, etc. So loop thro each row and get same colum, add to series.
                //then loop thru all rows and get next column.

                //if (Environment.MachineName.ToString() == "WKLTDAL0058")
                //{
                //    dataSet.Tables["DataTable1"].WriteXml(@"c:\dataTable4.xml");
                //}
                string nullFieldNames = string.Empty;
                //List<int> nullFieldsVKeys = new List<int>();
                int dataTable1RowsCount = dataSet.Tables["DataTable1"].Rows.Count;
                for (int rowCounter = 0; rowCounter < dataTable1RowsCount; rowCounter++)
                {
                    bool allNulls = true;
                    for (int colCount = 9; colCount > 4; colCount--)
                    {
                        if (dataSet.Tables["DataTable1"].Rows[rowCounter][colCount] != DBNull.Value)
                        {
                            allNulls = false;
                        }
                    }
                    if (allNulls)
                    {
                        nullFieldNames += dataSet.Tables["DataTable1"].Rows[rowCounter]["Description"].ToString() + ", ";



                        //NOTE:  Sort field here is NOT really the Sort. It is vkey, which is used to give each record an ID because
                        //      it might need to be deleted later, and will need the remove that that kpi 
                        //      from the list of kpis passed in.
                        //      When get the ytd/ave/mo data, THEN will change the Sort to be the real Sort
                        //nullFieldsVKeys.Add(Convert.ToInt32(dataSet.Tables["DataTable1"].Rows[rowCounter]["Sort"]));



                        dataSet.Tables["DataTable1"].Rows[rowCounter].Delete();
                        dataTable1RowsCount -= 1;
                    }
                }
                /*
                foreach (int vkey in nullFieldsVKeys)
                {
                    for (int rankVariablesCounter = 0; rankVariablesCounter < rankVariables.Count; rankVariablesCounter++)
                    {
                        if(Convert.ToInt32(rankVariables[rankVariablesCounter])==vkey)
                        {
                            rankVariables.RemoveAt(rankVariablesCounter);
                        }
                    }
                }
                */
                /*
                //clean up the reuse of the Sort field which was temporarily holding vkeys instead of sorts
                for (int rowCounter = 0; rowCounter < dataTable1RowsCount; rowCounter++)
                {
                    dataSet.Tables["DataTable1"].Rows[rowCounter]["Sort"] = DBNull.Value;
                }
                */
                //get kpi names from chart_lu based on vkeys abive
                //List<string> kpiFields = dbHelper.GetKpiFieldsFromChartLU(rankVariables);


                if (dataSet.Tables["DataTable1"].Rows.Count < 3)
                {
                    string errMsg = "Data error: No data was found for " + nullFieldNames + " causing the chart to have less than 3 KPIs.";
                    Chart err = new Chart();
                    err.Titles.Add(errMsg);
                    return err;

                    //return new Chart(     "<HTML>no data</html>";
                    //throw new Exception("Data error: No data was found for " + nullFields + " causing the chart to have less than 3 KPIs.");
                }
                /*
                try
                {                    
                    dataSet.Tables["DataTable1"].WriteXml(@"C:\DataTable1.xml");
                }
                catch { }
                */
                for (int colCount = 9; colCount > 4; colCount--)
                {
                    //first, check to see if has data; if null then don't add to seriesList-might only be tercile or halftile
                    string colName = dataSet.Tables["DataTable1"].Columns[colCount].ColumnName;
                    bool allNull = true;
                    foreach (DataRow row in dataSet.Tables["DataTable1"].Rows)
                    {
                        //string test = row[colName].ToString();
                        if (row[colName] != DBNull.Value)
                        {
                            allNull = false;
                            break;
                        }
                    }

                    if (!allNull)
                    {
                        seriesList.Add(BuildRadarChartPageSeries(dataSet.Tables["DataTable1"], dataSet.Tables["DataTable1"],
                            dataSet.Tables["DataTable1"].Columns[4].ColumnName, dataSet.Tables["DataTable1"].Columns[colCount].ColumnName,
                            seriesNum, dataSet.Tables["DataTable1"].Columns[colCount].ColumnName,
                            GetChartType("RADAR"), GetNextRadarColor(colCount), ref legend));
                        //first, check to see if has data; if null then don't add to seriesList
                        seriesNum++;
                    }

                }

                List<List<double>> lists = new List<List<double>>();
                foreach (RadarChartPageSeries series in seriesList)
                {
                    List<double> y = PointsToBind(series.YColumnName, series.YColumnDataTable);
                    if (y.Count > 0)
                    {
                        lists.Add(y);
                        series.FinalYValues = new List<double>();
                    }
                }
                //re-sort high to low so can see all the radar chart bands; low #s will cover up higher #s if not do this

                lists = builder.HighToLow(lists);// avgTgtYtdSeriesCount);

                //check to see if MoAveYtd requested and what colors
                int avgTgtYtdSeriesCount = 0;
                if (avgColor.Length > 0 || ytdColor.Length > 0 || monthColor.Length > 0)
                {
                    //YTD, AVG, MO last.
                    //skip target, the quartiles are the target per RIchard.
                    //It looks like these are always going to be lines....
                    for (int colCount = 10; colCount < 14; colCount++)
                    {
                        bool dontAdd = true;
                        string colorToUse = string.Empty;
                        switch (colCount)
                        {
                            case 10: // "Ytd":
                                if (ytdColor.Length > 0)
                                {
                                    colorToUse = ytdColor;
                                    dontAdd = false;
                                }
                                break;
                            case 11: // "Avg":
                                if (avgColor.Length > 0)
                                {
                                    colorToUse = avgColor;
                                    dontAdd = false;
                                }

                                break;
                            case 12: // "Target":
                                //skip target, the quartiles are the target per RIchard.
                                break;
                            case 13: // "Month":
                                if (monthColor.Length > 0)
                                {
                                    colorToUse = monthColor;
                                    dontAdd = false;
                                }

                                break;
                        }
                        if (!dontAdd)
                        {
                            RadarChartPageSeries series = BuildRadarChartPageSeries(dataSet.Tables["DataTable1"], dataSet.Tables["DataTable1"]
                                , "PeriodDisplay", dataSet.Tables["DataTable1"].Columns[colCount].ColumnName,
                            seriesNum, dataSet.Tables["DataTable1"].Columns[colCount].ColumnName,
                            SeriesChartType.Line, GetColor(colorToUse), ref legend);

                            List<double> y = PointsToBind(series.YColumnName, series.YColumnDataTable);
                            if (y.Count > 0)
                            {
                                lists.Add(y);
                                series.FinalYValues = new List<double>();
                            }

                            seriesList.Add(series);

                            seriesNum++;
                            avgTgtYtdSeriesCount++;
                        }
                        //}
                    }
                }

                if (avgTgtYtdSeriesCount > 0)
                {
                    bool foundData = false;
                    foreach (DataRow row in dataSet.Tables["DataTable1"].Rows)
                    {
                        if(row["Ytd"]!=DBNull.Value)
                        { foundData = true; }
                        if (row["Avg"] != DBNull.Value)
                        { foundData = true; }
                        if (row["Month"] != DBNull.Value)
                        { foundData = true; }
                    }
                    if (!foundData)
                        warnings += "No data found for Ytd, Month, or Study Equivalent. Preliminary Study Pricing setting might be incorrect." + Environment.NewLine;
                }





                //re-sort high to low so can see all the radar chart bands; low #s will cover up higher #s if not do this
                
                // move to above so only handle quartile data: lists = builder.HighToLow(lists, avgTgtYtdSeriesCount);
                /*
                List<Multiplier> mult = new List<Multiplier>();
                List<List<double>> listsFinal = builder.ZeroTo100a(lists, ref mult);
                */
                         
                
                
                       
                List<List<double>> listsFinal = new List<List<double>>();
                /*
                foreach (List<double> lst in lists)
                {
                    listsFinal.Add(builder.QuartileAvgMoYtdZeroTo100(lst));
                }
                */
                int listsLength = 0;
                foreach (List<double> lst in lists)
                {
                    //add blank list to listsFinal
                    listsFinal.Add(new List<double>());
                    //calc max length of any list in lists
                    if (lst.Count > listsLength)
                        listsLength = lst.Count;
                }
                for (int column = 0; column < listsLength; column++)
                {
                    //need to pivot here, new list of all the [0] 
                    List<double> temp = new List<double>();
                    foreach (List<double> lst in lists)
                    {
                        temp.Add(lst[column]);
                    }
                    //pass that in to QuartileAvgMoYtdZeroTo100,
                    //add to each new out list,
                    List<double> zeroTo100 = builder.QuartileAvgMoYtdZeroTo100(temp);
                    //unpivot; add each new outlist to listsFinal.
                    int listCount = 0;
                    foreach (List<double> lstFinal in listsFinal)
                    {
                        lstFinal.Add(zeroTo100[listCount]);
                        listCount++;
                    }
                }
                




                //if (Environment.MachineName.ToString() == "WKLTDAL0058")
                //{
                //    PrintLists("After ZeroTo100a", listsFinal);
                //}
                //need to change the Mo, ytd ave ???  or do it in ZeroTo100A ???

                int listsCount = 0;
                foreach (RadarChartPageSeries series in seriesList)
                {
                    if (series.FinalYValues != null)
                    {
                        //then was used in var lists above
                        series.FinalYValues = listsFinal[listsCount];
                        listsCount++;
                    }
                }

                RadarChartInfo info = new RadarChartInfo();
                info.SeriesList = seriesList;
                radarChart = new Chart();
                builder.BuildRadarChart(info, dataSet, ref radarChart);
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                throw new Exception("Database error (System.Data.SqlClient.SqlException): " + sqlEx.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return radarChart;
        }

        private void PrintLists1(string msg, List<List<double>> lsts)
        {
            try
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(@"C:\lists.txt", true))
                {
                    writer.WriteLine(msg);
                    foreach (List<double> lst in lsts)
                    {
                        string line = string.Empty;
                        foreach(Double d in lst)
                        {
                            line +=Math.Round(d,0).ToString() + "  ";
                        }
                        
                        writer.WriteLine(line);
                    }
                }
            } catch
            { }
        }


        private RadarChartPageSeries BuildRadarChartPageSeries(DataTable xColDataTable, DataTable yColDataTable,
            string xColName, string yColName,
            int seriesNumber, string seriesName, SeriesChartType chartType, Color seriesColor, ref RadarChartPageLegend legend)
        {
            //yColName = yColName.Replace("_x0020_", " ");
            RadarChartPageSeriesPoints points2 = new RadarChartPageSeriesPoints();

            //if(yColName=="Ytd" || yColName=="Avg" || yColName=="Target" )
            //{
            //    points2.Shape = MarkerStyle.Diamond;
            //    points2.Color = seriesColor; // System.Drawing.Color.Black;
            //    points2.Size = 5;
            //}


            RadarChartPageSeries second = new RadarChartPageSeries();
            second.XColumnDataTable = xColDataTable;
            second.YColumnDataTable = yColDataTable;
            second.SeriesNumber = seriesNumber;
            second.Legend = legend;
            second.ChartType = SeriesChartType.Radar;
            second.XColumnNumber = GetColumnNumber(xColDataTable, xColName);
            //second.YColumnNumber = GetColumnNumber(yColDataTable, yColName);
            second.YColumnName = yColName;
            second.Color = seriesColor;
            second.XAxisAngle = 0;
            second.NameToUse = seriesName;
            second.Points = points2;


            if (chartType == SeriesChartType.Line)
            {
                second.SeriesRadarDrawingStyle = "Line";
                second.SeriesRadarBorderDashStyle = System.Web.UI.DataVisualization.Charting.ChartDashStyle.Dash;
            }
            else
            {
                second.SeriesRadarDrawingStyle = "Area";
            }

            //.SeriesRadarBorderDashStyle = ChartDashStyle.Solid;
            second.SeriesAreaDrawingStyle = "Polygon";
            second.SeriesCircularLabelsStyle = "Horizontal";

            return second;
            //the chart is built in BuildRadarChart()
        }

        private Color GetNextRadarColor(int col)
        {
            switch (col)
            {
                case 5:
                    return Color.Red;
                case 6:
                    return Color.Orange;
                case 7:
                    return Color.Yellow;
                case 8:
                    return Color.Green;
                default:
                    return Color.Brown;
            }
        }
        private static Color GetNextSeriesColor(Color firstChartColor, ref List<Color> usedColors)
        {
            Color colorOut = Color.Transparent;
            if (usedColors.Count == 0)
            {
                colorOut = firstChartColor;
                usedColors.Add(colorOut);
                return colorOut;
            }
            int count = 0;
            while (colorOut == Color.Transparent)
            {
                switch (count)
                {
                    case 0:
                        colorOut = Color.Red;
                        break;
                    case 1:
                        colorOut = Color.Orange;
                        break;
                    case 2:
                        colorOut = Color.Yellow;
                        break;
                    case 3:
                        colorOut = Color.Green;
                        break;
                    case 4:
                        colorOut = Color.Brown;
                        break;
                    case 5:
                        colorOut = Color.Violet;
                        break;
                    default:
                        colorOut = Color.Gray;
                        break;
                }
                foreach (Color c in usedColors)
                {
                    if (colorOut == c)
                    {
                        colorOut = Color.Transparent;
                    }
                }
                count++;
            }
            usedColors.Add(colorOut);
            return colorOut;
        }

        internal List<double> PointsToBind(string columnName, System.Data.DataTable dataTable) //e, int tableNumberToUse = 0)
        {
            List<double> result = new List<double>();
            //if(Environment.MachineName.ToString() == "WKLTDAL0058")
            //{
            //    dataTable.WriteXml(@"C:\dataTable9.xml");
            //}
            for (int counter = 0; counter < dataTable.Rows.Count; counter++)
            {
                //var colCount = dataTable.Columns.Count;                
                //var colName = dataTable.Columns[columnName].ColumnName;
                //var test = dataTable.Rows[counter][columnName].ToString();
                if (dataTable.Rows[counter][columnName] != DBNull.Value)
                {
                    result.Add((double)dataTable.Rows[counter][columnName]);
                }
            }
            return result;
        }

        public List<string> PointsToBindString(int columnPosition, System.Data.DataTable dataTable) //, int tableNumberToUse = 0)
        {
            List<string> result = new List<string>();
            for (int counter = 0; counter < dataTable.Rows.Count; counter++)
            {
                result.Add((string)dataTable.Rows[counter][columnPosition]);
            }
            return result;
        }

        public DataTable GetUnitsList(string refineryId, string periodStart, string periodEnd)
        {
            DBHelper dbHelper = new DBHelper(_connectionString);
            return dbHelper.GetUnitsList(refineryId, periodStart, periodEnd);
        }

        public Chart GetUnitsCommonChart(List<string> field, string refineryID, string periodStart,
            string periodEnd, string ChartTitle, string currency, string factorSet,
            string uom, string scenario,
            string chartInfoRequested, string chartType,
            string chartColor1, string avgColor, string ytdColor, string targetColor, string unitId)
        {
            Chart chart = null;
            int seriesColorCount = 0;
            //int dataTable1FieldCount = 0;
            try
            {
                DBHelper dbHelper = new DBHelper(_connectionString);
                ChartsDataset dataSet = new ChartsDataset();
                //string sql = SqlHelper.BuildDataTable1Sql(refineryID,periodStart,periodEnd,ChartTitle,currency,factorSet,uom,scenario);
                dbHelper.table = string.Empty;
                dbtable = dbHelper.table;

                //expecting fields to be list of 3 column names to use for Target, then ytd, then avg.
                //convert from "ISNULL(RoutCostCap,0) AS 'UnitName'"  to jsut 'routCostCap"
                string dataTable1Field = string.Empty;
                List<string> dataTableFields = new List<string>();
                foreach (string item in field)
                {
                    if (item.Length > 0)
                    {
                        try
                        {

                            dataTable1Field = item;
                            dataTable1Field = dataTable1Field.Substring(0, dataTable1Field.IndexOf(" AS "));
                            dataTable1Field = dataTable1Field.Replace(" ", "");
                            dataTable1Field = dataTable1Field.Replace("ISNULL(", "");
                            dataTable1Field = dataTable1Field.Split(',')[0];
                            //switch (dataTable1FieldCount)
                            //{
                            //    case 0:
                            dataTable1Field = dataTable1Field + "_Target";
                            //        break;
                            //    case 1:
                            //        dataTable1Field = "Ann" + dataTable1Field;
                            //        break;
                            //    default:
                            //        //expect not to use this
                            //        break;
                            //}
                            //dataTable1FieldCount++;
                        }
                        catch
                        {
                            dataTable1Field = string.Empty;
                        }
                        dataTableFields.Add(dataTable1Field);
                    }
                }

                ChartsDataset.DataTable1DataTable dt1 = dbHelper.BuildDataTable1Units(dataTableFields, refineryID, periodStart, periodEnd, ChartTitle, currency, unitId);
                dataSet.Tables.Remove(dataSet.DataTable1);
                dt1.TableName = "DataTable1";
                dataSet.Tables.Add(dt1);

                //how will I know if there is another plant being requested???
                //first plant is field1 in conx string; other plants must be field2-field5
                //this will get the Plant info tables
                for (int i = 0; i < field.Count; i++)
                {
                    if (field[i].Length > 0)
                    {


                        //expecting field1 to be Desc and ^ and CurrentMonth column names
                        string currentMonthFields = "";
                        ChartsDataset.PlantsInfoTemplateDataTable dtPlant = dbHelper.GetUnitCurrentMonths(field[i], refineryID, periodStart, periodEnd, ChartTitle, currency, unitId);


                        dtPlant.TableName = "PlantInfo" + i.ToString();
                        dataSet.Tables.Add(dtPlant);
                    }
                }
                dbtable = dbHelper.table;
                //dbHelper.GetData(_connectionString, "");
                int seriesNum = 0;
                SeriesChartType seriesChartType = GetChartType(chartType);

                ChartBuilder builder = new ChartBuilder();
                ChartPageLegend legend = new ChartPageLegend();
                legend.Alignment = System.Drawing.StringAlignment.Center;
                legend.DockingPosition = Docking.Bottom;
                List<Color> usedColors = new List<Color>();
                List<ChartPageSeries> seriesList = new List<ChartPageSeries>();

                //always do this one first. other ones later.
                for (int i = 0; i < dataSet.Tables.Count; i++) //start at 1, table 0 will be handled later
                {
                    foreach (DataTable table in dataSet.Tables)
                    {
                        if (table.TableName == "PlantInfo" + i.ToString())
                        {
                            seriesList.Add(BuildChartPageSeries(table, table, "PeriodDisplay", "CurrentMonth", seriesNum, table.Rows[0]["Description"].ToString(),
                                GetChartType(chartType), GetNextSeriesColor(GetColor(chartColor1), ref usedColors), ref legend));
                            seriesNum++;
                            break;
                        }
                    }
                }

                //YTD, AVG, Targets last.
                //It looks like these are always going to be lines....
                SeriesChartType other3SeriesTypes = SeriesChartType.Line; //GetOther3ChartType(chartType);// 
                foreach (DataColumn col in dataSet.Tables["DataTable1"].Columns)
                {
                    if ((col.ColumnName == "YearToDate" && ytdColor.Length > 0)
                        || (col.ColumnName == "Target" && targetColor.Length > 0) ||
                        (col.ColumnName == "RollingAverage" && avgColor.Length > 0))
                    {
                        string colorToUse = string.Empty;
                        switch (col.ColumnName)
                        {
                            case "YearToDate":
                                colorToUse = ytdColor;
                                break;
                            case "Target":
                                colorToUse = targetColor;
                                break;
                            case "RollingAverage":
                                colorToUse = avgColor;
                                break;
                        }
                        seriesList.Add(BuildChartPageSeries(dataSet.Tables["DataTable1"], dataSet.Tables["DataTable1"], "PeriodMonth", col.ColumnName,
                        seriesNum, col.ColumnName, other3SeriesTypes, GetColor(colorToUse), ref legend));
                        seriesNum++;
                    }
                }

                ChartInfo info = new ChartInfo();
                info.SeriesList = seriesList;
                chart = new Chart();
                chart = builder.BuildChart(info, dataSet, ref chart);
                return chart;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

    public class RadarChartInfo
    {
        public List<RadarChartPageSeries> SeriesList { get; set; }
    }

    public class RadarChartPageSeries
    {
        public string SeriesRadarDrawingStyle { get; set; }
        public string SeriesAreaDrawingStyle { get; set; }
        public string SeriesCircularLabelsStyle { get; set; }
        public ChartDashStyle SeriesRadarBorderDashStyle { get; set; }

        //from ChartPageSeries:
        public int SeriesNumber { get; set; }
        public DataTable XColumnDataTable { get; set; }
        public DataTable YColumnDataTable { get; set; }
        public RadarChartPageLegend Legend { get; set; }
        public SeriesChartType ChartType { get; set; }
        public int XColumnNumber { get; set; }
        //public int YColumnNumber { get; set; }
        public string YColumnName { get; set; }
        public System.Drawing.Color Color { get; set; }
        public int XAxisAngle { get; set; }
        public string NameToUse { get; set; }
        public RadarChartPageSeriesPoints Points { get; set; }
        public List<double> FinalYValues { get; set; }
    }

    public class RadarChartPageSeriesPoints
    {
        public MarkerStyle Shape { get; set; }
        public System.Drawing.Color Color { get; set; }
        public int Size { get; set; }
    }
    public class RadarChartPageLegend
    {
        public Docking DockingPosition { get; set; }
        public System.Drawing.StringAlignment Alignment { get; set; }
    }

    internal class Multiplier
    {
        public double LowValue = 0;
        public double Spread = 0;
    }
}
