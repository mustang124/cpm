﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ServiceModel;

namespace Solomon.ProfileII.Contracts
{
    [ServiceContract]
    public interface IProfileDataService
    {
        //Public Class DataServices
        
        /*  Can't have two CheckService methods in WCF Service.svc
        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        string CheckService();
        */

        //[OperationContract]
        //[FaultContract(typeof(ApplicationException))]
        //Boolean RefineryIs2012(string RefineryID);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        DataSet GetLookups(string companyID,  byte[] cert);


        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        DataSet GetDataDump(string ReportCode, string ds, string scenario, string currency,
                                 DateTime startDate, string UOM, int studyYear, bool includeTarget,
                                 bool includeYTD, bool includeAVG,  byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        DataSet GetReferences( byte[] cert);


        //Public Class FileTransfer

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        string CheckService( byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        byte[] DownloadTplFile(string fileinfo, string appVersion,  byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        string UploadFile(string fileName, byte[] bytes ,  byte[] cert);

        //Public Class SubmitServices

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        DataSet GetDataByPeriod(DateTime periodStart, DateTime periodEnd, byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        DataSet GetInputData( byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        void SubmitRefineryData(DataSet ds , byte[] cert);

        [OperationContract]
        [FaultContract(typeof(ApplicationException))]
        void WriteActivityLogExtended(string applicationName, string methodology,  string CallerIP,
           string UserID, string ComputerName, string service,
           string Method, string EntityName, DateTime? PeriodStart, DateTime? PeriodEnd,
           string status,
           string version, string errorMessages, string localDomainName,
           string osVersion, string browserVersion, string officeVersion,
           string dataImportedFromBridgeFile, byte[] cert);


    }
}
