﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

//[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Solomon.ProfileII.Logger
{
    public static class ProfileLogManager
    {
        //leveraged from https://www.codeproject.com/Tips/1107824/Perfect-Log-Net-with-Csharp
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void LogException(Exception exception)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                GetException(sb, exception);
                Log.Error(sb.ToString());
                //log.Logger.Repository.Shutdown();
                //foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
                //{
                //    app.Close();
                //}
            }
            catch (Exception)
            {
                //do nothing
            }
        }
        private static void GetException(StringBuilder message, Exception exp)
        {
            if (exp == null)
            {
                return;
            }
            message.AppendFormat("Message: {0}\r\nStackTrace\r\n{1}\r\n\r\n", exp.Message, exp.StackTrace);
            GetException(message, exp.InnerException);
        }
        public static void LogInfo(string info)
        {
            try
            {
                Log.Info(info);
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
            }
            //log.Logger.Repository.Shutdown();
            //foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
            //{
            //    app.Close();
            //}
        }
    }
}
