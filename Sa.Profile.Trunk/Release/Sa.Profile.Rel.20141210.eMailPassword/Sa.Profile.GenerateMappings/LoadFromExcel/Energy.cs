﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Extensible
	{
		internal partial class Load
		{
			internal static void Energy(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Energy:               {0}", DateTime.Now.ToString());

				Excel.Worksheet wks = wkb.Worksheets["Energy"];
				Excel.Range rng = null;

				int colEnergyType = 1;
				int colTransType = 2;
				int colTransferTo = 3;
				
				int colRptSource = 5;
				int colRptLocal = 6;
				int colCompositionTotal = 24;

				map.Energy.BeginLoadData();

				for (int r = 10; r <= 84; r++)
				{
					rng = wks.Cells[r, colEnergyType];

					if (Common.RangeHasValue(rng))
					{
						Xsd.MappingsSchema.EnergyRow dr = map.Energy.FindByTransTypeTransferToEnergyType(Common.ReturnString(wks.Cells[r, colTransType]), Common.ReturnString(wks.Cells[r, colTransferTo]), Common.ReturnString(wks.Cells[r, colEnergyType]));

						if (dr != null)
						{
							rng = wks.Cells[r, colRptLocal];
							dr.RptPriceLocal = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colCompositionTotal];

							if (Common.RangeHasValue(rng))
							{
								rng = wks.Cells[r, 8];
								dr.Hydrogen = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 9];
								dr.Methane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 10];
								dr.Ethane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 11];
								dr.Ethylene = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 12];
								dr.Propane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 13];
								dr.Propylene = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 14];
								dr.Butane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 15];
								dr.Isobutane = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.Butylene = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 17];
								dr.C5Plus = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.h2s = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 19];
								dr.CO = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.nh3 = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.so2 = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 22];
								dr.CO2 = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 23];
								dr.N2 = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.Inerts = Common.ReturnAddress(rng);
							}
						}
						else
						{
							Xsd.MappingsSchema.EnergyRow nr = map.Energy.NewEnergyRow();

							nr.EnergyType = Common.ReturnString(rng);

							rng = wks.Cells[r, colTransType];
							nr.TransType = Common.ReturnString(rng);

							rng = wks.Cells[r, colTransferTo];
							nr.TransferTo = Common.ReturnString(rng);

							rng = wks.Cells[r, colRptSource];
							nr.RptSource = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colRptLocal];
							nr.RptPriceLocal = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colCompositionTotal];

							if (Common.RangeHasValue(rng))
							{
								nr.Composition = "1";

								rng = wks.Cells[r, 8];
								nr.Hydrogen = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 9];
								nr.Methane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 10];
								nr.Ethane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 11];
								nr.Ethylene = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 12];
								nr.Propane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 13];
								nr.Propylene = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 14];
								nr.Butane = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 15];
								nr.Isobutane = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.Butylene = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 17];
								nr.C5Plus = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.h2s = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 19];
								nr.CO = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.nh3 = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.so2 = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 22];
								nr.CO2 = Common.ReturnAddress(rng);

								rng = wks.Cells[r, 23];
								nr.N2 = Common.ReturnAddress(rng);

								//rng = wks.Cells[r, 0];
								//dr.Inerts = Common.ReturnAddress(rng);
							}
							else
							{
								nr.Composition = "0";
							}
							map.Energy.AddEnergyRow(nr);

							//  <TransCode></TransCode>
							//  <SortKey></SortKey>
							//  <Header></Header>
							//  <TransTypeDesc></TransTypeDesc>
							//  <EnergyTypeDesc></EnergyTypeDesc>
						}
					}
				}
				map.Energy.AcceptChanges();
				map.Energy.EndLoadData();
			}

			internal static void EnergyElectric(Sa.Xsd.MappingsSchema map, Excel.Workbook wkb)
			{
				System.Console.WriteLine("  Energy (Electric):    {0}", DateTime.Now.ToString());

				Excel.Worksheet wks = wkb.Worksheets["Energy"];
				Excel.Range rng = null;

				int colEnergyType = 1;
				int colTransType = 2;
				int colTransferTo = 3;

				int colRptMWH = 5;
				int colRptLocal = 6;
				int colRptGenEff = 7;

				map.Electric.BeginLoadData();

				for (int r = 90; r <= 97; r++)
				{
					rng = wks.Cells[r, colEnergyType];

					if (Common.RangeHasValue(rng))
					{
						Xsd.MappingsSchema.ElectricRow dr = map.Electric.FindByTransTypeTransferToEnergyType(Common.ReturnString(wks.Cells[r, colTransType]), Common.ReturnString(wks.Cells[r, colTransferTo]), Common.ReturnString(wks.Cells[r, colEnergyType]));

						if (dr != null)
						{
							rng = wks.Cells[r, colRptMWH];
							dr.RptMWH = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colRptLocal];
							dr.PriceLocal = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colRptGenEff];
							dr.RptGenEff = Common.ReturnAddress(rng);
						}
						else
						{
							Xsd.MappingsSchema.ElectricRow nr = map.Electric.NewElectricRow();

							nr.EnergyType = Common.ReturnString(rng);

							rng = wks.Cells[r, colTransType];
							nr.TransType = Common.ReturnString(rng);

							rng = wks.Cells[r, colTransferTo];
							nr.TransferTo = Common.ReturnString(rng);

							rng = wks.Cells[r, colRptMWH];
							nr.RptMWH = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colRptLocal];
							nr.PriceLocal = Common.ReturnAddress(rng);

							rng = wks.Cells[r, colRptGenEff];
							nr.RptGenEff = Common.ReturnAddress(rng);

							map.Electric.AddElectricRow(nr);

							//  <TransCode></TransCode>
							//  <SortKey></SortKey>
							//  <Header></Header>
							//  <TransTypeDesc></TransTypeDesc>
							//  <EnergyTypeDesc></EnergyTypeDesc>
						}
					}
				}
				map.Electric.AcceptChanges();
				map.Electric.EndLoadData();
			}
		}
	}
}