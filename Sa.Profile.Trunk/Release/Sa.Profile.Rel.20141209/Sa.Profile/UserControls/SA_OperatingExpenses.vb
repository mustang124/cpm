Option Explicit On

Friend Class SA_OperatingExpenses
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents gbTools As System.Windows.Forms.GroupBox
    Friend WithEvents tcOpEx As System.Windows.Forms.TabControl
    Friend WithEvents pageTable4 As System.Windows.Forms.TabPage
    Friend WithEvents lblOpEx As System.Windows.Forms.Label
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents rbLinks As System.Windows.Forms.RadioButton
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cbSheets As System.Windows.Forms.ComboBox
    Friend WithEvents tbFilePath As System.Windows.Forms.TextBox
    Friend WithEvents dgvOpEx As System.Windows.Forms.DataGridView
    Friend WithEvents dgvOpExLinks As System.Windows.Forms.DataGridView
    Friend WithEvents tlpStudyLevel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents rButtonStudy As System.Windows.Forms.RadioButton
    Friend WithEvents rButtonProfile As System.Windows.Forms.RadioButton
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents OpExLinksDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents costsRefineryLinks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OpexIDLinks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rbData As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_OperatingExpenses))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnlTools = New System.Windows.Forms.Panel()
        Me.gbTools = New System.Windows.Forms.GroupBox()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.chkComplete = New System.Windows.Forms.CheckBox()
        Me.lblView = New System.Windows.Forms.Label()
        Me.rbLinks = New System.Windows.Forms.RadioButton()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.rbData = New System.Windows.Forms.RadioButton()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbSheets = New System.Windows.Forms.ComboBox()
        Me.tbFilePath = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.tcOpEx = New System.Windows.Forms.TabControl()
        Me.pageTable4 = New System.Windows.Forms.TabPage()
        Me.dgvOpEx = New System.Windows.Forms.DataGridView()
        Me.dgvOpExLinks = New System.Windows.Forms.DataGridView()
        Me.OpExLinksDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.costsRefineryLinks = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OpexIDLinks = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblOpEx = New System.Windows.Forms.Label()
        Me.tlpStudyLevel = New System.Windows.Forms.TableLayoutPanel()
        Me.rButtonStudy = New System.Windows.Forms.RadioButton()
        Me.rButtonProfile = New System.Windows.Forms.RadioButton()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.pnlTools.SuspendLayout()
        Me.gbTools.SuspendLayout()
        Me.tcOpEx.SuspendLayout()
        Me.pageTable4.SuspendLayout()
        CType(Me.dgvOpEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvOpExLinks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpStudyLevel.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTools
        '
        Me.pnlTools.Controls.Add(Me.gbTools)
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(4, 40)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 105)
        Me.pnlTools.TabIndex = 946
        '
        'gbTools
        '
        Me.gbTools.Controls.Add(Me.btnBrowse)
        Me.gbTools.Controls.Add(Me.chkComplete)
        Me.gbTools.Controls.Add(Me.lblView)
        Me.gbTools.Controls.Add(Me.rbLinks)
        Me.gbTools.Controls.Add(Me.Label10)
        Me.gbTools.Controls.Add(Me.rbData)
        Me.gbTools.Controls.Add(Me.Label11)
        Me.gbTools.Controls.Add(Me.cbSheets)
        Me.gbTools.Controls.Add(Me.tbFilePath)
        Me.gbTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTools.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbTools.Location = New System.Drawing.Point(0, 0)
        Me.gbTools.Name = "gbTools"
        Me.gbTools.Size = New System.Drawing.Size(742, 99)
        Me.gbTools.TabIndex = 1
        Me.gbTools.TabStop = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(711, 15)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(21, 21)
        Me.btnBrowse.TabIndex = 968
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for bridge workbook")
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'chkComplete
        '
        Me.chkComplete.AutoSize = True
        Me.chkComplete.BackColor = System.Drawing.Color.Transparent
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkComplete.Location = New System.Drawing.Point(9, 49)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(136, 17)
        Me.chkComplete.TabIndex = 967
        Me.chkComplete.Text = "Check when completed"
        Me.chkComplete.UseVisualStyleBackColor = False
        '
        'lblView
        '
        Me.lblView.AutoSize = True
        Me.lblView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblView.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblView.Location = New System.Drawing.Point(6, 72)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(33, 13)
        Me.lblView.TabIndex = 962
        Me.lblView.Text = "View"
        Me.lblView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbLinks
        '
        Me.rbLinks.AutoSize = True
        Me.rbLinks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLinks.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbLinks.Location = New System.Drawing.Point(92, 70)
        Me.rbLinks.Name = "rbLinks"
        Me.rbLinks.Size = New System.Drawing.Size(53, 17)
        Me.rbLinks.TabIndex = 961
        Me.rbLinks.Text = "LINKS"
        Me.rbLinks.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(389, 45)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 13)
        Me.Label10.TabIndex = 966
        Me.Label10.Text = "Worksheet"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbData
        '
        Me.rbData.AutoSize = True
        Me.rbData.Checked = True
        Me.rbData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbData.Location = New System.Drawing.Point(41, 70)
        Me.rbData.Name = "rbData"
        Me.rbData.Size = New System.Drawing.Size(52, 17)
        Me.rbData.TabIndex = 960
        Me.rbData.TabStop = True
        Me.rbData.Text = "DATA"
        Me.rbData.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(389, 18)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 965
        Me.Label11.Text = "Workbook"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbSheets
        '
        Me.cbSheets.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets.FormattingEnabled = True
        Me.cbSheets.Location = New System.Drawing.Point(464, 42)
        Me.cbSheets.Name = "cbSheets"
        Me.cbSheets.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets.TabIndex = 964
        '
        'tbFilePath
        '
        Me.tbFilePath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFilePath.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbFilePath.Location = New System.Drawing.Point(464, 15)
        Me.tbFilePath.Name = "tbFilePath"
        Me.tbFilePath.ReadOnly = True
        Me.tbFilePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbFilePath.Size = New System.Drawing.Size(241, 21)
        Me.tbFilePath.TabIndex = 963
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon_Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon_Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(500, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 2
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Update input form with linked values")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'tcOpEx
        '
        Me.tcOpEx.Controls.Add(Me.pageTable4)
        Me.tcOpEx.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcOpEx.ItemSize = New System.Drawing.Size(100, 18)
        Me.tcOpEx.Location = New System.Drawing.Point(4, 145)
        Me.tcOpEx.Name = "tcOpEx"
        Me.tcOpEx.SelectedIndex = 0
        Me.tcOpEx.Size = New System.Drawing.Size(742, 451)
        Me.tcOpEx.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcOpEx.TabIndex = 958
        '
        'pageTable4
        '
        Me.pageTable4.Controls.Add(Me.dgvOpEx)
        Me.pageTable4.Controls.Add(Me.dgvOpExLinks)
        Me.pageTable4.Controls.Add(Me.lblOpEx)
        Me.pageTable4.Location = New System.Drawing.Point(4, 22)
        Me.pageTable4.Name = "pageTable4"
        Me.pageTable4.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.pageTable4.Size = New System.Drawing.Size(734, 425)
        Me.pageTable4.TabIndex = 1
        Me.pageTable4.Text = "Table 4"
        Me.pageTable4.UseVisualStyleBackColor = True
        '
        'dgvOpEx
        '
        Me.dgvOpEx.AllowUserToAddRows = False
        Me.dgvOpEx.AllowUserToDeleteRows = False
        Me.dgvOpEx.AllowUserToResizeRows = False
        Me.dgvOpEx.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOpEx.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvOpEx.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOpEx.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOpEx.Location = New System.Drawing.Point(6, 36)
        Me.dgvOpEx.MultiSelect = False
        Me.dgvOpEx.Name = "dgvOpEx"
        Me.dgvOpEx.RowHeadersVisible = False
        Me.dgvOpEx.RowHeadersWidth = 25
        Me.dgvOpEx.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOpEx.Size = New System.Drawing.Size(722, 383)
        Me.dgvOpEx.TabIndex = 1022
        '
        'dgvOpExLinks
        '
        Me.dgvOpExLinks.AllowUserToAddRows = False
        Me.dgvOpExLinks.AllowUserToDeleteRows = False
        Me.dgvOpExLinks.AllowUserToResizeColumns = False
        Me.dgvOpExLinks.AllowUserToResizeRows = False
        Me.dgvOpExLinks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOpExLinks.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvOpExLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOpExLinks.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OpExLinksDescription, Me.costsRefineryLinks, Me.OpexIDLinks})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOpExLinks.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvOpExLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOpExLinks.Location = New System.Drawing.Point(6, 36)
        Me.dgvOpExLinks.MultiSelect = False
        Me.dgvOpExLinks.Name = "dgvOpExLinks"
        Me.dgvOpExLinks.RowHeadersVisible = False
        Me.dgvOpExLinks.RowHeadersWidth = 25
        Me.dgvOpExLinks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOpExLinks.Size = New System.Drawing.Size(722, 383)
        Me.dgvOpExLinks.TabIndex = 1023
        '
        'OpExLinksDescription
        '
        Me.OpExLinksDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.OpExLinksDescription.DataPropertyName = "Description"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.OpExLinksDescription.DefaultCellStyle = DataGridViewCellStyle3
        Me.OpExLinksDescription.HeaderText = ""
        Me.OpExLinksDescription.Name = "OpExLinksDescription"
        Me.OpExLinksDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'costsRefineryLinks
        '
        Me.costsRefineryLinks.DataPropertyName = "costsRefinery"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.MediumBlue
        Me.costsRefineryLinks.DefaultCellStyle = DataGridViewCellStyle4
        Me.costsRefineryLinks.HeaderText = "Refinery Costs Excluding Turnarounds and Energy Cell Reference"
        Me.costsRefineryLinks.Name = "costsRefineryLinks"
        Me.costsRefineryLinks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'OpexIDLinks
        '
        Me.OpexIDLinks.DataPropertyName = "OpexID"
        Me.OpexIDLinks.HeaderText = "OpexID"
        Me.OpexIDLinks.Name = "OpexIDLinks"
        Me.OpexIDLinks.Visible = False
        '
        'lblOpEx
        '
        Me.lblOpEx.BackColor = System.Drawing.Color.Transparent
        Me.lblOpEx.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblOpEx.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpEx.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblOpEx.Location = New System.Drawing.Point(6, 0)
        Me.lblOpEx.Name = "lblOpEx"
        Me.lblOpEx.Size = New System.Drawing.Size(722, 36)
        Me.lblOpEx.TabIndex = 964
        Me.lblOpEx.Text = "Operating Expenses"
        Me.lblOpEx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tlpStudyLevel
        '
        Me.tlpStudyLevel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tlpStudyLevel.BackColor = System.Drawing.Color.Transparent
        Me.tlpStudyLevel.ColumnCount = 2
        Me.tlpStudyLevel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpStudyLevel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tlpStudyLevel.Controls.Add(Me.rButtonStudy, 1, 0)
        Me.tlpStudyLevel.Controls.Add(Me.rButtonProfile, 0, 0)
        Me.tlpStudyLevel.Location = New System.Drawing.Point(546, 142)
        Me.tlpStudyLevel.Name = "tlpStudyLevel"
        Me.tlpStudyLevel.RowCount = 1
        Me.tlpStudyLevel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tlpStudyLevel.Size = New System.Drawing.Size(197, 22)
        Me.tlpStudyLevel.TabIndex = 959
        '
        'rButtonStudy
        '
        Me.rButtonStudy.AutoSize = True
        Me.rButtonStudy.BackColor = System.Drawing.Color.Transparent
        Me.rButtonStudy.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rButtonStudy.Location = New System.Drawing.Point(108, 3)
        Me.rButtonStudy.Name = "rButtonStudy"
        Me.rButtonStudy.Size = New System.Drawing.Size(89, 17)
        Me.rButtonStudy.TabIndex = 954
        Me.rButtonStudy.Text = "STUDY-LEVEL"
        Me.rButtonStudy.UseVisualStyleBackColor = False
        '
        'rButtonProfile
        '
        Me.rButtonProfile.AutoSize = True
        Me.rButtonProfile.BackColor = System.Drawing.Color.Transparent
        Me.rButtonProfile.Checked = True
        Me.rButtonProfile.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rButtonProfile.Location = New System.Drawing.Point(3, 3)
        Me.rButtonProfile.Name = "rButtonProfile"
        Me.rButtonProfile.Size = New System.Drawing.Size(99, 17)
        Me.rButtonProfile.TabIndex = 955
        Me.rButtonProfile.TabStop = True
        Me.rButtonProfile.Text = "PROFILE-LEVEL"
        Me.rButtonProfile.UseVisualStyleBackColor = False
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1033
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(500, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Operating Expenses"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_OperatingExpenses
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tlpStudyLevel)
        Me.Controls.Add(Me.tcOpEx)
        Me.Controls.Add(Me.pnlTools)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.MediumBlue
        Me.Name = "SA_OperatingExpenses"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.pnlTools.ResumeLayout(False)
        Me.gbTools.ResumeLayout(False)
        Me.gbTools.PerformLayout()
        Me.tcOpEx.ResumeLayout(False)
        Me.pageTable4.ResumeLayout(False)
        CType(Me.dgvOpEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvOpExLinks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpStudyLevel.ResumeLayout(False)
        Me.tlpStudyLevel.PerformLayout()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private IsLoaded, HasRights As Boolean
    Private MyParent As Main
    Friend OpexNotSaved As Boolean

    Private dvWB As New DataView
    Private dvOpEx As New DataView
    Private dvOpExLinks As New DataView

    Friend tblFriendOpEx As DataTable

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        If IsLoaded = False Then Exit Sub
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "Operating Expenses")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Operating Expenses'")
        row(0)!Checked = chk.Checked

        btnSave.ForeColor = Color.White
        btnSave.BackColor = Color.Red
        OpexNotSaved = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveOpex()
        modeViewStudyLevel()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click

        If ProfileMsgBox("YN", "CLEAR", "operating expenses") = DialogResult.Yes Then

            If rbData.Checked Then
                For Each r As DataGridViewRow In Me.dgvOpEx.Rows
                    r.Cells("costsRefinery").Value = DBNull.Value
                Next r
            Else
                For Each r As DataGridViewRow In Me.dgvOpExLinks.Rows
                    r.Cells("costsRefinery").Value = DBNull.Value
                Next r
            End If
            ChangesMade(btnSave, chkComplete, OpexNotSaved)
        End If

    End Sub

    Friend Sub SetDataSet()
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.OpexRights

        Dim dvOpEx_LU As New DataView
        dvOpEx_LU.Table = MyParent.MyParent.ds_Ref.Tables("OpEx_LU")

        If MyParent.dsOperatingExpenses.Tables("OpExAll").Rows.Count = 0 Then
            MyParent.dsOperatingExpenses.Tables("OpExAll").Rows.Add()
        End If

        'Get data and pivot it
        tblFriendOpEx = pivotToDataTableOpex(MyParent.dsOperatingExpenses.Tables("OpExAll"))
        tblFriendOpEx.TableName = "tblFriendOpEx"

        Try

            tblFriendOpEx.Columns.Add("Description", System.Type.GetType("System.String"))
            tblFriendOpEx.Columns.Add("SortKey", System.Type.GetType("System.Double"))
            tblFriendOpEx.Columns.Add("Indent", System.Type.GetType("System.Double"))
            tblFriendOpEx.Columns.Add("ParentID", System.Type.GetType("System.String"))
            tblFriendOpEx.Columns.Add("detailStudy", System.Type.GetType("System.String"))
            tblFriendOpEx.Columns.Add("detailProfile", System.Type.GetType("System.String"))

        Catch ex As Exception
        End Try

        ' Add rows to OpEx Data if they do not exist.
        Dim drOpEx() As DataRow

        For Each r As DataRow In dvOpEx_LU.Table.Rows

            drOpEx = tblFriendOpEx.Select("OpexID = '" & r.Item("OpexID").ToString & "'")

            If (drOpEx.GetUpperBound(0) = -1) Then
                Dim newDR As DataRow = tblFriendOpEx.NewRow
                newDR("OpexID") = r.Item("OpexID")
                newDR("Description") = r.Item("Description")
                newDR("SortKey") = r.Item("SortKey")
                newDR("Indent") = r.Item("Indent")
                newDR("ParentID") = r.Item("ParentID")
                newDR("detailStudy") = r.Item("detailStudy")
                newDR("detailProfile") = r.Item("detailProfile")
                tblFriendOpEx.Rows.Add(newDR)
            Else
                drOpEx(0).Item("Description") = r.Item("Description")
                drOpEx(0).Item("SortKey") = r.Item("SortKey")
                drOpEx(0).Item("Indent") = r.Item("Indent")
                drOpEx(0).Item("ParentID") = r.Item("ParentID")
                drOpEx(0).Item("detailStudy") = r.Item("detailStudy")
                drOpEx(0).Item("detailProfile") = r.Item("detailProfile")
            End If

        Next r

        dvOpEx.Table = tblFriendOpEx
        dvOpEx.Sort = "SortKey ASC"
        dgvOpEx.DataSource = dvOpEx

        dvOpExLinks.Table = MyParent.dsMappings.Tables("OpEx")
        dvOpExLinks.Sort = "SortKey ASC"
        dgvOpExLinks.DataSource = dvOpExLinks

        tblFriendOpEx.AcceptChanges()

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Operating Expenses'")
        chkComplete.Checked = CBool(row(0)!Checked)

        'tbFilePath.Text = My.Settings.workbookOpex
        'cbSheets.Text = My.Settings.worksheetOpex

        Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = 'OpEx'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
            cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
        End If

        rButtonProfile.Checked = Not My.Settings.viewDetailOpex
        rButtonStudy.Checked = My.Settings.viewDetailOpex

        IsLoaded = True
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        MyParent.PrintPreviews("Table 4", Nothing)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If OpexNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Operating Expenses")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveOpex()
                Case DialogResult.No
                    tblFriendOpEx.RejectChanges()
                    MyParent.dsMappings.Tables("OpEx").RejectChanges()

                    MyParent.SaveMappings()
                    SaveMade(btnSave, OpexNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub dgvOpEx_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvOpEx.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, OpexNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvOpEx_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOpEx.CellEndEdit
        ChangesMade(btnSave, chkComplete, OpexNotSaved)
    End Sub

    Private Sub dgvOpExLinks_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvOpExLinks.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, OpexNotSaved)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvOpExLinks_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOpExLinks.CellEndEdit
        ChangesMade(btnSave, chkComplete, OpexNotSaved)
    End Sub

    Private Sub SA_OperatingExpenses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.btnSave.TabIndex = 0
        Me.btnClear.TabIndex = 1

        Me.tbFilePath.TabIndex = 2
        Me.btnBrowse.TabIndex = 3
        Me.cbSheets.TabIndex = 4

        Me.dgvOpEx.TabIndex = 6

        Me.btnPreview.TabIndex = 7
        Me.chkComplete.TabIndex = 8
        Me.btnClose.TabIndex = 9

    End Sub

    Private Sub viewLinksData()
        If rbData.Checked Then
            Me.dgvOpEx.BringToFront()
            Me.lblOpEx.Text = "Operating Expenses:   DATA"
        Else
            Me.dgvOpExLinks.BringToFront()
            Me.lblOpEx.Text = "Operating Expenses:   LINKS"
        End If

        modeViewStudyLevel()

        If IsLoaded And HasRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        selectWorkBook(Me.Cursor, tbFilePath, cbSheets, MyParent.MyParent.pnlProgress, dvWB)
        Me.ToolTip1.SetToolTip(Me.tbFilePath, tbFilePath.Text)
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If tbFilePath.Text = String.Empty Then
            MessageBox.Show("Import File not selected. Please select file.")
            Exit Sub
        End If

        If ProfileMsgBox("YN", "IMPORT", "Operating Expenses") = DialogResult.Yes Then

            importOpEx()

            'Set culture. 
            Dim userCulture As New UserCulture()
            Dim currentUserId As String = LoggedInUser

            Dim currentUserCulture As String = Convert.ToString(userCulture.GetCurrentUserCulture(currentUserId))
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(currentUserCulture)
        End If

    End Sub

    Friend Function importOpEx() As Boolean

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")

        Dim dtMapping As DataTable = MyParent.dsMappings.Tables("OpEx")   ' Making import code more generic
        Dim dtTarget As DataTable = tblFriendOpEx                           ' Making import code more generic

        Dim MyExcel As Object = OpenExcel()
        Dim MyWB As Object = OpenWorkbook(MyExcel, tbFilePath.Text)
        Dim MyWS As Object = OpenSheet(MyExcel, MyWB, cbSheets.Text)

        Dim culture As New ProfileEnvironment.Culture

        If MyExcel Is Nothing Then Exit Function
        If MyWB Is Nothing Then Exit Function
        If MyWS Is Nothing Then Exit Function


        Dim dr() As DataRow

        For Each r As DataRow In dtMapping.Rows

            dr = dtTarget.Select("OpexID = '" & r.Item("OpexID").ToString() & "'")

            If (dr.GetUpperBound(0) > -1) Then
                For Each c As DataColumn In dtMapping.Columns
                    Select Case c.ColumnName
                        Case "costsRefinery"
                            Try

                                If r.Item(c.ColumnName).ToString() <> String.Empty AndAlso _
                                    Not IsDBNull(r.Item(c.ColumnName).ToString()) AndAlso _
                                    IsNumeric(MyWS.Range(r.Item(c.ColumnName).ToString()).Value) Then

                                    dr(0).Item(c.ColumnName) = MyWS.Range(r.Item(c.ColumnName).ToString()).Value

                                Else

                                    If dr(0).Item(c.ColumnName).ToString() = String.Empty Then
                                        dr(0).Item(c.ColumnName) = DBNull.Value
                                    End If

                                End If

                            Catch ex As Exception
                                ProfileMsgBox("CRIT", "MAPPING", "Operation Expenses for the " & Trim(r.Item("Description").ToString()) & " item")
                                GoTo CleanupExcel
                            End Try

                    End Select
                Next c
            End If

        Next r

        ChangesMade(btnSave, chkComplete, OpexNotSaved)
        OpexNotSaved = True

        Me.Cursor = System.Windows.Forms.Cursors.Default

CleanupExcel:
        CloseExcel(MyExcel, MyWB, MyWS)
        Return OpexNotSaved


    End Function

    Private Sub modeViewStudyLevel()
        dgViewAggregate(dgvOpEx, rButtonProfile.Checked, rbData.Checked, "OpexID")
        dgViewAggregate(dgvOpExLinks, rButtonProfile.Checked, rbData.Checked, "OpexID")
    End Sub

    Friend Sub SetCurrencyAndUOM()
        Try
            dgvOpEx.Columns("costsRefinery").HeaderText = "Refinery Costs Excluding Turnarounds and Energy (" & MyParent.KCurrency.ToString() & ")"
        Catch ex As Exception
        End Try
    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub dgvOpEx_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvOpEx.BindingContextChanged

        Dim cWidth As Integer = 100

        For Each c As DataGridViewColumn In dgvOpEx.Columns
            Select Case c.DataPropertyName.ToString
                Case "Description" : setColumnFormat(c, "", 0, "Header", False, False, 0, "", GetType(String), DataGridViewAutoSizeColumnMode.Fill, DataGridViewContentAlignment.TopLeft) : Exit Select
                Case "costsRefinery" : setColumnFormat(c, "Refinery Costs Excluding Turnarounds and Energy (" & MyParent.KCurrency.ToString() & ")", 1, "Body", rbData.Checked, True, cWidth, "N1", GetType(Single), DataGridViewAutoSizeColumnMode.None, DataGridViewContentAlignment.TopRight) : Exit Select
                Case Else
                    setColumnFormat(c)
            End Select
        Next c

        modeViewStudyLevel()

    End Sub

    Private Sub dgvOpExLinks_BindingContextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvOpExLinks.BindingContextChanged
        modeViewStudyLevel()
    End Sub

    Private Sub dgvOpEx_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOpEx.CellValidated
        calculateAggregate(Me.dgvOpEx, e.ColumnIndex, e.RowIndex, rButtonStudy.Checked, "OpexID")
    End Sub

    Private Sub rButtonStudy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rButtonStudy.CheckedChanged
        My.Settings.viewDetailOpex = rButtonStudy.Checked
        My.Settings.Save()
        modeViewStudyLevel()
    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "OpEx", cbSheets.Name, cbSheets.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, OpexNotSaved)

        My.Settings.worksheetOpex = cbSheets.Text
        My.Settings.Save()
    End Sub

    Private Sub tbFilePath_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "OpEx", tbFilePath.Name, tbFilePath.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, OpexNotSaved)

        My.Settings.workbookOpex = tbFilePath.Text
        My.Settings.Save()
    End Sub

    Private Sub dgvOpEx_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvOpEx.DataError
        dgvDataError(sender, e)
    End Sub

  
End Class
