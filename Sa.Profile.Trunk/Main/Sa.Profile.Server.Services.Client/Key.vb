Imports System.IO
Public Class Key

    Public Shared Function GetRefineryID() As String



        If Not File.Exists("_CERT\\policy.ct") Then
            Throw New Exception("Key was not found. If you have a valid key, restart your application. Otherwise, please register your application " + _
                                "before you submit or download data.")
        End If

        Dim reader As New StreamReader("_CERT\\policy.ct")
        Dim id As String
        Dim delim As String = "$"
        Dim key As String
        If reader.Peek <> -1 Then
            key = Decrypt(reader.ReadLine)
            Dim tokens() As String = key.Split(delim.ToCharArray)
            id = tokens(tokens.Length - 1)
            Return id
        Else
            Throw New Exception("A key was not found")
        End If

    End Function

End Class
