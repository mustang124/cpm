<%@ Control Language="vb" ClassName="ReportControl" AutoEventWireup="false"  Codebehind="ReportControl.ascx.vb" Inherits="ProfileII_Corporate.ReportControl" %>
<LINK media="print" href="styles/print.css" type="text/css" rel="StyleSheet">
	<style>
	      .small { FONT-SIZE: 11px; FONT-FAMILY: Tahoma }
	      </style>
	<script>
	<!--
				function UpdatePageCounter(direction)
				{
					if (direction=='NEXT') {
						document.getElementById('ltPage').value= parseInt(document.getElementById('ltPage').value) + 1;	    
					}
					
					if (direction=='PREVIOUS') {
						document.getElementById('ltPage').value= parseInt(document.getElementById('ltPage').value) - 1;
					}
					
					var loc = location.toString().substring(0, location.toString().lastIndexOf("&page"));
					var size  = location.toString().substring(0, location.toString().lastIndexOf("&page")).length ;
					
					if (size > 0) {
						document.location= loc+ "&page=" + document.getElementById('ltPage').value;
					}
					else
					{
						document.location=location+ "&page=" + document.getElementById('ltPage').value;
					}
				}	
	//-->		
	</script>
	<input id=totPages type=hidden value="<%= TotalPages %>">
	<table style="Z-INDEX: 100; LEFT: 1px; POSITION: absolute; TOP: 15px" height="91" cellSpacing="0"
		cellPadding="0" width="100%" border="0">
		<tr>
			<td width="314" background="images\ReportBGLeft.gif" bgColor="dimgray">
				<DIV id="LabelName" align="center"><strong> <font color="white">
							<%=ReportName %>
						</font></strong>
					<br>
				</DIV>
				<br>
				<DIV id="Methodology" align="center"><font size="2" color="white">
						<%= IIf ( ReportName="Gross Product Value",Format(CDate(StartDate),"MMMM yyyy"), StudyYear.ToString + " Methodology <br> " + Format(CDate(StartDate),"MMMM yyyy")   )%>
					</font>
				</DIV>
			<td colspan="8" width="42%">
				<div align="right"><IMG height="50" alt="" src="https://webservices.solomononline.com/profileii/images/SA logo RECT.jpg"
						width="300"><br>
				</div>
				<DIV id="LabelMonth" align="right"><font size="2">
						<%=  Date.Today.ToLongDateString()%>
					</font>
				</DIV>
			</td>
		</tr>
	</table>
	<hr style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 105px" width="100%" color="#000000"
		SIZE="1">
	<div style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 120px" width="637">
		<p>
			<script>
			<!--
				if (parseInt(document.getElementById('ltPage').value)!= 1 ) {
					document.writeln("<a href=javascript:UpdatePageCounter('PREVIOUS'); >Previous </a>");
				}
			//-->
			</script>
			&nbsp;&nbsp;
			<script>
			<!--
				if (parseInt(document.getElementById('ltPage').value) < parseInt(document.getElementById('totPages').value)){
					document.writeln("<a href=javascript:UpdatePageCounter('NEXT'); >Next </a>");
				}
		    //-->
			</script>
		</p>
		<P align="center">
			<asp:panel id="Panel2" runat="server" Height="104px" Width="650">
				<asp:literal id="ltContent" runat="server"></asp:literal>
			</asp:panel>
		</P>
	</div>
