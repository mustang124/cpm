Imports System.Data.SqlClient

Module DBHelper

    Public Const CONNECTSTRING As String = "packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
           "1.13"";persist security info=True;initial catalog=ProfileFuels;password=ProfileFu" & _
           "els"

    
    Public Function SelectDistinct(ByVal TableName As String, ByVal SourceTable As DataTable, ByVal FieldName As String, Optional ByVal SortField As String = "") As DataTable
        Dim dt As DataTable = New DataTable(TableName)
        dt.Columns.Add(FieldName, SourceTable.Columns(FieldName).DataType)
        Dim key() As DataColumn = {dt.Columns(0)}
        dt.PrimaryKey = key

        Dim dr As DataRow

        If SortField.Trim.Length = 0 Then
            SortField = FieldName
        End If

        For Each dr In SourceTable.Select("", SortField)
            If Not dt.Rows.Contains(dr(FieldName)) Then
                dt.Rows.Add(New Object() {dr(FieldName)})
            End If
        Next
        Return dt
    End Function

    Public Function SelectDistinct(ByVal sourceTable As DataTable, ByVal sourceColumn As String) As DataTable

        Dim result As DataTable = New DataTable
        Try
            result.Columns.Add(sourceColumn, sourceTable.Columns(sourceColumn).DataType)

            Dim keys() As DataColumn = {result.Columns(0)}
            result.PrimaryKey = keys

            Dim dr As DataRow
            For Each dr In sourceTable.Rows
                If Not result.Rows.Contains(dr(sourceColumn)) Then

                    Dim newRow As DataRow = result.NewRow()
                    newRow(sourceColumn) = dr(sourceColumn)
                    result.Rows.Add(newRow)
                End If
            Next
            Return result
        Catch ex As System.Exception
            Return result
        Finally

        End Try
    End Function

    Public Function ExecuteScalar(ByVal sqlString As String) As Object
        Dim SqlConnection1 As New System.Data.SqlClient.SqlConnection
        Dim command As SqlCommand = New SqlCommand(sqlString, SqlConnection1)
        Dim result As Object
        '
        'SqlConnection1
        '
        SqlConnection1.ConnectionString = CONNECTSTRING
        Try
            SqlConnection1.Open()
            result = command.ExecuteScalar()
        Catch ex As Exception
            Throw ex
        Finally
            SqlConnection1.Close()
        End Try
        Return result
    End Function


    Public Function QueryDb(ByVal sqlString As String, Optional ByVal start As Integer = 0, Optional ByVal max As Integer = 4, Optional ByVal tableName As String = "") As DataSet
        Dim ds As New DataSet
        Dim SqlConnection1 As New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        SqlConnection1.ConnectionString = CONNECTSTRING

        'Reads only the first sql statement. 
        'It's to gaurd against attacks on the data
        Try
            Dim da As SqlDataAdapter = New SqlDataAdapter(sqlString, SqlConnection1)
            If tableName.Length > 0 Then
                da.Fill(ds, start, max, tableName)
            Else
                da.Fill(ds)
            End If

        Catch ex As Exception
            Throw ex
        Finally
            SqlConnection1.Close()
        End Try

        Return ds
    End Function


End Module
