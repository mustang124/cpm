Imports System.Collections.Specialized


Public Class RefineryScorecard
    Inherits ReportBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        BuildReport()
    End Sub

    Public Sub BuildReport()
        Dim strRow, query, UOM, axisFld As String
        Dim i, y, m As Integer
        Dim chartOptions As NameValueCollection = Request.QueryString
        Dim dsChartLU As DataSet
        Dim dsUserDefined As DataSet

        dsChartLU = QueryDb("SELECT *, " + _
                            "CASE(Decplaces)" + _
                            " WHEN 0 then '{0:#,##0}'" + _
                            " WHEN 1 then '{0:#,##0.0}'" + _
                            " WHEN 2 then '{0:N}' " + _
                            " END AS DecFormat " + _
                            " FROM Chart_LU where SortKey < 800 and " + _
                            "SectionHeader <>'By Process Unit' ORDER BY SortKey")

        dsUserDefined = QueryDb("SELECT * FROM UserDefined WHERE SubmissionID" + _
                                " IN (SELECT SubmissionID FROM Submissions " + _
                                " WHERE RefineryID ='" + RefineryID + "' )")

        UOM = Request.QueryString("UOM")

        If UOM.StartsWith("US") Then
            axisFld = "AxisLabelUS"
        Else
            axisFld = "AxisLabelMetric"
        End If

        'Get the number of different tables
        Dim dt As DataTable = SelectDistinct("Chart_LU", dsChartLU.Tables(0), "DataTable")
        For y = 0 To dt.Rows.Count - 1
            Dim row As DataRow = dt.Rows(y)
            Dim drs As DataSet

            Dim stmt As String = "SELECT * FROM " + row("DataTable") + " WHERE SubmissionID IN " + _
                          "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                          "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                          Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "') "


            If row("DataTable").ToUpper = "MAINTAVAILCALC" Then
                stmt += " AND FactorSet='" + Request.QueryString("yr") + "'"
            End If

            If row("DataTable").ToUpper = "GENSUM" Then
                stmt += " AND Currency='" + Request.QueryString("Currency") + "' AND FactorSet='" + Request.QueryString("yr") + "' and UOM='" + Request.QueryString("UOM") + "'"
            End If

            drs = QueryDb(stmt)
            drs.Tables(0).TableName = row("DataTable")
            If Not IsNothing(drs) Then
                dsChartLU.Tables.Add(drs.Tables(0).Copy)
            End If

        Next

        Response.Write("<br>")


        'Print table for each section 
        Dim dtSec As DataTable = SelectDistinct("Chart_LU", dsChartLU.Tables(0), "SectionHeader")
        Dim sectionName As String
        Dim sections() As String = {"Average Data", "Performance Indicators", "Energy", "Maintenance", "Personnel", "Operating Expenses", "Yields and Margins"}
        Response.Write(" <table class='small'  border=0 bordercolor='darkcyan' width=98% cellspacing=0 cellpadding=2>")


        'Build headers
        Response.Write("<td width=53%>&nbsp;</td><td align=center colspan=2 width=70><strong>" + Format(CDate(Request.QueryString("sd")), "MMMM<br>yyyy") + "</strong></td>")
        If Request.QueryString("target") Then
            Response.Write("<td  align=center colspan=2 width=70><strong>Refinery<br>Target</strong></td>")
        Else
            Response.Write("<td width=0 colspan=2>&nbsp;</td>")
        End If
        If Request.QueryString("avg") Then
            Response.Write("<td align=center colspan=2 width=70><strong>Rolling<br>Average</strong></td>")
        Else
            Response.Write("<td width=0 colspan=2>&nbsp;</td>")
        End If
        If Request.QueryString("ytd") Then
            Response.Write("<td align=center colspan=2 width=70><strong>YTD<br>Average</strong></td>")
        Else
            Response.Write("<td width=0 colspan=2>&nbsp;</td>")
        End If


        'Build sections and data
        For m = 0 To sections.Length - 1
            sectionName = sections(m)

            'insert page break before personnel
            If sections(m) = "Personnel" Then
                Response.Write("<P style=""page-break-before: always"">")
            End If
            Response.Write("<tr><td colspan=5><strong>" + sectionName + "</strong></td></tr>")


            Dim sectionRows() As DataRow = dsChartLU.Tables(0).Select("SectionHeader='" + sectionName + "'")

            For i = 0 To sectionRows.Length - 1
                Dim row As DataRow = sectionRows(i)
                Dim fieldName As String = row("TargetField").ToString.Replace("_Target", "")
                Dim chartTitle As String = row("ChartTitle")
                If row("ChartTitle".Trim) <> "EDC" And row("ChartTitle".Trim) <> "UEDC" Then
                    chartTitle += ", " & row(axisFld)
                End If

                chartTitle = chartTitle.Replace("CurrencyCode", Request.QueryString("currency"))

                Response.Write("<tr nowrap><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + chartTitle + "</td>")

                If fieldName.Length > 0 Then
                    If dsChartLU.Tables(row("DataTable")).Rows.Count > 0 Then

                        Dim fieldFormat As String = row("DecFormat").ToString



                        If dsChartLU.Tables(row("DataTable")).Columns.Contains((fieldName)) Then
                            Response.Write("<td align=right width=65>" + String.Format(fieldFormat, dsChartLU.Tables(row("DataTable")).Rows(0)(fieldName)) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                        Else
                            Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                        End If


                        If (CBool(Request.QueryString("target"))) And row("ChartTitle".Trim) <> "EDC" And row("ChartTitle".Trim) <> "UEDC" And _
                            dsChartLU.Tables(row("DataTable")).Columns.Contains((row("TargetField").ToString)) Then
                            Response.Write("<td align=right width=65>" + String.Format(fieldFormat, dsChartLU.Tables(row("DataTable")).Rows(0)(row("TargetField"))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                        Else
                            Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                        End If

                        If (CBool(Request.QueryString("avg"))) And _
                            dsChartLU.Tables(row("DataTable")).Columns.Contains((row("AvgField").ToString)) Then
                            Response.Write("<td align=right width=65>" + String.Format(fieldFormat, dsChartLU.Tables(row("DataTable")).Rows(0)(row("AvgField"))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                        Else
                            Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                        End If

                        If (CBool(Request.QueryString("ytd"))) And _
                            dsChartLU.Tables(row("DataTable")).Columns.Contains((row("YTDField").ToString)) Then
                            Response.Write("<td align=right width=65>" + String.Format(fieldFormat, dsChartLU.Tables(row("DataTable")).Rows(0)(row("YTDField"))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                        Else
                            Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                        End If
                    Else
                        Response.Write("<td>&nbsp</td><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td>")
                    End If

                    Response.Write("</tr>")

                End If
            Next
            Response.Write("<td>&nbsp</td><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td>")

        Next


        'Add HeaderText
        'Print table for each section 
        dtSec = SelectDistinct("UserDefined", dsUserDefined.Tables(0), "HeaderText")
        Dim userSection As String
        Dim secRow, userRow As DataRow

        For Each secRow In dtSec.Rows
            userSection = secRow("HeaderText")
            Response.Write("<tr><td colspan=5><strong>" + userSection + "</strong></td></tr>")
            For Each userRow In dsUserDefined.Tables(0).Select("HeaderText='" + userSection + "'")

                Response.Write("<tr nowrap><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + userRow("VariableDesc") + "</td>")



                Dim fieldFormat As String = userRow("DecPlaces").ToString

                If dsUserDefined.Tables(0).Columns.Contains("RptValue") Then
                    Response.Write("<td align=right width=65>" + String.Format(fieldFormat, userRow("RptValue")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                Else
                    Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                End If


                If (CBool(Request.QueryString("target"))) And _
                    dsUserDefined.Tables(0).Columns.Contains("RptValue_Target") Then
                    Response.Write("<td align=right width=65>" + String.Format(fieldFormat, userRow("RptValue_Target")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                Else
                    Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                End If

                If (CBool(Request.QueryString("avg"))) And _
                    dsUserDefined.Tables(0).Columns.Contains("RptValue_Avg") Then
                    Response.Write("<td align=right width=65>" + String.Format(fieldFormat, userRow("RptValue_Avg")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                Else
                    Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                End If

                If (CBool(Request.QueryString("ytd"))) And _
                    dsUserDefined.Tables(0).Columns.Contains("RptValue_YTD") Then
                    Response.Write("<td align=right width=65>" + String.Format(fieldFormat, userRow("RptValue_YTD")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                Else
                    Response.Write("<td >&nbsp</td><td>&nbsp</td>")
                End If


                Response.Write("</tr>")
            Next
        Next

        Response.Write("</table>")

    End Sub
End Class
