<!-- config-start -->
FILE=_CONFIG/Personnel.xml;
<!-- config-end -->
<!-- template-start -->
<table class='small' border=0>
<tr>
  <td width=250></td>
  <td align=center colspan=2><strong>O,C&amp;C<br><strong>Hours</td>
  <td align=center colspan=2><strong>M,P&amp;S<br><strong>Hours</td>
</tr>

SECTION(Absence,,SortKey)
BEGIN
<tr>
  <td width=250>@DEscription</td>
  <td width=80 align=right>Format(OCCAbs,'#,##0')</td>
  <td width=30></td>
  <td width=80 align=right>Format(MPSAbs,'#,##0')</td> 
  <td width=30></td>
</tr>
END 
</table>
<!-- template-end -->
