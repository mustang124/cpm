﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

public class FileSystem
{
    public static void CopyDirectory(string Src, string Dst)
    {
        String[] Files;

        if (Dst[Dst.Length - 1] != Path.DirectorySeparatorChar)
            Dst += Path.DirectorySeparatorChar;
        if (!Directory.Exists(Dst)) Directory.CreateDirectory(Dst);
        Files = Directory.GetFileSystemEntries(Src);

        int index = 0;
        try
        {
            foreach (string Element in Files)
            {
                // Sub directories

                if (Directory.Exists(Element))
                {
                    CopyDirectory(Element, Dst + Path.GetFileName(Element));
                    // Files in directory
                }
                else
                {
                    File.Copy(Element, Dst + Path.GetFileName(Element), true);
                }

                index++;
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public static void RecursiveDelete(string dir)
    {
        string[] files = Directory.GetFiles(dir);
        string[] dirs = Directory.GetDirectories(dir);

        // delete files in current directory dir 
        foreach (string file in files)
            try { 
                File.Delete(file); 
            }
            catch (System.IO.IOException ioException)
            {
                throw ioException;
            }

        // now work down through directories 
        foreach (string directory in dirs)
        {
            RecursiveDelete(directory);
            try { 
                    Directory.Delete(directory); 
            }
            catch (System.IO.IOException ioException)
            {
                throw ioException;
            }
        }
    }



}

