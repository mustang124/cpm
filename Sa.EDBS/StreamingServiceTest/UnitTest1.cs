﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using StreamingService;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Text;

namespace StreamingServiceTest
{
	[TestClass]
	public class UnitTest1
	{
		DataAccess da = new DataAccess();

		[TestMethod]
		public void TestgetData()
		{
			//just makes sure it can connect and get data back
			//first we try using a bad stored proc, just to make sure that's not giving false data
			//then we give the good version

			DataSet ds;

			ds = da.getData("web.ThisIsNotARealStoredProc", "EXAMPLE", new System.DateTime(2016, 7, 1));//Bad stored proc. This should fail (and we want it to, for the test). [NOTE: web service now returns empty database]
			Assert.AreEqual(0, ds.Tables.Count);

			ds = da.getData("web.ProcessData", "EXAMPLE", new System.DateTime(2016, 7, 1));
			Assert.AreNotEqual(0, ds.Tables.Count);
		}

		[TestMethod]
		public void TestisValidAPPId()
		{
			//tests the isValidAPPId function

			bool validAppID = da.isValidAPPId("8408bfb1-38bf-4541-98ef-af1986906f06 ");//this must fail, this is for the INVALID unit which is expired
			Assert.IsFalse(validAppID);

			validAppID = da.isValidAPPId("");//this must fail
			Assert.IsFalse(validAppID);

			validAppID = da.isValidAPPId("e85d0fb1-ffbe-476b-bfef-cc4cc22fd0ab ");//this must pass, this is for the EXAMPLE unit (expires 1/1/20)
			Assert.IsTrue(validAppID);

		}

		[TestMethod]
		public void TestgetCompanyID()
		{
			//tests the getCompanyID function

			string companyID = da.getCompanyID("e85d0fb1-ffbe-476b-bfef-cc4cc22fd0ab ");//this must pass (note, not a good way to do it, hardcoding)
			Assert.AreEqual(companyID, "EXAMPLE");

			companyID = da.getCompanyID("This is invalid");//this must fail
			Assert.AreNotEqual(companyID, "SHELL");

		}

		[TestMethod]
		public void TestGetAPIKey()
		{
			//tests the GetAPIKey function

			string companyID = da.getCompanyID("e85d0fb1-ffbe-476b-bfef-cc4cc22fd0ab ");//this must pass (note, not a good way to do it, hardcoding)
			Assert.AreEqual(companyID, "EXAMPLE");

			companyID = da.getCompanyID("This is invalid");//this must fail
			Assert.AreNotEqual(companyID, "SHELL");

		}

		[TestMethod]
		public void TestAllStoredProcedures()
		{
			//tests every one of the stored procedures
			//this gets back a table each time, even if it is empty
			//really just checks that the stored proc exists and can be called

			string companyName = "EXAMPLE";
			DateTime dateToTest = new System.DateTime(2016, 7, 1);

			DataSet ds = da.getData("web.ProcessData", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);
			//Assert.AreNotEqual(0, ds.Tables[0].Rows.Count);

			ds = da.getData("web.CTMaterialReports", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.CTPricesByBlend", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.FurnacePeerGroupDef", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.LubePAProductSlateDetails", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.LubePAReturnsAndFuelProductSales", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.OutputFiles", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.OutputFileSheetColumnHeaders", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.OutputFileSheetColumns", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.OutputFileSheetRows", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.OutputFileSheets", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.PeerGroupDef", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.ProcessPeers", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.ProcessVsPeer", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.Quartiles", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.Ranking", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.RefineryData", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.RefineryPeers", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.RefineryVsPeer", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.Sections", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

			ds = da.getData("web.VarDef", companyName, dateToTest);
			Assert.AreNotEqual(0, ds.Tables.Count);

		}

		[TestMethod]
		public async Task TestReceiveDataAndDecrypt()
		{

			//creates an HttpRequestMessage, then passes it to the StreamingController for testing
			//StreamingController should return a byte array, which we can decrypt and check for problems
			HttpRequestMessage msg = new HttpRequestMessage();

			//note the following two strings are from the EXAMPLE unit in the database
			string ApplicationId = "e85d0fb1-ffbe-476b-bfef-cc4cc22fd0ab";
			string APIKey = "DfPn+SeFUs6FjBv2gHjEcYlSEZEvGLEn4fgQNuWCcGdvBsI+tAzg7s02kRaXQ20rHpaFI7qGjIaYpsM2SKFrDQ==";
			DateTime dt = DateTime.Now;
			SAService saservice = new SAService(ApplicationId, dt);

			//creates content for the message
			msg.Content = new ObjectContent<SAService>(saservice, new System.Net.Http.Formatting.JsonMediaTypeFormatter());

			StreamingController sc = new StreamingController(msg);
			sc.Configuration = new HttpConfiguration();

			HttpResponseMessage response = sc.EDBSResponse("web.ProcessData");

			if (response.IsSuccessStatusCode)
			{
				byte[] eb = await response.Content.ReadAsByteArrayAsync();

				byte[] db = Sa.Decryption.Decrypt(eb, UTF8Encoding.UTF8.GetBytes(saservice.APPId), UTF8Encoding.UTF8.GetBytes(APIKey));

				string s = Encoding.UTF8.GetString(db);

				if (!s.Contains("\"Table\""))//it will contain "Table" if it returned something and was correctly decrypted
				{
					Assert.Fail("Decrypted string does not contain the phrase \"Table\"");
				}
			}
			else
			{
				Assert.Fail("{0} {1}", response.ReasonPhrase, response.Content.ReadAsStringAsync().Result);
			}

		}


		public class SAService
		{
			public string APPId;
			public DateTime StartDate;

			public SAService(string id, DateTime dt)
			{
				APPId = id;
				StartDate = dt;
			}
		}


	}
}
