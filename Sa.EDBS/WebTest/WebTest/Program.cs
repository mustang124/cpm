﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Security.Cryptography;
using System.Net.Http.Headers;
using System.Threading;
using System.Web.Http;
using System.Net;

namespace WebTest
{
	class Program
	{
		static void Main(string[] args)
		{
			var something = RunAsync();
			Console.ReadKey();
		}

		[HttpPost]
		public static async Task<HttpResponseMessage> RunAsync()
		{

			Console.WriteLine("Calling the back-end API");

			string apiBaseAddress = "http://localhost:50166/";
			string filePath = @"c:\temp\write.txt";

			//This example has hardcoded strings. You should not do this.
			//Instead, implement some method to get the strings from a file, a database, the call to the console program, or your other preferred method.
			string ApplicationId = "483c971c-aa10-4cac-adc7-0a1314fb9986";
			string APIKey = "HJeNlQ8dMIR9FlMXD2Jov9H8hdE3KNKOFi6sNPFuGsu4dlgUMeae6DHfm/L8pxJfTRaDNjcEiKGD1E6y7jCWtQ==";
			DateTime dt = DateTime.Now;
			dt = new DateTime(2016, 7, 1);
			SAService saservice = new SAService(ApplicationId, dt);

			CustomDelegatingHandler customDelegatingHandler = new CustomDelegatingHandler(ApplicationId, APIKey);
			HttpClient client = HttpClientFactory.Create(customDelegatingHandler);
			client.Timeout = TimeSpan.FromSeconds(10);
			HttpResponseMessage response = await client.PostAsJsonAsync(apiBaseAddress + "api/streaming/getProcessData", saservice);

			if (response.IsSuccessStatusCode)
			{
				byte[] eb = await response.Content.ReadAsByteArrayAsync();
				byte[] db = Sa.Decryption.Decrypt(eb, UTF8Encoding.UTF8.GetBytes(saservice.APPId), UTF8Encoding.UTF8.GetBytes(APIKey));

				System.IO.FileStream _FileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
				_FileStream.Write(db, 0, db.Length);

				Console.WriteLine("HTTP Status: {0}, Reason: {1}. Press ENTER to exit", response.StatusCode, response.ReasonPhrase);
			}
			else
			{
				Console.WriteLine("Failed to call the API. HTTP Status: {0}, Reason: {1}", response.StatusCode, response.ReasonPhrase);
			}
			return response;
		}
	}

	public class CustomDelegatingHandler : DelegatingHandler
	{

		private string AppId;
		private string AppKey;

		public CustomDelegatingHandler(string ApplicationID, string APIKey)
		{
			AppId = ApplicationID;
			AppKey = APIKey;
		}

		protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{

			//the following code is adapted from http://bitoftech.net/2014/12/15/secure-asp-net-web-api-using-api-key-authentication-hmac-authentication/

			HttpResponseMessage response = null;

			string requestContentBase64String = string.Empty;
			string requestUri = System.Web.HttpUtility.UrlEncode(request.RequestUri.AbsoluteUri.ToLower());
			string requestHttpMethod = request.Method.Method;

			//Calculate UNIX time
			DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan timeSpan = DateTime.UtcNow - epochStart;
			string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

			//create random nonce for each request
			string nonce = Guid.NewGuid().ToString("N");

			//Checking if the request contains body, usually will be null wiht HTTP GET and DELETE
			if (request.Content != null)
			{
				byte[] content = await request.Content.ReadAsByteArrayAsync();
				SHA256 md5 = SHA256.Create();
				//Hashing the request body, any change in request body will result in different hash, we'll incure message integrity
				byte[] requestContentHash = md5.ComputeHash(content);
				requestContentBase64String = Convert.ToBase64String(requestContentHash);
			}

			//Creating the raw signature string
			string signatureRawData = String.Format("{0}{1}{2}{3}{4}{5}", AppId, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);
			byte[] signature = Encoding.UTF8.GetBytes(signatureRawData);
			var secretKeyByteArray = Convert.FromBase64String(AppKey);

			using (HMACSHA256 hmac = new HMACSHA256(secretKeyByteArray))
			{
				byte[] signatureBytes = hmac.ComputeHash(signature);
				string requestSignatureBase64String = Convert.ToBase64String(signatureBytes);
				//Setting the values in the Authorization header using custom scheme (amx)
				request.Headers.Authorization = new AuthenticationHeaderValue("amx", string.Format("{0}:{1}:{2}:{3}", AppId, requestSignatureBase64String, nonce, requestTimeStamp));
			}

			try
			{
				response = await base.SendAsync(request, cancellationToken);
			}
			catch (HttpRequestException e)
			{
				response = new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
				response.ReasonPhrase = e.InnerException.Message.ToString();
			}

			return response;
		}
	}

	public class SAService
	{
		public string APPId;
		public DateTime StartDate;

		public SAService(string id, DateTime dt)
		{
			APPId = id;
			StartDate = dt;
		}
	}

}
