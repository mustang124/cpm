﻿using System.Net.Mail;

namespace StreamingService
{
    public class SendMail
    {
        #region Send Email Message
        public void SendEMailMessage(string author, string from, string to, string bcc, string cc, string subject, string body)
        {
            using (MailMessage mm = new MailMessage())
            {
                mm.From = new MailAddress(from, author);
                var recips = to.Split(";".ToCharArray());
                foreach (var recip in recips)
                {
                    mm.To.Add(new MailAddress(recip));
                }

                mm.IsBodyHtml = true;
                mm.Priority = MailPriority.Normal;

                SmtpClient client = new SmtpClient(System.Configuration.ConfigurationManager.ConnectionStrings["EmailServerAddress"].ToString());

                mm.Subject = subject;
                mm.Body = body;
                try
                {
                    client.Send(mm);
                }
                catch (System.Exception)
                {

                }
            }
        }
        #endregion
    }
}