﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.OrmTool
{
    public class DbHelper
    {
        public static string GetConnectionString(ConnectionType connectionType = ConnectionType.Default)
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        }

        public static SqlDataReader ExecuteDataReader(string query, ConnectionType connectionType = ConnectionType.Default)
        {
            using (SqlConnection connection = new SqlConnection(GetConnectionString(connectionType)))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    return command.ExecuteReader();
                }
            }
        }

        public static int ExecuteNonQuery(string query, ConnectionType connectionType = ConnectionType.Default)
        {
            using (SqlConnection connection = new SqlConnection(GetConnectionString(connectionType)))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    return command.ExecuteNonQuery();
                }
            }
        }

        public static object ExecuteQuery(string query, List<KeyValuePair<string, object>> parameters, ConnectionType connectionType = ConnectionType.Default)
        {
            using (SqlConnection connection = new SqlConnection(GetConnectionString(connectionType)))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    if (parameters != null)
                    {
                        foreach (var item in parameters)
                        {
                            command.Parameters.AddWithValue(item.Key, item.Value);
                        }
                    }

                    return command.ExecuteScalar();
                }
            }
        }

        public static object ExecuteQuery(string query, ConnectionType connectionType = ConnectionType.Default)
        {
            return ExecuteQuery(query, null, connectionType);
        }

        public static List<T> GetObjectSet<T>(string query, ConnectionType connectionType = ConnectionType.Default)
        {
            using (SqlConnection connection = new SqlConnection(GetConnectionString(connectionType)))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        PropertyInfo[] properties = typeof(T).GetProperties();
                        List<T> retVal = new List<T>();

                        while (reader.Read())
                        {
                            T t = (T)Activator.CreateInstance(typeof(T));


                            foreach (PropertyInfo property in properties)
                            {
                                property.SetValue(t, reader[property.Name] == DBNull.Value ? null : reader[property.Name]);
                                //property.SetValue(t, ConvertToType(property.PropertyType.FullName, reader[property.Name]));   

                            }

                            retVal.Add(t);
                        }

                        return retVal;
                    }
                }
            }
        }

        private static object ConvertToType(string propertyName, object value)
        {
            switch (propertyName)
            {
                case "System.Int32":
                    return int.Parse(value.ToString());
                case "System.Decimal":
                    return decimal.Parse(value.ToString());
                case "System.DateTime":
                    return DateTime.Parse(value.ToString());
                case "System.String":
                    return value.ToString();

                default:
                    {
                        if (propertyName.Contains("System.Nullable"))
                        {
                            string type = propertyName.Replace("System.Nullable`1[[", "");
                            type = type.Substring(0, type.IndexOf(","));

                            if (value != null)
                            {
                                return ConvertToType(type, value);
                            }

                            return null;
                        }

                        return value;
                    }
            }
        }
    }

    public enum ConnectionType
    {
        Development,
        Test,
        Production,
        Default,
        Source,
        Target
    }
}
