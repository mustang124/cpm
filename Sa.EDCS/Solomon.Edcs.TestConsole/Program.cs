﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Solomon.Edcs.Model;

namespace Solomon.Edcs.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //CheckEdcsTypes();

            using (EdcsEntities db = new EdcsEntities())
            {
                var tables = db.AppTables.Where(x => x.Study == 1).ToList();
            }
        }

        private static void CheckEdcsTypes()
        {
            var assembly = Assembly.LoadFrom(@"C:\Users\oazad.DC1\Documents\visual studio 2012\Projects\Solomon.Edcs\Solomon.Edcs.Model\bin\Debug\Solomon.Edcs.Model.dll");



            var types = assembly.GetTypes();

            foreach (var type in types)
            {
                if (type.GetProperty(type.Name + "Id") == null)
                    continue;

                Console.WriteLine("public partial class {0}Manager : Manager<{0}>", type.Name);
                Console.WriteLine("{");
                //WriteLine("IUnitOfWork uow;");

                Console.WriteLine("      public {0}Manager(IUnitOfWork uow) : base(uow) {{}}", type.Name);

                Console.WriteLine("      public {0} GetById(int id)", type.Name);
                Console.WriteLine("{");
                Console.WriteLine("              return Repository.Get(x => x.{0}ID == id);", type.Name);
                Console.WriteLine("}");

                Console.WriteLine("      public List<{0}> GetAll()", type.Name);
                Console.WriteLine("{");
                Console.WriteLine("              return Repository.GetAll().ToList();");
                Console.WriteLine("}");

                Console.WriteLine("      public void Add({0} {1})", type.Name, type.Name.ToLower());
                Console.WriteLine("{");
                Console.WriteLine("              Repository.Add({0});", type.Name.ToLower());
                Console.WriteLine("}");

                Console.WriteLine("      public void Delete({0} {1})", type.Name, type.Name.ToLower());
                Console.WriteLine("{");
                Console.WriteLine("              Repository.Delete({0});", type.Name.ToLower());
                Console.WriteLine("}");

                Console.WriteLine("}");
            }
        }
    }
}
