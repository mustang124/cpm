﻿
using Solomon.Edcs.Repository;
namespace Solomon.Edcs.Components
{
    public class Manager<TEntity> where TEntity : class
    {
        public IRepository<TEntity> Repository 
        { 
            get; 
            private set; 
        }

        public IUnitOfWork Uow
        {
            get;
            private set;
        }

        public Manager(IUnitOfWork uow)
        {
            this.Uow = uow;
            this.Repository = uow.GetRepository<TEntity>();
        }
    }
}
