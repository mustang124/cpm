﻿namespace Solomon.Edcs.Windows
{
    partial class frmAddColumn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDataType = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txtColumnName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddColumn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.ddlClient = new System.Windows.Forms.ComboBox();
            this.ddlYear = new System.Windows.Forms.ComboBox();
            this.ddlDataType = new System.Windows.Forms.ComboBox();
            this.lblDisplayName = new System.Windows.Forms.Label();
            this.txtDisplayName = new System.Windows.Forms.TextBox();
            this.lblOrder = new System.Windows.Forms.Label();
            this.txtColumnOrder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ddlReadOnly = new System.Windows.Forms.ComboBox();
            this.ddlGroupColumn = new System.Windows.Forms.ComboBox();
            this.ddlStatus = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblGroup = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblSource = new System.Windows.Forms.Label();
            this.ddlSource = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblDataType
            // 
            this.lblDataType.Location = new System.Drawing.Point(74, 149);
            this.lblDataType.Name = "lblDataType";
            this.lblDataType.Size = new System.Drawing.Size(149, 13);
            this.lblDataType.TabIndex = 35;
            this.lblDataType.Text = "Data Type:";
            this.lblDataType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(187, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "Client:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(191, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Year:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(147, 73);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(76, 13);
            this.lblName.TabIndex = 30;
            this.lblName.Text = "Column Name:";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtColumnName
            // 
            this.txtColumnName.Location = new System.Drawing.Point(234, 66);
            this.txtColumnName.Name = "txtColumnName";
            this.txtColumnName.Size = new System.Drawing.Size(243, 20);
            this.txtColumnName.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(485, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "***";
            // 
            // btnAddColumn
            // 
            this.btnAddColumn.Location = new System.Drawing.Point(234, 392);
            this.btnAddColumn.Name = "btnAddColumn";
            this.btnAddColumn.Size = new System.Drawing.Size(121, 23);
            this.btnAddColumn.TabIndex = 27;
            this.btnAddColumn.Text = "Add Column";
            this.btnAddColumn.UseVisualStyleBackColor = true;
            this.btnAddColumn.Click += new System.EventHandler(this.btnAddColumn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(236, 365);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "*** Required Fields.";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(155, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Table Name:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTableName
            // 
            this.txtTableName.Enabled = false;
            this.txtTableName.Location = new System.Drawing.Point(234, 40);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(243, 20);
            this.txtTableName.TabIndex = 24;
            // 
            // ddlClient
            // 
            this.ddlClient.FormattingEnabled = true;
            this.ddlClient.Location = new System.Drawing.Point(234, 203);
            this.ddlClient.Name = "ddlClient";
            this.ddlClient.Size = new System.Drawing.Size(121, 21);
            this.ddlClient.TabIndex = 23;
            // 
            // ddlYear
            // 
            this.ddlYear.FormattingEnabled = true;
            this.ddlYear.Location = new System.Drawing.Point(234, 174);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(121, 21);
            this.ddlYear.TabIndex = 22;
            // 
            // ddlDataType
            // 
            this.ddlDataType.FormattingEnabled = true;
            this.ddlDataType.Location = new System.Drawing.Point(234, 146);
            this.ddlDataType.Name = "ddlDataType";
            this.ddlDataType.Size = new System.Drawing.Size(243, 21);
            this.ddlDataType.TabIndex = 21;
            // 
            // lblDisplayName
            // 
            this.lblDisplayName.Location = new System.Drawing.Point(110, 96);
            this.lblDisplayName.Name = "lblDisplayName";
            this.lblDisplayName.Size = new System.Drawing.Size(113, 13);
            this.lblDisplayName.TabIndex = 37;
            this.lblDisplayName.Text = "Column Display Name:";
            this.lblDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDisplayName
            // 
            this.txtDisplayName.Location = new System.Drawing.Point(234, 93);
            this.txtDisplayName.Name = "txtDisplayName";
            this.txtDisplayName.Size = new System.Drawing.Size(243, 20);
            this.txtDisplayName.TabIndex = 36;
            // 
            // lblOrder
            // 
            this.lblOrder.Location = new System.Drawing.Point(113, 122);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(110, 13);
            this.lblOrder.TabIndex = 39;
            this.lblOrder.Text = "Column Order:";
            this.lblOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtColumnOrder
            // 
            this.txtColumnOrder.Location = new System.Drawing.Point(234, 119);
            this.txtColumnOrder.Name = "txtColumnOrder";
            this.txtColumnOrder.Size = new System.Drawing.Size(243, 20);
            this.txtColumnOrder.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(483, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 40;
            this.label2.Text = "***";
            // 
            // ddlReadOnly
            // 
            this.ddlReadOnly.FormattingEnabled = true;
            this.ddlReadOnly.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.ddlReadOnly.Location = new System.Drawing.Point(234, 230);
            this.ddlReadOnly.Name = "ddlReadOnly";
            this.ddlReadOnly.Size = new System.Drawing.Size(121, 21);
            this.ddlReadOnly.TabIndex = 41;
            // 
            // ddlGroupColumn
            // 
            this.ddlGroupColumn.FormattingEnabled = true;
            this.ddlGroupColumn.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.ddlGroupColumn.Location = new System.Drawing.Point(234, 259);
            this.ddlGroupColumn.Name = "ddlGroupColumn";
            this.ddlGroupColumn.Size = new System.Drawing.Size(121, 21);
            this.ddlGroupColumn.TabIndex = 42;
            // 
            // ddlStatus
            // 
            this.ddlStatus.FormattingEnabled = true;
            this.ddlStatus.Location = new System.Drawing.Point(234, 286);
            this.ddlStatus.Name = "ddlStatus";
            this.ddlStatus.Size = new System.Drawing.Size(121, 21);
            this.ddlStatus.TabIndex = 43;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(94, 286);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Status:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroup
            // 
            this.lblGroup.Location = new System.Drawing.Point(91, 262);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(132, 13);
            this.lblGroup.TabIndex = 45;
            this.lblGroup.Text = "Is Group Column:";
            this.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(152, 233);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "Is Read Only:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSource
            // 
            this.lblSource.Location = new System.Drawing.Point(94, 314);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(129, 13);
            this.lblSource.TabIndex = 48;
            this.lblSource.Text = "Source:";
            this.lblSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ddlSource
            // 
            this.ddlSource.FormattingEnabled = true;
            this.ddlSource.Location = new System.Drawing.Point(234, 314);
            this.ddlSource.Name = "ddlSource";
            this.ddlSource.Size = new System.Drawing.Size(121, 21);
            this.ddlSource.TabIndex = 47;
            // 
            // frmAddColumn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 484);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.ddlSource);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblGroup);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ddlStatus);
            this.Controls.Add(this.ddlGroupColumn);
            this.Controls.Add(this.ddlReadOnly);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblOrder);
            this.Controls.Add(this.txtColumnOrder);
            this.Controls.Add(this.lblDisplayName);
            this.Controls.Add(this.txtDisplayName);
            this.Controls.Add(this.lblDataType);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtColumnName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnAddColumn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTableName);
            this.Controls.Add(this.ddlClient);
            this.Controls.Add(this.ddlYear);
            this.Controls.Add(this.ddlDataType);
            this.Name = "frmAddColumn";
            this.Text = "frmAddColumn";
            this.Load += new System.EventHandler(this.frmAddColumn_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDataType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtColumnName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddColumn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.ComboBox ddlClient;
        private System.Windows.Forms.ComboBox ddlYear;
        private System.Windows.Forms.ComboBox ddlDataType;
        private System.Windows.Forms.Label lblDisplayName;
        private System.Windows.Forms.TextBox txtDisplayName;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.TextBox txtColumnOrder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ddlReadOnly;
        private System.Windows.Forms.ComboBox ddlGroupColumn;
        private System.Windows.Forms.ComboBox ddlStatus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.ComboBox ddlSource;
    }
}