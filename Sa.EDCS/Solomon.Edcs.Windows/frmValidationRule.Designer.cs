﻿namespace Solomon.Edcs.Windows
{
    partial class frmValidationRule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmValidationRule));
            this.dgColumns = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlClient = new System.Windows.Forms.ComboBox();
            this.ddlYear = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblStudy = new System.Windows.Forms.Label();
            this.txtStudy = new System.Windows.Forms.TextBox();
            this.txtTable = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtValidationMsg = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCreateDataset = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.dgRows = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.dgRules = new System.Windows.Forms.DataGridView();
            this.btnViewRuleAssociation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRules)).BeginInit();
            this.SuspendLayout();
            // 
            // dgColumns
            // 
            this.dgColumns.AllowUserToDeleteRows = false;
            this.dgColumns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgColumns.Location = new System.Drawing.Point(356, 121);
            this.dgColumns.Name = "dgColumns";
            this.dgColumns.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgColumns.Size = new System.Drawing.Size(258, 295);
            this.dgColumns.TabIndex = 17;
            this.dgColumns.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgDataTable_RowStateChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(353, 435);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(256, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Select Client if Validation Rule apply to specific Client";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 432);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(246, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Select Year if Validation Rule apply to specific year";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Study:";
            // 
            // ddlClient
            // 
            this.ddlClient.FormattingEnabled = true;
            this.ddlClient.Location = new System.Drawing.Point(358, 451);
            this.ddlClient.Name = "ddlClient";
            this.ddlClient.Size = new System.Drawing.Size(121, 21);
            this.ddlClient.TabIndex = 20;
            this.ddlClient.Text = "Select a Client";
            this.ddlClient.SelectionChangeCommitted += new System.EventHandler(this.ddlClient_SelectionChangeCommitted);
            // 
            // ddlYear
            // 
            this.ddlYear.FormattingEnabled = true;
            this.ddlYear.Location = new System.Drawing.Point(42, 452);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(121, 21);
            this.ddlYear.TabIndex = 19;
            this.ddlYear.Text = "Select a Year";
            this.ddlYear.SelectionChangeCommitted += new System.EventHandler(this.ddlYear_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(289, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Table:";
            // 
            // lblStudy
            // 
            this.lblStudy.AutoSize = true;
            this.lblStudy.Location = new System.Drawing.Point(328, 17);
            this.lblStudy.Name = "lblStudy";
            this.lblStudy.Size = new System.Drawing.Size(0, 13);
            this.lblStudy.TabIndex = 26;
            // 
            // txtStudy
            // 
            this.txtStudy.Enabled = false;
            this.txtStudy.Location = new System.Drawing.Point(106, 11);
            this.txtStudy.Name = "txtStudy";
            this.txtStudy.Size = new System.Drawing.Size(153, 20);
            this.txtStudy.TabIndex = 27;
            // 
            // txtTable
            // 
            this.txtTable.Enabled = false;
            this.txtTable.Location = new System.Drawing.Point(328, 11);
            this.txtTable.Name = "txtTable";
            this.txtTable.Size = new System.Drawing.Size(317, 20);
            this.txtTable.TabIndex = 28;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(40, 503);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(136, 23);
            this.btnSave.TabIndex = 33;
            this.btnSave.Text = "Create Rule Association";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "DeleteRed.png");
            this.imgList.Images.SetKeyName(1, "Save.png");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(353, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(243, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Select Column(s) to Associate with Validation Rule";
            // 
            // txtValidationMsg
            // 
            this.txtValidationMsg.Location = new System.Drawing.Point(106, 38);
            this.txtValidationMsg.Name = "txtValidationMsg";
            this.txtValidationMsg.Size = new System.Drawing.Size(408, 20);
            this.txtValidationMsg.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Validation Message:";
            // 
            // btnCreateDataset
            // 
            this.btnCreateDataset.Location = new System.Drawing.Point(788, 10);
            this.btnCreateDataset.Name = "btnCreateDataset";
            this.btnCreateDataset.Size = new System.Drawing.Size(139, 23);
            this.btnCreateDataset.TabIndex = 38;
            this.btnCreateDataset.Text = "Validation Rule Manager";
            this.btnCreateDataset.UseVisualStyleBackColor = true;
            this.btnCreateDataset.Click += new System.EventHandler(this.btnCreateDataset_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(39, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(230, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Select Row(s) to Associate with Validation Rule";
            // 
            // dgRows
            // 
            this.dgRows.AllowUserToDeleteRows = false;
            this.dgRows.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRows.Location = new System.Drawing.Point(42, 121);
            this.dgRows.Name = "dgRows";
            this.dgRows.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRows.Size = new System.Drawing.Size(258, 295);
            this.dgRows.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(669, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "Select Validation Rule(s)";
            // 
            // dgRules
            // 
            this.dgRules.AllowUserToDeleteRows = false;
            this.dgRules.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRules.Location = new System.Drawing.Point(672, 121);
            this.dgRules.Name = "dgRules";
            this.dgRules.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRules.Size = new System.Drawing.Size(258, 295);
            this.dgRules.TabIndex = 41;
            // 
            // btnViewRuleAssociation
            // 
            this.btnViewRuleAssociation.Location = new System.Drawing.Point(358, 503);
            this.btnViewRuleAssociation.Name = "btnViewRuleAssociation";
            this.btnViewRuleAssociation.Size = new System.Drawing.Size(156, 23);
            this.btnViewRuleAssociation.TabIndex = 43;
            this.btnViewRuleAssociation.Text = "View/Delete Rule Association";
            this.btnViewRuleAssociation.UseVisualStyleBackColor = true;
            this.btnViewRuleAssociation.Click += new System.EventHandler(this.btnViewRuleAssociation_Click);
            // 
            // frmValidationRule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 555);
            this.Controls.Add(this.btnViewRuleAssociation);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dgRules);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgRows);
            this.Controls.Add(this.btnCreateDataset);
            this.Controls.Add(this.txtValidationMsg);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtTable);
            this.Controls.Add(this.txtStudy);
            this.Controls.Add(this.lblStudy);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlClient);
            this.Controls.Add(this.ddlYear);
            this.Controls.Add(this.dgColumns);
            this.Name = "frmValidationRule";
            this.Text = "Data Table";
            this.Load += new System.EventHandler(this.frmDataTable_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRules)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgColumns;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlClient;
        private System.Windows.Forms.ComboBox ddlYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblStudy;
        private System.Windows.Forms.TextBox txtStudy;
        private System.Windows.Forms.TextBox txtTable;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtValidationMsg;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCreateDataset;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgRows;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgRules;
        private System.Windows.Forms.Button btnViewRuleAssociation;
    }
}