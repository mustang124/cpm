﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Solomon.Edcs.Windows
{
    public partial class frmAddColumn : Form
    {
        public int tableId = 0;
        public int selectedColumnId = 0;
        public bool IsRowAction = false;
        public bool newColumn = false;
        public frmAddColumn()
        {
            InitializeComponent();
        }

        private void frmAddColumn_Load(object sender, EventArgs e)
        {
            BindDataSource();
            var table = new AppTableService().GetById(tableId);
            txtTableName.Text = table.Name;
                        
            if (newColumn)
            {
                if (IsRowAction)
                {
                    btnAddColumn.Text = "Add Row";
                }
                else
                {
                    btnAddColumn.Text = "Add Column";
                }
            }
            else
            {
                if (IsRowAction)
                {
                    btnAddColumn.Text = "Update Row";
                }
                else
                {
                    btnAddColumn.Text = "Update Column";                    
                }
                PopulateForm();
            }

            if (IsRowAction)
            {
                lblDataType.Text = "Associate Column Id:";
                lblDisplayName.Visible = txtDisplayName.Visible = lblSource.Visible = ddlSource.Visible = false;
                lblGroup.Text = "Is Group Row:";
                lblName.Text = "Row Name:";
                lblOrder.Text = "Row Order:";
            }
        }

        private void BindDataSource()
        {
            var service = new AppLookupService();

            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "StudyYear").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlYear);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Client").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlClient);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "TableStatus").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlStatus);
            
            if (IsRowAction)
            {
                Utility.BindComboBox(new AppColumnService().GetAll(x => x.AppTableId == tableId && (x.Year == null || x.Year.Value == ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key) && (x.ClientId == null || x.ClientId == ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key)).Select(x => new KeyValuePair<int, string>(x.AppColumnId, x.Name)).ToList(), ddlDataType);
            }
            else
            {
                Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "AppDataType").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlDataType);
                Utility.BindComboBox(new AppLookupTypeService().GetAll().Select(x => new KeyValuePair<int, string>(x.AppLookupTypeId, x.DisplayName)).ToList(), ddlSource);
            }
            
        }

        private void btnAddColumn_Click(object sender, EventArgs e)
        {
            if (newColumn)
            {
                if (IsRowAction)
                {
                    AddRowProcess();
                }
                else
                {
                    AddColumnProcess();
                }
            }
            else
            {
                if (IsRowAction)
                {
                    UpdateRowProcess();
                }
                else
                {
                    UpdateColumnProcess();
                }
            }
        }


        private void AddColumnProcess()
        {
            if (string.IsNullOrWhiteSpace(txtColumnName.Text) || ddlDataType.SelectedItem == null || ddlDataType.SelectedIndex < 1)
            {
                MessageBox.Show("Column name and Data Type are required fields.", "Add Column");
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to add Column", "Add Column", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    AppColumn column = CollectColumnData();

                    AppColumnService service = new AppColumnService();
                    service.Add(column);

                    MessageBox.Show("Column added successfully.", "Add Column");

                    this.Close();

                    DataEvent.RaiseDataChanged(this, new EventArgs());
                }
            }
        }
        private void AddRowProcess()
        {
            if (string.IsNullOrWhiteSpace(txtColumnName.Text) || ddlDataType.SelectedItem == null || ddlDataType.SelectedIndex < 1)
            {
                MessageBox.Show("Row name and Associate Column Id are required fields.", "Add Row");
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to add Row", "Add Row", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    AppRow row = CollectRowData();

                    AppRowService service = new AppRowService();
                    service.Add(row);

                    MessageBox.Show("Row added successfully.", "Add Row");

                    this.Close();

                    DataEvent.RaiseDataChanged(this, new EventArgs());
                }
            }
        }

        private void UpdateColumnProcess()
        {
            if (string.IsNullOrWhiteSpace(txtColumnName.Text) || ddlDataType.SelectedItem == null || ddlDataType.SelectedIndex < 1)
            {
                MessageBox.Show("Column name and Data Type are required fields.", "Update Column");
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to Update Column", "Update Column", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    AppColumn column = CollectColumnData();
                    column.AppColumnId = selectedColumnId;

                    AppColumnService service = new AppColumnService();
                    service.Update(column);

                    MessageBox.Show("Column updated successfully.", "Update Column");

                    this.Close();

                    DataEvent.RaiseDataChanged(this, new EventArgs());
                }
            }
        }

        private void UpdateRowProcess()
        {
            if (string.IsNullOrWhiteSpace(txtColumnName.Text) || ddlDataType.SelectedItem == null || ddlDataType.SelectedIndex < 1)
            {
                MessageBox.Show("Column name and Associate Column Id are required fields.", "Update Row");
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to Update Row", "Update Row", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    AppRow row = CollectRowData();
                    row.AppRowId = selectedColumnId;

                    AppRowService service = new AppRowService();
                    service.Update(row);

                    MessageBox.Show("Row updated successfully.", "Update Row");

                    this.Close();

                    DataEvent.RaiseDataChanged(this, new EventArgs());
                }
            }
        }

        private void PopulateForm()
        {
            if (IsRowAction)
            {
                var row = new AppRowService().GetById(selectedColumnId);

                if (row != null)
                {
                    if (row.ClientId.HasValue)
                    {
                        ddlClient.SelectedValue = row.ClientId.Value;
                    }

                    if (row.Year.HasValue)
                    {
                        ddlYear.SelectedValue = row.Year.Value;
                    }

                    txtColumnName.Text = row.Name;
                    ddlDataType.SelectedValue = row.AssociateColumnId;                    
                }

            }
            else
            {
                var column = new AppColumnService().GetById(selectedColumnId);

                if (column != null)
                {
                    if (column.ClientId.HasValue)
                    {
                        ddlClient.SelectedValue = column.ClientId.Value;
                    }

                    if (column.Year.HasValue)
                    {
                        ddlYear.SelectedValue = column.Year.Value;
                    }

                    txtColumnName.Text = column.Name;
                    ddlDataType.SelectedValue = column.DataTypeId;
                    txtDisplayName.Text = column.DisplayName;
                }
            }
        }

        private AppRow CollectRowData()
        {
            AppRow row = new AppRow
            {
                Name = txtColumnName.Text,
                AppTableId = tableId,
                AssociateColumnId = ((KeyValuePair<int, string>)ddlDataType.SelectedItem).Key,
            };

            if (!string.IsNullOrWhiteSpace(txtColumnOrder.Text))
            {
                int rowOrder = 0;
                int.TryParse(txtColumnOrder.Text, out rowOrder);
                if (rowOrder > 0)
                {
                    row.RowOrder = rowOrder;
                }
            }

            if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key > 0)
            {
                row.Year = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
            }

            if (((KeyValuePair<int, string>)ddlClient.SelectedItem).Key > 0)
            {
                row.ClientId = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;
            }

            if (ddlReadOnly.SelectedItem != null && ddlReadOnly.SelectedText == "Yes")
            {
                row.IsReadOnly = true;
            }
            else
            {
                row.IsReadOnly = false;
            }

            if (ddlGroupColumn.SelectedItem != null && ddlGroupColumn.SelectedText == "Yes")
            {
                row.IsGroupRow = true;
            }
            else
            {
                row.IsGroupRow = false;
            }

            if (((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key > 0)
            {
                row.Status = ((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key;
            }

            return row;
        }

        private AppColumn CollectColumnData()
        {
            AppColumn column = new AppColumn
            {
                Name = txtColumnName.Text,
                DisplayName = txtDisplayName.Text,
                AppTableId = tableId,
                DataTypeId = ((KeyValuePair<int, string>)ddlDataType.SelectedItem).Key,
            };

            if (!string.IsNullOrWhiteSpace(txtColumnOrder.Text))
            {
                int columnOrder = 0;
                int.TryParse(txtColumnOrder.Text, out columnOrder);
                if (columnOrder > 0)
                {
                    column.ColumnOrder = columnOrder;
                }
            }

            if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key > 0)
            {
                column.Year = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
            }

            if (((KeyValuePair<int, string>)ddlClient.SelectedItem).Key > 0)
            {
                column.ClientId = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;
            }

            if (ddlReadOnly.SelectedItem != null && ddlReadOnly.SelectedText == "Yes")
            {
                column.IsReadOnly = true;
            }
            else
            {
                column.IsReadOnly = false;
            }

            if (ddlGroupColumn.SelectedItem != null && ddlGroupColumn.SelectedText == "Yes")
            {
                column.IsGroupColumn = true;
            }
            else
            {
                column.IsGroupColumn = false;
            }

            if (((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key > 0)
            {
                column.Status = ((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key;
            }

            if (ddlSource.SelectedItem != null && ((KeyValuePair<int, string>)ddlSource.SelectedItem).Key > 0)
            {
                column.SourceId = ((KeyValuePair<int, string>)ddlSource.SelectedItem).Key;
            }

            return column;
        }
    }
}
