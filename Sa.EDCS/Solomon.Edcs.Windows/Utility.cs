﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Solomon.Edcs.Windows
{
    public class Utility
    {
        public static void BindComboBox(IList<KeyValuePair<int, string>> data, ComboBox box, bool withSelect = true)
        {
            if (withSelect)
            {
                data.Add(new KeyValuePair<int, string>(-1, "-- Select --"));
                data = data.OrderBy(x => x.Key).ToList();
            }

            box.DataSource = data;
            box.DisplayMember = "Value";
            box.ValueMember = "Key";
        }

        public static DataGridViewComboBoxColumn CreateGvComboBox(IList<KeyValuePair<int, string>> data, string header)
        {
            DataGridViewComboBoxColumn box = new DataGridViewComboBoxColumn();

            box.DataSource = data;
            box.ValueType = typeof(string);
            box.HeaderText = header;
            //box.DataPropertyName = "DataTypeId";

            return box;
        }        
    }
}
