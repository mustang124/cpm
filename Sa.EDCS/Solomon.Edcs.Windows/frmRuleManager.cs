﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.Edcs.Utility;

namespace Solomon.Edcs.Windows
{
    public partial class frmRuleManager : Form
    {
        public frmRuleManager()
        {
            InitializeComponent();
        }

        private void frmRuleManager_Load(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void BindDataSource()
        {
            DataTable dt = new DataTable();
            //dgRules.DataSource
            BindingSource source = new BindingSource();

            source.DataSource = new AppRuleService().GetAll().ToList();

            //source.DataSource = new AppRuleService().GetAll().Select(x => 
            //    new 
            //    {
            //        x.AppRuleId,
            //        x.Name,
            //        x.DisplayName,
            //        x.IsActive,
            //        x.CreateDate,
            //        x.CreatedBy,
            //        x.UpdateDate,
            //        x.UpdatedBy                    
            //    }).ToList();

            dgRules.DataSource = source;
            dgRules.Columns[0].Visible = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var row = dgRules.CurrentRow;

            var rule = new AppRule
            {
                AppRuleId = row.Cells["AppRuleId"].Value.ToInt(),
                Name = row.Cells["Name"].Value.ToString(),
                DisplayName = row.Cells["DisplayName"].Value.ToString(),
                IsActive = row.Cells["IsActive"].Value.ToBool(),
                CreateDate = row.Cells["CreateDate"].Value.ToDateTime(),
                CreatedBy = row.Cells["CreatedBy"].Value.ToInt(),
                UpdateDate = row.Cells["UpdateDate"].Value.ToDateTime(),
                UpdatedBy = row.Cells["UpdatedBy"].Value.ToInt()
            };
            if (new AppRuleService().Save(rule))
            {
                MessageBox.Show("Validation Rule saved successfully", "Validation Rule");
            }
            else
            {
                MessageBox.Show("Validation Rule was NOT saved. Please enter right data and try again.", "Validation Rule");
            }
        }

        private void btnRuleDetails_Click(object sender, EventArgs e)
        {
            var frm = new frmRuleDetails();
            frm.selectedRuleId = dgRules.CurrentRow.Cells["AppRuleId"].Value.ToInt();
            frm.Show();
        }
    }
}
