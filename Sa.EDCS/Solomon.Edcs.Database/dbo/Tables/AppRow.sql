﻿CREATE TABLE [dbo].[AppRow] (
    [AppRowId]          INT          IDENTITY (1, 1) NOT NULL,
    [Name]              VARCHAR (50) NOT NULL,
    [AppTableId]        INT          NOT NULL,
    [AssociateColumnId] INT          NOT NULL,
    [Year]              INT          NULL,
    [ClientId]          INT          NULL,
    [IsGroupRow]        BIT          NULL,
    [RowOrder]          INT          NULL,
    [IsReadOnly]        BIT          NULL,
    [Status]            INT          NULL,
    CONSTRAINT [PK_AppRowId] PRIMARY KEY CLUSTERED ([AppRowId] ASC)
);



