﻿CREATE TABLE [dbo].[AppDataType] (
    [AppDataTypeId] INT          NOT NULL,
    [Name]          VARCHAR (50) NULL,
    CONSTRAINT [PK_AppDataType] PRIMARY KEY CLUSTERED ([AppDataTypeId] ASC)
);

