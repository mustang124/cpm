﻿CREATE TABLE [dbo].[AppCellValueInt] (
    [AppCellValueIntId] INT IDENTITY (1, 1) NOT NULL,
    [AppDataSetId]      INT NOT NULL,
    [AppRowId]          INT NOT NULL,
    [AppColumnId]       INT NOT NULL,
    [CellValue]         INT NULL,
    CONSTRAINT [PK_AppCellValueInt] PRIMARY KEY CLUSTERED ([AppCellValueIntId] ASC),
    CONSTRAINT [FK_AppCellValueInt_AppColumn] FOREIGN KEY ([AppColumnId]) REFERENCES [dbo].[AppColumn] ([AppColumnId]),
    CONSTRAINT [FK_AppCellValueInt_DataSet] FOREIGN KEY ([AppDataSetId]) REFERENCES [dbo].[AppDataSet] ([AppDataSetId])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20160427-141135]
    ON [dbo].[AppCellValueInt]([AppDataSetId] ASC, [AppRowId] ASC, [AppColumnId] ASC);

