﻿CREATE TABLE [dbo].[AppCellValueDateTime] (
    [AppCellValueDateTimeId] INT      IDENTITY (1, 1) NOT NULL,
    [AppDataSetId]           INT      NOT NULL,
    [AppRowId]               INT      NOT NULL,
    [AppColumnId]            INT      NOT NULL,
    [CellValue]              DATETIME NULL,
    CONSTRAINT [PK_AppCellValueDateTime] PRIMARY KEY CLUSTERED ([AppCellValueDateTimeId] ASC),
    CONSTRAINT [FK_AppCellValueDateTime_AppColumn] FOREIGN KEY ([AppColumnId]) REFERENCES [dbo].[AppColumn] ([AppColumnId]),
    CONSTRAINT [FK_AppCellValueDateTime_DataSet] FOREIGN KEY ([AppDataSetId]) REFERENCES [dbo].[AppDataSet] ([AppDataSetId])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20160427-141050]
    ON [dbo].[AppCellValueDateTime]([AppDataSetId] ASC, [AppRowId] ASC, [AppColumnId] ASC);

