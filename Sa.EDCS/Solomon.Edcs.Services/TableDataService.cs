﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using Solomon.Edcs.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.Edcs.Utility;

namespace Solomon.Edcs.Services
{
    public class TableDataService : BaseService
    {
        public bool Save(TableData data)
        {
            using (var uow = new UnitOfWork())
            {
                var manager = new AppCellValueDateTimeManager(uow);
                foreach (var cell in data.AppCellValueDateTimeList)
                {                    
                    var dbCell = manager.GetAll(x => x.AppColumnId == cell.AppColumnId && x.AppRowId == cell.AppRowId && x.AppDataSetId == cell.AppDataSetId).FirstOrDefault();

                    if (dbCell == null)
                    {
                        if (cell.CellValue != null && cell.CellValue.Value > DateTime.MinValue)
                        {
                            manager.Add(cell);
                        }
                    }
                    else
                    {
                        if (cell.CellValue == null)
                        {
                            manager.Delete(dbCell);
                        }
                        else
                        {
                            dbCell.CellValue = cell.CellValue;
                        }
                    }
                }

                var deciMalManager = new AppCellValueDecimalManager(uow);
                foreach (var cell in data.AppCellValueDecimalList)
                {

                    var dbCell = deciMalManager.GetAll(x => x.AppColumnId == cell.AppColumnId && x.AppRowId == cell.AppRowId && x.AppDataSetId == cell.AppDataSetId).FirstOrDefault();

                    if (dbCell == null)
                    {
                        if (cell.CellValue != null && cell.CellValue.Value > 0)
                        {
                            deciMalManager.Add(cell);
                        }
                    }
                    else
                    {
                        if (cell.CellValue == null)
                        {
                            deciMalManager.Delete(dbCell);
                        }
                        else
                        {
                            dbCell.CellValue = cell.CellValue;
                        }
                    }
                }
                var intManager = new AppCellValueIntManager(uow);
                foreach (var cell in data.AppCellValueIntList)
                {
                    var dbCell = intManager.GetAll(x => x.AppColumnId == cell.AppColumnId && x.AppRowId == cell.AppRowId && x.AppDataSetId == cell.AppDataSetId).FirstOrDefault();

                    if (dbCell == null)
                    {
                        if (cell.CellValue != null && cell.CellValue.Value > 0)
                        {
                            intManager.Add(cell);
                        }
                    }
                    else
                    {
                        if (cell.CellValue == null)
                        {
                            intManager.Delete(dbCell);
                        }
                        else
                        {
                            dbCell.CellValue = cell.CellValue;
                        }
                    }
                }
                var textManager = new AppCellValueTextManager(uow);
                foreach (var cell in data.AppCellValueTextList)
                {
                    var dbCell = textManager.GetAll(x => x.AppColumnId == cell.AppColumnId && x.AppRowId == cell.AppRowId && x.AppDataSetId == cell.AppDataSetId).FirstOrDefault();

                    if (dbCell == null)
                    {
                        if (!string.IsNullOrWhiteSpace(cell.CellValue))
                        {
                            textManager.Add(cell);
                        }
                    }
                    else
                    {
                        if (cell.CellValue == null)
                        {
                            textManager.Delete(dbCell);
                        }
                        else
                        {
                            dbCell.CellValue = cell.CellValue;
                        }
                    }
                }

                return uow.SaveChanges();
            }
        }

        public string CheckValidation(TableData data)
        {
            StringBuilder sb = new StringBuilder();
            using (var uow = new UnitOfWork())
            {
                var ruleAssociationManager = new AppRuleAssociationManager(uow);
                var columnManager = new AppColumnManager(uow);
                foreach (var cell in data.AppCellValueDateTimeList)
                {
                    CheckValidation(sb, ruleAssociationManager, columnManager, cell);
                }

                foreach (var cell in data.AppCellValueDecimalList)
                {
                    CheckValidation(sb, ruleAssociationManager, columnManager, cell);
                }

                foreach (var cell in data.AppCellValueIntList)
                {
                    CheckValidation(sb, ruleAssociationManager, columnManager, cell);
                }

                foreach (var cell in data.AppCellValueTextList)
                {
                    CheckValidation(sb, ruleAssociationManager, columnManager, cell);
                }

            }

            return sb.ToString();
        }

        private static void CheckValidation(StringBuilder sb, AppRuleAssociationManager ruleAssociationManager, AppColumnManager columnManager, dynamic cell)
        {
            int columnId = cell.AppColumnId;
            var ruleAssociationList = ruleAssociationManager.GetAll(x => x.AppColumnId == columnId).ToList();

            //Filter out all rules related to  Aggregate rule
            var rules = ruleAssociationList.GroupBy(x => x.AppRule).ToList();
            var otherRules = ruleAssociationList.GroupBy(x => x.AppRuleId).ToList();

            var dataSet = new AppDataSetService().GetById(cell.AppDataSetId);
            foreach (var ruleAssociation in ruleAssociationList)
            {
                if ((ruleAssociation.AppRowId.HasValue && ruleAssociation.AppRowId != cell.AppRowId)
                    || (ruleAssociation.YearId.HasValue && ruleAssociation.YearId.Value != dataSet.Year)
                    || (ruleAssociation.ClientId.HasValue && ruleAssociation.ClientId.Value != dataSet.Client))
                {
                    continue;
                }
                else
                {
                    if (ruleAssociation.AppRule.IsActive)
                    {
                        foreach (var rule in ruleAssociation.AppRule.AppRuleDetails)
                        {
                            if (rule.ValidationTypeId == 1 && (cell.CellValue == null))
                            {
                                SetRequiredFieldMsg(sb, columnManager, cell.AppColumnId);
                            }
                            if (rule.ValidationTypeId == 2)
                            {
                                if (cell.CellValue == null)
                                {
                                    SetRequiredFieldMsg(sb, columnManager, cell.AppColumnId);
                                }
                                else
                                {
                                    CheckComparisonValidation(sb, columnManager, cell, rule);
                                }
                            }
                            if (rule.ValidationTypeId == 5)
                            {
                                if (cell.CellValue == null)
                                {
                                    SetRequiredFieldMsg(sb, columnManager, cell.AppColumnId);
                                }
                                else
                                {
                                    CheckAggregateValidation(sb, columnManager, cell, rule, ruleAssociationList);
                                }
                            }
                        }
                    }
                }
            }
        }

        

        private static void CheckComparisonValidation(StringBuilder sb, AppColumnManager columnManager, dynamic cell, AppRuleDetail rule)
        {
            if (rule.ComparisonValue != null && rule.ComparisonValueDataTypeId > 1)
            {
                CheckFixedValueComparison(sb, columnManager, cell, rule);
            }
            if (rule.ComparisonColumnId.HasValue && rule.ComparisonColumnId.Value > 0)
            {
                CheckTableValueComparison(sb, columnManager, cell, rule, cell.CellValue);
            }
        }

        private static void CheckAggregateValidation(StringBuilder sb, AppColumnManager columnManager, dynamic cell, AppRuleDetail rule, List<AppRuleAssociation> ruleAssociations)
        {   
            if (rule.ComparisonValue != null && rule.ComparisonValueDataTypeId > 1)
            {
                CheckFixedValueAggregateValidation(sb, columnManager, cell, rule, ruleAssociations);
            }
            if (rule.ComparisonColumnId.HasValue && rule.ComparisonColumnId.Value > 0)
            {
                dynamic compareSource = GetAggregateCellValue(cell, rule, ruleAssociations);
                CheckTableValueComparison(sb, columnManager, cell, rule, compareSource);
            }
        }

        private static void CheckFixedValueAggregateValidation(StringBuilder sb, AppColumnManager columnManager, dynamic cell, AppRuleDetail rule, List<AppRuleAssociation> ruleAssociations)
        {
            if (rule.ComparisonValue != null)
            {
                dynamic compareWith = 0;
                if (rule.ComparisonValueDataTypeId == 2)
                {
                    compareWith = rule.ComparisonValue.ToDecimal();
                }

                if (rule.ComparisonValueDataTypeId == 3)
                {
                    compareWith = rule.ComparisonValue.ToInt();
                }

                if (rule.ComparisonValueDataTypeId == 4)
                {
                    compareWith = rule.ComparisonValue.ToDateTime();
                }

                var columnName = columnManager.GetById(cell.AppColumnId).DisplayName;
                dynamic compareSource = GetAggregateCellValue(cell, rule, ruleAssociations);

                if (rule.ComparisonValueDataTypeId > 1)
                {
                    CompareValue(sb, rule, compareSource, compareWith, columnName);
                }
            }
        }

        private static dynamic GetAggregateCellValue(dynamic cell, AppRuleDetail rule, List<AppRuleAssociation> ruleAssociations)
        {

            var rowsForAggregation = ruleAssociations.Where(x => x.AppRuleId == rule.AppRuleId && x.AppRowId != null).Select(y => new { y.AppRowId }).ToList();
            
            
            
            dynamic total = null;

            int datasetId = cell.AppDataSetId;
            int columnId = cell.AppColumnId;
            int currentRow = cell.AppRowId;

            if (cell is AppCellValueInt)
            {
                if (rowsForAggregation.Count > 0)
                {
                    total = new AppCellValueIntService().FindBy(x => x.AppDataSetId == datasetId && x.AppColumnId == columnId && x.AppRowId != currentRow)
                        .Join(rowsForAggregation, c => c.AppRowId, r => r.AppRowId, (c, r) => new { c.CellValue }).Sum(y => y.CellValue);
                }
                else
                {
                    total = new AppCellValueIntService().FindBy(x => x.AppDataSetId == datasetId && x.AppColumnId == columnId && x.AppRowId != currentRow).Sum(y => y.CellValue);
                }
                total += cell.CellValue;
            }

            if (cell is AppCellValueDecimal)
            {
                if (rowsForAggregation.Count > 0)
                {
                    total = new AppCellValueDecimalService().FindBy(x => x.AppDataSetId == datasetId && x.AppColumnId == columnId && x.AppRowId != currentRow)
                        .Join(rowsForAggregation, c => c.AppRowId, r => r.AppRowId, (c, r) => new { c.CellValue }).Sum(y => y.CellValue);
                }
                else
                {
                    total = new AppCellValueDecimalService().FindBy(x => x.AppDataSetId == datasetId && x.AppColumnId == columnId && x.AppRowId != currentRow).Sum(y => y.CellValue);
                }
                total += cell.CellValue;
            }

            return total;
        }

        private static void CheckFixedValueComparison(StringBuilder sb, AppColumnManager columnManager, dynamic cell, AppRuleDetail rule)
        {
            if (rule.ComparisonValue != null)
            {
                dynamic compareWith = 0;
                if (rule.ComparisonValueDataTypeId == 2)
                {
                    compareWith = rule.ComparisonValue.ToDecimal();
                }

                if (rule.ComparisonValueDataTypeId == 3)
                {
                    compareWith = rule.ComparisonValue.ToInt();
                }

                if (rule.ComparisonValueDataTypeId == 4)
                {
                   compareWith = rule.ComparisonValue.ToDateTime();
                }

                var columnName = columnManager.GetById(cell.AppColumnId).DisplayName;
                dynamic cellValue = cell.CellValue;
                if (rule.ComparisonValueDataTypeId > 1)
                {
                     CompareValue(sb, rule, cellValue, compareWith, columnName);
                }
            }
        }

        private static void CheckTableValueComparison(StringBuilder sb, AppColumnManager columnManager, dynamic cell, AppRuleDetail rule, dynamic compareSource)
        {
            if (rule.ComparisonColumnId.HasValue && rule.ComparisonColumnId.Value > 0)
            {
                var appDataSetService = new AppDataSetService();

                var currentDataset = appDataSetService.GetById(cell.AppDataSetId);

                int compareYear = rule.YearId.HasValue ? rule.YearId.Value : currentDataset.Year;
                int compareClient = rule.ClientId.HasValue ? rule.ClientId.Value : currentDataset.Client;

                //var relatedDatasets = appDataSetService.FindBy(x => x.Study == 1 && x.Year == compareYear && x.Client == compareClient).ToList();

                dynamic compareWith = 0;
                int compareRow = 0;
                var columnName = columnManager.GetById(cell.AppColumnId).DisplayName;

                int compareWithDatasetId = cell.AppDataSetId;

                if (rule.ComparisonDatasetId.HasValue && rule.ComparisonDatasetId.Value > 0)
                {
                    compareWithDatasetId = rule.ComparisonDatasetId.Value;


                }

                if (rule.ComparisonRowId.HasValue)
                {
                    compareRow = rule.ComparisonRowId.Value;
                }
                else
                {
                    compareRow = cell.AppRowId;
                }

                compareWith = GetCompareWithValue(sb, cell, rule, compareRow, compareWithDatasetId);

                

                if (compareWith != null)
                {
                    if (rule.ComparisonPercent.HasValue)
                    {
                        compareWith = rule.ComparisonPercent * compareWith / 100;
                    }

                    CompareValue(sb, rule, compareSource, compareWith, columnName);
                }

                //if (relatedDatasets == null || relatedDatasets.Count < 1)
                //{
                //    int currentDatasetId = currentDataset.AppDataSetId;
                //    if (rule.ComparisonRowId.HasValue)
                //    {
                //        compareRow = rule.ComparisonRowId.Value;
                //    }
                //    else
                //    {
                //        compareRow = cell.AppRowId;
                //    }
                //    compareWith = GetCompareWithValue(cell, rule, compareRow, currentDatasetId);

                //    if (compareWith != null)
                //    {
                //        CompareValue(sb, rule, compareSource, compareWith, columnName);
                //    }
                //}
                //else
                //{
                //    foreach (var dataset in relatedDatasets)
                //    {
                //        if (rule.ComparisonRowId.HasValue)
                //        {
                //            compareRow = rule.ComparisonRowId.Value;
                //        }
                //        else
                //        {
                //            compareRow = cell.AppRowId;
                //        }
                //        compareWith = GetCompareWithValue(cell, rule, compareRow, dataset.AppDataSetId);
                //        if (compareWith != null)
                //        {
                //            CompareValue(sb, rule, compareSource, compareWith, columnName);
                //        }
                //    }
                //}
            }
        }

        //private static void CheckTableValueAggregateComparison(StringBuilder sb, AppColumnManager columnManager, dynamic cell, AppRuleDetail rule, dynamic compareSource)
        //{
        //    if (rule.ComparisonColumnId.HasValue && rule.ComparisonColumnId.Value > 0)
        //    {
        //        //AppCellValueInt actualCell = (AppCellValueInt)cell;

        //        var appDataSetService = new AppDataSetService();

        //        var currentDataset = appDataSetService.GetById(cell.AppDataSetId);

        //        int compareYear = rule.YearId.HasValue ? rule.YearId.Value : currentDataset.Year;
        //        int compareClient = rule.ClientId.HasValue ? rule.ClientId.Value : currentDataset.Client;

        //        var relatedDatasets = appDataSetService.FindBy(x => x.Study == 1 && x.Year == compareYear && x.Client == compareClient).ToList();
                
        //        dynamic compareWith = 0;
        //        int compareRow = 0;
        //        var columnName = columnManager.GetById(cell.AppColumnId).DisplayName;

        //        if (relatedDatasets == null || relatedDatasets.Count < 1)
        //        {
        //            int currentDatasetId = currentDataset.AppDataSetId;
        //            if (rule.ComparisonRowId.HasValue)
        //            {
        //                compareRow = rule.ComparisonRowId.Value;
        //            }
        //            else
        //            {
        //                compareRow = cell.AppRowId;
        //            }
                    

        //            if (compareWith != null)
        //            {
        //                CompareValue(sb, rule, compareSource, compareWith, columnName);
        //            }
        //        }
        //        else
        //        {
        //            foreach (var dataset in relatedDatasets)
        //            {
        //                if (rule.ComparisonRowId.HasValue)
        //                {
        //                    compareRow = rule.ComparisonRowId.Value;
        //                }
        //                else
        //                {
        //                    compareRow = cell.AppRowId;
        //                }
                        
        //                if (compareWith != null)
        //                {
        //                    CompareValue(sb, rule, compareSource, compareWith, columnName);
        //                }
        //            }
        //        }
        //    }
        //}

        private static dynamic GetCompareWithValue(StringBuilder sb, dynamic cell, AppRuleDetail rule, int compareRow, int datasetId)
        {
            dynamic compareWith = null;

            if (cell is AppCellValueInt)
            {
                var result = new AppCellValueIntService().FindBy(x => x.AppDataSetId == datasetId && x.AppColumnId == rule.ComparisonColumnId && x.AppRowId == compareRow).FirstOrDefault();
                if (result == null)
                {
                    sb.Append(string.Format("Compare With value for Rule Id: {0} is null\n", rule.AppRuleId));
                }
                else
                {
                    compareWith = result.CellValue.HasValue ? result.CellValue.Value : 0;
                }
            }

            if (cell is AppCellValueDecimal)
            {
                var result = new AppCellValueDecimalService().FindBy(x => x.AppDataSetId == datasetId && x.AppColumnId == rule.ComparisonColumnId && x.AppRowId == compareRow).FirstOrDefault();
                if (result == null)
                {
                    sb.Append(string.Format("Compare With value for Rule Id: {0} is null\n", rule.AppRuleId));
                }
                else
                {
                    compareWith = result.CellValue.HasValue ? result.CellValue.Value : 0m;
                }
            }

            if (cell is AppCellValueDateTime)
            {
                var result = new AppCellValueDateTimeService().FindBy(x => x.AppDataSetId == datasetId && x.AppColumnId == rule.ComparisonColumnId && x.AppRowId == compareRow).FirstOrDefault();
                if (result == null)
                {
                    sb.Append(string.Format("Compare With value for Rule Id: {0} is null\n", rule.AppRuleId));
                }
                else
                {
                    compareWith = result.CellValue.HasValue ? result.CellValue.Value : DateTime.MinValue;
                }
            }

            return compareWith;
        }

        private static void CompareValue(StringBuilder sb, AppRuleDetail rule, dynamic compareSource, dynamic compareWith, string columnName)
        {
            if (rule.ComparisonOperatorTypeId.HasValue)
            {
                if (rule.ComparisonOperatorTypeId.Value == 1 && compareSource != compareWith)
                {
                    sb.Append(string.Format("Column \"{0}\" value should be equal to {1}\n", columnName, compareWith));
                }

                if (rule.ComparisonOperatorTypeId.Value == 2 && !(compareSource < compareWith))
                {
                    sb.Append(string.Format("Column \"{0}\" value should be less than {1}\n", columnName, compareWith));
                }

                if (rule.ComparisonOperatorTypeId.Value == 3 && !(compareSource > compareWith))
                {
                    sb.Append(string.Format("Column \"{0}\" value should be greater than {1}\n", columnName, compareWith));
                }

                if (rule.ComparisonOperatorTypeId.Value == 4 && !(compareSource <= compareWith))
                {
                    sb.Append(string.Format("Column \"{0}\" value should be less than or equal  to {1}\n", columnName, compareWith));
                }

                if (rule.ComparisonOperatorTypeId.Value == 5 && !(compareSource >= compareWith))
                {
                    sb.Append(string.Format("Column \"{0}\" value should be greater than or equal to {1}\n", columnName, compareWith));
                }
            }
        }

        private static void SetRequiredFieldMsg(StringBuilder sb, AppColumnManager columnManager, int columnId)
        {
            sb.Append(string.Format("Column \"{0}\" is required\n", columnManager.GetById(columnId).DisplayName));
        }
    }
}
