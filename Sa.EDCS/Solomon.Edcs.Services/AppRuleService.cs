﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using Solomon.Edcs.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Services
{
    public partial class AppRuleService
    {
        public bool Save(AppRule rule)
        {
            using (var uow = new UnitOfWork())
            {
                var manager = new AppRuleManager(uow);
                if (rule.AppRuleId == 0)
                {
                    manager.Add(rule);
                }
                else
                {
                    var dbRule = manager.GetById(rule.AppRuleId);
                    
                    dbRule.CreateDate = rule.CreateDate;
                    dbRule.CreatedBy = rule.CreatedBy;
                    dbRule.DisplayName = rule.DisplayName;
                    dbRule.IsActive = rule.IsActive;
                    dbRule.Name = rule.Name;
                    dbRule.UpdateDate = rule.UpdateDate;
                    dbRule.UpdatedBy = rule.UpdatedBy;
                }

                return uow.SaveChanges();
            }
        }
    }
}
