﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using Solomon.Edcs.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Services
{
    public partial class AppCellValueDateTimeService
    {
        public bool Save(AppCellValueDateTime cell)
        {
            using (var uow = new UnitOfWork())
            {
                var manager = new AppCellValueDateTimeManager(uow);
                var dbCell = manager.GetAll(x => x.AppColumnId == cell.AppColumnId && x.AppRowId == cell.AppRowId && x.AppDataSetId == cell.AppDataSetId).FirstOrDefault();

                if (dbCell == null)
                {
                    if (cell.CellValue != null && cell.CellValue.Value > DateTime.MinValue)
                    {
                        manager.Add(cell);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (cell.CellValue == null)
                    {
                        manager.Delete(dbCell);
                    }
                    else
                    {
                        dbCell.CellValue = cell.CellValue;
                    }
                }

                return uow.SaveChanges();
            }
        }
    }
}
