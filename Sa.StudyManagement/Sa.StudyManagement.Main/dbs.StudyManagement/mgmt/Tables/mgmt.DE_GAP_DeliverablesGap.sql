﻿CREATE TABLE [mgmt].[DE_GAP_DeliverablesGap]
(
	[Id] INT NOT NULL PRIMARY KEY,

	[DeliverableId]			INT,

	[PlantId]				INT,
	[GroupId]				INT,
	[SortKey]				INT
);