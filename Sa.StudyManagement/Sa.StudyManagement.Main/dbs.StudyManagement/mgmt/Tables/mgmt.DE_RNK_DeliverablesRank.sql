﻿CREATE TABLE [mgmt].[DE_RNK_DeliverablesRank]
(
	[Id] INT NOT NULL PRIMARY KEY,

	[DeliverableId]			INT,

	[PlantId]				INT,
	[GroupId]				INT,
	[SortKey]				INT
);