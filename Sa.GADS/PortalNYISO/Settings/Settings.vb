'********************************************************************************
' Settings.vb

'Copyright (C) 2011  The Outercurve Foundation

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; either version 2
'of the License, or (at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

' Ron Fluegge
' GADS Open Source Project Coordinator
' Coordinator@GADSOpenSource.com
' 972-625-5653                                                  
'********************************************************************************

Option Strict On
Option Explicit On 

Imports System.Xml              'for XmlTextReader
Imports System.IO
Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class Settings

#Region " Class Declarations "

    'class member variables
    Private m_sXMLFileName As String
    Private m_xmlDocument As XmlDocument
    'class private variables
    Private Const NOTFOUND As String = "<<nothing>>"

#End Region

#Region " Class Constructors "

    'on instantiation, save XML filename and open the XmlDocument
    Public Sub New(ByVal AppName As String)

        'default to appname.xml in app.path
        'XMLFileName = CurrentDirectory & "\" & AppName & ".xml"

        ' to be used primarily by the Windows UI

        XMLFileName = Application.StartupPath & "\" & AppName & ".xml"

        _OpenXMLFile()

    End Sub

    'SettingsFile specifies filename & optional path, path defaults to CurrentDirectory, ByVal reader As StreamReader
    Public Sub New(ByVal AppName As String, ByVal SettingsFile As String, ByRef myreader As StreamReader)

        XMLFileName = AppName & ".xml"
        _XBAPOpenXMLFile(myreader)

    End Sub

    'SettingsFile specifies filename & optional path, path defaults to CurrentDirectory
    Public Sub New(ByVal AppName As String, ByVal SettingsFile As String)

        ' to be used primarily by the WEB UI

        ' AppName parameter here makes constructor signature unique, but it is NOT used here

        If SettingsFile.IndexOf("\") > 0 Then
            'entire path and file passed
            XMLFileName = SettingsFile
        ElseIf SettingsFile.IndexOf("/") > 0 Then
            'entire path and file passed
            XMLFileName = SettingsFile
        Else
            'assume only filename passed
            'XMLFileName = CurrentDirectory & "\" & SettingsFile
            XMLFileName = SettingsFile
        End If

        _OpenXMLFile()

    End Sub

#End Region

#Region " Class Public Properties "

    'stores the open XML file's name
    Public Property XMLFileName() As String

        Get
            Return m_sXMLFileName
        End Get

        Set(ByVal Value As String)
            m_sXMLFileName = Value.Trim
        End Set

    End Property

#End Region

#Region " Class Public Methods "


    'deletes a specified key, deleting with empty KeyName deletes the section
    Public Sub DeleteKey(ByVal SectionName As String, ByVal KeyName As String)

        'Dim sKeyValue As String
        Dim xnSection As XmlNode
        Dim xnKey As XmlNode

        xnSection = m_xmlDocument.SelectSingleNode("//Section[@Name='" & SectionName & "']")

        If xnSection Is Nothing Then
            Exit Sub
        Else
            If KeyName.Length = 0 Then

                'delete the section
                Dim xnRoot As XmlNode = m_xmlDocument.DocumentElement
                xnRoot.RemoveChild(xnSection)

            Else

                'delete the key
                xnKey = xnSection.SelectSingleNode("descendant::Key[@Name='" & KeyName & "']")

                If xnKey Is Nothing Then
                    Exit Sub
                Else
                    xnSection.RemoveChild(xnKey)
                End If

            End If
        End If

        m_xmlDocument.Save(XMLFileName)
        xnKey = Nothing
        xnSection = Nothing

    End Sub

    'returns a string array containing all section names in the XML file
    Public Function GetSectionNames() As String()

        'Dim sSectionName As String
        Dim sSections() As String = Nothing
        Dim iCnt As Integer
        Dim xnSections As XmlNodeList
        Dim xnSection As XmlNode

        xnSections = m_xmlDocument.SelectNodes("//Section")

        If Not xnSections Is Nothing Then

            ReDim sSections(xnSections.Count - 1)

            For Each xnSection In xnSections
                sSections(iCnt) = xnSection.Attributes("Name").Value
                iCnt += 1
            Next

        End If

        xnSection = Nothing
        xnSections = Nothing
        Return sSections

    End Function

    'returns a 2-dim string array containing all keys and values in the specified section
    Public Function GetSettingAll(ByVal SectionName As String) As String(,)

        Dim sKeys(,) As String = Nothing
        Dim iCnt As Integer
        Dim xnSection As XmlNode
        Dim xnKey As XmlNode
        Dim xnKeys As XmlNodeList

        xnSection = m_xmlDocument.SelectSingleNode("//Section[@Name='" & SectionName & "']")

        If Not xnSection Is Nothing Then

            xnKeys = xnSection.SelectNodes("descendant::Key")

            If Not xnKeys Is Nothing Then

                ReDim sKeys(xnKeys.Count - 1, 1)

                For Each xnKey In xnKeys
                    sKeys(iCnt, 0) = xnKey.Attributes("Name").Value
                    sKeys(iCnt, 1) = xnKey.Attributes("Value").Value
                    iCnt += 1
                Next

            End If

        End If

        xnKeys = Nothing
        xnSection = Nothing
        Return sKeys

    End Function

    'find the matching SectionName/KeyName & return the value as Integer
    Public Function GetSettingInt(ByVal SectionName As String, ByVal KeyName As String, ByVal DefaultValue As Integer) As Integer

        Dim iKeyValue As Integer
        Dim sKeyValue As String

        sKeyValue = _GetSetting(SectionName, KeyName)

        If sKeyValue = NOTFOUND Then
            iKeyValue = DefaultValue
        Else
            Try
                iKeyValue = CType(sKeyValue, Integer)
            Catch
                'return zero if non-integer value found
                iKeyValue = 0
            End Try
        End If

        Return iKeyValue

    End Function

    'find the matching SectionName/KeyName & return the value as String
    Public Function GetSettingStr(ByVal SectionName As String, ByVal KeyName As String, ByVal DefaultValue As String) As String

        Dim sKeyValue As String

        sKeyValue = _GetSetting(SectionName, KeyName)

        If sKeyValue = NOTFOUND Then sKeyValue = DefaultValue

        Return sKeyValue

    End Function

    'saves a string setting value, creates the section and key if it doesn't exist
    Public Sub SaveSettingStr(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As String)

        _SaveSetting(SectionName, KeyName, Setting)

    End Sub

    Public Sub SaveSettingStr(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As String, ByRef info As Stream)

        _XBAPSaveSetting(SectionName, KeyName, Setting, info)

    End Sub

    'saves an integer setting value, creates the section and key if it doesn't exist
    Public Sub SaveSettingInt(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As Integer)

        Dim sSetting As String

        sSetting = CType(Setting, String)

        _SaveSetting(SectionName, KeyName, sSetting)

    End Sub

    Public Sub SaveSettingInt(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As Integer, ByRef info As Stream)

        Dim sSetting As String

        sSetting = CType(Setting, String)

        _XBAPSaveSetting(SectionName, KeyName, sSetting, info)

    End Sub

#End Region

#Region " Class Private Methods "

    'returns setting as string from XML file, NOTFOUND constant if it doesn't exist
    Private Function _GetSetting(ByVal SectionName As String, ByVal KeyName As String) As String

        Dim sKeyValue As String
        Dim xnSection As XmlNode
        Dim xnKey As XmlNode

        xnSection = m_xmlDocument.SelectSingleNode("//Section[@Name='" & SectionName & "']")

        If xnSection Is Nothing Then
            sKeyValue = NOTFOUND
        Else
            xnKey = xnSection.SelectSingleNode("descendant::Key[@Name='" & KeyName & "']")

            If xnKey Is Nothing Then
                sKeyValue = NOTFOUND
            Else
                sKeyValue = xnKey.Attributes("Value").Value
            End If

        End If

        xnKey = Nothing
        xnSection = Nothing
        Return sKeyValue

    End Function

    'loads the XML settings file 
    Private Sub _OpenXMLFile()

        Dim myreader As XmlTextReader = Nothing

        Try
            myreader = New XmlTextReader(XMLFileName)
            myreader.WhitespaceHandling = WhitespaceHandling.None
            m_xmlDocument = New XmlDocument

            m_xmlDocument.Load(myreader)

        Catch e As System.Exception

            ExceptionManager.Publish(e)

        Finally

            If Not (myreader Is Nothing) Then
                myreader.Close()
                myreader = Nothing
            End If

        End Try

    End Sub

    Private Sub _XBAPOpenXMLFile(ByRef myreader As StreamReader)


        Try
            m_xmlDocument = New XmlDocument

            m_xmlDocument.Load(myreader)

        Catch e As System.Exception

            ExceptionManager.Publish(e)

        Finally

            If Not (myreader Is Nothing) Then
                myreader.Close()
                myreader = Nothing
            End If

        End Try

    End Sub

    'saves a Key/Value, creating the Section and Keyname if required
    Private Sub _SaveSetting(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As String)

        Dim xnSection As XmlNode
        Dim xnKey As XmlNode
        Dim xnAttr As XmlAttribute

        'check the document exists, create if not
        If m_xmlDocument.DocumentElement Is Nothing Then
            Try
                m_xmlDocument.LoadXml("<?xml version=""1.0"" encoding=""UTF-8""?>" & ControlChars.CrLf & _
                 "<ApplicationSettings>" & ControlChars.CrLf & "</ApplicationSettings>")
            Catch e1 As System.Exception
                ExceptionManager.Publish(e1)
            End Try
        End If

        xnSection = m_xmlDocument.SelectSingleNode("//Section[@Name='" & SectionName & "']")

        'check the Section exists, create if not
        If xnSection Is Nothing Then
            Try
                'create the new Section node...
                xnSection = m_xmlDocument.CreateNode(XmlNodeType.Element, "Section", "")

                'add the Name attribute
                xnAttr = m_xmlDocument.CreateAttribute("Name")
                xnAttr.Value = SectionName
                xnSection.Attributes.SetNamedItem(xnAttr)

                'get the root XML node and add the new node to the document
                Dim xnRoot As XmlNode = m_xmlDocument.DocumentElement
                xnRoot.AppendChild(xnSection)
                xnRoot = Nothing

            Catch e2 As System.Exception
                ExceptionManager.Publish(e2)
            End Try
        End If

        xnKey = xnSection.SelectSingleNode("descendant::Key[@Name='" & KeyName & "']")

        'check the Key exists, create if not
        If xnKey Is Nothing Then
            Try
                'create the new Key node...
                xnKey = m_xmlDocument.CreateNode(XmlNodeType.Element, "Key", "")

                'add the Name attribute
                xnAttr = m_xmlDocument.CreateAttribute("Name")
                xnAttr.Value = KeyName
                xnKey.Attributes.SetNamedItem(xnAttr)

                'add the Value attribute
                xnAttr = m_xmlDocument.CreateAttribute("Value")
                xnAttr.Value = Setting
                xnKey.Attributes.SetNamedItem(xnAttr)

                'add the new node to its Section
                xnSection.AppendChild(xnKey)

            Catch e3 As System.Exception
                ExceptionManager.Publish(e3)
            End Try
        Else
            xnKey.Attributes("Value").Value = Setting
        End If

        'save changes
        Try
            m_xmlDocument.Save(XMLFileName)
        Catch e4 As System.Exception
            MsgBox(e4.Message.ToString)
            ExceptionManager.Publish(e4)
        End Try

        xnKey = Nothing
        xnSection = Nothing

    End Sub

    Private Sub _XBAPSaveSetting(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As String, ByRef info As Stream)

        Dim xnSection As XmlNode
        Dim xnKey As XmlNode
        Dim xnAttr As XmlAttribute

        'check the document exists, create if not
        If m_xmlDocument.DocumentElement Is Nothing Then
            Try
                m_xmlDocument.LoadXml("<?xml version=""1.0"" encoding=""UTF-8""?>" & ControlChars.CrLf & _
                 "<ApplicationSettings>" & ControlChars.CrLf & "</ApplicationSettings>")
            Catch e1 As System.Exception
                ExceptionManager.Publish(e1)
            End Try
        End If

        xnSection = m_xmlDocument.SelectSingleNode("//Section[@Name='" & SectionName & "']")

        'check the Section exists, create if not
        If xnSection Is Nothing Then
            Try
                'create the new Section node...
                xnSection = m_xmlDocument.CreateNode(XmlNodeType.Element, "Section", "")

                'add the Name attribute
                xnAttr = m_xmlDocument.CreateAttribute("Name")
                xnAttr.Value = SectionName
                xnSection.Attributes.SetNamedItem(xnAttr)

                'get the root XML node and add the new node to the document
                Dim xnRoot As XmlNode = m_xmlDocument.DocumentElement
                xnRoot.AppendChild(xnSection)
                xnRoot = Nothing

            Catch e2 As System.Exception
                ExceptionManager.Publish(e2)
            End Try
        End If

        xnKey = xnSection.SelectSingleNode("descendant::Key[@Name='" & KeyName & "']")

        'check the Key exists, create if not
        If xnKey Is Nothing Then
            Try
                'create the new Key node...
                xnKey = m_xmlDocument.CreateNode(XmlNodeType.Element, "Key", "")

                'add the Name attribute
                xnAttr = m_xmlDocument.CreateAttribute("Name")
                xnAttr.Value = KeyName
                xnKey.Attributes.SetNamedItem(xnAttr)

                'add the Value attribute
                xnAttr = m_xmlDocument.CreateAttribute("Value")
                xnAttr.Value = Setting
                xnKey.Attributes.SetNamedItem(xnAttr)

                'add the new node to its Section
                xnSection.AppendChild(xnKey)

            Catch e3 As System.Exception
                ExceptionManager.Publish(e3)
            End Try
        Else
            xnKey.Attributes("Value").Value = Setting
        End If

        'save changes
        Try
     
            Dim fStream As FileStream = File.Create("http://gadsopensource.com/xbap/gadsng.xml")
            m_xmlDocument.Save(fStream)
        Catch e4 As System.Exception
            MsgBox(e4.Message.ToString)
            ExceptionManager.Publish(e4)
        End Try

        xnKey = Nothing
        xnSection = Nothing

    End Sub

#End Region

End Class
