'********************************************************************************
' SettingsException.vb

'Copyright (C) 2011  The Outercurve Foundation

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; either version 2
'of the License, or (at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

' Ron Fluegge
' GADS Open Source Project Coordinator
' Coordinator@GADSOpenSource.com
' 972-625-5653                                                  
'********************************************************************************

Option Strict On
Option Explicit On 

Imports System.Xml              'for XmlTextReader
Imports System.Diagnostics      'for Debug
Imports System.Environment      'for CurrentDirectory
Imports System.Runtime.Serialization
Imports Microsoft.ApplicationBlocks.ExceptionManagement

<Serializable()> _
    Public Class SettingsException : Inherits BaseApplicationException

    ' default constructor
    Public Sub New()
        MyBase.New()
    End Sub

    'constructor with exception message
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ' constructor with message and inner exception
    Public Sub New(ByVal message As String, ByVal inner as System.Exception)
        MyBase.New(message, inner)
    End Sub

    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class


