using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Xml;
using System.Windows.Forms;
using System.Collections;
using GADSNG.Base;
using BusinessLayer.BusinessLayer;
using SetupSettings;

namespace GADSDataInterfaces
    {
    /// <summary>
    /// Summary description for WriteASCIIData.
    /// </summary>
    public class WriteASCIIData
        {

        private enum GrossNetOrBoth
            {
           
            GrossOnly, NetOnly, Both
       
            }
        protected string WriteFileType;

        public WriteASCIIData()
            {
            //
            // TODO: Add constructor logic here
            //
            }

        /// <summary>
        /// This function writes out either the NERC, ISO-NE, MISO, or NYISO GADS ASCII data file
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="intChoice"></param>
        /// <param name="intYear"></param>
        /// <param name="intPeriod"></param>
        /// <param name="strWho"></param>
        /// <param name="intNoOfColumns"></param>
        /// <returns>A status string related to the output file creation</returns>
        public static string WriteNERCFile(string strFileName, int intChoice, int intYear, int intPeriod, string strWho, int intNoOfColumns, string connect, string myUser, DataTable dtISOSetup, GADSNGBusinessLayer blConnect)
            {
            // strFileName is the file name of the text file to write to

            // intChoice 1 - Year-to-date - intPeriod = ending month
            //           2 - Quarterly    - intPeriod = quarter
            //           3 - Monthly      - intPeriod = specific month
            // intChoice is NOT 0-based, but is 1-based so that the number corresponds to either the ending month, quarter or specific month

            // intYear is the Year

            // intPeriod is the ending month, which quarter, or which specific month (see intChoice above)

            // strWho is the ISO or NERC and has the following values:  NERC, MISO, ISONE, NYISO

            // intNoOfColumns is either 82 (default) or 100 or 80.  The number of columns in each GADS record.
            // Kept the old 80-column record format in case someone is still using that format for internal use.

            /*
             * ----------------------------------------------------------
             * email from Mike Curley on February 13, 2003 at 3:35 PM
             * 
             * (1) I prefer a copy of all units year-to-date whenever the data is sent to GADS.  This is for several reasons:
             * 
             * (a) too many times if one plant sends corrections instead of the home office, the entire database
             * for that utility is overwritten and data is lost.  Unfortunately, that has happened more than once and I 
             * don't have the time or energy to ask for additional copies of lost data.  
             * 
             * (b) when the data is sent year-to-date for all units, my database and that at the utility remain equal.  It 
             * is a hassle to send data for units with no errors, but in the long run, it is beneficial to all.
             * 
             * (2) I like two large files sent:  one event records and one performance.  If that is not possible, then one
             * file with both events and performance.  I can sort them into two groups here.
             * 
             * ----------------------------------------------------------
             * email from Jim Pratico (NYISO) on February 14, 2002 at 9:37 AM
             * 
             * I prefer Companies with multiple plants to send me their data in one file (Perf & Event).  Most companies send
             * me their data in two separate files (one perf, one event).  I would never want to see a separate file(s) for each generating
             * unit.  As far as revisions go:  I would like the revised data for each generator to be a year-to-date file.
             * 
             * ----------------------------------------------------------
             * 
             * Changes to the GADS Data Reporting Instructions Effective January 1, 2010
             * 
             * For events that continue into the new year (page III-25) removed all reference to reporting an �XX� in the start and end date.
             * 
            */

            int i;
            int intSHCalc;
            int intSH;
            int intRSH;
            int intPump;
            int intSynch;
            int intAH;
            int intPOH;
            int intFOH;
            int intMOH;
            int intSEH;
            int intUAH;
            int intPH;
            int intIH;
            decimal decValue;
            int intValue;

            string stringFolder;
            string stringFileName;
            string stringPerfFile;
            string stringEventFile;
            string stringMISOFile;
            string stringException;
            bool lError;
            lError = false;
            bool lProcessPerf1;
            bool lProcessPerf2;
            bool lProcessPerf3;
            bool lProcessPerf4;
            bool lInternalOnly;
            Settings mySettings = new Settings("GADSNG");
            string strAllNERCData = mySettings.GetSettingStr("WRITENERC", "AllNERCData", "False");
            bool boolAllNERCData = false;

            switch (strAllNERCData.ToUpper())
                {
                case "TRUE":
                    boolAllNERCData = true;
                    break;
                case "FALSE":
                    boolAllNERCData = false;
                    break;
                default:
                    boolAllNERCData = false;
                    break;
                }

            stringFileName = "Invalid file name";

            stringPerfFile = strFileName;
            stringEventFile = strFileName;

            i = strFileName.LastIndexOf(@"\");

            string strTestFile;
            strTestFile = "Test.txt";

            if (i > 0)
                {
                // strFileName has at least one \ -- minimum should be x:\xxxevtnn.dbf

                // typically stringFolder will be x:\yyyy\zzzzz\
                stringFolder = strFileName.Substring(0, i);

                if (Directory.Exists(stringFolder) == false)
                    {
                    return "ERROR -- " + stringFolder + " is not a valid directory";
                    }

                if (stringFolder.EndsWith(@"\") == false)
                    {
                    stringFolder += @"\";
                    }

                strTestFile = stringFolder + "Test.Txt";
                }
            else
                {
                i = strFileName.LastIndexOf(@":");

                if (i > 0)
                    {
                    stringFolder = strFileName.Substring(0, i) + @":\";

                    if (Directory.Exists(stringFolder) == false)
                        {
                        return "ERROR -- " + stringFolder + " is not a valid directory";
                        }

                    strTestFile = stringFolder + "Test.Txt";
                    }
                }

            // This creates a "temporary" TestFileWriter so that the Finally use compiles
            StreamWriter TestFileWriter = new StreamWriter(strTestFile);
            TestFileWriter.WriteLine("Testing on " + System.DateTime.Now.ToLongDateString());
            TestFileWriter.Close();

            try
                {
                // Test to see if the entered strFileName is a valid file name
                TestFileWriter = new StreamWriter(strFileName, false);
                TestFileWriter.WriteLine("Testing on " + System.DateTime.Now.ToLongDateString());
                }
            catch (ArgumentException e)
                {
                lError = true;
                stringException = e.ToString();
                stringFileName = "Missing File Name";
                }
            catch (DirectoryNotFoundException e)
                {
                lError = true;
                stringException = e.ToString();
                stringFileName = "Directory not found";
                }
            catch (IOException e)
                {
                lError = true;
                stringException = e.ToString();
                stringFileName = "Invalid Syntax";
                }
            catch (Exception e)
                {
                lError = true;
                stringException = e.ToString();
                stringFileName = "General Exception or Invalid file name";
                }
            finally
                {
                TestFileWriter.Close();
                }

            if (lError == true)
                {
                return "ERROR -- " + stringFileName;
                }

            // NERC prefers a performance data file and a separate event data file

            i = strFileName.LastIndexOf(@"\");

            if (i > 0)
                {
                // strFileName has at least one \ -- minimum should be x:\xxxevtnn.dbf

                // stringFileName will be *.*
                stringFileName = strFileName.Substring(i + 1);

                // typically stringFolder will be x:\yyyy\zzzzz\
                stringFolder = strFileName.Substring(0, i);

                if (Directory.Exists(stringFolder) == false)
                    {
                    return "ERROR -- " + stringFolder + " is not a valid directory";
                    }

                if (stringFolder.EndsWith(@"\") == false)
                    {
                    stringFolder += @"\";
                    }
                stringPerfFile = stringFolder + "Perf_" + stringFileName;
                stringEventFile = stringFolder + "Event_" + stringFileName;
                stringMISOFile = stringFolder + "Combined_" + stringFileName;
                }
            else if (strFileName.LastIndexOf(@":") > 0)
                {
                i = strFileName.LastIndexOf(@":");

                stringFileName = strFileName.Substring(i + 1);

                stringFolder = strFileName.Substring(0, i) + @":\";

                if (Directory.Exists(stringFolder) == false)
                    {
                    return "ERROR -- " + stringFolder + " is not a valid directory";
                    }

                stringPerfFile = stringFolder + "Perf_" + stringFileName;
                stringEventFile = stringFolder + "Event_" + stringFileName;
                stringMISOFile = stringFolder + "Combined_" + stringFileName;
                }
            else
                {
                stringFileName = strFileName;
                stringFolder = "";
                stringPerfFile = "Perf_" + strFileName;
                stringEventFile = "Event_" + strFileName;
                stringMISOFile = "Combined_" + stringFileName;
                }

            StreamWriter EventFileWriter;
            try
                {
                EventFileWriter = new StreamWriter(stringEventFile, false);
                }
            catch (System.IO.IOException eIO)
                {
                stringException = eIO.ToString();
                return "ERROR -- " + stringEventFile + " is being used by another application";
                }
            catch (Exception e)
                {
                stringException = e.ToString();
                return "ERROR -- Could not create " + stringEventFile;
                // throw(e);
                }
            EventFileWriter.AutoFlush = true;

            StreamWriter PerfFileWriter;
            try
                {
                PerfFileWriter = new StreamWriter(stringPerfFile, false);
                }
            catch (System.IO.IOException eIO)
                {
                stringException = eIO.ToString();
                return "ERROR -- " + stringPerfFile + " is being used by another application";
                }
            catch (Exception e)
                {
                stringException = e.ToString();
                return "ERROR -- Could not create " + stringPerfFile;
                // throw(e);
                }
            PerfFileWriter.AutoFlush = true;

            StringBuilder PerfStringBuilder;
            StringBuilder EventStringBuilder;

            if (intNoOfColumns != 82 && intNoOfColumns != 80 && intNoOfColumns != 100)
                {
                intNoOfColumns = 82;
                }
            PerfStringBuilder = new StringBuilder(0, intNoOfColumns);
            EventStringBuilder = new StringBuilder(0, intNoOfColumns);

            if (intNoOfColumns == 100)
                {
                PerfStringBuilder = new StringBuilder(0, 125);
                EventStringBuilder = new StringBuilder(0, 82);
                }

            bool lResult;
            string strUnit;

            Regex l_regex = new Regex("[0-9]{2}");
            Regex dt_regex = new Regex("[0-9]{8}");

            //			string connect = "DataEntry";
            // GADSNGBusinessLayer blConnect = new GADSNGBusinessLayer(connect);
            
            lResult = true;

            BusinessLayer.Setup.SetupDataTable dtSetup;
            

            if (myUser.Trim().ToUpper() == "DASHBOARD")
                {
                if (dtISOSetup == null)
                    {
                    dtSetup = blConnect.GetSelectedUtilityUnitData(strWho, myUser);
                    }
                else
                    {
                    dtSetup = (BusinessLayer.Setup.SetupDataTable)dtISOSetup;
                    }
                }
            else
                {
                dtSetup = blConnect.GetSelectedUtilityUnitData(strWho, myUser);
                }

            BusinessLayer.Performance.PerformanceDataDataTable dtPerformance;

            BusinessLayer.AllEventData dsEvents = new BusinessLayer.AllEventData();

            try
                {
                // Process this unit's data

                foreach (BusinessLayer.Setup.SetupRow rowSetup in dtSetup.Rows)
                    {
                    //Application.DoEvents();

                    strUnit = rowSetup.UtilityUnitCode;

                    // get the performance and event datatables

                    dtPerformance = blConnect.GetPerformanceData(strUnit, intYear);
                    dsEvents = blConnect.GetBothEventSets(strUnit, intYear);


                    /*
                    * ORIGINAL -
                    * 
                    * To delete a single card (other than 01 card), repeat columns 1-12 (80-column format)
                    * as previously reported, enter an "X" in column 13 and enter the card number of the
                    * record to be deleted in columns 79 and 80.
                    * 
                    * To delete an entire report, repeat columns 1-12 (80-column format) as previously reported
                    * and enter an "X" in column 13.  Leave columns 79 and 80 blank.  ALL records (cards) for
                    * that report period will be deleted.
                    * 
                    * October 2002 -
                    * 
                    * To delete data from one or more data fields, GADS recommends that you resubmit the entire
                    * data set -- year-to-date -- for that unit (or all units you report) to GADS.  This 
                    * procedure will insure that both you and the GADS database have the same records on file.
                    * You have the option to find the record that has the highest revision code and then
                    * increase this number by 1 or set all revision codes back to zero.
                    * 
                    */

                    // intChoice 1 - Year-to-date - intPeriod = ending month
                    //           2 - Quarterly    - intPeriod = quarter
                    //           3 - Monthly      - intPeriod = specific month
                    // intChoice is NOT 0-based, but is 1-based so that the number corresponds to either the ending month, quarter or specific month

                    // intYear is the Year AND IS ALREADY SET.  THE DATASET HAS ONLY THE intYear DATA!!!

                    // intPeriod is the ending month, which quarter, or which specific month (see intChoice above)

                    // strWho is the ISO or NERC and has the following values:  NERC, ISONE, MISO, NYISO

                    // MISO is the NYISO subset in 05/07 format with Event and Performance in a single YTD ASCII file

                    // intNoOfColumns is either 82 (default) or 100 or 80.  The number of columns in each GADS record.
                    // Kept the old 80-column record format in case someone is still using that format for internal use.

                    /*
                     * Email from Jim Pratico, NYISO, on October 6, 2005
                     * 
                     * Beginning with the submission of September 2005 GADS data -- a second event
                     * record will be required for all events submitted to the NYISO.  The required
                     * data, specific to the NYISO, has been amended.  See attached file (Required
                     * GADS_NYISO.pdf).  The NYISO now requires that a cause code be entered for
                     * all events.  
                    */


                    //TODO see handling REV on page 18 of GADS80CL

                    #region Performance Data
                    try
                        {
                        foreach (BusinessLayer.Performance.PerformanceDataRow rowPerf in dtPerformance.Rows)
                            {
                            //Application.DoEvents();

                            if (rowPerf.Period.StartsWith("X") == true)
                                {
                                // NERC GADS originally allowed performance data to be entered for the "quarter".
                                // The quarterly periods were entered as X1, X2, X3 and X4.
                                // This section simply redefines the "quarter" to the ending month.

                                if (rowPerf.Period.EndsWith("1") == true)
                                    rowPerf.Period = "03";
                                else if (rowPerf.Period.EndsWith("2") == true)
                                    rowPerf.Period = "06";
                                else if (rowPerf.Period.EndsWith("3") == true)
                                    rowPerf.Period = "09";
                                else if (rowPerf.Period.EndsWith("4") == true)
                                    rowPerf.Period = "12";

                                rowPerf.AcceptChanges();
                                }

                            lProcessPerf1 = false;
                            lProcessPerf2 = false;
                            lProcessPerf3 = false;
                            lProcessPerf4 = false;

                            switch (intChoice)
                                {
                                case 1:

                                    // Year-to-date - intPeriod = ending month

                                    if (Convert.ToInt16(rowPerf.Period) <= intPeriod)
                                        {
                                        lProcessPerf1 = true;
                                        lProcessPerf2 = true;
                                        lProcessPerf3 = true;
                                        lProcessPerf4 = true;
                                        }
                                    break;

                                case 2:

                                    // Quarterly    - intPeriod = quarter

                                    if (Convert.ToInt16(rowPerf.Period) > ((intPeriod - 1) * 3) & Convert.ToInt16(rowPerf.Period) <= (intPeriod * 3))
                                        {
                                        lProcessPerf1 = true;
                                        lProcessPerf2 = true;
                                        lProcessPerf3 = true;
                                        lProcessPerf4 = true;
                                        }
                                    else
                                        {
                                        if (rowPerf.RevMonthCard1.Month > ((intPeriod - 1) * 3) & rowPerf.RevMonthCard1.Month <= (intPeriod * 3))
                                            lProcessPerf1 = true;

                                        if (rowPerf.RevMonthCard2.Month > ((intPeriod - 1) * 3) & rowPerf.RevMonthCard2.Month <= (intPeriod * 3))
                                            lProcessPerf2 = true;

                                        if (rowPerf.RevMonthCard3.Month > ((intPeriod - 1) * 3) & rowPerf.RevMonthCard3.Month <= (intPeriod * 3))
                                            lProcessPerf3 = true;

                                        if (rowPerf.RevMonthCard4.Month > ((intPeriod - 1) * 3) & rowPerf.RevMonthCard4.Month <= (intPeriod * 3))
                                            lProcessPerf4 = true;
                                        }
                                    break;

                                case 3:

                                    // Monthly      - intPeriod = specific month

                                    if (Convert.ToInt16(rowPerf.Period) == intPeriod)
                                        {
                                        lProcessPerf1 = true;
                                        lProcessPerf2 = true;
                                        lProcessPerf3 = true;
                                        lProcessPerf4 = true;
                                        }
                                    else
                                        {
                                        if (rowPerf.RevMonthCard1.Month == intPeriod)
                                            lProcessPerf1 = true;

                                        if (rowPerf.RevMonthCard2.Month == intPeriod)
                                            lProcessPerf2 = true;

                                        if (rowPerf.RevMonthCard3.Month == intPeriod)
                                            lProcessPerf3 = true;

                                        if (rowPerf.RevMonthCard4.Month == intPeriod)
                                            lProcessPerf4 = true;
                                        }
                                    break;

                                default:

                                    break;
                                }

                            if (lProcessPerf1 == true || lProcessPerf2 == true || lProcessPerf3 == true || lProcessPerf4 == true)
                                {
                                /*
                                 * ----------------------------------
                                 *  Performance 01 record
                                 * ----------------------------------
                                */

                                if (lProcessPerf1 == true)
                                    {
                                    PerfStringBuilder.Length = 0;
                                    if (intNoOfColumns == 100)
                                        {
                                        PerfStringBuilder.Insert(0, "05");
                                        }
                                    else
                                        {
                                        PerfStringBuilder.Insert(0, 95);
                                        }
                                    PerfStringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6, '0'));
                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                        {
                                        // 82-column data files have a 4-digit year
                                        PerfStringBuilder.Append(rowPerf.Year.ToString());
                                        }
                                    else
                                        {
                                        // 80-column data files have the 2-digit year -- historical GADS layout
                                        PerfStringBuilder.Append(rowPerf.Year.ToString().Substring(2, 2));
                                        }

                                    PerfStringBuilder.Append(rowPerf.Period);

                                    if (intChoice == 1)
                                        {
                                        // Year-to-date
                                        PerfStringBuilder.Append("0");
                                        }
                                    else
                                        {
                                        PerfStringBuilder.Append(rowPerf.RevisionCard1);
                                        }

                                    if (intNoOfColumns == 82 || intNoOfColumns == 80)
                                        {
                                        if ((strWho.ToUpper() == "NERC" && boolAllNERCData) || (strWho.ToUpper() == "ISONE" && boolAllNERCData) || rowSetup.GrossNetBoth == (byte)GrossNetOrBoth.GrossOnly)
                                            {
                                            if (rowPerf.IsGrossMaxCapNull() == false)
                                                {
                                                if (rowPerf.GrossMaxCap > 0)
                                                    PerfStringBuilder.Append(Convert.ToInt32(System.Math.Round(rowPerf.GrossMaxCap)).ToString().PadLeft(4, ' '));
                                                else
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                }

                                            if (rowPerf.IsGrossDepCapNull() == false)
                                                {
                                                if (rowPerf.GrossDepCap > 0)
                                                    PerfStringBuilder.Append(Convert.ToInt32(System.Math.Round(rowPerf.GrossDepCap)).ToString().PadLeft(4, ' '));
                                                else
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                }

                                            if (rowPerf.IsGrossGenNull() == false)
                                                {
                                                if (rowPerf.GrossMaxCap == 0 & rowPerf.GrossDepCap == 0 & rowPerf.GrossGen == 0)
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                                else
                                                    PerfStringBuilder.Append(Convert.ToInt32(System.Math.Round(rowPerf.GrossGen)).ToString().PadLeft(7, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                                }

                                            }
                                        else
                                            {
                                            // NYISO, MISO, and ISO-NE do not want gross data ... NERC: it is voluntary

                                            PerfStringBuilder.Append(String.Empty.PadLeft(15, ' '));
                                            }

                                        if (rowPerf.IsNetMaxCapNull() == false)
                                            {
                                            if (rowPerf.NetMaxCap > 0)
                                                PerfStringBuilder.Append(Convert.ToInt32(System.Math.Round(rowPerf.NetMaxCap)).ToString().PadLeft(4, ' '));
                                            else
                                                PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }

                                        if (rowPerf.IsNetDepCapNull() == false)
                                            {
                                            if (rowPerf.NetDepCap > 0)
                                                PerfStringBuilder.Append(Convert.ToInt32(System.Math.Round(rowPerf.NetDepCap)).ToString().PadLeft(4, ' '));
                                            else
                                                PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }

                                        if (rowPerf.IsNetGenNull() == false)
                                            {
                                            if (rowPerf.NetMaxCap == 0 & rowPerf.NetDepCap == 0 & rowPerf.NetGen == 0)
                                                PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                            else
                                                PerfStringBuilder.Append(Convert.ToInt32(System.Math.Round(rowPerf.NetGen)).ToString().PadLeft(7, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                            }

                                        if ((strWho.ToUpper() == "NERC" && boolAllNERCData) || (strWho.ToUpper() == "ISONE" && boolAllNERCData))
                                            {
                                            if (rowPerf.IsTypUnitLoadingNull() == false)
                                                PerfStringBuilder.Append(rowPerf.TypUnitLoading.ToString());
                                            else
                                                PerfStringBuilder.Append(" ");
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(" ");
                                            }

                                        if (rowPerf.IsAttemptedStartsNull() == false)
                                            PerfStringBuilder.Append(rowPerf.AttemptedStarts.ToString().PadLeft(3, ' '));
                                        else
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));

                                        if (rowPerf.IsActualStartsNull() == false)
                                            PerfStringBuilder.Append(rowPerf.ActualStarts.ToString().PadLeft(3, ' '));
                                        else
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));

                                        if ((strWho.ToUpper() == "NERC" && boolAllNERCData) || (strWho.ToUpper() == "ISONE" && boolAllNERCData))
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));

                                            if (rowPerf.IsVerbalDescNull() == false)
                                                PerfStringBuilder.Append(rowPerf.VerbalDesc.ToUpper().PadRight(25, ' '));
                                            else
                                                PerfStringBuilder.Append(String.Empty.PadLeft(25, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(28, ' '));
                                            }
                                        }
                                    else // 05 format
                                        {
                                        if ((strWho.ToUpper() == "NERC" && boolAllNERCData) || (strWho.ToUpper() == "ISONE" && boolAllNERCData) || rowSetup.GrossNetBoth == (byte)GrossNetOrBoth.GrossOnly)
                                            {
                                            if (rowPerf.IsGrossMaxCapNull() == false)
                                                {
                                                if (rowPerf.GrossMaxCap > 0)
                                                    PerfStringBuilder.Append(Convert.ToInt32(100 * System.Math.Round(rowPerf.GrossMaxCap, 2)).ToString().PadLeft(6, ' '));
                                                else
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                }

                                            if (rowPerf.IsGrossDepCapNull() == false)
                                                {
                                                if (rowPerf.GrossDepCap > 0)
                                                    PerfStringBuilder.Append(Convert.ToInt32(100 * System.Math.Round(rowPerf.GrossDepCap, 2)).ToString().PadLeft(6, ' '));
                                                else
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                }

                                            if (rowPerf.IsGrossGenNull() == false)
                                                {
                                                if (rowPerf.GrossMaxCap == 0 & rowPerf.GrossDepCap == 0 & rowPerf.GrossGen == 0)
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(9, ' '));
                                                else
                                                    PerfStringBuilder.Append(Convert.ToInt32(100 * System.Math.Round(rowPerf.GrossGen, 2)).ToString().PadLeft(9, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(9, ' '));
                                                }

                                            }
                                        else
                                            {
                                            // NYISO, MISO and ISO-NE do not want gross data

                                            PerfStringBuilder.Append(String.Empty.PadLeft(21, ' '));
                                            }

                                        if (rowPerf.IsNetMaxCapNull() == false)
                                            {
                                            if (rowPerf.NetMaxCap > 0)
                                                PerfStringBuilder.Append(Convert.ToInt32(100 * System.Math.Round(rowPerf.NetMaxCap, 2)).ToString().PadLeft(6, ' '));
                                            else
                                                PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                            }

                                        if (rowPerf.IsNetDepCapNull() == false)
                                            {
                                            if (rowPerf.NetDepCap > 0)
                                                PerfStringBuilder.Append(Convert.ToInt32(100 * System.Math.Round(rowPerf.NetDepCap, 2)).ToString().PadLeft(6, ' '));
                                            else
                                                PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                            }

                                        if (rowPerf.IsNetGenNull() == false)
                                            {
                                            if (rowPerf.NetMaxCap == 0 & rowPerf.NetDepCap == 0 & rowPerf.NetGen == 0)
                                                PerfStringBuilder.Append(String.Empty.PadLeft(9, ' '));
                                            else
                                                PerfStringBuilder.Append(Convert.ToInt32(100 * System.Math.Round(rowPerf.NetGen, 2)).ToString().PadLeft(9, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(9, ' '));
                                            }

                                        if ((strWho.ToUpper() == "NERC" && boolAllNERCData) || (strWho.ToUpper() == "ISONE" && boolAllNERCData))
                                            {
                                            if (rowPerf.IsTypUnitLoadingNull() == false)
                                                PerfStringBuilder.Append(rowPerf.TypUnitLoading.ToString());
                                            else
                                                PerfStringBuilder.Append(" ");
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(" ");
                                            }

                                        if (rowPerf.IsAttemptedStartsNull() == false)
                                            PerfStringBuilder.Append(rowPerf.AttemptedStarts.ToString().PadLeft(3, ' '));
                                        else
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));

                                        if (rowPerf.IsActualStartsNull() == false)
                                            PerfStringBuilder.Append(rowPerf.ActualStarts.ToString().PadLeft(3, ' '));
                                        else
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));

                                        if ((strWho.ToUpper() == "NERC" && boolAllNERCData) || (strWho.ToUpper() == "ISONE" && boolAllNERCData))
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(34, ' '));

                                            if (rowPerf.IsVerbalDescNull() == false)
                                                PerfStringBuilder.Append(rowPerf.VerbalDesc.ToUpper().PadRight(25, ' '));
                                            else
                                                PerfStringBuilder.Append(String.Empty.PadLeft(25, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(59, ' '));
                                            }

                                        }

                                    PerfStringBuilder.Append("01");

                                    PerfFileWriter.WriteLine(PerfStringBuilder.ToString());
                                    }

                                /*
                                 * ----------------------------------
                                 *  Performance 02 record
                                 * ----------------------------------
                                */

                                if (lProcessPerf2 == true)
                                    {
                                    PerfStringBuilder.Length = 0;
                                    if (intNoOfColumns == 100)
                                        {
                                        PerfStringBuilder.Insert(0, "05");
                                        }
                                    else
                                        {
                                        PerfStringBuilder.Insert(0, 95);
                                        }
                                    PerfStringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6, '0'));
                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                        {
                                        // 82-column data files have a 4-digit year
                                        PerfStringBuilder.Append(rowPerf.Year.ToString());
                                        }
                                    else
                                        {
                                        // 80-column data files have the 2-digit year -- historical GADS layout
                                        PerfStringBuilder.Append(rowPerf.Year.ToString().Substring(2, 2));
                                        }

                                    PerfStringBuilder.Append(rowPerf.Period);

                                    if (intChoice == 1)
                                        {
                                        // Year-to-date
                                        PerfStringBuilder.Append("0");
                                        }
                                    else
                                        {
                                        PerfStringBuilder.Append(rowPerf.RevisionCard2);
                                        }

                                    if (intNoOfColumns == 82 || intNoOfColumns == 80)
                                        {
                                        // PJM/MISO require 2 decimal places for the hours, but NERC/NYISO/ISO-NE only want whole hours

                                        intSH = Convert.ToInt16(Decimal.Round(rowPerf.ServiceHours, 0));
                                        intRSH = Convert.ToInt16(Decimal.Round(rowPerf.RSHours, 0));
                                        intPump = Convert.ToInt16(Decimal.Round(rowPerf.PumpingHours, 0));
                                        intSynch = Convert.ToInt16(Decimal.Round(rowPerf.SynchCondHours, 0));
                                        intPOH = Convert.ToInt16(Decimal.Round(rowPerf.PlannedOutageHours, 0));
                                        intFOH = Convert.ToInt16(Decimal.Round(rowPerf.ForcedOutageHours, 0));
                                        intMOH = Convert.ToInt16(Decimal.Round(rowPerf.MaintOutageHours, 0));
                                        intSEH = Convert.ToInt16(Decimal.Round(rowPerf.ExtofSchedOutages, 0));
                                        intPH = Convert.ToInt16(Decimal.Round(rowPerf.PeriodHours, 0));

                                        // New field InactiveHours defined for 2006
                                        if (rowPerf.IsInactiveHoursNull())
                                            {
                                            rowPerf.InactiveHours = 0;
                                            }
                                        intIH = Convert.ToInt16(Decimal.Round(rowPerf.InactiveHours, 0));

                                        intSHCalc = intPH - intRSH - intPump - intSynch - intPOH - intFOH - intMOH - intSEH - intIH;

                                        if (intSH != intSHCalc)
                                            {
                                            decValue = (rowPerf.PeriodHours - rowPerf.RSHours
                                                - rowPerf.PumpingHours - rowPerf.SynchCondHours - rowPerf.PlannedOutageHours
                                                - rowPerf.ForcedOutageHours - rowPerf.MaintOutageHours - rowPerf.ExtofSchedOutages
                                                - rowPerf.InactiveHours);

                                            if ((rowPerf.ServiceHours == 0) && (decValue <= 0.01m))
                                                {
                                                if (intRSH > 0)
                                                    {
                                                    intRSH = intPH - intPump - intSynch - intPOH - intFOH - intMOH - intSEH - intIH;
                                                    }
                                                else if (intPOH > 0)
                                                    {
                                                    intPOH = intPH - intRSH - intPump - intSynch - intFOH - intMOH - intSEH - intIH;
                                                    }
                                                else if (intMOH > 0)
                                                    {
                                                    intMOH = intPH - intRSH - intPump - intSynch - intPOH - intFOH - intSEH - intIH;
                                                    }
                                                else if (intSEH > 0)
                                                    {
                                                    intSEH = intPH - intRSH - intPump - intSynch - intPOH - intFOH - intMOH - intIH;
                                                    }
                                                else if (intFOH > 0)
                                                    {
                                                    intFOH = intPH - intRSH - intPump - intSynch - intPOH - intMOH - intSEH - intIH;
                                                    }
                                                else if (intPump > 0)
                                                    {
                                                    intPump = intPH - intRSH - intSynch - intPOH - intFOH - intMOH - intSEH - intIH;
                                                    }
                                                else if (intSynch > 0)
                                                    {
                                                    intSynch = intPH - intRSH - intPump - intPOH - intFOH - intMOH - intSEH - intIH;
                                                    }
                                                else if (intIH > 0)
                                                    {
                                                    intRSH = intPH - intRSH - intPump - intSynch - intPOH - intFOH - intMOH - intSEH;
                                                    }

                                                intSH = 0;

                                                }
                                            else
                                                {
                                                intSH = intSHCalc;
                                                }
                                            }

                                        intAH = intSH + intRSH + intPump + intSynch;
                                        intUAH = intPOH + intFOH + intMOH + intSEH;

                                        PerfStringBuilder.Append(intSH.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intRSH.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intPump.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intSynch.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intAH.ToString().PadLeft(4, ' '));

                                        PerfStringBuilder.Append(intPOH.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intFOH.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intMOH.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intSEH.ToString().PadLeft(4, ' '));
                                        PerfStringBuilder.Append(intUAH.ToString().PadLeft(4, ' '));

                                        // Period Hours is now "Active Hours"; not just the period hours
                                        PerfStringBuilder.Append((intPH - intIH).ToString().PadLeft(4, ' '));

                                        // New field defined for 2006
                                        PerfStringBuilder.Append(intIH.ToString().PadLeft(4, ' '));

                                        PerfStringBuilder.Append(String.Empty.PadLeft(17, ' '));

                                        }
                                    else  // 05 record
                                        {
                                        // PJM/MISO require 2 decimal places for the hours, but NERC/NYISO/ISO-NE only want whole hours

                                        intSH = Convert.ToInt32(Decimal.Round(100 * rowPerf.ServiceHours, 0));
                                        intRSH = Convert.ToInt32(Decimal.Round(100 * rowPerf.RSHours, 0));
                                        intPump = Convert.ToInt32(Decimal.Round(100 * rowPerf.PumpingHours, 0));
                                        intSynch = Convert.ToInt32(Decimal.Round(100 * rowPerf.SynchCondHours, 0));
                                        intPOH = Convert.ToInt32(Decimal.Round(100 * rowPerf.PlannedOutageHours, 0));
                                        intFOH = Convert.ToInt32(Decimal.Round(100 * rowPerf.ForcedOutageHours, 0));
                                        intMOH = Convert.ToInt32(Decimal.Round(100 * rowPerf.MaintOutageHours, 0));
                                        intSEH = Convert.ToInt32(Decimal.Round(100 * rowPerf.ExtofSchedOutages, 0));
                                        intPH = Convert.ToInt32(Decimal.Round(100 * rowPerf.PeriodHours, 0));

                                        // New field InactiveHours defined for 2006
                                        if (rowPerf.IsInactiveHoursNull())
                                            {
                                            rowPerf.InactiveHours = 0;
                                            }
                                        intIH = Convert.ToInt32(Decimal.Round(100 * rowPerf.InactiveHours, 0));

                                        intSHCalc = intPH - intRSH - intPump - intSynch - intPOH - intFOH - intMOH - intSEH - intIH;

                                        if (intSH != intSHCalc)
                                            {
                                            intSH = intSHCalc;
                                            }

                                        intAH = intSH + intRSH + intPump + intSynch;
                                        intUAH = intPOH + intFOH + intMOH + intSEH;

                                        PerfStringBuilder.Append(intSH.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intRSH.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intPump.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intSynch.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intAH.ToString().PadLeft(5, ' '));

                                        PerfStringBuilder.Append(intPOH.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intFOH.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intMOH.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intSEH.ToString().PadLeft(5, ' '));
                                        PerfStringBuilder.Append(intUAH.ToString().PadLeft(5, ' '));

                                        // Period Hours is now "Active Hours"; not just the period hours
                                        PerfStringBuilder.Append((intPH - intIH).ToString().PadLeft(5, ' '));

                                        // New field defined for 2006
                                        PerfStringBuilder.Append(intIH.ToString().PadLeft(5, ' '));

                                        PerfStringBuilder.Append(String.Empty.PadLeft(48, ' '));

                                        }

                                    PerfStringBuilder.Append("02");

                                    PerfFileWriter.WriteLine(PerfStringBuilder.ToString());

                                    }

                                if ((strWho.ToUpper() == "NERC") || (strWho.ToUpper() == "ISONE"))
                                    {
                                    // MISO, NYISO and ISO-NE do not want fuel data

                                    /*
                                     * ----------------------------------
                                     *  Performance 03 record
                                     * ----------------------------------
                                    */

                                    if (lProcessPerf3 == true)
                                        {
                                        PerfStringBuilder.Length = 0;
                                        if (intNoOfColumns == 100)
                                            {
                                            PerfStringBuilder.Insert(0, "05");
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Insert(0, 95);
                                            }
                                        PerfStringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6, '0'));
                                        if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                            {
                                            // 82-column data files have a 4-digit year
                                            PerfStringBuilder.Append(rowPerf.Year.ToString());
                                            }
                                        else
                                            {
                                            // 80-column data files have the 2-digit year -- historical GADS layout
                                            PerfStringBuilder.Append(rowPerf.Year.ToString().Substring(2, 2));
                                            }

                                        PerfStringBuilder.Append(rowPerf.Period);

                                        if (intChoice == 1)
                                            {
                                            // Year-to-date
                                            PerfStringBuilder.Append("0");
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(rowPerf.RevisionCard3);
                                            }

                                        // Primary Fuel Data

                                        PerfStringBuilder.Append(rowPerf.PriFuelCode);

                                        if (rowPerf.PriFuelCode.ToUpper() == "NU")
                                            {
                                            // Leave columns 18-24 blank when reporting data for Nuclear units
                                            PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                            }
                                        else
                                            {
                                            if (rowPerf.IsPriQtyBurnedNull() == false && boolAllNERCData)
                                                {
                                                decValue = rowPerf.PriQtyBurned * 100.00m;
                                                intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                PerfStringBuilder.Append(intValue.ToString().PadLeft(7, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                                }
                                            }

                                        if (rowPerf.IsPriAvgHeatContentNull() == false && boolAllNERCData)
                                            PerfStringBuilder.Append(rowPerf.PriAvgHeatContent.ToString().PadLeft(6, ' '));
                                        else
                                            PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));

                                        if (rowPerf.IsPriPercentAshNull() == false && rowPerf.PriPercentAsh > 0 && boolAllNERCData)
                                            {
                                            decValue = rowPerf.PriPercentAsh * 10.00m;
                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                            }

                                        if (rowPerf.IsPriPercentMoistureNull() == false && rowPerf.PriPercentMoisture > 0 && boolAllNERCData)
                                            {
                                            decValue = rowPerf.PriPercentMoisture * 10.00m;
                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                            }

                                        if (rowPerf.IsPriPercentSulfurNull() == false && rowPerf.PriPercentSulfur > 0 && boolAllNERCData)
                                            {

                                            decValue = rowPerf.PriPercentSulfur * 10.00m;
                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(2, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                            }

                                        if (rowPerf.IsPriPercentAlkalinesNull() == false && rowPerf.PriPercentAlkalines > 0 && boolAllNERCData)
                                            {
                                            decValue = rowPerf.PriPercentAlkalines * 10.00m;
                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                            }

                                        if (rowPerf.IsPriGrindIndexVanadNull() == false && rowPerf.PriGrindIndexVanad > 0 && boolAllNERCData)
                                            {
                                            if (rowPerf.PriFuelCode.ToUpper() == "CC" || rowPerf.PriFuelCode.ToUpper() == "LI")
                                                {
                                                decValue = rowPerf.PriGrindIndexVanad;
                                                }
                                            else if (rowPerf.PriFuelCode.ToUpper() == "OO" || rowPerf.PriFuelCode.ToUpper() == "KE" || rowPerf.PriFuelCode.ToUpper() == "JP" || rowPerf.PriFuelCode.ToUpper() == "DI")
                                                {
                                                decValue = rowPerf.PriGrindIndexVanad * 10.00m;
                                                }
                                            else
                                                {
                                                decValue = 0.00m;
                                                }

                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                            }

                                        if (rowPerf.IsPriAshSoftTempNull() == false && rowPerf.PriAshSoftTemp > 0 && boolAllNERCData)
                                            {
                                            PerfStringBuilder.Append(rowPerf.PriAshSoftTemp.ToString().PadLeft(4, ' '));
                                            }
                                        else
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }

                                        if (intNoOfColumns == 100)
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(21, ' '));
                                            }

                                        // Secondary Fuel Data

                                        if (rowPerf.IsSecFuelCodeNull() == false && boolAllNERCData)
                                            {
                                            if (rowPerf.SecFuelCode.Trim() != String.Empty)
                                                {

                                                PerfStringBuilder.Append(rowPerf.SecFuelCode);

                                                if (intNoOfColumns == 100)
                                                    {
                                                    if (rowPerf.IsSecQtyBurnedNull() == false)
                                                        {
                                                        decValue = rowPerf.SecQtyBurned * 100.00m;
                                                        intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                        PerfStringBuilder.Append(intValue.ToString().PadLeft(7, ' '));
                                                        }
                                                    else
                                                        {
                                                        PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                                        }
                                                    }
                                                else
                                                    {
                                                    if (rowPerf.IsSecQtyBurnedNull() == false)
                                                        {
                                                        decValue = rowPerf.SecQtyBurned * 100.00m;
                                                        intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                        PerfStringBuilder.Append(intValue.ToString().PadLeft(6, ' '));
                                                        }
                                                    else
                                                        {
                                                        PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                        }
                                                    }

                                                if (rowPerf.IsSecAvgHeatContentNull() == false)
                                                    {
                                                    PerfStringBuilder.Append(rowPerf.SecAvgHeatContent.ToString().PadLeft(6, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                    }

                                                if (rowPerf.IsSecPercentAshNull() == false && rowPerf.SecPercentAsh > 0)
                                                    {
                                                    decValue = rowPerf.SecPercentAsh * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsSecPercentMoistureNull() == false && rowPerf.SecPercentMoisture > 0)
                                                    {
                                                    decValue = rowPerf.SecPercentMoisture * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsSecPercentSulfurNull() == false && rowPerf.SecPercentSulfur > 0)
                                                    {
                                                    decValue = rowPerf.SecPercentSulfur * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(2, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                    }

                                                if (rowPerf.IsSecPercentAlkalinesNull() == false && rowPerf.SecPercentAlkalines > 0)
                                                    {
                                                    decValue = rowPerf.SecPercentAlkalines * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsSecGrindIndexVanadNull() == false && rowPerf.SecGrindIndexVanad > 0)
                                                    {
                                                    if (rowPerf.SecFuelCode.ToUpper() == "CC" || rowPerf.SecFuelCode.ToUpper() == "LI")
                                                        {
                                                        decValue = rowPerf.SecGrindIndexVanad;
                                                        }
                                                    else if (rowPerf.SecFuelCode.ToUpper() == "OO" || rowPerf.SecFuelCode.ToUpper() == "KE" || rowPerf.SecFuelCode.ToUpper() == "JP" || rowPerf.SecFuelCode.ToUpper() == "DI")
                                                        {
                                                        decValue = rowPerf.SecGrindIndexVanad * 10.00m;
                                                        }
                                                    else
                                                        {
                                                        decValue = 0.0m;
                                                        }

                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsSecAshSoftTempNull() == false && rowPerf.SecAshSoftTemp > 0)
                                                    {
                                                    PerfStringBuilder.Append(rowPerf.SecAshSoftTemp.ToString().PadLeft(4, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                    }
                                                }
                                            else
                                                {
                                                if (intNoOfColumns == 100)
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(33, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(32, ' '));
                                                    }
                                                }
                                            }
                                        else
                                            {
                                            if (intNoOfColumns == 100)
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(33, ' '));
                                                }
                                            else
                                                {
                                                PerfStringBuilder.Append(String.Empty.PadLeft(32, ' '));
                                                }
                                            }

                                        if (intNoOfColumns == 100)
                                            {
                                            PerfStringBuilder.Append(String.Empty.PadLeft(21, ' '));
                                            }

                                        PerfStringBuilder.Append("03");

                                        PerfFileWriter.WriteLine(PerfStringBuilder.ToString());

                                        }
                                    /*
                                     * ----------------------------------
                                     *  Performance 04 record
                                     * ----------------------------------
                                    */
                                    if (lProcessPerf4 == true && rowPerf.RevisionCard4.ToUpper() != "X" && boolAllNERCData)
                                        {
                                        if (rowPerf.IsTerFuelCodeNull() == false)
                                            {
                                            if (rowPerf.TerFuelCode.Trim() != String.Empty)
                                                {

                                                PerfStringBuilder.Length = 0;
                                                if (intNoOfColumns == 100)
                                                    {
                                                    PerfStringBuilder.Insert(0, "05");
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Insert(0, 95);
                                                    }
                                                PerfStringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6, '0'));
                                                if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                    {
                                                    // 82-column data files have a 4-digit year
                                                    PerfStringBuilder.Append(rowPerf.Year.ToString());
                                                    }
                                                else
                                                    {
                                                    // 80-column data files have the 2-digit year -- historical GADS layout
                                                    PerfStringBuilder.Append(rowPerf.Year.ToString().Substring(2, 2));
                                                    }

                                                PerfStringBuilder.Append(rowPerf.Period);

                                                if (intChoice == 1)
                                                    {
                                                    // Year-to-date
                                                    PerfStringBuilder.Append("0");
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(rowPerf.RevisionCard4);
                                                    }

                                                // Tertiary Fuel Data

                                                PerfStringBuilder.Append(rowPerf.TerFuelCode);

                                                if (rowPerf.IsTerQtyBurnedNull() == false)
                                                    {
                                                    decValue = rowPerf.TerQtyBurned * 100.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(7, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                                    }

                                                if (rowPerf.IsTerAvgHeatContentNull() == false)
                                                    {
                                                    PerfStringBuilder.Append(rowPerf.TerAvgHeatContent.ToString().PadLeft(6, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                    }

                                                if (rowPerf.IsTerPercentAshNull() == false && rowPerf.TerPercentAsh > 0)
                                                    {
                                                    decValue = rowPerf.TerPercentAsh * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsTerPercentMoistureNull() == false && rowPerf.TerPercentMoisture > 0)
                                                    {
                                                    decValue = rowPerf.TerPercentMoisture * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsTerPercentSulfurNull() == false && rowPerf.TerPercentSulfur > 0)
                                                    {
                                                    decValue = rowPerf.TerPercentSulfur * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(2, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                    }

                                                if (rowPerf.IsTerPercentAlkalinesNull() == false && rowPerf.TerPercentAlkalines > 0)
                                                    {
                                                    decValue = rowPerf.TerPercentAlkalines * 10.00m;
                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsTerGrindIndexVanadNull() == false && rowPerf.TerGrindIndexVanad > 0)
                                                    {
                                                    if (rowPerf.TerFuelCode.ToUpper() == "CC" || rowPerf.TerFuelCode.ToUpper() == "LI")
                                                        {
                                                        decValue = rowPerf.TerGrindIndexVanad;
                                                        }
                                                    else if (rowPerf.TerFuelCode.ToUpper() == "OO" || rowPerf.TerFuelCode.ToUpper() == "KE" || rowPerf.TerFuelCode.ToUpper() == "JP" || rowPerf.TerFuelCode.ToUpper() == "DI")
                                                        {
                                                        decValue = rowPerf.TerGrindIndexVanad * 10.00m;
                                                        }
                                                    else
                                                        {
                                                        decValue = 0.00m;
                                                        }

                                                    intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                    PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                    }

                                                if (rowPerf.IsTerAshSoftTempNull() == false && rowPerf.TerAshSoftTemp > 0)
                                                    {
                                                    PerfStringBuilder.Append(rowPerf.TerAshSoftTemp.ToString().PadLeft(4, ' '));
                                                    }
                                                else
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                    }

                                                if (intNoOfColumns == 100)
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(21, ' '));
                                                    }

                                                // Quaternary Fuel Data

                                                if (rowPerf.IsQuaFuelCodeNull() == false)
                                                    {
                                                    if (rowPerf.QuaFuelCode.Trim() != String.Empty)
                                                        {

                                                        if (rowPerf.IsQuaFuelCodeNull() == false)
                                                            {
                                                            PerfStringBuilder.Append(rowPerf.QuaFuelCode);
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                            }

                                                        if (intNoOfColumns == 100)
                                                            {
                                                            if (rowPerf.IsQuaQtyBurnedNull() == false)
                                                                {
                                                                decValue = rowPerf.QuaQtyBurned * 100.00m;
                                                                intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                                PerfStringBuilder.Append(intValue.ToString().PadLeft(7, ' '));
                                                                }
                                                            else
                                                                {
                                                                PerfStringBuilder.Append(String.Empty.PadLeft(7, ' '));
                                                                }
                                                            }
                                                        else
                                                            {
                                                            if (rowPerf.IsQuaQtyBurnedNull() == false)
                                                                {
                                                                decValue = rowPerf.QuaQtyBurned * 100.00m;
                                                                intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                                PerfStringBuilder.Append(intValue.ToString().PadLeft(6, ' '));
                                                                }
                                                            else
                                                                {
                                                                PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                                }
                                                            }

                                                        if (rowPerf.IsQuaAvgHeatContentNull() == false)
                                                            {
                                                            PerfStringBuilder.Append(rowPerf.QuaAvgHeatContent.ToString().PadLeft(6, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                            }

                                                        if (rowPerf.IsQuaPercentAshNull() == false && rowPerf.QuaPercentAsh > 0)
                                                            {
                                                            decValue = rowPerf.QuaPercentAsh * 10.00m;
                                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                            }

                                                        if (rowPerf.IsQuaPercentMoistureNull() == false && rowPerf.QuaPercentMoisture > 0)
                                                            {
                                                            decValue = rowPerf.QuaPercentMoisture * 10.00m;
                                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                            }

                                                        if (rowPerf.IsQuaPercentSulfurNull() == false && rowPerf.QuaPercentSulfur > 0)
                                                            {
                                                            decValue = rowPerf.QuaPercentSulfur * 10.00m;
                                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(2, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                            }

                                                        if (rowPerf.IsQuaPercentAlkalinesNull() == false && rowPerf.QuaPercentAlkalines > 0)
                                                            {
                                                            decValue = rowPerf.QuaPercentAlkalines * 10.00m;
                                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                            }

                                                        if (rowPerf.IsQuaGrindIndexVanadNull() == false && rowPerf.QuaGrindIndexVanad > 0)
                                                            {
                                                            if (rowPerf.QuaFuelCode.ToUpper() == "CC" || rowPerf.QuaFuelCode.ToUpper() == "LI")
                                                                {
                                                                decValue = rowPerf.QuaGrindIndexVanad;
                                                                }
                                                            else if (rowPerf.QuaFuelCode.ToUpper() == "OO" || rowPerf.QuaFuelCode.ToUpper() == "KE" || rowPerf.QuaFuelCode.ToUpper() == "JP" || rowPerf.QuaFuelCode.ToUpper() == "DI")
                                                                {
                                                                decValue = rowPerf.QuaGrindIndexVanad * 10.00m;
                                                                }
                                                            else
                                                                {
                                                                decValue = 0.00m;
                                                                }

                                                            intValue = Convert.ToInt32(Decimal.Round(decValue, 0));
                                                            PerfStringBuilder.Append(intValue.ToString().PadLeft(3, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(3, ' '));
                                                            }

                                                        if (rowPerf.IsQuaAshSoftTempNull() == false && rowPerf.QuaAshSoftTemp > 0)
                                                            {
                                                            PerfStringBuilder.Append(rowPerf.QuaAshSoftTemp.ToString().PadLeft(4, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                            }
                                                        }
                                                    else
                                                        {
                                                        if (intNoOfColumns == 100)
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(33, ' '));
                                                            }
                                                        else
                                                            {
                                                            PerfStringBuilder.Append(String.Empty.PadLeft(32, ' '));
                                                            }
                                                        }
                                                    }
                                                else
                                                    {
                                                    if (intNoOfColumns == 100)
                                                        {
                                                        PerfStringBuilder.Append(String.Empty.PadLeft(33, ' '));
                                                        }
                                                    else
                                                        {
                                                        PerfStringBuilder.Append(String.Empty.PadLeft(32, ' '));
                                                        }
                                                    }

                                                if (intNoOfColumns == 100)
                                                    {
                                                    PerfStringBuilder.Append(String.Empty.PadLeft(21, ' '));
                                                    }

                                                PerfStringBuilder.Append("04");

                                                PerfFileWriter.WriteLine(PerfStringBuilder.ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    catch (System.Data.StrongTypingException e)
                        {
                        stringException = e.ToString();
                        lResult = false;
                        // MessageBox.Show(e.ToString());
                        }
                    #endregion

                    #region Event Data
                    try
                        {

                        foreach (BusinessLayer.AllEventData.EventData01Row EventDriver in dsEvents.Tables["EventData01"].Rows)
                            {

                            if (EventDriver.CauseCode == 7777 ||
                                (EventDriver.CauseCode >= 9180 && EventDriver.CauseCode <= 9199))
                                {
                                lInternalOnly = true;
                                }
                            else if ((EventDriver.EventType == "PU" || EventDriver.EventType == "CO") &&
                                      strWho.ToUpper() != "ISONE")
                                {
                                // Dealing with special event types or cause codes that are NOT NERC GADS compliant
                                //    PU = Pumping events
                                //    CO = Synch Condensing events
                                // BUT are "required" by ISONE beginning in 2007

                                lInternalOnly = true;
                                }
                            else
                                {
                                lInternalOnly = false;
                                }

                            if (lInternalOnly == false)
                                {

                                //Application.DoEvents();

                                switch (intChoice)
                                    {
                                    case 1:

                                        // Year-to-date - intPeriod = ending month

                                        if (EventDriver.StartDateTime.Month <= intPeriod)
                                            {
                                            // The event starts in the year-to-date period

                                            if (EventDriver.RevisionCard01.ToUpper() != "X")
                                                {
                                                // ... and is not deleted.  

                                                // process the 01 and 02/03 cards 

                                                EventStringBuilder.Length = 0;
                                                if (intNoOfColumns == 100)
                                                    {
                                                    EventStringBuilder.Insert(0, "07");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Insert(0, 97);
                                                    }
                                                EventStringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6, '0'));
                                                if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                    {
                                                    // 82-column data files have a 4-digit year
                                                    EventStringBuilder.Append(EventDriver.Year.ToString());
                                                    }
                                                else
                                                    {
                                                    // 80-column data files have the 2-digit year -- historical GADS layout
                                                    EventStringBuilder.Append(EventDriver.Year.ToString().Substring(2, 2));
                                                    }

                                                EventStringBuilder.Append(EventDriver.EventNumber.ToString().PadLeft(4, '0'));

                                                // Year-to-date
                                                EventStringBuilder.Append("0");

                                                EventStringBuilder.Append(EventDriver.EventType);

                                                if (EventDriver.IsCarryOverLastYearNull())
                                                    {
                                                    EventDriver.CarryOverLastYear = false;
                                                    }

                                                if (EventDriver.CarryOverLastYear == true && EventDriver.StartDateTime.ToString("MMddHHmm") == "01010000")
                                                    {
                                                    // The event started in the prior year; therefore, the Start of Event date/time
                                                    // should be 010100XX
                                                    //EventStringBuilder.Append("010100XX");
                                                    EventStringBuilder.Append("01010000"); // 2010 Change
                                                    }
                                                else if (Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                                    // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                                    // "re-constructing" the 2400 as midnight

                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.StartDateTime.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    // Any other time

                                                    EventStringBuilder.Append(EventDriver.StartDateTime.ToString("MMddHHmm"));
                                                    }


                                                if (EventDriver.IsChangeDateTime1Null() == true || EventDriver.IsChangeInEventType1Null() == true)
                                                    {
                                                    /*
                                                    * 
                                                    * - the First Change in Event date/time is empty or the First Change in 
                                                    *   Event Type field is empty -- either way cannot process.
                                                    *
                                                    */

                                                    EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));

                                                    }
                                                else
                                                    {
                                                    // First Change in Event date/time IS NOT empty

                                                    if (EventDriver.ChangeDateTime1.Month > intPeriod)
                                                        {
                                                        /*
                                                        * First Change in Event date/time is NOT empty, but
                                                        * is later than the period of interest, so make it blank
                                                        * 
                                                        */

                                                        EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));
                                                        }
                                                    else
                                                        {
                                                        // Fill in First Change in Event date/time

                                                        if (Convert.ToDouble(EventDriver.ChangeDateTime1.ToString("HHmmss.fff")) >= 235959.000)
                                                            {
                                                            // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                                            // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                                            // "re-constructing" the 2400 as midnight

                                                            // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                            EventStringBuilder.Append(EventDriver.ChangeDateTime1.ToString("MMdd"));
                                                            EventStringBuilder.Append("2400");
                                                            }
                                                        else
                                                            {
                                                            // Any other time

                                                            EventStringBuilder.Append(EventDriver.ChangeDateTime1.ToString("MMddHHmm"));
                                                            }

                                                        EventStringBuilder.Append(EventDriver.ChangeInEventType1);
                                                        }
                                                    }

                                                // In the old style format, check to see if the 2nd Change in Event date/time is
                                                // blank -- working backwards "up the card fields".

                                                if (EventDriver.IsChangeDateTime2Null() == true || EventDriver.IsChangeInEventType2Null() == true)
                                                    {
                                                    /*
                                                    * 
                                                    * - the Second Change in Event date/time is empty or the Second Change in 
                                                    *   Event Type field is empty -- either way cannot process.
                                                    *
                                                    */

                                                    EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));

                                                    }
                                                else
                                                    {
                                                    // Second Change in Event date/time is NOT empty

                                                    if (EventDriver.ChangeDateTime2.Month > intPeriod)
                                                        {
                                                        /*
                                                        * Second Change in Event date/time is NOT empty, but
                                                        * is later than the period of interest, so make it blank
                                                        * 
                                                        */

                                                        EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));
                                                        }
                                                    else
                                                        {
                                                        // Fill in Second Change in Event date/time

                                                        if (Convert.ToDouble(EventDriver.ChangeDateTime2.ToString("HHmmss.fff")) >= 235959.000)
                                                            {
                                                            // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                                            // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                                            // "re-constructing" the 2400 as midnight

                                                            // douValue = Convert.ToDouble(EventDriver.ChangeDateTime2.ToString("HHmmss.fff"));
                                                            EventStringBuilder.Append(EventDriver.ChangeDateTime2.ToString("MMdd"));
                                                            EventStringBuilder.Append("2400");
                                                            }
                                                        else
                                                            {
                                                            // Any other time

                                                            EventStringBuilder.Append(EventDriver.ChangeDateTime2.ToString("MMddHHmm"));
                                                            }

                                                        EventStringBuilder.Append(EventDriver.ChangeInEventType2);
                                                        }
                                                    }

                                                if (EventDriver.IsEndDateTimeNull() == true)
                                                    {
                                                    // The End of Event date/time is blank (<null>)
                                                    EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                    }
                                                else
                                                    {
                                                    // End of Event date/time is NOT empty

                                                    if (EventDriver.EndDateTime.Month > intPeriod)
                                                        {
                                                        // the event "ends" after the end of the year-to-date period.
                                                        //TODO leave the End of Event Date/Time open; make sure that the Revision Month is
                                                        //    not greater than the intPeriod month; and drop Revision Code by 1 if greater 
                                                        //    than 0 -- SAME FOR THE TWO CHANGE IN EVENT TYPES
                                                        //    probably will have to do a EventStringBuilder.Replace(...);

                                                        // Leave date/time field blank

                                                        EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                        }
                                                    else
                                                        {
                                                        // Fill in the End of Event date/time

                                                        if (EventDriver.IsCarryOverNextYearNull())
                                                            {
                                                            EventDriver.CarryOverNextYear = false;
                                                            }

                                                        if (EventDriver.CarryOverNextYear == true && EventDriver.EndDateTime.ToString("MMdd") == "1231" &&
                                                            Convert.ToDouble(EventDriver.EndDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                                            {
                                                            //EventStringBuilder.Append("123124XX");
                                                            EventStringBuilder.Append("12312400"); // 2010 Change
                                                            }
                                                        else if (Convert.ToDouble(EventDriver.EndDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                                            {
                                                            // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                            EventStringBuilder.Append(EventDriver.EndDateTime.ToString("MMdd"));
                                                            EventStringBuilder.Append("2400");
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.EndDateTime.ToString("MMddHHmm"));
                                                            }
                                                        }
                                                    }


                                                if (intNoOfColumns == 100)
                                                    {
                                                    if (EventDriver.IsGrossAvailCapacityNull() == false && EventDriver.GrossAvailCapacity > 0 && (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData)))
                                                        {
                                                        EventStringBuilder.Append(Convert.ToInt32(100 * Math.Round(EventDriver.GrossAvailCapacity, 2)).ToString().PadLeft(6, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                        }

                                                    if (EventDriver.IsNetAvailCapacityNull() == false && EventDriver.NetAvailCapacity > 0)
                                                        {
                                                        EventStringBuilder.Append(Convert.ToInt32(100 * Math.Round(EventDriver.NetAvailCapacity, 2)).ToString().PadLeft(6, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                        }

                                                    }
                                                else
                                                    {
                                                    if (EventDriver.IsGrossAvailCapacityNull() == false && EventDriver.GrossAvailCapacity > 0 && (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData)))
                                                        {
                                                        EventStringBuilder.Append(Convert.ToInt16(Math.Round(EventDriver.GrossAvailCapacity)).ToString().PadLeft(4, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                        }

                                                    if (EventDriver.IsNetAvailCapacityNull() == false && EventDriver.NetAvailCapacity > 0)
                                                        {
                                                        EventStringBuilder.Append(Convert.ToInt16(Math.Round(EventDriver.NetAvailCapacity)).ToString().PadLeft(4, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                        }

                                                    }

                                                if (intNoOfColumns == 100)
                                                    {
                                                    if (EventDriver.IsDominantDerateNull() == false && EventDriver.DominantDerate == true && (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData)))
                                                        {
                                                        EventStringBuilder.Append(" D");
                                                        EventStringBuilder.Append(String.Empty.PadLeft(11, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(13, ' '));
                                                        }
                                                    }
                                                else
                                                    {
                                                    if (EventDriver.IsDominantDerateNull() == false && EventDriver.DominantDerate == true && (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData)))
                                                        {
                                                        EventStringBuilder.Append(" D");
                                                        EventStringBuilder.Append(String.Empty.PadLeft(15, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(17, ' '));
                                                        }
                                                    }

                                                EventStringBuilder.Append("01");

                                                if ((strWho.ToUpper() == "NERC" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "MISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "NYISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "ISONE" && EventDriver.EventType != "NC"))
                                                    {
                                                    /*
                                                    * NERC, MISO, NYISO and ISO-NE do not need NCs and Homer City uses NCs for Economic Dispatch
                                                    */
                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                    }
                                                // 02 Card

                                                if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                    EventStringBuilder.Length = 19;
                                                else
                                                    EventStringBuilder.Length = 17;

                                                // -------------------------

                                                if ((strWho.ToUpper() == "NERC") || (strWho.ToUpper() == "NYISO") || (strWho.ToUpper() == "MISO") || (strWho.ToUpper() == "ISONE"))
                                                    {
                                                    // NERC, MISO, and NYISO want Cause Code; ISONE does not

                                                    EventStringBuilder.Append(EventDriver.CauseCode.ToString().PadLeft(4, '0'));
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                    }

                                                // -------------------------

                                                if ((EventDriver.IsCauseCodeExtNull() == true) || (strWho.ToUpper() == "NYISO"))
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                    }
                                                else
                                                    {
                                                    if (EventDriver.CauseCodeExt.Trim() == string.Empty)
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                        }
                                                    }

                                                // -------------------------

                                                if ((EventDriver.IsWorkStartedNull() == true) || ((strWho.ToUpper() == "NERC") && boolAllNERCData == false) || (strWho.ToUpper() == "NYISO") || (strWho.ToUpper() == "MISO") || (strWho.ToUpper() == "ISONE"))
                                                    {
                                                    // The Time Work Started date/time is blank (<null>)
                                                    EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                    }
                                                else
                                                    {
                                                    // Time Work Started date/time is NOT empty

                                                    if (EventDriver.WorkStarted.Month > intPeriod)
                                                        {
                                                        // the Time Work Started "ends" after the end of the year-to-date period.
                                                        // Leave date/time field blank

                                                        EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                        }
                                                    else
                                                        {
                                                        // Fill in the Time Work Started date/time

                                                        if (EventDriver.IsCarryOverLastYearNull())
                                                            {
                                                            EventDriver.CarryOverLastYear = false;
                                                            }

                                                        if (EventDriver.CarryOverLastYear == true && EventDriver.WorkStarted.ToString("MMddHHmm") == "01010000")
                                                            {
                                                            //EventStringBuilder.Append("010100XX");
                                                            EventStringBuilder.Append("01010000"); // 2010 Change
                                                            }
                                                        else if (Convert.ToDouble(EventDriver.WorkStarted.ToString("HHmmss.fff")) > 235959.500)
                                                            {
                                                            // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                            EventStringBuilder.Append(EventDriver.WorkStarted.ToString("MMdd"));
                                                            EventStringBuilder.Append("2400");
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.WorkStarted.ToString("MMddHHmm"));
                                                            }
                                                        }
                                                    }

                                                // -------------------------

                                                if ((EventDriver.IsWorkEndedNull() == true) || ((strWho.ToUpper() == "NERC") && boolAllNERCData == false) || (strWho.ToUpper() == "NYISO") || (strWho.ToUpper() == "MISO") || (strWho.ToUpper() == "ISONE"))
                                                    {
                                                    // The Time Work Ended date/time is blank (<null>)
                                                    EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                    }
                                                else
                                                    {
                                                    // Time Work Ended date/time is NOT empty

                                                    if (EventDriver.WorkEnded.Month > intPeriod)
                                                        {
                                                        // the Time Work Ended "ends" after the end of the year-to-date period.
                                                        // Leave date/time field blank

                                                        EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                        }
                                                    else
                                                        {
                                                        // Fill in the Time Work Ended date/time

                                                        if (EventDriver.IsCarryOverNextYearNull())
                                                            {
                                                            EventDriver.CarryOverNextYear = false;
                                                            }

                                                        if (EventDriver.CarryOverNextYear == true && EventDriver.WorkEnded.ToString("MMdd") == "1231" &&
                                                            Convert.ToDouble(EventDriver.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                            {
                                                            EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMdd"));
                                                            //EventStringBuilder.Append("24XX");
                                                            EventStringBuilder.Append("2400"); // 2010 Change
                                                            }
                                                        else if (Convert.ToDouble(EventDriver.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                            {
                                                            // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                            EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMdd"));
                                                            EventStringBuilder.Append("2400");
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMddHHmm"));
                                                            }
                                                        }
                                                    }

                                                // -------------------------

                                                EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));

                                                // -------------------------

                                                EventStringBuilder.Append(EventDriver.ContribCode.ToString().PadLeft(1, ' '));

                                                // -------------------------

                                                if ((EventDriver.IsPrimaryAlertNull() == true) || ((strWho.ToUpper() == "NERC") && boolAllNERCData == false) || (strWho.ToUpper() == "NYISO") || (strWho.ToUpper() == "MISO") || (strWho.ToUpper() == "ISONE"))
                                                    {
                                                    EventStringBuilder.Append(" ");
                                                    }
                                                else
                                                    {
                                                    if (EventDriver.PrimaryAlert)
                                                        {
                                                        EventStringBuilder.Append("X");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(" ");
                                                        }
                                                    }

                                                // -------------------------

                                                if ((EventDriver.IsManhoursWorkedNull() == false && EventDriver.ManhoursWorked > 0) && (strWho.ToUpper() != "NYISO") && (strWho.ToUpper() != "MISO") && (strWho.ToUpper() != "ISONE"))
                                                    {
                                                    if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                        EventStringBuilder.Append(EventDriver.ManhoursWorked.ToString().PadLeft(4, ' '));
                                                    else
                                                        EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                    }

                                                // -------------------------

                                                if ((EventDriver.IsFailureMechCodeNull() == false && EventDriver.FailureMechCode.StartsWith("F")) && (strWho.ToUpper() != "NYISO") && (strWho.ToUpper() != "MISO") && (strWho.ToUpper() != "ISONE"))
                                                    {
                                                    if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                        {

                                                        EventStringBuilder.Append(EventDriver.FailureMechCode);

                                                        if (EventDriver.IsTripMechNull() == false && (EventDriver.TripMech.ToUpper() == "A" || EventDriver.TripMech.ToUpper() == "M"))
                                                            {
                                                            EventStringBuilder.Append(EventDriver.TripMech);

                                                            if (EventDriver.IsCumFiredHoursNull() == false)
                                                                {
                                                                EventStringBuilder.Append(EventDriver.CumFiredHours.ToString().PadLeft(6, ' '));
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                                }

                                                            if (EventDriver.IsCumEngineStartsNull() == false)
                                                                {
                                                                EventStringBuilder.Append(EventDriver.CumEngineStarts.ToString().PadLeft(5, ' '));
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(String.Empty.PadLeft(5, ' '));
                                                                }

                                                            EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 15));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 27));
                                                            }

                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadRight(86, ' ').Substring(0, 31));
                                                        }
                                                    }
                                                else
                                                    {
                                                    if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                        {
                                                        EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 31));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadRight(86, ' ').Substring(0, 31));
                                                        }
                                                    }

                                                // -------------------------

                                                EventStringBuilder.Append("02");

                                                // -------------------------

                                                if ((strWho.ToUpper() == "NERC" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "NYISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "MISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "ISONE" && EventDriver.EventType != "NC"))
                                                    {
                                                    // ISO-NE does not want Event 02/03 cards

                                                    // this writes out the 02 card
                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());

                                                    // 03 card
                                                    if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                        {
                                                        // NYISO does not want the 03 card

                                                        if (EventDriver.VerbalDesc86.PadRight(86, ' ').Substring(31, 55).Trim() != String.Empty)
                                                            {
                                                            // the "key" below INCLUDES the 2-character cause code extension
                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 25;
                                                            else
                                                                EventStringBuilder.Length = 23;

                                                            EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(31, 55));

                                                            EventStringBuilder.Append("03");

                                                            if (EventDriver.IsRevisionCard03Null())
                                                                {
                                                                EventDriver.RevisionCard03 = "0";
                                                                }

                                                            if (EventDriver.RevisionCard03.ToUpper() != "X")
                                                                {
                                                                // this card is not deleted; so write it out

                                                                // this writes out the 03 card
                                                                EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                                }
                                                            }
                                                        }
                                                    }

                                                // notes:  remember that the RDMS do NOT allow for 24:00, but 23:59:59.999 is stored.
                                                // also if not NULL the CarryOverLastYear and the CarryOverNextYear fields will 
                                                // control the use of 01/01/yyyy 00XX and 12/31/yyyy 24XX for the date/time fields.

                                                // process the 04/05 - 98/99 cards
                                                foreach (BusinessLayer.AllEventData.EventData02Row EventDetail in EventDriver.GetChildRows("AdditionalWork"))
                                                    {
                                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                        EventStringBuilder.Length = 19;
                                                    else
                                                        EventStringBuilder.Length = 17;

                                                    EventStringBuilder.Append(EventDetail.CauseCode.ToString().PadLeft(4, '0'));

                                                    if (EventDetail.IsCauseCodeExtNull() == true)
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                        }
                                                    else
                                                        {
                                                        if (EventDetail.CauseCodeExt.Trim() == string.Empty)
                                                            {
                                                            EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                            }
                                                        //EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                        }

                                                    if (EventDetail.IsWorkStartedNull() == true)
                                                        {
                                                        // The Time Work Started date/time is blank (<null>)
                                                        EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                        }
                                                    else
                                                        {
                                                        // Time Work Started date/time is NOT empty

                                                        if (EventDetail.WorkStarted.Month > intPeriod)
                                                            {
                                                            // the Time Work Started "ends" after the end of the year-to-date period.
                                                            // Leave date/time field blank

                                                            EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                            }
                                                        else
                                                            {
                                                            // Fill in the Time Work Started date/time

                                                            if (EventDriver.IsCarryOverLastYearNull())
                                                                {
                                                                EventDriver.CarryOverLastYear = false;
                                                                }

                                                            if (EventDriver.CarryOverLastYear == true && EventDetail.WorkStarted.ToString("MMddHHmm") == "01010000")
                                                                {
                                                                EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMdd"));
                                                                //EventStringBuilder.Append("00XX");
                                                                EventStringBuilder.Append("0000"); // 2010 Change
                                                                }
                                                            else if (Convert.ToDouble(EventDetail.WorkStarted.ToString("HHmmss.fff")) >= 235959.000)
                                                                {
                                                                // douValue = Convert.ToDouble(EventDetail.StartDateTime.ToString("HHmmss.fff"));
                                                                EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMdd"));
                                                                EventStringBuilder.Append("2400");
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMddHHmm"));
                                                                }
                                                            }
                                                        }

                                                    if (EventDetail.IsWorkEndedNull() == true)
                                                        {
                                                        // The Time Work Ended date/time is blank (<null>)
                                                        EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                        }
                                                    else
                                                        {
                                                        // Time Work Ended date/time is NOT empty

                                                        if (EventDetail.WorkEnded.Month > intPeriod)
                                                            {
                                                            // the Time Work Ended "ends" after the end of the year-to-date period.
                                                            // Leave date/time field blank

                                                            EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                            }
                                                        else
                                                            {
                                                            // Fill in the Time Work Ended date/time

                                                            if (EventDriver.IsCarryOverNextYearNull())
                                                                {
                                                                EventDriver.CarryOverNextYear = false;
                                                                }

                                                            if (EventDriver.CarryOverNextYear == true && EventDetail.WorkEnded.ToString("MMdd") == "1231" &&
                                                                Convert.ToDouble(EventDetail.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                                {
                                                                EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMdd"));
                                                                //EventStringBuilder.Append("24XX");
                                                                EventStringBuilder.Append("2400"); // 2010 Change
                                                                }
                                                            else if (Convert.ToDouble(EventDetail.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                                {
                                                                // douValue = Convert.ToDouble(EventDetail.StartDateTime.ToString("HHmmss.fff"));
                                                                EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMdd"));
                                                                EventStringBuilder.Append("2400");
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMddHHmm"));
                                                                }
                                                            }
                                                        }

                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));

                                                    EventStringBuilder.Append(EventDetail.ContribCode.ToString().PadLeft(1, ' '));

                                                    if (EventDetail.IsPrimaryAlertNull() == true)
                                                        {
                                                        EventStringBuilder.Append(" ");
                                                        }
                                                    else
                                                        {
                                                        if (EventDetail.PrimaryAlert)
                                                            {
                                                            EventStringBuilder.Append("X");
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(" ");
                                                            }
                                                        }

                                                    if (EventDetail.IsManhoursWorkedNull() == false && EventDetail.ManhoursWorked > 0)
                                                        {
                                                        EventStringBuilder.Append(EventDetail.ManhoursWorked.ToString().PadLeft(4, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                        }

                                                    if (EventDetail.IsFailureMechCodeNull() == false && EventDetail.FailureMechCode.StartsWith("F"))
                                                        {
                                                        EventStringBuilder.Append(EventDetail.FailureMechCode);

                                                        if (EventDetail.IsTripMechNull() == false && (EventDetail.TripMech.ToUpper() == "A" || EventDetail.TripMech.ToUpper() == "M"))
                                                            {
                                                            EventStringBuilder.Append(EventDetail.TripMech);

                                                            if (EventDetail.IsCumFiredHoursNull() == false)
                                                                {
                                                                EventStringBuilder.Append(EventDetail.CumFiredHours.ToString().PadLeft(6, ' '));
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                                }

                                                            if (EventDetail.IsCumEngineStartsNull() == false)
                                                                {
                                                                EventStringBuilder.Append(EventDetail.CumEngineStarts.ToString().PadLeft(5, ' '));
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(String.Empty.PadLeft(5, ' '));
                                                                }

                                                            EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 15));
                                                            }
                                                        else
                                                            {

                                                            EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 27));
                                                            }
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 31));
                                                        }

                                                    EventStringBuilder.Append(EventDetail.EvenCardNumber.ToString().PadLeft(2, '0'));

                                                    if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                        {
                                                        // NYISO, MISO, and ISO-NE do not want Event 04-99 cards

                                                        if (EventDetail.RevisionCardEven.ToUpper() != "X")
                                                            {
                                                            // this card is not deleted; so include it and the odd-numbered mate

                                                            // this writes out the even numbered cards between 04-99
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());

                                                            if (EventDetail.VerbalDesc86.PadRight(86, ' ').Substring(31, 55).Trim() != String.Empty)
                                                                {
                                                                // the "key" below INCLUDES the 2-character cause code extension
                                                                if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                    EventStringBuilder.Length = 25;
                                                                else
                                                                    EventStringBuilder.Length = 23;

                                                                EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(31, 55));

                                                                intValue = EventDetail.EvenCardNumber + 1;

                                                                EventStringBuilder.Append(intValue.ToString().PadLeft(2, '0'));

                                                                if (EventDetail.RevisionCardOdd.ToUpper() != "X")
                                                                    {
                                                                    // this card is not deleted

                                                                    // this writes out the odd numbered cards between 04-99
                                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        break;

                                    case 2:

                                        // Quarterly    - intPeriod = quarter

                                        // process the 01 and 02/03 cards 

                                        EventStringBuilder.Length = 0;
                                        if (intNoOfColumns == 100)
                                            {
                                            EventStringBuilder.Insert(0, "07");
                                            }
                                        else
                                            {
                                            EventStringBuilder.Insert(0, 97);
                                            }
                                        EventStringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6, '0'));
                                        if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                            {
                                            // 82-column data files have a 4-digit year
                                            EventStringBuilder.Append(EventDriver.Year.ToString());
                                            }
                                        else
                                            {
                                            // 80-column data files have the 2-digit year -- historical GADS layout
                                            EventStringBuilder.Append(EventDriver.Year.ToString().Substring(2, 2));
                                            }

                                        EventStringBuilder.Append(EventDriver.EventNumber.ToString().PadLeft(4, '0'));

                                        if (EventDriver.StartDateTime.Month < EventDriver.RevMonthCard01.Month)
                                            {
                                            if (EventDriver.RevisionCard01 != "X" && EventDriver.RevisionCard01 != "0")
                                                {
                                                if (EventDriver.RevMonthCard01.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard01.Month <= (intPeriod * 3))
                                                    {
                                                    EventStringBuilder.Append(EventDriver.RevisionCard01);
                                                    }
                                                else
                                                    {
                                                    intValue = Convert.ToInt16(EventDriver.RevisionCard01) - 1;
                                                    EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(EventDriver.RevisionCard01);
                                                }
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(EventDriver.RevisionCard01);
                                            }

                                        EventStringBuilder.Append(EventDriver.EventType);

                                        if (EventDriver.IsCarryOverLastYearNull())
                                            {
                                            EventDriver.CarryOverLastYear = false;
                                            }

                                        if (EventDriver.CarryOverLastYear == true && EventDriver.StartDateTime.ToString("MMddHHmm") == "01010000")
                                            {
                                            // The event started in the prior year; therefore, the Start of Event date/time
                                            // should be 010100XX
                                            //EventStringBuilder.Append("010100XX");
                                            EventStringBuilder.Append("01010000"); // 2010 Change
                                            }
                                        else if (Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                            {
                                            // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                            // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                            // "re-constructing" the 2400 as midnight

                                            // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                            EventStringBuilder.Append(EventDriver.StartDateTime.ToString("MMdd"));
                                            EventStringBuilder.Append("2400");
                                            }
                                        else
                                            {
                                            // Any other time

                                            EventStringBuilder.Append(EventDriver.StartDateTime.ToString("MMddHHmm"));
                                            }


                                        if (EventDriver.IsChangeDateTime1Null() == true || EventDriver.IsChangeInEventType1Null() == true)
                                            {
                                            /*
                                                        * 
                                                        * - the First Change in Event date/time is empty or the First Change in 
                                                        *   Event Type field is empty -- either way cannot process.
                                                        *
                                                        */

                                            EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));

                                            }
                                        else
                                            {
                                            // First Change in Event date/time IS NOT empty
                                            if ((EventDriver.ChangeDateTime1.Month > ((intPeriod - 1) * 3) & EventDriver.ChangeDateTime1.Month <= (intPeriod * 3)) ||
                                                (EventDriver.RevMonthCard01.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard01.Month <= (intPeriod * 3)))
                                                {

                                                // Fill in First Change in Event date/time

                                                if (Convert.ToDouble(EventDriver.ChangeDateTime1.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                                    // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                                    // "re-constructing" the 2400 as midnight

                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime1.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    // Any other time

                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime1.ToString("MMddHHmm"));
                                                    }

                                                EventStringBuilder.Append(EventDriver.ChangeInEventType1);
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));
                                                }
                                            }


                                        // In the old style format, check to see if the 2nd Change in Event date/time is
                                        // blank -- working backwards "up the card fields".

                                        if (EventDriver.IsChangeDateTime2Null() == true || EventDriver.IsChangeInEventType2Null() == true)
                                            {
                                            /*
                                                    * 
                                                    * - the Second Change in Event date/time is empty or the Second Change in 
                                                    *   Event Type field is empty -- either way cannot process.
                                                    *
                                                    */

                                            EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));

                                            }
                                        else
                                            {
                                            // Second Change in Event date/time is NOT empty

                                            if ((EventDriver.ChangeDateTime2.Month > ((intPeriod - 1) * 3) & EventDriver.ChangeDateTime2.Month <= (intPeriod * 3)) ||
                                                (EventDriver.RevMonthCard01.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard01.Month <= (intPeriod * 3)))
                                                {
                                                // Fill in Second Change in Event date/time

                                                if (Convert.ToDouble(EventDriver.ChangeDateTime2.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                                    // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                                    // "re-constructing" the 2400 as midnight

                                                    // douValue = Convert.ToDouble(EventDriver.ChangeDateTime2.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime2.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    // Any other time

                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime2.ToString("MMddHHmm"));
                                                    }

                                                EventStringBuilder.Append(EventDriver.ChangeInEventType2);
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));
                                                }
                                            }

                                        if (EventDriver.IsEndDateTimeNull() == true)
                                            {
                                            // The End of Event date/time is blank (<null>)
                                            EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                            }
                                        else
                                            {
                                            // End of Event date/time is NOT empty

                                            if ((EventDriver.EndDateTime.Month > ((intPeriod - 1) * 3) & EventDriver.EndDateTime.Month <= (intPeriod * 3)) ||
                                                (EventDriver.RevMonthCard01.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard01.Month <= (intPeriod * 3)))
                                                {
                                                // Fill in the End of Event date/time

                                                if (EventDriver.IsCarryOverNextYearNull())
                                                    {
                                                    EventDriver.CarryOverNextYear = false;
                                                    }

                                                if (EventDriver.CarryOverNextYear == true && EventDriver.EndDateTime.ToString("MMdd") == "1231" &&
                                                    Convert.ToDouble(EventDriver.EndDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    //EventStringBuilder.Append("123124XX");
                                                    EventStringBuilder.Append("12312400"); // 2010 Change
                                                    }
                                                else if (Convert.ToDouble(EventDriver.EndDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.EndDateTime.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.EndDateTime.ToString("MMddHHmm"));
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                }
                                            }

                                        if (EventDriver.IsGrossAvailCapacityNull() == false && EventDriver.GrossAvailCapacity > 0)
                                            {
                                            EventStringBuilder.Append(Convert.ToInt16(Math.Round(EventDriver.GrossAvailCapacity)).ToString().PadLeft(4, ' '));
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }

                                        if (EventDriver.IsNetAvailCapacityNull() == false && EventDriver.NetAvailCapacity > 0)
                                            {
                                            EventStringBuilder.Append(Convert.ToInt16(Math.Round(EventDriver.NetAvailCapacity)).ToString().PadLeft(4, ' '));
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }

                                        if (EventDriver.IsDominantDerateNull() == false && EventDriver.DominantDerate == true)
                                            {
                                            EventStringBuilder.Append(" D");
                                            EventStringBuilder.Append(String.Empty.PadLeft(15, ' '));
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(String.Empty.PadLeft(17, ' '));
                                            }

                                        EventStringBuilder.Append("01");

                                        if ((strWho.ToUpper() == "NERC" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "NYISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "MISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "ISONE" && EventDriver.EventType != "NC"))
                                            {
                                            /*
                                            *  NYISO, MISO and ISO-NE do not need NCs and Homer City uses NCs for Economic Dispatch
                                            */
                                            if ((EventDriver.StartDateTime.Month > ((intPeriod - 1) * 3) & EventDriver.StartDateTime.Month <= (intPeriod * 3)) ||
                                                (EventDriver.RevMonthCard01.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard01.Month <= (intPeriod * 3)))
                                                {
                                                if (EventDriver.RevisionCard01.ToUpper() != "X")
                                                    {
                                                    // Write the Event 01 card
                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                    }
                                                else
                                                    {
                                                    // Write the Event01 card to DELETE THE ENTIRE EVENT

                                                    EventStringBuilder.Length = 0;
                                                    if (intNoOfColumns == 100)
                                                        {
                                                        EventStringBuilder.Insert(0, "07");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Insert(0, 97);
                                                        }
                                                    EventStringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6, '0'));
                                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                        {
                                                        // 82-column data files have a 4-digit year
                                                        EventStringBuilder.Append(EventDriver.Year.ToString());
                                                        }
                                                    else
                                                        {
                                                        // 80-column data files have the 2-digit year -- historical GADS layout
                                                        EventStringBuilder.Append(EventDriver.Year.ToString().Substring(2, 2));
                                                        }

                                                    EventStringBuilder.Append(EventDriver.EventNumber.ToString().PadLeft(4, '0'));

                                                    EventStringBuilder.Append("X");

                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                    }
                                                }
                                            }


                                        if (EventDriver.RevisionCard01.ToUpper() != "X")
                                            {
                                            // if RevisionCard01 is "X" then the entire event is to be deleted
                                            // No 02-99 cards are allowed

                                            // 02 Card

                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                EventStringBuilder.Length = 16;
                                            else
                                                EventStringBuilder.Length = 14;

                                            if (EventDriver.StartDateTime.Month < EventDriver.RevMonthCard02.Month)
                                                {
                                                if (EventDriver.RevisionCard02 != "X" && EventDriver.RevisionCard02 != "0")
                                                    {
                                                    if (EventDriver.RevMonthCard02.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard02.Month <= (intPeriod * 3))
                                                        {
                                                        EventStringBuilder.Append(EventDriver.RevisionCard02);
                                                        }
                                                    else
                                                        {
                                                        intValue = Convert.ToInt16(EventDriver.RevisionCard02) - 1;
                                                        EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                        }
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.RevisionCard02);
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(EventDriver.RevisionCard02);
                                                }

                                            EventStringBuilder.Append(EventDriver.EventType);

                                            EventStringBuilder.Append(EventDriver.CauseCode.ToString().PadLeft(4, '0'));

                                            if (EventDriver.IsCauseCodeExtNull() == true)
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                }
                                            else
                                                {
                                                if (EventDriver.CauseCodeExt.Trim() == string.Empty)
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                    }
                                                //EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                }

                                            if (EventDriver.IsWorkStartedNull() == true)
                                                {
                                                // The Time Work Started date/time is blank (<null>)
                                                EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                }
                                            else
                                                {
                                                // Time Work Started date/time is NOT empty


                                                // Fill in the Time Work Started date/time

                                                if (EventDriver.IsCarryOverLastYearNull())
                                                    {
                                                    EventDriver.CarryOverLastYear = false;
                                                    }

                                                if (EventDriver.CarryOverLastYear == true && EventDriver.WorkStarted.ToString("MMddHHmm") == "01010000")
                                                    {
                                                    //EventStringBuilder.Append("010100XX");
                                                    EventStringBuilder.Append("01010000"); // 2010 Change
                                                    }
                                                else if (Convert.ToDouble(EventDriver.WorkStarted.ToString("HHmmss.fff")) > 235959.500)
                                                    {
                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.WorkStarted.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.WorkStarted.ToString("MMddHHmm"));
                                                    }

                                                }

                                            if (EventDriver.IsWorkEndedNull() == true)
                                                {
                                                // The Time Work Ended date/time is blank (<null>)
                                                EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                }
                                            else
                                                {

                                                // Fill in the Time Work Ended date/time

                                                if (EventDriver.IsCarryOverNextYearNull())
                                                    {
                                                    EventDriver.CarryOverNextYear = false;
                                                    }

                                                if (EventDriver.CarryOverNextYear == true && EventDriver.WorkEnded.ToString("MMdd") == "1231" &&
                                                    Convert.ToDouble(EventDriver.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMdd"));
                                                    //EventStringBuilder.Append("24XX");
                                                    EventStringBuilder.Append("2400"); // 2010 Change
                                                    }
                                                else if (Convert.ToDouble(EventDriver.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMddHHmm"));
                                                    }

                                                }

                                            EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));

                                            EventStringBuilder.Append(EventDriver.ContribCode.ToString().PadLeft(1, ' '));

                                            if (EventDriver.IsPrimaryAlertNull() == true)
                                                {
                                                EventStringBuilder.Append(" ");
                                                }
                                            else
                                                {
                                                if (EventDriver.PrimaryAlert)
                                                    {
                                                    EventStringBuilder.Append("X");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(" ");
                                                    }
                                                }

                                            if (EventDriver.IsManhoursWorkedNull() == false && EventDriver.ManhoursWorked > 0)
                                                {
                                                EventStringBuilder.Append(EventDriver.ManhoursWorked.ToString().PadLeft(4, ' '));
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                }

                                            if (EventDriver.IsFailureMechCodeNull() == false && EventDriver.FailureMechCode.StartsWith("F"))
                                                {
                                                EventStringBuilder.Append(EventDriver.FailureMechCode);

                                                if (EventDriver.IsTripMechNull() == false && (EventDriver.TripMech.ToUpper() == "A" || EventDriver.TripMech.ToUpper() == "M"))
                                                    {
                                                    EventStringBuilder.Append(EventDriver.TripMech);

                                                    if (EventDriver.IsCumFiredHoursNull() == false)
                                                        {
                                                        EventStringBuilder.Append(EventDriver.CumFiredHours.ToString().PadLeft(6, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                        }

                                                    if (EventDriver.IsCumEngineStartsNull() == false)
                                                        {
                                                        EventStringBuilder.Append(EventDriver.CumEngineStarts.ToString().PadLeft(5, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(5, ' '));
                                                        }

                                                    EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 15));
                                                    }
                                                else
                                                    {

                                                    EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 27));
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 31));
                                                }

                                            EventStringBuilder.Append("02");

                                            if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                {
                                                // NYISO, MISO, and ISO-NE do not want Event 02/03 cards

                                                if ((EventDriver.StartDateTime.Month > ((intPeriod - 1) * 3) & EventDriver.StartDateTime.Month <= (intPeriod * 3)) ||
                                                    (EventDriver.RevMonthCard02.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard02.Month <= (intPeriod * 3)))
                                                    {
                                                    // this writes out the 02 card
                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                    }
                                                // 03 card

                                                if (EventDriver.VerbalDesc86.PadRight(86, ' ').Substring(31, 55).Trim() != String.Empty)
                                                    {
                                                    // the "key" below INCLUDES the 2-character cause code extension

                                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                        EventStringBuilder.Length = 16;
                                                    else
                                                        EventStringBuilder.Length = 14;

                                                    if (EventDriver.IsRevisionCard03Null())
                                                        {
                                                        EventDriver.RevisionCard03 = "0";
                                                        }

                                                    if (EventDriver.StartDateTime.Month < EventDriver.RevMonthCard03.Month)
                                                        {
                                                        if (EventDriver.RevisionCard03 != "X" && EventDriver.RevisionCard03 != "0")
                                                            {
                                                            if (EventDriver.RevMonthCard03.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard03.Month <= (intPeriod * 3))
                                                                {
                                                                EventStringBuilder.Append(EventDriver.RevisionCard03);
                                                                }
                                                            else
                                                                {
                                                                intValue = Convert.ToInt16(EventDriver.RevisionCard03) - 1;
                                                                EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                                }
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.RevisionCard03);
                                                            }
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDriver.RevisionCard03);
                                                        }

                                                    EventStringBuilder.Append(EventDriver.EventType);

                                                    EventStringBuilder.Append(EventDriver.CauseCode.ToString().PadLeft(4, '0'));

                                                    if (EventDriver.IsCauseCodeExtNull() == true)
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                        }
                                                    else
                                                        {
                                                        if (EventDriver.CauseCodeExt.Trim() == string.Empty)
                                                            {
                                                            EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                            }
                                                        //EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                        }

                                                    EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(31, 55));

                                                    EventStringBuilder.Append("03");

                                                    if (EventDriver.IsRevisionCard03Null())
                                                        {
                                                        EventDriver.RevisionCard03 = "0";
                                                        }

                                                    if (EventDriver.RevisionCard03.ToUpper() != "X")
                                                        {
                                                        // this card is not deleted; so write it out
                                                        if ((EventDriver.StartDateTime.Month > ((intPeriod - 1) * 3) & EventDriver.StartDateTime.Month <= (intPeriod * 3)) ||
                                                            (EventDriver.RevMonthCard03.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard03.Month <= (intPeriod * 3)))
                                                            {
                                                            // this writes out the 03 card
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }
                                                        }
                                                    else
                                                        {
                                                        // this card is to be deleted
                                                        if (EventDriver.RevMonthCard03.Month > ((intPeriod - 1) * 3) & EventDriver.RevMonthCard03.Month <= (intPeriod * 3))
                                                            {
                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            EventStringBuilder.Append("X");
                                                            EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                            EventStringBuilder.Append("03");
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                            // notes:  remember that the RDMS do NOT allow for 24:00, but 23:59:59.999 is stored.
                                            // also if not NULL the CarryOverLastYear and the CarryOverNextYear fields will 
                                            // control the use of 01/01/yyyy 00XX and 12/31/yyyy 24XX for the date/time fields.

                                            // process the 04/05 - 98/99 cards
                                            foreach (BusinessLayer.AllEventData.EventData02Row EventDetail in EventDriver.GetChildRows("AdditionalWork"))
                                                {

                                                if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                    EventStringBuilder.Length = 16;
                                                else
                                                    EventStringBuilder.Length = 14;

                                                if (EventDriver.StartDateTime.Month < EventDetail.RevMonthCardEven.Month)
                                                    {
                                                    if (EventDetail.RevisionCardEven != "X" && EventDetail.RevisionCardEven != "0")
                                                        {
                                                        if (EventDetail.RevMonthCardEven.Month > ((intPeriod - 1) * 3) & EventDetail.RevMonthCardEven.Month <= (intPeriod * 3))
                                                            {
                                                            EventStringBuilder.Append(EventDetail.RevisionCardEven);
                                                            }
                                                        else
                                                            {
                                                            intValue = Convert.ToInt16(EventDetail.RevisionCardEven) - 1;
                                                            EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                            }
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.RevisionCardEven);
                                                        }
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDetail.RevisionCardEven);
                                                    }

                                                EventStringBuilder.Append(EventDriver.EventType);

                                                EventStringBuilder.Append(EventDetail.CauseCode.ToString().PadLeft(4, '0'));

                                                if (EventDetail.IsCauseCodeExtNull() == true)
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                    }
                                                else
                                                    {
                                                    if (EventDetail.CauseCodeExt.Trim() == string.Empty)
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                        }
                                                    //EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                    }

                                                if (EventDetail.IsWorkStartedNull() == true)
                                                    {
                                                    // The Time Work Started date/time is blank (<null>)
                                                    EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                    }
                                                else
                                                    {
                                                    // Time Work Started date/time is NOT empty

                                                    // Fill in the Time Work Started date/time

                                                    if (EventDriver.IsCarryOverLastYearNull())
                                                        {
                                                        EventDriver.CarryOverLastYear = false;
                                                        }

                                                    if (EventDriver.CarryOverLastYear == true && EventDetail.WorkStarted.ToString("MMddHHmm") == "01010000")
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMdd"));
                                                        //EventStringBuilder.Append("00XX");
                                                        EventStringBuilder.Append("0000"); // 2010 Change
                                                        }
                                                    else if (Convert.ToDouble(EventDetail.WorkStarted.ToString("HHmmss.fff")) >= 235959.000)
                                                        {
                                                        // douValue = Convert.ToDouble(EventDetail.StartDateTime.ToString("HHmmss.fff"));
                                                        EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMdd"));
                                                        EventStringBuilder.Append("2400");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMddHHmm"));
                                                        }

                                                    }

                                                if (EventDetail.IsWorkEndedNull() == true)
                                                    {
                                                    // The Time Work Ended date/time is blank (<null>)
                                                    EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                    }
                                                else
                                                    {
                                                    // Time Work Ended date/time is NOT empty

                                                    // Fill in the Time Work Ended date/time

                                                    if (EventDriver.IsCarryOverNextYearNull())
                                                        {
                                                        EventDriver.CarryOverNextYear = false;
                                                        }

                                                    if (EventDriver.CarryOverNextYear == true && EventDetail.WorkEnded.ToString("MMdd") == "1231" &&
                                                        Convert.ToDouble(EventDetail.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMdd"));
                                                        //EventStringBuilder.Append("24XX");
                                                        EventStringBuilder.Append("2400"); // 2010 Change
                                                        }
                                                    else if (Convert.ToDouble(EventDetail.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                        {
                                                        // douValue = Convert.ToDouble(EventDetail.StartDateTime.ToString("HHmmss.fff"));
                                                        EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMdd"));
                                                        EventStringBuilder.Append("2400");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMddHHmm"));
                                                        }

                                                    }

                                                EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));

                                                EventStringBuilder.Append(EventDetail.ContribCode.ToString().PadLeft(1, ' '));

                                                if (EventDetail.IsPrimaryAlertNull() == true)
                                                    {
                                                    EventStringBuilder.Append(" ");
                                                    }
                                                else
                                                    {
                                                    if (EventDetail.PrimaryAlert)
                                                        {
                                                        EventStringBuilder.Append("X");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(" ");
                                                        }
                                                    }

                                                if (EventDetail.IsManhoursWorkedNull() == false && EventDetail.ManhoursWorked > 0)
                                                    {
                                                    EventStringBuilder.Append(EventDetail.ManhoursWorked.ToString().PadLeft(4, ' '));
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                    }

                                                if (EventDetail.IsFailureMechCodeNull() == false && EventDetail.FailureMechCode.StartsWith("F"))
                                                    {
                                                    EventStringBuilder.Append(EventDetail.FailureMechCode);

                                                    if (EventDetail.IsTripMechNull() == false && (EventDetail.TripMech.ToUpper() == "A" || EventDetail.TripMech.ToUpper() == "M"))
                                                        {
                                                        EventStringBuilder.Append(EventDetail.TripMech);

                                                        if (EventDetail.IsCumFiredHoursNull() == false)
                                                            {
                                                            EventStringBuilder.Append(EventDetail.CumFiredHours.ToString().PadLeft(6, ' '));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                            }

                                                        if (EventDetail.IsCumEngineStartsNull() == false)
                                                            {
                                                            EventStringBuilder.Append(EventDetail.CumEngineStarts.ToString().PadLeft(5, ' '));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(String.Empty.PadLeft(5, ' '));
                                                            }

                                                        EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 15));
                                                        }
                                                    else
                                                        {

                                                        EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 27));
                                                        }
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 31));
                                                    }

                                                EventStringBuilder.Append(EventDetail.EvenCardNumber.ToString().PadLeft(2, '0'));

                                                if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                    {
                                                    // NYISO, MISO and ISO-NE does not want Event 04-99 cards

                                                    if (EventDetail.RevisionCardEven.ToUpper() != "X")
                                                        {
                                                        // this card is not deleted; so include it and the odd-numbered mate

                                                        if ((EventDriver.StartDateTime.Month > ((intPeriod - 1) * 3) & EventDriver.StartDateTime.Month <= (intPeriod * 3)) ||
                                                            (EventDetail.RevMonthCardEven.Month > ((intPeriod - 1) * 3) & EventDetail.RevMonthCardEven.Month <= (intPeriod * 3)))
                                                            {
                                                            // this writes out the even numbered cards between 04-99
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }

                                                        if (EventDetail.VerbalDesc86.PadRight(86, ' ').Substring(31, 55).Trim() != String.Empty)
                                                            {
                                                            // the "key" below INCLUDES the 2-character cause code extension

                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            if (EventDriver.StartDateTime.Month < EventDetail.RevMonthCardOdd.Month)
                                                                {
                                                                if (EventDetail.RevisionCardOdd != "X" && EventDetail.RevisionCardOdd != "0")
                                                                    {
                                                                    if (EventDetail.RevMonthCardOdd.Month > ((intPeriod - 1) * 3) & EventDetail.RevMonthCardOdd.Month <= (intPeriod * 3))
                                                                        {
                                                                        EventStringBuilder.Append(EventDetail.RevisionCardOdd);
                                                                        }
                                                                    else
                                                                        {
                                                                        intValue = Convert.ToInt16(EventDetail.RevisionCardOdd) - 1;
                                                                        EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                                        }
                                                                    }
                                                                else
                                                                    {
                                                                    EventStringBuilder.Append(EventDetail.RevisionCardOdd);
                                                                    }
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(EventDetail.RevisionCardOdd);
                                                                }

                                                            EventStringBuilder.Append(EventDriver.EventType);

                                                            EventStringBuilder.Append(EventDetail.CauseCode.ToString().PadLeft(4, '0'));

                                                            if (EventDetail.IsCauseCodeExtNull() == true)
                                                                {
                                                                EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                                }
                                                            else
                                                                {
                                                                if (EventDetail.CauseCodeExt.Trim() == string.Empty)
                                                                    {
                                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                                    }
                                                                else
                                                                    {
                                                                    EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                                    }
                                                                //EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                                }

                                                            EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(31, 55));

                                                            intValue = EventDetail.EvenCardNumber + 1;

                                                            EventStringBuilder.Append(intValue.ToString().PadLeft(2, '0'));

                                                            if (EventDetail.RevisionCardOdd.ToUpper() != "X")
                                                                {
                                                                // this card is not deleted

                                                                if ((EventDriver.StartDateTime.Month > ((intPeriod - 1) * 3) & EventDriver.StartDateTime.Month <= (intPeriod * 3)) ||
                                                                    (EventDetail.RevMonthCardOdd.Month > ((intPeriod - 1) * 3) & EventDetail.RevMonthCardOdd.Month <= (intPeriod * 3)))
                                                                    {
                                                                    // this writes out the odd numbered cards between 04-99
                                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                                    }
                                                                }
                                                            else
                                                                {
                                                                // this card is to be deleted
                                                                if (EventDetail.RevMonthCardOdd.Month > ((intPeriod - 1) * 3) & EventDetail.RevMonthCardOdd.Month <= (intPeriod * 3))
                                                                    {
                                                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                        EventStringBuilder.Length = 16;
                                                                    else
                                                                        EventStringBuilder.Length = 14;

                                                                    EventStringBuilder.Append("X");
                                                                    EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                                    intValue = EventDetail.EvenCardNumber + 1;
                                                                    EventStringBuilder.Append(intValue.ToString().PadLeft(2, '0'));
                                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    else
                                                        {
                                                        // this even card is to be deleted as well as the corresponding odd card
                                                        if (EventDetail.RevMonthCardEven.Month > ((intPeriod - 1) * 3) & EventDetail.RevMonthCardEven.Month <= (intPeriod * 3))
                                                            {
                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            EventStringBuilder.Append("X");
                                                            EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                            EventStringBuilder.Append(EventDetail.EvenCardNumber.ToString().PadLeft(2, '0'));
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());

                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            EventStringBuilder.Append("X");
                                                            EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                            intValue = EventDetail.EvenCardNumber + 1;
                                                            EventStringBuilder.Append(intValue.ToString().PadLeft(2, '0'));
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        break;

                                    case 3:

                                        // Monthly      - intPeriod = specific month

                                        // process the 01 and 02/03 cards 

                                        EventStringBuilder.Length = 0;
                                        if (intNoOfColumns == 100)
                                            {
                                            EventStringBuilder.Insert(0, "07");
                                            }
                                        else
                                            {
                                            EventStringBuilder.Insert(0, 97);
                                            }
                                        EventStringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6, '0'));
                                        if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                            {
                                            // 82-column data files have a 4-digit year
                                            EventStringBuilder.Append(EventDriver.Year.ToString());
                                            }
                                        else
                                            {
                                            // 80-column data files have the 2-digit year -- historical GADS layout
                                            EventStringBuilder.Append(EventDriver.Year.ToString().Substring(2, 2));
                                            }

                                        EventStringBuilder.Append(EventDriver.EventNumber.ToString().PadLeft(4, '0'));

                                        if (EventDriver.StartDateTime.Month < EventDriver.RevMonthCard01.Month)
                                            {
                                            if (EventDriver.RevisionCard01 != "X" && EventDriver.RevisionCard01 != "0")
                                                {
                                                if (EventDriver.RevMonthCard01.Month == intPeriod)
                                                    {
                                                    EventStringBuilder.Append(EventDriver.RevisionCard01);
                                                    }
                                                else
                                                    {
                                                    intValue = Convert.ToInt16(EventDriver.RevisionCard01) - 1;
                                                    EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(EventDriver.RevisionCard01);
                                                }
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(EventDriver.RevisionCard01);
                                            }

                                        EventStringBuilder.Append(EventDriver.EventType);

                                        if (EventDriver.IsCarryOverLastYearNull())
                                            {
                                            EventDriver.CarryOverLastYear = false;
                                            }

                                        if (EventDriver.CarryOverLastYear == true && EventDriver.StartDateTime.ToString("MMddHHmm") == "01010000")
                                            {
                                            // The event started in the prior year; therefore, the Start of Event date/time
                                            // should be 010100XX
                                            //EventStringBuilder.Append("010100XX");
                                            EventStringBuilder.Append("01010000"); // 2010 Change
                                            }
                                        else if (Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                            {
                                            // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                            // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                            // "re-constructing" the 2400 as midnight

                                            // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                            EventStringBuilder.Append(EventDriver.StartDateTime.ToString("MMdd"));
                                            EventStringBuilder.Append("2400");
                                            }
                                        else
                                            {
                                            // Any other time

                                            EventStringBuilder.Append(EventDriver.StartDateTime.ToString("MMddHHmm"));
                                            }


                                        if (EventDriver.IsChangeDateTime1Null() == true || EventDriver.IsChangeInEventType1Null() == true)
                                            {
                                            /*
                                                        * 
                                                        * - the First Change in Event date/time is empty or the First Change in 
                                                        *   Event Type field is empty -- either way cannot process.
                                                        *
                                                        */

                                            EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));

                                            }
                                        else
                                            {
                                            // First Change in Event date/time IS NOT empty
                                            if (EventDriver.ChangeDateTime1.Month == intPeriod || EventDriver.RevMonthCard01.Month == intPeriod)
                                                {

                                                // Fill in First Change in Event date/time

                                                if (Convert.ToDouble(EventDriver.ChangeDateTime1.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                                    // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                                    // "re-constructing" the 2400 as midnight

                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime1.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    // Any other time

                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime1.ToString("MMddHHmm"));
                                                    }

                                                EventStringBuilder.Append(EventDriver.ChangeInEventType1);
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));
                                                }
                                            }


                                        // In the old style format, check to see if the 2nd Change in Event date/time is
                                        // blank -- working backwards "up the card fields".

                                        if (EventDriver.IsChangeDateTime2Null() == true || EventDriver.IsChangeInEventType2Null() == true)
                                            {
                                            /*
                                                    * 
                                                    * - the Second Change in Event date/time is empty or the Second Change in 
                                                    *   Event Type field is empty -- either way cannot process.
                                                    *
                                                    */

                                            EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));

                                            }
                                        else
                                            {
                                            // Second Change in Event date/time is NOT empty

                                            if (EventDriver.ChangeDateTime2.Month == intPeriod || EventDriver.RevMonthCard01.Month == intPeriod)
                                                {
                                                // Fill in Second Change in Event date/time

                                                if (Convert.ToDouble(EventDriver.ChangeDateTime2.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // NERC allows for midnight being both 00:00 and 24:00, but the RDMS do not 
                                                    // allow the 24:00 so I have stored 24:00 as 23:59:59.999
                                                    // "re-constructing" the 2400 as midnight

                                                    // douValue = Convert.ToDouble(EventDriver.ChangeDateTime2.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime2.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    // Any other time

                                                    EventStringBuilder.Append(EventDriver.ChangeDateTime2.ToString("MMddHHmm"));
                                                    }

                                                EventStringBuilder.Append(EventDriver.ChangeInEventType2);
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(10, ' '));
                                                }
                                            }

                                        if (EventDriver.IsEndDateTimeNull() == true)
                                            {
                                            // The End of Event date/time is blank (<null>)
                                            EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                            }
                                        else
                                            {
                                            // End of Event date/time is NOT empty

                                            if (EventDriver.EndDateTime.Month == intPeriod || EventDriver.RevMonthCard01.Month == intPeriod)
                                                {
                                                // Fill in the End of Event date/time

                                                if (EventDriver.IsCarryOverNextYearNull())
                                                    {
                                                    EventDriver.CarryOverNextYear = false;
                                                    }

                                                if (EventDriver.CarryOverNextYear == true && EventDriver.EndDateTime.ToString("MMdd") == "1231" &&
                                                    Convert.ToDouble(EventDriver.EndDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    //EventStringBuilder.Append("123124XX");
                                                    EventStringBuilder.Append("12312400"); // 2010 Change
                                                    }
                                                else if (Convert.ToDouble(EventDriver.EndDateTime.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.EndDateTime.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.EndDateTime.ToString("MMddHHmm"));
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                }
                                            }

                                        if (EventDriver.IsGrossAvailCapacityNull() == false && EventDriver.GrossAvailCapacity > 0)
                                            {
                                            EventStringBuilder.Append(Convert.ToInt16(Math.Round(EventDriver.GrossAvailCapacity)).ToString().PadLeft(4, ' '));
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }

                                        if (EventDriver.IsNetAvailCapacityNull() == false && EventDriver.NetAvailCapacity > 0)
                                            {
                                            EventStringBuilder.Append(Convert.ToInt16(Math.Round(EventDriver.NetAvailCapacity)).ToString().PadLeft(4, ' '));
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                            }

                                        if (EventDriver.IsDominantDerateNull() == false && EventDriver.DominantDerate == true)
                                            {
                                            EventStringBuilder.Append(" D");
                                            EventStringBuilder.Append(String.Empty.PadLeft(15, ' '));
                                            }
                                        else
                                            {
                                            EventStringBuilder.Append(String.Empty.PadLeft(17, ' '));
                                            }

                                        EventStringBuilder.Append("01");

                                        if ((strWho.ToUpper() == "NERC" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "NYISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "MISO" && EventDriver.EventType != "NC") || (strWho.ToUpper() == "ISONE" && EventDriver.EventType != "NC"))
                                            {
                                            /*
                                            * NYISO, MISO and ISO-NE does not need NCs and Homer City uses NCs for Economic Dispatch
                                            */
                                            if (EventDriver.StartDateTime.Month == intPeriod || EventDriver.RevMonthCard01.Month == intPeriod)
                                                {
                                                if (EventDriver.RevisionCard01.ToUpper() != "X")
                                                    {
                                                    // Write the Event 01 card
                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                    }
                                                else
                                                    {
                                                    // Write the Event01 card to DELETE THE ENTIRE EVENT

                                                    EventStringBuilder.Length = 0;
                                                    if (intNoOfColumns == 100)
                                                        {
                                                        EventStringBuilder.Insert(0, "07");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Insert(0, 97);
                                                        }
                                                    EventStringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6, '0'));
                                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                        {
                                                        // 82-column data files have a 4-digit year
                                                        EventStringBuilder.Append(EventDriver.Year.ToString());
                                                        }
                                                    else
                                                        {
                                                        // 80-column data files have the 2-digit year -- historical GADS layout
                                                        EventStringBuilder.Append(EventDriver.Year.ToString().Substring(2, 2));
                                                        }

                                                    EventStringBuilder.Append(EventDriver.EventNumber.ToString().PadLeft(4, '0'));

                                                    EventStringBuilder.Append("X");

                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                    }
                                                }
                                            }


                                        if (EventDriver.RevisionCard01.ToUpper() != "X")
                                            {
                                            // if RevisionCard01 is "X" then the entire event is to be deleted
                                            // No 02-99 cards are allowed

                                            // 02 Card

                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                EventStringBuilder.Length = 16;
                                            else
                                                EventStringBuilder.Length = 14;

                                            if (EventDriver.StartDateTime.Month < EventDriver.RevMonthCard02.Month)
                                                {
                                                if (EventDriver.RevisionCard02 != "X" && EventDriver.RevisionCard02 != "0")
                                                    {
                                                    if (EventDriver.RevMonthCard02.Month == intPeriod)
                                                        {
                                                        EventStringBuilder.Append(EventDriver.RevisionCard02);
                                                        }
                                                    else
                                                        {
                                                        intValue = Convert.ToInt16(EventDriver.RevisionCard02) - 1;
                                                        EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                        }
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.RevisionCard02);
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(EventDriver.RevisionCard02);
                                                }

                                            EventStringBuilder.Append(EventDriver.EventType);

                                            EventStringBuilder.Append(EventDriver.CauseCode.ToString().PadLeft(4, '0'));

                                            if (EventDriver.IsCauseCodeExtNull() == true)
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                }
                                            else
                                                {
                                                if (EventDriver.CauseCodeExt.Trim() == string.Empty)
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                    }
                                                //EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                }

                                            if (EventDriver.IsWorkStartedNull() == true)
                                                {
                                                // The Time Work Started date/time is blank (<null>)
                                                EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                }
                                            else
                                                {
                                                // Time Work Started date/time is NOT empty

                                                // Fill in the Time Work Started date/time

                                                if (EventDriver.IsCarryOverLastYearNull())
                                                    {
                                                    EventDriver.CarryOverLastYear = false;
                                                    }

                                                if (EventDriver.CarryOverLastYear == true && EventDriver.WorkStarted.ToString("MMddHHmm") == "01010000")
                                                    {
                                                    //EventStringBuilder.Append("010100XX");
                                                    EventStringBuilder.Append("01010000"); // 2010 Change
                                                    }
                                                else if (Convert.ToDouble(EventDriver.WorkStarted.ToString("HHmmss.fff")) > 235959.500)
                                                    {
                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.WorkStarted.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.WorkStarted.ToString("MMddHHmm"));
                                                    }

                                                }

                                            if (EventDriver.IsWorkEndedNull() == true)
                                                {
                                                // The Time Work Ended date/time is blank (<null>)
                                                EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                }
                                            else
                                                {

                                                // Fill in the Time Work Ended date/time

                                                if (EventDriver.IsCarryOverNextYearNull())
                                                    {
                                                    EventDriver.CarryOverNextYear = false;
                                                    }

                                                if (EventDriver.CarryOverNextYear == true && EventDriver.WorkEnded.ToString("MMdd") == "1231" &&
                                                    Convert.ToDouble(EventDriver.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMdd"));
                                                    //EventStringBuilder.Append("24XX");
                                                    EventStringBuilder.Append("2400"); // 2010 Change
                                                    }
                                                else if (Convert.ToDouble(EventDriver.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                    {
                                                    // douValue = Convert.ToDouble(EventDriver.StartDateTime.ToString("HHmmss.fff"));
                                                    EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMdd"));
                                                    EventStringBuilder.Append("2400");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDriver.WorkEnded.ToString("MMddHHmm"));
                                                    }

                                                }

                                            EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));

                                            EventStringBuilder.Append(EventDriver.ContribCode.ToString().PadLeft(1, ' '));

                                            if (EventDriver.IsPrimaryAlertNull() == true)
                                                {
                                                EventStringBuilder.Append(" ");
                                                }
                                            else
                                                {
                                                if (EventDriver.PrimaryAlert)
                                                    {
                                                    EventStringBuilder.Append("X");
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(" ");
                                                    }
                                                }

                                            if (EventDriver.IsManhoursWorkedNull() == false && EventDriver.ManhoursWorked > 0)
                                                {
                                                EventStringBuilder.Append(EventDriver.ManhoursWorked.ToString().PadLeft(4, ' '));
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                }

                                            if (EventDriver.IsFailureMechCodeNull() == false && EventDriver.FailureMechCode.StartsWith("F"))
                                                {
                                                EventStringBuilder.Append(EventDriver.FailureMechCode);

                                                if (EventDriver.IsTripMechNull() == false && (EventDriver.TripMech.ToUpper() == "A" || EventDriver.TripMech.ToUpper() == "M"))
                                                    {
                                                    EventStringBuilder.Append(EventDriver.TripMech);

                                                    if (EventDriver.IsCumFiredHoursNull() == false)
                                                        {
                                                        EventStringBuilder.Append(EventDriver.CumFiredHours.ToString().PadLeft(6, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                        }

                                                    if (EventDriver.IsCumEngineStartsNull() == false)
                                                        {
                                                        EventStringBuilder.Append(EventDriver.CumEngineStarts.ToString().PadLeft(5, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(5, ' '));
                                                        }

                                                    EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 15));
                                                    }
                                                else
                                                    {

                                                    EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 27));
                                                    }
                                                }
                                            else
                                                {
                                                EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 31));
                                                }

                                            EventStringBuilder.Append("02");

                                            if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                {
                                                // NYISO, MISO and ISO-NE do not want Event 02/03 cards

                                                if (EventDriver.StartDateTime.Month == intPeriod || EventDriver.RevMonthCard02.Month == intPeriod)
                                                    {
                                                    // this writes out the 02 card
                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                    }
                                                // 03 card

                                                if (EventDriver.VerbalDesc86.PadRight(86, ' ').Substring(31, 55).Trim() != String.Empty)
                                                    {
                                                    // the "key" below INCLUDES the 2-character cause code extension

                                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                        EventStringBuilder.Length = 16;
                                                    else
                                                        EventStringBuilder.Length = 14;

                                                    if (EventDriver.IsRevisionCard03Null())
                                                        {
                                                        EventDriver.RevisionCard03 = "0";
                                                        }

                                                    if (EventDriver.StartDateTime.Month < EventDriver.RevMonthCard03.Month)
                                                        {
                                                        if (EventDriver.RevisionCard03 != "X" && EventDriver.RevisionCard03 != "0")
                                                            {
                                                            if (EventDriver.RevMonthCard03.Month == intPeriod)
                                                                {
                                                                EventStringBuilder.Append(EventDriver.RevisionCard03);
                                                                }
                                                            else
                                                                {
                                                                intValue = Convert.ToInt16(EventDriver.RevisionCard03) - 1;
                                                                EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                                }
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.RevisionCard03);
                                                            }
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDriver.RevisionCard03);
                                                        }

                                                    EventStringBuilder.Append(EventDriver.EventType);

                                                    EventStringBuilder.Append(EventDriver.CauseCode.ToString().PadLeft(4, '0'));

                                                    if (EventDriver.IsCauseCodeExtNull() == true)
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                        }
                                                    else
                                                        {
                                                        if (EventDriver.CauseCodeExt.Trim() == string.Empty)
                                                            {
                                                            EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                            }
                                                        //EventStringBuilder.Append(EventDriver.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                        }

                                                    EventStringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(31, 55));

                                                    EventStringBuilder.Append("03");

                                                    if (EventDriver.IsRevisionCard03Null())
                                                        {
                                                        EventDriver.RevisionCard03 = "0";
                                                        }

                                                    if (EventDriver.RevisionCard03.ToUpper() != "X")
                                                        {
                                                        // this card is not deleted; so write it out
                                                        if (EventDriver.StartDateTime.Month == intPeriod || EventDriver.RevMonthCard03.Month == intPeriod)
                                                            {
                                                            // this writes out the 03 card
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }
                                                        }
                                                    else
                                                        {
                                                        // this card is to be deleted
                                                        if (EventDriver.RevMonthCard03.Month == intPeriod)
                                                            {
                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            EventStringBuilder.Append("X");
                                                            EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                            EventStringBuilder.Append("03");
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                            // notes:  remember that the RDMS do NOT allow for 24:00, but 23:59:59.999 is stored.
                                            // also if not NULL the CarryOverLastYear and the CarryOverNextYear fields will 
                                            // control the use of 01/01/yyyy 00XX and 12/31/yyyy 24XX for the date/time fields.

                                            // process the 04/05 - 98/99 cards
                                            foreach (BusinessLayer.AllEventData.EventData02Row EventDetail in EventDriver.GetChildRows("AdditionalWork"))
                                                {

                                                if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                    EventStringBuilder.Length = 16;
                                                else
                                                    EventStringBuilder.Length = 14;

                                                if (EventDriver.StartDateTime.Month < EventDetail.RevMonthCardEven.Month)
                                                    {
                                                    if (EventDetail.RevisionCardEven != "X" && EventDetail.RevisionCardEven != "0")
                                                        {
                                                        if (EventDetail.RevMonthCardEven.Month == intPeriod)
                                                            {
                                                            EventStringBuilder.Append(EventDetail.RevisionCardEven);
                                                            }
                                                        else
                                                            {
                                                            intValue = Convert.ToInt16(EventDetail.RevisionCardEven) - 1;
                                                            EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                            }
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.RevisionCardEven);
                                                        }
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDetail.RevisionCardEven);
                                                    }

                                                EventStringBuilder.Append(EventDriver.EventType);

                                                EventStringBuilder.Append(EventDetail.CauseCode.ToString().PadLeft(4, '0'));

                                                if (EventDetail.IsCauseCodeExtNull() == true)
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                    }
                                                else
                                                    {
                                                    if (EventDetail.CauseCodeExt.Trim() == string.Empty)
                                                        {
                                                        EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                        }
                                                    //EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                    }

                                                if (EventDetail.IsWorkStartedNull() == true)
                                                    {
                                                    // The Time Work Started date/time is blank (<null>)
                                                    EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                    }
                                                else
                                                    {
                                                    // Time Work Started date/time is NOT empty

                                                    // Fill in the Time Work Started date/time

                                                    if (EventDriver.IsCarryOverLastYearNull())
                                                        {
                                                        EventDriver.CarryOverLastYear = false;
                                                        }

                                                    if (EventDriver.CarryOverLastYear == true && EventDetail.WorkStarted.ToString("MMddHHmm") == "01010000")
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMdd"));
                                                        //EventStringBuilder.Append("00XX");
                                                        EventStringBuilder.Append("0000"); // 2010 Change
                                                        }
                                                    else if (Convert.ToDouble(EventDetail.WorkStarted.ToString("HHmmss.fff")) >= 235959.000)
                                                        {
                                                        // douValue = Convert.ToDouble(EventDetail.StartDateTime.ToString("HHmmss.fff"));
                                                        EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMdd"));
                                                        EventStringBuilder.Append("2400");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkStarted.ToString("MMddHHmm"));
                                                        }
                                                    }

                                                if (EventDetail.IsWorkEndedNull() == true)
                                                    {
                                                    // The Time Work Ended date/time is blank (<null>)
                                                    EventStringBuilder.Append(String.Empty.PadLeft(8, ' '));
                                                    }
                                                else
                                                    {
                                                    // Time Work Ended date/time is NOT empty

                                                    // Fill in the Time Work Ended date/time

                                                    if (EventDriver.IsCarryOverNextYearNull())
                                                        {
                                                        EventDriver.CarryOverNextYear = false;
                                                        }

                                                    if (EventDriver.CarryOverNextYear == true && EventDetail.WorkEnded.ToString("MMdd") == "1231" &&
                                                        Convert.ToDouble(EventDetail.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMdd"));
                                                        //EventStringBuilder.Append("24XX");
                                                        EventStringBuilder.Append("2400"); // 2010 Change
                                                        }
                                                    else if (Convert.ToDouble(EventDetail.WorkEnded.ToString("HHmmss.fff")) >= 235959.000)
                                                        {
                                                        // douValue = Convert.ToDouble(EventDetail.StartDateTime.ToString("HHmmss.fff"));
                                                        EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMdd"));
                                                        EventStringBuilder.Append("2400");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(EventDetail.WorkEnded.ToString("MMddHHmm"));
                                                        }

                                                    }

                                                EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));

                                                EventStringBuilder.Append(EventDetail.ContribCode.ToString().PadLeft(1, ' '));

                                                if (EventDetail.IsPrimaryAlertNull() == true)
                                                    {
                                                    EventStringBuilder.Append(" ");
                                                    }
                                                else
                                                    {
                                                    if (EventDetail.PrimaryAlert)
                                                        {
                                                        EventStringBuilder.Append("X");
                                                        }
                                                    else
                                                        {
                                                        EventStringBuilder.Append(" ");
                                                        }
                                                    }

                                                if (EventDetail.IsManhoursWorkedNull() == false && EventDetail.ManhoursWorked > 0)
                                                    {
                                                    EventStringBuilder.Append(EventDetail.ManhoursWorked.ToString().PadLeft(4, ' '));
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(String.Empty.PadLeft(4, ' '));
                                                    }

                                                if (EventDetail.IsFailureMechCodeNull() == false && EventDetail.FailureMechCode.StartsWith("F"))
                                                    {
                                                    EventStringBuilder.Append(EventDetail.FailureMechCode);

                                                    if (EventDetail.IsTripMechNull() == false && (EventDetail.TripMech.ToUpper() == "A" || EventDetail.TripMech.ToUpper() == "M"))
                                                        {
                                                        EventStringBuilder.Append(EventDetail.TripMech);

                                                        if (EventDetail.IsCumFiredHoursNull() == false)
                                                            {
                                                            EventStringBuilder.Append(EventDetail.CumFiredHours.ToString().PadLeft(6, ' '));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(String.Empty.PadLeft(6, ' '));
                                                            }

                                                        if (EventDetail.IsCumEngineStartsNull() == false)
                                                            {
                                                            EventStringBuilder.Append(EventDetail.CumEngineStarts.ToString().PadLeft(5, ' '));
                                                            }
                                                        else
                                                            {
                                                            EventStringBuilder.Append(String.Empty.PadLeft(5, ' '));
                                                            }

                                                        EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 15));
                                                        }
                                                    else
                                                        {

                                                        EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 27));
                                                        }
                                                    }
                                                else
                                                    {
                                                    EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(0, 31));
                                                    }

                                                EventStringBuilder.Append(EventDetail.EvenCardNumber.ToString().PadLeft(2, '0'));

                                                if (((strWho.ToUpper() == "NERC") && boolAllNERCData) || ((strWho.ToUpper() == "ISONE") && boolAllNERCData))
                                                    {
                                                    // NYISO, MISO and ISO-NE do not want Event 04-99 cards

                                                    if (EventDetail.RevisionCardEven.ToUpper() != "X")
                                                        {
                                                        // this card is not deleted; so include it and the odd-numbered mate

                                                        if (EventDriver.StartDateTime.Month == intPeriod || EventDetail.RevMonthCardEven.Month == intPeriod)
                                                            {
                                                            // this writes out the even numbered cards between 04-99
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }

                                                        if (EventDetail.VerbalDesc86.PadRight(86, ' ').Substring(31, 55).Trim() != String.Empty)
                                                            {
                                                            // the "key" below INCLUDES the 2-character cause code extension

                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            if (EventDriver.StartDateTime.Month < EventDetail.RevMonthCardOdd.Month)
                                                                {
                                                                if (EventDetail.RevisionCardOdd != "X" && EventDetail.RevisionCardOdd != "0")
                                                                    {
                                                                    if (EventDetail.RevMonthCardOdd.Month == intPeriod)
                                                                        {
                                                                        EventStringBuilder.Append(EventDetail.RevisionCardOdd);
                                                                        }
                                                                    else
                                                                        {
                                                                        intValue = Convert.ToInt16(EventDetail.RevisionCardOdd) - 1;
                                                                        EventStringBuilder.Append(intValue.ToString().PadLeft(1, '0'));
                                                                        }
                                                                    }
                                                                else
                                                                    {
                                                                    EventStringBuilder.Append(EventDetail.RevisionCardOdd);
                                                                    }
                                                                }
                                                            else
                                                                {
                                                                EventStringBuilder.Append(EventDetail.RevisionCardOdd);
                                                                }

                                                            EventStringBuilder.Append(EventDriver.EventType);

                                                            EventStringBuilder.Append(EventDetail.CauseCode.ToString().PadLeft(4, '0'));

                                                            if (EventDetail.IsCauseCodeExtNull() == true)
                                                                {
                                                                EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                                }
                                                            else
                                                                {
                                                                if (EventDetail.CauseCodeExt.Trim() == string.Empty)
                                                                    {
                                                                    EventStringBuilder.Append(String.Empty.PadLeft(2, ' '));
                                                                    }
                                                                else
                                                                    {
                                                                    EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                                    }
                                                                //EventStringBuilder.Append(EventDetail.CauseCodeExt.Trim().PadLeft(2, ' '));
                                                                }

                                                            EventStringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().PadRight(86, ' ').Substring(31, 55));

                                                            intValue = EventDetail.EvenCardNumber + 1;

                                                            EventStringBuilder.Append(intValue.ToString().PadLeft(2, '0'));

                                                            if (EventDetail.RevisionCardOdd.ToUpper() != "X")
                                                                {
                                                                // this card is not deleted

                                                                if (EventDriver.StartDateTime.Month == intPeriod || EventDetail.RevMonthCardOdd.Month == intPeriod)
                                                                    {
                                                                    // this writes out the odd numbered cards between 04-99
                                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                                    }
                                                                }
                                                            else
                                                                {
                                                                // this card is to be deleted
                                                                if (EventDetail.RevMonthCardOdd.Month == intPeriod)
                                                                    {
                                                                    if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                        EventStringBuilder.Length = 16;
                                                                    else
                                                                        EventStringBuilder.Length = 14;

                                                                    EventStringBuilder.Append("X");
                                                                    EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                                    intValue = EventDetail.EvenCardNumber + 1;
                                                                    EventStringBuilder.Append(intValue.ToString().PadLeft(2, '0'));
                                                                    EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    else
                                                        {
                                                        // this even card is to be deleted as well as the corresponding odd card

                                                        if (EventDetail.RevMonthCardEven.Month == intPeriod)
                                                            {
                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            EventStringBuilder.Append("X");
                                                            EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                            EventStringBuilder.Append(EventDetail.EvenCardNumber.ToString().PadLeft(2, '0'));
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());

                                                            if (intNoOfColumns == 82 || intNoOfColumns == 100)
                                                                EventStringBuilder.Length = 16;
                                                            else
                                                                EventStringBuilder.Length = 14;

                                                            EventStringBuilder.Append("X");
                                                            EventStringBuilder.Append(String.Empty.PadLeft(63, ' '));
                                                            intValue = EventDetail.EvenCardNumber + 1;
                                                            EventStringBuilder.Append(intValue.ToString().PadLeft(2, '0'));
                                                            EventFileWriter.WriteLine(EventStringBuilder.ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        break;

                                    default:

                                        break;
                                    }
                                }
                            }
                        }
                    catch (System.Data.StrongTypingException e)
                        {
                        stringException = e.ToString();
                        lResult = false;
                        // MessageBox.Show(e.ToString());
                        }

                    #endregion
                    }
                }
            catch (Exception e)
                {
                stringException = e.ToString();
                lResult = false;
                // throw(e);
                }
            finally
                {
                PerfFileWriter.Close();
                EventFileWriter.Close();

                if ((strWho.ToUpper() == "MISO") || (strWho.ToUpper() == "ISONE") || (strWho.ToUpper() == "NERC"))
                    {
                    //Application.DoEvents();

                    //					stringPerfFile = "Perf_" + strFileName;
                    //					stringEventFile = "Event_" + strFileName;
                    //					stringMISOFile = "Combined_" + stringFileName;

                    if (File.Exists(stringPerfFile) && File.Exists(stringEventFile))
                        {
                        StreamWriter sw = null;
                        StreamReader sr1 = null;
                        StreamReader sr2 = null;
                        string input;

                        try
                            {
                            if (File.Exists(stringMISOFile))
                                {
                                File.Delete(stringMISOFile);
                                //Application.DoEvents();
                                }

                            sw = File.CreateText(stringMISOFile);

                            sr1 = File.OpenText(stringPerfFile);

                            while ((input = sr1.ReadLine()) != null)
                                {
                                sw.WriteLine(input);
                                }

                            sr1.Close();

                            sr2 = File.OpenText(stringEventFile);

                            while ((input = sr2.ReadLine()) != null)
                                {
                                sw.WriteLine(input);
                                }

                            sr2.Close();

                            sw.Flush();
                            sw.Close();
                            }
                        catch (Exception e)
                            {
                            stringException = e.ToString();
                            lResult = false;
                            }
                        finally
                            {
                            //Application.DoEvents();
                            }
                        }
                    }

                blConnect.WriteStatusUTC(strWho, stringEventFile, stringPerfFile, "");
                }

            if (lResult)
                return "OK -- " + strFileName;
            else
                return "ERROR -- " + strFileName;
            }

        public string FileType
            {
            get
                {
                return WriteFileType;
                }
            set
                {
                WriteFileType = value;
                }
            }
        }
    }
