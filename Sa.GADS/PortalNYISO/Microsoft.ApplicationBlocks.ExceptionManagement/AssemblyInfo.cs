//===============================================================================
// Microsoft Exception Management Application Block for .NET
// http://msdn.microsoft.com/library/en-us/dnbda/html/emab-rm.asp
//
// AssemblyInfo.cs
// This file sets attributes for the 
// Microsoft.ApplicationBlocks.ExceptionManagement assembly.
//===============================================================================
// Copyright (C) 2000-2001 Microsoft Corporation
// All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================


using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Security; 

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("Modified Microsoft Application Block")]
[assembly: AssemblyDescription("Version 1.1 upgraded to 3.5")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GADS Open Source Project")]
[assembly: AssemblyProduct("GADS Open Source")]
[assembly: AssemblyCopyright("2011 The Outercurve Foundation")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]	
[assembly: CLSCompliant (false)]
[assembly: ComVisible (false)]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("12.0.0.0")]
[assembly: AssemblyFileVersionAttribute("16.1.8.0")]
[assembly: GuidAttribute("BC3570B7-9F1C-4FB2-B55A-DEFEAFBE9F96")]
