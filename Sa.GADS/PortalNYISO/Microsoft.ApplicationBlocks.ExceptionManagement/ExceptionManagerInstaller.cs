//===============================================================================
// Microsoft Exception Management Application Block for .NET
// http://msdn.microsoft.com/library/en-us/dnbda/html/emab-rm.asp
//
// ExceptionManagerInstaller.cs
// This file contains the CustomPublisherException class, which it created when 
// a publisher throws an exception to the ExceptionManager class.
//
// For more information see the Implementation of the CustomPublisherException 
// Class section of the Exception Management Application Block Implementation Overview. 
//===============================================================================
// Copyright (C) 2000-2001 Microsoft Corporation
// All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Resources;
using System.Reflection;

namespace Microsoft.ApplicationBlocks.ExceptionManagement
{
	/// <summary>
	/// Installer class used to create two event sources for the 
	/// Exception Management Application Block to function correctly.
	/// </summary>
	[RunInstaller(true)]
	public class ExceptionManagerInstaller : System.Configuration.Install.Installer
	{
		private System.Diagnostics.EventLogInstaller exceptionManagerEventLogInstaller;
		private System.Diagnostics.EventLogInstaller exceptionManagementEventLogInstaller;
		
		private static ResourceManager resourceManager = new ResourceManager(typeof(ExceptionManager).Namespace + ".ExceptionManagerText",Assembly.GetAssembly(typeof(ExceptionManager)));
		
		/// <summary>
		/// Constructor with no params.
		/// </summary>
		public ExceptionManagerInstaller()
		{
			// Initialize variables.
			InitializeComponent();
		}

		/// <summary>
		/// Initialization function to set internal variables.
		/// </summary>
		private void InitializeComponent()
		{
			this.exceptionManagerEventLogInstaller = new System.Diagnostics.EventLogInstaller();
			this.exceptionManagementEventLogInstaller = new System.Diagnostics.EventLogInstaller();
			// 
			// exceptionManagerEventLogInstaller
			// 
			this.exceptionManagerEventLogInstaller.Log = "Application";
			this.exceptionManagerEventLogInstaller.Source = resourceManager.GetString("RES_EXCEPTIONMANAGER_INTERNAL_EXCEPTIONS");
			// 
			// exceptionManagementEventLogInstaller
			// 
			this.exceptionManagementEventLogInstaller.Log = "Application";
			this.exceptionManagementEventLogInstaller.Source = resourceManager.GetString("RES_EXCEPTIONMANAGER_PUBLISHED_EXCEPTIONS");

			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.exceptionManagerEventLogInstaller,
																					  this.exceptionManagementEventLogInstaller});
		}
	}
}

