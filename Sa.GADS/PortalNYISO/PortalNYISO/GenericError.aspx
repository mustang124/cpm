﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GenericError.aspx.vb" Inherits="PortalNYISO.GenericError" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>NYISO GADS Portal</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a href="MainDE.aspx">
            <img id="BannerImage" src="images/logo.gif" alt="NYISO GADS Portal" /></a>
        <table>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-family: Arial; font-size: medium; height: 24px;">
                    The following error has occured:
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="ErrorTextLabel" runat="server" EnableViewState="false" Style="font-family: Arial;
                        font-size: medium; height: 24px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="HomeButton" runat="server" Width="150px" Height="26px" Text="Go to home page">
                    </asp:Button>
                </td>
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>

