﻿Imports System
Imports System.Text
Imports System.Collections
Imports System.Web.Configuration
Imports System.DirectoryServices.Protocols
Imports System.Web.Security
Imports System.Security
Imports System.Security.Principal

Namespace FormsAuth

    Public Class LdapAuthentication

        Dim _path As String = ""  ' "ldap.stage.nyiso.com"

        Public Sub New()
            _path = WebConfigurationManager.AppSettings("ldapPath")
        End Sub

        Public Function IsAuthenticated(ByVal username As String, ByVal pwd As String) As Boolean

            Dim validation As Boolean = False

            Dim ldapDir As LdapDirectoryIdentifier = New LdapDirectoryIdentifier(_path)  ' ldap.stage.nyiso.com
            Dim ldapConn As LdapConnection = New LdapConnection(ldapDir)

            ldapConn.SessionOptions.SecureSocketLayer = False

            Dim myAuthType As AuthType = AuthType.Basic

            Select Case WebConfigurationManager.AppSettings("AuthType")
                Case "Anonymous"
                    myAuthType = AuthType.Anonymous
                Case "Basic"
                    myAuthType = AuthType.Basic
                Case "Digest"
                    myAuthType = AuthType.Digest
                Case "Dpa"
                    myAuthType = AuthType.Dpa
                Case "External"
                    myAuthType = AuthType.External
                Case "Kerberos"
                    myAuthType = AuthType.Kerberos
                Case "Msn"
                    myAuthType = AuthType.Msn
                Case "Negotiate"
                    myAuthType = AuthType.Negotiate
                Case "Ntlm"
                    myAuthType = AuthType.Ntlm
                Case "Sicily"
                    myAuthType = AuthType.Sicily
            End Select

            ldapConn.AuthType = myAuthType

            ' Another little wrinkle involves the use of SSL connections over port 636. 
            'It is not enough to just set the port number in the LdapDirectoryIdentifier parameter, 
            'it is also necessary to to set the SecureSocketLayer property to true within the SessionOptions.

            ' Update the next line to include the Fully Qualified LDAP name of the user along with that user's password
            ' needs to be the exact path "uid=xxxxx,ou=internal,ou=users,ou=markets,dc=nyiso,dc=com" or it will fail.

            Dim domain As String = WebConfigurationManager.AppSettings("FullyQualifiedUsernameTemplate")  ' MP LDAP directory (external)

            If domain.Trim = String.Empty Then
                Return False
            End If

            Dim myCredentials As System.Net.NetworkCredential = New System.Net.NetworkCredential(domain.Replace("xxxxx", username.Trim), pwd)

            ldapConn.Credential = myCredentials
            ldapConn.Timeout = New TimeSpan(0, 0, 30)
            ldapConn.SessionOptions.ProtocolVersion = 3

            Try
                'This is the actual Connection establishment here
                ldapConn.Bind()
                validation = True
            Catch e1 As ObjectDisposedException
                Throw New Exception("e1 The object handle is not valid. " & e1.Message)
            Catch e2 As LdapException
                Throw New Exception(e2.Message & " (" & e2.ErrorCode & ")")
            Catch e3 As InvalidOperationException
                Throw New Exception("AuthType property is Anonymous and one or more credentials are supplied (e3). " & e3.Message)
            Catch e4 As DirectoryOperationException

                domain = WebConfigurationManager.AppSettings("FullyQualifiedUsernameTemplateInternal")  ' NYISO staff LDAP directory (internal)

                If domain.Trim = String.Empty Then
                    Return False
                End If

                myCredentials = New System.Net.NetworkCredential(domain.Replace("xxxxx", username.Trim), pwd)

                ldapConn.Credential = myCredentials
                ldapConn.Timeout = New TimeSpan(0, 0, 30)
                ldapConn.SessionOptions.ProtocolVersion = 3

                Try
                    'This is the actual Connection establishment here
                    ldapConn.Bind()
                    validation = True
                Catch e1 As ObjectDisposedException
                    Throw New Exception("e1 The object handle is not valid. " & e1.Message)
                Catch e2 As LdapException
                    Throw New Exception(e2.Message & " (" & e2.ErrorCode & ")")
                Catch e3 As InvalidOperationException
                    Throw New Exception("AuthType property is Anonymous and one or more credentials are supplied (e3). " & e3.Message)
                Catch e4a As DirectoryOperationException
                    Throw New Exception("Invalid user name (" & e4a.Message & ")")
                Catch ex As Exception
                    Throw New Exception("LdapConnection.Bind Method exception (ex). " & ex.Message)
                End Try

            Catch ex As Exception
                Throw New Exception("LdapConnection.Bind Method exception (ex). " & ex.Message)
            End Try

            validation = False

            Try

                'The DN for the user is
                '   uid=FLUEGGE,ou=internal,ou=users,ou=markets,dc=nyiso,dc=com
                'The DN for the group is
                '   cn=GadsObserver,ou=GADS,ou=groups,ou=markets,dc=nyiso,dc=com
                Dim ldapSR As SearchRequest = New SearchRequest() With {
                    .DistinguishedName = WebConfigurationManager.AppSettings("SearchRequestDN"), ' ou=GADS,ou=groups,ou=markets,dc=nyiso,dc=com
                    .Filter = WebConfigurationManager.AppSettings("SearchRequestFilter").Replace("yyyyy", domain.Replace("xxxxx", username.Trim)), ' "(uniqueMember=uid=FLUEGGE,ou=internal,ou=users,ou=markets,dc=nyiso,dc=com)"
                    .Scope = System.DirectoryServices.Protocols.SearchScope.Subtree
                }

                'Search for the user
                Dim ldapSResp As SearchResponse = DirectCast(ldapConn.SendRequest(ldapSR), SearchResponse)

                If ldapSResp.Entries.Count >= 1 Then
                    validation = True
                Else
                    validation = False
                End If

            Catch e5 As DirectoryOperationException
                Throw New Exception("e5 Invalid SearchRequest (" & e5.InnerException.ToString & ")" & e5.Message)
            Catch e6 As LdapException
                Throw New Exception("LdapException exception (e6: " & e6.ErrorCode & " ). " & e6.Message)
            Catch ex As Exception
                Throw New Exception("ex Invalid SearchRequest (" & ex.InnerException.ToString & ")" & ex.Message)
            End Try

            Return validation

        End Function

    End Class

End Namespace