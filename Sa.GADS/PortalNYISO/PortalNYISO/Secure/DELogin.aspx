﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DELogin.aspx.vb" Inherits="PortalNYISO.DELogin" Debug="false" Trace="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NYISO GADS Secure Login</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>

    <form id="LoginForm" runat="server">
        <table style="padding: 8px; font-family: Tahoma;">
            <tr>
                <td>
                    <asp:Image ID="Image1" ImageUrl="logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-family: Tahoma; font-weight: 500; font-size: x-large;" colspan="2">NYISO GADS Portal
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-family: Tahoma; font-weight: 500; font-size: large;" colspan="2">Enter User Name &amp; Password
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: right">User Name:</td>
                <td>
                    <asp:TextBox ID="UserName" runat="server"></asp:TextBox></td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage="User Name required" ControlToValidate="UserName">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revUserName" ErrorMessage="Invalid User Name" ControlToValidate="UserName" Display="Static"
                        ValidationExpression="^[a-zA-Z0-9]\w{2,100}$" runat="server">*</asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td style="text-align: right">Password:</td>
                <td>
                    <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox></td>
                <td>
                    <%--<asp:RequiredFieldValidator ID="rfvPassword" ErrorMessage="Password required" ControlToValidate="Password" runat="server">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revPassword" ErrorMessage="Invalid Password" ControlToValidate="Password" Display="Static"
                        ValidationExpression="^[a-zA-Z0-9$]\w{3,20}$" runat="server">*</asp:RegularExpressionValidator>--%>

                    <asp:RequiredFieldValidator ID="rfvPassword" ErrorMessage="Password required" ControlToValidate="Password" runat="server">*</asp:RequiredFieldValidator>

                    <%--Minimum 8 and Maximum 20 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:--%>
                    <%--<asp:RegularExpressionValidator ID="revPassword" ErrorMessage="Invalid Password" ControlToValidate="Password" Display="Static"
                        ValidationExpression="^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,20}$" runat="server">*</asp:RegularExpressionValidator>--%>
                </td>

            </tr>
            <tr>
                <td colspan="3">
                    <h6>(User Name and Password are case sensitive)</h6>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btnLogin" runat="server" Text="Log In"></asp:Button></td>
                <td colspan="2">
                    <asp:CheckBox ID="PersistCookie" runat="server" Text="Remember Login" Checked="false" Visible="false" Enabled="false"></asp:CheckBox></td>
            </tr>
        </table>
        <asp:Label ID="lblError" runat="server" Font-Names="Arial" Text=""></asp:Label>
        <asp:ValidationSummary ID="vsLogin" runat="server" Font-Names="Arial" HeaderText="You have the following errors"></asp:ValidationSummary>
        <hr />
        <h4 style="font-family: Tahoma" title="NYISO GADS Secure Login">
            <asp:Label ForeColor="Red" ID="Output" runat="server"></asp:Label></h4>
    </form>
</body>
</html>
