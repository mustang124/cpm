Imports System.Collections

Namespace Process

    Public Class WorkManager
        Inherits GADSNG.Base.DALBase

        'this class was created to be able to get at the datafactory, since it is only available to classes derived from dalbase

        Public Enum ProcessStatus 'must be updated in WebProcessEnum table
            Queued = 0        'not yet processed
            Working           'processing
            Completed         'finished processing
            Rejected          'error found
            None              'when a user has never started a process or no process queued
            Aborted           'user cancelled queued item
            Invalid           'returned when requesting info on an invalid workid
            InvalidFileFormat 'for FileCheck type
        End Enum

        Public Enum ProcessType 'must be updated in WebProcessEnum table
            FileCheck = 0  'ASCII test routines
            DataCheck      'ISO routines
            Analysis       'Data analysis routines
            None           'for invalid processes, requests, etc.
        End Enum

        Public Enum LogTypes 'must be updated in SQL table
            Information = 0  'misc info
            Alert            'possible failure or warning
            GeneralError     'error source unknown
            DBError          'error on database operation
            FileError        'error on file operation
            CodeError        'error in source code
            ThreadError      'error in worker thread
            WebError         'IIS or other related web errors
        End Enum

        Public Sub New(ByVal Connect As String)
            MyBase.New(Connect)
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Public Sub UpdateStatus(ByVal WorkID As Integer, ByVal NewStatus As ProcessStatus, Optional ByVal ReasonForFailure As String = "")

            'Dim NextWorkID As Integer

            If ReasonForFailure <> "" AndAlso ReasonForFailure.Length > 200 Then
                ReasonForFailure = ReasonForFailure.Substring(0, 200)                'trim for database
            End If

            Dim SQL As String = String.Empty

            If Factory.Provider = "Oracle" Then
                SQL = "UPDATE WebProcess SET STATUS=" & CInt(NewStatus).ToString & ", REASONFORFAILURE='" & ReasonForFailure & "', " & _
                "UPDATED = SYSDATE WHERE WORKID=" & WorkID.ToString
            Else
                SQL = "UPDATE WebProcess SET Status=" & CInt(NewStatus).ToString & ", ReasonForFailure='" & ReasonForFailure & "', " & _
                "Updated='" & Now.ToShortDateString & " " & Now.ToLongTimeString & "' WHERE WORKID=" & WorkID.ToString
            End If

            Try
                Factory.ExecuteNonQuery(SQL, Nothing)
            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error updating status. (" & ex.Message & ")")
            End Try

        End Sub

        Public Function GetNextQueuedItem(ByVal Username As String) As Integer

            Dim NextWorkID As Integer = 0

            Dim SQL As String = ""

            If Factory.Provider = "Oracle" Then
                SQL = "SELECT MAX(WORKID) FROM WebProcess WHERE STATUS=0 AND USERNAME = '" & Username & "' ORDER BY PROCESSTYPE, INITIATED"
            Else
                SQL = "SELECT TOP 1 WORKID FROM WebProcess WHERE STATUS=0 AND USERNAME = '" & Username & "' ORDER BY PROCESSTYPE, INITIATED"
            End If

            Try
                NextWorkID = CInt(Factory.ExecuteScalar(SQL, Nothing))
            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error retrieving next queued item. (" & ex.Message & ")")
                Return 0
            End Try

            Return NextWorkID            'what does this return on no items?

        End Function

        Public Function GetQueuedItemCount(ByVal Username As String) As Integer

            Dim ItemsInQueue As Integer = 0
            Dim SQL As String = "SELECT COUNT(WORKID) FROM WebProcess WHERE STATUS=0 AND USERNAME = '" & Username & "'"
            Try
                Dim myObj As Object = Factory.ExecuteScalar(SQL, Nothing)
                If Not IsNothing(myObj) Then
                    Try
                        ItemsInQueue = CInt(Factory.ExecuteScalar(SQL, Nothing))
                    Catch ex As Exception
                        LogMessageToDB(LogTypes.DBError, "Error retrieving queued item count. (" & ex.Message & ")")
                        Return 0
                    End Try
                End If
            Catch ex As Exception
                Return 0
            End Try

            Return ItemsInQueue

        End Function

        Public Function GetHistoryList(ByVal Username As String) As ArrayList

            Dim HistoryList As New ArrayList

            Dim SQL As String

            'ThisMonthDate variable is created so that regional settings will take effect, and history only shows current month
            Dim ThisMonthDate As New Date(Now.Year, Now.Month, 1)

            'this will retrieve all completed and aborted processes this month, except for today, which are shown in the process list

            If Factory.Provider = "Oracle" Then
                SQL = "SELECT INITIATED, T.DESCRIPTION ProcessTypeDesc, S.DESCRIPTION StatusDesc FROM WebProcess W " & _
               "INNER JOIN WebProcessTypes T ON T.PROCESSTYPE=W.PROCESSTYPE INNER JOIN WebProcessStatus S ON S.STATUS=W.STATUS " & _
               "WHERE W.STATUS IN(2,3,5,6) AND UPDATED < to_date ('" & Now.ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS') " & _
               "AND UPDATED > to_date ('" & ThisMonthDate.ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS') AND USERNAME = '" & Username & "' ORDER BY INITIATED DESC"
            Else
                SQL = "SELECT INITIATED, T.DESCRIPTION ProcessTypeDesc, S.DESCRIPTION StatusDesc FROM WebProcess W " & _
               "INNER JOIN WebProcessTypes T ON T.PROCESSTYPE=W.PROCESSTYPE INNER JOIN WebProcessStatus S ON S.STATUS=W.STATUS " & _
               "WHERE W.STATUS IN(2,3,5,6) AND UPDATED < '" & Now.ToShortDateString & "' " & _
               "AND UPDATED > '" & ThisMonthDate.ToShortDateString & "' AND USERNAME = '" & Username & "' ORDER BY INITIATED DESC"
            End If

            Dim ProcessReader As IDataReader

            Try
                ProcessReader = Factory.ExecuteDataReader(SQL, Nothing, System.Data.CommandBehavior.CloseConnection)

                Do While ProcessReader.Read
                    Dim NewItem As New ProcessInfo With {
                        .Initiated = CType(ProcessReader("Initiated"), Date),
                        .ProcessTypeDesc = ProcessReader("ProcessTypeDesc").ToString,
                        .StatusDesc = ProcessReader("StatusDesc").ToString
                    }
                    HistoryList.Add(NewItem)
                Loop

                ProcessReader.Close()
                ProcessReader = Nothing

            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error retrieving history list. (" & ex.Message & ")")
                Return Nothing
            End Try

            Return HistoryList

        End Function

        Public Function GetHistoryList2(ByVal Username As String) As ArrayList

            Dim HistoryList As New ArrayList

            Dim SQL As String

            'ThisMonthDate variable is created so that regional settings will take effect, and history only shows current month
            Dim ThisMonthDate As New Date(Now.Year, Now.Month, 1)

            'this will retrieve all completed and aborted processes this month, except for today, which are shown in the process list
            'SQL = "SELECT INITIATED, T.DESCRIPTION ProcessTypeDesc, S.DESCRIPTION StatusDesc FROM WebProcess W " & _
            '   "INNER JOIN WebProcessTypes T ON T.PROCESSTYPE=W.PROCESSTYPE INNER JOIN WebProcessStatus S ON S.STATUS=W.STATUS " & _
            '   "WHERE W.STATUS IN(2,3,5,6) AND UPDATED < '" & Now.ToShortDateString & "' " & _
            '   "AND UPDATED > '" & ThisMonthDate.AddMonths(-1).ToShortDateString & "' AND USERNAME = '" & Username & "' ORDER BY INITIATED DESC"

            If Factory.Provider = "Oracle" Then
                SQL = "SELECT INITIATED, T.DESCRIPTION ProcessTypeDesc, S.DESCRIPTION StatusDesc FROM WebProcess W " & _
               "INNER JOIN WebProcessTypes T ON T.PROCESSTYPE=W.PROCESSTYPE INNER JOIN WebProcessStatus S ON S.STATUS=W.STATUS " & _
               "WHERE W.STATUS IN(2,3,5,6) AND UPDATED < to_date ('" & Now.ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS') " & _
               "AND UPDATED > to_date ('" & ThisMonthDate.AddMonths(-1).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS') AND USERNAME = '" & Username & "' ORDER BY INITIATED DESC"
            Else
                SQL = "SELECT INITIATED, T.DESCRIPTION ProcessTypeDesc, S.DESCRIPTION StatusDesc FROM WebProcess W " & _
               "INNER JOIN WebProcessTypes T ON T.PROCESSTYPE=W.PROCESSTYPE INNER JOIN WebProcessStatus S ON S.STATUS=W.STATUS " & _
               "WHERE W.STATUS IN(2,3,5,6) AND UPDATED < '" & Now.ToShortDateString & "' " & _
               "AND UPDATED > '" & ThisMonthDate.AddMonths(-1).ToShortDateString & "' AND USERNAME = '" & Username & "' ORDER BY INITIATED DESC"
            End If

            Dim ProcessReader As IDataReader

            Try
                ProcessReader = Factory.ExecuteDataReader(SQL, Nothing, System.Data.CommandBehavior.CloseConnection)

                Do While ProcessReader.Read
                    Dim NewItem As New ProcessInfo With {
                        .Initiated = CType(ProcessReader("Initiated"), Date),
                        .ProcessTypeDesc = ProcessReader("ProcessTypeDesc").ToString,
                        .StatusDesc = ProcessReader("StatusDesc").ToString
                    }
                    HistoryList.Add(NewItem)
                Loop

                ProcessReader.Close()
                ProcessReader = Nothing

            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error retrieving history list. (" & ex.Message & ")")
                Return Nothing
            End Try

            Return HistoryList

        End Function

        Public Function GetProcessList(ByVal Username As String) As ArrayList

            Dim ProcessList As New ArrayList

            'TodayDate variable is created so that regional settings will take effect, and process only shows items updated today
            Dim TodayDate As New Date(Now.Year, Now.Month, Now.Day, 0, 0, 0)

            Dim SQL As String

            'this will retrieve all active processes as well as any completed or aborted today

            If Factory.Provider = "Oracle" Then
                SQL = "SELECT WORKID from WebProcess WHERE (STATUS IN (0,1) OR (STATUS IN (2,3,5,6,7) AND UPDATED > to_date ('" & TodayDate.ToString("MM/dd/yyyy HH:mm:ss") & _
                    "', 'mm/dd/yyyy HH24:MI:SS') )) AND USERNAME = '" & Username & "' ORDER BY INITIATED DESC, STATUS"
            Else
                SQL = "SELECT WORKID from WebProcess WHERE (STATUS IN (0,1) OR (STATUS IN (2,3,5,6,7) AND UPDATED > '" & TodayDate.ToShortDateString & " " & _
               TodayDate.ToShortTimeString & "')) AND USERNAME = '" & Username & "' ORDER BY INITIATED DESC, STATUS"
            End If

            Dim ProcessReader As IDataReader

            Try
                ProcessReader = Factory.ExecuteDataReader(SQL, Nothing, System.Data.CommandBehavior.CloseConnection)

                If Not ProcessReader Is Nothing Then

                    Do While ProcessReader.Read
                        ProcessList.Add(CType(ProcessReader("WORKID"), Integer))
                    Loop

                    ProcessReader.Close()
                    ProcessReader = Nothing

                End If

            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error retrieving process list. (" & ex.Message & ")")
                Return Nothing
            End Try

            Return ProcessList

        End Function

        Public Sub LogUser(ByVal Username As String, ByVal AccessTime As Date, ByVal IPAddress As String)

            If Not Username Is Nothing AndAlso Username.Length > 50 Then
                Username = Username.Substring(0, 50)
            End If

            If Not IPAddress Is Nothing AndAlso IPAddress.Length > 15 Then
                IPAddress = IPAddress.Substring(0, 15)
            End If

            If Username.Trim = String.Empty Then
                Username = "UNKNOWN"
            End If

            Dim SQL As String

            If Factory.Provider = "Oracle" Then
                SQL = "INSERT INTO AccessLog (USERNAME, ACCESSTIME, IPADDRESS) " & _
                "VALUES ('" & Username & "',SYSDATE,'" & IPAddress & "')"
            Else
                SQL = "INSERT INTO AccessLog (Username, AccessTime, IPAddress) " & _
               "VALUES ('" & Username & "','" & AccessTime.ToShortDateString & " " & AccessTime.ToLongTimeString & "','" & IPAddress & "')"
            End If

            Try
                Factory.ExecuteNonQuery(SQL, Nothing)
            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error logging access of user. (" & ex.Message & ")")
            End Try

        End Sub

        Public Sub LogMessageToDB(ByVal MessageType As LogTypes, ByVal Message As String, Optional ByVal Message2 As String = "", Optional ByVal Message3 As String = "")

            If Not Message Is Nothing AndAlso Message.Length > 300 Then
                Message = Message.Substring(0, 300)
            End If

            If Not Message2 Is Nothing AndAlso Message2.Length > 300 Then
                Message2 = Message2.Substring(0, 300)
            End If

            If Not Message3 Is Nothing AndAlso Message3.Length > 300 Then
                Message3 = Message3.Substring(0, 300)
            Else
                Message3 = "(" & Now.ToShortDateString & " " & Now.ToLongTimeString & ")"
            End If

            Dim SQL As String
            SQL = "INSERT INTO WebProcessLog (TYPE, MESSAGE, MESSAGE2, MESSAGE3) " & _
               "VALUES (" & CInt(MessageType) & ",'" & Message & "','" & Message2 & "','" & Message3 & "')"

            Try
                Factory.ExecuteNonQuery(SQL, Nothing)
            Catch ex As Exception
                'cannot do anything here except catch error since logging is not possible
            End Try

        End Sub

        Public Function GetProcessInfo(ByVal WorkID As Integer) As ProcessInfo

            'load status from db
            Dim NewProcessInfo As ProcessInfo = Nothing

            Dim SQL As String
            SQL = "SELECT WORKID, USERNAME, W.PROCESSTYPE, W.STATUS, UPLOADFILE, INITIATED, UPDATED, T.DESCRIPTION ProcessTypeDesc, S.DESCRIPTION StatusDesc " & _
               "FROM WebProcess W INNER JOIN WebProcessTypes T ON T.PROCESSTYPE=W.PROCESSTYPE INNER JOIN WebProcessStatus S ON S.STATUS=W.STATUS " & _
               "WHERE WORKID = " & WorkID.ToString

            Dim ProcessTable As DataTable

            Try
                ProcessTable = Factory.GetDataTable(SQL, Nothing)

                If ProcessTable.Rows.Count = 0 Then
                    'invalid id passed
                    NewProcessInfo.Status = ProcessStatus.Rejected
                    NewProcessInfo.User = ""
                    NewProcessInfo.Initiated = Now()
                    NewProcessInfo.ProcessType = ProcessType.None
                Else
                    NewProcessInfo.Status = CType(ProcessTable.Rows(0)("STATUS"), ProcessStatus)
                    NewProcessInfo.ProcessType = CType(ProcessTable.Rows(0)("PROCESSTYPE"), ProcessType)

                    If IsDBNull(ProcessTable.Rows(0)("UPLOADFILE")) Then
                        NewProcessInfo.UploadFile = ""
                    Else
                        NewProcessInfo.UploadFile = ProcessTable.Rows(0)("UPLOADFILE").ToString
                    End If

                    'add filename to status description
                    If NewProcessInfo.ProcessType = ProcessType.FileCheck Then
                        NewProcessInfo.StatusDesc = "(" & NewProcessInfo.UploadFile.Split("_"c)(2) & ") " & ProcessTable.Rows(0)("StatusDesc").ToString
                    Else
                        NewProcessInfo.StatusDesc = ProcessTable.Rows(0)("StatusDesc").ToString
                    End If

                    NewProcessInfo.ProcessTypeDesc = ProcessTable.Rows(0)("ProcessTypeDesc").ToString
                    NewProcessInfo.User = ProcessTable.Rows(0)("USERNAME").ToString
                    NewProcessInfo.Initiated = CType(ProcessTable.Rows(0)("INITIATED"), Date)
                    NewProcessInfo.Updated = CType(ProcessTable.Rows(0)("UPDATED"), Date)
                    NewProcessInfo.WorkID = WorkID

                End If

                ProcessTable = Nothing

            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error retrieving process info. (" & ex.Message & ")")
                Return Nothing
            End Try

            Return NewProcessInfo

        End Function

        Public Sub AddProcess(ByVal User As String, ByVal Type As ProcessType, Optional ByVal Filename As String = "")

            Dim SQL As String

            If Factory.Provider = "Oracle" Then
                SQL = "INSERT INTO WebProcess (USERNAME, PROCESSTYPE, STATUS, UPLOADFILE, INITIATED, UPDATED) " & _
               "VALUES ('" & User & "'," & _
               CType(Type, Integer).ToString & "," & _
               "0,'" & _
               Filename & "',SYSDATE,SYSDATE)"
            Else
                SQL = "INSERT INTO WebProcess (Username, ProcessType, Status, UploadFile, Initiated, Updated) " & _
               "VALUES ('" & User & "'," & _
               CType(Type, Integer).ToString & "," & _
               "0,'" & _
               Filename & "','" & _
               Now.ToShortDateString & " " & Now.ToLongTimeString & "','" & _
               Now.ToShortDateString & " " & Now.ToLongTimeString & "')"

            End If

            Try
                Factory.ExecuteNonQuery(SQL, Nothing)
            Catch ex As Exception
                LogMessageToDB(LogTypes.DBError, "Error adding process to queue. (" & ex.Message & ")")
            End Try

        End Sub

        Public Sub AbortItem(ByVal WorkID As Integer)

            If GetProcessInfo(WorkID).Status <> ProcessStatus.Working Then

                Dim SQL As String
                SQL = "UPDATE WebProcess SET Status = " & CType(ProcessStatus.Aborted, Integer).ToString & " WHERE WORKID = " & WorkID.ToString

                Try
                    Factory.ExecuteNonQuery(SQL, Nothing)
                Catch ex As Exception
                    LogMessageToDB(LogTypes.DBError, "Error aborting item. (" & ex.Message & ")")
                End Try

            End If

        End Sub

    End Class

    Public Structure ProcessInfo

        Public Initiated As Date
        Public Updated As Date
        Public User As String
        Public Status As ProcessStatus
        Public StatusDesc As String
        Public WorkID As Integer
        Public ProcessType As ProcessType
        Public ProcessTypeDesc As String
        Public UploadFile As String
        Public ReasonForFailure As String

        'these properties are supplied as required for use by the databinder
        Public ReadOnly Property EvalInitiated() As Date
            Get
                Return Initiated
            End Get
        End Property

        Public ReadOnly Property EvalStatusDesc() As String
            Get
                Return StatusDesc
            End Get
        End Property

        Public ReadOnly Property EvalProcessTypeDesc() As String
            Get
                Return ProcessTypeDesc
            End Get
        End Property

    End Structure

End Namespace
