﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportsPage.aspx.vb" Inherits="PortalNYISO.ReportsPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reports Page</title>
    <link href="../NewStyles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" style="padding-left:5px;">
        <table>
            <tr>
                <td><a href="../MainDE.aspx">
                    <asp:Image ID="Image1" ImageUrl="../images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" /></a>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnUandP" runat="server" Height="24" Width="150" Text="Upload &amp; Process"></asp:Button>
                    <asp:Button ID="btnEditData" runat="server" Height="24" Width="150" Text="Edit Data" />
                    <asp:Button ID="btnReports" runat="server" Height="24" Width="150" Text="Reports" />
                    <asp:Button ID="HelpIndex" runat="server" Height="24" Width="150" Text="Help Index" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Heading" runat="server" Text="Report List:" CssClass="Header"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnDoReports" runat="server" Height="24" Width="150" Text="Std EFORd Report"></asp:Button>
                </td>
                <td>
                    <asp:Label ID="ReportLabel" runat="server" CssClass="Message" Width="362px" Text="View Standard EFORd Demand Statistics Report"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label7" runat="server" Font-Size="9pt">Select Starting Month</asp:Label><br />
                    <asp:DropDownList ID="comboStartPeriod" runat="server" Width="150" Font-Size="9pt" AutoPostBack="True"></asp:DropDownList><br />
                    <br />
                    <asp:Label ID="Label2" runat="server" Font-Size="9pt">Select Ending Month</asp:Label><br />
                    <asp:DropDownList ID="comboEndPeriod" runat="server" Width="150" Font-Size="9pt" AutoPostBack="True"></asp:DropDownList></td>
            </tr>
        </table>
    </form>
</body>
</html>
