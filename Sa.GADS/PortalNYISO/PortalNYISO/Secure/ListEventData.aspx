﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ListEventData.aspx.vb" Inherits="PortalNYISO.ListEventData" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Event Data Report</title>
    <link href="../NewStyles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <table style="width: 100%">
            <tr>
                <td><a href="../MainDE.aspx">
                    <asp:Image ID="Image1" ImageUrl="../images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" /></a>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 100%; text-align: left">
                    <asp:Button ID="btnPDF" runat="server" Text="Export to PDF"></asp:Button></td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 100%">                  
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="Small" Height="600px" Width="100%" ></rsweb:ReportViewer>
                </td>
            </tr>
        </table>
        <asp:ScriptManager ID="Scriptmanager1" runat="server"></asp:ScriptManager>
    </form>
</body>
</html>
