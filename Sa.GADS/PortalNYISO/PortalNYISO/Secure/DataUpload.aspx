﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DataUpload.aspx.vb" Inherits="PortalNYISO.DataUpload" Debug="false" Trace="false" %>

<%@ Register Src="MultipleFileContainer.ascx" TagName="MultipleFileContainer" TagPrefix="SCC" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NYISO GADS Portal - Upload Files</title>
    <script src="../utility.js" type="text/javascript"></script>
    <script src="DataUpload.js" type="text/javascript"></script>
    <link href="../NewStyles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" style="padding-left:5px;">
        <table>
            <tr>
                <td><a href="../MainDE.aspx">
                    <asp:Image ID="Image1" ImageUrl="../images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" /></a>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnUandP" runat="server" Height="24" Width="150" Text="Upload &amp; Process"></asp:Button>
                    <asp:Button ID="btnEditData" runat="server" Height="24" Width="150" Text="Edit Data" />
                    <asp:Button ID="btnReports" runat="server" Height="24" Width="150" Text="Reports" />
                    <asp:Button ID="HelpIndex" runat="server" Height="24" Width="150" Text="Help Index" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Heading" runat="server" Text="Upload File List:" CssClass="Header"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td id="TopOfFile">
                    <div id="FileUploadFrame" style="overflow: hidden; height: 24px;" runat="server">
                        <SCC:MultipleFileContainer ID="UploadMultipleFileContainer" runat="server" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="UploadButton" runat="server" Width="110px" Text="Upload Files"></asp:Button><img src="../images/spacer.gif" width="4" />
                    <asp:Label ID="UploadingLabel" runat="server" Height="27px" CssClass="Processing" BackColor="Transparent"
                        EnableViewState="False">Uploading</asp:Label>
                    <asp:Image ID="UploadingImage" runat="server" CssClass="Hide" EnableViewState="False" ImageUrl="../images/processing.gif"></asp:Image>  
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ProcessButton" runat="server" Width="110px" Text="Process Data" Enabled="False"></asp:Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="ProcessRefreshButton" runat="server" Width="150px" Text="Refresh Status" Enabled="True"
                        CausesValidation="False"></asp:Button></td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="Header">Processing:
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="vertical-align: middle">
                    <table>
                        <tr>
                            <td class="StautusHeader" style="width: 140px">Submitted 
											Date:</td>
                            <td class="StautusHeader" style="width: 76px">Type:</td>
                            <td class="StautusHeader" style="width: 464px">Status:&nbsp;(updates every 
											minute)</td>
                        </tr>
                        <asp:Repeater ID="StatusRepeater" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="padding-left: 4px;" colspan="3" >                                     
                                        <%--<iframe style="width: 680px; top: 0px; height: 26px" src="ProcessStatus.aspx?WorkID=<%# Container.DataItem %>"></iframe>--%>
                                        <iframe style="width: 750px; top: 0px; height: 30px" src="ProcessStatus.aspx?WorkID=<%# Container.DataItem %>"></iframe>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="Header" colspan="2">History: <span class="message">(defaults to Current 
								Month)&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:Button ID="btnCurrentMonth" runat="server" Text="Current Month"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnLast2Months" runat="server" Text="Last 2 Months"></asp:Button></span></td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="HistoryHeader" style="PADDING-LEFT: 4px; width: 140px">Submitted 
											Date:</td>
                            <td class="HistoryHeader" style="width: 76px">Type:</td>
                            <td class="HistoryHeader" style="width: 464px">Status:</td>
                        </tr>
                        <asp:Repeater ID="HistoryRepeater" runat="server">
                            <ItemTemplate>
                                <tr style="height:27px">
                                    <td style="PADDING-LEFT: 4px; WIDTH: 140px; FONT-SIZE: 10pt; FONT-FAMILY: 'Times New Roman'"><%# DataBinder.Eval(Container.DataItem, "EvalInitiated", "{0:g}")%></td>
                                    <td style="WIDTH: 76px; FONT-SIZE: 10pt; FONT-FAMILY: 'Times New Roman'"><%# DataBinder.Eval(Container.DataItem, "EvalProcessTypeDesc")%></td>
                                    <td style="WIDTH: 464px; FONT-SIZE: 10pt; FONT-FAMILY: 'Times New Roman'"><%# DataBinder.Eval(Container.DataItem, "EvalStatusDesc")%></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </td>
            </tr>
        </table>
        <input id="DisplayHeight" style="Z-INDEX: 101; LEFT: 21px; POSITION: absolute; TOP: 742px"
            type="hidden" value="24" runat="server" />
        <div id="WaitLayer" style="Z-INDEX: 102; LEFT: 1120px; BACKGROUND-IMAGE: url(../images/spacer.gif); VISIBILITY: hidden; WIDTH: 100px; CURSOR: wait; POSITION: absolute; TOP: 624px; HEIGHT: 100px"></div>
    </form>
</body>
</html>
