Imports System.IO
Namespace Controls
   Partial Class FileUpload
      Inherits System.Web.UI.UserControl

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            AddClientScript()
            If Status <> FileUploadStatus.Accepted Then
                FileName = FileInput.Value
            End If
            ChangeDisplay()
        End Sub

      Public Sub ChangeDisplay()
         Select Case Status
            Case FileUploadStatus.Invalid
               StatusImage.ImageUrl = "../images/invalidfile.gif"
               FileInput.Disabled = False
            Case FileUploadStatus.NotValidated
               StatusImage.ImageUrl = "../images/uncheckedfile.gif"
               FileInput.Disabled = False
            Case FileUploadStatus.NoError, FileUploadStatus.Accepted
               StatusImage.ImageUrl = "../images/validfile.gif"
               FileInput.Disabled = True
         End Select
      End Sub

      Private Sub AddClientScript()
         Session("UploadCounter") = CInt(Session("UploadCounter")) + 1
         Dim DisplayHeight As String
         DisplayHeight = CStr(CInt(Session("UploadCounter")) * 24)
         FileInput.Attributes.Add("onchange", "ResizeElement('FileUploadFrame',0,Math.max(" & DisplayHeight & ", GetElement('DisplayHeight').value),0,0);" & _
            "GetElement('DisplayHeight').value = parseInt(GetElement('FileUploadFrame').style.height);")
      End Sub

      Public Property FileName() As String
         Get
            If ViewState(Me.ID & ":Filename") Is Nothing Then
               Return String.Empty
            End If
            Return CType(ViewState(Me.ID & ":Filename"), String)
         End Get
         Set(ByVal Value As String)
            ViewState(Me.ID & ":Filename") = Value
         End Set
      End Property

      Public Property LongFileName() As String
         Get
            If ViewState(Me.ID & ":LongFilename") Is Nothing Then
               Return String.Empty
            End If
            Return CType(ViewState(Me.ID & ":LongFilename"), String)
         End Get
         Set(ByVal Value As String)
            ViewState(Me.ID & ":LongFilename") = Value
         End Set
      End Property

      Public Property Status() As FileUploadStatus
         Get
            If ViewState(Me.ID & ":Status") Is Nothing Then
               Return FileUploadStatus.NotValidated
            End If
            Return CType(ViewState(Me.ID & ":Status"), FileUploadStatus)
         End Get
         Set(ByVal Value As FileUploadStatus)
            ViewState(Me.ID & ":Status") = Value
            ChangeDisplay()
         End Set
      End Property

      Public Sub SetMessage(ByVal Message As String, ByVal IsError As Boolean)
         StatusLabel.Text = Message
         If IsError Then
                StatusLabel.ForeColor = Drawing.Color.DarkRed
            Else
                StatusLabel.ForeColor = Drawing.Color.Black
         End If
            StatusLabel.Visible = True
      End Sub
   End Class
End Namespace
