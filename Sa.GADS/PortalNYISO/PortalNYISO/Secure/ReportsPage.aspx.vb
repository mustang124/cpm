﻿Imports System.Text.RegularExpressions
Imports System.IO
Imports PortalNYISO.Controls
Imports PortalNYISO.Process
Imports System.Security.Principal

Imports System
Imports System.Collections.Specialized
Imports System.Collections
Imports ARWindowsUI
Imports ARWindowsUI.ARWindowsUI.Calcs
Imports System.Threading.Thread  ' for Sleep function
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports System.Drawing.Text
Imports System.Text
Imports SetupSettings.Settings
Imports Microsoft.DSG.Security.CryptoServices
Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class ReportsPage
    Inherits System.Web.UI.Page

    Private myUser As String
    Private StartPeriodsArray As New ArrayList
    Private EndPeriodsArray As New ArrayList
    Private ObligMonthArray As New ArrayList
    Private strDTFormat As String = "MMMM yyyy"
    Private strMode As String
    'Private reader As StreamReader = Nothing
    Public hdAllUnits As New HybridDictionary
    Public hdClassAvgEFORd As New HybridDictionary
    Public hdFCMEFORd As New HybridDictionary
    Public hdOutageFactor As New HybridDictionary
    Public hdStdEFORd As New HybridDictionary
    Public hdFCMPeriodRange As New HybridDictionary

    Public hdEFORdMonths As New HybridDictionary

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        myUser = Context.User.Identity.Name
        'reader = New IO.StreamReader(HttpContext.Current.Server.MapPath("~/Secure/GADSNG.xml"))
        Try

            If Not IsPostBack Then

                Me.comboEndPeriod.Items.Clear()
                Me.comboStartPeriod.Items.Clear()

                StartPeriodsArray.Clear()
                EndPeriodsArray.Clear()
                ObligMonthArray.Clear()

                Dim connect As String
                connect = Application("AppPath").ToString

                Dim blConnect As New ARWindowsUI.ARWindowsUI.Calcs(Application("AppPath").ToString)

                Dim rdrGetPeriods As IDataReader
                rdrGetPeriods = blConnect.GetMyPeriodsList("Monthly")

                Dim alTemp As New ArrayList

                While rdrGetPeriods.Read

                    If Not rdrGetPeriods.IsDBNull(0) Then

                        If Not alTemp.Contains(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat)) Then

                            alTemp.Add(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat))

                            StartPeriodsArray.Add(New StartList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))
                            EndPeriodsArray.Add(New EndList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))

                        End If

                    End If

                End While

                rdrGetPeriods.Close()

                
                rdrGetPeriods.Dispose()

                Me.comboStartPeriod.DataSource = StartPeriodsArray
                Me.comboStartPeriod.DataTextField = "PeriodDesc"
                Me.comboStartPeriod.DataValueField = "PeriodValue"
                Me.comboStartPeriod.DataBind()

                Try
                    If Me.comboStartPeriod.Items.Count > 0 Then
                        If Me.comboStartPeriod.Items.Count > 12 Then
                            Me.comboStartPeriod.SelectedIndex = Me.comboStartPeriod.Items.Count - 12
                        Else
                            Me.comboStartPeriod.SelectedIndex = 0
                        End If
                    End If
                Catch ex As System.ArgumentOutOfRangeException
                    'strError = ex.ToString
                End Try

                Me.comboEndPeriod.DataSource = EndPeriodsArray
                Me.comboEndPeriod.DataTextField = "PeriodDesc"
                Me.comboEndPeriod.DataValueField = "PeriodValue"
                Me.comboEndPeriod.DataBind()

                Try
                    If Me.comboEndPeriod.Items.Count > 0 Then
                        Me.comboEndPeriod.SelectedIndex = Me.comboEndPeriod.Items.Count - 1
                    End If
                Catch ex As System.ArgumentOutOfRangeException
                    'strError = ex.ToString
                End Try

            End If


        Catch ex As Exception

            Dim sScript As String = String.Empty
            sScript = "<script language='javascript'>"
            sScript += "alert('" & ex.ToString & "');"
            sScript += "</script>"

            Try
                Response.Write(sScript)
            Catch exAlert As System.Exception
                sScript = exAlert.ToString
            End Try

        End Try

    End Sub

    Protected Sub btnDoReports_Click(sender As Object, e As EventArgs) Handles btnDoReports.Click
        Dim AppName As String = "GADSNG"
        Dim strDateRange As String
        'Dim moXML As SetupSettings.Settings

        Dim str1 As String
        Dim str2 As String

        Dim connect As String = Application("AppPath").ToString

        Dim blConnect As New ARWindowsUI.ARWindowsUI.Calcs(Application("AppPath").ToString)

        'Dim reader As New IO.StreamReader(HttpContext.Current.Server.MapPath("~/Secure/GADSNG.xml"))
        'Dim strMyKey As String
        ''strMyKey = Server.MapPath("..") & "/secure/GADSNG.xml"
        'strMyKey = Server.MapPath("~").ToString & "/secure/GADSNG.xml"
        'moXML = New SetupSettings.Settings(AppName, strMyKey, reader)

        ' strDateRange = "(TL_DateTime  BETWEEN '1/31/2002 11:59:59 PM' AND '12/31/2002 11:59:59 PM')"
        If blConnect.ThisProvider = "Oracle" Then

            str1 = "to_date ('" & Me.comboStartPeriod.SelectedValue.ToString & "', 'mm/dd/yyyy HH:MI:SS PM')"
            str2 = "to_date ('" & Me.comboEndPeriod.SelectedValue.ToString & "', 'mm/dd/yyyy HH:MI:SS PM')"

        Else

            str1 = "'" & Me.comboStartPeriod.SelectedValue.ToString & "'"
            str2 = "'" & Me.comboEndPeriod.SelectedValue.ToString & "'"

        End If

        strDateRange = "(TL_DateTime BETWEEN " & str1 & " AND " & str2 & ")"

        Session.Item("strDateRange") = strDateRange

        'strDateRange += ")"
        'Response.Redirect("~\secure\WebReportViewer.aspx?choice=" & strDateRange)
        'Response.Redirect("~\secure\WebReportViewer.aspx")

        Dim sScript As String = String.Empty
        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('WebReportViewer.aspx', '_blank'); new_win.focus();"
        sScript += "</script>"
        Response.Write(sScript)

    End Sub

    Protected Sub btnUandP_Click(sender As Object, e As EventArgs) Handles btnUandP.Click
        Response.Redirect("~/secure/DataUpload.aspx")
    End Sub

    Protected Sub btnEditData_Click(sender As Object, e As EventArgs) Handles btnEditData.Click
        Response.Redirect("~/secure/DEApp.aspx")
    End Sub

    Protected Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/secure/ReportsPage.aspx")
    End Sub

    Protected Sub HelpIndex_Click(sender As Object, e As EventArgs) Handles HelpIndex.Click
        Dim sScript As String = String.Empty
        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('HTML/default.htm', 'child'); new_win.focus();"
        sScript += "</script>"
        Response.Write(sScript)
    End Sub
End Class