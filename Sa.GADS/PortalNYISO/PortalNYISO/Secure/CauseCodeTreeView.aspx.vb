﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml

Public Class CauseCodeTreeView
    Inherits System.Web.UI.Page

    Public intYear As Integer
    Public strUnitType As String
    Public dsCauseCodes As DataSet
    Public strWhichForm As String
    'Public drvDetail As DataRowView
    Public myUser As String = ""
    Public drSetup As BusinessLayer.Setup.SetupRow
    Public dtSetup As BusinessLayer.Setup.SetupDataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            strWhichForm = Request.QueryString("WhichForm")
            'CreateTree()
            'CreateTreeViewDataTable()
            CreateTree2()
            TreeView1.ExpandDepth = 1
            TreeView1.FindNode(Session.Item("newCauseCode"))
            ' Find the node specified by the user.
            Dim node As TreeNode = TreeView1.FindNode(Server.HtmlEncode(Session.Item("newCauseCode")))
            Dim msg As String
            If Not node Is Nothing Then

                ' Indicate that the node was found.
                msg = "The specified node (" & node.ValuePath & ") was found."

            Else

                ' Indicate that the node is not in the TreeView control.
                Msg = "The specified node (" & Session.Item("newCauseCode") & ") is not in this TreeView control."

            End If

        End If
    End Sub

    Protected Sub TreeView1_SelectedNodeChanged(sender As Object, e As EventArgs)
       
        If Convert.ToInt32(TreeView1.SelectedNode.Value) >= 0 Then
            Session.Item("newCauseCode") = TreeView1.SelectedNode.Value
            Session.Item("newNERCCauseCodeDesc") = TreeView1.SelectedNode.Text
        End If

    End Sub

    Private Sub CreateTree()
        'For intCounter = 0 To vue.Count - 1

        '    drTest = vue(intCounter)

        'If (Convert.ToInt16(drTest("Level3")) = 1 And _
        '    Convert.ToInt16(drTest("Level4")) = 4 And _
        '    Convert.ToInt16(drTest("Level5")) = 1 And _
        '    Convert.ToInt16(drTest("CauseCode")) = -1) And _
        '    Convert.ToString(drTest("CauseCodeDesc")).ToUpper = "FUEL SUPPLY" Then

        '    ' this solves a singularly unique problem with the combined cycle data structure

        'Else

        '    If (Convert.ToInt16(drTest("Level3")) = 1 And _
        '        Convert.ToInt16(drTest("Level4")) = 1 And _
        '        Convert.ToInt16(drTest("Level5")) = 0 And _
        '        Convert.ToInt16(drTest("CauseCode")) = -1) And _
        '        Convert.ToString(drTest("CauseCodeDesc")).ToUpper = "BOILER" Then

        '        ' this solves a singularly unique problem with the combined cycle data structure
        '        If strUnitType.ToUpper <> "MISC" Then
        '            drTest("CauseCodeDesc") = "Fuel Supply"
        '        End If

        '    End If


        '    If Convert.ToInt16(drTest("Level3")) > 0 And _
        '        Convert.ToInt16(drTest("Level4")) = 0 And _
        '        Convert.ToInt16(drTest("Level5")) = 0 And _
        '        Convert.ToInt16(drTest("CauseCode")) = -1 Then

        '        ' Root level

        '        root = .Nodes.Add(drTest("CauseCodeDesc").ToString.Trim)

        '    ElseIf Convert.ToInt16(drTest("Level3")) > 0 And _
        '            Convert.ToInt16(drTest("Level4")) = 0 And _
        '            Convert.ToInt16(drTest("Level5")) = 0 And _
        '            Convert.ToInt16(drTest("CauseCode")) > 0 Then

        '        ' Ending node

        '        If intYear >= Convert.ToInt16(drTest("StartingYear")) And intYear <= Convert.ToInt16(drTest("EndingYear")) Then
        '            EndNode = root.Nodes.Add(drTest("CauseCode").ToString.ToString.PadLeft(4, ChrW(48)) & " - " & drTest("CauseCodeDesc").ToString.Trim)
        '        End If

        '        ' ----------------------------------------------

        '    ElseIf Convert.ToInt16(drTest("Level3")) > 0 And _
        '            Convert.ToInt16(drTest("Level4")) > 0 And _
        '            Convert.ToInt16(drTest("Level5")) = 0 And _
        '            Convert.ToInt16(drTest("CauseCode")) = -1 Then

        '        ' Next level node

        '        NextLevel1 = root.Nodes.Add(drTest("CauseCodeDesc").ToString.Trim)

        '    ElseIf Convert.ToInt16(drTest("Level3")) > 0 And _
        '            Convert.ToInt16(drTest("Level4")) > 0 And _
        '            Convert.ToInt16(drTest("Level5")) = 0 And _
        '            Convert.ToInt16(drTest("CauseCode")) > 0 Then

        '        ' Next level ending node

        '        If intYear >= Convert.ToInt16(drTest("StartingYear")) And intYear <= Convert.ToInt16(drTest("EndingYear")) Then
        '            EndNode = NextLevel1.Nodes.Add(drTest("CauseCode").ToString.PadLeft(4, ChrW(48)) & " - " & drTest("CauseCodeDesc").ToString.Trim)
        '        End If

        '        ' ----------------------------------------------

        '    ElseIf Convert.ToInt16(drTest("Level3")) > 0 And _
        '            Convert.ToInt16(drTest("Level4")) > 0 And _
        '            Convert.ToInt16(drTest("Level5")) > 0 And _
        '            Convert.ToInt16(drTest("CauseCode")) = -1 Then

        '        ' Next level node

        '        NextLevel2 = NextLevel1.Nodes.Add(drTest("CauseCodeDesc").ToString.Trim)

        '    ElseIf Convert.ToInt16(drTest("Level3")) > 0 And _
        '            Convert.ToInt16(drTest("Level4")) > 0 And _
        '            Convert.ToInt16(drTest("Level5")) > 0 And _
        '            Convert.ToInt16(drTest("CauseCode")) > 0 Then

        '        ' Next level ending node

        '        If intYear >= Convert.ToInt16(drTest("StartingYear")) And intYear <= Convert.ToInt16(drTest("EndingYear")) Then
        '            EndNode = NextLevel2.Nodes.Add(drTest("CauseCode").ToString.PadLeft(4, ChrW(48)) & " - " & drTest("CauseCodeDesc").ToString.Trim)
        '        End If

        '    End If

        'End If

        'Next
    End Sub


    Private Sub CreateTree2()

        intYear = CType(Session("intCurrentYear"), Integer)

        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        drSetup = dtSetup.FindByUtilityUnitCode(Session("strCurrentUnit").ToString)
        dsCauseCodes = CType(Session("dsCauseCodes"), DataSet)
        'drvDetail = CType(Session("drvDetail"), DataRowView)
        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name

        Dim vue As DataView
        vue = New DataView
        vue.Table = dsCauseCodes.Tables("CauseCodes")

        ' ===================================
        ' This sets the DataView by Unit Type
        ' -----------------------------------
        strUnitType = drSetup.UnitType
        vue.RowFilter = strUnitType & " = 1 AND CauseCode <> 7777"

        Select Case strUnitType.ToUpper
            Case "FOSSILSTEAM"
                myTitle.Text = "Fossil Steam"
            Case "NUCLEAR"
                myTitle.Text = "Nuclear"
            Case "DIESEL"
                myTitle.Text = "Diesel"
            Case "HYDRO"
                myTitle.Text = "Hydro/Pumped Storage"
            Case "GASTURBINESIMPLECYCLE"
                myTitle.Text = "Gas Turbine"
            Case "JETENGINESIMPLECYCLE"
                myTitle.Text = "Jet Engine"
            Case "MISC"
                myTitle.Text = "Misc - Combined Cycle"
            Case "FLUIDIZEDBED"
                myTitle.Text = "Fluidized Bed"
            Case "COMBINEDCYCLECT"
                myTitle.Text = "Combined Cycle CT"
            Case "COMBINEDCYCLEJE"
                myTitle.Text = "Combined Cycle JE"
            Case "COMBINEDCYCLESTEAM"
                myTitle.Text = "Combined Cycle Steam"
            Case "GEOTHERMAL"
                myTitle.Text = "Misc - Geothermal"
            Case "MISCMULTI"
                myTitle.Text = "Multi-boiler/turbine"
            Case Else
                myTitle.Text = strUnitType
        End Select

        ' ===================================
        TreeView1.Nodes.Clear()
        Dim NextLevel0 As New TreeNode
        Dim NextLevel1 As New TreeNode
        Dim NextLevel2 As New TreeNode
        Dim NextLevel3 As New TreeNode
        Dim NextLevel4 As New TreeNode
        Dim NextLevel5 As New TreeNode

        Dim EndNode As New TreeNode

        Dim strPre As String = strUnitType & " = 1 AND CauseCode <> 7777"

        Dim Root As TreeNode = New TreeNode(myTitle.Text)
        Me.TreeView1.Nodes.Add(Root)
        myTitle.Text += " Cause Codes " ' & strWhichForm

        vue.Sort = "Level0, Level1, Level2, Level3, Level4, Level5, CauseCode"
        vue.RowFilter = strUnitType & " = 1 AND CauseCode <> 7777"
        
        For Each drv1 As DataRowView In vue

            If (Convert.ToInt16(drv1("Level3")) = 1 And _
                Convert.ToInt16(drv1("Level4")) = 4 And _
                Convert.ToInt16(drv1("Level5")) = 1 And _
                Convert.ToInt16(drv1("CauseCode")) = -1) And _
                Convert.ToString(drv1("CauseCodeDesc")).ToUpper = "FUEL SUPPLY" Then

                ' this solves a singularly unique problem with the combined cycle data structure

            Else

                If (Convert.ToInt16(drv1("Level3")) = 1 And _
                    Convert.ToInt16(drv1("Level4")) = 1 And _
                    Convert.ToInt16(drv1("Level5")) = 0 And _
                    Convert.ToInt16(drv1("CauseCode")) = -1) And _
                    Convert.ToString(drv1("CauseCodeDesc")).ToUpper = "BOILER" Then

                    ' this solves a singularly unique problem with the combined cycle data structure
                    If strUnitType.ToUpper <> "MISC" Then
                        drv1("CauseCodeDesc") = "Fuel Supply"
                    End If

                End If


                If Convert.ToInt16(drv1("Level3")) > 0 And _
                    Convert.ToInt16(drv1("Level4")) = 0 And _
                    Convert.ToInt16(drv1("Level5")) = 0 And _
                    Convert.ToInt16(drv1("CauseCode")) = -1 Then

                    ' Root level
                    NextLevel0 = New TreeNode
                    NextLevel0.Text = drv1("CauseCodeDesc").ToString.Trim
                    NextLevel0.Value = -1
                    Root.ChildNodes.Add(NextLevel0)

                ElseIf Convert.ToInt16(drv1("Level3")) > 0 And _
                        Convert.ToInt16(drv1("Level4")) = 0 And _
                        Convert.ToInt16(drv1("Level5")) = 0 And _
                        Convert.ToInt16(drv1("CauseCode")) > 0 Then

                    ' Ending node

                    If intYear >= Convert.ToInt16(drv1("StartingYear")) And intYear <= Convert.ToInt16(drv1("EndingYear")) Then
                        EndNode = New TreeNode
                        EndNode.Text = Convert.ToInt16(drv1("CauseCode")).ToString("0000") & " - " & drv1("CauseCodeDesc").ToString.Trim
                        EndNode.Value = Convert.ToInt16(drv1("CauseCode"))
                        NextLevel0.ChildNodes.Add(EndNode)
                    End If

                    ' ----------------------------------------------

                ElseIf Convert.ToInt16(drv1("Level3")) > 0 And _
                        Convert.ToInt16(drv1("Level4")) > 0 And _
                        Convert.ToInt16(drv1("Level5")) = 0 And _
                        Convert.ToInt16(drv1("CauseCode")) = -1 Then

                    ' Next level node
                    NextLevel1 = New TreeNode
                    NextLevel1.Text = drv1("CauseCodeDesc").ToString.Trim
                    NextLevel1.Value = -1
                    NextLevel0.ChildNodes.Add(NextLevel1)

                ElseIf Convert.ToInt16(drv1("Level3")) > 0 And _
                        Convert.ToInt16(drv1("Level4")) > 0 And _
                        Convert.ToInt16(drv1("Level5")) = 0 And _
                        Convert.ToInt16(drv1("CauseCode")) > 0 Then

                    ' Next level ending node

                    If intYear >= Convert.ToInt16(drv1("StartingYear")) And intYear <= Convert.ToInt16(drv1("EndingYear")) Then
                        EndNode = New TreeNode
                        EndNode.Text = Convert.ToInt16(drv1("CauseCode")).ToString("0000") & " - " & drv1("CauseCodeDesc").ToString.Trim
                        EndNode.Value = Convert.ToInt16(drv1("CauseCode"))
                        NextLevel1.ChildNodes.Add(EndNode)
                    End If

                    ' ----------------------------------------------

                ElseIf Convert.ToInt16(drv1("Level3")) > 0 And _
                        Convert.ToInt16(drv1("Level4")) > 0 And _
                        Convert.ToInt16(drv1("Level5")) > 0 And _
                        Convert.ToInt16(drv1("CauseCode")) = -1 Then

                    ' Next level node
                    NextLevel2 = New TreeNode
                    NextLevel2.Text = drv1("CauseCodeDesc").ToString.Trim
                    NextLevel2.Value = -1
                    NextLevel1.ChildNodes.Add(NextLevel2)

                ElseIf Convert.ToInt16(drv1("Level3")) > 0 And _
                        Convert.ToInt16(drv1("Level4")) > 0 And _
                        Convert.ToInt16(drv1("Level5")) > 0 And _
                        Convert.ToInt16(drv1("CauseCode")) > 0 Then

                    ' Next level ending node

                    If intYear >= Convert.ToInt16(drv1("StartingYear")) And intYear <= Convert.ToInt16(drv1("EndingYear")) Then
                        EndNode = New TreeNode
                        EndNode.Text = Convert.ToInt16(drv1("CauseCode")).ToString("0000") & " - " & drv1("CauseCodeDesc").ToString.Trim
                        EndNode.Value = Convert.ToInt16(drv1("CauseCode"))
                        NextLevel2.ChildNodes.Add(EndNode)
                    End If

                End If

            End If
        Next

    End Sub

End Class