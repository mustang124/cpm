﻿Imports Microsoft.DSG.Security.CryptoServices
Imports BusinessLayer.BusinessLayer
Imports SetupSettings.Settings
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Web.HttpRequest
Imports System.Security.Principal
Imports Microsoft.VisualBasic
Imports System.Threading
Imports PortalNYISO.WebUtilityModule
'Imports Microsoft.Web.UI.WebControls
Imports PortalNYISO.Controls
Imports PortalNYISO.Process
Imports System.Collections
Imports System.Collections.Specialized
Imports LanguageBuilder

Public Class DEApp
    Inherits System.Web.UI.Page

    Public AppName As String = "GADSNG"
    Public connect As String = ""
    Public blConnect As GADSNGBusinessLayer
    Public dtSetup As BusinessLayer.Setup.SetupDataTable
    Public drSetup As BusinessLayer.Setup.SetupRow
    Public dsCauseCodes As DataSet
    Public dtPerformance As BusinessLayer.Performance.PerformanceDataDataTable
    Public drPerformance As BusinessLayer.Performance.PerformanceDataRow
    Public dsAllEvents As AllEventDataISO
    Public dsGridEvents As DataSet
    Public dsErrors As DataSet
    Public dsFailMech As DataSet
    Public dsCauseCodeExt As DataSet
    Public dsSavedVerbDesc As DataSet
    Public dsRS As BusinessLayer.dsBulkRS
    Public intCurrentYear As Integer
    Public strCurrentUnit As String = ""
    Public strCurrentPeriod As String = ""
    Public strCurrentMonthNo As String = ""
    'Public intSECauseCode As Integer
    'Public dtSEBeginning As DateTime
    Public myUser As String = ""
    Public alPeriods As New ArrayList
    Public lbUnitsText As String = ""
    Public l_regex As System.Text.RegularExpressions.Regex
    Public lForceRefresh As Boolean

    Private _PageTitle As String

    Public intYearLimit As Integer = 1980
    Private _WorkManager As WorkManager
    Private lSimpleSetup As Boolean = False
    Private StringToPrint As String
    Private lMissingUnits As Boolean = False
    Private boolredoform As Boolean = False
    Public GetFromDictionary As New HybridDictionary
    Public strCulture As String = "en-US"

    'Const COPUCUTOFFDATE As String = "10/01/2006"

    Public Enum PerfReports
        Generation = 1
        Fuel = 2
        Time = 3
    End Enum

    Public Enum GrossNetOrBoth
        GrossOnly = 0
        NetOnly = 1
        Both = 2
    End Enum

    Private Sub DEApp_Init(sender As Object, e As EventArgs) Handles Me.Init
        blConnect = New GADSNGBusinessLayer(Application("AppPath").ToString)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
        'strCulture = CultureInfo.CurrentCulture().ToString

        If Not IsPostBack Then
            Session.Item("redoform") = False
            ' The first time the page loads, render the DefaultView.
            MultiPageMain.ActiveViewIndex = "0"

            ' Identity items for possible use -- start
            Dim MyIdentity As WindowsIdentity = WindowsIdentity.GetCurrent
            Dim MyPrincipal As New WindowsPrincipal(MyIdentity)

            ' Principal values
            Dim PrincipalName As String = MyPrincipal.Identity.Name
            Dim PrincipalType As String = MyPrincipal.Identity.AuthenticationType
            Dim PrincipalIsAuth As Boolean = MyPrincipal.Identity.IsAuthenticated

            ' Identity values
            Dim IdentName As String = MyIdentity.Name
            Dim IdentType As String = MyIdentity.AuthenticationType
            Dim IdentIsAuth As Boolean = MyIdentity.IsAuthenticated
            Dim IdentIsAnon As Boolean = MyIdentity.IsAnonymous
            Dim IdentIsGuest As Boolean = MyIdentity.IsGuest
            Dim IdentIsSys As Boolean = MyIdentity.IsSystem
            Dim IdentToken As String = MyIdentity.Token.ToString

            ' Identity items for possible use -- end

            Try
                connect = Application("AppPath").ToString

                GetFromDictionary = LanguageBuilder.LanguageBuilder.BuildDictionary("False", connect)
                'Session.Add("GetFromDictionary", GetFromDictionary)
                Session.Add("ErrorMessage", String.Empty)

                myUser = Context.User.Identity.Name

                Dim lValidUser As Boolean
                lValidUser = False
                lForceRefresh = False

                'blConnect = New GADSNGBusinessLayer(Application("AppPath").ToString)

                If blConnect.CountUsersDefined() > 0 Then
                    ' There are users defined in the NGUsers table
                    If blConnect.IsUserValid(myUser) = 0 Then
                        ' The user ID Tiger\Ron is not a valid user
                        'if myuser.LastIndexOf("\") 
                        myUser = myUser.Substring(myUser.LastIndexOf("\") + 1)
                        ' Try just the part to the right of the "\"
                        If blConnect.IsUserValid(myUser) = 0 Then
                            Session("ErrorMessage") = "User ID (" & myUser & ") not found."
                        Else
                            _PageTitle = "NYISO GADS Portal"
                            lValidUser = True
                        End If
                    Else
                        _PageTitle = "NYISO GADS Portal"
                        lValidUser = True
                    End If
                Else
                    ' No users are defined in table
                    _PageTitle = "NYISO GADS Portal"
                    lValidUser = False
                    lSimpleSetup = False
                End If

                dtSetup = blConnect.GetUtilityUnitData()

                ' in case the checkstatus field is null
                lblStatus.Text = "&nbsp;UNK&nbsp;"
                lblStatus.BackColor = System.Drawing.SystemColors.ControlDark
                lblStatus.ForeColor = System.Drawing.Color.Black

                If lValidUser = True And dtSetup.Rows.Count > 0 Then

                    ' dsGridEvents contains all of the event data for the specific unit and year

                    Me.dsGridEvents = New DataSet
                    Me.dsRS = New BusinessLayer.dsBulkRS

                    ' dsErrors contains all of the Errors data for the specific units this user is allowed to access
                    Me.dsErrors = New DataSet
                    
                    ' find out what group(s) this person is assigned to -- could be more than one group
                    Dim intGroups As Integer
                    intGroups = blConnect.IsUserAssignedToGroup(myUser)
                    If intGroups > 0 Then
                        ' assigned to one or more Groups
                        Dim GroupIDReader As IDataReader
                        Dim UnitsReader As IDataReader
                        Dim GroupArray As New ArrayList
                        Dim UnitArray As New ArrayList
                        'Dim myTest As Integer
                        Dim intValue As Integer
                        Dim strValue As String

                        GroupIDReader = blConnect.GetUserGroupsReader(myUser)

                        While GroupIDReader.Read()
                            GroupArray.Add(CInt(GroupIDReader.GetValue(0)))
                        End While

                        ' always call Close when done reading
                        GroupIDReader.Close()

                        'dtSetup = blConnect.GetUtilityUnitData()

                        For Each drSetup As BusinessLayer.Setup.SetupRow In dtSetup
                            drSetup.UnitSelected = False
                        Next

                        dtSetup.AcceptChanges()
                        Dim PermsReader As IDataReader
                        Dim intGroupID As Integer

                        intYearLimit = 1980

                        For intValue = 0 To (GroupArray.Count - 1)

                            UnitsReader = blConnect.GetUnitsInGroup(CInt(GroupArray.Item(intValue)))

                            While UnitsReader.Read()
                                strValue = UnitsReader.GetString(0)
                                drSetup = dtSetup.FindByUtilityUnitCode(UnitsReader.GetString(0))
                                If drSetup Is Nothing Then
                                    Session("ErrorMessage") = "Invalid or Missing Unit Code " & strValue
                                Else
                                    drSetup.UnitSelected = True
                                End If
                            End While

                            UnitsReader.Close()

                            PermsReader = blConnect.GetPermissions(CInt(GroupArray.Item(intValue)))

                            While PermsReader.Read
                                intGroupID = CInt(PermsReader.GetValue(0))
                                intYearLimit = CInt(PermsReader.GetValue(1))
                            End While

                            PermsReader.Close()
                        Next

                        For Each drSetup As BusinessLayer.Setup.SetupRow In dtSetup
                            If drSetup.UnitSelected = False Then
                                drSetup.Delete()
                            End If
                        Next

                        dtSetup.AcceptChanges()

                        lbUnits.DataTextField = "UnitName"
                        lbUnits.DataValueField = "UtilityUnitCode"
                        lbUnits.DataSource = dtSetup
                        lbUnits.SelectedIndex = 0
                        lbUnits.DataBind()

                        If dtSetup.Rows.Count = 0 Then
                            Session("ErrorMessage") = "No Units Assigned to " & myUser
                        Else
                            lbUnitsText = lbUnits.SelectedItem.Text
                            strCurrentUnit = lbUnits.SelectedValue.ToString

                            DisplayCheckStatus(True)

                            Dim strTest As String
                            'Dim iYear As Integer
                            Dim iMonth As Integer
                            Dim dtDate As Date
                            Dim dtStart As Date

                            'strTest = lbUnits.SelectedValue.ToString()
                            alPeriods = blConnect.GetPerfDataRange(lbUnits.SelectedValue.ToString)

                            ' alPeriods(0) is the Minimum Year
                            ' alPeriods(1) is the Minimum Month 
                            If Convert.ToInt32(alPeriods(0).ToString) < intYearLimit Then
                                ' In NGGroups is YearLimit -- the users are not allowed to edit below this year -- set by Administrator
                                dtStart = System.DateTime.Parse("01/01/" & intYearLimit.ToString())
                            Else
                                dtStart = System.DateTime.Parse(alPeriods(1).ToString() & "/01/" & alPeriods(0).ToString())
                            End If

                            alPeriods.Clear()

                            dtDate = dtStart

                            Do While dtDate < DateAdd(DateInterval.Month, -1, Now)                                 ' do not convert to UtcNow
                                strTest = dtDate.ToString("y")
                                alPeriods.Add(dtDate.ToString("y"))
                                dtDate = DateAdd(DateInterval.Month, 1, dtDate)
                            Loop

                            lbUpdateMonth.Items.Clear()

                            lbUpdateMonth.Items.Add(alPeriods(alPeriods.Count - 1).ToString)

                            If alPeriods.Count <= 1 Then
                                Me.intCurrentYear = CInt(alPeriods(0).ToString().Substring((alPeriods(0).ToString.Length - 4), 4))
                            Else
                                Me.intCurrentYear = CInt(alPeriods(alPeriods.Count - 2).ToString().Substring((alPeriods(alPeriods.Count - 2).ToString.Length - 4), 4))
                            End If

                            For iMonth = (alPeriods.Count - 1) To 1 Step -1
                                Me.lbUpdateMonth.Items.Add(alPeriods(iMonth - 1).ToString)
                            Next

                            'Me.lbUpdateMonth.SelectedIndex = 0
                            'Me.lbUpdateMonth.SelectedValue = blConnect.GetCurrentMonth(lbUnits.SelectedValue.ToString)
                            'Me.strCurrentPeriod = Me.lbUpdateMonth.SelectedItem.ToString

                            Me.strCurrentPeriod = blConnect.GetCurrentMonth(lbUnits.SelectedValue.ToString, GetFromDictionary)

                            If Me.strCurrentPeriod = "0" Then
                                Me.lbUpdateMonth.SelectedIndex = 0
                                Me.strCurrentPeriod = Me.lbUpdateMonth.SelectedItem.ToString
                            Else

                                Try
                                    Me.lbUpdateMonth.SelectedValue = Me.strCurrentPeriod.Trim
                                Catch ex As Exception
                                    Me.lbUpdateMonth.SelectedIndex = 0
                                    Me.strCurrentPeriod = Me.lbUpdateMonth.SelectedItem.ToString
                                End Try

                            End If

                            'strTest = Me.lbUpdateMonth.Text()
                            UpdatePerfFields()
                        End If
                    Else
                        Session("ErrorMessage") = "No Groups/Units Assigned to " & myUser
                        '_PageTitle = "GADS NxL (" & myUser & ")"
                        _PageTitle = "NYISO GADS Portal"
                        lValidUser = True
                        lSimpleSetup = True
                    End If

                Else
                    If dtSetup.Rows.Count = 0 Then
                        lMissingUnits = True
                        Session("ErrorMessage") = "No generating units defined" & "<br>" & "<br>" & "Contact the NYISO GADS Administrator"
                    Else
                        ' Not valid user, invalid keys.xml settings, etc
                        ' Stop user from doing anything else
                        Session("ErrorMessage") = "Login Problems for User:  " & myUser
                    End If
                End If

                dsCauseCodes = blConnect.GetCauseCodes
                dsFailMech = blConnect.GetFailMech()
                dsCauseCodeExt = blConnect.GetCauseCodeExt()

                'Code - Description
                '1 - Base loaded with minor load following at night and on weekends
                '2 - Periodic startups with daily load-following and reduced load nightly
                '3 - Weekly startup with daily load-following and reduced load nightly
                '4 - Daily startup with daily load-following and taken off-line nightly
                '5 - Startup chiefly to meet daily peaks
                '6 - Other (describe in verbal description)
                '7 - Seasonal Operation

                lbTULC.Items.Add("1 - Base loaded with minor load following at night and on weekends")
                lbTULC.Items.Add("2 - Periodic startups with daily load-following and reduced load nightly")
                lbTULC.Items.Add("3 - Weekly startup with daily load-following and reduced load nightly")
                lbTULC.Items.Add("4 - Daily startup with daily load-following and taken off-line nightly")
                lbTULC.Items.Add("5 - Startup chiefly to meet daily peaks")
                lbTULC.Items.Add("6 - Other")
                lbTULC.Items.Add("7 - Seasonal Operation")

                '' GMC must be >= NMC - batch validation
                'GMCNMC_CustomValidator.Enabled = False
                '' GDC must be >= NDC - batch validation
                'GDCNDC_CustomValidator.Enabled = False
                '' GAG must be >= NAG - batch validation
                'GAGNAG_CustomValidator.Enabled = False
                '' Either GDC or NDC must be greater than 0
                'GDCNDCMustExist.Enabled = False

                If Not lMissingUnits And Not IsNothing(drSetup) And dtSetup.Rows.Count > 0 Then
                    ResetValidations()
                    CreateSession()
                End If

            Catch ex As Exception
                ExceptionManager.Publish(ex)
                Session("ErrorMessage") = "Contact your GADS NxL Administrator" & "<br>" & "<br>" & _
                 "Use Event Viewer to find Application Error" & "<br>" & "<br>" & _
                 ex.Message & "<br>" & "<br>" & _
                 "GADS NxL Support will need this information"
            Finally
                Dim strErrors As String
                Try
                    strErrors = Session("ErrorMessage").ToString
                Catch ex As Exception
                    strErrors = ex.ToString
                End Try

                If strErrors.Trim <> String.Empty Then
                    HaveErrors(strErrors)
                End If
            End Try

            Page.Validate()
        Else
            boolredoform = CType(Session.Item("redoform"), Boolean)
        End If

        If boolredoform Then

            dsGridEvents = CType(Session("dsGridEvents"), DataSet)

            If dsGridEvents.HasChanges Then
                Me.btnSaveEvents.BackColor = Drawing.Color.Red
            End If

            Me.dgEvents.DataSource = dsGridEvents
            Me.dgEvents.DataMember = "EventData01"
            Me.dgEvents.DataBind()

            Dim strTemp As String

            Try
                strTemp = Me.dgEvents.DataKeys(Me.dgEvents.SelectedIndex).ToString
            Catch ex As System.Exception
                strTemp = "0000"
            End Try

            Session.Item("redoform") = False

        End If

        If Not Session("dsErrors") Is Nothing Then
            dsErrors = CType(Session("dsErrors"), DataSet)
            dsErrors.Clear()
        End If

        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)

        'blConnect = CType(Session("blConnect"), BusinessLayer.BusinessLayer.GADSNGBusinessLayer)

        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name
        dsErrors = GetUserErrors(myUser)

        Session.Item("dsErrors") = dsErrors

        dgErrors.DataSource = dsErrors
        dgErrors.DataMember = "Errors"

        dgErrors.DataBind()

    End Sub

#Region " DisplayCheckStatus(ByVal lGetStatus As Boolean) "

    Public Sub DisplayCheckStatus(ByVal lGetStatus As Boolean)

        If Not Session("dtSetup") Is Nothing Then
            dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        End If

        If Not Session("strCurrentUnit") Is Nothing Then
            strCurrentUnit = Session("strCurrentUnit").ToString
        End If

        drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)

        If lGetStatus Then
            drSetup.CheckStatus = blConnect.GetMyCheckStatus(strCurrentUnit)
        End If

        Select Case drSetup.CheckStatus.ToUpper
            Case "E"
                lblStatus.Text = "&nbsp;ERR&nbsp;"
                lblStatus.BackColor = System.Drawing.Color.Crimson
                lblStatus.ForeColor = System.Drawing.Color.White
            Case "R"
                lblStatus.Text = "&nbsp;MOD&nbsp;"
                lblStatus.BackColor = System.Drawing.Color.Gold
                lblStatus.ForeColor = System.Drawing.Color.Black
            Case "O"
                lblStatus.Text = "&nbsp;OK&nbsp;"
                lblStatus.BackColor = System.Drawing.Color.Green
                lblStatus.ForeColor = System.Drawing.Color.White
            Case Else
                lblStatus.Text = "&nbsp;UNK&nbsp;"
                lblStatus.BackColor = System.Drawing.SystemColors.ControlDark
                lblStatus.ForeColor = System.Drawing.Color.Black
        End Select

    End Sub

#End Region

    Protected Sub btnEventData_Click(sender As Object, e As EventArgs) Handles btnEventData.Click
        MultiPageMain.ActiveViewIndex = 0
    End Sub

    Protected Sub btnPerformanceData_Click(sender As Object, e As EventArgs) Handles btnPerformanceData.Click
        'MultiPageMain.ActiveViewIndex = 1
        MultiPageMain.SetActiveView(OperatingData)
    End Sub

    Protected Sub btnFinalValidation_Click(sender As Object, e As EventArgs) Handles btnFinalValidation.Click
        MultiPageMain.ActiveViewIndex = 2
    End Sub

    Protected Sub btnEditExistingEvents_Click(sender As Object, e As EventArgs) Handles btnEditExistingEvents.Click
        MultiPageEvent.ActiveViewIndex = 0 ' EventEdit
    End Sub

    Protected Sub btnTabEventErrors_Click(sender As Object, e As EventArgs) Handles btnTabEventErrors.Click
        MultiPageEvent.ActiveViewIndex = 1 ' EventErrors
    End Sub

    Protected Sub btnOperatingData_Click(sender As Object, e As EventArgs) Handles btnOperatingData.Click
        MultiPagePerformance.ActiveViewIndex = 0  ' Perf1
    End Sub

    Protected Sub btnUnitTimeInfo_Click(sender As Object, e As EventArgs) Handles btnUnitTimeInfo.Click
        MultiPagePerformance.ActiveViewIndex = 1  ' Perf3
    End Sub


    Public Function GetUserErrors(ByVal myUser As String) As DataSet

        intCurrentYear = CType(Session("intCurrentYear"), Integer)
        Dim stringErrorSeverity As String = String.Empty
        Dim stringSelect As String
        'Dim drMyRows As DataRow
        Dim dsMyErrors As New DataSet
        'Dim dtMyErrors As DataTable

        'Dim parms(1, 2) As String
        'parms(0, 0) = "@UtilityUnitCode"

        If cbMissing.Checked Then
            stringErrorSeverity += " OR (ErrorSeverity = 'M' AND Year = " & intCurrentYear.ToString & ")"
        End If

        If cbWarning.Checked Then
            stringErrorSeverity += " OR (ErrorSeverity = 'W' AND Year = " & intCurrentYear.ToString & ")"
        End If

        'stringSelect = "SELECT * FROM Errors WHERE ErrorSeverity IN (" & stringErrorSeverity & ") AND UtilityUnitCode IN (SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID IN (SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & myUser & "')) ORDER BY UtilityUnitCode"
        stringSelect = "SELECT * FROM Errors WHERE (ErrorSeverity = 'E'" & stringErrorSeverity & ") AND UtilityUnitCode IN (SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID IN (SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & myUser & "')) ORDER BY UtilityUnitCode"
        dsMyErrors = blConnect.GetADataset(stringSelect, Nothing)

        'For Each drMyRows In dtSetup.Rows
        '	parms(0, 1) = "UtilityUnitCode, " + drMyRows.Item("UtilityUnitCode").ToString
        '	stringSelect = "SELECT * FROM Errors WHERE (UtilityUnitCode = @UtilityUnitCode)"
        '	dtMyErrors = Factory.GetDataTable(stringSelect, parms)
        '	dsMyErrors.Merge(dtMyErrors)
        'Next

        Try
            dsMyErrors.Tables(0).TableName = "Errors"

        Catch ex As Exception

            Dim tbl As New DataTable("Errors")

            Dim col As DataColumn = tbl.Columns.Add("UtilityUnitCode", GetType(Char))
            col.AllowDBNull = False
            col = tbl.Columns.Add("Year", GetType(Int16))
            col.AllowDBNull = False
            col = tbl.Columns.Add("Period", GetType(Char))
            col.AllowDBNull = True
            col = tbl.Columns.Add("EventNumber", GetType(Int16))
            col.AllowDBNull = True
            col = tbl.Columns.Add("EventEvenCardNo", GetType(Byte))
            col.AllowDBNull = True
            col = tbl.Columns.Add("ErrorMessage", GetType(String))
            col.MaxLength = 80
            col.AllowDBNull = True
            col = tbl.Columns.Add("ErrorSeverity", GetType(Char))
            col.AllowDBNull = True
            col = tbl.Columns.Add("UnitShortName", GetType(Char))
            col.AllowDBNull = True

            dsMyErrors.Tables.Add(tbl)

        End Try

        dsMyErrors.AcceptChanges()
        dsMyErrors.Tables("Errors").DefaultView.Sort = "UnitShortName, Year, Period, EventNumber, EventEvenCardNo"

        Return dsMyErrors

    End Function

#Region " ErrorCheck() "

    Public Function ErrorCheck() As String

        Dim strErrorMsg As String = String.Empty
        Dim intOpenEvents As Integer
        Dim strHoursMatchMsg As String
        Dim intMonth As Integer
        Dim strErrors As String
        strErrors = String.Empty
        Dim dtRetirementDateTime As DateTime

        Try

            RestoreFromSession()

            ResetValidations()

            ' Autosave the Performance Data
            strErrors = PerformanceUpdate()

            If strErrors <> String.Empty Then

                Dim sScript As String

                sScript = "<script language='javascript'>"
                sScript += "alert('" & strErrors & "');"
                sScript += "</script>"

                Try
                    Response.Write(sScript)
                Catch ex As System.Exception
                    sScript = ex.ToString
                End Try

                Return strErrorMsg

            End If

            DisplayCheckStatus(True)

            'Page.DataBind()

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try
        ' ===================================================================
        Dim lHasErrors As Boolean

        ' Autosave the Event Data
        lHasErrors = EventUpdate()

        'Page.DataBind()

        If lHasErrors Then

            Dim sScript As String

            sScript = "<script language='javascript'>"
            sScript += "alert('There are Event Data errors');"
            sScript += "</script>"

            Try
                Response.Write(sScript)
            Catch ex As System.Exception
                sScript = ex.ToString
            End Try

            Return strErrorMsg

        End If

        DisplayCheckStatus(True)
        'Page.DataBind()

        blConnect.ClearMyISOErrors(strCurrentUnit, intCurrentYear)

        ' --------------------------------------------------
        ' Looking for open-ended events in any prior period
        ' --------------------------------------------------

        intOpenEvents = blConnect.CountOpenEvents(strCurrentUnit, intCurrentYear)

        If intOpenEvents > 0 Then

            Dim OpenEventsReader As IDataReader
            Dim intOpenYear As New ArrayList
            Dim intEventNumber As New ArrayList
            Dim strEventType As New ArrayList
            Dim dtimeStartDateTime As New ArrayList

            OpenEventsReader = blConnect.OpenEventYears(strCurrentUnit, intCurrentYear)

            While OpenEventsReader.Read

                ' Year, EventNumber, EventType, StartDateTime
                intOpenYear.Add(CInt(OpenEventsReader.GetValue(0)))
                intEventNumber.Add(CInt(OpenEventsReader.GetValue(1)))
                strEventType.Add(OpenEventsReader.GetString(2))
                dtimeStartDateTime.Add(OpenEventsReader.GetDateTime(3).ToString("MM/dd/yyyy  HH:mm"))

            End While

            OpenEventsReader.Close()

            If intOpenYear.Count > 0 Then

                Dim i As Integer
                Dim strOpenMsg As String

                For i = 0 To (intOpenYear.Count - 1)
                    strOpenMsg = "Open-ended " & strEventType(i).ToString & " starting at " & dtimeStartDateTime(i).ToString & " is still open"
                    blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(intOpenYear(i)), Nothing, Convert.ToInt32(intEventNumber(i)), 1, strOpenMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                Next

            End If

        End If

        Dim drCurrent As DataRow

        Dim dtDate As DateTime
        Dim douTime As Double

        Dim dtStart As DateTime
        Dim dtStart1 As DateTime
        Dim dtStart2 As DateTime

        Dim dtEnd As DateTime
        Dim dtEnd1 As DateTime
        Dim dtEnd2 As DateTime

        Dim dtTemp As DateTime
        Dim dtTemp2 As DateTime

        Dim alSHMethod As New ArrayList
        alSHMethod = blConnect.BuildALSHMethod()
        Dim dtCheck As System.DateTime = System.DateTime.Now

        Dim intDecPlaces As Int16 = 2
        Dim strHoursFormat As String = "000.00"

        Try
            Me.strCurrentPeriod = Me.lbUpdateMonth.SelectedItem.ToString
        Catch ex As Exception
            Me.lbUpdateMonth.SelectedIndex = 0
            Me.strCurrentPeriod = Me.lbUpdateMonth.SelectedItem.ToString
        End Try

        Dim intYear As Integer
        Dim intLoc As Integer
        Dim strMonth As String = String.Empty
        Dim strMonthNo As String = String.Empty
        Dim intMonthNo As Int16

        intYear = CShort(strCurrentPeriod.Substring((strCurrentPeriod.Length - 4), 4))
        intLoc = strCurrentPeriod.IndexOf(",")

        If intLoc > 0 Then
            strMonth = strCurrentPeriod.Substring(0, intLoc).ToUpper()
        Else
            intLoc = strCurrentPeriod.IndexOf(" ")
            If intLoc > 0 Then
                strMonth = strCurrentPeriod.Substring(0, intLoc).ToUpper()
            End If
        End If

        Select Case strMonth
            Case "JANUARY"
                strMonthNo = "01"
            Case "FEBRUARY"
                strMonthNo = "02"
            Case "MARCH"
                strMonthNo = "03"
            Case "APRIL"
                strMonthNo = "04"
            Case "MAY"
                strMonthNo = "05"
            Case "JUNE"
                strMonthNo = "06"
            Case "JULY"
                strMonthNo = "07"
            Case "AUGUST"
                strMonthNo = "08"
            Case "SEPTEMBER"
                strMonthNo = "09"
            Case "OCTOBER"
                strMonthNo = "10"
            Case "NOVEMBER"
                strMonthNo = "11"
            Case "DECEMBER"
                strMonthNo = "12"
            Case Else
                strMonthNo = "0"
        End Select

        intMonthNo = Convert.ToInt16(strMonthNo)

        If IsNumeric(strCurrentPeriod.Substring(strCurrentPeriod.Trim.Length - 4)) Then
            intYear = Convert.ToInt16(strCurrentPeriod.Substring(strCurrentPeriod.Trim.Length - 4))
        Else
            intYear = Convert.ToInt16(Now.Year)
        End If

        If intMonthNo > 0 Then
            dtCheck = System.DateTime.Parse(strMonthNo & "/01/" & intYear.ToString)
        Else
            dtCheck = System.DateTime.Now
        End If

        If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Or _
           blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 1 Then
            'drSetup.ServiceHourMethod = 0 Or drSetup.ServiceHourMethod = 1 Then

            ' ServiceHourMethod = 0 : All Event data are reported
            ' ServiceHourMethod = 1 : All Event data EXCEPT RS Events are reported
            ' ----------------------------------------------------------
            ' Comparing Performance 02 hours with calculated event data
            ' ----------------------------------------------------------

            Dim douAccuracy As Double

            intDecPlaces = 2
            strHoursFormat = "000.00"
            douAccuracy = 0.15

            Dim lFirstChange As Boolean
            Dim lSecondChange As Boolean

            Dim strTemp As String
            Dim douPOH(12) As Double
            Dim douFOH(12) As Double
            Dim douMOH(12) As Double
            Dim douSEH(12) As Double

            Dim douSH As Double
            Dim douPumpHrs(12) As Double
            Dim douSynHrs(12) As Double
            Dim douRSH(12) As Double
            Dim douPH(12) As Double

            Dim douIH(12) As Double

            For Each drCurrent In dsGridEvents.Tables("EventData01").Rows

                lFirstChange = False
                lSecondChange = False
                douTime = 0.0

                If drCurrent("RevisionCard01").ToString <> "X" Then

                    If Not Convert.IsDBNull(drCurrent("StartDateTime")) And Not Convert.IsDBNull(drCurrent("EventType")) Then

                        Try
                            ' the check to accept or reject this event record is based on the start of event datetime
                            dtCheck = System.DateTime.Parse(drCurrent("StartDateTime").ToString)
                        Catch ex As Exception
                            dtCheck = System.DateTime.Now
                        End Try

                        If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 2 Then
                            ' Event Data are NOT reported so do not load
                            blConnect.LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, "Settings show NO events to be reported", "E", drSetup("UnitShortName").ToString, GetFromDictionary)
                        ElseIf blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 1 And drCurrent("EventType").ToString.Trim = "RS" Then
                            ' Event Data are report but RS events are NOT reported so do not load
                            blConnect.LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, "Settings show NO RS events to be reported", "E", drSetup("UnitShortName").ToString, GetFromDictionary)
                            'ElseIf (drCurrent("EventType").ToString.Trim = "CO" Or drCurrent("EventType").ToString.Trim = "PU") And dtCheck < System.DateTime.Parse(COPUCUTOFFDATE) Then
                            '    ' Cannot report CO or PU events prior to 10/01/2006
                            '    blConnect.LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, "CO/PU events are not allowed before 10/01/2006", "E", drSetup("UnitShortName").ToString, GetFromDictionary)
                        End If

                        ' Have a valid Starting Date/time and Event Type

                        dtStart = Convert.ToDateTime(drCurrent("StartDateTime"))
                        strTemp = drCurrent("EventNumber").ToString

                        If Convert.IsDBNull(drCurrent("ChangeDateTime1")) Or Convert.IsDBNull(drCurrent("ChangeInEventType1")) Then

                            ' First Change in Event Date/time is null or the first Change in event type is null 
                            ' use End Date/time

                            If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                                ' This event is open ended use today's date and time

                                If drSetup.RetirementDate < System.DateTime.Now Then
                                    dtEnd = DateTime.Parse(drSetup.RetirementDate.ToShortDateString & " 23:59:59")
                                Else
                                    'dtEnd = System.DateTime.Now        ' do not convert to UtcNow
                                    dtEnd = blConnect.EOM(System.DateTime.Now)
                                End If

                                If dtEnd.Year <> dtStart.Year Then
                                    dtEnd = DateTime.Parse("12/31/" & dtStart.Year.ToString & " 23:59:59")
                                End If

                            Else
                                dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                            End If

                            'If Convert.ToInt16(drCurrent("CauseCode").ToString) = 0 Then
                            '	If drCurrent("EventType").ToString.ToUpper <> "RS" Then
                            '		If drSetup.InputDataRequirements = 0 Then
                            '			' NERC full data set requires that the Cause Code be filled in
                            '			blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drCurrent("Year")), dtStart.Month.ToString("00"), Convert.ToInt32(drCurrent("EventNumber")), 1, "Cause Code of 0000 is only valid for RS", "E", drSetup.UnitShortName)
                            '		End If
                            '	End If
                            'End If

                            Select Case drCurrent("EventType").ToString.ToUpper

                                Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "RU", "MB", "IR"

                                    'If drCurrent("EventType").ToString.ToUpper = "RS" Then
                                    '	If Convert.ToInt16(drCurrent("CauseCode").ToString) <> 0 Then
                                    '		blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drCurrent("Year")), dtStart.Month.ToString("00"), Convert.ToInt32(drCurrent("EventNumber")), 1, "RS requires a Cause Code of 0000", "E", drSetup.UnitShortName)
                                    '	End If
                                    'End If

                                    dtDate = dtStart

                                    If dtStart.Month = dtEnd.Month Then
                                        ' the event starts and stops in same month

                                        intMonth = dtStart.Month - 1

                                        douTime = TimeBetween(dtStart, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    ElseIf dtEnd.Month = dtStart.Month + 1 Then

                                        ' the event starts in month n and ends the next month

                                        intMonth = dtStart.Month - 1
                                        strTemp = dtStart.Month.ToString & "/" & System.DateTime.DaysInMonth(dtStart.Year, dtStart.Month).ToString & "/" & dtStart.Year.ToString & " 23:59:59"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtStart, dtTemp, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                        intMonth = dtEnd.Month - 1
                                        strTemp = dtEnd.Month.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtTemp, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    Else
                                        ' there are one or more months between the start and end of event

                                        intMonth = dtStart.Month - 1
                                        strTemp = dtStart.Month.ToString & "/" & System.DateTime.DaysInMonth(dtStart.Year, dtStart.Month).ToString & "/" & dtStart.Year.ToString & " 23:59:59"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtStart, dtTemp, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select


                                        For intMonth = dtStart.Month + 1 To dtEnd.Month - 1

                                            strTemp = intMonth.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                            dtTemp = System.DateTime.Parse(strTemp)
                                            strTemp = dtTemp.Month.ToString & "/" & System.DateTime.DaysInMonth(dtTemp.Year, dtTemp.Month).ToString & "/" & dtTemp.Year.ToString & " 23:59:59"
                                            dtTemp2 = System.DateTime.Parse(strTemp)

                                            douTime = TimeBetween(dtTemp, dtTemp2, drSetup.DaylightSavingTime)

                                            Select Case drCurrent("EventType").ToString.ToUpper
                                                Case "PO"
                                                    douPOH(intMonth - 1) += douTime
                                                Case "U1", "U2", "U3", "SF"
                                                    douFOH(intMonth - 1) += douTime
                                                Case "MO"
                                                    douMOH(intMonth - 1) += douTime
                                                Case "SE", "PE", "ME"
                                                    douSEH(intMonth - 1) += douTime
                                                Case "RS"
                                                    douRSH(intMonth - 1) += douTime
                                                Case "PU"
                                                    douPumpHrs(intMonth - 1) += douTime
                                                Case "CO"
                                                    douSynHrs(intMonth - 1) += douTime
                                                Case "IR", "MB", "RU"
                                                    douIH(intMonth - 1) += douTime
                                            End Select
                                        Next

                                        intMonth = dtEnd.Month - 1
                                        strTemp = dtEnd.Month.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtTemp, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    End If

                                Case Else
                                    douTime = 0.0
                            End Select

                        Else

                            lFirstChange = True
                            dtEnd1 = Convert.ToDateTime(drCurrent("ChangeDateTime1"))

                            Select Case drCurrent("EventType").ToString.ToUpper

                                Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                                    douTime = TimeBetween(dtStart, dtEnd1, drSetup.DaylightSavingTime)

                                    Select Case drCurrent("EventType").ToString.ToUpper
                                        Case "PO"
                                            douPOH(intMonth) += douTime
                                        Case "U1", "U2", "U3", "SF"
                                            douFOH(intMonth) += douTime
                                        Case "MO"
                                            douMOH(intMonth) += douTime
                                        Case "SE", "PE", "ME"
                                            douSEH(intMonth) += douTime
                                        Case "RS"
                                            douRSH(intMonth) += douTime
                                        Case "PU"
                                            douPumpHrs(intMonth) += douTime
                                        Case "CO"
                                            douSynHrs(intMonth) += douTime
                                        Case "IR", "MB", "RU"
                                            douIH(intMonth) += douTime
                                    End Select

                                Case Else
                                    douTime = 0.0
                            End Select

                            dtStart1 = dtEnd1

                            If Convert.IsDBNull(drCurrent("ChangeDateTime2")) Or Convert.IsDBNull(drCurrent("ChangeInEventType2")) Then
                                ' Second change in event date/time is null or the second change in event type is null
                                ' use End Date/time

                                If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                                    ' This event is open ended use today's date and time
                                    dtEnd = System.DateTime.Now          ' do not convert to UtcNow
                                    If dtEnd.Year <> dtStart1.Year Then
                                        dtEnd = DateTime.Parse("12/31/" & dtStart1.Year.ToString & " 23:59:59")
                                    End If

                                Else
                                    dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                                End If

                                Select Case drCurrent("ChangeInEventType1").ToString.ToUpper

                                    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                                        douTime = TimeBetween(dtStart1, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    Case Else
                                        douTime = 0.0
                                End Select

                            Else

                                lSecondChange = True
                                dtEnd2 = Convert.ToDateTime(drCurrent("ChangeDateTime2"))

                                Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                                    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                                        douTime = TimeBetween(dtStart1, dtEnd2, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    Case Else
                                        douTime = 0.0
                                End Select

                                dtStart2 = dtEnd2

                                If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                                    ' This event is open ended use today's date and time
                                    dtEnd = System.DateTime.Now           ' do not convert to UtcNow
                                    If dtEnd.Year <> dtStart2.Year Then
                                        dtEnd = DateTime.Parse("12/31/" & dtStart2.Year.ToString & " 23:59:59")
                                    End If

                                Else
                                    dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                                End If

                                Select Case drCurrent("ChangeInEventType2").ToString.ToUpper
                                    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"
                                        douTime = TimeBetween(dtStart2, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("ChangeInEventType2").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    Case Else
                                        douTime = 0.0
                                End Select
                            End If

                        End If

                    End If

                End If

            Next

            Dim intValue As Integer
            Dim intPerfMonth As Int16

            For Each drPerformance As BusinessLayer.Performance.PerformanceDataRow In dtPerformance
                ' This loop works for saved performance records including current month since a "Save" was performned when the Batch Check started

                intMonth = Convert.ToInt16(drPerformance.Period) - 1
                dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

                ' this was required because if the hours were supposed to be xxx.50 and there was the "one second" adjustment, 
                ' it would round down; not the same as xxx.50
                douPOH(intMonth) = System.Math.Round(douPOH(intMonth), 3)
                douMOH(intMonth) = System.Math.Round(douMOH(intMonth), 3)
                douFOH(intMonth) = System.Math.Round(douFOH(intMonth), 3)
                douSEH(intMonth) = System.Math.Round(douSEH(intMonth), 3)
                douRSH(intMonth) = System.Math.Round(douRSH(intMonth), 3)

                'If dtCheck >= System.DateTime.Parse(COPUCUTOFFDATE) Then
                '    ' At ISO-NE these events are only allowed afer 10/01/2006
                '    douPumpHrs(intMonth) = System.Math.Round(douPumpHrs(intMonth), 3)
                '    douSynHrs(intMonth) = System.Math.Round(douSynHrs(intMonth), 3)
                'Else
                douPumpHrs(intMonth) = System.Math.Round(drPerformance.PumpingHours, 3)
                douSynHrs(intMonth) = System.Math.Round(drPerformance.SynchCondHours, 3)
                'End If

                douIH(intMonth) = System.Math.Round(douIH(intMonth), 3)

                Select Case blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) 'drSetup.ServiceHourMethod
                    Case 0
                        ' ServiceHourMethod = 0 : All Event data are reported

                        If System.Math.Abs(drPerformance.RSHours - Convert.ToDecimal(System.Math.Round(douRSH(intMonth), intDecPlaces))) > douAccuracy Then

                            ' Issue Error since they are both reported and the error is bigger than allowed
                            strHoursMatchMsg = "Reserve Shutdown hours (" & drPerformance.RSHours.ToString & ") do not match event data hours (" & System.Math.Round(douRSH(intMonth), intDecPlaces).ToString & ")"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)

                        End If

                    Case 1
                        ' ServiceHourMethod = 1 : All Event data EXCEPT RS Events are reported

                        If System.Math.Abs(drPerformance.RSHours - Convert.ToDecimal(System.Math.Round(douRSH(intMonth), intDecPlaces))) > douAccuracy Then

                            If drPerformance.RSHours > 0.0 And douRSH(intMonth) > 0.0 Then
                                strHoursMatchMsg = "Setup indicates that Reserve Shutdowns are NOT Reported, but there are RS Events"
                                blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)

                                strHoursMatchMsg = "Reserve Shutdown hours (" & drPerformance.RSHours.ToString & ") do not match event data hours (" & System.Math.Round(douRSH(intMonth), intDecPlaces).ToString & ")"
                                blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                            End If

                            douRSH(intMonth) = drPerformance.RSHours

                        End If

                End Select

                ' Planned Outage Hours

                If System.Math.Abs(drPerformance.PlannedOutageHours - Convert.ToDecimal(System.Math.Round(douPOH(intMonth), intDecPlaces))) > douAccuracy Then
                    strHoursMatchMsg = "Planned Outage hours (" & drPerformance.PlannedOutageHours.ToString & ") do not match event data hours (" & System.Math.Round(douPOH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                ' Maintenance Outage Hours

                If System.Math.Abs(drPerformance.MaintOutageHours - Convert.ToDecimal(System.Math.Round(douMOH(intMonth), intDecPlaces))) > douAccuracy Then
                    strHoursMatchMsg = "Maintenance Outage hours (" & drPerformance.MaintOutageHours.ToString & ") do not match event data hours (" & System.Math.Round(douMOH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                ' Forced Outage Hours

                If System.Math.Abs(drPerformance.ForcedOutageHours - Convert.ToDecimal(System.Math.Round(douFOH(intMonth), intDecPlaces))) > douAccuracy Then
                    strHoursMatchMsg = "Forced Outage hours (" & drPerformance.ForcedOutageHours.ToString & ") do not match event data hours (" & System.Math.Round(douFOH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                'If dtCheck >= System.DateTime.Parse(COPUCUTOFFDATE) Then

                '    ' At ISO-NE these events are only allowed afer 10/01/2006

                '    ' Synch Condensing Hours

                '    If System.Math.Abs(drPerformance.SynchCondHours - Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))) > douAccuracy Then
                '        strHoursMatchMsg = "Synch Cond hours (" & drPerformance.SynchCondHours.ToString & ") do not match event data hours (" & System.Math.Round(douSynHrs(intMonth), intDecPlaces).ToString & ")"
                '        blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                '    End If

                '    ' Pumping Hours

                '    If System.Math.Abs(drPerformance.PumpingHours - Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))) > douAccuracy Then
                '        strHoursMatchMsg = "Pumping hours (" & drPerformance.PumpingHours.ToString & ") do not match event data hours (" & System.Math.Round(douPumpHrs(intMonth), intDecPlaces).ToString & ")"
                '        blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                '    End If

                'End If

                ' =========
                ' SE Hours

                If System.Math.Abs(drPerformance.ExtofSchedOutages - Convert.ToDecimal(System.Math.Round(douSEH(intMonth), intDecPlaces))) > douAccuracy Then
                    strHoursMatchMsg = "Ext of Sch Outage hours (" & drPerformance.ExtofSchedOutages.ToString & ") do not match event data hours (" & System.Math.Round(douSEH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                ' =============
                ' Period Hours

                dtRetirementDateTime = DateTime.Parse(drSetup.RetirementDate.ToShortDateString & " 23:59:59")

                If drSetup.CommercialDate >= DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString) Then

                    If drSetup.CommercialDate < DateTime.Parse(drPerformance.Period & "/" & System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) & "/" & drPerformance.Year.ToString & " 23:59:59") Then

                        douSH = TimeBetween(drSetup.CommercialDate, DateTime.Parse(drPerformance.Period & "/" & System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) & "/" & drPerformance.Year.ToString & " 23:59:59"), drSetup.DaylightSavingTime) _
                         - Convert.ToDouble(System.Math.Round(douRSH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douMOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douFOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSEH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPumpHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSynHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douIH(intMonth), intDecPlaces))
                    Else

                        douSH = 0.0

                    End If

                ElseIf dtRetirementDateTime <= DateTime.Parse(drPerformance.Period & "/" & System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) & "/" & drPerformance.Year.ToString & " 23:59:59") Then

                    If dtRetirementDateTime > DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString & " 0:00:00") Then

                        douSH = TimeBetween(DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString & " 0:00:00"), drSetup.RetirementDate, drSetup.DaylightSavingTime) _
                         - Convert.ToDouble(System.Math.Round(douRSH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douMOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douFOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSEH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPumpHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSynHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douIH(intMonth), intDecPlaces))

                    Else

                        douSH = 0.0

                    End If

                Else

                    intValue = System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) * 24
                    intPerfMonth = Convert.ToInt16(drPerformance.Period)

                    Select Case drSetup.DaylightSavingTime

                        Case 0

                            ' 0 = No daylight saving time
                            ' plus Japan and China

                        Case 1

                            ' USA and Canada

                            If drPerformance.Year < 2007 Then
                                If intPerfMonth = 4 Then
                                    intValue -= 1
                                ElseIf intPerfMonth = 10 Then
                                    intValue += 1
                                End If
                            Else
                                If intPerfMonth = 3 Then
                                    intValue -= 1
                                ElseIf intPerfMonth = 11 Then
                                    intValue += 1
                                End If
                            End If

                            'Case 2, 3, 4, 5

                            '    ' Europe and Russia
                            '    If intPerfMonth = 3 Then
                            '        intValue -= 1
                            '    ElseIf intPerfMonth = 10 Then
                            '        intValue += 1
                            '    End If

                            'Case 6, 7, 8

                            '    ' Australia, New Zealand, et al
                            '    If intPerfMonth = 3 Then
                            '        intValue += 1
                            '    ElseIf intPerfMonth = 10 Then
                            '        intValue -= 1
                            '    End If

                            'Case 9

                            '    ' Tonga
                            '    If intPerfMonth = 1 Then
                            '        intValue += 1
                            '    ElseIf intPerfMonth = 11 Then
                            '        intValue -= 1
                            '    End If

                    End Select

                    ' ==============================
                    ' Calculate an event-derived SH

                    douSH = Math.Round((Convert.ToDouble(intValue) _
                                  - Convert.ToDouble(System.Math.Round(douRSH(intMonth), intDecPlaces)) _
                                  - Convert.ToDouble(System.Math.Round(douPOH(intMonth), intDecPlaces)) _
                                  - Convert.ToDouble(System.Math.Round(douMOH(intMonth), intDecPlaces)) _
                                  - Convert.ToDouble(System.Math.Round(douFOH(intMonth), intDecPlaces)) _
                                  - Convert.ToDouble(System.Math.Round(douSEH(intMonth), intDecPlaces)) _
                                  - Convert.ToDouble(System.Math.Round(douPumpHrs(intMonth), intDecPlaces)) _
                                  - Convert.ToDouble(System.Math.Round(douSynHrs(intMonth), intDecPlaces)) _
                                  - Convert.ToDouble(System.Math.Round(douIH(intMonth), intDecPlaces))), intDecPlaces)

                End If
                ' =====================================
                ' Subtract Pumping Hours if applicable

                If drSetup.IsPumpingDataNull Then
                    drSetup.PumpingData = False
                End If

                If drSetup.PumpingData Or douPumpHrs(intMonth) > 0 Then

                    If douPumpHrs(intMonth) > 0 Then
                        drPerformance.PumpingHours = Convert.ToDecimal(douPumpHrs(intMonth))
                    Else
                        douSH -= drPerformance.PumpingHours
                    End If

                End If

                ' ====================================================
                ' Subtract Synchronous Condensing Hours if applicable

                If drSetup.IsSynchCondDataNull Then
                    drSetup.SynchCondData = False
                End If

                If drSetup.SynchCondData Or douSynHrs(intMonth) > 0 Then

                    If douSynHrs(intMonth) > 0 Then
                        drPerformance.SynchCondHours = Convert.ToDecimal(douSynHrs(intMonth))
                    Else
                        douSH -= drPerformance.SynchCondHours
                    End If

                End If

                ' ==============
                ' PJM 025 Check

                'If Not drPerformance.IsActualStartsNull Then

                '    If drSetup.PJM And ((douSH + drPerformance.SynchCondHours) = 0 Or (drPerformance.ServiceHours + drPerformance.SynchCondHours) = 0) And drPerformance.ActualStarts > 0 Then
                '        strHoursMatchMsg = "If Service and Condensing hours are 0, Actual Starts must also be 0 (PJM25)"
                '        blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                '    End If

                'End If

                '' =========================
                '' PJM 027 and PJM 6 Checks

                'If drSetup.PJM And drPerformance.TypUnitLoading = 1 And drPerformance.RSHours > 0 Then
                '    strHoursMatchMsg = "If RS hours are greater than 0, Loading Characteristic cannot be 1 (PJM027/6)"
                '    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                'End If

                ' =====================================
                ' Generation Checks with Service Hours

                If drSetup.IsGrossNetBothNull Then
                    drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.Both.ToString)
                End If

                If drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                    If drPerformance.IsGrossGenNull Then
                        drPerformance.GrossGen = 0
                    End If

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                    '		strHoursMatchMsg = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                    '		strHoursMatchMsg = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.GrossGen = 0 Then
                    '		strHoursMatchMsg = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                    If drPerformance.IsNetGenNull Then
                        drPerformance.NetGen = 0
                    End If

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                            strHoursMatchMsg = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                            strHoursMatchMsg = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.NetGen = 0 Then
                            strHoursMatchMsg = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Then

                    If drPerformance.IsGrossGenNull Then
                        drPerformance.GrossGen = 0
                    End If

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                    '		strHoursMatchMsg = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                    '		strHoursMatchMsg = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.GrossGen = 0 Then
                    '		strHoursMatchMsg = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Then

                    If drPerformance.IsNetGenNull Then
                        drPerformance.NetGen = 0
                    End If

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                            strHoursMatchMsg = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                            strHoursMatchMsg = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.NetGen = 0 Then
                            strHoursMatchMsg = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                End If

                ' ===================================================================
                ' Do Service hours from Performance 02 card match Event-derived SH ?

                If System.Math.Abs(drPerformance.ServiceHours - Convert.ToDecimal(System.Math.Round(douSH, intDecPlaces))) > douAccuracy Then

                    strHoursMatchMsg = "Service hours (" & drPerformance.ServiceHours.ToString & ") do not match hours derived from event data hours (" & System.Math.Round(douSH, intDecPlaces).ToString & ")"

                    dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

                    If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Or _
                       blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 1 Then

                        If drPerformance.RSHours > 0.0 And douRSH(intMonth) = 0.0 Then
                            ' Possibly this unit does not report Reserve Shutdown events -- issue warning only
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        Else
                            ' Issue Error since they are both reported and the error is bigger than allowed
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                End If

            Next

        Else
            ' ServiceHourMethod = 2 : Event data is NOT reported
            ' cannot cross-check event and performance 02 record data

            strHoursMatchMsg = "Setup indicates that Event data are NOT Reported"
            Try
                Dim strSQLChkDup As String
                Dim myDS As DataSet

                strSQLChkDup = "SELECT * FROM Errors WHERE (UtilityUnitCode = '" & strCurrentUnit & "') AND (ErrorMessage = '" & strHoursMatchMsg & "') AND (ErrorSeverity = 'W')"
                myDS = blConnect.GetADataset(strSQLChkDup, Nothing)

                If Not IsNothing(myDS) Then
                    If myDS.Tables(0).Rows.Count = 0 Then
                        blConnect.LoadErrors(strCurrentUnit, 0, String.Empty, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                    End If
                Else
                    blConnect.LoadErrors(strCurrentUnit, 0, String.Empty, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                End If

            Catch ex As Exception

            End Try

            'Dim intValue As Integer
            'Dim intPerfMonth As Int16

            For Each drPerformance As BusinessLayer.Performance.PerformanceDataRow In dtPerformance

                intMonth = Convert.ToInt16(drPerformance.Period) - 1

                If drSetup.IsGrossNetBothNull Then
                    drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.Both.ToString)
                End If

                If drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And drPerformance.ServiceHours = 0) Then
                    '		strHoursMatchMsg = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And drPerformance.ServiceHours > 0) Then
                    '		strHoursMatchMsg = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.GrossGen = 0) Then
                    '		strHoursMatchMsg = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And drPerformance.ServiceHours = 0) Then
                            strHoursMatchMsg = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And drPerformance.ServiceHours > 0) Then
                            strHoursMatchMsg = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.NetGen = 0) Then
                            strHoursMatchMsg = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Then

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And drPerformance.ServiceHours = 0) Then
                    '		strHoursMatchMsg = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And drPerformance.ServiceHours > 0) Then
                    '		strHoursMatchMsg = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.GrossGen = 0) Then
                    '		strHoursMatchMsg = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Then

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And drPerformance.ServiceHours = 0) Then
                            strHoursMatchMsg = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And drPerformance.ServiceHours > 0) Then
                            strHoursMatchMsg = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.NetGen = 0) Then
                            strHoursMatchMsg = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                End If

                If drSetup.IsSynchCondDataNull Then
                    drSetup.SynchCondData = True
                End If

                If drPerformance.IsInactiveHoursNull Then
                    drPerformance.InactiveHours = 0
                End If

                If Math.Round(drPerformance.PeriodHours, 2) <> Math.Round((drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.PumpingHours + drPerformance.SynchCondHours + drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages + drPerformance.InactiveHours), intDecPlaces) Then
                    Me.lblAvailHoursValue.Text = Math.Round((drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.PumpingHours + drPerformance.SynchCondHours), 2).ToString(strHoursFormat)
                    Me.lblUnavailHoursValue.Text = Math.Round((drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages), intDecPlaces).ToString(strHoursFormat)
                    strHoursMatchMsg = "Available + Unavailable Hours must equal Period Hours"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                'If drSetup.PJM And (drPerformance.ServiceHours + drPerformance.SynchCondHours) = 0 And drPerformance.ActualStarts > 0 Then
                '    strHoursMatchMsg = "If Service and Condensing hours are 0, Actual Starts must also be 0 (PJM25)"
                '    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                'End If

                'If drSetup.PJM And drPerformance.TypUnitLoading = 1 And drPerformance.RSHours > 0 Then
                '    strHoursMatchMsg = "If RS hours are greater than 0, Loading Characteristic cannot be 1 (PJM027/6)"
                '    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                'End If

            Next

        End If

        ' ----------------------------------------------------------------
        ' Checking Available Capacities against the Dependable Capacities
        ' ----------------------------------------------------------------

        Dim GDC(12) As Decimal
        Dim NDC(12) As Decimal
        Dim decFuelBtus As Decimal

        For Each drPerformance As BusinessLayer.Performance.PerformanceDataRow In dtPerformance

            intMonth = Convert.ToInt16(drPerformance.Period) - 1

            If drPerformance.IsGrossDepCapNull Then
                GDC(intMonth) = 0
            Else
                GDC(intMonth) = drPerformance.GrossDepCap
            End If

            If drPerformance.IsNetDepCapNull Then
                NDC(intMonth) = 0
            Else
                NDC(intMonth) = drPerformance.NetDepCap
            End If

            Select Case drPerformance.PriFuelCode.ToUpper
                Case "WA", "WM", "SO", "GE", "WH", "NU"

                    decFuelBtus = 0D

                Case Else

                    decFuelBtus = 0D

                    If Not drPerformance.IsPriBtusNull Then
                        decFuelBtus += drPerformance.PriBtus
                    End If

                    If Not drPerformance.IsSecBtusNull Then
                        decFuelBtus += drPerformance.SecBtus
                    End If

                    If Not drPerformance.IsTerBtusNull Then
                        decFuelBtus += drPerformance.TerBtus
                    End If

                    If Not drPerformance.IsQuaBtusNull Then
                        decFuelBtus += drPerformance.QuaBtus
                    End If

                    If drSetup.InputDataRequirements = 0 Then

                        ' ONLY NERC full set requires this warning; NYISO does not collect fuel

                        'If Not drPerformance.IsGrossGenNull Then
                        '	If (drPerformance.GrossGen > 0 And decFuelBtus = 0D) Then
                        '		strHoursMatchMsg = "Gross generation > 0, but no fuel quantity burned and heat content data"
                        '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                        '	End If
                        'End If

                        'If Not drPerformance.IsNetGenNull Then
                        '	If (drPerformance.NetGen > 0 And decFuelBtus = 0D) Then
                        '		strHoursMatchMsg = "Net generation > 0, but no fuel quantity burned and heat content data"
                        '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName)
                        '	End If
                        'End If

                    End If

            End Select

        Next

        Dim vueDeratings As DataView

        vueDeratings = dsGridEvents.Tables("EventData01").DefaultView

        vueDeratings.Sort = "StartDateTime, EndDateTime"
        vueDeratings.RowFilter = "EventType IN ('D1', 'D2', 'D3', 'D4', 'PD', 'DE', 'DM', 'DP') AND RevisionCard01 <> 'X'"

        Dim intCounter As Integer
        Dim drvEvent1 As DataRowView
        Dim intMyMonth As Integer

        For intCounter = 0 To vueDeratings.Count - 1

            drvEvent1 = vueDeratings(intCounter)

            If Not Convert.IsDBNull(drvEvent1("StartDateTime")) And Not Convert.IsDBNull(drvEvent1("EventType")) Then

                ' Have a valid Starting Date/time and Event Type

                dtStart = Convert.ToDateTime(drvEvent1("StartDateTime"))

                If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                    ' This event is open ended use midnight on last day of year
                    dtEnd = Convert.ToDateTime("12/31/" & drPerformance.Year.ToString & "  23:59:59")
                Else
                    dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                End If

            End If

            'If Not drSetup.IsPJMNull Then
            '    If drSetup.PJM Then
            '        If Not Convert.IsDBNull(drvEvent1("WorkStarted")) Then
            '            If Convert.ToDateTime(drvEvent1("WorkStarted")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkStarted")) >= dtEnd Then
            '                strHoursMatchMsg = "Work Start Time is outside the event times (PJM116)"
            '                blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
            '            End If
            '        End If
            '        If Not Convert.IsDBNull(drvEvent1("WorkEnded")) Then
            '            If Convert.ToDateTime(drvEvent1("WorkEnded")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkEnded")) >= dtEnd Then
            '                strHoursMatchMsg = "Work End Time is outside the event times (PJM117)"
            '                blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
            '            End If
            '        End If
            '    End If
            'End If

            For intMyMonth = dtStart.Month To dtEnd.Month

                'If Not IsDBNull(drvEvent1("GrossAvailCapacity")) Then

                '	If GDC(intMyMonth - 1) > 0 And Convert.ToInt32(drvEvent1("GrossAvailCapacity").ToString) > GDC(intMyMonth - 1) Then

                '		strHoursMatchMsg = "GAC cannot be greater than GDC for indicated month"
                '		blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "E", drSetup.UnitShortName)

                '	End If

                'End If

                If Not IsDBNull(drvEvent1("NetAvailCapacity")) Then

                    If NDC(intMyMonth - 1) > 0 And Convert.ToDecimal(drvEvent1("NetAvailCapacity").ToString) > NDC(intMyMonth - 1) Then

                        strHoursMatchMsg = "NAC cannot be greater than NDC for indicated month"
                        blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)

                    End If

                End If

            Next

        Next

        vueDeratings.RowFilter = String.Empty

        ' ===================================================
        ' Counting the number of Attempted and Actual Starts 


        ' |---- outage ---|--- outage ---|  |--- outage ---|  |--- outage ---|
        '    |-------------- month ---------------------------------|
        '                          start ^           start ^

        ' |--- outage ---|--- outage ---|--- SF ---|  |--- outage ---|
        '    |-------------- month ---------------------------------|
        '               ATTEMPTED Start ^    start ^

        ' |--- outage ---|--- outage ---|--- SF ---|--- outage ---|
        '    |-------------- month ---------------------------------|
        '               ATTEMPTED Start ^                   start ^

        Dim intAttemptedStarts(12) As Integer
        Dim intActualStarts(12) As Integer

        Dim intHotAttemptedStarts(12) As Integer
        Dim intWarmAttemptedStarts(12) As Integer
        Dim intColdAttemptedStarts(12) As Integer

        Dim intHotActualStarts(12) As Integer
        Dim intWarmActualStarts(12) As Integer
        Dim intColdActualStarts(12) As Integer
        Dim datetimePriorEvent As DateTime
        datetimePriorEvent = Convert.ToDateTime("11/22/1948")

        Dim longInterval As Long     ' time between events in seconds

        Dim vue As DataView

        vue = dsGridEvents.Tables("EventData01").DefaultView

        vue.Sort = "StartDateTime, EndDateTime"
        vue.RowFilter = "EventType IN ('U1','U2','U3','SF','PO','MO','SE','RS','PE','ME','IR','MB','RU') AND RevisionCard01 <> 'X'"

        Dim lLastEventWasSF As Boolean
        lLastEventWasSF = False
        Dim intLastSFEventNo As Integer

        If drSetup.IsPJMNull Then
            drSetup.PJM = False
        End If

        If drSetup.IsPJMStartsCountNull Then
            drSetup.PJMStartsCount = True
        End If

        For intCounter = 0 To vue.Count - 1

            drvEvent1 = vue(intCounter)

            If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Month = 1 And Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Day = 1 And _
                   Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Hour = 0 And Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute <= 1 Then

                ' Looking for a one-minute gap between the first event for the year and the last event for prior year

                Dim strSQLSelectPriorYear As String
                Dim dsChecking As DataSet

                strSQLSelectPriorYear = "SELECT * FROM EventData01 WHERE UtilityUnitCode = '" & drvEvent1.Item("UtilityUnitCode").ToString & "'"

                'If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                '    strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"

                'ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                '    strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"

                'End If

                If blConnect.ThisProvider = "Oracle" Then

                    If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                        'strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"
                        strSQLSelectPriorYear += " AND EndDateTime = to_date ('" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00").ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                    ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                        'strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"
                        strSQLSelectPriorYear += " AND EndDateTime = to_date ('" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59").ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                    End If

                Else

                    If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                        strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"

                    ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                        strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"

                    End If

                End If

                strSQLSelectPriorYear += " AND EventType IN ('U1','U2','U3','MO','PO','SE','SF','RS','PU','CO','PE','ME','IR','MB','RU') AND RevisionCard01 <> 'X'"

                dsChecking = blConnect.GetADataset(strSQLSelectPriorYear, Nothing)

                If Not IsNothing(dsChecking) Then
                    If dsChecking.Tables(0).Rows.Count > 0 Then
                        strHoursMatchMsg = "1 minute gap between this event and in last event in " & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString
                        blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), Nothing, Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                    End If
                End If

            End If


            If Not Convert.IsDBNull(drvEvent1("StartDateTime")) And Not Convert.IsDBNull(drvEvent1("EventType")) Then

                ' Have a valid Starting Date/time and Event Type

                dtStart = Convert.ToDateTime(drvEvent1("StartDateTime"))

                If Convert.IsDBNull(drvEvent1("ChangeDateTime1")) Or Convert.IsDBNull(drvEvent1("ChangeInEventType1")) Then

                    ' First Change in Event Date/time is null or the first Change in event type is null 
                    ' use End Date/time

                    If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                        ' This event is open ended use midnight on last day of year
                        dtEnd = Convert.ToDateTime("12/31/" & drPerformance.Year.ToString & "  23:59:59")
                    Else
                        dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                    End If

                    'If Not drSetup.IsPJMNull Then
                    '    If drSetup.PJM Then
                    '        If Not Convert.IsDBNull(drvEvent1("WorkStarted")) Then
                    '            If Convert.ToDateTime(drvEvent1("WorkStarted")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkStarted")) >= dtEnd Then
                    '                strHoursMatchMsg = "Work Start Time is outside the event times (PJM116)"
                    '                blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                    '            End If
                    '        End If
                    '        If Not Convert.IsDBNull(drvEvent1("WorkEnded")) Then
                    '            If Convert.ToDateTime(drvEvent1("WorkEnded")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkEnded")) >= dtEnd Then
                    '                strHoursMatchMsg = "Work End Time is outside the event times (PJM117)"
                    '                blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                    '            End If
                    '        End If
                    '    End If
                    'End If

                    dtDate = dtStart

                    If Convert.IsDBNull(drvEvent1("CarryOverLastYear")) Then
                        drvEvent1("CarryOverLastYear") = False
                    End If

                    If Not Convert.ToBoolean(drvEvent1("CarryOverLastYear")) Then

                        If datetimePriorEvent > Convert.ToDateTime("11/22/1948") Then

                            ' There is a prior event

                            If drvEvent1("EventType").ToString = "SF" Then

                                intAttemptedStarts(dtStart.Month - 1) += 1
                                lLastEventWasSF = True
                                intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval > 1 Then
                                    strHoursMatchMsg = "SF does not coincide with the End Date of any previous Event or RS"
                                    blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), dtStart.Month.ToString("00"), intLastSFEventNo, 1, strHoursMatchMsg, "E", drSetup.UnitShortName, GetFromDictionary)
                                End If

                            Else

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval > 61 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strHoursMatchMsg = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strHoursMatchMsg, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                ElseIf longInterval = 60 Or longInterval = 59 Or longInterval = 61 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strHoursMatchMsg = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strHoursMatchMsg, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                    strHoursMatchMsg = "1 minute inservice precedes this event -- affects count of Starts"
                                    blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), dtStart.Month.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)

                                End If

                                lLastEventWasSF = False

                            End If

                        Else

                            ' There is no prior event -- this is the first one for the year

                            If Not IsNothing(drvEvent1) Then

                                If drvEvent1("EventType").ToString = "SF" Then
                                    intAttemptedStarts(dtStart.Month - 1) += 1
                                    lLastEventWasSF = True
                                    intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                                Else
                                    lLastEventWasSF = False
                                End If

                            Else
                                lLastEventWasSF = False
                            End If

                        End If

                    End If

                    datetimePriorEvent = dtEnd

                Else

                    lLastEventWasSF = False

                    If drvEvent1("EventType").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(dtStart.Month - 1) += 1
                    End If

                    If drvEvent1("ChangeInEventType1").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(Convert.ToDateTime(drvEvent1("ChangeDateTime1")).Month - 1) += 1
                        lLastEventWasSF = True
                        intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                    End If

                    If drvEvent1("ChangeInEventType2").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(Convert.ToDateTime(drvEvent1("ChangeDateTime2")).Month - 1) += 1
                        lLastEventWasSF = True
                        intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                    Else
                        Select Case drvEvent1("EventType").ToString.ToUpper
                            Case "U1", "U2", "U3", "PO", "MO", "SE", "RS", "PE", "ME", "CO", "PU", "IR", "MB", "RU"
                                lLastEventWasSF = False
                        End Select

                    End If

                    If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                        ' This event is open ended use midnight on last day of year
                        dtEnd = Convert.ToDateTime("12/31/" & drPerformance.Year.ToString & "  23:59:59")
                    Else
                        dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                    End If

                    Select Case drvEvent1("EventType").ToString.ToUpper

                        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PE", "ME", "IR", "MB", "RU"

                            dtDate = dtStart

                            If Not IsNothing(datetimePriorEvent) Then

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval > 61 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strHoursMatchMsg = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strHoursMatchMsg, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                ElseIf longInterval = 60 Or longInterval = 59 Or longInterval = 61 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strHoursMatchMsg = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strHoursMatchMsg, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                    strHoursMatchMsg = "1 minute inservice precedes this event -- affects count of Starts"
                                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, dtStart.Month.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)

                                End If

                            End If

                    End Select

                    datetimePriorEvent = dtEnd

                End If

                Select Case drvEvent1("EventType").ToString.ToUpper
                    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "NC", "RS", "D1", "D2", "D3", "D4", "PD", "DE", "CO", "PU", "ME", "PE", "DM", "DP"
                    Case "RU", "MB", "IR"
                        'lHasErrors = True
                        'strHoursMatchMsg = drSetup.UnitName & " " & drvEvent1("Year").ToString & " " & drvEvent1.Item("EventNumber").ToString & " ERROR: Invalid Event Type"
                        'blConnect.LoadErrors(drSetup.UtilityUnitCode, CInt(drvEvent1.Item("Year")), String.Empty, CInt(drvEvent1.Item("EventNumber")), 1, "Contact ISO for this event type", "E", drSetup.UnitShortName, GetFromDictionary)
                        'drSetup.CheckStatus = "E"
                    Case Else
                        lHasErrors = True
                        strHoursMatchMsg = drSetup.UnitName & " " & drvEvent1.Item("Year").ToString & " " & drvEvent1.Item("EventNumber").ToString & " ERROR: Invalid Event Type"
                        blConnect.LoadErrors(drSetup.UtilityUnitCode, CInt(drvEvent1.Item("Year")), String.Empty, CInt(drvEvent1.Item("EventNumber")), 1, "Invalid Event Type", "E", drSetup.UnitShortName, GetFromDictionary)
                End Select

            End If

        Next

        If vue.Count > 0 Then

            If System.DateTime.Compare(dtEnd, Convert.ToDateTime("12/31/" & intYear.ToString & "  23:59:59")) <> 0 Then

                'If Not lLastEventWasSF Then
                intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                'End If

                intActualStarts(datetimePriorEvent.Month - 1) += 1

            End If

        End If

        dtCheck = System.DateTime.Now

        For Each drPerformance As BusinessLayer.Performance.PerformanceDataRow In dtPerformance

            dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

            If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Then  ' drSetup.ServiceHourMethod = 0 Then
                ' This loop works for saved performance records including current month since a "Save" was performned when the Batch Check started

                intMonth = Convert.ToInt16(drPerformance.Period) - 1

                If drPerformance.IsActualStartsNull Then
                    drPerformance.ActualStarts = 0
                End If

                If drPerformance.IsAttemptedStartsNull Then
                    drPerformance.AttemptedStarts = 0
                End If

                ' 2006 NERC GADS changes ... set these back to "W"
                If drPerformance.ActualStarts <> intActualStarts(intMonth) Then
                    strHoursMatchMsg = "Actual Starts (" & drPerformance.ActualStarts.ToString & ") do not match event derived starts data (" & intActualStarts(intMonth).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                End If

                If drPerformance.AttemptedStarts <> intAttemptedStarts(intMonth) Then
                    strHoursMatchMsg = "Attempted Starts (" & drPerformance.AttemptedStarts.ToString & ") do not match event derived starts data (" & intAttemptedStarts(intMonth).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strHoursMatchMsg, "W", drSetup.UnitShortName, GetFromDictionary)
                End If

            End If

            If drPerformance.IsNetDepCapNull Then
                drPerformance.NetDepCap = 0
            End If

            If drPerformance.IsNetMaxCapNull Then
                drPerformance.NetMaxCap = 0
            End If

            If drPerformance.NetDepCap <= 0 Then
                strHoursMatchMsg = drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC <= 0"
                blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
            End If

            If drPerformance.NetMaxCap <= 0 Then
                strHoursMatchMsg = drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NMC <= 0"
                blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NMC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
            End If

            If drPerformance.NetDepCap > drPerformance.NetMaxCap Then
                strHoursMatchMsg = drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC > NMC"
                blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC > NMC", "E", drSetup.UnitShortName, GetFromDictionary)
            End If

        Next

        vue.RowFilter = String.Empty

        blConnect.FindMissingMonths(dtSetup, intCurrentYear, strCurrentMonthNo, GetFromDictionary)

        ' If a card has an SE as the first event, look for prior PO, MO or SE
        ' this should include cards pre-1996
        ' IF eventtype = "SE" and eventnumber > 1 and the start datetime is NOT 010100XX or 01010000 then
        '    skip backwards thru file looking for prior full outage
        '    SE start datetime must match prior PO or MO end datetime, but SE to SE might have gap if testing
        '       offline.
        ' else
        '    (1) not an SE
        '    (2) an SE but the first event for the year and carries over from last year
        '    (3) an SE but the first event for the year but doesn't carry over from last year
        '    (4) an SE but not the first event for the year and does have a start datetime of 010100XX or 01010000
        '        (e.g., the first event entered is a derating)
        '    (5) for pre-1996 see if the 1st and 2nd change in event types are also an SE
        '
        ' Need to be careful for checking the transition datetime to make sure that we've addressed offline testing

        ' IF Full NERC set, make sure that the 02/03 card data has been filled in 

        ' Repeat the above for DEs and D4, PD and DEs...

        ' For Pre-1996 errors:
        '
        ' IF NOT EMPTY(time1) when EMPTY(et1):  1st Event Type change blank but 1st Time Change not blank
        ' If there is a 1st change in event type, then there must be a 04-99 card with Contribution Code = 6 (Card D)
        'ELSE
        '   IF NOT EMPTY(et1) when EMPTY(time1) 1st Event Type change must be blank if 1st Time Change is blank
        '   IF NOT EMPTY(time2) when EMPTY(time1) if 1st time change is empty but 2nd time change is not empty:  Second Time Change without First Time Change
        '   IF NOT EMPTY(et2) when EMPTY(time1) if 1st time change is empty but 2nd event type is not empty:  Second Event Type Change without First Type Change

        ' IF NOT EMPTY(time2)
        '   IF EMPTY(et2):  Second Type Change blank and Time Change not blank
        ' ELSE
        '   If there is a 2nd change in event type, then there must be a 04-99 card with Contribution Code = 7 (Card D)
        '	IF NOT EMPTY(et2):  Event Type Change must be blank if Time Change is blank
        ' 
        ' Count the number of open-ended SF, U1, U2, U3, PO, MO, SE, RS events
        '
        ' Need to verify that the dates on Pre-1996 changes in event types are in the right order
        '
        ' IF the reporting must is 12 issue a warning if there is still an open-ended event
        ' 
        ' For 02-99 cards
        '   Cause Code 0000 can only be used with RS as ECC = 1, 6 or 7 (includes pre-1996 records)
        '
        '   If there is an ECC > 5 and the Even Card Number > 03 and the changes in event types on the 01 card empty then "Invalid D Event Card"
        '
        '   If ECC = 0, Contribution Code cannot be "0" for any event card
        ' 
        '   If ECC = 1 and even card number > 03 and the event type != PO or MO, "Contribution Code "1" for Event Type Change "PO" or "MO" only"
        '   
        '   Event Contribution Code 6 can only occur once per event
        '   Event Contribution Code 7 can only occur once per event
        '
        ' Validity of "Time:  Work Started"  and "Time:  Work Completed" depends on when a change in event occurs as triggered
        ' by the event contribution codes "6" and "7".  All additional components worked on during an event must be associated with the original
        ' event indicated by the event contribution code "1".  After the first change in event, work associated with the 
        ' original event cannot begin.  This work should be reported on Section D cards immediately following Section C:  Primary Cause of Event.
        ' Similarly all work associated with the first change in event must be associated with that event; therefore, should be reported
        ' on Section D cards immediately following card containing event contributino code "6".
        ' ERROR MESSAGES THAT RESULT:
        '	(1) Time:	Work Started is after first time change
        '	(2) Time:	Work Started is after second time change
        '	(3) Time:	Work Completed is before first time change
        '	(4) Time:	Work Completed is before second time change
        ' (1) if six = 0 and seven = 0 and not empty(change1)
        '		Event Contribution Code = 1
        '		if time1 > change1
        ' (2) if six > 0 and seven = 0 and not empty(change2)
        '		Event Contribution Code = 6
        '		if time1 > change2
        ' (3) if six > 0 and seven = 0 and not empty(change2)
        '		Event Contribution Code = 6
        '		if time2 < change1
        ' (4) if six > 0 and seven > 0 and not empty(change2)
        '		Event Contribution Code = 7
        '		if time2 < change2
        '
        ' Make sure Time Work Completed if closed by end of year

        ' A DE must have the same Available Capacities as the original PD or D4

        ' Overlapping full outages/RSs

        ' Valid Event Type Changes for Full Outages
        '  Change for same datetime and for midnight day before with midnight this morning combinations
        '  Check for 1 minute of in-service between events as WARNING

        ' Valid Event Type Changes for Deratings

        ' SFs can only occur from off-line, but make sure to include "carry over from prior year" 
        ' if first event of year and not 010100XX or 01010000, SF must follow an outage or RS.
        ' If the SF is the only Event, the start must be 010100XX or 01010000
        '
        ' GAC and NAC cannot be larger than the GDC and NDC for each month of the derating.  Be sure to deal with open-ended deratings thru rest of 
        ' performance data.  xAC is greater than xDC for Period mmYYYY



        dsErrors.Clear()
        'dsErrors = blConnect.GetMyErrors(dtSetup)
        dsErrors = GetUserErrors(myUser)
        Session("dsErrors") = dsErrors

        Dim strFilter As String
        strFilter = "UtilityUnitCode = '" & strCurrentUnit & "' AND ErrorSeverity = 'E'"
        'strFilter = "UtilityUnitCode = '" & strCurrentUnit & "'"
        Dim dv As New DataView
        With dv
            .Table = dsErrors.Tables("Errors")
            .AllowDelete = True
            .AllowEdit = True
            .AllowNew = True
            .RowFilter = strFilter
            .RowStateFilter = DataViewRowState.CurrentRows
        End With

        If dv.Count() > 0 Then
            blConnect.UpdateErrorCheckStatus(strCurrentUnit, "E")
        Else
            blConnect.UpdateErrorCheckStatus(strCurrentUnit, "O")
        End If

        dv.RowFilter = String.Empty

        DisplayCheckStatus(True)

        dgErrors.DataSource = dsErrors
        dgErrors.DataMember = "Errors"
        dgErrors.DataBind()

        SaveToSession()

        Return strErrorMsg

    End Function

#End Region

#Region " EventUpdate() As Boolean "

    Public Function EventUpdate() As Boolean

        RestoreFromSession()

        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        strCurrentUnit = Session("strCurrentUnit").ToString
        drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)

        Dim lHasErrors As Boolean
        Dim lNewRecord As Boolean

        lNewRecord = False
        lHasErrors = False

        'Dim decValue As Decimal
        'Dim intValue As Integer
        Dim intCounter As Integer

        Dim decNAC As Decimal

        Dim longDiff As Long

        'Dim drEvent1 As DataRow
        'Dim drEvent2 As DataRow

        Dim drvEvent1 As DataRowView

        Dim dtEndOfPrior As DateTime
        Dim dtStartOfNext As DateTime
        Dim intPriorNumber As Integer
        Dim intPriorCauseCode As Integer
        Dim strPriorEventType As String

        Dim dtEndOfPriorPD As DateTime
        Dim dtStartOfNextPD As DateTime
        Dim intPriorNumberPD As Integer
        Dim intPriorCauseCodePD As Integer
        Dim strPriorEventTypePD As String

        Dim dtEndOfPriorD4 As DateTime
        Dim dtStartOfNextD4 As DateTime
        Dim intPriorNumberD4 As Integer
        Dim intPriorCauseCodeD4 As Integer
        Dim strPriorEventTypeD4 As String

        dtEndOfPrior = Nothing
        dtStartOfNext = Nothing
        intPriorNumber = -1
        intPriorCauseCode = -1
        strPriorEventType = Nothing

        dtEndOfPriorPD = Nothing
        dtStartOfNextPD = Nothing
        intPriorNumberPD = -1
        intPriorCauseCodePD = -1
        strPriorEventTypePD = Nothing

        dtEndOfPriorD4 = Nothing
        dtStartOfNextD4 = Nothing
        intPriorNumberD4 = -1
        intPriorCauseCodeD4 = -1
        strPriorEventTypeD4 = Nothing

        Dim dtEndOfPriorMOPO As DateTime
        Dim dtStartOfNextSE As DateTime
        Dim intPriorMOPONumber As Integer
        Dim strPriorMOPOEventType As String

        dtEndOfPriorMOPO = Nothing
        dtStartOfNextSE = Nothing
        intPriorMOPONumber = -1
        strPriorMOPOEventType = Nothing

        Dim strRowErrorMsg As String = String.Empty
        Dim strErrors As String = lbUnits.SelectedItem.Text & vbCrLf & vbCrLf

        Dim vue As DataView
        vue = dsGridEvents.Tables("EventData01").DefaultView
        vue.RowFilter = "RevisionCard01 <> 'X'"

        Dim dtTemp As DateTime

        If dsGridEvents.HasErrors Then
            For intCounter = 0 To vue.Count - 1
                drvEvent1 = vue(intCounter)
                drvEvent1.Row.RowError = String.Empty
            Next
        End If

        For intCounter = 0 To vue.Count - 1
            drvEvent1 = vue(intCounter)
            If (drvEvent1.Item("EventType").ToString.Trim = "") Then
                strRowErrorMsg = "- Missing Event Type" & vbCrLf
                blConnect.LoadErrors(drSetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, _
                    "Missing Event Type", "E", drSetup.UnitShortName, GetFromDictionary)
                lHasErrors = True
                strErrors = "Event No. " & drvEvent1.Item("EventNumber").ToString & " Primary Cause of Event" & _
                 vbCrLf & vbCrLf & strRowErrorMsg.Replace("-", (vbTab + "-")) & vbCrLf
                drvEvent1.Row.RowError = strRowErrorMsg
            End If
        Next

        vue.Sort = "StartDateTime ASC, EndDateTime ASC"

        Dim PDEndDT As New SortedList(Of DateTime, Integer)
        Dim D4EndDT As New SortedList(Of DateTime, Integer)
        Dim dtJunk As DateTime = System.DateTime.Now
        Dim intJunk As Integer = 0

        vue.RowFilter = "EventType IN ('D4','PD') AND RevisionCard01 <> 'X'"

        For intCounter = 0 To vue.Count - 1
            drvEvent1 = vue(intCounter)
            If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then
                dtJunk = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                intJunk = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                Select Case drvEvent1.Item("EventType").ToString
                    Case "D4"
                        If Not D4EndDT.ContainsKey(dtJunk) Then
                            D4EndDT.Add(dtJunk, intJunk)
                        End If
                    Case "PD"
                        If Not PDEndDT.ContainsKey(dtJunk) Then
                            PDEndDT.Add(dtJunk, intJunk)
                        End If
                End Select
            End If
        Next

        '=============================================================================================================================================================
        ' Validate the Deratings

        Dim boolFoundMatch As Boolean = False

        vue.RowFilter = "EventType IN ('D1','D2','D3','D4','PD','DE','DM','DP') AND RevisionCard01 <> 'X'"

        For intCounter = 0 To vue.Count - 1

            drvEvent1 = vue(intCounter)

            strRowErrorMsg = ""

            boolFoundMatch = False

            If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DM") And _
                   Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                If D4EndDT.ContainsKey(Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) Then
                    boolFoundMatch = True
                Else
                    For Each de As KeyValuePair(Of DateTime, Integer) In D4EndDT
                        longDiff = DateDiff(DateInterval.Minute, de.Key, Convert.ToDateTime(drvEvent1.Item("StartDateTime")))
                        If de.Key <= Convert.ToDateTime(drvEvent1.Item("StartDateTime")) Then
                            dtEndOfPriorD4 = de.Key
                            intPriorNumberD4 = de.Value
                            strPriorEventTypeD4 = "D4"
                        Else
                            Exit For
                        End If
                        If longDiff = 0 Then
                            boolFoundMatch = True
                            Exit For
                        End If
                    Next
                End If
            End If

            If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DP") And _
                   Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And boolFoundMatch = False Then

                If PDEndDT.ContainsKey(Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) Then
                    boolFoundMatch = True
                Else
                    For Each de As KeyValuePair(Of DateTime, Integer) In PDEndDT
                        longDiff = DateDiff(DateInterval.Minute, de.Key, Convert.ToDateTime(drvEvent1.Item("StartDateTime")))
                        If de.Key <= Convert.ToDateTime(drvEvent1.Item("StartDateTime")) Then
                            dtEndOfPriorPD = de.Key
                            intPriorNumberPD = de.Value
                            strPriorEventTypePD = "PD"
                        Else
                            Exit For
                        End If
                        If longDiff = 0 Then
                            boolFoundMatch = True
                            Exit For
                        End If
                    Next
                End If
            End If

            If boolFoundMatch = False Then

                ' D4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                If drvEvent1.Item("EventType").ToString = "D4" And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    dtEndOfPriorD4 = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                    intPriorNumberD4 = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                    strPriorEventTypeD4 = drvEvent1.Item("EventType").ToString
                    intPriorCauseCodeD4 = Convert.ToInt32(drvEvent1.Item("CauseCode"))

                End If

                If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DM") And _
                    Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                    dtStartOfNextD4 = Convert.ToDateTime(drvEvent1.Item("StartDateTime"))

                    dtTemp = Convert.ToDateTime("01/01/" & drvEvent1.Item("Year").ToString & " 00:00")

                    If dtStartOfNextD4 > dtTemp Then

                        ' This deals with the possibility that the DE/DM/DP might be the first reported event for the year or 
                        ' carries over from last year

                        If DateTime.Compare(dtEndOfPriorD4, dtStartOfNextD4) <> 0 Then

                            longDiff = DateDiff(DateInterval.Minute, dtEndOfPriorD4, dtStartOfNextD4)

                            If longDiff = 1 Then
                                strRowErrorMsg += "- 1 minute gap between this event and end of " & strPriorEventType & " (" & intPriorNumber.ToString & ")" & vbCrLf
                            Else

                                ' these must be the same D4 -> DE or PD -> DE or DE -> DE
                                lHasErrors = True
                                If strPriorEventTypeD4 = String.Empty Or intPriorNumberD4 = -1 Then
                                    strRowErrorMsg += "- Start of derating extension must exactly match end of missing PD, D4, or DE/DM/DP" & vbCrLf
                                Else
                                    strRowErrorMsg += "- Start of derating extension must exactly match end of " & strPriorEventTypeD4 & " (" & intPriorNumberD4.ToString & ")" & vbCrLf
                                End If

                            End If

                        End If

                    End If

                    If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                        ' Allow a DE to DE transition

                        dtEndOfPriorD4 = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                        intPriorNumberD4 = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                        strPriorEventTypeD4 = drvEvent1.Item("EventType").ToString
                        intPriorCauseCodeD4 = Convert.ToInt32(drvEvent1.Item("CauseCode"))

                    End If

                Else
                    dtStartOfNextD4 = Nothing
                End If

                ' PD ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                If drvEvent1.Item("EventType").ToString = "PD" And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    dtEndOfPriorPD = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                    intPriorNumberPD = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                    strPriorEventTypePD = drvEvent1.Item("EventType").ToString
                    intPriorCauseCodePD = Convert.ToInt32(drvEvent1.Item("CauseCode"))

                End If

                If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DP") And _
                    Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                    dtStartOfNextPD = Convert.ToDateTime(drvEvent1.Item("StartDateTime"))

                    dtTemp = Convert.ToDateTime("01/01/" & drvEvent1.Item("Year").ToString & " 00:00")

                    If dtStartOfNextPD > dtTemp Then

                        ' This deals with the possibility that the DE/DM/DP might be the first reported event for the year or 
                        ' carries over from last year

                        If DateTime.Compare(dtEndOfPriorPD, dtStartOfNextPD) <> 0 Then

                            longDiff = DateDiff(DateInterval.Minute, dtEndOfPriorPD, dtStartOfNextPD)

                            If longDiff = 1 Then

                                strRowErrorMsg += "- 1 minute gap between this event and end of " & strPriorEventType & " (" & intPriorNumber.ToString & ")" & vbCrLf
                            Else

                                ' these must be the same D4 -> DE or PD -> DE or DE -> DE
                                lHasErrors = True
                                If strPriorEventTypePD = String.Empty Or intPriorNumberPD = -1 Then
                                    strRowErrorMsg += "- Start of derating extension must exactly match end of missing PD, D4, or DE/DM/DP" & vbCrLf
                                Else
                                    strRowErrorMsg += "- Start of derating extension must exactly match end of " & strPriorEventTypePD & " (" & intPriorNumberPD.ToString & ")" & vbCrLf
                                End If

                            End If

                        End If

                    End If

                    If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                        ' Allow a DE to DE transition

                        dtEndOfPriorPD = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                        intPriorNumberPD = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                        strPriorEventTypePD = drvEvent1.Item("EventType").ToString
                        intPriorCauseCodePD = Convert.ToInt32(drvEvent1.Item("CauseCode"))

                    End If

                Else
                    dtStartOfNextPD = Nothing
                End If

            End If

            drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.NetOnly)

            ' =================================================
            ' Either GAC or NAC or BOTH must be greater than 0

            ' Accepts an unsigned integer number - also matches empty strings
            '   ^\d*$
            Try

                If Convert.IsDBNull(drvEvent1.Item("NetAvailCapacity")) = False Then

                    If Text.RegularExpressions.Regex.IsMatch(drvEvent1.Item("NetAvailCapacity").ToString, "^\d+(\.\d+)?$") Then

                        If IsNumeric(drvEvent1.Item("NetAvailCapacity").ToString) Then
                            decNAC = Convert.ToDecimal(drvEvent1.Item("NetAvailCapacity").ToString)
                        Else
                            decNAC = 0
                        End If
                    Else
                        decNAC = 0
                    End If
                Else
                    decNAC = 0
                End If

                If decNAC <= 0 Then
                    lHasErrors = True
                    strRowErrorMsg += "- NAC must be greater than 0" & vbCrLf
                End If

            Catch
                lHasErrors = True
                strRowErrorMsg += "- NAC not an valid value" & vbCrLf
            End Try

            ' ======================================
            ' Checking the Event Date/time values

            If Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                If DateTime.Compare(Convert.ToDateTime(drvEvent1.Item("EndDateTime")), Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) <= 0 Then
                    lHasErrors = True
                    strRowErrorMsg += "- Event End date/time must be after Start date/time" & vbCrLf
                End If

                ' do not convert to UtcNow
                If DateTime.Compare(Convert.ToDateTime(System.DateTime.Now), Convert.ToDateTime(drvEvent1.Item("EndDateTime"))) < 0 Then
                    lHasErrors = True
                    strRowErrorMsg += "- End of Event date/time cannot be a future date/time" & vbCrLf
                End If

            ElseIf Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = True Then

                ' do not convert to UtcNow
                If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year < System.DateTime.Now.Year Then
                    lHasErrors = True
                    strRowErrorMsg += "- End of Event must be filled in" & vbCrLf
                Else
                    ' Open-ended derating
                    If Convert.ToInt16(strCurrentMonthNo) = 12 Then
                        lHasErrors = True
                        strRowErrorMsg += "- End of Event must be filled in at end of year" & vbCrLf
                    Else
                        ' Open-ended derating
                        strRowErrorMsg += "- Warning:  End of Event is still open" & vbCrLf
                    End If
                End If

            End If

            'If drSetup.InputDataRequirements = 0 Then
            '	If Convert.IsDBNull(drvEvent1.Item("VerbalDesc86")) = True Then
            '		lHasErrors = True
            '		strRowErrorMsg += "- Verbal Description cannot be blank" & vbCrLf
            '	Else
            '		If drvEvent1.Item("VerbalDesc86").ToString.Trim = String.Empty Then
            '			lHasErrors = True
            '			strRowErrorMsg += "- Verbal Description cannot be blank" & vbCrLf
            '		End If
            '	End If
            'Else
            '	drvEvent1.Item("VerbalDesc86") = "Not Required"
            'End If


            ' ======================================
            ' Checking for valid Cause Code

            Dim foundRows As DataRow()

            Try
                If Convert.IsDBNull(drvEvent1.Item("CauseCode")) = False Then

                    If IsNumeric(drvEvent1.Item("CauseCode")) Then

                        If Convert.ToInt16(drvEvent1.Item("CauseCode")) >= 0 Then

                            foundRows = dsCauseCodes.Tables(0).Select("CauseCode = " & drvEvent1.Item("CauseCode").ToString)

                            If foundRows.Length = 0 Then
                                lHasErrors = True
                                strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                            Else
                                foundRows = dsCauseCodes.Tables(0).Select("CauseCode = " & drvEvent1.Item("CauseCode").ToString & " AND " & drSetup.UnitType & " = True")

                                If foundRows.Length = 0 Then
                                    strRowErrorMsg += "- WARNING:  Invalid Cause Code for this Unit Type" & vbCrLf
                                End If
                            End If
                        Else
                            lHasErrors = True
                            strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                        End If

                    End If

                End If

            Catch
                lHasErrors = True
                strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
            End Try

            If strRowErrorMsg.Trim <> String.Empty Then
                strErrors += "Event No. " & drvEvent1.Item("EventNumber").ToString & " Primary Cause of Event" & _
                 vbCrLf & vbCrLf & strRowErrorMsg.Replace("-", (vbTab + "-")) & vbCrLf
            End If

            drvEvent1.Row.RowError = strRowErrorMsg
            'drvEvent1.Row.SetColumnError("GrossAvailCapacity", "This is the GAC error")

        Next


        '=============================================================================================================================================================
        ' Validate the Full Outage and RS events

        vue.RowFilter = "EventType IN ('U1','U2','U3','MO','PO','SE','SF','RS','PE','ME','IR','MB','RU') AND RevisionCard01 <> 'X'"

        If vue.Count > 0 Then

            drvEvent1 = vue(0)

            ' This handles all outage types; not MO, PO and SE only

            If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) Then
                ' The first full outage has an open ended End date/time
                dtEndOfPrior = System.DateTime.Now                ' do not convert to UtcNow
            Else
                dtEndOfPrior = Convert.ToDateTime("01/01/1982")
            End If

            intPriorNumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
            strPriorEventType = drvEvent1.Item("EventType").ToString
            intPriorCauseCode = Convert.ToInt32(drvEvent1.Item("CauseCode"))

            For intCounter = 0 To vue.Count - 1

                drvEvent1 = vue(intCounter)

                If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Month = 1 And Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Day = 1 And _
                   Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Hour = 0 And Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute <= 1 Then

                    ' Looking for a one-minute gap between the first event for the year and the last event for prior year

                    Dim strSQLSelectPriorYear As String
                    Dim dsChecking As DataSet

                    strSQLSelectPriorYear = "SELECT * FROM EventData01 WHERE UtilityUnitCode = '" & drvEvent1.Item("UtilityUnitCode").ToString & "'"

                    'If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                    '    strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"

                    'ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                    '    strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"

                    'End If

                    If blConnect.ThisProvider = "Oracle" Then

                        If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                            'strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"
                            strSQLSelectPriorYear += " AND EndDateTime = to_date ('" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00").ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                        ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                            'strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"
                            strSQLSelectPriorYear += " AND EndDateTime = to_date ('" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59").ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                        End If

                    Else

                        If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                            strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"

                        ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                            strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"

                        End If

                    End If

                    strSQLSelectPriorYear += " AND EventType IN ('U1','U2','U3','MO','PO','SE','SF','RS','PU','CO','PE','ME','IR','MB','RU') AND RevisionCard01 <> 'X'"

                    dsChecking = blConnect.GetADataset(strSQLSelectPriorYear, Nothing)

                    If Not IsNothing(dsChecking) Then
                        If dsChecking.Tables(0).Rows.Count > 0 Then
                            strRowErrorMsg += "1 minute gap between this event and in last event in " & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString
                            blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), Nothing, Convert.ToInt32(drvEvent1("EventNumber")), 1, _
                                "1 minute gap between this event and in last event in " & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If
                    End If

                End If

                strRowErrorMsg = ""

                Try

                    If Convert.IsDBNull(drvEvent1.Item("NetAvailCapacity")) = False Then

                        If Text.RegularExpressions.Regex.IsMatch(drvEvent1.Item("NetAvailCapacity").ToString, "^\d+(\.\d+)?$") Then

                            If IsNumeric(drvEvent1.Item("NetAvailCapacity").ToString) Then
                                decNAC = Convert.ToDecimal(drvEvent1.Item("NetAvailCapacity").ToString)
                                If decNAC <> 0 Then
                                    lHasErrors = True
                                    strRowErrorMsg += "- Net Available Capacity must be 0" & vbCrLf
                                End If
                            End If
                        Else
                            lHasErrors = True
                            strRowErrorMsg += "- Invalid Net Available Capacity" & vbCrLf
                        End If

                    End If

                Catch
                    lHasErrors = True
                    strRowErrorMsg += "- NAC not an valid value" & vbCrLf
                End Try

                ' ======================================
                ' Checking the Event Date/time values on a single Event 01 record

                If Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    If DateTime.Compare(Convert.ToDateTime(drvEvent1.Item("EndDateTime")), Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) <= 0 Then
                        lHasErrors = True
                        strRowErrorMsg += "- Event End date/time must be after Start date/time" & vbCrLf
                    End If

                    ' do not convert to UtcNow
                    If DateTime.Compare(Convert.ToDateTime(System.DateTime.Now), Convert.ToDateTime(drvEvent1.Item("EndDateTime"))) < 0 Then
                        lHasErrors = True
                        strRowErrorMsg += "- End of Event date/time cannot be a future date/time" & vbCrLf
                    End If

                ElseIf Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = True Then

                    ' do not convert to UtcNow
                    If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year < System.DateTime.Now.Year Then
                        lHasErrors = True
                        strRowErrorMsg += "- End of Event must be filled in" & vbCrLf
                    Else
                        If Convert.ToInt16(strCurrentMonthNo) = 12 Then
                            lHasErrors = True
                            strRowErrorMsg += "- End of Event must be filled in at end of year" & vbCrLf
                        Else
                            ' Open-ended full outage
                            strRowErrorMsg += "- Warning:  End of Event is still open" & vbCrLf
                        End If
                    End If

                End If



                'If drSetup.InputDataRequirements = 0 Then
                '	If Convert.IsDBNull(drvEvent1.Item("VerbalDesc86")) = True Then
                '		lHasErrors = True
                '		strRowErrorMsg += "- Verbal Description cannot be blank" & vbCrLf
                '	Else
                '		If drvEvent1.Item("VerbalDesc86").ToString.Trim = String.Empty Then
                '			lHasErrors = True
                '			strRowErrorMsg += "- Verbal Description cannot be blank" & vbCrLf
                '		End If
                '	End If
                'Else
                '	drvEvent1.Item("VerbalDesc86") = "Not Required"
                'End If

                ' ======================================
                ' Checking for valid Cause Code

                Dim foundRows As DataRow()


                Try
                    If Convert.IsDBNull(drvEvent1.Item("CauseCode")) = False Then

                        If IsNumeric(drvEvent1.Item("CauseCode")) Then

                            If Convert.ToInt16(drvEvent1.Item("CauseCode")) >= 0 Then

                                Select Case drvEvent1.Item("EventType").ToString
                                    Case "IR"
                                        If Convert.ToInt16(drvEvent1.Item("CauseCode")) <> 2 Then
                                            lHasErrors = True
                                            strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                                        End If
                                    Case "MB"
                                        If Convert.ToInt16(drvEvent1.Item("CauseCode")) <> 9991 Then
                                            lHasErrors = True
                                            strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                                        End If
                                    Case "RU"
                                        If Convert.ToInt16(drvEvent1.Item("CauseCode")) <> 9990 Then
                                            lHasErrors = True
                                            strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                                        End If
                                    Case Else

                                        foundRows = dsCauseCodes.Tables(0).Select("CauseCode = " & drvEvent1.Item("CauseCode").ToString)

                                        If foundRows.Length = 0 Then
                                            lHasErrors = True
                                            strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                                        Else
                                            foundRows = dsCauseCodes.Tables(0).Select("CauseCode = " & drvEvent1.Item("CauseCode").ToString & " AND " & drSetup.UnitType & " = True")

                                            If foundRows.Length = 0 Then
                                                strRowErrorMsg += "- WARNING:  Invalid Cause Code for this Unit Type" & vbCrLf
                                            End If
                                        End If

                                End Select
                            Else
                                lHasErrors = True
                                strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                            End If

                        End If

                    End If

                Catch
                    lHasErrors = True
                    strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                End Try

                longDiff = DateDiff(DateInterval.Second, dtEndOfPrior, Convert.ToDateTime(drvEvent1.Item("StartDateTime")))

                If vue.Count > 1 And longDiff <= 61 And _
                   (drvEvent1.Item("EventType").ToString <> "SE" And _
                   drvEvent1.Item("EventType").ToString <> "ME" And _
                   drvEvent1.Item("EventType").ToString <> "PE") Then

                    If longDiff = 0 Then

                        If Convert.IsDBNull(drvEvent1.Item("EventType")) = False Then
                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strRowErrorMsg += "- Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                       drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                            End If
                        End If

                    ElseIf longDiff = 60 Or longDiff = 61 Then

                        If Convert.IsDBNull(drvEvent1.Item("EventType")) = False Then
                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strRowErrorMsg += "- Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                            Else
                                strRowErrorMsg += "- 1 minute gap between this event and event # " & intPriorNumber.ToString & vbCrLf
                            End If
                        End If

                    ElseIf longDiff = 1 Then

                        If dtEndOfPrior.Hour = 23 And dtEndOfPrior.Minute = 59 And dtEndOfPrior.Second = 59 And _
                            Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Hour = 0 And _
                            Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 And _
                            Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Second = 0 And _
                            ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then

                            'lHasErrors = True
                            ''strRowErrorMsg += "- End date/time and Start datetime are not exactly the same: " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                            ''    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ")" & vbCrLf
                            'strRowErrorMsg += "- End date/time and Start date/time are not exactly the same: " & strPriorEventType & " (" & intPriorNumber.ToString & ") " & MF.GetFromDictionary(791).ToString & " " & _
                            '    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ")" & vbCrLf

                        ElseIf Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then

                            lHasErrors = True
                            strRowErrorMsg += "- Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf

                        End If

                    ElseIf longDiff > 1 Then

                        lHasErrors = True
                        strRowErrorMsg += "- Unexpected event transition error: " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                            drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf

                    ElseIf longDiff < 0 Then

                        lHasErrors = True
                        If intPriorNumber.ToString.Trim = drvEvent1.Item("EventNumber").ToString.Trim Then
                            strRowErrorMsg += "- Overlapping Events: " & _
                            drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is overlapped by another event and this is not allowed" & vbCrLf
                        Else
                            strRowErrorMsg += "- Overlapping Events: " & strPriorEventType & " (" & intPriorNumber.ToString & ") and " & _
                                   drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                        End If

                    End If

                End If

                'intPriorNumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                'strPriorEventType = drvEvent1.Item("EventType").ToString
                'intPriorCauseCode = Convert.ToInt32(drvEvent1.Item("CauseCode"))
                'If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) Then
                '    ' The first full outage has an open ended End date/time
                '    dtEndOfPrior = System.DateTime.Now     ' do not convert to UtcNow
                'Else
                '    dtEndOfPrior = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                'End If

                'If (drvEvent1.Item("EventType").ToString = "MO" Or drvEvent1.Item("EventType").ToString = "PO") And _
                ' Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                '    dtEndOfPriorMOPO = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                '    intPriorMOPONumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                '    strPriorMOPOEventType = drvEvent1.Item("EventType").ToString
                '    

                'End If

                If drvEvent1.Item("EventType").ToString = "SE" And Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                    dtStartOfNextSE = Convert.ToDateTime(drvEvent1.Item("StartDateTime"))

                    If dtStartOfNextSE > Convert.ToDateTime("01/01/" & drvEvent1.Item("Year").ToString & " 00:00") Then

                        ' This deals with the possibility that the SE might be the first reported event for the year or 
                        ' carries over from last year

                        longDiff = DateDiff(DateInterval.Minute, dtEndOfPriorMOPO, dtStartOfNextSE)
                        ' The use of longDiff allows for 1/31/2003 24:00 to be the same as 2/1/2003 00:00

                        If longDiff = 1 Then

                            lHasErrors = True
                            strRowErrorMsg += "- 1 minute gap between this event and end of " & strPriorMOPOEventType & " (" & intPriorMOPONumber.ToString & ")" & vbCrLf

                        ElseIf longDiff > 1 Then

                            lHasErrors = True

                            If intPriorMOPONumber > 0 Then
                                ' these must be the same MO -> SE or PO -> SE or SE -> SE
                                strRowErrorMsg += "- Start of SE must exactly match end of " & strPriorMOPOEventType & " (" & intPriorMOPONumber.ToString & ")" & vbCrLf
                            Else
                                strRowErrorMsg += "- Start of SE must exactly match end of a prior PO or MO event" & strPriorMOPOEventType & " (" & intPriorMOPONumber.ToString & ")" & vbCrLf
                            End If

                        ElseIf longDiff < 0 Then

                            lHasErrors = True

                            If intPriorNumber.ToString.Trim = drvEvent1.Item("EventNumber").ToString.Trim Then
                                strRowErrorMsg += "- Overlapping Events: " & _
                                drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is overlapped by another event and this is not allowed" & vbCrLf
                            Else
                                strRowErrorMsg += "- Overlapping Events: " & strPriorEventType & " (" & intPriorNumber.ToString & ") and " & _
                                       drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                            End If

                        Else

                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strRowErrorMsg += "- Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                            End If

                        End If

                    End If

                    If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                        dtEndOfPriorMOPO = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                        intPriorMOPONumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                        strPriorMOPOEventType = drvEvent1.Item("EventType").ToString

                    Else

                        dtEndOfPriorMOPO = Nothing
                        dtStartOfNextSE = Nothing
                        intPriorMOPONumber = -1
                        strPriorMOPOEventType = Nothing

                    End If

                Else
                    dtStartOfNextSE = Nothing
                End If

                intPriorNumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                strPriorEventType = drvEvent1.Item("EventType").ToString
                intPriorCauseCode = Convert.ToInt32(drvEvent1.Item("CauseCode"))

                If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) Then
                    ' The first full outage has an open ended End date/time
                    dtEndOfPrior = blConnect.EOM(System.DateTime.Now)        ' do not convert to UtcNow
                Else
                    dtEndOfPrior = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                End If

                If (drvEvent1.Item("EventType").ToString = "MO" Or drvEvent1.Item("EventType").ToString = "PO") And _
                       Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    dtEndOfPriorMOPO = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                    intPriorMOPONumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                    strPriorMOPOEventType = drvEvent1.Item("EventType").ToString

                End If

                Dim strCardNo As String
                Dim drvEventDetail As DataRowView
                Dim vueChild As DataView
                vueChild = drvEvent1.CreateChildView("AdditionalWork")

                Dim i As Integer
                For i = 0 To vueChild.Count - 1

                    drvEventDetail = vueChild(i)
                    strCardNo = drvEventDetail.Item("EvenCardNumber").ToString

                Next

                If strRowErrorMsg.Trim <> String.Empty Then
                    strErrors += "Event No. " & drvEvent1.Item("EventNumber").ToString & " Primary Cause of Event" & _
                     vbCrLf & vbCrLf & strRowErrorMsg.Replace("-", (vbTab + "-")) & vbCrLf
                End If

                drvEvent1.Row.RowError = strRowErrorMsg

            Next

        End If

        '=============================================================================================================================================================
        ' Validate the NC events

        vue.RowFilter = "EventType = 'NC' AND RevisionCard01 <> 'X'"

        For intCounter = 0 To vue.Count - 1

            drvEvent1 = vue(intCounter)

            strRowErrorMsg = ""

            Try

                If Convert.IsDBNull(drvEvent1.Item("NetAvailCapacity")) = False Then

                    If Text.RegularExpressions.Regex.IsMatch(drvEvent1.Item("NetAvailCapacity").ToString, "^\d+(\.\d+)?$") Then

                        If IsNumeric(drvEvent1.Item("NetAvailCapacity").ToString) Then
                            decNAC = Convert.ToDecimal(drvEvent1.Item("NetAvailCapacity").ToString)
                            If decNAC <> 0 Then
                                strRowErrorMsg += "- WARNING: Net Available Capacity should be blank" & vbCrLf & _
                                   "            NERC's edit checks will issue a warning" & vbCrLf
                            End If
                        End If
                    Else
                        lHasErrors = True
                        strRowErrorMsg += "- Invalid Net Available Capacity" & vbCrLf
                    End If

                End If

            Catch
                lHasErrors = True
                strRowErrorMsg += "- NAC not a valid value" & vbCrLf
            End Try

            If Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                If DateTime.Compare(Convert.ToDateTime(drvEvent1.Item("EndDateTime")), Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) <= 0 Then
                    lHasErrors = True
                    strRowErrorMsg += "- Event End date/time must be after Start date/time" & vbCrLf
                End If

                ' do not convert to UtcNow
                If DateTime.Compare(Convert.ToDateTime(System.DateTime.Now), Convert.ToDateTime(drvEvent1.Item("EndDateTime"))) < 0 Then
                    lHasErrors = True
                    strRowErrorMsg += "- End of Event date/time cannot be a future date/time" & vbCrLf
                End If

            ElseIf Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = True Then

                ' do not convert to UtcNow
                If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year < System.DateTime.Now.Year Then
                    lHasErrors = True
                    strRowErrorMsg += "- End of Event must be filled in" & vbCrLf
                Else
                    If Convert.ToInt16(strCurrentMonthNo) = 12 Then
                        lHasErrors = True
                        strRowErrorMsg += "- End of Event must be filled in at end of year" & vbCrLf
                    Else
                        ' Open-ended NC
                        strRowErrorMsg += "- Warning:  End of Event is still open" & vbCrLf
                    End If
                End If

            End If

            If strRowErrorMsg.Trim <> String.Empty Then
                strErrors += "Event No. " & drvEvent1.Item("EventNumber").ToString & " Primary Cause of Event" & _
                 vbCrLf & vbCrLf & strRowErrorMsg.Replace("-", (vbTab + "-")) & vbCrLf
            End If

            drvEvent1.Row.RowError = strRowErrorMsg

        Next

        tbEventErrorList.Text = strErrors
        tbEventErrorList.DataBind()

        vue.RowFilter = String.Empty

        If lHasErrors Then
            MultiPageEvent.ActiveViewIndex = 1
        Else

            MultiPageEvent.ActiveViewIndex = 0
            Dim strErrorsLoad As String
            strErrorsLoad = blConnect.FormLoadEventData01(dsGridEvents, myUser)

            If strErrorsLoad <> String.Empty Then
                lHasErrors = True
                tbEventErrorList.Text = strErrors & strErrorsLoad.Replace("has already been added by another user", _
                        "has already been added to the database") & vbCrLf
                tbEventErrorList.DataBind()
                MultiPageEvent.ActiveViewIndex = 1
            End If

        End If

        Return lHasErrors

    End Function

#End Region


#Region " UpdatePerfFields() "

    Public Sub UpdatePerfFields()

        Dim strUnit As String
        Dim intYear As Short
        Dim intLoc As Integer
        Dim strMonth As String = String.Empty
        Dim strMonthNo As String = String.Empty
        Dim strPeriod As String
        'Dim decValue As Decimal
        Dim intValue As Integer
        Dim dtFirstOfMonth As DateTime
        Dim douTime As Double

        Dim intMaxChars As Int16
        Dim strHoursFormat As String

        Try

            Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")

            strPeriod = lbUpdateMonth.SelectedItem.ToString().Trim()
        Catch
            lbUpdateMonth.SelectedIndex = 0
            strPeriod = lbUpdateMonth.SelectedItem.ToString().Trim()
        End Try

        If strPeriod.Trim <> String.Empty Then

            strCurrentPeriod = strPeriod

            intYear = CShort(strPeriod.Substring((strPeriod.Length - 4), 4))
            intLoc = strPeriod.IndexOf(",")

            If intLoc > 0 Then
                strMonth = strPeriod.Substring(0, intLoc).ToUpper()
            Else
                intLoc = strPeriod.IndexOf(" ")
                If intLoc > 0 Then
                    strMonth = strPeriod.Substring(0, intLoc).ToUpper()
                End If
            End If

            Select Case strMonth
                Case "JANUARY"
                    strMonthNo = "01"
                Case "FEBRUARY"
                    strMonthNo = "02"
                Case "MARCH"
                    strMonthNo = "03"
                Case "APRIL"
                    strMonthNo = "04"
                Case "MAY"
                    strMonthNo = "05"
                Case "JUNE"
                    strMonthNo = "06"
                Case "JULY"
                    strMonthNo = "07"
                Case "AUGUST"
                    strMonthNo = "08"
                Case "SEPTEMBER"
                    strMonthNo = "09"
                Case "OCTOBER"
                    strMonthNo = "10"
                Case "NOVEMBER"
                    strMonthNo = "11"
                Case "DECEMBER"
                    strMonthNo = "12"
            End Select

            strCurrentMonthNo = strMonthNo
            strUnit = lbUnits.SelectedValue.ToString

            If intCurrentYear <> intYear Or strCurrentUnit <> strUnit Or lForceRefresh Then

                If dtPerformance Is Nothing Then

                    dtPerformance = blConnect.GetPerformanceData(strUnit, intYear)

                Else
                    dtPerformance.Clear()
                    dtPerformance = blConnect.GetPerformanceData(strUnit, intYear)

                End If

                dtPerformance.AcceptChanges()

                intCurrentYear = intYear
                strCurrentUnit = strUnit

                BuildEventDataGrids()
                'btnSaveEvents.BackColor = System.Drawing.SystemColors.ControlDark

            Else

                If dtPerformance Is Nothing Then

                    dtPerformance = blConnect.GetPerformanceData(strUnit, intYear)

                    BuildEventDataGrids()

                Else
                    dgEvents.DataSource = dsGridEvents
                    dgEvents.DataMember = "EventData01"
                    dgEvents.DataBind()
                End If

            End If

            drPerformance = dtPerformance.FindByUtilityUnitCodeYearPeriod(strUnit, intYear, strMonthNo)
            drSetup = dtSetup.FindByUtilityUnitCode(strUnit)


            ' ==========================================
            ' Does the Performance record already exist?
            ' ==========================================

            If drPerformance Is Nothing Then

                ' =================================================================================
                ' Performance record does not exist in the dataset; therefore, this is a new record
                ' =================================================================================

                If drSetup Is Nothing Then

                    ' ============================================================
                    ' The unit is NOT in Setup; therefore, make everything Nothing
                    ' ============================================================

                    Session("ErrorMessage") = "Unit " + strUnit + " has been removed from Setup"

                Else

                    ' =========================================================
                    ' The unit is in SETUP; therefore, fill in the default info
                    ' =========================================================

                    If drSetup.IsGrossNetBothNull Then
                        drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.Both)
                    End If

                    tbNMCValue.Enabled = True
                    tbNDC.Enabled = True
                    tbNAG.Enabled = True

                    'If drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Then

                    '	tbGMCValue.Enabled = True
                    '	tbGDC.Enabled = True
                    '	tbGAG.Enabled = True

                    '	tbNMCValue.Text = String.Empty
                    '	tbNDC.Text = String.Empty
                    '	tbNAG.Text = String.Empty

                    '	tbNMCValue.Enabled = False
                    '	tbNDC.Enabled = False
                    '	tbNAG.Enabled = False

                    'ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Then

                    '	tbGMCValue.Text = String.Empty
                    '	tbGDC.Text = String.Empty
                    '	tbGAG.Text = String.Empty

                    '	tbGMCValue.Enabled = False
                    '	tbGDC.Enabled = False
                    '	tbGAG.Enabled = False

                    '	tbNMCValue.Enabled = True
                    '	tbNDC.Enabled = True
                    '	tbNAG.Enabled = True

                    'Else

                    '	tbGMCValue.Enabled = True
                    '	tbGDC.Enabled = True
                    '	tbGAG.Enabled = True

                    '	tbNMCValue.Enabled = True
                    '	tbNDC.Enabled = True
                    '	tbNAG.Enabled = True

                    'End If

                    'If drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Or drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                    '	If drSetup.IsMaxCapReadOnlyNull Then
                    '		tbGMCValue.ReadOnly = True
                    '	Else
                    '		tbGMCValue.ReadOnly = drSetup.MaxCapReadOnly
                    '	End If

                    '	If drSetup.IsGrossMaxCapacityNull Then
                    '		tbGMCValue.Text = Nothing
                    '	Else
                    '		tbGMCValue.Text = drSetup.GrossMaxCapacity.ToString
                    '	End If

                    '	If drSetup.IsGrossDepCapacityNull Then
                    '		tbGDC.Text = Nothing
                    '	Else
                    '		tbGDC.Text = drSetup.GrossDepCapacity.ToString
                    '	End If

                    '	If drSetup.IsDepCapReadOnlyNull Then
                    '		tbGDC.ReadOnly = False
                    '	Else
                    '		tbGDC.ReadOnly = drSetup.DepCapReadOnly
                    '	End If

                    '	tbGAG.Text = Nothing


                    'End If

                    'If drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Or drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                    '    If drSetup.IsMaxCapReadOnlyNull Then
                    '        tbNMCValue.ReadOnly = True
                    '    Else
                    '        tbNMCValue.ReadOnly = drSetup.MaxCapReadOnly
                    '    End If

                    If drSetup.IsNetMaxCapacityNull Then
                        tbNMCValue.Text = Nothing
                    Else
                        tbNMCValue.Text = Convert.ToDecimal(drSetup.NetMaxCapacity).ToString
                    End If

                    If drSetup.IsNetDepCapacityNull Then
                        tbNDC.Text = Nothing
                    Else
                        tbNDC.Text = Convert.ToDecimal(drSetup.NetDepCapacity).ToString
                    End If

                    '    If drSetup.IsDepCapReadOnlyNull Then
                    '        tbNDC.ReadOnly = False
                    '    Else
                    '        tbNDC.ReadOnly = drSetup.DepCapReadOnly
                    '    End If

                    tbNAG.Text = Nothing

                    'End If

                    tbAttStarts.Text = "0"
                    tbActStarts.Text = "0"

                    If drSetup.IsTypUnitLoadingNull Then
                        lbTULC.SelectedIndex = 0
                    Else
                        If drSetup.TypUnitLoading = 0 Or drSetup.TypUnitLoading > 7 Then
                            lbTULC.SelectedIndex = 0
                        Else
                            ' when saving this to Setup make sure to +1 the value
                            lbTULC.SelectedIndex = drSetup.TypUnitLoading - 1
                        End If

                    End If

                    'If drSetup.IsVerbalDescNull Then
                    '	tbVerbalDesc.Text = Nothing
                    'Else
                    '	tbVerbalDesc.Text = drSetup.VerbalDesc
                    'End If

                    ' --------------
                    ' Hours Data tab
                    ' --------------

                    intMaxChars = 6
                    strHoursFormat = "##0.00"

                    intValue = HoursInMonth(lbUpdateMonth.SelectedItem.ToString().Trim())

                    If Not IsDBNull(drSetup.CommercialDate) Then

                        If drSetup.CommercialDate.Month = Convert.ToInt16(strCurrentMonthNo) And _
                           drSetup.CommercialDate.Year = intCurrentYear Then

                            dtFirstOfMonth = DateTime.Parse(strCurrentMonthNo & "/01/" & intCurrentYear)

                            douTime = TimeBetween(dtFirstOfMonth, drSetup.CommercialDate, drSetup.DaylightSavingTime)

                            intValue -= Convert.ToInt16(douTime)

                        End If

                    End If

                    tbServiceHours.MaxLength = intMaxChars
                    tbRSHours.MaxLength = intMaxChars
                    tbPumpingHours.MaxLength = intMaxChars
                    tbSynCondHours.MaxLength = intMaxChars

                    tbServiceHours.Text = intValue.ToString(strHoursFormat)
                    tbRSHours.Text = 0.ToString(strHoursFormat)
                    tbPumpingHours.Text = 0.ToString(strHoursFormat)
                    tbSynCondHours.Text = 0.ToString(strHoursFormat)
                    lblAvailHoursValue.Text = intValue.ToString(strHoursFormat)

                    If drSetup.IsPumpingDataNull Then
                        tbPumpingHours.ReadOnly = False
                    Else
                        ' drSetup.PumpingData is TRUE when you want to enter the data
                        tbPumpingHours.ReadOnly = Not drSetup.PumpingData
                    End If

                    tbPumpingHours.Enabled = Not tbPumpingHours.ReadOnly

                    If drSetup.IsSynchCondDataNull Then
                        tbSynCondHours.ReadOnly = False
                    Else
                        ' drSetup.SynchCondData is TRUE when you want to enter the data
                        tbSynCondHours.ReadOnly = Not drSetup.SynchCondData
                    End If

                    tbSynCondHours.Enabled = Not tbSynCondHours.ReadOnly

                    tbPOHours.MaxLength = intMaxChars
                    tbFOHSFHours.MaxLength = intMaxChars
                    tbMOHours.MaxLength = intMaxChars
                    tbSEHours.MaxLength = intMaxChars
                    tbPeriodHours.MaxLength = intMaxChars
                    tbInactiveHours.MaxLength = intMaxChars

                    tbPOHours.Text = 0.ToString(strHoursFormat)
                    tbFOHSFHours.Text = 0.ToString(strHoursFormat)
                    tbMOHours.Text = 0.ToString(strHoursFormat)
                    tbSEHours.Text = 0.ToString(strHoursFormat)
                    lblUnavailHoursValue.Text = 0.ToString(strHoursFormat)

                    tbPeriodHours.Text = intValue.ToString(strHoursFormat)

                    tbInactiveHours.Text = 0.ToString(strHoursFormat)

                End If

            Else

                ' =============================================
                ' The Performance record exists -- this is EDIT
                ' =============================================

                ' ------------------
                ' Operating Data Tab
                ' ------------------

                tbNMCValue.Enabled = True
                tbNDC.Enabled = True
                tbNAG.Enabled = True

                'If drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Then

                '	tbGMCValue.Enabled = True
                '	tbGDC.Enabled = True
                '	tbGAG.Enabled = True

                '	tbNMCValue.Text = String.Empty
                '	tbNDC.Text = String.Empty
                '	tbNAG.Text = String.Empty

                '	tbNMCValue.Enabled = False
                '	tbNDC.Enabled = False
                '	tbNAG.Enabled = False

                'ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Then

                '	tbGMCValue.Text = String.Empty
                '	tbGDC.Text = String.Empty
                '	tbGAG.Text = String.Empty

                '	tbGMCValue.Enabled = False
                '	tbGDC.Enabled = False
                '	tbGAG.Enabled = False

                '	tbNMCValue.Enabled = True
                '	tbNDC.Enabled = True
                '	tbNAG.Enabled = True

                'Else

                '	tbGMCValue.Enabled = True
                '	tbGDC.Enabled = True
                '	tbGAG.Enabled = True


                '	tbNMCValue.Enabled = True
                '	tbNDC.Enabled = True
                '	tbNAG.Enabled = True

                'End If

                'If drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Or drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                '	If drSetup.IsMaxCapReadOnlyNull Then
                '		tbGMCValue.ReadOnly = True
                '	Else
                '		tbGMCValue.ReadOnly = drSetup.MaxCapReadOnly
                '	End If

                '	If drPerformance.IsGrossMaxCapNull Then
                '		tbGMCValue.Text = Nothing
                '	Else
                '		tbGMCValue.Text = drPerformance.GrossMaxCap.ToString
                '	End If

                '	If drSetup.IsDepCapReadOnlyNull Then
                '		tbGDC.ReadOnly = False
                '	Else
                '		tbGDC.ReadOnly = drSetup.DepCapReadOnly
                '	End If

                '	If drPerformance.IsGrossDepCapNull Then
                '		tbGDC.Text = Nothing
                '	Else
                '		tbGDC.Text = drPerformance.GrossDepCap.ToString
                '	End If

                '	If drPerformance.IsGrossGenNull Then
                '		tbGAG.Text = Nothing
                '	Else
                '		tbGAG.Text = drPerformance.GrossGen.ToString
                '	End If

                'End If

                If drPerformance.IsAttemptedStartsNull Then
                    tbAttStarts.Text = "0"
                Else
                    tbAttStarts.Text = drPerformance.AttemptedStarts.ToString
                End If

                If drPerformance.IsActualStartsNull Then
                    tbActStarts.Text = "0"
                Else
                    tbActStarts.Text = drPerformance.ActualStarts.ToString
                End If

                If drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Or drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                    'If drSetup.IsMaxCapReadOnlyNull Then
                    '    tbNMCValue.ReadOnly = True
                    'Else
                    '    tbNMCValue.ReadOnly = drSetup.MaxCapReadOnly
                    'End If

                    If drPerformance.IsNetMaxCapNull Then
                        tbNMCValue.Text = Nothing
                    Else
                        tbNMCValue.Text = Convert.ToDecimal(drPerformance.NetMaxCap).ToString
                    End If

                    'If drSetup.IsDepCapReadOnlyNull Then
                    '    tbNDC.ReadOnly = False
                    'Else
                    '    tbNDC.ReadOnly = drSetup.DepCapReadOnly
                    'End If

                    If drPerformance.IsNetDepCapNull Then
                        tbNDC.Text = Nothing
                    Else
                        tbNDC.Text = Convert.ToDecimal(drPerformance.NetDepCap).ToString
                    End If

                    If drPerformance.IsNetGenNull Then
                        tbNAG.Text = Nothing
                    Else
                        tbNAG.Text = Convert.ToDecimal(drPerformance.NetGen).ToString
                    End If

                End If

                If drPerformance.IsTypUnitLoadingNull Then
                    lbTULC.SelectedIndex = 0
                ElseIf drPerformance.TypUnitLoading = 0 Or drPerformance.TypUnitLoading > 7 Then
                    lbTULC.SelectedIndex = 0
                Else
                    Try
                        lbTULC.SelectedIndex = drPerformance.TypUnitLoading - 1
                    Catch ex As Exception
                        lbTULC.SelectedIndex = 0
                    End Try
                End If

                'If drPerformance.IsVerbalDescNull Then
                '	tbVerbalDesc.Text = Nothing
                'Else
                '	tbVerbalDesc.Text = drPerformance.VerbalDesc
                'End If

                ' ---------
                ' Hours Tab
                ' ---------

                intMaxChars = 6
                strHoursFormat = "##0.00"

                tbServiceHours.MaxLength = intMaxChars
                tbRSHours.MaxLength = intMaxChars
                tbPumpingHours.MaxLength = intMaxChars
                tbSynCondHours.MaxLength = intMaxChars

                tbServiceHours.Text = drPerformance.ServiceHours.ToString(strHoursFormat)
                tbRSHours.Text = drPerformance.RSHours.ToString(strHoursFormat)
                tbPumpingHours.Text = drPerformance.PumpingHours.ToString(strHoursFormat)
                tbSynCondHours.Text = drPerformance.SynchCondHours.ToString(strHoursFormat)
                lblAvailHoursValue.Text = (drPerformance.ServiceHours + drPerformance.RSHours + _
                  drPerformance.PumpingHours + drPerformance.SynchCondHours).ToString(strHoursFormat)

                If drSetup.IsPumpingDataNull Then
                    tbPumpingHours.ReadOnly = False
                Else
                    ' drSetup.PumpingData is TRUE when you want to enter the data
                    tbPumpingHours.ReadOnly = Not drSetup.PumpingData
                End If

                tbPumpingHours.Enabled = Not tbPumpingHours.ReadOnly

                If drSetup.IsSynchCondDataNull Then
                    tbSynCondHours.ReadOnly = False
                Else
                    ' drSetup.SynchCondData is TRUE when you want to enter the data
                    tbSynCondHours.ReadOnly = Not drSetup.SynchCondData
                End If

                tbSynCondHours.Enabled = Not tbSynCondHours.ReadOnly

                tbPOHours.MaxLength = intMaxChars
                tbFOHSFHours.MaxLength = intMaxChars
                tbMOHours.MaxLength = intMaxChars
                tbSEHours.MaxLength = intMaxChars
                tbPeriodHours.MaxLength = intMaxChars
                tbInactiveHours.MaxLength = intMaxChars

                tbPOHours.Text = drPerformance.PlannedOutageHours.ToString(strHoursFormat)
                tbFOHSFHours.Text = drPerformance.ForcedOutageHours.ToString(strHoursFormat)
                tbMOHours.Text = drPerformance.MaintOutageHours.ToString(strHoursFormat)
                tbSEHours.Text = drPerformance.ExtofSchedOutages.ToString(strHoursFormat)

                lblUnavailHoursValue.Text = (drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + _
                  drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages).ToString(strHoursFormat)

                tbPeriodHours.Text = drPerformance.PeriodHours.ToString(strHoursFormat)

                If drPerformance.IsInactiveHoursNull Then
                    drPerformance.InactiveHours = 0
                End If

                tbInactiveHours.Text = drPerformance.InactiveHours.ToString(strHoursFormat)

            End If

            ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            Dim intValue2 As Integer

            intValue2 = HoursInMonth(Me.lbUpdateMonth.SelectedItem.ToString().Trim())

            If Not IsDBNull(Me.drSetup.CommercialDate) Then

                If Me.drSetup.CommercialDate.Month = Convert.ToInt16(strCurrentMonthNo) And _
                   Me.drSetup.CommercialDate.Year = intCurrentYear Then

                    dtFirstOfMonth = DateTime.Parse(strCurrentMonthNo & "/01/" & intCurrentYear)

                    douTime = TimeBetween(dtFirstOfMonth, Me.drSetup.CommercialDate, Me.drSetup.DaylightSavingTime)

                    intValue2 -= Convert.ToInt16(douTime)

                End If

            End If

            ' --------------
            ' Hours Data tab
            ' --------------

            Dim intDecPlaces As Integer
            Dim strHoursFormat2 As String

            intDecPlaces = 2
            strHoursFormat2 = "000.00"

            If Me.tbPeriodHours.Text.Trim = String.Empty Then
                Me.tbPeriodHours.Text = 0.ToString(strHoursFormat2)
            End If

            Dim intTemp As Int32
            intTemp = Convert.ToInt32(Convert.ToDecimal(Me.tbPeriodHours.Text))

            Me.tbPeriodHours.ForeColor = System.Drawing.Color.Black

            If intTemp <= 0 Then
                Me.tbPeriodHours.Text = intValue2.ToString(strHoursFormat2)
            Else
                If intTemp < intValue2 Then

                    Dim sScript As String

                    sScript = "<script language='javascript'>"
                    sScript += "alert('You have changed the period hours from " & intValue2.ToString() & " to " & Me.tbPeriodHours.Text & "');"
                    sScript += "</script>"

                    Response.Write(sScript)

                    Me.tbPeriodHours.ForeColor = System.Drawing.Color.Red
                    Me.tbPeriodHours.DataBind()

                ElseIf Math.Round(intTemp, 2) > Math.Round(intValue2, 2) Then

                    Me.tbPeriodHours.Text = intValue2.ToString(strHoursFormat2)
                    Me.tbPeriodHours.DataBind()
                    drPerformance.PeriodHours = intValue2
                    drPerformance.EndEdit()
                    'Session.Item("drPerformance") = drPerformance

                End If

            End If

            ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        End If

    End Sub

#End Region

#Region " BuildEventDataGrids() "

    Public Sub BuildEventDataGrids()
        ' This is how to set the culture info
        'Dim ciGB As CultureInfo = New CultureInfo("en-GB")

        Dim strFormat As String = "MM/dd/yyyy"
        Dim dtResult As DateTime
        Dim strTemp As String

        dsGridEvents.Clear()

        dsGridEvents = blConnect.GetBothEventINTSetsGrid(lbUnits.SelectedValue.ToString, intCurrentYear)

        dsGridEvents.Tables("EventData01").Columns.Add("strStartDateTime")
        dsGridEvents.Tables("EventData01").Columns.Add("strEndDateTime")

        Dim dr As DataRow

        For Each dr In dsGridEvents.Tables("EventData01").Rows

            If Not IsDBNull(dr("StartDateTime")) Then

                dtResult = Convert.ToDateTime(dr("StartDateTime"))

                strTemp = dtResult.ToString("HH:mm:ss")

                If strTemp = "23:59:59" Then
                    dr("strStartDateTime") = dtResult.ToString(strFormat) & " 24:00"
                Else
                    dr("strStartDateTime") = dtResult.ToString(strFormat) & dtResult.ToString(" HH:mm")
                End If

            End If

            If Not IsDBNull(dr("EndDateTime")) Then

                dtResult = Convert.ToDateTime(dr("EndDateTime"))

                strTemp = dtResult.ToString("HH:mm:ss")

                If strTemp = "23:59:59" Then
                    dr("strEndDateTime") = dtResult.ToString(strFormat) & " 24:00"
                Else
                    dr("strEndDateTime") = dtResult.ToString(strFormat) & dtResult.ToString(" HH:mm")
                End If

            End If

        Next

        dgEvents.DataSource = dsGridEvents
        dgEvents.DataMember = "EventData01"
        dgEvents.DataBind()
        dgEvents.SelectedIndex = -1

    End Sub

#End Region

#Region " HoursInMonth(ByVal strPeriod As String) As Integer "

    Public Function HoursInMonth(ByVal strPeriod As String) As Integer

        ' strPeriod = lbUpdateMonth.SelectedItem.ToString().Trim()
        ' strPeriod is formatted:  January, 2003

        Dim intYear As Integer
        Dim intLoc As Integer
        Dim intValue As Integer
        Dim strMonth As String = String.Empty
        Dim strMonthNo As String = String.Empty

        If strPeriod.Trim <> String.Empty Then

            intYear = CShort(strPeriod.Substring((strPeriod.Length - 4), 4))
            intLoc = strPeriod.IndexOf(",")

            If intLoc > 0 Then
                strMonth = strPeriod.Substring(0, intLoc).ToUpper()
            Else
                intLoc = strPeriod.IndexOf(" ")
                If intLoc > 0 Then
                    strMonth = strPeriod.Substring(0, intLoc).ToUpper()
                End If
            End If

            Select Case strMonth
                Case "JANUARY"
                    strMonthNo = "01"
                Case "FEBRUARY"
                    strMonthNo = "02"
                Case "MARCH"
                    strMonthNo = "03"
                Case "APRIL"
                    strMonthNo = "04"
                Case "MAY"
                    strMonthNo = "05"
                Case "JUNE"
                    strMonthNo = "06"
                Case "JULY"
                    strMonthNo = "07"
                Case "AUGUST"
                    strMonthNo = "08"
                Case "SEPTEMBER"
                    strMonthNo = "09"
                Case "OCTOBER"
                    strMonthNo = "10"
                Case "NOVEMBER"
                    strMonthNo = "11"
                Case "DECEMBER"
                    strMonthNo = "12"
            End Select

            strCurrentMonthNo = strMonthNo

            intValue = System.DateTime.DaysInMonth(intYear, Convert.ToInt16(strMonthNo)) * 24

            Select Case drSetup.DaylightSavingTime

                Case 0

                    ' 0 = No daylight saving time
                    ' plus Japan and China

                Case 1

                    ' USA and Canada

                    If intYear < 2007 Then
                        If strMonthNo = "04" Then
                            intValue -= 1
                        ElseIf strMonthNo = "10" Then
                            intValue += 1
                        End If
                    Else
                        If strMonthNo = "03" Then
                            intValue -= 1
                        ElseIf strMonthNo = "11" Then
                            intValue += 1
                        End If
                    End If

                Case 2, 3, 4, 5

                    ' Europe and Russia
                    If strMonthNo = "03" Then
                        intValue -= 1
                    ElseIf strMonthNo = "10" Then
                        intValue += 1
                    End If

                Case 6, 7, 8

                    ' Australia, New Zealand, et al
                    If strMonthNo = "03" Then
                        intValue += 1
                    ElseIf strMonthNo = "10" Then
                        intValue -= 1
                    End If

                Case 9

                    ' Tonga
                    If strMonthNo = "01" Then
                        intValue += 1
                    ElseIf strMonthNo = "11" Then
                        intValue -= 1
                    End If

            End Select

        Else

            intValue = 0

        End If

        Return intValue

    End Function

#End Region

#Region " ResetValidations() "

    Public Sub ResetValidations()

        ' Sets up the number of decimal places for the Hours page

        tbServiceHours_KeyPress.Type = ValidationDataType.Double
        tbRSHours_KeyPress.Type = ValidationDataType.Double
        tbPumpingHours_KeyPress.Type = ValidationDataType.Double
        tbSynCondHours_KeyPress.Type = ValidationDataType.Double
        tbPOHours_KeyPress.Type = ValidationDataType.Double
        tbFOHSFHours_KeyPress.Type = ValidationDataType.Double
        tbMOHours_KeyPress.Type = ValidationDataType.Double
        tbSEHours_KeyPress.Type = ValidationDataType.Double
        tbPeriodHours_KeyPress.Type = ValidationDataType.Double
        tbInactiveHours_KeyPress.Type = ValidationDataType.Double

    End Sub

#End Region

#Region " CreateSession() "

    Private Sub CreateSession()

        Session.Add("AppName", AppName)
        Session.Add("connect", connect)
        'Session.Add("blConnect", blConnect)
        Session.Add("dtSetup", dtSetup)
        'Session.Add("drSetup", drSetup)
        Session.Add("dsCauseCodes", dsCauseCodes)
        Session.Add("dtPerformance", dtPerformance)
        'Session.Add("drPerformance", drPerformance)
        Session.Add("dsAllEvents", dsAllEvents)
        Session.Add("dsGridEvents", dsGridEvents)
        Session.Add("dsFailMech", dsFailMech)
        Session.Add("dsCauseCodeExt", dsCauseCodeExt)
        Session.Add("dsSavedVerbDesc", dsSavedVerbDesc)
        Session.Add("dsRS", dsRS)
        Session.Add("intCurrentYear", intCurrentYear)
        Session.Add("strCurrentUnit", strCurrentUnit)
        Session.Add("strCurrentPeriod", strCurrentPeriod)
        Session.Add("strCurrentMonthNo", strCurrentMonthNo)
        'Session.Add("intSECauseCode", intSECauseCode)
        'Session.Add("dtSEBeginning", dtSEBeginning)
        Session.Add("myUser", Context.User.Identity.Name)
        Session.Add("alPeriods", alPeriods)
        Session.Add("lbUnitsText", lbUnits.SelectedItem.Text)
        Session.Add("intYearLimit", intYearLimit)
        Session.Add("dsErrors", dsErrors)
        'Session.Add("GetFromDictionary", GetFromDictionary)
        Session.Add("strCulture", strCulture)

    End Sub

#End Region

#Region " HaveErrors() "

    Public Sub HaveErrors(ByVal strErrors As String)

        Dim strErrorPage As String

        strErrorPage = "ErrorMessage.aspx"

        Try
            Response.Redirect(strErrorPage, True)
        Catch ex As System.Exception
            strErrorPage = ex.ToString
        Finally
            strErrorPage = ""
        End Try

    End Sub

#End Region

    Protected Sub HelpIndex_Click(sender As Object, e As EventArgs) Handles HelpIndex.Click
        Dim sScript As String = String.Empty
        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('HTML/default.htm', 'child'); new_win.focus();"
        sScript += "</script>"
        Response.Write(sScript)
    End Sub


#Region "btnStartsFill_Click"
    Protected Sub btnStartsFill_Click(sender As Object, e As EventArgs) Handles btnStartsFill.Click
        Try

            RestoreFromSession()

            FillinStarts()

            SaveToSession()

            Dim dtChanges As DataTable = dtPerformance.GetChanges()

            If IsNothing(dtChanges) Then
                btnPerfUpdate.BackColor = btnEditRecord.BackColor
            Else
                If dtChanges.Rows.Count > 0 Then
                    btnPerfUpdate.BackColor = Drawing.Color.Red
                Else
                    btnPerfUpdate.BackColor = btnEditRecord.BackColor
                End If
            End If

            'Page.DataBind()
            Dim sScript As String

            sScript = "<script language='javascript'>"
            sScript += "alert('Starts Auto Fill completed');"
            sScript += "</script>"

            Response.Write(sScript)

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try
    End Sub
#End Region

#Region " RestoreFromSession() "

    Public Sub RestoreFromSession()

        AppName = Session("AppName").ToString
        connect = Session("connect").ToString
        'blConnect = CType(Session("blConnect"), BusinessLayer.BusinessLayer.GADSNGBusinessLayer)
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        dsCauseCodes = CType(Session("dsCauseCodes"), DataSet)
        dtPerformance = CType(Session("dtPerformance"), BusinessLayer.Performance.PerformanceDataDataTable)
        'drPerformance = CType(Session("drPerformance"), BusinessLayer.Performance.PerformanceDataRow)
        dsAllEvents = CType(Session("dsAllEvents"), AllEventDataISO)
        dsGridEvents = CType(Session("dsGridEvents"), DataSet)
        dsFailMech = CType(Session("dsFailMech"), DataSet)
        dsCauseCodeExt = CType(Session("dsCauseCodeExt"), DataSet)
        dsSavedVerbDesc = CType(Session("dsSavedVerbDesc"), DataSet)
        dsRS = CType(Session("dsRS"), BusinessLayer.dsBulkRS)
        intCurrentYear = CType(Session("intCurrentYear"), Integer)
        strCurrentUnit = Session("strCurrentUnit").ToString
        strCurrentPeriod = Session("strCurrentPeriod").ToString
        strCurrentMonthNo = Session("strCurrentMonthNo").ToString
        'intSECauseCode = CType(Session("intSECauseCode"), Integer)
        'dtSEBeginning = CType(Session("dtSEBeginning"), DateTime)
        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name
        alPeriods = CType(Session("alPeriods"), ArrayList)
        lbUnitsText = Session("lbUnitsText").ToString
        intYearLimit = CType(Session("intYearLimit"), Integer)
        dsErrors = CType(Session("dsErrors"), DataSet)
        'GetFromDictionary = CType(Session("GetFromDictionary"), HybridDictionary)
        strCulture = Session("strCulture").ToString

    End Sub

#End Region

#Region " SaveToSession() "

    Public Sub SaveToSession()

        Session.Item("AppName") = AppName
        Session.Item("connect") = connect
        'Session.Item("blConnect") = blConnect
        Session.Item("dtSetup") = dtSetup
        'Session.Item("drSetup") = drSetup
        Session.Item("dsCauseCodes") = dsCauseCodes
        Session.Item("dtPerformance") = dtPerformance
        'Session.Item("drPerformance") = drPerformance
        Session.Item("dsAllEvents") = dsAllEvents
        Session.Item("dsGridEvents") = dsGridEvents
        Session.Item("dsFailMech") = dsFailMech
        Session.Item("dsCauseCodeExt") = dsCauseCodeExt
        Session.Item("dsSavedVerbDesc") = dsSavedVerbDesc
        Session.Item("dsRS") = dsRS
        Session.Item("intCurrentYear") = intCurrentYear
        Session.Item("strCurrentUnit") = strCurrentUnit
        Session.Item("strCurrentPeriod") = strCurrentPeriod
        Session.Item("strCurrentMonthNo") = strCurrentMonthNo
        'Session.Item("intSECauseCode") = intSECauseCode
        'Session.Item("dtSEBeginning") = dtSEBeginning
        myUser = Context.User.Identity.Name
        Session.Item("myUser") = myUser
        Session.Item("alPeriods") = alPeriods
        Session.Item("lbUnitsText") = lbUnits.SelectedItem.Text
        Session.Item("intYearLimit") = intYearLimit
        Session.Item("dsErrors") = dsErrors
        'Session.Item("GetFromDictionary") = GetFromDictionary
        Session.Item("strCulture") = strCulture


    End Sub

#End Region

#Region " FillinStarts() "

    Public Sub FillinStarts()

        ' ===================================================
        ' Counting the number of Attempted and Actual Starts 


        ' |---- outage ---|--- outage ---|  |--- outage ---|  |--- outage ---|
        '    |-------------- month ---------------------------------|
        '                          start ^           start ^

        ' |--- outage ---|--- outage ---|--- SF ---|  |--- outage ---|
        '    |-------------- month ---------------------------------|
        '               ATTEMPTED Start ^    start ^

        ' |--- outage ---|--- outage ---|--- SF ---|--- outage ---|
        '    |-------------- month ---------------------------------|
        '               ATTEMPTED Start ^                   start ^

        Dim intCounter As Integer
        Dim intMonth As Integer
        'Dim drCurrent As DataRow
        Dim drvEvent1 As DataRowView
        'Dim intMyMonth As Integer
        'Dim strHoursMatchMsg As String
        Dim dtStart As DateTime
        Dim dtDate As DateTime
        Dim dtEnd As DateTime

        'Dim dtTemp As DateTime
        'Dim dtTemp2 As DateTime
        Dim strTemp As String

        Dim intAttemptedStarts(12) As Integer
        Dim intActualStarts(12) As Integer

        Dim intHotAttemptedStarts(12) As Integer
        Dim intWarmAttemptedStarts(12) As Integer
        Dim intColdAttemptedStarts(12) As Integer

        Dim intHotActualStarts(12) As Integer
        Dim intWarmActualStarts(12) As Integer
        Dim intColdActualStarts(12) As Integer
        Dim datetimePriorEvent As DateTime
        datetimePriorEvent = Convert.ToDateTime("11/22/1948")

        Dim longInterval As Long           ' time between events in minutes

        PerformanceUpdate()

        Dim vue As DataView

        vue = dsGridEvents.Tables("EventData01").DefaultView

        vue.Sort = "StartDateTime, EndDateTime"
        vue.RowFilter = "EventType IN ('U1', 'U2', 'U3', 'SF', 'PO', 'MO', 'SE', 'RS', 'ME', 'PE','IR','MB','RU') AND RevisionCard01 <> 'X'"

        Dim lLastEventWasSF As Boolean
        lLastEventWasSF = False
        Dim intLastSFEventNo As Integer

        If drSetup.IsPJMNull Then
            drSetup.PJM = False
        End If

        If drSetup.IsPJMStartsCountNull Then
            drSetup.PJMStartsCount = False
        End If

        For intCounter = 0 To vue.Count - 1

            drvEvent1 = vue(intCounter)

            If Not Convert.IsDBNull(drvEvent1("StartDateTime")) And Not Convert.IsDBNull(drvEvent1("EventType")) Then

                ' Have a valid Starting Date/time and Event Type

                dtStart = Convert.ToDateTime(drvEvent1("StartDateTime"))

                If Convert.IsDBNull(drvEvent1("ChangeDateTime1")) Or Convert.IsDBNull(drvEvent1("ChangeInEventType1")) Then

                    ' First Change in Event Date/time is null or the first Change in event type is null 
                    ' use End Date/time

                    If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                        ' This event is open ended use midnight on last day of year
                        If drPerformance Is Nothing Then
                            dtEnd = Convert.ToDateTime("12/31/" & Convert.ToString(intCurrentYear) & "  23:59:59")
                        Else
                            dtEnd = Convert.ToDateTime("12/31/" & drPerformance.Year.ToString & "  23:59:59")
                        End If

                    Else
                        dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                    End If

                    dtDate = dtStart

                    If Convert.IsDBNull(drvEvent1("CarryOverLastYear")) Then
                        drvEvent1("CarryOverLastYear") = False
                    End If

                    If Not Convert.ToBoolean(drvEvent1("CarryOverLastYear")) Then

                        If datetimePriorEvent > Convert.ToDateTime("11/22/1948") Then

                            ' There is a prior event

                            If drvEvent1("EventType").ToString = "SF" Then

                                intAttemptedStarts(dtStart.Month - 1) += 1
                                lLastEventWasSF = True
                                intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)

                            Else

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval >= 59 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then

                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                End If

                                lLastEventWasSF = False

                            End If

                        Else

                            ' There is no prior event -- this is the first one for the year

                            If drvEvent1("EventType").ToString = "SF" Then
                                intAttemptedStarts(dtStart.Month - 1) += 1
                                lLastEventWasSF = True
                                intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                            Else
                                lLastEventWasSF = False
                            End If

                        End If

                    End If

                    datetimePriorEvent = dtEnd

                Else

                    lLastEventWasSF = False

                    If drvEvent1("EventType").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(dtStart.Month - 1) += 1
                    End If

                    If drvEvent1("ChangeInEventType1").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(Convert.ToDateTime(drvEvent1("ChangeDateTime1")).Month - 1) += 1
                        lLastEventWasSF = True
                        intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                    End If

                    If drvEvent1("ChangeInEventType2").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(Convert.ToDateTime(drvEvent1("ChangeDateTime2")).Month - 1) += 1
                        lLastEventWasSF = True
                        intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                    Else
                        Select Case drvEvent1("EventType").ToString.ToUpper
                            Case "U1", "U2", "U3", "PO", "MO", "SE", "RS", "ME", "PE", "IR", "MB", "RU"
                                lLastEventWasSF = False
                        End Select
                    End If

                    If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                        ' This event is open ended use midnight on last day of year
                        If drPerformance Is Nothing Then
                            dtEnd = Convert.ToDateTime("12/31/" & Convert.ToString(intCurrentYear) & "  23:59:59")
                        Else
                            dtEnd = Convert.ToDateTime("12/31/" & drPerformance.Year.ToString & "  23:59:59")
                        End If

                    Else
                        dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                    End If

                    Select Case drvEvent1("EventType").ToString.ToUpper

                        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "ME", "PE", "IR", "MB", "RU"

                            dtDate = dtStart

                            If Not IsNothing(datetimePriorEvent) Then

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval >= 59 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                End If

                            End If

                    End Select

                    datetimePriorEvent = dtEnd

                End If

            End If

        Next

        If vue.Count > 0 Then

            ' No event records for counting purposes
            If drPerformance Is Nothing Then
                strTemp = Convert.ToString(intCurrentYear)
            Else
                strTemp = drPerformance.Year.ToString
            End If

            If System.DateTime.Compare(dtEnd, Convert.ToDateTime("12/31/" & strTemp & "  23:59:59")) <> 0 Then

                'If Not lLastEventWasSF Then
                intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                'End If

                intActualStarts(datetimePriorEvent.Month - 1) += 1

            End If

        End If

        Dim alSHMethod As New ArrayList
        alSHMethod = blConnect.BuildALSHMethod()
        Dim dtCheck As System.DateTime = System.DateTime.Now

        For Each drPerformance As BusinessLayer.Performance.PerformanceDataRow In dtPerformance

            ' This loop works for saved performance records including current month since a "Save" was performned when the Batch Check started

            intMonth = Convert.ToInt16(drPerformance.Period) - 1

            dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

            If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Then
                ' ServiceHourMethod = 0 : All Event data are reported

                If drPerformance.IsActualStartsNull Then
                    drPerformance.ActualStarts = 0
                End If

                If drPerformance.IsAttemptedStartsNull Then
                    drPerformance.AttemptedStarts = 0
                End If

                If drPerformance.ActualStarts <> intActualStarts(intMonth) Then
                    drPerformance.ActualStarts = Convert.ToInt16(intActualStarts(intMonth))
                End If

                If drPerformance.AttemptedStarts <> intAttemptedStarts(intMonth) Then
                    drPerformance.AttemptedStarts = Convert.ToInt16(intAttemptedStarts(intMonth))
                End If

            End If

        Next

        vue.RowFilter = String.Empty

        UpdatePerfFields()

    End Sub

#End Region


#Region " CreateHours() "

    Public Sub CreateHours()

        Dim intDecPlaces As Int16
        Dim strHoursFormat As String

        Dim dtStamp As DateTime
        dtStamp = System.DateTime.UtcNow

        intDecPlaces = 2
        strHoursFormat = "000.00"

        Dim dtDate As DateTime

        Dim dtStart As DateTime
        Dim dtStart1 As DateTime
        Dim dtStart2 As DateTime

        Dim dtEnd As DateTime
        Dim dtEnd1 As DateTime
        Dim dtEnd2 As DateTime

        Dim dtTemp As DateTime
        Dim dtTemp2 As DateTime

        Dim lFirstChange As Boolean
        Dim lSecondChange As Boolean

        Dim drCurrent As DataRow
        Dim intMonth As Integer
        Dim douTime As Double
        Dim strTemp As String
        Dim douPOH(12) As Double
        Dim douFOH(12) As Double
        Dim douMOH(12) As Double
        Dim douSEH(12) As Double

        Dim douSH As Double
        Dim douPumpHrs(12) As Double
        Dim douSynHrs(12) As Double
        Dim douRSH(12) As Double
        Dim douPH(12) As Double

        Dim douIH(12) As Double

        PerformanceUpdate()

        For Each drCurrent In dsGridEvents.Tables("EventData01").Rows

            lFirstChange = False
            lSecondChange = False
            douTime = 0.0

            If Not Convert.IsDBNull(drCurrent("StartDateTime")) And Not Convert.IsDBNull(drCurrent("EventType")) And drCurrent("RevisionCard01").ToString.ToUpper <> "X" Then

                ' Have a valid Starting Date/time and Event Type and not a deleted record

                dtStart = Convert.ToDateTime(drCurrent("StartDateTime"))
                strTemp = drCurrent("EventNumber").ToString

                If Convert.IsDBNull(drCurrent("ChangeDateTime1")) Or Convert.IsDBNull(drCurrent("ChangeInEventType1")) Then

                    ' First Change in Event Date/time is null or the first Change in event type is null 
                    ' use End Date/time

                    If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                        ' This event is open ended use today's date and time
                        dtEnd = System.DateTime.Now      ' do not convert to UtcNow

                        If dtEnd.Year <> dtStart.Year Then
                            dtEnd = DateTime.Parse("12/31/" & dtStart.Year.ToString & " 23:59:59")
                        End If

                    Else
                        dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                    End If

                    Select Case drCurrent("EventType").ToString.ToUpper

                        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "ME", "PE", "IR", "MB", "RU"

                            dtDate = dtStart

                            If dtStart.Month = dtEnd.Month Then
                                ' the event starts and stops in same month

                                intMonth = dtStart.Month - 1

                                douTime = TimeBetween(dtStart, dtEnd, drSetup.DaylightSavingTime)

                                Select Case drCurrent("EventType").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select

                            ElseIf dtEnd.Month = dtStart.Month + 1 Then

                                ' the event starts in month n and ends the next month

                                intMonth = dtStart.Month - 1
                                strTemp = dtStart.Month.ToString & "/" & System.DateTime.DaysInMonth(dtStart.Year, dtStart.Month).ToString & "/" & dtStart.Year.ToString & " 23:59:59"
                                dtTemp = System.DateTime.Parse(strTemp)
                                douTime = TimeBetween(dtStart, dtTemp, drSetup.DaylightSavingTime)

                                Select Case drCurrent("EventType").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select

                                intMonth = dtEnd.Month - 1
                                strTemp = dtEnd.Month.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                dtTemp = System.DateTime.Parse(strTemp)
                                douTime = TimeBetween(dtTemp, dtEnd, drSetup.DaylightSavingTime)

                                Select Case drCurrent("EventType").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select

                            Else
                                ' there are one or more months between the start and end of event

                                intMonth = dtStart.Month - 1
                                strTemp = dtStart.Month.ToString & "/" & System.DateTime.DaysInMonth(dtStart.Year, dtStart.Month).ToString & "/" & dtStart.Year.ToString & " 23:59:59"
                                dtTemp = System.DateTime.Parse(strTemp)
                                douTime = TimeBetween(dtStart, dtTemp, drSetup.DaylightSavingTime)

                                Select Case drCurrent("EventType").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select


                                For intMonth = dtStart.Month + 1 To dtEnd.Month - 1

                                    strTemp = intMonth.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                    dtTemp = System.DateTime.Parse(strTemp)
                                    strTemp = dtTemp.Month.ToString & "/" & System.DateTime.DaysInMonth(dtTemp.Year, dtTemp.Month).ToString & "/" & dtTemp.Year.ToString & " 23:59:59"
                                    dtTemp2 = System.DateTime.Parse(strTemp)

                                    douTime = TimeBetween(dtTemp, dtTemp2, drSetup.DaylightSavingTime)

                                    Select Case drCurrent("EventType").ToString.ToUpper
                                        Case "PO"
                                            douPOH(intMonth - 1) += douTime
                                        Case "U1", "U2", "U3", "SF"
                                            douFOH(intMonth - 1) += douTime
                                        Case "MO"
                                            douMOH(intMonth - 1) += douTime
                                        Case "SE", "PE", "ME"
                                            douSEH(intMonth - 1) += douTime
                                        Case "RS"
                                            douRSH(intMonth - 1) += douTime
                                        Case "PU"
                                            douPumpHrs(intMonth - 1) += douTime
                                        Case "CO"
                                            douSynHrs(intMonth - 1) += douTime
                                        Case "IR", "MB", "RU"
                                            douIH(intMonth - 1) += douTime
                                    End Select
                                Next

                                intMonth = dtEnd.Month - 1
                                strTemp = dtEnd.Month.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                dtTemp = System.DateTime.Parse(strTemp)
                                douTime = TimeBetween(dtTemp, dtEnd, drSetup.DaylightSavingTime)

                                Select Case drCurrent("EventType").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select

                            End If

                        Case Else
                            douTime = 0.0
                    End Select

                Else

                    lFirstChange = True
                    dtEnd1 = Convert.ToDateTime(drCurrent("ChangeDateTime1"))

                    Select Case drCurrent("EventType").ToString.ToUpper

                        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                            douTime = TimeBetween(dtStart, dtEnd1, drSetup.DaylightSavingTime)

                            Select Case drCurrent("EventType").ToString.ToUpper
                                Case "PO"
                                    douPOH(intMonth) += douTime
                                Case "U1", "U2", "U3", "SF"
                                    douFOH(intMonth) += douTime
                                Case "MO"
                                    douMOH(intMonth) += douTime
                                Case "SE", "PE", "ME"
                                    douSEH(intMonth) += douTime
                                Case "RS"
                                    douRSH(intMonth) += douTime
                                Case "PU"
                                    douPumpHrs(intMonth) += douTime
                                Case "CO"
                                    douSynHrs(intMonth) += douTime
                                Case "IR", "MB", "RU"
                                    douIH(intMonth) += douTime
                            End Select

                        Case Else
                            douTime = 0.0
                    End Select

                    dtStart1 = dtEnd1

                    If Convert.IsDBNull(drCurrent("ChangeDateTime2")) Or Convert.IsDBNull(drCurrent("ChangeInEventType2")) Then
                        ' Second change in event date/time is null or the second change in event type is null
                        ' use End Date/time

                        If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                            ' This event is open ended use today's date and time
                            dtEnd = System.DateTime.Now         ' do not convert to UtcNow
                            If dtEnd.Year <> dtStart1.Year Then
                                dtEnd = DateTime.Parse("12/31/" & dtStart1.Year.ToString & " 23:59:59")
                            End If

                        Else
                            dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                        End If

                        Select Case drCurrent("ChangeInEventType1").ToString.ToUpper

                            Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                                douTime = TimeBetween(dtStart1, dtEnd, drSetup.DaylightSavingTime)

                                Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select

                            Case Else
                                douTime = 0.0
                        End Select

                    Else

                        lSecondChange = True
                        dtEnd2 = Convert.ToDateTime(drCurrent("ChangeDateTime2"))

                        Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                            Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                                douTime = TimeBetween(dtStart1, dtEnd2, drSetup.DaylightSavingTime)

                                Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select

                            Case Else
                                douTime = 0.0
                        End Select

                        dtStart2 = dtEnd2

                        If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                            ' This event is open ended use today's date and time
                            dtEnd = System.DateTime.Now         ' do not convert to UtcNow
                            If dtEnd.Year <> dtStart2.Year Then
                                dtEnd = DateTime.Parse("12/31/" & dtStart2.Year.ToString & " 23:59:59")
                            End If

                        Else
                            dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                        End If

                        Select Case drCurrent("ChangeInEventType2").ToString.ToUpper
                            Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"
                                douTime = TimeBetween(dtStart2, dtEnd, drSetup.DaylightSavingTime)

                                Select Case drCurrent("ChangeInEventType2").ToString.ToUpper
                                    Case "PO"
                                        douPOH(intMonth) += douTime
                                    Case "U1", "U2", "U3", "SF"
                                        douFOH(intMonth) += douTime
                                    Case "MO"
                                        douMOH(intMonth) += douTime
                                    Case "SE", "PE", "ME"
                                        douSEH(intMonth) += douTime
                                    Case "RS"
                                        douRSH(intMonth) += douTime
                                    Case "PU"
                                        douPumpHrs(intMonth) += douTime
                                    Case "CO"
                                        douSynHrs(intMonth) += douTime
                                    Case "IR", "MB", "RU"
                                        douIH(intMonth) += douTime
                                End Select

                            Case Else
                                douTime = 0.0
                        End Select
                    End If

                End If

            End If

        Next

        Dim alSHMethod As New ArrayList
        alSHMethod = blConnect.BuildALSHMethod()
        Dim dtCheck As New System.DateTime

        For Each drPerformance As BusinessLayer.Performance.PerformanceDataRow In dtPerformance
            ' This loop works for saved performance records, but won't get a "new month" before the save button is pressed
            ' Therefore, I will have to use the next to get the "current month"

            dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

            If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Or _
               blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 1 Then

                intMonth = Convert.ToInt16(drPerformance.Period) - 1

                douPOH(intMonth) = System.Math.Round(douPOH(intMonth), 3)
                douMOH(intMonth) = System.Math.Round(douMOH(intMonth), 3)
                douFOH(intMonth) = System.Math.Round(douFOH(intMonth), 3)
                douSEH(intMonth) = System.Math.Round(douSEH(intMonth), 3)
                douRSH(intMonth) = System.Math.Round(douRSH(intMonth), 3)
                douPumpHrs(intMonth) = System.Math.Round(douPumpHrs(intMonth), 3)
                douSynHrs(intMonth) = System.Math.Round(douSynHrs(intMonth), 3)
                douIH(intMonth) = System.Math.Round(douIH(intMonth), 3)

                If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Then ' drSetup.ServiceHourMethod = 0 Then
                    ' ServiceHourMethod = 0 : All Event data are reported
                    drPerformance.RSHours = Convert.ToDecimal(System.Math.Round(douRSH(intMonth), intDecPlaces))
                End If

                drPerformance.PlannedOutageHours = Convert.ToDecimal(System.Math.Round(douPOH(intMonth), intDecPlaces))
                drPerformance.MaintOutageHours = Convert.ToDecimal(System.Math.Round(douMOH(intMonth), intDecPlaces))
                drPerformance.ForcedOutageHours = Convert.ToDecimal(System.Math.Round(douFOH(intMonth), intDecPlaces))
                drPerformance.ExtofSchedOutages = Convert.ToDecimal(System.Math.Round(douSEH(intMonth), intDecPlaces))
                drPerformance.InactiveHours = Convert.ToDecimal(System.Math.Round(douIH(intMonth), intDecPlaces))

                douSH = drPerformance.PeriodHours - drPerformance.RSHours - drPerformance.PlannedOutageHours _
                  - drPerformance.MaintOutageHours - drPerformance.ForcedOutageHours - drPerformance.ExtofSchedOutages

                If drSetup.IsPumpingDataNull Then
                    drSetup.PumpingData = False
                End If

                If douPumpHrs(intMonth) > 0 Or drPerformance.PumpingHours > 0 Then
                    drSetup.PumpingData = True
                End If

                'If dtCheck >= System.DateTime.Parse(COPUCUTOFFDATE) Then
                '    ' At ISO-NE these events are required after 10/01/2006
                '    drPerformance.PumpingHours = Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))
                '    drPerformance.SynchCondHours = Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))
                'End If

                If drSetup.PumpingData Then

                    If douPumpHrs(intMonth) > 0 Then
                        douSH -= Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))
                        drPerformance.PumpingHours = Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))
                    Else
                        douSH -= drPerformance.PumpingHours
                    End If

                End If

                If drSetup.IsSynchCondDataNull Then
                    drSetup.SynchCondData = False
                End If

                If douSynHrs(intMonth) > 0 Or drPerformance.SynchCondHours > 0 Then
                    drSetup.SynchCondData = True
                End If

                If drSetup.SynchCondData Then

                    If douSynHrs(intMonth) > 0 Then
                        douSH -= Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))
                        drPerformance.SynchCondHours = Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))
                    Else
                        douSH -= drPerformance.SynchCondHours
                    End If

                End If

                If drPerformance.IsInactiveHoursNull Then
                    drPerformance.InactiveHours = 0D
                End If

                douSH -= drPerformance.InactiveHours

                If douSH <= 0.01 And douSH > 0D And intMonth = 0 Then

                    douSH = 0D

                    If douRSH(intMonth) > 0 Then
                        douRSH(intMonth) += 0.01
                        If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Then
                            ' ServiceHourMethod = 0 : All Event data are reported
                            drPerformance.RSHours = Convert.ToDecimal(System.Math.Round(douRSH(intMonth), intDecPlaces))
                        End If
                    ElseIf douPOH(intMonth) > 0 Then
                        douPOH(intMonth) += 0.01
                        drPerformance.PlannedOutageHours = Convert.ToDecimal(System.Math.Round(douPOH(intMonth), intDecPlaces))
                    ElseIf douMOH(intMonth) > 0 Then
                        douMOH(intMonth) += 0.01
                        drPerformance.MaintOutageHours = Convert.ToDecimal(System.Math.Round(douMOH(intMonth), intDecPlaces))
                    ElseIf douFOH(intMonth) > 0 Then
                        douFOH(intMonth) += 0.01
                        drPerformance.ForcedOutageHours = Convert.ToDecimal(System.Math.Round(douFOH(intMonth), intDecPlaces))
                    ElseIf douSEH(intMonth) > 0 Then
                        douSEH(intMonth) += 0.01
                        drPerformance.ExtofSchedOutages = Convert.ToDecimal(System.Math.Round(douSEH(intMonth), intDecPlaces))
                    ElseIf douPumpHrs(intMonth) > 0 Then
                        douPumpHrs(intMonth) += 0.01
                        drPerformance.PumpingHours = Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))
                    ElseIf douSynHrs(intMonth) > 0 Then
                        douSynHrs(intMonth) += 0.01
                        drPerformance.SynchCondHours = Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))
                    ElseIf douIH(intMonth) > 0 Then
                        douIH(intMonth) += 0.01
                        drPerformance.InactiveHours = Convert.ToDecimal(System.Math.Round(douIH(intMonth), intDecPlaces))
                    End If

                End If

                drPerformance.ServiceHours = Convert.ToDecimal(System.Math.Round(douSH, intDecPlaces))

                drPerformance.RevMonthCard2 = dtStamp

                'drPerformance.TimeStamp = dtStamp

            End If

        Next

        intMonth = Convert.ToInt16(strCurrentMonthNo) - 1

        tbRSHours.Text = Convert.ToDecimal(System.Math.Round(douRSH(intMonth), intDecPlaces)).ToString(strHoursFormat)
        tbPOHours.Text = Convert.ToDecimal(System.Math.Round(douPOH(intMonth), intDecPlaces)).ToString(strHoursFormat)
        tbMOHours.Text = Convert.ToDecimal(System.Math.Round(douMOH(intMonth), intDecPlaces)).ToString(strHoursFormat)
        tbFOHSFHours.Text = Convert.ToDecimal(System.Math.Round(douFOH(intMonth), intDecPlaces)).ToString(strHoursFormat)
        tbSEHours.Text = Convert.ToDecimal(System.Math.Round(douSEH(intMonth), intDecPlaces)).ToString(strHoursFormat)

        douSH = Convert.ToDouble(tbPeriodHours.Text) - System.Math.Round(douRSH(intMonth), intDecPlaces) - System.Math.Round(douPOH(intMonth), intDecPlaces) _
          - System.Math.Round(douMOH(intMonth), intDecPlaces) - System.Math.Round(douFOH(intMonth), intDecPlaces) - System.Math.Round(douSEH(intMonth), intDecPlaces)

        If drSetup.IsPumpingDataNull Then
            drSetup.PumpingData = False
        End If

        If douPumpHrs(intMonth) > 0 Or drPerformance.PumpingHours > 0 Then
            drSetup.PumpingData = True
        End If

        If drSetup.PumpingData Then

            If douPumpHrs(intMonth) > 0 Then
                douSH -= Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))
                'drPerformance.PumpingHours = Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))
                tbPumpingHours.Text = Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces)).ToString(strHoursFormat)
            Else
                '    douSH -= drPerformance.PumpingHours
                douSH -= Convert.ToDouble(tbPumpingHours.Text)
            End If

        End If

        If drSetup.IsSynchCondDataNull Then
            drSetup.SynchCondData = False
        End If

        If douSynHrs(intMonth) > 0 Or drPerformance.SynchCondHours > 0 Then
            drSetup.SynchCondData = True
        End If

        If drSetup.SynchCondData Then

            If douSynHrs(intMonth) > 0 Then
                douSH -= Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))
                'drPerformance.SynchCondHours = Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))
                tbSynCondHours.Text = Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces)).ToString(strHoursFormat)
            Else
                '    douSH -= drPerformance.SynchCondHours
                douSH -= Convert.ToDouble(tbSynCondHours.Text)
            End If

        End If

        If drPerformance.IsInactiveHoursNull Then
            drPerformance.InactiveHours = 0D
        End If

        If douIH(intMonth) > 0 Then
            douSH -= Convert.ToDecimal(System.Math.Round(douIH(intMonth), intDecPlaces))
            'drPerformance.InactiveHours = Convert.ToDecimal(System.Math.Round(douIH(intMonth), intDecPlaces))
            tbInactiveHours.Text = Convert.ToDecimal(System.Math.Round(douIH(intMonth), intDecPlaces)).ToString(strHoursFormat)
        Else
            '    douSH -= drPerformance.InactiveHours
            douSH -= Convert.ToDouble(tbInactiveHours.Text)
        End If

        tbInactiveHours.Text = Convert.ToDecimal(System.Math.Round(douIH(intMonth), intDecPlaces)).ToString(strHoursFormat)

        tbServiceHours.Text = Convert.ToDecimal(System.Math.Round(douSH, intDecPlaces)).ToString(strHoursFormat)

        lblAvailHoursValue.Text = (System.Math.Round(douSH, intDecPlaces) + _
           System.Math.Round(douRSH(intMonth), intDecPlaces) + _
           System.Math.Round(Convert.ToDouble(tbPumpingHours.Text), intDecPlaces) + _
           System.Math.Round(Convert.ToDouble(tbSynCondHours.Text), intDecPlaces)).ToString(strHoursFormat)

        lblUnavailHoursValue.Text = (System.Math.Round(douPOH(intMonth), intDecPlaces) + _
          System.Math.Round(douMOH(intMonth), intDecPlaces) + _
          System.Math.Round(douFOH(intMonth), intDecPlaces) + _
          System.Math.Round(douSEH(intMonth), intDecPlaces)).ToString(strHoursFormat)

        lblAvailHoursValue.DataBind()
        lblUnavailHoursValue.DataBind()

        lblAvailHoursValue_Validating.Validate()
        lblUnavailHoursValue_Validating.Validate()

        UpdatePerfFields()

    End Sub

#End Region



#Region " UpdatelbMonths() "

    Public Sub UpdatelbMonths()

        Dim strTest As String
        'Dim iYear As Integer
        Dim iMonth As Integer
        'Dim intValue As Integer
        Dim strValue As String
        Dim dtDate As Date
        Dim dtStart As Date
        'Dim drSetup As System.Data.DataRowView

        If lbUnits.SelectedIndex <> -1 Then

            'strTest = lbUnits.SelectedIndex.ToString()

            Try

                strValue = Me.lbUnits.SelectedValue.ToString

                alPeriods = blConnect.GetPerfDataRange(strValue)

                ' alPeriods(0) is the Minimum Year
                ' alPeriods(1) is the Minimum Month 
                If Convert.ToInt32(alPeriods(0).ToString) < intYearLimit Then
                    ' In NGGroups is YearLimit -- the users are not allowed to edit below this year -- set by Administrator
                    dtStart = System.DateTime.Parse("01/01/" & intYearLimit.ToString())
                Else
                    dtStart = System.DateTime.Parse(alPeriods(1).ToString() & "/01/" & alPeriods(0).ToString())
                End If

                alPeriods.Clear()

                dtDate = dtStart

                Do While dtDate < DateAdd(DateInterval.Month, -1, Now)               ' do not convert to UtcNow
                    'strTest = dtDate.ToString("y")
                    alPeriods.Add(dtDate.ToString("y"))
                    dtDate = DateAdd(DateInterval.Month, 1, dtDate)
                Loop

                Me.lbUpdateMonth.Items.Clear()

                Me.lbUpdateMonth.Items.Add(alPeriods(alPeriods.Count - 1).ToString)

                For iMonth = (alPeriods.Count - 1) To 1 Step -1
                    Me.lbUpdateMonth.Items.Add(alPeriods(iMonth - 1).ToString)
                Next

                Me.lbUpdateMonth.DataBind()

                If strCurrentPeriod.Trim = String.Empty Then
                    Me.lbUpdateMonth.SelectedIndex = 0
                Else
                    'Me.lbUpdateMonth.SelectedValue = blConnect.GetCurrentMonth(strValue)
                    Me.lbUpdateMonth.SelectedValue = strCurrentPeriod.Trim
                    UpdatePerfFields()
                End If

                'strTest = Me.lbUpdateMonth.Text()

            Catch ex As InvalidCastException

                strTest = ex.ToString

            Catch ex As System.Exception

                ExceptionManager.Publish(ex)

            End Try

        End If

    End Sub

#End Region

#Region " PerformanceUpdate() "

    Public Function PerformanceUpdate() As String

        Dim lHasErrors As Boolean
        Dim lNewRecord As Boolean

        lNewRecord = False
        lHasErrors = False

        Dim decValue As Decimal
        'Dim intValue As Integer

        Dim intDecPlaces As Int16
        Dim strHoursFormat As String

        'Dim decNAG As Decimal
        'Dim decGAG As Decimal
        'Dim decNDC As Decimal
        'Dim decGDC As Decimal
        'Dim decGMC As Decimal
        'Dim decNMC As Decimal
        'Dim intActStarts As Integer
        'Dim intAttStarts As Integer

        If Not Session("dtSetup") Is Nothing Then
            dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        End If

        If Not Session("strCurrentUnit") Is Nothing Then
            strCurrentUnit = Session("strCurrentUnit").ToString
        End If

        drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)

        If drSetup.IsGrossNetBothNull Then
            drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.NetOnly)
        End If

        ' 2 decimal places
        intDecPlaces = 2
        strHoursFormat = "000.00"


        ' ======================================
        ' Checking to make sure that the hours add up

        If tbServiceHours.Text = String.Empty Then
            tbServiceHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbRSHours.Text = String.Empty Then
            tbRSHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbPumpingHours.Text = String.Empty Then
            tbPumpingHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbSynCondHours.Text = String.Empty Then
            tbSynCondHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbPOHours.Text = String.Empty Then
            tbPOHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbFOHSFHours.Text = String.Empty Then
            tbFOHSFHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbMOHours.Text = String.Empty Then
            tbMOHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbSEHours.Text = String.Empty Then
            tbSEHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbPeriodHours.Text = String.Empty Then
            tbPeriodHours.Text = 0.ToString(strHoursFormat)
        End If

        If tbInactiveHours.Text.Trim = String.Empty Then
            tbInactiveHours.Text = 0.ToString(strHoursFormat)
        End If

        decValue = Convert.ToDecimal(tbServiceHours.Text) + Convert.ToDecimal(tbRSHours.Text) + _
          Convert.ToDecimal(tbPumpingHours.Text) + Convert.ToDecimal(tbSynCondHours.Text)

        lblAvailHoursValue.Text = Math.Round(decValue, 2).ToString(strHoursFormat)

        decValue = Convert.ToDecimal(tbPOHours.Text) + Convert.ToDecimal(tbFOHSFHours.Text) + _
          Convert.ToDecimal(tbMOHours.Text) + Convert.ToDecimal(tbSEHours.Text)

        lblUnavailHoursValue.Text = Math.Round(decValue, 2).ToString(strHoursFormat)

        ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        Dim intValue2 As Integer
        Dim dtFirstOfMonth As DateTime
        Dim douTime As Double

        intValue2 = HoursInMonth(Me.lbUpdateMonth.SelectedItem.ToString().Trim())

        If Not IsDBNull(Me.drSetup.CommercialDate) Then

            If Me.drSetup.CommercialDate.Month = Convert.ToInt16(strCurrentMonthNo) And _
               Me.drSetup.CommercialDate.Year = intCurrentYear Then

                dtFirstOfMonth = DateTime.Parse(strCurrentMonthNo & "/01/" & intCurrentYear)

                douTime = TimeBetween(dtFirstOfMonth, Me.drSetup.CommercialDate, Me.drSetup.DaylightSavingTime)

                intValue2 -= Convert.ToInt16(douTime)

            End If

        End If

        If Me.tbPeriodHours.Text.Trim = String.Empty Then
            Me.tbPeriodHours.Text = 0.ToString(strHoursFormat)
        End If

        Dim intTemp As Int32
        intTemp = Convert.ToInt32(Convert.ToDecimal(Me.tbPeriodHours.Text))


        ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


        'If Not Page.IsValid Then
        '    Return String.Empty
        'End If

        drPerformance = dtPerformance.FindByUtilityUnitCodeYearPeriod(strCurrentUnit, Convert.ToInt16(intCurrentYear), strCurrentMonthNo)

        ' ==========================================
        ' Does the Performance record already exist?
        ' ==========================================

        Dim dtTemp As DateTime
        dtTemp = System.DateTime.UtcNow

        If drPerformance Is Nothing Then

            drPerformance = dtPerformance.NewPerformanceDataRow
            lNewRecord = True

            drPerformance.TimeStamp = dtTemp

            drPerformance.UtilityUnitCode = strCurrentUnit
            drPerformance.Year = Convert.ToInt16(intCurrentYear)
            drPerformance.Period = strCurrentMonthNo

            drPerformance.RevisionCard1 = "0"
            drPerformance.RevisionCard2 = "0"
            drPerformance.RevisionCard3 = "0"
            drPerformance.RevisionCard4 = "0"

            drPerformance.ServiceHours = 0
            drPerformance.RSHours = 0
            drPerformance.PumpingHours = 0
            drPerformance.SynchCondHours = 0
            drPerformance.PlannedOutageHours = 0
            drPerformance.ForcedOutageHours = 0
            drPerformance.MaintOutageHours = 0
            drPerformance.ExtofSchedOutages = 0
            drPerformance.PeriodHours = 0
            drPerformance.InactiveHours = 0

            drPerformance.PriFuelCode = ""
            drPerformance.PriBtus = 0
            drPerformance.SecBtus = 0
            drPerformance.TerBtus = 0
            drPerformance.QuaBtus = 0

            drPerformance.RevMonthCard1 = dtTemp
            drPerformance.RevMonthCard2 = dtTemp
            drPerformance.RevMonthCard3 = dtTemp
            drPerformance.RevMonthCard4 = dtTemp


            If intTemp <= 0 Then
                Me.tbPeriodHours.Text = intValue2.ToString(strHoursFormat)
            Else
                If Math.Round(intTemp, 2) > Math.Round(intValue2, 2) Then
                    Me.tbPeriodHours.Text = intValue2.ToString(strHoursFormat)
                    Me.tbPeriodHours.DataBind()
                    drPerformance.PeriodHours = intValue2
                Else
                    drPerformance.PeriodHours = intTemp
                End If

            End If

        Else

            lNewRecord = False
            drPerformance.BeginEdit()
            drPerformance.TimeStamp = dtTemp

            If intTemp <= 0 Then
                Me.tbPeriodHours.Text = intValue2.ToString(strHoursFormat)
            Else
                If Math.Round(intTemp, 2) > Math.Round(intValue2, 2) Then
                    Me.tbPeriodHours.Text = intValue2.ToString(strHoursFormat)
                    Me.tbPeriodHours.DataBind()
                    drPerformance.PeriodHours = intValue2
                Else
                    drPerformance.PeriodHours = intTemp
                End If

            End If

        End If

        ' The following was necessary because the code was resetting the 4 fields that are to be updated in Setup
        ' to what was in there originally and was not updating either Setup or the textbox

        'Dim strGDC As String = String.Empty
        Dim strNDC As String
        'Dim byteTULC As Byte
        Dim strTULCVerbDesc As String

        'strGDC = tbGDC.Text
        strNDC = tbNDC.Text
        'byteTULC = Convert.ToByte(lbTULC.SelectedIndex + 1)
        strTULCVerbDesc = "NA"

        If Not tbAttStarts.Text Is Nothing And tbAttStarts.Text <> String.Empty Then
            drPerformance.AttemptedStarts = Convert.ToInt16(tbAttStarts.Text)
        End If

        If Not tbActStarts.Text Is Nothing And tbActStarts.Text <> String.Empty Then
            drPerformance.ActualStarts = Convert.ToInt16(tbActStarts.Text)
        End If

        If drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Or drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

            If Not tbNMCValue.Text Is Nothing And tbNMCValue.Text <> String.Empty Then
                drPerformance.NetMaxCap = Convert.ToDecimal(tbNMCValue.Text)
            End If

            If Not strNDC Is Nothing And strNDC <> String.Empty Then
                drPerformance.NetDepCap = Convert.ToDecimal(strNDC)
            End If

            If Not tbNAG.Text Is Nothing And tbNAG.Text <> String.Empty Then
                drPerformance.NetGen = Convert.ToDecimal(tbNAG.Text)
            End If

        End If

        Dim intTest As Integer
        intTest = lbTULC.SelectedIndex + 1
        drPerformance.TypUnitLoading = Convert.ToByte(intTest)

        'drPerformance.TypUnitLoading = byteTULC
        'drSetup.TypUnitLoading = byteTULC

        'If Not tbVerbalDesc.Text Is Nothing Then
        '    drPerformance.VerbalDesc = tbVerbalDesc.Text
        '    drSetup.VerbalDesc = tbVerbalDesc.Text
        'End If

        If Not strTULCVerbDesc Is Nothing Then
            drPerformance.VerbalDesc = strTULCVerbDesc
        End If


        ' ---------
        ' Hours Tab
        ' ---------

        If Not tbServiceHours.Text Is Nothing And tbServiceHours.Text <> String.Empty Then
            drPerformance.ServiceHours = Convert.ToDecimal(tbServiceHours.Text)
        End If

        If Not tbRSHours.Text Is Nothing And tbRSHours.Text <> String.Empty Then
            drPerformance.RSHours = Convert.ToDecimal(tbRSHours.Text)
        End If

        If Not tbPumpingHours.Text Is Nothing And tbPumpingHours.Text <> String.Empty Then
            drPerformance.PumpingHours = Convert.ToDecimal(tbPumpingHours.Text)
        End If

        If Not tbSynCondHours.Text Is Nothing And tbSynCondHours.Text <> String.Empty Then
            drPerformance.SynchCondHours = Convert.ToDecimal(tbSynCondHours.Text)
        End If

        If Not tbPOHours.Text Is Nothing And tbPOHours.Text <> String.Empty Then
            drPerformance.PlannedOutageHours = Convert.ToDecimal(tbPOHours.Text)
        End If

        If Not tbFOHSFHours.Text Is Nothing And tbFOHSFHours.Text <> String.Empty Then
            drPerformance.ForcedOutageHours = Convert.ToDecimal(tbFOHSFHours.Text)
        End If

        If Not tbMOHours.Text Is Nothing And tbMOHours.Text <> String.Empty Then
            drPerformance.MaintOutageHours = Convert.ToDecimal(tbMOHours.Text)
        End If

        If Not tbSEHours.Text Is Nothing And tbSEHours.Text <> String.Empty Then
            drPerformance.ExtofSchedOutages = Convert.ToDecimal(tbSEHours.Text)
        End If

        If Not tbPeriodHours.Text Is Nothing And tbPeriodHours.Text <> String.Empty Then
            drPerformance.PeriodHours = Convert.ToDecimal(tbPeriodHours.Text)
        End If

        If Not tbInactiveHours.Text Is Nothing And tbInactiveHours.Text <> String.Empty Then
            drPerformance.InactiveHours = Convert.ToDecimal(tbInactiveHours.Text)
        End If

        If lNewRecord Then

            Try
                dtPerformance.AddPerformanceDataRow(drPerformance)
            Catch ex As System.Exception
                Dim strTest As String
                strTest = ex.ToString
            End Try

        Else

            drPerformance.EndEdit()

        End If

        Dim strErrors As String = String.Empty

        If drPerformance.ActualStarts > drPerformance.AttemptedStarts Then
            strErrors += drPerformance.Period & "/" & drPerformance.Year.ToString & " ERROR: Actual Starts must be <= Attempted Starts" & vbCrLf
        End If

        If Not drPerformance.IsNetMaxCapNull And Not drPerformance.IsNetGenNull And drPerformance.ServiceHours > 0 Then
            If drPerformance.NetMaxCap > 0 Then
                Dim nof As Decimal = Math.Round(((drPerformance.NetGen * 100) / (drPerformance.NetMaxCap * drPerformance.ServiceHours)), 0)
                If nof > 100.0 Then
                    'strErrors += drPerformance.Period & "/" & drPerformance.Year.ToString & " WARNING: NOF is calculated as " & nof.ToString() & "% - should be <= 100%" & vbCrLf
                    Dim sScript As String = "<script language='javascript'>"
                    sScript += "alert('" & drPerformance.Period & "//" & drPerformance.Year.ToString & " WARNING: NOF is calculated as " & nof.ToString() & "% - should be <= 100%');"
                    sScript += "</script>"

                    Try
                        Response.Write(sScript)
                    Catch ex As System.Exception
                        sScript = ex.ToString
                    End Try
                End If
            End If
        End If

        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name

        If strErrors = String.Empty Then
            strErrors = blConnect.FormLoadPerformance(dtPerformance, myUser, "WEB")
        End If

        Page.Validate()

        If strErrors.Trim = String.Empty Then

            If Page.IsValid Then
                dtPerformance.AcceptChanges()
            End If

            'If Not strGDC Is Nothing And strGDC <> String.Empty Then
            '    drSetup.GrossDepCapacity = Convert.ToDecimal(strGDC)
            'Else
            '    drSetup.SetGrossDepCapacityNull()
            'End If
            drSetup.SetGrossDepCapacityNull()

            If Not strNDC Is Nothing And strNDC <> String.Empty Then
                drSetup.NetDepCapacity = Convert.ToDecimal(strNDC)
            Else
                drSetup.SetNetDepCapacityNull()
            End If

            drSetup.TypUnitLoading = Convert.ToByte(lbTULC.SelectedIndex + 1)

            If Not strTULCVerbDesc Is Nothing Then
                drSetup.VerbalDesc = strTULCVerbDesc
            Else
                drSetup.VerbalDesc = " ".PadRight(25)
            End If

        End If

        Return strErrors.Trim

    End Function

#End Region

#Region " btnPerfUpdate_Click "

    Protected Sub btnPerfUpdate_Click(sender As Object, e As EventArgs) Handles btnPerfUpdate.Click

        Dim strErrors As String
        strErrors = String.Empty

        Dim sScript As String

        Try

            RestoreFromSession()

            ResetValidations()

            strErrors = PerformanceUpdate()

            Dim dtChanges As DataTable = dtPerformance.GetChanges()

            If IsNothing(dtChanges) Then
                btnPerfUpdate.BackColor = btnEditRecord.BackColor
            Else
                btnPerfUpdate.BackColor = Drawing.Color.Red
            End If

            If strErrors <> String.Empty Then

                btnPerfUpdate.BackColor = Drawing.Color.Red

                sScript = "<script language='javascript'>"
                sScript += "alert('" & strErrors & "');"
                sScript += "</script>"

                Try
                    Response.Write(sScript)
                Catch ex As System.Exception
                    sScript = ex.ToString
                End Try

                Exit Sub

            End If

            DisplayCheckStatus(True)

            SaveToSession()

            'Page.DataBind()

            sScript = "<script language='javascript'>"
            sScript += "alert('Submit to Performance Data master database completed');"
            sScript += "</script>"

            Try
                Response.Write(sScript)
            Catch ex As System.Exception
                sScript = ex.ToString
            End Try

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try

    End Sub

#End Region

#Region " btnPerfCancel_Click "

    Protected Sub btnPerfCancel_Click(sender As Object, e As EventArgs) Handles btnPerfCancel.Click
        Try

            RestoreFromSession()

            UpdatePerfFields()

            DisplayCheckStatus(True)

            SaveToSession()

            'Page.DataBind()

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try
    End Sub

#End Region

#Region " lblAvailHoursValue_CustomValidator "

    Public Sub lblAvailHoursValue_CustomValidator(ByVal s As Object, ByVal e As ServerValidateEventArgs)

        Dim decValue As Decimal

        If drPerformance Is Nothing Then
            e.IsValid = True
        Else

            decValue = Convert.ToDecimal(tbServiceHours.Text) + Convert.ToDecimal(tbRSHours.Text) + _
              Convert.ToDecimal(tbPumpingHours.Text) + Convert.ToDecimal(tbSynCondHours.Text) + _
              Convert.ToDecimal(tbPOHours.Text) + Convert.ToDecimal(tbFOHSFHours.Text) + _
              Convert.ToDecimal(tbMOHours.Text) + Convert.ToDecimal(tbSEHours.Text) + _
              Convert.ToDecimal(tbInactiveHours.Text)


            If Math.Round(decValue, 2) <> Math.Round(Convert.ToDecimal(tbPeriodHours.Text), 2) Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If

            decValue = drPerformance.ServiceHours + drPerformance.RSHours + _
            drPerformance.PumpingHours + drPerformance.SynchCondHours + _
            drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + _
            drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages

            If Math.Round(decValue, 2) <> Math.Round(drPerformance.PeriodHours, 2) Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If

        End If

    End Sub

#End Region

#Region " lblUnavailHoursValue_CustomValidator "

    Public Sub lblUnavailHoursValue_CustomValidator(ByVal s As Object, ByVal e As ServerValidateEventArgs)

        Dim decValue As Decimal

        If drPerformance Is Nothing Then
            e.IsValid = True
        Else

            decValue = Convert.ToDecimal(tbServiceHours.Text) + Convert.ToDecimal(tbRSHours.Text) + _
              Convert.ToDecimal(tbPumpingHours.Text) + Convert.ToDecimal(tbSynCondHours.Text) + _
              Convert.ToDecimal(tbPOHours.Text) + Convert.ToDecimal(tbFOHSFHours.Text) + _
              Convert.ToDecimal(tbMOHours.Text) + Convert.ToDecimal(tbSEHours.Text) + _
              Convert.ToDecimal(tbInactiveHours.Text)

            If Math.Round(decValue, 2) <> Math.Round(Convert.ToDecimal(tbPeriodHours.Text), 2) Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If

            decValue = drPerformance.ServiceHours + drPerformance.RSHours + _
            drPerformance.PumpingHours + drPerformance.SynchCondHours + _
            drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + _
            drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages

            If Math.Round(decValue, 2) <> Math.Round(drPerformance.PeriodHours, 2) Then
                e.IsValid = False
            Else
                e.IsValid = True
            End If

        End If

    End Sub

#End Region


#Region " GDCNDCMustExist_CustomValidator "

    Public Sub GDCNDCMustExist_CustomValidator(ByVal s As Object, ByVal e As ServerValidateEventArgs)

        ' Either GDC or NDC must be greater than 0

        Dim intNDC As Integer
        Dim intGDC As Integer

        If IsNumeric(tbNDC.Text) Then
            intNDC = Convert.ToInt32(tbNDC.Text)
        Else
            intNDC = 0
        End If

        'If IsNumeric(tbGDC.Text) Then
        '    intGDC = Convert.ToInt32(tbGDC.Text)
        'Else
        '    intGDC = 0
        'End If

        If intNDC = 0 And intGDC = 0 Then
            e.IsValid = False
        Else
            e.IsValid = True
        End If

    End Sub

#End Region

#Region " tbVerbalDesc_Validator1 "

    'Public Sub tbVerbalDesc_Validator1(ByVal s As Object, ByVal e As ServerValidateEventArgs)

    '    If (lbTULC.SelectedIndex + 1) = 6 And tbVerbalDesc.Text.Trim = String.Empty Then
    '        e.IsValid = False
    '    Else
    '        e.IsValid = True
    '    End If

    '    tbVerbalDesc_Validator.ErrorMessage = "Description cannot be blank with Loading Characteristic is 6"
    '    tbVerbalDesc_Validator.ToolTip = "Description cannot be blank with Loading Characteristic is 6"
    '    drSetup = CType(Session("drSetup"), Setup.SetupRow)

    '    If Not IsNothing(drSetup) Then

    '        If Not drSetup.IsPJMNull Then

    '            If drSetup.PJM Then

    '                e.IsValid = True

    '                If drSetup.UnitType.ToUpper = "NUCLEAR" Then
    '                    If (lbTULC.SelectedIndex + 1) > 1 Then
    '                        e.IsValid = False
    '                        tbVerbalDesc_Validator.ErrorMessage = "Nuclear Units can only have a Loading Characteristic of 1 (PJM018)"
    '                        tbVerbalDesc_Validator.ToolTip = "Nuclear Units can only have a Loading Characteristic of 1 (PJM018)"
    '                    Else
    '                        e.IsValid = True
    '                    End If
    '                End If

    '            End If

    '        End If

    '    End If

    'End Sub

#End Region


#Region " lbUpdateMonth_SelectedIndexChanged "
    Protected Sub lbUpdateMonth_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

            RestoreFromSession()

            UpdatePerfFields()

            SaveToSession()

            'Page.DataBind()
            btnPerfUpdate.BackColor = btnEditRecord.BackColor

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try
    End Sub
#End Region

#Region " lbUnits_SelectedIndexChanged "
    Protected Sub lbUnits_SelectedIndexChanged(sender As Object, e As EventArgs)

        RestoreFromSession()

        UpdatelbMonths()

        Session.Item("strCurrentUnit") = lbUnits.SelectedValue

        DisplayCheckStatus(True)

        ResetValidations()

        'Page.DataBind()

        SaveToSession()

        Page.Validate()

        btnPerfUpdate.BackColor = btnEditRecord.BackColor

    End Sub
#End Region

#Region " btnCreateHours_Click "
    Protected Sub btnCreateHours_Click(sender As Object, e As EventArgs) Handles btnCreateHours.Click
        Try

            RestoreFromSession()

            CreateHours()

            SaveToSession()

            Dim dtChanges As DataTable = dtPerformance.GetChanges()

            If IsNothing(dtChanges) Then
                btnPerfUpdate.BackColor = btnEditRecord.BackColor
            Else
                btnPerfUpdate.BackColor = Drawing.Color.Red
            End If

            Dim sScript As String

            sScript = "<script language='javascript'>"
            sScript += "alert('Hours Auto Fill completed');"
            sScript += "</script>"

            Response.Write(sScript)

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try
    End Sub
#End Region

#Region " dgEvents_Sorting "
    Protected Sub dgEvents_Sorting(sender As Object, e As GridViewSortEventArgs)

        Dim _sortDirection As SortDirection = SortDirection.Ascending
        Dim _sortField As String = "EventNumber"

        _sortField = e.SortExpression
        _sortDirection = e.SortDirection

        If Not IsNothing(dgEvents.Attributes("CurrentSortField")) And _
            Not IsNothing(dgEvents.Attributes("CurrentSortDirection")) Then

            If _sortField = dgEvents.Attributes("CurrentSortField") Then

                If dgEvents.Attributes("CurrentSortDirection") = "ASC" Then
                    _sortDirection = SortDirection.Descending
                Else
                    _sortDirection = SortDirection.Ascending
                End If

            End If

        End If

        dgEvents.Attributes("CurrentSortField") = _sortField

        If _sortDirection = SortDirection.Ascending Then
            dgEvents.Attributes("CurrentSortDirection") = "ASC"
        Else
            dgEvents.Attributes("CurrentSortDirection") = "DESC"
        End If

        dsGridEvents = CType(Session("dsGridEvents"), DataSet)

        If _sortField.IndexOf(",") > 0 Then
            Dim _sortField1 As String = _sortField.Substring(0, _sortField.IndexOf(","))
            Dim _sortField2 As String = _sortField.Substring(_sortField.IndexOf(","))

            dsGridEvents.Tables("EventData01").DefaultView.Sort = _sortField1 & " " & dgEvents.Attributes("CurrentSortDirection") & _sortField2
        Else
            dsGridEvents.Tables("EventData01").DefaultView.Sort = e.SortExpression & " " & dgEvents.Attributes("CurrentSortDirection")
        End If

        Me.dgEvents.DataSource = Me.dsGridEvents.Tables("EventData01").DefaultView
        Me.dgEvents.DataMember = "EventData01"
        Me.dgEvents.DataBind()

        Session.Item("dsGridEvents") = dsGridEvents

    End Sub
#End Region




    Private Sub btnUandP_Click(sender As Object, e As EventArgs) Handles btnUandP.Click
        'Response.Redirect("~\secure\DataUpload.aspx")
        Response.Redirect("~/secure/DataUpload.aspx")
    End Sub



    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        'Response.Redirect("~\secure\ReportsPage.aspx")
        Response.Redirect("~/secure/ReportsPage.aspx")
    End Sub

    Protected Sub mnuRefresh_Click(sender As Object, e As EventArgs) Handles mnuRefresh.Click

        RestoreFromSession()

        lForceRefresh = True

        UpdatePerfFields()

        lForceRefresh = False

        DisplayCheckStatus(True)

        SaveToSession()

        Page.Validate()

    End Sub

    Protected Sub btnProcessButton_Click(sender As Object, e As EventArgs) Handles btnProcessButton.Click
        'Response.Redirect("~\secure\DataUpload.aspx?choice=CALC")
        Response.Redirect("~/secure/DataUpload.aspx?choice=CALC")
    End Sub

#Region " btnPrintErrors_Click "
    Protected Sub btnPrintErrors_Click(sender As Object, e As EventArgs) Handles btnPrintErrors.Click
        Dim sScript As String

        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('ListBatchErrors.aspx', 'child'); new_win.focus();"
        sScript += "</script>"

        Response.Write(sScript)
    End Sub
#End Region

#Region " btnPrintEventErrors_Click "
    Protected Sub btnPrintEventErrors_Click(sender As Object, e As EventArgs) Handles btnPrintEventErrors.Click
        Dim sScript As String
        Dim objStreamWriter As StreamWriter
        Dim strErrorFileName As String

        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name
        strErrorFileName = myUser & "EventErrors.txt"

        objStreamWriter = File.CreateText(Server.MapPath(".\" & strErrorFileName))
        objStreamWriter.WriteLine(Me.tbEventErrorList.Text)
        objStreamWriter.Close()

        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('" & Application("AppPath").ToString & "/secure/" & strErrorFileName & "', 'child'); new_win.focus();"
        sScript += "</script>"

        Response.Write(sScript)
    End Sub
#End Region

#Region " btnPerfListGen_Click "
    Protected Sub btnPerfListGen_Click(sender As Object, e As EventArgs) Handles btnPerfListGen.Click
        Dim sScript As String

        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('ListPerformanceData.aspx?choice=" & PerfReports.Generation & "', 'child'); new_win.focus();"
        sScript += "</script>"

        Response.Write(sScript)
    End Sub
#End Region


#Region " btnPerfListHours_Click "
    Protected Sub btnPerfListHours_Click(sender As Object, e As EventArgs) Handles btnPerfListHours.Click
        Dim sScript As String

        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('ListPerformanceData.aspx?choice=" & PerfReports.Time & "', 'child'); new_win.focus();"
        sScript += "</script>"

        Response.Write(sScript)
    End Sub
#End Region

#Region " btnEventList_Click "
    Protected Sub btnEventList_Click(sender As Object, e As EventArgs) Handles btnEventList.Click
        'Dim sScript As String

        'sScript = "<script language='javascript'>"
        'sScript += "var new_win=window.open('ListEventData.aspx', 'child'); new_win.focus();"
        'sScript += "</script>"

        'Response.Write(sScript)

        OpenWindow("ListEventData.aspx")


    End Sub
#End Region

    Public Sub OpenWindow(ByVal htmlPage As String)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "child", "window.open('" & htmlPage & "','','')", True)
    End Sub


    Public Shared Sub OpenWindow(ByVal currentPage As Page, ByVal window As String, ByVal htmlPage As String, ByVal width As Int32, ByVal height As Int32)
        'Dim sb As New System.Text.StringBuilder()
        'sb.Append("popWin=window.open('")
        'sb.Append(htmlPage)
        'sb.Append("','")
        'sb.Append(window)
        'sb.Append("','width=")
        'sb.Append(width)
        'sb.Append(",height=")
        'sb.Append(height)
        'sb.Append(",toolbar=no,location=no, directories=no,status=no,menubar=no,scrollbars=no,resizable=no")
        'sb.Append("');")
        'sb.Append("popWin.focus();")

        ''popWin=window.open('child','DefaultError.html','width=800,height=600,toolbar=no,location=no, directories=no,status=no,menubar=no,scrollbars=no,resizable=no');popWin.focus();
        'ScriptManager.RegisterClientScriptBlock(currentPage, currentPage.GetType(), "OpenWindow", sb.ToString(), True)
    End Sub

#Region " btnBatchCheck_Click "
    Protected Sub btnBatchCheck_Click(sender As Object, e As EventArgs) Handles btnBatchCheck.Click

        ErrorCheck()

        dgEvents.DataSource = dsGridEvents
        dgEvents.DataMember = "EventData01"
        dgEvents.DataBind()

        'Page.DataBind()

        Dim sScript As String

        sScript = "<script language='javascript'>"
        sScript += "alert('Final Validation Error Check completed');"
        sScript += "</script>"

        Try
            Response.Write(sScript)
        Catch ex As System.Exception
            sScript = ex.ToString
        End Try
    End Sub
#End Region

#Region " btnSaveEvents_Click "
    Protected Sub btnSaveEvents_Click(sender As Object, e As EventArgs) Handles btnSaveEvents.Click

        Dim lHasErrors As Boolean

        lHasErrors = EventUpdate()
        DisplayCheckStatus(True)

        Dim sScript As String = "<script language='javascript'>"
        If lHasErrors Then
            sScript += "alert('There are Event Data errors to be resolved');"
        Else
            btnSaveEvents.BackColor = btnEditRecord.BackColor
            sScript += "alert('Submit to Event Data master database completed');"
        End If
        sScript += "</script>"

        Try
            Response.Write(sScript)
        Catch ex As System.Exception
            sScript = ex.ToString
        End Try
    End Sub
#End Region


#Region " btndgEventsRefresh_Click "
    Protected Sub btndgEventsRefresh_Click(sender As Object, e As EventArgs) Handles btndgEventsRefresh.Click
        ' lnkbutRefresh is a hidden link button the duplicates this "automatically"
        ' this should stay here as a backup in case it doesn't work automatically with the postback

        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        'dsGridEvents = CType(Session("dsGridEvents"), DataSet)

        'Me.dgEvents.DataSource = dsGridEvents
        'Me.dgEvents.DataMember = "EventData01"
        'Me.dgEvents.DataBind()

        'Dim strTemp As String

        'Try
        '	strTemp = Me.dgEvents.DataKeys(Me.dgEvents.SelectedIndex).ToString
        'Catch ex As System.Exception
        '	strTemp = "0000"
        'End Try

        '      'Me.dsGridEvents.Tables("EventData02").DefaultView.RowFilter = "EventNumber = " & strTemp

        '      'If Me.dsGridEvents.Tables("EventData02").DefaultView.Count > 0 Then
        '      '	Me.dgEventsAddWork.DataSource = Me.dsGridEvents.Tables("EventData02").DefaultView
        '      'Else
        '      '	Me.dgEventsAddWork.DataSource = ""
        '      'End If

        '      'Me.dgEventsAddWork.DataMember = "EventData02"

        '      'Me.dgEventsAddWork.DataBind()

        '      Session.Item("dsGridEvents") = dsGridEvents

        ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        RestoreFromSession()

        dsGridEvents.Clear()

        dsGridEvents = blConnect.GetBothEventSetsGrid(lbUnits.SelectedValue.ToString, intCurrentYear)

        dsGridEvents.Tables("EventData01").Columns.Add("strStartDateTime")
        dsGridEvents.Tables("EventData01").Columns.Add("strEndDateTime")

        Dim dr As DataRow
        Dim strFormat As String = "MM/dd/yyyy"
        Dim dtResult As DateTime
        Dim strTemp As String = String.Empty

        For Each dr In dsGridEvents.Tables("EventData01").Rows

            If Not IsDBNull(dr("StartDateTime")) Then

                dtResult = Convert.ToDateTime(dr("StartDateTime"))

                strTemp = dtResult.ToString("HH:mm:ss")

                If strTemp = "23:59:59" Then
                    dr("strStartDateTime") = dtResult.ToString(strFormat) & " 24:00"
                Else
                    dr("strStartDateTime") = dtResult.ToString(strFormat) & dtResult.ToString(" HH:mm")
                End If

            End If

            If Not IsDBNull(dr("EndDateTime")) Then

                dtResult = Convert.ToDateTime(dr("EndDateTime"))

                strTemp = dtResult.ToString("HH:mm:ss")

                If strTemp = "23:59:59" Then
                    dr("strEndDateTime") = dtResult.ToString(strFormat) & " 24:00"
                Else
                    dr("strEndDateTime") = dtResult.ToString(strFormat) & dtResult.ToString(" HH:mm")
                End If

            End If

        Next

        dgEvents.DataSource = dsGridEvents
        dgEvents.DataMember = "EventData01"
        dgEvents.DataBind()

        Try
            strTemp = Me.dgEvents.DataKeys(Me.dgEvents.SelectedIndex).ToString
        Catch ex As System.Exception
            strTemp = "0000"
        End Try

        DisplayCheckStatus(True)

        SaveToSession()
    End Sub
#End Region

#Region " btnEditRecord_Click "
    Private Sub btnEditRecord_Click(sender As Object, e As EventArgs) Handles btnEditRecord.Click

        Dim strTitle As String
        Dim intRow As Integer
        Dim sScript As String

        Try

            Dim mydrvDetail As DataRowView

            'intRow = Me.dgEvents.Items.Count
            intRow = Me.dgEvents.Rows.Count

            If intRow = 0 Then

                sScript = "<script language='javascript'>"
                sScript += "alert('There are no event records to edit');"
                sScript += "</script>"

                Response.Write(sScript)

                Exit Sub

            End If

            intRow = Me.dgEvents.SelectedIndex()

            If intRow = -1 Then

                sScript = "<script language='javascript'>"
                sScript += "alert('Please select an event to edit');"
                sScript += "</script>"

                Response.Write(sScript)

                Exit Sub

            End If

            RestoreFromSession()

            Dim strTemp As String = dgEvents.DataKeys(Me.dgEvents.SelectedIndex).Value.ToString()
            Session.Add("EventNumber", strTemp)
            Me.dsGridEvents.Tables("EventData01").DefaultView.RowFilter = "EventNumber = " & strTemp
            mydrvDetail = Me.dsGridEvents.Tables("EventData01").DefaultView.Item(0)

            Me.dsGridEvents.Tables("EventData01").DefaultView.RowFilter = String.Empty

            'drvDetail = CType(Me.dsGridEvents.Tables("EventData01").Rows(intRow), DataRowView)
            'Session.Add("drvDetail", mydrvDetail)

            If mydrvDetail.Item("RevisionCard01").ToString.ToString = "X" Then

                sScript = "<script language='javascript'>"
                sScript += "alert('Event " & mydrvDetail.Item("EventNumber").ToString & " has been deleted (Revision Code = X)');"
                sScript += "</script>"

                Response.Write(sScript)

                Exit Sub

            End If

            sScript = "<script language='javascript'>"

            If Me.intCurrentYear < 1996 And _
               Convert.IsDBNull(mydrvDetail.Item("ChangeDateTime1")) = False And _
               Convert.IsDBNull(mydrvDetail.Item("ChangeInEventType1")) = False Then

                If mydrvDetail.Item("ChangeInEventType1").ToString.Trim <> String.Empty Then

                    sScript += "var new_win=window.open('EditEvent0103Pre1996.aspx', 'child'); new_win.focus();"

                Else

                    sScript += "var new_win=window.open('EditEvent0103.aspx?mode=edit', 'child'); new_win.focus();"

                End If

            Else

                sScript += "var new_win=window.open('EditEvent0103.aspx?mode=edit', 'child'); new_win.focus();"

            End If

            sScript += "</script>"
            OpenWindow("EditEvent0103.aspx?mode=edit")
            ' Response.Write(sScript)

        Catch ex As System.Exception

            strTitle = ex.ToString

        End Try
    End Sub
#End Region

    Protected Sub btnEditData_Click(sender As Object, e As EventArgs) Handles btnEditData.Click
        'Response.Redirect("~\secure\DEApp.aspx")
        Response.Redirect("~/secure/DEApp.aspx")

    End Sub


#Region " btnDeleteEvent_Click "
    'Protected Sub btnDeleteEvent_Click(sender As Object, e As EventArgs) Handles btnDeleteEvent.Click
    '    Dim intRow As Integer
    '    Dim sScript As String
    '    Dim strTemp As String

    '    Try

    '        'Dim mydrvDetail As DataRowView

    '        intRow = Me.dgEvents.Rows.Count

    '        If intRow = 0 Then

    '            sScript = "<script language='javascript'>"
    '            sScript += "alert('There are no event records to delete');"
    '            sScript += "</script>"

    '            Response.Write(sScript)

    '            Exit Sub

    '        End If

    '        dsGridEvents = CType(Session("dsGridEvents"), DataSet)

    '        Try
    '            strTemp = Me.dgEvents.DataKeys(Me.dgEvents.SelectedIndex).ToString
    '        Catch ex As System.Exception
    '            strTemp = "0000"
    '        End Try

    '        Me.dsGridEvents.Tables("EventData02").DefaultView.RowFilter = "EventNumber = " & strTemp

    '        For intRow = 0 To Me.dsGridEvents.Tables("EventData02").Rows.Count - 1
    '            Me.dsGridEvents.Tables("EventData02").Rows(intRow).Delete()
    '        Next

    '        Me.dsGridEvents.Tables("EventData02").DefaultView.RowFilter = String.Empty

    '        Me.dsGridEvents.Tables("EventData01").DefaultView.RowFilter = "EventNumber = " & strTemp
    '        Me.dsGridEvents.Tables("EventData01").DefaultView.Delete(0)
    '        Me.dsGridEvents.Tables("EventData01").DefaultView.RowFilter = String.Empty

    '        Session.Item("dsGridEvents") = dsGridEvents

    '        Me.dgEvents.DataSource = Me.dsGridEvents.Tables("EventData01").DefaultView
    '        Me.dgEvents.DataMember = "EventData01"
    '        Me.dgEvents.DataBind()

    '        Try
    '            strTemp = Me.dgEvents.DataKeys(Me.dgEvents.SelectedIndex).ToString
    '        Catch ex As System.Exception
    '            strTemp = "0000"
    '        End Try

    '        Me.dsGridEvents.Tables("EventData02").DefaultView.RowFilter = "EventNumber = " & strTemp

    '        'If Me.dsGridEvents.Tables("EventData02").DefaultView.Count > 0 Then
    '        '	Me.dgEventsAddWork.DataSource = Me.dsGridEvents.Tables("EventData02").DefaultView
    '        'Else
    '        '	Me.dgEventsAddWork.DataSource = ""
    '        'End If

    '        'Me.dgEventsAddWork.DataMember = "EventData02"

    '        'Me.dgEventsAddWork.DataBind()

    '    Catch ex As System.Exception

    '        strTemp = ex.ToString

    '    End Try
    'End Sub
#End Region

#Region " btnNewEvent_Click "
    'Protected Sub btnNewEvent_Click(sender As Object, e As EventArgs) Handles btnNewEvent.Click
    '    Dim sScript As String

    '    sScript = "<script language='javascript'>"
    '    sScript += "var new_win=window.open('EditEvent0103.aspx?mode=new', 'child'); new_win.focus();"
    '    sScript += "</script>"

    '    Response.Write(sScript)
    'End Sub
#End Region


    Private Sub dgErrors_Sorting(sender As Object, e As GridViewSortEventArgs) Handles dgErrors.Sorting
        Dim _sortDirection As SortDirection = SortDirection.Ascending
        Dim _sortField As String = "ErrorSeverity"

        _sortField = e.SortExpression
        _sortDirection = e.SortDirection

        If Not IsNothing(dgErrors.Attributes("CurrentSortField")) And _
            Not IsNothing(dgErrors.Attributes("CurrentSortDirection")) Then

            If _sortField = dgErrors.Attributes("CurrentSortField") Then

                If dgErrors.Attributes("CurrentSortDirection") = "ASC" Then
                    _sortDirection = SortDirection.Descending
                Else
                    _sortDirection = SortDirection.Ascending
                End If

            End If

        End If

        dgErrors.Attributes("CurrentSortField") = _sortField

        If _sortDirection = SortDirection.Ascending Then
            dgErrors.Attributes("CurrentSortDirection") = "ASC"
        Else
            dgErrors.Attributes("CurrentSortDirection") = "DESC"
        End If

        dsErrors = CType(Session("dsErrors"), DataSet)

        If _sortField.IndexOf(",") > 0 Then
            Dim _sortField1 As String = _sortField.Substring(0, _sortField.IndexOf(","))
            Dim _sortField2 As String = _sortField.Substring(_sortField.IndexOf(","))

            dsErrors.Tables("Errors").DefaultView.Sort = _sortField1 & " " & dgErrors.Attributes("CurrentSortDirection") & _sortField2
        Else
            dsErrors.Tables("Errors").DefaultView.Sort = e.SortExpression & " " & dgErrors.Attributes("CurrentSortDirection")
        End If

        Me.dgErrors.DataSource = Me.dsErrors.Tables("Errors").DefaultView
        Me.dgErrors.DataMember = "Errors"
        Me.dgErrors.DataBind()

        Session.Item("dsErrors") = dsErrors
    End Sub


    Protected Sub dgEvents_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        Dim i As Integer
        Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)

        If e.Row.RowType = ListItemType.Item Or e.Row.RowType = ListItemType.AlternatingItem Then

            If drv.Item("EndDateTime").ToString.Trim = String.Empty Then
                For i = 1 To e.Row.Cells.Count - 1
                    e.Row.Cells(i).BackColor = System.Drawing.Color.ForestGreen
                    e.Row.Cells(i).ForeColor = System.Drawing.Color.White
                Next
            ElseIf drv.Item("RevisionCard01").ToString.Trim.ToUpper = "X" Then
                For i = 1 To e.Row.Cells.Count - 1
                    e.Row.Cells(i).BackColor = System.Drawing.Color.Gold
                    e.Row.Cells(i).ForeColor = System.Drawing.Color.Black
                Next
            End If
        ElseIf e.Row.RowType = ListItemType.SelectedItem Then

            If drv.Item("EndDateTime").ToString.Trim = String.Empty Then
                For i = 1 To e.Row.Cells.Count - 1
                    e.Row.Cells(i).BackColor = System.Drawing.Color.ForestGreen
                    e.Row.Cells(i).ForeColor = System.Drawing.Color.White
                Next
            ElseIf drv.Item("RevisionCard01").ToString.Trim.ToUpper = "X" Then
                For i = 1 To e.Row.Cells.Count - 1
                    e.Row.Cells(i).BackColor = System.Drawing.Color.Gold
                    e.Row.Cells(i).ForeColor = System.Drawing.Color.Black
                Next
            Else
                For i = 1 To e.Row.Cells.Count - 1
                    e.Row.Cells(i).BackColor = System.Drawing.Color.Navy
                    e.Row.Cells(i).ForeColor = System.Drawing.Color.White
                Next
            End If

        Else
            For i = 1 To e.Row.Cells.Count - 1
                e.Row.Cells(i).BackColor = System.Drawing.Color.White
                e.Row.Cells(i).ForeColor = System.Drawing.Color.Black
            Next
        End If

    End Sub

    Private Sub lnkbutRefresh_Click(sender As Object, e As EventArgs) Handles lnkbutRefresh.Click
        dsGridEvents = CType(Session("dsGridEvents"), DataSet)

        Me.dgEvents.DataSource = dsGridEvents
        Me.dgEvents.DataMember = "EventData01"
        Me.dgEvents.DataBind()

        Dim strTemp As String

        Try
            strTemp = Me.dgEvents.DataKeys(Me.dgEvents.SelectedIndex).ToString
        Catch ex As System.Exception
            strTemp = "0000"
        End Try

        'Me.dsGridEvents.Tables("EventData02").DefaultView.RowFilter = "EventNumber = " & strTemp

        'If Me.dsGridEvents.Tables("EventData02").DefaultView.Count > 0 Then
        '	Me.dgEventsAddWork.DataSource = Me.dsGridEvents.Tables("EventData02").DefaultView
        'Else
        '	Me.dgEventsAddWork.DataSource = ""
        'End If

        'Me.dgEventsAddWork.DataMember = "EventData02"

        'Me.dgEventsAddWork.DataBind()

        Session.Item("dsGridEvents") = dsGridEvents

        'Me.dgEventsAddWork.DataSource = dsGridEvents
        'Me.dgEventsAddWork.DataMember = "EventData02"
        'Me.dgEventsAddWork.DataBind()

    End Sub

    Protected Sub dgEvents_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    '' TextChangedEventHandler delegate method. 
    'Private Sub textChangedEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    '    btnPerfUpdate.BackColor = Drawing.Color.Red
    'End Sub


    Protected Sub PerfValue_TextChanged(sender As Object, e As EventArgs)
        If Not btnPerfUpdate.BackColor = Drawing.Color.Red Then
            btnPerfUpdate.BackColor = Drawing.Color.Red
        End If
    End Sub
End Class