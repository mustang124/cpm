﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProcessStatus.aspx.vb" Inherits="PortalNYISO.ProcessView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Process Status</title>
    <link href="../NewStyles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <table>
            <tr>
                <td style="width: 136px">
                    <asp:Label ID="InitiatedLabel" EnableViewState="false" BackColor="Transparent" Width="136px"
                        runat="server" Font-Names="Times New Roman" Font-Size="10pt"></asp:Label></td>
                <td style="width: 72px">
                    <asp:Label ID="TypeLabel" EnableViewState="false" BackColor="Transparent" Width="72px" runat="server"
                        Font-Names="Times New Roman" Font-Size="10pt"></asp:Label></td>
                <td style="width: 388px">
                    <asp:Label ID="StatusLabel" runat="server" Width="388px" BackColor="Transparent" Font-Names="Times New Roman"
                        Font-Size="10pt" EnableViewState="false"></asp:Label></td>
                <td style="height: 26px">
                    <asp:Button ID="EditButton" runat="server" Width="72px" Text="Edit Data" EnableViewState="False"></asp:Button>
                    <asp:Button ID="AbortButton" runat="server" Width="72px" Text="Abort" EnableViewState="False"></asp:Button></td>
            </tr>
        </table>

    </form>
</body>
</html>
