﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditEvent0103.aspx.vb" Inherits="PortalNYISO.EditEvent0103" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Event 01-03 Data Records</title>
    <link href="../NewStyles.css" type="text/css" rel="stylesheet" />
    <script lang="jscript">
        var pWin
        function setParent() {
            pWin = top.window.opener
            //  onload="setParent()" 
            //   onblur="self.focus();"
            // onload="self.focus();setParent();"
        }
        function reloadParent() {
            pWin.location.reload(true)
        }
        function tickleParent() {
            pWin.dgEvents.databind();
        }
        function loadTree() {
            open('CauseCodeTreeView.aspx', 'child')
        }
    </script>
    <link href="../StylesMainDE.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="EditEvent0103" runat="server" style="padding-left:5px;">
        <a href="../MainDE.aspx">
            <asp:Image ID="Image1" ImageUrl="../images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" /></a>
        <asp:Panel ID="Panel2" Width="765px" runat="server" BorderStyle="Inset" BorderColor="Black">

            <table style="margin: 5px; width: 100%">
                <tr>
                    <td colspan="4" style="margin: 10px">
                        <asp:Label ID="lblFormHeader" Style="FONT-SIZE: 9pt; padding: 10px;" runat="server" ForeColor="ActiveCaptionText" Width="95%"
                            BackColor="ActiveCaption" BorderStyle="Outset">Edit Event 1 For 2014</asp:Label>
                    </td>
                </tr>
                <tr style="align-content: center; margin: 10px">
                    <td style="width: 25%;">
                        <asp:Button ID="btnSaveNewEvent" runat="server" Width="95%" Text="Save"></asp:Button></td>
                    <td style="width: 10%;"></td>
                    <td style="width: 10%;"></td>
                    <td style="width: 25%;">
                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Width="95%" Text="Cancel"></asp:Button></td>
                </tr>
            </table>
            <table style="padding: 10px; margin: 10px; width: 98%">
                <tr>
                    <td style="vertical-align: top; width: 45%">
                        <asp:Panel ID="gbNewEvent" runat="server" Width="100%" Height="152px">
                            <asp:Label ID="Label5" Style="FONT-SIZE: 9pt" Width="100%" BackColor="ActiveCaption" ForeColor="ActiveCaptionText"
                                Font-Bold="True" runat="server">Event Details</asp:Label>
                            <table style="WIDTH: 100%; HEIGHT: 100%">
                                <tr style="FONT-SIZE: small">
                                    <td>Event Type
										<asp:CustomValidator ID="lbEventType_CustV" runat="server" Font-Size="8pt" ErrorMessage="Must select an Event Type"
                                            Display="Dynamic" ControlToValidate="lbEventType" ToolTip="Must select an Event Type">*</asp:CustomValidator></td>
                                    <td colspan="3">
                                        <asp:ListBox ID="lbEventType" runat="server" Width="100%" Rows="1" AutoPostBack="True" OnSelectedIndexChanged="lbEventType_SelectedIndexChanged"></asp:ListBox></td>
                                </tr>
                                <tr style="FONT-SIZE: 8pt">
                                    <td>&nbsp;</td>
                                    <td><u>Month</u></td>
                                    <td><u>Day</u></td>
                                    <td><u>Military Time</u></td>
                                </tr>
                                <tr>
                                    <td style="FONT-SIZE: small;">Start of Event</td>
                                    <td>
                                        <asp:TextBox ID="tbStartOfEventMonth" runat="server" MaxLength="2" Columns="2">1</asp:TextBox>
                                        <asp:RequiredFieldValidator ID="tbStartOfEventMonth_RequiredFieldValidator" runat="server" Font-Size="8pt" ErrorMessage="Start of Event Month is a required field"
                                            Display="Dynamic" ControlToValidate="tbStartOfEventMonth" ToolTip="Start of Event Month is a required field"
                                            InitialValue="0">*</asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="tbStartOfEventMonth_RangeValidator" runat="server" Font-Size="8pt" ErrorMessage="Start of Event Month must be between 1 and 12"
                                            Display="Dynamic" ControlToValidate="tbStartOfEventMonth" ToolTip="Start of Event Month must be between 1 and 12"
                                            MinimumValue="1" MaximumValue="12" Enabled="true" Type="Integer">*</asp:RangeValidator></td>
                                    <td>
                                        <asp:TextBox ID="tbStartOfEventDay" runat="server" MaxLength="2" Columns="2">2</asp:TextBox>
                                        <asp:CustomValidator ID="tbStartOfEventDay_CustV" runat="server" ErrorMessage="Invalid Start of Event Day"
                                            Display="Dynamic" ControlToValidate="tbStartOfEventDay" ToolTip="Invalid Number of Days for Month"
                                            Enabled="true">*</asp:CustomValidator>
                                        <asp:CompareValidator ID="tbStartOfEventDay_CompV" runat="server" ErrorMessage="Invalid Start of Event Day"
                                            Display="Dynamic" ControlToValidate="tbEndOfEventDay" ToolTip="Invalid Start of Event Day" Type="Integer"
                                            Operator="DataTypeCheck">*</asp:CompareValidator></td>
                                    <td>
                                        <asp:TextBox ID="tbStartOfEventTime" runat="server" MaxLength="4" Columns="4">3</asp:TextBox>
                                        <asp:CustomValidator ID="tbStartOfEventTime_CustV" runat="server" ErrorMessage="Invalid Start of Event Time"
                                            Display="Dynamic" ControlToValidate="tbStartOfEventTime" ToolTip="Invalid Time" OnServerValidate="tbStartOfEventTime_CustV_ServerValidate">*</asp:CustomValidator>
                                        <asp:CustomValidator ID="StartDateTime_CV" runat="server" ErrorMessage="Invalid Start of Event Date/time"
                                            Display="Dynamic" ControlToValidate="tbStartOfEventTime" ToolTip="Invalid Start of Event Date/time"
                                            OnServerValidate="StartDateTime_CustomValidator">*</asp:CustomValidator></td>
                                </tr>
                                <tr>
                                    <td style="FONT-SIZE: small;">End of Event</td>
                                    <td>
                                        <asp:TextBox ID="tbEndOfEventMonth" runat="server" MaxLength="2" Columns="2">4</asp:TextBox>
                                        <asp:RangeValidator ID="tbEndOfEventMonth_RangeValidator" runat="server" Font-Size="8pt" ErrorMessage="End of Event Month must be between 1 and 12"
                                            Display="Dynamic" ControlToValidate="tbEndOfEventMonth" ToolTip="End of Event Month must be between 1 and 12"
                                            MinimumValue="1" MaximumValue="12" Type="Integer">*</asp:RangeValidator></td>
                                    <td>
                                        <asp:TextBox ID="tbEndOfEventDay" runat="server" MaxLength="2" Columns="2">5</asp:TextBox>
                                        <asp:CustomValidator ID="tbEndOfEventDay_CustV" runat="server" Font-Size="8pt" ErrorMessage="Invalid End of Event Day"
                                            Display="Dynamic" ControlToValidate="tbEndOfEventDay" ToolTip="Invalid Number of Days in Month">*</asp:CustomValidator></td>
                                    <td>
                                        <asp:TextBox ID="tbEndOfEventTime" runat="server" MaxLength="4" Columns="4">6</asp:TextBox>
                                        <asp:CustomValidator ID="tbEndOfEventTime_CustV" runat="server" Font-Size="8pt" ErrorMessage="Invalid End of Event Time"
                                            Display="Dynamic" ControlToValidate="tbEndOfEventTime" ToolTip="Invalid End of Event Time" OnServerValidate="tbEndOfEventTime_CustV_ServerValidate">*</asp:CustomValidator>
                                        <asp:CustomValidator ID="EndDateTime_CV" runat="server" Font-Size="8pt" ErrorMessage="Invalid End of Event Date/time"
                                            Display="Dynamic" ControlToValidate="tbEndOfEventTime" ToolTip="Invalid End of Event Date/time" OnServerValidate="EndDateTime_CustomValidator">*</asp:CustomValidator></td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <p></p>

                        <asp:Panel ID="gbCauseCodes" runat="server" Width="100%">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 60%">
                                        <asp:Button ID="btnCauseCodeTreeView" Width="100%" Text="System/Component Cause Code" runat="server"></asp:Button></td>
                                    <td>
                                        <asp:TextBox ID="tbCauseCode" runat="server" MaxLength="4" Columns="4"></asp:TextBox>
                                        <asp:CustomValidator ID="tbCauseCode_CustV" runat="server" Font-Size="8pt" ErrorMessage="Invalid Cause Code" OnServerValidate="tbCauseCode_CustV_ServerValidate"
                                            Display="Dynamic" ControlToValidate="tbCauseCode" ToolTip="Invalid Cause Code" Enabled="true">*</asp:CustomValidator></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblNERCCauseCodeDesc" runat="server" Text=""></asp:Label>

                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td style="width: 25%; vertical-align: top">
                        <asp:Panel ID="Panel1" Width="100%" runat="server">
                            <asp:Panel ID="gbDeratings" runat="server" Width="100%">
                                <asp:Label ID="Label1" Style="FONT-SIZE: 9pt;" Width="99%" BackColor="ActiveCaption" ForeColor="ActiveCaptionText"
                                    Font-Bold="True" runat="server">Deratings / Net Available Capacity MW</asp:Label>
                                <table style="width: 100%">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="width: 25%">
                                        <td></td>
                                        <td style="align-content:center; width:100%; " >
                                            <asp:TextBox ID="tbNAC" runat="server" Columns="7" MaxLength="7"></asp:TextBox>
                                            <asp:RangeValidator ID="tbNAC_RangeValidator" runat="server" Font-Size="8pt" ErrorMessage="NAC must be a positive number"
                                                Display="Dynamic" ControlToValidate="tbNAC" ToolTip="NAC must be a positive number" MinimumValue="0"
                                                MaximumValue="2000" Type="Double">*</asp:RangeValidator>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <p></p>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <p></p>
            <asp:ValidationSummary ID="ValidationSummaryEvent01" ForeColor="Red" runat="server" Font-Size="8pt" Enabled="true"
                EnableClientScript="true" EnableViewState="true" ShowSummary="true" ShowMessageBox="false" HeaderText="The following errors exist:"
                DisplayMode="BulletList"></asp:ValidationSummary>
        </asp:Panel>
    </form>
</body>
</html>
