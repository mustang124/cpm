Public Enum FileUploadStatus 'used for upload file control
    NotValidated = 0  'displays question mark
    NoError
    Invalid           'displays red X
    Accepted          'displays checkmark
End Enum

Public Enum GrossNetOrBoth
   GrossOnly = 0
   NetOnly = 1
   Both = 2
End Enum

Public Enum ProcessStatus 'must be updated in WebProcessEnum table
    Queued = 0        'not yet processed
    Working           'processing
    Completed         'finished processing
    Rejected          'error found
    None              'when a user has never started a process or no process queued
    Aborted           'user cancelled queued item
    Invalid           'returned when requesting info on an invalid workid
    InvalidFileFormat 'for FileCheck type
End Enum

Public Enum ProcessType 'must be updated in WebProcessEnum table
    FileCheck = 0  'ASCII test routines
    DataCheck      'ISO routines
    Analysis       'Data analysis routines
    None           'for invalid processes, requests, etc.
End Enum

Public Enum LogTypes 'must be updated in SQL table
    Information = 0  'misc info
    Alert            'possible failure or warning
    GeneralError     'error source unknown
    DBError          'error on database operation
    FileError        'error on file operation
    CodeError        'error in source code
    ThreadError      'error in worker thread
    WebError         'IIS or other related web errors
End Enum