﻿Imports System.Web.Security
Imports System.Xml
Imports Microsoft.DSG.Security.CryptoServices
Imports BusinessLayer
Imports SetupSettings.Settings
Imports System.Collections.Specialized
Imports System.Collections.Specialized.NameValueCollection

Public Class DELogin
    Inherits System.Web.UI.Page

    Public AppName As String = "GADSNG"
    Dim connect As String = "DataEntry"
    Public blConnect As BusinessLayer.BusinessLayer.GADSNGBusinessLayer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Output.Text = Application("AppPath").ToString

        ' this does NOT have Secure at the end of the path
        'Output.Text = Application("PhysicalAppPath").ToString()

        ' ServerMapPath goes directly to the Secure folder
        'Output.Text = Application("ServerMapPath").ToString()

        Output.Text = String.Empty

    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click

#If DEBUG Then

        'Dim adAuth As FormsAuth.LdapAuthentication = New FormsAuth.LdapAuthentication()
        'adAuth.IsAuthenticated(UserName.Text, Password.Text)

        If CustomAuthenticate(UserName.Text, Password.Text) Then

            FormsAuthentication.SetAuthCookie(UserName.Text, PersistCookie.Checked)
            'then, redirect whatever page you want here
            'Dim myReturn As String = FormsAuthentication.GetRedirectUrl(UserName.Text, PersistCookie.Checked)
            Response.Redirect(FormsAuthentication.GetRedirectUrl(UserName.Text, PersistCookie.Checked))

            'FormsAuthentication.RedirectFromLoginPage(UserName.Text, PersistCookie.Checked)
            Output.Text = String.Empty
        Else
            Output.Text = "Invalid login"
        End If
#Else

        'If CustomAuthenticate(UserName.Text, Password.Text) Then

        '    FormsAuthentication.SetAuthCookie(UserName.Text, PersistCookie.Checked)
        '    'then, redirect whatever page you want here
        '    'Dim myReturn As String = FormsAuthentication.GetRedirectUrl(UserName.Text, PersistCookie.Checked)
        '    Response.Redirect(FormsAuthentication.GetRedirectUrl(UserName.Text, PersistCookie.Checked))

        '    'FormsAuthentication.RedirectFromLoginPage(UserName.Text, PersistCookie.Checked)
        '    Output.Text = String.Empty
        'Else
        '    Output.Text = "Invalid login"
        'End If

        Output.Text = ""

        Dim adAuth As FormsAuth.LdapAuthentication = New FormsAuth.LdapAuthentication()

        Try
            If (True = adAuth.IsAuthenticated(UserName.Text, Password.Text)) Then

                'Create the ticket, and add the groups.
                Dim isCookiePersistent As Boolean = PersistCookie.Checked
                Dim authTicket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1, _
                     UserName.Text, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, "GADS")

                'Encrypt the ticket.
                Dim encryptedTicket As String = FormsAuthentication.Encrypt(authTicket)

                'Create a cookie, and then add the encrypted ticket to the cookie as data.
                Dim authCookie As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)

                If (isCookiePersistent = True) Then
                    authCookie.Expires = authTicket.Expiration
                End If
                'Add the cookie to the outgoing cookies collection.
                Response.Cookies.Add(authCookie)

                'You can redirect now.
                FormsAuthentication.SetAuthCookie(UserName.Text, PersistCookie.Checked)
                'then, redirect whatever page you want here
                Response.Redirect(FormsAuthentication.GetRedirectUrl(UserName.Text, PersistCookie.Checked))
                'FormsAuthentication.RedirectFromLoginPage(UserName.Text, PersistCookie.Checked)
                Output.Text = String.Empty
            Else
                Output.Text = "Authentication did not succeed. Check user name and password."
            End If

        Catch ex As Exception
            Output.Text = "Error authenticating. " & ex.Message
        End Try
#End If

    End Sub

    Private Function CustomAuthenticate(ByVal UserName As String, ByVal Password As String) As Boolean

        Dim myUser As String
        Dim lValidUser As Boolean = False

        ' the below returns path where the root web is installed.  Later in code I append Secure\ and then GADSNG.xml
        ' for web UI the GADSNG.xml file needs to go into the "Secure" folder

        connect = Request.ServerVariables("APPL_PHYSICAL_PATH")
        Try
            blConnect = New BusinessLayer.BusinessLayer.GADSNGBusinessLayer(connect)
        Catch ex As Exception
            Return lValidUser
        End Try

        If blConnect.CountUsersDefined() > 0 Then

            ' There are users defined in the NGUsers table

            If blConnect.IsFormsUserValid(UserName.Trim, Password.Trim) = 0 Then

                ' The user ID Tiger\Ron is not a valid user

                myUser = UserName.Substring(UserName.LastIndexOf("\") + 1)

                ' Try just the part to the right of the "\"

                If blConnect.IsFormsUserValid(myUser, Password.Trim) = 0 Then
                    lValidUser = False
                Else
                    lValidUser = True
                End If

            Else
                lValidUser = True
            End If

        Else

            ' No users are defined in table -- probably first time or standalone
            lValidUser = False

        End If

        Return lValidUser

    End Function

End Class