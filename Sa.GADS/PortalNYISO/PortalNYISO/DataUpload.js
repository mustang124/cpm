//javascript unique to DataUpload.aspx, assumes utility.js is included in main page

//hides file upload controls so user cant change values during postback
function HideFileUploads() {
    var Item = GetElement('WaitLayer');
    var Frame = GetElement('FileUploadFrame');
    Item.style.zIndex = 1000;
    ShowElement('WaitLayer', true);
    Item.style.top = Frame.offsetTop;
    Item.style.left = 0;
    Item.style.height = Frame.style.height;
    Item.style.width = document.body.clientWidth;
}

function HideForUpload() {
    HideFileUploads();
    //CollapseElement('UploadButton');
    EnableElement('UploadButton', false);
    ShowElement('UploadingLabel', true);
    ShowElement('UploadingImage', true);
    EnableElement('UploadingImage', true);
}


//stop user from leaving page while uploading *only works with IE
function UnloadWarning() {
    if (Uploading)
        return 'Closing this window before it returns from the server will stop all uploading files.\nAre you sure you wish to do this?';
}

Uploading = false;
window.onbeforeunload = UnloadWarning;

