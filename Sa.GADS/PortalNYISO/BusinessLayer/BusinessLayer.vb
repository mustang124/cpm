'BusinessLayer
'Copyright (C) 2011  The Outercurve Foundation

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; either version 2
'of the License, or (at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

' Ron Fluegge
' GADS Open Source Project Coordinator
' Coordinator@GADSOpenSource.com
' 972-625-5653

Option Strict On
Option Explicit On

Imports System
Imports System.IO
Imports System.Collections.Specialized
Imports System.Data.Common
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Diagnostics
Imports System.Windows.Forms
Imports System.Windows.Forms.Application
Imports System.Threading
Imports System.Web.SessionState
Imports GADSNG.DAL
Imports GADSNG.Base
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports Microsoft.DSG.Security.CryptoServices
Imports SetupSettings

Namespace BusinessLayer

    'FOLDERNAME F:\GADSNG Data Entry\GADSNGDE\BusinessLayer
    Public Class GADSNGBusinessLayer : Inherits GADSNG.Base.DALBase

        ' Executing a transaction
        ' Dim trans as IDbTransaction
        ' Try
        '   trans = Me.Factory.BeginTransaction(IsolationLevel.ReadCommitted)
        '   Try
        '       call _saveBook(b, trans)
        '       call _removeBookFromQueue(b.ProductID, trans)
        '       trans.Commit() ' Success
        '   Catch e as System.Exception
        '       trans.Rollback() ' Failure
        '       Me.ThrowAtomicException(".....", e)
        '   End Try
        ' Catch e as System.Exception
        '   Me.ThrowGADSNGException("Could not start a transaction to approve book", e)
        ' End Try

        Private GetFromDictionary As New HybridDictionary
        Private Const NOTFOUND As String = "<<nothing>>"

#Region " Constructor New(ByVal connect As String) "

        Public Sub New(ByVal connect As String)
            ' connect should be either "DataEntry" or "Analysis", or the WEB root folder

            MyBase.New(connect)

        End Sub

        Public Sub New(ByVal connect As String, ByRef reader As StreamReader)
            ' connect should be either "DataEntry" or "Analysis"
            MyBase.New(connect, reader)

        End Sub

#End Region

#Region " GetUtilityUnitData() As Setup.SetupDataTable "

        Public Function GetUtilityUnitData() As Setup.SetupDataTable

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            'Dim intRows As Int32

            stringSelect = "SELECT * FROM Setup ORDER BY UnitName, UtilityUnitCode"

            Dim tblSetup As Setup.SetupDataTable
            tblSetup = New Setup.SetupDataTable

            idaTest = Factory.GetDataAdapter(stringSelect, Nothing)

            Try
                idaTest.Fill(tblSetup)
                'intRows = tblSetup.Rows.Count()
            Catch e As System.Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, Nothing)
                        idaTest.Fill(tblSetup)
                    Catch ex As Exception
                        Me.ThrowGADSNGException("Error in GetUtilityUnitData", ex)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetUtilityUnitData", e)
                End If

            End Try

            Return tblSetup

        End Function

#End Region

#Region " GetSelectedUtilityUnitData(ByVal strWho As String, ByVal myUser As String) As Setup.SetupDataTable "

        Public Function GetSelectedUtilityUnitData(ByVal strWho As String, ByVal myUser As String) As Setup.SetupDataTable

            ' strWho is the ISO or NERC and has the following values:  NERC, NYISO, PJM, ISONE

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim intRows As Int32

            Dim parms(1, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters

            Select Case strWho
                Case "NERC"
                    stringSelect = "SELECT * FROM Setup WHERE (NERC = @NERC)"
                    parms(0, 0) = "@NERC"
                    parms(0, 1) = "NERC, true"
                Case "NYISO"
                    stringSelect = "SELECT * FROM Setup WHERE (NYISO = @NYISO)"
                    parms(0, 0) = "@NYISO"
                    parms(0, 1) = "NYISO, true"
                Case "PJM"
                    stringSelect = "SELECT * FROM Setup WHERE (PJM = @PJM)"
                    parms(0, 0) = "@PJM"
                    parms(0, 1) = "PJM, true"
                Case "ISONE"
                    stringSelect = "SELECT * FROM Setup WHERE (ISONE = @ISONE)"
                    parms(0, 0) = "@ISONE"
                    parms(0, 1) = "ISONE, true"
                Case Else
                    stringSelect = "SELECT * FROM Setup WHERE (CheckStatus <> @CheckStatus)"
                    parms(0, 0) = "@CheckStatus"
                    parms(0, 1) = "CheckStatus, Q"
            End Select

            If myUser = "DASHBOARD" Then
                stringSelect += " ORDER BY UtilityUnitCode"
            Else
                stringSelect += " AND UtilityUnitCode IN (SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID IN (SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & myUser & "')) ORDER BY UtilityUnitCode"
            End If

            Dim tblSetup As Setup.SetupDataTable
            tblSetup = New Setup.SetupDataTable

            idaTest = Factory.GetDataAdapter(stringSelect, parms)

            Try
                idaTest.Fill(tblSetup)
                intRows = tblSetup.Rows.Count()
            Catch e As System.Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(tblSetup)
                        intRows = tblSetup.Rows.Count()
                    Catch ex As Exception
                        Me.ThrowGADSNGException("Error in GetUtilityUnitData", ex)
                        intRows = 0
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetUtilityUnitData", e)
                    intRows = 0
                End If

            End Try

            Return tblSetup

        End Function

#End Region


        Public Function GetCFEEquiposR3(ByVal intCentralASARE As Integer, ByVal intUnit As Integer) As dsCFEEquiposR3.CFEEquiposR3DataTable

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim intRows As Int32

            Dim parms(2, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters
            parms(0, 0) = "@CCCC"
            parms(0, 1) = "CCCC, " + intCentralASARE.ToString
            parms(1, 0) = "@U"
            parms(1, 1) = "U, " + intUnit.ToString

            'Dim dsTemp As dsCFEEquiposR3
            Dim tblTemp As dsCFEEquiposR3.CFEEquiposR3DataTable

            tblTemp = New dsCFEEquiposR3.CFEEquiposR3DataTable

            stringSelect = "SELECT CCCC, U, SS, TTTT, NNNN, (CONVERT(char (4), TTTT) + ' - ' + EquipmentDesc) AS EquipmentDesc FROM CFEEquiposR3 WHERE (CCCC = @CCCC) AND (U = @U)"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.Fill(tblTemp)
            Catch e As Exception
                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(tblTemp)
                    Catch ex As Exception
                        Me.ThrowGADSNGException("Error in GetCFEEquiposR3", ex)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetCFEEquiposR3", e)
                End If

            End Try

            intRows = tblTemp.Rows.Count()

            Return tblTemp

        End Function


        Public Function GetCauseCodeToCFEPMSYS() As dsCFECauseCodeToPMSYS.CFECauseCodeToPMSYSDataTable

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim intRows As Int32

            'Dim dsTemp As dsCFECauseCodeToPMSYS
            Dim tblTemp As dsCFECauseCodeToPMSYS.CFECauseCodeToPMSYSDataTable

            tblTemp = New dsCFECauseCodeToPMSYS.CFECauseCodeToPMSYSDataTable

            stringSelect = "SELECT * FROM CFECauseCodeToPMSYS ORDER BY CauseCode"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, Nothing)
                idaTest.Fill(tblTemp)
            Catch e As Exception
                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, Nothing)
                        idaTest.Fill(tblTemp)
                    Catch ex As Exception
                        Me.ThrowGADSNGException("Error in GetCauseCodeToCFEPMSYS", ex)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetCauseCodeToCFEPMSYS", e)
                End If

            End Try

            intRows = tblTemp.Rows.Count()

            Return tblTemp

        End Function


        Public Function GetISOUnitData(ByVal strWho As String) As Setup.SetupDataTable

            Dim stringSelect As String = String.Empty
            Dim intRows As Integer
            Dim idaTest As DbDataAdapter

            Select Case strWho
                Case "NERC"
                    stringSelect = "SELECT * FROM Setup WHERE (NERC <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "NYISO"
                    stringSelect = "SELECT * FROM Setup WHERE (NYISO <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "ISONE"
                    stringSelect = "SELECT * FROM Setup WHERE (ISONE <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "CEA"
                    stringSelect = "SELECT * FROM Setup WHERE (CEA <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "GenElect"
                    stringSelect = "SELECT * FROM Setup WHERE (GenElect <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "Siemens"
                    stringSelect = "SELECT * FROM Setup WHERE (Siemens <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "CAISO"
                    stringSelect = "SELECT * FROM Setup WHERE (CAISO <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "MISO"
                    stringSelect = "SELECT * FROM Setup WHERE (MISO <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "ISO1"
                    stringSelect = "SELECT * FROM Setup WHERE (ISO1 <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "ISO2"
                    stringSelect = "SELECT * FROM Setup WHERE (ISO2 <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case "ISO3"
                    stringSelect = "SELECT * FROM Setup WHERE (ISO3 <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
                Case Else

            End Select

            Dim tblSetup As Setup.SetupDataTable
            tblSetup = New Setup.SetupDataTable

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, Nothing)
                idaTest.Fill(tblSetup)
                intRows = tblSetup.Rows.Count()
            Catch e As System.Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, Nothing)
                        idaTest.Fill(tblSetup)
                        intRows = tblSetup.Rows.Count()
                    Catch ex As Exception
                        Me.ThrowGADSNGException("Error in GetISOUnitData()", ex)
                        intRows = 0
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetISOUnitData()", e)
                    intRows = 0
                End If

            End Try

            Return tblSetup

        End Function

        Public Function GetSHMethodReader() As IDataReader

            Dim stringSelect As String
            stringSelect = "SELECT UtilityUnitCode, ServiceHourMethod, EffStartDate FROM SHMethod ORDER BY UtilityUnitCode, EffStartDate"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

#Region " GetCurrentMonth(ByVal strUnit As String) As String "

        Public Function GetCurrentMonth(ByVal strUnit As String, ByVal GetFromDictionary As HybridDictionary) As String

            Dim intMaxYearP As Integer = System.DateTime.Now.Year
            Dim intMaxMonthP As Integer = 1
            Dim intMaxYearE As Integer = System.DateTime.Now.Year
            Dim intMaxMonthE As Integer = 1
            Dim intCurrentMonth As Integer = 1
            Dim intCurrentYear As Integer = System.DateTime.Now.Year
            'Dim intNoOfRows As Int32
            Dim strMaxPeriod As String = String.Empty
            Dim arrayOutput As New ArrayList
            Dim objTest As Object

            Dim stringSelect As String
            Dim parms(2, 2) As String

            intMaxMonthP = 0
            intMaxYearP = System.DateTime.Now.Year

            stringSelect = "SELECT MAX(Year) FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode)"

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit

            objTest = Factory.ExecuteScalar(stringSelect, parms)

            If Not IsDBNull(objTest) Then

                intMaxYearP = CInt(objTest)

                parms(1, 0) = "@Year"
                parms(1, 1) = "Year, " + intMaxYearP.ToString

                stringSelect = "SELECT MAX(Period) FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"
                strMaxPeriod = CStr(Factory.ExecuteScalar(stringSelect, parms))

                ' strMinPeriod will be one of two formats:  01-12 or X1-X4 since this is coming from Performance Data

                If strMaxPeriod.StartsWith("X") = True Then
                    intMaxMonthP = 3 * (CInt(strMaxPeriod.Substring(1)) - 1) + 1
                Else
                    intMaxMonthP = (CInt(strMaxPeriod))
                End If

            End If

            ' ===========================
            ' Looking through Event Data
            ' ===========================

            Dim objEvnt As Object
            intMaxYearE = intMaxYearP
            intMaxMonthE = 0

            stringSelect = "SELECT MAX(StartDateTime) FROM EventData01 WHERE ( UtilityUnitCode = @UtilityUnitCode )"

            Dim parms2(1, 2) As String
            parms2(0, 0) = "@UtilityUnitCode"
            parms2(0, 1) = "UtilityUnitCode, " + strUnit

            objEvnt = Factory.ExecuteScalar(stringSelect, parms2)

            If Not IsDBNull(objEvnt) Then

                intMaxYearE = CDate(objEvnt).Year
                intMaxMonthE = CDate(objEvnt).Month

                stringSelect = "SELECT MAX(EndDateTime) FROM EventData01 WHERE ( UtilityUnitCode = @UtilityUnitCode ) AND EndDateTime IS NOT NULL"

                objEvnt = Factory.ExecuteScalar(stringSelect, parms2)

                If Not IsDBNull(objEvnt) Then

                    If CDate(objEvnt).Year > intMaxYearE Then
                        intMaxYearE = CDate(objEvnt).Year
                        intMaxMonthE = CDate(objEvnt).Month
                    ElseIf CDate(objEvnt).Year = intMaxYearE Then
                        If CDate(objEvnt).Month > intMaxMonthE Then
                            intMaxMonthE = CDate(objEvnt).Month
                        End If
                    End If

                End If

            End If

            If intMaxYearP > intMaxYearE Then
                intCurrentYear = intMaxYearP
                intCurrentMonth = intMaxMonthP
            ElseIf intMaxYearP = intMaxYearE Then
                intCurrentYear = intMaxYearP
                If intMaxMonthP > intMaxMonthE Then
                    intCurrentMonth = intMaxMonthP
                Else
                    intCurrentMonth = intMaxMonthE
                End If
            Else
                intCurrentYear = intMaxYearE
                intCurrentMonth = intMaxMonthE
            End If

            Select Case intCurrentMonth
                Case 0, 1
                    strMaxPeriod = "January, "
                Case 2
                    strMaxPeriod = "February, "
                Case 3
                    strMaxPeriod = "March, "
                Case 4
                    strMaxPeriod = "April, "
                Case 5
                    strMaxPeriod = "May, "
                Case 6
                    strMaxPeriod = "June, "
                Case 7
                    strMaxPeriod = "July, "
                Case 8
                    strMaxPeriod = "August, "
                Case 9
                    strMaxPeriod = "September, "
                Case 10
                    strMaxPeriod = "October, "
                Case 11
                    strMaxPeriod = "November, "
                Case 12
                    strMaxPeriod = "December, "
            End Select

            If strMaxPeriod.IndexOf("-") > 0 Then
                strMaxPeriod = strMaxPeriod.Trim.Substring(strMaxPeriod.IndexOf("-") + 1) & " "
            End If

            Return strMaxPeriod + intCurrentYear.ToString

        End Function

#End Region

#Region " GetPerfDataRange(ByVal strUnit As String) As ArrayList "

        Public Function GetPerfDataRange(ByVal strUnit As String) As ArrayList

            Dim intMinYear As Integer
            Dim intMinMonth As Integer
            Dim intNoOfRows As Int32
            Dim strMinPeriod As String
            Dim arrayOutput As New ArrayList
            Dim objTest As Object

            Dim stringSelect As String

            intMinMonth = 1
            intMinYear = Now.Year     ' do not convert to UtcNow

            If Now.Month = 1 Then     ' do not convert to UtcNow
                ' If this is January, then they might be wanting to load up prior year 
                ' since they can't load up any January data -- too soon
                intMinYear = Now.Year - 1     ' do not convert to UtcNow

            End If

            stringSelect = "SELECT COUNT(*) FROM PerformanceData"
            intNoOfRows = CInt(Factory.ExecuteScalar(stringSelect, Nothing))

            If intNoOfRows > 0 Then

                ' Table is NOT empty

                stringSelect = "SELECT MIN(Year) FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode)"

                Dim parms(2, 2) As String
                parms(0, 0) = "@UtilityUnitCode"
                parms(0, 1) = "UtilityUnitCode, " + strUnit

                objTest = Factory.ExecuteScalar(stringSelect, parms)

                If IsDBNull(objTest) Then

                    ' There is performance data in the database, but not for this unit

                    arrayOutput.Add(intMinYear)
                    arrayOutput.Add(intMinMonth)

                    Return arrayOutput

                End If

                intMinYear = CInt(objTest)

                parms(1, 0) = "@Year"
                parms(1, 1) = "Year, " + intMinYear.ToString

                stringSelect = "SELECT MIN(Period) FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"
                strMinPeriod = CStr(Factory.ExecuteScalar(stringSelect, parms))
                ' strMinPeriod will be one of two formats:  01-12 or X1-X4 since this is coming from Performance Data

                If strMinPeriod.StartsWith("X") = True Then
                    intMinMonth = 3 * (CInt(strMinPeriod.Substring(1)) - 1) + 1
                Else
                    intMinMonth = (CInt(strMinPeriod))
                End If

            End If

            stringSelect = "SELECT COUNT(*) FROM EventData01"
            intNoOfRows = CInt(Factory.ExecuteScalar(stringSelect, Nothing))

            If intNoOfRows > 0 Then

                ' Table is NOT empty
                Dim dtTest As DateTime

                stringSelect = "SELECT MIN(StartDateTime) FROM EventData01 WHERE (UtilityUnitCode = @UtilityUnitCode)"

                Dim parms(2, 2) As String
                parms(0, 0) = "@UtilityUnitCode"
                parms(0, 1) = "UtilityUnitCode, " + strUnit

                objTest = Factory.ExecuteScalar(stringSelect, parms)

                If Not IsDBNull(objTest) Then

                    dtTest = Convert.ToDateTime(objTest)

                    If ((dtTest.Year * 100) + dtTest.Month) < ((intMinYear * 100) + intMinMonth) Then
                        intMinYear = dtTest.Year
                        intMinMonth = dtTest.Month
                    End If

                End If

            End If

            arrayOutput.Add(intMinYear)
            arrayOutput.Add(intMinMonth)

            Return arrayOutput

        End Function

#End Region

#Region " GetPerformanceData(ByVal strUnit As String, ByVal intYear As Integer) As Performance.PerformanceDataDataTable "

        Public Function GetPerformanceData(ByVal strUnit As String, ByVal intYear As Integer) As Performance.PerformanceDataDataTable

            '   intChoice 1 - Year-to-date - intPeriod = ending month
            '             2 - Quarterly    - intPeriod = quarter
            '             3 - Monthly      - intPeriod = specific month
            '   intChoice is NOT 0-based, but is 1-based so that the number corresponds to either the ending month, quarter or specific month

            '   intYear   is the Year

            '   intPeriod is the ending month, which quarter, or which specific month (see intChoice above)

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim intRows As Int32

            Dim parms(2, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters
            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            'Dim dsPerformance As Performance
            Dim tblPerformance As Performance.PerformanceDataDataTable

            tblPerformance = New Performance.PerformanceDataDataTable

            stringSelect = "SELECT * FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) ORDER BY Period"

            Try

                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.Fill(tblPerformance)

                intRows = tblPerformance.Rows.Count()

            Catch e As System.Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(tblPerformance)

                        intRows = tblPerformance.Rows.Count()
                    Catch ex As Exception
                        Me.ThrowGADSNGException("Error in GetPerformanceData", ex)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetPerformanceData", e)
                End If

            End Try

            tblPerformance.AcceptChanges()

            Return tblPerformance

        End Function

        Public Function GetCustomPerformanceData(ByVal strUnit As String, ByVal intYear As Integer) As CustomPerfData.CustomPerfDataDataTable

            '   intChoice 1 - Year-to-date - intPeriod = ending month
            '             2 - Quarterly    - intPeriod = quarter
            '             3 - Monthly      - intPeriod = specific month
            '   intChoice is NOT 0-based, but is 1-based so that the number corresponds to either the ending month, quarter or specific month

            '   intYear   is the Year

            '   intPeriod is the ending month, which quarter, or which specific month (see intChoice above)

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim intRows As Int32

            Dim parms(2, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters
            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            Dim myCustomPerfData As CustomPerfData.CustomPerfDataDataTable = New CustomPerfData.CustomPerfDataDataTable

            If DoesTableExist("CustomPerfData") Then

                stringSelect = "SELECT * FROM CustomPerfData WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) ORDER BY Period"

                Try

                    idaTest = Factory.GetDataAdapter(stringSelect, parms)
                    idaTest.Fill(myCustomPerfData)
                    myCustomPerfData.TableName = "CustomPerfData"

                    intRows = myCustomPerfData.Rows.Count()

                Catch e As System.Exception

                    If Factory.Provider = "SqlClient" Then

                        Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                        Try
                            idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                            idaTest.Fill(myCustomPerfData)

                            intRows = myCustomPerfData.Rows.Count()
                        Catch ex As Exception
                            Me.ThrowGADSNGException("Error in GetCustomPerformanceData", ex)
                        End Try
                    Else
                        Me.ThrowGADSNGException("Error in GetCustomPerformanceData", e)
                    End If

                End Try

                myCustomPerfData.AcceptChanges()

            End If

            Return myCustomPerfData

        End Function

        Public Function GetPerformanceINTData(ByVal strUnit As String, ByVal intYear As Integer) As PerformanceINT.PerformanceDataDataTable
            ' this function was created for the ISO NE Portal before they changed to the "decimal" data
            ' types for the Generation and Capacities

            ' Once or if they adopt the new format, this function is no longer required

            '   intChoice 1 - Year-to-date - intPeriod = ending month
            '             2 - Quarterly    - intPeriod = quarter
            '             3 - Monthly      - intPeriod = specific month
            '   intChoice is NOT 0-based, but is 1-based so that the number corresponds to either the ending month, quarter or specific month

            '   intYear   is the Year

            '   intPeriod is the ending month, which quarter, or which specific month (see intChoice above)

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim intRows As Int32

            Dim parms(2, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters
            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            'Dim dsPerformance As PerformanceINT
            Dim tblPerformance As PerformanceINT.PerformanceDataDataTable

            tblPerformance = New PerformanceINT.PerformanceDataDataTable

            stringSelect = "SELECT * FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) ORDER BY Period"

            Try

                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.Fill(tblPerformance)

                intRows = tblPerformance.Rows.Count()

            Catch e As System.Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(tblPerformance)
                        intRows = tblPerformance.Rows.Count()
                    Catch ex As Exception
                        Me.ThrowGADSNGException("Error in GetPerformanceData", ex)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetPerformanceData", e)
                End If

            End Try

            tblPerformance.AcceptChanges()

            Return tblPerformance

        End Function

#End Region

#Region " FindMissingMonths(ByVal dtSetup As Setup.SetupDataTable, ByVal intYear As Integer, ByVal strPeriod As String) "

        Public Sub FindMissingMonths(ByVal dtSetup As Setup.SetupDataTable, ByVal intYear As Integer, ByVal strPeriod As String, ByRef GetFromDictionary As HybridDictionary)

            Dim stringSelect As String
            Dim drMyRows As Setup.SetupRow
            Dim strHoursMatchMsg As String = "Performance data has not been created for indicated month"
            Dim intRows As Int32
            Dim strSeverity As String = "M"
            Dim dtComm As DateTime = System.DateTime.Now     ' do not convert to UtcNow
            Dim dtRetire As DateTime = System.DateTime.Now   ' do not convert to UtcNow
            Dim dtRetirementDateTime As DateTime
            Dim lProcess As Boolean = True

            Dim intUpdateRows As Int32
            Dim updateStatement As String

            Dim parms(2, 2) As String
            Dim parmsD(4, 2) As String

            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters

            ' For the stringSelect

            parms(0, 0) = "@Year"
            parms(0, 1) = "Year, " + intYear.ToString

            parms(1, 0) = "@Period"
            parms(1, 1) = "Period, " + strPeriod

            ' For the updateStatement

            parmsD(0, 0) = "@UtilityUnitCode"

            parmsD(1, 0) = "@Year"
            parmsD(1, 1) = "Year, " + intYear.ToString

            parmsD(2, 0) = "@Period"
            parmsD(2, 1) = "Period, " + strPeriod

            parmsD(3, 0) = "@ErrorSeverity"
            parmsD(3, 1) = "ErrorSeverity, " + strSeverity

            Dim myArrayList As New ArrayList
            Dim myReader As IDataReader = Nothing

            stringSelect = "SELECT UtilityUnitCode FROM PerformanceData WHERE (Year = @Year) AND (Period = @Period)"

            Try
                myReader = Me.Factory.ExecuteDataReader(stringSelect, parms, CommandBehavior.CloseConnection)

                Do While myReader.Read
                    myArrayList.Add(myReader.GetString(0))
                Loop

            Catch ex As Exception

            Finally
                myReader.Close()
                myReader.Dispose()
            End Try

            Dim myErrorList As New ArrayList
            Dim myErrorReader As IDataReader = Nothing

            stringSelect = "SELECT UtilityUnitCode FROM Errors WHERE (Year = @Year) AND (Period = @Period) AND (ErrorSeverity = 'M')"

            Try
                myErrorReader = Me.Factory.ExecuteDataReader(stringSelect, parms, CommandBehavior.CloseConnection)

                Do While myErrorReader.Read
                    myErrorList.Add(myErrorReader.GetString(0))
                Loop

            Catch ex As Exception

            Finally
                myErrorReader.Close()
                myErrorReader.Dispose()
            End Try

            For Each drMyRows In dtSetup.Rows

                If drMyRows.IsCommercialDateNull Then
                    dtComm = Convert.ToDateTime("01/01/1948")
                Else
                    dtComm = drMyRows.CommercialDate
                End If

                If drMyRows.IsRetirementDateNull Then
                    dtRetire = System.DateTime.Now     ' do not convert to UtcNow
                Else
                    dtRetirementDateTime = DateTime.Parse(drMyRows.RetirementDate.ToShortDateString & " 23:59:59")
                    dtRetire = dtRetirementDateTime
                End If

                If IsNumeric(strPeriod) Then
                    If ((intYear > dtComm.Year Or (intYear = dtComm.Year And Convert.ToInt16(strPeriod) >= dtComm.Month)) And _
                      (intYear < dtRetire.Year Or (intYear = dtRetire.Year And Convert.ToInt16(strPeriod) <= dtRetire.Month))) Then
                        ' current month is between the commercial date and the retirement date
                        lProcess = True
                    Else
                        lProcess = False
                    End If
                Else
                    lProcess = True
                End If

                parmsD(0, 1) = "UtilityUnitCode, " + drMyRows.Item("UtilityUnitCode").ToString

                updateStatement = "DELETE FROM Errors WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) AND (Period = @Period) AND (ErrorSeverity = @ErrorSeverity)"

                intRows = myArrayList.IndexOf(drMyRows.Item("UtilityUnitCode").ToString)

                If intRows = -1 Then

                    ' the UtilityUnitCode is not in the Performance data for this month

                    intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parmsD)

                    If lProcess Then
                        LoadErrors(drMyRows.UtilityUnitCode, intYear, strPeriod, Nothing, Nothing, strHoursMatchMsg, strSeverity, drMyRows.UnitShortName, GetFromDictionary)
                    End If

                Else

                    intUpdateRows = myErrorList.IndexOf(drMyRows.Item("UtilityUnitCode").ToString)

                    If intUpdateRows <> -1 Then
                        ' the record is in the performance data and in the errors table so remove the message from the Errors table
                        intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parmsD)
                    End If

                End If

            Next

        End Sub

#End Region

#Region " GetMyErrors(ByVal dtSetup As Setup.SetupDataTable) As DataSet "

        Public Function GetMyErrors(ByVal myUser As String) As DataSet

            'Dim idaTest As IDataAdapter
            Dim stringSelect As String
            'Dim drMyRows As DataRow
            Dim dsMyErrors As New DataSet
            'Dim dtMyErrors As DataTable

            'Dim parms(1, 2) As String
            'parms(0, 0) = "@UtilityUnitCode"

            stringSelect = "SELECT * FROM Errors WHERE UtilityUnitCode IN (SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID IN (SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & myUser & "')) ORDER BY UtilityUnitCode"
            dsMyErrors = Factory.GetDataSet(stringSelect, Nothing)

            'For Each drMyRows In dtSetup.Rows
            '	parms(0, 1) = "UtilityUnitCode, " + drMyRows.Item("UtilityUnitCode").ToString
            '	stringSelect = "SELECT * FROM Errors WHERE (UtilityUnitCode = @UtilityUnitCode)"
            '	dtMyErrors = Factory.GetDataTable(stringSelect, parms)
            '	dsMyErrors.Merge(dtMyErrors)
            'Next

            Try
                dsMyErrors.Tables(0).TableName = "Errors"

            Catch ex As Exception

                Dim tbl As New DataTable("Errors")

                Dim col As DataColumn = tbl.Columns.Add("UtilityUnitCode", GetType(Char))
                col.AllowDBNull = False
                col = tbl.Columns.Add("Year", GetType(Int16))
                col.AllowDBNull = False
                col = tbl.Columns.Add("Period", GetType(Char))
                col.AllowDBNull = True
                col = tbl.Columns.Add("EventNumber", GetType(Int16))
                col.AllowDBNull = True
                col = tbl.Columns.Add("EventEvenCardNo", GetType(Byte))
                col.AllowDBNull = True
                col = tbl.Columns.Add("ErrorMessage", GetType(String))
                col.MaxLength = 80
                col.AllowDBNull = True
                col = tbl.Columns.Add("ErrorSeverity", GetType(Char))
                col.AllowDBNull = True
                col = tbl.Columns.Add("UnitShortName", GetType(Char))
                col.AllowDBNull = True

                dsMyErrors.Tables.Add(tbl)

            End Try

            dsMyErrors.AcceptChanges()
            dsMyErrors.Tables("Errors").DefaultView.Sort = "UnitShortName, Year, Period, EventNumber, EventEvenCardNo"

            Return dsMyErrors

        End Function

#End Region

#Region " GetBothEventSets(ByVal strUnit As String, ByVal intYear As Integer) As AllEventData "

        Public Function GetBothEventSets(ByVal strUnit As String, ByVal intYear As Integer) As AllEventData
            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim parms(2, 2) As String
            Dim intRows As Int32
            Dim EventDetailsRel As DataRelation
            Dim dsAllEvents As AllEventData
            dsAllEvents = New AllEventData

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            Dim tblEvent01 As AllEventData.EventData01DataTable
            Dim tblEvent02 As AllEventData.EventData02DataTable

            tblEvent01 = New AllEventData.EventData01DataTable
            tblEvent02 = New AllEventData.EventData02DataTable
            stringSelect = "SELECT * FROM EventData01 WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) ORDER BY EventNumber"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.Fill(tblEvent01)
                'intRows = tblEvent01.Rows.Count()
            Catch ex As Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(tblEvent01)
                    Catch e As Exception

                    End Try

                End If

            End Try

            stringSelect = "SELECT * FROM EventData02 WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) ORDER BY EventNumber, EvenCardNumber"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                intRows = Factory.ExecuteNonQuery("UPDATE EventData02 SET VerbalDesc1 = 'NA' WHERE (VerbalDesc1 IS NULL)", Nothing)
                idaTest.Fill(tblEvent02)
                'intRows = tblEvent02.Rows.Count()
            Catch ex As Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        intRows = Factory.ExecuteNonQuery("UPDATE EventData02 SET VerbalDesc1 = 'NA' WHERE (VerbalDesc1 IS NULL)", Nothing)
                        idaTest.Fill(tblEvent02)
                    Catch e As Exception
                        stringSelect = e.ToString
                    End Try
                Else
                    stringSelect = ex.ToString
                End If

            End Try

            dsAllEvents.Merge(tblEvent01)
            dsAllEvents.Merge(tblEvent02)
            'dsAllEvents.Tables.Add(tblEvent01)
            'dsAllEvents.Tables.Add(tblEvent02)

            EventDetailsRel = New DataRelation("AdditionalWork", _
            dsAllEvents.EventData01.Columns("EventNumber"), _
            dsAllEvents.EventData02.Columns("EventNumber"))

            dsAllEvents.Relations.Add(EventDetailsRel)

            Return dsAllEvents

        End Function

#End Region

#Region " GetBothEventSetsGrid(ByVal strUnit As String, ByVal intYear As Integer) As DataSet "

        Public Function GetBothEventSetsGrid(ByVal strUnit As String, ByVal intYear As Integer) As DataSet
            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim parms(2, 2) As String
            Dim EventDetailsRel As DataRelation
            Dim dsAllEvents As DataSet
            dsAllEvents = New DataSet

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            stringSelect = "SELECT UtilityUnitCode, Year, EventNumber, RevisionCard01, EventType, " & _
              "StartDateTime, CarryOverLastYear, ChangeDateTime1, ChangeInEventType1, " & _
              "ChangeDateTime2, ChangeInEventType2, EndDateTime, CarryOverNextYear, " & _
              "GrossAvailCapacity, NetAvailCapacity, CauseCode, CauseCodeExt, " & _
              "WorkStarted, WorkEnded, ContribCode, PrimaryAlert, ManhoursWorked, " & _
              "RevisionCard02, RevisionCard03, " & _
              "RevMonthCard01, RevMonthCard02, RevMonthCard03, EditFlag, " & _
              "VerbalDescFull, TimeStamp, VerbalDesc86, " & _
              "FailureMechCode, TripMech, CumFiredHours, CumEngineStarts, DominantDerate, PJMIOCode " & _
              "FROM EventData01 " & _
              "WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) " & _
              "ORDER BY StartDateTime DESC, EndDateTime DESC"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.TableMappings.Add("Table", "EventData01")
                idaTest.Fill(dsAllEvents, "EventData01")

            Catch ex As System.Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.TableMappings.Add("Table", "EventData01")
                        idaTest.Fill(dsAllEvents, "EventData01")
                    Catch e As Exception
                        Me.ThrowGADSNGException("Error in GetBothEventSetsGrid: " & strUnit & " - " & intYear.ToString, e)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetBothEventSetsGrid: " & strUnit & " - " & intYear.ToString, ex)
                End If

            End Try


            stringSelect = "SELECT  UtilityUnitCode, Year, EventNumber, RevisionCardEven, EventType, " & _
              "CauseCode, CauseCodeExt, WorkStarted, WorkEnded, ContribCode, PrimaryAlert, " & _
              "ManhoursWorked, EvenCardNumber, " & _
              "RevisionCardOdd, RevMonthCardEven, RevMonthCardOdd, EditFlag, VerbalDescFull, TimeStamp, " & _
              "VerbalDesc86, " & _
              "FailureMechCode, TripMech, CumFiredHours, CumEngineStarts " & _
              "FROM EventData02 " & _
              "WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) " & _
              "ORDER BY EventNumber, EvenCardNumber"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.Fill(dsAllEvents, "EventData02")
            Catch ex As Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(dsAllEvents, "EventData02")
                    Catch e As Exception
                        Me.ThrowGADSNGException("Error in GetBothEventSetsGrid02: " & strUnit & " - " & intYear.ToString, e)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetBothEventSetsGrid02: " & strUnit & " - " & intYear.ToString, ex)
                End If

            End Try


            EventDetailsRel = New DataRelation("AdditionalWork", _
            dsAllEvents.Tables("EventData01").Columns("EventNumber"), _
            dsAllEvents.Tables("EventData02").Columns("EventNumber"))

            dsAllEvents.Relations.Add(EventDetailsRel)

            Return dsAllEvents

        End Function

        Public Function GetBothEventINTSetsGrid(ByVal strUnit As String, ByVal intYear As Integer) As DataSet
            ' this function was created for the ISO NE Portal before they changed to the "decimal" data
            ' types for the Available Capacities
            ' Once or if they adopt the new format, this function is no longer required

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            Dim parms(2, 2) As String
            Dim EventDetailsRel As DataRelation
            Dim dsAllEvents As DataSet
            dsAllEvents = New DataSet
            Dim dsAllEventsINT As New AllEventDataINT

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            stringSelect = "SELECT UtilityUnitCode, Year, EventNumber, RevisionCard01, EventType, " & _
                "StartDateTime, CarryOverLastYear, ChangeDateTime1, ChangeInEventType1, " & _
                "ChangeDateTime2, ChangeInEventType2, EndDateTime, CarryOverNextYear, " & _
                "GrossAvailCapacity, NetAvailCapacity, CauseCode, CauseCodeExt, " & _
                "WorkStarted, WorkEnded, ContribCode, PrimaryAlert, ManhoursWorked, " & _
                "RevisionCard02, RevisionCard03, " & _
                "RevMonthCard01, RevMonthCard02, RevMonthCard03, EditFlag, " & _
                "VerbalDescFull, TimeStamp, VerbalDesc86, " & _
                "FailureMechCode, TripMech, CumFiredHours, CumEngineStarts, DominantDerate, PJMIOCode " & _
                "FROM EventData01 " & _
                "WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) " & _
                "ORDER BY StartDateTime DESC, EndDateTime DESC"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.TableMappings.Add("Table", "EventData01")
                idaTest.Fill(dsAllEventsINT, "EventData01")

                dsAllEvents.Merge(dsAllEventsINT.Tables("EventData01"))

            Catch ex As System.Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.TableMappings.Add("Table", "EventData01")
                        idaTest.Fill(dsAllEventsINT, "EventData01")

                        dsAllEvents.Merge(dsAllEventsINT.Tables("EventData01"))
                    Catch e As Exception
                        Me.ThrowGADSNGException("Error in GetBothEventINTSetsGrid: " & strUnit & " - " & intYear.ToString, e)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetBothEventINTSetsGrid: " & strUnit & " - " & intYear.ToString, ex)
                End If

            End Try


            stringSelect = "SELECT  UtilityUnitCode, Year, EventNumber, RevisionCardEven, EventType, " & _
                "CauseCode, CauseCodeExt, WorkStarted, WorkEnded, ContribCode, PrimaryAlert, " & _
                "ManhoursWorked, EvenCardNumber, " & _
                "RevisionCardOdd, RevMonthCardEven, RevMonthCardOdd, EditFlag, VerbalDescFull, TimeStamp, " & _
                "VerbalDesc86, " & _
                "FailureMechCode, TripMech, CumFiredHours, CumEngineStarts " & _
                "FROM EventData02 " & _
                "WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year) " & _
                "ORDER BY EventNumber, EvenCardNumber"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                'idaTest.Fill(dsAllEvents, "EventData02")
                idaTest.TableMappings.Add("Table", "EventData02")
                idaTest.Fill(dsAllEventsINT, "EventData02")
                dsAllEvents.Merge(dsAllEventsINT.Tables("EventData02"))
            Catch ex As Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        'idaTest.Fill(dsAllEvents, "EventData02")
                        idaTest.TableMappings.Add("Table", "EventData02")
                        idaTest.Fill(dsAllEventsINT, "EventData02")
                        dsAllEvents.Merge(dsAllEventsINT.Tables("EventData02"))
                    Catch e As Exception
                        Me.ThrowGADSNGException("Error in GetBothEventINTSetsGrid02: " & strUnit & " - " & intYear.ToString, e)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetBothEventINTSetsGrid02: " & strUnit & " - " & intYear.ToString, ex)
                End If

            End Try

            EventDetailsRel = New DataRelation("AdditionalWork", _
                dsAllEvents.Tables("EventData01").Columns("EventNumber"), _
                dsAllEvents.Tables("EventData02").Columns("EventNumber"))

            Try
                dsAllEvents.Relations.Add(EventDetailsRel)
            Catch ex As Exception
                Me.ThrowGADSNGException("Error in GetBothEventINTSetsGrid Add Relation: " & strUnit & " - " & intYear.ToString, ex)
            End Try

            Return dsAllEvents

        End Function

#End Region

#Region " GetEvent01Data(ByVal strUnit As String, ByVal intYear As Integer) As Event01.EventData01DataTable "

        Public Function GetEvent01Data(ByVal strUnit As String, ByVal intYear As Integer) As Event01.EventData01DataTable

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            'Dim intRows As Int32

            Dim parms(2, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters
            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            'Dim dsEvent01 As Event01
            Dim tblEvent01 As Event01.EventData01DataTable

            tblEvent01 = New Event01.EventData01DataTable

            stringSelect = "SELECT * FROM EventData01 WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.Fill(tblEvent01)
            Catch ex As Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(tblEvent01)
                    Catch e As Exception
                        Me.ThrowGADSNGException("Error in GetEvent01Data: " & strUnit & " - " & intYear.ToString, e)
                    End Try

                Else
                    Me.ThrowGADSNGException("Error in GetEvent01Data: " & strUnit & " - " & intYear.ToString, ex)
                End If

            End Try

            'intRows = tblEvent01.Rows.Count()

            Return tblEvent01

        End Function

#End Region

#Region " GetEvent02Data(ByVal strUnit As String, ByVal intYear As Integer) As Event0299.EventData02DataTable "

        Public Function GetEvent02Data(ByVal strUnit As String, ByVal intYear As Integer) As Event0299.EventData02DataTable

            Dim idaTest As DbDataAdapter
            Dim stringSelect As String
            'Dim intRows As Int32

            Dim parms(2, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters
            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + strUnit
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            'Dim dsEvent02 As Event0299
            Dim tblEvent02 As Event0299.EventData02DataTable

            tblEvent02 = New Event0299.EventData02DataTable

            stringSelect = "SELECT * FROM EventData02 WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"
            Try
                idaTest = Factory.GetDataAdapter(stringSelect, parms)
                idaTest.Fill(tblEvent02)
            Catch ex As Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, parms)
                        idaTest.Fill(tblEvent02)
                    Catch e As Exception
                        Me.ThrowGADSNGException("Error in GetEvent02Data: " & strUnit & " - " & intYear.ToString, e)
                    End Try

                Else
                    Me.ThrowGADSNGException("Error in GetEvent02Data: " & strUnit & " - " & intYear.ToString, ex)
                End If

            End Try

            'intRows = tblEvent02.Rows.Count()

            Return tblEvent02

        End Function

#End Region


#Region " UpdateSetupCheckStatus(ByVal dsSetup As DataSet) As Boolean "

        Private Function UpdateSetupCheckStatus(ByVal dsSetup As DataSet) As Boolean

            Dim parms(3, 2) As String
            Dim drCurrent As DataRow
            Dim intUpdateRows As Integer
            Dim updateStatement As String

            updateStatement = "UPDATE Setup SET CheckStatus = @Status " & _
             "WHERE " & _
             "(CheckStatus = @OKStatus) AND " & _
             "(UtilityUnitCode = @UtilityUnitCode)"

            ' Sets the Status to R - Revised
            ' Data for this unit has been revised, but before running Error Check

            parms(0, 0) = "@Status"
            parms(0, 1) = "CheckStatus, R"

            ' If the unit status is set to O - OK, then there are no revisions or errors
            ' If the error already has an error then don't set it to R -- only the ones it thinks are OK

            parms(1, 0) = "@OKStatus"
            parms(1, 1) = "CheckStatus, O"

            parms(2, 0) = "@UtilityUnitCode"

            For Each drCurrent In dsSetup.Tables(0).Rows

                If drCurrent.Item("CheckStatus").ToString() = "X" Then

                    parms(2, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                    intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parms)

                End If

            Next

            Return True

        End Function

#End Region

        Public Sub WriteStatusUTC(ByVal strOrg As String, ByVal strEFile As String, ByVal strPFile As String, ByVal strOFile As String)

            If IsNothing(strOFile) Then
                strOFile = String.Empty
            End If

            If IsNothing(strPFile) Then
                strPFile = String.Empty
            End If

            If IsNothing(strEFile) Then
                strEFile = String.Empty
            End If

            If IsNothing(strOrg) Then
                Return
            End If

            Dim parms(5, 2) As String

            Dim intUpdateRows As Integer
            Dim updateStatement As String

            updateStatement = "INSERT INTO WriteStatus (Organization, EventFile, PerfFile, OtherFile, WhenWrittenUTC) " & _
              "VALUES (@Organization, @EventFile, @PerfFile, @OtherFile, @WhenWrittenUTC)"

            parms(0, 0) = "@Organization"
            parms(0, 1) = "Organization, " + strOrg.Trim

            parms(1, 0) = "@EventFile"
            parms(1, 1) = "EventFile, " + strEFile.Trim

            parms(2, 0) = "@PerfFile"
            parms(2, 1) = "PerfFile, " + strPFile.Trim

            parms(3, 0) = "@OtherFile"
            parms(3, 1) = "OtherFile, " + strOFile.Trim

            parms(4, 0) = "@WhenWrittenUTC"
            parms(4, 1) = "WhenWrittenUTC, " + System.DateTime.UtcNow.ToString

            intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parms)

        End Sub

#Region " UpdateErrorCheckStatus(ByVal strWho As String, ByVal NewStatus As String) As Boolean "

        Public Function UpdateErrorCheckStatus(ByVal strWho As String, ByVal NewStatus As String) As Boolean

            Dim parms(3, 2) As String
            'Dim drCurrent As DataRow
            Dim intUpdateRows As Integer
            Dim updateStatement As String

            updateStatement = "UPDATE Setup SET CheckStatus = @Status " & _
             "WHERE (UtilityUnitCode = @UtilityUnitCode)"

            ' Sets the Status to E - Erros
            '                    O - OK ... no errors
            ' Data for this unit has been revised, but before running Error Check

            parms(0, 0) = "@Status"
            parms(0, 1) = "CheckStatus, " & NewStatus

            parms(1, 0) = "@UtilityUnitCode"
            parms(1, 1) = "UtilityUnitCode, " + strWho

            intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parms)

            Return True

        End Function

#End Region

#Region " Function EOM(ByVal dtValue As DateTime) As DateTime "

        Function EOM(ByVal dtValue As DateTime) As DateTime

            If Thread.CurrentThread.CurrentCulture.Name.ToString = "en-US" Then
                Return System.DateTime.Parse(dtValue.Month.ToString & "/" + _
             System.DateTime.DaysInMonth(dtValue.Year, dtValue.Month).ToString + "/" + _
             dtValue.Year.ToString + " 23:59:59")
            Else
                Return System.DateTime.Parse(System.DateTime.DaysInMonth(dtValue.Year, dtValue.Month).ToString + "/" + _
             dtValue.Month.ToString & "/" + _
             dtValue.Year.ToString + " 23:59:59")
            End If

        End Function

#End Region


#Region " GetCheckStatus(ByVal stringWho As String) As DataSet "

        '  Function Name:   GetCheckStatus ========================================
        '    Description:   Returns dataset with units that have been revised or
        '                   have errors
        '        Project:   GADS OS
        '   Date Created:   2003.02.10
        '	Code Copyright  2003 by The Outercurve Foundation, All Rights Reserved.
        '
        '      Revision History
        '   Who     Date		CodeVersion - Comments
        '
        '   Discussion:     The parameter passed in identifies which type of data
        '                   is written out.  You want to check on those units that
        '                   are shown as needing to write out this dataset.
        '==========================================================================
        Public Function GetCheckStatus(ByVal stringWho As String, ByVal myUser As String) As DataSet

            ' CheckStatus has 3 possible values:
            '   O = OK -- no revisions or errors
            '   R = Revised -- before running Error Check
            '   E = After running Error Check, there are Errors

            Dim stringSelect As String
            Dim parms(2, 2) As String
            Dim dsEmpty As DataSet = Nothing

            parms(0, 0) = "@Value"
            parms(0, 1) = "NERC, true"
            parms(1, 0) = "@Status"
            parms(1, 1) = "CheckStatus, O"

            stringSelect = "SELECT UnitName, CheckStatus FROM Setup WHERE "

            Select Case stringWho
                Case "NERC"
                    stringSelect += "(NERC = @Value)"
                    parms(0, 1) = "NERC, true"
                Case "NYISO"
                    stringSelect += "(NYISO = @Value)"
                    parms(0, 1) = "NYISO, true"
                Case "PJM"
                    stringSelect += "(PJM = @Value)"
                    parms(0, 1) = "PJM, true"
                Case "ISONE"
                    stringSelect += "(ISONE = @Value)"
                    parms(0, 1) = "ISONE, true"
                Case "CEA"
                    stringSelect += "(CEA = @Value)"
                    parms(0, 1) = "CEA, true"
                Case "GenElect"
                    stringSelect += "(GenElect = @Value)"
                    parms(0, 1) = "GenElect, true"
                Case "Siemens"
                    stringSelect += "(Siemens = @Value)"
                    parms(0, 1) = "Siemens, true"
                Case "CAISO"
                    stringSelect += "(CAISO = @Value)"
                    parms(0, 1) = "CAISO, true"
                Case "MISO"
                    stringSelect += "(MISO = @Value)"
                    parms(0, 1) = "MISO, true"
                Case "ISO1"
                    stringSelect += "(ISO1 = @Value)"
                    parms(0, 1) = "ISO1, true"
                Case "ISO2"
                    stringSelect += "(ISO2 = @Value)"
                    parms(0, 1) = "ISO2, true"
                Case "ISO3"
                    stringSelect += "(ISO3 = @Value)"
                    parms(0, 1) = "ISO3, true"
                Case Else
                    dsEmpty.Clear()
                    Return dsEmpty
            End Select

            stringSelect += " AND (CheckStatus <> @Status)"

            If myUser.Trim.ToUpper = "DASHBOARD" Then
                stringSelect += " ORDER BY UnitName"
            Else
                stringSelect += " AND UtilityUnitCode IN (SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID IN (SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & myUser & "')) ORDER BY UnitName"
            End If

            Return Factory.GetDataSet(stringSelect, parms)

        End Function

#End Region

#Region " GetMyCheckStatus(ByVal stringWho As String) As String "

        Public Function GetMyCheckStatus(ByVal stringWho As String) As String

            Dim strResults As String = String.Empty

            If stringWho Is Nothing Then
                Return CStr("X")
            End If

            ' CheckStatus has 3 possible values:
            '   O = OK -- no revisions or errors
            '   R = Revised -- before running Error Check
            '   E = After running Error Check, there are Errors

            Dim stringSelect As String
            stringSelect = "SELECT CheckStatus FROM Setup WHERE (UtilityUnitCode = '" & stringWho.Trim & "')"

            strResults = Convert.ToString(Factory.ExecuteScalar(stringSelect, Nothing))

            Return strResults

        End Function

#End Region

#Region " GetCountWithErrors(ByVal stringWho As String, ByVal myUser As String) As Integer "

        '  Function Name:   GetCountWithErrors ====================================
        '    Description:   Returns a count of the units that have been revised or
        '                   have errors
        '        Project:   GADS OS
        '   Date Created:   2003.02.10
        '	Code Copyright  2003 by The Outercurve Foundation, All Rights Reserved.
        '
        '      Revision History
        '   Who     Date		CodeVersion - Comments
        '
        '   Discussion:     The parameter passed in identifies which type of data
        '                   is written out.  You want to check on those units that
        '                   are shown as needing to write out this dataset.
        '==========================================================================
        Public Function GetCountWithErrors(ByVal stringWho As String, ByVal myUser As String) As Integer

            ' CheckStatus has 3 possible values:
            '   O = OK -- no revisions or errors
            '   R = Revised -- before running Error Check
            '   E = After running Error Check, there are Errors

            Dim stringSelect As String
            Dim parms(2, 2) As String

            parms(0, 0) = "@Value"
            parms(0, 1) = "NERC, true"
            parms(1, 0) = "@Status"
            parms(1, 1) = "CheckStatus, O"

            stringSelect = "SELECT COUNT(*) FROM Setup WHERE "

            Select Case stringWho
                Case "NERC"
                    stringSelect += "(NERC = @Value)"
                    parms(0, 1) = "NERC, true"
                Case "NYISO"
                    stringSelect += "(NYISO = @Value)"
                    parms(0, 1) = "NYISO, true"
                Case "MISO"
                    stringSelect += "(MISO = @Value)"
                    parms(0, 1) = "MISO, true"
                Case "PJM"
                    stringSelect += "(PJM = @Value)"
                    parms(0, 1) = "PJM, true"
                Case "ISONE"
                    stringSelect += "(ISONE = @Value)"
                    parms(0, 1) = "ISONE, true"
                Case Else
                    Return 0
            End Select

            stringSelect += " AND (CheckStatus <> @Status)"

            If myUser.Trim.ToUpper <> "DASHBOARD" Then
                stringSelect += " AND UtilityUnitCode IN (SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID IN (SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & myUser & "'))"
            End If

            Return CInt(Factory.ExecuteScalar(stringSelect, parms))

        End Function

#End Region

#Region " CountOpenEvents(ByVal UtilityUnitCode As String, ByVal intCurrentYear As Integer) As Integer "

        Public Function CountOpenEvents(ByVal UtilityUnitCode As String, ByVal intCurrentYear As Integer) As Integer

            Dim stringSelect As String
            Dim parms(2, 2) As String

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " & UtilityUnitCode
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " & intCurrentYear.ToString

            stringSelect = "SELECT COUNT(*) FROM EventData01 WHERE (RevisionCard01 <> 'X') AND (EndDateTime IS NULL) AND (UtilityUnitCode = @UtilityUnitCode) AND (Year < @Year)"

            Return CInt(Factory.ExecuteScalar(stringSelect, parms))

        End Function

#End Region

#Region " OpenEventYears(ByVal UtilityUnitCode As String, ByVal intCurrentYear As Integer) As IDataReader "

        Public Function OpenEventYears(ByVal UtilityUnitCode As String, ByVal intCurrentYear As Integer) As IDataReader

            Dim stringSelect As String
            Dim parms(2, 2) As String

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " & UtilityUnitCode
            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " & intCurrentYear.ToString

            stringSelect = "SELECT Year, EventNumber, EventType, StartDateTime FROM EventData01 WHERE (RevisionCard01 <> 'X') AND (EndDateTime IS NULL) AND (UtilityUnitCode = @UtilityUnitCode) AND (Year < @Year)"
            Return Me.Factory.ExecuteDataReader(stringSelect, parms, CommandBehavior.CloseConnection)

        End Function

#End Region


#Region " ClearMyErrors(ByVal UtilityUnitCode As String) As Boolean "

        Public Function ClearMyErrors(ByVal UtilityUnitCode As String) As Boolean

            Dim parms(1, 2) As String

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + UtilityUnitCode

            Dim intUpdateRows As Integer = 0
            Dim updateStatement As String
            Dim stringSelect As String

            stringSelect = "SELECT COUNT(*) FROM Errors WHERE (UtilityUnitCode = @UtilityUnitCode)"
            intUpdateRows = CInt(Factory.ExecuteScalar(stringSelect, parms))

            If intUpdateRows > 0 Then
                updateStatement = "DELETE FROM Errors WHERE (UtilityUnitCode = @UtilityUnitCode)"
                intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parms)
            End If

            Return True

        End Function

        Public Function ClearMyISOErrors(ByVal UtilityUnitCode As String, ByVal intYear As Integer) As Boolean

            Dim parms(2, 2) As String

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + UtilityUnitCode

            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            Dim intUpdateRows As Integer = 0
            Dim updateStatement As String
            Dim stringSelect As String

            stringSelect = "SELECT COUNT(*) FROM Errors WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"
            intUpdateRows = CInt(Factory.ExecuteScalar(stringSelect, parms))

            If intUpdateRows > 0 Then
                updateStatement = "DELETE FROM Errors WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"
                intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parms)
            End If

            Return True

        End Function


#End Region

        Public Function GetCFESAP(ByVal UtilityUnitCode As String, _
                          ByVal UnitShortName As String, _
                          ByVal intYear As Integer, _
                          ByVal EventNumber As Integer, _
                          ByVal EvenCardNumber As Integer) As Integer

            Dim parms(8, 2) As String
            'Dim intReturn As Integer
            Dim objReturn As Object
            'Dim insertStatement As String
            Dim deleteStatement As String

            If IsNothing(UtilityUnitCode) Or IsNothing(intYear) Then
                Return 0
            End If

            If IsNothing(UnitShortName) Then
                UnitShortName = ""
            End If

            If IsNothing(EventNumber) Then
                EventNumber = 0
            End If

            If IsNothing(EvenCardNumber) Then
                EvenCardNumber = 0
            End If

            deleteStatement = "SELECT TTTT FROM CFESAP WHERE (UtilityUnitCode = @UtilityUnitCode) AND (UnitShortName = @UnitShortName) AND " & _
                  "(Year = @Year) AND (EventNumber = @EventNumber) AND (EvenCardNumber = @EvenCardNumber)"

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + UtilityUnitCode

            parms(1, 0) = "@UnitShortName"
            parms(1, 1) = "UnitShortName, " + UnitShortName

            parms(2, 0) = "@Year"
            parms(2, 1) = "Year, " + intYear.ToString

            parms(3, 0) = "@EventNumber"
            parms(3, 1) = "EventNumber, " + EventNumber.ToString

            parms(4, 0) = "@EvenCardNumber"
            parms(4, 1) = "EvenCardNumber, " + EvenCardNumber.ToString

            objReturn = Factory.ExecuteScalar(deleteStatement, parms)

            If IsNothing(objReturn) Then
                Return 0
            Else
                Return CInt(objReturn)
            End If

        End Function

        Public Function SaveCFESAP(ByVal UtilityUnitCode As String, _
                  ByVal UnitShortName As String, _
                  ByVal intYear As Integer, _
                  ByVal EventNumber As Integer, _
                  ByVal EvenCardNumber As Integer, _
                  ByVal RevisionCardEven As String, _
                  ByVal TechnicalLocation As String, _
                  ByVal EquipmentDescription As String, _
                  ByVal SS As Integer, _
                  ByVal TTTT As Integer) As Boolean

            Dim parms(10, 2) As String
            Dim intInsertRows As Integer
            Dim intDeleteRows As Integer
            Dim insertStatement As String
            Dim deleteStatement As String

            If IsNothing(UtilityUnitCode) Or IsNothing(intYear) Then
                Return False
            End If

            If IsNothing(UnitShortName) Then
                UnitShortName = ""
            End If

            If IsNothing(EventNumber) Then
                EventNumber = 0
            End If

            If IsNothing(EvenCardNumber) Then
                EvenCardNumber = 0
            End If

            If IsNothing(RevisionCardEven) Then
                RevisionCardEven = "0"
            End If

            If IsNothing(TechnicalLocation) Then
                TechnicalLocation = "***"
            End If

            If IsNothing(EquipmentDescription) Then
                EquipmentDescription = ""
            End If

            If IsNothing(SS) Then
                SS = 0
            End If

            If IsNothing(TTTT) Then
                TTTT = 0
            End If

            deleteStatement = "DELETE FROM CFESAP WHERE (UtilityUnitCode = @UtilityUnitCode) AND (UnitShortName = @UnitShortName) AND " & _
                              "(Year = @Year) AND (EventNumber = @EventNumber) AND (EvenCardNumber = @EvenCardNumber)"

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + UtilityUnitCode

            parms(1, 0) = "@UnitShortName"
            parms(1, 1) = "UnitShortName, " + UnitShortName

            parms(2, 0) = "@Year"
            parms(2, 1) = "Year, " + intYear.ToString

            parms(3, 0) = "@EventNumber"
            parms(3, 1) = "EventNumber, " + EventNumber.ToString

            parms(4, 0) = "@EvenCardNumber"
            parms(4, 1) = "EvenCardNumber, " + EvenCardNumber.ToString

            intDeleteRows = Factory.ExecuteNonQuery(deleteStatement, parms)


            insertStatement = "INSERT INTO CFESAP (UtilityUnitCode, UnitShortName, Year, EventNumber, EvenCardNumber, RevisionCardEven, TechnicalLocation, EquipmentDescription, SS, TTTT) " & _
              "VALUES (@UtilityUnitCode, @UnitShortName, @Year, @EventNumber, @EvenCardNumber, @RevisionCardEven, @TechnicalLocation, @EquipmentDescription, @SS, @TTTT)"

            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + UtilityUnitCode

            parms(1, 0) = "@UnitShortName"
            parms(1, 1) = "UnitShortName, " + UnitShortName

            parms(2, 0) = "@Year"
            parms(2, 1) = "Year, " + intYear.ToString

            parms(3, 0) = "@EventNumber"
            parms(3, 1) = "EventNumber, " + EventNumber.ToString

            parms(4, 0) = "@EvenCardNumber"
            parms(4, 1) = "EvenCardNumber, " + EvenCardNumber.ToString

            parms(5, 0) = "@RevisionCardEven"
            parms(5, 1) = "RevisionCardEven, " + RevisionCardEven.ToString

            parms(6, 0) = "@TechnicalLocation"
            parms(6, 1) = "TechnicalLocation, " + TechnicalLocation

            parms(7, 0) = "@EquipmentDescription"
            parms(7, 1) = "EquipmentDescription, " + EquipmentDescription

            parms(8, 0) = "@SS"
            parms(8, 1) = "SS, " + SS.ToString

            parms(9, 0) = "@TTTT"
            parms(9, 1) = "TTTT, " + TTTT.ToString

            intInsertRows = Factory.ExecuteNonQuery(insertStatement, parms)

            If intInsertRows = 1 Then
                Return True
            Else
                Return False
            End If

        End Function



#Region " LoadErrors(UtilityUnitCode, intYear, Period, EventNumber, EventEvenCardNo, ErrorMessage, ErrorSeverity, strShortName) As Boolean "

        Public Function LoadErrors(ByVal UtilityUnitCode As String, _
          ByVal intYear As Integer, _
          ByVal Period As String, _
          ByVal EventNumber As Integer, _
          ByVal EventEvenCardNo As Integer, _
          ByVal ErrorMessage As String, _
          ByVal ErrorSeverity As String, _
          ByVal strShortName As String, _
          ByVal GetFromDictionary As HybridDictionary) As Boolean

            Dim parms(8, 2) As String
            Dim intInsertRows As Integer
            'Dim intDeleteRows As Integer
            Dim intFound As Integer
            Dim objFound As Object
            Dim insertStatement As String
            Dim deleteStatement As String
            Dim selectStatement As String

            If IsNothing(UtilityUnitCode) Or IsNothing(intYear) Then
                Return False
            End If

            If IsNothing(Period) Then
                Period = ""
            End If

            If IsNothing(strShortName) Then
                strShortName = ""
            End If

            If IsNothing(EventNumber) Then
                EventNumber = 0
            End If

            If IsNothing(EventEvenCardNo) Then
                EventEvenCardNo = 0
            End If

            If IsNothing(ErrorMessage) Then
                ErrorMessage = "Unspecified Error"
            End If

            If IsNothing(ErrorSeverity) Then
                ErrorSeverity = "W"    ' Warning
            End If

            insertStatement = "INSERT INTO Errors (UtilityUnitCode, Year, Period, EventNumber, EventEvenCardNo, ErrorMessage, ErrorSeverity, UnitShortName) " & _
              "VALUES (@UtilityUnitCode, @Year, @Period, @EventNumber, @EventEvenCardNo, @ErrorMessage, @ErrorSeverity, @UnitShortName)"

            deleteStatement = "DELETE FROM Errors WHERE " & _
            "UtilityUnitCode = @UtilityUnitCode AND " & _
            "Year = @Year AND " & _
            "Period = @Period AND " & _
            "EventNumber = @EventNumber AND " + _
            "EventEvenCardNo = @EventEvenCardNo AND " & _
            "ErrorMessage = @ErrorMessage AND " & _
            "ErrorSeverity = @ErrorSeverity AND " & _
            "UnitShortName = @UnitShortName"

            selectStatement = "SELECT COUNT(*) FROM Errors WHERE " & _
            "UtilityUnitCode = @UtilityUnitCode AND " & _
            "Year = @Year AND " & _
            "Period = @Period AND " & _
            "EventNumber = @EventNumber AND " + _
            "EventEvenCardNo = @EventEvenCardNo AND " & _
            "ErrorMessage = @ErrorMessage AND " & _
            "ErrorSeverity = @ErrorSeverity AND " & _
            "UnitShortName = @UnitShortName"


            parms(0, 0) = "@UtilityUnitCode"
            parms(0, 1) = "UtilityUnitCode, " + UtilityUnitCode

            parms(1, 0) = "@Year"
            parms(1, 1) = "Year, " + intYear.ToString

            parms(2, 0) = "@Period"
            parms(2, 1) = "Period, " + Period

            parms(3, 0) = "@EventNumber"
            parms(3, 1) = "EventNumber, " + EventNumber.ToString

            parms(4, 0) = "@EventEvenCardNo"
            parms(4, 1) = "EventEvenCardNo, " + EventEvenCardNo.ToString

            parms(5, 0) = "@ErrorMessage"
            parms(5, 1) = "ErrorMessage, " + ErrorMessage

            parms(6, 0) = "@ErrorSeverity"
            parms(6, 1) = "ErrorSeverity, " + ErrorSeverity

            parms(7, 0) = "@UnitShortName"
            parms(7, 1) = "UnitShortName, " + strShortName

            'intDeleteRows = factory.ExecuteNonQuery(deleteStatement, parms)
            objFound = Factory.ExecuteScalar(selectStatement, parms)

            If Not objFound Is Nothing Then
                Try
                    intFound = Convert.ToInt32(objFound)
                Catch ex As Exception
                    intFound = 0
                End Try
            Else
                intFound = 0
            End If

            If intFound = 0 Then
                intInsertRows = Factory.ExecuteNonQuery(insertStatement, parms)
            Else
                intInsertRows = 0
            End If

            If intInsertRows = 1 Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region " IsUserValid(ByVal stringWho As String) As Integer "

        '  Function Name:   IsUserValid ===========================================
        '    Description:   Returns a count to indicate if the user ID is in the
        '                   NGUsers table
        '        Project:   GADS OS
        '   Date Created:   2003.02.22
        '	Code Copyright  2003 by The Outercurve Foundation, All Rights Reserved.
        '
        '      Revision History
        '   Who     Date		CodeVersion - Comments
        '
        '   Discussion:
        '   
        '
        '==========================================================================
        Public Function IsUserValid(ByVal stringWho As String) As Integer
            Dim stringSelect As String
            stringSelect = "SELECT COUNT(*) FROM NGUsers WHERE LoginID = '" & stringWho & "'"
            Return CInt(Factory.ExecuteScalar(stringSelect, Nothing))
        End Function

        Public Function IsFormsUserValid(ByVal stringWho As String, ByVal stringPW As String) As Integer

            Dim stringSelect As String

            Dim parms(1, 1) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters


            parms(0, 0) = "@LoginID"
            parms(0, 1) = "LoginID, " & stringWho

            parms(1, 0) = "@Password"
            stringSelect = Encrypt(stringPW.Trim)
            parms(1, 1) = "Password, " & stringSelect

            stringSelect = "SELECT COUNT(*) FROM NGUsers WHERE LoginID = @LoginID AND Password = @Password"

            Return CInt(Factory.ExecuteScalar(stringSelect, parms))

        End Function

#End Region

#Region " IsUserAssignedToGroup(ByVal stringWho As String) As Integer "

        '  Function Name:   IsUserAssignedToGroup =================================
        '    Description:   Returns a count to indicate if the user ID is in the
        '                   NGUsers table
        '        Project:   GADS OS
        '   Date Created:   2003.02.22
        '	Code Copyright  2003 by The Outercurve Foundation, All Rights Reserved.
        '
        '      Revision History
        '   Who     Date		CodeVersion - Comments
        '
        '   Discussion:
        '   
        '
        '==========================================================================
        Public Function IsUserAssignedToGroup(ByVal stringWho As String) As Integer
            Dim stringSelect As String
            stringSelect = "SELECT COUNT(*) FROM NGUserToGroup WHERE LoginID = '" & stringWho & "'"
            Return CInt(Factory.ExecuteScalar(stringSelect, Nothing))
        End Function

#End Region

#Region " CountUsersDefined() As Integer "

        Public Function CountUsersDefined() As Integer
            Dim stringSelect As String
            stringSelect = "SELECT COUNT(*) FROM NGUsers"
            Return CInt(Factory.ExecuteScalar(stringSelect, Nothing))
        End Function

#End Region

#Region " GetMyKey() As String "

        Public Function GetMyKey() As String

            Dim moXML As New SetupSettings.Settings("Keys")
            Dim encryptedCSValue As String = moXML.GetSettingStr("GADSNGKey", "MyKey", "")

            If encryptedCSValue.Trim = String.Empty Then
                Return "MISSINGFILE"
            Else
                Dim decryptedCSValue As String = Decrypt(encryptedCSValue)
                Return decryptedCSValue
            End If

        End Function

#End Region

#Region " GetUsers() As DataSet "

        Public Function GetUsers() As DataSet

            Dim encryptedCSValue As String
            'Dim decryptedCSValue As String
            Dim dsResult As DataSet
            Dim stringSelect As String

            stringSelect = "SELECT First_Name, Last_Name, LoginID, Password, Email_Address, Phone, LocationDescription FROM NGUsers"
            dsResult = Me.Factory.GetDataSet(stringSelect, Nothing)

            Dim tbl As DataTable = dsResult.Tables(0)
            Dim row As DataRow

            For Each row In tbl.Rows
                If Not row.IsNull("Password") And row("Password").ToString.Trim <> String.Empty Then
                    encryptedCSValue = row("Password").ToString
                    row("Password") = Decrypt(row("Password").ToString.Trim)
                End If
            Next

            Return dsResult

        End Function

#End Region

        Public Function GetNERCXRefReader() As IDataReader

            Dim stringSelect As String
            stringSelect = "SELECT UnitShortName, UnitName, NERCClassID, GADSMethod FROM NERCXRef ORDER BY UnitShortName,UnitName"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

        Public Function GetNERCClassesReader() As IDataReader

            Dim stringSelect As String
            stringSelect = "SELECT NERCClassID, ClassDesc, CCF, CEFORd FROM NERCClasses ORDER BY NERCClassID"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

        Public Function GetEFORdForUCAP() As IDataReader

            Dim stringSelect As String

            stringSelect = "SELECT UnitShortName, TL_DateTime, PO, PO_SE, MO, MO_SE, " & _
                    "SF, U1, U2, U3, D1, D2, D3, D4, D4_DE, PD, PD_DE, RS, EUFDH_RS, SH, PH, " & _
                    "ESEDH, AH, FOCount, RSCount, ActualStartsCount, AttemptedStartsCount, " & _
                    "AttemptedStarts, ActualStarts, StartingReliability, GrossMaxCap, NetMaxCap, " & _
                    "PumpingHours, SynchCondHours, GMC_Weight, NMC_Weight, DEFOR, DFOR, FP, " & _
                    "FF_ID, FF_IT, FF_IR, FF, FF_D, FF_T, FF_R, FL_Numerator, FL_Denominator, " & _
                    "FL_FORdNumerator, FOCount_OMC, FOH_OMC, AH_OMC, EFDH_OMC, E_PO, PO_SE, E_MO, E_MO_SE, " & _
                    "E_SF, E_U1, E_U2, E_U3, E_D1, E_D2, E_D3, E_D4, E_D4_DE, E_PD, E_PD_DE, E_RS, E_EUFDH_RS, E_SH, E_PH, " & _
                    "E_ESEDH, E_AH FROM EFORd " & _
                    "WHERE (Granularity = 'Monthly') AND (ServiceHourMethod <> 2) " & _
                    "ORDER BY UnitShortName, TL_DateTime"

            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

        Public Function GetOFForUCAP(ByVal strUnitShortName As String) As IDataReader
            Dim stringSelect As String

            stringSelect = "SELECT UnitShortName, Year, Period, RevisionCard2, NetMaxCap, NetDepCap, " & _
           "NetGen, ServiceHours, RSHours, PumpingHours, SynchCondHours," & _
           "PlannedOutageHours, ForcedOutageHours, MaintOutageHours, ExtofSchedOutages, " & _
           "PeriodHours FROM PerformanceDataAR WHERE (UnitShortName = '" & strUnitShortName.Trim & "') ORDER BY UnitShortName, Year, Period"

            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

#Region " GetUserGroupsReader(ByVal stringWho As String) As IDataReader "

        Public Function GetUserGroupsReader(ByVal stringWho As String) As IDataReader

            Dim stringSelect As String
            stringSelect = "SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & stringWho & "'"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

#End Region

#Region " GetUnitsInGroup(ByVal GroupID As Integer) As IDataReader "

        Public Function GetUnitsInGroup(ByVal GroupID As Integer) As IDataReader

            Dim stringSelect As String
            stringSelect = "SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID = " & GroupID.ToString()
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

#End Region

#Region " GetPermissions(ByVal GroupID As Integer) As IDataReader "

        Public Function GetPermissions(ByVal GroupID As Integer) As IDataReader

            Dim stringSelect As String
            stringSelect = "SELECT GroupID, YearLimit, LoadASCII, LoadDBFs, WriteNERC, WriteNYISO, WritePJM, WriteISONE, WriteMISO FROM NGGroups WHERE GroupID = " & GroupID.ToString()
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

#End Region


#Region " LoadPerformance(ByVal dsLoad As DataSet, ByVal lJO As Boolean, ByVal lUseTimeStamp As Boolean) As Boolean "

        Public Function LoadPerformance(ByVal dsLoad As DataSet, ByVal lJO As Boolean, ByVal lUseTimeStamp As Boolean) As Boolean

            Dim selectStatement As String
            Dim selectTimeStamp As String
            Dim checkSetupStatement As String
            Dim insertStatement As String
            Dim updateStatement1 As String
            Dim updateStatement2 As String
            Dim updateStatement3 As String
            Dim updateStatement4 As String
            'Dim deleteStatement As String
            Dim buildDsSetupStatement As String
            'Dim tableName As String
            Dim parms(81, 2) As String
            Dim intRows As Integer
            Dim intUpdateRows As Integer
            'Dim daTest As IDataAdapter
            'Dim strTest As String
            Dim drCurrent As DataRow
            Dim dsSetup As DataSet
            Dim drSetup As DataRow
            Dim dtUTCNow As DateTime = System.DateTime.UtcNow

            selectStatement = "SELECT COUNT(*) FROM PerformanceData WHERE " & _
             "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
             "(Year = @Original_Year) AND " & _
             "(Period = @Original_Period)"

            selectTimeStamp = "SELECT TimeStamp FROM PerformanceData WHERE " & _
             "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
             "(Year = @Original_Year) AND " & _
             "(Period = @Original_Period)"

            checkSetupStatement = "SELECT COUNT(*) FROM Setup WHERE " & _
             "(UtilityUnitCode = @UtilityUnitCode)"

            buildDsSetupStatement = "SELECT UtilityUnitCode, CheckStatus, UnitShortName FROM Setup"
            dsSetup = Me.Factory.GetDataSet(buildDsSetupStatement, Nothing)

            If Factory.Provider = "OleDb" Then
                Dim keys(0) As DataColumn
                keys(0) = dsSetup.Tables(0).Columns.Item("UtilityUnitCode")
                dsSetup.Tables(0).PrimaryKey = keys
            End If

            insertStatement = "INSERT INTO PerformanceData (UtilityUnitCode, Year, Period, RevisionCard1, " & _
                "GrossMaxCap, GrossDepCap, GrossGen, NetMaxCap, NetDepCap, NetGen, TypUnitLoading, " & _
                "AttemptedStarts, ActualStarts, VerbalDesc, RevisionCard2, ServiceHours, RSHours, " & _
                "PumpingHours, SynchCondHours, PlannedOutageHours, ForcedOutageHours, MaintOutageHours, " & _
                "ExtofSchedOutages, PeriodHours, RevisionCard3, PriFuelCode, PriQtyBurned, " & _
                "PriAvgHeatContent, PriBtus, PriPercentAsh, PriPercentMoisture, PriPercentSulfur, " & _
                "PriPercentAlkalines, PriGrindIndexVanad, PriAshSoftTemp, SecFuelCode, SecQtyBurned, " & _
                "SecAvgHeatContent, SecBtus, SecPercentAsh, SecPercentMoisture, SecPercentSulfur, " & _
                "SecPercentAlkalines, SecGrindIndexVanad, SecAshSoftTemp, RevisionCard4, " & _
                "TerFuelCode, TerQtyBurned, TerAvgHeatContent, TerBtus, TerPercentAsh, TerPercentMoisture, " & _
                "TerPercentSulfur, TerPercentAlkalines, TerGrindIndexVanad, TerAshSoftTemp, " & _
                "QuaFuelCode, QuaQtyBurned, QuaAvgHeatContent, QuaBtus, QuaPercentAsh, QuaPercentMoisture, " & _
                "QuaPercentSulfur, QuaPercentAlkalines, QuaGrindIndexVanad, QuaAshSoftTemp, " & _
                "RevMonthCard1, RevMonthCard2, RevMonthCard3, RevMonthCard4, JOGrossMaxCap, " & _
                "JOGrossGen, JONetMaxCap, JONetGen, JOPriQtyBurned, JOSecQtyBurned, JOTerQtyBurned, " & _
                "JOQuaQtyBurned, TimeStamp, UnitShortName, InactiveHours) " & _
                "VALUES " & _
                "(@UtilityUnitCode, @Year, @Period, @RevisionCard1, @GrossMaxCap, @GrossDepCap, @GrossGen, " & _
                "@NetMaxCap, @NetDepCap, @NetGen, @TypUnitLoading, @AttemptedStarts, @ActualStarts, @VerbalDesc, " & _
                "@RevisionCard2, @ServiceHours, @RSHours, @PumpingHours, @SynchCondHours, @PlannedOutageHours, " & _
                "@ForcedOutageHours, @MaintOutageHours, @ExtofSchedOutages, @PeriodHours, @RevisionCard3, " & _
                "@PriFuelCode, @PriQtyBurned, @PriAvgHeatContent, @PriBtus, @PriPercentAsh, " & _
                "@PriPercentMoisture, @PriPercentSulfur, @PriPercentAlkalines, @PriGrindIndexVanad, " & _
                "@PriAshSoftTemp, @SecFuelCode, @SecQtyBurned, @SecAvgHeatContent, @SecBtus, " & _
                "@SecPercentAsh, @SecPercentMoisture, @SecPercentSulfur, @SecPercentAlkalines, " & _
                "@SecGrindIndexVanad, @SecAshSoftTemp, @RevisionCard4, @TerFuelCode, @TerQtyBurned, " & _
                "@TerAvgHeatContent, @TerBtus, @TerPercentAsh, @TerPercentMoisture, @TerPercentSulfur, " & _
                "@TerPercentAlkalines, @TerGrindIndexVanad, @TerAshSoftTemp, @QuaFuelCode, " & _
                "@QuaQtyBurned, @QuaAvgHeatContent, @QuaBtus, @QuaPercentAsh, @QuaPercentMoisture, " & _
                "@QuaPercentSulfur, @QuaPercentAlkalines, @QuaGrindIndexVanad, @QuaAshSoftTemp, " & _
                "@RevMonthCard1, @RevMonthCard2, @RevMonthCard3, @RevMonthCard4, @JOGrossMaxCap, " & _
                "@JOGrossGen, @JONetMaxCap, @JONetGen, @JOPriQtyBurned, @JOSecQtyBurned, @JOTerQtyBurned, " & _
                "@JOQuaQtyBurned, @TimeStamp, @UnitShortName, @InactiveHours)"

            updateStatement1 = "UPDATE PerformanceData SET RevisionCard1 = @RevisionCard1, GrossMaxCap = @GrossMaxCap, " & _
              "GrossDepCap = @GrossDepCap, GrossGen = @GrossGen, NetMaxCap = @NetMaxCap, NetDepCap = @NetDepCap, " & _
              "NetGen = @NetGen, TypUnitLoading = @TypUnitLoading, AttemptedStarts = @AttemptedStarts, " & _
              "ActualStarts = @ActualStarts, VerbalDesc = @VerbalDesc, " & _
              "JOGrossMaxCap = @JOGrossMaxCap, JOGrossGen = @JOGrossGen, JONetMaxCap = @JONetMaxCap, " & _
              "JONetGen = @JONetGen, RevMonthCard1 = @RevMonthCard1, TimeStamp = @TimeStamp " & _
              "WHERE " & _
              "(Period = @Original_Period) AND " & _
              "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
              "(Year = @Original_Year) AND " & _
              "(RevisionCard1 <= @Original_RevisionCard1)"

            updateStatement2 = "UPDATE PerformanceData SET RevisionCard2 = @RevisionCard2, " & _
              "ServiceHours = @ServiceHours, RSHours = @RSHours, PumpingHours = @PumpingHours, " & _
              "SynchCondHours = @SynchCondHours, PlannedOutageHours = @PlannedOutageHours, " & _
              "ForcedOutageHours = @ForcedOutageHours, MaintOutageHours = @MaintOutageHours, " & _
              "ExtofSchedOutages = @ExtofSchedOutages, PeriodHours = @PeriodHours, RevMonthCard2 = @RevMonthCard2, " & _
              "TimeStamp = @TimeStamp, InactiveHours = @InactiveHours " & _
              "WHERE " & _
              "(Period = @Original_Period) AND " & _
              "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
              "(Year = @Original_Year) AND " & _
              "(RevisionCard2 <= @Original_RevisionCard2)"

            updateStatement3 = "UPDATE PerformanceData SET RevisionCard3 = @RevisionCard3, PriFuelCode = @PriFuelCode, PriQtyBurned = @PriQtyBurned, " & _
              "PriAvgHeatContent = @PriAvgHeatContent, PriBtus = @PriBtus, PriPercentAsh = @PriPercentAsh, " & _
              "PriPercentMoisture = @PriPercentMoisture, PriPercentSulfur = @PriPercentSulfur, " & _
              "PriPercentAlkalines = @PriPercentAlkalines, PriGrindIndexVanad = @PriGrindIndexVanad, " & _
              "PriAshSoftTemp = @PriAshSoftTemp, SecFuelCode = @SecFuelCode, SecQtyBurned = @SecQtyBurned, " & _
              "SecAvgHeatContent = @SecAvgHeatContent, SecBtus = @SecBtus, SecPercentAsh = @SecPercentAsh, " & _
              "SecPercentMoisture = @SecPercentMoisture, SecPercentSulfur = @SecPercentSulfur, " & _
              "SecPercentAlkalines = @SecPercentAlkalines, SecGrindIndexVanad = @SecGrindIndexVanad, " & _
              "SecAshSoftTemp = @SecAshSoftTemp, " & _
              "JOPriQtyBurned = @JOPriQtyBurned, JOSecQtyBurned = @JOSecQtyBurned, RevMonthCard3 = @RevMonthCard3, " & _
              "TimeStamp = @TimeStamp " & _
              "WHERE " & _
              "(Period = @Original_Period) AND " & _
              "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
              "(Year = @Original_Year) AND " & _
              "(RevisionCard3 <= @Original_RevisionCard3)"

            updateStatement4 = "UPDATE PerformanceData SET RevisionCard4 = @RevisionCard4, TerFuelCode = @TerFuelCode, " & _
              "TerQtyBurned = @TerQtyBurned, TerAvgHeatContent = @TerAvgHeatContent, TerBtus = @TerBtus, " & _
              "TerPercentAsh = @TerPercentAsh, TerPercentMoisture = @TerPercentMoisture, " & _
              "TerPercentSulfur = @TerPercentSulfur, TerPercentAlkalines = @TerPercentAlkalines, " & _
              "TerGrindIndexVanad = @TerGrindIndexVanad, TerAshSoftTemp = @TerAshSoftTemp, " & _
              "QuaFuelCode = @QuaFuelCode, QuaQtyBurned = @QuaQtyBurned, QuaAvgHeatContent = @QuaAvgHeatContent, " & _
              "QuaBtus = @QuaBtus, QuaPercentAsh = @QuaPercentAsh, QuaPercentMoisture = @QuaPercentMoisture, " & _
              "QuaPercentSulfur = @QuaPercentSulfur, QuaPercentAlkalines = @QuaPercentAlkalines, " & _
              "QuaGrindIndexVanad = @QuaGrindIndexVanad, QuaAshSoftTemp = @QuaAshSoftTemp, " & _
              "JOTerQtyBurned = @JOTerQtyBurned, JOQuaQtyBurned = @JOQuaQtyBurned, RevMonthCard4 = @RevMonthCard4, " & _
              "TimeStamp = @TimeStamp " & _
              "WHERE " & _
              "(Period = @Original_Period) AND " & _
              "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
              "(Year = @Original_Year) AND " & _
              "(RevisionCard4 <= @Original_RevisionCard4)"

            Try

                For Each drCurrent In dsLoad.Tables(0).Rows

                    System.Array.Clear(parms, 0, parms.Length)
                    'Application.DoEvents()

                    parms(0, 0) = "@UtilityUnitCode"
                    parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                    ' Check to see if this unit is in Setup -- skip loading this record if not in Setup
                    drSetup = dsSetup.Tables(0).Rows.Find(drCurrent("UtilityUnitCode").ToString)

                    If drSetup Is Nothing Then

                        ' Do nothing with this record

                    Else

                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()

                        ' The unit is in Setup

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                        parms(2, 0) = "@Original_Period"
                        parms(2, 1) = "Period, " + drCurrent("Period").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows = 0 Then

                            ' *** The row does not exist so use INSERT ***

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@UtilityUnitCode"
                            parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(1, 0) = "@Year"
                            parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(2, 0) = "@Period"
                            parms(2, 1) = "Period, " + drCurrent("Period").ToString()
                            parms(3, 0) = "@RevisionCard1"
                            parms(3, 1) = "RevisionCard1, " + drCurrent("RevisionCard1").ToString()
                            parms(4, 0) = "@GrossMaxCap"
                            parms(4, 1) = "GrossMaxCap, " + drCurrent("GrossMaxCap").ToString()
                            parms(5, 0) = "@GrossDepCap"
                            parms(5, 1) = "GrossDepCap, " + drCurrent("GrossDepCap").ToString()
                            parms(6, 0) = "@GrossGen"
                            parms(6, 1) = "GrossGen, " + drCurrent("GrossGen").ToString()
                            parms(7, 0) = "@NetMaxCap"
                            parms(7, 1) = "NetMaxCap, " + drCurrent("NetMaxCap").ToString()
                            parms(8, 0) = "@NetDepCap"
                            parms(8, 1) = "NetDepCap, " + drCurrent("NetDepCap").ToString()
                            parms(9, 0) = "@NetGen"
                            parms(9, 1) = "NetGen, " + drCurrent("NetGen").ToString()
                            parms(10, 0) = "@TypUnitLoading"
                            parms(10, 1) = "TypUnitLoading, " + drCurrent("TypUnitLoading").ToString()
                            parms(11, 0) = "@AttemptedStarts"
                            parms(11, 1) = "AttemptedStarts, " + drCurrent("AttemptedStarts").ToString()
                            parms(12, 0) = "@ActualStarts"
                            parms(12, 1) = "ActualStarts, " + drCurrent("ActualStarts").ToString()
                            parms(13, 0) = "@VerbalDesc"
                            parms(13, 1) = "VerbalDesc, " + drCurrent("VerbalDesc").ToString()
                            parms(14, 0) = "@RevisionCard2"
                            parms(14, 1) = "RevisionCard2, " + drCurrent("RevisionCard2").ToString()
                            parms(15, 0) = "@ServiceHours"
                            parms(15, 1) = "ServiceHours, " + drCurrent("ServiceHours").ToString()
                            parms(16, 0) = "@RSHours"
                            parms(16, 1) = "RSHours, " + drCurrent("RSHours").ToString()
                            parms(17, 0) = "@PumpingHours"
                            parms(17, 1) = "PumpingHours, " + drCurrent("PumpingHours").ToString()
                            parms(18, 0) = "@SynchCondHours"
                            parms(18, 1) = "SynchCondHours, " + drCurrent("SynchCondHours").ToString()
                            parms(19, 0) = "@PlannedOutageHours"
                            parms(19, 1) = "PlannedOutageHours, " + drCurrent("PlannedOutageHours").ToString()
                            parms(20, 0) = "@ForcedOutageHours"
                            parms(20, 1) = "ForcedOutageHours, " + drCurrent("ForcedOutageHours").ToString()
                            parms(21, 0) = "@MaintOutageHours"
                            parms(21, 1) = "MaintOutageHours, " + drCurrent("MaintOutageHours").ToString()
                            parms(22, 0) = "@ExtofSchedOutages"
                            parms(22, 1) = "ExtofSchedOutages, " + drCurrent("ExtofSchedOutages").ToString()
                            parms(23, 0) = "@PeriodHours"
                            parms(23, 1) = "PeriodHours, " + drCurrent("PeriodHours").ToString()
                            parms(24, 0) = "@RevisionCard3"
                            parms(24, 1) = "RevisionCard3, " + drCurrent("RevisionCard3").ToString()
                            parms(25, 0) = "@PriFuelCode"
                            parms(25, 1) = "PriFuelCode, " + drCurrent("PriFuelCode").ToString()
                            parms(26, 0) = "@PriQtyBurned"
                            parms(26, 1) = "PriQtyBurned, " + drCurrent("PriQtyBurned").ToString()
                            parms(27, 0) = "@PriAvgHeatContent"
                            parms(27, 1) = "PriAvgHeatContent, " + drCurrent("PriAvgHeatContent").ToString()
                            parms(28, 0) = "@PriBtus"
                            parms(28, 1) = "PriBtus, " + drCurrent("PriBtus").ToString()
                            parms(29, 0) = "@PriPercentAsh"
                            parms(29, 1) = "PriPercentAsh, " + drCurrent("PriPercentAsh").ToString()
                            parms(30, 0) = "@PriPercentMoisture"
                            parms(30, 1) = "PriPercentMoisture, " + drCurrent("PriPercentMoisture").ToString()
                            parms(31, 0) = "@PriPercentSulfur"
                            parms(31, 1) = "PriPercentSulfur, " + drCurrent("PriPercentSulfur").ToString()
                            parms(32, 0) = "@PriPercentAlkalines"
                            parms(32, 1) = "PriPercentAlkalines, " + drCurrent("PriPercentAlkalines").ToString()
                            parms(33, 0) = "@PriGrindIndexVanad"
                            parms(33, 1) = "PriGrindIndexVanad, " + drCurrent("PriGrindIndexVanad").ToString()
                            parms(34, 0) = "@PriAshSoftTemp"
                            parms(34, 1) = "PriAshSoftTemp, " + drCurrent("PriAshSoftTemp").ToString()
                            parms(35, 0) = "@SecFuelCode"
                            parms(35, 1) = "SecFuelCode, " + drCurrent("SecFuelCode").ToString()
                            parms(36, 0) = "@SecQtyBurned"
                            parms(36, 1) = "SecQtyBurned, " + drCurrent("SecQtyBurned").ToString()
                            parms(37, 0) = "@SecAvgHeatContent"
                            parms(37, 1) = "SecAvgHeatContent, " + drCurrent("SecAvgHeatContent").ToString()
                            parms(38, 0) = "@SecBtus"
                            parms(38, 1) = "SecBtus, " + drCurrent("SecBtus").ToString()
                            parms(39, 0) = "@SecPercentAsh"
                            parms(39, 1) = "SecPercentAsh, " + drCurrent("SecPercentAsh").ToString()
                            parms(40, 0) = "@SecPercentMoisture"
                            parms(40, 1) = "SecPercentMoisture, " + drCurrent("SecPercentMoisture").ToString()
                            parms(41, 0) = "@SecPercentSulfur"
                            parms(41, 1) = "SecPercentSulfur, " + drCurrent("SecPercentSulfur").ToString()
                            parms(42, 0) = "@SecPercentAlkalines"
                            parms(42, 1) = "SecPercentAlkalines, " + drCurrent("SecPercentAlkalines").ToString()
                            parms(43, 0) = "@SecGrindIndexVanad"
                            parms(43, 1) = "SecGrindIndexVanad, " + drCurrent("SecGrindIndexVanad").ToString()
                            parms(44, 0) = "@SecAshSoftTemp"
                            parms(44, 1) = "SecAshSoftTemp, " + drCurrent("SecAshSoftTemp").ToString()
                            parms(45, 0) = "@RevisionCard4"
                            If drCurrent("RevisionCard4").ToString() = "*" Then
                                parms(45, 1) = "RevisionCard4, 0"
                            Else
                                parms(45, 1) = "RevisionCard4, " + drCurrent("RevisionCard4").ToString()
                            End If
                            parms(46, 0) = "@TerFuelCode"
                            parms(46, 1) = "TerFuelCode, " + drCurrent("TerFuelCode").ToString()
                            parms(47, 0) = "@TerQtyBurned"
                            parms(47, 1) = "TerQtyBurned, " + drCurrent("TerQtyBurned").ToString()
                            parms(48, 0) = "@TerAvgHeatContent"
                            parms(48, 1) = "TerAvgHeatContent, " + drCurrent("TerAvgHeatContent").ToString()
                            parms(49, 0) = "@TerBtus"
                            parms(49, 1) = "TerBtus, " + drCurrent("TerBtus").ToString()
                            parms(50, 0) = "@TerPercentAsh"
                            parms(50, 1) = "TerPercentAsh, " + drCurrent("TerPercentAsh").ToString()
                            parms(51, 0) = "@TerPercentMoisture"
                            parms(51, 1) = "TerPercentMoisture, " + drCurrent("TerPercentMoisture").ToString()
                            parms(52, 0) = "@TerPercentSulfur"
                            parms(52, 1) = "TerPercentSulfur, " + drCurrent("TerPercentSulfur").ToString()
                            parms(53, 0) = "@TerPercentAlkalines"
                            parms(53, 1) = "TerPercentAlkalines, " + drCurrent("TerPercentAlkalines").ToString()
                            parms(54, 0) = "@TerGrindIndexVanad"
                            parms(54, 1) = "TerGrindIndexVanad, " + drCurrent("TerGrindIndexVanad").ToString()
                            parms(55, 0) = "@TerAshSoftTemp"
                            parms(55, 1) = "TerAshSoftTemp, " + drCurrent("TerAshSoftTemp").ToString()
                            parms(56, 0) = "@QuaFuelCode"
                            parms(56, 1) = "QuaFuelCode, " + drCurrent("QuaFuelCode").ToString()
                            parms(57, 0) = "@QuaQtyBurned"
                            parms(57, 1) = "QuaQtyBurned, " + drCurrent("QuaQtyBurned").ToString()
                            parms(58, 0) = "@QuaAvgHeatContent"
                            parms(58, 1) = "QuaAvgHeatContent, " + drCurrent("QuaAvgHeatContent").ToString()
                            parms(59, 0) = "@QuaBtus"
                            parms(59, 1) = "QuaBtus, " + drCurrent("QuaBtus").ToString()
                            parms(60, 0) = "@QuaPercentAsh"
                            parms(60, 1) = "QuaPercentAsh, " + drCurrent("QuaPercentAsh").ToString()
                            parms(61, 0) = "@QuaPercentMoisture"
                            parms(61, 1) = "QuaPercentMoisture, " + drCurrent("QuaPercentMoisture").ToString()
                            parms(62, 0) = "@QuaPercentSulfur"
                            parms(62, 1) = "QuaPercentSulfur, " + drCurrent("QuaPercentSulfur").ToString()
                            parms(63, 0) = "@QuaPercentAlkalines"
                            parms(63, 1) = "QuaPercentAlkalines, " + drCurrent("QuaPercentAlkalines").ToString()
                            parms(64, 0) = "@QuaGrindIndexVanad"
                            parms(64, 1) = "QuaGrindIndexVanad, " + drCurrent("QuaGrindIndexVanad").ToString()
                            parms(65, 0) = "@QuaAshSoftTemp"
                            parms(65, 1) = "QuaAshSoftTemp, " + drCurrent("QuaAshSoftTemp").ToString()
                            parms(66, 0) = "@RevMonthCard1"
                            parms(66, 1) = "RevMonthCard1, " + drCurrent("RevMonthCard1").ToString()
                            parms(67, 0) = "@RevMonthCard2"
                            parms(67, 1) = "RevMonthCard2, " + drCurrent("RevMonthCard2").ToString()
                            parms(68, 0) = "@RevMonthCard3"
                            parms(68, 1) = "RevMonthCard3, " + drCurrent("RevMonthCard3").ToString()
                            parms(69, 0) = "@RevMonthCard4"
                            parms(69, 1) = "RevMonthCard4, " + drCurrent("RevMonthCard4").ToString()

                            If lJO = True Then

                                parms(70, 0) = "@JOGrossMaxCap"
                                parms(70, 1) = "JOGrossMaxCap, " + drCurrent("JOGrossMaxCap").ToString()
                                parms(71, 0) = "@JOGrossGen"
                                parms(71, 1) = "JOGrossGen, " + drCurrent("JOGrossGen").ToString()
                                parms(72, 0) = "@JONetMaxCap"
                                parms(72, 1) = "JONetMaxCap, " + drCurrent("JONetMaxCap").ToString()
                                parms(73, 0) = "@JONetGen"
                                parms(73, 1) = "JONetGen, " + drCurrent("JONetGen").ToString()
                                parms(74, 0) = "@JOPriQtyBurned"
                                parms(74, 1) = "JOPriQtyBurned, " + drCurrent("JOPriQtyBurned").ToString()
                                parms(75, 0) = "@JOSecQtyBurned"
                                parms(75, 1) = "JOSecQtyBurned, " + drCurrent("JOSecQtyBurned").ToString()
                                parms(76, 0) = "@JOTerQtyBurned"
                                parms(76, 1) = "JOTerQtyBurned, " + drCurrent("JOTerQtyBurned").ToString()
                                parms(77, 0) = "@JOQuaQtyBurned"
                                parms(77, 1) = "JOQuaQtyBurned, " + drCurrent("JOQuaQtyBurned").ToString()

                            Else

                                'parms(70, 0) = "@JOGrossMaxCap"
                                'parms(70, 1) = "JOGrossMaxCap, " + drCurrent("GrossMaxCap").ToString()
                                'parms(71, 0) = "@JOGrossGen"
                                'parms(71, 1) = "JOGrossGen, " + drCurrent("GrossGen").ToString()
                                'parms(72, 0) = "@JONetMaxCap"
                                'parms(72, 1) = "JONetMaxCap, " + drCurrent("NetMaxCap").ToString()
                                'parms(73, 0) = "@JONetGen"
                                'parms(73, 1) = "JONetGen, " + drCurrent("NetGen").ToString()
                                'parms(74, 0) = "@JOPriQtyBurned"
                                'parms(74, 1) = "JOPriQtyBurned, " + drCurrent("PriQtyBurned").ToString()
                                'parms(75, 0) = "@JOSecQtyBurned"
                                'parms(75, 1) = "JOSecQtyBurned, " + drCurrent("SecQtyBurned").ToString()
                                'parms(76, 0) = "@JOTerQtyBurned"
                                'parms(76, 1) = "JOTerQtyBurned, " + drCurrent("TerQtyBurned").ToString()
                                'parms(77, 0) = "@JOQuaQtyBurned"
                                'parms(77, 1) = "JOQuaQtyBurned, " + drCurrent("QuaQtyBurned").ToString()

                                parms(70, 0) = "@JOGrossMaxCap"
                                parms(70, 1) = "JOGrossMaxCap, "
                                parms(71, 0) = "@JOGrossGen"
                                parms(71, 1) = "JOGrossGen, "
                                parms(72, 0) = "@JONetMaxCap"
                                parms(72, 1) = "JONetMaxCap, "
                                parms(73, 0) = "@JONetGen"
                                parms(73, 1) = "JONetGen, "
                                parms(74, 0) = "@JOPriQtyBurned"
                                parms(74, 1) = "JOPriQtyBurned, "
                                parms(75, 0) = "@JOSecQtyBurned"
                                parms(75, 1) = "JOSecQtyBurned, "
                                parms(76, 0) = "@JOTerQtyBurned"
                                parms(76, 1) = "JOTerQtyBurned, "
                                parms(77, 0) = "@JOQuaQtyBurned"
                                parms(77, 1) = "JOQuaQtyBurned, "


                            End If

                            parms(78, 0) = "@TimeStamp"
                            parms(78, 1) = "TimeStamp, " + dtUTCNow.ToString
                            parms(79, 0) = "@UnitShortName"
                            If IsDBNull(drSetup.Item("UnitShortName")) Then
                                parms(79, 1) = "UnitShortName, UNKNOWN"
                            Else
                                parms(79, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                            End If

                            parms(80, 0) = "@InactiveHours"
                            If IsDBNull(drCurrent("InactiveHours")) Then
                                parms(80, 1) = "InactiveHours, 0"
                            Else
                                parms(80, 1) = "InactiveHours, " + drCurrent("InactiveHours").ToString()
                            End If

                            intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                        Else

                            ' *** The row exists so use UPDATE *** 

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@Original_UtilityUnitCode"
                            parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(1, 0) = "@Original_Year"
                            parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(2, 0) = "@Original_Period"
                            parms(2, 1) = "Period, " + drCurrent("Period").ToString()

                            intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                            ' Loading up the Performance 01 card

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@RevisionCard1"
                            parms(0, 1) = "RevisionCard1, " + drCurrent("RevisionCard1").ToString()
                            parms(1, 0) = "@GrossMaxCap"
                            parms(1, 1) = "GrossMaxCap, " + drCurrent("GrossMaxCap").ToString()
                            parms(2, 0) = "@GrossDepCap"
                            parms(2, 1) = "GrossDepCap, " + drCurrent("GrossDepCap").ToString()
                            parms(3, 0) = "@GrossGen"
                            parms(3, 1) = "GrossGen, " + drCurrent("GrossGen").ToString()
                            parms(4, 0) = "@NetMaxCap"
                            parms(4, 1) = "NetMaxCap, " + drCurrent("NetMaxCap").ToString()
                            parms(5, 0) = "@NetDepCap"
                            parms(5, 1) = "NetDepCap, " + drCurrent("NetDepCap").ToString()
                            parms(6, 0) = "@NetGen"
                            parms(6, 1) = "NetGen, " + drCurrent("NetGen").ToString()
                            parms(7, 0) = "@TypUnitLoading"
                            parms(7, 1) = "TypUnitLoading, " + drCurrent("TypUnitLoading").ToString()
                            parms(8, 0) = "@AttemptedStarts"
                            parms(8, 1) = "AttemptedStarts, " + drCurrent("AttemptedStarts").ToString()
                            parms(9, 0) = "@ActualStarts"
                            parms(9, 1) = "ActualStarts, " + drCurrent("ActualStarts").ToString()
                            parms(10, 0) = "@VerbalDesc"
                            parms(10, 1) = "VerbalDesc, " + drCurrent("VerbalDesc").ToString()

                            If lJO = True Then
                                parms(11, 0) = "@JOGrossMaxCap"
                                parms(11, 1) = "JOGrossMaxCap, " + drCurrent("JOGrossMaxCap").ToString()
                                parms(12, 0) = "@JOGrossGen"
                                parms(12, 1) = "JOGrossGen, " + drCurrent("JOGrossGen").ToString()
                                parms(13, 0) = "@JONetMaxCap"
                                parms(13, 1) = "JONetMaxCap, " + drCurrent("JONetMaxCap").ToString()
                                parms(14, 0) = "@JONetGen"
                                parms(14, 1) = "JONetGen, " + drCurrent("JONetGen").ToString()
                            Else
                                'parms(11, 0) = "@JOGrossMaxCap"
                                'parms(11, 1) = "JOGrossMaxCap, " + drCurrent("GrossMaxCap").ToString()
                                'parms(12, 0) = "@JOGrossGen"
                                'parms(12, 1) = "JOGrossGen, " + drCurrent("GrossGen").ToString()
                                'parms(13, 0) = "@JONetMaxCap"
                                'parms(13, 1) = "JONetMaxCap, " + drCurrent("NetMaxCap").ToString()
                                'parms(14, 0) = "@JONetGen"
                                'parms(14, 1) = "JONetGen, " + drCurrent("NetGen").ToString()

                                parms(11, 0) = "@JOGrossMaxCap"
                                parms(11, 1) = "JOGrossMaxCap, "
                                parms(12, 0) = "@JOGrossGen"
                                parms(12, 1) = "JOGrossGen, "
                                parms(13, 0) = "@JONetMaxCap"
                                parms(13, 1) = "JONetMaxCap, "
                                parms(14, 0) = "@JONetGen"
                                parms(14, 1) = "JONetGen, "
                            End If

                            parms(15, 0) = "@RevMonthCard1"
                            parms(15, 1) = "RevMonthCard1, " + drCurrent("RevMonthCard1").ToString()
                            parms(16, 0) = "@TimeStamp"
                            parms(16, 1) = "TimeStamp, " + dtUTCNow.ToString

                            parms(17, 0) = "@Original_Period"
                            parms(17, 1) = "Period, " + drCurrent("Period").ToString()
                            parms(18, 0) = "@Original_UtilityUnitCode"
                            parms(18, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(19, 0) = "@Original_Year"
                            parms(19, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(20, 0) = "@Original_RevisionCard1"
                            parms(20, 1) = "RevisionCard1, " + drCurrent("RevisionCard1").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement1, parms)

                            ' Loading up the Performance 02 card

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@RevisionCard2"
                            parms(0, 1) = "RevisionCard2, " + drCurrent("RevisionCard2").ToString()
                            parms(1, 0) = "@ServiceHours"
                            parms(1, 1) = "ServiceHours, " + drCurrent("ServiceHours").ToString()
                            parms(2, 0) = "@RSHours"
                            parms(2, 1) = "RSHours, " + drCurrent("RSHours").ToString()
                            parms(3, 0) = "@PumpingHours"
                            parms(3, 1) = "PumpingHours, " + drCurrent("PumpingHours").ToString()
                            parms(4, 0) = "@SynchCondHours"
                            parms(4, 1) = "SynchCondHours, " + drCurrent("SynchCondHours").ToString()
                            parms(5, 0) = "@PlannedOutageHours"
                            parms(5, 1) = "PlannedOutageHours, " + drCurrent("PlannedOutageHours").ToString()
                            parms(6, 0) = "@ForcedOutageHours"
                            parms(6, 1) = "ForcedOutageHours, " + drCurrent("ForcedOutageHours").ToString()
                            parms(7, 0) = "@MaintOutageHours"
                            parms(7, 1) = "MaintOutageHours, " + drCurrent("MaintOutageHours").ToString()
                            parms(8, 0) = "@ExtofSchedOutages"
                            parms(8, 1) = "ExtofSchedOutages, " + drCurrent("ExtofSchedOutages").ToString()
                            parms(9, 0) = "@PeriodHours"
                            parms(9, 1) = "PeriodHours, " + drCurrent("PeriodHours").ToString()
                            parms(10, 0) = "@RevMonthCard2"
                            parms(10, 1) = "RevMonthCard2, " + drCurrent("RevMonthCard2").ToString()
                            parms(11, 0) = "@TimeStamp"
                            parms(11, 1) = "TimeStamp, " + dtUTCNow.ToString

                            parms(12, 0) = "@InactiveHours"
                            If IsDBNull(drCurrent("InactiveHours")) Then
                                parms(12, 1) = "InactiveHours, 0"
                            Else
                                parms(12, 1) = "InactiveHours, " + drCurrent("InactiveHours").ToString()
                            End If

                            parms(13, 0) = "@Original_Period"
                            parms(13, 1) = "Period, " + drCurrent("Period").ToString()
                            parms(14, 0) = "@Original_UtilityUnitCode"
                            parms(14, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(15, 0) = "@Original_Year"
                            parms(15, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(16, 0) = "@Original_RevisionCard2"
                            parms(16, 1) = "RevisionCard2, " + drCurrent("RevisionCard2").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement2, parms)

                            ' Loading up the Performance 03 card

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@RevisionCard3"
                            parms(0, 1) = "RevisionCard3, " + drCurrent("RevisionCard3").ToString()
                            parms(1, 0) = "@PriFuelCode"
                            parms(1, 1) = "PriFuelCode, " + drCurrent("PriFuelCode").ToString()
                            parms(2, 0) = "@PriQtyBurned"
                            parms(2, 1) = "PriQtyBurned, " + drCurrent("PriQtyBurned").ToString()
                            parms(3, 0) = "@PriAvgHeatContent"
                            parms(3, 1) = "PriAvgHeatContent, " + drCurrent("PriAvgHeatContent").ToString()
                            parms(4, 0) = "@PriBtus"
                            parms(4, 1) = "PriBtus, " + drCurrent("PriBtus").ToString()
                            parms(5, 0) = "@PriPercentAsh"
                            parms(5, 1) = "PriPercentAsh, " + drCurrent("PriPercentAsh").ToString()
                            parms(6, 0) = "@PriPercentMoisture"
                            parms(6, 1) = "PriPercentMoisture, " + drCurrent("PriPercentMoisture").ToString()
                            parms(7, 0) = "@PriPercentSulfur"
                            parms(7, 1) = "PriPercentSulfur, " + drCurrent("PriPercentSulfur").ToString()
                            parms(8, 0) = "@PriPercentAlkalines"
                            parms(8, 1) = "PriPercentAlkalines, " + drCurrent("PriPercentAlkalines").ToString()
                            parms(9, 0) = "@PriGrindIndexVanad"
                            parms(9, 1) = "PriGrindIndexVanad, " + drCurrent("PriGrindIndexVanad").ToString()
                            parms(10, 0) = "@PriAshSoftTemp"
                            parms(10, 1) = "PriAshSoftTemp, " + drCurrent("PriAshSoftTemp").ToString()
                            parms(11, 0) = "@SecFuelCode"
                            parms(11, 1) = "SecFuelCode, " + drCurrent("SecFuelCode").ToString()
                            parms(12, 0) = "@SecQtyBurned"
                            parms(12, 1) = "SecQtyBurned, " + drCurrent("SecQtyBurned").ToString()
                            parms(13, 0) = "@SecAvgHeatContent"
                            parms(13, 1) = "SecAvgHeatContent, " + drCurrent("SecAvgHeatContent").ToString()
                            parms(14, 0) = "@SecBtus"
                            parms(14, 1) = "SecBtus, " + drCurrent("SecBtus").ToString()
                            parms(15, 0) = "@SecPercentAsh"
                            parms(15, 1) = "SecPercentAsh, " + drCurrent("SecPercentAsh").ToString()
                            parms(16, 0) = "@SecPercentMoisture"
                            parms(16, 1) = "SecPercentMoisture, " + drCurrent("SecPercentMoisture").ToString()
                            parms(17, 0) = "@SecPercentSulfur"
                            parms(17, 1) = "SecPercentSulfur, " + drCurrent("SecPercentSulfur").ToString()
                            parms(18, 0) = "@SecPercentAlkalines"
                            parms(18, 1) = "SecPercentAlkalines, " + drCurrent("SecPercentAlkalines").ToString()
                            parms(19, 0) = "@SecGrindIndexVanad"
                            parms(19, 1) = "SecGrindIndexVanad, " + drCurrent("SecGrindIndexVanad").ToString()
                            parms(20, 0) = "@SecAshSoftTemp"
                            parms(20, 1) = "SecAshSoftTemp, " + drCurrent("SecAshSoftTemp").ToString()

                            If lJO = True Then
                                parms(21, 0) = "@JOPriQtyBurned"
                                parms(21, 1) = "JOPriQtyBurned, " + drCurrent("JOPriQtyBurned").ToString()
                                parms(22, 0) = "@JOSecQtyBurned"
                                parms(22, 1) = "JOSecQtyBurned, " + drCurrent("JOSecQtyBurned").ToString()
                            Else
                                'parms(21, 0) = "@JOPriQtyBurned"
                                'parms(21, 1) = "JOPriQtyBurned, " + drCurrent("PriQtyBurned").ToString()
                                'parms(22, 0) = "@JOSecQtyBurned"
                                'parms(22, 1) = "JOSecQtyBurned, " + drCurrent("SecQtyBurned").ToString()

                                parms(21, 0) = "@JOPriQtyBurned"
                                parms(21, 1) = "JOPriQtyBurned, "
                                parms(22, 0) = "@JOSecQtyBurned"
                                parms(22, 1) = "JOSecQtyBurned, "
                            End If

                            parms(23, 0) = "@RevMonthCard3"
                            parms(23, 1) = "RevMonthCard3, " + drCurrent("RevMonthCard3").ToString()
                            parms(24, 0) = "@TimeStamp"
                            parms(24, 1) = "TimeStamp, " + dtUTCNow.ToString

                            parms(25, 0) = "@Original_Period"
                            parms(25, 1) = "Period, " + drCurrent("Period").ToString()
                            parms(26, 0) = "@Original_UtilityUnitCode"
                            parms(26, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(27, 0) = "@Original_Year"
                            parms(27, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(28, 0) = "@Original_RevisionCard3"
                            parms(28, 1) = "RevisionCard3, " + drCurrent("RevisionCard3").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement3, parms)

                            ' Loading up the Performance 04 card

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@RevisionCard4"
                            parms(0, 1) = "RevisionCard4, " + drCurrent("RevisionCard4").ToString()
                            parms(1, 0) = "@TerFuelCode"
                            parms(1, 1) = "TerFuelCode, " + drCurrent("TerFuelCode").ToString()
                            parms(2, 0) = "@TerQtyBurned"
                            parms(2, 1) = "TerQtyBurned, " + drCurrent("TerQtyBurned").ToString()
                            parms(3, 0) = "@TerAvgHeatContent"
                            parms(3, 1) = "TerAvgHeatContent, " + drCurrent("TerAvgHeatContent").ToString()
                            parms(4, 0) = "@TerBtus"
                            parms(4, 1) = "TerBtus, " + drCurrent("TerBtus").ToString()
                            parms(5, 0) = "@TerPercentAsh"
                            parms(5, 1) = "TerPercentAsh, " + drCurrent("TerPercentAsh").ToString()
                            parms(6, 0) = "@TerPercentMoisture"
                            parms(6, 1) = "TerPercentMoisture, " + drCurrent("TerPercentMoisture").ToString()
                            parms(7, 0) = "@TerPercentSulfur"
                            parms(7, 1) = "TerPercentSulfur, " + drCurrent("TerPercentSulfur").ToString()
                            parms(8, 0) = "@TerPercentAlkalines"
                            parms(8, 1) = "TerPercentAlkalines, " + drCurrent("TerPercentAlkalines").ToString()
                            parms(9, 0) = "@TerGrindIndexVanad"
                            parms(9, 1) = "TerGrindIndexVanad, " + drCurrent("TerGrindIndexVanad").ToString()
                            parms(10, 0) = "@TerAshSoftTemp"
                            parms(10, 1) = "TerAshSoftTemp, " + drCurrent("TerAshSoftTemp").ToString()
                            parms(11, 0) = "@QuaFuelCode"
                            parms(11, 1) = "QuaFuelCode, " + drCurrent("QuaFuelCode").ToString()
                            parms(12, 0) = "@QuaQtyBurned"
                            parms(12, 1) = "QuaQtyBurned, " + drCurrent("QuaQtyBurned").ToString()
                            parms(13, 0) = "@QuaAvgHeatContent"
                            parms(13, 1) = "QuaAvgHeatContent, " + drCurrent("QuaAvgHeatContent").ToString()
                            parms(14, 0) = "@QuaBtus"
                            parms(14, 1) = "QuaBtus, " + drCurrent("QuaBtus").ToString()
                            parms(15, 0) = "@QuaPercentAsh"
                            parms(15, 1) = "QuaPercentAsh, " + drCurrent("QuaPercentAsh").ToString()
                            parms(16, 0) = "@QuaPercentMoisture"
                            parms(16, 1) = "QuaPercentMoisture, " + drCurrent("QuaPercentMoisture").ToString()
                            parms(17, 0) = "@QuaPercentSulfur"
                            parms(17, 1) = "QuaPercentSulfur, " + drCurrent("QuaPercentSulfur").ToString()
                            parms(18, 0) = "@QuaPercentAlkalines"
                            parms(18, 1) = "QuaPercentAlkalines, " + drCurrent("QuaPercentAlkalines").ToString()
                            parms(19, 0) = "@QuaGrindIndexVanad"
                            parms(19, 1) = "QuaGrindIndexVanad, " + drCurrent("QuaGrindIndexVanad").ToString()
                            parms(20, 0) = "@QuaAshSoftTemp"
                            parms(20, 1) = "QuaAshSoftTemp, " + drCurrent("QuaAshSoftTemp").ToString()

                            If lJO = True Then
                                parms(21, 0) = "@JOTerQtyBurned"
                                parms(21, 1) = "JOTerQtyBurned, " + drCurrent("JOTerQtyBurned").ToString()
                                parms(22, 0) = "@JOQuaQtyBurned"
                                parms(22, 1) = "JOQuaQtyBurned, " + drCurrent("JOQuaQtyBurned").ToString()
                            Else
                                'parms(21, 0) = "@JOTerQtyBurned"
                                'parms(21, 1) = "JOTerQtyBurned, " + drCurrent("TerQtyBurned").ToString()
                                'parms(22, 0) = "@JOQuaQtyBurned"
                                'parms(22, 1) = "JOQuaQtyBurned, " + drCurrent("QuaQtyBurned").ToString()

                                parms(21, 0) = "@JOTerQtyBurned"
                                parms(21, 1) = "JOTerQtyBurned, "
                                parms(22, 0) = "@JOQuaQtyBurned"
                                parms(22, 1) = "JOQuaQtyBurned, "
                            End If

                            parms(23, 0) = "@RevMonthCard4"
                            parms(23, 1) = "RevMonthCard4, " + drCurrent("RevMonthCard4").ToString()
                            parms(24, 0) = "@TimeStamp"
                            parms(24, 1) = "TimeStamp, " + dtUTCNow.ToString

                            parms(25, 0) = "@Original_Period"
                            parms(25, 1) = "Period, " + drCurrent("Period").ToString()
                            parms(26, 0) = "@Original_UtilityUnitCode"
                            parms(26, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(27, 0) = "@Original_Year"
                            parms(27, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(28, 0) = "@Original_RevisionCard4"
                            parms(28, 1) = "RevisionCard4, " + drCurrent("RevisionCard4").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement4, parms)

                        End If

                    End If

                Next

                intUpdateRows = Factory.ExecuteNonQuery("UPDATE PerformanceData SET InactiveHours = 0 WHERE (InactiveHours IS NULL)", Nothing)

                UpdateSetupCheckStatus(dsSetup)

                Return True

            Catch e As System.Exception

                Me.ThrowGADSNGException("Error in LoadPerformance", e)
                Return False

            End Try

        End Function

#End Region

#Region " FormLoadEventData01(Byref dsload As DataSet, ByVal myUser As String) As String "
        Public Function FormLoadEventData01(ByRef dsload As DataSet, ByVal myUser As String) As String

            Dim parms(80, 2) As String
            Dim AuditParms(12, 2) As String
            Dim intRows As Integer
            Dim intUpdateRows As Integer
            Dim intRevNo As Integer
            Dim arrayUniCode(2) As Char
            arrayUniCode(0) = Chr(10)
            arrayUniCode(1) = Chr(13)
            arrayUniCode(2) = Chr(9)

            Dim insertStatement1 As String
            Dim updateStatement1 As String
            Dim deleteStatement1 As String

            Dim insertAuditStatement1 As String
            Dim insertAuditStatement2 As String

            Dim insertStatement2 As String
            Dim updateStatement2 As String
            Dim deleteStatement2 As String

            insertStatement1 = "INSERT INTO EventData01 " & _
                "(UtilityUnitCode, Year, EventNumber, RevisionCard01, EventType, StartDateTime, CarryOverLastYear, " & _
                "ChangeDateTime1, ChangeInEventType1, ChangeDateTime2, ChangeInEventType2, EndDateTime, CarryOverNextYear, " & _
                "GrossAvailCapacity, NetAvailCapacity, CauseCode, CauseCodeExt, WorkStarted, WorkEnded, " & _
                "ContribCode, PrimaryAlert, ManhoursWorked, RevisionCard02, RevisionCard03, RevMonthCard01, RevMonthCard02, RevMonthCard03, " & _
                "FailureMechCode, TripMech, CumFiredHours, CumEngineStarts, DominantDerate, VerbalDesc86, TimeStamp, EditFlag, VerbalDescFull, VerbalDesc1, PJMIOCode, UnitShortName) " & _
                "VALUES " + _
                "(@UtilityUnitCode, @Year, @EventNumber, @RevisionCard01, @EventType, @StartDateTime, @CarryOverLastYear, " & _
                "@ChangeDateTime1, @ChangeInEventType1, @ChangeDateTime2, @ChangeInEventType2, @EndDateTime, @CarryOverNextYear, " & _
                "@GrossAvailCapacity, @NetAvailCapacity, @CauseCode, @CauseCodeExt, @WorkStarted, @WorkEnded, " & _
                "@ContribCode, @PrimaryAlert, @ManhoursWorked, @RevisionCard02, @RevisionCard03, @RevMonthCard01, @RevMonthCard02, @RevMonthCard03, " & _
                "@FailureMechCode, @TripMech, @CumFiredHours, @CumEngineStarts, @DominantDerate, @VerbalDesc86, @TimeStamp, @EditFlag, @VerbalDescFull, @VerbalDesc1, @PJMIOCode, @UnitShortName)"

            insertAuditStatement1 = "INSERT INTO AuditEventData01 " & _
                "(UtilityUnitCode, Year, EventNumber, RevisionCard01, " & _
                "RevisionCard02, RevisionCard03, RevMonthCard01, RevMonthCard02, RevMonthCard03, " & _
                "LoginID, TimeStamp, UnitShortName) " & _
                "VALUES " + _
                "(@UtilityUnitCode, @Year, @EventNumber, @RevisionCard01, @RevisionCard02, @RevisionCard03, " & _
                "@RevMonthCard01, @RevMonthCard02, @RevMonthCard03, " & _
                "@LoginID, @TimeStamp, @UnitShortName)"

            insertStatement2 = "INSERT INTO EventData02 " & _
                "(UtilityUnitCode, Year, EventNumber, RevisionCardEven, EventType, CauseCode, CauseCodeExt, WorkStarted, WorkEnded, " & _
                "ContribCode, PrimaryAlert, ManhoursWorked, VerbalDesc1, EvenCardNumber, " & _
                "VerbalDesc2, RevisionCardOdd, RevMonthCardEven, RevMonthCardOdd, EditFlag, VerbalDesc86, VerbalDescFull, " & _
                "FailureMechCode, TripMech, CumFiredHours, CumEngineStarts, TimeStamp, UnitShortName) " & _
                "VALUES " + _
                "(@UtilityUnitCode, @Year, @EventNumber, @RevisionCardEven, @EventType, @CauseCode, @CauseCodeExt, @WorkStarted, @WorkEnded, " & _
                "@ContribCode, @PrimaryAlert, @ManhoursWorked, @VerbalDesc1, @EvenCardNumber, " & _
                "@VerbalDesc2, @RevisionCardOdd, @RevMonthCardEven, @RevMonthCardOdd, @EditFlag, @VerbalDesc86, @VerbalDescFull, " & _
                "@FailureMechCode, @TripMech, @CumFiredHours, @CumEngineStarts, @TimeStamp, @UnitShortName)"

            insertAuditStatement2 = "INSERT INTO AuditEventData02 " & _
                "(UtilityUnitCode, Year, EventNumber, RevisionCardEven, EvenCardNumber, " & _
                "RevisionCardOdd, RevMonthCardEven, RevMonthCardOdd, " & _
                "LoginID, TimeStamp, UnitShortName) " & _
                "VALUES " + _
                "(@UtilityUnitCode, @Year, @EventNumber, @RevisionCardEven, @EvenCardNumber, " & _
                "@RevisionCardOdd, @RevMonthCardEven, @RevMonthCardOdd, " & _
                "@LoginID, @TimeStamp, @UnitShortName)"

            updateStatement1 = "UPDATE EventData01 SET " & _
                "RevisionCard01 = @RevisionCard01, " & _
                "EventType = @EventType, " & _
                "StartDateTime = @StartDateTime, " & _
                "CarryOverLastYear = @CarryOverLastYear, " & _
                "ChangeDateTime1 = @ChangeDateTime1, " & _
                "ChangeInEventType1 = @ChangeInEventType1, " & _
                "ChangeDateTime2 = @ChangeDateTime2, " & _
                "ChangeInEventType2 = @ChangeInEventType2, " & _
                "EndDateTime = @EndDateTime, " & _
                "CarryOverNextYear = @CarryOverNextYear, " & _
                "GrossAvailCapacity = @GrossAvailCapacity, " & _
                "NetAvailCapacity = @NetAvailCapacity, " & _
                "CauseCode = @CauseCode, " & _
                "CauseCodeExt = @CauseCodeExt, " & _
                "WorkStarted = @WorkStarted, " & _
                "WorkEnded = @WorkEnded, " & _
                "ContribCode = @ContribCode, " & _
                "PrimaryAlert = @PrimaryAlert, " & _
                "ManhoursWorked = @ManhoursWorked, " & _
                "RevisionCard02 = @RevisionCard02, " & _
                "RevisionCard03 = @RevisionCard03, " & _
                "RevMonthCard01 = @RevMonthCard01, " & _
                "RevMonthCard02 = @RevMonthCard02, " & _
                "RevMonthCard03 = @RevMonthCard03, " & _
                "FailureMechCode = @FailureMechCode, " & _
                "TripMech = @TripMech, " & _
                "CumFiredHours = @CumFiredHours, " & _
                "CumEngineStarts = @CumEngineStarts, " & _
                "DominantDerate = @DominantDerate, " & _
                "VerbalDesc86 = @VerbalDesc86, " & _
                "TimeStamp = @TimeStamp, " & _
                "EditFlag = @EditFlag, " & _
                "VerbalDescFull = @VerbalDescFull, " & _
                "PJMIOCode = @PJMIOCode " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber)"
            '"(EventNumber = @Original_EventNumber) AND " & _
            '"(TimeStamp = @Original_TimeStamp)"

            updateStatement2 = "UPDATE EventData02 SET " & _
                "RevisionCardEven = @RevisionCardEven, " & _
                "EventType = @EventType, " & _
                "CauseCode = @CauseCode, " & _
                "CauseCodeExt = @CauseCodeExt, " & _
                "WorkStarted = @WorkStarted, " & _
                "WorkEnded = @WorkEnded, " & _
                "ContribCode = @ContribCode, " & _
                "PrimaryAlert = @PrimaryAlert, " & _
                "ManhoursWorked = @ManhoursWorked, " & _
                "VerbalDesc1 = @VerbalDesc1, " & _
                "VerbalDesc2 = @VerbalDesc2, " & _
                "RevisionCardOdd = @RevisionCardOdd, " & _
                "RevMonthCardEven = @RevMonthCardEven, " & _
                "RevMonthCardOdd = @RevMonthCardOdd, " & _
                "EditFlag = @EditFlag, " & _
                "VerbalDesc86 = @VerbalDesc86, " & _
                "VerbalDescFull = @VerbalDescFull, " & _
                "FailureMechCode = @FailureMechCode, " & _
                "TripMech = @TripMech, " & _
                "CumFiredHours = @CumFiredHours, " & _
                "CumEngineStarts = @CumEngineStarts, " & _
                "TimeStamp = @TimeStamp " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(EvenCardNumber = @Original_EvenCardNumber)"
            '"(EvenCardNumber = @Original_EvenCardNumber) AND " & _
            '"(TimeStamp = @Original_TimeStamp)"


            deleteStatement1 = "UPDATE EventData01 SET " & _
                "RevisionCard01 = 'X', " & _
                "RevMonthCard01 = @RevMonthCard01, " & _
                "TimeStamp = @TimeStamp, " & _
                "EditFlag = @EditFlag " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber)"
            '"(TimeStamp = @Original_TimeStamp)"

            deleteStatement2 = "UPDATE EventData02 SET " & _
                "RevisionCardEven = 'X', " & _
                "RevMonthCardEven = @RevMonthCardEven, " & _
                "TimeStamp = @TimeStamp, " & _
                "EditFlag = @EditFlag " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(EvenCardNumber = @Original_EvenCardNumber)"
            '"(TimeStamp = @Original_TimeStamp)"

            Dim selectStatement As String

            Dim buildDsSetupStatement As String
            buildDsSetupStatement = "Select UtilityUnitCode, CheckStatus, UnitShortName FROM Setup"

            Dim dsSetup As DataSet
            dsSetup = Me.Factory.GetDataSet(buildDsSetupStatement, Nothing)

            If Factory.Provider = "OleDb" Then
                Dim keys(0) As DataColumn
                keys(0) = dsSetup.Tables(0).Columns.Item("UtilityUnitCode")
                dsSetup.Tables(0).PrimaryKey = keys
            End If

            Dim drSetup As DataRow

            Dim dtStampNow As DateTime
            dtStampNow = System.DateTime.UtcNow

            Dim dtTemp1 As DataTable
            Dim dtTemp2 As DataTable

            Dim drEvent1 As DataRow
            Dim drEvent2 As DataRow

            Dim strTemp As String = String.Empty

            ' Looking for new rows

            ' ------------------------------------
            ' Event 01-03 records into EventData01
            ' ------------------------------------

            selectStatement = "SELECT COUNT(*) FROM EventData01 WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber)"

            dtTemp1 = dsload.Tables("EventData01").GetChanges(DataRowState.Added)

            If Not dtTemp1 Is Nothing Then

                If dtTemp1.Rows.Count > 0 Then

                    drEvent1 = dtTemp1.Rows(0)

                    drSetup = dsSetup.Tables(0).Rows.Find(drEvent1("UtilityUnitCode").ToString)

                    If drSetup Is Nothing Then
                        Return "Unit does not exist in Setup"
                    Else
                        ' The Unit is in Setup
                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()
                    End If

                    For Each drEvent1 In dtTemp1.Rows

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drEvent1("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent1("EventNumber").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows <> 0 Then

                            drEvent1.RowError = "Event " & drEvent1("EventNumber").ToString() & " has already been added by another user"
                            Return ("Event " & drEvent1("EventNumber").ToString() & " has already been added by another user")

                        End If

                        ' *** The row does not exist so use INSERT ***

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Year"
                        parms(1, 1) = "Year, " + drEvent1("Year").ToString()
                        parms(2, 0) = "@EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent1("EventNumber").ToString()
                        parms(3, 0) = "@RevisionCard01"
                        parms(3, 1) = "RevisionCard01, 0"
                        parms(4, 0) = "@EventType"
                        parms(4, 1) = "EventType, " + drEvent1("EventType").ToString()
                        parms(5, 0) = "@StartDateTime"
                        parms(5, 1) = "StartDateTime, " + drEvent1("StartDateTime").ToString()
                        parms(6, 0) = "@CarryOverLastYear"
                        parms(6, 1) = "CarryOverLastYear, " + drEvent1("CarryOverLastYear").ToString()

                        parms(7, 0) = "@ChangeDateTime1"
                        parms(7, 1) = "ChangeDateTime1, " + drEvent1("ChangeDateTime1").ToString()
                        parms(8, 0) = "@ChangeInEventType1"
                        parms(8, 1) = "ChangeInEventType1, " + drEvent1("ChangeInEventType1").ToString()
                        parms(9, 0) = "@ChangeDateTime2"
                        parms(9, 1) = "ChangeDateTime2, " + drEvent1("ChangeDateTime2").ToString()
                        parms(10, 0) = "@ChangeInEventType2"
                        parms(10, 1) = "ChangeInEventType2, " + drEvent1("ChangeInEventType2").ToString()
                        parms(11, 0) = "@EndDateTime"
                        parms(11, 1) = "EndDateTime, " + drEvent1("EndDateTime").ToString()
                        parms(12, 0) = "@CarryOverNextYear"
                        parms(12, 1) = "CarryOverNextYear, " + drEvent1("CarryOverNextYear").ToString()

                        parms(13, 0) = "@GrossAvailCapacity"
                        parms(13, 1) = "GrossAvailCapacity, " + drEvent1("GrossAvailCapacity").ToString()
                        parms(14, 0) = "@NetAvailCapacity"
                        parms(14, 1) = "NetAvailCapacity, " + drEvent1("NetAvailCapacity").ToString()
                        parms(15, 0) = "@CauseCode"
                        parms(15, 1) = "CauseCode, " + drEvent1("CauseCode").ToString()
                        parms(16, 0) = "@CauseCodeExt"
                        If IsDBNull(drEvent1("CauseCodeExt")) Then
                            drEvent1("CauseCodeExt") = "  "
                        ElseIf drEvent1("CauseCodeExt").ToString().Trim() = String.Empty Then
                            drEvent1("CauseCodeExt") = "  "
                        End If
                        parms(16, 1) = "CauseCodeExt, " + drEvent1("CauseCodeExt").ToString()
                        parms(17, 0) = "@WorkStarted"
                        parms(17, 1) = "WorkStarted, " + drEvent1("WorkStarted").ToString()
                        parms(18, 0) = "@WorkEnded"
                        parms(18, 1) = "WorkEnded, " + drEvent1("WorkEnded").ToString()

                        parms(19, 0) = "@ContribCode"
                        parms(19, 1) = "ContribCode, " + drEvent1("ContribCode").ToString()
                        parms(20, 0) = "@PrimaryAlert"
                        parms(20, 1) = "PrimaryAlert, " + drEvent1("PrimaryAlert").ToString()
                        parms(21, 0) = "@ManhoursWorked"
                        parms(21, 1) = "ManhoursWorked, " + drEvent1("ManhoursWorked").ToString()
                        parms(22, 0) = "@RevisionCard02"
                        parms(22, 1) = "RevisionCard02, 0"

                        parms(23, 0) = "@RevisionCard03"
                        parms(23, 1) = "RevisionCard03, 0"
                        parms(24, 0) = "@RevMonthCard01"
                        parms(24, 1) = "RevMonthCard01, " + dtStampNow.ToString
                        parms(25, 0) = "@RevMonthCard02"
                        parms(25, 1) = "RevMonthCard02, " + dtStampNow.ToString
                        parms(26, 0) = "@RevMonthCard03"
                        parms(26, 1) = "RevMonthCard03, " + dtStampNow.ToString

                        parms(27, 0) = "@FailureMechCode"
                        parms(27, 1) = "FailureMechCode, " + drEvent1("FailureMechCode").ToString()
                        parms(28, 0) = "@TripMech"
                        parms(28, 1) = "TripMech, " + drEvent1("TripMech").ToString()
                        parms(29, 0) = "@CumFiredHours"
                        parms(29, 1) = "CumFiredHours, " + drEvent1("CumFiredHours").ToString()
                        parms(30, 0) = "@CumEngineStarts"
                        parms(30, 1) = "CumEngineStarts, " + drEvent1("CumEngineStarts").ToString()
                        parms(31, 0) = "@DominantDerate"
                        parms(31, 1) = "DominantDerate, " + drEvent1("DominantDerate").ToString()

                        If drEvent1("VerbalDesc86").ToString.IndexOfAny(arrayUniCode) > -1 Then
                            strTemp = drEvent1("VerbalDesc86").ToString.Replace(ChrW(9), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(10), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(13), ChrW(32))
                            drEvent1("VerbalDesc86") = strTemp
                        End If

                        parms(32, 0) = "@VerbalDesc86"
                        parms(32, 1) = "VerbalDesc86, " + drEvent1("VerbalDesc86").ToString()

                        parms(33, 0) = "@TimeStamp"
                        parms(33, 1) = "TimeStamp, " + dtStampNow.ToString
                        parms(34, 0) = "@EditFlag"
                        parms(34, 1) = "EditFlag, True"
                        parms(35, 0) = "@VerbalDescFull"
                        parms(35, 1) = "VerbalDescFull, " + drEvent1("VerbalDescFull").ToString()
                        parms(36, 0) = "@VerbalDesc1"
                        parms(36, 1) = "VerbalDesc1, NA"
                        parms(37, 0) = "@PJMIOCode"
                        parms(37, 1) = "PJMIOCode, " & drEvent1("PJMIOCode").ToString
                        parms(38, 0) = "@UnitShortName"
                        If IsDBNull(drSetup.Item("UnitShortName")) Then
                            parms(38, 1) = "UnitShortName, UNKNOWN"
                        Else
                            parms(38, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                        End If

                        intUpdateRows = Factory.ExecuteNonQuery(insertStatement1, parms)

                        Try
                            System.Array.Clear(AuditParms, 0, AuditParms.Length)

                            AuditParms(0, 0) = "@UtilityUnitCode"
                            AuditParms(0, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode").ToString()
                            AuditParms(1, 0) = "@Year"
                            AuditParms(1, 1) = "Year, " + drEvent1("Year").ToString()
                            AuditParms(2, 0) = "@EventNumber"
                            AuditParms(2, 1) = "EventNumber, " + drEvent1("EventNumber").ToString()
                            AuditParms(3, 0) = "@RevisionCard01"
                            AuditParms(3, 1) = "RevisionCard01, " + drEvent1("RevisionCard01").ToString()
                            AuditParms(4, 0) = "@RevisionCard02"
                            AuditParms(4, 1) = "RevisionCard02, " + drEvent1("RevisionCard02").ToString()

                            AuditParms(5, 0) = "@RevisionCard03"
                            AuditParms(5, 1) = "RevisionCard03, " + drEvent1("RevisionCard03").ToString()
                            AuditParms(6, 0) = "@RevMonthCard01"
                            AuditParms(6, 1) = "RevMonthCard01, " + drEvent1("RevMonthCard01").ToString()
                            AuditParms(7, 0) = "@RevMonthCard02"
                            AuditParms(7, 1) = "RevMonthCard02, " + drEvent1("RevMonthCard02").ToString()
                            AuditParms(8, 0) = "@RevMonthCard03"
                            AuditParms(8, 1) = "RevMonthCard03, " + drEvent1("RevMonthCard03").ToString()

                            AuditParms(9, 0) = "@LoginID"
                            AuditParms(9, 1) = "LoginID, " + myUser
                            AuditParms(10, 0) = "@TimeStamp"
                            AuditParms(10, 1) = "TimeStamp, " + dtStampNow.ToString

                            AuditParms(11, 0) = "@UnitShortName"
                            If IsDBNull(drSetup.Item("UnitShortName")) Then
                                AuditParms(11, 1) = "UnitShortName, UNKNOWN"
                            Else
                                AuditParms(11, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                            End If

                            intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement1, AuditParms)

                        Catch ex As Exception
                            insertAuditStatement1 = ex.ToString
                        End Try

                    Next

                End If

            End If

            ' Looking for modified rows
            dtTemp1 = dsload.Tables("EventData01").GetChanges(DataRowState.Modified)

            If Not dtTemp1 Is Nothing Then

                If dtTemp1.Rows.Count > 0 Then

                    drEvent1 = dtTemp1.Rows(0)

                    drSetup = dsSetup.Tables(0).Rows.Find(drEvent1("UtilityUnitCode").ToString)

                    If drSetup Is Nothing Then
                        Return "Unit does not exist in Setup"
                    Else
                        ' The Unit is in Setup
                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()
                    End If

                    For Each drEvent1 In dtTemp1.Rows

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drEvent1("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent1("EventNumber").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows = 0 Then

                            drEvent1.RowError = "Event " & drEvent1("EventNumber").ToString() & " has been removed from the database"
                            Return ("Event " & drEvent1("EventNumber").ToString() & " has been removed from the database")

                        End If

                        ' *** The row exists so use UPDATE ***
                        If drEvent1("RevMonthCard01", DataRowVersion.Original).ToString <> String.Empty Then

                            If Not (DateTime.Parse(drEvent1("RevMonthCard01", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                                DateTime.Parse(drEvent1("RevMonthCard01", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                                If drEvent1("RevisionCard01").ToString.ToUpper <> "X" Then

                                    intRevNo = Convert.ToInt16(drEvent1("RevisionCard01").ToString)
                                    intRevNo += 1

                                    If intRevNo > 9 Then
                                        intRevNo = 9
                                    End If

                                    drEvent1("RevisionCard01") = intRevNo.ToString
                                    drEvent1("RevMonthCard01") = dtStampNow
                                Else
                                    drEvent1("RevMonthCard01") = dtStampNow
                                End If

                            End If

                        End If


                        If drEvent1("RevMonthCard02", DataRowVersion.Original).ToString <> String.Empty Then

                            If Not (DateTime.Parse(drEvent1("RevMonthCard02", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                                DateTime.Parse(drEvent1("RevMonthCard02", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                                If drEvent1("RevisionCard02").ToString.ToUpper <> "X" Then

                                    intRevNo = Convert.ToInt16(drEvent1("RevisionCard02").ToString)
                                    intRevNo += 1

                                    If intRevNo > 9 Then
                                        intRevNo = 9
                                    End If

                                    drEvent1("RevisionCard02") = intRevNo.ToString
                                    drEvent1("RevMonthCard02") = dtStampNow

                                End If

                            End If

                        End If

                        If drEvent1("RevMonthCard03", DataRowVersion.Original).ToString <> String.Empty Then

                            If Not (DateTime.Parse(drEvent1("RevMonthCard03", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                                DateTime.Parse(drEvent1("RevMonthCard03", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                                If drEvent1("RevisionCard03").ToString.ToUpper <> "X" Then

                                    intRevNo = Convert.ToInt16(drEvent1("RevisionCard03").ToString)
                                    intRevNo += 1

                                    If intRevNo > 9 Then
                                        intRevNo = 9
                                    End If

                                    drEvent1("RevisionCard03") = intRevNo.ToString
                                    drEvent1("RevMonthCard03") = dtStampNow

                                End If

                            End If

                        End If

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@RevisionCard01"
                        parms(0, 1) = "RevisionCard01, " + drEvent1("RevisionCard01").ToString()
                        parms(1, 0) = "@EventType"
                        parms(1, 1) = "EventType, " + drEvent1("EventType").ToString()
                        parms(2, 0) = "@StartDateTime"
                        parms(2, 1) = "StartDateTime, " + drEvent1("StartDateTime").ToString()
                        parms(3, 0) = "@CarryOverLastYear"
                        parms(3, 1) = "CarryOverLastYear, " + drEvent1("CarryOverLastYear").ToString()
                        parms(4, 0) = "@ChangeDateTime1"
                        parms(4, 1) = "ChangeDateTime1, " ' + drEvent1("ChangeDateTime1").ToString()
                        parms(5, 0) = "@ChangeInEventType1"
                        parms(5, 1) = "ChangeInEventType1, " ' + drEvent1("ChangeInEventType1").ToString()
                        parms(6, 0) = "@ChangeDateTime2"
                        parms(6, 1) = "ChangeDateTime2, " ' + drEvent1("ChangeDateTime2").ToString()
                        parms(7, 0) = "@ChangeInEventType2"
                        parms(7, 1) = "ChangeInEventType2, " ' + drEvent1("ChangeInEventType2").ToString()
                        parms(8, 0) = "@EndDateTime"
                        parms(8, 1) = "EndDateTime, " + drEvent1("EndDateTime").ToString()
                        parms(9, 0) = "@CarryOverNextYear"
                        parms(9, 1) = "CarryOverNextYear, " + drEvent1("CarryOverNextYear").ToString()
                        parms(10, 0) = "@GrossAvailCapacity"
                        parms(10, 1) = "GrossAvailCapacity, " ' + drEvent1("GrossAvailCapacity").ToString()
                        parms(11, 0) = "@NetAvailCapacity"
                        parms(11, 1) = "NetAvailCapacity, " + drEvent1("NetAvailCapacity").ToString()
                        parms(12, 0) = "@CauseCode"
                        parms(12, 1) = "CauseCode, " + drEvent1("CauseCode").ToString()
                        parms(13, 0) = "@CauseCodeExt"
                        If IsDBNull(drEvent1("CauseCodeExt")) Then
                            drEvent1("CauseCodeExt") = "  "
                        ElseIf drEvent1("CauseCodeExt").ToString().Trim() = String.Empty Then
                            drEvent1("CauseCodeExt") = "  "
                        End If
                        parms(13, 1) = "CauseCodeExt, " + drEvent1("CauseCodeExt").ToString()
                        parms(14, 0) = "@WorkStarted"
                        parms(14, 1) = "WorkStarted, " ' + drEvent1("WorkStarted").ToString()
                        parms(15, 0) = "@WorkEnded"
                        parms(15, 1) = "WorkEnded, " ' + drEvent1("WorkEnded").ToString()
                        parms(16, 0) = "@ContribCode"
                        parms(16, 1) = "ContribCode, " + drEvent1("ContribCode").ToString()
                        parms(17, 0) = "@PrimaryAlert"
                        parms(17, 1) = "PrimaryAlert, " ' + drEvent1("PrimaryAlert").ToString()
                        parms(18, 0) = "@ManhoursWorked"
                        parms(18, 1) = "ManhoursWorked, " ' + drEvent1("ManhoursWorked").ToString()
                        parms(19, 0) = "@RevisionCard02"
                        parms(19, 1) = "RevisionCard02, " + drEvent1("RevisionCard02").ToString()
                        parms(20, 0) = "@RevisionCard03"
                        parms(20, 1) = "RevisionCard03, " + drEvent1("RevisionCard03").ToString()
                        parms(21, 0) = "@RevMonthCard01"
                        parms(21, 1) = "RevMonthCard01, " + drEvent1("RevMonthCard01").ToString()
                        parms(22, 0) = "@RevMonthCard02"
                        parms(22, 1) = "RevMonthCard02, " + drEvent1("RevMonthCard02").ToString()
                        parms(23, 0) = "@RevMonthCard03"
                        parms(23, 1) = "RevMonthCard03, " + drEvent1("RevMonthCard03").ToString()
                        parms(24, 0) = "@FailureMechCode"
                        parms(24, 1) = "FailureMechCode, " ' + drEvent1("FailureMechCode").ToString()
                        parms(25, 0) = "@TripMech"
                        parms(25, 1) = "TripMech, " ' + drEvent1("TripMech").ToString()
                        parms(26, 0) = "@CumFiredHours"
                        parms(26, 1) = "CumFiredHours, " ' + drEvent1("CumFiredHours").ToString()
                        parms(27, 0) = "@CumEngineStarts"
                        parms(27, 1) = "CumEngineStarts, " ' + drEvent1("CumEngineStarts").ToString()
                        parms(28, 0) = "@DominantDerate"
                        parms(28, 1) = "DominantDerate, " + drEvent1("DominantDerate").ToString()

                        If drEvent1("VerbalDesc86").ToString.IndexOfAny(arrayUniCode) > -1 Then
                            strTemp = drEvent1("VerbalDesc86").ToString.Replace(ChrW(9), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(10), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(13), ChrW(32))
                            drEvent1("VerbalDesc86") = strTemp
                        End If

                        parms(29, 0) = "@VerbalDesc86"
                        parms(29, 1) = "VerbalDesc86, " + drEvent1("VerbalDesc86").ToString()

                        parms(30, 0) = "@TimeStamp"
                        parms(30, 1) = "TimeStamp, " + dtStampNow.ToString
                        parms(31, 0) = "@EditFlag"
                        parms(31, 1) = "EditFlag, true"
                        parms(32, 0) = "@VerbalDescFull"
                        parms(32, 1) = "VerbalDescFull, " + drEvent1("VerbalDescFull").ToString()
                        parms(33, 0) = "@PJMIOCode"
                        parms(33, 1) = "PJMIOCode, " & drEvent1("PJMIOCode").ToString
                        parms(34, 0) = "@Original_UtilityUnitCode"
                        parms(34, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode", DataRowVersion.Original).ToString
                        parms(35, 0) = "@Original_Year"
                        parms(35, 1) = "Year, " + drEvent1("Year", DataRowVersion.Original).ToString
                        parms(36, 0) = "@Original_EventNumber"
                        parms(36, 1) = "EventNumber, " + drEvent1("EventNumber", DataRowVersion.Original).ToString
                        'parms(37, 0) = "@Original_TimeStamp"
                        'parms(37, 1) = "TimeStamp, " + drEvent1("TimeStamp", DataRowVersion.Original).ToString

                        intUpdateRows = Factory.ExecuteNonQuery(updateStatement1, parms)

                        Try
                            System.Array.Clear(AuditParms, 0, AuditParms.Length)

                            AuditParms(0, 0) = "@UtilityUnitCode"
                            AuditParms(0, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode").ToString()
                            AuditParms(1, 0) = "@Year"
                            AuditParms(1, 1) = "Year, " + drEvent1("Year").ToString()
                            AuditParms(2, 0) = "@EventNumber"
                            AuditParms(2, 1) = "EventNumber, " + drEvent1("EventNumber").ToString()
                            AuditParms(3, 0) = "@RevisionCard01"
                            AuditParms(3, 1) = "RevisionCard01, " + drEvent1("RevisionCard01").ToString()
                            AuditParms(4, 0) = "@RevisionCard02"
                            AuditParms(4, 1) = "RevisionCard02, " + drEvent1("RevisionCard02").ToString()

                            AuditParms(5, 0) = "@RevisionCard03"
                            AuditParms(5, 1) = "RevisionCard03, " + drEvent1("RevisionCard03").ToString()
                            AuditParms(6, 0) = "@RevMonthCard01"
                            AuditParms(6, 1) = "RevMonthCard01, " + drEvent1("RevMonthCard01").ToString()
                            AuditParms(7, 0) = "@RevMonthCard02"
                            AuditParms(7, 1) = "RevMonthCard02, " + drEvent1("RevMonthCard02").ToString()
                            AuditParms(8, 0) = "@RevMonthCard03"
                            AuditParms(8, 1) = "RevMonthCard03, " + drEvent1("RevMonthCard03").ToString()

                            AuditParms(9, 0) = "@LoginID"
                            AuditParms(9, 1) = "LoginID, " + myUser
                            AuditParms(10, 0) = "@TimeStamp"
                            AuditParms(10, 1) = "TimeStamp, " + dtStampNow.ToString

                            AuditParms(11, 0) = "@UnitShortName"
                            If IsDBNull(drSetup.Item("UnitShortName")) Then
                                AuditParms(11, 1) = "UnitShortName, UNKNOWN"
                            Else
                                AuditParms(11, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                            End If

                            intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement1, AuditParms)

                        Catch ex As Exception
                            insertAuditStatement1 = ex.ToString
                        End Try

                    Next

                End If

            End If

            ' Looking for deleted rows

            dtTemp1 = dsload.Tables("EventData01").GetChanges(DataRowState.Deleted)

            If Not dtTemp1 Is Nothing Then

                If dtTemp1.Rows.Count > 0 Then

                    dtTemp1.RejectChanges()

                    'Dim drvTemp As DataRowView

                    For Each drEvent1 In dtTemp1.Rows

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drEvent1("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent1("EventNumber").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows > 0 Then

                            If MessageBox.Show("Event number " & drEvent1("EventNumber").ToString() & " (" & drEvent1("EventType").ToString() & ") will be deleted." & vbCrLf & vbCrLf & _
                                               "Do you wish to delete this event?" & vbCrLf & vbCrLf & _
                                               "To DELETE the event, press Yes...", _
                                               "DELETING EVENT " & drEvent1("EventNumber").ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then

                                ' Delete only (REV = X) those records that are already in the database
                                ' Deleted records that are only in the dataset can just be deleted from the dataset or ignored

                                'deleteStatement1 = "UPDATE EventData01 SET " & _
                                '    "RevisionCard01 = 'X', " & _
                                '    "RevMonthCard01 = @RevMonthCard01, " & _
                                '    "TimeStamp = @TimeStamp, " & _
                                '    "EditFlag = @EditFlag " & _
                                '"WHERE " & _
                                '    "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                                '    "(Year = @Original_Year) AND " & _
                                '    "(EventNumber = @Original_EventNumber) AND " & _
                                '    "(TimeStamp = @Original_TimeStamp)"

                                ' *** The row exists so use UPDATE ***

                                System.Array.Clear(parms, 0, parms.Length)

                                parms(0, 0) = "@RevMonthCard01"
                                parms(0, 1) = "RevMonthCard01, " + dtStampNow.ToString
                                parms(1, 0) = "@TimeStamp"
                                parms(1, 1) = "TimeStamp, " + dtStampNow.ToString
                                parms(2, 0) = "@EditFlag"
                                parms(2, 1) = "EditFlag, true"

                                parms(3, 0) = "@Original_UtilityUnitCode"
                                parms(3, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode", DataRowVersion.Original).ToString
                                parms(4, 0) = "@Original_Year"
                                parms(4, 1) = "Year, " + drEvent1("Year", DataRowVersion.Original).ToString
                                parms(5, 0) = "@Original_EventNumber"
                                parms(5, 1) = "EventNumber, " + drEvent1("EventNumber", DataRowVersion.Original).ToString
                                'parms(6, 0) = "@Original_TimeStamp"
                                'parms(6, 1) = "TimeStamp, " + drEvent1("TimeStamp", DataRowVersion.Original).ToString

                                intUpdateRows = Factory.ExecuteNonQuery(deleteStatement1, parms)

                                ' When we delete the 01-03 records, it also deletes the associated 04-99 records (marks them as deleted)

                                drSetup = dsSetup.Tables(0).Rows.Find(drEvent1("UtilityUnitCode").ToString)

                                If drSetup Is Nothing Then
                                    Return "Unit does not exist in Setup"
                                Else
                                    ' The Unit is in Setup
                                    drSetup.Item("CheckStatus") = "X"
                                    drSetup.AcceptChanges()
                                End If

                                Try
                                    System.Array.Clear(AuditParms, 0, AuditParms.Length)

                                    AuditParms(0, 0) = "@UtilityUnitCode"
                                    AuditParms(0, 1) = "UtilityUnitCode, " + drEvent1("UtilityUnitCode").ToString()
                                    AuditParms(1, 0) = "@Year"
                                    AuditParms(1, 1) = "Year, " + drEvent1("Year").ToString()
                                    AuditParms(2, 0) = "@EventNumber"
                                    AuditParms(2, 1) = "EventNumber, " + drEvent1("EventNumber").ToString()
                                    AuditParms(3, 0) = "@RevisionCard01"
                                    AuditParms(3, 1) = "RevisionCard01, X"
                                    AuditParms(4, 0) = "@RevisionCard02"
                                    AuditParms(4, 1) = "RevisionCard02, " + drEvent1("RevisionCard02").ToString()

                                    AuditParms(5, 0) = "@RevisionCard03"
                                    AuditParms(5, 1) = "RevisionCard03, " + drEvent1("RevisionCard03").ToString()
                                    AuditParms(6, 0) = "@RevMonthCard01"
                                    AuditParms(6, 1) = "RevMonthCard01, " + dtStampNow.ToString
                                    AuditParms(7, 0) = "@RevMonthCard02"
                                    AuditParms(7, 1) = "RevMonthCard02, " + drEvent1("RevMonthCard02").ToString()
                                    AuditParms(8, 0) = "@RevMonthCard03"
                                    AuditParms(8, 1) = "RevMonthCard03, " + drEvent1("RevMonthCard03").ToString()

                                    AuditParms(9, 0) = "@LoginID"
                                    AuditParms(9, 1) = "LoginID, " + myUser
                                    AuditParms(10, 0) = "@TimeStamp"
                                    AuditParms(10, 1) = "TimeStamp, " + dtStampNow.ToString

                                    AuditParms(11, 0) = "@UnitShortName"
                                    If IsDBNull(drSetup.Item("UnitShortName")) Then
                                        AuditParms(11, 1) = "UnitShortName, UNKNOWN"
                                    Else
                                        AuditParms(11, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                                    End If

                                    intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement1, AuditParms)

                                Catch ex As Exception
                                    insertAuditStatement1 = ex.ToString
                                End Try

                            End If

                        End If

                    Next

                End If

            End If



            ' -----------------------------------
            ' Event 04-99 records for EventData02
            ' -----------------------------------

            selectStatement = "SELECT COUNT(*) FROM EventData02 WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(EvenCardNumber = @Original_EvenCardNumber)"

            ' Looking for new rows
            dtTemp2 = dsload.Tables("EventData02").GetChanges(DataRowState.Added)

            If Not dtTemp2 Is Nothing Then

                If dtTemp2.Rows.Count > 0 Then

                    drEvent2 = dtTemp2.Rows(0)

                    drSetup = dsSetup.Tables(0).Rows.Find(drEvent2("UtilityUnitCode").ToString)

                    If drSetup Is Nothing Then
                        Return "Unit does not exist in Setup"
                    Else
                        ' The Unit is in Setup
                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()
                    End If


                    For Each drEvent2 In dtTemp2.Rows

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drEvent2("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent2("EventNumber").ToString()
                        parms(3, 0) = "@Original_EvenCardNumber"
                        parms(3, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber").ToString

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows <> 0 Then

                            drEvent2.RowError = "Event " & drEvent2("EventNumber").ToString() & " Record " & drEvent2("EvenCardNumber").ToString & " has already been added by another user"
                            Return ("Event " & drEvent2("EventNumber").ToString() & " Record " & drEvent2("EvenCardNumber").ToString & " has already been added by another user")

                        End If

                        ' *** The row does not exist so use INSERT ***

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Year"
                        parms(1, 1) = "Year, " + drEvent2("Year").ToString()
                        parms(2, 0) = "@EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent2("EventNumber").ToString()
                        parms(3, 0) = "@RevisionCardEven"
                        parms(3, 1) = "RevisionCardEven, 0"
                        parms(4, 0) = "@EventType"
                        parms(4, 1) = "EventType, " + drEvent2("EventType").ToString()
                        parms(5, 0) = "@CauseCode"
                        parms(5, 1) = "CauseCode, " + drEvent2("CauseCode").ToString()
                        parms(6, 0) = "@CauseCodeExt"
                        If IsDBNull(drEvent2("CauseCodeExt")) Then
                            drEvent2("CauseCodeExt") = "  "
                        ElseIf drEvent2("CauseCodeExt").ToString().Trim() = String.Empty Then
                            drEvent2("CauseCodeExt") = "  "
                        End If
                        parms(6, 1) = "CauseCodeExt, " + drEvent2("CauseCodeExt").ToString()
                        parms(7, 0) = "@WorkStarted"
                        parms(7, 1) = "WorkStarted, " + drEvent2("WorkStarted").ToString()
                        parms(8, 0) = "@WorkEnded"
                        parms(8, 1) = "WorkEnded, " + drEvent2("WorkEnded").ToString()

                        parms(9, 0) = "@ContribCode"
                        parms(9, 1) = "ContribCode, " + drEvent2("ContribCode").ToString()
                        parms(10, 0) = "@PrimaryAlert"
                        parms(10, 1) = "PrimaryAlert, " + drEvent2("PrimaryAlert").ToString()
                        parms(11, 0) = "@ManhoursWorked"
                        parms(11, 1) = "ManhoursWorked, " + drEvent2("ManhoursWorked").ToString()
                        parms(12, 0) = "@VerbalDesc1"
                        parms(12, 1) = "VerbalDesc1, NA"
                        parms(13, 0) = "@EvenCardNumber"
                        parms(13, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber").ToString

                        parms(14, 0) = "@VerbalDesc2"
                        parms(14, 1) = "VerbalDesc2, "
                        parms(15, 0) = "@RevisionCardOdd"
                        parms(15, 1) = "RevisionCardOdd, 0"
                        parms(16, 0) = "@RevMonthCardEven"
                        parms(16, 1) = "RevMonthCardEven, " + dtStampNow.ToString
                        parms(17, 0) = "@RevMonthCardOdd"
                        parms(17, 1) = "RevMonthCardOdd, " + dtStampNow.ToString
                        parms(18, 0) = "@EditFlag"
                        parms(18, 1) = "EditFlag, True"

                        If drEvent2("VerbalDesc86").ToString.IndexOfAny(arrayUniCode) > -1 Then
                            strTemp = drEvent2("VerbalDesc86").ToString.Replace(ChrW(9), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(10), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(13), ChrW(32))
                            drEvent2("VerbalDesc86") = strTemp
                        End If

                        parms(19, 0) = "@VerbalDesc86"
                        parms(19, 1) = "VerbalDesc86, " + drEvent2("VerbalDesc86").ToString()
                        parms(20, 0) = "@VerbalDescFull"
                        parms(20, 1) = "VerbalDescFull, " + drEvent2("VerbalDescFull").ToString()

                        parms(21, 0) = "@FailureMechCode"
                        parms(21, 1) = "FailureMechCode, " + drEvent2("FailureMechCode").ToString()
                        parms(22, 0) = "@TripMech"
                        parms(22, 1) = "TripMech, " + drEvent2("TripMech").ToString()
                        parms(23, 0) = "@CumFiredHours"
                        parms(23, 1) = "CumFiredHours, " + drEvent2("CumFiredHours").ToString()
                        parms(24, 0) = "@CumEngineStarts"
                        parms(24, 1) = "CumEngineStarts, " + drEvent2("CumEngineStarts").ToString()
                        parms(25, 0) = "@TimeStamp"
                        parms(25, 1) = "TimeStamp, " + dtStampNow.ToString
                        parms(26, 0) = "@UnitShortName"
                        If IsDBNull(drSetup.Item("UnitShortName")) Then
                            parms(26, 1) = "UnitShortName, UNKNOWN"
                        Else
                            parms(26, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                        End If

                        intUpdateRows = Factory.ExecuteNonQuery(insertStatement2, parms)


                        Try
                            System.Array.Clear(AuditParms, 0, AuditParms.Length)

                            AuditParms(0, 0) = "@UtilityUnitCode"
                            AuditParms(0, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode").ToString()
                            AuditParms(1, 0) = "@Year"
                            AuditParms(1, 1) = "Year, " + drEvent2("Year").ToString()
                            AuditParms(2, 0) = "@EventNumber"
                            AuditParms(2, 1) = "EventNumber, " + drEvent2("EventNumber").ToString()
                            AuditParms(3, 0) = "@RevisionCardEven"
                            AuditParms(3, 1) = "RevisionCardEven, " + drEvent2("RevisionCardEven").ToString()
                            AuditParms(4, 0) = "@EvenCardNumber"
                            AuditParms(4, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber").ToString

                            AuditParms(5, 0) = "@RevisionCardOdd"
                            AuditParms(5, 1) = "RevisionCardOdd, " + drEvent2("RevisionCardOdd").ToString()
                            AuditParms(6, 0) = "@RevMonthCardEven"
                            AuditParms(6, 1) = "RevMonthCardEven, " + drEvent2("RevMonthCardEven").ToString()
                            AuditParms(7, 0) = "@RevMonthCardOdd"
                            AuditParms(7, 1) = "RevMonthCardOdd, " + drEvent2("RevMonthCardOdd").ToString()

                            AuditParms(8, 0) = "@LoginID"
                            AuditParms(8, 1) = "LoginID, " + myUser
                            AuditParms(9, 0) = "@TimeStamp"
                            AuditParms(9, 1) = "TimeStamp, " + dtStampNow.ToString
                            AuditParms(10, 0) = "@UnitShortName"
                            If IsDBNull(drSetup.Item("UnitShortName")) Then
                                AuditParms(10, 1) = "UnitShortName, UNKNOWN"
                            Else
                                AuditParms(10, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                            End If

                            intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement2, AuditParms)

                        Catch ex As Exception
                            insertAuditStatement2 = ex.ToString
                        End Try

                    Next

                End If

            End If

            ' Looking for modified rows
            dtTemp2 = dsload.Tables("EventData02").GetChanges(DataRowState.Modified)

            If Not dtTemp2 Is Nothing Then

                If dtTemp2.Rows.Count > 0 Then

                    drEvent2 = dtTemp2.Rows(0)

                    drSetup = dsSetup.Tables(0).Rows.Find(drEvent2("UtilityUnitCode").ToString)

                    If drSetup Is Nothing Then
                        Return "Unit does not exist in Setup"
                    Else
                        ' The Unit is in Setup
                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()
                    End If


                    For Each drEvent2 In dtTemp2.Rows

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drEvent2("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent2("EventNumber").ToString()
                        parms(3, 0) = "@Original_EvenCardNumber"
                        parms(3, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber").ToString

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows = 0 Then

                            drEvent2.RowError = "Event " & drEvent2("EventNumber").ToString() & " Record " & drEvent2("EvenCardNumber").ToString & " has been removed from the database"
                            Return ("Event " & drEvent2("EventNumber").ToString() & " Record " & drEvent2("EvenCardNumber").ToString & " has been removed from the database")

                        End If

                        ' *** The row exists so use UPDATE ***

                        If Not (DateTime.Parse(drEvent2("RevMonthCardEven", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                            DateTime.Parse(drEvent2("RevMonthCardEven", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                            If drEvent2("RevisionCardEven").ToString.ToUpper <> "X" Then

                                intRevNo = Convert.ToInt16(drEvent2("RevisionCardEven").ToString)
                                intRevNo += 1

                                If intRevNo > 9 Then
                                    intRevNo = 9
                                End If

                                drEvent2("RevisionCardEven") = intRevNo.ToString
                                drEvent2("RevMonthCardEven") = dtStampNow

                            End If

                        End If

                        If Not (DateTime.Parse(drEvent2("RevMonthCardOdd", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                            DateTime.Parse(drEvent2("RevMonthCardOdd", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                            If drEvent2("RevisionCardOdd").ToString.ToUpper <> "X" Then

                                intRevNo = Convert.ToInt16(drEvent2("RevisionCardOdd").ToString)
                                intRevNo += 1

                                If intRevNo > 9 Then
                                    intRevNo = 9
                                End If

                                drEvent2("RevisionCardOdd") = intRevNo.ToString
                                drEvent2("RevMonthCardOdd") = dtStampNow

                            End If

                        End If


                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@RevisionCardEven"
                        parms(0, 1) = "RevisionCardEven, " + drEvent2("RevisionCardEven").ToString()
                        parms(1, 0) = "@EventType"
                        parms(1, 1) = "EventType, " + drEvent2("EventType").ToString()
                        parms(2, 0) = "@CauseCode"
                        parms(2, 1) = "CauseCode, " + drEvent2("CauseCode").ToString()
                        parms(3, 0) = "@CauseCodeExt"
                        If IsDBNull(drEvent2("CauseCodeExt")) Then
                            drEvent2("CauseCodeExt") = "  "
                        ElseIf drEvent2("CauseCodeExt").ToString().Trim() = String.Empty Then
                            drEvent2("CauseCodeExt") = "  "
                        End If
                        parms(3, 1) = "CauseCodeExt, " + drEvent2("CauseCodeExt").ToString()
                        parms(4, 0) = "@WorkStarted"
                        parms(4, 1) = "WorkStarted, " + drEvent2("WorkStarted").ToString()
                        parms(5, 0) = "@WorkEnded"
                        parms(5, 1) = "WorkEnded, " + drEvent2("WorkEnded").ToString()
                        parms(6, 0) = "@ContribCode"
                        parms(6, 1) = "ContribCode, " + drEvent2("ContribCode").ToString()
                        parms(7, 0) = "@PrimaryAlert"
                        parms(7, 1) = "PrimaryAlert, " + drEvent2("PrimaryAlert").ToString()
                        parms(8, 0) = "@ManhoursWorked"
                        parms(8, 1) = "ManhoursWorked, " + drEvent2("ManhoursWorked").ToString()
                        parms(9, 0) = "@VerbalDesc1"
                        parms(9, 1) = "VerbalDesc1, NA"
                        parms(10, 0) = "@VerbalDesc2"
                        parms(10, 1) = "VerbalDesc2, "
                        parms(11, 0) = "@RevisionCardOdd"
                        parms(11, 1) = "RevisionCardOdd, " + drEvent2("RevisionCardOdd").ToString()
                        parms(12, 0) = "@RevMonthCardEven"
                        parms(12, 1) = "RevMonthCardEven, " + drEvent2("RevMonthCardEven").ToString()
                        parms(13, 0) = "@RevMonthCardOdd"
                        parms(13, 1) = "RevMonthCardOdd, " + drEvent2("RevMonthCardOdd").ToString()
                        parms(14, 0) = "@EditFlag"
                        parms(14, 1) = "EditFlag, true"

                        If drEvent2("VerbalDesc86").ToString.IndexOfAny(arrayUniCode) > -1 Then
                            strTemp = drEvent2("VerbalDesc86").ToString.Replace(ChrW(9), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(10), ChrW(32))
                            strTemp = strTemp.Replace(ChrW(13), ChrW(32))
                            drEvent2("VerbalDesc86") = strTemp
                        End If

                        parms(15, 0) = "@VerbalDesc86"
                        parms(15, 1) = "VerbalDesc86, " + drEvent2("VerbalDesc86").ToString()
                        parms(16, 0) = "@VerbalDescFull"
                        parms(16, 1) = "VerbalDescFull, " + drEvent2("VerbalDescFull").ToString()
                        parms(17, 0) = "@FailureMechCode"
                        parms(17, 1) = "FailureMechCode, " + drEvent2("FailureMechCode").ToString()
                        parms(18, 0) = "@TripMech"
                        parms(18, 1) = "TripMech, " + drEvent2("TripMech").ToString()
                        parms(19, 0) = "@CumFiredHours"
                        parms(19, 1) = "CumFiredHours, " + drEvent2("CumFiredHours").ToString()
                        parms(20, 0) = "@CumEngineStarts"
                        parms(20, 1) = "CumEngineStarts, " + drEvent2("CumEngineStarts").ToString()
                        parms(21, 0) = "@TimeStamp"
                        parms(21, 1) = "TimeStamp, " + dtStampNow.ToString
                        parms(22, 0) = "@Original_UtilityUnitCode"
                        parms(22, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode", DataRowVersion.Original).ToString
                        parms(23, 0) = "@Original_Year"
                        parms(23, 1) = "Year, " + drEvent2("Year", DataRowVersion.Original).ToString
                        parms(24, 0) = "@Original_EventNumber"
                        parms(24, 1) = "EventNumber, " + drEvent2("EventNumber", DataRowVersion.Original).ToString
                        parms(25, 0) = "@Original_EvenCardNumber"
                        parms(25, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber", DataRowVersion.Original).ToString
                        'parms(26, 0) = "@Original_TimeStamp"
                        'parms(26, 1) = "TimeStamp, " + drEvent2("TimeStamp", DataRowVersion.Original).ToString

                        intUpdateRows = Factory.ExecuteNonQuery(updateStatement2, parms)

                        Try
                            System.Array.Clear(AuditParms, 0, AuditParms.Length)

                            AuditParms(0, 0) = "@UtilityUnitCode"
                            AuditParms(0, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode").ToString()
                            AuditParms(1, 0) = "@Year"
                            AuditParms(1, 1) = "Year, " + drEvent2("Year").ToString()
                            AuditParms(2, 0) = "@EventNumber"
                            AuditParms(2, 1) = "EventNumber, " + drEvent2("EventNumber").ToString()
                            AuditParms(3, 0) = "@RevisionCardEven"
                            AuditParms(3, 1) = "RevisionCardEven, " + drEvent2("RevisionCardEven").ToString()
                            AuditParms(4, 0) = "@EvenCardNumber"
                            AuditParms(4, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber").ToString

                            AuditParms(5, 0) = "@RevisionCardOdd"
                            AuditParms(5, 1) = "RevisionCardOdd, " + drEvent2("RevisionCardOdd").ToString()
                            AuditParms(6, 0) = "@RevMonthCardEven"
                            AuditParms(6, 1) = "RevMonthCardEven, " + drEvent2("RevMonthCardEven").ToString()
                            AuditParms(7, 0) = "@RevMonthCardOdd"
                            AuditParms(7, 1) = "RevMonthCardOdd, " + drEvent2("RevMonthCardOdd").ToString()

                            AuditParms(8, 0) = "@LoginID"
                            AuditParms(8, 1) = "LoginID, " + myUser
                            AuditParms(9, 0) = "@TimeStamp"
                            AuditParms(9, 1) = "TimeStamp, " + dtStampNow.ToString
                            AuditParms(10, 0) = "@UnitShortName"
                            If IsDBNull(drSetup.Item("UnitShortName")) Then
                                AuditParms(10, 1) = "UnitShortName, UNKNOWN"
                            Else
                                AuditParms(10, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                            End If

                            intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement2, AuditParms)

                        Catch ex As Exception
                            insertAuditStatement2 = ex.ToString
                        End Try

                    Next

                End If

            End If

            ' Looking for deleted rows

            dtTemp2 = dsload.Tables("EventData02").GetChanges(DataRowState.Deleted)

            If Not dtTemp2 Is Nothing Then

                If dtTemp2.Rows.Count > 0 Then

                    dtTemp2.RejectChanges()

                    For Each drEvent2 In dtTemp2.Rows

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drEvent2("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drEvent2("EventNumber").ToString()
                        parms(3, 0) = "@Original_EvenCardNumber"
                        parms(3, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber").ToString

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows > 0 Then

                            If MessageBox.Show("Event number " & drEvent2("EventNumber").ToString() & " Additional Work record will be deleted." & vbCrLf & vbCrLf & _
                                               "Do you wish to delete card " & drEvent2("EvenCardNumber").ToString & " for this event?" & vbCrLf & vbCrLf & _
                                               "To DELETE card " & drEvent2("EvenCardNumber").ToString & ", press Yes...", _
                                               "DELETING ADDITIONAL WORK FOR EVENT " & drEvent2("EventNumber").ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then

                                ' Delete only (REV = X) those records that are already in the database
                                ' Deleted records that are only in the dataset can just be deleted from the dataset or ignored

                                'deleteStatement2 = "UPDATE EventData02 SET " & _
                                '   "RevisionCardEven = 'X', " & _
                                '   "RevMonthCardEven = @RevMonthCardEven, " & _
                                '   "TimeStamp = @TimeStamp, " & _
                                '   "EditFlag = @EditFlag " & _
                                '"WHERE " & _
                                '   "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                                '   "(Year = @Original_Year) AND " & _
                                '   "(EventNumber = @Original_EventNumber) AND " & _
                                '   "(EvenCardNumber = @Original_EvenCardNumber) AND " & _
                                '   "(TimeStamp = @Original_TimeStamp)"

                                ' *** The row exists so use UPDATE ***

                                System.Array.Clear(parms, 0, parms.Length)

                                parms(0, 0) = "@RevMonthCardEven"
                                parms(0, 1) = "RevMonthCardEven, " + dtStampNow.ToString
                                parms(1, 0) = "@TimeStamp"
                                parms(1, 1) = "TimeStamp, " + dtStampNow.ToString
                                parms(2, 0) = "@EditFlag"
                                parms(2, 1) = "EditFlag, true"

                                parms(3, 0) = "@Original_UtilityUnitCode"
                                parms(3, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode", DataRowVersion.Original).ToString
                                parms(4, 0) = "@Original_Year"
                                parms(4, 1) = "Year, " + drEvent2("Year", DataRowVersion.Original).ToString
                                parms(5, 0) = "@Original_EventNumber"
                                parms(5, 1) = "EventNumber, " + drEvent2("EventNumber", DataRowVersion.Original).ToString
                                parms(6, 0) = "@Original_EvenCardNumber"
                                parms(6, 1) = "EvenCardNumber, " + drEvent2("EvenCardNumber").ToString
                                'parms(7, 0) = "@Original_TimeStamp"
                                'parms(7, 1) = "TimeStamp, " + drEvent2("TimeStamp", DataRowVersion.Original).ToString

                                intUpdateRows = Factory.ExecuteNonQuery(deleteStatement2, parms)

                                Try
                                    System.Array.Clear(AuditParms, 0, AuditParms.Length)

                                    AuditParms(0, 0) = "@UtilityUnitCode"
                                    AuditParms(0, 1) = "UtilityUnitCode, " + drEvent2("UtilityUnitCode").ToString()
                                    AuditParms(1, 0) = "@Year"
                                    AuditParms(1, 1) = "Year, " + drEvent2("Year").ToString()
                                    AuditParms(2, 0) = "@EventNumber"
                                    AuditParms(2, 1) = "EventNumber, " + drEvent2("EventNumber").ToString()
                                    AuditParms(3, 0) = "@RevisionCard01"
                                    AuditParms(3, 1) = "RevisionCard01, " + drEvent2("RevisionCard02").ToString()
                                    AuditParms(4, 0) = "@RevisionCard02"
                                    AuditParms(4, 1) = "RevisionCard02, X"

                                    AuditParms(5, 0) = "@RevisionCard03"
                                    AuditParms(5, 1) = "RevisionCard03, X"
                                    AuditParms(6, 0) = "@RevMonthCard01"
                                    AuditParms(6, 1) = "RevMonthCard01, " + drEvent2("RevMonthCard01").ToString()
                                    AuditParms(7, 0) = "@RevMonthCard02"
                                    AuditParms(7, 1) = "RevMonthCard02, " + dtStampNow.ToString
                                    AuditParms(8, 0) = "@RevMonthCard03"
                                    AuditParms(8, 1) = "RevMonthCard03, " + dtStampNow.ToString

                                    AuditParms(9, 0) = "@LoginID"
                                    AuditParms(9, 1) = "LoginID, " + myUser
                                    AuditParms(10, 0) = "@TimeStamp"
                                    AuditParms(10, 1) = "TimeStamp, " + dtStampNow.ToString

                                    AuditParms(11, 0) = "@UnitShortName"
                                    drSetup = dsSetup.Tables(0).Rows.Find(drEvent2("UtilityUnitCode").ToString)
                                    If IsDBNull(drSetup.Item("UnitShortName")) Then
                                        AuditParms(11, 1) = "UnitShortName, UNKNOWN"
                                    Else
                                        AuditParms(11, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                                    End If

                                    intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement1, AuditParms)

                                Catch ex As Exception
                                    insertAuditStatement1 = ex.ToString
                                End Try

                            End If

                        End If

                    Next

                End If

            End If


            Try
                dsload.Tables("EventData01").AcceptChanges()
            Catch ex As Exception

            End Try

            Try
                dsload.Tables("EventData02").AcceptChanges()
            Catch ex As Exception

            End Try

            dsload.AcceptChanges()

            UpdateSetupCheckStatus(dsSetup)

            Return String.Empty

        End Function

#End Region

#Region " FormLoadPerformance(ByVal dsLoad As Performance.PerformanceDataDataTable, ByVal myUser As String) As String "

        Public Function FormLoadPerformance(ByRef dsLoad As Performance.PerformanceDataDataTable, ByVal myUser As String, ByVal strUI As String) As String

            Dim selectStatement As String
            Dim selectTimeStamp As String
            Dim selectTimeStampWeb As String
            Dim checkSetupStatement As String
            Dim insertStatement As String
            Dim updateStatement1 As String
            Dim updateStatement1Web As String
            Dim insertAuditStatement As String
            Dim updateAuditStatement1 As String
            Dim updateSetup As String
            'Dim deleteStatement As String
            Dim buildDsSetupStatement As String
            'Dim tableName As String
            Dim parms(81, 2) As String
            Dim AuditParms(14, 2) As String
            Dim intRows As Integer
            Dim intUpdateRows As Integer
            'Dim daTest As IDataAdapter
            'Dim strTest As String
            Dim drCurrent As DataRow = Nothing
            Dim dsSetup As DataSet
            Dim drSetup As DataRow
            Dim dtTemp As DataTable
            Dim douFuelQtyBurned As Double
            Dim dtStampNow As DateTime
            dtStampNow = System.DateTime.UtcNow
            Dim FuelQtyFormat As Integer
            FuelQtyFormat = 0

            Dim dtTempRev As DateTime
            Dim dtTempTS As DateTime

            If strUI Is Nothing Then
                strUI = "WINDOWS"
            End If

            If strUI.Trim = String.Empty Then
                strUI = "WINDOWS"
            End If

            selectStatement = "SELECT COUNT(*) FROM PerformanceData WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(Period = @Original_Period)"

            selectTimeStamp = "SELECT COUNT(*) FROM PerformanceData WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(Period = @Original_Period) AND " & _
                "(TimeStamp = @Original_TimeStamp) "

            selectTimeStampWeb = "SELECT COUNT(*) FROM PerformanceData WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(Period = @Original_Period)"

            updateSetup = "UPDATE Setup SET GrossDepCapacity = @GDC, NetDepCapacity = @NDC, TypUnitLoading = @TUL, VerbalDesc = @Verbal " & _
                "WHERE (UtilityUnitCode = @UtilityUnitCode)"

            checkSetupStatement = "SELECT COUNT(*) FROM Setup WHERE " & _
                "(UtilityUnitCode = @UtilityUnitCode)"

            buildDsSetupStatement = "SELECT UtilityUnitCode, CheckStatus, FuelQtyFormat, UnitShortName FROM Setup"
            dsSetup = Me.Factory.GetDataSet(buildDsSetupStatement, Nothing)

            If Factory.Provider = "OleDb" Then
                Dim keys(0) As DataColumn
                keys(0) = dsSetup.Tables(0).Columns.Item("UtilityUnitCode")
                dsSetup.Tables(0).PrimaryKey = keys
            End If

            insertStatement = "INSERT INTO PerformanceData (UtilityUnitCode, Year, Period, " & _
                "RevisionCard1, " & _
                "GrossMaxCap, GrossDepCap, GrossGen, NetMaxCap, NetDepCap, NetGen, " & _
                "TypUnitLoading, AttemptedStarts, ActualStarts, VerbalDesc, " & _
                "RevisionCard2, " & _
                "ServiceHours, RSHours, PumpingHours, SynchCondHours, " & _
                "PlannedOutageHours, ForcedOutageHours, MaintOutageHours, ExtofSchedOutages, PeriodHours, " & _
                "RevisionCard3, " & _
                "PriFuelCode, PriQtyBurned, PriAvgHeatContent, PriBtus, PriPercentAsh, PriPercentMoisture, " & _
                "PriPercentSulfur, PriPercentAlkalines, PriGrindIndexVanad, PriAshSoftTemp, " & _
                "SecFuelCode, SecQtyBurned, SecAvgHeatContent, SecBtus, SecPercentAsh, SecPercentMoisture, " & _
                "SecPercentSulfur, SecPercentAlkalines, SecGrindIndexVanad, SecAshSoftTemp, " & _
                "RevisionCard4, " & _
                "TerFuelCode, TerQtyBurned, TerAvgHeatContent, TerBtus, TerPercentAsh, TerPercentMoisture, " & _
                "TerPercentSulfur, TerPercentAlkalines, TerGrindIndexVanad, TerAshSoftTemp, " & _
                "QuaFuelCode, QuaQtyBurned, QuaAvgHeatContent, QuaBtus, QuaPercentAsh, QuaPercentMoisture, " & _
                "QuaPercentSulfur, QuaPercentAlkalines, QuaGrindIndexVanad, QuaAshSoftTemp, " & _
                "RevMonthCard1, RevMonthCard2, RevMonthCard3, RevMonthCard4, " & _
                "JOGrossMaxCap, JOGrossGen, JONetMaxCap, JONetGen, " & _
                "JOPriQtyBurned, JOSecQtyBurned, JOTerQtyBurned, JOQuaQtyBurned, " & _
                "TimeStamp, UnitShortName, InactiveHours) " & _
                "VALUES " & _
                "(@UtilityUnitCode, @Year, @Period, " & _
                "@RevisionCard1, " & _
                "@GrossMaxCap, @GrossDepCap, @GrossGen, @NetMaxCap, @NetDepCap, @NetGen, " & _
                "@TypUnitLoading, @AttemptedStarts, @ActualStarts, @VerbalDesc, " & _
                "@RevisionCard2, " & _
                "@ServiceHours, @RSHours, @PumpingHours, @SynchCondHours, " & _
                "@PlannedOutageHours, @ForcedOutageHours, @MaintOutageHours, @ExtofSchedOutages, @PeriodHours, " & _
                "@RevisionCard3, " & _
                "@PriFuelCode, @PriQtyBurned, @PriAvgHeatContent, @PriBtus, @PriPercentAsh, @PriPercentMoisture, " & _
                "@PriPercentSulfur, @PriPercentAlkalines, @PriGrindIndexVanad, @PriAshSoftTemp, " & _
                "@SecFuelCode, @SecQtyBurned, @SecAvgHeatContent, @SecBtus, @SecPercentAsh, @SecPercentMoisture, " & _
                "@SecPercentSulfur, @SecPercentAlkalines, @SecGrindIndexVanad, @SecAshSoftTemp, " & _
                "@RevisionCard4, " & _
                "@TerFuelCode, @TerQtyBurned, @TerAvgHeatContent, @TerBtus, @TerPercentAsh, @TerPercentMoisture, " & _
                "@TerPercentSulfur, @TerPercentAlkalines, @TerGrindIndexVanad, @TerAshSoftTemp, " & _
                "@QuaFuelCode, @QuaQtyBurned, @QuaAvgHeatContent, @QuaBtus, @QuaPercentAsh, @QuaPercentMoisture, " & _
                "@QuaPercentSulfur, @QuaPercentAlkalines, @QuaGrindIndexVanad, @QuaAshSoftTemp, " & _
                "@RevMonthCard1, @RevMonthCard2, @RevMonthCard3, @RevMonthCard4, " & _
                "@JOGrossMaxCap, @JOGrossGen, @JONetMaxCap, @JONetGen, " & _
                "@JOPriQtyBurned, @JOSecQtyBurned, @JOTerQtyBurned, @JOQuaQtyBurned, " & _
                "@TimeStamp, @UnitShortName, @InactiveHours)"

            updateStatement1 = "UPDATE PerformanceData SET " & _
                "RevisionCard1 = @RevisionCard1, GrossMaxCap = @GrossMaxCap, " & _
                "GrossDepCap = @GrossDepCap, GrossGen = @GrossGen, NetMaxCap = @NetMaxCap, NetDepCap = @NetDepCap, " & _
                "NetGen = @NetGen, TypUnitLoading = @TypUnitLoading, AttemptedStarts = @AttemptedStarts, " & _
                "ActualStarts = @ActualStarts, VerbalDesc = @VerbalDesc, RevMonthCard1 = @RevMonthCard1, " & _
                "" & _
                "RevisionCard2 = @RevisionCard2, " & _
                "ServiceHours = @ServiceHours, RSHours = @RSHours, PumpingHours = @PumpingHours, " & _
                "SynchCondHours = @SynchCondHours, PlannedOutageHours = @PlannedOutageHours, " & _
                "ForcedOutageHours = @ForcedOutageHours, MaintOutageHours = @MaintOutageHours, " & _
                "ExtofSchedOutages = @ExtofSchedOutages, PeriodHours = @PeriodHours, RevMonthCard2 = @RevMonthCard2, " & _
                "" & _
                "RevisionCard3 = @RevisionCard3, " & _
                "PriFuelCode = @PriFuelCode, PriQtyBurned = @PriQtyBurned, " & _
                "PriAvgHeatContent = @PriAvgHeatContent, PriBtus = @PriBtus, PriPercentAsh = @PriPercentAsh, " & _
                "PriPercentMoisture = @PriPercentMoisture, PriPercentSulfur = @PriPercentSulfur, " & _
                "PriPercentAlkalines = @PriPercentAlkalines, PriGrindIndexVanad = @PriGrindIndexVanad, " & _
                "PriAshSoftTemp = @PriAshSoftTemp, SecFuelCode = @SecFuelCode, SecQtyBurned = @SecQtyBurned, " & _
                "SecAvgHeatContent = @SecAvgHeatContent, SecBtus = @SecBtus, SecPercentAsh = @SecPercentAsh, " & _
                "SecPercentMoisture = @SecPercentMoisture, SecPercentSulfur = @SecPercentSulfur, " & _
                "SecPercentAlkalines = @SecPercentAlkalines, SecGrindIndexVanad = @SecGrindIndexVanad, " & _
                "SecAshSoftTemp = @SecAshSoftTemp, RevMonthCard3 = @RevMonthCard3, " & _
                "" & _
                "RevisionCard4 = @RevisionCard4, " & _
                "TerFuelCode = @TerFuelCode, TerQtyBurned = @TerQtyBurned, TerAvgHeatContent = @TerAvgHeatContent, TerBtus = @TerBtus, " & _
                "TerPercentAsh = @TerPercentAsh, TerPercentMoisture = @TerPercentMoisture, " & _
                "TerPercentSulfur = @TerPercentSulfur, TerPercentAlkalines = @TerPercentAlkalines, " & _
                "TerGrindIndexVanad = @TerGrindIndexVanad, TerAshSoftTemp = @TerAshSoftTemp, " & _
                "QuaFuelCode = @QuaFuelCode, QuaQtyBurned = @QuaQtyBurned, QuaAvgHeatContent = @QuaAvgHeatContent, " & _
                "QuaBtus = @QuaBtus, QuaPercentAsh = @QuaPercentAsh, QuaPercentMoisture = @QuaPercentMoisture, " & _
                "QuaPercentSulfur = @QuaPercentSulfur, QuaPercentAlkalines = @QuaPercentAlkalines, " & _
                "QuaGrindIndexVanad = @QuaGrindIndexVanad, QuaAshSoftTemp = @QuaAshSoftTemp, RevMonthCard4 = @RevMonthCard4, " & _
                "" & _
                "JOGrossMaxCap = @JOGrossMaxCap, JOGrossGen = @JOGrossGen, JONetMaxCap = @JONetMaxCap, " & _
                "JONetGen = @JONetGen, JOPriQtyBurned = @JOPriQtyBurned, JOSecQtyBurned = @JOSecQtyBurned, " & _
                "JOTerQtyBurned = @JOTerQtyBurned, JOQuaQtyBurned = @JOQuaQtyBurned, " & _
                "TimeStamp = @TimeStamp, InactiveHours = @InactiveHours " & _
                "WHERE " & _
                "(Period = @Original_Period) AND " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(TimeStamp = @Original_TimeStamp) "

            updateStatement1Web = "UPDATE PerformanceData SET " & _
                "RevisionCard1 = @RevisionCard1, GrossMaxCap = @GrossMaxCap, " & _
                "GrossDepCap = @GrossDepCap, GrossGen = @GrossGen, NetMaxCap = @NetMaxCap, NetDepCap = @NetDepCap, " & _
                "NetGen = @NetGen, TypUnitLoading = @TypUnitLoading, AttemptedStarts = @AttemptedStarts, " & _
                "ActualStarts = @ActualStarts, VerbalDesc = @VerbalDesc, RevMonthCard1 = @RevMonthCard1, " & _
                "" & _
                "RevisionCard2 = @RevisionCard2, " & _
                "ServiceHours = @ServiceHours, RSHours = @RSHours, PumpingHours = @PumpingHours, " & _
                "SynchCondHours = @SynchCondHours, PlannedOutageHours = @PlannedOutageHours, " & _
                "ForcedOutageHours = @ForcedOutageHours, MaintOutageHours = @MaintOutageHours, " & _
                "ExtofSchedOutages = @ExtofSchedOutages, PeriodHours = @PeriodHours, RevMonthCard2 = @RevMonthCard2, " & _
                "" & _
                "RevisionCard3 = @RevisionCard3, " & _
                "PriFuelCode = @PriFuelCode, PriQtyBurned = @PriQtyBurned, " & _
                "PriAvgHeatContent = @PriAvgHeatContent, PriBtus = @PriBtus, PriPercentAsh = @PriPercentAsh, " & _
                "PriPercentMoisture = @PriPercentMoisture, PriPercentSulfur = @PriPercentSulfur, " & _
                "PriPercentAlkalines = @PriPercentAlkalines, PriGrindIndexVanad = @PriGrindIndexVanad, " & _
                "PriAshSoftTemp = @PriAshSoftTemp, SecFuelCode = @SecFuelCode, SecQtyBurned = @SecQtyBurned, " & _
                "SecAvgHeatContent = @SecAvgHeatContent, SecBtus = @SecBtus, SecPercentAsh = @SecPercentAsh, " & _
                "SecPercentMoisture = @SecPercentMoisture, SecPercentSulfur = @SecPercentSulfur, " & _
                "SecPercentAlkalines = @SecPercentAlkalines, SecGrindIndexVanad = @SecGrindIndexVanad, " & _
                "SecAshSoftTemp = @SecAshSoftTemp, RevMonthCard3 = @RevMonthCard3, " & _
                "" & _
                "RevisionCard4 = @RevisionCard4, " & _
                "TerFuelCode = @TerFuelCode, TerQtyBurned = @TerQtyBurned, TerAvgHeatContent = @TerAvgHeatContent, TerBtus = @TerBtus, " & _
                "TerPercentAsh = @TerPercentAsh, TerPercentMoisture = @TerPercentMoisture, " & _
                "TerPercentSulfur = @TerPercentSulfur, TerPercentAlkalines = @TerPercentAlkalines, " & _
                "TerGrindIndexVanad = @TerGrindIndexVanad, TerAshSoftTemp = @TerAshSoftTemp, " & _
                "QuaFuelCode = @QuaFuelCode, QuaQtyBurned = @QuaQtyBurned, QuaAvgHeatContent = @QuaAvgHeatContent, " & _
                "QuaBtus = @QuaBtus, QuaPercentAsh = @QuaPercentAsh, QuaPercentMoisture = @QuaPercentMoisture, " & _
                "QuaPercentSulfur = @QuaPercentSulfur, QuaPercentAlkalines = @QuaPercentAlkalines, " & _
                "QuaGrindIndexVanad = @QuaGrindIndexVanad, QuaAshSoftTemp = @QuaAshSoftTemp, RevMonthCard4 = @RevMonthCard4, " & _
                "" & _
                "JOGrossMaxCap = @JOGrossMaxCap, JOGrossGen = @JOGrossGen, JONetMaxCap = @JONetMaxCap, " & _
                "JONetGen = @JONetGen, JOPriQtyBurned = @JOPriQtyBurned, JOSecQtyBurned = @JOSecQtyBurned, " & _
                "JOTerQtyBurned = @JOTerQtyBurned, JOQuaQtyBurned = @JOQuaQtyBurned, " & _
                "TimeStamp = @TimeStamp, InactiveHours = @InactiveHours " & _
                "WHERE " & _
                "(Period = @Original_Period) AND " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) "


            insertAuditStatement = "INSERT INTO AuditPerformanceData (UtilityUnitCode, UnitShortName, Year, Period, " & _
                "RevisionCard1, RevisionCard2, RevisionCard3, RevisionCard4, " & _
                "RevMonthCard1, RevMonthCard2, RevMonthCard3, RevMonthCard4, " & _
                "LoginID, TimeStamp) " & _
                "VALUES " & _
                "(@UtilityUnitCode, @UnitShortName, @Year, @Period, " & _
                "@RevisionCard1, @RevisionCard2, @RevisionCard3, @RevisionCard4, " & _
                "@RevMonthCard1, @RevMonthCard2, @RevMonthCard3, @RevMonthCard4, " & _
                "@LoginID, @TimeStamp)"

            updateAuditStatement1 = "UPDATE AuditPerformanceData SET " & _
                "RevisionCard1 = @RevisionCard1, RevMonthCard1 = @RevMonthCard1, " & _
                "RevisionCard2 = @RevisionCard2, RevMonthCard2 = @RevMonthCard2, " & _
                "RevisionCard3 = @RevisionCard3, RevMonthCard3 = @RevMonthCard3, " & _
                "RevisionCard4 = @RevisionCard4, RevMonthCard4 = @RevMonthCard4, " & _
                "LoginID = @LoginID, TimeStamp = @TimeStamp " & _
                "WHERE " & _
                "(Period = @Original_Period) AND " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(TimeStamp = @Original_TimeStamp) "

            ' This is a NEW row

            dtTemp = dsLoad.GetChanges(DataRowState.Added)

            If Not dtTemp Is Nothing Then

                If dtTemp.Rows.Count > 0 Then

                    For Each drCurrent In dtTemp.Rows

                        If dtTemp.Rows.Count > 0 Then

                            'drCurrent = dtTemp.Rows(0)

                            'System.Array.Clear(parms, 0, parms.Length)

                            'parms(0, 0) = "@UtilityUnitCode"
                            'parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                            ' Check to see if this unit is in Setup -- skip loading this record if not in Setup
                            drSetup = dsSetup.Tables(0).Rows.Find(drCurrent("UtilityUnitCode").ToString)

                            If drSetup Is Nothing Then
                                Return "Unit does not exist in Setup"
                            Else
                                ' The unit is in Setup

                                If Convert.IsDBNull(drSetup.Item("FuelQtyFormat")) Then
                                    drSetup.Item("FuelQtyFormat") = 0
                                End If

                                If Not IsNumeric(drSetup.Item("FuelQtyFormat")) Then
                                    drSetup.Item("FuelQtyFormat") = 0
                                End If

                                FuelQtyFormat = Convert.ToInt16(drSetup.Item("FuelQtyFormat"))
                                drSetup.Item("CheckStatus") = "X"
                                drSetup.AcceptChanges()

                            End If

                        Else
                            Return "Error code logic" & vbCrLf & "dtTemp.Rows.Count <= 0"
                        End If

                        drCurrent("TimeStamp") = dtStampNow

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                        parms(2, 0) = "@Original_Period"
                        parms(2, 1) = "Period, " + drCurrent("Period").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows <> 0 Then
                            drCurrent.RowError = "Data has already been added by another user"
                            Return "Row has been added by another user"
                        End If

                        ' *** The row does not exist so use INSERT ***

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Year"
                        parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                        parms(2, 0) = "@Period"
                        parms(2, 1) = "Period, " + drCurrent("Period").ToString()
                        parms(3, 0) = "@RevisionCard1"
                        parms(3, 1) = "RevisionCard1, " + drCurrent("RevisionCard1").ToString()

                        parms(4, 0) = "@GrossMaxCap"
                        parms(4, 1) = "GrossMaxCap, " + drCurrent("GrossMaxCap").ToString()
                        parms(5, 0) = "@GrossDepCap"
                        parms(5, 1) = "GrossDepCap, " + drCurrent("GrossDepCap").ToString()
                        parms(6, 0) = "@GrossGen"
                        parms(6, 1) = "GrossGen, " + drCurrent("GrossGen").ToString()
                        parms(7, 0) = "@NetMaxCap"
                        parms(7, 1) = "NetMaxCap, " + drCurrent("NetMaxCap").ToString()
                        parms(8, 0) = "@NetDepCap"
                        parms(8, 1) = "NetDepCap, " + drCurrent("NetDepCap").ToString()
                        parms(9, 0) = "@NetGen"
                        parms(9, 1) = "NetGen, " + drCurrent("NetGen").ToString()
                        parms(10, 0) = "@TypUnitLoading"
                        parms(10, 1) = "TypUnitLoading, " + drCurrent("TypUnitLoading").ToString()
                        parms(11, 0) = "@AttemptedStarts"
                        parms(11, 1) = "AttemptedStarts, " + drCurrent("AttemptedStarts").ToString()
                        parms(12, 0) = "@ActualStarts"
                        parms(12, 1) = "ActualStarts, " + drCurrent("ActualStarts").ToString()
                        parms(13, 0) = "@VerbalDesc"
                        parms(13, 1) = "VerbalDesc, " + drCurrent("VerbalDesc").ToString()

                        parms(14, 0) = "@RevisionCard2"
                        parms(14, 1) = "RevisionCard2, " + drCurrent("RevisionCard2").ToString()
                        parms(15, 0) = "@ServiceHours"
                        parms(15, 1) = "ServiceHours, " + drCurrent("ServiceHours").ToString()
                        parms(16, 0) = "@RSHours"
                        parms(16, 1) = "RSHours, " + drCurrent("RSHours").ToString()
                        parms(17, 0) = "@PumpingHours"
                        parms(17, 1) = "PumpingHours, " + drCurrent("PumpingHours").ToString()
                        parms(18, 0) = "@SynchCondHours"
                        parms(18, 1) = "SynchCondHours, " + drCurrent("SynchCondHours").ToString()
                        parms(19, 0) = "@PlannedOutageHours"
                        parms(19, 1) = "PlannedOutageHours, " + drCurrent("PlannedOutageHours").ToString()
                        parms(20, 0) = "@ForcedOutageHours"
                        parms(20, 1) = "ForcedOutageHours, " + drCurrent("ForcedOutageHours").ToString()
                        parms(21, 0) = "@MaintOutageHours"
                        parms(21, 1) = "MaintOutageHours, " + drCurrent("MaintOutageHours").ToString()
                        parms(22, 0) = "@ExtofSchedOutages"
                        parms(22, 1) = "ExtofSchedOutages, " + drCurrent("ExtofSchedOutages").ToString()
                        parms(23, 0) = "@PeriodHours"
                        parms(23, 1) = "PeriodHours, " + drCurrent("PeriodHours").ToString()

                        parms(24, 0) = "@RevisionCard3"
                        parms(24, 1) = "RevisionCard3, " + drCurrent("RevisionCard3").ToString()
                        parms(25, 0) = "@PriFuelCode"
                        parms(25, 1) = "PriFuelCode, " + drCurrent("PriFuelCode").ToString()

                        parms(26, 0) = "@PriQtyBurned"

                        If Convert.IsDBNull(drCurrent("PriQtyBurned")) Then
                            parms(26, 1) = "PriQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("PriQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("PriQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("PriQtyBurned").ToString)
                            End If
                            parms(26, 1) = "PriQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        parms(27, 0) = "@PriAvgHeatContent"
                        parms(27, 1) = "PriAvgHeatContent, " + drCurrent("PriAvgHeatContent").ToString()
                        parms(28, 0) = "@PriBtus"
                        parms(28, 1) = "PriBtus, " + drCurrent("PriBtus").ToString()
                        parms(29, 0) = "@PriPercentAsh"
                        parms(29, 1) = "PriPercentAsh, " + drCurrent("PriPercentAsh").ToString()
                        parms(30, 0) = "@PriPercentMoisture"
                        parms(30, 1) = "PriPercentMoisture, " + drCurrent("PriPercentMoisture").ToString()
                        parms(31, 0) = "@PriPercentSulfur"
                        parms(31, 1) = "PriPercentSulfur, " + drCurrent("PriPercentSulfur").ToString()
                        parms(32, 0) = "@PriPercentAlkalines"
                        parms(32, 1) = "PriPercentAlkalines, " + drCurrent("PriPercentAlkalines").ToString()
                        parms(33, 0) = "@PriGrindIndexVanad"
                        parms(33, 1) = "PriGrindIndexVanad, " + drCurrent("PriGrindIndexVanad").ToString()
                        parms(34, 0) = "@PriAshSoftTemp"
                        parms(34, 1) = "PriAshSoftTemp, " + drCurrent("PriAshSoftTemp").ToString()
                        parms(35, 0) = "@SecFuelCode"
                        parms(35, 1) = "SecFuelCode, " + drCurrent("SecFuelCode").ToString()
                        parms(36, 0) = "@SecQtyBurned"

                        If Convert.IsDBNull(drCurrent("SecQtyBurned")) Then
                            parms(36, 1) = "SecQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("SecQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("SecQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("SecQtyBurned").ToString)
                            End If
                            parms(36, 1) = "SecQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        parms(37, 0) = "@SecAvgHeatContent"
                        parms(37, 1) = "SecAvgHeatContent, " + drCurrent("SecAvgHeatContent").ToString()
                        parms(38, 0) = "@SecBtus"
                        parms(38, 1) = "SecBtus, " + drCurrent("SecBtus").ToString()
                        parms(39, 0) = "@SecPercentAsh"
                        parms(39, 1) = "SecPercentAsh, " + drCurrent("SecPercentAsh").ToString()
                        parms(40, 0) = "@SecPercentMoisture"
                        parms(40, 1) = "SecPercentMoisture, " + drCurrent("SecPercentMoisture").ToString()
                        parms(41, 0) = "@SecPercentSulfur"
                        parms(41, 1) = "SecPercentSulfur, " + drCurrent("SecPercentSulfur").ToString()
                        parms(42, 0) = "@SecPercentAlkalines"
                        parms(42, 1) = "SecPercentAlkalines, " + drCurrent("SecPercentAlkalines").ToString()
                        parms(43, 0) = "@SecGrindIndexVanad"
                        parms(43, 1) = "SecGrindIndexVanad, " + drCurrent("SecGrindIndexVanad").ToString()
                        parms(44, 0) = "@SecAshSoftTemp"
                        parms(44, 1) = "SecAshSoftTemp, " + drCurrent("SecAshSoftTemp").ToString()

                        parms(45, 0) = "@RevisionCard4"
                        parms(45, 1) = "RevisionCard4, " + drCurrent("RevisionCard4").ToString()
                        parms(46, 0) = "@TerFuelCode"
                        parms(46, 1) = "TerFuelCode, " + drCurrent("TerFuelCode").ToString()
                        parms(47, 0) = "@TerQtyBurned"

                        If Convert.IsDBNull(drCurrent("TerQtyBurned")) Then
                            parms(47, 1) = "TerQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("TerQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("TerQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("TerQtyBurned").ToString)
                            End If
                            parms(47, 1) = "TerQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        parms(48, 0) = "@TerAvgHeatContent"
                        parms(48, 1) = "TerAvgHeatContent, " + drCurrent("TerAvgHeatContent").ToString()
                        parms(49, 0) = "@TerBtus"
                        parms(49, 1) = "TerBtus, " + drCurrent("TerBtus").ToString()
                        parms(50, 0) = "@TerPercentAsh"
                        parms(50, 1) = "TerPercentAsh, " + drCurrent("TerPercentAsh").ToString()
                        parms(51, 0) = "@TerPercentMoisture"
                        parms(51, 1) = "TerPercentMoisture, " + drCurrent("TerPercentMoisture").ToString()
                        parms(52, 0) = "@TerPercentSulfur"
                        parms(52, 1) = "TerPercentSulfur, " + drCurrent("TerPercentSulfur").ToString()
                        parms(53, 0) = "@TerPercentAlkalines"
                        parms(53, 1) = "TerPercentAlkalines, " + drCurrent("TerPercentAlkalines").ToString()
                        parms(54, 0) = "@TerGrindIndexVanad"
                        parms(54, 1) = "TerGrindIndexVanad, " + drCurrent("TerGrindIndexVanad").ToString()
                        parms(55, 0) = "@TerAshSoftTemp"
                        parms(55, 1) = "TerAshSoftTemp, " + drCurrent("TerAshSoftTemp").ToString()
                        parms(56, 0) = "@QuaFuelCode"
                        parms(56, 1) = "QuaFuelCode, " + drCurrent("QuaFuelCode").ToString()
                        parms(57, 0) = "@QuaQtyBurned"

                        If Convert.IsDBNull(drCurrent("QuaQtyBurned")) Then
                            parms(57, 1) = "QuaQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("QuaQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("QuaQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("QuaQtyBurned").ToString)
                            End If
                            parms(57, 1) = "QuaQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        parms(58, 0) = "@QuaAvgHeatContent"
                        parms(58, 1) = "QuaAvgHeatContent, " + drCurrent("QuaAvgHeatContent").ToString()
                        parms(59, 0) = "@QuaBtus"
                        parms(59, 1) = "QuaBtus, " + drCurrent("QuaBtus").ToString()
                        parms(60, 0) = "@QuaPercentAsh"
                        parms(60, 1) = "QuaPercentAsh, " + drCurrent("QuaPercentAsh").ToString()
                        parms(61, 0) = "@QuaPercentMoisture"
                        parms(61, 1) = "QuaPercentMoisture, " + drCurrent("QuaPercentMoisture").ToString()
                        parms(62, 0) = "@QuaPercentSulfur"
                        parms(62, 1) = "QuaPercentSulfur, " + drCurrent("QuaPercentSulfur").ToString()
                        parms(63, 0) = "@QuaPercentAlkalines"
                        parms(63, 1) = "QuaPercentAlkalines, " + drCurrent("QuaPercentAlkalines").ToString()
                        parms(64, 0) = "@QuaGrindIndexVanad"
                        parms(64, 1) = "QuaGrindIndexVanad, " + drCurrent("QuaGrindIndexVanad").ToString()
                        parms(65, 0) = "@QuaAshSoftTemp"
                        parms(65, 1) = "QuaAshSoftTemp, " + drCurrent("QuaAshSoftTemp").ToString()
                        parms(66, 0) = "@RevMonthCard1"
                        parms(66, 1) = "RevMonthCard1, " + drCurrent("RevMonthCard1").ToString()
                        parms(67, 0) = "@RevMonthCard2"
                        parms(67, 1) = "RevMonthCard2, " + drCurrent("RevMonthCard2").ToString()
                        parms(68, 0) = "@RevMonthCard3"
                        parms(68, 1) = "RevMonthCard3, " + drCurrent("RevMonthCard3").ToString()
                        parms(69, 0) = "@RevMonthCard4"
                        parms(69, 1) = "RevMonthCard4, " + drCurrent("RevMonthCard4").ToString()
                        parms(70, 0) = "@JOGrossMaxCap"
                        parms(70, 1) = "JOGrossMaxCap, " + drCurrent("JOGrossMaxCap").ToString()
                        parms(71, 0) = "@JOGrossGen"
                        parms(71, 1) = "JOGrossGen, " + drCurrent("JOGrossGen").ToString()
                        parms(72, 0) = "@JONetMaxCap"
                        parms(72, 1) = "JONetMaxCap, " + drCurrent("JONetMaxCap").ToString()
                        parms(73, 0) = "@JONetGen"
                        parms(73, 1) = "JONetGen, " + drCurrent("JONetGen").ToString()
                        parms(74, 0) = "@JOPriQtyBurned"

                        If Convert.IsDBNull(drCurrent("JOPriQtyBurned")) Then
                            parms(74, 1) = "JOPriQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOPriQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOPriQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOPriQtyBurned").ToString)
                            End If
                            parms(74, 1) = "JOPriQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        parms(75, 0) = "@JOSecQtyBurned"

                        If Convert.IsDBNull(drCurrent("JOSecQtyBurned")) Then
                            parms(75, 1) = "JOSecQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOSecQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOSecQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOSecQtyBurned").ToString)
                            End If
                            parms(75, 1) = "JOSecQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        parms(76, 0) = "@JOTerQtyBurned"

                        If Convert.IsDBNull(drCurrent("JOTerQtyBurned")) Then
                            parms(76, 1) = "JOTerQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOTerQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOTerQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOTerQtyBurned").ToString)
                            End If
                            parms(76, 1) = "JOTerQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        parms(77, 0) = "@JOQuaQtyBurned"

                        If Convert.IsDBNull(drCurrent("JOQuaQtyBurned")) Then
                            parms(77, 1) = "JOQuaQtyBurned, "
                        Else
                            If FuelQtyFormat = 1 Then
                                ' Full format in tons, Mcf, bbls, etc
                                'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOQuaQtyBurned").ToString) / 1000.0
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOQuaQtyBurned").ToString)
                            Else
                                ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                                douFuelQtyBurned = Convert.ToDouble(drCurrent("JOQuaQtyBurned").ToString)
                            End If
                            parms(77, 1) = "JOQuaQtyBurned, " + douFuelQtyBurned.ToString()
                        End If

                        If IsDBNull(drCurrent("TimeStamp")) Then
                            drCurrent("TimeStamp") = dtStampNow
                        End If

                        dtTempTS = System.DateTime.Parse(drCurrent("TimeStamp").ToString)

                        If Not IsDBNull(drCurrent("RevMonthCard1")) Then
                            dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard1").ToString)
                            If dtTempRev > dtTempTS Then
                                dtTempTS = dtTempRev
                            End If
                        End If

                        If Not IsDBNull(drCurrent("RevMonthCard2")) Then
                            dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard2").ToString)
                            If dtTempRev > dtTempTS Then
                                dtTempTS = dtTempRev
                            End If
                        End If

                        If Not IsDBNull(drCurrent("RevMonthCard3")) Then
                            dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard3").ToString)
                            If dtTempRev > dtTempTS Then
                                dtTempTS = dtTempRev
                            End If
                        End If

                        If Not IsDBNull(drCurrent("RevMonthCard4")) Then
                            dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard4").ToString)
                            If dtTempRev > dtTempTS Then
                                dtTempTS = dtTempRev
                            End If
                        End If

                        drCurrent("TimeStamp") = dtTempTS

                        parms(78, 0) = "@TimeStamp"
                        parms(78, 1) = "TimeStamp, " + dtTempTS.ToString()
                        parms(79, 0) = "@UnitShortName"
                        If IsDBNull(drSetup.Item("UnitShortName")) Then
                            parms(79, 1) = "UnitShortName, UNKNOWN"
                        Else
                            parms(79, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                        End If

                        parms(80, 0) = "@InactiveHours"
                        If IsDBNull(drCurrent.Item("InactiveHours")) Then
                            parms(80, 1) = "InactiveHours, 0"
                        Else
                            parms(80, 1) = "InactiveHours, " + drCurrent("InactiveHours").ToString
                        End If

                        Try
                            ' try to update the record in the database
                            ' even with a "new" record, someone else could have already loaded this same performance record

                            intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)
                            drCurrent.AcceptChanges()

                        Catch dbEx As DBConcurrencyException

                            Me.ThrowGADSNGException("Data Concurrency Exception Occurred", dbEx)
                            Return "Data Concurrency Exception Occurred"

                        Catch Ex As System.Exception

                            Me.ThrowGADSNGException("Exception occurred in FormLoadPerformance", Ex)
                            Return "Exception Occurred in FormLoadPerformance"

                        End Try

                        System.Array.Clear(AuditParms, 0, AuditParms.Length)

                        AuditParms(0, 0) = "@UtilityUnitCode"
                        AuditParms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                        AuditParms(1, 0) = "@UnitShortName"
                        If IsDBNull(drSetup.Item("UnitShortName")) Then
                            AuditParms(1, 1) = "UnitShortName, UNKNOWN"
                        Else
                            AuditParms(1, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                        End If

                        AuditParms(2, 0) = "@Year"
                        AuditParms(2, 1) = "Year, " + drCurrent("Year").ToString()
                        AuditParms(3, 0) = "@Period"
                        AuditParms(3, 1) = "Period, " + drCurrent("Period").ToString()

                        AuditParms(4, 0) = "@RevisionCard1"
                        AuditParms(4, 1) = "RevisionCard1, " + drCurrent("RevisionCard1").ToString()
                        AuditParms(5, 0) = "@RevisionCard2"
                        AuditParms(5, 1) = "RevisionCard2, " + drCurrent("RevisionCard2").ToString()
                        AuditParms(6, 0) = "@RevisionCard3"
                        AuditParms(6, 1) = "RevisionCard3, " + drCurrent("RevisionCard3").ToString()
                        AuditParms(7, 0) = "@RevisionCard4"
                        AuditParms(7, 1) = "RevisionCard4, " + drCurrent("RevisionCard4").ToString()
                        AuditParms(8, 0) = "@RevMonthCard1"
                        AuditParms(8, 1) = "RevMonthCard1, " + drCurrent("RevMonthCard1").ToString()
                        AuditParms(9, 0) = "@RevMonthCard2"
                        AuditParms(9, 1) = "RevMonthCard2, " + drCurrent("RevMonthCard2").ToString()
                        AuditParms(10, 0) = "@RevMonthCard3"
                        AuditParms(10, 1) = "RevMonthCard3, " + drCurrent("RevMonthCard3").ToString()
                        AuditParms(11, 0) = "@RevMonthCard4"
                        AuditParms(11, 1) = "RevMonthCard4, " + drCurrent("RevMonthCard4").ToString()
                        AuditParms(12, 0) = "@LoginID"
                        AuditParms(12, 1) = "LoginID, " + myUser.Trim
                        AuditParms(13, 0) = "@TimeStamp"
                        AuditParms(13, 1) = "TimeStamp, " + drCurrent("TimeStamp").ToString()

                        Try
                            ' try to update the record in the database
                            ' even with a "new" record, someone else could have already loaded this same performance record

                            intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement, AuditParms)

                        Catch dbEx As DBConcurrencyException

                            Me.ThrowGADSNGException("Data Concurrency Exception Occurred", dbEx)
                            Return "Data Concurrency Exception Occurred"

                        Catch Ex As System.Exception

                            Me.ThrowGADSNGException("Exception occurred in FormLoadPerformance Audit", Ex)
                            Return "Exception Occurred in FormLoadPerformance"

                        End Try

                    Next

                End If

            End If

            dtTemp = dsLoad.GetChanges(DataRowState.Modified)

            If Not dtTemp Is Nothing Then

                If dtTemp.Rows.Count > 0 Then

                    drCurrent = dtTemp.Rows(0)

                    'System.Array.Clear(parms, 0, parms.Length)

                    'parms(0, 0) = "@UtilityUnitCode"
                    'parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                    ' Check to see if this unit is in Setup -- skip loading this record if not in Setup
                    drSetup = dsSetup.Tables(0).Rows.Find(drCurrent("UtilityUnitCode").ToString)

                    If drSetup Is Nothing Then
                        Return "Unit does not exist in Setup"
                    Else
                        ' The unit is in Setup
                        If Convert.IsDBNull(drSetup.Item("FuelQtyFormat")) Then
                            FuelQtyFormat = 0
                        Else

                            If Not IsNumeric(drSetup.Item("FuelQtyFormat")) Then
                                drSetup.Item("FuelQtyFormat") = 0
                            End If

                            FuelQtyFormat = Convert.ToInt16(drSetup.Item("FuelQtyFormat"))

                        End If

                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()
                    End If

                Else

                    Return "Error code logic" & vbCrLf & "dtTemp.Rows.Count <= 0"

                End If

                For Each drCurrent In dtTemp.Rows

                    drCurrent.RowError = String.Empty

                    System.Array.Clear(parms, 0, parms.Length)

                    If Factory.Provider <> "OleDb" Then

                        If strUI.ToUpper = "WINDOWS" Then
                            parms(0, 0) = "@Original_UtilityUnitCode"
                            parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(1, 0) = "@Original_Year"
                            parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(2, 0) = "@Original_Period"
                            parms(2, 1) = "Period, " + drCurrent("Period").ToString()
                            parms(3, 0) = "@Original_TimeStamp"
                            parms(3, 1) = "TimeStamp, " + drCurrent("TimeStamp", DataRowVersion.Original).ToString()
                            intRows = CInt(Factory.ExecuteScalar(selectTimeStamp, parms))
                        Else
                            ' TimeStamp on the Web servers has some problems
                            parms(0, 0) = "@Original_UtilityUnitCode"
                            parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(1, 0) = "@Original_Year"
                            parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(2, 0) = "@Original_Period"
                            parms(2, 1) = "Period, " + drCurrent("Period").ToString()

                            intRows = CInt(Factory.ExecuteScalar(selectTimeStampWeb, parms))
                        End If

                    Else

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                        parms(2, 0) = "@Original_Period"
                        parms(2, 1) = "Period, " + drCurrent("Period").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectTimeStampWeb, parms))

                    End If

                    If intRows = 0 Then

                        drCurrent.RowError = "Data has been modified by another user"
                        Return "Data has been modified by another user"

                    End If

                    'drCurrent("TimeStamp") = dtStampNow

                    ' *** The row exists so use UPDATE *** 

                    If drCurrent("GrossMaxCap", DataRowVersion.Current).ToString <> drCurrent("GrossMaxCap", DataRowVersion.Original).ToString OrElse _
                        drCurrent("GrossDepCap", DataRowVersion.Current).ToString <> drCurrent("GrossDepCap", DataRowVersion.Original).ToString OrElse _
                        drCurrent("GrossGen", DataRowVersion.Current).ToString <> drCurrent("GrossGen", DataRowVersion.Original).ToString OrElse _
                        drCurrent("NetMaxCap", DataRowVersion.Current).ToString <> drCurrent("NetMaxCap", DataRowVersion.Original).ToString OrElse _
                        drCurrent("NetDepCap", DataRowVersion.Current).ToString <> drCurrent("NetDepCap", DataRowVersion.Original).ToString OrElse _
                        drCurrent("NetGen", DataRowVersion.Current).ToString <> drCurrent("NetGen", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TypUnitLoading", DataRowVersion.Current).ToString <> drCurrent("TypUnitLoading", DataRowVersion.Original).ToString OrElse _
                        drCurrent("AttemptedStarts", DataRowVersion.Current).ToString <> drCurrent("AttemptedStarts", DataRowVersion.Original).ToString OrElse _
                        drCurrent("ActualStarts", DataRowVersion.Current).ToString <> drCurrent("ActualStarts", DataRowVersion.Original).ToString OrElse _
                        drCurrent("VerbalDesc", DataRowVersion.Current).ToString <> drCurrent("VerbalDesc", DataRowVersion.Original).ToString Then

                        If drCurrent("RevisionCard1").ToString.ToUpper <> "X" Then

                            If Not (DateTime.Parse(drCurrent("RevMonthCard1", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                                DateTime.Parse(drCurrent("RevMonthCard1", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                                Dim intRevNo As Integer

                                ' if it is being revised in the same month as it was last modified, there is no need to increase
                                ' the revision number; however, if this is a different month then up the revision code

                                If Not IsNumeric(drCurrent("RevisionCard1")) Then
                                    drCurrent("RevisionCard1") = "0"
                                End If

                                intRevNo = Convert.ToInt16(drCurrent("RevisionCard1").ToString)
                                intRevNo += 1

                                If intRevNo > 9 Then
                                    intRevNo = 9
                                End If

                                drCurrent("RevisionCard1") = intRevNo.ToString()

                            End If

                            drCurrent("RevMonthCard1") = dtStampNow

                        End If

                    End If

                    If IsDBNull(drCurrent("InactiveHours")) Then
                        drCurrent("InactiveHours") = 0
                    End If

                    If drCurrent("ServiceHours", DataRowVersion.Current).ToString <> drCurrent("ServiceHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("RSHours", DataRowVersion.Current).ToString <> drCurrent("RSHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("PumpingHours", DataRowVersion.Current).ToString <> drCurrent("PumpingHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("SynchCondHours", DataRowVersion.Current).ToString <> drCurrent("SynchCondHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("PlannedOutageHours", DataRowVersion.Current).ToString <> drCurrent("PlannedOutageHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("ForcedOutageHours", DataRowVersion.Current).ToString <> drCurrent("ForcedOutageHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("MaintOutageHours", DataRowVersion.Current).ToString <> drCurrent("MaintOutageHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("ExtofSchedOutages", DataRowVersion.Current).ToString <> drCurrent("ExtofSchedOutages", DataRowVersion.Original).ToString OrElse _
                        drCurrent("PeriodHours", DataRowVersion.Current).ToString <> drCurrent("PeriodHours", DataRowVersion.Original).ToString OrElse _
                        drCurrent("InactiveHours", DataRowVersion.Current).ToString <> drCurrent("InactiveHours", DataRowVersion.Original).ToString Then

                        If drCurrent("RevisionCard2").ToString.ToUpper <> "X" Then

                            If Not (DateTime.Parse(drCurrent("RevMonthCard2", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                                DateTime.Parse(drCurrent("RevMonthCard2", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                                Dim intRevNo As Integer

                                ' if it is being revised in the same month as it was last modified, there is no need to increase
                                ' the revision number; however, if this is a different month then up the revision code

                                If Not IsNumeric(drCurrent("RevisionCard2")) Then
                                    drCurrent("RevisionCard2") = "0"
                                End If

                                intRevNo = Convert.ToInt16(drCurrent("RevisionCard2").ToString)
                                intRevNo += 1

                                If intRevNo > 9 Then
                                    intRevNo = 9
                                End If

                                drCurrent("RevisionCard2") = intRevNo.ToString()

                            End If

                            drCurrent("RevMonthCard2") = dtStampNow

                        End If

                    End If


                    If drCurrent("PriFuelCode", DataRowVersion.Current).ToString <> drCurrent("PriFuelCode", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriQtyBurned", DataRowVersion.Current).ToString <> drCurrent("PriQtyBurned", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriAvgHeatContent", DataRowVersion.Current).ToString <> drCurrent("PriAvgHeatContent", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriBtus", DataRowVersion.Current).ToString <> drCurrent("PriBtus", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriPercentAsh", DataRowVersion.Current).ToString <> drCurrent("PriPercentAsh", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriPercentMoisture", DataRowVersion.Current).ToString <> drCurrent("PriPercentMoisture", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriPercentSulfur", DataRowVersion.Current).ToString <> drCurrent("PriPercentSulfur", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriPercentAlkalines", DataRowVersion.Current).ToString <> drCurrent("PriPercentAlkalines", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriGrindIndexVanad", DataRowVersion.Current).ToString <> drCurrent("PriGrindIndexVanad", DataRowVersion.Original).ToString OrElse _
                            drCurrent("PriAshSoftTemp", DataRowVersion.Current).ToString <> drCurrent("PriAshSoftTemp", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecFuelCode", DataRowVersion.Current).ToString <> drCurrent("SecFuelCode", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecQtyBurned", DataRowVersion.Current).ToString <> drCurrent("SecQtyBurned", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecAvgHeatContent", DataRowVersion.Current).ToString <> drCurrent("SecAvgHeatContent", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecBtus", DataRowVersion.Current).ToString <> drCurrent("SecBtus", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecPercentAsh", DataRowVersion.Current).ToString <> drCurrent("SecPercentAsh", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecPercentMoisture", DataRowVersion.Current).ToString <> drCurrent("SecPercentMoisture", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecPercentSulfur", DataRowVersion.Current).ToString <> drCurrent("SecPercentSulfur", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecPercentAlkalines", DataRowVersion.Current).ToString <> drCurrent("SecPercentAlkalines", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecGrindIndexVanad", DataRowVersion.Current).ToString <> drCurrent("SecGrindIndexVanad", DataRowVersion.Original).ToString OrElse _
                            drCurrent("SecAshSoftTemp", DataRowVersion.Current).ToString <> drCurrent("SecAshSoftTemp", DataRowVersion.Original).ToString Then

                        If drCurrent("RevisionCard3").ToString.ToUpper <> "X" Then

                            If Not (DateTime.Parse(drCurrent("RevMonthCard3", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                                DateTime.Parse(drCurrent("RevMonthCard3", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                                Dim intRevNo As Integer

                                ' if it is being revised in the same month as it was last modified, there is no need to increase
                                ' the revision number; however, if this is a different month then up the revision code

                                If Not IsNumeric(drCurrent("RevisionCard3")) Then
                                    drCurrent("RevisionCard3") = "0"
                                End If

                                intRevNo = Convert.ToInt16(drCurrent("RevisionCard3").ToString)
                                intRevNo += 1

                                If intRevNo > 9 Then
                                    intRevNo = 9
                                End If

                                drCurrent("RevisionCard3") = intRevNo.ToString()

                            End If

                            drCurrent("RevMonthCard3") = dtStampNow

                        End If

                    End If


                    If drCurrent("TerFuelCode", DataRowVersion.Current).ToString <> drCurrent("TerFuelCode", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerQtyBurned", DataRowVersion.Current).ToString <> drCurrent("TerQtyBurned", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerAvgHeatContent", DataRowVersion.Current).ToString <> drCurrent("TerAvgHeatContent", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerBtus", DataRowVersion.Current).ToString <> drCurrent("TerBtus", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerPercentAsh", DataRowVersion.Current).ToString <> drCurrent("TerPercentAsh", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerPercentMoisture", DataRowVersion.Current).ToString <> drCurrent("TerPercentMoisture", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerPercentSulfur", DataRowVersion.Current).ToString <> drCurrent("TerPercentSulfur", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerPercentAlkalines", DataRowVersion.Current).ToString <> drCurrent("TerPercentAlkalines", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerGrindIndexVanad", DataRowVersion.Current).ToString <> drCurrent("TerGrindIndexVanad", DataRowVersion.Original).ToString OrElse _
                        drCurrent("TerAshSoftTemp", DataRowVersion.Current).ToString <> drCurrent("TerAshSoftTemp", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaFuelCode", DataRowVersion.Current).ToString <> drCurrent("QuaFuelCode", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaQtyBurned", DataRowVersion.Current).ToString <> drCurrent("QuaQtyBurned", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaAvgHeatContent", DataRowVersion.Current).ToString <> drCurrent("QuaAvgHeatContent", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaBtus", DataRowVersion.Current).ToString <> drCurrent("QuaBtus", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaPercentAsh", DataRowVersion.Current).ToString <> drCurrent("QuaPercentAsh", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaPercentMoisture", DataRowVersion.Current).ToString <> drCurrent("QuaPercentMoisture", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaPercentSulfur", DataRowVersion.Current).ToString <> drCurrent("QuaPercentSulfur", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaPercentAlkalines", DataRowVersion.Current).ToString <> drCurrent("QuaPercentAlkalines", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaGrindIndexVanad", DataRowVersion.Current).ToString <> drCurrent("QuaGrindIndexVanad", DataRowVersion.Original).ToString OrElse _
                        drCurrent("QuaAshSoftTemp", DataRowVersion.Current).ToString <> drCurrent("QuaAshSoftTemp", DataRowVersion.Original).ToString Then

                        If drCurrent("RevisionCard4").ToString.ToUpper <> "X" Then

                            If Not (DateTime.Parse(drCurrent("RevMonthCard4", DataRowVersion.Original).ToString).Month = dtStampNow.Month And _
                                DateTime.Parse(drCurrent("RevMonthCard4", DataRowVersion.Original).ToString).Year = dtStampNow.Year) Then

                                Dim intRevNo As Integer

                                ' if it is being revised in the same month as it was last modified, there is no need to increase
                                ' the revision number; however, if this is a different month then up the revision code

                                If Not IsNumeric(drCurrent("RevisionCard4")) Then
                                    drCurrent("RevisionCard4") = "0"
                                End If

                                intRevNo = Convert.ToInt16(drCurrent("RevisionCard4").ToString)
                                intRevNo += 1

                                If intRevNo > 9 Then
                                    intRevNo = 9
                                End If

                                drCurrent("RevisionCard4") = intRevNo.ToString()

                            End If

                            drCurrent("RevMonthCard4") = dtStampNow

                        End If

                    End If


                    ' Loading up the Performance 01 card

                    System.Array.Clear(parms, 0, parms.Length)

                    parms(0, 0) = "@RevisionCard1"
                    parms(0, 1) = "RevisionCard1, " + drCurrent("RevisionCard1").ToString()
                    parms(1, 0) = "@GrossMaxCap"
                    parms(1, 1) = "GrossMaxCap, " + drCurrent("GrossMaxCap").ToString()
                    parms(2, 0) = "@GrossDepCap"
                    parms(2, 1) = "GrossDepCap, " + drCurrent("GrossDepCap").ToString()
                    parms(3, 0) = "@GrossGen"
                    parms(3, 1) = "GrossGen, " + drCurrent("GrossGen").ToString()
                    parms(4, 0) = "@NetMaxCap"
                    parms(4, 1) = "NetMaxCap, " + drCurrent("NetMaxCap").ToString()
                    parms(5, 0) = "@NetDepCap"
                    parms(5, 1) = "NetDepCap, " + drCurrent("NetDepCap").ToString()
                    parms(6, 0) = "@NetGen"
                    parms(6, 1) = "NetGen, " + drCurrent("NetGen").ToString()
                    parms(7, 0) = "@TypUnitLoading"
                    parms(7, 1) = "TypUnitLoading, " + drCurrent("TypUnitLoading").ToString()
                    parms(8, 0) = "@AttemptedStarts"
                    parms(8, 1) = "AttemptedStarts, " + drCurrent("AttemptedStarts").ToString()
                    parms(9, 0) = "@ActualStarts"
                    parms(9, 1) = "ActualStarts, " + drCurrent("ActualStarts").ToString()
                    parms(10, 0) = "@VerbalDesc"
                    parms(10, 1) = "VerbalDesc, " + drCurrent("VerbalDesc").ToString()
                    parms(11, 0) = "@RevMonthCard1"
                    parms(11, 1) = "RevMonthCard1, " + drCurrent("RevMonthCard1").ToString()

                    ' Loading up the Performance 02 card

                    parms(12, 0) = "@RevisionCard2"
                    parms(12, 1) = "RevisionCard2, " + drCurrent("RevisionCard2").ToString()
                    parms(13, 0) = "@ServiceHours"
                    parms(13, 1) = "ServiceHours, " + drCurrent("ServiceHours").ToString()
                    parms(14, 0) = "@RSHours"
                    parms(14, 1) = "RSHours, " + drCurrent("RSHours").ToString()
                    parms(15, 0) = "@PumpingHours"
                    parms(15, 1) = "PumpingHours, " + drCurrent("PumpingHours").ToString()
                    parms(16, 0) = "@SynchCondHours"
                    parms(16, 1) = "SynchCondHours, " + drCurrent("SynchCondHours").ToString()
                    parms(17, 0) = "@PlannedOutageHours"
                    parms(17, 1) = "PlannedOutageHours, " + drCurrent("PlannedOutageHours").ToString()
                    parms(18, 0) = "@ForcedOutageHours"
                    parms(18, 1) = "ForcedOutageHours, " + drCurrent("ForcedOutageHours").ToString()
                    parms(19, 0) = "@MaintOutageHours"
                    parms(19, 1) = "MaintOutageHours, " + drCurrent("MaintOutageHours").ToString()
                    parms(20, 0) = "@ExtofSchedOutages"
                    parms(20, 1) = "ExtofSchedOutages, " + drCurrent("ExtofSchedOutages").ToString()
                    parms(21, 0) = "@PeriodHours"
                    parms(21, 1) = "PeriodHours, " + drCurrent("PeriodHours").ToString()
                    parms(22, 0) = "@RevMonthCard2"
                    parms(22, 1) = "RevMonthCard2, " + drCurrent("RevMonthCard2").ToString()

                    ' Loading up the Performance 03 card

                    parms(23, 0) = "@RevisionCard3"
                    parms(23, 1) = "RevisionCard3, " + drCurrent("RevisionCard3").ToString()
                    parms(24, 0) = "@PriFuelCode"
                    parms(24, 1) = "PriFuelCode, " + drCurrent("PriFuelCode").ToString()
                    parms(25, 0) = "@PriQtyBurned"

                    If Convert.IsDBNull(drCurrent("PriQtyBurned")) Then
                        parms(25, 1) = "PriQtyBurned, "
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("PriQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("PriQtyBurned").ToString)
                        Else
                            'NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("PriQtyBurned").ToString)
                        End If
                        parms(25, 1) = "PriQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    parms(26, 0) = "@PriAvgHeatContent"
                    parms(26, 1) = "PriAvgHeatContent, " + drCurrent("PriAvgHeatContent").ToString()
                    parms(27, 0) = "@PriBtus"
                    parms(27, 1) = "PriBtus, " + drCurrent("PriBtus").ToString()
                    parms(28, 0) = "@PriPercentAsh"
                    parms(28, 1) = "PriPercentAsh, " + drCurrent("PriPercentAsh").ToString()
                    parms(29, 0) = "@PriPercentMoisture"
                    parms(29, 1) = "PriPercentMoisture, " + drCurrent("PriPercentMoisture").ToString()
                    parms(30, 0) = "@PriPercentSulfur"
                    parms(30, 1) = "PriPercentSulfur, " + drCurrent("PriPercentSulfur").ToString()
                    parms(31, 0) = "@PriPercentAlkalines"
                    parms(31, 1) = "PriPercentAlkalines, " + drCurrent("PriPercentAlkalines").ToString()
                    parms(32, 0) = "@PriGrindIndexVanad"
                    parms(32, 1) = "PriGrindIndexVanad, " + drCurrent("PriGrindIndexVanad").ToString()
                    parms(33, 0) = "@PriAshSoftTemp"
                    parms(33, 1) = "PriAshSoftTemp, " + drCurrent("PriAshSoftTemp").ToString()
                    parms(34, 0) = "@SecFuelCode"
                    parms(34, 1) = "SecFuelCode, " + drCurrent("SecFuelCode").ToString()
                    parms(35, 0) = "@SecQtyBurned"

                    If Convert.IsDBNull(drCurrent("SecQtyBurned")) Then
                        parms(35, 1) = "SecQtyBurned, " + douFuelQtyBurned.ToString()
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("SecQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("SecQtyBurned").ToString)
                        Else
                            ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("SecQtyBurned").ToString)
                        End If
                        parms(35, 1) = "SecQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    parms(36, 0) = "@SecAvgHeatContent"
                    parms(36, 1) = "SecAvgHeatContent, " + drCurrent("SecAvgHeatContent").ToString()
                    parms(37, 0) = "@SecBtus"
                    parms(37, 1) = "SecBtus, " + drCurrent("SecBtus").ToString()
                    parms(38, 0) = "@SecPercentAsh"
                    parms(38, 1) = "SecPercentAsh, " + drCurrent("SecPercentAsh").ToString()
                    parms(39, 0) = "@SecPercentMoisture"
                    parms(39, 1) = "SecPercentMoisture, " + drCurrent("SecPercentMoisture").ToString()
                    parms(40, 0) = "@SecPercentSulfur"
                    parms(40, 1) = "SecPercentSulfur, " + drCurrent("SecPercentSulfur").ToString()
                    parms(41, 0) = "@SecPercentAlkalines"
                    parms(41, 1) = "SecPercentAlkalines, " + drCurrent("SecPercentAlkalines").ToString()
                    parms(42, 0) = "@SecGrindIndexVanad"
                    parms(42, 1) = "SecGrindIndexVanad, " + drCurrent("SecGrindIndexVanad").ToString()
                    parms(43, 0) = "@SecAshSoftTemp"
                    parms(43, 1) = "SecAshSoftTemp, " + drCurrent("SecAshSoftTemp").ToString()
                    parms(44, 0) = "@RevMonthCard3"
                    parms(44, 1) = "RevMonthCard3, " + drCurrent("RevMonthCard3").ToString()

                    ' Loading up the Performance 04 card

                    parms(45, 0) = "@RevisionCard4"
                    parms(45, 1) = "RevisionCard4, " + drCurrent("RevisionCard4").ToString()
                    parms(46, 0) = "@TerFuelCode"
                    parms(46, 1) = "TerFuelCode, " + drCurrent("TerFuelCode").ToString()
                    parms(47, 0) = "@TerQtyBurned"

                    If Convert.IsDBNull(drCurrent("TerQtyBurned")) Then
                        parms(47, 1) = "TerQtyBurned, "
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("TerQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("TerQtyBurned").ToString)
                        Else
                            ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("TerQtyBurned").ToString)
                        End If
                        parms(47, 1) = "TerQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    parms(48, 0) = "@TerAvgHeatContent"
                    parms(48, 1) = "TerAvgHeatContent, " + drCurrent("TerAvgHeatContent").ToString()
                    parms(49, 0) = "@TerBtus"
                    parms(49, 1) = "TerBtus, " + drCurrent("TerBtus").ToString()
                    parms(50, 0) = "@TerPercentAsh"
                    parms(50, 1) = "TerPercentAsh, " + drCurrent("TerPercentAsh").ToString()
                    parms(51, 0) = "@TerPercentMoisture"
                    parms(51, 1) = "TerPercentMoisture, " + drCurrent("TerPercentMoisture").ToString()
                    parms(52, 0) = "@TerPercentSulfur"
                    parms(52, 1) = "TerPercentSulfur, " + drCurrent("TerPercentSulfur").ToString()
                    parms(53, 0) = "@TerPercentAlkalines"
                    parms(53, 1) = "TerPercentAlkalines, " + drCurrent("TerPercentAlkalines").ToString()
                    parms(54, 0) = "@TerGrindIndexVanad"
                    parms(54, 1) = "TerGrindIndexVanad, " + drCurrent("TerGrindIndexVanad").ToString()
                    parms(55, 0) = "@TerAshSoftTemp"
                    parms(55, 1) = "TerAshSoftTemp, " + drCurrent("TerAshSoftTemp").ToString()
                    parms(56, 0) = "@QuaFuelCode"
                    parms(56, 1) = "QuaFuelCode, " + drCurrent("QuaFuelCode").ToString()
                    parms(57, 0) = "@QuaQtyBurned"

                    If Convert.IsDBNull(drCurrent("QuaQtyBurned")) Then
                        parms(57, 1) = "QuaQtyBurned, "
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("QuaQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("QuaQtyBurned").ToString)
                        Else
                            ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("QuaQtyBurned").ToString)
                        End If
                        parms(57, 1) = "QuaQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    parms(58, 0) = "@QuaAvgHeatContent"
                    parms(58, 1) = "QuaAvgHeatContent, " + drCurrent("QuaAvgHeatContent").ToString()
                    parms(59, 0) = "@QuaBtus"
                    parms(59, 1) = "QuaBtus, " + drCurrent("QuaBtus").ToString()
                    parms(60, 0) = "@QuaPercentAsh"
                    parms(60, 1) = "QuaPercentAsh, " + drCurrent("QuaPercentAsh").ToString()
                    parms(61, 0) = "@QuaPercentMoisture"
                    parms(61, 1) = "QuaPercentMoisture, " + drCurrent("QuaPercentMoisture").ToString()
                    parms(62, 0) = "@QuaPercentSulfur"
                    parms(62, 1) = "QuaPercentSulfur, " + drCurrent("QuaPercentSulfur").ToString()
                    parms(63, 0) = "@QuaPercentAlkalines"
                    parms(63, 1) = "QuaPercentAlkalines, " + drCurrent("QuaPercentAlkalines").ToString()
                    parms(64, 0) = "@QuaGrindIndexVanad"
                    parms(64, 1) = "QuaGrindIndexVanad, " + drCurrent("QuaGrindIndexVanad").ToString()
                    parms(65, 0) = "@QuaAshSoftTemp"
                    parms(65, 1) = "QuaAshSoftTemp, " + drCurrent("QuaAshSoftTemp").ToString()
                    parms(66, 0) = "@RevMonthCard4"
                    parms(66, 1) = "RevMonthCard4, " + drCurrent("RevMonthCard4").ToString()

                    parms(67, 0) = "@JOGrossMaxCap"
                    parms(67, 1) = "JOGrossMaxCap, " + drCurrent("JOGrossMaxCap").ToString()
                    parms(68, 0) = "@JOGrossGen"
                    parms(68, 1) = "JOGrossGen, " + drCurrent("JOGrossGen").ToString()
                    parms(69, 0) = "@JONetMaxCap"
                    parms(69, 1) = "JONetMaxCap, " + drCurrent("JONetMaxCap").ToString()
                    parms(70, 0) = "@JONetGen"
                    parms(70, 1) = "JONetGen, " + drCurrent("JONetGen").ToString()
                    parms(71, 0) = "@JOPriQtyBurned"

                    If Convert.IsDBNull(drCurrent("JOPriQtyBurned")) Then
                        parms(71, 1) = "JOPriQtyBurned, "
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOPriQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOPriQtyBurned").ToString)
                        Else
                            ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOPriQtyBurned").ToString)
                        End If
                        parms(71, 1) = "JOPriQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    parms(72, 0) = "@JOSecQtyBurned"

                    If Convert.IsDBNull(drCurrent("JOSecQtyBurned")) Then
                        parms(72, 1) = "JOSecQtyBurned, "
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOSecQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOSecQtyBurned").ToString)
                        Else
                            ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOSecQtyBurned").ToString)
                        End If
                        parms(72, 1) = "JOSecQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    parms(73, 0) = "@JOTerQtyBurned"

                    If Convert.IsDBNull(drCurrent("JOTerQtyBurned")) Then
                        parms(73, 1) = "JOTerQtyBurned, "
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOTerQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOTerQtyBurned").ToString)
                        Else
                            ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOTerQtyBurned").ToString)
                        End If
                        parms(73, 1) = "JOTerQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    parms(74, 0) = "@JOQuaQtyBurned"

                    If Convert.IsDBNull(drCurrent("JOQuaQtyBurned")) Then
                        parms(74, 1) = "JOQuaQtyBurned, "
                    Else
                        If FuelQtyFormat = 1 Then
                            ' Full format in tons, Mcf, bbls, etc
                            'douFuelQtyBurned = Convert.ToDouble(drCurrent("JOQuaQtyBurned").ToString) / 1000.0
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOQuaQtyBurned").ToString)
                        Else
                            ' NERC format in ktons, MMcf, kbbls, etc. with 2 decimal places
                            douFuelQtyBurned = Convert.ToDouble(drCurrent("JOQuaQtyBurned").ToString)
                        End If
                        parms(74, 1) = "JOQuaQtyBurned, " + douFuelQtyBurned.ToString()
                    End If

                    If IsDBNull(drCurrent("TimeStamp")) Then
                        drCurrent("TimeStamp") = dtStampNow
                    End If

                    dtTempTS = System.DateTime.Parse(drCurrent("TimeStamp", DataRowVersion.Current).ToString)

                    If Not IsDBNull(drCurrent("RevMonthCard1")) Then
                        dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard1").ToString)
                        If dtTempRev > dtTempTS Then
                            dtTempTS = dtTempRev
                        End If
                    End If

                    If Not IsDBNull(drCurrent("RevMonthCard2")) Then
                        dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard2").ToString)
                        If dtTempRev > dtTempTS Then
                            dtTempTS = dtTempRev
                        End If
                    End If

                    If Not IsDBNull(drCurrent("RevMonthCard3")) Then
                        dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard3").ToString)
                        If dtTempRev > dtTempTS Then
                            dtTempTS = dtTempRev
                        End If
                    End If

                    If Not IsDBNull(drCurrent("RevMonthCard4")) Then
                        dtTempRev = System.DateTime.Parse(drCurrent("RevMonthCard4").ToString)
                        If dtTempRev > dtTempTS Then
                            dtTempTS = dtTempRev
                        End If
                    End If

                    drCurrent("TimeStamp") = dtTempTS

                    parms(75, 0) = "@TimeStamp"
                    parms(75, 1) = "TimeStamp, " + dtTempTS.ToString()

                    parms(76, 0) = "@InactiveHours"
                    parms(76, 1) = "InactiveHours, " + drCurrent("InactiveHours").ToString

                    parms(77, 0) = "@Original_Period"
                    parms(77, 1) = "Period, " + drCurrent("Period").ToString()
                    parms(78, 0) = "@Original_UtilityUnitCode"
                    parms(78, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                    parms(79, 0) = "@Original_Year"
                    parms(79, 1) = "Year, " + drCurrent("Year").ToString()

                    If strUI.ToUpper = "WINDOWS" Then

                        ' TimeStamp on the Web servers has some problems

                        parms(80, 0) = "@Original_TimeStamp"
                        parms(80, 1) = "TimeStamp, " + drCurrent("TimeStamp", DataRowVersion.Original).ToString()

                        intUpdateRows = Factory.ExecuteNonQuery(updateStatement1, parms)

                    Else

                        intUpdateRows = Factory.ExecuteNonQuery(updateStatement1Web, parms)

                    End If

                    Try

                        drCurrent.AcceptChanges()

                    Catch dbEx As DBConcurrencyException

                        Me.ThrowGADSNGException("Data Concurrency Exception Occurred", dbEx)
                        Return "Data Concurrency Exception Occurred"

                    Catch Ex As System.Exception

                        Me.ThrowGADSNGException("Exception occurred in FormLoadPerformance", Ex)
                        Return "Exception in FormLoadPerformance"

                    End Try

                    System.Array.Clear(AuditParms, 0, AuditParms.Length)

                    AuditParms(0, 0) = "@UtilityUnitCode"
                    AuditParms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                    AuditParms(1, 0) = "@UnitShortName"
                    If IsDBNull(drSetup.Item("UnitShortName")) Then
                        AuditParms(1, 1) = "UnitShortName, UNKNOWN"
                    Else
                        AuditParms(1, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                    End If

                    AuditParms(2, 0) = "@Year"
                    AuditParms(2, 1) = "Year, " + drCurrent("Year").ToString()
                    AuditParms(3, 0) = "@Period"
                    AuditParms(3, 1) = "Period, " + drCurrent("Period").ToString()

                    AuditParms(4, 0) = "@RevisionCard1"
                    AuditParms(4, 1) = "RevisionCard1, " + drCurrent("RevisionCard1").ToString()
                    AuditParms(5, 0) = "@RevisionCard2"
                    AuditParms(5, 1) = "RevisionCard2, " + drCurrent("RevisionCard2").ToString()
                    AuditParms(6, 0) = "@RevisionCard3"
                    AuditParms(6, 1) = "RevisionCard3, " + drCurrent("RevisionCard3").ToString()
                    AuditParms(7, 0) = "@RevisionCard4"
                    AuditParms(7, 1) = "RevisionCard4, " + drCurrent("RevisionCard4").ToString()
                    AuditParms(8, 0) = "@RevMonthCard1"
                    AuditParms(8, 1) = "RevMonthCard1, " + drCurrent("RevMonthCard1").ToString()
                    AuditParms(9, 0) = "@RevMonthCard2"
                    AuditParms(9, 1) = "RevMonthCard2, " + drCurrent("RevMonthCard2").ToString()
                    AuditParms(10, 0) = "@RevMonthCard3"
                    AuditParms(10, 1) = "RevMonthCard3, " + drCurrent("RevMonthCard3").ToString()
                    AuditParms(11, 0) = "@RevMonthCard4"
                    AuditParms(11, 1) = "RevMonthCard4, " + drCurrent("RevMonthCard4").ToString()
                    AuditParms(12, 0) = "@LoginID"
                    AuditParms(12, 1) = "LoginID, " + myUser.Trim
                    AuditParms(13, 0) = "@TimeStamp"
                    AuditParms(13, 1) = "TimeStamp, " + drCurrent("TimeStamp").ToString()

                    Try

                        intUpdateRows = Factory.ExecuteNonQuery(insertAuditStatement, AuditParms)

                    Catch dbEx As DBConcurrencyException

                        Me.ThrowGADSNGException("Data Concurrency Exception Occurred", dbEx)
                        Return "Data Concurrency Exception Occurred"

                    Catch Ex As System.Exception

                        Me.ThrowGADSNGException("Exception occurred in FormLoadPerformance Audit", Ex)
                        Return "Exception Occurred in FormLoadPerformance"

                    End Try

                Next

            End If


            'updateSetup = "UPDATE Setup SET GrossDepCapacity = @GDC, NetDepCapacity = @NDC, TypUnitLoading = @TUL, VerbalDesc = @Verbal " & _
            '                 "WHERE (UtilityUnitCode = @UtilityUnitCode)"

            System.Array.Clear(parms, 0, parms.Length)

            parms(0, 0) = "@GDC"
            parms(0, 1) = "GrossDepCapacity, " + drCurrent("GrossDepCap").ToString()

            parms(1, 0) = "@NDC"
            parms(1, 1) = "NetDepCapacity, " + drCurrent("NetDepCap").ToString()

            parms(2, 0) = "@TUL"
            parms(2, 1) = "TypUnitLoading, " + drCurrent("TypUnitLoading").ToString()

            parms(3, 0) = "@Verbal"
            parms(3, 1) = "VerbalDesc, " + drCurrent("VerbalDesc").ToString()

            parms(4, 0) = "@UtilityUnitCode"
            parms(4, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

            Try

                intUpdateRows = Factory.ExecuteNonQuery(updateSetup, parms)

                intUpdateRows = Factory.ExecuteNonQuery("UPDATE PerformanceData SET InactiveHours = 0 WHERE (InactiveHours IS NULL)", Nothing)

            Catch dbEx As DBConcurrencyException

                Me.ThrowGADSNGException("Data Concurrency Exception Occurred", dbEx)
                Return "Data Concurrency Exception Occurred"

            Catch Ex As System.Exception

                Me.ThrowGADSNGException("Exception occurred in FormLoadPerformance", Ex)
                Return "Exception in FormLoadPerformance"

            End Try

            dsLoad.AcceptChanges()

            UpdateSetupCheckStatus(dsSetup)

            Return ""

        End Function

#End Region

#Region " FormLoadCustPerformance(ByVal dsLoad As Performance.PerformanceDataDataTable, ByVal myUser As String) As String "

        Public Function FormLoadCustPerformance(ByRef dsLoad As CustomPerfData.CustomPerfDataDataTable) As String

            Dim selectStatement As String

            Dim insertStatement As String
            Dim updateStatement1 As String

            'Dim tableName As String
            Dim parms(18, 2) As String

            Dim intRows As Integer
            Dim intUpdateRows As Integer
            'Dim daTest As IDataAdapter
            'Dim strTest As String
            'Dim drCurrent As DataRow = Nothing
            Dim drCurrent As CustomPerfData.CustomPerfDataRow = Nothing
            Dim dtCurrent As CustomPerfData.CustomPerfDataDataTable = New CustomPerfData.CustomPerfDataDataTable()

            selectStatement = "SELECT COUNT(*) FROM CustomPerfData WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(Period = @Original_Period)"

            ' UtilityUnitCode, UnitShortName, Year, Period, Field01, Field02, Field03, Field04, Field05, Field06, Field07, Field08, Field09, Field10

            insertStatement = "INSERT INTO CustomPerfData (UtilityUnitCode, UnitShortName, Year, Period, PerfCalcDate, Field01, Field02, Field03, Field04, Field05, Field06, Field07, Field08, Field09, Field10) " & _
                "VALUES " & _
                "(@UtilityUnitCode, @UnitShortName, @Year, @Period, @PerfCalcDate, @Field01, @Field02, @Field03, @Field04, @Field05, @Field06, @Field07, @Field08, @Field09, @Field10)"

            updateStatement1 = "UPDATE CustomPerfData SET " & _
                "UtilityUnitCode = @UtilityUnitCode, UnitShortName = @UnitShortName, Year = @Year, Period = @Period, PerfCalcDate = @PerfCalcDate, " & _
                "Field01 = @Field01, Field02 = @Field02, Field03 = @Field03, Field04 = @Field04, Field05 = @Field05,  " & _
                "Field06 = @Field06, Field07 = @Field07, Field08 = @Field08, Field09 = @Field09, Field10 = @Field10" & _
                " " & _
                "WHERE " & _
                "(Period = @Original_Period) AND " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) "

            ' This is a NEW row

            Dim dtTemp As DataTable = dsLoad.GetChanges(DataRowState.Added)

            If Not dtTemp Is Nothing Then

                If dtTemp.Rows.Count > 0 Then

                    dtCurrent.Clear()
                    dtCurrent.Merge(dtTemp)

                    For Each drCurrent In dtCurrent.Rows

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent.UtilityUnitCode.ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drCurrent.Year.ToString()
                        parms(2, 0) = "@Original_Period"
                        parms(2, 1) = "Period, " + drCurrent.Period.ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows <> 0 Then
                            drCurrent.RowError = "Data has already been added by another user"
                            Return "Row has been added by another user"
                        End If

                        ' *** The row does not exist so use INSERT ***

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent.UtilityUnitCode.ToString()
                        parms(1, 0) = "@UnitShortName"
                        parms(1, 1) = "UnitShortName, " + drCurrent.UnitShortName.ToString()
                        parms(2, 0) = "@Year"
                        parms(2, 1) = "Year, " + drCurrent.Year.ToString()
                        parms(3, 0) = "@Period"
                        parms(3, 1) = "Period, " + drCurrent.Period.ToString()
                        parms(4, 0) = "@PerfCalcDate"
                        If Thread.CurrentThread.CurrentCulture.Name.ToString = "en-US" Then
                            parms(4, 1) = "PerfCalcDate, " + EOM(System.DateTime.Parse(drCurrent.Period & "/01/" & drCurrent.Year.ToString)).ToString
                        Else
                            parms(4, 1) = "PerfCalcDate, " + EOM(System.DateTime.Parse("01/" & drCurrent.Period & "/" & drCurrent.Year.ToString)).ToString
                        End If

                        parms(5, 0) = "@Field01"
                        If drCurrent.IsField01Null Then
                            parms(5, 1) = "Field01, 0"
                        Else
                            parms(5, 1) = "Field01, " + drCurrent.Field01.ToString()
                        End If

                        parms(6, 0) = "@Field02"
                        If drCurrent.IsField02Null Then
                            parms(6, 1) = "Field02, 0"
                        Else
                            parms(6, 1) = "Field02, " + drCurrent.Field02.ToString()
                        End If

                        parms(7, 0) = "@Field03"
                        If drCurrent.IsField03Null Then
                            parms(7, 1) = "Field03, 0"
                        Else
                            parms(7, 1) = "Field03, " + drCurrent.Field01.ToString()
                        End If

                        parms(8, 0) = "@Field04"
                        If drCurrent.IsField04Null Then
                            parms(8, 1) = "Field04, 0"
                        Else
                            parms(8, 1) = "Field04, " + drCurrent.Field04.ToString()
                        End If

                        parms(9, 0) = "@Field05"
                        If drCurrent.IsField05Null Then
                            parms(9, 1) = "Field05, 0"
                        Else
                            parms(9, 1) = "Field05, " + drCurrent.Field05.ToString()
                        End If

                        parms(10, 0) = "@Field06"
                        If drCurrent.IsField06Null Then
                            parms(10, 1) = "Field06, 0"
                        Else
                            parms(10, 1) = "Field06, " + drCurrent.Field06.ToString()
                        End If

                        parms(11, 0) = "@Field07"
                        If drCurrent.IsField07Null Then
                            parms(11, 1) = "Field07, 0"
                        Else
                            parms(11, 1) = "Field07, " + drCurrent.Field07.ToString()
                        End If

                        parms(12, 0) = "@Field08"
                        If drCurrent.IsField08Null Then
                            parms(12, 1) = "Field08, 0"
                        Else
                            parms(12, 1) = "Field08, " + drCurrent.Field08.ToString()
                        End If

                        parms(13, 0) = "@Field09"
                        If drCurrent.IsField09Null Then
                            parms(13, 1) = "Field09, 0"
                        Else
                            parms(13, 1) = "Field09, " + drCurrent.Field09.ToString()
                        End If

                        parms(14, 0) = "@Field10"
                        If drCurrent.IsField10Null Then
                            parms(14, 1) = "Field10, 0"
                        Else
                            parms(14, 1) = "Field10, " + drCurrent.Field10.ToString()
                        End If

                        Try
                            ' try to update the record in the database
                            ' even with a "new" record, someone else could have already loaded this same performance record

                            intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)
                            drCurrent.AcceptChanges()

                        Catch dbEx As DBConcurrencyException

                            Me.ThrowGADSNGException("Data Concurrency Exception Occurred", dbEx)
                            Return "Data Concurrency Exception Occurred"

                        Catch Ex As System.Exception

                            Me.ThrowGADSNGException("Exception occurred in FormLoadCustPerformance", Ex)
                            Return "Exception Occurred in FormLoadCustPerformance"

                        End Try

                    Next

                End If

            End If

            dtTemp = dsLoad.GetChanges(DataRowState.Modified)

            If Not dtTemp Is Nothing Then

                If dtTemp.Rows.Count > 0 Then

                    dtCurrent.Clear()
                    dtCurrent.Merge(dtTemp)

                    For Each drCurrent In dtTemp.Rows

                        drCurrent.RowError = String.Empty

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent.UtilityUnitCode.ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drCurrent.Year.ToString()
                        parms(2, 0) = "@Original_Period"
                        parms(2, 1) = "Period, " + drCurrent.Period.ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows = 0 Then
                            drCurrent.RowError = "Data has been deleted"
                            Return "Row has been deleted"
                        End If

                        System.Array.Clear(parms, 0, parms.Length)

                        ' *** The row exists so use UPDATE *** 

                        parms(0, 0) = "@UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent.UtilityUnitCode.ToString()
                        parms(1, 0) = "@UnitShortName"
                        parms(1, 1) = "UnitShortName, " + drCurrent.UnitShortName.ToString()
                        parms(2, 0) = "@Year"
                        parms(2, 1) = "Year, " + drCurrent.Year.ToString()
                        parms(3, 0) = "@Period"
                        parms(3, 1) = "Period, " + drCurrent.Period.ToString()
                        parms(4, 0) = "@PerfCalcDate"
                        If Thread.CurrentThread.CurrentCulture.Name.ToString = "en-US" Then
                            parms(4, 1) = "PerfCalcDate, " + EOM(System.DateTime.Parse(drCurrent.Period & "/01/" & drCurrent.Year.ToString)).ToString
                        Else
                            parms(4, 1) = "PerfCalcDate, " + EOM(System.DateTime.Parse("01/" & drCurrent.Period & "/" & drCurrent.Year.ToString)).ToString
                        End If

                        parms(5, 0) = "@Field01"
                        If drCurrent.IsField01Null Then
                            parms(5, 1) = "Field01, 0"
                        Else
                            parms(5, 1) = "Field01, " + drCurrent.Field01.ToString()
                        End If

                        parms(6, 0) = "@Field02"
                        If drCurrent.IsField02Null Then
                            parms(6, 1) = "Field02, 0"
                        Else
                            parms(6, 1) = "Field02, " + drCurrent.Field02.ToString()
                        End If

                        parms(7, 0) = "@Field03"
                        If drCurrent.IsField03Null Then
                            parms(7, 1) = "Field03, 0"
                        Else
                            parms(7, 1) = "Field03, " + drCurrent.Field03.ToString()
                        End If

                        parms(8, 0) = "@Field04"
                        If drCurrent.IsField04Null Then
                            parms(8, 1) = "Field04, 0"
                        Else
                            parms(8, 1) = "Field04, " + drCurrent.Field04.ToString()
                        End If

                        parms(9, 0) = "@Field05"
                        If drCurrent.IsField05Null Then
                            parms(9, 1) = "Field05, 0"
                        Else
                            parms(9, 1) = "Field05, " + drCurrent.Field05.ToString()
                        End If

                        parms(10, 0) = "@Field06"
                        If drCurrent.IsField06Null Then
                            parms(10, 1) = "Field06, 0"
                        Else
                            parms(10, 1) = "Field06, " + drCurrent.Field06.ToString()
                        End If

                        parms(11, 0) = "@Field07"
                        If drCurrent.IsField07Null Then
                            parms(11, 1) = "Field07, 0"
                        Else
                            parms(11, 1) = "Field07, " + drCurrent.Field07.ToString()
                        End If

                        parms(12, 0) = "@Field08"
                        If drCurrent.IsField08Null Then
                            parms(12, 1) = "Field08, 0"
                        Else
                            parms(12, 1) = "Field08, " + drCurrent.Field08.ToString()
                        End If

                        parms(13, 0) = "@Field09"
                        If drCurrent.IsField09Null Then
                            parms(13, 1) = "Field09, 0"
                        Else
                            parms(13, 1) = "Field09, " + drCurrent.Field09.ToString()
                        End If

                        parms(14, 0) = "@Field10"
                        If drCurrent.IsField10Null Then
                            parms(14, 1) = "Field10, 0"
                        Else
                            parms(14, 1) = "Field10, " + drCurrent.Field10.ToString()
                        End If

                        parms(15, 0) = "@Original_Period"
                        parms(15, 1) = "Period, " + drCurrent.Period.ToString()
                        parms(16, 0) = "@Original_UtilityUnitCode"
                        parms(16, 1) = "UtilityUnitCode, " + drCurrent.UtilityUnitCode.ToString()
                        parms(17, 0) = "@Original_Year"
                        parms(17, 1) = "Year, " + drCurrent.Year.ToString()

                        intUpdateRows = Factory.ExecuteNonQuery(updateStatement1, parms)

                        Try

                            drCurrent.AcceptChanges()

                        Catch dbEx As DBConcurrencyException

                            Me.ThrowGADSNGException("Data Concurrency Exception Occurred", dbEx)
                            Return "Data Concurrency Exception Occurred"

                        Catch Ex As System.Exception

                            Me.ThrowGADSNGException("Exception occurred in FormLoadCustPerformance", Ex)
                            Return "Exception in FormLoadCustPerformance"

                        End Try

                    Next

                End If

            End If

            dsLoad.AcceptChanges()

            Return ""

        End Function

#End Region

#Region " LoadPJMPerformance(ByVal dtLoad As DataTablet) As Boolean "

        Public Function LoadPJMPerformance(ByVal dtLoad As DataTable) As Boolean

            Dim updateStatement As String
            Dim parms(5, 2) As String
            Dim intUpdateRows As Integer
            Dim drCurrent As DataRow

            If dtLoad Is Nothing Then
                ' No changes in the Performance data
                Return True
            End If

            updateStatement = "UPDATE PerformanceData SET PJMLoadStatus = @PJMLoadStatus, PJMStatusDate = @PJMStatusDate " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(Period = @Original_Period)"

            Try

                System.Array.Clear(parms, 0, parms.Length)

                parms(0, 0) = "@PJMLoadStatus"
                parms(1, 0) = "@PJMStatusDate"
                parms(2, 0) = "@Original_UtilityUnitCode"
                parms(3, 0) = "@Original_Year"
                parms(4, 0) = "@Original_Period"

                For Each drCurrent In dtLoad.Rows

                    parms(0, 1) = "PJMLoadStatus, " + drCurrent("PJMLoadStatus").ToString()
                    parms(1, 1) = "PJMStatusDate, " + drCurrent("PJMStatusDate").ToString()
                    parms(2, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                    parms(3, 1) = "Year, " + drCurrent("Year").ToString()
                    parms(4, 1) = "Period, " + drCurrent("Period").ToString()

                    intUpdateRows = Factory.ExecuteNonQuery(updateStatement, parms)

                Next

                Return True

            Catch e As System.Exception

                Me.ThrowGADSNGException("Error in LoadPJMPerformance", e)
                Return False

            End Try

        End Function

#End Region

#Region " LoadPJMEvents(ByVal dsLoad As AllEventData) As Boolean "

        Public Function LoadPJMEvents(ByVal dsLoad As AllEventData) As Boolean

            Dim updateStatement1 As String
            Dim updateStatement2 As String

            Dim parms(6, 2) As String
            Dim intUpdateRows As Integer
            Dim drCurrent1 As DataRow
            Dim drCurrent2 As DataRow
            Dim dtEvent01Changes As New DataTable
            Dim dtEvent02Changes As New DataTable

            Try
                dtEvent01Changes = dsLoad.Tables("EventData01").GetChanges

                If Not dtEvent01Changes Is Nothing Then

                    If dtEvent01Changes.Rows.Count > 0 Then

                        updateStatement1 = _
"UPDATE EventData01 SET PJMLoadStatus = @PJMLoadStatus, PJMStatusDate = @PJMStatusDate " & _
"WHERE " & _
"(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
"(Year = @Original_Year) AND " & _
"(EventNumber = @Original_EventNumber)"

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@PJMLoadStatus"
                        parms(1, 0) = "@PJMStatusDate"
                        parms(2, 0) = "@Original_UtilityUnitCode"
                        parms(3, 0) = "@Original_Year"
                        parms(4, 0) = "@Original_EventNumber"

                        For Each drCurrent1 In dtEvent01Changes.Rows

                            parms(0, 1) = "PJMLoadStatus, " + drCurrent1("PJMLoadStatus").ToString()
                            parms(1, 1) = "PJMStatusDate, " + drCurrent1("PJMStatusDate").ToString()
                            parms(2, 1) = "UtilityUnitCode, " + drCurrent1("UtilityUnitCode", DataRowVersion.Original).ToString
                            parms(3, 1) = "Year, " + drCurrent1("Year", DataRowVersion.Original).ToString
                            parms(4, 1) = "EventNumber, " + drCurrent1("EventNumber", DataRowVersion.Original).ToString

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement1, parms)

                        Next

                    End If

                End If

                dtEvent02Changes = dsLoad.Tables("EventData02").GetChanges

                If Not dtEvent02Changes Is Nothing Then

                    If dtEvent02Changes.Rows.Count > 0 Then

                        updateStatement2 = _
"UPDATE EventData02 SET PJMLoadStatus = @PJMLoadStatus, PJMStatusDate = @PJMStatusDate " & _
"WHERE " & _
"(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
"(Year = @Original_Year) AND " & _
"(EventNumber = @Original_EventNumber) AND " & _
"(EvenCardNumber = @Original_EvenCardNumber)"

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@PJMLoadStatus"
                        parms(1, 0) = "@PJMStatusDate"
                        parms(2, 0) = "@Original_UtilityUnitCode"
                        parms(3, 0) = "@Original_Year"
                        parms(4, 0) = "@Original_EventNumber"
                        parms(5, 0) = "@Original_EvenCardNumber"

                        For Each drCurrent2 In dtEvent02Changes.Rows

                            parms(0, 1) = "PJMLoadStatus, " + drCurrent2("PJMLoadStatus").ToString()
                            parms(1, 1) = "PJMStatusDate, " + drCurrent2("PJMStatusDate").ToString()
                            parms(2, 1) = "UtilityUnitCode, " + drCurrent2("UtilityUnitCode", DataRowVersion.Original).ToString
                            parms(3, 1) = "Year, " + drCurrent2("Year", DataRowVersion.Original).ToString
                            parms(4, 1) = "EventNumber, " + drCurrent2("EventNumber", DataRowVersion.Original).ToString
                            parms(5, 1) = "EvenCardNumber, " + drCurrent2("EvenCardNumber").ToString

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement2, parms)

                        Next

                    End If

                End If

                Return True

            Catch e As System.Exception

                Me.ThrowGADSNGException("Error in LoadPJMEvents", e)
                Return False

            End Try

        End Function

#End Region

#Region " UpdateRow(ByVal TableName As String, ByVal UtilityUnitCode As String) "

        Private Sub UpdateRow(ByVal TableName As String, ByVal UtilityUnitCode As String)
            ' Get a reference to the specified row
            'Dim dr As DataRow = dsAllData.tables(TableName).Rows.Find(UtilityUnitCode)

            ' Create a command update to pull the new underlying data
            ' create a SQL command to select * from performance where ...
            ' open a datareader
            ' dim rdr as sqlClient.sqldatareader = cmd.executeReader()
            ' rdr.read()
            ' Copy the new data from the database to the DataRow
            ' Dim dc as DataColumn
            ' for each dc in dr.table.Columns
            '   if dc.readonly = false then _
            '       dr.Item(dc.ColumnName) = rdr.Item(dc.ColumnName)
            ' Next

            ' Accept changes in the DataRow
            ' dr.AcceptChanges()
            ' close the data reader 


        End Sub

#End Region

#Region " LoadEvent01(ByVal dsLoad As DataSet) As Boolean "

        Public Function LoadEvent01(ByVal dsLoad As DataSet, ByVal boolISOFlag As Boolean) As Boolean

            Dim selectStatement As String
            Dim checkSetupStatement As String
            Dim insertStatement As String
            Dim updateStatement01 As String
            Dim updateStatement02 As String
            Dim updateStatement03 As String
            'Dim deleteStatement As String
            Dim buildDsSetupStatement As String
            'Dim tableName As String
            Dim parms(36, 2) As String
            Dim intRows As Integer
            Dim intUpdateRows As Integer
            'Dim daTest As IDataAdapter
            'Dim strTest As String
            Dim drCurrent As DataRow
            Dim dsSetup As DataSet
            Dim drSetup As DataRow
            Dim dtUTCNow As DateTime = System.DateTime.UtcNow

            selectStatement = "SELECT COUNT(*) FROM EventData01 WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber)"

            checkSetupStatement = "SELECT COUNT(*) FROM Setup WHERE " & _
                "(UtilityUnitCode = @UtilityUnitCode)"

            buildDsSetupStatement = "SELECT UtilityUnitCode, CheckStatus, UnitShortName, ServiceHourMethod FROM Setup"
            dsSetup = Me.Factory.GetDataSet(buildDsSetupStatement, Nothing)

            If Factory.Provider = "OleDb" Then
                Dim keys(0) As DataColumn
                keys(0) = dsSetup.Tables(0).Columns.Item("UtilityUnitCode")
                dsSetup.Tables(0).PrimaryKey = keys
            End If

            insertStatement = "INSERT INTO EventData01 " & _
                "(UtilityUnitCode, Year, EventNumber, RevisionCard01, EventType, " & _
                "StartDateTime, CarryOverLastYear, ChangeDateTime1, ChangeInEventType1, ChangeDateTime2, ChangeInEventType2, " & _
                "EndDateTime, CarryOverNextYear, GrossAvailCapacity, NetAvailCapacity, CauseCode, CauseCodeExt, " & _
                "WorkStarted, WorkEnded, ContribCode, PrimaryAlert, ManhoursWorked, VerbalDesc1, " & _
                "RevisionCard02, VerbalDesc2, RevisionCard03, RevMonthCard01, RevMonthCard02, " & _
                "RevMonthCard03, FailureMechCode, TripMech, CumFiredHours, CumEngineStarts, DominantDerate, VerbalDesc86, TimeStamp, UnitShortName) " & _
                "VALUES " + _
                "(@UtilityUnitCode, @Year, @EventNumber, @RevisionCard01, @EventType, @StartDateTime, @CarryOverLastYear, " & _
                "@ChangeDateTime1, @ChangeInEventType1, @ChangeDateTime2, @ChangeInEventType2, @EndDateTime, @CarryOverNextYear, " & _
                "@GrossAvailCapacity, @NetAvailCapacity, @CauseCode, @CauseCodeExt, @WorkStarted, @WorkEnded, " & _
                "@ContribCode, @PrimaryAlert, @ManhoursWorked, @VerbalDesc1, @RevisionCard02, " & _
                "@VerbalDesc2, @RevisionCard03, @RevMonthCard01, @RevMonthCard02, @RevMonthCard03, " & _
                "@FailureMechCode, @TripMech, @CumFiredHours, @CumEngineStarts, @DominantDerate, @VerbalDesc86, @TimeStamp, @UnitShortName)"

            updateStatement01 = "UPDATE EventData01 SET " & _
                "RevisionCard01 = @RevisionCard01, " & _
                "EventType = @EventType, " & _
                "StartDateTime = @StartDateTime, " & _
                "CarryOverLastYear = @CarryOverLastYear, " & _
                "ChangeDateTime1 = @ChangeDateTime1, " & _
                "ChangeInEventType1 = @ChangeInEventType1, " & _
                "ChangeDateTime2 = @ChangeDateTime2, " & _
                "ChangeInEventType2 = @ChangeInEventType2, " & _
                "EndDateTime = @EndDateTime, " & _
                "CarryOverNextYear = @CarryOverNextYear, " & _
                "GrossAvailCapacity = @GrossAvailCapacity, " & _
                "NetAvailCapacity = @NetAvailCapacity, " & _
                "RevMonthCard01 = @RevMonthCard01, " & _
                "DominantDerate = @DominantDerate, " & _
                "TimeStamp = @TimeStamp " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(RevisionCard01 <= @Original_RevisionCard01)"

            updateStatement02 = "UPDATE EventData01 SET " & _
                "CauseCode = @CauseCode, " & _
                "CauseCodeExt = @CauseCodeExt, " & _
                "WorkStarted = @WorkStarted, " & _
                "WorkEnded = @WorkEnded, " & _
                "ContribCode = @ContribCode, " & _
                "PrimaryAlert = @PrimaryAlert, " & _
                "ManhoursWorked = @ManhoursWorked, " & _
                "VerbalDesc1 = @VerbalDesc1, " & _
                "RevisionCard02 = @RevisionCard02, " & _
                "RevMonthCard02 = @RevMonthCard02, " & _
                "FailureMechCode = @FailureMechCode, " & _
                "TripMech = @TripMech, " & _
                "CumFiredHours = @CumFiredHours, " & _
                "CumEngineStarts = @CumEngineStarts, " & _
                "VerbalDesc86 = @VerbalDesc86, " & _
                "TimeStamp = @TimeStamp " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(RevisionCard02 <= @Original_RevisionCard02)"

            updateStatement03 = "UPDATE EventData01 SET " & _
                "VerbalDesc2 = @VerbalDesc2, " & _
                "RevisionCard03 = @RevisionCard03, " & _
                "RevMonthCard03 = @RevMonthCard03, " & _
                "VerbalDesc86 = @VerbalDesc86, " & _
                "TimeStamp = @TimeStamp " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(RevisionCard03 <= @Original_RevisionCard03)"

            Dim alSHMethod As New ArrayList
            alSHMethod = BuildALSHMethod()
            Dim dtCheck As System.DateTime = System.DateTime.Now

            Try

                For Each drCurrent In dsLoad.Tables(0).Rows

                    System.Array.Clear(parms, 0, parms.Length)
                    'Application.DoEvents()

                    parms(0, 0) = "@UtilityUnitCode"
                    parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                    ' Check to see if this unit is in Setup -- skip loading this record if not in Setup
                    drSetup = dsSetup.Tables(0).Rows.Find(drCurrent("UtilityUnitCode").ToString)

                    'If not drSetup is nothing and boolISOFlag = True Then

                    '  THIS BLOCK ALLOWS YOU TO KEEP CERTAIN EVENT TYPES BASED ON THE SHMETHOD FROM BEING LOADED AT ALL
                    '  RIGHT NOW IT ALLOWS THEM TO BE LOADED BUT THEN FLAGS THEM AS ERRORS (E)

                    '    If Not IsDBNull(drCurrent("StartDateTime")) Then
                    '        Try
                    '            ' the check to accept or reject this event record is based on the start of event datetime
                    '            dtCheck = System.DateTime.Parse(drCurrent("StartDateTime").ToString)
                    '        Catch ex As Exception
                    '            dtCheck = System.DateTime.Now
                    '        End Try
                    '    Else
                    '        dtCheck = System.DateTime.Now
                    '    End If

                    '    If IncludedEvents(drSetup, alSHMethod, dtCheck) = 2 Then
                    '        ' Event Data are NOT reported so do not load
                    '        LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, "Settings show NO events to be reported", "W", drSetup("UnitShortName").ToString, GetFromDictionary)
                    '        drSetup = Nothing
                    '    ElseIf IncludedEvents(drSetup, alSHMethod, dtCheck) = 1 And drCurrent("EventType").ToString.Trim = "RS" Then
                    '        ' Event Data are report but RS events are NOT reported so do not load
                    '        LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, "Settings show NO RS events to be reported", "W", drSetup("UnitShortName").ToString, GetFromDictionary)
                    '        drSetup = Nothing
                    '    End If

                    'End If

                    If drSetup Is Nothing Then

                        ' Skip this record

                    Else

                        ' The unit is in Setup
                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows = 0 Then

                            ' *** The row does not exist so use INSERT ***

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@UtilityUnitCode"
                            parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(1, 0) = "@Year"
                            parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(2, 0) = "@EventNumber"
                            parms(2, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                            parms(3, 0) = "@RevisionCard01"
                            If drCurrent("RevisionCard01").ToString = "*" Then
                                parms(3, 1) = "RevisionCard01, 0"
                            Else
                                parms(3, 1) = "RevisionCard01, " + drCurrent("RevisionCard01").ToString()
                            End If
                            parms(4, 0) = "@EventType"
                            parms(4, 1) = "EventType, " + drCurrent("EventType").ToString()
                            parms(5, 0) = "@StartDateTime"
                            parms(5, 1) = "StartDateTime, " + drCurrent("StartDateTime").ToString()
                            parms(6, 0) = "@CarryOverLastYear"
                            parms(6, 1) = "CarryOverLastYear, " + drCurrent("CarryOverLastYear").ToString()
                            parms(7, 0) = "@ChangeDateTime1"
                            parms(7, 1) = "ChangeDateTime1, " + drCurrent("ChangeDateTime1").ToString()
                            parms(8, 0) = "@ChangeInEventType1"
                            parms(8, 1) = "ChangeInEventType1, " + drCurrent("ChangeInEventType1").ToString()
                            parms(9, 0) = "@ChangeDateTime2"
                            parms(9, 1) = "ChangeDateTime2, " + drCurrent("ChangeDateTime2").ToString()
                            parms(10, 0) = "@ChangeInEventType2"
                            parms(10, 1) = "ChangeInEventType2, " + drCurrent("ChangeInEventType2").ToString()
                            parms(11, 0) = "@EndDateTime"
                            parms(11, 1) = "EndDateTime, " + drCurrent("EndDateTime").ToString()
                            parms(12, 0) = "@CarryOverNextYear"
                            parms(12, 1) = "CarryOverNextYear, " + drCurrent("CarryOverNextYear").ToString()
                            parms(13, 0) = "@GrossAvailCapacity"
                            parms(13, 1) = "GrossAvailCapacity, " + drCurrent("GrossAvailCapacity").ToString()
                            parms(14, 0) = "@NetAvailCapacity"
                            parms(14, 1) = "NetAvailCapacity, " + drCurrent("NetAvailCapacity").ToString()
                            parms(15, 0) = "@CauseCode"
                            parms(15, 1) = "CauseCode, " + drCurrent("CauseCode").ToString()
                            parms(16, 0) = "@CauseCodeExt"
                            If IsDBNull(drCurrent("CauseCodeExt")) Then
                                drCurrent("CauseCodeExt") = "  "
                            ElseIf drCurrent("CauseCodeExt").ToString().Trim() = String.Empty Then
                                drCurrent("CauseCodeExt") = "  "
                            End If
                            parms(16, 1) = "CauseCodeExt, " + drCurrent("CauseCodeExt").ToString()
                            parms(17, 0) = "@WorkStarted"
                            parms(17, 1) = "WorkStarted, " + drCurrent("WorkStarted").ToString()
                            parms(18, 0) = "@WorkEnded"
                            parms(18, 1) = "WorkEnded, " + drCurrent("WorkEnded").ToString()
                            parms(19, 0) = "@ContribCode"
                            parms(19, 1) = "ContribCode, " + drCurrent("ContribCode").ToString()
                            parms(20, 0) = "@PrimaryAlert"
                            parms(20, 1) = "PrimaryAlert, " + drCurrent("PrimaryAlert").ToString()
                            parms(21, 0) = "@ManhoursWorked"
                            parms(21, 1) = "ManhoursWorked, " + drCurrent("ManhoursWorked").ToString()
                            parms(22, 0) = "@VerbalDesc1"
                            If IsDBNull(drCurrent("VerbalDesc1")) Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            If drCurrent("VerbalDesc1").ToString().Trim = String.Empty Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            parms(22, 1) = "VerbalDesc1, " + drCurrent("VerbalDesc1").ToString()
                            parms(23, 0) = "@RevisionCard02"
                            If drCurrent("RevisionCard02").ToString() = "*" Then
                                parms(23, 1) = "RevisionCard02, 0"
                            Else
                                parms(23, 1) = "RevisionCard02, " + drCurrent("RevisionCard02").ToString()
                            End If
                            parms(24, 0) = "@VerbalDesc2"
                            parms(24, 1) = "VerbalDesc2, " + drCurrent("VerbalDesc2").ToString()
                            parms(25, 0) = "@RevisionCard03"
                            If drCurrent("RevisionCard03").ToString() = "*" Then
                                parms(25, 1) = "RevisionCard03, 0"
                            Else
                                parms(25, 1) = "RevisionCard03, " + drCurrent("RevisionCard03").ToString()
                            End If
                            parms(26, 0) = "@RevMonthCard01"
                            parms(26, 1) = "RevMonthCard01, " + drCurrent("RevMonthCard01").ToString()
                            parms(27, 0) = "@RevMonthCard02"
                            parms(27, 1) = "RevMonthCard02, " + drCurrent("RevMonthCard02").ToString()
                            parms(28, 0) = "@RevMonthCard03"
                            parms(28, 1) = "RevMonthCard03, " + drCurrent("RevMonthCard03").ToString()
                            parms(29, 0) = "@FailureMechCode"
                            parms(29, 1) = "FailureMechCode, " + drCurrent("FailureMechCode").ToString()
                            parms(30, 0) = "@TripMech"
                            parms(30, 1) = "TripMech, " + drCurrent("TripMech").ToString()
                            parms(31, 0) = "@CumFiredHours"
                            parms(31, 1) = "CumFiredHours, " + drCurrent("CumFiredHours").ToString()
                            parms(32, 0) = "@CumEngineStarts"
                            parms(32, 1) = "CumEngineStarts, " + drCurrent("CumEngineStarts").ToString()
                            parms(33, 0) = "@DominantDerate"
                            parms(33, 1) = "DominantDerate, " + drCurrent("DominantDerate").ToString()
                            parms(34, 0) = "@VerbalDesc86"
                            parms(34, 1) = "VerbalDesc86, " + drCurrent("VerbalDesc86").ToString()
                            parms(35, 0) = "@TimeStamp"
                            parms(35, 1) = "TimeStamp, " + dtUTCNow.ToString
                            parms(36, 0) = "@UnitShortName"
                            If IsDBNull(drSetup.Item("UnitShortName")) Then
                                parms(36, 1) = "UnitShortName, UNKNOWN"
                            Else
                                parms(36, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                            End If


                            intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                        Else

                            ' *** The row exists so use UPDATE *** 

                            ' Loading up the Event 01 card

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@RevisionCard01"
                            parms(0, 1) = "RevisionCard01, " + drCurrent("RevisionCard01").ToString()
                            parms(1, 0) = "@EventType"
                            parms(1, 1) = "EventType, " + drCurrent("EventType").ToString()
                            parms(2, 0) = "@StartDateTime"
                            parms(2, 1) = "StartDateTime, " + drCurrent("StartDateTime").ToString()
                            parms(3, 0) = "@CarryOverLastYear"
                            parms(3, 1) = "CarryOverLastYear, " + drCurrent("CarryOverLastYear").ToString()
                            parms(4, 0) = "@ChangeDateTime1"
                            parms(4, 1) = "ChangeDateTime1, " + drCurrent("ChangeDateTime1").ToString()
                            parms(5, 0) = "@ChangeInEventType1"
                            parms(5, 1) = "ChangeInEventType1, " + drCurrent("ChangeInEventType1").ToString()
                            parms(6, 0) = "@ChangeDateTime2"
                            parms(6, 1) = "ChangeDateTime2, " + drCurrent("ChangeDateTime2").ToString()
                            parms(7, 0) = "@ChangeInEventType2"
                            parms(7, 1) = "ChangeInEventType2, " + drCurrent("ChangeInEventType2").ToString()
                            parms(8, 0) = "@EndDateTime"
                            parms(8, 1) = "EndDateTime, " + drCurrent("EndDateTime").ToString()
                            parms(9, 0) = "@CarryOverNextYear"
                            parms(9, 1) = "CarryOverNextYear, " + drCurrent("CarryOverNextYear").ToString()
                            parms(10, 0) = "@GrossAvailCapacity"
                            parms(10, 1) = "GrossAvailCapacity, " + drCurrent("GrossAvailCapacity").ToString()
                            parms(11, 0) = "@NetAvailCapacity"
                            parms(11, 1) = "NetAvailCapacity, " + drCurrent("NetAvailCapacity").ToString()
                            parms(12, 0) = "@RevMonthCard01"
                            parms(12, 1) = "RevMonthCard01, " + drCurrent("RevMonthCard01").ToString()
                            parms(13, 0) = "@DominantDerate"
                            parms(13, 1) = "DominantDerate, " + drCurrent("DominantDerate").ToString()
                            parms(14, 0) = "@TimeStamp"
                            parms(14, 1) = "TimeStamp, " + dtUTCNow.ToString

                            parms(15, 0) = "@Original_UtilityUnitCode"
                            parms(15, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(16, 0) = "@Original_Year"
                            parms(16, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(17, 0) = "@Original_EventNumber"
                            parms(17, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                            parms(18, 0) = "@Original_RevisionCard01"
                            parms(18, 1) = "RevisionCard01, " + drCurrent("RevisionCard01").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement01, parms)

                            ' Loading up the Event 02 card

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@CauseCode"
                            parms(0, 1) = "CauseCode, " + drCurrent("CauseCode").ToString()
                            parms(1, 0) = "@CauseCodeExt"
                            If IsDBNull(drCurrent("CauseCodeExt")) Then
                                drCurrent("CauseCodeExt") = "  "
                            ElseIf drCurrent("CauseCodeExt").ToString().Trim() = String.Empty Then
                                drCurrent("CauseCodeExt") = "  "
                            End If
                            parms(1, 1) = "CauseCodeExt, " + drCurrent("CauseCodeExt").ToString()
                            parms(2, 0) = "@WorkStarted"
                            parms(2, 1) = "WorkStarted, " + drCurrent("WorkStarted").ToString()
                            parms(3, 0) = "@WorkEnded"
                            parms(3, 1) = "WorkEnded, " + drCurrent("WorkEnded").ToString()
                            parms(4, 0) = "@ContribCode"
                            parms(4, 1) = "ContribCode, " + drCurrent("ContribCode").ToString()
                            parms(5, 0) = "@PrimaryAlert"
                            parms(5, 1) = "PrimaryAlert, " + drCurrent("PrimaryAlert").ToString()
                            parms(6, 0) = "@ManhoursWorked"
                            parms(6, 1) = "ManhoursWorked, " + drCurrent("ManhoursWorked").ToString()
                            parms(7, 0) = "@VerbalDesc1"
                            If IsDBNull(drCurrent("VerbalDesc1")) Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            If drCurrent("VerbalDesc1").ToString().Trim = String.Empty Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            parms(7, 1) = "VerbalDesc1, " + drCurrent("VerbalDesc1").ToString()
                            parms(8, 0) = "@RevisionCard02"
                            parms(8, 1) = "RevisionCard02, " + drCurrent("RevisionCard02").ToString()
                            parms(9, 0) = "@RevMonthCard02"
                            parms(9, 1) = "RevMonthCard02, " + drCurrent("RevMonthCard02").ToString()
                            parms(10, 0) = "@FailureMechCode"
                            parms(10, 1) = "FailureMechCode, " + drCurrent("FailureMechCode").ToString()
                            parms(11, 0) = "@TripMech"
                            parms(11, 1) = "TripMech, " + drCurrent("TripMech").ToString()
                            parms(12, 0) = "@CumFiredHours"
                            parms(12, 1) = "CumFiredHours, " + drCurrent("CumFiredHours").ToString()
                            parms(13, 0) = "@CumEngineStarts"
                            parms(13, 1) = "CumEngineStarts, " + drCurrent("CumEngineStarts").ToString()
                            parms(14, 0) = "@VerbalDesc86"
                            parms(14, 1) = "VerbalDesc86, " + drCurrent("VerbalDesc86").ToString()
                            parms(15, 0) = "@TimeStamp"
                            parms(15, 1) = "TimeStamp, " + dtUTCNow.ToString

                            parms(16, 0) = "@Original_UtilityUnitCode"
                            parms(16, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(17, 0) = "@Original_Year"
                            parms(17, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(18, 0) = "@Original_EventNumber"
                            parms(18, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                            parms(19, 0) = "@Original_RevisionCard02"
                            parms(19, 1) = "RevisionCard02, " + drCurrent("RevisionCard02").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement02, parms)

                            ' Loading up the Event 03 card

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@VerbalDesc2"
                            parms(0, 1) = "VerbalDesc2, " + drCurrent("VerbalDesc2").ToString()
                            parms(1, 0) = "@RevisionCard03"
                            parms(1, 1) = "RevisionCard03, " + drCurrent("RevisionCard03").ToString()
                            parms(2, 0) = "@RevMonthCard03"
                            parms(2, 1) = "RevMonthCard03, " + drCurrent("RevMonthCard03").ToString()
                            parms(3, 0) = "@VerbalDesc86"
                            parms(3, 1) = "VerbalDesc86, " + drCurrent("VerbalDesc86").ToString()
                            parms(4, 0) = "@TimeStamp"
                            parms(4, 1) = "TimeStamp, " + dtUTCNow.ToString

                            parms(5, 0) = "@Original_UtilityUnitCode"
                            parms(5, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(6, 0) = "@Original_Year"
                            parms(6, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(7, 0) = "@Original_EventNumber"
                            parms(7, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                            parms(8, 0) = "@Original_RevisionCard03"
                            parms(8, 1) = "RevisionCard03, " + drCurrent("RevisionCard03").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement03, parms)

                        End If

                    End If

                Next

                UpdateSetupCheckStatus(dsSetup)

                Return True

            Catch ex As System.Exception

                Me.ThrowGADSNGException("Error in LoadEvent01", ex)

                Return False

            End Try

        End Function

#End Region

        Function IncludedEvents(ByRef drSetup As DataRow, ByRef alSHMethod As ArrayList, ByRef dtCheck As DateTime) As Integer

            Dim mySHM As ServiceHrMethod

            Dim myEnumerator As System.Collections.IEnumerator = alSHMethod.GetEnumerator()

            While myEnumerator.MoveNext()

                mySHM = DirectCast(myEnumerator.Current, ServiceHrMethod)

                If mySHM.UtilityUnitCode.Trim = drSetup("UtilityUnitCode").ToString.Trim Then

                    If mySHM.EffStartDate <= dtCheck And dtCheck <= mySHM.EffEndDate Then
                        Return mySHM.ServiceHourMethod
                    End If

                End If

            End While

            Return Convert.ToInt32(drSetup("ServiceHourMethod"))

        End Function

        Public Function BuildALSHMethod() As ArrayList

            Dim alReturn As New ArrayList
            Dim myReader As IDataReader = Nothing
            Dim stringSelect As String
            Dim myEffStartDate As DateTime
            Dim myEffEndDate As DateTime
            Dim mySHMethod As Integer
            Dim myUtilityUnitCode As String

            stringSelect = "SELECT EffStartDate, EffEndDate, ServiceHourMethod, UtilityUnitCode FROM SHMethod ORDER BY UtilityUnitCode, EffStartDate DESC"

            Try
                myReader = Factory.ExecuteDataReader(stringSelect, Nothing)

                Do While myReader.Read

                    If myReader.IsDBNull(0) Then
                        myEffStartDate = System.DateTime.Parse("01/01/1980")
                    Else
                        myEffStartDate = myReader.GetDateTime(0)
                    End If

                    If myReader.IsDBNull(1) Then
                        myEffEndDate = System.DateTime.Parse("01/01/2100")
                    Else
                        myEffEndDate = EOD(myReader.GetDateTime(1))
                    End If

                    If myReader.IsDBNull(2) Then
                        mySHMethod = 0
                    Else
                        mySHMethod = Convert.ToInt32(myReader.GetValue(2))
                    End If

                    If myReader.IsDBNull(3) Then
                        myUtilityUnitCode = "ZZZZZZ"
                    Else
                        myUtilityUnitCode = myReader.GetValue(3).ToString.Trim
                    End If

                    alReturn.Add(New ServiceHrMethod(myUtilityUnitCode, myEffStartDate, myEffEndDate, mySHMethod))

                Loop

            Catch ex As Exception
                'stringSelect = ex.Message
                'stringSelect = ex.ToString
                ' the table SHMethod does not exist
                myEffStartDate = System.DateTime.Parse("01/01/1980")
                myEffEndDate = System.DateTime.Parse("01/01/2100")
                mySHMethod = 0
                myUtilityUnitCode = "ZZZZZZ"
                alReturn.Add(New ServiceHrMethod(myUtilityUnitCode, myEffStartDate, myEffEndDate, mySHMethod))
            End Try

            Try
                myReader.Close()
                myReader.Dispose()
            Catch ex As Exception

            End Try

            If alReturn.Count = 0 Then
                myEffStartDate = System.DateTime.Parse("01/01/1980")
                myEffEndDate = System.DateTime.Parse("01/01/2100")
                mySHMethod = 0
                myUtilityUnitCode = "ZZZZZZ"
                alReturn.Add(New ServiceHrMethod(myUtilityUnitCode, myEffStartDate, myEffEndDate, mySHMethod))
            End If

            Return alReturn

        End Function

        Function EOD(ByVal dtValue As DateTime) As DateTime
            Dim strDateTimeFormat As String
            If Thread.CurrentThread.CurrentCulture.Name.ToString = "en-US" Then
                strDateTimeFormat = "MM/dd/yyyy"
            Else
                strDateTimeFormat = "dd/MM/yyyy"
            End If
            Return DateTime.Parse(dtValue.ToString(strDateTimeFormat) + " 23:59:59")
        End Function

#Region " LoadEvent0499(ByVal dsLoad As DataSet) As Boolean "

        Public Function LoadEvent0499(ByVal dsLoad As DataSet, ByVal boolISOFlag As Boolean) As Boolean

            Dim selectStatement As String
            Dim checkSetupStatement As String
            Dim insertStatement As String
            Dim updateStatement04 As String
            Dim updateStatement05 As String
            'Dim deleteStatement As String
            Dim buildDsSetupStatement As String
            'Dim tableName As String
            Dim parms(24, 2) As String
            Dim intRows As Integer
            Dim intUpdateRows As Integer
            'Dim daTest As IDataAdapter
            'Dim strTest As String
            Dim drCurrent As DataRow
            Dim dsSetup As DataSet
            Dim drSetup As DataRow
            Dim dtUTCNow As DateTime = System.DateTime.UtcNow

            selectStatement = "SELECT COUNT(*) FROM EventData02 WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(EvenCardNumber = @Original_EvenCardNumber)"

            checkSetupStatement = "SELECT COUNT(*) FROM Setup WHERE " & _
                "(UtilityUnitCode = @UtilityUnitCode)"

            buildDsSetupStatement = "SELECT UtilityUnitCode, CheckStatus, UnitShortName, ServiceHourMethod FROM Setup"
            dsSetup = Me.Factory.GetDataSet(buildDsSetupStatement, Nothing)

            If Factory.Provider = "OleDb" Then
                Dim keys(0) As DataColumn
                keys(0) = dsSetup.Tables(0).Columns.Item("UtilityUnitCode")
                dsSetup.Tables(0).PrimaryKey = keys
            End If

            insertStatement = "INSERT INTO EventData02 " & _
                "(UtilityUnitCode, Year, EventNumber, RevisionCardEven, EventType, " & _
                "CauseCode, CauseCodeExt, " & _
                "WorkStarted, WorkEnded, ContribCode, PrimaryAlert, ManhoursWorked, VerbalDesc1, " & _
                "EvenCardNumber, VerbalDesc2, RevisionCardOdd, RevMonthCardEven, RevMonthCardOdd, " & _
                "FailureMechCode, TripMech, CumFiredHours, CumEngineStarts, VerbalDesc86, TimeStamp, UnitShortName) " & _
                "VALUES " + _
                "(@UtilityUnitCode, @Year, @EventNumber, @RevisionCardEven, @EventType, @CauseCode, " & _
                "@CauseCodeExt, @WorkStarted, @WorkEnded, @ContribCode, @PrimaryAlert, @ManhoursWorked, " & _
                "@VerbalDesc1, @EvenCardNumber, @VerbalDesc2, @RevisionCardOdd, @RevMonthCardEven, @RevMonthCardOdd, " & _
                "@FailureMechCode, @TripMech, @CumFiredHours, @CumEngineStarts, @VerbalDesc86, @TimeStamp, @UnitShortName)"

            updateStatement04 = "UPDATE EventData02 SET " & _
                "EventType = @EventType, " & _
                "RevisionCardEven = @RevisionCardEven, " & _
                "CauseCode = @CauseCode, " & _
                "CauseCodeExt = @CauseCodeExt, " & _
                "WorkStarted = @WorkStarted, " & _
                "WorkEnded = @WorkEnded, " & _
                "ContribCode = @ContribCode, " & _
                "PrimaryAlert = @PrimaryAlert, " & _
                "ManhoursWorked = @ManhoursWorked, " & _
                "VerbalDesc1 = @VerbalDesc1, " & _
                "EvenCardNumber = @EvenCardNumber, " & _
                "RevMonthCardEven = @RevMonthCardEven, " & _
                "FailureMechCode = @FailureMechCode, " & _
                "TripMech = @TripMech, " & _
                "CumFiredHours = @CumFiredHours, " & _
                "CumEngineStarts = @CumEngineStarts, " & _
                "VerbalDesc86 = @VerbalDesc86, " & _
                "TimeStamp = @TimeStamp " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(EvenCardNumber = @Original_EvenCardNumber) AND " & _
                "(RevisionCardEven <= @Original_RevisionCardEven)"

            updateStatement05 = "UPDATE EventData02 SET " & _
                "VerbalDesc2 = @VerbalDesc2, " & _
                "RevisionCardOdd = @RevisionCardOdd, " & _
                "RevMonthCardOdd = @RevMonthCardOdd, " & _
                "VerbalDesc86 = @VerbalDesc86, " & _
                "TimeStamp = @TimeStamp " & _
                "WHERE " & _
                "(UtilityUnitCode = @Original_UtilityUnitCode) AND " & _
                "(Year = @Original_Year) AND " & _
                "(EventNumber = @Original_EventNumber) AND " & _
                "(EvenCardNumber = @Original_EvenCardNumber) AND " & _
                "(RevisionCardOdd <= @Original_RevisionCardOdd)"

            Dim alSHMethod As New ArrayList
            alSHMethod = BuildALSHMethod()
            Dim dtCheck As System.DateTime = System.DateTime.Now

            Try

                For Each drCurrent In dsLoad.Tables(0).Rows

                    System.Array.Clear(parms, 0, parms.Length)
                    'Application.DoEvents()

                    parms(0, 0) = "@UtilityUnitCode"
                    parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()

                    ' Check to see if this unit is in Setup -- skip loading this record if not in Setup
                    drSetup = dsSetup.Tables(0).Rows.Find(drCurrent("UtilityUnitCode").ToString)

                    If Not drSetup Is Nothing And boolISOFlag = True Then
                        If IncludedEvents(drSetup, alSHMethod, dtCheck) = 2 Then
                            ' Event Data are NOT reported so do not load
                            LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, "Settings show NO events to be reported", "W", drSetup("UnitShortName").ToString, GetFromDictionary)
                            drSetup = Nothing
                        ElseIf IncludedEvents(drSetup, alSHMethod, dtCheck) = 1 And drCurrent("EventType").ToString.Trim = "RS" Then
                            ' Event Data are report but RS events are NOT reported so do not load
                            LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, "Settings show NO RS events to be reported", "W", drSetup("UnitShortName").ToString, GetFromDictionary)
                            drSetup = Nothing
                        End If
                    End If

                    If drSetup Is Nothing Then

                        ' Skip this record

                    Else

                        ' The unit is in Setup
                        drSetup.Item("CheckStatus") = "X"
                        drSetup.AcceptChanges()

                        System.Array.Clear(parms, 0, parms.Length)

                        parms(0, 0) = "@Original_UtilityUnitCode"
                        parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                        parms(1, 0) = "@Original_Year"
                        parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                        parms(2, 0) = "@Original_EventNumber"
                        parms(2, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                        parms(3, 0) = "@Original_EvenCardNumber"
                        parms(3, 1) = "EvenCardNumber, " + drCurrent("EvenCardNumber").ToString()

                        intRows = CInt(Factory.ExecuteScalar(selectStatement, parms))

                        If intRows = 0 Then

                            ' *** The row does not exist so use INSERT ***

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@UtilityUnitCode"
                            parms(0, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(1, 0) = "@Year"
                            parms(1, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(2, 0) = "@EventNumber"
                            parms(2, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                            parms(3, 0) = "@RevisionCardEven"
                            If drCurrent("RevisionCardEven").ToString = "*" Then
                                parms(3, 1) = "RevisionCardEven, 0"
                            Else
                                parms(3, 1) = "RevisionCardEven, " + drCurrent("RevisionCardEven").ToString()
                            End If
                            parms(4, 0) = "@EventType"
                            parms(4, 1) = "EventType, " + drCurrent("EventType").ToString()
                            parms(5, 0) = "@CauseCode"
                            parms(5, 1) = "CauseCode, " + drCurrent("CauseCode").ToString()
                            parms(6, 0) = "@CauseCodeExt"
                            If IsDBNull(drCurrent("CauseCodeExt")) Then
                                drCurrent("CauseCodeExt") = "  "
                            ElseIf drCurrent("CauseCodeExt").ToString().Trim() = String.Empty Then
                                drCurrent("CauseCodeExt") = "  "
                            End If
                            parms(6, 1) = "CauseCodeExt, " + drCurrent("CauseCodeExt").ToString()
                            parms(7, 0) = "@WorkStarted"
                            parms(7, 1) = "WorkStarted, " + drCurrent("WorkStarted").ToString()
                            parms(8, 0) = "@WorkEnded"
                            parms(8, 1) = "WorkEnded, " + drCurrent("WorkEnded").ToString()
                            parms(9, 0) = "@ContribCode"
                            parms(9, 1) = "ContribCode, " + drCurrent("ContribCode").ToString()
                            parms(10, 0) = "@PrimaryAlert"
                            parms(10, 1) = "PrimaryAlert, " + drCurrent("PrimaryAlert").ToString()
                            parms(11, 0) = "@ManhoursWorked"
                            parms(11, 1) = "ManhoursWorked, " + drCurrent("ManhoursWorked").ToString()
                            parms(12, 0) = "@VerbalDesc1"
                            If IsDBNull(drCurrent("VerbalDesc1")) Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            If drCurrent("VerbalDesc1").ToString().Trim = String.Empty Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            parms(12, 1) = "VerbalDesc1, " + drCurrent("VerbalDesc1").ToString()
                            parms(13, 0) = "@EvenCardNumber"
                            parms(13, 1) = "EvenCardNumber, " + drCurrent("EvenCardNumber").ToString()
                            parms(14, 0) = "@VerbalDesc2"
                            parms(14, 1) = "VerbalDesc2, " + drCurrent("VerbalDesc2").ToString()
                            parms(15, 0) = "@RevisionCardOdd"
                            If drCurrent("RevisionCardOdd").ToString = "*" Then
                                parms(15, 1) = "RevisionCardOdd, 0"
                            Else
                                parms(15, 1) = "RevisionCardOdd, " + drCurrent("RevisionCardOdd").ToString()
                            End If
                            parms(16, 0) = "@RevMonthCardEven"
                            parms(16, 1) = "RevMonthCardEven, " + drCurrent("RevMonthCardEven").ToString()
                            parms(17, 0) = "@RevMonthCardOdd"
                            parms(17, 1) = "RevMonthCardOdd, " + drCurrent("RevMonthCardOdd").ToString()
                            parms(18, 0) = "@FailureMechCode"
                            parms(18, 1) = "FailureMechCode, " + drCurrent("FailureMechCode").ToString()
                            parms(19, 0) = "@TripMech"
                            parms(19, 1) = "TripMech, " + drCurrent("TripMech").ToString()
                            parms(20, 0) = "@CumFiredHours"
                            parms(20, 1) = "CumFiredHours, " + drCurrent("CumFiredHours").ToString()
                            parms(21, 0) = "@CumEngineStarts"
                            parms(21, 1) = "CumEngineStarts, " + drCurrent("CumEngineStarts").ToString()
                            parms(22, 0) = "@VerbalDesc86"
                            parms(22, 1) = "VerbalDesc86, " + drCurrent("VerbalDesc86").ToString()
                            parms(23, 0) = "@TimeStamp"
                            parms(23, 1) = "TimeStamp, " + dtUTCNow.ToString()
                            parms(24, 0) = "@UnitShortName"
                            If IsDBNull(drSetup.Item("UnitShortName")) Then
                                parms(24, 1) = "UnitShortName, UNKNOWN"
                            Else
                                parms(24, 1) = "UnitShortName, " + drSetup.Item("UnitShortName").ToString.Trim
                            End If

                            intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                        Else

                            ' *** The row exists so use UPDATE *** 

                            ' Loading up the Event 04 and even cards

                            System.Array.Clear(parms, 0, parms.Length)


                            parms(0, 0) = "@EventType"
                            parms(0, 1) = "EventType, " + drCurrent("EventType").ToString()
                            parms(1, 0) = "@RevisionCardEven"
                            parms(1, 1) = "RevisionCardEven, " + drCurrent("RevisionCardEven").ToString()
                            parms(2, 0) = "@CauseCode"
                            parms(2, 1) = "CauseCode, " + drCurrent("CauseCode").ToString()
                            parms(3, 0) = "@CauseCodeExt"
                            If IsDBNull(drCurrent("CauseCodeExt")) Then
                                drCurrent("CauseCodeExt") = "  "
                            ElseIf drCurrent("CauseCodeExt").ToString().Trim() = String.Empty Then
                                drCurrent("CauseCodeExt") = "  "
                            End If
                            parms(3, 1) = "CauseCodeExt, " + drCurrent("CauseCodeExt").ToString()
                            parms(4, 0) = "@WorkStarted"
                            parms(4, 1) = "WorkStarted, " + drCurrent("WorkStarted").ToString()
                            parms(5, 0) = "@WorkEnded"
                            parms(5, 1) = "WorkEnded, " + drCurrent("WorkEnded").ToString()
                            parms(6, 0) = "@ContribCode"
                            parms(6, 1) = "ContribCode, " + drCurrent("ContribCode").ToString()
                            parms(7, 0) = "@PrimaryAlert"
                            parms(7, 1) = "PrimaryAlert, " + drCurrent("PrimaryAlert").ToString()
                            parms(8, 0) = "@ManhoursWorked"
                            parms(8, 1) = "ManhoursWorked, " + drCurrent("ManhoursWorked").ToString()
                            parms(9, 0) = "@VerbalDesc1"
                            If IsDBNull(drCurrent("VerbalDesc1")) Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            If drCurrent("VerbalDesc1").ToString().Trim = String.Empty Then
                                drCurrent("VerbalDesc1") = "NA"
                            End If
                            parms(9, 1) = "VerbalDesc1, " + drCurrent("VerbalDesc1").ToString()
                            parms(10, 0) = "@EvenCardNumber"
                            parms(10, 1) = "EvenCardNumber, " + drCurrent("EvenCardNumber").ToString()
                            parms(11, 0) = "@RevMonthCardEven"
                            parms(11, 1) = "RevMonthCardEven, " + drCurrent("RevMonthCardEven").ToString()
                            parms(12, 0) = "@FailureMechCode"
                            parms(12, 1) = "FailureMechCode, " + drCurrent("FailureMechCode").ToString()
                            parms(13, 0) = "@TripMech"
                            parms(13, 1) = "TripMech, " + drCurrent("TripMech").ToString()
                            parms(14, 0) = "@CumFiredHours"
                            parms(14, 1) = "CumFiredHours, " + drCurrent("CumFiredHours").ToString()
                            parms(15, 0) = "@CumEngineStarts"
                            parms(15, 1) = "CumEngineStarts, " + drCurrent("CumEngineStarts").ToString()
                            parms(16, 0) = "@VerbalDesc86"
                            parms(16, 1) = "VerbalDesc86, " + drCurrent("VerbalDesc86").ToString()
                            parms(17, 0) = "@TimeStamp"
                            parms(17, 1) = "TimeStamp, " + dtUTCNow.ToString()

                            parms(18, 0) = "@Original_UtilityUnitCode"
                            parms(18, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(19, 0) = "@Original_Year"
                            parms(19, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(20, 0) = "@Original_EventNumber"
                            parms(20, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                            parms(21, 0) = "@Original_EvenCardNumber"
                            parms(21, 1) = "EvenCardNumber, " + drCurrent("EvenCardNumber").ToString()
                            parms(22, 0) = "@Original_RevisionCardEven"
                            parms(22, 1) = "RevisionCardEven, " + drCurrent("RevisionCardEven").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement04, parms)

                            ' Loading up the Event 05 and odd cards

                            System.Array.Clear(parms, 0, parms.Length)

                            parms(0, 0) = "@VerbalDesc2"
                            parms(0, 1) = "VerbalDesc2, " + drCurrent("VerbalDesc2").ToString()
                            parms(1, 0) = "@RevisionCardOdd"
                            parms(1, 1) = "RevisionCardOdd, " + drCurrent("RevisionCardOdd").ToString()
                            parms(2, 0) = "@RevMonthCardOdd"
                            parms(2, 1) = "RevMonthCardOdd, " + drCurrent("RevMonthCardOdd").ToString()
                            parms(3, 0) = "@VerbalDesc86"
                            parms(3, 1) = "VerbalDesc86, " + drCurrent("VerbalDesc86").ToString()
                            parms(4, 0) = "@TimeStamp"
                            parms(4, 1) = "TimeStamp, " + dtUTCNow.ToString()

                            parms(5, 0) = "@Original_UtilityUnitCode"
                            parms(5, 1) = "UtilityUnitCode, " + drCurrent("UtilityUnitCode").ToString()
                            parms(6, 0) = "@Original_Year"
                            parms(6, 1) = "Year, " + drCurrent("Year").ToString()
                            parms(7, 0) = "@Original_EventNumber"
                            parms(7, 1) = "EventNumber, " + drCurrent("EventNumber").ToString()
                            parms(8, 0) = "@Original_EvenCardNumber"
                            parms(8, 1) = "EvenCardNumber, " + drCurrent("EvenCardNumber").ToString()
                            parms(9, 0) = "@Original_RevisionCardOdd"
                            parms(9, 1) = "RevisionCardOdd, " + drCurrent("RevisionCardOdd").ToString()

                            intUpdateRows = Factory.ExecuteNonQuery(updateStatement05, parms)

                        End If

                    End If

                Next

                UpdateSetupCheckStatus(dsSetup)

                Return True

            Catch ex As System.Exception

                Me.ThrowGADSNGException("Error in LoadEvent0499", ex)
                Return False

            End Try

        End Function

#End Region


#Region " NumberOfUnits() as Integer "

        Public Function NumberOfUnits() As Integer

            Dim checkSetupStatement As String
            Dim intNoOfRows As Integer

            checkSetupStatement = "SELECT COUNT(*) FROM Setup"
            intNoOfRows = CInt(Factory.ExecuteScalar(checkSetupStatement, Nothing))

            Return intNoOfRows

        End Function

#End Region

#Region " InsertCustomEventData( ... ) As Boolean "

        Public Function InsertCustomEventData(ByVal UtilityUnitCode As String, _
                                      ByVal UnitShortName As String, _
                                      ByVal Year As Integer, _
                                      ByVal EventNumber As Integer, _
                                      ByVal CustomField1 As String, _
                                      ByVal CustomField2 As String, _
                                      ByRef CustomListbox As String, _
                                      ByVal CardNumber As Int16) As Boolean


            Dim deleteStatement As String = String.Empty
            Dim insertStatement As String = String.Empty
            Dim intUpdateRows As Integer = 0
            Dim parms(8, 2) As String

            Try

                parms(0, 0) = "@UtilityUnitCode"
                parms(0, 1) = "UtilityUnitCode, " + UtilityUnitCode

                parms(1, 0) = "@Year"
                parms(1, 1) = "Year, " + Year.ToString().Trim()

                parms(2, 0) = "@EventNumber"
                parms(2, 1) = "EventNumber, " + EventNumber.ToString().Trim()

                parms(3, 0) = "@CardNumber"
                parms(3, 1) = "CardNumber, " + CardNumber.ToString().Trim()

                deleteStatement = "DELETE FROM CustomEventData WHERE UtilityUnitCode = @UtilityUnitCode AND Year = @Year AND EventNumber = @EventNumber AND CardNumber = @CardNumber"
                intUpdateRows = Factory.ExecuteNonQuery(deleteStatement, parms)

                parms(4, 0) = "@UnitShortName"
                parms(4, 1) = "UnitShortName, " + UnitShortName

                parms(5, 0) = "@CustomField1"
                parms(5, 1) = "CustomField1, " + CustomField1

                parms(6, 0) = "@CustomField2"
                parms(6, 1) = "CustomField2, " + CustomField2

                parms(7, 0) = "@CustomListbox"
                parms(7, 1) = "CustomListbox, " + CustomListbox

                insertStatement = "INSERT INTO CustomEventData (UtilityUnitCode, Year, EventNumber, CardNumber, UnitShortName, CustomField1, CustomField2, CustomListbox) VALUES (@UtilityUnitCode, @Year, @EventNumber, @CardNumber, @UnitShortName, @CustomField1, @CustomField2, @CustomListbox)"
                intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                If intUpdateRows = 1 Then
                    Return True
                Else
                    Return False
                End If

            Catch e As System.Exception

                Me.ThrowGADSNGException("Error in InsertCustomEventData", e)

                Return False

            End Try

        End Function

#End Region


#Region " DeleteEvents(ByVal myList As IEnumerable) As Boolean "

        Public Function DeleteEvents(ByVal myList As IEnumerable) As Boolean

            Dim deleteStatement As String
            Dim intRows As Integer
            Dim myEnumerator As System.Collections.IEnumerator = myList.GetEnumerator

            Try

                While myEnumerator.MoveNext

                    deleteStatement = "DELETE FROM EventData01 WHERE " + myEnumerator.Current().ToString
                    intRows = Factory.ExecuteNonQuery(deleteStatement, Nothing)

                    deleteStatement = "DELETE FROM EventData02 WHERE " + myEnumerator.Current().ToString
                    intRows = Factory.ExecuteNonQuery(deleteStatement, Nothing)

                End While

                Return True

            Catch e As System.Exception
                Me.ThrowGADSNGException("Error in DeleteEvents", e)

                Return False

            End Try

        End Function

#End Region

#Region " DeleteEventRecords(ByVal myList As IEnumerable) As Boolean "

        Public Function DeleteEventRecords(ByVal myList As IEnumerable) As Boolean

            Dim updateStatement02 As String
            Dim intRows As Integer
            Dim myEnumerator As System.Collections.IEnumerator = myList.GetEnumerator
            Dim parms(13, 2) As String

            'System.Array.Clear(parms, 0, parms.Length)

            parms(0, 0) = "@CauseCode"
            parms(0, 1) = "CauseCode, 0"
            parms(1, 0) = "@CauseCodeExt"
            parms(1, 1) = "CauseCodeExt, "
            parms(2, 0) = "@WorkStarted"
            parms(2, 1) = "WorkStarted, "
            parms(3, 0) = "@WorkEnded"
            parms(3, 1) = "WorkEnded, "
            parms(4, 0) = "@ContribCode"
            parms(4, 1) = "ContribCode, 0"
            parms(5, 0) = "@PrimaryAlert"
            parms(5, 1) = "PrimaryAlert, "
            parms(6, 0) = "@ManhoursWorked"
            parms(6, 1) = "ManhoursWorked, "
            parms(7, 0) = "@VerbalDesc1"
            parms(7, 1) = "VerbalDesc1, NA"
            parms(8, 0) = "@RevisionCard02"
            parms(8, 1) = "RevisionCard02, X"
            parms(9, 0) = "@RevMonthCard02"
            parms(9, 1) = "RevMonthCard02, " & DateTime.UtcNow.ToString
            parms(10, 0) = "@VerbalDesc2"
            parms(10, 1) = "VerbalDesc2, "
            parms(11, 0) = "@RevisionCard03"
            parms(11, 1) = "RevisionCard03, X"
            parms(12, 0) = "@RevMonthCard03"
            parms(12, 1) = "RevMonthCard03, " & DateTime.UtcNow.ToString

            Try

                While myEnumerator.MoveNext

                    ' Deleting the Event 02 card

                    updateStatement02 = "UPDATE EventData01 SET " & _
                        "CauseCode = @CauseCode, " & _
                        "CauseCodeExt = @CauseCodeExt, " & _
                        "WorkStarted = @WorkStarted, " & _
                        "WorkEnded = @WorkEnded, " & _
                        "ContribCode = @ContribCode, " & _
                        "PrimaryAlert = @PrimaryAlert, " & _
                        "ManhoursWorked = @ManhoursWorked, " & _
                        "VerbalDesc1 = @VerbalDesc1, " & _
                        "RevisionCard02 = @RevisionCard02, " & _
                        "RevMonthCard02 = @RevMonthCard02, " & _
                        "VerbalDesc2 = @VerbalDesc2, " & _
                        "RevisionCard03 = @RevisionCard03, " & _
                        "RevMonthCard03 = @RevMonthCard03 " & _
                        "WHERE " & myEnumerator.Current().ToString

                    intRows = Factory.ExecuteNonQuery(updateStatement02, parms)

                End While

                Return True

            Catch e As System.Exception
                Me.ThrowGADSNGException("Error in DeleteEventRecords", e)

                Return False

            End Try

        End Function

#End Region

#Region " DeleteEvent03Records(ByVal myList As IEnumerable) As Boolean "

        Public Function DeleteEvent03Records(ByVal myList As IEnumerable) As Boolean

            Dim updateStatement03 As String
            Dim intRows As Integer
            Dim myEnumerator As System.Collections.IEnumerator = myList.GetEnumerator
            Dim parms(3, 2) As String

            'System.Array.Clear(parms, 0, parms.Length)

            parms(0, 0) = "@VerbalDesc2"
            parms(0, 1) = "VerbalDesc2, "
            parms(1, 0) = "@RevisionCard03"
            parms(1, 1) = "RevisionCard03, X"
            parms(2, 0) = "@RevMonthCard03"
            parms(2, 1) = "RevMonthCard03, " & DateTime.UtcNow.ToString

            Try

                While myEnumerator.MoveNext

                    ' Deleting the Event 03 card

                    updateStatement03 = "UPDATE EventData01 SET " & _
                                        "VerbalDesc2 = @VerbalDesc2, " & _
                                        "RevisionCard03 = @RevisionCard03, " & _
                                        "RevMonthCard03 = @RevMonthCard03 " & _
                                        "WHERE " & myEnumerator.Current().ToString

                    intRows = Factory.ExecuteNonQuery(updateStatement03, parms)

                End While

                Return True

            Catch e As System.Exception
                Me.ThrowGADSNGException("Error in DeleteEvent03Records", e)

                Return False

            End Try

        End Function

#End Region

#Region " DeleteEvent04Records(ByVal myList As IEnumerable) As Boolean "

        Public Function DeleteEvent04Records(ByVal myList As IEnumerable) As Boolean

            Dim updateStatement04 As String
            Dim intRows As Integer
            Dim myEnumerator As System.Collections.IEnumerator = myList.GetEnumerator
            Dim parms(13, 2) As String

            'System.Array.Clear(parms, 0, parms.Length)

            parms(0, 0) = "@CauseCode"
            parms(0, 1) = "CauseCode, 0"
            parms(1, 0) = "@CauseCodeExt"
            parms(1, 1) = "CauseCodeExt, "
            parms(2, 0) = "@WorkStarted"
            parms(2, 1) = "WorkStarted, "
            parms(3, 0) = "@WorkEnded"
            parms(3, 1) = "WorkEnded, "
            parms(4, 0) = "@ContribCode"
            parms(4, 1) = "ContribCode, 0"
            parms(5, 0) = "@PrimaryAlert"
            parms(5, 1) = "PrimaryAlert, "
            parms(6, 0) = "@ManhoursWorked"
            parms(6, 1) = "ManhoursWorked, "
            parms(7, 0) = "@VerbalDesc1"
            parms(7, 1) = "VerbalDesc1, NA"
            parms(8, 0) = "@RevisionCardEven"
            parms(8, 1) = "RevisionCardEven, X"
            parms(9, 0) = "@RevMonthCardEven"
            parms(9, 1) = "RevMonthCardEven, " & DateTime.UtcNow.ToString
            parms(10, 0) = "@VerbalDesc2"
            parms(10, 1) = "VerbalDesc2, "
            parms(11, 0) = "@RevisionCardOdd"
            parms(11, 1) = "RevisionCardOdd, X"
            parms(12, 0) = "@RevMonthCardOdd"
            parms(12, 1) = "RevMonthCardOdd, " & DateTime.UtcNow.ToString

            Try

                While myEnumerator.MoveNext

                    ' Deleting the even numbered Event cards between 04 and 99

                    updateStatement04 = "UPDATE EventData02 SET " & _
                                        "CauseCode = @CauseCode, " & _
                                        "CauseCodeExt = @CauseCodeExt, " & _
                                        "WorkStarted = @WorkStarted, " & _
                                        "WorkEnded = @WorkEnded, " & _
                                        "ContribCode = @ContribCode, " & _
                                        "PrimaryAlert = @PrimaryAlert, " & _
                                        "ManhoursWorked = @ManhoursWorked, " & _
                                        "VerbalDesc1 = @VerbalDesc1, " & _
                                        "RevisionCardEven = @RevisionCardEven, " & _
                                        "RevMonthCardEven = @RevMonthCardEven, " & _
                                        "VerbalDesc2 = @VerbalDesc2, " & _
                                        "RevisionCardOdd = @RevisionCardOdd, " & _
                                        "RevMonthCardOdd = @RevMonthCardOdd " & _
                                        "WHERE " & myEnumerator.Current().ToString

                    intRows = Factory.ExecuteNonQuery(updateStatement04, parms)

                End While

                Return True

            Catch e As System.Exception
                Me.ThrowGADSNGException("Error in DeleteEvent04Records", e)

                Return False

            End Try

        End Function

#End Region

#Region " DeleteEvent05Records(ByVal myList As IEnumerable) As Boolean "

        Public Function DeleteEvent05Records(ByVal myList As IEnumerable) As Boolean

            Dim updateStatement05 As String
            Dim intRows As Integer
            Dim myEnumerator As System.Collections.IEnumerator = myList.GetEnumerator
            Dim parms(3, 2) As String

            'System.Array.Clear(parms, 0, parms.Length)

            parms(0, 0) = "@VerbalDesc2"
            parms(0, 1) = "VerbalDesc2, "
            parms(1, 0) = "@RevisionCardOdd"
            parms(1, 1) = "RevisionCardOdd, X"
            parms(2, 0) = "@RevMonthCardOdd"
            parms(2, 1) = "RevMonthCardOdd, " & DateTime.UtcNow.ToString

            Try

                While myEnumerator.MoveNext

                    ' Deleting the odd numbered Event cards between 04 and 99

                    updateStatement05 = "UPDATE EventData02 SET " & _
                                        "VerbalDesc2 = @VerbalDesc2, " & _
                                        "RevisionCardOdd = @RevisionCardOdd, " & _
                                        "RevMonthCardOdd = @RevMonthCardOdd " & _
                                        "WHERE " & myEnumerator.Current().ToString

                    intRows = Factory.ExecuteNonQuery(updateStatement05, parms)

                End While

                Return True

            Catch e As System.Exception
                Me.ThrowGADSNGException("Error in DeleteEvent05Records", e)

                Return False

            End Try

        End Function

#End Region


#Region " GetPJMReader(ByVal stringWho As String) As IDataReader "

        Public Function GetPJMReader(ByVal stringWho As String, ByVal intYear As Integer, ByVal strStatus As String) As IDataReader

            Dim stringSelect As String = String.Empty
            Dim stringEndSelect As String = String.Empty

            Select Case strStatus
                Case "S"
                    stringEndSelect = " AND (PJMLoadStatus = 'S' OR PJMLoadStatus IS NULL OR PJMLoadStatus = ' ') AND Year = " & intYear.ToString & " ORDER BY Period DESC"
                Case "A"
                    stringEndSelect = " AND (PJMLoadStatus = 'S' OR PJMLoadStatus = 'R') AND Year = " & intYear.ToString & " ORDER BY Period DESC"
                Case "R"
                    stringEndSelect = " AND PJMLoadStatus = 'A' AND Year = " & intYear.ToString & " ORDER BY Period DESC"
                Case "F"
                    stringEndSelect = " AND PJMLoadStatus = 'A' AND Year = " & intYear.ToString & " ORDER BY Period DESC"
            End Select

            stringSelect = "SELECT Period FROM PerformanceData WHERE UtilityUnitCode = '" & stringWho & "'" & stringEndSelect

            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)

        End Function

#End Region

        Public Function AreTherePJMUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (PJM <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereNERCUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32

            Try
                ' this will set the InactiveHours to zero for all historical data records with this new field
                intRows = Factory.ExecuteNonQuery("UPDATE PerformanceData SET InactiveHours = 0 WHERE (InactiveHours IS NULL)", Nothing)
            Catch ex As Exception

            End Try

            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (NERC <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereNYISOUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (NYISO <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereISONEUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (ISONE <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereCEAUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (CEA <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereGEUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (GenElect <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereSiemensUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (Siemens <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereCAISOUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (CAISO <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereMISOUnits() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (MISO <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereISO1Units() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (ISO1 <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereISO2Units() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (ISO2 <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function AreThereISO3Units() As Boolean

            Dim stringSelect As String
            Dim intRows As Int32
            stringSelect = "SELECT COUNT(*) FROM Setup WHERE (ISO3 <> 0)"
            intRows = Convert.ToInt32(Factory.ExecuteScalar(stringSelect, Nothing))
            If intRows > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

#Region " GetPJMUnitData(ByVal strWho As String) As Setup.SetupDataTable "

        Public Function GetPJMUnitData(ByVal strWho As String, ByVal myUser As String) As Setup.SetupDataTable

            ' strWho can be one of the following PJM Load Status indicators:

            ' S - Which means that I'm creating (submitting) for the first time or resubmitting units that have NOT been marked as Accepted

            ' A - Which means that these units have been submitted and Accepted by PJM as valid data

            ' R - Which means that I am going to Revise a data record that has been Accepted by PJM as valid data

            ' F - This data has been marked as Final which generally occurs at System Lockout (20th day of the month following the data month)
            '     However, if the PJM Admin grants it this doesn't have to be set until the final acceptance

            Dim idaTest As DbDataAdapter = Nothing
            Dim stringSelect As String
            Dim intRows As Int32
            Dim intYear As Integer

            Dim parms(1, 2) As String
            ' params is structured as key = "SQL Parameter with @" and value = "SourceColumn, ParamValue"
            ' these must be Add-ed in the same order as the parameters in the stringSelect so that
            '    it will work with "?" parameters

            'stringSelect = "SELECT * FROM Setup WHERE (PJM = @PJM)"
            'parms(0, 0) = "@PJM"
            'parms(0, 1) = "PJM, true"

            'stringSelect += " ORDER BY UtilityUnitCode"

            If myUser.Trim.ToUpper <> "DASHBOARD" Then
                stringSelect = "SELECT * FROM Setup WHERE (PJM <> 0) AND UtilityUnitCode IN (SELECT UtilityUnitCode FROM NGUnitPerm WHERE GroupID IN (SELECT GroupID FROM NGUserToGroup WHERE LoginID = '" & myUser & "')) ORDER BY UtilityUnitCode"
            Else
                ' getting only those PJM units with OK status
                stringSelect = "SELECT * FROM Setup WHERE (PJM <> 0) AND CheckStatus = 'O' ORDER BY UnitName"
            End If

            Try
                idaTest = Factory.GetDataAdapter(stringSelect, Nothing)
            Catch ex As Exception

                If Factory.Provider = "SqlClient" Then

                    Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                    Try
                        idaTest = Factory.GetDataAdapter(tmpStatement, Nothing)
                    Catch e As Exception
                        Me.ThrowGADSNGException("Error in GetPJMUnitData: " & strWho & " - " & myUser, e)
                    End Try
                Else
                    Me.ThrowGADSNGException("Error in GetPJMUnitData: " & strWho & " - " & myUser, ex)
                End If

            End Try

            'Dim dsSetup As Setup
            Dim tblSetup As Setup.SetupDataTable
            tblSetup = New Setup.SetupDataTable

            'idaTest = Factory.GetDataAdapter(stringSelect, parms)

            Dim checkSetupStatement As String
            Dim intNoOfRows As Integer
            Dim strTemp As String
            Dim strSelectEnd As String = String.Empty
            Dim drPJM As Setup.SetupRow

            Dim parmsdates(2, 2) As String

            Try

                Try
                    idaTest.Fill(tblSetup)
                Catch ex As Exception

                    If Factory.Provider = "SqlClient" Then

                        Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                        Try
                            idaTest = Factory.GetDataAdapter(tmpStatement, Nothing)
                            idaTest.Fill(tblSetup)
                        Catch e As Exception
                            Me.ThrowGADSNGException("Error in GetPJMUnitData A: " & strWho & " - " & myUser, e)
                        End Try
                    Else
                        Me.ThrowGADSNGException("Error in GetPJMUnitData A: " & strWho & " - " & myUser, ex)
                    End If

                End Try

                intRows = tblSetup.Rows.Count()

                If intRows = 0 Then
                    ' there are no PJM units defined in the Setup Table
                    Return tblSetup
                End If

                Select Case strWho.ToUpper
                    Case "S"
                        ' I can only Submit new or revised records 
                        strSelectEnd = "' AND (PJMLoadStatus = 'S' OR PJMLoadStatus IS NULL OR PJMLoadStatus = ' ')"
                    Case "A"
                        ' I can only mark as Accepted the originally submitted data and any that were Revised
                        strSelectEnd = "' AND (PJMLoadStatus = 'S' OR PJMLoadStatus = 'R')"
                    Case "R"
                        ' I can only Revise records that PJM has already Accepted; otherwise they're re-submitted
                        strSelectEnd = "' AND PJMLoadStatus = 'A'"
                    Case "F"
                        ' I can only Finalize those records that have been Accepted; once Finalized they can never be reopened
                        ' I cannot finalize an Event Record that has an Open End of Event Date/Time
                        strSelectEnd = "' AND PJMLoadStatus = 'A'"
                    Case Else
                End Select

                For Each drPJM In tblSetup.Rows

                    strTemp = drPJM.UtilityUnitCode

                    checkSetupStatement = "SELECT COUNT(*) FROM PerformanceData WHERE UtilityUnitCode = '" & strTemp & strSelectEnd

                    intNoOfRows = CInt(Factory.ExecuteScalar(checkSetupStatement, Nothing))

                    If strWho.ToUpper = "F" Then

                        ' This means the open ended events will be marked as accepted only

                        checkSetupStatement = "SELECT COUNT(*) FROM EventData01 WHERE UtilityUnitCode = '" & strTemp & strSelectEnd & _
                                                " AND NOT EndDateTime IS NULL"

                    Else

                        checkSetupStatement = "SELECT COUNT(*) FROM EventData01 WHERE UtilityUnitCode = '" & strTemp & strSelectEnd & _
                                                " AND (StartDateTime BETWEEN @StartOfMonth AND @EndOfMonth OR (PJMLoadStatus = 'A' AND EndDateTime IS NULL))"

                        If Now.Month = 1 Then      ' do not convert to UtcNow
                            ' in January you will be doing the prior year's PJM data
                            intYear = (Now.Year - 1)        ' do not convert to UtcNow
                        Else
                            ' current month is February through December so work with current year
                            intYear = Now.Year        ' do not convert to UtcNow
                        End If

                        Dim dtStart As DateTime = DateTime.Parse("01/01/" & intYear.ToString & " 00:00")
                        Dim dtEnd As DateTime

                        If Thread.CurrentThread.CurrentCulture.Name.ToString = "en-US" Then
                            dtEnd = DateTime.Parse("12/31/" & intYear.ToString & " 23:59:59")
                        Else
                            dtEnd = DateTime.Parse("31/12/" & intYear.ToString & " 23:59:59")
                        End If

                        parmsdates(0, 0) = "@StartOfMonth"
                        parmsdates(0, 1) = "StartDateTime, " & dtStart.ToString
                        parmsdates(1, 0) = "@EndOfMonth"
                        parmsdates(1, 1) = "StartDateTime, " & dtEnd.ToString

                    End If

                    intNoOfRows += CInt(Factory.ExecuteScalar(checkSetupStatement, parmsdates))

                    checkSetupStatement = "SELECT COUNT(*) FROM EventData02 WHERE UtilityUnitCode = '" & strTemp & strSelectEnd

                    intNoOfRows += CInt(Factory.ExecuteScalar(checkSetupStatement, Nothing))

                    If intNoOfRows = 0 Then
                        drPJM.Delete()
                    End If

                Next

                tblSetup.AcceptChanges()
                ' at this point tblSetup has only the Applicable row set
                intRows = tblSetup.Rows.Count()

            Catch e As System.Exception
                Me.ThrowGADSNGException("Error in GetPJMUnitData", e)
                intRows = 0
            End Try

            Return tblSetup

        End Function

#End Region


#Region " GetCauseCodes() As DataSet "

        Public Function GetCauseCodes() As DataSet

            Dim stringSelect As String

            stringSelect = "SELECT * FROM CauseCodes ORDER BY Level0, Level1, Level2, Level3, Level4, Level5, CauseCode"

            Return Factory.GetDataSet(stringSelect, Nothing)

        End Function

#End Region

#Region " GetFailMech() As DataSet "

        Public Function GetFailMech() As DataSet

            Dim stringSelect As String

            stringSelect = "SELECT Code AS FAILUREMECHCODE, (Code + ' - ' + Description) AS FULLDESC FROM FailureMechanism"

            Return Factory.GetDataSet(stringSelect, Nothing)

        End Function

#End Region

        Public Function GetCustomListbox() As DataSet
            Dim stringSelect As String

            stringSelect = "SELECT ListboxValues FROM CustomListbox"

            Return Factory.GetDataSet(stringSelect, Nothing)
        End Function

#Region " GetSavedVerbDesc(ByVal stringWho As String) As DataSet "

        Public Function GetSavedVerbDesc(ByVal stringWho As String) As DataSet

            Dim stringSelect As String
            stringSelect = "SELECT Description FROM SavedVerbalDesc WHERE LoginID = '" & stringWho & "'"
            Return Factory.GetDataSet(stringSelect, Nothing)

        End Function

#End Region


#Region " UpdateSavedVerbDesc(ByVal stringWho As String, ByVal dsSavedVerbDesc As DataSet) As Boolean "

        Public Function UpdateSavedVerbDesc(ByVal stringWho As String, ByVal dsSavedVerbDesc As DataSet) As Boolean

            If dsSavedVerbDesc.HasChanges() Then

                dsSavedVerbDesc.AcceptChanges()

                Dim stringSelect As String
                Dim insertStatement As String
                Dim intUpdateRows As Integer
                Dim parms(2, 2) As String
                Dim drTemp As DataRow

                parms(0, 0) = "@LoginID"
                parms(0, 1) = "LoginID, " + stringWho

                stringSelect = "DELETE FROM SavedVerbalDesc WHERE (LoginID = @LoginID)"
                intUpdateRows = Factory.ExecuteNonQuery(stringSelect, parms)

                insertStatement = "INSERT INTO SavedVerbalDesc (LoginID, Description) VALUES (@LoginID, @Description)"

                parms(1, 0) = "@Description"

                For Each drTemp In dsSavedVerbDesc.Tables(0).Rows
                    parms(1, 1) = "Description, " + drTemp("Description").ToString
                    intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)
                Next

            End If

            Return True

        End Function

#End Region

        Public Function UpdateNERCXRef(ByVal dsNERCXRef As DataSet) As Boolean

            If dsNERCXRef.HasChanges Then

                ' "SELECT UnitShortName, UnitName, NERCClassID, GADSMethod FROM NERCXRef ORDER BY UnitShortName"
                Dim stringSelect As String
                Dim insertStatement As String
                Dim intUpdateRows As Integer
                Dim parms(4, 2) As String
                Dim drTemp As DataRow

                parms(0, 0) = "@UnitShortName"
                parms(1, 0) = "@UnitName"
                parms(2, 0) = "@NERCClassID"
                parms(3, 0) = "@GADSMethod"

                stringSelect = "DELETE FROM NERCXRef"
                intUpdateRows = Factory.ExecuteNonQuery(stringSelect, Nothing)

                insertStatement = "INSERT INTO NERCXRef (UnitShortName, UnitName, NERCClassID, GADSMethod) VALUES (@UnitShortName, @UnitName, @NERCClassID, @GADSMethod)"

                For Each drTemp In dsNERCXRef.Tables(0).Rows

                    If Not IsDBNull(drTemp("UnitShortName")) Then

                        If drTemp("UnitShortName").ToString.Trim <> String.Empty Then

                            parms(0, 1) = "UnitShortName, " + drTemp("UnitShortName").ToString

                            If IsDBNull(drTemp("UnitName")) Then
                                drTemp("UnitName") = String.Empty
                            End If

                            If IsDBNull(drTemp("NERCClassID")) Then
                                drTemp("NERCClassID") = -1
                            End If

                            If Convert.ToInt16(drTemp("NERCClassID")) > 32000 Or Convert.ToInt16(drTemp("NERCClassID")) < -1 Then
                                drTemp("NERCClassID") = -1
                            End If

                            If IsDBNull(drTemp("GADSMethod")) Then
                                drTemp("GADSMethod") = 2
                            End If

                            If Convert.ToInt16(drTemp("GADSMethod")) > 2 Or Convert.ToInt16(drTemp("GADSMethod")) < 0 Then
                                drTemp("GADSMethod") = 2
                            End If

                            parms(1, 1) = "UnitName, " + drTemp("UnitName").ToString.Trim
                            parms(2, 1) = "NERCClassID, " + drTemp("NERCClassID").ToString
                            parms(3, 1) = "GADSMethod, " + drTemp("GADSMethod").ToString

                            intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                        End If

                    End If

                Next

            End If

            Return True

        End Function

        Public Function UpdateNERCClasses(ByVal dsNERCClasses As DataSet) As Boolean

            If dsNERCClasses.HasChanges Then

                ' "SELECT NERCClassID, ClassDesc, CCF, CEFORd FROM NERClasses ORDER BY NERCClassID"
                Dim stringSelect As String
                Dim insertStatement As String
                Dim intUpdateRows As Integer
                Dim parms(4, 2) As String
                Dim drTemp As DataRow

                parms(0, 0) = "@NERCClassID"
                parms(1, 0) = "@ClassDesc"
                parms(2, 0) = "@CCF"
                parms(3, 0) = "@CEFORd"

                stringSelect = "DELETE FROM NERCClasses"
                intUpdateRows = Factory.ExecuteNonQuery(stringSelect, Nothing)

                insertStatement = "INSERT INTO NERCClasses (NERCClassID, ClassDesc, CCF, CEFORd) VALUES (@NERCClassID, @ClassDesc, @CCF, @CEFORd)"

                For Each drTemp In dsNERCClasses.Tables(0).Rows

                    If Not IsDBNull(drTemp("NERCClassID")) Then

                        If drTemp("NERCClassID").ToString.Trim <> String.Empty Then

                            If Convert.ToInt16(drTemp("NERCClassID")) < 0 Or Convert.ToInt16(drTemp("NERCClassID")) > 32000 Then
                                drTemp("NERCClassID") = 0
                            End If

                            If Convert.ToInt16(drTemp("NERCClassID")) <> 0 Then

                                parms(0, 1) = "NERCClassID, " + drTemp("NERCClassID").ToString

                                If IsDBNull(drTemp("ClassDesc")) Then
                                    drTemp("ClassDesc") = String.Empty
                                End If

                                If IsDBNull(drTemp("CCF")) Then
                                    drTemp("CCF") = 0.0
                                End If

                                If Convert.ToDecimal(drTemp("CCF")) > 1.0 Or Convert.ToDecimal(drTemp("CCF")) < 0.0 Then
                                    drTemp("CCF") = 0.0
                                End If

                                If IsDBNull(drTemp("CEFORd")) Then
                                    drTemp("CEFORd") = 0.0
                                End If

                                If Convert.ToDecimal(drTemp("CEFORd")) > 1.0 Or Convert.ToDecimal(drTemp("CEFORd")) < 0.0 Then
                                    drTemp("CEFORd") = 0.0
                                End If

                                parms(1, 1) = "ClassDesc, " + drTemp("ClassDesc").ToString.Trim
                                parms(2, 1) = "CCF, " + drTemp("CCF").ToString
                                parms(3, 1) = "CEFORd, " + drTemp("CEFORd").ToString

                                intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                            End If

                        End If

                    End If

                Next

            End If

            Return True

        End Function

        Public Function UpdateOFCalcResults(ByVal insertStatement As String) As Integer
            Dim intUpdateRows As Integer
            intUpdateRows = Factory.ExecuteNonQuery(insertStatement, Nothing)
            Return intUpdateRows
        End Function

        Public Function GetADataset(ByVal strSelect As String, ByVal parms As Array) As DataSet
            Dim dsData As DataSet
            dsData = Factory.GetDataSet(strSelect, parms)
            Return dsData
        End Function

        Public Function DeleteRecords(ByVal strDelete As String) As Integer
            Dim i As Integer
            i = Factory.ExecuteNonQuery(strDelete, Nothing)
            Return i
        End Function

        Public Function NERCXRefReader() As IDataReader
            Dim stringSelect As String
            stringSelect = "SELECT UnitShortName, UnitName, NERCClassID, GADSMethod FROM NERCXRef ORDER BY UnitShortName"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)
        End Function

        Public Function NERCClassesReader() As IDataReader
            Dim stringSelect As String
            stringSelect = "SELECT NERCClassID, CCF, CEFORd, ClassDesc, EffectiveDate FROM NERCClasses ORDER BY NERCClassID, EffectiveDate DESC"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)
        End Function

        Public Function GetAReader(ByVal stringSelect As String) As IDataReader
            Try
                Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function EFORdCalcResultsReader() As IDataReader
            Dim stringSelect As String
            stringSelect = "SELECT UtilityUnitCode, ISTge, FL_Numerator, FL_Denominator, ISTbefore, CEFORd, ISTMissing, EFORdgm, DateRange FROM EFORdCalcResults ORDER BY UtilityUnitCode"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)
        End Function

        Public Function OFCalcResultsReader() As IDataReader
            Dim stringSelect As String
            stringSelect = "SELECT UtilityUnitCode, ISTge, CFNumerator, CFDenominator, ISTbefore, CCF, ISTMissing, OFgm, DateRange FROM OFCalcResults ORDER BY UtilityUnitCode"
            Return Me.Factory.ExecuteDataReader(stringSelect, Nothing, CommandBehavior.CloseConnection)
        End Function

        Public Function CountMissing(ByVal strSQL As String) As Integer

            Dim myObj As Object

            myObj = Me.Factory.ExecuteScalar(strSQL, Nothing)

            If Not IsNothing(myObj) Then

                If Not IsDBNull(myObj) Then

                    If IsNumeric(myObj) Then
                        Return Convert.ToInt32(myObj)
                    Else
                        Return 0
                    End If

                Else
                    Return 0
                End If

            Else
                Return 0
            End If

            Return 0

        End Function

        Public Function CountMissingMonth(ByVal strSQL As String) As Integer

            Dim myObj As Object
            Dim intMonth As Integer
            Dim dtDate As DateTime

            myObj = Me.Factory.ExecuteScalar(strSQL, Nothing)

            If Not IsNothing(myObj) Then

                If Not IsDBNull(myObj) Then

                    Try
                        dtDate = Convert.ToDateTime(myObj)
                        intMonth = dtDate.Month
                    Catch ex As Exception
                        intMonth = 0
                    End Try

                    Return intMonth

                Else
                    Return 0
                End If

            Else
                Return 0
            End If

            Return 0

        End Function

        Public Sub LoadToPerfData(ByVal dsLoad As DataSet)

            Dim selectStatement As String
            Dim insertStatement As String
            Dim drCurrent As DataRow
            Dim parms(21, 2) As String
            Dim parmsD(3, 2) As String
            Dim intUpdateRows As Integer

            selectStatement = "DELETE FROM PerfData " & _
                              "WHERE UnitShortName = @UnitShortName AND PerfYear = @PerfYear AND ReportPeriod = @ReportPeriod"

            insertStatement = "INSERT INTO PerfData (UnitShortName, PerfYear, ReportPeriod, RevisionCode, " & _
                "NMC, NDC, NAGen, Numerator, Denominator, CFForMonth, ServiceHours, RSHours, " & _
                "PumpingHours, SynchCondHours, AvailableHours, POHours, FOHours, MOHours, " & _
                "SEHours, UnavailableHours, PeriodHours) " & _
                "VALUES " & _
                "(@UnitShortName, @PerfYear, @ReportPeriod, @RevisionCode, @NMC, @NDC, @NAGen, " & _
                "@Numerator, @Denominator, @CFForMonth, @ServiceHours, @RSHours, @PumpingHours, @SynchCondHours, @AvailableHours, @POHours, " & _
                "@FOHours, @MOHours, @SEHours, @UnavailableHours, @PeriodHours)"

            Try

                For Each drCurrent In dsLoad.Tables(0).Rows

                    'Application.DoEvents()

                    parmsD(0, 0) = "@UnitShortName"
                    parmsD(0, 1) = "UnitShortName, " + drCurrent("UnitShortName").ToString()
                    parmsD(1, 0) = "@PerfYear"
                    parmsD(1, 1) = "PerfYear, " + drCurrent("PerfYear").ToString()
                    parmsD(2, 0) = "@ReportPeriod"
                    parmsD(2, 1) = "ReportPeriod, " + drCurrent("ReportPeriod").ToString()

                    intUpdateRows = Factory.ExecuteNonQuery(selectStatement, parmsD)

                    parms(0, 0) = "@UnitShortName"
                    parms(0, 1) = "UnitShortName, " + drCurrent("UnitShortName").ToString()
                    parms(1, 0) = "@PerfYear"
                    parms(1, 1) = "PerfYear, " + drCurrent("PerfYear").ToString()
                    parms(2, 0) = "@ReportPeriod"
                    parms(2, 1) = "ReportPeriod, " + drCurrent("ReportPeriod").ToString()
                    parms(3, 0) = "@RevisionCode"
                    parms(3, 1) = "RevisionCode, " + drCurrent("RevisionCode").ToString()

                    parms(4, 0) = "@NMC"
                    parms(4, 1) = "NMC, " + drCurrent("NMC").ToString()
                    parms(5, 0) = "@NDC"
                    parms(5, 1) = "NDC, " + drCurrent("NDC").ToString()
                    parms(6, 0) = "@NAGen"
                    parms(6, 1) = "NAGen, " + drCurrent("NAGen").ToString()

                    parms(7, 0) = "@Numerator"
                    parms(7, 1) = "Numerator, " + drCurrent("Numerator").ToString()
                    parms(8, 0) = "@Denominator"
                    parms(8, 1) = "Denominator, " + drCurrent("Denominator").ToString()
                    parms(9, 0) = "@CFForMonth"
                    parms(9, 1) = "CFForMonth, " + drCurrent("CFForMonth").ToString()

                    parms(10, 0) = "@ServiceHours"
                    parms(10, 1) = "ServiceHours, " + drCurrent("ServiceHours").ToString()
                    parms(11, 0) = "@RSHours"
                    parms(11, 1) = "RSHours, " + drCurrent("RSHours").ToString()
                    parms(12, 0) = "@PumpingHours"
                    parms(12, 1) = "PumpingHours, " + drCurrent("PumpingHours").ToString()
                    parms(13, 0) = "@SynchCondHours"
                    parms(13, 1) = "SynchCondHours, " + drCurrent("SynchCondHours").ToString()
                    parms(14, 0) = "@AvailableHours"
                    parms(14, 1) = "AvailableHours, " + drCurrent("AvailableHours").ToString()

                    parms(15, 0) = "@POHours"
                    parms(15, 1) = "POHours, " + drCurrent("POHours").ToString()
                    parms(16, 0) = "@FOHours"
                    parms(16, 1) = "FOHours, " + drCurrent("FOHours").ToString()
                    parms(17, 0) = "@MOHours"
                    parms(17, 1) = "MOHours, " + drCurrent("MOHours").ToString()
                    parms(18, 0) = "@SEHours"
                    parms(18, 1) = "SEHours, " + drCurrent("SEHours").ToString()
                    parms(19, 0) = "@UnavailableHours"
                    parms(19, 1) = "UnavailableHours, " + drCurrent("UnavailableHours").ToString()

                    parms(20, 0) = "@PeriodHours"
                    parms(20, 1) = "PeriodHours, " + drCurrent("PeriodHours").ToString()

                    intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                Next

            Catch e As System.Exception

                Me.ThrowGADSNGException("Error in LoadToPerfData", e)

            End Try

        End Sub

        Public Sub LoadToPerfDataFromPortal(ByVal dsLoad As DataSet, ByVal strUnitShortName As String)

            Dim selectStatement As String
            Dim insertStatement As String
            Dim drCurrent As DataRow
            Dim parms(21, 2) As String
            Dim intUpdateRows As Integer

            selectStatement = "DELETE FROM PerfData WHERE UnitShortName = '" & strUnitShortName.Trim & "'"
            intUpdateRows = Factory.ExecuteNonQuery(selectStatement, Nothing)

            insertStatement = "INSERT INTO PerfData (UnitShortName, PerfYear, ReportPeriod, RevisionCode, " & _
                "NMC, NDC, NAGen, Numerator, Denominator, CFForMonth, ServiceHours, RSHours, " & _
                "PumpingHours, SynchCondHours, AvailableHours, POHours, FOHours, MOHours, " & _
                "SEHours, UnavailableHours, PeriodHours) " & _
                "VALUES " & _
                "(@UnitShortName, @PerfYear, @ReportPeriod, @RevisionCode, @NMC, @NDC, @NAGen, " & _
                "@Numerator, @Denominator, @CFForMonth, @ServiceHours, @RSHours, @PumpingHours, @SynchCondHours, @AvailableHours, @POHours, " & _
                "@FOHours, @MOHours, @SEHours, @UnavailableHours, @PeriodHours)"

            Try

                For Each drCurrent In dsLoad.Tables(0).Rows

                    'Application.DoEvents()

                    parms(0, 0) = "@UnitShortName"
                    parms(0, 1) = "UnitShortName, " + drCurrent("UnitShortName").ToString()
                    parms(1, 0) = "@PerfYear"
                    parms(1, 1) = "PerfYear, " + drCurrent("PerfYear").ToString()
                    parms(2, 0) = "@ReportPeriod"
                    parms(2, 1) = "ReportPeriod, " + drCurrent("ReportPeriod").ToString()
                    parms(3, 0) = "@RevisionCode"
                    parms(3, 1) = "RevisionCode, " + drCurrent("RevisionCode").ToString()

                    parms(4, 0) = "@NMC"
                    parms(4, 1) = "NMC, " + drCurrent("NMC").ToString()
                    parms(5, 0) = "@NDC"
                    parms(5, 1) = "NDC, " + drCurrent("NDC").ToString()
                    parms(6, 0) = "@NAGen"
                    parms(6, 1) = "NAGen, " + drCurrent("NAGen").ToString()

                    parms(7, 0) = "@Numerator"
                    parms(7, 1) = "Numerator, " + drCurrent("Numerator").ToString()
                    parms(8, 0) = "@Denominator"
                    parms(8, 1) = "Denominator, " + drCurrent("Denominator").ToString()
                    parms(9, 0) = "@CFForMonth"
                    parms(9, 1) = "CFForMonth, " + drCurrent("CFForMonth").ToString()

                    parms(10, 0) = "@ServiceHours"
                    parms(10, 1) = "ServiceHours, " + drCurrent("ServiceHours").ToString()
                    parms(11, 0) = "@RSHours"
                    parms(11, 1) = "RSHours, " + drCurrent("RSHours").ToString()
                    parms(12, 0) = "@PumpingHours"
                    parms(12, 1) = "PumpingHours, " + drCurrent("PumpingHours").ToString()
                    parms(13, 0) = "@SynchCondHours"
                    parms(13, 1) = "SynchCondHours, " + drCurrent("SynchCondHours").ToString()
                    parms(14, 0) = "@AvailableHours"
                    parms(14, 1) = "AvailableHours, " + drCurrent("AvailableHours").ToString()

                    parms(15, 0) = "@POHours"
                    parms(15, 1) = "POHours, " + drCurrent("POHours").ToString()
                    parms(16, 0) = "@FOHours"
                    parms(16, 1) = "FOHours, " + drCurrent("FOHours").ToString()
                    parms(17, 0) = "@MOHours"
                    parms(17, 1) = "MOHours, " + drCurrent("MOHours").ToString()
                    parms(18, 0) = "@SEHours"
                    parms(18, 1) = "SEHours, " + drCurrent("SEHours").ToString()
                    parms(19, 0) = "@UnavailableHours"
                    parms(19, 1) = "UnavailableHours, " + drCurrent("UnavailableHours").ToString()

                    parms(20, 0) = "@PeriodHours"
                    parms(20, 1) = "PeriodHours, " + drCurrent("PeriodHours").ToString()

                    intUpdateRows = Factory.ExecuteNonQuery(insertStatement, parms)

                Next

            Catch e As System.Exception

                Me.ThrowGADSNGException("Error in LoadToPerfDataFromPortal", e)

            End Try

        End Sub


#Region " GetCauseCodeExt() As DataSet "

        Public Function GetCauseCodeExt() As DataSet

            Dim stringSelect As String

            stringSelect = "SELECT CODE, (Code + ' - ' + CodeDescription) AS FULLDESC, StartingCauseCode, EndingCauseCode FROM CauseCodeExtensions ORDER BY Code"

            Return Factory.GetDataSet(stringSelect, Nothing)

        End Function

#End Region

        Public Function DoesTableExist(ByVal tableName As String) As Boolean
            Dim boolReturn As Boolean = False
            If Factory.DoesTableExist(tableName) > 0 Then
                boolReturn = True
            Else
                boolReturn = False
            End If
            Return boolReturn
        End Function

        'Function GetPerformanceData(strUnit As String, intYear As Short) As Performance.PerformanceDataDataTable
        '    Throw New NotImplementedException
        'End Function

#Region " Get/Set Analysis Settings "

        Public Function GetAnalysisSettingInt(ByVal SectionName As String, ByVal KeyName As String, ByVal DefaultValue As Integer) As Integer
            Dim iKeyValue As Integer
            Dim sKeyValue As String

            sKeyValue = _GetAnalysisSetting(SectionName, KeyName)

            If sKeyValue = NOTFOUND Then
                iKeyValue = DefaultValue
            Else
                Try
                    iKeyValue = CType(sKeyValue, Integer)
                Catch ex As Exception
                    ' return zero if non-integer value found
                    iKeyValue = 0
                End Try
            End If

            Return iKeyValue

        End Function

        Public Function GetAnalysisSettingStr(ByVal SectionName As String, ByVal KeyName As String, ByVal DefaultValue As String) As String

            Dim sKeyValue As String
            sKeyValue = _GetAnalysisSetting(SectionName, KeyName)

            If sKeyValue = NOTFOUND Then
                sKeyValue = DefaultValue
            Else
                Try
                    sKeyValue = CType(sKeyValue, String)
                Catch ex As Exception
                    ' return zero if non-integer value found
                    sKeyValue = String.Empty
                End Try
            End If

            Return sKeyValue

        End Function

        Public Sub SaveAnalysisSettingStr(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As String)
            _SaveAnalysisSetting(SectionName, KeyName, Setting)
        End Sub

        Public Sub SaveAnalysisSettingInt(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As Integer)
            Dim sSetting As String
            sSetting = CType(Setting, String)
            _SaveAnalysisSetting(SectionName, KeyName, sSetting)
        End Sub

        Private Sub _SaveAnalysisSetting(ByVal SectionName As String, ByVal KeyName As String, ByVal Setting As String)

            'Dim sKeyValue As String
            Dim oKeyValue As Object

            If SectionName Is Nothing Then
                Exit Sub
            End If

            If KeyName Is Nothing Then
                Exit Sub
            End If

            If SectionName.Trim = String.Empty Then
                Exit Sub
            End If

            If KeyName.Trim = String.Empty Then
                Exit Sub
            End If

            If Setting Is Nothing Then
                Setting = String.Empty
            End If

            Dim parms(3, 2) As String
            parms(0, 0) = "@SectionName"
            parms(0, 1) = "SectionName, " + SectionName.Trim

            parms(1, 0) = "@KeyName"
            parms(1, 1) = "KeyName, " + KeyName.Trim

            Dim stringSelect As String

            stringSelect = "DELETE FROM AnalysisSettings WHERE (SectionName = @SectionName) AND (KeyName = @KeyName)"
            oKeyValue = Me.Factory.ExecuteNonQuery(stringSelect, parms)

            parms(2, 0) = "@Value"
            parms(2, 1) = "Value, " + Setting.Trim

            stringSelect = "INSERT INTO AnalysisSettings (SectionName, KeyName, Value) " & _
                     "VALUES (@SectionName, @KeyName, @Value)"

            Try
                oKeyValue = Me.Factory.ExecuteNonQuery(stringSelect, parms)
            Catch ex As Exception

            End Try

        End Sub

        Private Function _GetAnalysisSetting(ByVal SectionName As String, ByVal KeyName As String) As String

            Dim sKeyValue As String
            Dim oKeyValue As Object

            If SectionName Is Nothing Then
                Return NOTFOUND
            End If

            If KeyName Is Nothing Then
                Return NOTFOUND
            End If

            If SectionName.Trim = String.Empty Then
                Return NOTFOUND
            End If

            If KeyName.Trim = String.Empty Then
                Return NOTFOUND
            End If

            Dim parms(2, 2) As String
            parms(0, 0) = "@SectionName"
            parms(0, 1) = "SectionName, " + SectionName.Trim

            parms(1, 0) = "@KeyName"
            parms(1, 1) = "KeyName, " + KeyName.Trim

            Dim stringSelect As String
            stringSelect = "SELECT Value FROM AnalysisSettings WHERE (SectionName = @SectionName) AND (KeyName = @KeyName)"
            oKeyValue = Me.Factory.ExecuteScalar(stringSelect, parms)

            If oKeyValue Is Nothing Then
                Return NOTFOUND
            Else
                If IsDBNull(oKeyValue) Then
                    Return NOTFOUND
                Else
                    sKeyValue = oKeyValue.ToString
                End If
            End If

            Return sKeyValue

        End Function

#End Region

    End Class

    Public Class ServiceHrMethod

        Private myEffStartDate As DateTime
        Private myEffEndDate As DateTime
        Private myServiceHourMethod As Integer
        Private myUtilityUnitCode As String

        Public Sub New(ByVal UtilityUnitCode As String, ByVal EffStartDate As DateTime, ByVal EffEndDate As DateTime, ByVal ServiceHourMethod As Integer)
            Me.myEffStartDate = EffStartDate
            Me.myEffEndDate = EffEndDate
            Me.myServiceHourMethod = ServiceHourMethod
            Me.myUtilityUnitCode = UtilityUnitCode
        End Sub

        Public ReadOnly Property UtilityUnitCode() As String
            Get
                Return myUtilityUnitCode
            End Get
        End Property

        Public ReadOnly Property EffStartDate() As DateTime
            Get
                Return myEffStartDate
            End Get
        End Property

        Public ReadOnly Property EffEndDate() As DateTime
            Get
                Return myEffEndDate
            End Get
        End Property

        Public ReadOnly Property ServiceHourMethod() As Integer
            Get
                Return myServiceHourMethod
            End Get
        End Property

    End Class

End Namespace

'178 matches found 
