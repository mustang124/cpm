'********************************************************************************
' LanguageBuilder.vb

'Copyright (C) 2011  The Outercurve Foundation

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; either version 2
'of the License, or (at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

' Ron Fluegge
' GADS Open Source Project Coordinator
' Coordinator@GADSOpenSource.com
' 972-625-5653                                                  
'********************************************************************************

Imports System.Collections.Specialized
Imports System.Threading
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Xml              'for XmlTextReader
'Imports System.Environment      'for CurrentDirectory
Imports System.IO


Public Class LanguageBuilder

#Region "Private Members"

    Private Shared _lngID As Long
    Private Shared _CodeSource As String = ""
    Private Shared _dict As System.Collections.Specialized.StringDictionary
    Private Shared XMLFileName As String = "Languageen-US.xml"
    Private Shared m_xmlDocument As XmlDocument
    Private Shared _ServerPath As String = ""

#End Region

#Region "Public Methods"

    Public Shared Function BuildDictionary(ByVal strDebug As String, ByVal strServerMapPath As String) As HybridDictionary

        _ServerPath = strServerMapPath

        Return BuildDictionary(strDebug)

    End Function

    Public Shared Function BuildDictionary(ByVal strDebug As String) As HybridDictionary

        Dim myD As HybridDictionary
        myD = New HybridDictionary

        Dim boolIndex As Boolean = False

        If IsNothing(strDebug) Then
            strDebug = "False"
        Else
            If strDebug.Trim.ToUpper <> "TRUE" And strDebug.Trim.ToUpper <> "FALSE" Then
                strDebug = "False"
            End If
        End If

        boolIndex = Convert.ToBoolean(strDebug)

        Dim strMyLanguage As String = Thread.CurrentThread.CurrentCulture.Name.ToString

        If strMyLanguage.StartsWith("en") Then

            Select Case strMyLanguage
                Case "en-US"
                    strMyLanguage = "en-US"
                Case "en-AU"
                    strMyLanguage = "en-US"
                Case "en-GB"
                    strMyLanguage = "en-US"
                Case Else
                    strMyLanguage = "en-US"
            End Select

        ElseIf strMyLanguage.StartsWith("es") Then

            Select Case strMyLanguage
                Case "es-MX"
                    strMyLanguage = "es-MX"
                Case Else
                    strMyLanguage = "es-MX"
            End Select

        Else
            strMyLanguage = "en-US"
        End If

        If String.IsNullOrEmpty(_ServerPath) Then
            XMLFileName = "Language" & strMyLanguage & ".xml"
        Else
            If _ServerPath.EndsWith("\") Then
                XMLFileName = _ServerPath & "Secure\Language" & strMyLanguage & ".xml"
            Else
                XMLFileName = _ServerPath & "\Secure\Language" & strMyLanguage & ".xml"
            End If
        End If


        Dim myreader As XmlTextReader = Nothing

        Try
            myreader = New XmlTextReader(XMLFileName)
            myreader.WhitespaceHandling = WhitespaceHandling.None
            m_xmlDocument = New XmlDocument

            m_xmlDocument.Load(myreader)

        Catch e As System.Exception

            ExceptionManager.Publish(e)

        Finally

            If Not (myreader Is Nothing) Then
                myreader.Close()
                myreader = Nothing
            End If

        End Try

        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        'execute reader and populate dictionary
        Try

            m_nodelist = m_xmlDocument.SelectNodes("/NewDataSet/Table")

            Try
                Dim k As Integer = 0

                For Each m_node In m_nodelist

                    Dim kObj As Object = m_node.ChildNodes.Item(0).InnerText
                    Dim strAddToDB As String = Convert.ToString(m_node.ChildNodes.Item(1).InnerText)
                    Dim strAddTrimmed As String = strAddToDB.Trim
                    Dim LastChar As String
                    If strAddToDB.Length > 1 And strAddTrimmed.Length >= 1 Then
                        LastChar = strAddToDB.Trim.Chars((strAddToDB.Trim.Length) - 1)
                    Else
                        LastChar = ""
                    End If

                    k = CInt(kObj)
                    If boolIndex Then

                        ' This displays the Language dictionary "index number"
                        ' if DisplayString | Index is set to "True", then display the index number in addition to the text
                        myD.Add(k, m_node.ChildNodes.Item(0).InnerText & "-" & m_node.ChildNodes.Item(1).InnerText.TrimEnd)

                    Else

                        If IsAlpha(LastChar) Then
                            myD.Add(k, m_node.ChildNodes.Item(1).InnerText.TrimEnd)
                        Else
                            myD.Add(k, m_node.ChildNodes.Item(1).InnerText)
                        End If

                    End If

                Next

            Catch ex As System.Exception
                ExceptionManager.Publish(ex)
            End Try

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try

        Return myD

    End Function

    Public Shared Function BuildDictionary(ByVal strDebug As String, ByVal info As Stream) As HybridDictionary

        Dim myD As HybridDictionary
        myD = New HybridDictionary

        Dim boolIndex As Boolean = False

        If IsNothing(strDebug) Then
            strDebug = "False"
        Else
            If strDebug.Trim.ToUpper <> "TRUE" And strDebug.Trim.ToUpper <> "FALSE" Then
                strDebug = "False"
            End If
        End If

        boolIndex = Convert.ToBoolean(strDebug)


        Dim myreader As StreamReader = Nothing
        Try
            myreader = New StreamReader(info)
            m_xmlDocument = New XmlDocument

            m_xmlDocument.Load(myreader)

        Catch e As System.Exception

            ExceptionManager.Publish(e)

        Finally

            If Not (myreader Is Nothing) Then
                myreader.Close()
                myreader = Nothing
            End If

        End Try

        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode
        'execute reader and populate dictionary
        Try

            m_nodelist = m_xmlDocument.SelectNodes("/NewDataSet/Table")

            Try
                Dim k As Integer = 0

                For Each m_node In m_nodelist

                    Dim kObj As Object = m_node.ChildNodes.Item(0).InnerText
                    Dim strAddToDB As String = Convert.ToString(m_node.ChildNodes.Item(1).InnerText)
                    Dim strAddTrimmed As String = strAddToDB.Trim
                    Dim LastChar As String
                    If strAddToDB.Length > 1 And strAddTrimmed.Length >= 1 Then
                        LastChar = strAddToDB.Trim.Chars((strAddToDB.Trim.Length) - 1)
                    Else
                        LastChar = ""
                    End If

                    k = CInt(kObj)
                    If boolIndex Then

                        ' This displays the Language dictionary "index number"
                        ' if DisplayString | Index is set to "True", then display the index number in addition to the text
                        myD.Add(k, m_node.ChildNodes.Item(0).InnerText & "-" & m_node.ChildNodes.Item(1).InnerText.TrimEnd)

                    Else

                        If IsAlpha(LastChar) Then
                            myD.Add(k, m_node.ChildNodes.Item(1).InnerText.TrimEnd)
                        Else
                            myD.Add(k, m_node.ChildNodes.Item(1).InnerText)
                        End If

                    End If

                Next

            Catch ex As System.Exception
                ExceptionManager.Publish(ex)
            End Try

        Catch ex As System.Exception
            ExceptionManager.Publish(ex)
        End Try

        Return myD

    End Function

    Private Shared Function IsAlpha(ByVal _s As String) As Boolean

        For Each c As String In _s
            If Asc(c.ToUpper) < Asc("A"c) OrElse Asc(c.ToUpper) > Asc("Z"c) Then
                Return False
            End If
        Next

        Return True

    End Function

#End Region

End Class
