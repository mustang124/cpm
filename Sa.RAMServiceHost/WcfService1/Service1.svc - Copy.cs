﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IService1
    {
        public string GetData(string value)
        {
            //return string.Format("You entered: {0}", value);

            long count = 1000000000;
            for (int i = 0; i < count; i++)
            {
                string abc = string.Empty;
            } 
            
                ExcelEntities excelEnt = new ExcelEntities();
                using (var db = new WcfService1.ExcelEntities())
                {
                    try
                    {
                        db.InsertData(value, "test", true);
                    }
                    catch (Exception ex)
                    {
                        return "Error Receiving Data, Please Try Again.";
                    }
                }
                return string.Format("Data Successfully Received. {0}", value);
            }

        public WorksheetData GetXMLData(string xmlstring)
        {
            try
            {
                WorksheetData wsd = new WorksheetData();
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                //xmldoc.Save(stream);
                System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
                xdoc.LoadXml(xmlstring);
                xdoc.Save(stream);

                //System.Xml.XmlDocument xdoc2 = new System.Xml.XmlDocument();
                //xdoc2.Load(stream);

                //System.Xml.XmlReader xreader = System.Xml.XmlReader.Create(stream);

                //System.Xml.XmlTextReader r = new System.Xml.XmlTextReader(new System.IO.StringReader(xdoc.OuterXml));


                //System.Xml.XmlReader xmlReader = new System.Xml.XmlTextReader(xdoc.OuterXml);
                System.Xml.XmlReader xmlReader = new System.Xml.XmlTextReader(new System.IO.StringReader(xmlstring));
                //new System.Xml.Serialization.XmlSerializer(typeof(WorksheetData)).Deserialize(stream);
                //new System.Xml.Serialization.XmlSerializer(typeof(WorksheetData)).Deserialize(xmlReader);
                System.Xml.Serialization.XmlSerializer xmlSer = new System.Xml.Serialization.XmlSerializer(typeof(WorksheetData));

                wsd = (WorksheetData)xmlSer.Deserialize(xmlReader);
                SaveXMLValuesToDB(wsd);
            }
            catch (Exception ex)
            {
            }

                

                //wsd = DeserializeFromXml<WorksheetData>(xdoc.OuterXml);


                //Person peep = new Person();
                //peep = DeserializeFromXml<Person>(xdoc.OuterXml);

            
            //return string.Format("Data Successfully Received. {0}", value);
            return null;
        }

        private void SaveXMLValuesToDB(WorksheetData wsd)
        {
            ExcelEntities excelEnt = new ExcelEntities();
            using (var db = new WcfService1.ExcelEntities())
            {
                try
                {
                    db.InsertData(wsd.CompanyName.ToString(), wsd.Country.ToString(), true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
