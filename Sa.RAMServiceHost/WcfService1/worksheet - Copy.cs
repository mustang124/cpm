﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Xml;
using System.Data.SqlTypes;
using System.Xml.Serialization;
using System.Text;

[XmlRoot("WorksheetData")]   
public class WorksheetData 
{
    public WorksheetData()
    {
    }

    public WorksheetData(Country iCountry)
    {
        mCountry = iCountry;
    }


    
    //private CompanyName mCompanyName;
    private Country mCountry;

    //[XmlElement("CompanyName")]
    //public CompanyName CompanyNameField
    //{
    //    get
    //    {
    //        if (mCompanyName == null)
    //            mCompanyName = new CompanyName();
    //        return mCompanyName;
    //    }
    //    set { mCompanyName = value; }
    //}

    //public class CompanyName
    //{
    //    #region Construcor
    //    public CompanyName()
    //    {

    //    }
    //    private String mCompanyName;

    //    #region Properties
    //    public String xCompanyName
    //    {
    //        get { return mCompanyName; }
    //        set { mCompanyName = value; }
    //    }
    //    #endregion
    //    #endregion
    //}

    [XmlElement("Country")]
    public Country CountryField
    {
        get
        {
            if (mCountry == null)
                mCountry = new Country();
            return mCountry;
        }
        set { mCountry = value; }
    }

    public class Country
    {
        #region Construcor
        public Country()
        {

        }
        private String mCountry;

        #region Properties
        public String xCountry
        {
            get { return mCountry; }
            set { mCountry = value; }
        }
        #endregion
        #endregion
    }

    //public String CompanyName
    //{
    //    get { return mCompanyName; }
    //    set { mCompanyName = value; }
    //}
    //public String Country
    //{
    //    get { return mCountry; }
    //    set { mCountry = value; }
    //}
}
