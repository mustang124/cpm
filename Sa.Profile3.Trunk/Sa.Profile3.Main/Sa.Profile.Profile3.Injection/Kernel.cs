﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using Ninject;
using Sa.Profile.Profile3.DataAccess;
using Sa.Profile.Profile3.Repository.SA;
using Sa.Profile.Profile3.Common;

namespace Sa.Profile.Profile3.Injection
{
    //ttodo:
    //  error handling will be added later with logger
    //  all connection string encrypted
    //  PROD/TEST/ mode protected added (ask system where dll is running)
    public class Kernel
    {
        public IKernel SetupDI(RepositoryMode runningIn)
        {

            //note, folders hard coded for now, later move to config files
            

            IKernel kernel = new StandardKernel();
            RepositoryParms parms = new RepositoryParms();
            parms.RunningIn = runningIn;

            switch (runningIn)
            {
                case RepositoryMode.DEV_MODE:
                    if (Directory.Exists(ModeLocations.DEV_MODE_DIR) == false)
                    {
                        Directory.CreateDirectory(ModeLocations.DEV_MODE_DIR);
                    }
                    parms.XMLFolder = ModeLocations.DEV_MODE_DIR;
                   
                   // parms.ConnStr = "metadata=res://*/EF.pf12.csdl|res://*/EF.pf12.ssdl|res://*/EF.pf12.msl;provider=System.Data.SqlClient;provider connection string=';data source=THINK-H1N7J78;initial catalog=ProfileFuels12;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework'";
                   //vm db 
                   parms.ConnStr = "metadata=res://*/EF.pf12.csdl|res://*/EF.pf12.ssdl|res://*/EF.pf12.msl;provider=System.Data.SqlClient;provider connection string=';data source=10.10.27.45;initial catalog=ProfileFuels12;user id=sa;password=SAHyperV14;MultipleActiveResultSets=True;App=EntityFramework'";                       
             
                    kernel.Bind<IRepository>()
                        .To<SqlRepository>().WithConstructorArgument<RepositoryParms>(parms);    
                    break;
                //this mode will be used against sql db for calcs then
                //pull xml files from BUILD directory for compare
                case RepositoryMode.TEST_MODE://will use test db
                    if (Directory.Exists(ModeLocations.TEST_MODE_DIR) == false)
                    {
                        Directory.CreateDirectory(ModeLocations.TEST_MODE_DIR);
                    }
                    parms.XMLFolder = ModeLocations.TEST_MODE_DIR;
                    parms.ConnStr = "metadata=res://*/EF.pf12.csdl|res://*/EF.pf12.ssdl|res://*/EF.pf12.msl;provider=System.Data.SqlClient;provider connection string=';data source=THINK-H1N7J78;initial catalog=ProfileFuels12;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework'";                       
                    kernel.Bind<IRepository>()
                       .To<SqlRepository>().WithConstructorArgument<RepositoryParms>(parms);
                    break;
                //this mode is only used to build test cases, its main purpose
                //is to build data used during TEST_MODE, 
                case RepositoryMode.TEST_MODE_BUILD: //will use test db
                    if (Directory.Exists(ModeLocations.TEST_MODE_BUILD_DIR) == false)
                    {
                        Directory.CreateDirectory(ModeLocations.TEST_MODE_BUILD_DIR);
                    }
                    parms.XMLFolder = ModeLocations.TEST_MODE_BUILD_DIR;
                    parms.ConnStr = "";
                    kernel.Bind<IRepository>()
                       .To<XmlRepository>().WithConstructorArgument<RepositoryParms>(parms);
                    break;
                case RepositoryMode.PROD_MODE:
                    if (Directory.Exists(ModeLocations.PROD_MODE_DIR) == false)
                    {
                        Directory.CreateDirectory(ModeLocations.PROD_MODE_DIR);
                    }
                    parms.XMLFolder = ModeLocations.PROD_MODE_DIR;
                    //make sure and verify on prod server before allowing
                    parms.ConnStr = "prod server";
                    break;
            }                
            return kernel;
        }
    }
}
