﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
//using System.ServiceModel;
using System.Text;

namespace Sa.Profile.Profile3.Services.Library
{
    [DataContract]
    public class PL
    {
        [DataMember]
        //public List<A> A_List { get; set; }
        public List<Absences> A_List { get; set; }
        [DataMember]
        //public List<C> C_List { get; set; }
        public List<Configuration> C_List { get; set; }
        [DataMember]
        //public List<C> C_List { get; set; }
        public List<ConfigBuoy> CB_List { get; set; }
        [DataMember]
        public List<Crude> CD_List { get; set; }
        [DataMember]
        public List<ConfigRS> CR_List { get; set; }
        [DataMember]
        public List<DieselData> D_List { get; set; }
        [DataMember]
        public List<Emissions> E_List { get; set; }
        [DataMember]
        public List<Energy> EG_List { get; set; }
        [DataMember]
        public List<Electricity> EL_List { get; set; }
        [DataMember]
        public List<FiredHeaterData> F_List { get; set; }
        [DataMember]
        public List<Gasoline> G_List { get; set; }
        [DataMember]
        public List<GeneralMisc> GM_List { get; set; }
        [DataMember]
        public List<Inventory> I_List { get; set; }
        [DataMember]
        public List<Kerosene> K_List { get; set; }
        [DataMember]
        public List<LPG> L_List { get; set; }
        [DataMember]
        public List<MarineBunkerData> M_List { get; set; }
        [DataMember]
        public List<MaintExp> ME_List { get; set; }
        [DataMember]
        public List<MiscellaneousInput> MI_List { get; set; }
        [DataMember]
        public List<MaintRoutine> MR_List { get; set; }
        [DataMember]
        public List<MaintTurnAround> MT_List { get; set; }
        [DataMember]
        public List<OperatingExpenses> O_List { get; set; }
        [DataMember]
        public List<Personnel> P_List { get; set; }
        [DataMember]
        public List<ProcessingData> PD_List { get; set; }
        [DataMember]
        public List<Residual> R_List { get; set; }
        [DataMember]
        public List<RPFResidual> RP_List { get; set; }
        [DataMember]
        public List<Steam> S_List { get; set; }
        [DataMember]
        public List<Submissions> SB_List { get; set; }
        [DataMember]
        public List<Yields> Y_List { get; set; }

    }

    //[DataContract]
    //public class A {
    //    [DataMember]
    //    public decimal? A1 { get; set; }//A
    //    [DataMember]
    //    public decimal? A2 { get; set; }//A
    //    [DataMember]
    //    public string A3 { get; set; }//A
    //    [DataMember]
    //    public int? A4 { get; set; }//A  
    //}

    public class Absences
    {
        [DataMember]
        public string CategoryID { get; set; }//A
        [DataMember]
        public decimal? MPSAbs { get; set; }//A
        [DataMember]
        public decimal? OCCAbs { get; set; }//A
        [DataMember]
        public int? SubmissionID { get; set; }//A  
    }



    //[DataContract]
    //public class C {
    //    [DataMember]
    //    public decimal? C1 { get; set; }//C
    //    [DataMember]
    //    public decimal? C2 { get; set; }//C
    //    [DataMember]
    //    public string C3 { get; set; }//C
    //    [DataMember]
    //    public decimal? C4 { get; set; }//C
    //    [DataMember]
    //    public decimal? C5 { get; set; }//C
    //    [DataMember]
    //    public decimal? C6 { get; set; }//C
    //    [DataMember]
    //    public decimal? C7 { get; set; }//C
    //    [DataMember]
    //    public decimal? C8 { get; set; }//C
    //    [DataMember]
    //    public decimal? C9 { get; set; }//C
    //    [DataMember]
    //    public decimal? C10 { get; set; }//C
    //    [DataMember]
    //    public decimal? C11 { get; set; }//C
    //    [DataMember]
    //    public string C12 { get; set; }//C
    //    [DataMember]
    //    public string C13 { get; set; }//C
    //    [DataMember]
    //    public string C14 { get; set; }//C
    //    [DataMember]
    //    public int? C15 { get; set; }//C
    //    [DataMember]
    //    public int? C16 { get; set; }//C    
    //}

    [DataContract]
    public class Configuration
    {
        [DataMember]
        public decimal? AllocPcntOfCap { get; set; }//C
        [DataMember]
        public string BlockOp { get; set; }//C
        [DataMember]
        public decimal? Cap { get; set; }//C
        [DataMember]
        public decimal? EnergyPcnt { get; set; }//C
        [DataMember]
        public decimal? InServicePcnt { get; set; }//C
        [DataMember]
        public decimal? MHPerWeek { get; set; }//C
        [DataMember]
        public decimal? PostPerShift { get; set; }//C
        [DataMember]
        public string ProcessID { get; set; }//C
        [DataMember]
        public string ProcessType { get; set; }//C
        [DataMember]
        public decimal? StmCap { get; set; }//C
        [DataMember]
        public decimal? StmUtilPcnt { get; set; }//C
        [DataMember]
        public int? SubmissionID { get; set; }//C
        [DataMember]
        public int? UnitID { get; set; }//C
        [DataMember]
        public string UnitName { get; set; }//C
        [DataMember]
        public decimal? UtilPcnt { get; set; }//C
        [DataMember]
        public decimal? YearsOper { get; set; }//C    
    }


    //[DataContract]
    //public class CB {
    //    [DataMember]
    //    public decimal? CB1 { get; set; }//CB
    //    [DataMember]
    //    public decimal? CB2 { get; set; }//CB
    //    [DataMember]
    //    public decimal? CB3 { get; set; }//CB
    //    [DataMember]
    //    public string CB4 { get; set; }//CB
    //    [DataMember]
    //    public string CB5 { get; set; }//CB
    //    [DataMember]
    //    public int? CB6 { get; set; }//CB
    //    [DataMember]
    //    public int? CB7 { get; set; }//CB   
    //}

    [DataContract]
    public class ConfigBuoy
    {
        [DataMember]
        public decimal? LineSize { get; set; }//CB
        [DataMember]
        public decimal? PcntOwnership { get; set; }//CB
        [DataMember]
        public decimal? ShipCap { get; set; }//CB
        [DataMember]
        public string ProcessID { get; set; }//CB
        [DataMember]
        public string UnitName { get; set; }//CB
        [DataMember]
        public int? UnitID { get; set; }//CB
        [DataMember]
        public int? SubmissionID { get; set; }//CB   
    }


    [DataContract]
    public class Crude
    {
        [DataMember]
        public decimal? BBL { get; set; }//CD
        [DataMember]
        public decimal? Gravity { get; set; }//CD
        [DataMember]
        public decimal? Sulfur { get; set; }//CD
        [DataMember]
        public string CrudeName { get; set; }//CD
        [DataMember]
        public int? CrudeID { get; set; }//CD
        [DataMember]
        public string Period { get; set; }//CD
        [DataMember]
        public string CrudeNumber { get; set; }//CD
        [DataMember]
        public int? SubmissionID { get; set; }//CD    
    }


    [DataContract]
    public class CD {
        [DataMember]
        public decimal? CD1 { get; set; }//CD
        [DataMember]
        public decimal? CD2 { get; set; }//CD
        [DataMember]
        public decimal? CD3 { get; set; }//CD
        [DataMember]
        public string CD4 { get; set; }//CD
        [DataMember]
        public string CD5 { get; set; }//CD
        [DataMember]
        public int? CD6 { get; set; }//CD
        [DataMember]
        public string CD7 { get; set; }//CD
        [DataMember]
        public int? CD8 { get; set; }//CD    
    }

    [DataContract]
    public class ConfigRS
    {
        [DataMember]
        public decimal? AvgSize { get; set; }//CR
        [DataMember]
        public decimal? Throughput { get; set; }//CR
        [DataMember]
        public string ProcessID { get; set; }//CR
        [DataMember]
        public string ProcessType { get; set; }//CR
        [DataMember]
        public int? SubmissionID { get; set; }//CR
        [DataMember]
        public int? UnitID { get; set; }//CR
    }

    [DataContract]
    public class CR {
        [DataMember]public decimal? CR1{ get; set; }//CR
        [DataMember]public decimal? CR2{ get; set; }//CR
        [DataMember]public string CR3{ get; set; }//CR
        [DataMember]public string CR4{ get; set; }//CR
        [DataMember]public int? CR5{ get; set; }//CR
        [DataMember]public int? CR6{ get; set; }//CR
    }

    [DataContract]
    public class DieselData
    {
        [DataMember]
        public decimal? ASTM90 { get; set; }//D
        [DataMember]
        public decimal? BiodieselPcnt { get; set; }//D
        [DataMember]
        public decimal? Cetane { get; set; }//D
        [DataMember]
        public decimal? CloudPt { get; set; }//D
        [DataMember]
        public decimal? Density { get; set; }//D
        [DataMember]
        public string Grade { get; set; }//D
        [DataMember]
        public string Market { get; set; }//D
        [DataMember]
        public decimal? E350 { get; set; }//D
        [DataMember]
        public decimal? PourPt { get; set; }//D
        [DataMember]
        public decimal? Sulfur { get; set; }//D
        [DataMember]
        public decimal? KMT { get; set; }//D
        [DataMember]
        public string Type { get; set; }//D
        [DataMember]
        public int? BlendID { get; set; }//D
        [DataMember]
        public int? SubmissionID { get; set; }//D    
    }


    [DataContract]
    public class D {
        [DataMember]public decimal? D1{ get; set; }//D
        [DataMember]public decimal? D2{ get; set; }//D
        [DataMember]public decimal? D3{ get; set; }//D
        [DataMember]public decimal? D4{ get; set; }//D
        [DataMember]public decimal? D5{ get; set; }//D
        [DataMember]public decimal? D6{ get; set; }//D
        [DataMember]public decimal? D7{ get; set; }//D
        [DataMember]public decimal? D8{ get; set; }//D
        [DataMember]
        public decimal? D9 { get; set; }//D
        [DataMember]
        public string D10 { get; set; }//D
        [DataMember]
        public string D11 { get; set; }//D
        [DataMember]
        public string D12 { get; set; }//D
        [DataMember]
        public int? D13 { get; set; }//D
        [DataMember]
        public int? D14 { get; set; }//D    
    }


    [DataContract]
    public class Emissions
    {
        [DataMember]
        public decimal? RefineryEmission { get; set; }//E
        [DataMember]
        public string EmissionType { get; set; }//E
        [DataMember]
        public int? SubmissionID { get; set; }//E  
    }


    [DataContract]
    public class E {
        [DataMember]
        public decimal? E1 { get; set; }//E
        [DataMember]
        public string E2 { get; set; }//E
        [DataMember]
        public int? E3 { get; set; }//E  
    }


    [DataContract]
    public class Electricity
    {
        [DataMember]
        public decimal? Amount { get; set; }//EG
        [DataMember]
        public string EnergyType { get; set; }//EG
        [DataMember]
        public decimal? GenEff { get; set; }//EG
        [DataMember]
        public decimal? PriceLocal { get; set; }//EG
        [DataMember]
        public int? SubmissionID { get; set; }//EG
        [DataMember]
        public string posElect { get; set; }//EG
        [DataMember]
        public string TransactionType { get; set; }//EG
        
    }

    [DataContract]
    public class EL
    {
        [DataMember]
        public decimal? EL1 { get; set; }//EL
        [DataMember]
        public decimal? EL2 { get; set; }//EL
        [DataMember]
        public decimal? EL3 { get; set; }//EL
        [DataMember]
        public string EL4 { get; set; }//EL
        [DataMember]
        public string EL5 { get; set; }//EL
        [DataMember]
        public int? EL6 { get; set; }//EL
    }

    [DataContract]
    public class Energy
    {
        [DataMember]
        public decimal? Amount { get; set; }//EG
        [DataMember]
        public decimal? Butane { get; set; }//EG
        [DataMember]
        public decimal? Butylenes { get; set; }//EG
        [DataMember]
        public decimal? C5Plus { get; set; }//EG
        [DataMember]
        public decimal? CO { get; set; }//EG
        [DataMember]
        public decimal? CO2 { get; set; }//EG
        [DataMember]
        public decimal? Ethane { get; set; }//EG
        [DataMember]
        public decimal? Ethylene { get; set; }//EG
        [DataMember]
        public decimal? H2S { get; set; }//EG
        [DataMember]
        public decimal? Hydrogen { get; set; }//EG
        [DataMember]
        public decimal? Isobutane { get; set; }//EG
        [DataMember]
        public decimal? Methane { get; set; }//EG
        [DataMember]
        public decimal? MillionBritishThermalUnitsOut { get; set; }//EG
        [DataMember]
        public decimal? N2 { get; set; }//EG
        [DataMember]
        public decimal? NH3 { get; set; }//EG
        [DataMember]
        public decimal? PriceLocal { get; set; }//EG
        [DataMember]
        public decimal? Propane { get; set; }//EG
        [DataMember]
        public char? OverrideCalculation { get; set; }//EG
        [DataMember]
        public decimal? Propylene { get; set; }//EG
        [DataMember]
        public decimal? SO2 { get; set; }//EG
        [DataMember]
        public string TransType { get; set; }//EG
        [DataMember]
        public string EnergyType { get; set; }//EG
        [DataMember]
        public string EG23 { get; set; }//EG
        [DataMember]
        public int? SubmissionID { get; set; }//EG    
    }


    [DataContract]
    public class EG {
        [DataMember]
        public decimal? EG1 { get; set; }//EG
        [DataMember]
        public decimal? EG2 { get; set; }//EG
        [DataMember]
        public decimal? EG3 { get; set; }//EG
        [DataMember]
        public decimal? EG4 { get; set; }//EG
        [DataMember]
        public decimal? EG5 { get; set; }//EG
        [DataMember]
        public decimal? EG6 { get; set; }//EG
        [DataMember]
        public decimal? EG7 { get; set; }//EG
        [DataMember]
        public decimal? EG8 { get; set; }//EG
        [DataMember]
        public decimal? EG9 { get; set; }//EG
        [DataMember]
        public decimal? EG10 { get; set; }//EG
        [DataMember]
        public decimal? EG11 { get; set; }//EG
        [DataMember]
        public decimal? EG12 { get; set; }//EG
        [DataMember]
        public decimal? EG13 { get; set; }//EG
        [DataMember]
        public decimal? EG14 { get; set; }//EG
        [DataMember]
        public decimal? EG15 { get; set; }//EG
        [DataMember]
        public decimal? EG16 { get; set; }//EG
        [DataMember]
        public decimal? EG17 { get; set; }//EG
        [DataMember]
        public char? EG18 { get; set; }//EG
        [DataMember]
        public decimal? EG19 { get; set; }//EG
        [DataMember]
        public decimal? EG20 { get; set; }//EG
        [DataMember]
        public decimal? EG21 { get; set; }//EG
        [DataMember]
        public string EG22 { get; set; }//EG
        [DataMember]
        public string EG23 { get; set; }//EG
        [DataMember]
        public int? EG24 { get; set; }//EG    
    }


    [DataContract]
    public class FiredHeaterData
    {
        [DataMember]
        public decimal? AbsorbedDuty { get; set; }//F
        [DataMember]
        public decimal? CombAirTemp { get; set; }//F
        [DataMember]
        public string FuelType { get; set; }//F
        [DataMember]
        public decimal? FiredDuty { get; set; }//F
        [DataMember]
        public decimal? FurnaceInTemp { get; set; }//F
        [DataMember]
        public decimal? FurnOutTemp { get; set; }//F
        [DataMember]
        public decimal? HeatLossPcnt { get; set; }//F
        [DataMember]
        public decimal? OthCombDuty { get; set; }//F
        [DataMember]
        public decimal? OtherDuty { get; set; }//F
        [DataMember]
        public decimal? ProcessDuty { get; set; }//F
        [DataMember]
        public decimal? ShaftDuty { get; set; }//F
        [DataMember]
        public decimal? StackO2 { get; set; }//F
        [DataMember]
        public decimal? StackTemp { get; set; }//F
        [DataMember]
        public string HeaterName { get; set; }//F
        [DataMember]
        public decimal? ThroughputRpt { get; set; }//F
        [DataMember]
        public int? HeaterNo { get; set; }//F
        [DataMember]
        public decimal? SteamDuty { get; set; }//F
        [DataMember]
        public string ProcessFluid { get; set; }//F
        [DataMember]
        public string ProcessID { get; set; }//F
        [DataMember]
        public string Service { get; set; }//F
        [DataMember]
        public char? SteamSuperHeated { get; set; }//F
        [DataMember]
        public int? SubmissionID { get; set; }//F
        [DataMember]
        public int? UnitID { get; set; }//F
        [DataMember]
        public string ThroughputUOM { get; set; }//F   
    }


    [DataContract]
    public class F {
        [DataMember]
        public decimal? F1 { get; set; }//F
        [DataMember]
        public decimal? F2 { get; set; }//F
        [DataMember]
        public char? F3 { get; set; }//F
        [DataMember]
        public decimal? F4 { get; set; }//F
        [DataMember]
        public decimal? F5 { get; set; }//F
        [DataMember]
        public decimal? F6 { get; set; }//F
        [DataMember]
        public decimal? F7 { get; set; }//F
        [DataMember]
        public decimal? F8 { get; set; }//F
        [DataMember]
        public decimal? F9 { get; set; }//F
        [DataMember]
        public decimal? F10 { get; set; }//F
        [DataMember]
        public decimal? F11 { get; set; }//F
        [DataMember]
        public decimal? F12 { get; set; }//F
        [DataMember]
        public decimal? F13 { get; set; }//F
        [DataMember]
        public string F14 { get; set; }//F
        [DataMember]
        public decimal? F15 { get; set; }//F
        [DataMember]
        public string F16 { get; set; }//F
        [DataMember]
        public decimal? F17 { get; set; }//F
        [DataMember]
        public string F18 { get; set; }//F
        [DataMember]
        public string F19 { get; set; }//F
        [DataMember]
        public string F20 { get; set; }//F
        [DataMember]
        public string F21 { get; set; }//F
        [DataMember]
        public int? F22 { get; set; }//F
        [DataMember]
        public int? F23 { get; set; }//F
        [DataMember]
        public int? F24 { get; set; }//F   
    }


    [DataContract]
    public class Gasoline
    {
        [DataMember]
        public decimal? Density { get; set; }//G
        [DataMember]
        public decimal? Ethanol { get; set; }//G
        [DataMember]
        public decimal? ETBE { get; set; }//G
        [DataMember]
        public decimal? TAME { get; set; }//G
        [DataMember]
        public decimal? Lead { get; set; }//G
        [DataMember]
        public decimal? KMT { get; set; }//G
        [DataMember]
        public decimal? MTBE { get; set; }//G
        [DataMember]
        public decimal? MON { get; set; }//G
        [DataMember]
        public decimal? OthOxygen { get; set; }//G
        [DataMember]
        public decimal? Oxygen { get; set; }//G
        [DataMember]
        public decimal? RVP { get; set; }//G
        [DataMember]
        public decimal? RON { get; set; }//G
        [DataMember]
        public decimal? Sulfur { get; set; }//G
        [DataMember]
        public string Grade { get; set; }//G
        [DataMember]
        public string Market { get; set; }//G
        [DataMember]
        public string Type { get; set; }//G
        [DataMember]
        public int? BlendID { get; set; }//G
        [DataMember]
        public int? SubmissionID { get; set; }//G   
    }


    [DataContract]
    public class G {
        [DataMember]
        public decimal? G1 { get; set; }//G
        [DataMember]
        public decimal? G2 { get; set; }//G
        [DataMember]
        public decimal? G3 { get; set; }//G
        [DataMember]
        public decimal? G4 { get; set; }//G
        [DataMember]
        public decimal? G5 { get; set; }//G
        [DataMember]
        public decimal? G6 { get; set; }//G
        [DataMember]
        public decimal? G7 { get; set; }//G
        [DataMember]
        public decimal? G8 { get; set; }//G
        [DataMember]
        public decimal? G9 { get; set; }//G
        [DataMember]
        public decimal? G10 { get; set; }//G
        [DataMember]
        public decimal? G11 { get; set; }//G
        [DataMember]
        public decimal? G12 { get; set; }//G
        [DataMember]
        public decimal? G13 { get; set; }//G
        [DataMember]
        public string G14 { get; set; }//G
        [DataMember]
        public string G15 { get; set; }//G
        [DataMember]
        public string G16 { get; set; }//G
        [DataMember]
        public int? G17 { get; set; }//G
        [DataMember]
        public int? G18 { get; set; }//G   
    }

    [DataContract]
    public class GeneralMisc
    {
        [DataMember]
        public decimal? FlareLossMT { get; set; }//GM
        [DataMember]
        public decimal? TotLossMT { get; set; }//GM
        [DataMember]
        public int? SubmissionID { get; set; }//GM   
    }


    [DataContract]
    public class GM {
        [DataMember]
        public decimal? GM1 { get; set; }//GM
        [DataMember]
        public decimal? GM2 { get; set; }//GM
        [DataMember]
        public int? GM3 { get; set; }//GM   
    }

    [DataContract]
    public class Inventory
    {
        [DataMember]
        public decimal? AvgLevel { get; set; }//I
        [DataMember]
        public decimal? LeasedPcnt { get; set; }//I
        [DataMember]
        public int? SubmissionID { get; set; }//I
        [DataMember]
        public decimal? MandStorage { get; set; }//I
        [DataMember]
        public decimal? MktgStorage { get; set; }//I
        [DataMember]
        public decimal? RefStorage { get; set; }//I
        [DataMember]
        public decimal? TotStorage { get; set; }//I
        [DataMember]
        public string TankType { get; set; }//I
        [DataMember]
        public int? NumTank { get; set; }//I   
    }


    [DataContract]
    public class I {
        [DataMember]
        public decimal? I1 { get; set; }//I
        [DataMember]
        public decimal? I2 { get; set; }//I
        [DataMember]
        public int? I3 { get; set; }//I
        [DataMember]
        public decimal? I4 { get; set; }//I
        [DataMember]
        public decimal? I5 { get; set; }//I
        [DataMember]
        public decimal? I6 { get; set; }//I
        [DataMember]
        public decimal? I7 { get; set; }//I
        [DataMember]
        public string I8 { get; set; }//I
        [DataMember]
        public int? I9 { get; set; }//I   
    }


    [DataContract]
    public class Kerosene
    {
        [DataMember]
        public decimal? Density { get; set; }//K
        [DataMember]
        public decimal? Sulfur { get; set; }//K
        [DataMember]
        public decimal? KMT { get; set; }//K
        [DataMember]
        public string Grade { get; set; }//K
        [DataMember]
        public string Type { get; set; }//K
        [DataMember]
        public int? BlendID { get; set; }//K
        [DataMember]
        public int? SubmissionID { get; set; }//K    
    }


    [DataContract]
    public class K {
        [DataMember]
        public decimal? K1 { get; set; }//K
        [DataMember]
        public decimal? K2 { get; set; }//K
        [DataMember]
        public decimal? K3 { get; set; }//K
        [DataMember]
        public string K4 { get; set; }//K
        [DataMember]
        public string K5 { get; set; }//K
        [DataMember]
        public int? K6 { get; set; }//K
        [DataMember]
        public int? K7 { get; set; }//K    
    }

    [DataContract]
    public class LPG
    {
        [DataMember]
        public decimal? VolC2Lt { get; set; }//L
        [DataMember]
        public decimal? VolC3 { get; set; }//L
        [DataMember]
        public decimal? VolC3ene { get; set; }//L
        [DataMember]
        public decimal? VolnC4 { get; set; }//L
        [DataMember]
        public decimal? VolC4ene { get; set; }//L
        [DataMember]
        public decimal? VolC5Plus { get; set; }//L
        [DataMember]
        public decimal? VoliC4 { get; set; }//L
        [DataMember]
        public char? MolOrVol { get; set; }//L
        [DataMember]
        public int? BlendID { get; set; }//L
        [DataMember]
        public int? SubmissionID { get; set; }//L    
    }


    [DataContract]
    public class L {
        [DataMember]
        public decimal? L1 { get; set; }//L
        [DataMember]
        public decimal? L2 { get; set; }//L
        [DataMember]
        public decimal? L3 { get; set; }//L
        [DataMember]
        public decimal? L4 { get; set; }//L
        [DataMember]
        public decimal? L5 { get; set; }//L
        [DataMember]
        public decimal? L6 { get; set; }//L
        [DataMember]
        public decimal? L7 { get; set; }//L
        [DataMember]
        public char? L8 { get; set; }//L
        [DataMember]
        public int? L9 { get; set; }//L
        [DataMember]
        public int? L10 { get; set; }//L    
    }

    [DataContract]
    public class MarineBunkerData
    {
        [DataMember]
        public decimal? CrackedStock { get; set; }//M
        [DataMember]
        public decimal? Density { get; set; }//M
        [DataMember]
        public decimal? PourPt { get; set; }//M
        [DataMember]
        public decimal? Sulfur { get; set; }//M
        [DataMember]
        public decimal? KMT { get; set; }//M
        [DataMember]
        public decimal? ViscCSAtTemp { get; set; }//M
        [DataMember]
        public decimal? ViscTemp { get; set; }//M
        [DataMember]
        public string Grade { get; set; }//M
        [DataMember]
        public int? BlendID { get; set; }//M
        [DataMember]
        public int? SubmissionID { get; set; }//M    
    }


    [DataContract]
    public class M {
        [DataMember]
        public decimal? M1 { get; set; }//M
        [DataMember]
        public decimal? M2 { get; set; }//M
        [DataMember]
        public decimal? M3 { get; set; }//M
        [DataMember]
        public decimal? M4 { get; set; }//M
        [DataMember]
        public decimal? M5 { get; set; }//M
        [DataMember]
        public decimal? M6 { get; set; }//M
        [DataMember]
        public decimal? M7 { get; set; }//M
        [DataMember]
        public string M8 { get; set; }//M
        [DataMember]
        public int? M9 { get; set; }//M
        [DataMember]
        public int? M10 { get; set; }//M    
    }

    [DataContract]
    public class MaintExp
    {
        [DataMember]
        public decimal? ConstraintRemoval { get; set; }//ME
        [DataMember]
        public decimal? Energy { get; set; }//ME
        [DataMember]
        public decimal? MaintExpRout { get; set; }//ME
        [DataMember]
        public decimal? MaintExpTA { get; set; }//ME
        [DataMember]
        public decimal? MaintOvhdRout { get; set; }//ME
        [DataMember]
        public decimal? MaintOvhdTA { get; set; }//ME
        [DataMember]
        public decimal? NonMaintInvestExp { get; set; }//ME
        [DataMember]
        public decimal? NonRefExcl { get; set; }//ME
        [DataMember]
        public decimal? NonRegUnit { get; set; }//ME
        [DataMember]
        public decimal? OthInvest { get; set; }//ME
        [DataMember]
        public decimal? RegDiesel { get; set; }//ME
        [DataMember]
        public decimal? RegExp { get; set; }//ME
        [DataMember]
        public decimal? RegGaso { get; set; }//ME
        [DataMember]
        public decimal? RegOth { get; set; }//ME
        [DataMember]
        public decimal? RoutMaintCptl { get; set; }//ME
        [DataMember]
        public decimal? SafetyOth { get; set; }//ME
        [DataMember]
        public decimal? ComplexCptl { get; set; }//ME
        [DataMember]
        public decimal? MaintExpense { get; set; }//ME
        [DataMember]
        public decimal? TotMaint { get; set; }//ME
        [DataMember]
        public decimal? MaintOvhd { get; set; }//ME
        [DataMember]
        public decimal? TAMaintCptl { get; set; }//ME
        [DataMember]
        public int? SubmissionID { get; set; }//ME    
    }


    [DataContract]
    public class ME {
        [DataMember]
        public decimal? ME1 { get; set; }//ME
        [DataMember]
        public decimal? ME2 { get; set; }//ME
        [DataMember]
        public decimal? ME3 { get; set; }//ME
        [DataMember]
        public decimal? ME4 { get; set; }//ME
        [DataMember]
        public decimal? ME5 { get; set; }//ME
        [DataMember]
        public decimal? ME6 { get; set; }//ME
        [DataMember]
        public decimal? ME7 { get; set; }//ME
        [DataMember]
        public decimal? ME8 { get; set; }//ME
        [DataMember]
        public decimal? ME9 { get; set; }//ME
        [DataMember]
        public decimal? ME10 { get; set; }//ME
        [DataMember]
        public decimal? ME11 { get; set; }//ME
        [DataMember]
        public decimal? ME12 { get; set; }//ME
        [DataMember]
        public decimal? ME13 { get; set; }//ME
        [DataMember]
        public decimal? ME14 { get; set; }//ME
        [DataMember]
        public decimal? ME15 { get; set; }//ME
        [DataMember]
        public decimal? ME16 { get; set; }//ME
        [DataMember]
        public decimal? ME17 { get; set; }//ME
        [DataMember]
        public decimal? ME18 { get; set; }//ME
        [DataMember]
        public decimal? ME19 { get; set; }//ME
        [DataMember]
        public decimal? ME20 { get; set; }//ME
        [DataMember]
        public decimal? ME21 { get; set; }//ME
        [DataMember]
        public int? ME22 { get; set; }//ME    
    }

    [DataContract]
    public class MiscellaneousInput
    {
        [DataMember]
        public decimal? CDUChargeBbl { get; set; }//MI
        [DataMember]
        public decimal? CDUChargeMT { get; set; }//MI
        [DataMember]
        public decimal? OffsiteEnergyPcnt { get; set; }//MI
        [DataMember]
        public int? SubmissionID { get; set; }//MI   
    }


    [DataContract]
    public class MI {
        [DataMember]
        public decimal? MI1 { get; set; }//MI
        [DataMember]
        public decimal? MI2 { get; set; }//MI
        [DataMember]
        public decimal? MI3 { get; set; }//MI
        [DataMember]
        public int? MI4 { get; set; }//MI   
    }

    [DataContract]
    public class MaintRoutine
    {
        [DataMember]
        public decimal? RoutCptlLocal { get; set; }//MR
        [DataMember]
        public decimal? RoutCostLocal { get; set; }//MR
        [DataMember]
        public decimal? RoutExpLocal { get; set; }//MR
        [DataMember]
        public decimal? InServicePcnt { get; set; }//MR
        [DataMember]
        public decimal? MaintDown { get; set; }//MR
        [DataMember]
        public decimal? UtilPcnt { get; set; }//MR
        [DataMember]
        public decimal? OthDownOffsiteUpsets { get; set; }//MR
        [DataMember]
        public decimal? OthDownOther { get; set; }//MR
        [DataMember]
        public decimal? OthDown { get; set; }//MR
        [DataMember]
        public decimal? OthDownEconomic { get; set; }//MR
        [DataMember]
        public decimal? OthDownExternal { get; set; }//MR
        [DataMember]
        public decimal? OthDownUnitUpsets { get; set; }//MR
        [DataMember]
        public decimal? OthSlow { get; set; }//MR
        [DataMember]
        public int? UnitID { get; set; }//MR
        [DataMember]
        public decimal? RoutOvhdLocal { get; set; }//MR
        [DataMember]
        public int? OthNum { get; set; }//MR
        [DataMember]
        public decimal? RegDown { get; set; }//MR
        [DataMember]
        public int? SubmissionID { get; set; }//MR
        [DataMember]
        public string ProcessID { get; set; }//MR
        [DataMember]
        public int? RegNum { get; set; }//MR
        [DataMember]
        public int? MaintNum { get; set; }//MR    
    }


    [DataContract]
    public class MR {
        [DataMember]
        public decimal? MR1 { get; set; }//MR
        [DataMember]
        public decimal? MR2 { get; set; }//MR
        [DataMember]
        public decimal? MR3 { get; set; }//MR
        [DataMember]
        public decimal? MR4 { get; set; }//MR
        [DataMember]
        public decimal? MR5 { get; set; }//MR
        [DataMember]
        public decimal? MR6 { get; set; }//MR
        [DataMember]
        public decimal? MR7 { get; set; }//MR
        [DataMember]
        public decimal? MR8 { get; set; }//MR
        [DataMember]
        public decimal? MR9 { get; set; }//MR
        [DataMember]
        public decimal? MR10 { get; set; }//MR
        [DataMember]
        public decimal? MR11 { get; set; }//MR
        [DataMember]
        public decimal? MR12 { get; set; }//MR
        [DataMember]
        public decimal? MR13 { get; set; }//MR
        [DataMember]
        public int? MR14 { get; set; }//MR
        [DataMember]
        public decimal? MR15 { get; set; }//MR
        [DataMember]
        public int? MR16 { get; set; }//MR
        [DataMember]
        public decimal? MR17 { get; set; }//MR
        [DataMember]
        public int? MR18 { get; set; }//MR
        [DataMember]
        public string MR19 { get; set; }//MR
        [DataMember]
        public int? MR20 { get; set; }//MR
        [DataMember]
        public int? MR21 { get; set; }//MR    
    }


    [DataContract]
    public class MaintTurnAround
    {
        [DataMember]
        public int? UnitID { get; set; }//MT
        [DataMember]
        public DateTime? PrevTADate { get; set; }//MT
        [DataMember]
        public decimal? TACptlLocal { get; set; }//MT
        [DataMember]
        public decimal? TAContMPS { get; set; }//MT
        [DataMember]
        public decimal? TAContOCC { get; set; }//MT
        [DataMember]
        public decimal? TACostLocal { get; set; }//MT
        [DataMember]
        public decimal? TAOvhdLocal { get; set; }//MT
        [DataMember]
        public decimal? TAExpLocal { get; set; }//MT
        [DataMember]
        public decimal? TAHrsDown { get; set; }//MT
        [DataMember]
        public decimal? TALaborCostLocal { get; set; }//MT
        [DataMember]
        public decimal? TAMPSOVTPcnt { get; set; }//MT
        [DataMember]
        public decimal? TAMPSSTH { get; set; }//MT
        [DataMember]
        public decimal? TAOCCOVT { get; set; }//MT
        [DataMember]
        public decimal? TAOCCSTH { get; set; }//MT
        [DataMember]
        public DateTime? TADate { get; set; }//MT
        [DataMember]
        public string ProcessID { get; set; }//MT
        [DataMember]
        public int? SubmissionID { get; set; }//MT
        [DataMember]
        public int? TAExceptions { get; set; }//MT   
    }


    [DataContract]
    public class MT {
        [DataMember]
        public int? MT1 { get; set; }//MT
        [DataMember]
        public DateTime? MT2 { get; set; }//MT
        [DataMember]
        public decimal? MT3 { get; set; }//MT
        [DataMember]
        public decimal? MT4 { get; set; }//MT
        [DataMember]
        public decimal? MT5 { get; set; }//MT
        [DataMember]
        public decimal? MT6 { get; set; }//MT
        [DataMember]
        public decimal? MT7 { get; set; }//MT
        [DataMember]
        public decimal? MT8 { get; set; }//MT
        [DataMember]
        public decimal? MT9 { get; set; }//MT
        [DataMember]
        public decimal? MT10 { get; set; }//MT
        [DataMember]
        public decimal? MT11 { get; set; }//MT
        [DataMember]
        public decimal? MT12 { get; set; }//MT
        [DataMember]
        public decimal? MT13 { get; set; }//MT
        [DataMember]
        public decimal? MT14 { get; set; }//MT
        [DataMember]
        public DateTime? MT15 { get; set; }//MT
        [DataMember]
        public string MT16 { get; set; }//MT
        [DataMember]
        public int? MT17 { get; set; }//MT
        [DataMember]
        public int? MT18 { get; set; }//MT   
    }

    [DataContract]
    public class OperatingExpenses
    {
        [DataMember]
        public string Property { get; set; }//O
        [DataMember]
        public decimal? RptValue { get; set; }//O
        [DataMember]
        public string OthDescription { get; set; }//O
        [DataMember]
        public int? SubmissionID { get; set; }//O   
    }


    [DataContract]
    public class O {
        [DataMember]
        public string O1 { get; set; }//O
        [DataMember]
        public decimal? O2 { get; set; }//O
        [DataMember]
        public string O3 { get; set; }//O
        [DataMember]
        public int? O4 { get; set; }//O   
    }

    [DataContract]
    public class Personnel
    {
        [DataMember]
        public decimal? AbsHrs { get; set; }//P
        [DataMember]
        public decimal? Contract { get; set; }//P
        [DataMember]
        public decimal? GA { get; set; }//P
        [DataMember]
        public decimal? NumPers { get; set; }//P
        [DataMember]
        public decimal? OVTHours { get; set; }//P
        [DataMember]
        public decimal? OVTPcnt { get; set; }//P
        [DataMember]
        public decimal? STH { get; set; }//P
        [DataMember]
        public string PersID { get; set; }//P
        [DataMember]
        public int? SubmissionID { get; set; }//P    
    }

    [DataContract]
    public class P
    {
        [DataMember]
        public decimal? P1 { get; set; }//P
        [DataMember]
        public decimal? P2 { get; set; }//P
        [DataMember]
        public decimal? P3 { get; set; }//P
        [DataMember]
        public decimal? P4 { get; set; }//P
        [DataMember]
        public decimal? P5 { get; set; }//P
        [DataMember]
        public decimal? P6 { get; set; }//P
        [DataMember]
        public decimal? P7 { get; set; }//P
        [DataMember]
        public string P8 { get; set; }//P
        [DataMember]
        public int? P9 { get; set; }//P    
    }

    [DataContract]
    public class ProcessingData
    {
        [DataMember]
        public string Property { get; set; }//PD
        [DataMember]
        public DateTime? RptDVal { get; set; }//PD
        [DataMember]
        public string UOM { get; set; }//PD
        [DataMember]
        public decimal? RptNVal { get; set; }//PD
        [DataMember]
        public string RptTVal { get; set; }//PD
        [DataMember]
        public int? SubmissionID { get; set; }//PD
        [DataMember]
        public int? UnitID { get; set; }//PD    
    }


        [DataContract]
        public class PD
        {
            [DataMember]
            public string PD1 { get; set; }//PD
            [DataMember]
            public DateTime? PD2 { get; set; }//PD
            [DataMember]
            public string PD3 { get; set; }//PD
            [DataMember]
            public decimal? PD4 { get; set; }//PD
            [DataMember]
            public string PD5 { get; set; }//PD
            [DataMember]
            public int? PD6 { get; set; }//PD
            [DataMember]
            public int? PD7 { get; set; }//PD    
        }


        [DataContract]
        public class Residual
        {
            [DataMember]
            public decimal? Density { get; set; }//R
            [DataMember]
            public decimal? PourPT { get; set; }//R
            [DataMember]
            public decimal? Sulfur { get; set; }//R
            [DataMember]
            public decimal? KMT { get; set; }//R
            [DataMember]
            public decimal? ViscCSAtTemp { get; set; }//R
            [DataMember]
            public decimal? ViscTemp { get; set; }//R
            [DataMember]
            public string Grade { get; set; }//R
            [DataMember]
            public int? BlendID { get; set; }//R
            [DataMember]
            public int? SubmissionID { get; set; }//R    
        }

        [DataContract]
        public class R
        {
            [DataMember]
            public decimal? R1 { get; set; }//R
            [DataMember]
            public decimal? R2 { get; set; }//R
            [DataMember]
            public decimal? R3 { get; set; }//R
            [DataMember]
            public decimal? R4 { get; set; }//R
            [DataMember]
            public decimal? R5 { get; set; }//R
            [DataMember]
            public decimal? R6 { get; set; }//R
            [DataMember]
            public string R7 { get; set; }//R
            [DataMember]
            public int? R8 { get; set; }//R
            [DataMember]
            public int? R9 { get; set; }//R    
        }

        [DataContract]
        public class RPFResidual
        {
            [DataMember]
            public decimal? Density { get; set; }//RP
            [DataMember]
            public decimal? Sulfur { get; set; }//RP
            [DataMember]
            public decimal? ViscCSAtTemp { get; set; }//RP
            [DataMember]
            public decimal? ViscTemp { get; set; }//RP
            [DataMember]
            public string EnergyType { get; set; }//RP
            [DataMember]
            public int? SubmissionID { get; set; }//RP    
        }


        [DataContract]
        public class RP
        {
            [DataMember]
            public decimal? RP1 { get; set; }//RP
            [DataMember]
            public decimal? RP2 { get; set; }//RP
            [DataMember]
            public decimal? RP3 { get; set; }//RP
            [DataMember]
            public decimal? RP4 { get; set; }//RP
            [DataMember]
            public string RP5 { get; set; }//RP
            [DataMember]
            public int? RP6 { get; set; }//RP    
        }



        [DataContract]
        public class Steam
        {

            [DataMember]
            public decimal? ActualPress { get; set; }//S
            [DataMember]
            public decimal? Calciner { get; set; }//S
            [DataMember]
            public decimal? ConsOth { get; set; }//S
            [DataMember]
            public decimal? ConsCombAirPreheat { get; set; }//S
            [DataMember]
            public decimal? ConsCondTurb { get; set; }//S
            [DataMember]
            public decimal? ConsDeaerators { get; set; }//S
            [DataMember]
            public decimal? ConsOthHeaters { get; set; }//S
            [DataMember]
            public decimal? ConsPressControlHigh { get; set; }//S
            [DataMember]
            public decimal? ConsPressControlLow { get; set; }//S
            [DataMember]
            public decimal? ConsProcessCOK { get; set; }//S
            [DataMember]
            public decimal? ConsProcessCDU { get; set; }//S
            [DataMember]
            public decimal? ConsProcessFCC { get; set; }//S
            [DataMember]
            public decimal? ConsProcessOth { get; set; }//S
            [DataMember]
            public decimal? ConsReboil { get; set; }//S
            [DataMember]
            public decimal? ConsFlares { get; set; }//S
            [DataMember]
            public decimal? ConsTracingHeat { get; set; }//S
            [DataMember]
            public decimal? ConsTopTurbHigh { get; set; }//S
            [DataMember]
            public decimal? ConsTopTurbLow { get; set; }//S
            [DataMember]
            public decimal? TotCons { get; set; }//S
            [DataMember]
            public decimal? FiredBoiler { get; set; }//S
            [DataMember]
            public decimal? FiredProcessHeater { get; set; }//S
            [DataMember]
            public decimal? FTCogen { get; set; }//S
            [DataMember]
            public decimal? FCCCatCoolers { get; set; }//S
            [DataMember]
            public decimal? FCCStackGas { get; set; }//S
            [DataMember]
            public decimal? FluidCokerCOBoiler { get; set; }//S
            [DataMember]
            public decimal? H2PlantExport { get; set; }//S
            [DataMember]
            public decimal? OthSource { get; set; }//S
            [DataMember]
            public decimal? Pur { get; set; }//S
            [DataMember]
            public decimal? Sold { get; set; }//S
            [DataMember]
            public decimal? NetPur { get; set; }//S
            [DataMember]
            public decimal? STProd { get; set; }//S
            [DataMember]
            public decimal? TotSupply { get; set; }//S
            [DataMember]
            public decimal? WasteHeatCOK { get; set; }//S
            [DataMember]
            public decimal? WasteHeatFCC { get; set; }//S
            [DataMember]
            public decimal? WasteHeatOth { get; set; }//S
            [DataMember]
            public decimal? WasteHeatTCR { get; set; }//S
            [DataMember]
            public string PressureRange { get; set; }//S
            [DataMember]
            public int? SubmissionID { get; set; }//S    
        }


        [DataContract]
        public class S
        {

            [DataMember]
            public decimal? S1 { get; set; }//S
            [DataMember]
            public decimal? S2 { get; set; }//S
            [DataMember]
            public decimal? S3 { get; set; }//S
            [DataMember]
            public decimal? S4 { get; set; }//S
            [DataMember]
            public decimal? S5 { get; set; }//S
            [DataMember]
            public decimal? S6 { get; set; }//S
            [DataMember]
            public decimal? S7 { get; set; }//S
            [DataMember]
            public decimal? S8 { get; set; }//S
            [DataMember]
            public decimal? S9 { get; set; }//S
            [DataMember]
            public decimal? S10 { get; set; }//S
            [DataMember]
            public decimal? S11 { get; set; }//S
            [DataMember]
            public decimal? S12 { get; set; }//S
            [DataMember]
            public decimal? S13 { get; set; }//S
            [DataMember]
            public decimal? S14 { get; set; }//S
            [DataMember]
            public decimal? S15 { get; set; }//S
            [DataMember]
            public decimal? S16 { get; set; }//S
            [DataMember]
            public decimal? S17 { get; set; }//S
            [DataMember]
            public decimal? S18 { get; set; }//S
            [DataMember]
            public decimal? S19 { get; set; }//S
            [DataMember]
            public decimal? S20 { get; set; }//S
            [DataMember]
            public decimal? S21 { get; set; }//S
            [DataMember]
            public decimal? S22 { get; set; }//S
            [DataMember]
            public decimal? S23 { get; set; }//S
            [DataMember]
            public decimal? S24 { get; set; }//S
            [DataMember]
            public decimal? S25 { get; set; }//S
            [DataMember]
            public decimal? S26 { get; set; }//S
            [DataMember]
            public decimal? S27 { get; set; }//S
            [DataMember]
            public decimal? S28 { get; set; }//S
            [DataMember]
            public decimal? S29 { get; set; }//S
            [DataMember]
            public decimal? S30 { get; set; }//S
            [DataMember]
            public decimal? S31 { get; set; }//S
            [DataMember]
            public decimal? S32 { get; set; }//S
            [DataMember]
            public decimal? S33 { get; set; }//S
            [DataMember]
            public decimal? S34 { get; set; }//S
            [DataMember]
            public decimal? S35 { get; set; }//S
            [DataMember]
            public decimal? S36 { get; set; }//S
            [DataMember]
            public string S37 { get; set; }//S
            [DataMember]
            public int? S38 { get; set; }//S    
        }


        [DataContract]
        public class Submissions
        {
            [DataMember]
            public string RefID { get; set; }//SB
            [DataMember]
            public string RptCurrency { get; set; }//SB
            [DataMember]
            public DateTime? Date { get; set; }//SB
            [DataMember]
            public string UOM { get; set; }//SB
            [DataMember]
            public int? SubmissionID { get; set; }//SB    
        }


        [DataContract]
        public class SB
        {
            [DataMember]
            public string SB1 { get; set; }//SB
            [DataMember]
            public string SB2 { get; set; }//SB
            [DataMember]
            public DateTime? SB3 { get; set; }//SB
            [DataMember]
            public string SB4 { get; set; }//SB
            [DataMember]
            public int? SB5 { get; set; }//SB    
        }


        [DataContract]
        public class Yields
        {
            [DataMember]
            public decimal? BBL { get; set; }//Y
            [DataMember]
            public decimal? Density { get; set; }//Y
            [DataMember]
            public decimal? MT { get; set; }//Y
            [DataMember]
            public string Category { get; set; }//Y
            [DataMember]
            public string MaterialID { get; set; }//Y
            [DataMember]
            public string MaterialName { get; set; }//Y
            [DataMember]
            public string Period { get; set; }//Y
            [DataMember]
            public int? SubmissionID { get; set; }//Y
        }


        [DataContract]
        public class Y
        {
            [DataMember]
            public decimal? Y1 { get; set; }//Y
            [DataMember]
            public decimal? Y2 { get; set; }//Y
            [DataMember]
            public decimal? Y3 { get; set; }//Y
            [DataMember]
            public string Y4 { get; set; }//Y
            [DataMember]
            public string Y5 { get; set; }//Y
            [DataMember]
            public string Y6 { get; set; }//Y
            [DataMember]
            public string Y7 { get; set; }//Y
            [DataMember]
            public int? Y8 { get; set; }//Y
        }

          
    }

