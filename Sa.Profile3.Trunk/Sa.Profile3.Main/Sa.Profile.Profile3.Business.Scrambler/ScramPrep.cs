﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sa.Profile.Profile3.Services.Library;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using Sa.Profile.Profile3.Business.Scrambler.ScramDataSet;
using Sa.Profile.Profile3.Business.PlantFactory;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using Sa.Profile.Profile3.Business.PlantUnits;

namespace Sa.Profile.Profile3.Business.Scrambler
{
    public class ScramPrep
    {
        public FuelsRefineryCompletePlant ClientToBus(NewDataSet dataSetIn)
        {
            string level = "Absence";
            FuelsRefineryCompletePlant retPlant = new FuelsRefineryCompletePlant(new FuelsRefineryPlantUnitFactory(PlantLevel.COMPLETE));
            try
            {
                NewDataSet ds = dataSetIn;
                DataTableExtentions de = new DataTableExtentions();
                if (ds.Absence != null)
                {
                    List<Absence> itms = (from p in de.AsEnumerable2<Absence>(ds.Absence) select p).ToList();
                    foreach (Absence itm in itms)
                    {
                        FuelsRefineryPersonnelAbsenceHoursComplete _FuelsRefineryPersonnelAbsenceHoursComplete = new FuelsRefineryPersonnelAbsenceHoursComplete(); 
                        _FuelsRefineryPersonnelAbsenceHoursComplete.CategoryID = itm.CategoryID; // Absence
                        _FuelsRefineryPersonnelAbsenceHoursComplete.ManagementProfessionalAndStaffAbsence = itm.MPSAbs; // Absence
                        _FuelsRefineryPersonnelAbsenceHoursComplete.OperatorCraftAndClericalAbsence = itm.OCCAbs; // Absence
                        _FuelsRefineryPersonnelAbsenceHoursComplete.SubmissionID = itm.SubmissionID; // Absence
                        retPlant.PersonnelAbsenceHoursCompleteList.Add(_FuelsRefineryPersonnelAbsenceHoursComplete);
                    }
                }
                if (ds.Config != null)
                {
                    level = "Config";
                    List<Config> itms = (from p in de.AsEnumerable2<Config>(ds.Config) select p).ToList();
                    foreach (Config itm in itms)
                    {
                        FuelsRefineryConfigurationComplete _FuelsRefineryConfigurationComplete = new FuelsRefineryConfigurationComplete(); //Config
                        _FuelsRefineryConfigurationComplete.AllocatedPercentOfCapacity = itm.AllocPcntOfCap; // Config
                        _FuelsRefineryConfigurationComplete.BlockOperated = itm.BlockOp; // Config
                        _FuelsRefineryConfigurationComplete.Capacity = itm.Cap; // Config
                        _FuelsRefineryConfigurationComplete.EnergyPercent = itm.EnergyPcnt; // Config
                        _FuelsRefineryConfigurationComplete.InServicePercent = itm.InServicePcnt; // Config
                        _FuelsRefineryConfigurationComplete.ManHourWeek = itm.MHPerWeek; // Config
                        _FuelsRefineryConfigurationComplete.PostPerShift = itm.PostPerShift; // Config
                        _FuelsRefineryConfigurationComplete.ProcessID = itm.ProcessID; // Config
                        _FuelsRefineryConfigurationComplete.ProcessType = itm.ProcessType; // Config
                        _FuelsRefineryConfigurationComplete.SteamCapacity = itm.StmCap; // Config
                        _FuelsRefineryConfigurationComplete.SteamUtilizationPercent = itm.StmUtilPcnt; // Config
                        _FuelsRefineryConfigurationComplete.SubmissionID = itm.SubmissionID; // Config
                        _FuelsRefineryConfigurationComplete.UnitID = itm.UnitID; // Config
                        _FuelsRefineryConfigurationComplete.UnitName = itm.UnitName; // Config
                        _FuelsRefineryConfigurationComplete.UtilizationPercent = itm.UtilPcnt; // Config
                        _FuelsRefineryConfigurationComplete.YearsInOperation = itm.YearsOper; // Config
                        retPlant.ConfigurationCompleteList.Add(_FuelsRefineryConfigurationComplete);
                    }
                }
                if (ds.ConfigBuoy != null)
                {
                    level="ConfigBuoy";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ConfigBuoy> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ConfigBuoy>(ds.ConfigBuoy) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ConfigBuoy itm in itms)
                    {
                        FuelsRefineryConfigurationBuoyComplete _FuelsRefineryConfigurationBuoyComplete = new FuelsRefineryConfigurationBuoyComplete(); //ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.LineSize = itm.LineSize; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.PercentOwnership = itm.PcntOwnership; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.ProcessID = itm.ProcessID; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.ShipCapacity = itm.ShipCap; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.SubmissionID = itm.SubmissionID; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.UnitID = itm.UnitID; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.UnitName = itm.UnitName; // ConfigBuoy
                        retPlant.ConfigurationBuoyCompleteList.Add(_FuelsRefineryConfigurationBuoyComplete);
                    }
                }
                if (ds.Crude != null)
                {level="Crude";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude>(ds.Crude) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude itm in itms)
                    {
                        FuelsRefineryCrudeComplete _FuelsRefineryCrudeComplete = new FuelsRefineryCrudeComplete(); //Crude
                        _FuelsRefineryCrudeComplete.Barrels = itm.BBL; // Crude
                        _FuelsRefineryCrudeComplete.CrudeID = itm.CrudeID; // Crude
                        _FuelsRefineryCrudeComplete.CrudeName = itm.CrudeName; // Crude
                        _FuelsRefineryCrudeComplete.CrudeNumber = itm.CNum; // Crude
                        _FuelsRefineryCrudeComplete.Gravity = itm.Gravity; // Crude
                        _FuelsRefineryCrudeComplete.Period = itm.Period; // Crude
                        _FuelsRefineryCrudeComplete.SubmissionID = itm.SubmissionID; // Crude
                        _FuelsRefineryCrudeComplete.Sulfur = itm.Sulfur; // Crude
                        retPlant.CrudeCompleteList.Add(_FuelsRefineryCrudeComplete);
                    }
                }
                if (ds.ConfigRS != null)
                {level="ConfigRS";
                    List<ConfigR> itms = (from p in de.AsEnumerable2<ConfigR>(ds.ConfigRS) select p).ToList();
                    foreach (ConfigR itm in itms)
                    {
                        FuelsRefineryConfigurationReceiptAndShipmentComplete _FuelsRefineryConfigurationReceiptAndShipmentComplete = new FuelsRefineryConfigurationReceiptAndShipmentComplete(); //ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.AverageSize = itm.AvgSize; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessID = itm.ProcessID; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessType = itm.ProcessType; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.SubmissionID = itm.SubmissionID; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.Throughput = itm.Throughput; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.UnitID = itm.UnitID; // ConfigRS
                        retPlant.ConfigurationReceiptAndShipmentCompleteList.Add(_FuelsRefineryConfigurationReceiptAndShipmentComplete);

                    }
                }
                if (ds.Diesel != null)
                {
                    level = "Diesel";
                    List<DieselData> itms = (from p in de.AsEnumerable2<DieselData>(ds.Diesel) select p).ToList();
                    foreach (DieselData itm in itms)
                    {
                        FuelsRefineryDieselComplete _FuelsRefineryDieselComplete = new FuelsRefineryDieselComplete(); //Diesel
                        _FuelsRefineryDieselComplete.AmericanStandardForTestingMaterials90DistillationPoint = itm.ASTM90; // Diesel
                        _FuelsRefineryDieselComplete.BiodieselInBlendPercent = itm.BiodieselPcnt; // Diesel
                        _FuelsRefineryDieselComplete.BlendID = (int?)itm.BlendID; // Diesel
                        _FuelsRefineryDieselComplete.Cetane = itm.Cetane; // Diesel
                        _FuelsRefineryDieselComplete.CloudPointOfBlend = itm.CloudPt; // Diesel
                        _FuelsRefineryDieselComplete.Density = itm.Density; // Diesel
                        _FuelsRefineryDieselComplete.Grade = itm.Grade; // Diesel
                        _FuelsRefineryDieselComplete.Market = itm.Market; // Diesel
                        _FuelsRefineryDieselComplete.PercentEvaporatedAt350C = itm.E350; // Diesel
                        _FuelsRefineryDieselComplete.PourPointOfBlend = itm.PourPt; // Diesel
                        _FuelsRefineryDieselComplete.SubmissionID = itm.SubmissionID; // Diesel
                        _FuelsRefineryDieselComplete.Sulfur = itm.Sulfur; // Diesel
                        _FuelsRefineryDieselComplete.ThousandMetricTons = itm.KMT; // Diesel
                        _FuelsRefineryDieselComplete.Type = itm.Type; // Diesel
                        retPlant.DieselCompleteList.Add(_FuelsRefineryDieselComplete);
                    }
                }
                if (ds.Emissions != null)
                {
                    level = "Emissions";
                    List<Emission> itms = (from p in de.AsEnumerable2<Emission>(ds.Emissions) select p).ToList();
                    foreach (Emission itm in itms)
                    {
                        FuelsRefineryEmissionComplete _FuelsRefineryEmissionComplete = new FuelsRefineryEmissionComplete(); //Emissions
                        _FuelsRefineryEmissionComplete.EmissionType = itm.EmissionType; // Emissions
                        _FuelsRefineryEmissionComplete.RefineryEmission = itm.RefEmissions; // Emissions
                        _FuelsRefineryEmissionComplete.SubmissionID = itm.SubmissionID; // Emissions
                        retPlant.EmissionCompleteList.Add(_FuelsRefineryEmissionComplete);
                    }
                }
                if (ds.Energy != null)
                {
                    int posElect = 1;
                    int posEnerg = 1;
                    level = "Electric";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy>(ds.Energy) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy itm in itms)
                    {
                        if (itm.EnergyType == "ELE" ||
                           itm.EnergyType == "POW" ||
                           itm.EnergyType == "COG")
                        {
                            FuelsRefineryEnergyElectricBasic _FuelsRefineryEnergyElectricBasic = new FuelsRefineryEnergyElectricBasic(); //Electric

                            _FuelsRefineryEnergyElectricBasic.Amount = itm.Amount; // Electric
                            _FuelsRefineryEnergyElectricBasic.EnergyType = itm.EnergyType; // Electric
                            _FuelsRefineryEnergyElectricBasic.GenerationEfficiency = itm.GenEff; // Electric
                            _FuelsRefineryEnergyElectricBasic.PriceLocal = itm.PriceLocal; // Electric
                            _FuelsRefineryEnergyElectricBasic.SubmissionID = itm.SubmissionID; // Electric
                            _FuelsRefineryEnergyElectricBasic.TransactionType = itm.TransType; // Electric
                            _FuelsRefineryEnergyElectricBasic.TransactionCode = posElect;
                            posElect++;
                            retPlant.ElectricBasicList.Add(_FuelsRefineryEnergyElectricBasic);                     
                        }
                        else//Energy
                        {
                            level = "Energy";
                            FuelsRefineryEnergyComplete _FuelsRefineryEnergyComplete = new FuelsRefineryEnergyComplete(); //Energy
                          //  _electric_2.GenerationEfficiency = itm.GenEff; // Energy
                            _FuelsRefineryEnergyComplete.Amount = itm.Amount; // Energy
                            _FuelsRefineryEnergyComplete.Butane = itm.Butane; // Energy
                            _FuelsRefineryEnergyComplete.Butylenes = itm.Butylenes; // Energy
                            _FuelsRefineryEnergyComplete.C5Plus = itm.C5Plus; // Energy
                            _FuelsRefineryEnergyComplete.CO = itm.CO; // Energy
                            _FuelsRefineryEnergyComplete.CO2 = itm.CO2; // Energy
                            _FuelsRefineryEnergyComplete.EnergyType = itm.EnergyType; // Energy
                            _FuelsRefineryEnergyComplete.Ethane = itm.Ethane; // Energy
                            _FuelsRefineryEnergyComplete.Ethylene = itm.Ethylene; // Energy
                            _FuelsRefineryEnergyComplete.H2S = itm.H2S; // Energy
                            _FuelsRefineryEnergyComplete.Hydrogen = itm.Hydrogen; // Energy
                            _FuelsRefineryEnergyComplete.Isobutane = itm.Isobutane; // Energy
                            _FuelsRefineryEnergyComplete.Methane = itm.Methane; // Energy
                            _FuelsRefineryEnergyComplete.MillionBritishThermalUnitsOut = itm.MBTUOut; // Energy
                            _FuelsRefineryEnergyComplete.N2 = itm.N2; // Energy
                            _FuelsRefineryEnergyComplete.NH3 = itm.NH3; // Energy
                            _FuelsRefineryEnergyComplete.OverrideCalculation = itm.OverrideCalcs; // Energy
                            _FuelsRefineryEnergyComplete.PriceLocal = itm.PriceLocal; // Energy
                            _FuelsRefineryEnergyComplete.Propane = itm.Propane; // Energy
                            _FuelsRefineryEnergyComplete.Propylene = itm.Propylene; // Energy
                            _FuelsRefineryEnergyComplete.SO2 = itm.SO2; // Energy
                            _FuelsRefineryEnergyComplete.SubmissionID = itm.SubmissionID; // Energy
                            _FuelsRefineryEnergyComplete.TransactionType = itm.TransType; // Energy
                            _FuelsRefineryEnergyComplete.TransactionCode = posEnerg;
                            posEnerg++;
                            retPlant.EnergyCompleteList.Add(_FuelsRefineryEnergyComplete);
                        }
                    }
                }
                if (ds.FiredHeaters != null)
                {
                    level = "FiredHeaters";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.FiredHeater> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.FiredHeater>(ds.FiredHeaters) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.FiredHeater itm in itms)
                    {
                        FuelsRefineryFiredHeatersComplete _FuelsRefineryFiredHeatersComplete = new FuelsRefineryFiredHeatersComplete(); //FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.AbsorbedDuty = itm.AbsorbedDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.CombustionAirTemperature = itm.CombAirTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FiredDuty = itm.FiredDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FuelType = itm.FuelType; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FurnaceInTemperature = itm.FurnInTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FurnaceOutTemperature = itm.FurnOutTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.HeaterName = itm.HeaterName; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.HeaterNumber = itm.HeaterNo; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.HeatLossPercent = itm.HeatLossPcnt; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.OtherCombustionDuty = itm.OthCombDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.OtherDuty = itm.OtherDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ProcessDuty = itm.ProcessDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ProcessFluid = itm.ProcessFluid; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ProcessID = itm.ProcessID; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.Service = itm.Service; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ShaftDuty = itm.ShaftDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.StackO2 = itm.StackO2; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.StackTemperature = itm.StackTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.SteamDuty = itm.SteamDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.SteamSuperHeated = itm.SteamSuperHeated; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.SubmissionID = itm.SubmissionID; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ThroughputReport = itm.ThroughputRpt; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ThroughputUnitOfMeasure = itm.ThroughputUOM; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.UnitID = itm.UnitID; // FiredHeaters
                        retPlant.FiredHeatersCompleteList.Add(_FuelsRefineryFiredHeatersComplete);
                    }
                }
                if (ds.Gasoline != null)
                {
                    level = "Gasoline";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Gasoline> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Gasoline>(ds.Gasoline) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Gasoline itm in itms)
                    {
                        FuelsRefineryGasolineComplete _FuelsRefineryGasolineComplete = new FuelsRefineryGasolineComplete(); //Gasoline
                        _FuelsRefineryGasolineComplete.BlendID = (int?)itm.BlendID; // Gasoline
                        _FuelsRefineryGasolineComplete.Density = itm.Density; // Gasoline
                        _FuelsRefineryGasolineComplete.Ethanol = itm.Ethanol; // Gasoline
                        _FuelsRefineryGasolineComplete.EthylTertButylEther = itm.ETBE; // Gasoline
                        _FuelsRefineryGasolineComplete.Grade = itm.Grade; // Gasoline
                        _FuelsRefineryGasolineComplete.Lead = itm.Lead; // Gasoline
                        _FuelsRefineryGasolineComplete.Market = itm.Market; // Gasoline
                        _FuelsRefineryGasolineComplete.MethylTertButylEther = itm.MTBE; // Gasoline
                        _FuelsRefineryGasolineComplete.MotorOctaneNumber = itm.MON; // Gasoline
                        _FuelsRefineryGasolineComplete.OtherOxygen = itm.OthOxygen; // Gasoline
                        _FuelsRefineryGasolineComplete.Oxygen = itm.Oxygen; // Gasoline
                        _FuelsRefineryGasolineComplete.ReidVaporPressure = itm.RVP; // Gasoline
                        _FuelsRefineryGasolineComplete.ResearchOctaneNumber = itm.RON; // Gasoline
                        _FuelsRefineryGasolineComplete.SubmissionID = itm.SubmissionID; // Gasoline
                        _FuelsRefineryGasolineComplete.Sulfur = itm.Sulfur; // Gasoline
                        _FuelsRefineryGasolineComplete.TertAmylMethylEther = itm.TAME; // Gasoline
                        _FuelsRefineryGasolineComplete.ThousandMetricTons = itm.KMT; // Gasoline
                        _FuelsRefineryGasolineComplete.Type = itm.Type; // Gasoline
                        retPlant.GasolineCompleteList.Add(_FuelsRefineryGasolineComplete);
                    }
                }

                if (ds.GeneralMisc != null)
                {
                    level = "GeneralMisc";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.GeneralMisc> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.GeneralMisc>(ds.GeneralMisc) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.GeneralMisc itm in itms)
                    {
                        FuelsRefineryGeneralMiscellaneousComplete _FuelsRefineryGeneralMiscellaneousComplete = new FuelsRefineryGeneralMiscellaneousComplete(); //GeneralMisc
                        _FuelsRefineryGeneralMiscellaneousComplete.FlareLossMetricTons = itm.FlareLossMT; // GeneralMisc
                        _FuelsRefineryGeneralMiscellaneousComplete.SubmissionID = itm.SubmissionID; // GeneralMisc
                        _FuelsRefineryGeneralMiscellaneousComplete.TotalLossMetricTons = itm.TotLossMT; // GeneralMisc
                        retPlant.GeneralMiscellaneousCompleteList.Add(_FuelsRefineryGeneralMiscellaneousComplete);
                    }
                }
                if (ds.Inventory != null)
                {
                    level = "Inventory";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory>(ds.Inventory) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory itm in itms)
                    {
                        FuelsRefineryInventoryComplete _FuelsRefineryInventoryComplete = new FuelsRefineryInventoryComplete(); //Inventory
                        _FuelsRefineryInventoryComplete.AverageLevel = itm.AvgLevel; // Inventory
                        _FuelsRefineryInventoryComplete.LeasedPercent = itm.LeasedPcnt; // Inventory
                        _FuelsRefineryInventoryComplete.ManditoryStorageLevel = itm.MandStorage; // Inventory
                        _FuelsRefineryInventoryComplete.MarketingStorage = itm.MktgStorage; // Inventory
                        _FuelsRefineryInventoryComplete.RefineryStorage = itm.RefStorage; // Inventory
                        _FuelsRefineryInventoryComplete.SubmissionID = itm.SubmissionID; // Inventory
                        _FuelsRefineryInventoryComplete.TankNumber = (int?)itm.NumTank; // Inventory
                        _FuelsRefineryInventoryComplete.TankType = itm.TankType; // Inventory
                        _FuelsRefineryInventoryComplete.TotalStorage = itm.TotStorage; // Inventory
                        retPlant.InventoryCompleteList.Add(_FuelsRefineryInventoryComplete);
                    }
                }
                if (ds.Kerosene != null)
                {
                    level = "Kerosene";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Kerosene> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Kerosene>(ds.Kerosene) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Kerosene itm in itms)
                    {
                        FuelsRefineryKeroseneComplete _FuelsRefineryKeroseneComplete = new FuelsRefineryKeroseneComplete(); //Kerosene
                        _FuelsRefineryKeroseneComplete.BlendID = (int?)itm.BlendID; // Kerosene
                        _FuelsRefineryKeroseneComplete.Density = itm.Density; // Kerosene
                        _FuelsRefineryKeroseneComplete.Grade = itm.Grade; // Kerosene
                        _FuelsRefineryKeroseneComplete.SubmissionID = itm.SubmissionID; // Kerosene
                        _FuelsRefineryKeroseneComplete.Sulfur = itm.Sulfur; // Kerosene
                        _FuelsRefineryKeroseneComplete.ThousandMetricTons = itm.KMT; // Kerosene
                        _FuelsRefineryKeroseneComplete.Type = itm.Type; // Kerosene
                        retPlant.KeroseneCompleteList.Add(_FuelsRefineryKeroseneComplete);
                    }
                }
                if (ds.LPG != null)
                {
                    level = "LPG";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.LPG> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.LPG>(ds.LPG) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.LPG itm in itms)
                    {
                        FuelsRefineryLiquifiedPetroleumGasComplete _FuelsRefineryLiquifiedPetroleumGasComplete = new FuelsRefineryLiquifiedPetroleumGasComplete(); //LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.BlendID = (int?)itm.BlendID; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.MoleOrVolume = itm.MolOrVol; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.SubmissionID = itm.SubmissionID; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC2LT = itm.VolC2Lt; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3 = itm.VolC3; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3ene = itm.VolC3ene; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC4ene = itm.VolC4ene; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC5Plus = itm.VolC5Plus; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeiC4 = itm.VoliC4; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumenC4 = itm.VolnC4; // LPG
                        retPlant.LiquifiedPetroleumGasCompleteList.Add(_FuelsRefineryLiquifiedPetroleumGasComplete);
                    }
                }
                if (ds.MarineBunkers != null)
                {
                    level = "MarineBunkers";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MarineBunker> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MarineBunker>(ds.MarineBunkers) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MarineBunker itm in itms)
                    {
                        FuelsRefineryMarineBunkersComplete _FuelsRefineryMarineBunkersComplete = new FuelsRefineryMarineBunkersComplete(); //MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.BlendID = (int?)itm.BlendID; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.CrackedStock = itm.CrackedStock; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.Density = itm.Density; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.Grade = itm.Grade; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.PourPointOfBlend = itm.PourPt; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.SubmissionID = itm.SubmissionID; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.Sulfur = itm.Sulfur; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.ThousandMetricTons = itm.KMT; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendCentiStokeAtTemperature = itm.ViscCSAtTemp; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendTemperatureDifferent122 = itm.ViscTemp; // MarineBunkers
                        retPlant.MarineBunkersCompleteList.Add(_FuelsRefineryMarineBunkersComplete);
                    }
                }
                if (ds.MExp != null)
                {level="MExp";
                    List<MExp> itms = (from p in de.AsEnumerable2<MExp>(ds.MExp) select p).ToList();
                    foreach (MExp itm in itms)
                    {
                        FuelsRefineryMaintenanceExpenseAndCapitalComplete _FuelsRefineryMaintenanceExpenseAndCapitalComplete = new FuelsRefineryMaintenanceExpenseAndCapitalComplete(); //MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.ConstraintRemoval = itm.ConstraintRemoval; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.Energy = itm.Energy; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseRoutine = itm.MaintExpRout; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseTurnaround = itm.MaintExpTA; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadRoutine = itm.MaintOvhdRout; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadTurnaround = itm.MaintOvhdTA; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonMaintenenceInvestmentExpense = itm.NonMaintInvestExp; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRefineryExclusions = itm.NonRefExcl; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRegulatoryNewProcessUnitConstruction = itm.NonRegUnit; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.OtherCapitalInvestment = itm.OthInvest; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryDiesel = itm.RegDiesel; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryExpense = itm.RegExp; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryGasoline = itm.RegGaso; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryOther = itm.RegOth; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RoutineMaintenanceCapital = itm.RoutMaintCptl; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SafetyOther = itm.SafetyOth; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SubmissionID = itm.SubmissionID; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalComplexCapital = itm.ComplexCptl; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenance = itm.MaintExp; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceExpense = itm.TotMaint; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceOverhead = itm.MaintOvhd; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TurnaroundMaintenanceCapital = itm.TAMaintCptl; // MExp
                        retPlant.MaintenanceExpenseAndCapitalCompleteList.Add(_FuelsRefineryMaintenanceExpenseAndCapitalComplete);
                    }
                }
                if (ds.MiscInput != null)
                {
                    level = "MiscInput";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MiscInput> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MiscInput>(ds.MiscInput) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MiscInput itm in itms)
                    {
                        FuelsRefineryMiscellaneousInputComplete _FuelsRefineryMiscellaneousInputComplete = new FuelsRefineryMiscellaneousInputComplete(); //MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.BarrelsInputToCrudeUnits = itm.CDUChargeBbl; // MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.MetricTonsInputToCrudeUnits = itm.CDUChargeMT; // MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.OffsiteEnergyPercent = itm.OffsiteEnergyPcnt; // MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.SubmissionID = itm.SubmissionID; // MiscInput
                        retPlant.MiscellaneousInputCompleteList.Add(_FuelsRefineryMiscellaneousInputComplete);
                    }
                }
                if (ds.MaintRout != null)
                {
                    level = "MaintRout";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MaintRout> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MaintRout>(ds.MaintRout) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MaintRout itm in itms)
                    {
                        FuelsRefineryRoutineMaintenanceComplete _FuelsRefineryRoutineMaintenanceComplete = new FuelsRefineryRoutineMaintenanceComplete(); //MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.CapitalInLocal = itm.RoutCptlLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.CostInLocal = itm.RoutCostLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.ExpenseInLocal = itm.RoutExpLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.InServicePercent = itm.InServicePcnt; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.MaintenanceHoursDown = itm.MaintDown; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.MaintenanceNumberOfDowntime = (int?)itm.MaintNum; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOffsiteUpsets = itm.OthDownOffsiteUpsets; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOther = itm.OthDownOther; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDown = itm.OthDown; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownEconomic = itm.OthDownEconomic; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownExternal = itm.OthDownExternal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownUnitUpsets = itm.OthDownUnitUpsets; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherNumberOfDowntime = (int?)itm.OthNum; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherSlowdownHours = itm.OthSlow; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OverheadInLocal = itm.RoutOvhdLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.PercentUtilization = itm.UtilPcnt; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.ProcessID = itm.ProcessID; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.RegulatoryHoursDown = itm.RegDown; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.RegulatoryNumberOfDowntime = (int?)itm.RegNum; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.SubmissionID = itm.SubmissionID; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.UnitID = itm.UnitID; // MaintRout
                        retPlant.RoutineMaintenanceCompleteList.Add(_FuelsRefineryRoutineMaintenanceComplete);
                    }
                }
                if (ds.MaintTA != null)
                {
                    level = "MaintTA";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MaintTA> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MaintTA>(ds.MaintTA) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.MaintTA itm in itms)
                    {
                        FuelsRefineryTurnaroundMaintenanceComplete _FuelsRefineryTurnaroundMaintenanceComplete = new FuelsRefineryTurnaroundMaintenanceComplete(); //MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.CapitalLocal = itm.TACptlLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ContractorManagementAndProfessionalSalaried = itm.TAContMPS; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ContractorOperatorCraftAndClericalHours = itm.TAContOCC; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.CostLocal = itm.TACostLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.Exceptions = (int?)itm.TAExceptions; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ExpenseLocal = itm.TAExpLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.HoursDown = itm.TAHrsDown; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.LaborCostLocal = itm.TALaborCostLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalOvertimePercent = itm.TAMPSOVTPcnt; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalSalariedStraightTimeHours = itm.TAMPSSTH; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalOvertimeHours = itm.TAOCCOVT; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalStraightTimeHours = itm.TAOCCSTH; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.OverheadLocal = itm.TAOvhdLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.PreviousTurnaroundDate = (DateTime?)itm.PrevTADate; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ProcessID = itm.ProcessID; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.SubmissionID = itm.SubmissionID; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.TurnaroundDate = (DateTime?)itm.TADate; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.UnitID = itm.UnitID; // MaintTA               
                        retPlant.TurnaroundMaintenanceCompleteList.Add(_FuelsRefineryTurnaroundMaintenanceComplete);
                    }
                }
                if (ds.OpexData != null)
                {
                    level = "OpexData";
                    List<OpexData> itms = (from p in de.AsEnumerable2<OpexData>(ds.OpexData) select p).ToList();
                    foreach (OpexData itm in itms)
                    {
                        OperatingExpenseStandard _OperatingExpenseStandard = new OperatingExpenseStandard(); //OpexData
                        _OperatingExpenseStandard.OtherDescription = itm.OthDescription; // OpexData
                        _OperatingExpenseStandard.Property = itm.Property; // OpexData
                        _OperatingExpenseStandard.ReportValue = itm.RptValue; // OpexData
                        _OperatingExpenseStandard.SubmissionID = itm.SubmissionID; // OpexData
                        retPlant.OperatingExpenseStandardList.Add(_OperatingExpenseStandard);
                    }
                }
                if (ds.Pers != null)
                {
                    level = "Pers";
                    List<Per> itms = (from p in de.AsEnumerable2<Per>(ds.Pers) select p).ToList();
                    foreach (Per itm in itms)
                    {
                        FuelsRefineryPersonnelHoursComplete _FuelsRefineryPersonnelHoursComplete = new FuelsRefineryPersonnelHoursComplete(); //Pers
                        _FuelsRefineryPersonnelHoursComplete.AbsenceHours = itm.AbsHrs; // Pers
                        _FuelsRefineryPersonnelHoursComplete.Contract = itm.Contract; // Pers
                        _FuelsRefineryPersonnelHoursComplete.GeneralAndAdministrativeHours = itm.GA; // Pers
                        _FuelsRefineryPersonnelHoursComplete.NumberOfPersonnel = itm.NumPers; // Pers
                        _FuelsRefineryPersonnelHoursComplete.OvertimeHours = itm.OVTHours; // Pers
                        _FuelsRefineryPersonnelHoursComplete.OvertimePercent = itm.OVTPcnt; // Pers
                        _FuelsRefineryPersonnelHoursComplete.PersonnelHoursID = itm.PersID; // Pers
                        _FuelsRefineryPersonnelHoursComplete.StraightTimeHours = itm.STH; // Pers
                        _FuelsRefineryPersonnelHoursComplete.SubmissionID = itm.SubmissionID; // Pers
                        retPlant.PersonnelHoursCompleteList.Add(_FuelsRefineryPersonnelHoursComplete);
                    }
                }
                if (ds.ProcessData != null)
                {
                    level = "ProcessData";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ProcessData> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ProcessData>(ds.ProcessData) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ProcessData itm in itms)
                    {

                        PlantOperatingConditionBasic _PlantOperatingConditionBasic = new PlantOperatingConditionBasic(); //ProcessData
                        _PlantOperatingConditionBasic.Property = itm.Property; // ProcessData
                        _PlantOperatingConditionBasic.ReportDateValue = (DateTime?)itm.RptDVal; // ProcessData
                        _PlantOperatingConditionBasic.ReportNumericValue = itm.RptNVal; // ProcessData
                        _PlantOperatingConditionBasic.ReportTextValue = itm.RptTVal; // ProcessData
                        _PlantOperatingConditionBasic.SubmissionID = itm.SubmissionID; // ProcessData
                        _PlantOperatingConditionBasic.UnitID = itm.UnitID; // ProcessData
                        _PlantOperatingConditionBasic.UnitOfMeasure = itm.UOM; // ProcessData
                        retPlant.PlantOperatingConditionList.Add(_PlantOperatingConditionBasic);
                    }
                }
                if (ds.Resid != null)
                {
                    level = "Resid";
                    List<Resid> itms = (from p in de.AsEnumerable2<Resid>(ds.Resid) select p).ToList();
                    foreach (Resid itm in itms)
                    {
                        FuelsRefineryFinishedResidualFuelComplete _FuelsRefineryFinishedResidualFuelComplete = new FuelsRefineryFinishedResidualFuelComplete(); //Resid
                        _FuelsRefineryFinishedResidualFuelComplete.BlendID = (int?)itm.BlendID; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.Density = itm.Density; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.Grade = itm.Grade; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.PourPointOfBlend = itm.PourPT; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.SubmissionID = itm.SubmissionID; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.Sulfur = itm.Sulfur; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.ThousandMetricTons = itm.KMT; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendCentiStokeAtTemperature = itm.ViscCSAtTemp; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendTemperatureDifferent122 = itm.ViscTemp; // Resid
                        retPlant.FinishedResidualFuelCompleteList.Add(_FuelsRefineryFinishedResidualFuelComplete);
                    }
                }
                if (ds.RPFResid != null)
                {
                    level = "RPFResid";

                    List<RPFResid> itms = (from p in de.AsEnumerable2<RPFResid>(ds.RPFResid) select p).ToList();
                    foreach (RPFResid itm in itms)
                    {
                        FuelsRefineryProducedFuelResidualComplete _FuelsRefineryProducedFuelResidualComplete = new FuelsRefineryProducedFuelResidualComplete(); //RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.Density = itm.Density; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.EnergyType = itm.EnergyType; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.SubmissionID = itm.SubmissionID; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.Sulfur = itm.Sulfur; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.ViscosityCentiStokesAtTemperature = itm.ViscCSAtTemp; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.ViscosityTemperature = itm.ViscTemp; // RPFResid
                        retPlant.RefineryProducedFuelResidualCompleteList.Add(_FuelsRefineryProducedFuelResidualComplete);
                    }
                }
                if (ds.SteamSystem != null)
                {
                    level = "SteamSystem";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem>(ds.SteamSystem) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem itm in itms)
                    {
                        FuelsRefinerySteamSystemComplete _FuelsRefinerySteamSystemComplete = new FuelsRefinerySteamSystemComplete(); //SteamSystem
                        _FuelsRefinerySteamSystemComplete.ActualPressure = itm.ActualPress; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.Calciner = itm.Calciner; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedAllOther = itm.ConsOth; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedCombustionAirPreheat = itm.ConsCombAirPreheat; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedCondensingTurbines = itm.ConsCondTurb; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedDeaerators = itm.ConsDeaerators; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedOtherHeaters = itm.ConsOthHeaters; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedPressureControlFromHigherPressure = itm.ConsPressControlHigh; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedPressureControlToLowerPressure = itm.ConsPressControlLow; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessCoker = itm.ConsProcessCOK; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessCrudeAndVacuum = itm.ConsProcessCDU; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessFluidCatalystCracker = itm.ConsProcessFCC; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessOther = itm.ConsProcessOth; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedReboilersEvaporators = itm.ConsReboil; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedSteamToFlares = itm.ConsFlares; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedSteamTracingTankBuildingHeat = itm.ConsTracingHeat; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionFromHigherPressure = itm.ConsTopTurbHigh; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionToLowerPressure = itm.ConsTopTurbLow; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedTotalSteam = itm.TotCons; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FiredBoiler = itm.FiredBoiler; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FiredProcessHeaterConvectionSection = itm.FiredProcessHeater; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FiredTurbineCogeneration = itm.FTCogen; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerCoolers = itm.FCCCatCoolers; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerStackGas = itm.FCCStackGas; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FluidCokerCOBoiler = itm.FluidCokerCOBoiler; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.H2PlantNetExportSteam = itm.H2PlantExport; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.OtherSteamSources = itm.OthSource; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.PressureRange = itm.PressureRange; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.Purchased = itm.Pur; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.Sold = itm.Sold; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.SubmissionID = itm.SubmissionID; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.SubtotalNetPurchased = itm.NetPur; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.SubtotalSteamProduction = itm.STProd; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.TotalSupply = itm.TotSupply; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatCokerHeavyGasOilPumparound = itm.WasteHeatCOK; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatFluidCatalystCracker = itm.WasteHeatFCC; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatOtherBoilers = itm.WasteHeatOth; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatThermalCracker = itm.WasteHeatTCR; // SteamSystem
                        retPlant.SteamSystemCompleteList.Add(_FuelsRefinerySteamSystemComplete);
                    }
                }
                if (ds.Submission != null)
                {
                    level = "Submissions";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission>(ds.Submission) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission itm in itms)
                    {
                        SubmissionBasic _SubmissionBasic = new SubmissionBasic(); //Submissions
                        _SubmissionBasic.RefineryID = itm.RefID; // Submissions
                        _SubmissionBasic.ReportCurrency = itm.RptCurrency; // Submissions
                        _SubmissionBasic.SubmissionDate = (DateTime?)itm.Date; // Submissions
                        _SubmissionBasic.SubmissionID = itm.SubmissionID; // Submissions
                        _SubmissionBasic.UnitOfMeasure = itm.UOM; // Submissions
                        retPlant.PlantSubmission = _SubmissionBasic;
                    }
                }
                if (ds.Yield != null)
                {
                    level = "Yield";

                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Yield> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Yield>(ds.Yield) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Yield itm in itms)
                    {
                        FuelsRefineryMaterialBalanceComplete _FuelsRefineryMaterialBalanceComplete = new FuelsRefineryMaterialBalanceComplete(); //Yield
                        _FuelsRefineryMaterialBalanceComplete.Barrels = itm.BBL; // Yield
                        _FuelsRefineryMaterialBalanceComplete.Category = itm.Category; // Yield
                        _FuelsRefineryMaterialBalanceComplete.Density = itm.Density; // Yield
                        _FuelsRefineryMaterialBalanceComplete.MaterialID = itm.MaterialID; // Yield
                        _FuelsRefineryMaterialBalanceComplete.MaterialName = itm.MaterialName; // Yield
                        _FuelsRefineryMaterialBalanceComplete.MetricTons = itm.MT; // Yield
                        _FuelsRefineryMaterialBalanceComplete.Period = itm.Period; // Yield
                        _FuelsRefineryMaterialBalanceComplete.SubmissionID = itm.SubmissionID; // Yield
                        retPlant.MaterialBalanceCompleteList.Add(_FuelsRefineryMaterialBalanceComplete);
                    }
                }                           
            }         
            catch (Exception ex)
            {
                string template = "ClientToBusError:Level={0} Error:{1}";
                Exception infoException = new Exception(string.Format(template,level,ex.Message));
                throw infoException;
            }
            return retPlant;
        }

        public FuelsRefineryCompletePlant WCFToBus(PL plantIn)
        {
            string level = "CB";
            FuelsRefineryCompletePlant retPlant = new FuelsRefineryCompletePlant(new FuelsRefineryPlantUnitFactory(PlantLevel.COMPLETE));                  
            try
            {
                foreach (Sa.Profile.Profile3.Services.Library.ConfigBuoy _CB in plantIn.CB_List)
                {
                    FuelsRefineryConfigurationBuoyComplete _FuelsRefineryConfigurationBuoyComplete = new FuelsRefineryConfigurationBuoyComplete();                  
                    _FuelsRefineryConfigurationBuoyComplete.LineSize = _CB.LineSize;
                    _FuelsRefineryConfigurationBuoyComplete.PercentOwnership = _CB.PcntOwnership;
                    _FuelsRefineryConfigurationBuoyComplete.ProcessID = _CB.ProcessID;
                    _FuelsRefineryConfigurationBuoyComplete.ShipCapacity = _CB.ShipCap;
                    _FuelsRefineryConfigurationBuoyComplete.SubmissionID = _CB.SubmissionID;
                    _FuelsRefineryConfigurationBuoyComplete.UnitID = _CB.UnitID;
                    _FuelsRefineryConfigurationBuoyComplete.UnitName = _CB.UnitName;
                    retPlant.ConfigurationBuoyCompleteList.Add(_FuelsRefineryConfigurationBuoyComplete);
                }
                level = "C";
                foreach (Sa.Profile.Profile3.Services.Library.Configuration _C in plantIn.C_List)
                {
                    FuelsRefineryConfigurationComplete _FuelsRefineryConfigurationComplete = new FuelsRefineryConfigurationComplete();
                    _FuelsRefineryConfigurationComplete.AllocatedPercentOfCapacity = _C.AllocPcntOfCap;
                    _FuelsRefineryConfigurationComplete.BlockOperated = _C.BlockOp;
                    _FuelsRefineryConfigurationComplete.Capacity = _C.Cap;
                    _FuelsRefineryConfigurationComplete.EnergyPercent = _C.EnergyPcnt;
                    _FuelsRefineryConfigurationComplete.InServicePercent = _C.InServicePcnt;
                    _FuelsRefineryConfigurationComplete.ManHourWeek = _C.MHPerWeek;
                    _FuelsRefineryConfigurationComplete.PostPerShift = _C.PostPerShift;
                    _FuelsRefineryConfigurationComplete.ProcessID = _C.ProcessID;
                    _FuelsRefineryConfigurationComplete.ProcessType = _C.ProcessType;
                    _FuelsRefineryConfigurationComplete.SteamCapacity = _C.StmCap;
                    _FuelsRefineryConfigurationComplete.SteamUtilizationPercent = _C.StmUtilPcnt;
                    _FuelsRefineryConfigurationComplete.SubmissionID = _C.SubmissionID;
                    _FuelsRefineryConfigurationComplete.UnitID = _C.UnitID;
                    _FuelsRefineryConfigurationComplete.UnitName = _C.UnitName;
                    _FuelsRefineryConfigurationComplete.UtilizationPercent = _C.UtilPcnt;
                    _FuelsRefineryConfigurationComplete.YearsInOperation = _C.YearsOper;
                    retPlant.ConfigurationCompleteList.Add(_FuelsRefineryConfigurationComplete);
                }
                level = "CR";
                foreach (Sa.Profile.Profile3.Services.Library.ConfigRS _CR in plantIn.CR_List)
                {
                    FuelsRefineryConfigurationReceiptAndShipmentComplete _FuelsRefineryConfigurationReceiptAndShipmentComplete = new FuelsRefineryConfigurationReceiptAndShipmentComplete();
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.AverageSize = _CR.AvgSize;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessID = _CR.ProcessID;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessType = _CR.ProcessType;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.SubmissionID = _CR.SubmissionID;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.Throughput = _CR.Throughput;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.UnitID = _CR.UnitID;
                    retPlant.ConfigurationReceiptAndShipmentCompleteList.Add(_FuelsRefineryConfigurationReceiptAndShipmentComplete);
                }
                level = "CD";
                foreach (Sa.Profile.Profile3.Services.Library.Crude _CD in plantIn.CD_List)
                {
                    FuelsRefineryCrudeComplete _FuelsRefineryCrudeComplete = new FuelsRefineryCrudeComplete();
                    _FuelsRefineryCrudeComplete.Barrels = _CD.BBL;
                    _FuelsRefineryCrudeComplete.CrudeID = _CD.CrudeID;
                    _FuelsRefineryCrudeComplete.CrudeName = _CD.CrudeName;
                    _FuelsRefineryCrudeComplete.CrudeNumber = _CD.CrudeNumber;
                    _FuelsRefineryCrudeComplete.Gravity = _CD.Gravity;
                    _FuelsRefineryCrudeComplete.Period = _CD.Period;
                    _FuelsRefineryCrudeComplete.SubmissionID = _CD.SubmissionID;
                    _FuelsRefineryCrudeComplete.Sulfur = _CD.Sulfur;
                    retPlant.CrudeCompleteList.Add(_FuelsRefineryCrudeComplete);
                }
                level = "D";
                foreach (Sa.Profile.Profile3.Services.Library.DieselData _D in plantIn.D_List)
                {
                    FuelsRefineryDieselComplete _FuelsRefineryDieselComplete = new FuelsRefineryDieselComplete();
                    _FuelsRefineryDieselComplete.AmericanStandardForTestingMaterials90DistillationPoint = _D.ASTM90;
                    _FuelsRefineryDieselComplete.BiodieselInBlendPercent = _D.BiodieselPcnt;
                    _FuelsRefineryDieselComplete.BlendID = _D.BlendID;
                    _FuelsRefineryDieselComplete.Cetane = _D.Cetane;
                    _FuelsRefineryDieselComplete.CloudPointOfBlend = _D.CloudPt;
                    _FuelsRefineryDieselComplete.Density = _D.Density;
                    _FuelsRefineryDieselComplete.Grade = _D.Grade;
                    _FuelsRefineryDieselComplete.Market = _D.Market;
                    _FuelsRefineryDieselComplete.PercentEvaporatedAt350C = _D.E350;
                    _FuelsRefineryDieselComplete.PourPointOfBlend = _D.PourPt;
                    _FuelsRefineryDieselComplete.SubmissionID = _D.SubmissionID;
                    _FuelsRefineryDieselComplete.Sulfur = _D.Sulfur;
                    _FuelsRefineryDieselComplete.ThousandMetricTons = _D.KMT;
                    _FuelsRefineryDieselComplete.Type = _D.Type;
                    retPlant.DieselCompleteList.Add(_FuelsRefineryDieselComplete);
                }
                level = "E";
                foreach (Sa.Profile.Profile3.Services.Library.Emissions _E in plantIn.E_List)
                {
                    FuelsRefineryEmissionComplete _FuelsRefineryEmissionComplete = new FuelsRefineryEmissionComplete();
                    _FuelsRefineryEmissionComplete.EmissionType = _E.EmissionType;
                    _FuelsRefineryEmissionComplete.RefineryEmission = _E.RefineryEmission;
                    _FuelsRefineryEmissionComplete.SubmissionID = _E.SubmissionID;
                    retPlant.EmissionCompleteList.Add(_FuelsRefineryEmissionComplete);
                }
                level = "S";
                foreach (Sa.Profile.Profile3.Services.Library.Steam _S in plantIn.S_List)
                {
                    FuelsRefinerySteamSystemComplete _FuelsRefinerySteamSystemComplete = new FuelsRefinerySteamSystemComplete();
                    _FuelsRefinerySteamSystemComplete.ActualPressure = _S.ActualPress;
                    _FuelsRefinerySteamSystemComplete.Calciner = _S.Calciner;
                    _FuelsRefinerySteamSystemComplete.ConsumedAllOther = _S.ConsOth;
                    _FuelsRefinerySteamSystemComplete.ConsumedCombustionAirPreheat = _S.ConsCombAirPreheat;
                    _FuelsRefinerySteamSystemComplete.ConsumedCondensingTurbines = _S.ConsCondTurb;
                    _FuelsRefinerySteamSystemComplete.ConsumedDeaerators = _S.ConsDeaerators;
                    _FuelsRefinerySteamSystemComplete.ConsumedOtherHeaters = _S.ConsOthHeaters;
                    _FuelsRefinerySteamSystemComplete.ConsumedPressureControlFromHigherPressure = _S.ConsPressControlHigh;
                    _FuelsRefinerySteamSystemComplete.ConsumedPressureControlToLowerPressure = _S.ConsPressControlLow;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessCoker = _S.ConsProcessCOK;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessCrudeAndVacuum = _S.ConsProcessCDU;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessFluidCatalystCracker = _S.ConsProcessFCC;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessOther = _S.ConsProcessOth;
                    _FuelsRefinerySteamSystemComplete.ConsumedReboilersEvaporators = _S.ConsReboil;
                    _FuelsRefinerySteamSystemComplete.ConsumedSteamToFlares = _S.ConsFlares;
                    _FuelsRefinerySteamSystemComplete.ConsumedSteamTracingTankBuildingHeat = _S.ConsTracingHeat;
                    _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionFromHigherPressure = _S.ConsTopTurbHigh;
                    _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionToLowerPressure = _S.ConsTopTurbLow;
                    _FuelsRefinerySteamSystemComplete.ConsumedTotalSteam = _S.TotCons;
                    _FuelsRefinerySteamSystemComplete.FiredBoiler = _S.FiredBoiler;
                    _FuelsRefinerySteamSystemComplete.FiredProcessHeaterConvectionSection = _S.FiredProcessHeater;
                    _FuelsRefinerySteamSystemComplete.FiredTurbineCogeneration = _S.FTCogen;
                    _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerCoolers = _S.FCCCatCoolers;
                    _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerStackGas = _S.FCCStackGas;
                    _FuelsRefinerySteamSystemComplete.FluidCokerCOBoiler = _S.FluidCokerCOBoiler;
                    _FuelsRefinerySteamSystemComplete.H2PlantNetExportSteam = _S.H2PlantExport;
                    _FuelsRefinerySteamSystemComplete.OtherSteamSources = _S.OthSource;
                    _FuelsRefinerySteamSystemComplete.PressureRange = _S.PressureRange;
                    _FuelsRefinerySteamSystemComplete.Purchased = _S.Pur;
                    _FuelsRefinerySteamSystemComplete.Sold = _S.Sold;
                    _FuelsRefinerySteamSystemComplete.SubmissionID = _S.SubmissionID;
                    _FuelsRefinerySteamSystemComplete.SubtotalNetPurchased = _S.NetPur;
                    _FuelsRefinerySteamSystemComplete.SubtotalSteamProduction = _S.STProd;
                    _FuelsRefinerySteamSystemComplete.TotalSupply = _S.TotSupply;
                    _FuelsRefinerySteamSystemComplete.WasteHeatCokerHeavyGasOilPumparound = _S.WasteHeatCOK;
                    _FuelsRefinerySteamSystemComplete.WasteHeatFluidCatalystCracker = _S.WasteHeatFCC;
                    _FuelsRefinerySteamSystemComplete.WasteHeatOtherBoilers = _S.WasteHeatOth;
                    _FuelsRefinerySteamSystemComplete.WasteHeatThermalCracker = _S.WasteHeatTCR;
                    retPlant.SteamSystemCompleteList.Add(_FuelsRefinerySteamSystemComplete);
                }
                level = "EG";
                int posEnerg = 1;
                foreach (Sa.Profile.Profile3.Services.Library.Energy _EG in plantIn.EG_List)
                {
                    FuelsRefineryEnergyComplete _FuelsRefineryEnergyComplete = new FuelsRefineryEnergyComplete();
                    _FuelsRefineryEnergyComplete.Amount = _EG.Amount;
                    _FuelsRefineryEnergyComplete.Butane = _EG.Butane;
                    _FuelsRefineryEnergyComplete.Butylenes = _EG.Butylenes;
                    _FuelsRefineryEnergyComplete.C5Plus = _EG.C5Plus;
                    _FuelsRefineryEnergyComplete.CO = _EG.CO;
                    _FuelsRefineryEnergyComplete.CO2 = _EG.CO2;
                    _FuelsRefineryEnergyComplete.EnergyType = _EG.EnergyType;
                    _FuelsRefineryEnergyComplete.Ethane = _EG.Ethane;
                    _FuelsRefineryEnergyComplete.Ethylene = _EG.Ethylene;
                   //moved to electric  _FuelsRefineryEnergyComplete.GenerationEfficiency = _EG.EG17;
                    _FuelsRefineryEnergyComplete.H2S = _EG.H2S;
                    _FuelsRefineryEnergyComplete.Hydrogen = _EG.Hydrogen;
                    _FuelsRefineryEnergyComplete.Isobutane = _EG.Isobutane;
                    _FuelsRefineryEnergyComplete.Methane = _EG.Methane;
                    _FuelsRefineryEnergyComplete.MillionBritishThermalUnitsOut = _EG.MillionBritishThermalUnitsOut;
                    _FuelsRefineryEnergyComplete.N2 = _EG.N2;
                    _FuelsRefineryEnergyComplete.NH3 = _EG.NH3;
                    _FuelsRefineryEnergyComplete.OverrideCalculation = _EG.OverrideCalculation;
                    _FuelsRefineryEnergyComplete.PriceLocal = _EG.PriceLocal;
                    _FuelsRefineryEnergyComplete.Propane = _EG.Propane;
                    _FuelsRefineryEnergyComplete.Propylene = _EG.Propylene;
                    _FuelsRefineryEnergyComplete.SO2 = _EG.SO2;
                    _FuelsRefineryEnergyComplete.SubmissionID = _EG.SubmissionID;
                    _FuelsRefineryEnergyComplete.TransactionType = _EG.TransType;
                    _FuelsRefineryEnergyComplete.TransactionCode = posEnerg;
                    posEnerg++;
                    retPlant.EnergyCompleteList.Add(_FuelsRefineryEnergyComplete);
                }
                level = "EL";
                int posElect = 1;
                foreach (Sa.Profile.Profile3.Services.Library.Electricity _EL in plantIn.EL_List)
                {
                    FuelsRefineryEnergyElectricBasic _FuelsRefineryEnergyElectricBasic = new FuelsRefineryEnergyElectricBasic();
                       _FuelsRefineryEnergyElectricBasic.Amount = _EL.Amount;
                    _FuelsRefineryEnergyElectricBasic.EnergyType = _EL.EnergyType;
                    _FuelsRefineryEnergyElectricBasic.GenerationEfficiency = _EL.GenEff;
                    _FuelsRefineryEnergyElectricBasic.PriceLocal = _EL.PriceLocal;
                    _FuelsRefineryEnergyElectricBasic.SubmissionID = _EL.SubmissionID;
                    _FuelsRefineryEnergyElectricBasic.TransactionType = _EL.TransactionType;
                    _FuelsRefineryEnergyElectricBasic.TransactionCode = posElect;
                    posElect++;
                    retPlant.ElectricBasicList.Add(_FuelsRefineryEnergyElectricBasic);

                }
                level = "R";
                foreach (Sa.Profile.Profile3.Services.Library.Residual _R in plantIn.R_List)
                {
                    FuelsRefineryFinishedResidualFuelComplete _FuelsRefineryFinishedResidualFuelComplete = new FuelsRefineryFinishedResidualFuelComplete();
                    _FuelsRefineryFinishedResidualFuelComplete.BlendID = _R.BlendID;
                    _FuelsRefineryFinishedResidualFuelComplete.Density = _R.Density;
                    _FuelsRefineryFinishedResidualFuelComplete.Grade = _R.Grade;
                    _FuelsRefineryFinishedResidualFuelComplete.PourPointOfBlend = _R.PourPT;
                    _FuelsRefineryFinishedResidualFuelComplete.SubmissionID = _R.SubmissionID;
                    _FuelsRefineryFinishedResidualFuelComplete.Sulfur = _R.Sulfur;
                    _FuelsRefineryFinishedResidualFuelComplete.ThousandMetricTons = _R.KMT;
                    _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendCentiStokeAtTemperature = _R.ViscCSAtTemp;
                    _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendTemperatureDifferent122 = _R.ViscTemp;
                    retPlant.FinishedResidualFuelCompleteList.Add(_FuelsRefineryFinishedResidualFuelComplete);
                }
                level = "F";
                foreach (Sa.Profile.Profile3.Services.Library.FiredHeaterData _F in plantIn.F_List)
                {
                    FuelsRefineryFiredHeatersComplete _FuelsRefineryFiredHeatersComplete = new FuelsRefineryFiredHeatersComplete();
                    _FuelsRefineryFiredHeatersComplete.AbsorbedDuty = _F.AbsorbedDuty;
                    _FuelsRefineryFiredHeatersComplete.CombustionAirTemperature = _F.CombAirTemp;
                    _FuelsRefineryFiredHeatersComplete.FiredDuty = _F.FiredDuty;
                    _FuelsRefineryFiredHeatersComplete.FuelType = _F.FuelType.ToString();
                    _FuelsRefineryFiredHeatersComplete.FurnaceInTemperature = _F.FurnaceInTemp;
                    _FuelsRefineryFiredHeatersComplete.FurnaceOutTemperature = _F.FurnOutTemp;
                    _FuelsRefineryFiredHeatersComplete.HeaterName = _F.HeaterName;
                    _FuelsRefineryFiredHeatersComplete.HeaterNumber = _F.HeaterNo;
                    _FuelsRefineryFiredHeatersComplete.HeatLossPercent = _F.HeatLossPcnt;
                    _FuelsRefineryFiredHeatersComplete.OtherCombustionDuty = _F.OthCombDuty;
                    _FuelsRefineryFiredHeatersComplete.OtherDuty = _F.OtherDuty;
                    _FuelsRefineryFiredHeatersComplete.ProcessDuty = _F.ProcessDuty;
                    _FuelsRefineryFiredHeatersComplete.ProcessFluid = _F.ProcessFluid;
                    _FuelsRefineryFiredHeatersComplete.ProcessID = _F.ProcessID;
                    _FuelsRefineryFiredHeatersComplete.Service = _F.Service;
                    _FuelsRefineryFiredHeatersComplete.ShaftDuty = _F.ShaftDuty;
                    _FuelsRefineryFiredHeatersComplete.StackO2 = _F.StackO2;
                    _FuelsRefineryFiredHeatersComplete.StackTemperature = _F.StackTemp;
                    _FuelsRefineryFiredHeatersComplete.SteamDuty = _F.SteamDuty;
                    _FuelsRefineryFiredHeatersComplete.SteamSuperHeated = _F.SteamSuperHeated;
                    _FuelsRefineryFiredHeatersComplete.SubmissionID = _F.SubmissionID;
                    _FuelsRefineryFiredHeatersComplete.ThroughputReport = _F.ThroughputRpt;
                    _FuelsRefineryFiredHeatersComplete.ThroughputUnitOfMeasure = _F.ThroughputUOM;
                    _FuelsRefineryFiredHeatersComplete.UnitID = _F.UnitID;
                    retPlant.FiredHeatersCompleteList.Add(_FuelsRefineryFiredHeatersComplete);
                }
                level = "G";
                foreach (Sa.Profile.Profile3.Services.Library.Gasoline _G in plantIn.G_List)
                {
                    FuelsRefineryGasolineComplete _FuelsRefineryGasolineComplete = new FuelsRefineryGasolineComplete();
                    _FuelsRefineryGasolineComplete.BlendID = _G.BlendID;
                    _FuelsRefineryGasolineComplete.Density = _G.Density;
                    _FuelsRefineryGasolineComplete.Ethanol = _G.Ethanol;
                    _FuelsRefineryGasolineComplete.EthylTertButylEther = _G.ETBE;
                    _FuelsRefineryGasolineComplete.Grade = _G.Grade;
                    _FuelsRefineryGasolineComplete.Lead = _G.Lead;
                    _FuelsRefineryGasolineComplete.Market = _G.Market;
                    _FuelsRefineryGasolineComplete.MethylTertButylEther = _G.MTBE;
                    _FuelsRefineryGasolineComplete.MotorOctaneNumber = _G.MON;
                    _FuelsRefineryGasolineComplete.OtherOxygen = _G.OthOxygen;
                    _FuelsRefineryGasolineComplete.Oxygen = _G.Oxygen;
                    _FuelsRefineryGasolineComplete.ReidVaporPressure = _G.RVP;
                    _FuelsRefineryGasolineComplete.ResearchOctaneNumber = _G.RON;
                    _FuelsRefineryGasolineComplete.SubmissionID = _G.SubmissionID;
                    _FuelsRefineryGasolineComplete.Sulfur = _G.Sulfur;
                    _FuelsRefineryGasolineComplete.TertAmylMethylEther = _G.TAME;
                    _FuelsRefineryGasolineComplete.ThousandMetricTons = _G.KMT;
                    _FuelsRefineryGasolineComplete.Type = _G.Type;
                    retPlant.GasolineCompleteList.Add(_FuelsRefineryGasolineComplete);
                }
                level = "GM";
                foreach (Sa.Profile.Profile3.Services.Library.GeneralMisc _GM in plantIn.GM_List)
                {
                    FuelsRefineryGeneralMiscellaneousComplete _FuelsRefineryGeneralMiscellaneousComplete = new FuelsRefineryGeneralMiscellaneousComplete();
                    _FuelsRefineryGeneralMiscellaneousComplete.FlareLossMetricTons = _GM.FlareLossMT;
                    _FuelsRefineryGeneralMiscellaneousComplete.SubmissionID = _GM.SubmissionID;
                    _FuelsRefineryGeneralMiscellaneousComplete.TotalLossMetricTons = _GM.TotLossMT;
                    retPlant.GeneralMiscellaneousCompleteList.Add(_FuelsRefineryGeneralMiscellaneousComplete);
                }
                level = "K";
                foreach (Sa.Profile.Profile3.Services.Library.Kerosene _K in plantIn.K_List)
                {
                    FuelsRefineryKeroseneComplete _FuelsRefineryKeroseneComplete = new FuelsRefineryKeroseneComplete();
                    _FuelsRefineryKeroseneComplete.BlendID = _K.BlendID;
                    _FuelsRefineryKeroseneComplete.Density = _K.Density;
                    _FuelsRefineryKeroseneComplete.Grade = _K.Grade;
                    _FuelsRefineryKeroseneComplete.SubmissionID = _K.SubmissionID;
                    _FuelsRefineryKeroseneComplete.Sulfur = _K.Sulfur;
                    _FuelsRefineryKeroseneComplete.ThousandMetricTons = _K.KMT;
                    _FuelsRefineryKeroseneComplete.Type = _K.Type;
                    retPlant.KeroseneCompleteList.Add(_FuelsRefineryKeroseneComplete);
                }
                level = "L";
                foreach (Sa.Profile.Profile3.Services.Library.LPG _L in plantIn.L_List)
                {
                    FuelsRefineryLiquifiedPetroleumGasComplete _FuelsRefineryLiquifiedPetroleumGasComplete = new FuelsRefineryLiquifiedPetroleumGasComplete();
                    _FuelsRefineryLiquifiedPetroleumGasComplete.BlendID = _L.BlendID;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.MoleOrVolume = _L.MolOrVol;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.SubmissionID = _L.SubmissionID;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC2LT = _L.VolC2Lt;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3 = _L.VolC3;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3ene = _L.VolC3ene;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC4ene = _L.VolC4ene;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC5Plus = _L.VolC5Plus;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeiC4 = _L.VoliC4;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumenC4 = _L.VolnC4;
                    retPlant.LiquifiedPetroleumGasCompleteList.Add(_FuelsRefineryLiquifiedPetroleumGasComplete);
                }
                level = "ME";
                foreach (Sa.Profile.Profile3.Services.Library.MaintExp _ME in plantIn.ME_List)
                {
                    FuelsRefineryMaintenanceExpenseAndCapitalComplete _FuelsRefineryMaintenanceExpenseAndCapitalComplete = new FuelsRefineryMaintenanceExpenseAndCapitalComplete();
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.ConstraintRemoval = _ME.ConstraintRemoval;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.Energy = _ME.Energy;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseRoutine = _ME.MaintExpRout;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseTurnaround = _ME.MaintExpTA;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadRoutine = _ME.MaintOvhdTA;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadTurnaround = _ME.MaintOvhdTA;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonMaintenenceInvestmentExpense = _ME.NonMaintInvestExp;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRefineryExclusions = _ME.NonRefExcl;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRegulatoryNewProcessUnitConstruction = _ME.NonRegUnit;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.OtherCapitalInvestment = _ME.OthInvest;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryDiesel = _ME.RegDiesel;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryExpense = _ME.RegExp;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryGasoline = _ME.RegGaso;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryOther = _ME.RegOth;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RoutineMaintenanceCapital = _ME.RoutMaintCptl;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SafetyOther = _ME.SafetyOth;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SubmissionID = _ME.SubmissionID;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalComplexCapital = _ME.ComplexCptl;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenance = _ME.TotMaint;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceExpense = _ME.MaintExpense;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceOverhead = _ME.MaintOvhd;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TurnaroundMaintenanceCapital = _ME.TAMaintCptl;
                    retPlant.MaintenanceExpenseAndCapitalCompleteList.Add(_FuelsRefineryMaintenanceExpenseAndCapitalComplete);
                }
                level = "M";
                foreach (Sa.Profile.Profile3.Services.Library.MarineBunkerData _M in plantIn.M_List)
                {
                    FuelsRefineryMarineBunkersComplete _FuelsRefineryMarineBunkersComplete = new FuelsRefineryMarineBunkersComplete();
                    _FuelsRefineryMarineBunkersComplete.BlendID = _M.BlendID;
                    _FuelsRefineryMarineBunkersComplete.CrackedStock = _M.CrackedStock;
                    _FuelsRefineryMarineBunkersComplete.Density = _M.Density;
                    _FuelsRefineryMarineBunkersComplete.Grade = _M.Grade;
                    _FuelsRefineryMarineBunkersComplete.PourPointOfBlend = _M.PourPt;
                    _FuelsRefineryMarineBunkersComplete.SubmissionID = _M.SubmissionID;
                    _FuelsRefineryMarineBunkersComplete.Sulfur = _M.Sulfur;
                    _FuelsRefineryMarineBunkersComplete.ThousandMetricTons = _M.KMT;
                    _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendCentiStokeAtTemperature = _M.ViscCSAtTemp;
                    _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendTemperatureDifferent122 = _M.ViscTemp;
                    retPlant.MarineBunkersCompleteList.Add(_FuelsRefineryMarineBunkersComplete);
                }
                level = "MI";
                foreach (Sa.Profile.Profile3.Services.Library.MiscellaneousInput _MI in plantIn.MI_List)
                {
                    FuelsRefineryMiscellaneousInputComplete _FuelsRefineryMiscellaneousInputComplete = new FuelsRefineryMiscellaneousInputComplete();
                    _FuelsRefineryMiscellaneousInputComplete.BarrelsInputToCrudeUnits = _MI.CDUChargeBbl;
                    _FuelsRefineryMiscellaneousInputComplete.MetricTonsInputToCrudeUnits = _MI.CDUChargeMT;
                    _FuelsRefineryMiscellaneousInputComplete.OffsiteEnergyPercent = _MI.OffsiteEnergyPcnt;
                    _FuelsRefineryMiscellaneousInputComplete.SubmissionID = _MI.SubmissionID;
                    retPlant.MiscellaneousInputCompleteList.Add(_FuelsRefineryMiscellaneousInputComplete);
                }
                level = "A";
                foreach (Sa.Profile.Profile3.Services.Library.Absences _A in plantIn.A_List)
                {
                    FuelsRefineryPersonnelAbsenceHoursComplete _FuelsRefineryPersonnelAbsenseHoursComplete = new FuelsRefineryPersonnelAbsenceHoursComplete();
                    _FuelsRefineryPersonnelAbsenseHoursComplete.CategoryID = _A.CategoryID;
                    _FuelsRefineryPersonnelAbsenseHoursComplete.ManagementProfessionalAndStaffAbsence = _A.MPSAbs;
                    _FuelsRefineryPersonnelAbsenseHoursComplete.OperatorCraftAndClericalAbsence = _A.OCCAbs;
                    _FuelsRefineryPersonnelAbsenseHoursComplete.SubmissionID = _A.SubmissionID;
                    retPlant.PersonnelAbsenceHoursCompleteList.Add(_FuelsRefineryPersonnelAbsenseHoursComplete);
             
                }
                level = "P";
                foreach (Sa.Profile.Profile3.Services.Library.Personnel _P in plantIn.P_List)
                {
                    FuelsRefineryPersonnelHoursComplete _FuelsRefineryPersonnelHoursComplete = new FuelsRefineryPersonnelHoursComplete();
                    _FuelsRefineryPersonnelHoursComplete.AbsenceHours = _P.AbsHrs;
                    _FuelsRefineryPersonnelHoursComplete.Contract = _P.Contract;
                    _FuelsRefineryPersonnelHoursComplete.GeneralAndAdministrativeHours = _P.GA;
                    _FuelsRefineryPersonnelHoursComplete.NumberOfPersonnel = _P.NumPers;
                    _FuelsRefineryPersonnelHoursComplete.OvertimeHours = _P.OVTHours;
                    _FuelsRefineryPersonnelHoursComplete.OvertimePercent = _P.OVTPcnt;
                    _FuelsRefineryPersonnelHoursComplete.PersonnelHoursID = _P.PersID;
                    _FuelsRefineryPersonnelHoursComplete.StraightTimeHours = _P.STH;
                    _FuelsRefineryPersonnelHoursComplete.SubmissionID = _P.SubmissionID;
                    retPlant.PersonnelHoursCompleteList.Add(_FuelsRefineryPersonnelHoursComplete);
                }
                level = "RP";
                foreach (Sa.Profile.Profile3.Services.Library.RPFResidual _RP in plantIn.RP_List)
                {
                    FuelsRefineryProducedFuelResidualComplete _FuelsRefineryProducedFuelResidualComplete = new FuelsRefineryProducedFuelResidualComplete();
                    _FuelsRefineryProducedFuelResidualComplete.Density = _RP.Density;
                    _FuelsRefineryProducedFuelResidualComplete.EnergyType = _RP.EnergyType;
                    _FuelsRefineryProducedFuelResidualComplete.SubmissionID = _RP.SubmissionID;
                    _FuelsRefineryProducedFuelResidualComplete.Sulfur = _RP.Sulfur;
                    _FuelsRefineryProducedFuelResidualComplete.ViscosityCentiStokesAtTemperature = _RP.ViscCSAtTemp;
                    _FuelsRefineryProducedFuelResidualComplete.ViscosityTemperature = _RP.ViscTemp;
                    retPlant.RefineryProducedFuelResidualCompleteList.Add(_FuelsRefineryProducedFuelResidualComplete);
                }
                level = "MR";
                foreach (Sa.Profile.Profile3.Services.Library.MaintRoutine _MR in plantIn.MR_List)
                {
                    FuelsRefineryRoutineMaintenanceComplete _FuelsRefineryRoutineMaintenanceComplete = new FuelsRefineryRoutineMaintenanceComplete();
                    _FuelsRefineryRoutineMaintenanceComplete.CapitalInLocal = _MR.RoutCptlLocal;
                    _FuelsRefineryRoutineMaintenanceComplete.CostInLocal = _MR.RoutCostLocal;
                    _FuelsRefineryRoutineMaintenanceComplete.ExpenseInLocal = _MR.RoutExpLocal;
                    _FuelsRefineryRoutineMaintenanceComplete.InServicePercent = _MR.InServicePcnt;
                    _FuelsRefineryRoutineMaintenanceComplete.MaintenanceHoursDown = _MR.MaintDown;
                    _FuelsRefineryRoutineMaintenanceComplete.MaintenanceNumberOfDowntime = _MR.MaintNum;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOffsiteUpsets = _MR.OthDownOffsiteUpsets;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOther = _MR.OthDownOther;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDown = _MR.OthDown;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownEconomic = _MR.OthDownEconomic;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownExternal = _MR.OthDownExternal;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownUnitUpsets = _MR.OthDownUnitUpsets;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherNumberOfDowntime = _MR.OthNum;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherSlowdownHours = _MR.OthSlow;
                    _FuelsRefineryRoutineMaintenanceComplete.OverheadInLocal = _MR.RoutOvhdLocal;
                    _FuelsRefineryRoutineMaintenanceComplete.PercentUtilization = _MR.UtilPcnt;
                    _FuelsRefineryRoutineMaintenanceComplete.ProcessID = _MR.ProcessID;
                    _FuelsRefineryRoutineMaintenanceComplete.RegulatoryHoursDown = _MR.RegDown;
                    _FuelsRefineryRoutineMaintenanceComplete.RegulatoryNumberOfDowntime = _MR.RegNum;
                    _FuelsRefineryRoutineMaintenanceComplete.SubmissionID = _MR.SubmissionID;
                    _FuelsRefineryRoutineMaintenanceComplete.UnitID = _MR.UnitID;
                    retPlant.RoutineMaintenanceCompleteList.Add(_FuelsRefineryRoutineMaintenanceComplete);
                }
                level = "MT";
                foreach (Sa.Profile.Profile3.Services.Library.MaintTurnAround _MT in plantIn.MT_List)
                {
                    FuelsRefineryTurnaroundMaintenanceComplete _FuelsRefineryTurnaroundMaintenanceComplete = new FuelsRefineryTurnaroundMaintenanceComplete();
                    _FuelsRefineryTurnaroundMaintenanceComplete.CapitalLocal = _MT.TACptlLocal;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ContractorManagementAndProfessionalSalaried = _MT.TAContMPS;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ContractorOperatorCraftAndClericalHours = _MT.TAContOCC;
                    _FuelsRefineryTurnaroundMaintenanceComplete.CostLocal = _MT.TACostLocal;
                    _FuelsRefineryTurnaroundMaintenanceComplete.Exceptions = _MT.TAExceptions;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ExpenseLocal = _MT.TAExpLocal;
                    _FuelsRefineryTurnaroundMaintenanceComplete.HoursDown = _MT.TAHrsDown;
                    _FuelsRefineryTurnaroundMaintenanceComplete.LaborCostLocal = _MT.TALaborCostLocal;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalOvertimePercent = _MT.TAMPSOVTPcnt;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalSalariedStraightTimeHours = _MT.TAMPSSTH;
                    _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalOvertimeHours = _MT.TAOCCOVT;
                    _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalStraightTimeHours = _MT.TAOCCSTH;
                    _FuelsRefineryTurnaroundMaintenanceComplete.OverheadLocal = _MT.TAOvhdLocal;
                    _FuelsRefineryTurnaroundMaintenanceComplete.PreviousTurnaroundDate = _MT.PrevTADate;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ProcessID = _MT.ProcessID;
                    _FuelsRefineryTurnaroundMaintenanceComplete.SubmissionID = _MT.SubmissionID;
                    _FuelsRefineryTurnaroundMaintenanceComplete.TurnaroundDate = _MT.TADate;
                    _FuelsRefineryTurnaroundMaintenanceComplete.UnitID = _MT.UnitID;
                    retPlant.TurnaroundMaintenanceCompleteList.Add(_FuelsRefineryTurnaroundMaintenanceComplete);
                }
                level = "I";
                foreach (Sa.Profile.Profile3.Services.Library.Inventory _I in plantIn.I_List)
                {
                    FuelsRefineryInventoryComplete _FuelsRefineryInventoryComplete = new FuelsRefineryInventoryComplete();
                    _FuelsRefineryInventoryComplete.AverageLevel = _I.AvgLevel;
                    _FuelsRefineryInventoryComplete.LeasedPercent = _I.LeasedPcnt;
                    _FuelsRefineryInventoryComplete.ManditoryStorageLevel = _I.MandStorage;
                    _FuelsRefineryInventoryComplete.MarketingStorage = _I.MktgStorage;
                    _FuelsRefineryInventoryComplete.RefineryStorage = _I.RefStorage;
                    _FuelsRefineryInventoryComplete.SubmissionID = _I.SubmissionID;
                    _FuelsRefineryInventoryComplete.TankNumber = _I.NumTank;
                    _FuelsRefineryInventoryComplete.TankType = _I.TankType;
                    _FuelsRefineryInventoryComplete.TotalStorage = _I.TotStorage;
                    retPlant.InventoryCompleteList.Add(_FuelsRefineryInventoryComplete);
                }
                level = "Y";
                foreach (Sa.Profile.Profile3.Services.Library.Yields _Y in plantIn.Y_List)
                {
                    FuelsRefineryMaterialBalanceComplete _FuelsRefineryMaterialBalanceComplete = new FuelsRefineryMaterialBalanceComplete();
                    _FuelsRefineryMaterialBalanceComplete.Barrels = _Y.BBL;
                    _FuelsRefineryMaterialBalanceComplete.Category = _Y.Category;
                    _FuelsRefineryMaterialBalanceComplete.Density = _Y.Density;
                    _FuelsRefineryMaterialBalanceComplete.MaterialID = _Y.MaterialID;
                    _FuelsRefineryMaterialBalanceComplete.MaterialName = _Y.MaterialName;
                    _FuelsRefineryMaterialBalanceComplete.MetricTons = _Y.MT;
                    _FuelsRefineryMaterialBalanceComplete.Period = _Y.Period;
                    _FuelsRefineryMaterialBalanceComplete.SubmissionID = _Y.SubmissionID;
                    retPlant.MaterialBalanceCompleteList.Add(_FuelsRefineryMaterialBalanceComplete);
                }
                level = "O";
                foreach (Sa.Profile.Profile3.Services.Library.OperatingExpenses _O in plantIn.O_List)
                {
                    OperatingExpenseStandard _OperatingExpenseStandard = new OperatingExpenseStandard();
                    _OperatingExpenseStandard.OtherDescription = _O.OthDescription;
                    _OperatingExpenseStandard.Property = _O.Property;
                    _OperatingExpenseStandard.ReportValue = _O.RptValue;
                    _OperatingExpenseStandard.SubmissionID = _O.SubmissionID;
                    retPlant.OperatingExpenseStandardList.Add(_OperatingExpenseStandard);
                }
                level = "PD";
                foreach (Sa.Profile.Profile3.Services.Library.ProcessingData _PD in plantIn.PD_List)
                {
                    PlantOperatingConditionBasic _PlantOperatingConditionBasic = new PlantOperatingConditionBasic();
                    _PlantOperatingConditionBasic.Property = _PD.Property;
                    _PlantOperatingConditionBasic.ReportDateValue = _PD.RptDVal;
                    _PlantOperatingConditionBasic.ReportNumericValue = _PD.RptNVal;
                    _PlantOperatingConditionBasic.ReportTextValue = _PD.RptTVal;
                    _PlantOperatingConditionBasic.SubmissionID = _PD.SubmissionID;
                    _PlantOperatingConditionBasic.UnitID = _PD.UnitID;
                    _PlantOperatingConditionBasic.UnitOfMeasure = _PD.UOM;
                    retPlant.PlantOperatingConditionList.Add(_PlantOperatingConditionBasic);
                }
                level = "SB";
                if (plantIn.SB_List.Count == 0)
                {
                    throw new Exception("No Submissions detected!");
                }
                foreach (Sa.Profile.Profile3.Services.Library.Submissions _SB in plantIn.SB_List)
                {
                    SubmissionBasic _SubmissionBasic = new SubmissionBasic();
                    _SubmissionBasic.RefineryID = _SB.RefID;
                    _SubmissionBasic.ReportCurrency = _SB.RptCurrency;
                    _SubmissionBasic.SubmissionDate = _SB.Date;
                    _SubmissionBasic.SubmissionID = _SB.SubmissionID;
                    _SubmissionBasic.UnitOfMeasure = _SB.UOM;
                    retPlant.PlantSubmission = _SubmissionBasic;
                    if (plantIn.SB_List.Count > 1)
                    {
                        throw new Exception("Multiple Submissions detected!");
                    }
                }
            }
            catch (Exception ex)
            {
                string template = "WCFToBus:Level={0} Error:{1}";
                Exception infoException = new Exception(string.Format(template, level, ex.Message));
                throw infoException;
            }

            return retPlant;
        }
        public PL BusToWCF(FuelsRefineryCompletePlant plantIn)
        {
            PL retVal = new PL();
            string level = "Absence";
            try
            {
                retVal.A_List = new List<Absences>();
                retVal.C_List = new List<Configuration>();
                retVal.CB_List = new List<Services.Library.ConfigBuoy>();
                retVal.CD_List = new List<Services.Library.Crude>();
                retVal.CR_List = new List<Services.Library.ConfigRS>();
                retVal.D_List = new List<Services.Library.DieselData>();
                retVal.E_List = new List<Services.Library.Emissions>();
                retVal.EG_List = new List<Services.Library.Energy>();
                retVal.EL_List = new List<Services.Library.Electricity>();
                retVal.F_List = new List<Services.Library.FiredHeaterData>();
                retVal.G_List = new List<Services.Library.Gasoline>();
                retVal.GM_List = new List<Services.Library.GeneralMisc>();
                retVal.I_List = new List<Services.Library.Inventory>();
                retVal.K_List = new List<Services.Library.Kerosene>();
                retVal.L_List = new List<Services.Library.LPG>();
                retVal.M_List = new List<Services.Library.MarineBunkerData>();
                retVal.ME_List = new List<Services.Library.MaintExp>();
                retVal.MI_List = new List<Services.Library.MiscellaneousInput>();
                retVal.MR_List = new List<Services.Library.MaintRoutine>();
                retVal.MT_List = new List<Services.Library.MaintTurnAround>();
                retVal.O_List = new List<Services.Library.OperatingExpenses>();
                retVal.P_List = new List<Services.Library.Personnel>();
                retVal.PD_List = new List<Services.Library.ProcessingData>();
                retVal.R_List = new List<Services.Library.Residual>();
                retVal.RP_List = new List<Services.Library.RPFResidual>();
                retVal.S_List = new List<Services.Library.Steam>();
                retVal.SB_List = new List<Services.Library.Submissions>();
                retVal.Y_List = new List<Services.Library.Yields>();

                if(plantIn.PersonnelAbsenceHoursCompleteList.Count>0)
                {
                    foreach(FuelsRefineryPersonnelAbsenceHoursComplete _FuelsRefineryPersonnelAbsenceHoursComplete in plantIn.PersonnelAbsenceHoursCompleteList)
                    {
                        Absences _A = new Absences();
                        _A.SubmissionID = _FuelsRefineryPersonnelAbsenceHoursComplete.SubmissionID; // Absence
                        _A.CategoryID = _FuelsRefineryPersonnelAbsenceHoursComplete.CategoryID; // Absence
                        _A.OCCAbs = _FuelsRefineryPersonnelAbsenceHoursComplete.OperatorCraftAndClericalAbsence; // Absence
                        _A.MPSAbs = _FuelsRefineryPersonnelAbsenceHoursComplete.ManagementProfessionalAndStaffAbsence; // Absence
                        retVal.A_List.Add(_A);
                    }
                }
               
                if(plantIn.ConfigurationCompleteList.Count>0)
                {
                    level = "Config";
                     foreach (FuelsRefineryConfigurationComplete _FuelsRefineryConfigurationComplete in plantIn.ConfigurationCompleteList)
                    {
                        Configuration _C = new Configuration();
                        _C.SubmissionID = _FuelsRefineryConfigurationComplete.SubmissionID; // Config
                        _C.UnitID = _FuelsRefineryConfigurationComplete.UnitID; // Config
                        _C.ProcessID = _FuelsRefineryConfigurationComplete.ProcessID; // Config
                        _C.UnitName = _FuelsRefineryConfigurationComplete.UnitName; // Config
                        _C.ProcessType = _FuelsRefineryConfigurationComplete.ProcessType; // Config
                        _C.Cap = _FuelsRefineryConfigurationComplete.Capacity; // Config
                        _C.UtilPcnt = _FuelsRefineryConfigurationComplete.UtilizationPercent; // Config
                        _C.StmCap = _FuelsRefineryConfigurationComplete.SteamCapacity; // Config
                        _C.StmUtilPcnt = _FuelsRefineryConfigurationComplete.SteamUtilizationPercent; // Config
                        _C.InServicePcnt = _FuelsRefineryConfigurationComplete.InServicePercent; // Config
                        _C.YearsOper = _FuelsRefineryConfigurationComplete.YearsInOperation; // Config
                        _C.MHPerWeek = _FuelsRefineryConfigurationComplete.ManHourWeek; // Config
                        _C.PostPerShift = _FuelsRefineryConfigurationComplete.PostPerShift; // Config
                        _C.BlockOp = _FuelsRefineryConfigurationComplete.BlockOperated; // Config
                        _C.AllocPcntOfCap = _FuelsRefineryConfigurationComplete.AllocatedPercentOfCapacity; // Config
                        _C.EnergyPcnt = _FuelsRefineryConfigurationComplete.EnergyPercent; // Config
                        retVal.C_List.Add(_C);
                    }
                }
                if(plantIn.ConfigurationBuoyCompleteList.Count>0)
                {
                    level = "ConfigBuoy";
                    foreach (FuelsRefineryConfigurationBuoyComplete _FuelsRefineryConfigurationBuoyComplete in plantIn.ConfigurationBuoyCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.ConfigBuoy _CB = new Sa.Profile.Profile3.Services.Library.ConfigBuoy();
                        _CB.SubmissionID = _FuelsRefineryConfigurationBuoyComplete.SubmissionID; // ConfigBuoy
                        _CB.UnitID = _FuelsRefineryConfigurationBuoyComplete.UnitID; // ConfigBuoy
                        _CB.UnitName = _FuelsRefineryConfigurationBuoyComplete.UnitName; // ConfigBuoy
                        _CB.ProcessID = _FuelsRefineryConfigurationBuoyComplete.ProcessID; // ConfigBuoy
                        _CB.ShipCap = _FuelsRefineryConfigurationBuoyComplete.ShipCapacity; // ConfigBuoy
                        _CB.LineSize = _FuelsRefineryConfigurationBuoyComplete.LineSize; // ConfigBuoy
                        _CB.PcntOwnership = _FuelsRefineryConfigurationBuoyComplete.PercentOwnership; // ConfigBuoy
                        retVal.CB_List.Add(_CB);
                    }
                }
        
                if(plantIn.CrudeCompleteList.Count>0)
                {
                    level = "Crude";
                    foreach (FuelsRefineryCrudeComplete _FuelsRefineryCrudeComplete in plantIn.CrudeCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.Crude _CD = new Sa.Profile.Profile3.Services.Library.Crude();
                        _CD.SubmissionID = _FuelsRefineryCrudeComplete .SubmissionID; // Crude
                        _CD.Period = _FuelsRefineryCrudeComplete.Period; // Crude
                        _CD.CrudeID = _FuelsRefineryCrudeComplete.CrudeID; // Crude
                        _CD.CrudeNumber = _FuelsRefineryCrudeComplete.CrudeNumber; // Crude
                        _CD.CrudeName = _FuelsRefineryCrudeComplete.CrudeName; // Crude
                        _CD.BBL = _FuelsRefineryCrudeComplete.Barrels; // Crude
                        _CD.Gravity = _FuelsRefineryCrudeComplete.Gravity; // Crude
                        _CD.Sulfur = _FuelsRefineryCrudeComplete.Sulfur; // Crude
                        retVal.CD_List.Add(_CD);
                    }
                }

                if (plantIn.ConfigurationReceiptAndShipmentCompleteList.Count>0)                    
                {
                    level = "ConfigRS";
                    foreach (FuelsRefineryConfigurationReceiptAndShipmentComplete _FuelsRefineryConfigurationReceiptAndShipmentComplete in plantIn.ConfigurationReceiptAndShipmentCompleteList)
                    {
                        ConfigRS _CR = new ConfigRS();
                        _CR.SubmissionID = _FuelsRefineryConfigurationReceiptAndShipmentComplete.SubmissionID; // ConfigRS
                        _CR.UnitID = _FuelsRefineryConfigurationReceiptAndShipmentComplete.UnitID; // ConfigRS
                        _CR.ProcessID = _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessID; // ConfigRS
                        _CR.ProcessType = _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessType; // ConfigRS
                        _CR.AvgSize = _FuelsRefineryConfigurationReceiptAndShipmentComplete.AverageSize; // ConfigRS
                        _CR.Throughput = _FuelsRefineryConfigurationReceiptAndShipmentComplete.Throughput; // ConfigRS

                        retVal.CR_List.Add(_CR);
                    }
                }
                if (plantIn.DieselCompleteList.Count>0)
                {
                    level = "Diesel";
                    foreach (FuelsRefineryDieselComplete _FuelsRefineryDieselComplete in plantIn.DieselCompleteList)
                    {
                        DieselData _D = new DieselData();
                        _D.SubmissionID = _FuelsRefineryDieselComplete.SubmissionID; // Diesel
                        _D.BlendID = _FuelsRefineryDieselComplete.BlendID; // Diesel
                        _D.Grade = _FuelsRefineryDieselComplete.Grade; // Diesel
                        _D.Market = _FuelsRefineryDieselComplete.Market; // Diesel
                        _D.Type = _FuelsRefineryDieselComplete.Type; // Diesel
                        _D.Density = _FuelsRefineryDieselComplete.Density; // Diesel
                        _D.Cetane = _FuelsRefineryDieselComplete.Cetane; // Diesel
                        _D.PourPt = _FuelsRefineryDieselComplete.PourPointOfBlend; // Diesel
                        _D.Sulfur = _FuelsRefineryDieselComplete.Sulfur; // Diesel
                        _D.CloudPt = _FuelsRefineryDieselComplete.CloudPointOfBlend; // Diesel
                        _D.ASTM90 = _FuelsRefineryDieselComplete.AmericanStandardForTestingMaterials90DistillationPoint; // Diesel
                        _D.E350 = _FuelsRefineryDieselComplete.PercentEvaporatedAt350C; // Diesel
                        _D.BiodieselPcnt = _FuelsRefineryDieselComplete.BiodieselInBlendPercent; // Diesel
                        _D.KMT = _FuelsRefineryDieselComplete.ThousandMetricTons; // Diesel

                        retVal.D_List.Add(_D);
                    }
                }
                if (plantIn.EmissionCompleteList.Count>0)
                {
                    level = "Emissions";
                     foreach (FuelsRefineryEmissionComplete _FuelsRefineryEmissionComplete in plantIn.EmissionCompleteList)
                    {
                        Emissions _E = new Emissions();
                        _E.SubmissionID = _FuelsRefineryEmissionComplete.SubmissionID; // Emissions
                        _E.EmissionType = _FuelsRefineryEmissionComplete.EmissionType; // Emissions
                        _E.RefineryEmission = _FuelsRefineryEmissionComplete.RefineryEmission; // Emissions

                        retVal.E_List.Add(_E);
                    }
                }
                if (plantIn.ElectricBasicList.Count>0)
                {
                    level = "Electric";
                    foreach (FuelsRefineryEnergyElectricBasic _FuelsRefineryEnergyElectricBasic in plantIn.ElectricBasicList)
                    {
                        Electricity _EL = new Electricity();
                        _EL.SubmissionID = _FuelsRefineryEnergyElectricBasic.SubmissionID; // Electric
                        _EL.EnergyType = _FuelsRefineryEnergyElectricBasic.EnergyType; // Electric
                        _EL.TransactionType = _FuelsRefineryEnergyElectricBasic.TransactionType; // Electric
                        _EL.Amount = _FuelsRefineryEnergyElectricBasic.Amount; // Electric
                        _EL.PriceLocal = _FuelsRefineryEnergyElectricBasic.PriceLocal; // Electric
                        _EL.GenEff = _FuelsRefineryEnergyElectricBasic.GenerationEfficiency; // Electric                       
                        retVal.EL_List.Add(_EL);

                    }
                }
                if (plantIn.EnergyCompleteList.Count>0)
                {
                    level = "Energy";
                    foreach (FuelsRefineryEnergyComplete _FuelsRefineryEnergyComplete in plantIn.EnergyCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.Energy _EG = new Sa.Profile.Profile3.Services.Library.Energy();
                        _EG.SubmissionID = _FuelsRefineryEnergyComplete.SubmissionID; // Energy
                        _EG.EnergyType = _FuelsRefineryEnergyComplete.EnergyType; // Energy
                        _EG.TransType = _FuelsRefineryEnergyComplete.TransactionType; // Energy
                        _EG.Amount = _FuelsRefineryEnergyComplete.Amount; // Energy
                        _EG.PriceLocal = _FuelsRefineryEnergyComplete.PriceLocal; // Energy
                        _EG.MillionBritishThermalUnitsOut = _FuelsRefineryEnergyComplete.MillionBritishThermalUnitsOut; // Energy
                        _EG.OverrideCalculation = _FuelsRefineryEnergyComplete.OverrideCalculation; // Energy
                        //moved to electric _EG.EG17 = _electric_2.GenerationEfficiency; // Energy
                        _EG.Hydrogen = _FuelsRefineryEnergyComplete.Hydrogen; // Energy
                        _EG.Methane = _FuelsRefineryEnergyComplete.Methane; // Energy
                        _EG.Ethane = _FuelsRefineryEnergyComplete.Ethane; // Energy
                        _EG.Ethylene = _FuelsRefineryEnergyComplete.Ethylene; // Energy
                        _EG.Propane = _FuelsRefineryEnergyComplete.Propane; // Energy
                        _EG.Propylene = _FuelsRefineryEnergyComplete.Propylene; // Energy
                        _EG.Butane = _FuelsRefineryEnergyComplete.Butane; // Energy
                        _EG.Isobutane = _FuelsRefineryEnergyComplete.Isobutane; // Energy
                        _EG.Butylenes = _FuelsRefineryEnergyComplete.Butylenes; // Energy
                        _EG.C5Plus = _FuelsRefineryEnergyComplete.C5Plus; // Energy
                        _EG.H2S = _FuelsRefineryEnergyComplete.H2S; // Energy
                        _EG.CO = _FuelsRefineryEnergyComplete.CO; // Energy
                        _EG.CO2 = _FuelsRefineryEnergyComplete.NH3; // Energy
                        _EG.SO2 = _FuelsRefineryEnergyComplete.SO2; // Energy
                        _EG.CO2 = _FuelsRefineryEnergyComplete.CO2; // Energy
                        _EG.N2 = _FuelsRefineryEnergyComplete.N2; // Energy
                        retVal.EG_List.Add(_EG);
                     }
                }
                

                if (plantIn.FiredHeatersCompleteList.Count>0)
                {
                    level = "FiredHeaters";
                    foreach (FuelsRefineryFiredHeatersComplete _FuelsRefineryFiredHeatersComplete in plantIn.FiredHeatersCompleteList) 
                    {
                        FiredHeaterData _F = new FiredHeaterData();
                        _F.SubmissionID = _FuelsRefineryFiredHeatersComplete.SubmissionID; // FiredHeaters
                        _F.HeaterNo = _FuelsRefineryFiredHeatersComplete.HeaterNumber; // FiredHeaters
                        _F.UnitID = _FuelsRefineryFiredHeatersComplete.UnitID; // FiredHeaters
                        _F.ProcessID = _FuelsRefineryFiredHeatersComplete.ProcessID; // FiredHeaters
                        _F.HeaterName = _FuelsRefineryFiredHeatersComplete.HeaterName; // FiredHeaters
                        _F.Service = _FuelsRefineryFiredHeatersComplete.Service; // FiredHeaters
                        _F.ProcessFluid = _FuelsRefineryFiredHeatersComplete.ProcessFluid; // FiredHeaters
                        _F.ThroughputRpt = _FuelsRefineryFiredHeatersComplete.ThroughputReport; // FiredHeaters
                        _F.ThroughputUOM = _FuelsRefineryFiredHeatersComplete.ThroughputUnitOfMeasure; // FiredHeaters
                        _F.FiredDuty = _FuelsRefineryFiredHeatersComplete.FiredDuty; // FiredHeaters
                        _F.FuelType = _FuelsRefineryFiredHeatersComplete.FuelType; // FiredHeaters
                        _F.OthCombDuty = _FuelsRefineryFiredHeatersComplete.OtherCombustionDuty; // FiredHeaters
                        _F.FurnaceInTemp = _FuelsRefineryFiredHeatersComplete.FurnaceInTemperature; // FiredHeaters
                        _F.FurnOutTemp = _FuelsRefineryFiredHeatersComplete.FurnaceOutTemperature; // FiredHeaters
                        _F.StackTemp = _FuelsRefineryFiredHeatersComplete.StackTemperature; // FiredHeaters
                        _F.StackO2 = _FuelsRefineryFiredHeatersComplete.StackO2; // FiredHeaters
                        _F.HeatLossPcnt = _FuelsRefineryFiredHeatersComplete.HeatLossPercent; // FiredHeaters
                        _F.CombAirTemp = _FuelsRefineryFiredHeatersComplete.CombustionAirTemperature; // FiredHeaters
                        _F.AbsorbedDuty = _FuelsRefineryFiredHeatersComplete.AbsorbedDuty; // FiredHeaters
                        _F.ProcessDuty = _FuelsRefineryFiredHeatersComplete.ProcessDuty; // FiredHeaters
                        _F.SteamDuty = _FuelsRefineryFiredHeatersComplete.SteamDuty; // FiredHeaters
                        _F.SteamSuperHeated = _FuelsRefineryFiredHeatersComplete.SteamSuperHeated; // FiredHeaters
                        _F.ShaftDuty = _FuelsRefineryFiredHeatersComplete.ShaftDuty; // FiredHeaters
                        _F.OtherDuty = _FuelsRefineryFiredHeatersComplete.OtherDuty; // FiredHeaters

                        retVal.F_List.Add(_F);
                    }
                }
                if (plantIn.GasolineCompleteList.Count>0)
                {
                    level = "Gasoline";
                     foreach (FuelsRefineryGasolineComplete _FuelsRefineryGasolineComplete in plantIn.GasolineCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.Gasoline _G = new Sa.Profile.Profile3.Services.Library.Gasoline();
                        _G.SubmissionID = _FuelsRefineryGasolineComplete.SubmissionID; // Gasoline
                        _G.BlendID = _FuelsRefineryGasolineComplete.BlendID; // Gasoline
                        _G.Grade = _FuelsRefineryGasolineComplete.Grade; // Gasoline
                        _G.Market = _FuelsRefineryGasolineComplete.Market; // Gasoline
                        _G.Type = _FuelsRefineryGasolineComplete.Type; // Gasoline
                        _G.Density = _FuelsRefineryGasolineComplete.Density; // Gasoline
                        _G.RVP = _FuelsRefineryGasolineComplete.ReidVaporPressure; // Gasoline
                        _G.RON = _FuelsRefineryGasolineComplete.ResearchOctaneNumber; // Gasoline
                        _G.MON = _FuelsRefineryGasolineComplete.MotorOctaneNumber; // Gasoline
                        _G.Oxygen = _FuelsRefineryGasolineComplete.Oxygen; // Gasoline
                        _G.Ethanol = _FuelsRefineryGasolineComplete.Ethanol; // Gasoline
                        _G.MTBE = _FuelsRefineryGasolineComplete.MethylTertButylEther; // Gasoline
                        _G.ETBE = _FuelsRefineryGasolineComplete.EthylTertButylEther; // Gasoline
                        _G.TAME = _FuelsRefineryGasolineComplete.TertAmylMethylEther; // Gasoline
                        _G.OthOxygen = _FuelsRefineryGasolineComplete.OtherOxygen; // Gasoline
                        _G.Sulfur = _FuelsRefineryGasolineComplete.Sulfur; // Gasoline
                        _G.Lead = _FuelsRefineryGasolineComplete.Lead; // Gasoline
                        _G.KMT = _FuelsRefineryGasolineComplete.ThousandMetricTons; // Gasoline
                        retVal.G_List.Add(_G);
                    }
                }

                if (plantIn.GeneralMiscellaneousCompleteList.Count>0)
                {
                    level = "GeneralMisc";
                     foreach (FuelsRefineryGeneralMiscellaneousComplete _FuelsRefineryGeneralMiscellaneousComplete in plantIn.GeneralMiscellaneousCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.GeneralMisc _GM = new Sa.Profile.Profile3.Services.Library.GeneralMisc();

                         _GM.SubmissionID = _FuelsRefineryGeneralMiscellaneousComplete.SubmissionID; // GeneralMisc
                        _GM.FlareLossMT = _FuelsRefineryGeneralMiscellaneousComplete.FlareLossMetricTons; // GeneralMisc
                        _GM.TotLossMT = _FuelsRefineryGeneralMiscellaneousComplete.TotalLossMetricTons; // GeneralMisc
                        retVal.GM_List.Add(_GM);
                    }
                }
                if (plantIn.InventoryCompleteList.Count>0)
                {
                    level = "Inventory";
                    foreach (FuelsRefineryInventoryComplete _FuelsRefineryInventoryComplete in plantIn.InventoryCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.Inventory _I = new Sa.Profile.Profile3.Services.Library.Inventory();
                        _I.SubmissionID = _FuelsRefineryInventoryComplete.SubmissionID; // Inventory
                        _I.TankType = _FuelsRefineryInventoryComplete.TankType; // Inventory
                        _I.TotStorage = _FuelsRefineryInventoryComplete.TotalStorage; // Inventory
                        _I.MktgStorage = _FuelsRefineryInventoryComplete.MarketingStorage; // Inventory
                        _I.MandStorage = _FuelsRefineryInventoryComplete.ManditoryStorageLevel; // Inventory
                        _I.RefStorage = _FuelsRefineryInventoryComplete.RefineryStorage; // Inventory
                        _I.NumTank = _FuelsRefineryInventoryComplete.TankNumber; // Inventory
                        _I.LeasedPcnt = _FuelsRefineryInventoryComplete.LeasedPercent; // Inventory
                        _I.AvgLevel = _FuelsRefineryInventoryComplete.AverageLevel; // Inventory

                        retVal.I_List.Add(_I);
                    }
                }
                if (plantIn.KeroseneCompleteList.Count>0)
                {
                    level = "Kerosene";
                    foreach (FuelsRefineryKeroseneComplete _FuelsRefineryKeroseneComplete in plantIn.KeroseneCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.Kerosene _K = new Sa.Profile.Profile3.Services.Library.Kerosene();
                        _K.SubmissionID = _FuelsRefineryKeroseneComplete.SubmissionID; // Kerosene
                        _K.BlendID = _FuelsRefineryKeroseneComplete.BlendID; // Kerosene
                        _K.Grade = _FuelsRefineryKeroseneComplete.Grade; // Kerosene
                        _K.Type = _FuelsRefineryKeroseneComplete.Type; // Kerosene
                        _K.Density = _FuelsRefineryKeroseneComplete.Density; // Kerosene
                        _K.Sulfur = _FuelsRefineryKeroseneComplete.Sulfur; // Kerosene
                        _K.KMT = _FuelsRefineryKeroseneComplete.ThousandMetricTons; // Kerosene

                        retVal.K_List.Add(_K);
                    }
                }
                if (plantIn.LiquifiedPetroleumGasCompleteList.Count>0)
                {
                    level = "LPG";
                     foreach (FuelsRefineryLiquifiedPetroleumGasComplete _FuelsRefineryLiquifiedPetroleumGasComplete in plantIn.LiquifiedPetroleumGasCompleteList)
                    {
                        Sa.Profile.Profile3.Services.Library.LPG _L = new Sa.Profile.Profile3.Services.Library.LPG();

                        _L.SubmissionID = _FuelsRefineryLiquifiedPetroleumGasComplete.SubmissionID; // LPG
                        _L.BlendID = _FuelsRefineryLiquifiedPetroleumGasComplete.BlendID; // LPG
                        _L.MolOrVol = _FuelsRefineryLiquifiedPetroleumGasComplete.MoleOrVolume; // LPG
                        _L.VolC2Lt = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC2LT; // LPG
                        _L.VolC3 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3; // LPG
                        _L.VolC3ene = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3ene; // LPG
                        _L.VoliC4 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeiC4; // LPG
                        _L.VolnC4 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumenC4; // LPG
                        _L.VolC4ene = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC4ene; // LPG
                        _L.VolC5Plus = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC5Plus; // LPG
                        retVal.L_List.Add(_L);
                    }
                }
                if (plantIn.MarineBunkersCompleteList.Count>0)
                {
                    level = "MarineBunkers";
                    foreach (FuelsRefineryMarineBunkersComplete _FuelsRefineryMarineBunkersComplete in plantIn.MarineBunkersCompleteList)
                    {
                        MarineBunkerData _M = new MarineBunkerData();

                        _M.SubmissionID = _FuelsRefineryMarineBunkersComplete.SubmissionID; // MarineBunkers
                        _M.BlendID = _FuelsRefineryMarineBunkersComplete.BlendID; // MarineBunkers
                        _M.Grade = _FuelsRefineryMarineBunkersComplete.Grade; // MarineBunkers
                        _M.Density = _FuelsRefineryMarineBunkersComplete.Density; // MarineBunkers
                        _M.Sulfur = _FuelsRefineryMarineBunkersComplete.Sulfur; // MarineBunkers
                        _M.PourPt = _FuelsRefineryMarineBunkersComplete.PourPointOfBlend; // MarineBunkers
                        _M.ViscCSAtTemp = _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendCentiStokeAtTemperature; // MarineBunkers
                        _M.ViscTemp = _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendTemperatureDifferent122; // MarineBunkers
                        _M.CrackedStock = _FuelsRefineryMarineBunkersComplete.CrackedStock; // MarineBunkers
                        _M.KMT = _FuelsRefineryMarineBunkersComplete.ThousandMetricTons; // MarineBunkers
                        retVal.M_List.Add(_M);
                    }
                }
                if (plantIn.MaintenanceExpenseAndCapitalCompleteList.Count>0)
                {
                    level = "MExp";
                     foreach (FuelsRefineryMaintenanceExpenseAndCapitalComplete _FuelsRefineryMaintenanceExpenseAndCapitalComplete in plantIn.MaintenanceExpenseAndCapitalCompleteList)
                    {
                        MaintExp _ME = new MaintExp();
                        _ME.SubmissionID = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SubmissionID; // MExp
                        _ME.ComplexCptl = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalComplexCapital; // MExp
                        _ME.NonRefExcl = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRefineryExclusions; // MExp
                        _ME.TAMaintCptl = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TurnaroundMaintenanceCapital; // MExp
                        _ME.RoutMaintCptl = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RoutineMaintenanceCapital; // MExp
                        _ME.NonMaintInvestExp = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonMaintenenceInvestmentExpense; // MExp
                        _ME.NonRegUnit = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRegulatoryNewProcessUnitConstruction; // MExp
                        _ME.ConstraintRemoval = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.ConstraintRemoval; // MExp
                        _ME.RegExp = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryExpense; // MExp
                        _ME.RegGaso = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryGasoline; // MExp
                        _ME.RegDiesel = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryDiesel; // MExp
                        _ME.RegOth = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryOther; // MExp
                        _ME.SafetyOth = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SafetyOther; // MExp
                        _ME.Energy = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.Energy; // MExp
                        _ME.OthInvest = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.OtherCapitalInvestment; // MExp
                        _ME.MaintExpTA = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseTurnaround; // MExp
                        _ME.MaintExpRout = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseRoutine; // MExp
                        _ME.TotMaint = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenance; // MExp
                        _ME.MaintOvhdTA = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadTurnaround; // MExp
                        _ME.MaintOvhdRout = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadRoutine; // MExp
                        _ME.TotMaint = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceOverhead; // MExp
                        _ME.MaintExpense = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceExpense; // MExp

                        retVal.ME_List.Add(_ME);
                    }
                }
                if (plantIn.MiscellaneousInputCompleteList.Count>0)
                {
                    level = "MiscInput";
                    foreach (FuelsRefineryMiscellaneousInputComplete _FuelsRefineryMiscellaneousInputComplete in plantIn.MiscellaneousInputCompleteList)
                    {
                        MiscellaneousInput _MI = new MiscellaneousInput();
                        _MI.SubmissionID = _FuelsRefineryMiscellaneousInputComplete.SubmissionID; // MiscInput
                        _MI.CDUChargeBbl = _FuelsRefineryMiscellaneousInputComplete.BarrelsInputToCrudeUnits; // MiscInput
                        _MI.CDUChargeMT = _FuelsRefineryMiscellaneousInputComplete.MetricTonsInputToCrudeUnits; // MiscInput
                        _MI.OffsiteEnergyPcnt = _FuelsRefineryMiscellaneousInputComplete.OffsiteEnergyPercent; // MiscInput
                        retVal.MI_List.Add(_MI);
                    }
                }
                if (plantIn.RoutineMaintenanceCompleteList.Count>0)
                {
                    level = "MaintRout";
                    foreach (FuelsRefineryRoutineMaintenanceComplete _FuelsRefineryRoutineMaintenanceComplete in plantIn.RoutineMaintenanceCompleteList)
                    {
                        MaintRoutine _MR = new MaintRoutine();
                        _MR.SubmissionID = _FuelsRefineryRoutineMaintenanceComplete.SubmissionID; // MaintRout
                        _MR.UnitID = _FuelsRefineryRoutineMaintenanceComplete.UnitID; // MaintRout
                        _MR.ProcessID = _FuelsRefineryRoutineMaintenanceComplete.ProcessID; // MaintRout
                        _MR.RegNum = _FuelsRefineryRoutineMaintenanceComplete.RegulatoryNumberOfDowntime; // MaintRout
                        _MR.RegDown = _FuelsRefineryRoutineMaintenanceComplete.RegulatoryHoursDown; // MaintRout
                        _MR.MaintNum = _FuelsRefineryRoutineMaintenanceComplete.MaintenanceNumberOfDowntime; // MaintRout
                        _MR.MaintDown = _FuelsRefineryRoutineMaintenanceComplete.MaintenanceHoursDown; // MaintRout
                        _MR.OthNum = _FuelsRefineryRoutineMaintenanceComplete.OtherNumberOfDowntime; // MaintRout
                        _MR.OthDown = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDown; // MaintRout
                        _MR.OthSlow = _FuelsRefineryRoutineMaintenanceComplete.OtherSlowdownHours; // MaintRout
                        _MR.RoutCostLocal = _FuelsRefineryRoutineMaintenanceComplete.CostInLocal; // MaintRout
                        _MR.RoutExpLocal = _FuelsRefineryRoutineMaintenanceComplete.ExpenseInLocal; // MaintRout
                        _MR.RoutCptlLocal = _FuelsRefineryRoutineMaintenanceComplete.CapitalInLocal; // MaintRout
                        _MR.RoutOvhdLocal = _FuelsRefineryRoutineMaintenanceComplete.OverheadInLocal; // MaintRout
                        _MR.OthDownEconomic = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownEconomic; // MaintRout
                        _MR.OthDownExternal = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownExternal; // MaintRout
                        _MR.OthDownUnitUpsets = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownUnitUpsets; // MaintRout
                        _MR.OthDownOffsiteUpsets = _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOffsiteUpsets; // MaintRout
                        _MR.OthDownOther = _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOther; // MaintRout
                        _MR.UtilPcnt = _FuelsRefineryRoutineMaintenanceComplete.PercentUtilization; // MaintRout
                        _MR.InServicePcnt = _FuelsRefineryRoutineMaintenanceComplete.InServicePercent; // MaintRout

                        retVal.MR_List.Add(_MR);
                    }
                }
                if (plantIn.TurnaroundMaintenanceCompleteList.Count>0)
                {
                    level = "MaintTA";
                    foreach (FuelsRefineryTurnaroundMaintenanceComplete _FuelsRefineryTurnaroundMaintenanceComplete in plantIn.TurnaroundMaintenanceCompleteList)
                    {
                        MaintTurnAround _MT = new MaintTurnAround();
                        _MT.SubmissionID = _FuelsRefineryTurnaroundMaintenanceComplete.SubmissionID; // MaintTA
                        _MT.UnitID = _FuelsRefineryTurnaroundMaintenanceComplete.UnitID; // MaintTA
                        _MT.ProcessID = _FuelsRefineryTurnaroundMaintenanceComplete.ProcessID; // MaintTA
                        _MT.TADate = _FuelsRefineryTurnaroundMaintenanceComplete.TurnaroundDate; // MaintTA
                        _MT.TAHrsDown = _FuelsRefineryTurnaroundMaintenanceComplete.HoursDown; // MaintTA
                        _MT.TACostLocal = _FuelsRefineryTurnaroundMaintenanceComplete.CostLocal; // MaintTA
                        _MT.TAExpLocal = _FuelsRefineryTurnaroundMaintenanceComplete.ExpenseLocal; // MaintTA
                        _MT.TACptlLocal = _FuelsRefineryTurnaroundMaintenanceComplete.CapitalLocal; // MaintTA
                        _MT.TAOvhdLocal = _FuelsRefineryTurnaroundMaintenanceComplete.OverheadLocal; // MaintTA
                        _MT.TALaborCostLocal = _FuelsRefineryTurnaroundMaintenanceComplete.LaborCostLocal; // MaintTA
                        _MT.TAOCCSTH = _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalStraightTimeHours; // MaintTA
                        _MT.TAOCCOVT = _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalOvertimeHours; // MaintTA
                        _MT.TAMPSSTH = _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalSalariedStraightTimeHours; // MaintTA
                        _MT.TAMPSOVTPcnt = _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalOvertimePercent; // MaintTA
                        _MT.TAContOCC = _FuelsRefineryTurnaroundMaintenanceComplete.ContractorOperatorCraftAndClericalHours; // MaintTA
                        _MT.TAContMPS = _FuelsRefineryTurnaroundMaintenanceComplete.ContractorManagementAndProfessionalSalaried; // MaintTA
                        _MT.PrevTADate = _FuelsRefineryTurnaroundMaintenanceComplete.PreviousTurnaroundDate; // MaintTA
                        _MT.TAExceptions = _FuelsRefineryTurnaroundMaintenanceComplete.Exceptions; // MaintTA

                        retVal.MT_List.Add(_MT);
                    }
                }
                if (plantIn.OperatingExpenseStandardList.Count>0)
                {
                    level = "OpexData";
                     foreach (OperatingExpenseStandard _OperatingExpenseStandard in plantIn.OperatingExpenseStandardList)
                    {
                        OperatingExpenses _O = new OperatingExpenses();
                        _O.SubmissionID = _OperatingExpenseStandard.SubmissionID; // OpexData
                        _O.Property = _OperatingExpenseStandard.Property; // OpexData
                        _O.RptValue = _OperatingExpenseStandard.ReportValue; // OpexData
                        _O.OthDescription = _OperatingExpenseStandard.OtherDescription; // OpexData

                        retVal.O_List.Add(_O);
                    }
                }
                if (plantIn.PersonnelHoursCompleteList.Count>0)
                {
                    level = "Pers";
                     foreach (FuelsRefineryPersonnelHoursComplete _FuelsRefineryPersonnelHoursComplete in plantIn.PersonnelHoursCompleteList)
                    {
                        Personnel _P = new Personnel();

                        _P.SubmissionID = _FuelsRefineryPersonnelHoursComplete.SubmissionID; // Pers
                        _P.PersID = _FuelsRefineryPersonnelHoursComplete.PersonnelHoursID; // Pers
                        _P.NumPers = _FuelsRefineryPersonnelHoursComplete.NumberOfPersonnel; // Pers
                        _P.STH = _FuelsRefineryPersonnelHoursComplete.StraightTimeHours; // Pers
                        _P.OVTHours = _FuelsRefineryPersonnelHoursComplete.OvertimeHours; // Pers
                        _P.OVTPcnt = _FuelsRefineryPersonnelHoursComplete.OvertimePercent; // Pers
                        _P.Contract = _FuelsRefineryPersonnelHoursComplete.Contract; // Pers
                        _P.GA = _FuelsRefineryPersonnelHoursComplete.GeneralAndAdministrativeHours; // Pers
                        _P.AbsHrs = _FuelsRefineryPersonnelHoursComplete.AbsenceHours; // Pers
                        retVal.P_List.Add(_P);
                    }
                }
                if (plantIn.PlantOperatingConditionList.Count>0)
                {
                    level = "ProcessData";
                    foreach (PlantOperatingConditionBasic _PlantOperatingConditionBasic in plantIn.PlantOperatingConditionList)
                    {
                        ProcessingData _PD = new ProcessingData();
                        _PD.SubmissionID = _PlantOperatingConditionBasic.SubmissionID; // ProcessData
                        _PD.UnitID = _PlantOperatingConditionBasic.UnitID; // ProcessData
                        _PD.Property = _PlantOperatingConditionBasic.Property; // ProcessData
                        _PD.RptNVal = _PlantOperatingConditionBasic.ReportNumericValue; // ProcessData
                        _PD.RptTVal = _PlantOperatingConditionBasic.ReportTextValue; // ProcessData
                        _PD.RptDVal = _PlantOperatingConditionBasic.ReportDateValue; // ProcessData
                        _PD.UOM = _PlantOperatingConditionBasic.UnitOfMeasure; // ProcessData

                        retVal.PD_List.Add(_PD);
                    }
                }
                if (plantIn.FinishedResidualFuelCompleteList.Count>0)
                {
                    level = "Resid";
                       foreach (FuelsRefineryFinishedResidualFuelComplete _FuelsRefineryFinishedResidualFuelComplete in plantIn.FinishedResidualFuelCompleteList)
                       {
                           Residual _R = new Residual();
                        _R.SubmissionID = _FuelsRefineryFinishedResidualFuelComplete.SubmissionID; // Resid
                        _R.BlendID = _FuelsRefineryFinishedResidualFuelComplete.BlendID; // Resid
                        _R.Grade = _FuelsRefineryFinishedResidualFuelComplete.Grade; // Resid
                        _R.Density = _FuelsRefineryFinishedResidualFuelComplete.Density; // Resid
                        _R.Sulfur = _FuelsRefineryFinishedResidualFuelComplete.Sulfur; // Resid
                        _R.PourPT = _FuelsRefineryFinishedResidualFuelComplete.PourPointOfBlend; // Resid
                        _R.ViscCSAtTemp = _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendCentiStokeAtTemperature; // Resid
                        _R.ViscTemp = _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendTemperatureDifferent122; // Resid
                        _R.KMT = _FuelsRefineryFinishedResidualFuelComplete.ThousandMetricTons; // Resid
                        retVal.R_List.Add(_R);
                    }
                }
                if (plantIn.RefineryProducedFuelResidualCompleteList.Count>0)
                {
                    level = "RPFResid";
                   foreach (FuelsRefineryProducedFuelResidualComplete _FuelsRefineryProducedFuelResidualComplete in plantIn.RefineryProducedFuelResidualCompleteList)               
                    {
                        RPFResidual _RP = new RPFResidual();

                        _RP.SubmissionID = _FuelsRefineryProducedFuelResidualComplete.SubmissionID; // RPFResid
                        _RP.EnergyType = _FuelsRefineryProducedFuelResidualComplete.EnergyType; // RPFResid
                        _RP.Sulfur = _FuelsRefineryProducedFuelResidualComplete.Sulfur; // RPFResid
                        _RP.ViscTemp = _FuelsRefineryProducedFuelResidualComplete.ViscosityTemperature; // RPFResid
                        _RP.ViscCSAtTemp = _FuelsRefineryProducedFuelResidualComplete.ViscosityCentiStokesAtTemperature; // RPFResid
                        _RP.Density = _FuelsRefineryProducedFuelResidualComplete.Density; // RPFResid

                        retVal.RP_List.Add(_RP);
                    }
                }

                if (plantIn.SteamSystemCompleteList.Count>0)
                {
                    level = "SteamSystem";
                    foreach (FuelsRefinerySteamSystemComplete _FuelsRefinerySteamSystemComplete in plantIn.SteamSystemCompleteList)
                    {
                        Steam _S = new Steam();

                        _S.SubmissionID = _FuelsRefinerySteamSystemComplete.SubmissionID; // SteamSystem
                        _S.PressureRange = _FuelsRefinerySteamSystemComplete.PressureRange; // SteamSystem
                        _S.ActualPress = _FuelsRefinerySteamSystemComplete.ActualPressure; // SteamSystem
                        _S.H2PlantExport = _FuelsRefinerySteamSystemComplete.H2PlantNetExportSteam; // SteamSystem
                        _S.FiredProcessHeater = _FuelsRefinerySteamSystemComplete.FiredProcessHeaterConvectionSection; // SteamSystem
                        _S.FiredBoiler = _FuelsRefinerySteamSystemComplete.FiredBoiler; // SteamSystem
                        _S.FCCCatCoolers = _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerCoolers; // SteamSystem
                        _S.FCCStackGas = _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerStackGas; // SteamSystem
                        _S.FluidCokerCOBoiler = _FuelsRefinerySteamSystemComplete.FluidCokerCOBoiler; // SteamSystem
                        _S.Calciner = _FuelsRefinerySteamSystemComplete.Calciner; // SteamSystem
                        _S.FTCogen = _FuelsRefinerySteamSystemComplete.FiredTurbineCogeneration; // SteamSystem
                        _S.WasteHeatFCC = _FuelsRefinerySteamSystemComplete.WasteHeatFluidCatalystCracker; // SteamSystem
                        _S.WasteHeatTCR = _FuelsRefinerySteamSystemComplete.WasteHeatThermalCracker; // SteamSystem
                        _S.WasteHeatCOK = _FuelsRefinerySteamSystemComplete.WasteHeatCokerHeavyGasOilPumparound; // SteamSystem
                        _S.WasteHeatOth = _FuelsRefinerySteamSystemComplete.WasteHeatOtherBoilers; // SteamSystem
                        _S.OthSource = _FuelsRefinerySteamSystemComplete.OtherSteamSources; // SteamSystem
                        _S.STProd = _FuelsRefinerySteamSystemComplete.SubtotalSteamProduction; // SteamSystem
                        _S.Sold = _FuelsRefinerySteamSystemComplete.Sold; // SteamSystem
                        _S.Pur = _FuelsRefinerySteamSystemComplete.Purchased; // SteamSystem
                        _S.NetPur = _FuelsRefinerySteamSystemComplete.SubtotalNetPurchased; // SteamSystem
                        _S.TotSupply = _FuelsRefinerySteamSystemComplete.TotalSupply; // SteamSystem
                        _S.ConsProcessCDU = _FuelsRefinerySteamSystemComplete.ConsumedProcessCrudeAndVacuum; // SteamSystem
                        _S.ConsProcessCOK = _FuelsRefinerySteamSystemComplete.ConsumedProcessCoker; // SteamSystem
                        _S.ConsProcessFCC = _FuelsRefinerySteamSystemComplete.ConsumedProcessFluidCatalystCracker; // SteamSystem
                        _S.ConsProcessOth = _FuelsRefinerySteamSystemComplete.ConsumedProcessOther; // SteamSystem
                        _S.ConsReboil = _FuelsRefinerySteamSystemComplete.ConsumedReboilersEvaporators; // SteamSystem
                        _S.ConsOthHeaters = _FuelsRefinerySteamSystemComplete.ConsumedOtherHeaters; // SteamSystem
                        _S.ConsCondTurb = _FuelsRefinerySteamSystemComplete.ConsumedCondensingTurbines; // SteamSystem
                        _S.ConsTopTurbHigh = _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionFromHigherPressure; // SteamSystem
                        _S.ConsTopTurbLow = _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionToLowerPressure; // SteamSystem
                        _S.ConsCombAirPreheat = _FuelsRefinerySteamSystemComplete.ConsumedCombustionAirPreheat; // SteamSystem
                        _S.ConsPressControlHigh = _FuelsRefinerySteamSystemComplete.ConsumedPressureControlFromHigherPressure; // SteamSystem
                        _S.ConsPressControlLow = _FuelsRefinerySteamSystemComplete.ConsumedPressureControlToLowerPressure; // SteamSystem
                        _S.ConsTracingHeat = _FuelsRefinerySteamSystemComplete.ConsumedSteamTracingTankBuildingHeat; // SteamSystem
                        _S.ConsDeaerators = _FuelsRefinerySteamSystemComplete.ConsumedDeaerators; // SteamSystem
                        _S.ConsFlares = _FuelsRefinerySteamSystemComplete.ConsumedSteamToFlares; // SteamSystem
                        _S.ConsOth = _FuelsRefinerySteamSystemComplete.ConsumedAllOther; // SteamSystem
                        _S.ConsOth = _FuelsRefinerySteamSystemComplete.ConsumedTotalSteam; // SteamSystem
                        retVal.S_List.Add(_S);
                    }
                }
                if (plantIn.PlantSubmission != null)
                {
                    level = "Submissions";
                    SubmissionBasic _SubmissionBasic = plantIn.PlantSubmission;

                    Submissions _SB = new Submissions();
                        _SB.SubmissionID = _SubmissionBasic.SubmissionID; // Submissions
                        _SB.RefID = _SubmissionBasic.RefineryID; // Submissions
                        _SB.Date = _SubmissionBasic.SubmissionDate; // Submissions
                        _SB.UOM = _SubmissionBasic.UnitOfMeasure; // Submissions
                        _SB.RptCurrency = _SubmissionBasic.ReportCurrency; // Submissions

                        retVal.SB_List.Add(_SB);
                }
                if (plantIn.MaterialBalanceCompleteList.Count>0)
                {
                    level = "Yield";
                    foreach (FuelsRefineryMaterialBalanceComplete _FuelsRefineryMaterialBalanceComplete in plantIn.MaterialBalanceCompleteList)
                    {
                        Yields _Y = new Yields();

                        _Y.SubmissionID = _FuelsRefineryMaterialBalanceComplete .SubmissionID; // Yield
                        _Y.Period = _FuelsRefineryMaterialBalanceComplete .Period; // Yield
                        _Y.Category = _FuelsRefineryMaterialBalanceComplete .Category; // Yield
                        _Y.MaterialID = _FuelsRefineryMaterialBalanceComplete .MaterialID; // Yield
                        _Y.MaterialName = _FuelsRefineryMaterialBalanceComplete .MaterialName; // Yield
                        _Y.BBL = _FuelsRefineryMaterialBalanceComplete .Barrels; // Yield
                        _Y.MT = _FuelsRefineryMaterialBalanceComplete .MetricTons; // Yield
                        _Y.Density = _FuelsRefineryMaterialBalanceComplete .Density; // Yield
                        retVal.Y_List.Add(_Y);
                    }
                }
            }
            catch (Exception ex)
            {
                string template = "BusToWCF:Level={0} Error:{1}";
                Exception infoException = new Exception(string.Format(template, level, ex.Message));
                throw infoException;
            }
            return retVal;
        }

        //decommiss this
        public PL PrimeScram(string xmlFileIn, NewDataSet dataSetIn)
        {
            PL retVal = new PL();
            try
            {
                retVal.A_List = new List<Absences>();
                retVal.C_List = new List<Configuration>();
                retVal.CB_List = new List<Sa.Profile.Profile3.Services.Library.ConfigBuoy>();
                retVal.CD_List = new List<Sa.Profile.Profile3.Services.Library.Crude>();
                retVal.CR_List = new List<Sa.Profile.Profile3.Services.Library.ConfigRS>();
                retVal.D_List = new List<Sa.Profile.Profile3.Services.Library.DieselData>();
                retVal.E_List = new List<Sa.Profile.Profile3.Services.Library.Emissions>();
                retVal.EG_List = new List<Sa.Profile.Profile3.Services.Library.Energy>();
                retVal.EL_List = new List<Sa.Profile.Profile3.Services.Library.Electricity>();
                retVal.F_List = new List<Sa.Profile.Profile3.Services.Library.FiredHeaterData>();
                retVal.G_List = new List<Sa.Profile.Profile3.Services.Library.Gasoline>();
                retVal.GM_List = new List<Sa.Profile.Profile3.Services.Library.GeneralMisc>();
                retVal.I_List = new List<Sa.Profile.Profile3.Services.Library.Inventory>();
                retVal.K_List = new List<Sa.Profile.Profile3.Services.Library.Kerosene>();
                retVal.L_List = new List<Sa.Profile.Profile3.Services.Library.LPG>();
                retVal.M_List = new List<Sa.Profile.Profile3.Services.Library.MarineBunkerData>();
                retVal.ME_List = new List<Sa.Profile.Profile3.Services.Library.MaintExp>();
                retVal.MI_List = new List<Sa.Profile.Profile3.Services.Library.MiscellaneousInput>();
                retVal.MR_List = new List<Sa.Profile.Profile3.Services.Library.MaintRoutine>();
                retVal.MT_List = new List<Sa.Profile.Profile3.Services.Library.MaintTurnAround>();
                retVal.O_List = new List<Sa.Profile.Profile3.Services.Library.OperatingExpenses>();
                retVal.P_List = new List<Sa.Profile.Profile3.Services.Library.Personnel>();
                retVal.PD_List = new List<Sa.Profile.Profile3.Services.Library.ProcessingData>();
                retVal.R_List = new List<Sa.Profile.Profile3.Services.Library.Residual>();
                retVal.RP_List = new List<Sa.Profile.Profile3.Services.Library.RPFResidual>();
                retVal.S_List = new List<Sa.Profile.Profile3.Services.Library.Steam>();
                retVal.SB_List = new List<Sa.Profile.Profile3.Services.Library.Submissions>();
                retVal.Y_List = new List<Sa.Profile.Profile3.Services.Library.Yields>();

                NewDataSet ds = null;
                if (xmlFileIn.Length == 0)
                {
                    ds = dataSetIn;
                }
                else
                {
                    ds = new NewDataSet();
                    ds.ReadXml(xmlFileIn);
                }
                DataTableExtentions de = new DataTableExtentions();
                if (ds.Absence != null)
                {
                    List<Absences> itms = (from p in de.AsEnumerable2<Absences>(ds.Absence) select p).ToList();
                    foreach (Absences itm in itms)
                    {
                        Absences _A = new Absences();
                        _A.MPSAbs = itm.MPSAbs; // Absence
                        _A.OCCAbs = itm.OCCAbs; // Absence
                        _A.CategoryID = itm.CategoryID; // Absence
                        _A.SubmissionID = itm.SubmissionID; // Absence
                        retVal.A_List.Add(_A);
                    }
                }
                if (ds.Config != null)
                {
                    List<Configuration> itms = (from p in de.AsEnumerable2<Configuration>(ds.Config) select p).ToList();
                    foreach (Configuration itm in itms)
                    {
                        Configuration _C = new Configuration();
                        _C.EnergyPcnt = itm.EnergyPcnt; // Config
                        _C.UtilPcnt = itm.UtilPcnt; // Config
                        _C.Cap = itm.Cap; // Config
                        _C.ProcessType = itm.ProcessType; // Config
                        _C.UnitName= itm.UnitName; // Config
                        _C.ProcessID = itm.ProcessID; // Config
                        _C.UnitID = itm.UnitID; // Config
                        _C.SubmissionID = itm.SubmissionID; // Config
                        _C.AllocPcntOfCap = itm.AllocPcntOfCap; // Config
                        _C.BlockOp = itm.BlockOp; // Config
                        _C.PostPerShift = itm.PostPerShift; // Config
                        _C.MHPerWeek = itm.MHPerWeek; // Config
                        _C.YearsOper = itm.YearsOper; // Config
                        _C.InServicePcnt = itm.InServicePcnt; // Config
                        _C.StmUtilPcnt = itm.StmUtilPcnt; // Config
                        _C.StmCap = itm.StmCap; // Config
                        retVal.C_List.Add(_C);
                    }
                }
                if (ds.ConfigBuoy != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.ConfigBuoy> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.ConfigBuoy>(ds.ConfigBuoy) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.ConfigBuoy itm in itms)
                    {
                       Sa.Profile.Profile3.Services.Library.ConfigBuoy _CB = new Sa.Profile.Profile3.Services.Library.ConfigBuoy();
                        _CB.PcntOwnership = itm.PcntOwnership; // ConfigBuoy
                        _CB.LineSize = itm.LineSize; // ConfigBuoy
                        _CB.ShipCap = itm.ShipCap; // ConfigBuoy
                        _CB.ProcessID = itm.ProcessID; // ConfigBuoy
                        _CB.UnitName = itm.UnitName; // ConfigBuoy
                        _CB.UnitID = itm.UnitID; // ConfigBuoy
                        _CB.SubmissionID = itm.SubmissionID; // ConfigBuoy
                        retVal.CB_List.Add(_CB);
                    }
                }
                if (ds.Crude != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Crude> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Crude>(ds.Crude) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Crude itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Crude _CD = new Sa.Profile.Profile3.Services.Library.Crude();
                        _CD.Sulfur =itm.Sulfur; // Crude
                        _CD.Gravity = itm.Gravity; // Crude
                        _CD.BBL = itm.BBL; // Crude
                        _CD.CrudeName = itm.CrudeName; // Crude
                        _CD.CrudeNumber = itm.CrudeNumber;  // Crude
                        _CD.CrudeID = itm.CrudeID; // Crude
                        _CD.Period = itm.Period; // Crude
                        _CD.SubmissionID = itm.SubmissionID; // Crude
                        retVal.CD_List.Add(_CD);
                    }
                }
                if (ds.ConfigRS != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.ConfigRS> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.ConfigRS>(ds.ConfigRS) select p).ToList();            
                    foreach (Sa.Profile.Profile3.Services.Library.ConfigRS itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.ConfigRS _CR = new Sa.Profile.Profile3.Services.Library.ConfigRS();
                        _CR.Throughput = itm.Throughput; // ConfigRS
                        _CR.AvgSize = itm.AvgSize; // ConfigRS
                        _CR.ProcessType = itm.ProcessType; // ConfigRS
                        _CR.ProcessID = itm.ProcessID; // ConfigRS
                        _CR.UnitID = itm.UnitID; // ConfigRS
                        _CR.SubmissionID = itm.SubmissionID; // ConfigRS
                        retVal.CR_List.Add(_CR);
                    }
                }
                if (ds.Diesel != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.DieselData> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.DieselData>(ds.Diesel) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.DieselData itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.DieselData _D = new Sa.Profile.Profile3.Services.Library.DieselData();
                        _D.KMT = itm.KMT; // Diesel
                        _D.Type = itm.Type; // Diesel
                        _D.Market = itm.Market; // Diesel
                        _D.Grade = itm.Grade; // Diesel
                        _D.BlendID = (int?)itm.BlendID; // Diesel
                        _D.SubmissionID = itm.SubmissionID; // Diesel
                        _D.BiodieselPcnt = itm.BiodieselPcnt; // Diesel
                        _D.E350 = itm.E350; // Diesel
                        _D.ASTM90 = itm.ASTM90; // Diesel
                        _D.CloudPt = itm.CloudPt; // Diesel
                        _D.Sulfur = itm.Sulfur; // Diesel
                        _D.PourPt = itm.PourPt; // Diesel
                        _D.Cetane = itm.Cetane; // Diesel
                        _D.Density = itm.Density; // Diesel
                        retVal.D_List.Add(_D);
                    }
                }


                if (ds.Emissions != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Emissions> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Emissions>(ds.Emissions) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Emissions itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Emissions _E = new Sa.Profile.Profile3.Services.Library.Emissions();
                        _E.RefineryEmission = itm.RefineryEmission; // Emissions
                        _E.EmissionType = itm.EmissionType; // Emissions
                        _E.SubmissionID = itm.SubmissionID; // Emissions
                        retVal.E_List.Add(_E);
                    }
                }


                if (ds.Energy != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Electricity> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Electricity>(ds.Energy) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Electricity itm in itms)
                    {
                        
                            Sa.Profile.Profile3.Services.Library.Electricity _EL = new Sa.Profile.Profile3.Services.Library.Electricity();
                            _EL.GenEff = itm.GenEff; // Electric
                            _EL.PriceLocal = itm.PriceLocal; // Electric
                            _EL.Amount = itm.Amount; // Electric
                            _EL.TransactionType = itm.TransactionType; // Electric
                            _EL.EnergyType = itm.EnergyType; // Electric
                            _EL.SubmissionID = itm.SubmissionID; // Electric

                            retVal.EL_List.Add(_EL);

                     }
                }


                if (ds.Energy != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Energy> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Energy>(ds.Energy) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Energy itm in itms)
                    {
                            Sa.Profile.Profile3.Services.Library.Energy _EG = new Sa.Profile.Profile3.Services.Library.Energy();
                            _EG.N2 = itm.N2; // Energy
                            _EG.Butane = itm.Butane; // Energy
                            _EG.Propylene = itm.Propylene; // Energy
                            _EG.Propane = itm.Propane; // Energy
                            _EG.Ethylene = itm.Ethylene; // Energy
                            _EG.Ethane = itm.Ethane; // Energy
                            _EG.Methane = itm.Methane; // Energy
                            _EG.Hydrogen = itm.Hydrogen; // Energy
                            _EG.OverrideCalculation = itm.OverrideCalculation; // Energy
                            _EG.MillionBritishThermalUnitsOut = itm.MillionBritishThermalUnitsOut; // Energy
                            _EG.SO2 = itm.SO2; // Energy
                            _EG.PriceLocal = itm.PriceLocal; // Energy
                            _EG.Amount = itm.Amount; // Energy
                            _EG.TransType = itm.TransType; // Energy
                            _EG.EnergyType = itm.EnergyType; // Energy
                            _EG.SubmissionID = itm.SubmissionID; // Energy
                            _EG.SO2 = itm.SO2; // Energy
                            _EG.NH3 = itm.NH3; // Energy
                            _EG.CO = itm.CO; // Energy
                            _EG.H2S = itm.H2S; // Energy
                            _EG.C5Plus = itm.C5Plus; // Energy
                            _EG.Butylenes = itm.Butylenes; // Energy
                            _EG.Isobutane = itm.Isobutane; // Energy

                            retVal.EG_List.Add(_EG);
                    }
                }
    

                if (ds.FiredHeaters != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.FiredHeaterData> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.FiredHeaterData>(ds.FiredHeaters) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.FiredHeaterData itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.FiredHeaterData _F = new Sa.Profile.Profile3.Services.Library.FiredHeaterData();
                        _F.OtherDuty = itm.OtherDuty; // FiredHeaters
                        _F.StackTemp = itm.StackTemp; // FiredHeaters
                        _F.FurnOutTemp = itm.FurnOutTemp; // FiredHeaters
                        _F.FurnaceInTemp = itm.FurnaceInTemp; // FiredHeaters
                        _F.OthCombDuty = itm.OthCombDuty; // FiredHeaters
                        _F.FuelType = itm.FuelType; // FiredHeaters
                        _F.FiredDuty = itm.FiredDuty; // FiredHeaters
                        _F.ThroughputUOM = itm.ThroughputUOM; // FiredHeaters
                        _F.ThroughputRpt = itm.ThroughputRpt; // FiredHeaters
                        _F.ProcessFluid = itm.ProcessFluid; // FiredHeaters
                        _F.Service = itm.Service; // FiredHeaters
                        _F.ShaftDuty = itm.ShaftDuty; // FiredHeaters
                        _F.HeaterName = itm.HeaterName; // FiredHeaters
                        _F.ProcessID = itm.ProcessID; // FiredHeaters
                        _F.UnitID = itm.UnitID; // FiredHeaters
                        _F.HeaterNo = itm.HeaterNo; // FiredHeaters
                        _F.SubmissionID = itm.SubmissionID; // FiredHeaters
                        _F.SteamSuperHeated = itm.SteamSuperHeated; // FiredHeaters
                        _F.SteamDuty = itm.SteamDuty; // FiredHeaters
                        _F.ProcessDuty = itm.ProcessDuty; // FiredHeaters
                        _F.AbsorbedDuty = itm.AbsorbedDuty; // FiredHeaters
                        _F.CombAirTemp = itm.CombAirTemp; // FiredHeaters
                        _F.HeatLossPcnt = itm.HeatLossPcnt; // FiredHeaters
                        _F.StackO2 = itm.StackO2; // FiredHeaters
                        retVal.F_List.Add(_F);
                    }
                }
                if (ds.Gasoline != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Gasoline> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Gasoline>(ds.Gasoline) select p).ToList();
                     foreach (Sa.Profile.Profile3.Services.Library.Gasoline itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Gasoline _G = new Sa.Profile.Profile3.Services.Library.Gasoline();
                        _G.KMT = itm.KMT; // Gasoline
                        _G.MON = itm.MON; // Gasoline
                        _G.RON = itm.RON; // Gasoline
                        _G.RVP = itm.RVP; // Gasoline
                        _G.Density = itm.Density; // Gasoline
                        _G.Type = itm.Type; // Gasoline
                        _G.Market = itm.Market; // Gasoline
                        _G.Grade = itm.Grade; // Gasoline
                        _G.BlendID = itm.BlendID; // Gasoline
                        _G.SubmissionID = itm.SubmissionID; // Gasoline
                        _G.Lead = itm.Lead; // Gasoline
                        _G.Sulfur = itm.Sulfur; // Gasoline
                        _G.OthOxygen = itm.OthOxygen; // Gasoline
                        _G.TAME = itm.TAME; // Gasoline
                        _G.ETBE = itm.ETBE; // Gasoline
                        _G.MTBE = itm.MTBE; // Gasoline
                        _G.Ethanol = itm.Ethanol; // Gasoline
                        _G.Oxygen = itm.Oxygen; // Gasoline
                        retVal.G_List.Add(_G);
                    }
                }

                if (ds.GeneralMisc != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.GeneralMisc> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.GeneralMisc>(ds.GeneralMisc) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.GeneralMisc itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.GeneralMisc _GM = new Sa.Profile.Profile3.Services.Library.GeneralMisc();

                        _GM.TotLossMT = itm.TotLossMT; // GeneralMisc
                        _GM.FlareLossMT = itm.FlareLossMT; // GeneralMisc
                        _GM.SubmissionID = itm.SubmissionID; // GeneralMisc
                        retVal.GM_List.Add(_GM);
                    }
                }
                if (ds.Inventory != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Inventory> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Inventory>(ds.Inventory) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Inventory itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Inventory _I = new Sa.Profile.Profile3.Services.Library.Inventory();
                        _I.AvgLevel = itm.AvgLevel; // Inventory
                        _I.LeasedPcnt = itm.LeasedPcnt; // Inventory
                        _I.NumTank = (int?)itm.NumTank; // Inventory
                        _I.RefStorage = itm.RefStorage; // Inventory
                        _I.MandStorage = itm.MandStorage; // Inventory
                        _I.MktgStorage = itm.MktgStorage; // Inventory
                        _I.TotStorage = itm.TotStorage; // Inventory
                        _I.TankType = itm.TankType; // Inventory
                        _I.SubmissionID = itm.SubmissionID; // Inventory
                        retVal.I_List.Add(_I);
                    }
                }
                if (ds.Kerosene != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Kerosene> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Kerosene>(ds.Kerosene) select p).ToList();
                   foreach (Sa.Profile.Profile3.Services.Library.Kerosene itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Kerosene _K = new Sa.Profile.Profile3.Services.Library.Kerosene();
                        _K.KMT = itm.KMT; // Kerosene
                        _K.Sulfur = itm.Sulfur; // Kerosene
                        _K.Density = itm.Density; // Kerosene
                        _K.Type = itm.Type; // Kerosene
                        _K.Grade = itm.Grade; // Kerosene
                        _K.BlendID = itm.BlendID; // Kerosene
                        _K.SubmissionID = itm.SubmissionID; // Kerosene
                        retVal.K_List.Add(_K);
                    }
                }
                if (ds.LPG != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.LPG> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.LPG>(ds.LPG) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.LPG itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.LPG _L = new Sa.Profile.Profile3.Services.Library.LPG();
                        _L.VolC5Plus = itm.VolC5Plus; // LPG
                        _L.SubmissionID = itm.SubmissionID; // LPG
                        _L.VolC4ene = itm.VolC4ene; // LPG
                        _L.VolnC4 = itm.VolnC4; // LPG
                        _L.VoliC4 = itm.VoliC4; // LPG
                        _L.VolC3ene = itm.VolC3ene; // LPG
                        _L.VolC3 = itm.VolC3; // LPG
                        _L.VolC2Lt = itm.VolC2Lt; // LPG                       
                        _L.MolOrVol = itm.MolOrVol; // LPG
                        _L.BlendID =itm.BlendID; // LPG
                        retVal.L_List.Add(_L);
                    }
                }
                if (ds.MarineBunkers != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.MarineBunkerData> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.MarineBunkerData>(ds.MarineBunkers) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.MarineBunkerData itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.MarineBunkerData _M = new Sa.Profile.Profile3.Services.Library.MarineBunkerData();
                        _M.KMT = itm.KMT; // MarineBunkers
                        _M.SubmissionID = itm.SubmissionID; // MarineBunkers
                        _M.CrackedStock = itm.CrackedStock; // MarineBunkers
                        _M.ViscTemp = itm.ViscTemp; // MarineBunkers
                        _M.ViscCSAtTemp = itm.ViscCSAtTemp; // MarineBunkers
                        _M.PourPt = itm.PourPt; // MarineBunkers                        
                        _M.Sulfur = itm.Sulfur; // MarineBunkers
                        _M.Density = itm.Density; // MarineBunkers
                        _M.Grade = itm.Grade; // MarineBunkers
                        _M.BlendID = itm.BlendID; // MarineBunkers
                        retVal.M_List.Add(_M);
                    }
                }
                if (ds.MExp != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.MaintExp> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.MaintExp>(ds.MExp) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.MaintExp itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.MaintExp _ME = new Sa.Profile.Profile3.Services.Library.MaintExp();
                        _ME.TotMaint = itm.TotMaint; // MExp
                        _ME.SafetyOth = itm.SafetyOth; // MExp
                        _ME.RegOth = itm.RegOth; // MExp
                        _ME.RegDiesel = itm.RegDiesel; // MExp
                        _ME.RegGaso = itm.RegGaso; // MExp
                        _ME.RegExp = itm.RegExp; // MExp
                        _ME.ConstraintRemoval = itm.ConstraintRemoval; // MExp
                        _ME.NonRegUnit = itm.NonRegUnit; // MExp
                        _ME.NonMaintInvestExp = itm.NonMaintInvestExp; // MExp
                        _ME.RoutMaintCptl = itm.RoutMaintCptl; // MExp
                        _ME.TAMaintCptl = itm.TAMaintCptl; // MExp
                        _ME.MaintOvhd = itm.MaintOvhd; // MExp
                        _ME.NonRefExcl = itm.NonRefExcl; // MExp
                        _ME.ComplexCptl = itm.ComplexCptl; // MExp
                        _ME.SubmissionID = itm.SubmissionID; // MExp
                        _ME.MaintOvhdRout = itm.MaintOvhdRout; // MExp
                        _ME.MaintOvhdTA = itm.MaintOvhdTA; // MExp
                        _ME.MaintExpense = itm.MaintExpense; // MExp
                        _ME.MaintExpRout = itm.MaintExpRout; // MExp
                        _ME.MaintExpTA = itm.MaintExpTA; // MExp
                        _ME.OthInvest = itm.OthInvest; // MExp
                        _ME.Energy = itm.Energy; // MExp
                        retVal.ME_List.Add(_ME);
                    }
                }
                if (ds.MiscInput != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.MiscellaneousInput> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.MiscellaneousInput>(ds.MiscInput) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.MiscellaneousInput itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.MiscellaneousInput _MI = new Sa.Profile.Profile3.Services.Library.MiscellaneousInput();
                        _MI.OffsiteEnergyPcnt = itm.OffsiteEnergyPcnt; // MiscInput
                        _MI.CDUChargeMT = itm.CDUChargeMT; // MiscInput
                        _MI.CDUChargeBbl = itm.CDUChargeBbl; // MiscInput
                        _MI.SubmissionID = itm.SubmissionID; // MiscInput
                        retVal.MI_List.Add(_MI);
                    }
                }
                if (ds.MaintRout != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.MaintRoutine> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.MaintRoutine>(ds.MaintRout) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.MaintRoutine itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.MaintRoutine _MR = new Sa.Profile.Profile3.Services.Library.MaintRoutine();
                        _MR.InServicePcnt = itm.InServicePcnt; // MaintRout
                        _MR.RoutExpLocal = itm.RoutExpLocal; // MaintRout
                        _MR.RoutCostLocal = itm.RoutCostLocal; // MaintRout
                        _MR.OthSlow = itm.OthSlow; // MaintRout
                        _MR.OthDown = itm.OthDown; // MaintRout
                        _MR.OthNum = (int?)itm.OthNum; // MaintRout
                        _MR.MaintDown = itm.MaintDown; // MaintRout
                        _MR.MaintNum = (int?)itm.MaintNum; // MaintRout
                        _MR.RegDown = itm.RegDown; // MaintRout
                        _MR.RegNum = (int?)itm.RegNum; // MaintRout
                        _MR.ProcessID = itm.ProcessID; // MaintRout
                        _MR.UtilPcnt = itm.UtilPcnt; // MaintRout
                        _MR.UnitID = itm.UnitID; // MaintRout
                        _MR.SubmissionID = itm.SubmissionID; // MaintRout
                        _MR.OthDownOther = itm.OthDownOther; // MaintRout
                        _MR.OthDownOffsiteUpsets = itm.OthDownOffsiteUpsets; // MaintRout
                        _MR.OthDownUnitUpsets = itm.OthDownUnitUpsets; // MaintRout
                        _MR.OthDownExternal = itm.OthDownExternal; // MaintRout
                        _MR.OthDownEconomic = itm.OthDownEconomic; // MaintRout
                        _MR.RoutOvhdLocal = itm.RoutOvhdLocal; // MaintRout
                        _MR.RoutCptlLocal = itm.RoutCptlLocal; // MaintRout
                        retVal.MR_List.Add(_MR);
                    }
                }
                if (ds.MaintTA != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.MaintTurnAround> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.MaintTurnAround>(ds.MaintTA) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.MaintTurnAround itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.MaintTurnAround _MT = new Sa.Profile.Profile3.Services.Library.MaintTurnAround();
                        _MT.TAExceptions = (int?)itm.TAExceptions; // MaintTA
                        _MT.TAOvhdLocal= itm.TAOvhdLocal; // MaintTA
                        _MT.TACptlLocal = itm.TACptlLocal; // MaintTA
                        _MT.TAExpLocal = itm.TAExpLocal; // MaintTA
                        _MT.TACostLocal = itm.TACostLocal; // MaintTA
                        _MT.TAHrsDown = itm.TAHrsDown; // MaintTA
                        _MT.TADate = (DateTime?)itm.TADate; // MaintTA
                        _MT.ProcessID = itm.ProcessID; // MaintTA
                        _MT.UnitID = itm.UnitID; // MaintTA
                        _MT.SubmissionID = itm.SubmissionID; // MaintTA                       
                        _MT.PrevTADate = (DateTime?)itm.PrevTADate; // MaintTA
                        _MT.TAContMPS = itm.TAContMPS; // MaintTA
                        _MT.TAContOCC = itm.TAContOCC; // MaintTA
                        _MT.TAMPSOVTPcnt = itm.TAMPSOVTPcnt; // MaintTA
                        _MT.TAMPSSTH = itm.TAMPSSTH; // MaintTA
                        _MT.TAOCCOVT = itm.TAOCCOVT; // MaintTA
                        _MT.TAOCCSTH = itm.TAOCCSTH; // MaintTA
                        _MT.TALaborCostLocal = itm.TALaborCostLocal; // MaintTA
                        retVal.MT_List.Add(_MT);
                    }
                }
                if (ds.OpexData != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.OperatingExpenses> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.OperatingExpenses>(ds.OpexData) select p).ToList();
                   foreach (Sa.Profile.Profile3.Services.Library.OperatingExpenses itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.OperatingExpenses _O = new Sa.Profile.Profile3.Services.Library.OperatingExpenses();
                        _O.OthDescription = itm.OthDescription; // OpexData
                        _O.RptValue = itm.RptValue; // OpexData
                        _O.Property = itm.Property; // OpexData
                        _O.SubmissionID = itm.SubmissionID; // OpexData
                        retVal.O_List.Add(_O);
                    }
                }
                if (ds.Pers != null)
                {
                   List<Sa.Profile.Profile3.Services.Library.Personnel> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Personnel>(ds.Pers) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Personnel itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Personnel _P = new Sa.Profile.Profile3.Services.Library.Personnel();
                        _P.AbsHrs = itm.AbsHrs;
                        _P.GA = itm.GA;
                        _P.Contract = itm.Contract;
                        _P.OVTPcnt = itm.OVTPcnt;
                        _P.OVTHours = itm.OVTHours;
                        _P.STH = itm.STH;
                        _P.NumPers = itm.NumPers;
                        _P.PersID = itm.PersID;
                        _P.SubmissionID = itm.SubmissionID;
                        retVal.P_List.Add(_P);
                    }
                }
                if (ds.ProcessData != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.ProcessingData> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.ProcessingData>(ds.ProcessData) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.ProcessingData itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.ProcessingData _PD = new Sa.Profile.Profile3.Services.Library.ProcessingData();
                        _PD.UOM = itm.UOM; // ProcessData                       
                        _PD.RptDVal = (DateTime?)itm.RptDVal; // ProcessData
                        _PD.RptTVal = itm.RptTVal; // ProcessData
                        _PD.RptNVal = itm.RptNVal; // ProcessData                       
                        _PD.Property = itm.Property; // ProcessData
                        _PD.UnitID = itm.UnitID; // ProcessData
                        _PD.SubmissionID = itm.SubmissionID; // ProcessData
                        retVal.PD_List.Add(_PD);
                    }
                }
                if (ds.Resid != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Residual> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Residual>(ds.Resid) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Residual itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Residual _R = new Sa.Profile.Profile3.Services.Library.Residual();
                        _R.KMT = itm.KMT; // Resid
                        _R.ViscTemp = itm.ViscTemp; // Resid
                        _R.ViscCSAtTemp = itm.ViscCSAtTemp; // Resid
                        _R.PourPT = itm.PourPT; // Resid
                        _R.Sulfur = itm.Sulfur; // Resid
                        _R.Density = itm.Density; // Resid
                        _R.Grade = itm.Grade; // Resid
                        _R.BlendID = (int?)itm.BlendID; // Resid
                        _R.SubmissionID = itm.SubmissionID; // Resid
                        retVal.R_List.Add(_R);
                    }
                }
                if (ds.RPFResid != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.RPFResidual> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.RPFResidual>(ds.RPFResid) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.RPFResidual itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.RPFResidual _RP = new Sa.Profile.Profile3.Services.Library.RPFResidual();

                        _RP.Density = itm.Density; // RPFResid
                        _RP.ViscCSAtTemp = itm.ViscCSAtTemp; // RPFResid
                        _RP.ViscTemp = itm.ViscTemp; // RPFResid
                        _RP.Sulfur = itm.Sulfur; // RPFResid
                        _RP.EnergyType = itm.EnergyType; // RPFResid
                        _RP.SubmissionID = itm.SubmissionID; // RPFResid
                        retVal.RP_List.Add(_RP);
                    }
                }

                if (ds.SteamSystem != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Steam> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Steam>(ds.SteamSystem) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Steam itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Steam _S = new Sa.Profile.Profile3.Services.Library.Steam();
                        _S.TotCons = itm.TotCons; // SteamSystem
                        _S.ConsTopTurbHigh = itm.ConsTopTurbHigh; // SteamSystem
                        _S.ConsCondTurb = itm.ConsCondTurb; // SteamSystem
                        _S.ConsOthHeaters = itm.ConsOthHeaters; // SteamSystem
                        _S.ConsReboil = itm.ConsReboil; // SteamSystem
                        _S.ConsProcessOth = itm.ConsProcessOth; // SteamSystem
                        _S.ConsProcessFCC = itm.ConsProcessFCC; // SteamSystem
                        _S.ConsProcessCOK = itm.ConsProcessCOK; // SteamSystem
                        _S.ConsProcessCDU = itm.ConsProcessCDU; // SteamSystem
                        _S.TotSupply = itm.TotSupply; // SteamSystem
                        _S.NetPur = itm.NetPur; // SteamSystem
                        _S.ConsOth = itm.ConsOth; // SteamSystem
                        _S.Pur = itm.Pur; // SteamSystem
                        _S.Sold = itm.Sold; // SteamSystem
                        _S.STProd = itm.STProd; // SteamSystem
                        _S.OthSource = itm.OthSource; // SteamSystem
                        _S.WasteHeatOth = itm.WasteHeatOth; // SteamSystem
                        _S.WasteHeatCOK = itm.WasteHeatCOK; // SteamSystem
                        _S.WasteHeatTCR = itm.WasteHeatTCR; // SteamSystem
                        _S.WasteHeatFCC = itm.WasteHeatFCC; // SteamSystem
                        _S.FTCogen = itm.FTCogen; // SteamSystem
                        _S.Calciner = itm.Calciner; // SteamSystem
                        _S.ConsFlares = itm.ConsFlares; // SteamSystem
                        _S.FluidCokerCOBoiler = itm.FluidCokerCOBoiler; // SteamSystem
                        _S.FCCStackGas = itm.FCCStackGas; // SteamSystem
                        _S.FCCCatCoolers = itm.FCCCatCoolers; // SteamSystem
                        _S.FiredBoiler = itm.FiredBoiler; // SteamSystem
                        _S.FiredProcessHeater = itm.FiredProcessHeater; // SteamSystem
                        _S.H2PlantExport = itm.H2PlantExport; // SteamSystem
                        _S.ActualPress = itm.ActualPress; // SteamSystem
                        _S.PressureRange = itm.PressureRange; // SteamSystem
                        _S.SubmissionID = itm.SubmissionID; // SteamSystem
                        _S.ConsDeaerators = itm.ConsDeaerators; // SteamSystem
                        _S.ConsTracingHeat = itm.ConsTracingHeat; // SteamSystem
                        _S.ConsPressControlLow = itm.ConsPressControlLow; // SteamSystem
                        _S.ConsPressControlHigh = itm.ConsPressControlHigh; // SteamSystem
                        _S.ConsCombAirPreheat = itm.ConsCombAirPreheat; // SteamSystem
                        _S.ConsTopTurbLow = itm.ConsTopTurbLow; // SteamSystem
                        retVal.S_List.Add(_S);
                    }
                }
                if (ds.Submission != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Submissions> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Submissions>(ds.Submission) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Submissions itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Submissions _SB = new Sa.Profile.Profile3.Services.Library.Submissions();
                        _SB.RptCurrency = itm.RptCurrency; // Submissions
                        _SB.UOM = itm.UOM; // Submissions
                        _SB.Date = (DateTime?)itm.Date; // Submissions
                        _SB.RefID = itm.RefID; // Submissions
                        _SB.SubmissionID = itm.SubmissionID; // Submissions
                        retVal.SB_List.Add(_SB);
                    }
                }
                if (ds.Yield != null)
                {
                    List<Sa.Profile.Profile3.Services.Library.Yields> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Services.Library.Yields>(ds.Yield) select p).ToList();
                    foreach (Sa.Profile.Profile3.Services.Library.Yields itm in itms)
                    {
                        Sa.Profile.Profile3.Services.Library.Yields _Y = new Sa.Profile.Profile3.Services.Library.Yields();

                        _Y.Density = itm.Density; // Yield
                        _Y.MT = itm.MT; // Yield
                        _Y.BBL = itm.BBL; // Yield
                        _Y.MaterialName = itm.MaterialName; // Yield
                        _Y.MaterialID = itm.MaterialID; // Yield
                        _Y.Category = itm.Category; // Yield
                        _Y.Period = itm.Period; // Yield
                        _Y.SubmissionID = itm.SubmissionID; // Yield
                        retVal.Y_List.Add(_Y);
                    }
                }              
            }
            catch (Exception ex)
            {
                string template = "PrimeScram:Error={0}";
                Exception infoException = new Exception(string.Format(template, ex.Message));
                throw infoException;
            }
            return retVal;
        }
        //currently not used
        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                 //   MessageBox.Show("Error: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                  //  MessageBox.Show("Warning: {0}", e.Message);
                    break;

            }
        }
    }
}
