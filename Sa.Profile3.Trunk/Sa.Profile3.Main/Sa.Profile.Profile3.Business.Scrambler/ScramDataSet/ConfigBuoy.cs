using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class ConfigBuoy
    {
        public decimal? LineSize { get; set; }
        
        public decimal? PcntOwnership { get; set; }
        
        public string ProcessID { get; set; }
        
        public decimal? ShipCap { get; set; }
        
        public int? SubmissionID { get; set; }
        
        public int? UnitID { get; set; }
        
        public string UnitName { get; set; }
    }
}
