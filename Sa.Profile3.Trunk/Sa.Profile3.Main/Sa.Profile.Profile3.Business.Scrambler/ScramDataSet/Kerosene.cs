using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Kerosene
    {
        public int BlendID { get; set; }

        public decimal? Density { get; set; }
        
        public string Grade { get; set; }

        public decimal? KMT { get; set; }
        
        public int SubmissionID { get; set; }

        public decimal? Sulfur { get; set; }
        
        public string Type { get; set; }
    }
}
