using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    public class Inventory
    {
        public decimal? AvgLevel { get; set; }
        
        public decimal? LeasedPcnt { get; set; }

        public decimal? MandStorage { get; set; }

        public decimal? MktgStorage { get; set; }
        
        public short? NumTank { get; set; }

        public decimal? RefStorage { get; set; }
        
        public int SubmissionID { get; set; }
        
        public string TankType { get; set; }

        public decimal? TotStorage { get; set; }
    }
}
