using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MiscInput
    {
        public decimal? CDUChargeBbl { get; set; }
        
        public decimal? CDUChargeMT { get; set; }
        
        public decimal? OffsiteEnergyPcnt { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
