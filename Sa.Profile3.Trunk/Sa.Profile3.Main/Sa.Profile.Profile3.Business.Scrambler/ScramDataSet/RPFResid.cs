using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class RPFResid
    {
        public decimal? Density { get; set; }
        
        public string EnergyType { get; set; }
        
        public int SubmissionID { get; set; }
        
        public decimal? Sulfur { get; set; }
        
        public decimal? ViscCSAtTemp { get; set; }
        
        public decimal? ViscTemp { get; set; }
    }
}
