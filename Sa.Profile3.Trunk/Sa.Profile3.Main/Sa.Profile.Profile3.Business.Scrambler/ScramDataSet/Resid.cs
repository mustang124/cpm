using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Resid
    {
        public int BlendID { get; set; }
        
        public decimal? Density { get; set; }
        
        public string Grade { get; set; }
        
        public decimal? KMT { get; set; }
        
        public decimal? PourPT { get; set; }
        
        public int SubmissionID { get; set; }
        
        public decimal? Sulfur { get; set; }
        
        public decimal? ViscCSAtTemp { get; set; }
        
        public decimal? ViscTemp { get; set; }
    }
}
