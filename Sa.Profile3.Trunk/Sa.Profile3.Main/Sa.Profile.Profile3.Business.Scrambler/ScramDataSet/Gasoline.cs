using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Gasoline
    {
        public int BlendID { get; set; }

        public decimal? Density { get; set; }

        public decimal? ETBE { get; set; }

        public decimal? Ethanol { get; set; }
        
        public string Grade { get; set; }

        public decimal? KMT { get; set; }

        public decimal? Lead { get; set; }
        
        public string Market { get; set; }

        public decimal? MON { get; set; }

        public decimal? MTBE { get; set; }

        public decimal? OthOxygen { get; set; }

        public decimal? Oxygen { get; set; }

        public decimal? RON { get; set; }

        public decimal? RVP { get; set; }
        
        public int SubmissionID { get; set; }

        public decimal? Sulfur { get; set; }

        public decimal? TAME { get; set; }
        
        public string Type { get; set; }
    }
}
