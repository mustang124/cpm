using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Emission
    {
        public string EmissionType { get; set; }

        public decimal? RefEmissions { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
