using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Config
    {
        public decimal? AllocPcntOfCap { get; set; }
        
        public string BlockOp { get; set; }
        
        public decimal? Cap { get; set; }
        
        public decimal? EnergyPcnt { get; set; }
        
        public decimal? InServicePcnt { get; set; }
        
        public decimal? MHPerWeek { get; set; }
        
        public decimal? PostPerShift { get; set; }
        
        public string ProcessID { get; set; }
        
        public string ProcessType { get; set; }
        
        public decimal? StmCap { get; set; }
        
        public decimal? StmUtilPcnt { get; set; }
        
        public int SubmissionID { get; set; }
        
        public int UnitID { get; set; }
        
        public string UnitName { get; set; }
        
        public decimal? UtilPcnt { get; set; }
        
        public decimal? YearsOper { get; set; }
    }
}
