using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
   using Sa.Profile.Profile3.Business.PlantUnits;

namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public class PlantUnitFactory
    {
        public PlantUnitFactory()
        {
        }
        
        public PlantUnitFactory(PlantLevel plantConfigurationLevel)
        {
        }
        
        ~PlantUnitFactory()
        {
        }
        
        public List<ConfigurationBasic> ConfigurationList { get; set; }
        
        public PlantLevel PlantConfigurationLevel { get; set; }
        
        public List<PlantOperatingConditionBasic> PlantOperatingConditionList { get; set; }
        
        public SubmissionBasic PlantSubmission { get; set; }
    }
}
