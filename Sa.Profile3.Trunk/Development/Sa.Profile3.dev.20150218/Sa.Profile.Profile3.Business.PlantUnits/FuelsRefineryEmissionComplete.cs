///////////////////////////////////////////////////////////
//  FuelsRefineryEmissionComplete.cs
//  Implementation of the Class FuelsRefineryEmissionComplete
//  Generated by Enterprise Architect
//  Created on:      03-Nov-2014 10:06:42 AM
//  Original author: HJOHNSON
///////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;



using Sa.Profile.Profile3.Business.PlantUnits;
namespace Sa.Profile.Profile3.Business.PlantUnits {
	public class FuelsRefineryEmissionComplete : SABusinessObject {

		public FuelsRefineryEmissionComplete(){

		}

		~FuelsRefineryEmissionComplete(){

		}



		public string EmissionType{
			get;
			set;
		}

		public decimal? RefineryEmission{
			get;
			set;
		}
        protected override List<SARule> CreateRules()
        {
            List<SARule> rules = base.CreateRules();
            rules.Add(new SABasicRule("FuelsRefineryEmissionComplete", "Missing required field: EmmissionType or SubmissionID.", MandatoryFieldCheck, 0, false));
            return rules;
        }

        private bool MandatoryFieldCheck()
        {
            return (this.EmissionType != null 
                && this.SubmissionID != null);
        }
	}//end FuelsRefineryEmissionComplete

}//end namespace Sa.Profile.Profile3.Business.PlantUnits