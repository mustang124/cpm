﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data.Linq;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using Sa.Profile.Profile3.Common;

namespace Sa.Profile.Profile3.Repository.SA
{
    //error handling will be added later with logger
    public class XmlRepository : IRepository
    {
        private XDocument doc;
        string _plantStudyLogPathAndFile;
        Table<CalcResultSet> _plantTable;
        public RepositoryParms CurrentRepositoryParms { get; set; }

        public XmlRepository(RepositoryParms parms)
        {
            try
            {
                CurrentRepositoryParms = parms;
                _plantStudyLogPathAndFile = parms.XMLFolder + "PlantStudyLog.xml";
                if (!(System.IO.File.Exists(_plantStudyLogPathAndFile)))
                {
                    System.IO.File.Copy(_plantStudyLogPathAndFile + ".back", _plantStudyLogPathAndFile);
                }
                doc = XDocument.Load(_plantStudyLogPathAndFile);
                _plantTable = (new DataContext(parms.ConnStr)).GetTable<CalcResultSet>();
            }
            catch (Exception ex)
            {
                string template = "XmlRepository() Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, ex.Message));
                throw infoException;
            }
        }

        public int GetLatestSubmissionID()
        {
            List<PlantStudyLog> studs = GetAllStudies();
            return studs[studs.Count - 1].SASubmissionID;
        }

        public PlantStudyLog LoadStudy(DataWith wd, int ClientSubmissionID)
        {
            PlantStudyLog found = null;
            try
            {
                //get max sa sub id using client sub id
                found = (PlantStudyLog)(doc.Descendants("PlantStudyLog")
                    .Where(x => ((PlantStudyLog)x).ClientSubmissionID == ClientSubmissionID)
                    .OrderByDescending(x => ((PlantStudyLog)x).SASubmissionID)
                    .FirstOrDefault());
                if (found == null)
                {
                    throw new StudyException("Could not find PlantStudyLog for ClientSubmissionID=" + ClientSubmissionID);                
                }
                FuelsRefineryCompletePlantStudy stud = XmlTools.DeserializeFromXML<FuelsRefineryCompletePlantStudy>(CurrentRepositoryParms.XMLFolder + found.SASubmissionID.ToString() + ".xml");
                found.StudyBusinessObject = stud;
            }
            catch (Exception ex)
            {
                string template = "LoadStudy() Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, ex.Message));
                throw infoException;
            }
            return found;
        }

        public void SaveNewStudy(PlantStudyLog newPlantStudy)
        {
            try
            {
                var query = from plst in doc.Descendants("PlantStudyLog")
                            orderby (int)plst.Attribute("ID") descending
                            select plst;
                var lastPlant = (PlantStudyLog)query.FirstOrDefault();
                int id = 1;
                if (lastPlant != null)
                {
                    id = lastPlant.ID + 1;
                }
                var data = new XElement("PlantStudyLog",
                new XAttribute("ID", id),
                new XAttribute("SASubmissionID", id), //use same xml file ID for SAS
                new XAttribute("ClientSubmissionID", newPlantStudy.ClientSubmissionID),
                new XAttribute("PlantName", newPlantStudy.PlantName),
                new XAttribute("StudyLevel", newPlantStudy.StudyLevel),
                new XAttribute("StudyMode", newPlantStudy.StudyMode),
                new XAttribute("XMLFileLocation", newPlantStudy.XMLFileLocation),
                new XAttribute("TransactionDate", newPlantStudy.TransactionDate));
                doc.Element("PlantStudyLogs").Add(data);
                doc.Save(_plantStudyLogPathAndFile);
                XmlTools.SerializeToXML<FuelsRefineryCompletePlantStudy>((FuelsRefineryCompletePlantStudy)newPlantStudy.StudyBusinessObject, CurrentRepositoryParms.XMLFolder + id.ToString() + ".xml");
            }
            catch (Exception ex)
            {
                string template = "SaveNewStudy() Error:{0}";
                StudyException infoException = new StudyException(string.Format(template, ex.Message));
                throw infoException;
            }
        }

        public List<PlantStudyLog> GetAllStudies()
        {
            List<PlantStudyLog> retVal = doc.Descendants("PlantStudyLog").Select(x => (PlantStudyLog)x).ToList();
            return retVal;
        }

        //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        //doc.Load("SubmissionFull.xml");
        //DataSet ds = new DataSet();
        //byte[] buff = System.Text.ASCIIEncoding.ASCII.GetBytes(doc.OuterXml);
        //System.IO.MemoryStream ms = new System.IO.MemoryStream(buff);
        //ds.ReadXml(ms, XmlReadMode.InferSchema);
        //ms.Close();
        //ds.WriteXmlSchema("SubFul.xsd");
    }
}