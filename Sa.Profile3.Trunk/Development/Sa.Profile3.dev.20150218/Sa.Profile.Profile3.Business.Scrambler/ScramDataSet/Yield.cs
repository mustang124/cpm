using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    public class Yield
    {
        public double BBL { get; set; }
        
        public string Category { get; set; }
        
        public float? Density { get; set; }
        
        public string MaterialID { get; set; }
        
        public string MaterialName { get; set; }
        
        public double? MT { get; set; }
        
        public string Period { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
