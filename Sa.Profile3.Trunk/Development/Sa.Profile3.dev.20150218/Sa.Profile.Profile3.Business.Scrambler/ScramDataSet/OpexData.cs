using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class OpexData
    {
        public string OthDescription { get; set; }
        
        public string Property { get; set; }
        
        public decimal? RptValue { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
