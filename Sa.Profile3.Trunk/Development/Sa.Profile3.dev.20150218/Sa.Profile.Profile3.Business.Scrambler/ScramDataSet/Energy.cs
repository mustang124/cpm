using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Energy
    {
        public float? Amount { get; set; }
        
        public float? Butane { get; set; }
        
        public float? Butylenes { get; set; }
        
        public float? C5Plus { get; set; }
        
        public float? CO { get; set; }
        
        public float? CO2 { get; set; }
        
        public string EnergyType { get; set; }
        
        public float? Ethane { get; set; }
        
        public float? Ethylene { get; set; }
        
        public float? GenEff { get; set; }
        
        public float? H2S { get; set; }
        
        public float? Hydrogen { get; set; }
        
        public float? Isobutane { get; set; }
        
        public double? MBTUOut { get; set; }
        
        public float? Methane { get; set; }
        
        public float? N2 { get; set; }
        
        public float? NH3 { get; set; }
        
        public char? OverrideCalcs { get; set; }
        
        public float? PriceLocal { get; set; }
        
        public float? Propane { get; set; }
        
        public float? Propylene { get; set; }
        
        public float? SO2 { get; set; }
        
        public int SubmissionID { get; set; }
        
        public string TransType { get; set; }
    }
}
