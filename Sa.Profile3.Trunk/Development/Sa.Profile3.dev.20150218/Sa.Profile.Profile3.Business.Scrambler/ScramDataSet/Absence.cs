using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Absence
    {
        public string CategoryID { get; set; }
        
        public float? MPSAbs { get; set; }
        
        public float? OCCAbs { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
