﻿namespace Sa.Profile.Profile3.UI.StudyManager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlantButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BeerRadioButton1 = new System.Windows.Forms.RadioButton();
            this.ChemRadioButton2 = new System.Windows.Forms.RadioButton();
            this.FuelsRadioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CompleteRadioButton1 = new System.Windows.Forms.RadioButton();
            this.BasicRadioButton1 = new System.Windows.Forms.RadioButton();
            this.StandardRadioButton1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.XMLPrepButton2 = new System.Windows.Forms.Button();
            this.GetKPIsButton = new System.Windows.Forms.Button();
            this.CreateStudyButton = new System.Windows.Forms.Button();
            this.ScramblerButton = new System.Windows.Forms.Button();
            this.SubmissionIDTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PlantButton
            // 
            this.PlantButton.Location = new System.Drawing.Point(12, 23);
            this.PlantButton.Name = "PlantButton";
            this.PlantButton.Size = new System.Drawing.Size(109, 102);
            this.PlantButton.TabIndex = 0;
            this.PlantButton.Text = "Load Plant";
            this.PlantButton.UseVisualStyleBackColor = true;
            this.PlantButton.Click += new System.EventHandler(this.PlantButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BeerRadioButton1);
            this.groupBox1.Controls.Add(this.ChemRadioButton2);
            this.groupBox1.Controls.Add(this.FuelsRadioButton1);
            this.groupBox1.Location = new System.Drawing.Point(139, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 55);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // BeerRadioButton1
            // 
            this.BeerRadioButton1.AutoSize = true;
            this.BeerRadioButton1.Location = new System.Drawing.Point(168, 21);
            this.BeerRadioButton1.Name = "BeerRadioButton1";
            this.BeerRadioButton1.Size = new System.Drawing.Size(47, 17);
            this.BeerRadioButton1.TabIndex = 2;
            this.BeerRadioButton1.Text = "Beer";
            this.BeerRadioButton1.UseVisualStyleBackColor = true;
            // 
            // ChemRadioButton2
            // 
            this.ChemRadioButton2.AutoSize = true;
            this.ChemRadioButton2.Location = new System.Drawing.Point(96, 22);
            this.ChemRadioButton2.Name = "ChemRadioButton2";
            this.ChemRadioButton2.Size = new System.Drawing.Size(52, 17);
            this.ChemRadioButton2.TabIndex = 1;
            this.ChemRadioButton2.Text = "Chem";
            this.ChemRadioButton2.UseVisualStyleBackColor = true;
            // 
            // FuelsRadioButton1
            // 
            this.FuelsRadioButton1.AutoSize = true;
            this.FuelsRadioButton1.Checked = true;
            this.FuelsRadioButton1.Location = new System.Drawing.Point(9, 22);
            this.FuelsRadioButton1.Name = "FuelsRadioButton1";
            this.FuelsRadioButton1.Size = new System.Drawing.Size(50, 17);
            this.FuelsRadioButton1.TabIndex = 0;
            this.FuelsRadioButton1.TabStop = true;
            this.FuelsRadioButton1.Text = "Fuels";
            this.FuelsRadioButton1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CompleteRadioButton1);
            this.groupBox2.Controls.Add(this.BasicRadioButton1);
            this.groupBox2.Controls.Add(this.StandardRadioButton1);
            this.groupBox2.Location = new System.Drawing.Point(139, 73);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 52);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // CompleteRadioButton1
            // 
            this.CompleteRadioButton1.AutoSize = true;
            this.CompleteRadioButton1.Checked = true;
            this.CompleteRadioButton1.Location = new System.Drawing.Point(163, 19);
            this.CompleteRadioButton1.Name = "CompleteRadioButton1";
            this.CompleteRadioButton1.Size = new System.Drawing.Size(69, 17);
            this.CompleteRadioButton1.TabIndex = 2;
            this.CompleteRadioButton1.TabStop = true;
            this.CompleteRadioButton1.Text = "Complete";
            this.CompleteRadioButton1.UseVisualStyleBackColor = true;
            // 
            // BasicRadioButton1
            // 
            this.BasicRadioButton1.AutoSize = true;
            this.BasicRadioButton1.Location = new System.Drawing.Point(96, 19);
            this.BasicRadioButton1.Name = "BasicRadioButton1";
            this.BasicRadioButton1.Size = new System.Drawing.Size(51, 17);
            this.BasicRadioButton1.TabIndex = 1;
            this.BasicRadioButton1.Text = "Basic";
            this.BasicRadioButton1.UseVisualStyleBackColor = true;
            // 
            // StandardRadioButton1
            // 
            this.StandardRadioButton1.AutoSize = true;
            this.StandardRadioButton1.Location = new System.Drawing.Point(9, 19);
            this.StandardRadioButton1.Name = "StandardRadioButton1";
            this.StandardRadioButton1.Size = new System.Drawing.Size(68, 17);
            this.StandardRadioButton1.TabIndex = 0;
            this.StandardRadioButton1.Text = "Standard";
            this.StandardRadioButton1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // XMLPrepButton2
            // 
            this.XMLPrepButton2.Location = new System.Drawing.Point(15, 167);
            this.XMLPrepButton2.Name = "XMLPrepButton2";
            this.XMLPrepButton2.Size = new System.Drawing.Size(106, 23);
            this.XMLPrepButton2.TabIndex = 4;
            this.XMLPrepButton2.Text = "XML Prep";
            this.XMLPrepButton2.UseVisualStyleBackColor = true;
            this.XMLPrepButton2.Click += new System.EventHandler(this.XMLPrepButton2_Click);
            // 
            // GetKPIsButton
            // 
            this.GetKPIsButton.Location = new System.Drawing.Point(15, 225);
            this.GetKPIsButton.Name = "GetKPIsButton";
            this.GetKPIsButton.Size = new System.Drawing.Size(109, 23);
            this.GetKPIsButton.TabIndex = 7;
            this.GetKPIsButton.Text = "Get KPIs";
            this.GetKPIsButton.UseVisualStyleBackColor = true;
            this.GetKPIsButton.Click += new System.EventHandler(this.GetKPIsButton_Click);
            // 
            // CreateStudyButton
            // 
            this.CreateStudyButton.Location = new System.Drawing.Point(15, 196);
            this.CreateStudyButton.Name = "CreateStudyButton";
            this.CreateStudyButton.Size = new System.Drawing.Size(109, 23);
            this.CreateStudyButton.TabIndex = 8;
            this.CreateStudyButton.Text = "Create Study";
            this.CreateStudyButton.UseVisualStyleBackColor = true;
            this.CreateStudyButton.Click += new System.EventHandler(this.CreateStudyButton_Click);
            // 
            // ScramblerButton
            // 
            this.ScramblerButton.Location = new System.Drawing.Point(148, 167);
            this.ScramblerButton.Name = "ScramblerButton";
            this.ScramblerButton.Size = new System.Drawing.Size(105, 23);
            this.ScramblerButton.TabIndex = 9;
            this.ScramblerButton.Text = "Test Scrambler";
            this.ScramblerButton.UseVisualStyleBackColor = true;
            this.ScramblerButton.Click += new System.EventHandler(this.ScramblerButton_Click);
            // 
            // SubmissionIDTextBox
            // 
            this.SubmissionIDTextBox.Location = new System.Drawing.Point(148, 225);
            this.SubmissionIDTextBox.Name = "SubmissionIDTextBox";
            this.SubmissionIDTextBox.Size = new System.Drawing.Size(40, 20);
            this.SubmissionIDTextBox.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 269);
            this.Controls.Add(this.SubmissionIDTextBox);
            this.Controls.Add(this.ScramblerButton);
            this.Controls.Add(this.CreateStudyButton);
            this.Controls.Add(this.GetKPIsButton);
            this.Controls.Add(this.XMLPrepButton2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.PlantButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PlantButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton ChemRadioButton2;
        private System.Windows.Forms.RadioButton FuelsRadioButton1;
        private System.Windows.Forms.RadioButton BeerRadioButton1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton BasicRadioButton1;
        private System.Windows.Forms.RadioButton StandardRadioButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton CompleteRadioButton1;
        private System.Windows.Forms.Button XMLPrepButton2;
        private System.Windows.Forms.Button GetKPIsButton;
        private System.Windows.Forms.Button CreateStudyButton;
        private System.Windows.Forms.Button ScramblerButton;
        private System.Windows.Forms.TextBox SubmissionIDTextBox;
    }
}

