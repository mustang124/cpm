﻿//===============================================================================
// Microsoft patterns & practices
// Enterprise Library 6 Samples
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;

using System.Diagnostics;
using System.Runtime.Serialization;

using Sa.Profile.Profile3.Services.Library;
using Sa.Profile.Profile3.Common;

  public class Global : System.Web.HttpApplication
  {
      static ExceptionManager exManagerWeb;

    protected void Application_Start(object sender, EventArgs e)
    {
        LoggingConfiguration loggingConfiguration = BuildLoggingConfig();
        LogWriter logWriter = new LogWriter(loggingConfiguration);
        exManagerWeb = BuildExceptionManagerConfig(logWriter);
        ExceptionPolicy.SetExceptionManager(exManagerWeb);
    }


    private static ExceptionManager BuildExceptionManagerConfig(LogWriter logWriter)
    {
        var policies = new List<ExceptionPolicyDefinition>();
        var mappings = new NameValueCollection();
        mappings.Add("FaultID", "{Guid}");
        mappings.Add("FaultMessage", "{Message}");

        var assistingAdministrators = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(typeof (Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                     {
                       new LoggingExceptionHandler("General", 9000, TraceEventType.Error,
                         "Study Calculations Service", 5, typeof(TextExceptionFormatter), logWriter),
                       new ReplaceHandler("Application error.  Please advise your administrator and provide them with this error code: {handlingInstanceID}",
                         typeof(StudyException))
                     })
            };

        var exceptionShieldingAndLogging = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(typeof (Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                     {
                       new LoggingExceptionHandler("General", 9001, TraceEventType.Error,
                         "Study Calculations Service", 5, typeof(TextExceptionFormatter), logWriter),
                       new WrapHandler("Application Error. Please contact your administrator.",
                         typeof(StudyException))
                     })
            };

        var webStudyServicePolicy = new List<ExceptionPolicyEntry>
            {
                new ExceptionPolicyEntry(typeof(Exception),
                    PostHandlingAction.ThrowNewException,
                    new IExceptionHandler[]
                    {
                        new LoggingExceptionHandler("General", 9008, TraceEventType.Error,
                         "Web Study Calculations Service", 5, typeof(TextExceptionFormatter), logWriter),                 
                        new FaultContractExceptionHandler(typeof(Sa.Profile.Profile3.Services.Library.StudyCalculationFault), "Service Error. Please contact your administrator", mappings)
                    })
            };

       // policies.Add(new ExceptionPolicyDefinition("WebStudyServicePolicy", webStudyServicePolicy));
       
       // policies.Add(new ExceptionPolicyDefinition("AssistingAdministrators", assistingAdministrators));
        policies.Add(new ExceptionPolicyDefinition("ExceptionShieldingAndLogging", exceptionShieldingAndLogging));
       // policies.Add(new ExceptionPolicyDefinition("LoggingAndReplacingException", loggingAndReplacing));
       // policies.Add(new ExceptionPolicyDefinition("LogAndWrap", logAndWrap));
       // policies.Add(new ExceptionPolicyDefinition("ReplacingException", replacingException));
        return new ExceptionManager(policies);
    }

    private static LoggingConfiguration BuildLoggingConfig()
    {
        // Formatters
        TextFormatter formatter = new TextFormatter("Timestamp: {timestamp}{newline}Message: {message}{newline}Category: {category}{newline}Priority: {priority}{newline}EventId: {eventid}{newline}Severity: {severity}{newline}Title:{title}{newline}Machine: {localMachine}{newline}App Domain: {localAppDomain}{newline}ProcessId: {localProcessId}{newline}Process Name: {localProcessName}{newline}Thread Name: {threadName}{newline}Win32 ThreadId:{win32ThreadId}{newline}Extended Properties: {dictionary({key} - {value}{newline})}");

        // Listeners
        var flatFileTraceListener = new FlatFileTraceListener(@"C:\KNPC\Logs\WebStudyCalculator.log", "----------------------------------------", "----------------------------------------", formatter);
        var eventLog = new EventLog("Application", ".", "Enterprise Library Logging");
        var eventLogTraceListener = new FormattedEventLogTraceListener(eventLog);
        // Build Configuration
        var config = new LoggingConfiguration();
        config.AddLogSource("General", SourceLevels.All, true).AddTraceListener(eventLogTraceListener);
        config.LogSources["General"].AddTraceListener(flatFileTraceListener);

        // Special Sources Configuration
        config.SpecialSources.LoggingErrorsAndWarnings.AddTraceListener(flatFileTraceListener);

        return config;
    }

  }

