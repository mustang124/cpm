﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Sa.Profile.Profile3.Services.Library;


namespace Sa.Profile.Profile3.Services.StudyWeb
{
    //[ExceptionShielding("WebStudyServicePolicy")]
    [ExceptionShielding("ExceptionShieldingAndLogging")]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.Single)]
    public class StudyWebService : IStudyWebService
    {
        public Sa.Profile.Profile3.Services.Library.ResponseKs SubmitSt(Sa.Profile.Profile3.Services.Library.RequestSt reqSt)
        {
            Sa.Profile.Profile3.Services.Library.ResponseKs theResults = null;
            theResults = new Sa.Profile.Profile3.Services.Library.ResponseKs();
            theResults.ReturnKs = new Library.KPR();         
            win.StudyWinServiceClient cli = null;
            try
            {
                using (cli = new win.StudyWinServiceClient())
                {
                  theResults = cli.SubmitStudy(reqSt);
                }
            }
            catch (Exception ex)
            {
                var faultContract = ex as FaultException<Sa.Profile.Profile3.Services.Library.StudyCalculationFault>;
                cli.Abort();
                StudyCalculationFault salaryCalculationFault = faultContract.Detail;
                Sa.Profile.Profile3.Common.StudyException webEx = new Common.StudyException("Error in SubmitSt()." + Environment.NewLine + "Please report the following error code to admin:" + salaryCalculationFault.FaultID,ex.InnerException);
                throw webEx;
            }
            return theResults;
        }
     
        public Sa.Profile.Profile3.Services.Library.ResponseKs RequestStRes(Sa.Profile.Profile3.Services.Library.RequestSt requestSt)
        {
            Sa.Profile.Profile3.Services.Library.ResponseKs theResults = null;
            win.StudyWinServiceClient cli = null;
            try
            {
                using (cli = new win.StudyWinServiceClient())
                {
                    theResults = cli.RequestStudyResults(requestSt);
                }
            }
            catch (Exception ex)
            {
                var faultContract = ex as FaultException<Sa.Profile.Profile3.Services.Library.StudyCalculationFault>;                
                cli.Abort();
                StudyCalculationFault salaryCalculationFault = faultContract.Detail;              
                Sa.Profile.Profile3.Common.StudyException webEx = new Common.StudyException("Error in RequestStudyResults()." + Environment.NewLine +"Please report the following error code to admin:" + salaryCalculationFault.FaultID,ex.InnerException);
                throw webEx;
            }
            return theResults;
        }

        private string ShowFaultContract(Exception ex)
        {
            var faultContract = ex as FaultException<Sa.Profile.Profile3.Services.Library.StudyCalculationFault>;
             StringBuilder sb = new StringBuilder();
            if (faultContract != null)
            {               
                StudyCalculationFault salaryCalculationFault = faultContract.Detail;
                sb.Append("Fault contract detail: ");
                sb.Append(System.Environment.NewLine);
                sb.Append(String.Format("Fault ID: {0}", salaryCalculationFault.FaultID));
                sb.Append(System.Environment.NewLine);
                sb.Append(String.Format("Message: {0}", salaryCalculationFault.FaultMessage));
                
            }return sb.ToString();
        }
    }
}
