﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Sa.Profile.Profile3.Services.StudyWeb
{
    [ServiceContract]
    public interface IStudyWebService
    {
        [OperationContract]
        [FaultContract(typeof(Sa.Profile.Profile3.Services.Library.StudyCalculationFault))]
        Sa.Profile.Profile3.Services.Library.ResponseKs SubmitSt(Sa.Profile.Profile3.Services.Library.RequestSt submitSt);

        [OperationContract]
        [FaultContract(typeof(Sa.Profile.Profile3.Services.Library.StudyCalculationFault))]
        Sa.Profile.Profile3.Services.Library.ResponseKs RequestStRes(Sa.Profile.Profile3.Services.Library.RequestSt requestSt);
    }

}
