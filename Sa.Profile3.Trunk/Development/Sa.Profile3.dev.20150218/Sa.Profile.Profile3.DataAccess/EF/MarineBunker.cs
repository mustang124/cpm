//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Profile.Profile3.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class MarineBunker
    {
        public int SubmissionID { get; set; }
        public int BlendID { get; set; }
        public string Grade { get; set; }
        public Nullable<float> Gravity { get; set; }
        public Nullable<float> Density { get; set; }
        public Nullable<float> Sulfur { get; set; }
        public Nullable<float> PourPt { get; set; }
        public Nullable<float> ViscCS { get; set; }
        public Nullable<float> ViscCSAtTemp { get; set; }
        public Nullable<float> ViscTemp { get; set; }
        public Nullable<float> CrackedStock { get; set; }
        public Nullable<float> Vanadium { get; set; }
        public Nullable<float> KBbl { get; set; }
        public Nullable<float> KMT { get; set; }
    }
}
