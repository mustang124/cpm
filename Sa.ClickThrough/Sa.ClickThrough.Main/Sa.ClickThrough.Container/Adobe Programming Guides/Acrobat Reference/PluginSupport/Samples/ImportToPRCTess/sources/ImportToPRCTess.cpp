/*********************************************************************

ADOBE SYSTEMS INCORPORATED
Copyright(C)1998-2008 Adobe Systems Incorporated
All rights reserved.

NOTICE: Adobe permits you to use,modify,and distribute this file
in accordance with the terms of the Adobe license agreement
accompanying it. If you have received this file from a source other
than Adobe,then your use,modification,or distribution of it
requires the prior written permission of Adobe.

*********************************************************************/

// Acrobat Headers.
#ifndef MAC_PLATFORM
#include <PIHeaders.h>
#endif

#include <math.h>

#define A3D_API(returntype,name,params)\
typedef returntype (*PF##name) params;\
static PF##name name = NULL;

#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKMarkup.h>
#include <A3DSDKGlobalData.h>
#include <A3DSDKTexture.h>
#include <A3DSDKMisc.h>

#undef __A3DPRCSDK_H__
#undef __A3DPRCTYPES_H__
#undef __A3DPRCBASE_H__
#undef __A3DPRCERRORCODES_H__
#undef __A3DPRCGEOMETRY_H__
#undef __A3DPRCTESSELLATION_H__
#undef __A3DPRCGRAPHICS_H__
#undef __A3DPRCSTRUCTURE_H__
#undef __A3DPRCROOTENTITIES_H__
#undef __A3DPRCREPITEMS_H__
#undef __A3DPRCMARKUP_H__
#undef __A3DPRCGLOBALDATA_H__
#undef __A3DPRCTEXTURE_H__
#undef __A3DPRCMISC_H__

#undef A3D_API

void A3DPRCFunctionPointersInitialize(HMODULE hModule)
{
#define A3D_API(returntype,name,params) name = (PF##name)GetProcAddress(hModule,#name);
#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKMarkup.h>
#include <A3DSDKGlobalData.h>
#include <A3DSDKTexture.h>
#include <A3DSDKMisc.h>
#undef A3D_API
}

HMODULE A3DPRCLoadLibrary()
{
	HMODULE hModuleA3DPRCSDK;

	wchar_t acFilePath[MAX_PATH];
	GetModuleFileNameW(NULL, acFilePath, MAX_PATH);
	wchar_t* backslash = wcsrchr(acFilePath, L'\\');

	if (backslash)
		acFilePath[backslash - acFilePath] = 0;

	wcscat(acFilePath, L"\\A3DLIB.dll");

#ifdef UNICODE 
	hModuleA3DPRCSDK = LoadLibraryExW(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#else
	hModuleA3DPRCSDK = LoadLibraryExA(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#endif
	if (hModuleA3DPRCSDK)
		return hModuleA3DPRCSDK;

	return NULL;
}

void A3DPRCUnloadLibrary(HMODULE hModule)
{
	FreeLibrary(hModule);
}

AVStatusMonitorProcsRec gStatusMonitor;

ASInt32 gLast = 0;
ASInt32 gSize = 0;
ASInt32 gBreak = 0;

enum {
	keParsing,
	keReading,
	keSaving
} eProcessType;

//////////////////////////////////////////////////////////////////////////
//
//	A3DPRC SDK Callbacks
//
//////////////////////////////////////////////////////////////////////////
A3DPtr allocCllBck(ASUns32 size)
{
	return (A3DPtr)ASmalloc(size);
}

A3DVoid freeCllBck(A3DVoid* p)
{
	if(p)
		ASfree(p);
}

ASInt32 msgCllBck(A3DUTF8Char* msg)
{
#ifdef _DEBUG
	OutputDebugStringA(msg);
#endif
	return 0;
}

ASInt32 warCllBck(A3DUTF8Char* war, A3DUTF8Char* msg)
{
	if(gStatusMonitor.reportProc && msg && strlen(msg) > 0) {
		ASText asText = ASTextNew();
		ASTextSetEncoded(asText, (char*)msg, ASScriptToHostEncoding(kASRomanScript));
		gStatusMonitor.reportProc(kASReportWarning, 0, asText, NULL, NULL,
			gStatusMonitor.reportProcClientData);
		ASTextDestroy(asText);
	}
	return msgCllBck(war);
}

ASInt32 errCllBck(A3DUTF8Char* err, A3DUTF8Char* msg)
{
	if(gStatusMonitor.reportProc && msg && strlen(msg) > 0){
		ASText asText = ASTextNew();
		ASTextSetEncoded(asText, (char*)msg, ASScriptToHostEncoding(kASRomanScript));
		gStatusMonitor.reportProc(kASReportError, 0, asText, NULL, NULL,
			gStatusMonitor.reportProcClientData);
		ASTextDestroy(asText);
	}
	return msgCllBck(err);
}

A3DVoid prgStartCllBck(ASInt32 iProc)
{
	gLast = 0;
	if(gStatusMonitor.progMon && gStatusMonitor.progMon->beginOperation)
		gStatusMonitor.progMon->beginOperation(gStatusMonitor.progMonClientData);
	if(gStatusMonitor.progMon && gStatusMonitor.progMon->setCurrValue)
		gStatusMonitor.progMon->setCurrValue(0, gStatusMonitor.progMonClientData);

	AVSysSetWaitCursor(true, 0);
}

A3DVoid prgSizeCllBck(ASInt32 iSize)
{
	gSize = iSize;
	if(gStatusMonitor.progMon && gStatusMonitor.progMon->setDuration)
		gStatusMonitor.progMon->setDuration(100, gStatusMonitor.progMonClientData);
}

A3DVoid prgIncCllBck(ASInt32 i)
{
	if((i * 100) / gSize > gLast) {
		if(gStatusMonitor.progMon && gStatusMonitor.progMon->setCurrValue && gSize){
			gStatusMonitor.progMon->setCurrValue((i * 100) / gSize,
				gStatusMonitor.progMonClientData);
		}
		gLast = (i * 100) / gSize;
	}
}

A3DVoid prgEndCllBck(A3DVoid)
{
	if(gStatusMonitor.progMon && gStatusMonitor.progMon->endOperation)
		gStatusMonitor.progMon->endOperation(gStatusMonitor.progMonClientData);

	gLast = 0;

	AVSysSetWaitCursor(false, 0);
}

A3DVoid prgTitleCllBck(A3DUTF8Char* msg)
{
	if(gStatusMonitor.progMon && gStatusMonitor.progMon->setText 
		&& msg && strlen(msg) > 0) {	
		ASText asText = ASTextNew();
		ASTextSetEncoded(asText, (char*)msg, ASScriptToHostEncoding(kASRomanScript));
		gStatusMonitor.progMon->setText(asText, gStatusMonitor.progMonClientData);
		ASTextDestroy(asText);
	} else if (gStatusMonitor.progMon && gStatusMonitor.progMon->setText) {
		ASText asText = ASTextNew();
		ASTextSetEncoded(asText, "Processing...", ASScriptToHostEncoding(kASRomanScript));
		gStatusMonitor.progMon->setText(asText, gStatusMonitor.progMonClientData);
		ASTextDestroy(asText);
	}	
}

//////////////////////////////////////////////////////////////////////////
//
//	Processing ASCII input file 
//
//////////////////////////////////////////////////////////////////////////

A3DVoid setEntityName(const A3DUTF8Char* pName, A3DEntity** ppEntity)
{
	if(ppEntity != NULL) {
		A3DRootBaseData sBaseData;
		A3D_INITIALIZE_DATA(sBaseData);
		sBaseData.m_pcName = (A3DUTF8Char*)pName;
		A3DRootBaseSet(*ppEntity, &sBaseData);
	}
}

ASBool buildTess(const ASUTF16Val* stlName, const A3DUTF8Char* prcName)
{
	ASBool bRet = false;
	FILE* stlFile = _wfopen(reinterpret_cast<const wchar_t*>(stlName), L"r");
	if(stlFile) {
		char sBuffer[A3D_MAX_BUFFER];
		memset(sBuffer, 0, A3D_MAX_BUFFER);
		A3DUTF8Char key[100];

		// sanity check, make sure the input stl file is an ascii file
		if(fgets(sBuffer, A3D_MAX_BUFFER, stlFile) > 0) {
			sscanf(sBuffer, "%s", key);
			if(_stricmp(key, "solid")) {
				fclose(stlFile);
				AVAlertNote("The file selected is not an ASCII STL file!");
				return false;
			}
		}

		fseek(stlFile, 80, SEEK_SET);
		char ch;
		for(ASUns32 idx = 0; (idx < 100) && (feof(stlFile) == 0); idx++) {
			ch = fgetc(stlFile);
			if((ch < 0) || (ch > 127)) {
				fclose(stlFile);
				AVAlertNote("The file selected is not an ASCII STL file!");
				return false;
			}
		}

		fseek(stlFile, 0, SEEK_END);
		ASInt32 curr = 0, size = ftell(stlFile);

		ASUns32 facetSize = 0;

		prgStartCllBck((ASInt32)keParsing);
		prgTitleCllBck("Parsing file...");
		prgSizeCllBck(size);
		fseek(stlFile, 0, SEEK_SET);

		while(fgets(sBuffer, A3D_MAX_BUFFER, stlFile) > 0) {
			sscanf(sBuffer, "%s", key);
			if (!_stricmp(key, "facet"))
				facetSize++;

			curr += strlen(sBuffer);
			memset(sBuffer, 0, A3D_MAX_BUFFER);
			prgIncCllBck(curr);
		}
		prgEndCllBck();

		A3DTessBaseData sTessBaseData;
		A3D_INITIALIZE_DATA(sTessBaseData);
		sTessBaseData.m_uiCoordSize = 9 * facetSize;
		sTessBaseData.m_pdCoords = (A3DDouble*)allocCllBck(sTessBaseData.m_uiCoordSize * sizeof(A3DDouble));
		A3DTess3DData sTess3DData;
		A3D_INITIALIZE_DATA(sTess3DData);
		sTess3DData.m_uiNormalSize = 3 * facetSize;
		sTess3DData.m_pdNormals = (A3DDouble*)allocCllBck(sTess3DData.m_uiNormalSize * sizeof(A3DDouble));

		// Total amount of indexes is (one normal + one triangle (3 points)) * Number of facets 
		sTess3DData.m_uiTriangulatedIndexSize = 4 * facetSize;
		sTess3DData.m_puiTriangulatedIndexes = (ASUns32*)allocCllBck(sTess3DData.m_uiTriangulatedIndexSize * sizeof(ASUns32));
		A3DUTF8Char name[100];
		A3DUTF8Char dummy[100];
		A3DDouble x, y, z;
		ASInt32 iRet = 0;
		ASUns32 i = 0;
		ASUns32 j = 0;
		const A3DUTF8Char* pcName = NULL;

		prgStartCllBck((ASInt32)keReading);
		prgTitleCllBck("Reading file...");
		prgSizeCllBck(size);

		rewind(stlFile);
		curr = 0;
		facetSize = 0;
		memset(sBuffer, 0, A3D_MAX_BUFFER);
		while(fgets(sBuffer, A3D_MAX_BUFFER, stlFile) > 0) {
			sscanf(sBuffer, "%s", key);
			if(!_stricmp(key, "solid")) {
				iRet = sscanf(sBuffer, "%s %s", key, name);
				if (iRet != 2)
					sprintf(name,"unnamed");
			} else if (!_stricmp(key, "facet")) {
				iRet = sscanf(sBuffer,"%s %s %lf %lf %lf", key, dummy, &x, &y, &z);
				sTess3DData.m_puiTriangulatedIndexes[4 * facetSize] = i;
				sTess3DData.m_puiTriangulatedIndexes[4 * facetSize + 1] = j;
				sTess3DData.m_puiTriangulatedIndexes[4 * facetSize + 2] = j + 3;
				sTess3DData.m_puiTriangulatedIndexes[4 * facetSize + 3] = j + 6;
				sTess3DData.m_pdNormals[i++] = x;
				sTess3DData.m_pdNormals[i++] = y;
				sTess3DData.m_pdNormals[i++] = z;
				facetSize++;
			} else if (!_stricmp(key, "vertex")) {
				iRet = sscanf(sBuffer, "%s %lf %lf %lf", key, &x, &y, &z);
				sTessBaseData.m_pdCoords[j++] = x;
				sTessBaseData.m_pdCoords[j++] = y;
				sTessBaseData.m_pdCoords[j++] = z;
			}
			curr += strlen(sBuffer);
			prgIncCllBck(curr);
			memset(sBuffer, 0, A3D_MAX_BUFFER);
		}
		fclose(stlFile);
		prgEndCllBck();

		// one can consider that STL consists in one single face made of total amount of triangles 
		A3DTessFaceData sTessFaceData;
		A3D_INITIALIZE_DATA(sTessFaceData);
		sTessFaceData.m_usUsedEntitiesFlags = kA3DTessFaceDataTriangleOneNormal; 
		sTessFaceData.m_uiSizesTriangulatedSize = 1; /* size of the next array */
		sTessFaceData.m_puiSizesTriangulated = &facetSize;
		sTessFaceData.m_uiStartTriangulated = 0;

		// so, only one face 
		sTess3DData.m_uiFaceTessSize = 1;
		sTess3DData.m_psFaceTessData = &sTessFaceData;
		A3DTess3D* pTess3D = NULL;
		A3DTess3DCreate(&sTess3DData, &pTess3D);
		A3DTessBaseSet(pTess3D, &sTessBaseData);

		// create a PolyBrepModel representation item 
		A3DRiPolyBrepModel *pRiPolyBrepModel = NULL;
		A3DRiPolyBrepModelData sPolyBrepModelData;
		A3D_INITIALIZE_DATA(sPolyBrepModelData);
		sPolyBrepModelData.m_bIsClosed = TRUE;
		A3DRiPolyBrepModelCreate(&sPolyBrepModelData, &pRiPolyBrepModel);

		// assign the tessellation to the PolyBrepModel 
		A3DRiRepresentationItemData sRiData;
		A3D_INITIALIZE_DATA(sRiData);
		sRiData.m_pTessBase = pTess3D;
		A3DRiRepresentationItemSet(pRiPolyBrepModel, &sRiData);

		// create a PartDefinition to stock the PolyBrepModel 
		A3DAsmPartDefinition *pPartDefinition = NULL;
		A3DAsmPartDefinitionData sPartDefinitionData;
		A3D_INITIALIZE_DATA(sPartDefinitionData);
		sPartDefinitionData.m_uiRepItemsSize = 1;
		sPartDefinitionData.m_ppRepItems = &pRiPolyBrepModel;
		A3DAsmPartDefinitionCreate(&sPartDefinitionData, &pPartDefinition);

		// create a ProductOccurrence to stock the PartDefinition 
		A3DAsmProductOccurrence* pRootProductOccurrence = NULL;
		A3DAsmProductOccurrenceData sProductOccurrenceData;
		A3D_INITIALIZE_DATA(sProductOccurrenceData);
		sProductOccurrenceData.m_pPart = pPartDefinition;
		A3DAsmProductOccurrenceCreate(&sProductOccurrenceData, &pRootProductOccurrence);

		// name is mandatory for Product occurrence
		setEntityName(name, &pRootProductOccurrence);

		// create a ModelFile
		A3DAsmModelFile* pModelFile = NULL;
		A3DAsmModelFileData sData;
		A3D_INITIALIZE_DATA(sData);
		sData.m_uiPOccurrencesSize = 1;
		sData.m_dUnit = 1.0;
		sData.m_ppPOccurrences = &pRootProductOccurrence;
		A3DAsmModelFileCreate(&sData, &pModelFile);

		// Name is mandatory for ModelFile
		setEntityName("ModelFile", &pModelFile);


		/* Save the PRC File */
		A3DAsmModelFileWriteParametersData sParametersData;
		A3D_INITIALIZE_DATA(sParametersData);
		sParametersData.m_eFormatChoice = kA3DPRC;
		sParametersData.m_bCompressBrep = true;
		sParametersData.m_bCompressTessellation = true;
		sParametersData.m_eCompressBrepChoice = kA3DMeddiumCompression;

		// Write the model file into physical file
		A3DAsmModelFileWriteToFile(pModelFile, &sParametersData, prcName);
		A3DAsmModelFileDelete(pModelFile);

		if(sTessBaseData.m_pdCoords)
			ASfree(sTessBaseData.m_pdCoords);
		if(sTess3DData.m_pdNormals)
			ASfree(sTess3DData.m_pdNormals);
		if(sTess3DData.m_puiTriangulatedIndexes)
			ASfree(sTess3DData.m_puiTriangulatedIndexes);
		bRet = true;
	} else {
		AVAlertNote("Unable to open STL file!");
	}
	return bRet;
}

//////////////////////////////////////////////////////////////////////////
//
//	build the apperance stream in its AP entry that has a normal appearance 
//	(the N entry) for the 3D annotation
//
//////////////////////////////////////////////////////////////////////////
PDDoc Create1PageDocWithWatermark(void)
{
	// create new document
	PDDoc pdDoc = PDDocCreate();

	// Set the media box of the new document pages
	ASFixedRect mediaBox = {fixedZero, Int16ToFixed(792), Int16ToFixed(612), fixedZero};

	// Create page with the mediabox and insert as first page
	PDPage pdPage = PDDocCreatePage(pdDoc, PDBeforeFirstPage, mediaBox);

	ASText astextSrcText = ASTextFromScriptText("Import To PRC", kASRomanScript);

	PDColorValueRec pdColorValue = {PDDeviceRGB, {fixedZero, fixedZero, fixedOne, fixedZero}};

	PDDocWatermarkTextParamsRec pdDocWatermarkTextParamsRec;
	memset(&pdDocWatermarkTextParamsRec, 0, sizeof(pdDocWatermarkTextParamsRec));
	pdDocWatermarkTextParamsRec.size = sizeof(PDDocAddWatermarkParamsRec);
	pdDocWatermarkTextParamsRec.srcText     = astextSrcText;
	pdDocWatermarkTextParamsRec.textAlign   = kPDHorizCenter; 
	pdDocWatermarkTextParamsRec.pdeFont     = NULL; // use sysFontName only
	pdDocWatermarkTextParamsRec.sysFontName = ASAtomFromString("Courier");
	pdDocWatermarkTextParamsRec.fontSize    = 20.0f;
	pdDocWatermarkTextParamsRec.color       = pdColorValue; 

	PDPageRange pdPageRange = {0, 0, PDAllPages};

	PDDocAddWatermarkParamsRec  pdDocAddWatermarkParamsRec;
	memset(&pdDocAddWatermarkParamsRec, 0, sizeof(pdDocAddWatermarkParamsRec));
	pdDocAddWatermarkParamsRec.size = sizeof(PDDocAddWatermarkParamsRec);
	pdDocAddWatermarkParamsRec.targetRange = pdPageRange;
	pdDocAddWatermarkParamsRec.fixedPrint = false;
	pdDocAddWatermarkParamsRec.zOrderTop = true; // false for background
	pdDocAddWatermarkParamsRec.showOnScreen = true;
	pdDocAddWatermarkParamsRec.showOnPrint = true;
	pdDocAddWatermarkParamsRec.horizAlign = kPDHorizCenter;
	pdDocAddWatermarkParamsRec.vertAlign = kPDVertCenter;
	pdDocAddWatermarkParamsRec.horizValue = 0.0f;
	pdDocAddWatermarkParamsRec.vertValue = 1.0f;
	pdDocAddWatermarkParamsRec.percentageVals = false;
	pdDocAddWatermarkParamsRec.scale = 1.0f;
	pdDocAddWatermarkParamsRec.rotation = 0.0f;
	pdDocAddWatermarkParamsRec.opacity = 1.0f;
	pdDocAddWatermarkParamsRec.progMonData = NULL;
	pdDocAddWatermarkParamsRec.cancelProcData = NULL;
	pdDocAddWatermarkParamsRec.progMon = NULL;
	pdDocAddWatermarkParamsRec.cancelProc = NULL;
	PDDocAddWatermarkFromText(pdDoc, &pdDocWatermarkTextParamsRec, &pdDocAddWatermarkParamsRec);

	ASTextDestroy(astextSrcText);

	return pdDoc;
}

CosObj ExtractFormXObj(PDDoc pdDoc)
{
	CosObj cosObj = CosNewNull();
	ASInt32 i, numElems, pdeType, pdeType2, numSubElems;
	PDEContent pdeContent = NULL, pdeContent2 = NULL;
	PDPage pdPage = NULL;
	PDEElement pdeElement, pdeSubElem;
	PDEForm pdeForm;

DURING
	// go through the first page to find the Form XObject
	pdPage = PDDocAcquirePage(pdDoc, 0);
	pdeContent = PDPageAcquirePDEContent(pdPage, 0);
	numElems = PDEContentGetNumElems(pdeContent);
	for (i = 0; i < numElems; i++) {
		pdeElement = PDEContentGetElem(pdeContent, i); 
		pdeType = PDEObjectGetType((PDEObject) pdeElement);

		// if found, get its Cos object
		if(pdeType == kPDEForm) {
			pdeForm = (PDEForm)pdeElement;
			PDEFormGetCosObj (pdeForm, &cosObj);
			if(!CosObjEqual(cosObj, CosNewNull()))
				break;
		} else if(pdeType == kPDEContainer) {
			ASBool bFound = false;
			pdeContent2 = PDEContainerGetContent ((PDEContainer) pdeElement);
			numSubElems = PDEContentGetNumElems (pdeContent2);
			for (i = 0; i < numSubElems; i++){
				pdeSubElem = PDEContentGetElem(pdeContent2, i);
				pdeType2 = PDEObjectGetType((PDEObject) pdeSubElem);

				// if found, get its Cos object
				if(pdeType2 == kPDEForm) {
					pdeForm = (PDEForm) pdeSubElem;
					PDEFormGetCosObj (pdeForm, &cosObj);
					if(!CosObjEqual(cosObj, CosNewNull())){
						bFound = true;
						break;
					}
				}
			}
			if(bFound)
				break;
		}
	}
HANDLER
	ASInt32 iErrCode = ASGetExceptionErrorCode();
	char errBuf[A3D_MAX_BUFFER];
	memset(errBuf, 0, A3D_MAX_BUFFER);
	ASGetErrorString(iErrCode, errBuf, A3D_MAX_BUFFER);
	if(strlen(errBuf))
		AVAlertNote(errBuf);
	else
		AVAlertNote("Unknown error");
END_HANDLER
	
	if (pdeContent != NULL)
		PDPageReleasePDEContent(pdPage, 0);
	if (pdPage != NULL)
		PDPageRelease(pdPage);

	// return COS object of the PDEForm XObject
	return cosObj;
}

CosObj BuildNEntry(PDDoc pdDoc)
{
	PDDoc newDoc = Create1PageDocWithWatermark();
	if (!newDoc) {
		return CosNewNull();
	}

	CosObj formXObj = ExtractFormXObj(newDoc);

	PDDocClose(newDoc);

	return CosObjCopy(formXObj, PDDocGetCosDoc(pdDoc), true);
}

//////////////////////////////////////////////////////////////////////////
//
//	insert prc file into 3D annot
//
//////////////////////////////////////////////////////////////////////////

A3DVoid addPrcToAnnot(PDDoc pdDoc, PDPage thePage, PDAnnot theAnnot,
						const A3DUTF8Char* prcName, const A3DUTF8Char* jsName)
{
	//	insert PRC data
	ASFile prcFile = NULL;
	ASPathName prcPathName = ASFileSysCreatePathName(NULL, ASAtomFromString("Cstring"), prcName, 0);
	ASFileSysOpenFile(ASGetDefaultFileSys(), prcPathName, ASFILE_READ, &prcFile);
	ASFileSysReleasePath(ASGetDefaultFileSys(), prcPathName);

	// create a new stream and set it under 3DD key in the annotation dictionary
	CosObj cosAnnot = PDAnnotGetCosObj(theAnnot);
	CosDoc cosDoc = CosObjGetDoc(cosAnnot);
	ASStm prcStm = ASFileStmRdOpen(prcFile, 0);
	CosObj stm3D = CosNewStream(cosDoc, true, prcStm, 0, false, CosNewNull(), CosNewNull(), -1);
	ASStmClose(prcStm);
	ASFileClose(prcFile);

	// create a new stream and set it under 3DD key in the annotation dictionary
	CosObj attrObj = CosStreamDict(stm3D);
	CosDictPutKeyString(attrObj, "Type", CosNewNameFromString(cosDoc, false, "3D"));
	CosDictPutKeyString(attrObj, "Subtype", CosNewName(cosDoc, false, ASAtomFromString("PRC")));
	CosDictPutKeyString(cosAnnot, "3DD", stm3D);

	// insert JavaScript
	ASFile jsFile = NULL;
	ASPathName jsPathName = ASFileSysCreatePathName(NULL, ASAtomFromString("Cstring"), jsName, 0);
	ASFileSysOpenFile(ASGetDefaultFileSys(), jsPathName, ASFILE_READ, &jsFile);
	ASFileSysReleasePath(ASGetDefaultFileSys(), jsPathName);

	ASStm jsStm = ASFileStmRdOpen(jsFile, 0);
	CosObj jsCos = CosNewDict(cosDoc, false, 1);
	CosDictPutKeyString(jsCos, "Filter", CosNewNameFromString(cosDoc, false, "FlateDecode"));
	CosObj stm3Djscode = CosNewStream(cosDoc, true, jsStm, 0, true, jsCos, CosNewNull(), -1);
	CosDictPutKeyString(attrObj, "OnInstantiate", stm3Djscode);
	ASStmClose(jsStm);
	ASFileClose(jsFile);

	CosDictPutKeyString(cosAnnot, "P", PDPageGetCosObj(thePage));
	CosDictPutKeyString(cosAnnot, "Contents", CosNewString(cosDoc, false, "3D Model", strlen("3D Model")));

	// set optionnal activation
	CosObj activationDict = CosNewDict(cosDoc, false, 1);
	if(CosObjGetType(activationDict)== CosDict) {
		CosDictPutKeyString(cosAnnot, "3DA", activationDict);
		CosDictPutKeyString(activationDict, "DIS", CosNewName(cosDoc, false, ASAtomFromString("I")));
		CosDictPutKeyString(activationDict, "A", CosNewName(cosDoc, false, ASAtomFromString("PO")));
	}

	CosObj formXObj = BuildNEntry(pdDoc);
	if(CosObjCmp(formXObj, CosNewNull())) {
		// build the required key "AP"
		CosObj apprDict = CosNewDict(cosDoc, false, 1);
		CosDictPutKeyString(apprDict, "N", formXObj);
		CosDictPutKeyString(cosAnnot, "AP", apprDict);
	}

	PDTextAnnotSetOpen(theAnnot, true);
}



//////////////////////////////////////////////////////////////////////////
//
//	Acquire the main Monitor
//
//////////////////////////////////////////////////////////////////////////

A3DVoid acquireMainMonitor(A3DVoid)
{
	memset(&gStatusMonitor,0,sizeof(gStatusMonitor));
	gStatusMonitor.size = sizeof(gStatusMonitor);
	gStatusMonitor.cancelProc = AVAppGetCancelProc(&gStatusMonitor.cancelProcClientData);
	gStatusMonitor.progMon = AVAppGetDocProgressMonitor(&gStatusMonitor.progMonClientData);
	gStatusMonitor.reportProc = AVAppGetReportProc(&gStatusMonitor.reportProcClientData);
}

//////////////////////////////////////////////////////////////////////////
//
//	Add meta data informations to current doc
//
//////////////////////////////////////////////////////////////////////////

A3DVoid addMetaDataToDoc(PDDoc theDoc)
{
	// Producer 
	PDDocSetXAPMetadataProperty(theDoc,
		ASTextFromPDText("http://ns.adobe.com/pdf/1.3/"),
		ASTextFromPDText("pdf"),
		ASTextFromPDText("Producer"),
		ASTextFromPDText(A3D_DLL_COPYRIGHT));

	// Creator 
	PDDocSetXAPMetadataProperty(theDoc,
		ASTextFromPDText("http://ns.adobe.com/xap/1.0/"),
		ASTextFromPDText("xmp"),
		ASTextFromPDText("CreatorTool"),
		ASTextFromPDText(A3D_DLL_NAME));
} 

//////////////////////////////////////////////////////////////////////////
//
//	returns a file selected NULL if none
//
//////////////////////////////////////////////////////////////////////////

ASUTF16Val* getStlFileName(void)
{
	ASUTF16Val* stlName = NULL;
	AVOpenSaveDialogParamsRec dialogParams;
	AVFileFilterRec filterRec, *filterRecP;
	AVFileDescRec descRec = {"stl", 0, 0};
	ASPathName* stlPath = NULL;

	// Configure the dialog box parameters
	memset(&filterRec, NULL, sizeof(AVFileFilterRec));
	filterRec.fileDescs = &descRec;
	filterRec.numFileDescs = 1;
	filterRecP = &filterRec;
	memset(&dialogParams, NULL, sizeof(AVOpenSaveDialogParamsRec));
	dialogParams.size = sizeof(AVOpenSaveDialogParamsRec);
	dialogParams.fileFilters = &filterRecP;
	dialogParams.numFileFilters = 1;

	filterRec.filterDescription = ASTextNew();
	ASTextSetEncoded(filterRec.filterDescription, "stl files", ASScriptToHostEncoding(kASRomanScript));
	dialogParams.windowTitle = ASTextNew();
	ASTextSetEncoded(dialogParams.windowTitle, "Load ASCII STL file", ASScriptToHostEncoding(kASRomanScript));
	dialogParams.flags = kAVOpenSaveAllowForeignFileSystems;
	dialogParams.initialFileSys = ASGetDefaultUnicodeFileSys(); 

	ASFileSys outFileSys = NULL;
	if(AVAppOpenDialog(&dialogParams, &outFileSys, (ASPathName**)&stlPath, NULL, NULL)) {
		ASText asText = ASTextNew();
		ASFileSysDisplayASTextFromPath(outFileSys, *stlPath, asText);
		if(*stlPath)
			ASFileSysReleasePath(outFileSys, *stlPath);
		stlName = ASTextGetUnicodeCopy(asText, kUTF16HostEndian);
		ASTextDestroy(asText);
	}

	// we retain ownership of the ASText objects so we have to destroy them
	ASTextDestroy(filterRec.filterDescription);
	ASTextDestroy(dialogParams.windowTitle);

	return stlName;
}

//////////////////////////////////////////////////////////////////////////
//
//	Build a js file on the fly
//
//////////////////////////////////////////////////////////////////////////

A3DVoid buildJsFile(const A3DUTF8Char* jsName)
{
	FILE* jsFile = fopen(jsName, "w");
	if(jsFile) {
		A3DVector3dData vUp = {0,0.57735, 0.57735, 0.57735};
		A3DDouble roll = 0;

		fprintf(jsFile, "runtime.overrideViewChange = true\n");
		fprintf(jsFile, "activeCamera = scene.cameras.getByIndex( 0 );\n");
		fprintf(jsFile, "bounds = scene.computeBoundingBox();\n");
		fprintf(jsFile, "bounds.center = bounds.max.blend( bounds.min, .5 );\n");
		fprintf(jsFile, "diag = bounds.max.subtract( bounds.min ).length;\n");
		fprintf(jsFile, "dist =(diag * 0.5) / Math.tan(0.6 * activeCamera.fov);\n");
		fprintf(jsFile, "activeCamera.position.set3(bounds.center.x, bounds.center.y - dist, bounds.center.z);\n");
		fprintf(jsFile, "activeCamera.targetPosition.set(bounds.center);\n");
		fprintf(jsFile, "activeCamera.up.set3(%g, %g, %g);\n", vUp.m_dX, vUp.m_dY, vUp.m_dZ);
		fprintf(jsFile, "activeCamera.roll = %g;\n",roll);
		fprintf(jsFile, "scene.lightScheme = scene.LIGHT_MODE_HEADLAMP;\n");
		fprintf(jsFile, "scene.renderMode = scene.RENDER_MODE_SHADED_SOLID_WIRFRAME;\n");
		fprintf(jsFile, "\n");
		fprintf(jsFile, "\n");
		fprintf(jsFile, "\n");
		fclose(jsFile);
	}
}

//////////////////////////////////////////////////////////////////////////
//
//	Main part of the application
//
//////////////////////////////////////////////////////////////////////////

ACCB1 void ACCB2 MyPluginCommand(void *clientData)
{
	const A3DUTF8Char* prcName = "C:\\tmp.prc";
	const A3DUTF8Char* jsName = "C:\\tmp.js";
	const ASUTF16Val* stlName = getStlFileName();
	if(stlName) {
		PDDoc theDoc = PDDocCreate();
		ASFixedRect theBox = {fixedZero, ASInt32ToFixed(612), ASInt32ToFixed(792), fixedZero};
		PDPage thePage = PDDocCreatePage(theDoc, PDBeforeFirstPage, theBox);
		AVDoc avDoc = AVDocOpenFromPDDoc(theDoc, NULL);

		addMetaDataToDoc(theDoc);
		acquireMainMonitor();

		HMODULE hModuleA3DPRCSDK = A3DPRCLoadLibrary();

		if (!hModuleA3DPRCSDK) {
			AVAlertNote("Failed to load A3DLIB.dll!");
			_unlink(prcName);
			_unlink(jsName);
			ASfree((void*)stlName);
			return;
		}

		A3DPRCFunctionPointersInitialize(hModuleA3DPRCSDK);

		ASInt32 iMajorVersion = 0, iMinorVersion = 0; 
		A3DDllGetVersion(&iMajorVersion, &iMinorVersion);
		ASInt32 iErr = A3D_SUCCESS; 
		if(iMajorVersion != A3D_DLL_MAJORVERSION) 
			iErr = A3D_ERROR;
		else if(iMinorVersion < A3D_DLL_MINORVERSION) 
			iErr = A3D_ERROR;

		if(iErr == A3D_SUCCESS) {
			ASInt32 iRet = A3DDllInitialize(A3D_DLL_MAJORVERSION, A3D_DLL_MINORVERSION);
			if(iRet == A3D_SUCCESS) {
				A3DDllSetOutputFunctions(msgCllBck, warCllBck, errCllBck);
				A3DDllSetProgressFunctions(prgStartCllBck, prgSizeCllBck, 
					prgIncCllBck, prgEndCllBck, prgTitleCllBck, &gBreak);
				A3DDllSetAllocationFunctions(allocCllBck, freeCllBck);

				if(!buildTess(stlName, prcName)) {
					PDPageRelease(thePage);
					_unlink(prcName);
					_unlink(jsName);
					ASfree((void*)stlName);
					A3DDllTerminate();
					A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
					return;
				}
				buildJsFile(jsName);

				ASFixedRect theAnnotBox = {ASInt32ToFixed(108), ASInt32ToFixed(612 - 108),
					ASInt32ToFixed(792 - 108), ASInt32ToFixed(108)};
				PDAnnot theAnnot = PDPageAddNewAnnot(thePage, -2, ASAtomFromString("3D"), &theAnnotBox);
				PDAnnotSetFlags(theAnnot, pdAnnotPrint | pdAnnotReadOnly);

				//	load prc stream inside 3d annotation
				addPrcToAnnot(theDoc, thePage, theAnnot, prcName, jsName);

				_unlink(prcName);
				_unlink(jsName);
				ASfree((void*)stlName);
				A3DDllTerminate();
				A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
			} else {
				char sBuffer[A3D_MAX_BUFFER];
				sprintf(sBuffer, "A3DDllInitialize returned %d\n", iRet);
				AVAlertNote(sBuffer); 
				_unlink(prcName);
				_unlink(jsName);
				ASfree((void*)stlName);
				A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
			}
		} else {
			_unlink(prcName);
			_unlink(jsName);
			ASfree((void*)stlName);
			A3DPRCUnloadLibrary(hModuleA3DPRCSDK);
			AVAlertNote("PRC library version on this system is not supported!");
		}

		PDPageRelease(thePage);
	}
	return;
}
