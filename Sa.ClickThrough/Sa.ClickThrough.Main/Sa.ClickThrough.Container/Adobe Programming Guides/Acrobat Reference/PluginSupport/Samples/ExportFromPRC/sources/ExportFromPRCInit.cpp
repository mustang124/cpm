/*********************************************************************

ADOBE SYSTEMS INCORPORATED
Copyright(C) 1998-2008 Adobe Systems Incorporated
All rights reserved.

NOTICE: Adobe permits you to use,modify,and distribute this file
in accordance with the terms of the Adobe license agreement
accompanying it. If you have received this file from a source other
than Adobe,then your use,modification,or distribution of it
requires the prior written permission of Adobe.

*********************************************************************/

// Acrobat Headers.
#ifndef MAC_PLATFORM
#include <PIHeaders.h>
#endif

#include <SmartPDPage.h>

#include <math.h>

#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>

//////////////////////////////////////////////////////////////////////////
//
//	Constants/Declarations
//
//////////////////////////////////////////////////////////////////////////

#define TITLE "Export From PRC"
#define SUBTITLE "ADBE:ExportFromPRC"
#define MAX_BUFFER_LENGTH 1024

// stuff for Menu set up 
static AVMenuItem menuItem = NULL;
const char* MyPluginExtensionName = SUBTITLE;

ACCB1 ASBool ACCB2 PluginMenuItem(char* MyMenuItemTitle, char* MyMenuItemName);

//////////////////////////////////////////////////////////////////////////
//
//	Dumping PRC in a physical file
//
//////////////////////////////////////////////////////////////////////////

A3DVoid saveStream(CosObj stream, const A3DUTF8Char* prcName)
{
	ASTCount iCount = 0;

	FILE* pf = fopen(prcName, "wb");
	if (pf) {
		ASStm stm = CosStreamOpenStm(stream, cosOpenFiltered);
		char sBuffer[MAX_BUFFER_LENGTH];

		do {
			iCount = ASStmRead(sBuffer, sizeof(char), MAX_BUFFER_LENGTH, stm);
			fwrite(sBuffer, sizeof(char), iCount, pf);
		} while (iCount == MAX_BUFFER_LENGTH);

		ASStmClose(stm);
		fclose(pf);
	}
}

//////////////////////////////////////////////////////////////////////////
//
//	Core Handshake Callbacks
//
//////////////////////////////////////////////////////////////////////////

ACCB1 ASBool ACCB2 PluginExportHFTs(void)
{
	return true;
}

ACCB1 ASBool ACCB2 PluginImportReplaceAndRegister(void) 
{
	return true;
}

ACCB1 ASBool ACCB2 PluginLoad(void)
{
	return PluginMenuItem(TITLE, SUBTITLE);
}

ACCB1 ASBool ACCB2 PluginUnload(void)
{
	if(menuItem)
		AVMenuItemRemove(menuItem);

	return true;
}

ASAtom GetExtensionName(void)
{
	return ASAtomFromString(MyPluginExtensionName);
}

//////////////////////////////////////////////////////////////////////////
//
//	Main part of the application
//
//////////////////////////////////////////////////////////////////////////

ASInt32 doTheTest(A3DUTF8Char acFileName[]);

ACCB1 void ACCB2 MyPluginCommand(void *clientData)
{
	AVDoc avDoc = AVAppGetActiveDoc();
	PDDoc pdDoc = AVDocGetPDDoc(avDoc);

	// Acquire the displayed page.
	AVPageView pageView = AVDocGetPageView(avDoc);
	ASInt32 pageNum = AVPageViewGetPageNum(pageView);

	CSmartPDPage pdPage;
	pdPage.AcquirePage(pdDoc, pageNum);

	ASUns32 numAnnots = PDPageGetNumAnnots(pdPage);
	ASBool bFound = false;
	ASInt32 iRet = A3D_ERROR;

	for (ASUns32 ui = 0; ui < numAnnots; ui++) {
		PDAnnot pdAnnot = PDPageGetAnnot(pdPage, ui);
		if (PDAnnotIsValid(pdAnnot) && PDAnnotGetSubtype(pdAnnot) == ASAtomFromString("3D")) {
			CosObj objAnnot = PDAnnotGetCosObj(pdAnnot);
			CosObj obj3DD = CosDictGet(objAnnot, ASAtomFromString("3DD"));
			if (CosObjGetType(obj3DD) == CosStream) {
				CosObj obj = CosDictGet(obj3DD, ASAtomFromString("Subtype"));
				if (CosNameValue(obj) == ASAtomFromString("PRC")) {
					bFound = true;
					A3DUTF8Char acbuffer[A3D_MAX_BUFFER];
					sprintf(acbuffer, "C:%sprc", tmpnam(NULL));
					saveStream(obj3DD, acbuffer);

					AVSysSetWaitCursor(true, 0);
					iRet = doTheTest(acbuffer);
					AVSysSetWaitCursor(false, 0);

					_unlink(acbuffer);

					break;
				}
			}
		}
	}

	if(!bFound) {
		AVAlertNote("The current page of the front document does not have 3d data embedded!");
	} else if (iRet == A3D_SUCCESS) {
		AVAlertNote("The exported data has been written into C:\\ExportFromPRCTrace.xml!");
	} else
		AVAlertNote("Exporting PRC failed.");
		

	return;
}

ACCB1 ASBool ACCB2 PIHandshake(Uns32 handshakeVersion, void *handshakeData)
{
	if(handshakeVersion == HANDSHAKE_V0200) 
	{
		PIHandshakeData_V0200 *hsData = (PIHandshakeData_V0200 *)handshakeData;
		hsData->extensionName = GetExtensionName();
		hsData->exportHFTsCallback = (void*)ASCallbackCreateProto(PIExportHFTsProcType, &PluginExportHFTs);
		hsData->importReplaceAndRegisterCallback = (void*)ASCallbackCreateProto(PIImportReplaceAndRegisterProcType, 
			&PluginImportReplaceAndRegister);
		hsData->initCallback = (void*)ASCallbackCreateProto(PIInitProcType, &PluginLoad);
		hsData->unloadCallback = (void*)ASCallbackCreateProto(PIUnloadProcType, &PluginUnload);
		return true;
	} 
	return false;
}

//////////////////////////////////////////////////////////////////////////
//
//	This code make it is enabled only if there 
//	is open PDF document. 
//
//////////////////////////////////////////////////////////////////////////
ACCB1 ASBool ACCB2 MyPluginIsEnabled(void *clientData)
{
	return(AVAppGetActiveDoc() != NULL);
}

ACCB1 ASBool ACCB2 PluginMenuItem(char* MyMenuItemTitle, char* MyMenuItemName)
{
	AVMenubar menubar = AVAppGetMenubar();
	AVMenu volatile commonMenu = NULL;
	ASBool bCreatedCommonMenu = false;
	AVMenu parentMenu = NULL;
	AVMenuItem endToolsGroup1MenuItem = NULL;
	AVMenuItem commonMenuItem = NULL;

	if(!menubar)
		return false;

	DURING;

	// Create our menuitem
	menuItem = AVMenuItemNew(MyMenuItemTitle, MyMenuItemName, NULL, true, NO_SHORTCUT, 0, NULL, gExtensionID);
	AVExecuteProc execPluginCmd = ASCallbackCreateProto(AVExecuteProc, MyPluginCommand);
	AVMenuItemSetExecuteProc(menuItem, execPluginCmd, NULL);
	AVComputeEnabledProc checkPluginEnable = ASCallbackCreateProto(AVComputeEnabledProc, MyPluginIsEnabled);
	AVMenuItemSetComputeEnabledProc(menuItem, checkPluginEnable, (void *)pdPermEdit);
	parentMenu = AVMenubarAcquireMenuByName(menubar, "Advanced");
	endToolsGroup1MenuItem = AVMenubarAcquireMenuItemByName(menubar, "endToolsGroup1");
	ASInt32 sepIndex = AVMenuGetMenuItemIndex(parentMenu, endToolsGroup1MenuItem);

	// Acquire the common "PRC SDK" sub menu.
	commonMenu = AVMenubarAcquireMenuByName(menubar, "ADBE:Acrobat_SDK"); 

	// if "PRC SDK" sub menu is not existing, create one under Advanced menu.
	if (!commonMenu) {
		commonMenu = AVMenuNew("Acrobat SDK", "ADBE:Acrobat_SDK", gExtensionID);
		commonMenuItem = AVMenuItemNew("Acrobat SDK", "ADBE:Acrobat_SDK", commonMenu, 
			false, NO_SHORTCUT, 0, NULL, gExtensionID);
		bCreatedCommonMenu = true;
		AVMenuAddMenuItem(parentMenu, commonMenuItem, sepIndex);
	}

	// add new menu item to PRC SDK sub menu
	AVMenuAddMenuItem(commonMenu, menuItem, APPEND_MENUITEM);
	AVMenuRelease(parentMenu);
	AVMenuItemRelease(endToolsGroup1MenuItem);
	AVMenuRelease(commonMenu);

	HANDLER;

	if(endToolsGroup1MenuItem) 
		AVMenuItemRelease(endToolsGroup1MenuItem);
	if(parentMenu) 
		AVMenuRelease(parentMenu);
	if(commonMenu) 
		AVMenuRelease(commonMenu);

	return false;

	END_HANDLER;

	return true;
}
