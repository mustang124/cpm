/*********************************************************************

ADOBE SYSTEMS INCORPORATED
Copyright(C) 1998-2008 Adobe Systems Incorporated
All rights reserved.

NOTICE: Adobe permits you to use,modify,and distribute this file
in accordance with the terms of the Adobe license agreement
accompanying it. If you have received this file from a source other
than Adobe,then your use,modification,or distribution of it
requires the prior written permission of Adobe.

*********************************************************************/
#include <stdio.h>
#include <windows.h>

#include <tinyxml.h>

#define A3D_API(returntype,name,params)\
typedef returntype (*PF##name) params;\
static PF##name name = NULL;

#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKMisc.h>
#include <A3DSDKGeometryCrv.h>
#include <A3DSDKGeometrySrf.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKTopology.h>

#undef __A3DPRCSDK_H__
#undef __A3DPRCTYPES_H__
#undef __A3DPRCBASE_H__
#undef __A3DPRCERRORCODES_H__
#undef __A3DPRCGEOMETRY_H__
#undef __A3DPRCMISC_H__
#undef __A3DPRCGEOMETRYCRV_H__
#undef __A3DPRCGEOMETRYSRF_H__
#undef __A3DPRCTESSELLATION_H__
#undef __A3DPRCGRAPHICS_H__
#undef __A3DPRCSTRUCTURE_H__
#undef __A3DPRCREPITEMS_H__
#undef __A3DPRCROOTENTITIES_H__
#undef __A3DPRCTOPOLOGY_H__

#undef A3D_API

void A3DPRCFunctionPointersInitialize(HMODULE hModule)
{
#define A3D_API(returntype,name,params) name = (PF##name)GetProcAddress(hModule,#name);
#include <A3DSDK.h>
#include <A3DSDKTypes.h>
#include <A3DSDKBase.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKMisc.h>
#include <A3DSDKGeometryCrv.h>
#include <A3DSDKGeometrySrf.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKStructure.h>
#include <A3DSDKRepItems.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKTopology.h>
#undef A3D_API
}

HMODULE A3DPRCLoadLibrary()
{
	HMODULE hModuleA3DPRCSDK;

	wchar_t acFilePath[MAX_PATH];
	GetModuleFileNameW(NULL, acFilePath, MAX_PATH);
	wchar_t* backslash = wcsrchr(acFilePath, L'\\');

	if (backslash)
		acFilePath[backslash - acFilePath] = 0;

	wcscat(acFilePath, L"\\A3DLIB.dll");

#ifdef UNICODE 
	hModuleA3DPRCSDK = LoadLibraryExW(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#else
	hModuleA3DPRCSDK = LoadLibraryExA(acFilePath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#endif
	if (hModuleA3DPRCSDK)
		return hModuleA3DPRCSDK;

	return NULL;
}

void A3DPRCUnloadLibrary(HMODULE hModule)
{
	FreeLibrary(hModule);
}

// Read geometry CascadedAttributes
ASInt32 CreateAndPushCascadedAttributes(const A3DRootBaseWithGraphics* pBase,
										const A3DMiscCascadedAttributes* pParentAttr,
										A3DMiscCascadedAttributes** ppAttr,
										A3DMiscCascadedAttributesData* psAttrData,
										TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DMiscCascadedAttributesGet-Geometry");

	// CascadedAttributes is a facitlity to get values for attributes when
	// reading a model file. There is nothing equivalent for writing phase.

	// This API is in charge of handling a stack of attributes, regardless
	// the inheritance flags. That's why you have to pass as argument parent attribute
	// everywhere.
	ASInt32 iRet = A3DMiscCascadedAttributesCreate(ppAttr);
	// This API is dedicated to the geometry itself.
	iRet = A3DMiscCascadedAttributesPush(*ppAttr, pBase, pParentAttr);

	A3D_INITIALIZE_DATA((*psAttrData));
	iRet = A3DMiscCascadedAttributesGet(*ppAttr, psAttrData);

	child->SetAttribute("bShow", psAttrData->m_bShow);
	child->SetAttribute("bRemoved", psAttrData->m_bRemoved);

	parent->LinkEndChild(child);

	return iRet;
}


// Read surface CascadedAttributes
ASInt32 CreateAndPushCascadedAttributesFace(const A3DRiRepresentationItem* pRepItem,
											const A3DTessBase* pTessBase,
											const A3DTessFaceData* psTessFaceData,
											ASUns32 uiFaceIndex,
											const A3DMiscCascadedAttributes* pParentAttr,
											A3DMiscCascadedAttributes** ppAttr,
											A3DMiscCascadedAttributesData* psAttrData,
											TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DMiscCascadedAttributesGet-Tess");
	ASInt32 iRet = A3DMiscCascadedAttributesCreate(ppAttr);
	// This API is dedicated to tessellation
	iRet = A3DMiscCascadedAttributesPushTessFace(*ppAttr, pRepItem, pTessBase, psTessFaceData, uiFaceIndex, pParentAttr);

	A3D_INITIALIZE_DATA((*psAttrData));
	iRet = A3DMiscCascadedAttributesGet(*ppAttr, psAttrData);

	child->SetAttribute("uiFaceIndex", uiFaceIndex);
	child->SetAttribute("bShow", psAttrData->m_bShow);
	child->SetAttribute("bRemoved", psAttrData->m_bRemoved);

	parent->LinkEndChild(child);

	return iRet;
}


A3DVoid parseRI(A3DRiRepresentationItem* pRI, A3DMiscCascadedAttributes* pParentAttr, TiXmlElement *parent);

// Read entity's name
A3DVoid parseSource(const A3DEntity* p, TiXmlElement *parent)
{
	if(!p)
		return;

	A3DRootBaseData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRootBaseGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		if (sData.m_pcName && strlen(sData.m_pcName) > 0)
			parent->SetAttribute("Name", sData.m_pcName);

		A3DRootBaseGet(NULL, &sData);
	}
}

A3DDouble stdContextScale = 1.0;

A3DVoid parseFace(ASUns32 ui, A3DTopoFace* p, TiXmlElement *parent)
{	
	TiXmlElement *child = new TiXmlElement("A3DTopoFaceGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DTopoFaceData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoFaceGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		A3DTopoFaceGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseShell(A3DTopoShell* p, TiXmlElement *parent)
{	
	TiXmlElement *child = new TiXmlElement("A3DTopoShellGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DTopoShellData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoShellGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		for(ASUns32 ui = 0; ui < sData.m_uiFaceSize; ui++)
 			parseFace(ui, sData.m_ppFaces[ui], child);

		A3DTopoShellGet(NULL, &sData);
	}
	else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseConnex(A3DTopoConnex* p, TiXmlElement *parent)
{	
	TiXmlElement *child = new TiXmlElement("A3DTopoConnexGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DTopoConnexData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoConnexGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		for(ASUns32 ui = 0; ui < sData.m_uiShellSize; ui++)
			parseShell(sData.m_ppShells[ui], child);

		A3DTopoConnexGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseContext(A3DTopoContext* p, TiXmlElement *parent)
{	
	TiXmlElement *child = new TiXmlElement("A3DTopoContextGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DTopoContextData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoContextGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		child->SetDoubleAttribute("scale", sData.m_dScale);
		stdContextScale = sData.m_dScale;
		A3DTopoContextGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseBody(A3DTopoBody* p, TiXmlElement *parent)
{	
	TiXmlElement *child = new TiXmlElement("A3DTopoBodyGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DTopoBodyData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoBodyGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		parseContext(sData.m_pContext, child);
		A3DTopoBodyGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}


A3DVoid parseBrepData(A3DTopoBrepData* p, TiXmlElement *parent)
{	
	TiXmlElement *child = new TiXmlElement("A3DTopoBrepDataGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DTopoBrepDataData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTopoBrepDataGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		for(ASUns32 ui = 0; ui < sData.m_uiConnexSize; ui++)
			parseConnex(sData.m_ppConnexes[ui], child);

		A3DTopoBrepDataGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parse3DWireTess(A3DTess3D* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DTess3DWireGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	A3DTess3DWireData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTess3DWireGet(p, &sData);
	if (iRet == A3D_SUCCESS ) {
		child->SetAttribute("SizesWiresSize", sData.m_uiSizesWiresSize);
		child->SetAttribute("RGBAVerticesSize", sData.m_uiRGBAVerticesSize);
		A3DTess3DWireGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parse3DTess(A3DTess3D* p, A3DRiRepresentationItem* pRepItem, A3DMiscCascadedAttributes* pParentAttr,
					TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DTess3DGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	A3DTess3DData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTess3DGet(p, &sData);
	if (iRet == A3D_SUCCESS ) {
		child->SetAttribute("HasFaces", (ASUns32)(sData.m_bHasFaces));
		child->SetAttribute("NormalSize", sData.m_uiNormalSize);
		child->SetAttribute("WireIndexSize", sData.m_uiWireIndexSize);
		child->SetAttribute("TriangulatedIndexSize", sData.m_uiTriangulatedIndexSize);
		child->SetAttribute("TextureCoordSize", sData.m_uiTextureCoordSize);
		child->SetDoubleAttribute("CreaseAngle", sData.m_dCreaseAngle);

		ASUns32 uiNumberOfFaces	= sData.m_uiFaceTessSize;

		for (ASUns32 ui = 0; ui < uiNumberOfFaces; ui++) {
			A3DTessFaceData& sTessFaceData = sData.m_psFaceTessData[ui];

			A3DMiscCascadedAttributes* pAttr;
			A3DMiscCascadedAttributesData sAttrData;
			// Read CascadedAttributes for one of the faces in the surface
			CreateAndPushCascadedAttributesFace(pRepItem, p, &sTessFaceData, 
				ui, pParentAttr, &pAttr, &sAttrData, child);

			A3DMiscCascadedAttributesDelete(pAttr);
			A3DMiscCascadedAttributesGet(NULL, &sAttrData);
		}
		A3DTess3DGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseTess(A3DTessBase* p, A3DRiRepresentationItem* pRepItem, 
				  A3DMiscCascadedAttributes* pParentAttr, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DTessBaseGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	A3DTessBaseData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DTessBaseGet(p, &sData);
	if (iRet == A3D_SUCCESS){
		child->SetAttribute("CoordSize", (ASInt32)sData.m_uiCoordSize);
		child->SetAttribute("Calculated", (ASInt32)sData.m_bIsCalculated);

		A3DEEntityType eType;
		ASInt32 iErr = A3DEntityGetType(p, &eType);
		if (iErr == A3D_SUCCESS) {
			switch(eType) {
				case kA3DTypeTess3D:
					parse3DTess(p, pRepItem, pParentAttr, child);
					break;
				case kA3DTypeTess3DWire:
					parse3DWireTess(p, child);
					break;
				}
		}

		A3DTessBaseGet(NULL, &sData);
	}
	else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiBrepModel(A3DRiBrepModel* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiBrepModelGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DRiBrepModelData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRiBrepModelGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		parseBody(sData.m_pBrepData, child);
		parseBrepData(sData.m_pBrepData, child);
		A3DRiBrepModelGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiPolyBrepModel(A3DRiPolyBrepModel* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiPolyBrepModelGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DRiPolyBrepModelData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRiPolyBrepModelGet(p, &sData);
	if (iRet == A3D_SUCCESS ) {
		child->SetAttribute("Closed", (ASInt32)sData.m_bIsClosed);
		A3DRiPolyBrepModelGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiCSys(A3DRiCoordinateSystem* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiCSysGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}
	parseSource(p, child);

	A3DRiCoordinateSystemData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRiCoordinateSystemGet(p, &sData);
	if (iRet == A3D_SUCCESS ) {
		A3DRiCoordinateSystemGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiCurve(A3DRiCurve* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiCurveGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DRiCurveData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRiCurveGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		A3DRiCurveGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiPlane(A3DRiPlane* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiPlaneGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DRiPlaneData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRiPlaneGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		A3DRiPlaneGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiDirection(A3DRiDirection* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiDirectionGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DRiDirectionData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRiDirectionGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		A3DRiDirectionGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiPointSet(A3DRiPointSet* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiPointSetGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DRiPointSetData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DRiPointSetGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		A3DRiPointSetGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parseRiSet(A3DRiSet* p, A3DMiscCascadedAttributes* pParentAttr, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DRiSetGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DMiscCascadedAttributes* pAttr;
	A3DMiscCascadedAttributesData sAttrData;

	ASInt32 iRet = CreateAndPushCascadedAttributes(p, pParentAttr, &pAttr, &sAttrData, child);

	A3DRiSetData sData;
	A3D_INITIALIZE_DATA(sData);

	iRet = A3DRiSetGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		for(ASUns32 ui = 0; ui<sData.m_uiRepItemsSize; ui++)
			parseRI(sData.m_ppRepItems[ui], pAttr, child);

		A3DRiSetGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	A3DMiscCascadedAttributesDelete(pAttr);
	A3DMiscCascadedAttributesGet(NULL, &sAttrData);

	parent->LinkEndChild(child);
}

// Parse Representation Item
A3DVoid parseRI(A3DRiRepresentationItem* p, A3DMiscCascadedAttributes* pParentAttr, TiXmlElement *parent)
{
	A3DMiscCascadedAttributes* pAttr;
	A3DMiscCascadedAttributesData sAttrData;

	ASInt32 iRet = CreateAndPushCascadedAttributes(p, pParentAttr, &pAttr, &sAttrData, parent);

	A3DEEntityType eType;

	iRet = A3DEntityGetType(p, &eType);
	switch(eType) {
	case kA3DTypeRiSet:
		parseRiSet(p, pAttr, parent);
		break;
	case kA3DTypeRiPointSet:
		parseRiPointSet(p, parent);
		break;
	case kA3DTypeRiDirection:
		parseRiDirection(p, parent);
		break;
	case kA3DTypeRiCurve:
		parseRiCurve(p, parent);
		break;
	case kA3DTypeRiCoordinateSystem:
		parseRiCSys(p, parent);
		break;
	case kA3DTypeRiPlane:
		parseRiPlane(p, parent);
		break;
	case kA3DTypeRiBrepModel:
		parseRiBrepModel(p, parent);
		break;
	case kA3DTypeRiPolyBrepModel:
		parseRiPolyBrepModel(p, parent);
		break;
	default:
		parent->SetAttribute("Type", "Unknown");
	}

	TiXmlElement *child = new TiXmlElement("A3DRiSetGet");

	A3DRiRepresentationItemData sData;
	A3D_INITIALIZE_DATA(sData);

	iRet = A3DRiRepresentationItemGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		parseTess(sData.m_pTessBase, p, pAttr, child);
		parseRiCSys(sData.m_pCoordinateSystem, child);
		A3DRiRepresentationItemGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	A3DMiscCascadedAttributesDelete(pAttr);
	A3DMiscCascadedAttributesGet(NULL, &sAttrData);

	parent->LinkEndChild(child);
}

A3DVoid parsePart(A3DAsmPartDefinition* p, A3DMiscCascadedAttributes* pParentAttr, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DAsmPartDefinitionGet");

	parseSource(p, child);

	A3DMiscCascadedAttributes* pAttr;
	A3DMiscCascadedAttributesData sAttrData;

	ASInt32 iRet = CreateAndPushCascadedAttributes(p, pParentAttr, &pAttr, &sAttrData, child);

	A3DAsmPartDefinitionData sData;
	A3D_INITIALIZE_DATA(sData);

	iRet = A3DAsmPartDefinitionGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		for (ASUns32 ui = 0; ui < sData.m_uiRepItemsSize; ui++)
			parseRI(sData.m_ppRepItems[ui], pAttr, child);

		A3DAsmPartDefinitionGet(NULL, &sData);
	} else
		child->SetAttribute("error", iRet);

	A3DMiscCascadedAttributesDelete(pAttr);
	A3DMiscCascadedAttributesGet(NULL, &sAttrData);

	parent->LinkEndChild(child);
}

A3DVoid parsePOccurrence(A3DAsmProductOccurrence* p, 
						 A3DMiscCascadedAttributes* pParentAttr,
						 TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DAsmProductOccurrenceGet");

	parseSource(p, child);

	A3DMiscCascadedAttributes* pAttr;
	A3DMiscCascadedAttributesData sAttrData;

	ASInt32 iRet = CreateAndPushCascadedAttributes(p, pParentAttr, &pAttr, &sAttrData, child);

	A3DAsmProductOccurrenceData sData;
	A3D_INITIALIZE_DATA(sData);

	iRet = A3DAsmProductOccurrenceGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		if (sData.m_pPrototype) {
			child->SetAttribute("type", "Prototype");
			parsePOccurrence(sData.m_pPrototype, pAttr, child);
		}

		if (sData.m_pExternalData) {
			child->SetAttribute("type", "ExternalData");
			parsePOccurrence(sData.m_pExternalData, pAttr, child);
		}

		for (ASUns32 ui = 0; ui < sData.m_uiPOccurrencesSize; ui++)
			parsePOccurrence(sData.m_ppPOccurrences[ui], pAttr, child);

		if (sData.m_pPart)
			parsePart(sData.m_pPart, pAttr, child);

		iRet = A3DAsmProductOccurrenceGet(NULL, &sData);
	}
	else
		child->SetAttribute("Error", iRet);

	A3DMiscCascadedAttributesDelete(pAttr);
	A3DMiscCascadedAttributesGet(NULL, &sAttrData);

	parent->LinkEndChild(child);
}

A3DVoid parseModelFile(A3DAsmModelFile* p, TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DAsmModelFileGet");
	if (!p) {
		parent->LinkEndChild(child);
		return;
	}

	parseSource(p, child);

	A3DAsmModelFileData sData;
	A3D_INITIALIZE_DATA(sData);

	ASInt32 iRet = A3DAsmModelFileGet(p, &sData);
	if (iRet == A3D_SUCCESS) {
		child->SetAttribute("m_bUnitFromCAD", sData.m_bUnitFromCAD);
		child->SetDoubleAttribute("m_dUnit", sData.m_dUnit);

		A3DMiscCascadedAttributes* pAttr;
		A3DMiscCascadedAttributesCreate(&pAttr);

		for (ASUns32 ui = 0; ui < sData.m_uiPOccurrencesSize; ui++)
			parsePOccurrence(sData.m_ppPOccurrences[ui], pAttr, child);

		iRet = A3DAsmModelFileGet(NULL, &sData);
		iRet = A3DMiscCascadedAttributesDelete(pAttr);
	} else
		child->SetAttribute("Error", iRet);

	parent->LinkEndChild(child);
}

A3DVoid parsePrcFile(A3DUTF8Char acFileName[], TiXmlElement *parent)
{
	TiXmlElement *child = new TiXmlElement("A3DAsmModelFileLoadFromFile");

	A3DAsmModelFile* p = NULL;

	// load model file
	ASInt32 iRet = A3DAsmModelFileLoadFromFile(acFileName, NULL, &p);
	if (iRet == A3D_SUCCESS) {
		parseModelFile(p, child);
		iRet = A3DAsmModelFileDelete(p);
	} else
		child->SetAttribute("Error", iRet);

	parent->LinkEndChild(child);
}

// entry point for the parsing
ASInt32 doTheTest(A3DUTF8Char acFileName[])
{	
	HMODULE hModuleA3DPRCSDK = A3DPRCLoadLibrary();

	if (!hModuleA3DPRCSDK) 
		return A3D_ERROR;

	A3DPRCFunctionPointersInitialize(hModuleA3DPRCSDK);

	TiXmlDocument *doc = new TiXmlDocument();
	doc->SetCondenseWhiteSpace(false);

	TiXmlComment* comment = new TiXmlComment();
	comment->SetValue("XML dump from PDF file");
	doc->LinkEndChild(comment);

	ASInt32 iMajorVersion = 0, iMinorVersion = 0; 
	A3DDllGetVersion(&iMajorVersion, &iMinorVersion);

	ASInt32 iRet = A3D_SUCCESS; 
	if(iMajorVersion != A3D_DLL_MAJORVERSION) 
		iRet = A3D_ERROR;
	else if(iMinorVersion < A3D_DLL_MINORVERSION) 
		iRet = A3D_ERROR;

	if(iRet == A3D_SUCCESS) {
		TiXmlElement *child = new TiXmlElement("A3DDllInitialize");

		child->SetAttribute("MajorVersion", iMajorVersion);
		child->SetAttribute("MinorVersion", iMinorVersion);
		child->SetAttribute("PrcFile", acFileName);

		ASInt32 iRet = A3DDllInitialize(A3D_DLL_MAJORVERSION, A3D_DLL_MINORVERSION);
		if (iRet == A3D_SUCCESS) {
			parsePrcFile(acFileName, child);
			A3DDllTerminate();
		} else
			child->SetAttribute("Error", iRet);
		
		doc->LinkEndChild(child);
	} 

	A3DPRCUnloadLibrary(hModuleA3DPRCSDK);

	doc->SaveFile("C:\\ExportFromPRCTrace.xml");
	delete doc;
	return iRet;
}
