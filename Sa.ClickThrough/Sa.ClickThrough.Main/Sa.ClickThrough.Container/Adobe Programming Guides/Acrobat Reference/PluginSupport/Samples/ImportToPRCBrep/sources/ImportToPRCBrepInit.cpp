/*********************************************************************

ADOBE SYSTEMS INCORPORATED
Copyright(C)1998-2008 Adobe Systems Incorporated
All rights reserved.

NOTICE: Adobe permits you to use,modify,and distribute this file
in accordance with the terms of the Adobe license agreement
accompanying it. If you have received this file from a source other
than Adobe,then your use,modification,or distribution of it
requires the prior written permission of Adobe.

*********************************************************************/

// Acrobat Headers.
#ifndef MAC_PLATFORM
#include "PIHeaders.h"
#endif

//////////////////////////////////////////////////////////////////////////
//
//	Constants/Declarations
//
//////////////////////////////////////////////////////////////////////////

#define TITLE "Import To PRC - Brep"
#define SUBTITLE "ADBE:ImportToPRCBrep"

// stuff for Menu set up 
static AVMenuItem menuItem = NULL;
const char* MyPluginExtensionName = SUBTITLE;

ACCB1 ASBool ACCB2 PluginMenuItem(char* MyMenuItemTitle, char* MyMenuItemName);

extern ACCB1 void ACCB2 MyPluginCommand(void *clientData);

//////////////////////////////////////////////////////////////////////////
//
//	Core Handshake Callbacks
//
//////////////////////////////////////////////////////////////////////////
ACCB1 ASBool ACCB2 PluginExportHFTs(void)
{
	return true;
}

ACCB1 ASBool ACCB2 PluginImportReplaceAndRegister(void)
{
	return true;
}

ACCB1 ASBool ACCB2 PluginLoad(void)
{
	return PluginMenuItem(TITLE, SUBTITLE);
}

ACCB1 ASBool ACCB2 PluginUnload(void)
{
	if(menuItem)
		AVMenuItemRemove(menuItem);

	return true;
}

ASAtom GetExtensionName(void)
{
	return ASAtomFromString(MyPluginExtensionName);
}

ACCB1 ASBool ACCB2 PIHandshake(Uns32 handshakeVersion,
										 void *handshakeData)
{
	if(handshakeVersion == HANDSHAKE_V0200)
	{
		PIHandshakeData_V0200 *hsData = (PIHandshakeData_V0200 *)handshakeData;
		hsData->extensionName = GetExtensionName();
		hsData->exportHFTsCallback = (void*)ASCallbackCreateProto(PIExportHFTsProcType, &PluginExportHFTs);
		hsData->importReplaceAndRegisterCallback = (void*)ASCallbackCreateProto(PIImportReplaceAndRegisterProcType, 
			&PluginImportReplaceAndRegister);
		hsData->initCallback = (void*)ASCallbackCreateProto(PIInitProcType, &PluginLoad);
		hsData->unloadCallback = (void*)ASCallbackCreateProto(PIUnloadProcType, &PluginUnload);
		return true;
	} 
	return false;
}

ACCB1 ASBool ACCB2 MyPluginIsEnabled(void *clientData)
{
	return true;
}

ACCB1 ASBool ACCB2 PluginMenuItem(char* MyMenuItemTitle, char* MyMenuItemName)
{
	AVMenubar menubar = AVAppGetMenubar();
	AVMenu volatile commonMenu = NULL;
	ASBool bCreatedCommonMenu = false;
	AVMenu parentMenu = NULL;
	AVMenuItem endToolsGroup1MenuItem = NULL;
	AVMenuItem commonMenuItem = NULL;

	if(!menubar)
		return false;

	DURING;

	// Create our menuitem
	menuItem = AVMenuItemNew(MyMenuItemTitle, MyMenuItemName, NULL, true, NO_SHORTCUT, 0, NULL, gExtensionID);
	AVExecuteProc execPluginCmd = ASCallbackCreateProto(AVExecuteProc, MyPluginCommand);
	AVMenuItemSetExecuteProc(menuItem, execPluginCmd, NULL);
	AVComputeEnabledProc checkPluginEnable = ASCallbackCreateProto(AVComputeEnabledProc, MyPluginIsEnabled);
	AVMenuItemSetComputeEnabledProc(menuItem, checkPluginEnable, (void *)pdPermEdit);
	parentMenu = AVMenubarAcquireMenuByName(menubar, "Advanced");
	endToolsGroup1MenuItem = AVMenubarAcquireMenuItemByName(menubar, "endToolsGroup1");
	ASInt32 sepIndex = AVMenuGetMenuItemIndex(parentMenu, endToolsGroup1MenuItem);

	// Acquire the common"PRC SDK"sub menu.
	commonMenu=AVMenubarAcquireMenuByName(menubar, "ADBE:Acrobat_SDK"); 

	// if"PRC SDK"sub menu is not existing,create one under Advanced menu.
	if(!commonMenu)
	{
		commonMenu = AVMenuNew("Acrobat SDK", "ADBE:Acrobat_SDK", gExtensionID);
		commonMenuItem = AVMenuItemNew("Acrobat SDK", "ADBE:Acrobat_SDK", commonMenu, false, NO_SHORTCUT, 0, NULL, gExtensionID);
		bCreatedCommonMenu = true;
		AVMenuAddMenuItem(parentMenu, commonMenuItem, sepIndex);
	}

	// add new menu item to PRC SDK sub menu
	AVMenuAddMenuItem(commonMenu, menuItem, APPEND_MENUITEM);
	AVMenuRelease(parentMenu);
	AVMenuItemRelease(endToolsGroup1MenuItem);
	AVMenuRelease(commonMenu);

	HANDLER;

	if(endToolsGroup1MenuItem)
		AVMenuItemRelease(endToolsGroup1MenuItem);
	if(parentMenu)
		AVMenuRelease(parentMenu);
	if(commonMenu)
		AVMenuRelease(commonMenu);

	return false;

	END_HANDLER;

	return true;
}
