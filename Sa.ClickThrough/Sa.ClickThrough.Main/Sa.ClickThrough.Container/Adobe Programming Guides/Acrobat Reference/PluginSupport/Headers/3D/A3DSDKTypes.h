/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2008 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Header file for type definitions
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved.
*/

#ifndef __A3DPRCTYPES_H__
#define __A3DPRCTYPES_H__


/*!
\defgroup a3d_types_def Type Definitions

This section describes the basic types defined by the \COMPONENT_A3D_API. 
These type definitions concern fundamental types such as int, float, and strings and 
types specific to the \COMPONENT_A3D_API entities such as geometries, parts, and representation items. 
@{
*/
/*!
@} <!-- end of a3d_types_def -->
*/

/*!
\defgroup a3d_fundamental_types Fundamental Type Definitions
\ingroup a3d_types_def
\version 2.0

These fundamental types are defined in common Acrobat library: 
<b>ASInt8, ASInt16, ASInt32, ASUns8, ASUns16, ASUns32, ASBool</b>.
Here are the fundamental types specific to the \COMPONENT_A3D_API.
@{
*/
typedef void*				A3DPtr;			/*!< Void pointer. */
typedef void				A3DVoid;		/*!< Void. */
typedef double				A3DDouble;		/*!< Double standard type. */
typedef char				A3DUTF8Char;	/*!< UTF-8 char standard type. */
/*!
@} <!-- end of a3d_fundamental_types -->
*/

#if defined(WIN32) && !defined(WINCE)
#	include <windows.h>
#	include <tchar.h>
#endif
#if defined _UNICODE || defined UNICODE
typedef wchar_t A3DUniChar;	/*!< Unicode \c char standard type. */
#else
typedef char A3DUniChar;	/*!< A \c char standard type. */
#endif

/*!
\defgroup a3d_entitytypes_def Entity Type Definitions
\ingroup a3d_types_def
This section describes the entity types used for the PRC entities defined in the \REF_PRC_SPEC.
Every type corresponds to a unique entry in the \ref A3DEEntityType enumerator.
The types are grouped into the following modules in this reference (the \COMPONENT_A3D_API_REF). 
\li Root types
\li Structure
\li Geometry
\li Topology
\li Tessellation
\li Markup
\li Texture
\li Graphics
\li Miscellaneous data

\sa a3d_types_enum
*/

/*!
\defgroup a3d_types_enum Entity Constants
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
#define kA3DTypeRoot							0								/*!< This type does not correspond to any entity */
#define kA3DTypeCrv							( kA3DTypeRoot +  10 )	/*!< Types for PRC geometrical curves. \sa a3d_crv */
#define kA3DTypeSurf							( kA3DTypeRoot +  75 )	/*!< Types for PRC geometrical surfaces. \sa a3d_srf */
#define kA3DTypeTopo							( kA3DTypeRoot + 140 )	/*!< Types for PRC topology. \sa a3d_topology_module */
#define kA3DTypeTess							( kA3DTypeRoot + 170 )	/*!< Types for PRC tessellation. \sa a3d_tessellation_module */
#define kA3DTypeMisc							( kA3DTypeRoot + 200 )	/*!< Types for PRC miscellaneous data */
#define kA3DTypeRi							( kA3DTypeRoot + 230 )	/*!< Types for PRC representation items. \sa a3d_geometry_module */
#define kA3DTypeAsm							( kA3DTypeRoot + 300 )	/*!< Types for PRC assembly. \sa a3d_structure_module */
#define kA3DTypeMkp							( kA3DTypeRoot + 500 )	/*!< Types for PRC markup */
#define kA3DTypeGraph						( kA3DTypeRoot + 700 )	/*!< Types for PRC graphics */
#define kA3DTypeMath							( kA3DTypeRoot + 900 )	/*!< Types for PRC mathematical operators */

/*!
\brief Enumerations for Entity Types
This enumumeration defines a unique type for each PRC entity defined in the \REF_PRC_SPEC.
\version 2.0

*/
typedef enum 
{
	kA3DTypeUnknown								= -1,

	kA3DTypeRootBase								= kA3DTypeRoot + 1,	/*!< Abstract root type for any PRC entity. */
	kA3DTypeRootBaseWithGraphics				= kA3DTypeRoot + 2,	/*!< Abstract root type for any PRC entity that can bear graphics. */

	kA3DTypeTopoContext							= kA3DTypeTopo + 1,	/*!< Self-containing set of topological entities. */
	kA3DTypeTopoItem								= kA3DTypeTopo + 2,	/*!< Abstract root type for any topological entity (body or single item). */
	kA3DTypeTopoMultipleVertex					= kA3DTypeTopo + 3,	/*!< Vertex whose position is the average of all edges' extremity positions to whom it belongs. \sa a3d_multiple_vertex */
	kA3DTypeTopoUniqueVertex					= kA3DTypeTopo + 4,	/*!< Vertex with one set of coordinates (absolute position). \sa a3d_unique_vertex */
	kA3DTypeTopoWireEdge							= kA3DTypeTopo + 5,	/*!< Edge belonging to a wire body. */
	kA3DTypeTopoEdge								= kA3DTypeTopo + 6,	/*!< Edge belonging to a Brepdata body. \sa a3d_edge */
	kA3DTypeTopoCoEdge							= kA3DTypeTopo + 7,	/*!< Usage of an edge in a loop. \sa a3d_coedge */
	kA3DTypeTopoLoop								= kA3DTypeTopo + 8,	/*!< Array of co-edges that delimit a face. \sa a3d_loop */
	kA3DTypeTopoFace								= kA3DTypeTopo + 9,	/*!< Topological face delimiting a shell. \sa a3d_face */
	kA3DTypeTopoShell								= kA3DTypeTopo + 10,	/*!< Topological shell (open or closed). \sa a3d_shell */
	kA3DTypeTopoConnex							= kA3DTypeTopo + 11,	/*!< Topological region. \sa a3d_connex */
	kA3DTypeTopoBody								= kA3DTypeTopo + 12,	/*!< Abstract root type for any topological body. */
	kA3DTypeTopoSingleWireBody					= kA3DTypeTopo + 13,	/*!< Single wire body. */
	kA3DTypeTopoBrepData							= kA3DTypeTopo + 14,	/*!< Main entry to non-wire topology. \sa a3d_topology_module */
	kA3DTypeTopoWireBody							= kA3DTypeTopo + 17,	/*!< Main entry to wire topology. */

	kA3DTypeTessBase								= kA3DTypeTess + 1,	/*!< Abstract root type for any tessellated entity. */
	kA3DTypeTess3D									= kA3DTypeTess + 2,	/*!< Tessellated faceted data; regular form. \sa a3d_tessellation_module */
	kA3DTypeTessFace								= kA3DTypeTess + 4,	/*!< Tessellated face. \sa a3d_tessface */
	kA3DTypeTess3DWire							= kA3DTypeTess + 5,	/*!< Tessellated wireframe. \sa a3d_tess3dwire */
	kA3DTypeTessMarkup							= kA3DTypeTess + 6,	/*!< Tessellated markup. \sa a3d_tessmarkup */

	kA3DTypeMiscAttribute						= kA3DTypeMisc + 1,	/*!< Entity attribute. \sa a3d_attribute. */
	kA3DTypeMiscCartesianTransformation		= kA3DTypeMisc + 2,	/*!< Cartesian transformation. */
	kA3DTypeMiscEntityReference				= kA3DTypeMisc + 3,	/*!< Entity reference. Used to overwrite properties of referenced entities. */
	kA3DTypeMiscMarkupLinkedItem				= kA3DTypeMisc + 4,	/*!< Link between a markup and an entity. */

	kA3DTypeMiscReferenceOnTopology   		= kA3DTypeMisc + 6,	/*!< Reference pointing on a topological entity. */
	kA3DTypeMiscGeneralTransformation		= kA3DTypeMisc + 7,	/*!< General transformation. This type allows for storage of any kind of transformation. */

	kA3DTypeRiRepresentationItem				= kA3DTypeRi + 1,		/*!< Basic abstract type for representation items. */
	kA3DTypeRiBrepModel							= kA3DTypeRi + 2,		/*!< Basic type for surfaces and solids. */
	kA3DTypeRiCurve								= kA3DTypeRi + 3,		/*!< Basic type for curves. */
	kA3DTypeRiDirection							= kA3DTypeRi + 4,		/*!< Optional point + vector. */
	kA3DTypeRiPlane								= kA3DTypeRi + 5,		/*!< Construction plane. (Do not confuse this type with the plane surface geometry type \c kA3DTypeSrfPlane.) */
	kA3DTypeRiPointSet							= kA3DTypeRi + 6,		/*!< Set of points. \sa A3DRiPointSetData. */
	kA3DTypeRiPolyBrepModel						= kA3DTypeRi + 7,		/*!< Basic type to polyhedral surfaces and solids. */
	kA3DTypeRiPolyWire							= kA3DTypeRi + 8,		/*!< Polyedric wireframe entity. */
	kA3DTypeRiSet								= kA3DTypeRi + 9,	/*!< Logical grouping of arbitrary number of representation items. \sa a3d_riset.*/
	kA3DTypeRiCoordinateSystem					= kA3DTypeRi + 10,		/*!< Coordinate system. */

	kA3DTypeAsmModelFile						= kA3DTypeAsm + 1,	/*!< Basic entry type for PRC. \sa a3d_modelfile */

	kA3DTypeAsmProductOccurrence				= kA3DTypeAsm + 10,	/*!< Basic construction for assemblies. \sa a3d_productoccurrence */
	kA3DTypeAsmPartDefinition					= kA3DTypeAsm + 11,	/*!< Basic construction for parts. \sa a3d_partdefinition */
	kA3DTypeAsmFilter							= kA3DTypeAsm + 20,	/*!< Entities filtering. */

	kA3DTypeMkpView								= kA3DTypeMkp + 1,	/*!< Grouping of markup by views. \sa a3d_annots_view */
	kA3DTypeMkpMarkup								= kA3DTypeMkp + 2,	/*!< Basic type for simple markups. \sa a3d_markup */
	kA3DTypeMkpLeader								= kA3DTypeMkp + 3,	/*!< Basic type for markup leader. \sa a3d_markupleader */
	kA3DTypeMkpAnnotationItem					= kA3DTypeMkp + 4,	/*!< Usage of a markup in a logical group. \sa a3d_annotationitem */
	kA3DTypeMkpAnnotationSet					= kA3DTypeMkp + 5,	/*!< Group of annotations. \sa a3d_annotationset */
	kA3DTypeMkpAnnotationReference			= kA3DTypeMkp + 6,	/*!< Logical group of annotations. \sa a3d_annotationreference */

	kA3DTypeGraphStyle							= kA3DTypeGraph + 1,		/*!< This type gathers all information to configure the display style of every entity: color / material / texture, line pattern, fill pattern... */
	kA3DTypeGraphMaterial						= kA3DTypeGraph + 2,		/*!< Basic material definition with colors and alpha. */
	kA3DTypeGraphPicture							= kA3DTypeGraph + 3,		/*!< Picture. */
	kA3DTypeGraphTextureApplication			= kA3DTypeGraph + 11,	/*!< Defines a complete texture pipe to apply*/
	kA3DTypeGraphTextureDefinition			= kA3DTypeGraph + 12,	/*!< Defines a single texture set of parameters to be used in a texture application. */
	kA3DTypeGraphTextureTransformation		= kA3DTypeGraph + 13,	/*!< Texture transformation. */
	kA3DTypeGraphLinePattern					= kA3DTypeGraph + 21,	/*!< One dimensional display style. \sa A3DGraphLinePatternData. */
	kA3DTypeGraphFillPattern					= kA3DTypeGraph + 22,	/*!< Abstract class for two-dimensional display style. One of the four *Pattern types. */
	kA3DTypeGraphDottingPattern				= kA3DTypeGraph + 23,	/*!< Two-dimensional filling with dots. */
	kA3DTypeGraphHatchingPattern				= kA3DTypeGraph + 24,	/*!< Two-dimensional filling with hatches. */
	kA3DTypeGraphSolidPattern					= kA3DTypeGraph + 25,	/*!< Two-dimensional filling with particular style (color, material, texture). */
	kA3DTypeGraphVPicturePattern				= kA3DTypeGraph + 26,	/*!< Two-dimensional filling with vectorized picture. \sa A3DPictureData. */
	kA3DTypeGraphAmbientLight					= kA3DTypeGraph + 31,	/*!< Scene ambient illumination. */
	kA3DTypeGraphPointLight						= kA3DTypeGraph + 32,	/*!< Scene point illumination. */
	kA3DTypeGraphDirectionalLight				= kA3DTypeGraph + 33,	/*!< Scene directional illumination. */
	kA3DTypeGraphSpotLight						= kA3DTypeGraph + 34,	/*!< Scene spot illumination. */
	kA3DTypeGraphSceneDisplayParameters  	= kA3DTypeGraph + 41,	/*!< Parameters for scene visualization. */
	kA3DTypeGraphCamera							= kA3DTypeGraph + 42,	/*!< */

	kA3DTypeCrvBase								= kA3DTypeCrv + 1,	/*!< Abstract type for all geometric curves. */
	kA3DTypeCrvBlend02Boundary					= kA3DTypeCrv + 2,	/*!< Blend02Boundary curve. \sa a3d_crvboundary */
	kA3DTypeCrvNurbs								= kA3DTypeCrv + 3,	/*!< NURBS curve. \sa a3d_crvnurbs */
	kA3DTypeCrvCircle								= kA3DTypeCrv + 4,	/*!< Circle. \sa a3d_crvcircle */
	kA3DTypeCrvComposite							= kA3DTypeCrv + 5,	/*!< Array of oriented curves. \sa a3d_crvcomposite */
	kA3DTypeCrvOnSurf								= kA3DTypeCrv + 6,	/*!< Curve defined by a UV curve on a surface. \sa a3d_crvonsurf */
	kA3DTypeCrvEllipse							= kA3DTypeCrv + 7,	/*!< Ellipse. \sa a3d_crvellipse */
	kA3DTypeCrvEquation							= kA3DTypeCrv + 8,	/*!< Curve described by specific equation elements. \sa a3d_crvequation */
	kA3DTypeCrvHelix								= kA3DTypeCrv + 9,	/*!< Helix. \sa a3d_crvhelix */
	kA3DTypeCrvHyperbola							= kA3DTypeCrv + 10,	/*!< Hyperbola. \sa a3d_crvhyperbola */
	kA3DTypeCrvIntersection						= kA3DTypeCrv + 11,	/*!< Intersection between 2 surfaces. \sa a3d_crvintersection */
	kA3DTypeCrvLine								= kA3DTypeCrv + 12,	/*!< Line curve. \sa a3d_crvline */
	kA3DTypeCrvOffset								= kA3DTypeCrv + 13,	/*!< Offset curve. \sa a3d_crvoffset */
	kA3DTypeCrvParabola							= kA3DTypeCrv + 14,	/*!< Parabola. \sa a3d_crvparabola */
	kA3DTypeCrvPolyLine							= kA3DTypeCrv + 15,	/*!< Polyedric curve. \sa a3d_crvpolyline */
	kA3DTypeCrvTransform							= kA3DTypeCrv + 16,	/*!< Transformed curve. \sa a3d_crvtransform */

	kA3DTypeSurfBase								= kA3DTypeSurf + 1,	/*!< Abstract type for all geometric surfaces. */
	kA3DTypeSurfBlend01							= kA3DTypeSurf + 2,	/*!< Blend type 1. */
	kA3DTypeSurfBlend02							= kA3DTypeSurf + 3,	/*!< Blend type 2. */
	kA3DTypeSurfBlend03							= kA3DTypeSurf + 4,	/*!< Blend type 3. */
	kA3DTypeSurfNurbs							= kA3DTypeSurf + 5,	/*!< NURBS surface. */
	kA3DTypeSurfCone							= kA3DTypeSurf + 6,	/*!< Conical surface. */
	kA3DTypeSurfCylinder						= kA3DTypeSurf + 7,	/*!< Cylindrical surface. */
	kA3DTypeSurfCylindrical						= kA3DTypeSurf + 8,	/*!< Surface that is defined in cylindrical space. */
	kA3DTypeSurfOffset							= kA3DTypeSurf + 9,	/*!< Offset surface. */
	kA3DTypeSurfPipe							= kA3DTypeSurf + 10,	/*!< Pipe. */
	kA3DTypeSurfPlane							= kA3DTypeSurf + 11,	/*!< Plane. */
	kA3DTypeSurfRuled							= kA3DTypeSurf + 12,	/*!< Ruled surface. */
	kA3DTypeSurfSphere							= kA3DTypeSurf + 13,	/*!< Sphere. */
	kA3DTypeSurfRevolution						= kA3DTypeSurf + 14,	/*!< Surface of revolution. */
	kA3DTypeSurfExtrusion						= kA3DTypeSurf + 15,	/*!< Surface of extrusion. */
	kA3DTypeSurfFromCurves						= kA3DTypeSurf + 16,	/*!< Surface build from curves. */
	kA3DTypeSurfTorus							= kA3DTypeSurf + 17,	/*!< Torus. */
	kA3DTypeSurfTransform						= kA3DTypeSurf + 18,	/*!< Transformed surface. */
	kA3DTypeSurfBlend04							= kA3DTypeSurf + 19,	/*!< Defined for future use. */

	kA3DTypeMathFct1D							= kA3DTypeMath + 1,			/*!< Basic type for first-degree equation object. */
	kA3DTypeMathFct1DPolynom					= kA3DTypeMathFct1D +  1,	/*!< Polynomial equation. */
	kA3DTypeMathFct1DTrigonometric				= kA3DTypeMathFct1D +  2,	/*!< Cosine-based equation. */
	kA3DTypeMathFct1DFraction					= kA3DTypeMathFct1D +  3,	/*!< Fraction between 2 first-degree equation objects. */
	kA3DTypeMathFct1DArctanCos					= kA3DTypeMathFct1D +  4,	/*!< Specific equation. */
	kA3DTypeMathFct1DCombination				= kA3DTypeMathFct1D +  5,	/*!< Combination of first-degree equation object. */
	kA3DTypeMathFct3D							= kA3DTypeMath + 10,		/*!< Basic type for third-degree equation object. */
	kA3DTypeMathFct3DLinear						= kA3DTypeMathFct3D +  1,	/*!< Linear transformation (with a matrix). */
	kA3DTypeMathFct3DNonLinear					= kA3DTypeMathFct3D +  2,	/*!< Specific transformation. */
} A3DEEntityType;
/*!
@} <!-- end of a3d_types_enum -->
*/
/*!
@} <!-- end of a3d_entitytypes_def -->
*/

/*!
\defgroup a3d_root_types Root Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DEntity;				/*!< Root type for any entity. */
typedef void A3DRootBase;					/*!< Root type for any entity with attributes. */
typedef void A3DRootBaseWithGraphics;	/*!< Root type for any entity with attributes and graphics. */
/*!
@} <!-- end of a3d_root_types -->
*/

/*!
\defgroup a3d_structure_types Structure Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DAsmModelFile;				/*!< Model file entity. The top level for accessing entities. \sa a3d_modelfile. */
typedef void A3DAsmProductOccurrence;		/*!< Product occurrence. Refer to \ref a3d_structure_module for more details. \sa a3d_productoccurrence. */
typedef void A3DAsmPartDefinition;			/*!< Part definition. Refer to \ref a3d_structure_module for more details. \sa a3d_partdefinition. */
typedef void A3DAsmFilter;					/*!< Filter. Refer to \ref a3d_filter for more details. */
/*!
@} <!-- end of a3d_structure_types -->
*/
/*!
\defgroup a3d_repitems_types Representation Item Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DRiRepresentationItem;	/*!< Representation item entity. Refer to \ref a3d_structure_module for more details. \sa a3d_repitem. */
typedef void A3DRiBrepModel;				/*!< Solid/surface model as representation item. \sa a3d_ribrepmodel, a3d_topology_module. */
typedef void A3DRiCurve;						/*!< Curve as representation item. \sa a3d_ricurve. */
typedef void A3DRiDirection;				/*!< Direction representation item. \sa a3d_ridirection. */
typedef void A3DRiPlane;					/*!< Plane as representation item. \sa a3d_riplane. */
typedef void A3DRiPointSet;					/*!< Set of 3D points. \sa a3d_ripointset. */
typedef void A3DRiPolyBrepModel;		/*!< Faceted model as representation item. \sa a3d_ripolybrepmodel, a3d_topology_module. */
typedef void A3DRiPolyWire;				/*!< Wireframe model as representation item. \sa a3d_ripolywire, a3d_topology_module. */
typedef void A3DRiSet;						/*!< Set of representation item entities. \sa a3d_riset. */
typedef void A3DRiCoordinateSystem;		/*!< Coordinate system representation item. \sa a3d_ricoordinatesystem. */
/*!
@} <!-- end of a3d_repitems_types -->
*/

/*!
\defgroup a3d_geometry_types Geometry Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

In general, each curve and surface has a parametric function that describes its minimal natural definition.

<ul>
	<li>Curves have a parametric function that takes a single argument,
		\c Parameter), which is a real number. The result of the \c PointOnCurve function  
		is a 3D Cartesian point represented by three real numbers.

		<CODE> PointOnCurve = F(Parameter) </CODE>

		For example, the following parametric function provides the minimal natural definition of a circle
		on the Z=0 plane, centered in (0,0,0), and having the radius: \c R. 
	
		<CODE> X = Radius * cos(Parameter), Y = R * sin(Parameter), Z = 0  </CODE>
	</li>

	<li>Surfaces have a parametric function that takes two arguments,
		\c Parameter_U and \c Parameter_V, which are real numbers. 
		The result of the function (\c PointOnSurface) is a 3D Cartesian point represented by three real numbers.

		<CODE> PointOnSurface = F(Parameter_U, Parameter_V) </CODE>

		For example, the following parametric function provides the minimal natural definition of the Z=0 plane:
		
		<CODE> X = Parameter_U, Y = Parameter_V, Z = 0 </CODE>
	</li>
</ul>

To represent other circles and planes, the following items are sequentially applied to each curve and surface
(except for NURBS curves and NURBS surfaces):
<ol>
	<li>Trim </li>
	<li>Parametric transformation (an affine function) </li>
	<li>Cartesian transformation</li>
</ol>

For example, the following equation shows the application of these modifications:

<CODE>PointOnCurve = CartesianTransformation( F(CoefA * Parameter + CoefB) ) </CODE>

Where the equation components have the following characeristics:
\li \c Parameter value is bounded by two real numbers as follows: <CODE>IntervalMin <= Parameter <= IntervalMax</CODE>.
\li \c CoefA and \c CoefB are real numbers that define the affine function (the parametric transformation).
\li \c CartesianTransformation is a spacial transformation.
@{
*/
/*!
@} <!-- end of a3d_geometry_types -->
*/


/*!
\defgroup a3d_curves_type Curve Type Declarations
\ingroup a3d_geometry_types
\version 2.0

@{
*/
typedef void A3DCrvBase;			/*!< Abstract type for a geometric curve. \sa a3d_crv. */
typedef void A3DCrvBlend02Boundary;	/*!< Blend02Boundary curve. \sa a3d_crvboundary. */
typedef void A3DCrvNurbs;			/*!< NURBS curve. \sa a3d_crvnurbs. */
typedef void A3DCrvCircle;			/*!< Circular curve. \sa a3d_crvcircle. */
typedef void A3DCrvComposite;		/*!< Composite curve. \sa a3d_crvcomposite. */
typedef void A3DCrvOnSurf;			/*!< On-surface curve. \sa a3d_crvonsurf. */
typedef void A3DCrvEllipse;			/*!< Elliptic curve. \sa a3d_crvellipse. */
typedef void A3DCrvEquation;		/*!< Equation curve. \sa a3d_crvequation. */
typedef void A3DCrvHelix;			/*!< Helical curve. \sa a3d_crvhelix. */
typedef void A3DCrvHyperbola;		/*!< Hyperbolic curve. \sa a3d_crvhyperbola. */
typedef void A3DCrvIntersection;	/*!< Intersection curve. \sa a3d_crvintersection. */
typedef void A3DCrvLine;			/*!< Linear curve. \sa a3d_crvline. */
typedef void A3DCrvOffset;			/*!< Offset curve. \sa a3d_crvoffset. */
typedef void A3DCrvParabola;		/*!< Parabolic curve. \sa a3d_crvparabola. */
typedef void A3DCrvPolyLine;		/*!< Polygonal curve. \sa a3d_crvpolyline. */
typedef void A3DCrvTransform;		/*!< Transform curve. \sa a3d_crvtransform. */
/*!
@} <!-- end of a3d_curves_type -->
*/

/*!
\defgroup a3d_surfaces_type Surface Type Declarations
\ingroup a3d_geometry_types
\version 2.0

@{
*/
typedef void A3DSurfBase;			/*!< Abstract type for a Surface. \sa a3d_srf */
typedef void A3DSurfBlend01;		/*!< Surface for blend type 1. \sa a3d_srfblend01 */
typedef void A3DSurfBlend02;		/*!< Surface for blend type 2. \sa a3d_srfblend02 */
typedef void A3DSurfBlend03;		/*!< Surface for blend type 3. \sa a3d_srfblend03 */
typedef void A3DSurfNurbs;			/*!< NURBS surface. \sa a3d_srfnurbs */
typedef void A3DSurfCone;			/*!< Conical surface. \sa a3d_srfcone */
typedef void A3DSurfCylinder;		/*!< Cylindrical surface. \sa a3d_srfcylinder */
typedef void A3DSurfCylindrical;	/*!< Cylindrical surface. \sa a3d_srfcylindrical */
typedef void A3DSurfOffset;			/*!< Offset surface. \sa a3d_srfoffset */
typedef void A3DSurfPipe;			/*!< Pipe surface. \sa a3d_srfpipe */
typedef void A3DSurfPlane;			/*!< Planar surface. \sa a3d_srfplane */
typedef void A3DSurfRuled;			/*!< Ruled surface. \sa a3d_srfruled */
typedef void A3DSurfSphere;			/*!< Spherical surface. \sa a3d_srfsphere */
typedef void A3DSurfRevolution;		/*!< Surface of revolution. \sa a3d_srfrevolution */
typedef void A3DSurfExtrusion;		/*!< Surface of extrusion. \sa a3d_srfextrusion */
typedef void A3DSurfFromCurves;		/*!< Surface from curves. \sa a3d_srffromcurves */
typedef void A3DSurfTorus;			/*!< Torical surface. \sa a3d_srftorus */
typedef void A3DSurfTransform;		/*!< Transform surface. \sa a3d_srftransform */
typedef void A3DSurfBlend04;		/*!< Surface for blend type 4. \sa a3d_srfblend04 */
/*!
@} <!-- end of a3d_surfaces_type -->
*/

/*!
\defgroup a3d_math_types Mathematical Operator Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DMathFct1D;				/*!< Basic type for first-degree equation object. \sa a3d_maths */
typedef void A3DMathFct1DPolynom;		/*!< Type for a polynomial object. \sa  a3d_maths */
typedef void A3DMathFct1DTrigonometric;	/*!< Type for a cosine-based equation object. \sa a3d_maths */
typedef void A3DMathFct1DFraction;		/*!< Type for an object representing linear combination of other equations. \sa a3d_maths */
typedef void A3DMathFct1DArctanCos;		/*!< Type for an arctangent-based equation object. \sa a3d_maths */
typedef void A3DMathFct1DCombination;	/*!< Type for an object representing weighted combinations of other equations. \sa a3d_maths */
typedef void A3DMathFct3D;				/*!< Basic type for third-degree equation object. \sa a3d_maths */
typedef void A3DMathFct3DLinear ;		/*!< Type for an object representing linear equations for 3x1 matrices. \sa a3d_maths */
typedef void A3DMathFct3DNonLinear;		/*!< Type for an object representing trigonometric equations for 3x1 matrices. \sa a3d_maths */
/*!
@} <!-- end of a3d_math_types -->
*/

/*!
\defgroup a3d_topology_types Topology Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DTopoContext;					/*!< Topological context. \sa a3d_context */
typedef void A3DTopoItem;						/*!< Abstract root type for any topological entity (body or single item). */
typedef void A3DTopoVertex;					/*!< Vertex. One of the two following types. */
typedef void A3DTopoMultipleVertex;			/*!< Vertex. \sa a3d_multiple_vertex. */
typedef void A3DTopoUniqueVertex;			/*!< Vertex. \sa a3d_unique_vertex. */
typedef void A3DTopoWireEdge;					/*!< Wire Edge. \sa a3d_wireedge. */
typedef void A3DTopoEdge;						/*!< Edge. \sa a3d_edge. */
typedef void A3DTopoCoEdge;					/*!< Co-Edge. \sa a3d_coedge. */
typedef void A3DTopoLoop;						/*!< Loop. \sa a3d_loop. */
typedef void A3DTopoFace;						/*!< Face. \sa a3d_face. */
typedef void A3DTopoShell;						/*!< Shell. \sa a3d_shell. */
typedef void A3DTopoConnex;					/*!< Connex. \sa a3d_connex. */
typedef void A3DTopoBody;						/*!< Abstract root type for any topological body. */
typedef void A3DTopoSingleWireBody;			/*!< Single wire body. \sa a3d_singlewirebody. */
typedef void A3DTopoBrepData;					/*!< Main entry to solid and surface topology (regular form). \sa a3d_brepdata. */

/*!
@} <!-- end of a3d_topology_types -->
*/

/*!
\defgroup a3d_tessellation_types Tessellation Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DTessBase;			/*!< Generic type for all kind of tessellations. \sa a3d_tessellation_module. */
typedef void A3DTess3D;			/*!< Tessellation type for solids/surfaces. \sa a3d_tess3d. */
typedef void A3DTess3DWire;		/*!< Tessellation type for wire bodies. \sa a3d_tess3dwire. */
typedef void A3DTessMarkup;		/*!< Tessellation type for markups. \sa a3d_tessmarkup. */
/*!
@} <!-- end of a3d_tessellation_types -->
*/

/*!
\defgroup a3d_markup_types Markup Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DMkpView;				/*!< Grouping of markup by views. Refer to \ref a3d_markup_module for more details. \sa a3d_annots_view. */
typedef void A3DMkpMarkup;				/*!< Definition of a markup. May contain tessellation. \sa a3d_markup_module */
typedef void A3DMkpLeader;				/*!< Definition of a markup's leader. May contain tessellation. \sa a3d_markupleader */
typedef void A3DMkpAnnotationEntity;	/*!< Abstract type for a Annotation entity (3 types below). Refer to \ref a3d_markup_module for more details. \sa a3d_annotationentity. */
typedef void A3DMkpAnnotationItem;		/*!< Usage of a markup. An annotation item references a single markup.
											A single markup can be referenced by multiple annotations. \sa a3d_annotationitem */
typedef void A3DMkpAnnotationSet;		/*!< Group of annotations. 
											An annotation set is a group of annotation items or other annotation sets. 
											For instance, an \ref A3DMkpView entity contains a set of markups associated with a plane. 
											In another example, a tolerance can be a set containing a datum, a feature control frame, 
											and a dimension. \sa a3d_annotationset */
typedef void A3DMkpAnnotationReference;	/*!< Logical grouping of annotations for reference. An annotation reference is a set of references on other markups. It is mainly used as a combination of datum with modifiers. \sa a3d_annotationreference */
/*!
@} <!-- end of a3d_markup_types -->
*/

/*!
\defgroup a3d_graphics_types Graphics Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DGraphics;			/*!< Graphics associated to \ref A3DRootBaseWithGraphics. \sa a3d_graphics. */
typedef void A3DGraphSceneDisplayParameters;	/*!< Parameters to display a scene; associated to a product occurrence  or a view. \sa a3d_productoccurrence. */
typedef void A3DGraphCamera;			/*!< Camera. */
typedef void A3DGraphAmbientLight;	/*!< Ambient light. */
typedef void A3DGraphPointLight;		/*!< Point light. */
typedef void A3DGraphSpotLight;		/*!< Spot light. */
typedef void A3DGraphDirectionalLight  ;	/*!< Directional light. */
/*!
@} <!-- end of a3d_graphics_types -->
*/

/*!
\defgroup a3d_texture_types Texture Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DGraphTextureApplication;	/*!< Defines a complete texture pipe to apply.*/
typedef void A3DGraphTextureDefinition;		/*!< Defines a single texture set of parameters to be used in an \ref A3DGraphTextureApplication.*/
typedef void A3DGraphTextureTransformation;/*!< Defines transformation for a texture. */
/*!
@} <!-- end of a3d_texture_types module --> 
*/

/*!
\defgroup a3d_misc_data_types Miscellaneous Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DMiscTransformation;			/*!< Transformation base entity. This entity appears in structure members as a pointer that references 
												either an \ref A3DMiscCartesianTransformation or an \ref A3DMiscGeneralTransformation entity. 
												\sa a3d_misc_trsf. */
typedef void A3DMiscAttribute;				/*!< Attributes of an \ref A3DRootBase entity. \sa a3d_attribute. */
typedef void A3DMiscCartesianTransformation;/*!< Cartesian transformation where individual types of transformations 
												(scaling, rotation, relocation) are selected. \sa a3d_cartesiantransfo3d. */
typedef void A3DMiscEntityReference;		/*!< Entity reference. \sa a3d_entity_reference. */
typedef void A3DMiscMarkupLinkedItem;		/*!< Link between a markup and an entity. This type allows a logical link 
												between a markup and a pointed entity, which can be geometry or another markup. 
												Conversely to an EntityReference, the pointed entity 
												can be a "remote" entity in another product occurence, 
												in case where an assembly markup points to a subpart. 
												It can be associated with a leader of the markup. 
												\sa a3d_markuplinkeditem */
typedef void A3DMiscReferenceOnTopology;	/*!< Entity reference on a topological item. \sa a3d_reference_on_topo. */
typedef void A3DMiscGeneralTransformation;	/*!< Cartesian transformation that uses a 4x4 matrix. \sa a3d_generaltransfo3d. */

typedef void A3DGlobal;				/*!< Global container. Refer to \ref a3d_global_data_module for more details. */

typedef void A3DAsmModelFileReadHelper;				/*!< Reserved for future use. */
typedef void A3DAsmModelFileWriteHelper;				/*!< Reserved for future use. */

/*!
@} <!-- end of a3d_misc_data_types -->
*/

/*!
\defgroup a3d_tools_data_types Tools Type Declarations
\ingroup a3d_entitytypes_def
\version 2.0

@{
*/
typedef void A3DMiscCascadedAttributes; /*!< Inheritable settings for the associated PRC entity 
											such as layer, style, and coordinate system. 
											\sa \ref a3d_misc_cascaded_attributes */
/*!
@} <!-- end of a3d_tools_data_types -->
*/

#endif	/*	__A3DPRCTYPES_H__ */
