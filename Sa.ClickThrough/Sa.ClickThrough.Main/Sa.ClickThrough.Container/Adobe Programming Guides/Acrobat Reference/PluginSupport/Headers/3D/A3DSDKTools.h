/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2009 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Header of <b>A3DSDK</b>. Tool section.
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved.
*/

#ifndef __A3DPRCTOOLS_H__
#define __A3DPRCTOOLS_H__

/*!
\defgroup a3d_tools_module Tools Module
\ingroup a3d_base_module
*/

/*!
\ingroup a3d_tools_module
\brief Entity deletion facility. 

This function is reserved for future use. It is excluded from this current version of the api.

\warning This function must be called very carefully, only on locally created data. When
used in another case, it may corrupt global structure and drastically disturb the library behavior.

\return \ref A3D_NOT_IMPLEMENTED \n
*/
A3D_API (ASInt32, A3DEntityDelete,(A3DEntity* pEntity));

#endif	/*	__A3DPRCTOOLS_H__ */
