/*******************************************************************************
* 
*  ADOBE SYSTEMS INCORPORATED
*  Copyright 2008 Adobe Systems Incorporated
*  All Rights Reserved
*  
*  NOTICE:  Adobe permits you to use, modify, and distribute this file in 
*  accordance with the terms of the Adobe license agreement accompanying it.  
*  If you have received this file from a source other than Adobe, then your use, 
*  modification, or distribution of it requires the prior written permission of 
*  Adobe.
* 
*******************************************************************************/

/*!
\file
\brief	Header file for the structure module
\author	ADOBE SYSTEMS INCORPORATED
\version	2.0
\date		February 2009
\par		(c) Copyright 2009 Adobe Systems Incorporated. All rights reserved.
*/

#ifndef __A3DPRCSTRUCTURE_H__
#define __A3DPRCSTRUCTURE_H__

#ifndef __A3DPRCGRAPHICS_H__
#error  A3DSDKGraphics.h must be included before current file
#endif

/*!
\defgroup a3d_structure_module Structure Module
\ingroup a3d_entitiesdata_module
\brief Creates and accesses structural PRC entities 

This module describes the functions and structures that allow you to create and parse PRC structure entities. 
PRC structure entities have names of the form <code>A3DAsm<i>Entity_name</i></code>, for example \ref A3DAsmProductOccurrence.

The PRC structure entities can be adapted to support a variety of 3D modelling formats, including specific CAD formats, neutral 3D formats such as IGS, and tessellation formats such as VRML. 
\sa \REF_PRC_SPEC
*/

#ifndef __A3D_ASM_MODELFILE__
/*!
\defgroup a3d_modelfile Model File
\ingroup a3d_structure_module
\brief Reads and writes a physical file containing a PRC model file; and creates and accesses model file entities

The \ref A3DAsmModelFile is the root entity of the PRC data. It is the starting point for parsing a PRC file. 
It is also one of the arguments used to create a 3D annotation within a PDF document. 

Entity's type is \ref kA3DTypeAsmModelFile.
\sa \REF_PLUGIN_DEV_GUIDE
*/

/*!
\brief Loads an \ref A3DAsmModelFile from a physical file containing a PRC stream.
\ingroup a3d_modelfile
\par

After initializing the \COMPONENT_A3D_LIBRARY, this is the first function you call to begin reading a PRC file. 
This function loads the \ref A3DAsmModelFile from the PRC file. 
Before calling this function, you must export the contents of a PDF 3D annotation containing PRC content into a temporary PRC file. 

\version 2.0

\param pcFileName References the path to the file containing the PRC stream.
\param pA3DAsmModelFileReadHelper Must be null. File-read helpers are not supported.
\param ppModelFile References a pointer into which Acrobat 3D stores the location 
of the model file. Set this pointer to null before calling \ref A3DAsmModelFileLoadFromFile.

\return \ref A3D_LOAD_CANNOT_OPEN_FILE \n
\return \ref A3D_LOAD_INITIALIZATION_FAILURE \n
\return \ref A3D_LOAD_READING_ERROR \n
\return \ref A3D_LOAD_BUILD_ERROR \n
\return \ref A3D_SUCCESS \n

\sa \REF_PLUGIN_DEV_GUIDE
*/
A3D_API (ASInt32, A3DAsmModelFileLoadFromFile,(
	const A3DUTF8Char* pcFileName,
	A3DAsmModelFileReadHelper** pA3DAsmModelFileReadHelper,
	A3DAsmModelFile** ppModelFile));

/*!
\brief Format used to write the PRC model file to a file
\ingroup a3d_modelfile
\version 2.0
*/
typedef enum
{
	kA3DPRC = 0,	/*!< PRC writing. */ 
	kA3DECMA1,		/*!< ECMA-363, version 1 writing. */ 
	kA3DECMA3		/*!< ECMA-363, version 3 writing. */
} A3DEThreeDFormatType;

/*!
\brief Level of compression used to write the PRC model file to a file
\ingroup a3d_modelfile
\version 2.0
*/
typedef enum
{
	kA3DLooseCompression = 0,	/*!< Compression with tolerance set to 0.001 mm. */
	kA3DMeddiumCompression,		/*!< Compression with tolerance set to 0.01 mm. */
	kA3DHighCompression			/*!< Compression with tolerance set to 0.1 mm. */
} A3DECompressBrepType;

/*!
\brief A structure that specifies parameters used to write the PRC model file to a physical file
\par

This structure specifies parameters used to write the PRC model file to a physical file. 

\ingroup a3d_modelfile
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;								/*!< Reserved; must be initialized by \ref A3D_INITIALIZE_DATA. */
	A3DEThreeDFormatType m_eFormatChoice;			/*!< Output type. */
	ASBool m_bCompressBrep;								/*!< Are Breps to be compressed? Unused if \ref m_eFormatChoice is different from \ref kA3DPRC. */
	ASBool m_bCompressTessellation;					/*!< Are Tessellations to be compressed? Unused if \ref m_eFormatChoice is different from \ref kA3DPRC. */
	A3DECompressBrepType m_eCompressBrepChoice;	/*!< Level of compression. Unused if \ref m_eFormatChoice is different from \ref kA3DPRC. */
	ASBool m_bMeshQuality;								/*!< Are Tessellations to be compressed? Unused if \ref m_eFormatChoice is equal to \ref kA3DPRC. */
	ASUns8 m_ucMeshQualityValue;						/*!< Level of compression [0, 100]. Unused if \ref m_eFormatChoice is equal to \ref kA3DPRC. */
} A3DAsmModelFileWriteParametersData;

/*!
\brief Writes a model file to a physical file.
\ingroup a3d_modelfile
\par

This function writes the \ref A3DAsmModelFile entity to a physical file. 
You can then add this physical file 
to a 3D annotation in a PDF file, as described in \REF_PLUGIN_DEV_GUIDE.

\version 2.0

\param pModelFile References the \ref A3DAsmModelFile to be written out.
\param pData Reserved for future use.
\param pcFileName References the path of the file into which the \COMPONENT_A3D_LIBRARY stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_FILE_FORMAT \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DAsmModelFileWriteToFile,(
	const A3DAsmModelFile* pModelFile,
	const A3DAsmModelFileWriteParametersData* pData,
	const A3DUTF8Char* pcFileName));

/*!
\brief ModelFile structure. 

\COMPONENT_A3D_API version 2.1 adds support for the \ref m_bUnitFromCAD field. 
In earlier versions of the SDK, this field was described as \e Reserved.

\ingroup a3d_modelfile

\version 2.0

\warning \ref m_dUnit in multiple of millimeters. You must provide a value for this member to create a model file.

*/
typedef struct
{
	ASUns16 m_usStructSize;						/*!< Reserved; must be initialized by \ref A3D_INITIALIZE_DATA. */
	ASBool	m_bUnitFromCAD;					/*!< Indicates whether the following unit is read from the native CAD file. */
	A3DDouble m_dUnit;							/*!< Unit. */
	ASUns32 m_uiPOccurrencesSize;				/*!< Number of product occurrences in next array. */
	A3DAsmProductOccurrence** m_ppPOccurrences;	/*!< Array of \ref A3DAsmProductOccurrence. */
} A3DAsmModelFileData;

/*!
\brief Populates the \ref A3DAsmModelFileData structure
\ingroup a3d_modelfile

\version 2.0

\param pModelFile A reference to the model file, which is created by invoking \ref A3DAsmModelFileLoadFromFile
\param pData A reference to the A3DAsmModelFileData structure in which the \COMPONENT_A3D_LIBRARY stores the model file data

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n

\par Sample code
\include ModelFile.cpp
*/
A3D_API (ASInt32, A3DAsmModelFileGet,(
											  const A3DAsmModelFile* pModelFile,
											  A3DAsmModelFileData* pData));

/*!
\brief Creates an \ref A3DAsmModelFile from \ref A3DAsmModelFileData structure
\ingroup a3d_modelfile
\version 2.0
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_MODELFILE_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DAsmModelFileCreate,(
												  const A3DAsmModelFileData* pData,
												  A3DAsmModelFile** ppModelFile));

/*!
\brief Deletes the entire \ref A3DAsmModelFile entity from memory
\ingroup a3d_modelfile
\par

Every entity under this model file is also deleted. This function is responsible for
freeing the whole memory reserved for ModelFile. After this call, access to 
the model file is no longer possible, and pointer must be set to NULL. 

\version 2.0

\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DAsmModelFileDelete,(
												  A3DAsmModelFile* pModelFile));

#endif	/*	__A3D_ASM_MODELFILE__ */

#ifndef __A3D_ASM_POCCURRENCE__
/*!
\defgroup a3d_productoccurrence Product Occurrence
\ingroup a3d_structure_module
\brief Creates and accesses product occurrence PRC entities

A product occurrence denotes an assembly tree. If the assembly tree contains a single part, the product occurrence
points directly to a part definition. In the case of a more complex assembly tree, 
a product occurrence is a hierarchy of child product occurrences. 
And a product occurrence can reference product occurrences that are designated as ProductPrototypes or
ExternalData. Such ProductOccurrences denote subassembly structures, which in turn can point to PartDefinitions.


Entity's type is \ref kA3DTypeAsmProductOccurrence. 

*/

/*!
\brief Load status of the PRC model file
\ingroup a3d_productoccurrence
\version 2.1
*/
typedef enum
{
	kA3DProductLoadStatus_Unknown = 0,	/*!< Unknown status. */ 
	kA3DProductLoadStatus_Error,			/*!< Loading error. For example, there is a missing file. */
	kA3DProductLoadStatus_NotLoaded,		/*!< Not loaded. */
	kA3DProductLoadStatus_NotLoadable,	/*!< Not loadable. For example, limitations exist that prevent the product from loading. */
	kA3DProductLoadStatus_Loaded			/*!< The product was successfully loaded. */
} A3DEProductLoadStatus;

/*!
\defgroup a3d_product_flag Bit field flag definitions for product occurrences
@ingroup a3d_productoccurrence
@{

These flags represent characteristics of product occurrences.

A product occurrence can be:<BR>
\li A container. In this case, it acts as a repository of son occurrences that do not necessarily have 
	relationships between them. This is useful for situations where a single CAD file can correspond to a whole database of parts and assemblies.
\li A configuration. This is a specific arrangement of a product with respect to its whole hierarchy.<BR>
\li A view. A view refers to another product occurrence (its prototype) to denote a particular setting 
	of visibilities and position within the same hierarchy.<BR>

If none of these flags is specified, a product occurrence is regular. If the product occurrence has no 
father, it is similar to a configuration. 

\attention <b>A product occurrence with no father cannot have a product prototype or external data property.</b>

For containers, configurations, and views, a product occurrence can be the default, which means that it 
is loaded by default in the originating CAD system.

\version 2.1

*/
#define A3D_PRODUCT_FLAG_DEFAULT		0x0001	/*!< The product occurrence is the default container, configuration, or view.*/
#define A3D_PRODUCT_FLAG_INTERNAL	0x0002	/*!< The product occurrence is internal. This flag is used only when the product occurrence has no father. */
#define A3D_PRODUCT_FLAG_CONTAINER	0x0004	/*!< The product occurrence is a container. */
#define A3D_PRODUCT_FLAG_CONFIG		0x0008	/*!< The product occurrence is a configuration. */
#define A3D_PRODUCT_FLAG_VIEW			0x0010	/*!< The product occurrence is a view. */

/*!
@}
*/

/*!
\brief A structure specifying product occurrence data
\ingroup a3d_productoccurrence
\par

A product occurrence can have the following data:
\li PartDefinition : Pointer to the corresponding part definition. Can be NULL. \n
\li ProductPrototype : Pointer to the corresponding product occurrence prototype. Can be NULL. \n
\li ExternalData : Pointer to the corresponding external product occurrence. Can be NULL. \n
\li Sons : Array of pointers to the son product occurrences\n
\li Location : Relative placement of the product occurrence in the father local coordinate system. Can be NULL. \n

A product occurrence can use product prototype links and external data links 
to reference part or all of other product occurrences.
Those referenced product occurrences can be outside of the assembly.
The PRC format defines these entities for referencing other product occurrences:
/li Product prototype entities reference other CAD content that uses the same conventions as the assembly (homogeneous). 
/li External data entities reference CAD content that uses a different convention (heterogeneous).

A product prototype entity or external data entity references the product occurrence of a subassembly or part that  
is used in the upper assembly. A product prototype acts as a template for a given product occurrence by 
referencing information inside the sub-part or assembly (geometry, ...). 

When applications interpret an assembly that uses subassemblies, 
they resolve links in the product prototype and external data entities and import the content described by those links. 
That is, those entities are resolved in the same way that software macros are resolved.
<!-- Delete? When building assemblies from subassemblies, 
the product prototype acts as a macro by duplicating  
in the product occurrence the data that they reference.
 When the subassembly is heterogeneous (different CAD systems), the link is done through the ExternalData 
rather than the prototype. -->

In addition to the data described in the \ref A3DAsmProductOccurrenceData structure, 
an \ref A3DAsmProductOccurrence entity can have attributes specified through the \ref A3DRootBase and 
\ref A3DMiscCascadedAttributes entities. 

For version 2.1, new fields were added to the end of this structure. These new fields are identified with the tag "version 2.1."

\version 2.0

\par Sample code

The following pseudocode demonstrates how to get the external data of a product occurrence.

\code
POccurrence* function GetExternalData(POccurrence* pocc)
{
	If pocc->pExternalData is null and pocc->pPrototype is not null
		Return GetExternalData(pocc->pPrototype)
	Else
		Return pocc->pExternalData
}
\endcode

The following pseudocode demonstrates how to get the part definition of a product occurrence.

\code
POccurrence* GetPart(POccurrence* pocc)
{
	If pocc->pPart is not null
		Return pocc->pPart

	POccurrence* po = pocc->pPrototype
	While po is not null
		If po->pPart is not null
			Return po->pPart
		Else
			po = po->pPrototype

	If pocc->uiPOccurrencesSize = 0 and GetExternalData(pocc) is not null
		Return GetExternalData(pocc)->pPart

	Return null
}
\endcode

The following pseudocode demonstrates how to get the location of a product occurrence.

\code
CartesianTransfo* GetLocation(POccurrence* pocc)
{
	If GetExternalData(pocc) is not null
		If GetExternalData(pocc)->pLocation is not null
			Return GetExternalData(pocc)->pLocation
	If pocc->pLocation is null and pocc->pPrototype is not null
		Return GetLocation(pocc->pPrototype)
	Return pocc->pLocation
}
\endcode
*/
typedef struct
{
	ASUns16 m_usStructSize;								/*!< Reserved; must be initialized by \ref A3D_INITIALIZE_DATA. */
	ASUns32 m_uiPOccurrencesSize;						/*!< Number of product occurrences in next array. */
	A3DAsmProductOccurrence** m_ppPOccurrences;			/*!< Array of \ref A3DAsmProductOccurrence references. */
	A3DAsmPartDefinition* m_pPart;						/*!< Pointer to an \ref A3DAsmPartDefinition entity. May be NULL. */
	A3DAsmProductOccurrence* m_pPrototype;				/*!< Pointer to a product prototype. May be NULL. */
	A3DAsmProductOccurrence* m_pExternalData;			/*!< Pointer to a external data product. May be NULL. */
	ASUns8 m_ucBehaviour;								/*!< Reserved for future use. */
	A3DMiscTransformation* m_pLocation;				/*!< Pointer to an \ref A3DMiscCartesianTransformation 
														or \ref A3DMiscGeneralTransformation entity. May be NULL. */
	ASUns32 m_uiEntityReferenceSize;					/*!< Size of next array. */
	A3DMiscEntityReference** m_ppEntityReferences;		/*!< Entity references. */
	ASUns32 m_uiAnnotationsSize;						/*!< Size of next array. */
	A3DMkpAnnotationEntity** m_ppAnnotations;			/*!< Annotation entities stored under the current product occurrence. */
	ASUns32 m_uiViewsSize;								/*!< Size of next array. */
	A3DMkpView** m_ppViews;								/*!< Views stored under the current product occurrence. */
	A3DAsmFilter* m_pEntityFilter;						/*!< Array of \c A3DAsmFilter references on entities that are kept by the occurrence. */
	ASUns32 m_uiDisplayFilterSize;						/*!< Size of next array. */
	A3DAsmFilter** m_ppDisplayFilters;					/*!< Array of Filter references that specify the filters 
															to use for display. Several filters can be specified but only one is active. */
	ASUns32 m_uiSceneDisplayParameterSize;				/*!< Size of next array. */
	A3DGraphSceneDisplayParametersData* m_psSceneDisplayParameters;		/*!< Array of SceneDisplayParameters. Reserved for future use. */
	A3DEProductLoadStatus m_eProductLoadStatus;		/*!< Current load status for the ProductOccurrence, 
														where values are defined by the \ref A3DEProductLoadStatus enum. \version 2.1 */
	ASUns32 m_uiProductFlags;							/*!< Refer to \ref a3d_product_flag for explanations \version 2.1 */
	ASBool m_bUnitFromCAD;								/*!< Indicates whether \ref m_dUnit (below) was obtained from the native CAD file. \version 2.1 */
	A3DDouble m_dUnit;									/*!< Unit. \version 2.1 */
} A3DAsmProductOccurrenceData;

/*!
\brief Populates the \ref A3DAsmProductOccurrenceData structure
\ingroup a3d_productoccurrence
\version 2.0

\par Sample code
\include ProductOccurrence.cpp
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DAsmProductOccurrenceGet,(
	const A3DAsmProductOccurrence* pProductOccurrence,
	A3DAsmProductOccurrenceData* pData));

/*!
\brief Creates an \ref A3DAsmProductOccurrence from \ref A3DAsmProductOccurrenceData structure
\ingroup a3d_productoccurrence
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_PRODUCTOCCURENCE_INCONSISTENT_PART_EXTERNAL \n
\return \ref A3D_PRODUCTOCCURENCE_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS.\n
*/
A3D_API (ASInt32, A3DAsmProductOccurrenceCreate,(
	const A3DAsmProductOccurrenceData* pData,
	A3DAsmProductOccurrence** ppProductOccurrence));

/*!
\brief Sets up an \ref A3DAsmProductOccurrence entity as a prototype 
to another \ref A3DAsmProductOccurrence entity
\ingroup a3d_productoccurrence
\version 2.0

This function sets an \ref A3DAsmProductOccurrence entity as a prototype to another \ref A3DAsmProductOccurrence entity,
and duplicates the \ref A3DAsmProductOccurrenceData structure. <!-- verify this is intent -->

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_PRODUCTOCCURENCE_INCONSISTENT_PART_EXTERNAL \n
\return \ref A3D_PRODUCTOCCURENCE_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS.\n
*/
A3D_API (ASInt32, A3DAsmProductOccurrenceSetProductPrototype,(
			A3DAsmProductOccurrence* pProductOccurrence,
			A3DAsmProductOccurrence* pProductPrototype));

#endif	/*	__A3D_ASM_POCCURRENCE__ */

#ifndef __A3D_ASM_PARTDEF__
/*!
\defgroup a3d_partdefinition Part Definition
\ingroup a3d_productoccurrence
\version 2.0

Entity's type is \ref kA3DTypeAsmPartDefinition.
*/

/*!
\brief A structure that specifies the part definition data
\ingroup a3d_partdefinition
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;	/*!< Reserved; must be initialized by \ref A3D_INITIALIZE_DATA. */
	A3DBoundingBoxData m_sBoundingBox;	/*!< Bounding box. */
	ASUns32 m_uiRepItemsSize;	/*!< Size of next array. */
	A3DRiRepresentationItem** m_ppRepItems;	/*!< Representation items stored under current Part definition. */
	ASUns32 m_uiAnnotationsSize;	/*!< Size of next array. */
	A3DMkpAnnotationEntity** m_ppAnnotations;	/*!< Annotation entities stored under current Part definition. */
	ASUns32 m_uiViewsSize;	/*!< Size of next array. */
	A3DMkpView** m_ppViews;	/*!< Views stored under current Part definition. */
} A3DAsmPartDefinitionData;

/*!
\brief Populates an \ref A3DAsmPartDefinitionData structure
\ingroup a3d_partdefinition
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DAsmPartDefinitionGet,(
													 const A3DAsmPartDefinition* pPartDefinition,
													 A3DAsmPartDefinitionData* pData));


/*!
\brief Creates an \ref A3DAsmPartDefinition entity from an \ref A3DAsmPartDefinitionData structure
\ingroup a3d_partdefinition
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_PARTDEFINITION_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS.\n
*/
A3D_API (ASInt32, A3DAsmPartDefinitionCreate,(
	const A3DAsmPartDefinitionData* pData,
	A3DAsmPartDefinition** ppPartDefinition));

#endif	/*	__A3D_ASM_PARTDEF__ */

#ifndef __A3D_ASM_FILTER__
/*!
\defgroup a3d_filter Filter Module
\ingroup a3d_productoccurrence
\brief Creates and accesses filter entities that control the entities and layers included in the 3D image

Entity's type is \ref kA3DTypeAsmFilter.

The filter entity lets you include or exclude product occurrences or representation items based on their layer or entity type. 
Such inclusive or exclusive filtering applies to the primary entity and its descendants. 
For example, you can exclude all wireframe entities.

You can filter entities from these perspectives:
<ul>
	<li>Layer, inclusively. Only the entities belonging to a certain layer are included in the 3D view. 
		Entities in other layers are removed. </li>
	<li>Layer, exclusively. Only the entities not belonging to a certain layer are included in the 3D view. 
		Entities in other layers are removed. </li>
	<li>Entity, inclusively. Only entities of certain types are included in the 3D view. 
		Entities of other types are removed. </li>
	<li>Entity, exclusively. Only entities not of certain types are included in the 3D view. 
		Entities of other types are removed.</li>
</ul>

If the \ref A3DAsmLayerFilterItemData::m_bIsInclusive member is \c TRUE, the layer filter is inclusive; otherwise, it is exclusive. 
Similarly for an entity filter,
if the \ref A3DAsmEntityFilterItemData::m_bIsInclusive member is \c TRUE, the entity filter is inclusive; otherwise, it is exclusive. 
If a layer or entity satisfies an inclusive filter criteria, then it and all of its child entities are included in the 3D view.
If a layer or entity satisfies an exclusive filter criteria, then it and all its child entities are excluded from the 3D view.

For example, if one of the entries in the \ref A3DAsmLayerFilterItemData::m_puiLayerIndexes member 
specifies a layer index of "3" 
and if the \ref A3DAsmLayerFilterItemData::m_bIsInclusive member is \c FALSE, 
then entities with miscellaneous cascaded attributes member \c m_usLayer set to 3 are excluded from the 3D view.

Multiple filters can be specified within a particular \ref a3d_productoccurrence entity. 
The filters specified within one product are propagated to descendent product occurrences.

*/

/*!
\brief A structure that specifies the layers to consider in a layer filter
\ingroup a3d_filter
\version 2.0

This structure is used for layer-filtering where one of the following situations occur, 
depending on the setting of the \ref m_bIsInclusive member:
<ul>
	<li>Only the entities belonging to a certain layer are present. 
		Entities in other layers are removed. </li>
	<li>Only entities not belonging to a certain layer are present. 
		Entities in other layers are retained. </li>
</ul>
*/

typedef struct
{
	ASUns16 m_usStructSize;				/*!< Reserved; must be initialized by \ref A3D_INITIALIZE_DATA. */
	ASBool m_bIsInclusive;				/*!< A value of \c TRUE indicates the entities in the specified layer are retained. 
											A value of \c FALSE indicates the entities in the specified layer are removed.  */
	ASUns32 m_uiSize;					/*!< Size of the next array. */
	ASUns32* m_puiLayerIndexes;			/*!< Layers considered by the filter. The index references a layer in the global data.   */
} A3DAsmLayerFilterItemData;

/*!
\brief A structure that specifies the entities to consider in an entity filter
\ingroup a3d_filter
\version 2.0

The \ref A3DAsmEntityFilterItemData structure is similar to the \ref A3DAsmLayerFilterItemData 
except that filtering is done by entity.
*/

typedef struct
{
	ASUns16 m_usStructSize;					/*!< Reserved; must be initialized by \ref A3D_INITIALIZE_DATA. */
	ASBool m_bIsInclusive;					/*!< A value of \c TRUE indicates the specified entity types are retained. 
												A value of \c FALSE indicates the specified entity types are removed. */
	ASUns32 m_uiSize;						/*!< Size of next array. */
	A3DMiscEntityReference** m_ppEntities;	/*!< Entities referenced in the current filter. */
} A3DAsmEntityFilterItemData;

/*!
\brief A structure that specifies entity and layer-filtering characteristics
\ingroup a3d_filter
\version 2.0
*/
typedef struct
{
	ASUns16 m_usStructSize;							/*!< Reserved; must be initialized by \ref A3D_INITIALIZE_DATA. */
	ASBool m_bIsActive;								/*!< If \c TRUE, this filter corresponds to the active layout when loading the file. */
	A3DAsmLayerFilterItemData m_sLayerFilterItem;	/*!< For filtering by layer. */
	A3DAsmEntityFilterItemData m_sEntityFilterItem;	/*!< For filtering by entity item. */
} A3DAsmFilterData;

/*!
\brief Populates the \ref A3DAsmFilterData structure with data from an \ref A3DAsmFilter entity
\ingroup a3d_filter
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (ASInt32, A3DAsmFilterGet,(
										  const A3DAsmFilter* pFilter,
										  A3DAsmFilterData* pData));


/*!
\brief Creates an \ref A3DAsmFilter entity from an \ref A3DAsmFilterData structure
\ingroup a3d_filter
\version 2.0
<!-- The error conditions for this function aren't implemented, 
	which makes me wonder whether this function is ready for public consumption. 
	On 5/30, send Didier an email asking about this. -->

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_PARTDEFINITION_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS.\n
*/
A3D_API (ASInt32, A3DAsmFilterCreate,(
											  const A3DAsmFilterData* pData,
											  A3DAsmFilter** ppFilter));

#endif	/*	__A3D_ASM_FILTER__ */

#endif	/*	__A3DPRCSTRUCTURE_H__ */
