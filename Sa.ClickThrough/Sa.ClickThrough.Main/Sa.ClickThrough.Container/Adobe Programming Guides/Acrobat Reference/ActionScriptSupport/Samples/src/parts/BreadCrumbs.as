package parts
{
	import controller.Controller;
	
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.collections.CursorBookmark;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.LinkButton;
	import mx.core.Container;
	import mx.core.IUIComponent;
	import mx.core.UITextField;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;

	public class BreadCrumbs extends Container
	{
		public function BreadCrumbs() {
			super();
			
			BindingUtils.bindProperty(this, "dataProvider", Controller.instance, "breadCrumbsData");
			horizontalScrollPolicy = "off";
		}
		
		private var _crumbsChanged:Boolean = false;
		private var _topLineHeight:Number = 0;
        private var _topLineWidth:Number = 0;
        private var _bottomLineHeight:Number = 0;
        private var _bottomLineWidth:Number = 0;
		
		//--------------------------------------------------------------------------
	    //
	    //  Properties
	    //
	    //--------------------------------------------------------------------------
		protected var collection:ICollectionView;
				
		[Bindable("collectionChange")]
    	public function get dataProvider():Object {
			return collection;
		}
		
		public function set dataProvider(value:Object):void {
			if (value is Array) {
	            collection = new ArrayCollection(value as Array);
	        } else if (value is ICollectionView) {
	            collection = ICollectionView(value);
	        } else if (value == null) {
	        	collection = new ArrayCollection();
	        } else {
	            // convert it to an array containing this one item
	            var tmp:Array = [value];
	            collection = new ArrayCollection(tmp);
	        }	        
	
	        // weak listeners to collections and dataproviders
	        collection.addEventListener(CollectionEvent.COLLECTION_CHANGE, collection_changeHandler, false, 0, true);
	
	        var event:CollectionEvent =
	            new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);
	        event.kind = CollectionEventKind.RESET;
	        collection_changeHandler(event);
	        dispatchEvent(event);
		}
		
		private var controller:Controller = Controller.instance;
		
		//--------------------------------------------------------------------------
	    //
	    //  Overridden methods
	    //
	    //--------------------------------------------------------------------------
		
		override protected function commitProperties():void {
	        super.commitProperties();
	        
	        if (_crumbsChanged) {
	        	createCrumbs();
	        	_crumbsChanged = false;
	        }
	    }
	    	    	
		override protected function measure():void {
	        super.measure();  
	        
	        var hSpace:Number = getStyle("horizontalGap"); // Amount of space between items
	        var vSpace:Number = getStyle("verticalGap"); // Amount of space between lines
	        _topLineHeight = 0;
	        _topLineWidth = 0;
	        _bottomLineHeight = 0;
	        _bottomLineWidth = 0;
	        
	        var childCount:int = numChildren;
	        var child:IUIComponent;
	        for (var i:int = 0; i < childCount; i++) {
	        	child = IUIComponent(getChildAt(i));
	        	measuredHeight = Math.max(measuredHeight, child.getExplicitOrMeasuredHeight());
	        	measuredWidth += child.getExplicitOrMeasuredWidth() + (childCount - 1 == i ? 0 : hSpace);
	        }
	    }

		override protected function updateDisplayList(unscaledWidth:Number,
                                                  unscaledHeight:Number):void {
	        super.updateDisplayList(unscaledWidth, unscaledHeight);
	        
	        var widthToRemove:Number = measuredWidth > unscaledWidth 
	        								? measuredWidth - unscaledWidth : 0;
	        
	        var hSpace:Number = getStyle("horizontalGap"); // Amount of space between items
	        var vSpace:Number = getStyle("verticalGap"); // Amount of space between lines
	        var xPos:Number = 0;
	        var yPos:Number = 0;
	        var w:Number = 0;
	        var h:Number = 0;
	        var truncatedW:Number = 0;
	        
	        var childCount:int = numChildren;
	        var child:IUIComponent;
	        for (var i:int = 0; i < childCount; i++) {
	        	child = IUIComponent(getChildAt(i));
	        	w = child.getExplicitOrMeasuredWidth();
	        	h = child.getExplicitOrMeasuredHeight();
	        	
	        	if (widthToRemove > 0 && (i != 0) && (i != childCount - 1 ) && child is LinkButton) {
	        		truncatedW = Math.max(getMinLabelWidth(), w - widthToRemove);
	        		widthToRemove -= w - truncatedW;
	        		w = truncatedW;
	        	}
	        	
	        	child.setActualSize(w, h);
	        	
	        	yPos = (unscaledHeight - h) / 2; // Vertically center        	
	        	child.move(xPos, yPos);
	        	xPos += w + hSpace;	   	
	        }
	    }
	    
	    //--------------------------------------------------------------------------
	    //
	    //  Methods
	    //
	    //--------------------------------------------------------------------------
		
		protected function createCrumbs():void {
			var data:Object;
			var separator:UITextField;
			var crumb:LinkButton;

			// Delete all crumbs
			removeAllChildren();
			
			var iterator:IViewCursor = collection.createCursor();
			
			// Move to the beginning of the collection
			iterator.seek(CursorBookmark.FIRST);
			
			// Iterate through the collection
			while (!iterator.afterLast) {
				data = iterator.current;
				iterator.moveNext();
				
				crumb = new LinkButton();

				if (iterator.afterLast) {
					crumb.enabled = false;
					crumb.buttonMode = false;
				}
				
				crumb.label = data["label"];
				crumb.addEventListener(MouseEvent.CLICK, crumb_clickHandler);
				addChild(crumb);
				
				crumb.data = data;

				// Add the separtor
				// According to the latest spec, it appears after the last crumb also
				separator = new UITextField();
				separator.text = "/";
				addChild(separator);
			}
						
	        invalidateSize();
	        invalidateDisplayList();
		}
		
		protected function getMinLabelWidth():Number {
			return measureText("M...").width;
		}
		
		
		//--------------------------------------------------------------------------
	    //
	    //  Event Handlers
	    //
	    //--------------------------------------------------------------------------
		
		protected function crumb_clickHandler(event:MouseEvent):void {
			var data:Object = LinkButton(event.currentTarget).data;
			Controller.instance.currentFolder = data["folder"]; 
		}
		
		/*
		 *  Recreate the crumbs if the collection has changed
		 */				
		protected function collection_changeHandler(event:CollectionEvent):void {
			_crumbsChanged = true;
			invalidateProperties();
		}					
	}
}