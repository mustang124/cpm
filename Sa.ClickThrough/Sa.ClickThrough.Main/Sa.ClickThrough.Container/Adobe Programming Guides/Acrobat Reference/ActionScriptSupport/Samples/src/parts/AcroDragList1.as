package parts
{
	import mx.controls.List;
	import mx.core.DragSource;

	public class AcroDragList1 extends List
	{
		public function AcroDragList1()
		{
			super();
		}
		
		override protected function addDragData(ds:Object):void // guaranteed to be a DragSource
	    {
	        ds.addHandler(copySelectedItems, "attachments");
	    }
	}
}