package controller
{
	import acrobat.collection.IAttachment;
	import acrobat.collection.INavigatorHost;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.events.CollectionEvent;
	
	import parts.AcroSdkTileList;
	
	import utils.BindingSupport;
	import utils.LogUtil;

	public class Controller extends EventDispatcher {
		public function Controller(val:Object = null){
			super();
			if(val != kSingletonKey) 
				throw new ArgumentError("This is a singleton class. do not use the Constructor");
			selectionManager.mainController = this;
		}
		private static const kSingletonKey:Object = new Object;
			
		/** gets the instance of the Controller */
		public static function get instance():Controller { 
			if(!_instance)
				_instance = new Controller(kSingletonKey);
				
			return _instance;
		}
		private static var _instance:Controller;
		
		private var _host:INavigatorHost;
		private var _bindingSupport:BindingSupport = new BindingSupport();
		
		// sub-objects
		public const selectionManager:SelectionManager = new SelectionManager;
		public const breadCrumbsData:BreadCrumbsData = new BreadCrumbsData;
		public const commands:Commands = new Commands;
		public const eventSupport:EventSupport = new EventSupport;
		
		[Bindable]
		public function get host():INavigatorHost { return _host;}
		public function set host(val:INavigatorHost):void {
			if(_host != null) 
				_bindingSupport.dropChangeWatchers();
			
			_host = val;
			breadCrumbsData.host = val;
			if(_host != null) {
				_bindingSupport.bindSetter(aux_currentItems, this, "host.collection.items");
				_bindingSupport.bindSetter(aux_currentFolder, this, "host.currentFolder");
				_bindingSupport.bindSetter(aux_numFilesString, this, "currentItems.length");				
				
				_bindingSupport.bindProperty(breadCrumbsData, "currentDirectory", this, "currentFolder");				
			} else {
				// clear all the values for the watchers manually to defaults
				// doing this AFTER _host has been set to null so that no actions
				// are taken on the host, otherwise this would be up above...
				aux_currentFolder(null);
				aux_numFilesString();
				breadCrumbsData.currentDirectory = null;
				LogUtil.logger.info("Controller Set host=null");
			}
			
			selectionManager.host = _host;
			commands.host = _host;
			eventSupport.host = _host;
		}
		
		[Bindable]
		public function get mainNavigator():AcroSdkTileList { return _mainNavigator};
		public function set mainNavigator(val:AcroSdkTileList):void {
			_mainNavigator = val;
		}				
		private var _mainNavigator:AcroSdkTileList;
		
		private function aux_currentFolder(val:IAttachment):void {
			currentFolder = val;
		}			
					
		[Bindable]
		public function get currentFolder():IAttachment { return _currentFolder; }		
		public function set currentFolder(val:IAttachment):void {
			// record it
			_currentFolder = val;
			// tell the host (might be from the host actually)
			if(_host)
				_host.currentFolder = val;
			// update current items
			aux_currentItems(null);
		}
		private var _currentFolder:IAttachment = null;
		
		// update current items 
		private function aux_currentItems(val:IList):void {
			if(currentFolder)
				currentItems = currentFolder.children;
			else if(_host)
				currentItems = _host.collection.items;
		}
		
		[Bindable]
		public function get currentItems():IList { return _currentItems; }
		public function set currentItems(val:IList):void {
			_currentItems = val;
		}
		private var _currentItems:IList = new ArrayCollection();
		
		[Bindable ("numFilesStringChanged")]
		public function get numFilesString():String {
			var strTmp:String = currentItems.length.toString();
			strTmp += currentItems.length > 1 ? 
				_host.collection.getLocalizedString("items", " items") : _host.collection.getLocalizedString("item", " item");
			return strTmp;
		}

		private function aux_numFilesString(v:*=null):void {
			dispatchEvent(new Event("numFilesStringChanged"));
		}
	}
}