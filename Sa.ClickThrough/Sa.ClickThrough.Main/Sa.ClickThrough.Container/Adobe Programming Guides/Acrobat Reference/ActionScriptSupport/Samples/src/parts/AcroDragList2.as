package parts
{
	import acrobat.collection.IAttachment;
	import acrobat.collection.ICollectionDropManager;
	
	import mx.collections.IList;
	import mx.controls.List;
	import mx.events.DragEvent;

/** adds utility function used by DragWithin.mxml
 * 
 *   
 *   external drag directly into a folder
 * 
 *   internal drag into a folder
 * 
 */
	public class AcroDragList2 extends List
	{
		public function AcroDragList2() {
			super();
		}

		public var collectionDropManager:ICollectionDropManager;
		
		override protected function addDragData(ds:Object):void // actually a DragSource
	    {
	        ds.addHandler(copySelectedItems, "attachments");
	    }
	    

	    override public function showDropFeedback(event:DragEvent):void {
    		// TODO Add any visual drop feedback here. 
    		// We prevent the default because it uses an insertion point
    	}
    	
    	override public function hideDropFeedback(event:DragEvent):void {
    		// TODO Hide any visual drop feedback here. 
    		// We prevent the default because it uses an insertion point
    	}
	    
	    
	    public function getDropTargetAttachment(event:DragEvent):IAttachment  {
	    	// we can only do this if we know for a fact that the dataProvider is an IList
	    	// note that this will break as soon as the dataProvider is an ICollectionView
	    	// (to be fully safe, you'd have to impl this in terms of iterators 
	    	var cheatList:IList = dataProvider as IList;
	    	if(!cheatList || !cheatList.length) 
	    		return null;
	    	
	    	
			var idx:int = calculateDropIndex(event);
			if(idx >= cheatList.length)
				return null;
			return idx == -1 ? null : cheatList.getItemAt(idx) as IAttachment;	    	
	    }
	}
}