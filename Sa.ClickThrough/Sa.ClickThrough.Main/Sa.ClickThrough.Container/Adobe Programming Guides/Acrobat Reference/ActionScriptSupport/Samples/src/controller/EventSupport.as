package controller
{
	import acrobat.collection.IAttachment;
	import acrobat.collection.ICollection;
	import acrobat.collection.INavigatorHost;
	import acrobat.collection.RightClickEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	
	import mx.core.UIComponent;
	
	import parts.BreadCrumbs;
	
	import utils.LogUtil;
	
	public class EventSupport
	{
		public function EventSupport() {
			super();
		}

		public function get host():INavigatorHost { return _host;}
		public function set host(val:INavigatorHost):void {	
			try {				
				if(_host)
					_host.removeEventListener(RightClickEvent.RIGHT_CLICK, onRightClick);
				if(_coll)
					_coll.removeEventListener('needsSave', onNeedsSave);
				
				_host = val;
				_coll = val ? val.collection : null;
				
				if(_host)
					_host.addEventListener(RightClickEvent.RIGHT_CLICK, onRightClick);
				if(_coll)
					_coll.addEventListener('needsSave', onNeedsSave);	
			} catch (e:Error) {
				LogUtil.logger.error("stack trace: {0}", e.getStackTrace());
			}								
		}
		private var _coll:ICollection;
		private var _host:INavigatorHost;		
		
		private function onRightClick(e:RightClickEvent):void {		
			var where:Point = new Point(e.stageX, e.stageY);
	
			// BreadCrumbs does not allow context menu
			if(hasVetoUnderPoint(where)) return;
			
			var theAttachment:IAttachment = getAttachmentUnderPoint(where);
		
			if(theAttachment) {
				// if the attachment is not already part of the selection
				if(-1 == mainController.selectionManager.selectedItems.indexOf(theAttachment)) {
					mainController.selectionManager.selectedItems = [theAttachment];
				}
			} 
			mainController.commands.contextMenu(where);
		}
		
		private function hasVetoUnderPoint(where:Point):Boolean {
			var veto:Boolean = false;
			var moduleRoot:UIComponent = UIComponent(mainController.mainNavigator).parent as UIComponent;
	
			// check all the objects under the right-click point
			var hits:Array = moduleRoot.stage.getObjectsUnderPoint(where);
			if(!hits) return false;
			
			hits = hits.reverse();
			hits.some(function(hit:*, a:*, b:*):Boolean {
				if(!hit || !hit is DisplayObject) return false;
				var rents:Array = parentage(hit);
				return rents.some(function(comp:*, aa:*, bb:*):Boolean {
					veto = comp is BreadCrumbs;
					return veto;
				}); // the click was on BreadCrumbs
			}); // find one hit
			return veto;
		}
	
		private function getAttachmentUnderPoint(where:Point):IAttachment {
			var theAttachment:IAttachment;
			var moduleRoot:UIComponent = UIComponent(mainController.mainNavigator).parent as UIComponent;
	
			// check all the objects under the right-click point
			var hits:Array = moduleRoot.stage.getObjectsUnderPoint(where);
			if(!hits) return null;
			
			hits = hits.reverse();
			hits.some(function(hit:*, a:*, b:*):Boolean {
				if(!hit || !hit is DisplayObject) return false;
				var rents:Array = parentage(hit);
				return rents.some(function(comp:*, aa:*, bb:*):Boolean {
					if(comp && 'data' in comp)
						theAttachment = comp.data as IAttachment;
					return theAttachment != null;
				}); // find one good ancestor
			}); // find one hit
			return theAttachment;
		}
		
		private function parentage(obj:DisplayObject):Array {
			var rval:Array = [];
			while(obj){
				rval.push(obj);
				obj = obj.parent;
			}
			return rval;
		}
		
		private function get mainController():Controller { return Controller.instance;}

		private function onNeedsSave(e:Event):void {
			host.collection.setDirty();
		}
	}
}