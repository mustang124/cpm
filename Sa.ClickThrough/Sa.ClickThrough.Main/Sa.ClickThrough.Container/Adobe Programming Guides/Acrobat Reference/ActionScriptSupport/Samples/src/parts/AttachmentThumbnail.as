package parts
{
	import acrobat.collection.IAttachment;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	import mx.core.UIComponent;
	import mx.core.BitmapAsset;
	import mx.binding.utils.ChangeWatcher;
	
	import utils.LogUtil;

	public class AttachmentThumbnail extends UIComponent
	{
		[Embed(source='/assets/A_Folder_48x48_N.png')] 
		public var defaultFolderIcon:Class;
		
		private var _attachmentThumbnailWatcher:ChangeWatcher;
		private var _thumbnail:BitmapAsset = null;
		
		public function AttachmentThumbnail() {
			super();
			_attachmentThumbnailWatcher = ChangeWatcher.watch(null, "thumbnail", aux_thumbnail);
		}
		
		private function aux_thumbnail(event:Event):void {
			var bitmapData:BitmapData = getAttachmentThumbnailData();
			if (bitmapData) thumbnailData = bitmapData;
		}
		
		private function getAttachmentThumbnailData():BitmapData {
			var result:BitmapData = null;
			var bitmap:Bitmap = _attachment.thumbnail;
			if (bitmap) {
				result = bitmap.bitmapData;
			}
			return result;
		}
		
		private function requestThumbnail(requestWidth:int, requestHeight:int):void {
			if (_attachment) {
					_attachment.requestThumbnail(requestWidth, requestHeight);
			}
		}

		[Bindable]
		public function get attachment():IAttachment { return _attachment;}
		public function set attachment(value:IAttachment):void {
			_attachmentThumbnailWatcher.unwatch();
			_attachment = value;
			if (value == null) {
				thumbnailData = null;
			} else if (value.isFolder) {
				thumbnailClass = defaultFolderIcon;
			} else {
				var bitmapData:BitmapData = getAttachmentThumbnailData();
				if ((bitmapData == null) && value.icon) bitmapData = value.icon.bitmapData;
				thumbnailData = bitmapData;
				_attachmentThumbnailWatcher.reset(value); 
			}
		}
		private var _attachment:IAttachment;

		public function set thumbnailData(bitmapData:BitmapData):void {
			var bitmap:BitmapAsset = null;
			if (bitmapData) {
				bitmap = new BitmapAsset(bitmapData);
				bitmap.smoothing = true;
			}
			thumbnail = bitmap;
		}
		
		public function set thumbnailClass(bitmapAssetClass:Class):void {
			if (!(_thumbnail is bitmapAssetClass)) {
				var newThumbnail:BitmapAsset = new bitmapAssetClass();
				newThumbnail.smoothing = true;
				thumbnail = newThumbnail; 
			}
		}
		
		private function set thumbnail(bitmap:BitmapAsset):void {
			if (_thumbnail) removeChild(_thumbnail);			
			_thumbnail = bitmap;
			if (_thumbnail)  {
				_thumbnail.visible = false;
				addChild(_thumbnail);
			}
			
			invalidateSize();
			invalidateDisplayList();
		}

		override protected function measure():void {
			super.measure();
			if (_thumbnail) {
				measuredWidth = _thumbnail.width;
				measuredHeight = _thumbnail.height;
			} 				
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			requestThumbnail(unscaledWidth, unscaledHeight);			
			if (_thumbnail) {
				_thumbnail.visible = true;
				
				var thumbWidth:Number = _thumbnail.width / _thumbnail.scaleX;
				var thumbHeight:Number = _thumbnail.height / _thumbnail.scaleY;
				// Scale image only if the image is too big.
				if ((thumbWidth > unscaledWidth) || (thumbHeight > unscaledHeight)) {
					var ar:Number = thumbWidth / thumbHeight;
					var pr:Number = unscaledWidth / unscaledHeight;
					if (ar > pr) {
						thumbWidth = unscaledWidth;
						thumbHeight = unscaledWidth / ar;
					} else {
						thumbWidth = unscaledHeight * ar;
						thumbHeight = unscaledHeight;
					}
				}
				_thumbnail.setActualSize(thumbWidth, thumbHeight);
								
				var horizontalAlignFactor:Number = 0.5;
				var verticalAlignFactor:Number = 0.5;

				var thumbX:Number = (unscaledWidth - _thumbnail.width) * horizontalAlignFactor;
				var thumbY:Number = (unscaledHeight - _thumbnail.height) * verticalAlignFactor; 
				_thumbnail.move(Math.max(0, thumbX), Math.max(0, thumbY));
			}
		}
	}
}