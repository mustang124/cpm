package controller
{
	import acrobat.collection.CommandType;
	import acrobat.collection.IAttachment;
	import acrobat.collection.ICommand;
	import acrobat.collection.INavigatorHost;
	
	import flash.events.EventDispatcher;
	import flash.events.Event;
	import flash.geom.Point;
	
	import mx.binding.utils.BindingUtils;
	
	import utils.LogUtil;
	import utils.BindingSupport;

	public class Commands extends EventDispatcher
	{
		private static const k_DefaultCommands:Array = 
			[
				CommandType.PREVIEW,
				CommandType.CONTEXT_MENU
			];	
				
		private var bindingSupport:BindingSupport = new BindingSupport();

		public function Commands() {
			super();
		}
		
		[Bindable]
		public function get host():INavigatorHost { return _host;}
		public function set host(val:INavigatorHost):void {
			if (_host != null) {
				bindingSupport.dropChangeWatchers();
			}
			_host = val;
			if (_host != null) {
				bindingSupport.bindSetter(processCommands, this, "host.commands");
				_host.requestCommands(k_DefaultCommands);
			}
		}
		private var _host:INavigatorHost;
		
		private function requestCommand(key:String, a:* = null, b:* = null):void {
			if (_host != null)
				_host.requestCommands([key], null);
		}
		
		private function processCommands(cmds:Object):void {
			var old:Object = _cmdMap;
			
			_cmdMap = {};

			for (var name:String in cmds) {
				_cmdMap[name] = cmds[name];	
				if(!(name in old))
					dispatchEvent(new Event(name));
			}
		}
		
		private var _cmdMap:Object = {};
		
		/**
		 * asks that the acrobat context menu for packages be opened 
		 * takes <strike>an optional</strike> Point to <strike>override the default</strike> set the x,y location
		 **/
		public function contextMenu(where:Point):void {
			getCommand(CommandType.CONTEXT_MENU).execute(null, [where.x, where.y]);
		}
		
		/**
		 * asks that the acrobat do full-window preview of the supplied attachment
		 * currently, it ignores the argument and uses the current selection 
		 **/
		public function preview(item:IAttachment):void { 
			Controller.instance.selectionManager.selectedItems = [item];
			getCommand(CommandType.PREVIEW).execute([item]);
		}
	
		
		private function getCommand(key:String):ICommand { 
			var rval:* = null;
			try {
				rval = key in  _cmdMap ? _cmdMap[key] : null;
				if(!rval)
					requestCommand(key);
			} catch (e:Error) {
				
				LogUtil.logger.error("{0}", e.getStackTrace());
			}
			return rval as ICommand;
		}
	}
}