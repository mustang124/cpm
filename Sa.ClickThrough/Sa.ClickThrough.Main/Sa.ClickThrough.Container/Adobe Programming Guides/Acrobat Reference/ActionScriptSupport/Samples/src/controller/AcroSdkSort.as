package controller
{
	import acrobat.collection.ISort;

	public class AcroSdkSort implements ISort
	{
		private var _name:String;
		private var _ascending:Boolean;
		
		public function AcroSdkSort(name:String, ascending:Boolean)
		{
			_name = name;
			_ascending = ascending;
		}

		public function get name():String
		{
			return _name;
		}
		
		public function get ascending():Boolean
		{
			return _ascending;
		}
		
	}
}