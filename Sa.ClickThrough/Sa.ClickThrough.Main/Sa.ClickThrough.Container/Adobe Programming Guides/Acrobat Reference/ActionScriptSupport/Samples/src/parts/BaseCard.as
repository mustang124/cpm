package parts
{
	import acrobat.collection.IAttachment;
	
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import mx.containers.VBox;
	
	import controller.Controller;
	import utils.LogUtil;

	public class BaseCard extends VBox
	{
		public function BaseCard() {
			super();
		}
		
		[Bindable]
		private var _controller:Controller = Controller.instance;
		
		public override function set data(value:Object):void {		
			super.data = _attachment = value as IAttachment;
		}
		
		[Bindable]
		protected var _attachment:IAttachment;
		
		protected override function initializationComplete():void {
			doubleClickEnabled = true;
			addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick);
		}
		
		protected function onDoubleClick(event:MouseEvent):void {			
			if(_attachment.isFolder)
				_controller.currentFolder = _attachment;
			else
				_controller.commands.preview(_attachment);				
		}
		
		[Bindable]
		public function get thumbnailSize():Number { 
			return _thumbnailSize;
		}
		public function set thumbnailSize(val:Number):void { 
			_thumbnailSize = val;
		}
		private var _thumbnailSize:Number = 70;		
	}
}