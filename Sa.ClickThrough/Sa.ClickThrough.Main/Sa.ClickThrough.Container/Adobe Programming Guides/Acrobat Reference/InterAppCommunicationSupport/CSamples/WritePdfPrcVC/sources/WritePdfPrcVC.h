// WritePdfPrcVC.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

// CWritePdfPrcVC:
// See WritePdfPrcVC.cpp for the implementation of this class
//

class CWritePdfPrcVC : public CWinApp
{
public:
	CWritePdfPrcVC();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CWritePdfPrcVC theApp;