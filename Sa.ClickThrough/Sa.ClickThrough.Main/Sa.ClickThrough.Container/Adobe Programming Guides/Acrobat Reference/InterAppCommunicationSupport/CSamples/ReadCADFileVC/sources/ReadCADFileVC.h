// ReadCADFileVC.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

// CReadCADFileVC:
// See ReadCADFileVC.cpp for the implementation of this class
//

class CReadCADFileVC : public CWinApp
{
public:
	CReadCADFileVC();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CReadCADFileVC theApp;