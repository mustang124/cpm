// SampleCADApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ReadPdfPrc.h"

#include <acrobat.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static HWND msgWnd = NULL;
PROCESS_INFORMATION processInfo;
#define BUF_LEN	1024

// CReadPdfPrc

BEGIN_MESSAGE_MAP(CReadPdfPrc, CWinApp)

END_MESSAGE_MAP()


// CReadPdfPrc construction

CReadPdfPrc::CReadPdfPrc()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CReadPdfPrc object

CReadPdfPrc theApp;

void ExportFromPRC(CString strPathName) {
	// Find the invisible message window created by the
	// ReadPdfPrcHelper plug-in.
	if (!msgWnd)
		msgWnd = ::FindWindow(TEXT("PRCIACMsgWin"), NULL);

	if (msgWnd && !strPathName.IsEmpty()) {
		char strPathNameA[BUF_LEN];
		memset(strPathNameA, 0x00, BUF_LEN);

		WideCharToMultiByte(CP_UTF8, 0, strPathName.GetString(), -1, 
							strPathNameA, BUF_LEN, NULL, NULL);

		COPYDATASTRUCT copyData;
		copyData.dwData = NULL;
		copyData.cbData = strlen(strPathNameA);
		copyData.lpData = (PVOID)strPathNameA;

		::SendMessage (msgWnd, WM_COPYDATA, NULL, (LPARAM)&copyData);
	}
}

void Display3dData(CString strFileName)
{
	STARTUPINFO si;
    PROCESS_INFORMATION pi;
    TCHAR szCmdline[BUF_LEN];
	memset(szCmdline, 0, BUF_LEN);

	wcscpy_s(szCmdline, TEXT("notepad.exe "));
	wcscat_s(szCmdline, TEXT("\""));
	wcscat_s(szCmdline, strFileName.GetString());
	wcscat_s(szCmdline, TEXT("\""));

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    // Start the child process. 
    if( !CreateProcess( NULL,   // No module name (use command line)
        szCmdline,				// Command line
        NULL,					// Process handle not inheritable
        NULL,					// Thread handle not inheritable
        FALSE,					// Set handle inheritance to FALSE
        0,						// No creation flags
        NULL,					// Use parent's environment block
        NULL,					// Use parent's starting directory 
        &si,					// Pointer to STARTUPINFO structure
        &pi )					// Pointer to PROCESS_INFORMATION structure
    ) {
        printf( "CreateProcess for notepad.exe failed (%d).\n", GetLastError() );
        return;
    }

    // Wait until child process exits.
    WaitForSingleObject(pi.hProcess, INFINITE);

    // Close process and thread handles. 
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
}

// CReadPdfPrc initialization

BOOL CReadPdfPrc::InitInstance()
{
	CWinApp::InitInstance();

	// === Initialize OLE libraries. 
	if (!AfxOleInit())	{
		AfxMessageBox(TEXT("OLE initialization failed."));
		return FALSE;
	}

	AfxEnableControlContainer();

	CString strFilter = TEXT("PDF (*.pdf)|*.pdf||");
					
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_OVERWRITEPROMPT, strFilter);
	fileDlg.GetOFN().lpstrTitle = TEXT("Select a PDF File to read PRC From");

	if(fileDlg.DoModal() == IDOK){
		CString strPathName = fileDlg.GetPathName();
		CString strFileName = fileDlg.GetFileName();
		CString strFileTitle = fileDlg.GetFileTitle();
		CString strFileExt = fileDlg.GetFileExt();
		CString strXmlFileName = TEXT("");
		strXmlFileName = strPathName.Left(strPathName.ReverseFind(TEXT('.'))) + TEXT(".xml");

		COleException e;

		CAcroApp * pAcroApp = new CAcroApp();
		pAcroApp->CreateDispatch(TEXT("AcroExch.App"), &e);

		CAcroAVDoc * pAcroAvDoc = new CAcroAVDoc();
		pAcroAvDoc->CreateDispatch(TEXT("AcroExch.AVDoc"), &e);

		if(!pAcroAvDoc->Open(strPathName, strFileName)) {
			CString strMsg = TEXT("Failed to open file ");
			strMsg += strFileName;
			strMsg += TEXT("!");
			AfxMessageBox(strMsg);

			delete pAcroAvDoc;

			pAcroApp->Exit();
			delete pAcroApp;
			return FALSE;
		}

		pAcroApp->Hide();

		if (_waccess(strXmlFileName, 00) != -1)
			_wunlink(strXmlFileName);
		ExportFromPRC(strPathName);
		Display3dData(strXmlFileName);

		pAcroAvDoc->Close(1);
		delete pAcroAvDoc;

		pAcroApp->Hide(); 
		pAcroApp->CloseAllDocs();
		pAcroApp->Exit();
		delete pAcroApp;
	} 

	// Since the dialog has been closed, return FALSE so that we exit the
	// application, rather than start the application's message pump.
	return FALSE;
}
