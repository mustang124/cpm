
// Copyright � 2011 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

// Saves the PDF as a certified document with an invisible signature and a time stamp.

// http://www.adobe.com/security/digsig.html
// http://help.adobe.com/en_US/acrobat/standard/using/WS58a04a822e3e50102bd615109794195ff-7d8b.w.html

// Forum Resources:
// http://forums.adobe.com/thread/432502?tstart=0
// http://forums.adobe.com/message/2501882#2501882


if (typeof strVersion == "undefined") var strVersion = "1.0.4";
// 1.0.3 2011.09.01 RRH - Inital release


///////////////////////////////////////////////////////////////////////////////////////////////////
// Parameters
if (typeof cerSolomon == "undefined") var cerSolomon = {};

    // Set signature properties to be viewed by end users

    cerSolomon.Legal = "This document contains a Click-though agreement via an on-open dialog box written in JavaScript."
    cerSolomon.Copyright = "� " + util.printd("yyyy", new Date()) + " HSB Solomon Associates LLC";
    cerSolomon.Reason = "This document has been digitally signed, authenticated, and encrypted by HSB Solomon Associates LLC. (Version: " + strVersion + ")\r\n\r\n" + cerSolomon.Copyright;
    cerSolomon.Location = "Dallas, Texas, USA";
    cerSolomon.ContactInfo = "www.solomononline.com";


    // The MPDValue certifies the document and sets the types of changes allowed.
    //
    // allowAll             � Allow all changes to a document without any of these changes invalidating
    //                        the signature. This results in MDP not being used for the signature. This
    //                        was the behavior for Acrobat 4.0 through 5.1.
    //
    // allowNone            � Do not allow any changes to the document without invalidating the signature.
    //                        Note that this will also lock down the author�s signature.
    //
    // default              � Allow form field fill-in if form fields are present in the document. Otherwise,
    //                        do not allow any changes to the document without invalidating the signature.
    //
    // defaultAndComments   � Allow form field fill-in if form fields are present in the document and allow
    //                        annotations (comments) to be added, deleted or modified. Otherwise, do not
    //                        allow any changes to the document without invalidating the signature. Note
    //                        that annotations can be used to obscure portions of a document and thereby
    //                        affect the visual presentation of the document.

    cerSolomon.MPDValue = "allowNone";


    // The signature handler must be Adobe.PPKLite

    cerSolomon.HandlerName = "Adobe.PPKLite";


    // The hash function used to encrypt the document
    //
    // SHA1      -
    // SHA256    - Acrobat 8.0 or later
    // SHA384    - Acrobat 8.0 or later
    // SHA512    - Acrobat 8.0 or later
    // RIPEMD160 - Acrobat 8.0 or later

    cerSolomon.Hash = "SHA1";


    // Set certificate to encrypt the document and the password.

    cerSolomon.DigitalIDPath = "/C/Certificates/Solomon2010-2012.pfx";
    cerSolomon.Password = "Jara18blanton";


    // TimeStamp server authenticates when the document was signed.
    // TimeStamps can be verified in Acrobat 7 or later.

    cerSolomon.TimeStamp = "https://timestamp.geotrust.com/tsa/";


    // Signature field name.  The name of the field used to create the signature.

    cerSolomon.Fieldname = "SigCert";


///////////////////////////////////////////////////////////////////////////////////////////////////
// Functions

// Public function to certify and save the document.
if (typeof CertifyDocument == "undefined")
function CertifyDocument() {

    if (typeof curDoc == "undefined")
    var curDoc = this;

    if (typeof sigField == "undefined")
    var sigField = curDoc.addField(cerSolomon.Fieldname, "signature", 0, [0, 0, 0, 0]);

    CertifyDoc(sigField, cerSolomon);

};

///////////////////////////////////////////////////////////////////////////////////////////////////
// Trusted Functions

if (typeof CertifyDoc == "undefined")
var CertifyDoc = app.trustedFunction(function (signatureField, certificateInfo)
    {
        app.beginPriv();

        if (typeof myEngine == "undefined")
        var myEngine = security.getHandler(certificateInfo.HandlerName);

        //    var policyOptions = {
        //        cHandler: security.PPKLiteHandler,
        //        cTarget: "RoggeHeflin(SA).pfx"
        //    };

        //    var policyArray = security.getSecurityPolicies(
        //        {
        //            oOptions: policyOptions
        //        }
        //        );

        //    var LoginParams = {
        //        oEndUserSignCert: policyArray
        //    };

        // Login parameters for the security handler
        if (typeof loginParams == "undefined")
        var loginParams = {
            cDIPath: certificateInfo.DigitalIDPath,
            cPassword: certificateInfo.Password
        };

        // Login into the security hander
        myEngine.login(
        {
            oParams: loginParams,
            bUI: 0
        }
        );
        
        // Set the properties for the signature
        if (typeof signatureInfo == "undefined")
        var signatureInfo = {
            password: certificateInfo.Password,
            reason: certificateInfo.Reason,
            location: certificateInfo.Location,
            contactInfo: certificateInfo.ContactInfo,
            timeStamp: certificateInfo.TimeStamp,
            mdp: certificateInfo.MPDValue,
            digestMethod: certificateInfo.Hash
        };

        // Certify and save the document
        signatureField.signatureSign(
        {
            oSig: myEngine,
            bUI: 0,
            cLegalAttest: certificateInfo.Legal,
            oInfo: signatureInfo
        }
        );

        app.endPriv();
    }
);
