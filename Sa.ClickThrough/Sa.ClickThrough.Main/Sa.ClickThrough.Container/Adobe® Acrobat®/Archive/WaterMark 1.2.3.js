// Copyright � 2013 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

if (typeof strVersion == "undefined") var strVersion = "1.2.3";

// RGB Color co-ordinates for screen and printing.  Acrobat uses [0, 1]
// � Screen (144, 0, 40) - (0.5625    , 0.0, 0.15625)
// � Print  (165, 0, 33) - (0.64453125, 0.0, 0.12890625)

if (typeof colorSolomon == "undefined") var colorSolomon = ["RGB", 0.5625, 0.0, 0.15625];

if (typeof SolomonMark == "undefined")
function SolomonMark()
{
	var curDoc = this;

	var strWaterMark = curDoc.info.WaterMarkType;

	if (typeof strWaterMark != "undefined")
	{

		var pageBeg = 2;

		// Determine the style of watermark to appply.  The style depends on the format of the source document. 
		// strWaterMark is the same as in SolomonUI subL
		switch(strWaterMark)
		{
			// Note: margin values are inches; however, Acrobat uses points. 
			// addSolMark(curDoc, pageBeg, sideTrigP, pTop, pMargin, sideTrigL, lTop, lMargin, strHeader, bDataTable) 
			case "8.5 � 11, Upper Left":
				addSolMark(curDoc, 1, "Left", 0.475, 0.985, "Left", 0.182, 0.845, "", false);
				break;

			case "8 � 9.5, Upper Alternating (NGPP)":
				addSolMark(curDoc, pageBeg, "Both", 0.475, 0.980, "Left", 0.182, 0.845, "Gas Plant Study Methodology", true);
				break;

			case "8 � 9.5, Upper Alternating (Olefins)":
				addSolMark(curDoc, pageBeg, "Both", 1.39, 1.679, "Left", 0.182, 0.845, "Worldwide Olefin Plant Performance Analysis", true);
				break;

			case "8.5 � 11, Upper Alternating (Olefins)":
				addSolMark(curDoc, pageBeg, "Both", 1.0, 0.625, "Left", 0.182, 0.845, "Worldwide Olefin Plant Performance Analysis", false);
				break;

			case "8 � 9.5, Upper Alternating (Fuels)":
				addSolMark(curDoc, pageBeg, "Both", 1.355, 1.65, "Left", 0.182, 0.845, "Worldwide Fuels Refinery Performance Analysis", true);
				break;

			case "8 � 9.5, Upper Alternating (Lubes)":
				addSolMark(curDoc, pageBeg, "Both", 1.355, 1.65, "Left", 0.182, 0.845, "Worldwide Paraffinic Lube Refinery Performance Analysis", true);
				break;

			case "8 � 9.5, Lower Center":
				addSolMark(curDoc, pageBeg, "Right", 10.342, 1.000, "Right", 7.842, 1.000, "", true);
				break;

			case "No WaterMark":
				console.println("No WaterMark type defined for '" + curDoc.documentFileName + "'");
				break;

			default:
				console.println("Improper WaterMark type '" + curDoc.info.DocType + "' specified for '" + curDoc.documentFileName + "'\r\nVerify SolomonUI " + strVersion + ".js and WaterMark " + strVersion + ".js");
				break;
		};
	}
	else
	{
		console.println("WaterMarkType is undefined for '" + curDoc.documentFileName + "'");
	};

	};

	// pageBeg		-
	// sideTrigP	- Portrait pages; trigger for watermark alignment ("Right", "Left", or "Other")
	// pTop			-
	// pMargin		-
	// sideTrigL	- Landscape pages; trigger for watermark alignment ("Right", "Left", or "Other")
	// lTop			-
	// lMargin		-
	// bDataTable	-

	if (typeof addSolMark == "undefined")
	function addSolMark(curDoc, pageBeg, sideTrigP, pTop, pMargin, sideTrigL, lTop, lMargin, strHeader, bDataTable)
	{
	if (curDoc.numPages >= pageBeg)
	{
		// Create the recepient information
		if ((curDoc.info.Refnum != "") && (typeof curDoc.info.Refnum != "undefined"))
		{
			var strPreparedFor = curDoc.info.Client + " (" + curDoc.info.Refnum + ")"
		}
		else
		{
			var strPreparedFor = curDoc.info.Client
		};

		// Prefix the string with 'Prepared for '
		var strPrepared = "Prepared for " + strPreparedFor;

		// Initialize and display a status window and progress bar
		var wBar = app.thermometer;
		wBar.duration = curDoc.numPages - pageBeg + 1;
		wBar.begin();

		for (pageWm = pageBeg - 1; pageWm < curDoc.numPages; pageWm++)
		{
			if (curDoc.getPageNumWords(pageWm) > 0)
			{
				// Indicate the current page 
				wBar.text = "Adding watermark to page " + pageWm + ".";

				if (strHeader != "")
				{
					if (IsTextOnPage(curDoc, pageWm, strHeader) == true)
					{
						switch (curDoc.getPageRotation(pageWm))
						{
							case 0: { addWmPortrait(curDoc, pageWm, strPrepared, sideTrigP, -pTop, pMargin, false); break; };
							case 90: { addWmLandscape(curDoc, pageWm, strPrepared, sideTrigL, -lTop, lMargin, false); break; };
						};
					}
					else
					{
						var vMargin = 0.500;
						var lMargin = 1.800;
						var rMargin = 0.800;

						addDataTableFooter(curDoc, pageWm, "Proprietary and Confidential", strPrepared, vMargin, lMargin, rMargin)
					};
				}
				else
				{
					if (bDataTable != true)
					{
						switch (curDoc.getPageRotation(pageWm))
						{
							case 0: { addWmPortrait(curDoc, pageWm, strPrepared, sideTrigP, -pTop, pMargin, bDataTable); break; };
							case 90: { addWmLandscape(curDoc, pageWm, strPrepared, sideTrigL, -lTop, lMargin, bDataTable); break; };
						};
					}
					else
					{
						var vMargin = 0.500;
						var lMargin = 1.800;
						var rMargin = 0.800;

						addDataTableFooter(curDoc, pageWm, "Proprietary and Confidential", strPrepared, vMargin, lMargin, rMargin)
					};
				};

			};

			// Step the progress bar
			wBar.value = pageWm - pageBeg + 1;

		};

		// Close progress bar
		wBar.end();

	};
	};

	if (typeof IsTextOnPage == "undefined")
	function IsTextOnPage(curDoc, pageWm, strText)
	{
	var b = false;

	var arrTitle = strText.split(" ");
	var l = arrTitle.length;

	var w = 0;
	var c = 0;
	var t = 0;

	for (w = 0; w < l * 10 + 1; w++)
	{
		for (t = 0; t < arrTitle.length; t++)
		{
			if (arrTitle[t] == curDoc.getPageNthWord(pageWm, w + t))
			{
				c = t;
			}
			else
			{
				break;
			};
		};

		if (c == l - 1)
		{
			b = true;
			break;
		};
	};

	return b;

	};

	if (typeof addDataTableFooter == "undefined")
	function addDataTableFooter(curDoc, pageWm, strNotice, strPrepared, vMargin, lMargin, rMargin)
	{

	var vFont = font.Helv;
	var vSize = 7;

	lMargin = lMargin * 72.0;
	rMargin = rMargin * 72.0;
	vMargin = vMargin * 72.0;

	// Left side footer
	curDoc.addWatermarkFromText
	(
	{
		cText: strNotice,
		nTextAlign: app.constants.align.left,
		cFont: vFont,
		nFontSize: vSize,
		aColor: colorSolomon,
		nStart: pageWm,
		bOnTop: true,
		bOnScreen: true,
		bOnPrint: true,
		nHorizAlign: app.constants.align.left,
		nVertAlign: app.constants.align.bottom,
		nHorizValue: lMargin,
		nVertValue: vMargin,
		bPercentage: false,
		nScale: 1.0,
		bFixedPrint: false,
		nRotation: 0,
		nOpacity: 1.0
	}
	);

	// Right side footer
	curDoc.addWatermarkFromText
	(
	{
		cText: strPrepared,
		nTextAlign: app.constants.align.right,
		cFont: vFont,
		nFontSize: vSize,
		aColor: colorSolomon,
		nStart: pageWm,
		bOnTop: true,
		bOnScreen: true,
		bOnPrint: true,
		nHorizAlign: app.constants.align.right,
		nVertAlign: app.constants.align.bottom,
		nHorizValue: - rMargin,
		nVertValue: vMargin,
		bPercentage: false,
		nScale: 1.0,
		bFixedPrint: false,
		nRotation: 0,
		nOpacity: 1.0
	}
	);

	};

	if (typeof addWmLandscape == "undefined")
	function addWmLandscape(curDoc, pageWm, strPrepared, sideTrigL, vMargin, hMargin, bDataTable)
	{

	var vFont;
	var vSize;

	if(bDataTable == true)
	{
		vFont = font.Helv;
		vSize = 7;
	}
	else
	{
		vFont = font.Helv;
		vSize = 7;
	};

	hMargin = hMargin * 72.0;
	vMargin = vMargin * 72.0;

	if ((sideTrigL == "Left") || ((pageWm % 2 == 0) && (sideTrigL == "Both")))
	{
		curDoc.addWatermarkFromText
		(
		{
			cText: strPrepared, 
			nTextAlign: app.constants.align.left, 
			cFont: vFont, 
			nFontSize: vSize, 
			aColor: colorSolomon, 
			nStart: pageWm, 
			bOnTop: true,
			bOnScreen: true,
			bOnPrint: true, 
			nHorizAlign: app.constants.align.left, 
			nVertAlign: app.constants.align.top,
			nHorizValue: hMargin,
			nVertValue: vMargin,
			bPercentage: false, 
			nScale: 1.0, 
			bFixedPrint: false, 
			nRotation: 0,
			nOpacity: 1.0
		}
		);
	};

	if ((sideTrigL == "Right") || ((pageWm % 2 == 1) && (sideTrigL == "Both")))
	{
		curDoc.addWatermarkFromText
		(
		{
			cText: strPrepared, 
			nTextAlign: app.constants.align.right, 
			cFont: vFont, 
			nFontSize: vSize, 
			aColor: colorSolomon, 
			nStart: pageWm, 
			bOnTop: true,
			bOnScreen: true,
			bOnPrint: true, 
			nHorizAlign: app.constants.align.right, 
			nVertAlign: app.constants.align.top,
			nHorizValue: - hMargin,
			nVertValue: vMargin,
			bPercentage: false, 
			nScale: 1.0, 
			bFixedPrint: false, 
			nRotation: 0,
			nOpacity: 1.0
		}
		);
	};
	};

	if (typeof addWmPortrait == "undefined")
	function addWmPortrait(curDoc, pageWm, strPrepared, sideTrigP, vMargin, hMargin, bDataTable)
	{

	var vFont;
	var vSize;

	if(bDataTable == true)
	{
		vFont = font.Helv;
		vSize = 7;
	}
	else
	{
		vFont = font.TimesI;
		vSize = 10;
	};

	hMargin = hMargin * 72.0;
	vMargin = vMargin * 72.0;

	if ((sideTrigP == "Left") || ((pageWm % 2 == 0) && (sideTrigP == "Both")))
	{
		curDoc.addWatermarkFromText
		(
		{
			cText: strPrepared, 
			nTextAlign: app.constants.align.left, 
			cFont: vFont, 
			nFontSize: vSize, 
			aColor: colorSolomon, 
			nStart: pageWm, 
			bOnTop: true,
			bOnScreen: true,
			bOnPrint: true, 
			nHorizAlign: app.constants.align.left, 
			nVertAlign: app.constants.align.top,
			nHorizValue: hMargin,
			nVertValue: vMargin,
			bPercentage: false, 
			nScale: 1.0, 
			bFixedPrint: false, 
			nRotation: 0,
			nOpacity: 1.0
		}
		);
	};

	if ((sideTrigP == "Right") || ((pageWm % 2 == 1) && (sideTrigP == "Both")))
	{
		curDoc.addWatermarkFromText
		(
		{
			cText: strPrepared, 
			nTextAlign: app.constants.align.right, 
			cFont: vFont, 
			nFontSize: vSize, 
			aColor: colorSolomon, 
			nStart: pageWm, 
			bOnTop: true,
			bOnScreen: true,
			bOnPrint: true, 
			nHorizAlign: app.constants.align.right, 
			nVertAlign: app.constants.align.top,
			nHorizValue: - hMargin,
			nVertValue: vMargin,
			bPercentage: false, 
			nScale: 1.0, 
			bFixedPrint: false, 
			nRotation: 0,
			nOpacity: 1.0
		}
	);
	};
};