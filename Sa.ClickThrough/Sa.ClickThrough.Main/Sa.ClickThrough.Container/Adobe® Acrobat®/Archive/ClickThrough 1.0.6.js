
// Copyright � 2012 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

if (typeof strVersion == "undefined") var strVersion = "1.0.6 (2012.06.27)";

// Sets the current document.
var curDoc = this;

// Stops access from external JavaScripts to this document.
curDoc.disclosed = false;

// Notify the user and close the document if expiry has occured.  'try' is used since two (2) variables used for expiration
// are not always present.  These variables are added when setting a document expiry date through the UI.
// An example of the code, that when added to the top, expires the document: var strExpiry = "01/31/2011"; var forExpiry = "mm/dd/yyyy";

try
{
    //var strExpiry = "01/31/2011"; var forExpiry = "mm/dd/yyyy";
    var dtExpiry = util.scand(forExpiry, strExpiry);
    
    if (dtExpiry.getTime() < new Date().getTime())
    {
        var boolExpired = 1;

        try{curDoc.deletePages({nStart: 1, nEnd: curDoc.numPages - 1});}
        catch(e){};

        var pFormat = "dddd, mmmm d, yyyy";
        var strViolation = "The information in this document expired on " + util.printd(pFormat, dtExpiry) + ".\r\n\r\nPlease contact Solomon for more information.";
  
        curDoc.dirty = false;
        app.alert(strViolation);
        curDoc.closeDoc(true);
    }
    else{var boolExpired = 0;};
}
catch(e){var boolExpired = 0;};

// Solomon logo
var SALogo =  
"ff810026ff7f0126ff7d0022ff7c0022ff7a0122ff7a0122ff7d0024ff7c0022ff7a0122ff790220ff7c0022ff7c0022ff7a0122ff7a0122ff7c0022ff7c0022ff7a0122ff7a0122ff7c00" +
"24ff7a0122ff7a0122ff790220ff7c0022ff7c0022ff7a0122ff7a0122ff7c0120ff7c0120ff7a0124ff7a0124ff7c0022ff7c0022ff7a0120ff7a0120ff7c0022ff7c0022ff7a0124ff7a" +
"0124ff7c0022ff7c0022ff7a0120ff7a0120ff7d0022ff7c0022ff7c0022ff7a0122ff7d0022ff7c0022ff7c0022ff7a0122ff7c0021ff7e0123ff810027ff810026ff7c0427ff7c142fff" +
"771830ff761830ff761830ff771732ff771732ff751930ff751930ff761830ff761830ff761832ff761830ff771732ff761830ff761830ff761830ff771732ff761832ff751930ff751930" +
"ff761830ff761830ff761832ff761830ff771830ff761830ff761832ff761832ff771732ff771830ff761830ff761830ff771830ff761832ff761832ff761832ff771732ff761830ff7618" +
"30ff761830ff771732ff771732ff761832ff761830ff771732ff771830ff761832ff76172fff7d1631ff790c29ff7c0326ff7e0025ff7b1230ffecaebdfffcd3dbfffed6defffed6deffff" +
"d7e0fffed6defffcd7defffcd7defffed6defffed6defffed6dffffcd7defffed6dffffed6defffed6defffcd7defffed6defffed6defffcd7defffcd7defffcd7defffed6defffed6dfff" +
"fed6defffed6defffed6defffed6defffed6deffffd6deffffd6defffed6defffcd7defffed6defffed6dffffed6dffffcd7dffffed6dffffed6defffed6defffed6defffed6dffffed6df" +
"fffed6dffffed6defffed6dffffed6defffed6dfffffd4ddffffced9ffb76a7cff71051fff7c0024ff771732ffffd9e2fffff8faffffe6ebffffe5ecffffe4edffffe5ecffffe6ecffffe6" +
"ecffffe4edffffe4edffffe5edffffe5edffffe5edffffe5ecffffe5ecffffe5ecffffe5ecffffe5ecffffe5edffffe5edffffe5edffffe5edffffe5edffffe4edffffe4edffffe5ecffff" +
"e5ecffffe5ecffffe4ecffffe4ecffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edffffe4edffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edff" +
"ffe5edffffe5ecffffe9effffff3faffbf8591ff6c061cff7a0122ff781931ffffdae3ffecc9cfff712839ff7f263cff82263fff7f253eff7f253eff7f263cff82243eff81253eff7f253e" +
"ff7f253eff81253eff81253eff81253eff7f263cff7f263cff81253cff81253eff81253eff81253eff81253eff81253eff81253eff81253eff81253eff81253eff7f263cff81253cff7f26" +
"3cff7f253eff7f253eff81253eff81253eff7f253eff7f253eff81253eff81253eff81253eff81253eff81253eff7f253eff7f253eff7f253eff81253eff7f253eff7a253cff9a5365ffff" +
"f0faffbc8491ff69051dff7a0122ff781931ffffdce6ffe9bec7ff670923ff780121ff790124ff790124ff790124ff7a0124ff7a0124ff7a0124ff790124ff790124ff7a0124ff7a0124ff" +
"7a0124ff790124ff790124ff790124ff7a0124ff7a0124ff7a0124ff7a0124ff790124ff790124ff7a0124ff7a0124ff790124ff790222ff790124ff790222ff790124ff790124ff7a0124" +
"ff7a0124ff790124ff790124ff7a0124ff7a0124ff7a0124ff7a0122ff790124ff790222ff790124ff790124ff7a0124ff790124ff710320ff8e354bffffedfaffbd8592ff690720ff7a01" +
"22ff791a32ffffdce6ffedbfc9ff690924ff7e0226ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7e" +
"0227ff7f0127ff810027ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7e0226ff7f0127ff7f0126ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7e0227ff" +
"7f0127ff7f0127ff7f0127ff7f0126ff7f0127ff7e0226ff7e0227ff7e0227ff7f0127ff7e0226ff760322ff953950ffffedfaffbc8491ff69071eff7a0122ff771732ffffdbe7ffeebeca" +
"ff6b0824ff800227ff820128ff820027ff810027ff810027ff810027ff810027ff810027ff810027ff820029ff820029ff810029ff810029ff7f0127ff7f0127ff810027ff7f0126ff7c03" +
"26ff7b0326ff7b0326ff7b0326ff7e0226ff7e0227ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff810027ff810027ff7f0127ff7f0127ff810027ff810027ff810027ff81" +
"0027ff810027ff810027ff7f0127ff810027ff810027ff7f0126ff780222ff953950ffffedfbffba8491ff6a081fff7a0122ff781833ffffdbe6ffeebecaff6b0824ff7f0126ff810026ff" +
"820026ff810027ff810027ff810029ff810029ff810027ff810027ff810029ff800028ff810027ff810027ff7f0127ff7e0227ff7d0125ff760020ff6f0420ff6a0620ff6b0721ff6d0520" +
"ff740321ff790124ff7d0125ff7f0126ff810026ff820026ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff820029ff810029ff810029ff7f0029ff8100" +
"27ff810027ff810027ff7e0226ff780222ff953950ffffedfbffba8491ff69071eff7a0122ff781833ffffdbe6ffedbfccff6c0725ff810026ff810026ff820026ff810027ff810027ff81" +
"0029ff810029ff810027ff810027ff810029ff810027ff810027ff7e0227ff770326ff700523ff6c0925ff90374dffbb667bffd48195ffde869cffcb7389ffab4f66ff7d1d38ff6e0523ff" +
"750324ff7c0326ff7f0126ff810027ff810027ff7f0127ff7f0127ff800026ff810027ff810027ff810027ff820029ff810029ff810029ff810027ff810027ff810027ff810027ff7e0226" +
"ff780222ff953950ffffedfbffba8492ff69071eff7a0122ff781833ffffdbe8ffedbfccff6c0725ff810026ff810027ff810027ff820027ff810027ff810027ff810027ff810027ff8100" +
"27ff810027ff810027ff7f0127ff770425ff6a0a25ff984d61ffe5b1bdffffeef8ffffeaf9ffffc4d6fffeb1c5ffffc7d9ffffecfaffffd5e2ffb47182ff6b182cff6f0420ff7a0323ff7e" +
"0226ff800227ff7d0125ff7c0024ff7f0126ff800227ff810027ff810027ff810027ff810027ff810027ff820027ff820027ff810027ff810027ff7e0226ff780320ff953950ffffedfbff" +
"ba8492ff69071eff7a0122ff781833ffffdbe8ffedbfccff6c0725ff810026ff810027ff810027ff820027ff810027ff810027ff7f0127ff810027ff810027ff810027ff800228ff790325" +
"ff6d0925ffc57d91ffffeaf6fffff3fafff7ced6ff945465ff62142aff610d25ff67182eff985b6affffdae5fffff0fafff3bbc8ff9e5465ff6f162cff690622ff6c0522ff760b29ff8516" +
"36ff7a0225ff7e0226ff7f0127ff820128ff810027ff810027ff810027ff820027ff820027ff810027ff810027ff7e0226ff780320ff953950ffffedfbffba8492ff69071eff7a0120ff78" +
"1931ffffdbe6ffedbfccff6b0825ff7f0126ff810026ff810026ff810027ff810027ff810027ff810027ff7f0029ff810029ff810027ff7c0428ff6e0724ffb1667bfffff2fdfffff5fbff" +
"ffcbd8ff772a3cff670722ff720525ff740526ff720524ff690720ff904559ffffe6f0fffff7fbfffff6faffeac5ccffbf909affb77a89fffab0c1ffaf566cff720522ff7b0326ff800228" +
"ff810027ff810029ff810029ff810029ff810027ff810027ff810027ff810027ff7e0226ff780222ff953950ffffedfbffbc8392ff6a061eff7a0120ff781931ffffdbe6ffedbfcaff6b08" +
"25ff7f0126ff810026ff810026ff810027ff810027ff810027ff810027ff7f0029ff810027ff810027ff750325ff7d2b41ffffdae6fffff8fdfffff2f9ffa35c6eff6b0721ff7b0527ff7f" +
"0127ff7f0127ff7f0327ff7b0424ff680a24ffd3a7b4fffffafdfffffdfbfffffbfafffff4faffffeffaffba808cffa05365ffbd5671ff790222ff7e0026ff810027ff810029ff810029ff" +
"7f0029ff7f0029ff810027ff810027ff7f0127ff7c0326ff780224ff953952ffffedfbffbc8392ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff810027ff810027" +
"ff810027ff810027ff810027ff810027ff810027ff810027ff800026ff6f0624ffbd8090fffff6fbfffffafdfffad6e0ff761e36ff790325ff7e0227ff7f0127ff7f0127ff7f0127ff7e00" +
"25ff6c0725ffa97383fffff7fdfffff9fbfffff8fbffffcddaff8f4d5bff6f3541ffffc5d4ffae4a64ff790222ff7e0026ff820128ff810027ff810027ff810027ff810027ff810027ff81" +
"0027ff7f0127ff7c0326ff780224ff953952ffffedfbffbc8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6c0725ff7f0126ff810027ff810027ff810027ff810027ff810027ff" +
"810027ff810027ff810027ff800227ff6f0c28ffecbec9fffffafbfffffbfdffeec9d1ff731530ff7d0125ff7f0127ff810027ff7f0127ff7f0127ff820128ff6f0625ff9f6574fffff4fd" +
"fffff5faffebc3ccff662938ff6e2e3cffffcdd6ffe6a8b5ff6c0822ff7b0223ff800228ff800026ff810027ff810027ff810027ff810027ff810027ff810027ff7f0127ff7c0326ff7802" +
"24ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdce6ffedbfcaff6c0725ff810026ff810026ff810026ff820128ff810027ff810029ff810029ff820027ff810027ff7d" +
"0125ff7c1e38fffcd7dffffffcfdfffffcfdfff1ced5ff741732ff7b0225ff7f0127ff810027ff810027ff810027ff7f0126ff6d0623ffbb8291fffff1fbfffac0ceff652935ff7a4853ff" +
"fdd2dbfffff2faffb27886ff6e0722ff7f0224ff7e0026ff7f0127ff810029ff810027ff810027ff810027ff810027ff810029ff7f0127ff7c0326ff780224ff953950ffffedfbffba8491" +
"ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6c0725ff810026ff810026ff810026ff810027ff810027ff82012aff810029ff820027ff810027ff7d0125ff82243effffdae2fffffc" +
"fdfffffdfdffffe7efff84354bff730424ff7b0225ff820128ff800026ff810026ff7d0024ff79122dffffc3d2fff2c3cdff6b2f3bffa06873ffffe6ecfffff7fbfffff6fbffc18d9aff6d" +
"0623ff7f0224ff7f0127ff7f0127ff810027ff810027ff810026ff810026ff810029ff810029ff7f0127ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ff" +
"ffdbe6ffeebecaff6b0825ff7f0126ff7f0127ff7f0127ff7f0127ff7f0127ff810027ff810027ff820027ff810027ff7e0025ff761631fff8d0d9fffffcfdfffffbfdfffff7ffffcc8f9f" +
"ff660a23ff770425ff7f0126ff810026ff7f0126ff7c0024ffa33b56fffcb3c4ff7e4b54ffc098a0fffff5fafffffafdfffff9fafffffbfdffe6bcc6ff6e0e29ff7e0123ff800228ff7f01" +
"27ff810027ff810026ff810026ff7f0126ff7f0029ff7f0029ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ffffdae6ffeebecaff6b0825ff7e" +
"0226ff7f0127ff7f0127ff7f0127ff800228ff810027ff820128ff800026ff810027ff800227ff6c0824ffdca8b5fffff9fbfffffafdfffff9feffffecf7ff9e5b6cff680921ff770423ff" +
"7e0226ff7f0126ff7d0125ff811232ff973e54fff5b3c1ffffe1ecfffff5fbfffffafdfffefcfdfffffdfcffffe3eaff872f47ff7a0225ff7d0126ff810027ff810027ff810026ff810026" +
"ff7f0127ff7f0127ff810029ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ffffdae6ffeebecaff6b0825ff7e0226ff7f0127ff810027ff8100" +
"27ff810027ff820027ff810027ff810027ff7f0127ff7f0127ff700726ff9a5567fffff3fbfffffafefffffafcfffffaffffffe7eeff9c5769ff670820ff790325ff7f0127ff810027ff7a" +
"0025ff891c39ffaa4c64ff73263affc995a2fffffafdfffdfdfdfffefefcfffff7fbffa75c70ff720524ff7c0227ff820128ff810027ff810027ff810027ff810027ff810027ff820027ff" +
"820027ff800227ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ffffdbe6ffeebecaff6b0825ff7f0126ff810027ff810027ff810027ff800026ff820027ff820027" +
"ff820128ff7f0127ff7e0026ff760426ff690f29ffe1a4b4fffff5fcfffffafbfffffcfdfffffafdffffecf6ffb57283ff690c27ff730425ff7c0227ff7e0226ff7b0424ff700521ff6208" +
"21ffb87b8bfffff8fdfffdfdfdfffdfdfbfffff9fbffd898a8ff690924ff7a0427ff800028ff800228ff7f0127ff810027ff810027ff810027ff820027ff820027ff7f0126ff780224ff95" +
"3950ffffedfbffba8491ff6a061eff790222ff771933ffffdce6ffedbfcaff6c0725ff810026ff810026ff810026ff810027ff810027ff820027ff820027ff800026ff800026ff810029ff" +
"7c0227ff730425ff7f2740fffacbd5fffff8fafffffafdfffffcfefffffafdfffff5fdffd59fadff712237ff710421ff7c0324ff7d0123ff730623ff6a1b30fff8c8d4fffff9fdfffefdfb" +
"fffdfdfdfffffafdfffdc9d6ff701c33ff760426ff810027ff800026ff800026ff7f0029ff7f0127ff7f0127ff810026ff820027ff7f0126ff780224ff953950ffffedfbffba8491ff6a06" +
"1eff790222ff771933ffffdce6ffedbfcaff6c0725ff810026ff810026ff810026ff810027ff800026ff820027ff820027ff810027ff810027ff810029ff7f0127ff7e0227ff710624ff7f" +
"3546fffbcdd7fffff7fdfffffafdfffdfbfcfffffcfdfffff7fbfff5c3ceff893a4dff6a0620ff760523ff6e0621ffba6f83ffffd5dffffce6e9fffffbfbfffefcfdfffffafcffffebf4ff" +
"894254ff720525ff7f0127ff7a0025ff790125ff7e0026ff7f0127ff7f0127ff810026ff820027ff7f0126ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6" +
"ffedbfcaff6c0725ff7f0126ff810027ff810027ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff7f0127ff7f0127ff7e0026ff7a0225ff700521ff813043fff4c6d0fffff9" +
"fbfffffcfcfffefcfdfffff9fafffff8fdffffdde6ff9b5e6dff600b20ff791d32ffffc6d7ff905865ffd2b1b8fffffafdfffffcfdfffffafdfffff6fdffb17786ff6f0624ff7a0225ffb5" +
"4a68ff861b39ff7b0126ff800026ff810027ff810027ff830127ff7f0126ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff" +
"810027ff810027ff7f0127ff7f0127ff820128ff810027ff800228ff7e0227ff7a0226ff780224ff770123ff770423ff790b28ff73152dff854553ffedc8cffffffafdfffffdfefffefcfd" +
"fffffcfdfffffafdfffff0f6ffb57d8affcf8898ffe598aaff561221ffac7e89fffff7fdfffffbfdfffffcfdfffffafdffdeaebaff6c0c27ff770526ffdb7c94ff902e47ff7a0226ff8200" +
"27ff810027ff810027ff810026ff800227ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdce6ffedbfcaff6b0825ff7f0126ff810027ff810027ff7f0029ff7f" +
"0029ff810027ff800026ff7c0326ff760324ff700725ff912f48ffb95c71ffd4788dffdf839affe38ba3ffda8da1ffe4aab8fffff1f8fffff9fcfffffcfdfffdfdfdfffffbfdfffffbfdff" +
"fff9fbffffdce5ff7a293aff610a1dff874958ffffeef6fffffafefffdfdfdfffffdfdffffdce3ff7a223aff730826ffe88aa2ff7f2038ff790326ff810027ff800026ff810027ff810027" +
"ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff810027ff810027ff7f0029ff7f0127ff7f0127ff7b0327ff7304" +
"24ff9e3553ffdd7993ffdd7e96ffb75a6fff94384dff81233dff761832ff73152fff721e35ff995c6bffffe2eafffffafdfffffcfdfffdfbfcfffffdfefffffffdffffeff3ff9f5b6aff60" +
"0d1fff672332fffed0dafffffafdfffdfdfdfffdfdfbfffff7fdff8e495cffaa4e67ffe88aa4ff6e0724ff7c0428ff810027ff810027ff820128ff7f0127ff7f0328ff780224ff953950ff" +
"ffedfbffba8491ff6a061eff7c0022ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff810027ff810027ff810027ff7d0126ff780225ff790e2cffc66882ffde809affa33856ff760523" +
"ff750221ff750221ff760324ff770326ff770425ff730424ff69051fff964c5dffffe0e9fffffafdfffffbfdfffbfbfbfffefefefffffafdffffdee6ff703642ff53111dffcd9aa3fffffa" +
"fdfffcfcfcfffdfdfdfffffafdffefc4cefff9b0c3ff8f2b47ff780225ff7d0126ff820128ff810027ff810027ff820128ff7e0227ff780224ff953950ffffedfbffba8491ff6a061eff7c" +
"0022ff7a1733ffffdbe6ffedbfcaff6c0725ff7f0126ff810027ff810027ff7f0127ff7a0225ff7b102effe6839fffc56781ff6e0b27ff750223ff7e0226ff7f0126ff7f0126ff7f0127ff" +
"7f0127ff7f0126ff7d0125ff7c0324ff6d0621ff914c5effffdce5fffffafdfffdfbfcfffdfdfdfffffafcfffff8fdfff1c9d1ffae727cffce99a3fffff8fdfffefcfdfffdfdfdfffffbfc" +
"ffffeaf1ff8e4e5eff6e0524ff800228ff7f0127ff810027ff810026ff810026ff810029ff7e0227ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbf" +
"caff6c0725ff7f0126ff810026ff7f0126ff7b0225ff740525ffdc7e98ffc76981ff700523ff790325ff7f0328ff7f0127ff820128ff810027ff810027ff810027ff800026ff7f0127ff7f" +
"0126ff7a0426ff6d0621ff883f50fff9dadffffffdfafffefcfdfffcfcfcfffffbfdfffff8fbfff5c2cbffe9b5c1fffff0f6fffffcfefffdfdfffffffbffffffdde8ff6d283bff730625ff" +
"7f0126ff7f0126ff800227ff810026ff810026ff810029ff7f0328ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6c0725ff7f0126ff810026" +
"ff7c0326ff750324ffad4663ffeb8da7ff6c0925ff790325ff7f0127ff820128ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff800026ff7e0226ff770624ff6b14" +
"27fff4c6d0fffff1f3fffffbfbfffcfefdfffdfffefffffafdffb3818cff531d2affe2c4ccfffffbfffffffcfdfffffbfffffff2fcff9b586aff710624ff800025ff7f0126ff800227ff81" +
"0026ff810027ff810027ff7e0227ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffedbfcaff6c0725ff7f0126ff810026ff790325ff70102bfff79bb4ff" +
"9c3552ff750325ff7e0227ff810027ff800026ff820128ff810027ff810027ff820027ff820027ff810027ff810027ff820027ff7b0326ff6b0922ffcb7d8dffe4a6b5ffc49ca5fffffafd" +
"fffefefefffdfdfdfffff9fdffd99fabff551321ffba909afffff7fdfffffbfefffffbfdfffff7fdffc48e9cff6e0724ff7f0126ff810027ff810027ff810027ff810027ff820128ff7e02" +
"27ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffedbfccff6c0725ff7f0126ff810026ff760425ff943f56fff7a3baff750828ff7f0328ff7f0127ff81" +
"0027ff820128ff810027ff810027ff810027ff820027ff810027ff810027ff810027ff810027ff760425ff853045ffffc9d9ff7c3344ffb57f8cfffff8fcfffdfbfcfffefefefffff7fcff" +
"edb0bfff5b1223ff8e5865fffff4fbfffff9fbfffffcfbfffffbfdffedc2ccff70122cff7d0125ff7e0026ff810027ff810027ff810027ff810027ff7e0226ff780224ff953950ffffedfb" +
"ffba8491ff6a061eff7a0122ff781833ffffdbe8ffedbfccff6c0725ff7f0126ff7f0126ff730424ffbc6a81ffde8fa5ff730625ff7f0126ff810026ff810026ff810027ff810027ff7f01" +
"27ff7f0127ff810029ff810029ff810027ff800228ff7c0326ff6f0624ffea9dafffd08e9cff590c1effc38594fffff7fdfffefcfffffefefefffff9fdfff9bfcdff5c1125ff722f40ffff" +
"dbe6fffffafdfffefdfbfffffdffffffe6efff853047ff760322ff7f0127ff820029ff810027ff810027ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff" +
"781833ffffdbe8ffedbfccff6c0725ff7f0126ff7f0126ff730424ffc6788effcf8197ff710423ff7f0126ff820026ff830127ff800026ff810027ff7e0026ff7f0127ff820128ff810027" +
"ff7f0127ff7b0327ff720524ffa74762ffffc2d5ff6f2637ff5d1020ffd79aa9fffff8fdfffdfdfdfffdfdfdfffff8fdffeeb1c1ff610f25ff631429ffefb5c3fffff9fdfffdfdfdfffffd" +
"fefffff6fdffaa5f73ff710622ff7e0227ff820029ff810027ff810027ff810027ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfccff6c" +
"0725ff7f0126ff810026ff730424ffb96c80ffe598acff730625ff7e0025ff810025ff820026ff810027ff810027ff810027ff810027ff800026ff810027ff7f0127ff760426ff6b112aff" +
"ffb5ccffc1657cff62091dff661d2efff7c8d2fffffafdfffcfefdfffdfffefffff8fdffda97a9ff660c25ff670b22ffc58092fffff7fdfffdfffefffcfcfcfffff7fdffda97a8ff6c0925" +
"ff7b0327ff830028ff810027ff800228ff810026ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810026ff7503" +
"24ff903e54ffffbed3ff7f1835ff790325ff800227ff810026ff820128ff810027ff810027ff820128ff820127ff7e0025ff7f0327ff700523ffba6b80ffffb4c6ff731128ff65091eff99" +
"5d69fffff5fafffffafbfffdfdfbfffefcfdfffff6fdffb4697dff6d0623ff6f0722ff9c4d62fffff5fcfffffdfefffdfdfdfffff9fdffffcad8ff6d182fff770326ff810027ff810027ff" +
"7f0127ff810026ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff790222ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810027ff770326ff6d112affffb1c8ffc36b81" +
"ff6b0721ff790325ff810026ff810027ff810027ff7f0127ff810027ff810027ff7f0126ff770423ff821e38ffffc6d9ff994c5eff66091eff742336fff5cdd5fffffafdfffffcfdfffefc" +
"fdfffffcfdffffe7eeff862e46ff770423ff790325ff7e243dfffedae4fffffdfdfffefdfbfffffbfdffffe9f2ff884054ff710525ff7e0026ff810027ff810027ff820027ff7f0126ff78" +
"0222ff953950ffffedfbffba8491ff6a061eff790222ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810027ff7b0327ff6f0624ffb0546dffffd0e3ff833044ff6c0522ff780526ff" +
"7e0227ff7f0127ff7f0127ff7f0126ff810026ff7b0326ff6e0722ffe0899cffe89eafff560f1fff661c2dffe2a8b6fffff7fdfffffbfcfffdfbfcfffffafdfffff7fdffd39daaff6e0a26" +
"ff7e0226ff7c0024ff6e0b27ffe1b3befffffafdfffffbfafffffafbfffff6fdffac7281ff6e0725ff7e0227ff810027ff810027ff820027ff7f0126ff780224ff953950ffffedfbffba84" +
"91ff6a061eff7a0122ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810027ff7e0227ff790325ff700926ffe093a5ffffcad9ff8c3a50ff670722ff740224ff7d0126ff7d0125ff7e" +
"0123ff7e0123ff730522ff8a3c4cffffc8d4ff722b3bff7a3c4bffe2b8c2fffff7fdfffffafdfffcfcfcfffefcfdfffff9fdffffd2dfff7e2c42ff740525ff7f0126ff7f0126ff6d0623ff" +
"b57c8bfffff7fdfffff9fafffffcfbfffffafdffdaacb7ff6a0723ff7d0125ff810027ff810029ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833" +
"ffffdbe6ffedbfcaff6c0725ff7f0126ff810027ff810027ff7f0126ff760425ff6a122affe79daeffffddf0ffcb7e92ff852b45ff6a0a25ff6a0723ff6a0821ff6b0721ff610e22ffdea9" +
"b3ffd8afb7ffb78590ffffe1ebfffff9fdfffffdfffffefafbfffffbfcfffff8fdffffd6e3ff8f4055ff6f0624ff7a0226ff810027ff810026ff730625ff904b5effffeef5fffffbfdfffe" +
"fcfdfffffdfffffbd7e1ff791d36ff7b0223ff7f0127ff810027ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffedbfcaff6c0725ff" +
"7f0126ff810027ff810027ff810027ff7d0427ff750324ff6f0c28ffd08194ffffebf8ffffe9f4ffe5bac3ffc798a2ffc3919cffc697a1ffd8b0b9fffff4f7fffff1f4fffffafdfffffafd" +
"fffffcfdfffffefffffffafdfffff5fbfff3b9c8ff89374dff6e0523ff7b0326ff7f0127ff820027ff810027ff760426ff752339ffffd0dcfffffafdfffdfdfdfffffeffffffeff7ff9644" +
"5aff750422ff7e0224ff810026ff7f0127ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffeebeccff6c0725ff7f0126ff810027ff810027ff81" +
"0027ff810027ff7e0026ff780526ff680a24ff944b5cffeebac6fffff2fafffff3f8fffff4f8fffff5fafffff9fbfffff9fdfffffafdfffff9fdfffff9fdfffff8fdfffff6fbfffdd3ddff" +
"b57786ff6f152eff710423ff7b0326ff810026ff810026ff800026ff800026ff770527ff620d24ffe1abb9fffff9fdfffefcfdfffefcfdfffff7fdffba7788ff66071dff740321ff7b0223" +
"ff7f0126ff7d0125ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdae8ffeebeccff6c0725ff7f0126ff810026ff810027ff810027ff810027ff810027ff7d01" +
"25ff770425ff700523ff6d0d28ff9a455affca808fffe3abb4ffffe9eeffffe4e9fffdd3ddffffd3dfffffcddafff2b8c6ffda98a6ffaf6274ff7c2037ff6e0523ff760324ff7c0227ff7e" +
"0026ff800025ff820127ff810026ff810027ff740526ff5f0d23ffdeacb8fffffafdfffffefdfffffcfdfffff8fdffffdce6ffb16d7cff9e4156ff7d0c2aff7b0225ff7e0226ff780224ff" +
"953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdae8ffeebeccff6c0725ff7f0126ff810026ff810027ff810027ff810029ff810027ff810027ff810026ff7e0226ff770425" +
"ff6b041fff5c071aff753641ffffe4ecffc29aa3ff7a404eff7b3245ff701c33ff680c23ff690720ff71051fff790222ff7f0126ff810027ff810027ff810027ff810027ff810027ff7f01" +
"26ff790125ff6d0422ff9a4d61ffffe9f6fffff3fbffffedf3fff8d3dbffeabcc7ffe3a9b7ffeea5b6ffdf8098ff831230ff7d0125ff7d0125ff780224ff953950ffffedfbffba8491ff6a" +
"061eff7c0022ff7a1733ffffdbe6ffedbfccff6b0825ff7e0226ff7f0127ff7f0127ff820128ff810027ff820128ff810027ff800026ff7c0326ff720524ff7c2037ffa85f70ffffcad6ff" +
"ffe8f4ffffcedbffeaa1b4ffb85e77ff720725ff790325ff7b0326ff7c0326ff7f0126ff810026ff810027ff800026ff820027ff820027ff810027ff7c0227ff780d2bffc0647dffffcee2" +
"fff8b1c3ffbd7384ff934054ff791d36ff6d0a26ff6e0621ff710421ff750223ff7c0326ff810027ff7f0126ff780224ff953950ffffedfbffba8491ff6a061eff7c0022ff781833ffffdb" +
"e6ffedbfcaff6b0825ff7f0126ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff820027ff7b0225ffa63958fffb9db5ffe08ea4ffa85d71ff873c50ff823046ff862a43ff7d" +
"1432ff7c0326ff810026ff810027ff810027ff810026ff810026ff810027ff810027ff820027ff820027ff810029ff7b0126ff861b39ffd87893ff9d455eff670d26ff6d0520ff740321ff" +
"7a0122ff7d0022ff810026ff810026ff7f0127ff820128ff800026ff7e0026ff780224ff953952ffffedfbffbc8491ff6a061eff7a0124ff771732ffffdbe6ffedbfcaff6c0725ff810026" +
"ff810027ff800026ff810027ff810027ff830028ff810026ff800025ff7d0125ff8f1c3dff932444ff710421ff700320ff710423ff730424ff760324ff7a0225ff7f0126ff810026ff8100" +
"26ff7f0126ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff800228ff7c0227ff780225ff760225ff780427ff7c0427ff7e0226ff7e0226ff7f0126ff810027ff81" +
"0027ff810027ff810027ff810027ff7e0227ff780224ff953952ffffedfbffbc8491ff6a061eff790023ff781833ffffdce6ffedbfcaff6b0723ff810026ff810027ff810027ff810027ff" +
"810027ff820027ff820027ff810027ff7f0126ff790023ff7a0124ff7e0025ff7e0025ff7f0126ff7f0126ff7f0126ff7f0126ff810026ff810026ff810026ff7f0126ff810027ff810027" +
"ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff8100" +
"27ff7e0227ff780224ff953952ffffeefbffbd8592ff6a061eff7a0122ff781833ffffdce7ffeebecaff6c0824ff810026ff810027ff800028ff800028ff82012aff800028ff810027ff81" +
"0027ff810027ff7e0026ff7f0127ff810027ff810027ff810027ff810027ff810027ff810027ff820027ff810027ff810027ff810027ff820027ff820027ff810029ff810029ff820029ff" +
"820029ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff7e0227ff760324ff953950" +
"ffffeefaffbc848fff6a061eff7a0122ff781833ffffd9e3ffecc1caff680a24ff7b0326ff7c0326ff7d0328ff7b0327ff7a0226ff7b0327ff7b0327ff7b0326ff7b0326ff7d0427ff7b03" +
"26ff7c0326ff7b0326ff7c0326ff7a0225ff7b0326ff7b0326ff7c0326ff7c0326ff7c0326ff7b0326ff7c0326ff7c0326ff7c0227ff7b0327ff7c0227ff7c0227ff7c0326ff7b0326ff7c" +
"0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7a0426ff720522ff923a50ffffeefaffbc848fff6a061eff" +
"7a0124ff781833ffffdce5ffe7bfc7ff5c0b1cff6e061fff70051fff6f041eff6d051eff6c061eff6d051eff6e061fff6c041dff6c041dff6e031dff6d051eff6f041eff6e061fff6f041e" +
"ff6d051eff6d051eff6d051eff6f041eff6d051eff6d051eff6d051eff6f041eff6d051eff6d051eff6d051eff6f041eff6f041eff6f041eff6d051eff6f041eff6d051eff6d051eff6d05" +
"1eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6c061eff68061dff823444fffff0f8ffbc848fff6a061eff7a0124ff781833ffffd5dffffd" +
"d9ddffaf707bffbd6c7dffbf6c7effbe6b7dffbc6b7cffbb6d7dffbc6b7cffbd6c7dffbd6c7dffbe6b7bffbe6b7dffbe6b7dffbe6b7dffbd6c7dffbe6b7dffbd6c7dffbd6c7dffbd6c7dff" +
"be6b7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbe6b7dffbe6b7dffbe6b7dffbd6c7dffbd6c7dffbd6c7dffbe6b7dffbd6c7dffbd6c7d" +
"ffbd6c7dffbe6b7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffb86b7bffcc8a98ffffeff8ffbe828eff6d071fff7c0024ff7c1834ffffd5e0fffff4f8fffff1f8ffffeef7ffffef" +
"f8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff6ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffff" +
"eff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ff" +
"ffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeffaffffedf9ffc98292ff6e031fff7e0025ff790c2bffab5e70ffb07480ffad737fffad7580ffab737effad737fffad737fffad737f" +
"ffad737fffae7480ffad7580ffac747dffad737fffad737fffac747fffac747fffac747fffac747fffac747fffac747fffad737fffad737fffad737fffac747fffac747fffac747fffac74" +
"7fffac747fffac747fffac747fffac747fffac747fffad737fffad737fffac747fffac747fffad737fffad737fffac747fffac747fffad737fffad737fffac747fffac747fffac747fffac" +
"747fffad7580ffb0727fffb87181ff933b51ff750324ff810027ff7c0326ff710421ff6c0520ff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff69071eff" +
"69081cff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6c061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061e" +
"ff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a071cff6c061cff6f04" +
"1eff750422ff7c0326";

// Dialog box title.
var strBoxTitle = curDoc.info.Author + "\r\n" + curDoc.info.Title;

// Determine if a company has been provided to customize the information.  If a company name is provided, include the name in the text.
if (curDoc.info.preparedFor != "")
{
    var strPrepared = "This document prepared exclusively for " + curDoc.info.preparedFor.toUpperCase() + ".";
}
else
{
    var strPrepared = "";
    var prepHeight = 0;
};

// Display the coordinator's name
if (curDoc.info.CoordName != "")
{
    var strCoordName = curDoc.info.CoordName + " is responsible for the controlled distribution of this document.";
}
else
{
    var strCoordName = "";
    var recpHeight = 0;
};

// Display the coordinator's phone information
if (curDoc.info.CoordPhone != "")
{
    var strCoordPhone = util.printf("%s", curDoc.info.CoordPhone);
}
else
{
    var strCoordPhone = "";
    var phoneHeight = 0;
};

// Display the coordinator's e-mail address information
if (curDoc.info.CoordEMail != "")
{
    var strCoordEMail = curDoc.info.CoordEMail;
}
else
{
    var strCoordEMail = "";
    var eMailHeight = 0;
};

// Display the expiration date
try {
    var dtExpiry = util.scand(forExpiry, strExpiry);
    var pFormat = "dddd, mmmm d, yyyy";
    var strExpiresOn = "This document expires on " + util.printd(pFormat, dtExpiry) + ".\r\n\r\nPlease contact Solomon for more information.";
}
catch (e) {
    var strExpiresOn = "";
    var exprHeight = 0;
};

// the main agreement text; this appears in the multi-line text box.                 
var strAgreement =   "This document and its contents are the proprietary property of HSB Solomon Associates LLC (Solomon)." +
            "\r\n" + "It is provided to you for reference purposes only." +
        "\r\n\r\n" + "Without express written permission of Solomon, this material may not be:" +
            "\r\n" + "\u2022 Disclosed to consultants, contractors, or other third parties for any purpose, or" +
            "\r\n" + "\u2022 Copied, duplicated, or reproduced in whole or in part in any way." +
        "\r\n\r\n" + "I will contact my company's legal advisor and Solomon Study Coordinator if I become aware of any of the prohibited activities." +
        "\r\n\r\n" + "To agree with the foregoing and view the document, click 'Accept'.";

// Set the copyright date using date modified.  The unicode text "\u00A9" creates the Circle-C copyright symbol.
// var strCopyright = "\u00A9 " + curDoc.modDate.getFullYear() + " " + curDoc.info.Author;
var strCopyright = curDoc.info.cRight;
    
// Determine if "Accept" is clicked.
var boolAccept = 0;

// Set the dialog box width.
var numWidth = 51;

// Allow only this version or later of Reader to open the document.
var docVersion = 7;

var ClickThrough =
{
    DoDialog: function () { return app.execDialog(this); },

    initialize: function (dialog)
    {
        var initDialog =
        {
            "img1":
            {
                offset: 0,
                "width": 53,
                "height": 57,
                "read": function (bytes) { return SALogo.slice(this.offset, this.offset += bytes); }
            },

            // Populate the 'name' parameter for these static_text elements.
            "prep": strPrepared,
            "recp": strCoordName,
            "phne": strCoordPhone,
            "mail": strCoordEMail,
            "sta1": strAgreement,
            "copy": strCopyright,
            "expr": strExpiresOn
        };

        dialog.load(initDialog);

    },

    commit: function (dialog) {
        boolAccept = 1;
        if ((navToPage != "") && (navToPage >= 0) && (navToPage <= curDoc.numPages - 1)) { curDoc.pageNum = navToPage };
    },

    destroy: function (dialog) {
        if (boolAccept != 1)
        {
            app.alert("You must accept the agreement to view this document.  This document will now close.");
            curDoc.closeDoc(true);
        };
    },

    description:
        {
        name: "HSB Solomon Associates LLC",
        first_tab: "btn1",
        elements:
            [
            {
            type: "view",
            char_width: numWidth,
            elements:
                [
                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    type: "image",
                    item_id: "img1",
                    height: 57,
                    width: 53
                    },
                    {
                    type: "static_text",
                    item_id: "tit0",
                    font: "dialog",
                    bold: true,
                    char_height: 4,
                    name: strBoxTitle
                    }
                    ]
                },

                {
                type: "static_text",
                item_id: "prep",
                alignment: "align_fill",
                font: "dialog",
                bold: true,
                char_height: prepHeight
                },

                {
                type: "static_text",
                item_id: "recp",
                alignment: "align_fill",
                font: "dialog",
                bold: true,
                char_height: recpHeight
                },

                {
                type: "static_text",
                item_id: "phne",
                alignment: "align_fill",
                font: "dialog",
                char_height: phoneHeight
                },

                {
                type: "static_text",
                item_id: "mail",
                alignment: "align_fill",
                font: "dialog",
                char_height: eMailHeight
                },

                {
                type: "static_text",
                item_id: "sta1",
                alignment: "align_fill",
                multiline: "true",
                char_height: 25
                },

                {
                type: "static_text",
                item_id: "expr",
                alignment: "align_fill",
                font: "dialog",
                char_height: exprHeight
                },

                {
                type: "static_text",
                item_id: "copy",
                alignment: "align_fill",
                font: "dialog",
                bold: true,
                char_height: 2
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Version " + strVersion,
                    type: "static_text",
                    char_width: numWidth - 18
                    },
                    {
                    type: "ok_cancel",
                    item_id: "btn1",
                    ok_name: "&Accept",
                    cancel_name: "&Decline"
                    }
                    ]
                }
                ]
            }
            ]
        }
};

// Verify Reader version is current enough for the PDF
if  (   (app.viewerVersion < docVersion) &&
    (
        (app.viewerType == "Reader") ||
        (app.viewerType == "Exchange") ||
        (app.viewerType == "Exchange-Pro")
    )
    )
{
    app.alert("This document requires Adobe Acrobat " + docVersion + ".0 or greater.  This document will now close.");
    curDoc.closeDoc(true);
}
else
{
    if (boolExpired == 0)
    {
        if (curDoc.numPageViews != 1)
        {
        ClickThrough.DoDialog();
        };
        curDoc.numPageViews = 1;
    };
};
