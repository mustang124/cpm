
// Copyright � 2012 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

// Saves the PDF with password protection.

if (typeof strVersion == "undefined") var strVersion = "1.0.6";

///////////////////////////////////////////////////////////////////////////////////////////////////
// Functions

// Public function to add security policy to a document.
if (typeof ProtectDocument == "undefined")
function ProtectDocument(sPolicyName)
{

    if (typeof curDoc == "undefined") var curDoc = this;

    // List of Policies:
    // Solomon Policy Encryption
    // Solomon Password Policy Encryption

    ProtectDoc(curDoc, sPolicyName);

};


///////////////////////////////////////////////////////////////////////////////////////////////////
// Trusted Functions

if (typeof ProtectDoc == "undefined")
var ProtectDoc = app.trustedFunction(function (curDoc, sPolicyName)
{
    app.beginPriv();

    var oMyPolicy = null;
    var aPols = security.getSecurityPolicies();

    for (var i = 0; i < aPols.length; i++)
    {
        if (aPols[i].name == sPolicyName)
        {
            oMyPolicy = aPols[i];
            break;
        }
    }

    if (oMyPolicy != null)
    {
        var rtn = curDoc.encryptUsingPolicy({ oPolicy: oMyPolicy });
    }

    app.endPriv();

}
);
