

// Copyright � 2011 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240
//
// Programming Notes:
// � Place this JS in: C:\Program Files\Adobe\Acrobat X.0\Acrobat\Javascripts\
// � Code is case sensitive
//
// � 'IF' Statements:
//      ==   Equal to
//      !=   Not equal to
//      &&   And
//      ||   Or
//
// � 'Special characters'
//      \r   carriage return
//      \n   new line
//      \t   tab

var strVersion = "1.0.2"                   // 2011.08.01

var colorSolomon = ["RGB", 0.5625, 0.0, 0.15625];           // RGB(144, 0, 40) - For screen
//var colorSolomon = ["RGB", 0.64453125, 0.0, 0.12890625];    // RGB(165, 0, 33) - For print

function addSolWatermark()
{
    var curDoc = this;

    var strWaterMark = curDoc.info.WaterMarkType;

    if (typeof strWaterMark != "undefined")
    {

        var pageBeg = 2;

        switch(strWaterMark)
        {
            case "Electronic: Single Side Printing":
                addSolMark(curDoc, pageBeg, "Left", 0.57, 0.985, "Left", 0.182, 0.845, false);
                break;

            case "Print: Analysis or Methodology":
                addSolMark(curDoc, pageBeg, "Both", 1.39, 1.679, "Left", 0.182, 0.845, false);
                break;

            case "Print: Data Tables":
                addSolMark(curDoc, pageBeg, "Right", 10.342, 1.000, "Right", 7.842, 1.000, true);
                break;

            case "No WaterMark":
                console.println("No WaterMark type defined for '" + curDoc.documentFileName + "'");
                break;
           
            default:
                console.println("Improper WaterMark type '" + curDoc.info.DocType + "' specified for '" + curDoc.documentFileName + "'\r\nVerify SolomonUI " + strVersion + ".js and WaterMark " + strVersion + ".js");
                break;
        };
    }
    else
    {
        console.println("WaterMarkType is undefined for '" + curDoc.documentFileName + "'");
    };

};

// pageBeg      -
// sideTrigP    - Portrait pages; trigger for watermark alignment ("Right", "Left", or "Other")
// pTop         -
// pMargin      -
// sideTrigL    - Landscape pages; trigger for watermark alignment ("Right", "Left", or "Other")
// lTop         -
// lMargin      -
// bDataTable   -

function addSolMark(curDoc, pageBeg, sideTrigP, pTop, pMargin, sideTrigL, lTop, lMargin, bDataTable)
{

    if (curDoc.numPages >= pageBeg)
    {
        if (curDoc.info.Refnum != "")
        {
            var strPreparedFor = curDoc.info.Client + " (" + curDoc.info.Refnum + ")"
        }
        else
        {
            var strPreparedFor = curDoc.info.Client
        };

        var strPrepared = "Prepared for " + strPreparedFor;

        // Initialize a status window and progress bar
        var wBar = app.thermometer;

        // Display a status window and progress bar
        wBar.duration = curDoc.numPages - pageBeg + 1;
        wBar.begin();

        for (pageWm = pageBeg - 1; pageWm < curDoc.numPages; pageWm++)
        {

            // Indicate the current company 
            wBar.text = "Adding watermark to page " + pageWm + ".";

            if (curDoc.getPageNumWords(pageWm) > 0)
            {
                switch (curDoc.getPageRotation(pageWm))
                {
                    case 0: {addWmPortrait(curDoc, pageWm, strPrepared, sideTrigP, -pTop * 72.0, pMargin * 72.0, bDataTable); break;};
                    case 90: {addWmLandscape(curDoc, pageWm, strPrepared, sideTrigL, - lTop * 72.0, lMargin * 72.0, bDataTable); break;};
                };

                if (bDataTable == true)
                {
                    var vVertValue = 10.342;
                    var vMargin = 1.000;
                    addDataTableFooter(curDoc, pageWm, "Proprietary and Confidential", - vVertValue * 72.0, vMargin * 72.0)
                };
                
            };

            // Step the progress bar
            wBar.value = pageWm - pageBeg + 1;

        };

        // Close progress bar
        wBar.end();
        
    };
};

function addDataTableFooter(curDoc, pageWm, strText, vVertValue, vMargin)
{

    var vVertValue;
    var vMargin;

    var vFont = font.Helv;
    var vSize = 7;
    
    switch (curDoc.getPageRotation(pageWm)) {
        case 0: 
        {
            vVertValue = - 10.342 * 72.0;
            vMargin = 1.000 * 72.0;
            break;
        };
        case 90:
        {
            vVertValue = -7.842 * 72.0;
            vMargin = 1.000 * 72.0;
            break;
        };
    };

    curDoc.addWatermarkFromText
    (
    {
        cText: strText,
        nTextAlign: app.constants.align.left,
        cFont: vFont,
        nFontSize: vSize,
        aColor: colorSolomon,
        nStart: pageWm,
        bOnTop: true,
        bOnScreen: true,
        bOnPrint: true,
        nHorizAlign: app.constants.align.left,
        nVertAlign: app.constants.align.top,
        nHorizValue: vMargin,
        nVertValue: vVertValue,
        bPercentage: false,
        nScale: 1.0,
        bFixedPrint: false,
        nRotation: 0,
        nOpacity: 1.0
    }
    );
};

function addWmLandscape(curDoc, pageWm, strPrepared, sideTrigL, vVertValue, vMargin, bDataTable)
{

    var vFont;
    var vSize;

    if(bDataTable == true)
    {
        vFont = font.Helv;
        vSize = 7;
    }
    else
    {
        vFont = font.Helv;
        vSize = 7;
    };

    if ((sideTrigL == "Left") || ((pageWm % 2 == 0) && (sideTrigL == "Both")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.left, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.left, 
            nVertAlign: app.constants.align.top,
            nHorizValue: vMargin,
            nVertValue: vVertValue,
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        }
        );
    };

    if ((sideTrigL == "Right") || ((pageWm % 2 == 1) && (sideTrigL == "Both")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.right, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.right, 
            nVertAlign: app.constants.align.top,
            nHorizValue: - vMargin,                     // Points
            nVertValue: vVertValue,                     // Points
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        }
        );
    };
};

function addWmPortrait(curDoc, pageWm, strPrepared, sideTrigP, vVertValue, vMargin, bDataTable)
{

    var vFont;
    var vSize;

    if(bDataTable == true)
    {
        vFont = font.Helv;
        vSize = 7;
    }
    else
    {
        vFont = font.TimesI;
        vSize = 10;
    };

    if ((sideTrigP == "Left") || ((pageWm % 2 == 0) && (sideTrigP == "Both")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.left, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.left, 
            nVertAlign: app.constants.align.top,
            nHorizValue: vMargin,
            nVertValue: vVertValue,
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        }
        );
    };

    if ((sideTrigP == "Right") || ((pageWm % 2 == 1) && (sideTrigP == "Both")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.right, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.right, 
            nVertAlign: app.constants.align.top,
            nHorizValue: - vMargin,
            nVertValue: vVertValue,
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        }
        );
    };
};