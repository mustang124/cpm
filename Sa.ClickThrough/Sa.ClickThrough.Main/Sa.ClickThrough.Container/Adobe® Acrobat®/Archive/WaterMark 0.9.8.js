

// Copyright � 2009 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

var strVersion = "0.9.8"                   // 2009.07.22 08:22

var colorSolomon = ["RGB", 0.5625, 0.0, 0.15625];           // RGB(144, 0, 40) - For screen
var colorSolomon = ["RGB", 0.64453125, 0.0, 0.12890625];    // RGB(165, 0, 33) - For print

// addSolWatermark(1, "Left", .57, .985, "Left", .182, .845) 2008 Gray Book

function addSolWatermark(pageBeg, sideTrigP, pTop, pMargin, sideTrigL, lTop, lMargin)
{
    var curDoc = this;
    
    if (curDoc.numPages >= pageBeg)
    {
        if (curDoc.info.Refnum != "")
        {
            var strPreparedFor = curDoc.info.Client + " (" + curDoc.info.Refnum + ")"
        }
        else
        {
            var strPreparedFor = curDoc.info.Client
        };

        var strPrepared = "Prepared for " + strPreparedFor;

        // Initialize a status window and progress bar
        var wBar = app.thermometer;

        // Display a status window and progress bar
        wBar.duration = curDoc.numPages - pageBeg + 1;
        wBar.begin();

        for (pageWm = pageBeg - 1; pageWm < curDoc.numPages; pageWm++)
        {

            // Indicate the current company 
            wBar.text = "Adding watermark to page " + pageWm + ".";

            switch (curDoc.getPageRotation(pageWm))
            {
                case 0:  {addWmPortrait (curDoc, pageWm, strPrepared, sideTrigP, - pTop * 72, pMargin * 72); break;};
                case 90: {addWmLandscape(curDoc, pageWm, strPrepared, sideTrigL, - lTop * 72, lMargin * 72); break;};
            };
            
            // Step the progress bar
            wBar.value = pageWm - pageBeg + 1;

        };

        // Close progress bar
        wBar.end();
        
    };
};

function addWmLandscape(curDoc, pageWm, strPrepared, sideTrigL, vVertValue, vMargin)
{
    var vFont = font.Helv;
    var vSize = 7;

    if ((sideTrigL == "Left") || ((pageWm % 2 == 1) && (sideTrigL != "Right")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.left, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.left, 
            nVertAlign: app.constants.align.top,
            nHorizValue: vMargin,
            nVertValue: vVertValue,
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        });
    };
    
    if ((sideTrigL == "Right") || ((pageWm % 2 == 0) && (sideTrigL != "Left")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.right, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.right, 
            nVertAlign: app.constants.align.top,
            nHorizValue: - vMargin,                     // Points
            nVertValue: vVertValue,                     // Points
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        });
    };
};

function addWmPortrait(curDoc, pageWm, strPrepared, sideTrigP, vVertValue, vMargin)
{
    var vFont = font.TimesI;
    var vSize = 10;

    if ((sideTrigP == "Left") || ((pageWm % 2 == 1) && (sideTrigP != "Right")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.left, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.left, 
            nVertAlign: app.constants.align.top,
            nHorizValue: vMargin,
            nVertValue: vVertValue,
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        });
    };

    if ((sideTrigP == "Right") || ((pageWm % 2 == 0) && (sideTrigP != "Left")))
    {
        curDoc.addWatermarkFromText(
        {
            cText: strPrepared, 
            nTextAlign: app.constants.align.right, 
            cFont: vFont, 
            nFontSize: vSize, 
            aColor: colorSolomon, 
            nStart: pageWm, 
            bOnTop: true,
            bOnScreen: true,
            bOnPrint: true, 
            nHorizAlign: app.constants.align.right, 
            nVertAlign: app.constants.align.top,
            nHorizValue: - vMargin,
            nVertValue: vVertValue,
            bPercentage: false, 
            nScale: 1.0, 
            bFixedPrint: false, 
            nRotation: 0,
            nOpacity: 1.0
        });
    };
};