

// Copyright © 2009 HSB Solomon Associates LLC
// Solomon Associates
// Two Galleria Tower
// 13455 Noel Road, Suite 1500
// Dallas, Texas 75240

// Programming Notes:
// • Place this JS in: C:\Program Files\Adobe\Acrobat X.0\Acrobat\Javascripts\
// • Item id must be no more then 4 characters
// • Code is case sensitive
//
// • 'IF' Statements:
//      ==   Equal to
//      !=   Not equal to
//      &&   And
//      ||   Or
//
// • 'Special characters'
//      \r   carriage return
//      \n   new line
//      \t   tab

var strVersion = "0.9.6"                   // 2009.06.31 08:22
var strFileCT = "ClickThrough 0.9.6.js"
var strDelimRecord = "\r\n"
var strDelimField = "|"
var txtCharset = "utf-8" //utf-8, utf-16, Shift-JIS, BigFive, GBK, and UHC
var jsCharset = "utf-8" //utf-8, utf-16, Shift-JIS, BigFive, GBK, and UHC

// Icon stream for button
var strIcon = 
"00d1d1d100d0d2cf00d0d2d100d1d1cf00d5d1d200d1d3d000d9d7d800d5d7d400dad8db00d9d9db00d8d8da00d5d4d200dadada00d9d9db00d8d8d600d5d5d700d9d7d800d8d8d600d1d1d300d1d1d1" + 
"00d1d1d300d1d3d000d1d1d100d1d1d300ced2d1b4d1d1d1ffd3cfd0ffd2a39d00d1d2cd00d1d1d1b4d3cfceffc2b9ba00d0d2d100d1d1d100d0d0ce80bcb2b300d2d0d100d1d3d200d2d0d100d2d1cf" + 
"00d3d1d200d1d1d300d0d0d200d1d1cf00d0d0ceffd3d1d2ff9d8488ffcb7775ffd16e6900d3d1d4ffd5a196ffa48f96ffc1bcc000d1d1d1ff8b7b88ffb7a7aaffe6c0bd00d1d1d100d1d1d100d1d1d1" + 
"00d1d1d100d1d1cf00d3d2d000ced3cf00d1d1d1ffd2d0d1ffb3a9b2ffcbccceffdacacdffd4a7a4ffceabafffcac6c5ffb5b6baffaeb1b6ffbcbcbeffe0cbc8ffdacfcd00d1d1d100d1d1d300d1d1cf" + 
"00cfd3d400d3d3d1ffc8cdd1ffc2c7caffc3c7c8ffa9afafffc6ccccff8b9191ffdce6e7fff4f8f9fffcfdfffffffefffffafeffffb9c6ccffb8b9beffcd7c79b4d0cfcd00d2d0d100d1d0ce00d1d1d3" + 
"00d0d0d0ffcfd3d4ff9fa4a8ff54554fff656e69ff818b8dff67706dff757a74fff0f2f1fffffffffffdfffefffffffffff6f7fbffbdc7d1ffb4b7bcffbc2824ffc9aaaf80c6cdd500d1d1d100d0d1d3" + 
"00d3d1d2ffd0d1d5ff8f99a2ff7b8a8dff707d85ff94a3a6ff676d6dff969a9dfffcfcfafffffefffffefefcfffcfffffffcfdffffbecbd4ffb4b7c0ffbd2f2dffcfadae80c9d5d500d1d1d100d1d1d1" + 
"00d1d1d1ffcdd2d5ff97a7b6ffaabfd0ff83949eff6c7675ffafb3b6ffbbc0c6ffd8dcddffd3d6dbffd8d6d9ffd2d3d7ffc5c9ccff98a2aeff9a99a7ffbe2f2bffc4a7ac80cdd6db00d1d1d100d1d1d1" + 
"00d1d1d300d1d3d0ffd4d7deffdfe2e7ffedeef2fff2f3f5ffeff0f4fff0eff4ffebecf0ffeaebedffefecf3fff3f1f6ffd9dee2ff9bacb6ff827f90ffb92a24ffb2959780bec8ca00d1d1d100d1d1d1" + 
"00d2d1cf00d2d0d1ffd8dedefff6fafdfffbfafffffcfdfffffbfffffffefdfffffdfdfdfff8f9fbfff9f7fafffcfafdffdfe8edffa4b5bdff8b8d99ffba2924ffd5b9b880c1c9cc00d2d2d200d0d0d0" + 
"00d1d1cf00d2d0d3ffdadbddfff9f9fbfffafbfffffdfffefffefffafffefdfbfffefaf9fffbfbfdfffcf8f7fffefafbffe0e8ebffa9b7c0ff938992ffc22b24ffd7beba80c4ccce00d2d0d100d1d1d1" + 
"00d1d1cf00d1d3d2ffd6dbdffff5f5fdfffafbfdfffbfcfefffefdfbfffefcfdfffcfefdfffcfcfefffcfafdfffffcffffe4e8ebffacb8c6ff998793ffc02d26ffd9c4c180c5cacd00d2d0d100d1d1d1" + 
"00d0d1d300d2d2d2ffd2d9dfffeff6fcfff3f7fafff3f8fbfffafafafffbfdfcfffefefefffefefefffcfefdfffefffdffe3e9e9ffaabac7ff928694ffbb2e27ffd1c1c180c2cacc00d1d1d100d1d1d1" + 
"00d2d0d300d0d2d1ffced9ddffecf5fafff2f6fffff1f9fcfffbfcfefffbfffffffdfdfdfffcfdfffffefefcfffefffdffe6e7ebffabb9c4ff968190ffc13129ffcbc1c280bfc7ca00d1d1d100d1d1d1" + 
"00d2d0d100d1d3d2ffd0d7dfffebf6faffe9ecf3ffeee9e6fff2fbfafff1e3e3fffbfffefff1e2dffffafefdfffffdfeffe2e7edffa2b6c1ff98808effbe302cffc2bdc380bcc6c800d1d1d100d1d1d1" + 
"00d0d2d100d1d1d3ffcfd8e1ffebfdfdffdcb5b8ffe78c8bffebbfbeffe5a09bfff4d2d1ffe38b89fff3eae5fff5e3e3ffdce4e6ff9eb4c1ff957d8affc0312dffbab7c080bac4c600d1d1d100d1d1d1" + 
"00d1d1d100d1d1cfffc9d6dfffe6fbffffdc9792ffda645affe08b8effdd7e7affe48f8cffe06d66ffdf8483ffdc6f68ffd0bdc3ff9bb5c4ff977583ffc2312effafb2c180c2c7cd00d0d2d100d1d1cf" + 
"00d1d1d100d2d4d3ffc6d2deffd6edfbffd6a8a8ffda9295ffdc7672ffdf7577ffe48c8affe37673ffe27572ffe27c78ffc8c5ceff94aebbff947280ffbb2a2dffa0aab333cbccd000d0d2d100d1d1d1" + 
"00d1d1d100d0d2cd80cfd3d4ffb4bfc1ffbbc3ceffb1c7d4ffbcb3b8ffb7afbeffb8a3acffbb9499ffc8aab6ffc98b96ffbbbecfff8c9fadff8e818affb7989600cfcfcd00d1d1d100d2d0d100d1d1d1" + 
"00d1d3d200d1d1d324d2d0d348cfd1d06cd1d1d190d1d1d1b4d0d1d3d8d0d0ceffccd0d1ffcaced1ffc6c9ceffbfcaceffbdc2c8ffbec2c500cecfd100d1d1d100d1d1d100d1d1d100d1d1d100d0d2d1" ;

// Icon variable for the button
var imgIcon = 
{
    offset: 0,
    "width": 20,
    "height": 20,
    "read": function(bytes){return strIcon.slice(this.offset,this.offset+=bytes);}
};

// Add the toolbar button
app.addToolButton({oIcon: imgIcon, cName: "bPreparePDF" + strVersion, cLabel: "Prepare PDF " + strVersion, cTooltext: "Prepare PDFs for Client Distribution", cExec: "fPreparePDF()", cEnable: true, nPos: 0});


///////////////////////////////////////////////////////////////////////////////////////////////////
// User Interface Dialog Box.
function fPreparePDF()
{
    try
    {
        // Select the current active document to process
        var curDoc = this;
    
        // Set the document title
        var solTitle = curDoc.info.Title;
        var docTitle = curDoc.info.Title;

        // Set the inital author.
        var solAuthor = "HSB Solomon Associates LLC";
        var solWatermark = "Do NOT distribute this document.";
        var docAuthor = curDoc.info.Author;
        var strBaseURL = "http://www.SolomonOnline.com/";

        // Set the copyright notice using the date modified.
        var strCopyright = "© " + curDoc.creationDate.getFullYear();
        var strCopyrightURL = "http://www.SolomonOnline.com/";
        var bolCopyright = "True";
        
        // Calculate the expiry date; 4 years from the current date
        var expYear = 1 * util.printd("yyyy",  new Date()) + 4
        expYear = util.printd("mm/dd/",  new Date()) + expYear;
        
        // Set the navigate to page
        var solToPage = "3"
        var docToPage = "1"
        
    }
    catch(e)
    {
        // Catch the error if no document is open.
        // This value is used when notifying the user that a document must be open.
        var err = e;
    };

    // Set the dialog box title
    var dialogTitle = "HSB Solomon Associates LLC";
    var boxTitle = dialogTitle + "\r\nCopyright and Click-through Agreement for PDF Documents";

    // Set the instructions for applying pdf information
    var strJavaScript = "<Select a PDF Open event JavaScript file>";
    var strClientList = "<Select a TXT file with the client list>";

    // Set other values...
    var boolAccept = 0;
    var lblWidth = 15;
    var txtWidth = 25;
    var btnWidth = 6;
    var numWidth = 8;
    var dateFormat = "mm/dd/yyyy";
        
    // Company count...
    var countCo;
    
    // Single Recipient
    var strRecipient;
    
    // Path to the expiry JavaScript; fully qualified, does not rely on mapping.
    var strPathCT = "/Dallas1/DATA/COMMONP/SA ClickThrough Agreement/Adobe® Acrobat®/" + strFileCT

    // Set the Solomon Logo
    var SALogo =  
    "ff810026ff7f0126ff7d0022ff7c0022ff7a0122ff7a0122ff7d0024ff7c0022ff7a0122ff790220ff7c0022ff7c0022ff7a0122ff7a0122ff7c0022ff7c0022ff7a0122ff7a0122ff7c00" +
    "24ff7a0122ff7a0122ff790220ff7c0022ff7c0022ff7a0122ff7a0122ff7c0120ff7c0120ff7a0124ff7a0124ff7c0022ff7c0022ff7a0120ff7a0120ff7c0022ff7c0022ff7a0124ff7a" +
    "0124ff7c0022ff7c0022ff7a0120ff7a0120ff7d0022ff7c0022ff7c0022ff7a0122ff7d0022ff7c0022ff7c0022ff7a0122ff7c0021ff7e0123ff810027ff810026ff7c0427ff7c142fff" +
    "771830ff761830ff761830ff771732ff771732ff751930ff751930ff761830ff761830ff761832ff761830ff771732ff761830ff761830ff761830ff771732ff761832ff751930ff751930" +
    "ff761830ff761830ff761832ff761830ff771830ff761830ff761832ff761832ff771732ff771830ff761830ff761830ff771830ff761832ff761832ff761832ff771732ff761830ff7618" +
    "30ff761830ff771732ff771732ff761832ff761830ff771732ff771830ff761832ff76172fff7d1631ff790c29ff7c0326ff7e0025ff7b1230ffecaebdfffcd3dbfffed6defffed6deffff" +
    "d7e0fffed6defffcd7defffcd7defffed6defffed6defffed6dffffcd7defffed6dffffed6defffed6defffcd7defffed6defffed6defffcd7defffcd7defffcd7defffed6defffed6dfff" +
    "fed6defffed6defffed6defffed6defffed6deffffd6deffffd6defffed6defffcd7defffed6defffed6dffffed6dffffcd7dffffed6dffffed6defffed6defffed6defffed6dffffed6df" +
    "fffed6dffffed6defffed6dffffed6defffed6dfffffd4ddffffced9ffb76a7cff71051fff7c0024ff771732ffffd9e2fffff8faffffe6ebffffe5ecffffe4edffffe5ecffffe6ecffffe6" +
    "ecffffe4edffffe4edffffe5edffffe5edffffe5edffffe5ecffffe5ecffffe5ecffffe5ecffffe5ecffffe5edffffe5edffffe5edffffe5edffffe5edffffe4edffffe4edffffe5ecffff" +
    "e5ecffffe5ecffffe4ecffffe4ecffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edffffe4edffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edffffe5edff" +
    "ffe5edffffe5ecffffe9effffff3faffbf8591ff6c061cff7a0122ff781931ffffdae3ffecc9cfff712839ff7f263cff82263fff7f253eff7f253eff7f263cff82243eff81253eff7f253e" +
    "ff7f253eff81253eff81253eff81253eff7f263cff7f263cff81253cff81253eff81253eff81253eff81253eff81253eff81253eff81253eff81253eff81253eff7f263cff81253cff7f26" +
    "3cff7f253eff7f253eff81253eff81253eff7f253eff7f253eff81253eff81253eff81253eff81253eff81253eff7f253eff7f253eff7f253eff81253eff7f253eff7a253cff9a5365ffff" +
    "f0faffbc8491ff69051dff7a0122ff781931ffffdce6ffe9bec7ff670923ff780121ff790124ff790124ff790124ff7a0124ff7a0124ff7a0124ff790124ff790124ff7a0124ff7a0124ff" +
    "7a0124ff790124ff790124ff790124ff7a0124ff7a0124ff7a0124ff7a0124ff790124ff790124ff7a0124ff7a0124ff790124ff790222ff790124ff790222ff790124ff790124ff7a0124" +
    "ff7a0124ff790124ff790124ff7a0124ff7a0124ff7a0124ff7a0122ff790124ff790222ff790124ff790124ff7a0124ff790124ff710320ff8e354bffffedfaffbd8592ff690720ff7a01" +
    "22ff791a32ffffdce6ffedbfc9ff690924ff7e0226ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7e" +
    "0227ff7f0127ff810027ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7e0226ff7f0127ff7f0126ff7f0127ff7f0127ff7f0127ff7f0127ff7e0227ff7e0227ff" +
    "7f0127ff7f0127ff7f0127ff7f0126ff7f0127ff7e0226ff7e0227ff7e0227ff7f0127ff7e0226ff760322ff953950ffffedfaffbc8491ff69071eff7a0122ff771732ffffdbe7ffeebeca" +
    "ff6b0824ff800227ff820128ff820027ff810027ff810027ff810027ff810027ff810027ff810027ff820029ff820029ff810029ff810029ff7f0127ff7f0127ff810027ff7f0126ff7c03" +
    "26ff7b0326ff7b0326ff7b0326ff7e0226ff7e0227ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff810027ff810027ff7f0127ff7f0127ff810027ff810027ff810027ff81" +
    "0027ff810027ff810027ff7f0127ff810027ff810027ff7f0126ff780222ff953950ffffedfbffba8491ff6a081fff7a0122ff781833ffffdbe6ffeebecaff6b0824ff7f0126ff810026ff" +
    "820026ff810027ff810027ff810029ff810029ff810027ff810027ff810029ff800028ff810027ff810027ff7f0127ff7e0227ff7d0125ff760020ff6f0420ff6a0620ff6b0721ff6d0520" +
    "ff740321ff790124ff7d0125ff7f0126ff810026ff820026ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff820029ff810029ff810029ff7f0029ff8100" +
    "27ff810027ff810027ff7e0226ff780222ff953950ffffedfbffba8491ff69071eff7a0122ff781833ffffdbe6ffedbfccff6c0725ff810026ff810026ff820026ff810027ff810027ff81" +
    "0029ff810029ff810027ff810027ff810029ff810027ff810027ff7e0227ff770326ff700523ff6c0925ff90374dffbb667bffd48195ffde869cffcb7389ffab4f66ff7d1d38ff6e0523ff" +
    "750324ff7c0326ff7f0126ff810027ff810027ff7f0127ff7f0127ff800026ff810027ff810027ff810027ff820029ff810029ff810029ff810027ff810027ff810027ff810027ff7e0226" +
    "ff780222ff953950ffffedfbffba8492ff69071eff7a0122ff781833ffffdbe8ffedbfccff6c0725ff810026ff810027ff810027ff820027ff810027ff810027ff810027ff810027ff8100" +
    "27ff810027ff810027ff7f0127ff770425ff6a0a25ff984d61ffe5b1bdffffeef8ffffeaf9ffffc4d6fffeb1c5ffffc7d9ffffecfaffffd5e2ffb47182ff6b182cff6f0420ff7a0323ff7e" +
    "0226ff800227ff7d0125ff7c0024ff7f0126ff800227ff810027ff810027ff810027ff810027ff810027ff820027ff820027ff810027ff810027ff7e0226ff780320ff953950ffffedfbff" +
    "ba8492ff69071eff7a0122ff781833ffffdbe8ffedbfccff6c0725ff810026ff810027ff810027ff820027ff810027ff810027ff7f0127ff810027ff810027ff810027ff800228ff790325" +
    "ff6d0925ffc57d91ffffeaf6fffff3fafff7ced6ff945465ff62142aff610d25ff67182eff985b6affffdae5fffff0fafff3bbc8ff9e5465ff6f162cff690622ff6c0522ff760b29ff8516" +
    "36ff7a0225ff7e0226ff7f0127ff820128ff810027ff810027ff810027ff820027ff820027ff810027ff810027ff7e0226ff780320ff953950ffffedfbffba8492ff69071eff7a0120ff78" +
    "1931ffffdbe6ffedbfccff6b0825ff7f0126ff810026ff810026ff810027ff810027ff810027ff810027ff7f0029ff810029ff810027ff7c0428ff6e0724ffb1667bfffff2fdfffff5fbff" +
    "ffcbd8ff772a3cff670722ff720525ff740526ff720524ff690720ff904559ffffe6f0fffff7fbfffff6faffeac5ccffbf909affb77a89fffab0c1ffaf566cff720522ff7b0326ff800228" +
    "ff810027ff810029ff810029ff810029ff810027ff810027ff810027ff810027ff7e0226ff780222ff953950ffffedfbffbc8392ff6a061eff7a0120ff781931ffffdbe6ffedbfcaff6b08" +
    "25ff7f0126ff810026ff810026ff810027ff810027ff810027ff810027ff7f0029ff810027ff810027ff750325ff7d2b41ffffdae6fffff8fdfffff2f9ffa35c6eff6b0721ff7b0527ff7f" +
    "0127ff7f0127ff7f0327ff7b0424ff680a24ffd3a7b4fffffafdfffffdfbfffffbfafffff4faffffeffaffba808cffa05365ffbd5671ff790222ff7e0026ff810027ff810029ff810029ff" +
    "7f0029ff7f0029ff810027ff810027ff7f0127ff7c0326ff780224ff953952ffffedfbffbc8392ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff810027ff810027" +
    "ff810027ff810027ff810027ff810027ff810027ff810027ff800026ff6f0624ffbd8090fffff6fbfffffafdfffad6e0ff761e36ff790325ff7e0227ff7f0127ff7f0127ff7f0127ff7e00" +
    "25ff6c0725ffa97383fffff7fdfffff9fbfffff8fbffffcddaff8f4d5bff6f3541ffffc5d4ffae4a64ff790222ff7e0026ff820128ff810027ff810027ff810027ff810027ff810027ff81" +
    "0027ff7f0127ff7c0326ff780224ff953952ffffedfbffbc8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6c0725ff7f0126ff810027ff810027ff810027ff810027ff810027ff" +
    "810027ff810027ff810027ff800227ff6f0c28ffecbec9fffffafbfffffbfdffeec9d1ff731530ff7d0125ff7f0127ff810027ff7f0127ff7f0127ff820128ff6f0625ff9f6574fffff4fd" +
    "fffff5faffebc3ccff662938ff6e2e3cffffcdd6ffe6a8b5ff6c0822ff7b0223ff800228ff800026ff810027ff810027ff810027ff810027ff810027ff810027ff7f0127ff7c0326ff7802" +
    "24ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdce6ffedbfcaff6c0725ff810026ff810026ff810026ff820128ff810027ff810029ff810029ff820027ff810027ff7d" +
    "0125ff7c1e38fffcd7dffffffcfdfffffcfdfff1ced5ff741732ff7b0225ff7f0127ff810027ff810027ff810027ff7f0126ff6d0623ffbb8291fffff1fbfffac0ceff652935ff7a4853ff" +
    "fdd2dbfffff2faffb27886ff6e0722ff7f0224ff7e0026ff7f0127ff810029ff810027ff810027ff810027ff810027ff810029ff7f0127ff7c0326ff780224ff953950ffffedfbffba8491" +
    "ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6c0725ff810026ff810026ff810026ff810027ff810027ff82012aff810029ff820027ff810027ff7d0125ff82243effffdae2fffffc" +
    "fdfffffdfdffffe7efff84354bff730424ff7b0225ff820128ff800026ff810026ff7d0024ff79122dffffc3d2fff2c3cdff6b2f3bffa06873ffffe6ecfffff7fbfffff6fbffc18d9aff6d" +
    "0623ff7f0224ff7f0127ff7f0127ff810027ff810027ff810026ff810026ff810029ff810029ff7f0127ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ff" +
    "ffdbe6ffeebecaff6b0825ff7f0126ff7f0127ff7f0127ff7f0127ff7f0127ff810027ff810027ff820027ff810027ff7e0025ff761631fff8d0d9fffffcfdfffffbfdfffff7ffffcc8f9f" +
    "ff660a23ff770425ff7f0126ff810026ff7f0126ff7c0024ffa33b56fffcb3c4ff7e4b54ffc098a0fffff5fafffffafdfffff9fafffffbfdffe6bcc6ff6e0e29ff7e0123ff800228ff7f01" +
    "27ff810027ff810026ff810026ff7f0126ff7f0029ff7f0029ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ffffdae6ffeebecaff6b0825ff7e" +
    "0226ff7f0127ff7f0127ff7f0127ff800228ff810027ff820128ff800026ff810027ff800227ff6c0824ffdca8b5fffff9fbfffffafdfffff9feffffecf7ff9e5b6cff680921ff770423ff" +
    "7e0226ff7f0126ff7d0125ff811232ff973e54fff5b3c1ffffe1ecfffff5fbfffffafdfffefcfdfffffdfcffffe3eaff872f47ff7a0225ff7d0126ff810027ff810027ff810026ff810026" +
    "ff7f0127ff7f0127ff810029ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ffffdae6ffeebecaff6b0825ff7e0226ff7f0127ff810027ff8100" +
    "27ff810027ff820027ff810027ff810027ff7f0127ff7f0127ff700726ff9a5567fffff3fbfffffafefffffafcfffffaffffffe7eeff9c5769ff670820ff790325ff7f0127ff810027ff7a" +
    "0025ff891c39ffaa4c64ff73263affc995a2fffffafdfffdfdfdfffefefcfffff7fbffa75c70ff720524ff7c0227ff820128ff810027ff810027ff810027ff810027ff810027ff820027ff" +
    "820027ff800227ff780224ff953950ffffedfbffba8491ff6a061eff7a0120ff781931ffffdbe6ffeebecaff6b0825ff7f0126ff810027ff810027ff810027ff800026ff820027ff820027" +
    "ff820128ff7f0127ff7e0026ff760426ff690f29ffe1a4b4fffff5fcfffffafbfffffcfdfffffafdffffecf6ffb57283ff690c27ff730425ff7c0227ff7e0226ff7b0424ff700521ff6208" +
    "21ffb87b8bfffff8fdfffdfdfdfffdfdfbfffff9fbffd898a8ff690924ff7a0427ff800028ff800228ff7f0127ff810027ff810027ff810027ff820027ff820027ff7f0126ff780224ff95" +
    "3950ffffedfbffba8491ff6a061eff790222ff771933ffffdce6ffedbfcaff6c0725ff810026ff810026ff810026ff810027ff810027ff820027ff820027ff800026ff800026ff810029ff" +
    "7c0227ff730425ff7f2740fffacbd5fffff8fafffffafdfffffcfefffffafdfffff5fdffd59fadff712237ff710421ff7c0324ff7d0123ff730623ff6a1b30fff8c8d4fffff9fdfffefdfb" +
    "fffdfdfdfffffafdfffdc9d6ff701c33ff760426ff810027ff800026ff800026ff7f0029ff7f0127ff7f0127ff810026ff820027ff7f0126ff780224ff953950ffffedfbffba8491ff6a06" +
    "1eff790222ff771933ffffdce6ffedbfcaff6c0725ff810026ff810026ff810026ff810027ff800026ff820027ff820027ff810027ff810027ff810029ff7f0127ff7e0227ff710624ff7f" +
    "3546fffbcdd7fffff7fdfffffafdfffdfbfcfffffcfdfffff7fbfff5c3ceff893a4dff6a0620ff760523ff6e0621ffba6f83ffffd5dffffce6e9fffffbfbfffefcfdfffffafcffffebf4ff" +
    "894254ff720525ff7f0127ff7a0025ff790125ff7e0026ff7f0127ff7f0127ff810026ff820027ff7f0126ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6" +
    "ffedbfcaff6c0725ff7f0126ff810027ff810027ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff7f0127ff7f0127ff7e0026ff7a0225ff700521ff813043fff4c6d0fffff9" +
    "fbfffffcfcfffefcfdfffff9fafffff8fdffffdde6ff9b5e6dff600b20ff791d32ffffc6d7ff905865ffd2b1b8fffffafdfffffcfdfffffafdfffff6fdffb17786ff6f0624ff7a0225ffb5" +
    "4a68ff861b39ff7b0126ff800026ff810027ff810027ff830127ff7f0126ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff" +
    "810027ff810027ff7f0127ff7f0127ff820128ff810027ff800228ff7e0227ff7a0226ff780224ff770123ff770423ff790b28ff73152dff854553ffedc8cffffffafdfffffdfefffefcfd" +
    "fffffcfdfffffafdfffff0f6ffb57d8affcf8898ffe598aaff561221ffac7e89fffff7fdfffffbfdfffffcfdfffffafdffdeaebaff6c0c27ff770526ffdb7c94ff902e47ff7a0226ff8200" +
    "27ff810027ff810027ff810026ff800227ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdce6ffedbfcaff6b0825ff7f0126ff810027ff810027ff7f0029ff7f" +
    "0029ff810027ff800026ff7c0326ff760324ff700725ff912f48ffb95c71ffd4788dffdf839affe38ba3ffda8da1ffe4aab8fffff1f8fffff9fcfffffcfdfffdfdfdfffffbfdfffffbfdff" +
    "fff9fbffffdce5ff7a293aff610a1dff874958ffffeef6fffffafefffdfdfdfffffdfdffffdce3ff7a223aff730826ffe88aa2ff7f2038ff790326ff810027ff800026ff810027ff810027" +
    "ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff810027ff810027ff7f0029ff7f0127ff7f0127ff7b0327ff7304" +
    "24ff9e3553ffdd7993ffdd7e96ffb75a6fff94384dff81233dff761832ff73152fff721e35ff995c6bffffe2eafffffafdfffffcfdfffdfbfcfffffdfefffffffdffffeff3ff9f5b6aff60" +
    "0d1fff672332fffed0dafffffafdfffdfdfdfffdfdfbfffff7fdff8e495cffaa4e67ffe88aa4ff6e0724ff7c0428ff810027ff810027ff820128ff7f0127ff7f0328ff780224ff953950ff" +
    "ffedfbffba8491ff6a061eff7c0022ff781833ffffdbe6ffedbfcaff6b0825ff7f0126ff810027ff810027ff810027ff7d0126ff780225ff790e2cffc66882ffde809affa33856ff760523" +
    "ff750221ff750221ff760324ff770326ff770425ff730424ff69051fff964c5dffffe0e9fffffafdfffffbfdfffbfbfbfffefefefffffafdffffdee6ff703642ff53111dffcd9aa3fffffa" +
    "fdfffcfcfcfffdfdfdfffffafdffefc4cefff9b0c3ff8f2b47ff780225ff7d0126ff820128ff810027ff810027ff820128ff7e0227ff780224ff953950ffffedfbffba8491ff6a061eff7c" +
    "0022ff7a1733ffffdbe6ffedbfcaff6c0725ff7f0126ff810027ff810027ff7f0127ff7a0225ff7b102effe6839fffc56781ff6e0b27ff750223ff7e0226ff7f0126ff7f0126ff7f0127ff" +
    "7f0127ff7f0126ff7d0125ff7c0324ff6d0621ff914c5effffdce5fffffafdfffdfbfcfffdfdfdfffffafcfffff8fdfff1c9d1ffae727cffce99a3fffff8fdfffefcfdfffdfdfdfffffbfc" +
    "ffffeaf1ff8e4e5eff6e0524ff800228ff7f0127ff810027ff810026ff810026ff810029ff7e0227ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbf" +
    "caff6c0725ff7f0126ff810026ff7f0126ff7b0225ff740525ffdc7e98ffc76981ff700523ff790325ff7f0328ff7f0127ff820128ff810027ff810027ff810027ff800026ff7f0127ff7f" +
    "0126ff7a0426ff6d0621ff883f50fff9dadffffffdfafffefcfdfffcfcfcfffffbfdfffff8fbfff5c2cbffe9b5c1fffff0f6fffffcfefffdfdfffffffbffffffdde8ff6d283bff730625ff" +
    "7f0126ff7f0126ff800227ff810026ff810026ff810029ff7f0328ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfcaff6c0725ff7f0126ff810026" +
    "ff7c0326ff750324ffad4663ffeb8da7ff6c0925ff790325ff7f0127ff820128ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff800026ff7e0226ff770624ff6b14" +
    "27fff4c6d0fffff1f3fffffbfbfffcfefdfffdfffefffffafdffb3818cff531d2affe2c4ccfffffbfffffffcfdfffffbfffffff2fcff9b586aff710624ff800025ff7f0126ff800227ff81" +
    "0026ff810027ff810027ff7e0227ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffedbfcaff6c0725ff7f0126ff810026ff790325ff70102bfff79bb4ff" +
    "9c3552ff750325ff7e0227ff810027ff800026ff820128ff810027ff810027ff820027ff820027ff810027ff810027ff820027ff7b0326ff6b0922ffcb7d8dffe4a6b5ffc49ca5fffffafd" +
    "fffefefefffdfdfdfffff9fdffd99fabff551321ffba909afffff7fdfffffbfefffffbfdfffff7fdffc48e9cff6e0724ff7f0126ff810027ff810027ff810027ff810027ff820128ff7e02" +
    "27ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffedbfccff6c0725ff7f0126ff810026ff760425ff943f56fff7a3baff750828ff7f0328ff7f0127ff81" +
    "0027ff820128ff810027ff810027ff810027ff820027ff810027ff810027ff810027ff810027ff760425ff853045ffffc9d9ff7c3344ffb57f8cfffff8fcfffdfbfcfffefefefffff7fcff" +
    "edb0bfff5b1223ff8e5865fffff4fbfffff9fbfffffcfbfffffbfdffedc2ccff70122cff7d0125ff7e0026ff810027ff810027ff810027ff810027ff7e0226ff780224ff953950ffffedfb" +
    "ffba8491ff6a061eff7a0122ff781833ffffdbe8ffedbfccff6c0725ff7f0126ff7f0126ff730424ffbc6a81ffde8fa5ff730625ff7f0126ff810026ff810026ff810027ff810027ff7f01" +
    "27ff7f0127ff810029ff810029ff810027ff800228ff7c0326ff6f0624ffea9dafffd08e9cff590c1effc38594fffff7fdfffefcfffffefefefffff9fdfff9bfcdff5c1125ff722f40ffff" +
    "dbe6fffffafdfffefdfbfffffdffffffe6efff853047ff760322ff7f0127ff820029ff810027ff810027ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff" +
    "781833ffffdbe8ffedbfccff6c0725ff7f0126ff7f0126ff730424ffc6788effcf8197ff710423ff7f0126ff820026ff830127ff800026ff810027ff7e0026ff7f0127ff820128ff810027" +
    "ff7f0127ff7b0327ff720524ffa74762ffffc2d5ff6f2637ff5d1020ffd79aa9fffff8fdfffdfdfdfffdfdfdfffff8fdffeeb1c1ff610f25ff631429ffefb5c3fffff9fdfffdfdfdfffffd" +
    "fefffff6fdffaa5f73ff710622ff7e0227ff820029ff810027ff810027ff810027ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffedbfccff6c" +
    "0725ff7f0126ff810026ff730424ffb96c80ffe598acff730625ff7e0025ff810025ff820026ff810027ff810027ff810027ff810027ff800026ff810027ff7f0127ff760426ff6b112aff" +
    "ffb5ccffc1657cff62091dff661d2efff7c8d2fffffafdfffcfefdfffdfffefffff8fdffda97a9ff660c25ff670b22ffc58092fffff7fdfffdfffefffcfcfcfffff7fdffda97a8ff6c0925" +
    "ff7b0327ff830028ff810027ff800228ff810026ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810026ff7503" +
    "24ff903e54ffffbed3ff7f1835ff790325ff800227ff810026ff820128ff810027ff810027ff820128ff820127ff7e0025ff7f0327ff700523ffba6b80ffffb4c6ff731128ff65091eff99" +
    "5d69fffff5fafffffafbfffdfdfbfffefcfdfffff6fdffb4697dff6d0623ff6f0722ff9c4d62fffff5fcfffffdfefffdfdfdfffff9fdffffcad8ff6d182fff770326ff810027ff810027ff" +
    "7f0127ff810026ff7e0226ff780222ff953950ffffedfbffba8491ff6a061eff790222ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810027ff770326ff6d112affffb1c8ffc36b81" +
    "ff6b0721ff790325ff810026ff810027ff810027ff7f0127ff810027ff810027ff7f0126ff770423ff821e38ffffc6d9ff994c5eff66091eff742336fff5cdd5fffffafdfffffcfdfffefc" +
    "fdfffffcfdffffe7eeff862e46ff770423ff790325ff7e243dfffedae4fffffdfdfffefdfbfffffbfdffffe9f2ff884054ff710525ff7e0026ff810027ff810027ff820027ff7f0126ff78" +
    "0222ff953950ffffedfbffba8491ff6a061eff790222ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810027ff7b0327ff6f0624ffb0546dffffd0e3ff833044ff6c0522ff780526ff" +
    "7e0227ff7f0127ff7f0127ff7f0126ff810026ff7b0326ff6e0722ffe0899cffe89eafff560f1fff661c2dffe2a8b6fffff7fdfffffbfcfffdfbfcfffffafdfffff7fdffd39daaff6e0a26" +
    "ff7e0226ff7c0024ff6e0b27ffe1b3befffffafdfffffbfafffffafbfffff6fdffac7281ff6e0725ff7e0227ff810027ff810027ff820027ff7f0126ff780224ff953950ffffedfbffba84" +
    "91ff6a061eff7a0122ff781833ffffdbe6ffeebecaff6c0725ff7f0126ff810027ff7e0227ff790325ff700926ffe093a5ffffcad9ff8c3a50ff670722ff740224ff7d0126ff7d0125ff7e" +
    "0123ff7e0123ff730522ff8a3c4cffffc8d4ff722b3bff7a3c4bffe2b8c2fffff7fdfffffafdfffcfcfcfffefcfdfffff9fdffffd2dfff7e2c42ff740525ff7f0126ff7f0126ff6d0623ff" +
    "b57c8bfffff7fdfffff9fafffffcfbfffffafdffdaacb7ff6a0723ff7d0125ff810027ff810029ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833" +
    "ffffdbe6ffedbfcaff6c0725ff7f0126ff810027ff810027ff7f0126ff760425ff6a122affe79daeffffddf0ffcb7e92ff852b45ff6a0a25ff6a0723ff6a0821ff6b0721ff610e22ffdea9" +
    "b3ffd8afb7ffb78590ffffe1ebfffff9fdfffffdfffffefafbfffffbfcfffff8fdffffd6e3ff8f4055ff6f0624ff7a0226ff810027ff810026ff730625ff904b5effffeef5fffffbfdfffe" +
    "fcfdfffffdfffffbd7e1ff791d36ff7b0223ff7f0127ff810027ff810027ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffedbfcaff6c0725ff" +
    "7f0126ff810027ff810027ff810027ff7d0427ff750324ff6f0c28ffd08194ffffebf8ffffe9f4ffe5bac3ffc798a2ffc3919cffc697a1ffd8b0b9fffff4f7fffff1f4fffffafdfffffafd" +
    "fffffcfdfffffefffffffafdfffff5fbfff3b9c8ff89374dff6e0523ff7b0326ff7f0127ff820027ff810027ff760426ff752339ffffd0dcfffffafdfffdfdfdfffffeffffffeff7ff9644" +
    "5aff750422ff7e0224ff810026ff7f0127ff7e0226ff780224ff953950ffffedfbffba8491ff6a061eff790222ff771933ffffdbe6ffeebeccff6c0725ff7f0126ff810027ff810027ff81" +
    "0027ff810027ff7e0026ff780526ff680a24ff944b5cffeebac6fffff2fafffff3f8fffff4f8fffff5fafffff9fbfffff9fdfffffafdfffff9fdfffff9fdfffff8fdfffff6fbfffdd3ddff" +
    "b57786ff6f152eff710423ff7b0326ff810026ff810026ff800026ff800026ff770527ff620d24ffe1abb9fffff9fdfffefcfdfffefcfdfffff7fdffba7788ff66071dff740321ff7b0223" +
    "ff7f0126ff7d0125ff780224ff953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdae8ffeebeccff6c0725ff7f0126ff810026ff810027ff810027ff810027ff810027ff7d01" +
    "25ff770425ff700523ff6d0d28ff9a455affca808fffe3abb4ffffe9eeffffe4e9fffdd3ddffffd3dfffffcddafff2b8c6ffda98a6ffaf6274ff7c2037ff6e0523ff760324ff7c0227ff7e" +
    "0026ff800025ff820127ff810026ff810027ff740526ff5f0d23ffdeacb8fffffafdfffffefdfffffcfdfffff8fdffffdce6ffb16d7cff9e4156ff7d0c2aff7b0225ff7e0226ff780224ff" +
    "953950ffffedfbffba8491ff6a061eff7a0122ff781833ffffdae8ffeebeccff6c0725ff7f0126ff810026ff810027ff810027ff810029ff810027ff810027ff810026ff7e0226ff770425" +
    "ff6b041fff5c071aff753641ffffe4ecffc29aa3ff7a404eff7b3245ff701c33ff680c23ff690720ff71051fff790222ff7f0126ff810027ff810027ff810027ff810027ff810027ff7f01" +
    "26ff790125ff6d0422ff9a4d61ffffe9f6fffff3fbffffedf3fff8d3dbffeabcc7ffe3a9b7ffeea5b6ffdf8098ff831230ff7d0125ff7d0125ff780224ff953950ffffedfbffba8491ff6a" +
    "061eff7c0022ff7a1733ffffdbe6ffedbfccff6b0825ff7e0226ff7f0127ff7f0127ff820128ff810027ff820128ff810027ff800026ff7c0326ff720524ff7c2037ffa85f70ffffcad6ff" +
    "ffe8f4ffffcedbffeaa1b4ffb85e77ff720725ff790325ff7b0326ff7c0326ff7f0126ff810026ff810027ff800026ff820027ff820027ff810027ff7c0227ff780d2bffc0647dffffcee2" +
    "fff8b1c3ffbd7384ff934054ff791d36ff6d0a26ff6e0621ff710421ff750223ff7c0326ff810027ff7f0126ff780224ff953950ffffedfbffba8491ff6a061eff7c0022ff781833ffffdb" +
    "e6ffedbfcaff6b0825ff7f0126ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff820027ff7b0225ffa63958fffb9db5ffe08ea4ffa85d71ff873c50ff823046ff862a43ff7d" +
    "1432ff7c0326ff810026ff810027ff810027ff810026ff810026ff810027ff810027ff820027ff820027ff810029ff7b0126ff861b39ffd87893ff9d455eff670d26ff6d0520ff740321ff" +
    "7a0122ff7d0022ff810026ff810026ff7f0127ff820128ff800026ff7e0026ff780224ff953952ffffedfbffbc8491ff6a061eff7a0124ff771732ffffdbe6ffedbfcaff6c0725ff810026" +
    "ff810027ff800026ff810027ff810027ff830028ff810026ff800025ff7d0125ff8f1c3dff932444ff710421ff700320ff710423ff730424ff760324ff7a0225ff7f0126ff810026ff8100" +
    "26ff7f0126ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff800228ff7c0227ff780225ff760225ff780427ff7c0427ff7e0226ff7e0226ff7f0126ff810027ff81" +
    "0027ff810027ff810027ff810027ff7e0227ff780224ff953952ffffedfbffbc8491ff6a061eff790023ff781833ffffdce6ffedbfcaff6b0723ff810026ff810027ff810027ff810027ff" +
    "810027ff820027ff820027ff810027ff7f0126ff790023ff7a0124ff7e0025ff7e0025ff7f0126ff7f0126ff7f0126ff7f0126ff810026ff810026ff810026ff7f0126ff810027ff810027" +
    "ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff7f0127ff7f0127ff810027ff810027ff810027ff810027ff8100" +
    "27ff7e0227ff780224ff953952ffffeefbffbd8592ff6a061eff7a0122ff781833ffffdce7ffeebecaff6c0824ff810026ff810027ff800028ff800028ff82012aff800028ff810027ff81" +
    "0027ff810027ff7e0026ff7f0127ff810027ff810027ff810027ff810027ff810027ff810027ff820027ff810027ff810027ff810027ff820027ff820027ff810029ff810029ff820029ff" +
    "820029ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff810027ff7e0227ff760324ff953950" +
    "ffffeefaffbc848fff6a061eff7a0122ff781833ffffd9e3ffecc1caff680a24ff7b0326ff7c0326ff7d0328ff7b0327ff7a0226ff7b0327ff7b0327ff7b0326ff7b0326ff7d0427ff7b03" +
    "26ff7c0326ff7b0326ff7c0326ff7a0225ff7b0326ff7b0326ff7c0326ff7c0326ff7c0326ff7b0326ff7c0326ff7c0326ff7c0227ff7b0327ff7c0227ff7c0227ff7c0326ff7b0326ff7c" +
    "0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7b0326ff7a0426ff720522ff923a50ffffeefaffbc848fff6a061eff" +
    "7a0124ff781833ffffdce5ffe7bfc7ff5c0b1cff6e061fff70051fff6f041eff6d051eff6c061eff6d051eff6e061fff6c041dff6c041dff6e031dff6d051eff6f041eff6e061fff6f041e" +
    "ff6d051eff6d051eff6d051eff6f041eff6d051eff6d051eff6d051eff6f041eff6d051eff6d051eff6d051eff6f041eff6f041eff6f041eff6d051eff6f041eff6d051eff6d051eff6d05" +
    "1eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6d051eff6c061eff68061dff823444fffff0f8ffbc848fff6a061eff7a0124ff781833ffffd5dffffd" +
    "d9ddffaf707bffbd6c7dffbf6c7effbe6b7dffbc6b7cffbb6d7dffbc6b7cffbd6c7dffbd6c7dffbe6b7bffbe6b7dffbe6b7dffbe6b7dffbd6c7dffbe6b7dffbd6c7dffbd6c7dffbd6c7dff" +
    "be6b7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbe6b7dffbe6b7dffbe6b7dffbd6c7dffbd6c7dffbd6c7dffbe6b7dffbd6c7dffbd6c7d" +
    "ffbd6c7dffbe6b7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffbd6c7dffb86b7bffcc8a98ffffeff8ffbe828eff6d071fff7c0024ff7c1834ffffd5e0fffff4f8fffff1f8ffffeef7ffffef" +
    "f8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff6ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffff" +
    "eff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeff8ff" +
    "ffeff8ffffeff8ffffeff8ffffeff8ffffeff8ffffeffaffffedf9ffc98292ff6e031fff7e0025ff790c2bffab5e70ffb07480ffad737fffad7580ffab737effad737fffad737fffad737f" +
    "ffad737fffae7480ffad7580ffac747dffad737fffad737fffac747fffac747fffac747fffac747fffac747fffac747fffad737fffad737fffad737fffac747fffac747fffac747fffac74" +
    "7fffac747fffac747fffac747fffac747fffac747fffad737fffad737fffac747fffac747fffad737fffad737fffac747fffac747fffad737fffad737fffac747fffac747fffac747fffac" +
    "747fffad7580ffb0727fffb87181ff933b51ff750324ff810027ff7c0326ff710421ff6c0520ff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff69071eff" +
    "69081cff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6c061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061e" +
    "ff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a061eff6a071cff6c061cff6f04" +
    "1eff750422ff7c0326";
        
    var prepareDocument =
    {
        DoDialog: function()
        {
            return app.execDialog(this);
        },

        initialize: function(dialog)
        {
            // Load default Expiry JavaScript 'strPathCT'
            try
            {
                // load Exipry JavaScript
                tfAddAttachmentPath(curDoc, "jsFile", strPathCT);
                
                // Show file name in static_text box
                dialog.load({"JaSc": curDoc.getDataObject("jsFile").path});
            }
            catch(e){};
            
            var initDialog =
            {
            
                "img1": 
                {
                    "width": 53,
                    "height": 57,
                    offset: 0,
                    "read": function(bytes){return SALogo.slice(this.offset,this.offset+=bytes);}
                },
                
                "bTop": boxTitle,
                "file": curDoc.documentFileName,
                "titl": docTitle,
                "auth": solAuthor,
                "copy": strCopyright + " " + solAuthor,
                "pdfE": expYear,
                "navP": solToPage
                
            };
            
            dialog.load(initDialog);

        },

        description:
        {
            name: dialogTitle,
            first_tab: "titl",
            align_children: "align_row",        
            elements:
            [
                {
                type: "view",
                align_children: "align_row",
                alignment: "align_fill",
                elements:
                    [
                    {
                    type: "image",
                    item_id: "img1",
                    height: 57,
                    width: 53,
                    },
                    {
                    item_id: "bTop",
                    type: "static_text",
                    alignment: "align_fill",
                    bold: true,
                    font: "dialog",
                    char_height: 3
                    }
                    ]
                },
                
                {
                type: "gap"
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "File name:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "file",
                    type: "static_text",
                    char_width: txtWidth
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Document Title:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "titl",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "auth"
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Document Author:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "auth",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "copy"
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "PDF Copyright:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "copy",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "pdfE"
                    }
                    ]
                },
                
                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "PDF Expiry Date:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "pdfE",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "btnD"
                    },
                    {
                    name: "(" + dateFormat + ")",
                    type: "static_text",
                    char_width: btnWidth
                    }
                    ]
                },


                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Open to Page:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "navP",
                    type: "edit_text",
                    char_width: numWidth,
                    next_tab: "btnD"
                    }
                    ]
                },


                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    type: "static_text",
                    char_width: lblWidth
                    },
                    {
                    name: "Document",
                    item_id: "btnD",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "btnS"
                    },
                    {
                    name: "Solomon",
                    item_id: "btnS",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "btn3"
                    }
                    ]
                },

                {
                type: "gap"
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Click-through (*.js):",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    name: strJavaScript,
                    item_id: "JaSc",
                    type: "static_text",
                    char_width: txtWidth,
                    },
                    {
                    name: "Select file",
                    item_id: "btn3",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "Cnme"
                    }
                    ]
                },
                
                {
                type: "gap"
                },
                
                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Create recipient:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "Cnme",
                    type: "static_text",
                    char_width: txtWidth
                    },
                    {
                    name: "Create",
                    item_id: "btnR",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "btn4"
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Client list (*.txt):",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    name: strClientList,
                    item_id: "Comp",
                    type: "static_text",
                    char_width: txtWidth,
                    },
                    {
                    name: "Select file",
                    item_id: "btn4",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "btnP" 
                    }
                    ]
                },

                {
                type: "gap"
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Version " + strVersion,
                    type: "static_text",
                    char_width: lblWidth + txtWidth - btnWidth - 2
                    },
                    {
                    name: "Process",
                    item_id: "btnP",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "btnC" 
                    },
                    {
                    name: "Cancel",
                    item_id: "btnC",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "titl" 
                    }
                    ]
                }
            ]
        },
        
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // UI Contols

        // Change the copyright noticed based on the author
        auth: function(dialog) {dialog.load({"copy": strCopyright + " " + dialog.store()["auth"]});},

        // Set strTitle when a new title is typed
        titl: function(dialog) {solTitle = dialog.store()["titl"];},
        
        // Set navToPage when a new page to navigate to is typed
        navP: function(dialog) {docToPage = dialog.store()["navP"];},
        
        // Set 
        Cnme: function(dialog) {
        
            var strText = 
            {
                "Comp": strClientList
            };

            dialog.load(strText);            
        
        },
       
        // Reset fields to document
        btnD: function(dialog)
        {
            var strText = 
            {
                "titl": docTitle,
                "auth": docAuthor,
                "copy": strCopyright + " " + docAuthor,
                "pdfE": expYear,
                "navP": docToPage
            };

            dialog.load(strText);

        },

        // Set fields to Solomon Standard
        btnS: function(dialog)
        {
            //var y = 1 * util.printd("yyyy",  new Date()) + expiryYears;
            
            var strText = 
            {
                "titl": solTitle,
                "auth": solAuthor,
                "copy": strCopyright + " " + solAuthor,
                "pdfE": expYear,
                "navP": solToPage
            };

            dialog.load(strText);

        },

        // Click-through JavaScript File
        btn3: function(dialog)
        {
            // Prompt the user for a data file to embed.
            tfAddAttachment(curDoc, "jsFile");

            // Show file name in static_text box
            dialog.load({"JaSc": curDoc.getDataObject("jsFile").path});

            var aPath = dialog.store()["JaSc"].split(".");
            if (aPath[aPath.length - 1] != "js")
            {
                app.alert("Select a JavaScript (*.js) file!");
                dialog.load({"JaSc": strJavaScript});
            };
        },
       
        // Company List
        btn4: function(dialog)
        {
            // Clear company name
            var strText = 
            {
                "Cnme": ""
            };

            dialog.load(strText);
            
            // Prompt the user for a company file to embed.
            tfAddAttachment(curDoc, "coFile");

            // Show file name in static_text box
            dialog.load({"Comp": curDoc.getDataObject("coFile").path});
            
            var aPath = dialog.store()["Comp"].split(".");
            if (aPath[aPath.length - 1] != "txt")
            {
                app.alert("Select a Text (*.txt) file!");
                dialog.load({"Comp": strClientList});
            };

        },
        
        // Create Recipient
        btnR: function(dialog)
        {
            strRecipient = dialog.store()["Cnme"]
            prepareRecipient.DoDialog();
            
            dialog.load({"Cnme": strRecipient});
            
            if (strRecipient != "")
            {
                dialog.load({"Comp": strClientList});
                curDoc.removeDataObject("coFile");
            };
        },
        
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // OK - Cancel Buttons

        btnC: function(dialog) {boolAccept = 0; dialog.end("btnC");},

        btnP: function(dialog)
        {
        
            var validated = 0;
            
            if (    (dialog.store()["JaSc"] != strJavaScript)
                && ((dialog.store()["Comp"] != strClientList) || ((dialog.store()["Cnme"] != "") && true))
                )
            {
                validated = 1;
            };
            
            if (validated == 0)
            {
                if (dialog.store()["JaSc"] == strJavaScript){app.alert("A JavaScript file must be selected before continuing.");};
                if ((dialog.store()["Comp"] == strClientList) && (dialog.store()["Cnme"] == "")){app.alert("A company name must be entered or list text file must be selected before continuing.");};
            }
            else
            {
                // Set document properties
                curDoc.info.Title = dialog.store()["titl"];
                curDoc.info.Author = dialog.store()["auth"];
                curDoc.baseURL = strBaseURL;

                prepareDocument.setCopyright(dialog.store()["copy"]);
                prepareDocument.addJavaScript(dialog.store()["pdfE"], dialog.store()["navP"]);
                prepareDocument.savePDFs(dialog.store()["Comp"], dialog.store()["Cnme"]);

                dialog.end("btnP");
            };
        },
        
        destroy: function(dialog)
        {

            // Remove Company list file and reset dialog
            curDoc.removeDataObject("coFile");

            // Remove JavaScript file and reset dialog
            curDoc.removeDataObject("jsFile");
            
            curDoc.info.Title = docTitle;
            curDoc.info.Author = docAuthor;
            
            curDoc.dirty = false; 
            
            switch (boolAccept)
            {
                case 0: {break;};
                case 1:
                {
                    app.alert("Finished processing " + countCo + " documents.\r\nThis document will now close.");
                    curDoc.dirty = false;
                    curDoc.closeDoc(true);
                    break;
                };
                case -1:
                {
                    app.alert("The process was canceled.  " + countCo + " documents were processed.\r\nThis document will now close.");
                    curDoc.dirty = false;
                    curDoc.closeDoc(true);
                    break;
                };
                default: {app.alert("ERROR!"); break;};
            };
           
        },

///////////////////////////////////////////////////////////////////////////////////////////////////
// Functions

        addJavaScript: function(strExpiryDate, strPageNav)
        {
            // get the file stream object of the embedded file
            var jsFile = curDoc.getDataObjectContents("jsFile");

            // convert to a string 
            var jsContents = util.stringFromStream(jsFile, jsCharset);
            
            // Add expiry information
            if (strExpiryDate != "")
            {
                var strExpityJS = " var strExpiry = \"" + strExpiryDate + "\"; var forExpiry = \"" + dateFormat + "\";"
                jsContents = strExpityJS + jsContents
            };
            
            // Add page navigation, since page navigation is zero (0) based, subtract 1 from the value,
            // then verify the number is acceptable.
            var numPageNav = strPageNav - 1
            if ((navToPage = "") || (navToPage < 0) || (navToPage > curDoc.numPages - 1)) {numPageNav = 0};
            jsContents = "var navToPage = " + numPageNav + ";" + jsContents

            // Add JavaScript to first page, open
            curDoc.setPageAction(0, "Open", jsContents);
            
            // Remove JavaScript file
            curDoc.removeDataObject("jsFile");

        },
               
        setCopyright: function(strCopyright)
        {
            var meta = curDoc.metadata;
            var myXMPData = new XML(meta);
            
            myx = new Namespace("adobe:ns:meta/");
            myrdf = new Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
            mypdf = new Namespace("http://ns.adobe.com/pdf/1.3/");
            myxap = new Namespace("http://ns.adobe.com/xap/1.0/");
            mydc = new Namespace("http://purl.org/dc/elements/1.1/");
            myxapRights = new Namespace("http://ns.adobe.com/xap/1.0/rights/");
            var p = myXMPData.myrdf::RDF.myrdf::Description;

            p.mydc::rights.myrdf::Alt.myrdf::li = strCopyright;
            p.@myxapRights::Marked = bolCopyright;
            p.@myxapRights::WebStatement = strCopyrightURL;

            //Convert myXMPData into a string
            myNewXMPStr = myXMPData.toXMLString();
            
            //Assign myNewXMPStr to the document metadata
            curDoc.metadata = myNewXMPStr;
        },

        savePDFs: function(strClientFile, dlgRecipient)
        {
            
            // Get the path of the document
            var strPath = curDoc.path.replace(curDoc.documentFileName, "") + "/";
                        
            // Get the file name of the document
            var strFile = curDoc.documentFileName.replace(".pdf","");

            // Initialize a status window and progress bar
            var pBar = app.thermometer;

            // Process companies in the text file
            if ((strClientFile != strClientList) && (dlgRecipient == ""))
            {
            
                // get the file stream object of the embedded file
                var oFile = curDoc.getDataObjectContents("coFile");
                var fContents = util.stringFromStream(oFile, txtCharset);
                var rContents = fContents.split(strDelimRecord);
              
                // Remove Company list file
                curDoc.removeDataObject("coFile");
                
                // Display a status window and progress bar
                pBar.duration = rContents.length;
                pBar.begin();
                
                // Step through each of the clients
                for (i = 0; i < rContents.length; i++)
                { 
                    if (rContents[i] != "")
                    {                      
                        if (pBar.cancelled)
                        {
                            boolAccept = -1;
                            break;
                        }
                        else
                        {
                            // Execute prepareToSave
                            prepareDocument.prepareToSave(strPath, strFile, rContents[i], pBar, i + 1);
                            boolAccept = 1;
                        };
                    };
                };
                pBar.end();
            }
            // Process the one company in the text box
            else if ((strClientFile == strClientList) && (dlgRecipient != ""))
            {                
                // Execute prepareToSave
                prepareDocument.prepareToSave(strPath, strFile, dlgRecipient, pBar, 1);
                boolAccept = 1;
            };
        },
        
        prepareToSave: function(strPath, strFile, varRecipient, pBar, itemsSaved)
        {
            var arrRecipient    = varRecipient.split(strDelimField);
            
            var strPackage      = arrRecipient[0].replace( /^\s+/g,'').replace(/\s+$/g,'');
            var strClient       = arrRecipient[1].replace( /^\s+/g,'').replace(/\s+$/g,'');
            var strClientID     = arrRecipient[2].replace( /^\s+/g,'').replace(/\s+$/g,'');
            var strFacility     = arrRecipient[3].replace( /^\s+/g,'').replace(/\s+$/g,'');
            var strRefnum       = arrRecipient[4].replace( /^\s+/g,'').replace(/\s+$/g,'');
            var strCoordName    = arrRecipient[5].replace( /^\s+/g,'').replace(/\s+$/g,'');
            var strCoordPhone   = arrRecipient[6].replace( /^\s+/g,'').replace(/\s+$/g,'');
            var strCoordEMail   = arrRecipient[7].replace( /^\s+/g,'').replace(/\s+$/g,'');
                        
            // Indicate the current company 
            pBar.text = "Processing company " + strClient + "...";

            // Set the custom document properties (File -> Properties -> Custom)
            if ((strClient != "" ) && (strClient != "NULL" ))         {curDoc.info.Client     = strClient};
            if ((strClientID != "" ) && (strClientID != "NULL" ))     {curDoc.info.ClientID   = strClientID};
            if ((strFacility != "" ) && (strFacility != "NULL" ))     {curDoc.info.Facility   = strFacility};
            if ((strRefnum != "" ) && (strRefnum != "NULL" ))         {curDoc.info.Refnum     = strRefnum};
            if ((strCoordName != "" )  && (strCoordName != "NULL" ))  {curDoc.info.CoordName  = strCoordName};
            if ((strCoordPhone != "" ) && (strCoordPhone != "NULL" )) {curDoc.info.CoordPhone = strCoordPhone};
            if ((strCoordEMail != "" ) && (strCoordEMail != "NULL" )) {curDoc.info.CoordEMail = strCoordEMail};
                        
            // Set "preparedFor" custom document property and file name
            if (strRefnum != "" )
            {
                // Set the 'preparedFor' document property (File -> Properties -> Custom)
                curDoc.info.preparedFor = strClient + " (" + strRefnum + ")";      
            }
            else
            {
                // Set the 'preparedFor' document property (File -> Properties -> Custom)
                curDoc.info.preparedFor = strClient;
            };
                        
            // Set the file name
            var strFile = strPackage + " " + strFile;
            
            // Save the document
            tfSaveDoc(curDoc, strPath, strFile);
            
            // Set the number of companies processed
            countCo = itemsSaved;
                        
            // Step the progress bar
            pBar.value = itemsSaved;
            
        }
        
    };
    
///////////////////////////////////////////////////////////////////////////////////////////////////
// Create Single Recipient Dialog Box

    var prepareRecipient = 
    {
            
        DoDialog: function()
        {
            return app.execDialog(this);
        },
        
        initialize: function(dialog)
        {
            var arrRecipient = strRecipient.split(strDelimField);

            var strText =
            {
                "pack": arrRecipient[0],
                "clnt": arrRecipient[1],
                "clID": arrRecipient[2],
                "fact": arrRecipient[3],
                "rnum": arrRecipient[4],
                "dcrd": arrRecipient[5],
                "phne": arrRecipient[6],
                "emal": arrRecipient[7]
            };

            dialog.load(strText);

        },
        
        description:
        {
            name: "Recipient",
            first_tab: "pack",
            align_children: "align_row",        
            elements:
            [
            
                {
                type: "view",
                align_children: "align_row",
                alignment: "align_fill",
                elements:
                    [
                    {
                    name: "Please enter recipient details:",
                    item_id: "rTop",
                    type: "static_text",
                    alignment: "align_fill",
                    bold: true,
                    font: "dialog"
                    }
                    ]
                },
                
                {
                type: "gap"
                },
                
                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Package Name\u2020:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "pack",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "clnt"
                    }
                    ]
                },
                
                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Client Name\u2020:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "clnt",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "clID"
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Client ID:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "clID",
                    type: "edit_text",
                    char_width: numWidth,
                    next_tab: "fact"
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Facility name:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "fact",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "rnum"
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Refnum:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "rnum",
                    type: "edit_text",
                    char_width: numWidth,
                    next_tab: "dcrd"
                    }
                    ]
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Recipient Name:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "dcrd",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "phne"
                    }
                    ]
                },
                
                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "Telephone:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "phne",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "emal"
                    }
                    ]
                },
                
                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    name: "E-mail address:",
                    type: "static_text",
                    font: "dialog",
                    bold: true,
                    char_width: lblWidth
                    },
                    {
                    item_id: "emal",
                    type: "edit_text",
                    char_width: txtWidth,
                    next_tab: "clnt"
                    }
                    ]
                },
                
                {
                type: "gap"
                },

                {
                type: "view",
                align_children: "align_row",
                elements:
                    [
                    {
                    type: "static_text",
                    char_width: lblWidth + txtWidth - btnWidth - btnWidth - 5
                    },
                    {
                    name: "Ok",
                    item_id: "btnO",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "btnC" 
                    },
                    {
                    name: "Cancel && Clear",
                    item_id: "btnC",
                    type: "button",
                    char_width: btnWidth,
                    next_tab: "titl" 
                    }
                    ]
                }
            ]
        },
        
        btnO: function(dialog)
        {
            strRecipient = dialog.store()["pack"] + strDelimField + dialog.store()["clnt"] + strDelimField + dialog.store()["clID"] + strDelimField + dialog.store()["fact"] + strDelimField + dialog.store()["rnum"] + strDelimField + dialog.store()["dcrd"] + strDelimField + dialog.store()["phne"] + strDelimField + dialog.store()["emal"];
            dialog.end("btnO");
        },
        
        btnC: function(dialog)
        {
            strRecipient = "";
            dialog.end("btnC");
        }

    };
        
///////////////////////////////////////////////////////////////////////////////////////////////////
// Display Main Dialog Box

    if (err != "TypeError: curDoc.info has no properties")
    {
        if (app.viewerVersion < 8) {app.alert("This function requires Acrobat 8.0 or greater.");}
        else {prepareDocument.DoDialog();};
    }
    else
    {
        app.alert("You must have a document open to apply the Solomon Security.");
    };


};


///////////////////////////////////////////////////////////////////////////////////////////////////
// Trusted Functions

var tfSaveDoc = app.trustedFunction(function(curDoc, strPath, strFile)
    {                                    
        app.beginPriv();
        curDoc.saveAs(strPath + strFile + ".pdf");     
        app.endPriv();
    }
);

var tfAddAttachment = app.trustedFunction(function(curDoc, strAttach)
    {                    
        app.beginPriv();
        curDoc.removeDataObject(strAttach);
        curDoc.importDataObject(strAttach);
        app.endPriv();
    }
);

var tfAddAttachmentPath = app.trustedFunction(function(curDoc, strAttach, strPath)
    {                    
        app.beginPriv();
        curDoc.removeDataObject(strAttach);
        curDoc.importDataObject(strAttach, strPath);
        app.endPriv();
    }
);

