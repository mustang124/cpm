﻿
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.ServiceModel



<ComClass(ServiceClient.ClassId, ServiceClient.InterfaceId, ServiceClient.EventsId)> _
Public Class ServiceClient


#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "51ce87c2-a6b1-4902-b562-c91b1ad0c803"
    Public Const InterfaceId As String = "a084a870-703c-43e2-b97c-7c69568653ea"
    Public Const EventsId As String = "17b3e209-456a-490c-b83f-7747f8994ee9"
#End Region


    Private myTransferProtocol As TRANSFER_PROTOCOL
    Private myFTPSetting As FTPSetting
    Private myServiceSetting As ServiceSetting

#Region "Enums"
    Public Enum BINDING_TYPE
        BASIC
        SECURED
        TCP
    End Enum

    Public Enum TRANSFER_PROTOCOL
        FTP
        SOAP
        CUSTOM
    End Enum
#End Region

#Region "Structures"

    Public Structure ServiceSetting
        Public Address As String
        Public BindingType As BINDING_TYPE
    End Structure

    Public Structure FTPSetting
        Public Address As String
        Public Username As String
        Public Password As String
    End Structure

#End Region

#Region "Properties"
    ''' <summary>
    ''' Only available to .NET 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ServiceConfiguration() As ServiceSetting
        Get
            Return Me.myServiceSetting
        End Get
        Set(ByVal value As ServiceSetting)
            Me.myServiceSetting = value
        End Set
    End Property

    ''' <summary>
    ''' Only available to .NET
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FTPConfiguration() As FTPSetting
        Get
            Return Me.myFTPSetting
        End Get
        Set(ByVal value As FTPSetting)
            Me.myFTPSetting = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the protocol that will be used to upload files to the server
    ''' FTP or SOAP
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TransferProtocol() As TRANSFER_PROTOCOL
        Get
            Return Me.myTransferProtocol
        End Get
        Set(ByVal value As TRANSFER_PROTOCOL)
            Me.myTransferProtocol = value
        End Set
    End Property
#End Region


#Region "Constructor"
    ' A creatable COM class must have a Public Sub New() 
    ' with no parameters, otherwise, the class will not be 
    ' registered in the COM registry and cannot be created 
    ' via CreateObject.
    Public Sub New()
        MyBase.New()
        Me.myServiceSetting.Address = "http://webservices.solomononline.com/DLM/v2/DownloadManager.asmx"
        Me.myServiceSetting.BindingType = BINDING_TYPE.BASIC
        Me.myFTPSetting.Address = "ftp://webservices.solomononline.com/"
        Me.myFTPSetting.Username = "dlm"
        Me.myFTPSetting.Password = "direc2007"
    End Sub
#End Region

#Region "Setters"
    ''' <summary>
    ''' Use for configuring the connection the FTP server
    ''' </summary>
    ''' <param name="Addr"> FTP server address. ftp://ftpserver.com/</param>
    ''' <param name="Username">Username</param>
    ''' <param name="Password"> Password </param>
    ''' <remarks></remarks>
    Public Sub SetFTPConfiguration(ByVal Addr As String, ByVal Username As String, ByVal Password As String)
        Me.myFTPSetting.Address = Addr
        Me.myFTPSetting.Username = Username
        Me.myFTPSetting.Password = Password
    End Sub

    ''' <summary>
    ''' Use for configuring the connection to download manager web service
    ''' </summary>
    ''' <param name="ServiceAddr">URL of the web service. eg. http://serviceurl.com/service</param>
    ''' <param name="BindingType">Type of connection: BASIC, SECURED, or TCP </param>
    ''' <remarks></remarks>
    Public Sub SetServiceConfiguration(ByVal ServiceAddr As String, ByVal BindingType As BINDING_TYPE)
        Me.myServiceSetting.Address = ServiceAddr
        Me.myServiceSetting.BindingType = BindingType
    End Sub
#End Region

#Region "test"
    Public Function uploadtest(ByVal filename As String, ByVal saveAs As String)
        Dim srvref As DLMLocal.DownloadManagerSoapClient
        Dim bindingType As System.ServiceModel.Channels.Binding

        bindingType = New BasicHttpBinding()

        If ServiceConfiguration.BindingType = BINDING_TYPE.SECURED Then
            bindingType = New WSHttpBinding()
        ElseIf ServiceConfiguration.BindingType = BINDING_TYPE.TCP Then
            bindingType = New NetPeerTcpBinding()
        End If
        
        Using dlmSrv As New DLMLocal.DownloadManagerSoapClient(bindingType, New EndpointAddress("http://localhost/dlm/DownloadManager.asmx"))
            UploadFileM1test(filename, saveAs, dlmSrv)
        End Using

    End Function

    Private Sub UploadFileM1test(ByVal filename As String, ByVal saveFileAs As String, ByVal dlmSrv As DLMLocal.DownloadManagerSoapClient, Optional ByVal ClientName As String = "", Optional ByVal UseClientFTP As Boolean = False)
        Using inStream As New FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read)
            Dim bRead As New BinaryReader(inStream)
            Dim theData() As Byte
            Dim bufSize As Long = 1000000 'Bufsize is 1 million bytes long

            While (inStream.Position < inStream.Length)

                If (inStream.Length - inStream.Position < bufSize) Then
                    bufSize = inStream.Length - inStream.Position
                End If
                theData = bRead.ReadBytes(bufSize)
                If (UseClientFTP) Then
                    dlmSrv.PutFileClientFTP(theData, saveFileAs, True, ClientName)
                Else
                    dlmSrv.PutFile(theData, saveFileAs, True)
                End If

            End While
        End Using
    End Sub

    Public Function GetDownloadLinktest(ByVal Email As String, ByVal Password As String, ByVal Filename As String, Optional ByVal NumOfDownloads As Single = CSng(1), Optional ByVal GroupOrKey As String = "", Optional ByVal DisabledAutoDelete As Single = 0, Optional ByVal OriginatorName As String = "", Optional ByVal ClientName As String = "", Optional ByVal UseClientFTP As Boolean = False) As String
        Try

            Dim bindingType As System.ServiceModel.Channels.Binding

            bindingType = New BasicHttpBinding()

            If ServiceConfiguration.BindingType = BINDING_TYPE.SECURED Then
                bindingType = New WSHttpBinding()
            ElseIf ServiceConfiguration.BindingType = BINDING_TYPE.TCP Then
                bindingType = New NetPeerTcpBinding()
            End If


            Using dlmSrv As New DLMLocal.DownloadManagerSoapClient(bindingType, New EndpointAddress("http://localhost/dlm/DownloadManager.asmx"))


                Dim serviceResponse = dlmSrv.getDownloadUrl_v2(Email, Password, Filename, NumOfDownloads, GroupOrKey, DisabledAutoDelete, OriginatorName, ClientName)

                If Not serviceResponse.error.errorOccured Then
                    Dim saveAs As String = serviceResponse.fileSavedAs

                    If Me.TransferProtocol = TRANSFER_PROTOCOL.FTP Then
                        FTPUploadFile(Filename, saveAs)
                    ElseIf Me.TransferProtocol = TRANSFER_PROTOCOL.SOAP Then
                        UploadFileM1test(Filename, saveAs, dlmSrv, ClientName, UseClientFTP)
                    End If
                    Return serviceResponse.downloadURL
                End If

                'return the link 
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return ""
    End Function

#End Region

#Region "Get Link Method"
    ''' <summary>
    ''' Returns as a download key for a given email address and filename  
    ''' </summary>
    ''' <param name="deliveryMethod"> method for uploading to server</param>
    ''' <param name="email"> email address</param>
    ''' <param name="password"> password that will be used for downloading this link</param>
    ''' <param name="filename"> filename </param>
    ''' <param name="numOfDownloads"> Number of downloads allowed </param>
    ''' <param name="GroupOrKey"> Key or tag to associate with this file</param>
    ''' <param name="DisabledAutoDelete"> 0- should be deleted after 30 days  and 1 - should never be deleted</param>
    ''' <param name="originatorName"> Name of the person who is uploading this file</param>
    ''' <param name="client"> Client name who this file is for</param>
    ''' <returns> Returns a web address link for downloading the file </returns>
    ''' <remarks></remarks>
    Public Function GetDownloadLink(ByVal Email As String, ByVal Password As String, ByVal Filename As String, Optional ByVal NumOfDownloads As Single = CSng(1), Optional ByVal GroupOrKey As String = "", Optional ByVal DisabledAutoDelete As Single = 0, Optional ByVal OriginatorName As String = "", Optional ByVal ClientName As String = "") As String
        Try

            Dim bindingType As System.ServiceModel.Channels.Binding

            bindingType = New BasicHttpBinding()

            If ServiceConfiguration.BindingType = BINDING_TYPE.SECURED Then
                bindingType = New WSHttpBinding()
            ElseIf ServiceConfiguration.BindingType = BINDING_TYPE.TCP Then
                bindingType = New NetPeerTcpBinding()
            End If


            Using dlmSrv As New DLMServiceReference.DownloadManagerSoapClient( _
                              bindingType, _
                              New EndpointAddress(Me.ServiceConfiguration.Address))


                Dim serviceResponse = dlmSrv.getDownloadUrl_v2(Email, Password, Filename, NumOfDownloads, GroupOrKey, DisabledAutoDelete, OriginatorName, ClientName)

                If Not serviceResponse.error.errorOccured Then
                    Dim saveAs As String = serviceResponse.fileSavedAs

                    If Me.TransferProtocol = TRANSFER_PROTOCOL.FTP Then
                        FTPUploadFile(Filename, saveAs)
                    ElseIf Me.TransferProtocol = TRANSFER_PROTOCOL.SOAP Then
                        UploadFileM1(Filename, saveAs, dlmSrv)
                    End If
                    Return serviceResponse.downloadURL
                End If

                'return the link 
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return ""
    End Function
#End Region

#Region "File Transfer Methods"
    ''' <summary>
    ''' Uploads file using a web service call
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UploadFileM1(ByVal filename As String, ByVal saveFileAs As String, ByVal dlmSrv As DLMServiceReference.DownloadManagerSoapClient)
        Using inStream As New FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read)
            Dim bRead As New BinaryReader(inStream)
            Dim theData() As Byte
            Dim bufSize As Long = 1000000 'Bufsize is 1 million bytes long

            While (inStream.Position < inStream.Length)

                If (inStream.Length - inStream.Position < bufSize) Then
                    bufSize = inStream.Length - inStream.Position
                End If
                theData = bRead.ReadBytes(bufSize)
                dlmSrv.PutFile(theData, saveFileAs, True)
            End While
        End Using

    End Sub


    ''' <summary>
    ''' Uploads file using FTP
    ''' </summary>
    ''' <param name="filename"> File full pathname  </param>
    ''' <param name="saveFileAs"> Saves the file as given name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function FTPUploadFile(ByVal filename As String, ByVal saveFileAs As String) As String
        Dim request As FtpWebRequest = CType(WebRequest.Create(Me.FTPConfiguration.Address & saveFileAs), FtpWebRequest)
        request.Method = WebRequestMethods.Ftp.UploadFile

        ' This example assumes the FTP site uses anonymous logon.
        request.Credentials = New NetworkCredential(Me.FTPConfiguration.Username, Me.FTPConfiguration.Password)

        ' Copy the contents of the file to the request stream.
        Dim sourceStream As New BinaryReader(New FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        Dim fileContents() As Byte = sourceStream.ReadBytes(sourceStream.BaseStream.Length)

        sourceStream.Close()
        request.ContentLength = fileContents.Length

        Using requestStream As Stream = request.GetRequestStream()
            requestStream.Write(fileContents, 0, fileContents.Length)
        End Using

        Using response As FtpWebResponse = CType(request.GetResponse(), FtpWebResponse)

            Return response.StatusDescription
        End Using
    End Function

#End Region



End Class


