<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Utility" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Import Namespace="System.Configuration" %>

<%@ Page Language="C#" Trace="False" %>

<html>
<head>
    <script runat="server">

        void txtPassword_TextChanged(object sender, EventArgs e)
        {
            String password = null;
            int max = 0;
            int loginAttempts = 0;
            String email = "";
            String encPassword = null;
            string connStr = string.Empty;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                connStr = ConfigurationManager.ConnectionStrings["DownloadManagerDEV"].ToString();
            else
                connStr = ConfigurationManager.ConnectionStrings["DownloadManagerPROD"].ToString();

            OdbcConnection conn = new OdbcConnection(connStr);
            OdbcDataReader reader = null;
            OdbcCommand cCommand = new OdbcCommand("SELECT Password,MaxDownloadCount,LoginAttempts,email FROM FileKey WHERE RTRIM(filekey)='" + Request.QueryString["key"].Trim() + "'", conn);

            try
            {
                conn.Open();
                reader = cCommand.ExecuteReader();
                reader.Read();
                password = reader.GetString(0);
                max = reader.GetInt32(1);
                loginAttempts = reader.GetInt32(2);
                email = reader.GetString(3);

                reader.Close();

                if (loginAttempts < max * 5)
                {
                    //Response.Write(loginAttempts);
                    if ((password == EncryptText(txtPassword.Text)) && Application[Request.QueryString["key"]] == null)
                    {
                        Session["SAValid"] = true;
                        Application[Request.QueryString["key"]] = 1;
                        // Response.Redirect("http://webservices.solomononline.com/dlm/Download.aspx?key="+  Request.QueryString["key"]);
                        Response.Redirect("./Download.aspx?key=" + Request.QueryString["key"]);
                    }
                    else if (Application[Request.QueryString["key"]] != null)
                    {
                        lblInfo.Text = "This file is currently being downloaded. Please wait " + Application[Request.QueryString["key"]] + " and try again.";
                    }
                    else
                    {
                        lblInfo.Text = "Password is invalid. Please try again. ";
                        OdbcCommand uCommand = new OdbcCommand("UPDATE FileKey SET LoginAttempts=" + ++loginAttempts + " WHERE FileKey ='" + Request.QueryString["key"] + "'", conn);
                        uCommand.ExecuteNonQuery();
                    }
                }
                else
                {
                    lblInfo.Text = "The access to this file has been disabled because of too many failed login attempts. Please contact your Solomon Associate representative for help.";
                    //if (!Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                    //    Logger.LoggedEntry("<" + Request.UserHostAddress + "> SECURITY ALERT -" + email + " had too many login failures");
                }
                conn.Close();
            }
            catch (System.Exception ex)   // file IO errors
            {
                lblInfo.Text = "Unable to locate file. Please contact Solomon Asscociates for assistance.";
            }
        }

        private String EncryptText(String msg)
        {
            MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
            Byte[] encryptedKey;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

            try
            {
                encryptedKey = encrypter.ComputeHash(encoder.GetBytes(msg));
                return HexEncoding.ToString(encryptedKey);

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    </script>
    <link href="styles.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" type="text/JavaScript">
<!--
        function MM_reloadPage(init) {  //reloads the window if Nav4 resized
            if (init == true) with (navigator) {
                if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
                    document.MM_pgW = innerWidth; document.MM_pgH = innerHeight; onresize = MM_reloadPage;
                } 
            }
            else if (innerWidth != document.MM_pgW || innerHeight != document.MM_pgH) location.reload();
        }
        MM_reloadPage(true);
//-->
    </script>
    <style type="text/css">
        .style1
        {
            font-size: 12px;
        }
    </style>
</head>
<body style="background: url(images/DownloadManagerBG.jpg) no-repeat; align: center"
    leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <!-- ImageReady Slices (template.psd) -->
    <form id="Form1" runat="server">
    <table id="MainTable" bordercolor="lightgrey" height="600" cellspacing="0" cellpadding="0"
        width="800" border="0">
        <tr>
            <td colspan="8" height="8">
            </td>
        </tr>
        <tr>
            <td width="3" colspan="2" height="70" rowspan="2">
                &nbsp;
            </td>
            <td valign="bottom" colspan="6" height="89">
                <font color="#ffffcc"><strong>
                    <table bordercolor="#000099" height="44" width="584" align="left" border="0">
                        <tr>
                            <td width="478">
                                <div align="left">
                                    <span class="style1"><strong><font color="#990000" size="3">Enter&nbsp;Password To&nbsp;Retrieve
                                        Your File</font></strong>&nbsp;<font class="style1" color="#990000" size="5"><strong>
                                        </strong></font></span>
                                </div>
                            </td>
                            <td width="134">
                                <asp:TextBox ID="txtPassword" runat="server" MaxLength="25" Columns="15" OnTextChanged="txtPassword_TextChanged"
                                    TextMode="Password" Font-Size="Medium" BorderStyle="Solid" BorderColor="#C00000"
                                    BorderWidth="1px" Height="25px" Width="160px"></asp:TextBox>
                            </td>
                            <td width="112">
                                <p align="left">
                                    <asp:Button ID="btnSubmit" TabIndex="1" runat="server" Font-Size="Small" Font-Bold="True"
                                        BorderStyle="Solid" BackColor="White" Text="Submit" ForeColor="Firebrick" BorderColor="#C00000"
                                        BorderWidth="1px"></asp:Button></p>
                            </td>
                        </tr>
                    </table>
                </strong></font>
            </td>
        </tr>
        <tr>
            <td width="186" bgcolor="#cc9999" height="1">
                &nbsp;
            </td>
            <td width="16" bgcolor="#cc9999" height="1">
                &nbsp;
            </td>
            <td width="498" bgcolor="#cc9999" height="1">
                &nbsp;
            </td>
            <td width="90" bgcolor="#cc9999" height="1">
                &nbsp;
            </td>
            <td width="90" bgcolor="#cc9999" height="1">
            </td>
            <td width="178" bgcolor="#cc9999" height="1">
            </td>
        </tr>
        <tr valign="top">
            <td align="right" width="3" colspan="2" height="459">
            </td>
            <td valign="middle" align="center" colspan="6" height="459">
                <span class="style1">
                    <asp:Label ID="lblInfo" runat="server" BackColor="Transparent" BorderColor="Transparent"
                        Font-Bold="True" Font-Size="Medium" Font-Names="Arial" Width="371px" Height="31px"
                        ForeColor="DarkRed"></asp:Label>
                </span>
                <!--- My Content -->
            </td>
        </tr>
        <tr>
            <td width="2" height="45">
                &nbsp;
            </td>
            <td colspan="7" height="45">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
    <!-- End ImageReady Slices -->
    <br>
    <br>
</body>
</html>
