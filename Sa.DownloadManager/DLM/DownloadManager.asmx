<%@ WebService Language="C#" Class="DownloadManager" %>

using System;
using System.Web;
using System.Web.Services;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Data.Odbc;
using Utility;
using System.Configuration;
using Microsoft.Web.Administration;
using System.ComponentModel;
using System.Runtime.InteropServices;


[WebService(Namespace = "http://webservices.solomononline.com/dlm/")]
public class DownloadManager : WebService
{
    public struct ServiceError
    {
        public bool errorOccured;
        public string description;
    }

    public struct DownloadLinkInfo
    {
        public string downloadURL;
        public string fileSavedAs;
        public ServiceError error;
    }
    
    [WebMethod]
    public void PutFileClientFTP(byte[] buffer, string filename, bool isChunking, string clientName)
    {
        //if (!Directory.Exists(@"d:\clientftp\LocalUser\" + clientName))
        //{
        //    Directory.CreateDirectory(@"d:\clientftp\LocalUser\" + clientName);
        //}
        if (!Directory.Exists(@"c:\temp\" + clientName))
        {
            Directory.CreateDirectory(@"c:\temp\" + clientName);
        }
        //using (Stream fileStream = new FileStream(@"d:\clientftp\LocalUser\" + clientName + "\\" + filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))     
        using (Stream fileStream = new FileStream(@"c:\temp\" + clientName + "\\" + filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
        {
            using (BinaryWriter bw = new BinaryWriter(fileStream))
            {
                bw.Write(buffer);
            }
        }
    }

    [WebMethod]
    public void PutFile(byte[] buffer, string filename, bool isChunking)
    {
        using (Stream fileStream = new FileStream(@"D:\dlm_ftproot\" + filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
        {
            using (BinaryWriter bw = new BinaryWriter(fileStream))
            {
                bw.Write(buffer);
            }
        }
    }

    //Updated method
    [WebMethod]
    public DownloadLinkInfo getDownloadUrl_v2(String email, String password, String filename, Single DownloadsAllowed, String group, Single DisabledAutoDelete, String Poster, String Client)
    {
        string connStr = string.Empty;
        if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
            connStr = ConfigurationManager.ConnectionStrings["DownloadManagerDEV"].ToString();
        else
            connStr = ConfigurationManager.ConnectionStrings["DownloadManagerPROD"].ToString();

        String tmpFileName;
        String genKey;
        String cmd = null;
        String SaveAs = System.IO.Path.GetFileName(filename);
        DownloadLinkInfo dResponse = new DownloadLinkInfo();

        OdbcConnection conn = new OdbcConnection(connStr);
        OdbcCommand iCommand = null;

        try
        {
            conn.Open();

            genKey = this.GenerateKey(email, conn);
            tmpFileName = this.CheckFilename(Path.GetFileName(filename));
            dResponse.fileSavedAs = tmpFileName;

            cmd = "INSERT INTO FileKey(FileKey,FilePath,Email,Password,MaxDownloadCount,PostedDate,GroupBy,DownloadCount,DisabledAutoDelete,PostedBy,ClientID,ActualFileName) " +
                  "VALUES('" + genKey + "','D:\\dlm_ftproot\\" + tmpFileName + "','" + email + "','" + EncryptText(password) + "'," + DownloadsAllowed + ",'" + DateTime.Now.ToString() + "', '" + group + "',0," + DisabledAutoDelete + ",'" + Poster + "','" + Client + "','" + SaveAs + "')";

            iCommand = new OdbcCommand(cmd, conn);

            // Execute the SQL command
            int nNoAdded = iCommand.ExecuteNonQuery();

            //if (!Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
            //    Logger.LoggedEntry("UPLOAD INFO - " + filename + " was uploaded on the server for " + email);

            dResponse.downloadURL = "http://webservices.solomononline.com/dlm/Authenticate.aspx?key=" + genKey;
        }
        catch (System.Exception ex)   // file IO errors
        {
            dResponse.error.errorOccured = true;
            dResponse.error.description = ex.Message;
        }
        finally
        {
            conn.Close();
        }

        return dResponse;
    }

    //Legacy method
    [WebMethod]
    public String getDownloadURL(String email, String password, String filename, Single DownloadsAllowed, String group, Single DisabledAutoDelete)
    {
        string connStr = string.Empty;
        if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
            connStr = ConfigurationManager.ConnectionStrings["DownloadManagerDEV"].ToString();
        else
            connStr = ConfigurationManager.ConnectionStrings["DownloadManagerPROD"].ToString();

        String genKey;
        String cmd = null;
        OdbcConnection conn = new OdbcConnection(connStr);
        OdbcCommand iCommand = null;

        try
        {
            conn.Open();
            genKey = GenerateKey(email, conn);

            cmd = "INSERT INTO FileKey(FileKey,FilePath,Email,Password,MaxDownloadCount,PostedDate,GroupBy,DownloadCount,DisabledAutoDelete) " +
                  "VALUES('" + genKey + "','" + filename + "','" + email + "','" + EncryptText(password) + "'," + DownloadsAllowed + ",'" + DateTime.Now.ToString() + "', '" + group + "',0," + DisabledAutoDelete + ")";

            iCommand = new OdbcCommand(cmd, conn);

            // Execute the SQL command
            int nNoAdded = iCommand.ExecuteNonQuery();

            //if (!Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
            //    Logger.LoggedEntry("UPLOAD INFO - " + filename + " was uploaded on the server for " + email);

            return "http://webservices.solomononline.com/dlm/Authenticate.aspx?key=" + genKey;
        }
        catch (System.Exception ex)   // file IO errors
        {
            return ex.Message + " " + cmd;
        }
        finally
        {
            conn.Close();
        }
    }

    [WebMethod]
    public String CheckFilename(String fileName)
    {
        String tmpFileName;
        Int32 i = 0;

        tmpFileName = fileName;
        while (File.Exists("D:\\dlm_ftproot\\" + fileName))
        {
            tmpFileName = i + "_" + fileName;

            if (!File.Exists("D:\\dlm_ftproot\\" + tmpFileName))
            {
                fileName = tmpFileName;
            }
            i += 1;
        }
        return fileName;
    }


    private String GenerateKey(String email, OdbcConnection conn)
    {
        String genKey;
        OdbcDataReader reader = null;
        OdbcCommand cCommand = new OdbcCommand("select Count(*) from FileKey", conn);

        try
        {
            reader = cCommand.ExecuteReader();
            reader.Read();
            genKey = EncryptText(email + reader.GetInt32(0) + DateTime.Now.ToShortTimeString());
            reader.Close();
            return genKey;

        }
        catch (System.Exception ex)
        {
            throw ex;
        }
    }

    private String EncryptText(String msg)
    {
        MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
        Byte[] encryptedKey;
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
        try
        {
            encryptedKey = encrypter.ComputeHash(encoder.GetBytes(msg));
            return HexToString(encryptedKey);
        }
        catch (System.Exception ex)
        {
            throw ex;
        }
    }

    private string HexToString(byte[] bytes)
    {
        string hexString = "";
        for (int i = 0; i < bytes.Length; i++)
        {
            hexString += bytes[i].ToString("X2");
        }
        return hexString;
    }
}
