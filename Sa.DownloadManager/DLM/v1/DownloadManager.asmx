<%@ WebService Language="C#"  Class="DownloadManager"  %>



using System;
using System.Web;
using System.Web.Services;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Data.Odbc;
using Utility;



[WebService(Namespace="http://webservices.solomononline.com/dlm/")]

public class DownloadManager:WebService
{

[WebMethod]
    public String getDownloadURL(String email, String password, String filename,Single DownloadsAllowed , String group, Single DisabledAutoDelete)
    {

        String connStr ="Driver={Sql Server};" +
                                    "Server=10.10.41.13;Database=DLM;" +
                                    "UID=DownloadMgrUser;PWD=dlm6112009;";
        //connStr = "DSN=DownloadManager;UID=;PWD=;";

        String genKey;
        String cmd = null ;
        OdbcConnection conn = new OdbcConnection(connStr);
        OdbcCommand iCommand = null;

        try
        {
            conn.Open();
            genKey = GenerateKey(email,conn);


            cmd = "INSERT INTO FileKey(FileKey,FilePath,Email,Password,MaxDownloadCount,PostedDate,GroupBy,DownloadCount,DisabledAutoDelete) " +
                  "VALUES('"+ genKey + "','" + filename +"','"+ email + "','"+ EncryptText(password) +"',"+ DownloadsAllowed+",'"+ DateTime.Now.ToString()+ "', '" + group+ "',0," + DisabledAutoDelete + ")";

            iCommand = new OdbcCommand(cmd,conn);

            // Execute the SQL command
            int nNoAdded = iCommand.ExecuteNonQuery();

            conn.Close();

	         Logger.LoggedEntry("UPLOAD INFO - " +
                       filename +" was uploaded on the server for "+
                       email);

            return "http://webservices.solomononline.com/dlm/Authenticate.aspx?key="+genKey;

        }
        catch (System.Exception ex)   // file IO errors
        {
           return ex.Message +" "+ cmd;
        }
    }


    /*[WebMethod]
    public String getDownloadURL(String email, String password, String filename,Single DownloadsAllowed, Single DisabledAutoDelete )
    {

        String connStr ="Driver={Sql Server};" +
                                    "Server=10.10.41.13;Database=DLM;" +
                                    "UID=DownloadMgrUser;PWD=dlm6112009;";
        //connStr = "DSN=DownloadManager;UID=;PWD=;";

        String genKey;
        String cmd = null ;
        OdbcConnection conn = new OdbcConnection(connStr);
        OdbcCommand iCommand = null;

        try
        {
            conn.Open();
            genKey = GenerateKey(email,conn);


            cmd = "Insert into FileKey(FileKey,FilePath,Email,Password,MaxDownloadCount,PostedDate,DisabledAutoDelete) " +
                  "Values('"+ genKey + "','" + filename +"','"+ email + "','"+ EncryptText(password) +"',"+ DownloadsAllowed+",'"+ DateTime.Now.ToString()+ "'," + DisabledAutoDelete +")";

            iCommand = new OdbcCommand(cmd,conn);

            // Execute the SQL command
            int nNoAdded = iCommand.ExecuteNonQuery();

            conn.Close();
 	    
            Logger.LoggedEntry("UPLOAD INFO -" +
                       filename +" was uploaded on the server for "+
                       email);

            return "http://sanet.solomononline.com/dlm/Authenticate.aspx?key="+genKey;

        }
        catch (System.Exception ex)   // file IO errors
        {
           return ex.Message;
        }
    }*/


 [WebMethod]
 public String CheckFilename(String fileName)
{
 String tmpFileName;
 Int32 i =0;

 tmpFileName = fileName;
 while (File.Exists("D:\\dlm_ftproot\\"+fileName)){
       tmpFileName = i+"_"+ fileName;

       if (!File.Exists("D:\\dlm_ftproot\\"+ tmpFileName)) {
             fileName = tmpFileName;
       }
       i+=1;
  }

 return fileName;
}


    private String  GenerateKey(String email, OdbcConnection conn)
    {
        String genKey;
        OdbcDataReader reader = null;
        OdbcCommand cCommand = new OdbcCommand("select Count(*) from FileKey",conn);

        try
        {
            reader = cCommand.ExecuteReader();
            reader.Read();
            genKey = EncryptText(email + reader.GetInt32(0) + DateTime.Now.ToShortTimeString());
            reader.Close();
            return genKey;

        }
        catch(System.Exception ex)
        {
            throw ex;
        }


    }


    private String EncryptText(String msg)
    {
        MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
        Byte[] encryptedKey ;
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
        try
        {
            encryptedKey = encrypter.ComputeHash(encoder.GetBytes(msg));
 	        return HexToString(encryptedKey);

	    }
        catch(System.Exception ex)
        {
            throw ex;
        }

    }


    private string HexToString(byte[] bytes)
    {
	   string hexString = "";
	   for (int i=0; i<bytes.Length; i++)
	   {
	      hexString += bytes[i].ToString("X2");
	   }
       return hexString;
    }

}