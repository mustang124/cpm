<%@ import Namespace="System.Data.Odbc" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="Utility" %>
<%@ import Namespace="System.IO" %>
<%@ import Namespace="System.Security.Cryptography" %>
<%@ Page Language="C#"  Trace="False" %>
<HTML>
	<HEAD>
		<script runat="server">

    void txtPassword_TextChanged(object sender, EventArgs e)
    {
        String password = null;
		int max=0;
		int loginAttempts = 0;
		String email="";
        String encPassword= null;
        String connStr ="Driver={Sql Server};" +
                         "Server=10.10.41.13;Database=DLM;" +
                         "UID=DownloadMgrUser;PWD=dlm6112009;";
        OdbcConnection conn = new OdbcConnection(connStr);
        OdbcDataReader reader = null;
        OdbcCommand cCommand = new OdbcCommand("SELECT Password,MaxDownloadCount,LoginAttempts,email FROM FileKey WHERE RTRIM(filekey)='"+ Request.QueryString["key"].Trim() + "'",
                                                 conn);
    
         try
         {
            conn.Open();
            reader = cCommand.ExecuteReader();
            reader.Read();
            password=reader.GetString(0);  
			max = reader.GetInt32(1);
			loginAttempts =reader.GetInt32(2);
            email=reader.GetString(3);

            reader.Close();
           
    
      	    if ( loginAttempts < max*5) 
	    {
           
              if ((password==EncryptText(txtPassword.Text))  &&  Application[Request.QueryString["key"]]== null  )
              {

                 Session["SAValid"]=true;
                 //Application[Request.QueryString["key"]]=1;
                 Response.Redirect("http://webservices.solomononline.com/dlm/Download.aspx?key="+  Request.QueryString["key"]);
                 
              }
              else if (Application[Request.QueryString["key"]]!= null)
              {
                 lblInfo.Text = "This file is currently being downloaded. Please wait " + Application[Request.QueryString["key"]] + " and try again.";
              }
              else
              {
                lblInfo.Text = "Password is invalid. Please try again. ";
                OdbcCommand uCommand = new OdbcCommand("UPDATE FileKey SET LoginAttempts="+ ++loginAttempts +" WHERE FileKey ='"+Request.QueryString["key"]+"'",conn);
				uCommand.ExecuteNonQuery();
              }
 	      
	    }
	    else
	    {
   				lblInfo.Text = "The access to this file has been disabled because of too many failed login attempts." +
                               "Please contact your Solomon Associate representative for help.";
                Logger.LoggedEntry("<" + Request.UserHostAddress +"> SECURITY ALERT -" + email +" had too many login failures" );
	    }
	    conn.Close();

         }
         catch (System.Exception ex)   // file IO errors
         {

			lblInfo.Text = "Unable to locate file. Please contact Solomon Asscociates for assistance.";
    
         }
    }
    
    
    
     private String EncryptText(String msg)
     {
        MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
        Byte[] encryptedKey ;
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
    
        try
        {
           encryptedKey = encrypter.ComputeHash(encoder.GetBytes(msg));
           return HexEncoding.ToString(encryptedKey);
    
        }
        catch(System.Exception ex)
        {
           throw ex;
        }
     }

		</script>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
			<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
			</script>
			<style type="text/css">.style1 {
	FONT-SIZE: 12px
}
</style>
	</HEAD>
	<body style="BACKGROUND: url(images/DownloadManagerBG.jpg) no-repeat; align: center" leftMargin="0"
		topMargin="0" marginheight="0" marginwidth="0">
		<!-- ImageReady Slices (template.psd) -->
		<form id="Form1" runat="server">
			<table id="MainTable" borderColor="lightgrey" height="600" cellSpacing="0" cellPadding="0"
				width="800" border="0">
				<tr>
					<td colSpan="8" height="8"></td>
				</tr>
				<tr>
					<td width="3" colSpan="2" height="70" rowSpan="2">&nbsp;
					</td>
					<td vAlign="bottom" colSpan="6" height="89"><FONT color="#ffffcc"><STRONG>
								<TABLE borderColor="#000099" height="44" width="584" align="left" border="0">
									<TR>
										<TD width="478">
											<DIV align="left"><SPAN class="style1"><STRONG><FONT color="#990000" size="3">Enter&nbsp;Password 
															To&nbsp;Retrieve Your File</FONT></STRONG>&nbsp;<FONT class="style1" color="#990000" size="5"><STRONG>
														</STRONG></FONT></SPAN></DIV>
										</TD>
										<TD width="134"><asp:textbox id="txtPassword" runat="server" MaxLength="25" Columns="15" OnTextChanged="txtPassword_TextChanged"
												TextMode="Password" Font-Size="Medium" BorderStyle="Solid" BorderColor="#C00000" BorderWidth="1px" Height="25px"
												Width="160px"></asp:textbox></TD>
										<TD width="112">
											<P align="left"><asp:button id="btnSubmit" tabIndex="1" runat="server" Font-Size="Small" Font-Bold="True" BorderStyle="Solid"
													BackColor="White" Text="Submit" ForeColor="Firebrick" BorderColor="#C00000" BorderWidth="1px"></asp:button></P>
										</TD>
									</TR>
								</TABLE>
							</STRONG></FONT>
					</td>
				</tr>
				<tr>
					<td width="186" bgColor="#cc9999" height="1">&nbsp;</td>
					<td width="16" bgColor="#cc9999" height="1">&nbsp;</td>
					<td width="498" bgColor="#cc9999" height="1">&nbsp;</td>
					<td width="90" bgColor="#cc9999" height="1">&nbsp;</td>
					<td width="90" bgColor="#cc9999" height="1"></td>
					<td width="178" bgColor="#cc9999" height="1"></td>
				</tr>
				<tr vAlign="top">
					<td align="right" width="3" colSpan="2" height="459"></td>
					<td vAlign="middle" align="center" colSpan="6" height="459"><span class="style1"><asp:label id="lblInfo" runat="server" BackColor="Transparent" BorderColor="Transparent" font-bold="True"
								font-size="Medium" font-names="Arial" width="371px" height="31px" forecolor="DarkRed"></asp:label>
						</span>
						<!--- My Content --></td>
				</tr>
				<tr>
					<td width="2" height="45">&nbsp;
					</td>
					<td colSpan="7" height="45">&nbsp;
					</td>
				</tr>
			</table>
		</form>
		<!-- End ImageReady Slices --><br>
		<br>
	</body>
</HTML>
