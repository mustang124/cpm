<%@ import Namespace="System.Data.Odbc" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="Utility" %>
<%@ import Namespace="System.IO" %>
<%@ import Namespace="System.Security.Cryptography" %>
<%@ Page Language="c#"  %>
<HTML>
	<HEAD>
		<title>Download Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<LINK href="styles.css" type="text/css" rel="stylesheet">
			<style type="text/css">
			.style1 { FONT-SIZE: 12px }
			.style2 { COLOR: #696969 }
			.style3 { FONT-SIZE: xx-small }
			.style15 { COLOR: #666666; FONT-SIZE: 12px }
			.style16 { FONT-SIZE: 10px }
			</style>
			<script runat="server">

    private String orderby ="order by posted";

   
    
    private const String CONNSTR = "Driver={Sql Server};" +
                                    "Server=10.10.41.13;Database=DLM;" +
                                    "UID=DownloadMgrUser;PWD=dlm6112009;";
    
    
    
    
    protected void  Page_Load(object sender, EventArgs e)
    {

      if (Session["orderby"] !=null)
          orderby= Session["orderby"].ToString(); 
              

      if (!IsPostBack)
        BindData();
    
        
     
    }
    
    protected void DataGrid2_EditCommand(object source, DataGridCommandEventArgs e)
    {
       DataGrid2.EditItemIndex = e.Item.ItemIndex;
       BindData();
    
    }
    
    
    protected void DataGrid2_Cancel(object source, DataGridCommandEventArgs e)
    {
        DataGrid2.EditItemIndex = -1;
        BindData();
       
    }
    
    
    protected void  DataGrid2_UpdateCommand(object source , DataGridCommandEventArgs e)
    {
        try
        {
           String failedLogins = ((TextBox) e.Item.Cells[6].Controls[0]).Text;
           String makePermanent = ((TextBox) e.Item.Cells[5].Controls[0]).Text;
           String email = ((TextBox) e.Item.Cells[3].Controls[0]).Text;
           String file =  ((TextBox) e.Item.Cells[4].Controls[0]).Text;
           String key = e.Item.Cells[2].Text.Trim();//id = mygrid.DataKeys[mygrid.EditItemIndex];
           String updateQry = "UPDATE FileKey SET email='"+email+"',filepath='"+ file +"' , LoginAttempts="+failedLogins +", DisabledAutoDelete="+ makePermanent +" WHERE filekey='"+key+"'";
    
           OdbcConnection conn = new OdbcConnection(CONNSTR);
           conn.Open();
    
           OdbcCommand updateCmd = new OdbcCommand(updateQry,conn);
           updateCmd.ExecuteNonQuery();
           DataGrid2.EditItemIndex = -1;
           BindData(conn);
           conn.Close();
    
         }
        catch (System.Exception ex)   // file IO errors
        {
           Response.Write(ex.Message);
        }
    }
    
    
    
    protected void DataGrid2_SortCommad(Object source, DataGridSortCommandEventArgs e)
    {
        String columnName  = e.SortExpression;
    
        if (e.SortExpression=="Posted" )
             columnName = "PostedDate";
    
        if (e.SortExpression=="Downloaded" )
             columnName = "DownloadDate";

        if (e.SortExpression=="File" )
             columnName = "FilePath";
    
        orderby =" order by " + columnName;
      
       if (Session["orderby"]!=null)
       		if (Session["orderby"].ToString().IndexOf(columnName) > 0 && Session["orderby"].ToString()== orderby)
	    		orderby += " DESC";   
  
        Session["orderby"] = orderby;
       
        BindData();
    }
    
    
    protected void DataGrid2_DeleteCommand(object source , DataGridCommandEventArgs e )
    {
        try
        {
    	   String strScript = @"<script>alert(""Help! I am being held captive by Wile E Coyote!"");<\script>";
			if (!Page.IsStartupScriptRegistered("MyScript") )
				Page.RegisterStartupScript("MyScript", strScript);
          //id = mygrid.DataKeys[mygrid.EditItemIndex];
       
          String delQry = "DELETE from FileKey Where filekey='"+e.Item.Cells[2].Text+"'";
    	   String[]  tokens= e.Item.Cells[4].Text.Split("\\".ToCharArray());
           String filename = tokens[tokens.Length -1 ]  ;

           if ( File.Exists("D:\\dlm_ftproot\\"+filename)) 
           {
           	File.Delete("D:\\dlm_ftproot\\"+filename);
           	lblKey.Text =filename + " was deleted ";
           }

           OdbcConnection conn = new OdbcConnection(CONNSTR);
           conn.Open();
    
           OdbcCommand delCmd = new OdbcCommand(delQry,conn);
           delCmd.ExecuteNonQuery();


           BindData(conn);
           conn.Close();
      
         }
        catch (System.Exception ex)   // file IO errors
        {
           Response.Write(ex.StackTrace);
        }
    }
    
    protected void DataGrid2_OnGridItemCreated(object sender, DataGridItemEventArgs e)
	{
		//e.Item.Cells[2].Visible = false;
    }
    
    protected  void DataGrid2_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataGrid2.CurrentPageIndex = e.NewPageIndex;
        BindData();
    }
    
    
    protected void DataGrid2_SelectedIndexChanged(object sender, EventArgs e)
    {
          // Let empty
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
       String strScript="";
       String message ="";
       Boolean  err = false;
    
       try
       {
         if (txtEmail.Text=="")
         {
            message = "Please enter an email";
            err= true;
         }
    
         if (txtPassword.Text=="")
         {
            message = "Please enter a password";
            err= true;
         }
    
         if (txtPasswordAgain.Text=="")
         {
            message = "Please enter the password again";
            err= true;
         }
    
    
         if (txtFile.PostedFile.FileName=="")
         {
            message = "Please enter a file name";
            err= true;
         }
    
         if (txtMaxDownload.Text=="")
         {
            message = "Please enter the maximum number of downloads";
            err= true;
         }
    
         if (txtPassword.Text != txtPasswordAgain.Text){
           message = "The passwords does not match. Please enter passwords again.";
           err= true;
         }
    
         if (!err)
         {
            SubmitData();
            UploadFile();
         }
         else
         {
    
            strScript = "<script language=JavaScript>";
            strScript+= "alert('"+ message + "')";
            strScript+= "</script"+">";
    
          if (!Page.IsStartupScriptRegistered("ClientScript"))
          {
             Page.RegisterStartupScript("ClientScript", strScript);
          }
         }
       }
       catch (System.Exception ex)   // file IO errors
       {
           Response.Write(ex.Message);
       }
    }
    
    protected void Item_Bound(Object sender, DataGridItemEventArgs e) 
	{
		try
		{
			DataView dv = (DataView)DataGrid2.DataSource;
			DataColumnCollection dc = dv.Table.Columns;
			int index = dc.IndexOf(dc["Key"])+2;
			e.Item.Cells[index].Visible = false;
		 }
       catch (System.Exception ex)   // file IO errors
       {
           Response.Write(ex.StackTrace);
       }
	}


    
    private void SubmitData()
    {
        String Url =  Request.ServerVariables["PATH_INFO"];
        String [] PathArray =  Request.ServerVariables["PATH_INFO"].Split('/');
        String genKey;
        String cmd = null;
        OdbcConnection conn = new OdbcConnection(CONNSTR);
        OdbcCommand iCommand = null;
    
        FileInfo fInfo = new FileInfo(txtFile.PostedFile.FileName);
        String fileName = System.IO.Path.GetFileName(txtFile.PostedFile.FileName);
    
       
    
        try
        {
            lblKey.Text="";
    
    
            fileName = CheckFilename(fileName);
    
            conn.Open();
            genKey = GenerateKey(conn);
    
            lblKey.Text = "Click here to download study: <br><br>";
            lblKey.Text += "http://webservices.solomononline.com/"+ PathArray[1]+"/Authenticate.aspx?key="+genKey;
            lblKey.Text += "<br>This link will be enabled for 30 days.<br>";
            lblKey.Text += "Your password will be sent to you in a separate email.";
    
    
            cmd = "Insert into FileKey(FileKey,FilePath,Email,Password,MaxDownloadCount,PostedDate,DisabledAutoDelete,DownloadCount) " +
                  "Values('"+
                  genKey + "','" +
                  "D:\\dlm_ftproot\\"+ fileName +"','"+
                  txtEmail.Text + "','" +
                  EncryptText(txtPassword.Text) +"',"+
                  Single.Parse(txtMaxDownload.Text.Trim()) +",'"+
                  DateTime.Now.ToString() +
                  "',"+ Convert.ToByte(Convert.ToBoolean(RadioButtonList1.SelectedItem.Value)) +",0)";
    
            

            iCommand = new OdbcCommand(cmd,conn);
    
            // Execute the SQL command
            int nNoAdded = iCommand.ExecuteNonQuery();
            BindData(conn);
            
            Logger.LoggedEntry("UPLOAD INFO -" +
                       fileName +" was uploaded on the server for "+
                       txtEmail.Text
                       );
        }
        catch (System.Exception ex)   // file IO errors
        {
           throw ex;
        }
	finally
	{
	   conn.Close();
           iCommand.Dispose();
	
	}
    
    
    }
    
    
    
    private String GenerateKey(OdbcConnection conn)
    {
        OdbcDataReader reader = null;
        OdbcCommand cCommand = new OdbcCommand("select Count(*) from FileKey",conn);
        MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
        Byte[] encryptedKey ;
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
    
        try
        {
            reader = cCommand.ExecuteReader();
            reader.Read();
            encryptedKey = encrypter.ComputeHash(encoder.GetBytes(txtEmail.Text + reader.GetInt32(0)+ DateTime.Now.ToShortTimeString() ));
            reader.Close();
        }
        catch(System.Exception ex)
        {
            throw ex;
        }
        return HexEncoding.ToString(encryptedKey);
    }
    
  
    
    
    private void BindData()
    {
        String genKey;
        String cmd = null;
    
        

        String sltQry = "select FileKey as [Key],Email,FilePath as [File],CAST(DisabledAutoDelete as int) as [Post Permanently], LoginAttempts as [Failed Logins],Cast(PostedDate AS datetime) as [Posted], Cast(DownloadDate AS datetime) as [Downloaded] from FileKey " + orderby;
        OdbcConnection conn = new OdbcConnection(CONNSTR);
    
        try
        {
            conn.Open();
    
            
            OdbcDataAdapter odbcAdapter= new OdbcDataAdapter(sltQry, conn);
            DataSet datSet = new DataSet();
    
            odbcAdapter.Fill(datSet);
           
            datSet.Tables[0].Columns["Key"].ReadOnly=true; //Prevents the key field from being updated
            datSet.Tables[0].Columns["Posted"].ReadOnly=true;//Prevents the date field from being updated
            datSet.Tables[0].Columns["Downloaded"].ReadOnly=true;  //Prevents the date field from being updated
    
    
            DataGrid2.DataSource = datSet.Tables[0].DefaultView;
            DataGrid2.VirtualItemCount = datSet.Tables[0].Rows.Count;
            DataGrid2.ItemStyle.Wrap=false;
            DataGrid2.DataKeyField="Key";
            
            DataGrid2.DataBind();
            //DataGrid2.Columns[2].Visble=false;
           // conn.Close();
    
        }
        catch (System.Exception ex)   // file IO errors
        {
           Response.Write(ex.StackTrace);
        }
        finally
        {
          conn.Close();
          
        }
    }
    
    
    private void BindData(OdbcConnection conn)
    {
        String genKey;
        String cmd = null;
        String sltQry = "select  FileKey as [Key],Email,FilePath as [File],CAST(DisabledAutoDelete as int) as [Post Permanently], LoginAttempts as [Failed Logins],CAST(PostedDate AS datetime) as [Posted], CAST(DownloadDate AS datetime) as [Downloaded] from FileKey " + orderby;
    
        try
        {
            
            OdbcDataAdapter odbcAdapter= new OdbcDataAdapter(sltQry, conn);
            DataSet datSet = new DataSet();
    
            datSet.Clear();
            odbcAdapter.Fill(datSet);
       
            datSet.Tables[0].Columns["Key"].ReadOnly=true; //Prevents the key field from being updated
            datSet.Tables[0].Columns["Posted"].ReadOnly=true;//Prevents the date field from being updated
            datSet.Tables[0].Columns["Downloaded"].ReadOnly=true;//Prevents the date field from being updated
    
    
            DataGrid2.DataSource = datSet.Tables[0].DefaultView;
            DataGrid2.VirtualItemCount = datSet.Tables[0].Rows.Count;
            DataGrid2.ItemStyle.Wrap=false;
            DataGrid2.DataBind();
         
        }
        catch (System.Exception ex)   // file IO errors
        {
           Response.Write(ex.StackTrace);
        }
    }
    
    
    private String EncryptText(String msg)
     {
        MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
        Byte[] encryptedKey ;
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
    
        try
        {
           encryptedKey = encrypter.ComputeHash(encoder.GetBytes(msg));
           return HexEncoding.ToString(encryptedKey);
    
        }
        catch(System.Exception ex)
        {
           throw ex;
        }
     }
    
    
    
    private void UploadFile()
    {
        try
        {
           FileInfo fInfo = new FileInfo(txtFile.PostedFile.FileName);
           String fileName = System.IO.Path.GetFileName(txtFile.PostedFile.FileName);
           //String dirName= fInfo.Directory.Name;
          // String tmpFileName;
          // String username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\')[0];
           //Int32 i =0;
    
    
           //if ((dirName.Length > 0) &amp;amp;&amp;amp; (fInfo.Directory.Root.Name != fInfo.Directory.Name)) {
           //      fileName = dirName +"_"+fileName;
           //}
    
    
          // tmpFileName = fileName;
          // while (File.Exists("D:\\dlm_ftproot\\"+fileName)){
           //   tmpFileName = i+"_"+ fileName;
    
          //    if (!File.Exists("D:\\dlm_ftproot\\"+ tmpFileName)) {
          //       fileName = tmpFileName;
          //    }
    
          //    i+=1;
          // }
    
           fileName = CheckFilename(fileName);
    
           if (txtFile.PostedFile != null)
           {
             if (txtFile.PostedFile.ContentLength <= 0)
             {
                lblKey.Text="";
                lblKey.Text=" Upload failed ";
             }
             else
             {
              //Save the file with the same name under the folder where the aspx file exists
                txtFile.PostedFile.SaveAs("D:\\dlm_ftproot\\"+ fileName);
             }
           }
       }
       catch(System.Exception ex)
       {
         throw ex;
       }
    }
    
    
    private String CheckFilename(String fileName)
    {
     String tmpFileName;
     Int32 i =0;
    
     tmpFileName = fileName;
     while (File.Exists("D:\\dlm_ftproot\\"+fileName)){
           tmpFileName = i+"_"+ fileName;
    
           if (!File.Exists("D:\\dlm_ftproot\\"+ tmpFileName)) {
                 fileName = tmpFileName;
           }
           i+=1;
      }
    
     return fileName;
    }
    
    

			</script>
	</HEAD>
	<body leftMargin="0" topMargin="0" marginheight="0" marginwidth="0" style="BACKGROUND:url(images/DownloadManager_Console.jpg) no-repeat">
		<!-- ImageReady Slices (template.psd) -->
		<table id="Table_01" height="662" cellSpacing="0" cellPadding="5" width="900" border="0">
			<tr>
				<td colSpan="2" height="69" rowSpan="2" width="391">&nbsp;
				</td>
				<td height="15">
				</td>
			</tr>
			<tr>
				<td height="22" align="right">
					<a href="http://webservices.solomononline.com/dlm/event.html" target="_blank"><FONT size="1">
							View Log</FONT></a></td>
			</tr>
			<tr vAlign="top">
				<td align="right" colSpan="2" width="425" height="100%">
					<p align="left" class="style3"><FONT color="#696969"></FONT>&nbsp;</p>
				</td>
				<td height="100%">
					<!-- My Content -->
					<form id="Form1" name="myForm" encType="multipart/form-data" runat="server">
						<table cellSpacing="0" cellPadding="0" width="538" align="right" border="0" height="424">
							<tbody>
								<tr>
									<td width="687" colSpan="2" height="218">
										<P align="left" class="style16">
											<table style="HEIGHT: 142px" height="168" cellSpacing="0" cellPadding="0" width="91%" align="center"
												border="0">
												<tbody>
													<tr>
														<td width="156" height="23"><span class="style16">Email																	    </span></td>
														<td vAlign="middle" width="377" height="23"><span class="style1"><strong><asp:textbox id="txtEmail" runat="server" Font-Size="XX-Small" BorderColor="Maroon" BorderWidth="1px"
																		BorderStyle="Solid"></asp:textbox><font color="#000000" size="1"></font></strong></span></td>
													</tr>
													<tr>
														<td width="156" height="23"><span class="style16">File															    </span></td>
														<td width="377"><span class="style1"><strong><input class="SmallFont" style="BORDER-BOTTOM:maroon 1px solid; BORDER-LEFT:maroon 1px solid; BORDER-TOP:maroon 1px solid; BORDER-RIGHT:maroon 1px solid"
																		id="txtFile" type="file" size="40" name="txtFile" runat="server" font-size="XX-Small">
																</strong>
															</span></td>
													</tr>
													<tr>
														<td width="156"><span class="style16">Password																    </span></td>
														<td class="SmallFont" width="377"><span class="style1"><strong><asp:textbox id="txtPassword" runat="server" Font-Size="XX-Small" Width="117px" Columns="25"
																		MaxLength="25" TextMode="Password" Font-Names="Arial" BorderColor="Maroon" BorderWidth="1px" BorderStyle="Solid"></asp:textbox></strong></span></td>
													</tr>
													<tr>
														<td width="156" height="17"><font face="Arial" class="style16">Re-enter Password </font>
														</td>
														<td width="377" height="17"><span class="style1"><asp:textbox id="txtPasswordAgain" runat="server" Font-Size="XX-Small" Width="117px" Columns="25"
																	MaxLength="25" TextMode="Password" Font-Names="Arial" BorderColor="Maroon" BorderWidth="1px" BorderStyle="Solid"></asp:textbox>
															</span></td>
													</tr>
													<tr>
														<td width="156" height="26"><span class="style16">Number of Allowed &nbsp;Downloads&nbsp; </span></td>
														<td width="377"><span class="style1"><asp:textbox id="txtMaxDownload" runat="server" Columns="2" value="1" Font-Size="XX-Small" BorderColor="Maroon"
																	BorderWidth="1px" BorderStyle="Solid"></asp:textbox>
															</span></td>
													</tr>
													<TR>
														<TD width="156" height="35"><span class="style16">Do you want to post the file 
                  post 
																		permanently?</span></TD>
														<TD width="377" height="35"><span class="style1"><asp:radiobuttonlist id="RadioButtonList1" runat="server" Font-Size="XX-Small" Width="72px" RepeatDirection="Horizontal"
																	BorderColor="Maroon">
																	<asp:ListItem Value="true">Yes</asp:ListItem>
																	<asp:ListItem Value="false" Selected="True">No</asp:ListItem>
																</asp:radiobuttonlist>
															</span></TD>
													</TR>
													<tr>
														<td align="center" colSpan="2"><span class="style1"><asp:button id="Button1" onclick="btnSubmit_Click" runat="server" Width="103px" Font-Names="Arial"
																	Height="21px" Text="Submit" Font-Size="12px" BorderStyle="Solid" BorderWidth="1px" BorderColor="Maroon"></asp:button>
															</span>
															<p align="center"><span class="style1"><asp:label id="lblKey" runat="server" font-size="XX-Small" forecolor="#0000C0" font-bold="True"
																		height="17px" font-names="Arial" width="100%"></asp:label>
																</span></p>
														</td>
													</tr>
												</tbody></table>
										</P>
									</td>
								</tr>
								<tr>
									<td vAlign="top" align="center" width="587" colSpan="2"><span class="style1">
											<asp:DataGrid ID="DataGrid2" runat="server" Font-Size="XX-Small" Font-Names="Arial" OnItemDataBound="Item_Bound"
												OnUpdateCommand="DataGrid2_UpdateCommand" OnCancelCommand="DataGrid2_Cancel" OnEditCommand="DataGrid2_EditCommand"
												OnDeleteCommand="DataGrid2_DeleteCommand" BorderStyle="Solid" BorderWidth="1px" CellPadding="2"
												BackColor="White" AllowPaging="True" AllowSorting="True" BorderColor="#CC9966" OnSelectedIndexChanged="DataGrid2_SelectedIndexChanged"
												OnPageIndexChanged="DataGrid2_PageIndexChanged" OnSortCommand="DataGrid2_SortCommad" Width="90%"
												GridLines="Horizontal" PageSize="6">
												<FooterStyle Wrap="False" ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
												<SelectedItemStyle Font-Bold="True" Wrap="False" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" BorderColor="Transparent"></AlternatingItemStyle>
												<ItemStyle Font-Size="XX-Small" ForeColor="#330099" Width="30%" CssClass="ColItem" BackColor="White"></ItemStyle>
												<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="#FFFFCC" CssClass="ColItem" BackColor="#990000"></HeaderStyle>
												<Columns>
													<asp:ButtonColumn Text="&lt;IMG SRC=images/delete.jpg Border=0 Width=12 Height=12&gt;" HeaderText="Delete"
														CommandName="Delete"></asp:ButtonColumn>
													<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" HeaderText="Edit" CancelText="Cancel"
														EditText="&lt;IMG SRC=images/Edit.gif Border=0 Width=12 Height=12&gt;"></asp:EditCommandColumn>
												</Columns>
												<PagerStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"
													Wrap="False" Mode="NumericPages"></PagerStyle>
											</asp:DataGrid>
										</span></td>
								</tr>
							</tbody></table>
					</form>
					<!--My Content --></td>
			</tr>
		</table>
		<!-- End ImageReady Slices -->
	</body>
</HTML>
