<%@ Page Language="C#"  Trace="False" %>
<%@ import Namespace="System.Security.Cryptography" %>
<%@ import Namespace="System.IO" %>
<%@ import Namespace="Utility" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.Odbc" %>

<script runat="server">

    void Page_Load (Object sender, EventArgs e)
         {
             //Response.Write(1);
             String filename = null;
             String connStr ="Driver={Sql Server};" +
                                    "Server=10.10.41.13;Database=DLM;" +
                                    "UID=DownloadMgrUser;PWD=dlm6112009;";
             OdbcConnection conn = new OdbcConnection(connStr);
             OdbcDataReader reader = null;
             OdbcCommand cCommand = new OdbcCommand("SELECT FilePath,MaxDownloadCount,DownloadCount," +
                                                     "Email FROM FileKey WHERE RTRIM(FileKey)='"+
                                                      Request.QueryString["key"].Trim() +
                                                      "'",conn);
             //Response.Write(145);
             int intByteSize = 1024 * 800; // send chunks of 800 kbytes
             int intOffset;
             double estTime;
             int intFileChunks;
             int intTotalFileDownLoaded = 0;
             int totalDownloads=0;
             int expectedDownloads =0;
             bool deleteIt = false;
             String email = null;
    
            
             try
              {

				if (Session["SAValid"]==null || ! ((Boolean) Session["SAValid"])){
					Response.Redirect("Authenticate.aspx?key="+ Request.QueryString["key"].Trim());
					Response.End();
				} 

                 conn.Open();
               
                 reader = cCommand.ExecuteReader();
                 
                 reader.Read();
                 
                 filename=reader.GetString(0);
                 
                 expectedDownloads = reader.GetInt32(1);
                 
                 totalDownloads=  reader.GetInt32(2)+ 1 ;
                  
                 email = reader.GetString(3);
               
    
                 Logger.LoggedEntry("< <a href=http://api.hostip.info/get_html.php?ip=" + Request.UserHostAddress + ">" +                                                                 Request.UserHostAddress + " </a> > DOWNLOAD NOTICE -" + email +" is downloading " + filename );

                 lblInfo.Text = filename;
                 reader.Close();
    
				//Flag row for deletion if total downloads reach its limit
                 if (totalDownloads >= expectedDownloads){
                    deleteIt = true;
                    
                 }
    
                OdbcCommand inCommand = new OdbcCommand("UPDATE FileKey SET DeleteLater="+ Convert.ToByte(Convert.ToBoolean(deleteIt)) + "," +  
                                                      "DownloadCount=" + totalDownloads  +
                                                      "WHERE RTRIM(FileKey)='"+
                                                       Request.QueryString["key"].Trim() +
                                                      "';" + 
                                                      "INSERT INTO Downloads (FileKey,ClientIP,DownloadDate,FilePath,Email) " + 
                                                             " VALUES('" + Request.QueryString["key"].Trim() + "','" + 
                                                             Request.ServerVariables["REMOTE_ADDR"] +"','" + 
                                                             DateTime.Now.ToString()+ "','" + filename + "','" + email + "');",conn);

                 
                /*OdbcCommand downloadCommand = new OdbcCommand("INSERT INTO Downloads (FileKey,ClientIP,DownloadDate) " + 
                                                             " VALUES('" + Request.QueryString["key"].Trim() + "','" + 
                                                             Request.ServerVariables["REMOTE_ADDR"] +"','" + 
                                                             DateTime.Now.ToString()+ "')", conn);
                Logger.LoggedEntry("INSERT INTO Downloads (FileKey,ClientIP,DownloadDate) " + 
                                     " VALUES('"+ Request.QueryString["key"].Trim() + "','" + 
                                     Request.ServerVariables["REMOTE_ADDR"] +"','" + 
                                                                DateTime.Now.ToString()+ "')");*/
    
                 System.IO.FileInfo fileToDownload = new System.IO.FileInfo(filename);
                 intFileChunks = (int)Math.Ceiling((double)(fileToDownload.Length/intByteSize));
    
                 String disHeader = "Attachment;Filename=\"" + Path.GetFileName(filename)+"\"" ;
    
                 // initialize the http content-disposition header to
                 // indicate a file attachment
                
		 Response.Clear();
                 Response.BufferOutput = true;
                 Response.AppendHeader("Content-Disposition", disHeader);
                 Response.AddHeader ("Content-Length", fileToDownload.Length.ToString());
    
    
                 // set the http content type to APPLICATION/OCTET-STREAM
                 Response.ContentType ="application/octet-stream";
                 Response.Flush();
    
                 int intCount;
                 for (intCount = 0; intCount < intFileChunks + 1; intCount++)
                 {
                     //want to use Response.IsClientConnected but it's
                     //freezing the system
    
                     if (Response.IsClientConnected)
                     {
                         intOffset = intCount * intByteSize;
    
                         //intByteSize is probably larger than the final chunk of
                         //data so figure how big that last chunk is so the end
                         //data size isn't past the end of the file
    
                         if ((intOffset + intByteSize) > (int)fileToDownload.Length)
                         {
                            intByteSize = (int)(fileToDownload.Length - intOffset);
                         }
                         
                         estTime = Math.Ceiling((double)(fileToDownload.Length/400000));
                         
			       if (estTime >= 1)
				    Application[Request.QueryString["key"]]= String.Format("{0:0.##}",  estTime/60) + " minutes ";
                         else
                            Application[Request.QueryString["key"]]= String.Format("{0:0.##}",  estTime) + " seconds ";
                            
                         Response.WriteFile(filename, intOffset, intByteSize);
                         intTotalFileDownLoaded = intOffset + intByteSize;
                         Response.Flush();
                     }
                     else
                     {
                         //If the file was not downloaded , do nothing
                         Logger.LoggedEntry("< <a href=http://www.geobytes.com/IpLocator.htm?GetLocation&IpAddress=" + Request.UserHostAddress + ">" +                                                                 Request.UserHostAddress + " </a> > DOWNLOAD NOTICE- " + email +
                                            " was disconnected. (" + 				    			           				                            intTotalFileDownLoaded + " bytes out of " + 
                                            fileToDownload.Length + " bytes)" );
						 break;
                     }
    
    
                     
    
                 }
    
               
                 // If the entire file is downloaded, marked file for deletion
                 if (intTotalFileDownLoaded >= fileToDownload.Length )
                 {  
                         inCommand.ExecuteReader(); //Marked file for deletion
			 //downloadCommand.ExecuteReader(); //Log Download
                         Logger.LoggedEntry("< <a href=http://www.geobytes.com/IpLocator.htm?GetLocation&IpAddress=" +                                            Request.UserHostAddress + ">" +                                                                                                      Request.UserHostAddress + " </a> >  DOWNLOAD NOTICE-" + 
                                             filename + " was successfully downloaded by " + email );
                 }
                 
                 reader.Close();
                 conn.Close();
   
              }
              catch (System.Exception ex)   // file IO errors
              {
               lblInfo.Text = "Unable to locate file. Please contact Solomon Asscociates for assistance.";
              }
              finally
              {
                  Application.Remove(Request.QueryString["key"]);
              }
    
         }

</script>
<html>
<head>
</head>
<body>
    <p align="center">
        <font face="Tahoma" size="6">Solomon Associates</font> 
    </p>
    <p align="center">
    </p>
    <p align="center">
    </p>
    <p align="center">
        <asp:Label id="lblInfo" runat="server" height="97px" width="679px" font-bold="True"></asp:Label>
    </p>
    <p align="center">
    </p>
    <p align="center">
    </p>
</body>
</html>
