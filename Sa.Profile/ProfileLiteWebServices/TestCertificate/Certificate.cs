﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Threading.Tasks;


namespace TestCertificate
{
    [Serializable]
	public class Certificate
	{
		public bool Get(string name, out X509Certificate2 certificate)
		{

            bool retVal = false;

			try
			{
                X509Store storeMy = new X509Store(StoreName.TrustedPeople, StoreLocation.LocalMachine);
				
				storeMy.Open(OpenFlags.ReadOnly);

                X509Certificate2Collection certColl = storeMy.Certificates.Find(X509FindType.FindBySubjectName, name, true);

                if (certColl.Count > 0)
                {
                    certificate = certColl[0];
                    storeMy.Close();
                    certColl.Clear();
                    retVal = true;
                }
                else
                {
                    certificate = null;
                    storeMy.Close();
                    certColl.Clear();
                }
                
                return retVal;
				
			}
			catch(Exception ex)
			{

				certificate = new X509Certificate2();
				return false;
			}
		}
	}
}