﻿namespace TestCertificate
{
    partial class TestCertificateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnCheckService = new System.Windows.Forms.Button();
			this.txtCheckService = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnGetCertificate = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.txtVerify = new System.Windows.Forms.TextBox();
			this.btnVerifyCertificate = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.lblCertResponse = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnGetRefNum = new System.Windows.Forms.Button();
			this.lblGetCert = new System.Windows.Forms.Label();
			this.txtClientKey = new System.Windows.Forms.TextBox();
			this.lblGetName = new System.Windows.Forms.Label();
			this.txtGetCertResponse = new System.Windows.Forms.TextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnTestNonRepudiationSigning = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCheckService
			// 
			this.btnCheckService.Location = new System.Drawing.Point(26, 25);
			this.btnCheckService.Name = "btnCheckService";
			this.btnCheckService.Size = new System.Drawing.Size(120, 23);
			this.btnCheckService.TabIndex = 0;
			this.btnCheckService.Text = "Check Service";
			this.btnCheckService.UseVisualStyleBackColor = true;
			this.btnCheckService.Click += new System.EventHandler(this.btnCheckService_Click);
			// 
			// txtCheckService
			// 
			this.txtCheckService.Location = new System.Drawing.Point(267, 27);
			this.txtCheckService.Name = "txtCheckService";
			this.txtCheckService.Size = new System.Drawing.Size(308, 20);
			this.txtCheckService.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(198, 29);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(58, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Response:";
			// 
			// btnGetCertificate
			// 
			this.btnGetCertificate.Location = new System.Drawing.Point(14, 51);
			this.btnGetCertificate.Name = "btnGetCertificate";
			this.btnGetCertificate.Size = new System.Drawing.Size(120, 23);
			this.btnGetCertificate.TabIndex = 3;
			this.btnGetCertificate.Text = "Get Certificate";
			this.btnGetCertificate.UseVisualStyleBackColor = true;
			this.btnGetCertificate.Click += new System.EventHandler(this.btnGetCertificate_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(199, 125);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(58, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Response:";
			// 
			// txtVerify
			// 
			this.txtVerify.Location = new System.Drawing.Point(255, 13);
			this.txtVerify.Multiline = true;
			this.txtVerify.Name = "txtVerify";
			this.txtVerify.Size = new System.Drawing.Size(308, 96);
			this.txtVerify.TabIndex = 7;
			// 
			// btnVerifyCertificate
			// 
			this.btnVerifyCertificate.Location = new System.Drawing.Point(17, 44);
			this.btnVerifyCertificate.Name = "btnVerifyCertificate";
			this.btnVerifyCertificate.Size = new System.Drawing.Size(120, 23);
			this.btnVerifyCertificate.TabIndex = 6;
			this.btnVerifyCertificate.Text = "Verify Certificate";
			this.btnVerifyCertificate.UseVisualStyleBackColor = true;
			this.btnVerifyCertificate.Click += new System.EventHandler(this.btnVerifyCertificate_Click);
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.lblCertResponse);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.btnGetRefNum);
			this.panel1.Controls.Add(this.lblGetCert);
			this.panel1.Controls.Add(this.txtClientKey);
			this.panel1.Controls.Add(this.lblGetName);
			this.panel1.Controls.Add(this.txtGetCertResponse);
			this.panel1.Controls.Add(this.btnGetCertificate);
			this.panel1.Location = new System.Drawing.Point(12, 64);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(586, 116);
			this.panel1.TabIndex = 9;
			// 
			// lblCertResponse
			// 
			this.lblCertResponse.AutoSize = true;
			this.lblCertResponse.Location = new System.Drawing.Point(251, 89);
			this.lblCertResponse.Name = "lblCertResponse";
			this.lblCertResponse.Size = new System.Drawing.Size(0, 13);
			this.lblCertResponse.TabIndex = 12;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(174, 89);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 11;
			this.label2.Text = "Response :";
			// 
			// btnGetRefNum
			// 
			this.btnGetRefNum.Location = new System.Drawing.Point(15, 14);
			this.btnGetRefNum.Name = "btnGetRefNum";
			this.btnGetRefNum.Size = new System.Drawing.Size(120, 23);
			this.btnGetRefNum.TabIndex = 10;
			this.btnGetRefNum.Text = "Get RefNum";
			this.btnGetRefNum.UseVisualStyleBackColor = true;
			this.btnGetRefNum.Click += new System.EventHandler(this.btnGetRefNum_Click);
			// 
			// lblGetCert
			// 
			this.lblGetCert.AutoSize = true;
			this.lblGetCert.Location = new System.Drawing.Point(174, 54);
			this.lblGetCert.Name = "lblGetCert";
			this.lblGetCert.Size = new System.Drawing.Size(69, 13);
			this.lblGetCert.TabIndex = 9;
			this.lblGetCert.Text = "Enter Name :";
			// 
			// txtClientKey
			// 
			this.txtClientKey.Location = new System.Drawing.Point(253, 16);
			this.txtClientKey.Name = "txtClientKey";
			this.txtClientKey.Size = new System.Drawing.Size(308, 20);
			this.txtClientKey.TabIndex = 8;
			// 
			// lblGetName
			// 
			this.lblGetName.AutoSize = true;
			this.lblGetName.Location = new System.Drawing.Point(155, 19);
			this.lblGetName.Name = "lblGetName";
			this.lblGetName.Size = new System.Drawing.Size(88, 13);
			this.lblGetName.TabIndex = 7;
			this.lblGetName.Text = "Enter Client Key :";
			// 
			// txtGetCertResponse
			// 
			this.txtGetCertResponse.Location = new System.Drawing.Point(252, 51);
			this.txtGetCertResponse.Name = "txtGetCertResponse";
			this.txtGetCertResponse.Size = new System.Drawing.Size(308, 20);
			this.txtGetCertResponse.TabIndex = 6;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnVerifyCertificate);
			this.panel2.Controls.Add(this.txtVerify);
			this.panel2.Location = new System.Drawing.Point(12, 186);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(586, 124);
			this.panel2.TabIndex = 11;
			// 
			// btnTestNonRepudiationSigning
			// 
			this.btnTestNonRepudiationSigning.Location = new System.Drawing.Point(26, 337);
			this.btnTestNonRepudiationSigning.Name = "btnTestNonRepudiationSigning";
			this.btnTestNonRepudiationSigning.Size = new System.Drawing.Size(222, 23);
			this.btnTestNonRepudiationSigning.TabIndex = 12;
			this.btnTestNonRepudiationSigning.Text = "Test Non Repudiation Signing";
			this.btnTestNonRepudiationSigning.UseVisualStyleBackColor = true;
			this.btnTestNonRepudiationSigning.Click += new System.EventHandler(this.btnTestNonRepudiationSigning_Click);
			// 
			// TestCertificateForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(610, 445);
			this.Controls.Add(this.btnTestNonRepudiationSigning);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtCheckService);
			this.Controls.Add(this.btnCheckService);
			this.Name = "TestCertificateForm";
			this.Text = "Certificate Test Form";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCheckService;
        private System.Windows.Forms.TextBox txtCheckService;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGetCertificate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtVerify;
        private System.Windows.Forms.Button btnVerifyCertificate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnGetRefNum;
        private System.Windows.Forms.Label lblGetCert;
        private System.Windows.Forms.TextBox txtClientKey;
        private System.Windows.Forms.Label lblGetName;
        private System.Windows.Forms.TextBox txtGetCertResponse;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblCertResponse;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnTestNonRepudiationSigning;
    }
}

