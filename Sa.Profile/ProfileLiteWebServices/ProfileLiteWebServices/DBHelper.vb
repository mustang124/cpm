Option Strict Off
Imports System.Data.SqlTypes
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text


Module DBHelper

    Private _RefineryID As String
    Public Property RefineryID() As String
        Get
            Return _RefineryID
        End Get
        Set(ByVal value As String)
            _RefineryID = value
        End Set
    End Property
    
#Region "Connection Strings"
    'Private CONNECTSTR10 As String = "packet size=4096;user id=ProfileFuels;password=ProfileFuels;data source=10.10.41.7;persist security info=False;initial catalog=ProfileFuels"
    Private CONNECTSTR10 As String = ConfigurationManager.ConnectionStrings("DbHelperConnectionString").ToString()
    'Private CONNECTSTR As String = "packet size=4096;user id=ProfileFuels;password=ProfileFuels;data source=10.10.41.7;persist security info=False;initial catalog=ProfileFuels12"
    Private CONNECTSTR As String = CONNECTSTR10.Replace("initial catalog=ProfileFuels", "initial catalog=ProfileFuels12")
#End Region

#Region "DeNull functions"
    '<summary>
    '   Returns object value if object is not null else null is returned.
    '</summary>
    ' <param name="obj">object</param>
    ' <param name="objType">native system type</param>
    '<returns>Object value is returned. If object is null,it returns 0. </returns>
    Public Function DeNull(ByVal obj As Object, ByVal objType As Type) As Object

        If IsDBNull(obj) Or IsNothing(obj) Then
            If objType.ToString = "System.String" Then
                Return String.Empty
            ElseIf objType.ToString = "System.DateTime" Then
                Return obj
            Else
                Return 0
            End If
        Else
            Return obj
        End If

    End Function


    '<summary>
    '   Returns object value if object is not null else null is returned.
    '</summary>
    ' <param name="row">DataTable's DataRow</param>
    ' <param name="colName"> Column name of  data row</param>
    '<returns>Object value is returned. If object is null,it returns 0. </returns>
    Public Function DeNull(ByVal row As DataRow, ByVal colName As String) As Object
        Dim colType As Type = row.Table.Columns(colName).DataType

        If row.IsNull(colName) Then
            If colType.ToString = "System.String" Then
                Return String.Empty
            ElseIf colType.ToString = "System.DateTime" Then
                Return DBNull.Value
            Else
                Return 0 'return 0 for numeric types
            End If
        Else
            Return row(colName)
        End If
    End Function



#End Region

#Region "QueryDb Functions"



    Function NonQueryDb(ByVal sqlString As String) As Integer


        Dim SqlConnection1 As SqlConnection = New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        If Not RefineryIs2012(_RefineryID) Then
            SqlConnection1 = New System.Data.SqlClient.SqlConnection(CONNECTSTR10)
        Else
            SqlConnection1 = New System.Data.SqlClient.SqlConnection(CONNECTSTR)
        End If

        Try
            SqlConnection1.Open()
            Dim sqlCmd As New SqlCommand(sqlString, SqlConnection1)
            Return sqlCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            SqlConnection1.Close()
        End Try

    End Function
    Function RefineryIs2012(RefineryID As String) As Boolean

        Dim Is2012 As String = False
        Dim cn As New SqlConnection()
        cn.ConnectionString = CONNECTSTR

        Dim ds As New DataSet

        Dim sqlstmt As String = "GetRefinery '" & RefineryID & "'"

        Dim da As SqlDataAdapter = New SqlDataAdapter(sqlstmt, cn)
        da.Fill(ds)
        If Not IsDBNull(ds) Then
            Is2012 = ds.Tables(0).Rows(0)(0).ToString
        End If

        cn.Close()
        If Is2012 = "0" Then
            Return False
        End If

        Return True
    End Function

    Function QueryDb(ByVal sqlString As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim SqlConnection1 As SqlConnection = New System.Data.SqlClient.SqlConnection
        '
        'SqlConnection1
        '
        If Not RefineryIs2012(_RefineryID) Then
            SqlConnection1 = New System.Data.SqlClient.SqlConnection(CONNECTSTR10)
        Else
            SqlConnection1 = New System.Data.SqlClient.SqlConnection(CONNECTSTR)
        End If
        'SqlConnection1.ConnectionString = CONNECTSTR

        Try
            SqlConnection1.Open()
            Dim da As New SqlDataAdapter(sqlString, SqlConnection1)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            SqlConnection1.Close()
        End Try

    End Function

    Public Function CleanText(strIn As String) As String
        On Error GoTo VBError

        Dim strIllegalChar As String
        Dim strChars As String
        Dim intPos As Integer
        Dim intCount As Integer
        Dim vLimit As Integer

        'strIn
        If strIn = "" Then
            Return strIn
        End If


        'double quote, percentage, single quotes, asterisks
        strChars = Chr(34) & Chr(37) & Chr(39) & Chr(42)
        vLimit = Len(strChars)
        'loop thru illegal chars
        For intCount = 1 To vLimit
            strIllegalChar = Mid$(strChars, intCount, 1)
            intPos = InStr(strIn, strIllegalChar)
            Do While intPos > 0
                'remove illegal chars
                strIn = Replace(strIn, strIllegalChar, "")
                intPos = InStr(strIn, strIllegalChar)
            Loop
        Next
        CleanText = strIn

        Exit Function
VBError:
        MsgBox("VBError in Sub Parse_SQL_Text : " & Err.Number & " - " & Err.Description)
        Resume Next
    End Function
#End Region
End Module
