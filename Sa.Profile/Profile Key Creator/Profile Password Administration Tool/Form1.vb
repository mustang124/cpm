﻿
Imports System
Imports System.IO
Imports System.Xml
Imports System.Data.SqlClient

Public Class Form1
    Private Const connstr = "user id=ProfileFuels;password=ProfileFuels;data source=""10.10.41.7"";persist security info=False;initial catalog=ProfileFuels"
    Private ProfileUtilities As ProfileLiteWebService.ProfileLiteWebServicesSoap

    Sub DisconnectProfileUtilities()
        ProfileUtilities = Nothing
    End Sub

    Sub ConnectProfileUtilities()
        Try
            If ProfileUtilities Is Nothing Then
                ProfileUtilities = New ProfileLiteWebService.ProfileLiteWebServicesSoapClient
            End If
        catch ex As Exception
            lblStatus.Text = ex.Message
        End Try

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim authorized As Boolean = False
        If CheckBox1.Checked Then
            Try
                authorized = ProfileUtilities.AuthorizeRefineryLogin(txtLogin.Text, txtPassword.Text)
            Catch ex As Exception
                lblStatus.Text = ex.Message
            End Try
        Else
            authorized = AuthorizePassword(txtLogin.Text, txtPassword.Text)
        End If
        lblStatus.Text = authorized.ToString().ToUpper()

    End Sub

    Public Function ChangeUserPassword(ByVal companyLogin As String, ByVal password As String, ByVal newpassword As String) As Boolean
        Dim conn As New SqlConnection(connstr)
        Dim result As SqlDataReader

        Dim cmd As New SqlCommand()
        Try
            conn.Open()
            cmd.Connection = conn
            cmd.CommandText = "spChangePassword"
            cmd.Parameters.Add(New SqlParameter("@RefineryID", companyLogin))
            cmd.Parameters.Add(New SqlParameter("@NewPassword", Encrypt(newpassword)))
            cmd.CommandType = CommandType.StoredProcedure
            result = cmd.ExecuteReader()
            If result.Read() Then
                Return IIf(result.GetInt32(0) = 1, True, False)
            End If

        Catch Ex As Exception
            lblStatus.Text = Ex.Message
            Return False
        End Try
        conn.Close()

        Return False

    End Function
    Public Function RetrievePassword(ByVal companyLogin As String) As String
        Dim conn As New SqlConnection(connstr)

        Dim result As SqlDataReader
        Dim cmd As New SqlCommand()
        Dim UnsaltedPW As String
        Dim strpassword As String = Nothing
        Try

            conn.Open()
            cmd.Connection = conn
            cmd.CommandText = "select refinerypwd from tsort where refineryid='" & companyLogin & "'"

            cmd.CommandType = CommandType.Text
            result = cmd.ExecuteReader()
            If result.Read() Then
                UnsaltedPW = result.GetString(0)
                strpassword = Decrypt(UnsaltedPW)
            End If


        Catch Ex As Exception
            lblStatus.Text = Ex.Message
            Return "NOT FOUND"
        End Try
        conn.Close()
        Return strpassword

    End Function
    Public Function AuthorizePassword(ByVal companyLogin As String, ByVal password As String) As Boolean
        Dim conn As New SqlConnection(connstr)

        Dim result As SqlDataReader
        Dim cmd As New SqlCommand()
        Dim UnsaltedPW As String
        Dim strpassword As String = Nothing
        Try

            conn.Open()
            cmd.Connection = conn
            cmd.CommandText = "spAuthorizePassword"
            cmd.Parameters.Add(New SqlParameter("@RefineryID", companyLogin))
            cmd.Parameters.Add(New SqlParameter("@Password", password))
            cmd.CommandType = CommandType.StoredProcedure
            result = cmd.ExecuteReader()
            If result.Read() Then
                UnsaltedPW = result.GetString(0)
                strpassword = Decrypt(UnsaltedPW)
            End If

            If password = strpassword Then Return True
        Catch Ex As Exception
            lblStatus.Text = Ex.Message
            Return False
        End Try
        conn.Close()
        Return False

    End Function
    Public Function GetSalt(ByVal companyLogin As String) As String

        Dim conn As New SqlConnection(connstr)
        Dim result As SqlDataReader
        Dim cmd As New SqlCommand()
        Dim salt As String = Nothing
        Try
            conn.Open()
            cmd.Connection = conn
            cmd.CommandText = "spGetSalt"
            cmd.Parameters.Add(New SqlParameter("@CompanyLogin", companyLogin))
            cmd.CommandType = CommandType.StoredProcedure
            result = cmd.ExecuteReader()
            If result.Read() Then
                salt = result.GetString(0)
                Return salt
            End If
        Catch Ex As Exception
            lblStatus.Text = Ex.Message
            Return ""
        End Try
        conn.Close()
        Return ""

    End Function
   

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        lblStatus.Text = ""
        If CheckBox1.Checked Then
            Try
                If ProfileUtilities.ChangeRefineryPassword(txtLogin.Text, txtPassword.Text, txtNewPW.Text, txtNewPW.Text) Then
                    lblStatus.Text = "CHANGED TO " & txtNewPW.Text
                Else
                    lblStatus.Text = "NOT CHANGED"
                End If
            Catch ex As Exception
                lblStatus.Text = ex.Message
                Exit Sub
            End Try
        Else
            If ChangeUserPassword(txtLogin.Text, txtPassword.Text, txtNewPW.Text) Then
                lblStatus.Text = "CHANGED TO " & txtNewPW.Text
            Else
                lblStatus.Text = "NOT CHANGED"
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        lblStatus.Text = ""
        ProfileUtilities = Nothing
        End
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblStatus.Text = ""
        If CheckBox1.Checked Then ConnectProfileUtilities()
        cboLength.Text = "8"
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        lblStatus.Text = ""
        txtPassword.Text = RetrievePassword(txtLogin.Text)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        lblStatus.Text = ""
        txtNewPW.Text = RandomModule.RandomPassword.Generate(cboLength.Text)
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            ConnectProfileUtilities()
            Label2.Text = "CLIENT KEY:"
        Else
            DisconnectProfileUtilities()
            Label2.Text = "REFINERY ID:"
        End If
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs)
        Dim id As String = Encrypt(txtLogin.Text)
        lblStatus.Text = id
        Dim ref As String = GetRefineryID(id)
    End Sub
    Public Shared Function VerifyKey(ByVal token As String) As Boolean
        ' This is a very simple manager.
        ' In most production systems the following code
        ' typically consults an external database of (username,password) pairs where
        ' the password is often not the real password but a password equivalent
        ' (for example, the hash of the password). Provided that both client and
        ' server can generate the same value for a particular username, there is
        ' no requirement that the password be the actual password for the user.
        ' For this sample the password is simply the reverse of the username.

        'Return "password"
        Dim key, company, location As String
        Dim locIndex As Integer
        Try
            company = Decrypt(token).Split("$".ToCharArray)(0)

            locIndex = Decrypt(token).Split("$".ToCharArray).Length - 2
            location = Decrypt(token).Split("$".ToCharArray)(locIndex)

            'Try new way 
            If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                Dim reader As New StreamReader("C:\\ClientKeys\\" + company + "_" + location + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return True 'ceate mini key 
                End If


            ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                Dim reader As New StreamReader("D:\\ClientKeys\\" + company + "_" + location + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return True 'ceate mini key 
                End If

            End If


            '-------Try the old way --------
            ' Will be deleted later
            If File.Exists("C:\\ClientKeys\\" + company + ".key") Then
                Dim reader As New StreamReader("C:\\ClientKeys\\" + company + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return True 'ceate mini key 
                End If
            ElseIf File.Exists("D:\\ClientKeys\\" + company + ".key") Then
                Dim reader As New StreamReader("D:\\ClientKeys\\" + company + ".key")
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return True 'ceate mini key 
                End If
            End If



            '----------------------------------------
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function 'AuthenticateToken
    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        If VerifyKey(txtLogin.Text.Trim) Then
            lblStatus.Text = GetRefineryID(txtLogin.Text.Trim)
        End If

    End Sub
End Class
