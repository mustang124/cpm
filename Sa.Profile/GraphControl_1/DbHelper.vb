Imports System.Data.SqlClient
Imports System.IO

Module DbHelper

    Const CONNECTIONSTR As String = "packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
        "1.7"";persist security info=True;initial catalog=ProfileFuels;password=ProfileFu" & _
        "els"

    Const CONNECTIONSTR12 As String = "packet size=4096;user id=ProfileFuels;data source=""10.10.4" & _
        "1.7"";persist security info=True;initial catalog=ProfileFuels12;password=ProfileFu" & _
        "els"

    Private Sub WriteLog(ByVal st As String)
        'Dim sw As StreamWriter = New StreamWriter("d:\inetpub\refinerycharts\chart.log", True)
        'sw.WriteLine(Now.ToString & ": " & st)
        'sw.Flush()
        'sw.Close()
    End Sub
    Private Function Is2012(ByVal refid As String) As Boolean
        Dim ret As Boolean = False
        Dim SqlConnection1 As New System.Data.SqlClient.SqlConnection
        Dim cmd As New SqlCommand
        SqlConnection1.ConnectionString = CONNECTIONSTR12
        SqlConnection1.Open()

        cmd.Connection = SqlConnection1
        cmd.CommandText = "GetRefinery"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add(New SqlParameter("@RefineryID", refid))

        Dim i As Integer = Convert.ToInt32(cmd.ExecuteScalar)

        If i = 1 Then
            ret = True
        End If
        Return ret
    End Function
    Public Function QueryDb(ByVal sqlString As String, ByVal refineryid As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim SqlConnection1 As New System.Data.SqlClient.SqlConnection

        '
        'SqlConnection1
        '
        WriteLog("QueryDB - RefineryID:" & refineryid)
        If Is2012(refineryid) Then
            SqlConnection1.ConnectionString = CONNECTIONSTR12
        Else
            SqlConnection1.ConnectionString = CONNECTIONSTR
        End If
        WriteLog("ConnectionID:" & SqlConnection1.ConnectionString)


        'Reads only the first sql statement. 
        'It's to gaurd against attacks on the data
        Try
            If sqlString.Trim().StartsWith("SELECT") Then
                Dim da As SqlDataAdapter = New SqlDataAdapter(sqlString, SqlConnection1)
                da.Fill(ds)
            End If
        Catch ex As Exception
            Throw New Exception("An database error just occurred." + sqlString)
        Finally
            SqlConnection1.Close()
        End Try

        Return ds

    End Function

End Module
