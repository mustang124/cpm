﻿CREATE FUNCTION [dbo].[GetAvgOCCAbsPcnt](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS real
AS
BEGIN
	DECLARE @AbsPcnt real, @AbsHrs float, @STH float

	SELECT @AbsHrs = OCCAbs FROM dbo.SumAbsenceTot(@RefineryID, @Dataset, @StartDate, @EndDate)
	SELECT @STH = STH FROM dbo.SumPersSection(@RefineryID, @Dataset, @StartDate, @EndDate, 'TO')
	
	IF @STH > 0
		SELECT @AbsPcnt = @AbsHrs/@STH*100
	ELSE
		SET @AbsPcnt = NULL
		
	RETURN @AbsPcnt
	END
