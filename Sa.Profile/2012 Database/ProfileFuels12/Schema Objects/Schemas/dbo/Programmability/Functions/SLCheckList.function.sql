﻿
CREATE FUNCTION [dbo].[SLCheckList](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT NumRefs = COUNT(DISTINCT RefineryID), MIN(PeriodStart) AS PeriodStart, MAX(PeriodEnd) AS PeriodEnd
		, DATEDIFF(d, MIN(PeriodStart), MAX(PeriodEnd)) AS NumDays, DATEDIFF(d, MIN(PeriodStart), MAX(PeriodEnd))/365.0 AS FractionOfYear
		, NumSubmissions = COUNT(*), NumMonths = COUNT(DISTINCT PeriodYear*100+PeriodMonth)
		, ValidList = CASE WHEN COUNT(DISTINCT RefineryID) * COUNT(DISTINCT PeriodYear*100+PeriodMonth) = COUNT(*) AND DATEDIFF(m, MIN(PeriodStart), MAX(PeriodEnd)) = COUNT(DISTINCT PeriodYear*100+PeriodMonth) THEN 'Y' ELSE 'N' END
	FROM Submissions s INNER JOIN @SubmissionList l ON l.SubmissionID = s.SubmissionID
)
