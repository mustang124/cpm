﻿CREATE FUNCTION [dbo].[SumEnergyCons](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT TotEnergyConsMBTU = SUM(TotEnergyConsMBTU), PurTotMBTU = SUM(PurTotMBTU), ProdTotMBTU = SUM(ProdTotMBTU)
		, PurFGMBTU = SUM(PurFGMBTU), PurLiquidMBTU = SUM(PurLiquidMBTU), PurSolidMBTU = SUM(PurSolidMBTU), PurSteamMBTU = SUM(PurSteamMBTU), PurThermMBTU = SUM(PurThermMBTU), PurPowerMBTU = SUM(PurPowerMBTU)
		, ProdFGMBTU = SUM(ProdFGMBTU), ProdOthMBTU = SUM(ProdOthMBTU), ProdPowerAdj = SUM(ProdPowerAdj)
		, PurPowerConsMWH = SUM(PurPowerConsMWH), ProdPowerConsMWH = SUM(ProdPowerConsMWH), TotPowerConsMWH = SUM(TotPowerConsMWH)
	FROM EnergyTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND PeriodStart >= @StartDate AND PeriodStart < @EndDate
)
