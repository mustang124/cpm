﻿CREATE FUNCTION [dbo].[GetPeriodSubmissions](@RefineryID varchar(6), @Dataset varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT s.SubmissionID, s.RefineryID, s.DataSet, s.PeriodStart, s.PeriodEnd, s.NumDays, s.FractionOfYear
	FROM Submissions s 
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
	AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate
	AND s.UseSubmission = 1
)
