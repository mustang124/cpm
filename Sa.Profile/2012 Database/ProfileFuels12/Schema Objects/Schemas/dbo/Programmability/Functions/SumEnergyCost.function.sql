﻿CREATE FUNCTION [dbo].[SumEnergyCost](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT Currency
		, PurFGCostK = SUM(PurFGCostK)
		, PurLiquidCostK = SUM(PurLiquidCostK)
		, PurSolidCostK = SUM(PurSolidCostK)
		, PurSteamCostK = SUM(PurSteamCostK)
		, PurThermCostK = SUM(PurThermCostK)
		, PurPowerCostK = SUM(PurPowerCostK)
		, PurTotCostK = SUM(PurTotCostK)
		, ProdFGCostK = SUM(ProdFGCostK)
		, ProdOthCostK = SUM(ProdOthCostK)
		, ProdTotCostK = SUM(ProdTotCostK)
		, TotCostK = SUM(TotCostK)
	FROM dbo.EnergyTotCost c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND PeriodStart >= @StartDate AND PeriodStart < @EndDate
	AND c.Scenario = 'CLEINT'
	GROUP BY Currency
)
