﻿CREATE FUNCTION [dbo].[SLSumOpex](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet, @Currency varchar(4))
RETURNS TABLE
AS
RETURN (
	WITH SumOpex AS 
	(
		SELECT a.Currency, OCCSal = SUM(a.OCCSal), MPSSal = SUM(a.MPSSal), OCCBen = SUM(a.OCCBen), MPSBen = SUM(a.MPSBen), MaintMatlST = SUM(a.MaintMatlST), ContMaintLaborST = SUM(a.ContMaintLaborST), TAAdj = SUM(a.TAAdj), Envir = SUM(a.Envir), OthNonVol = SUM(a.OthNonVol), STNonVol = SUM(a.STNonVol)
		, STVol = SUM(a.STVol), TotCashOpEx = SUM(a.TotCashOpex), PersCostExclTA = SUM(a.PersCostExclTA), EnergyCost = SUM(a.EnergyCost), NEOpex = SUM(a.NEOpex)
		, OthRevenue = SUM(a.OthRevenue)
		FROM OpexAll a INNER JOIN @SubmissionList s ON s.SubmissionID = a.SubmissionID   
		WHERE a.DataType = 'ADJ' AND a.Scenario = 'CLIENT' AND a.Currency = ISNULL(@Currency, a.Currency)
		GROUP BY a.Currency
	)
	SELECT ta.FactorSet, o.Currency, o.OCCSal, o.MPSSal, o.OCCBen, o.MPSBen, o.MaintMatlST, o.ContMaintLaborST
		, ta.PeriodTAAdj AS TAAdj
		, o.Envir, o.OthNonVol
		, STNonVol = o.STNonVol - o.TAAdj + ta.PeriodTAAdj
		, o.STVol
		, TotCashOpEx = o.TotCashOpEx - o.TAAdj + ta.PeriodTAAdj
		, o.PersCostExclTA, o.EnergyCost
		, NEOpex = o.NEOpex - o.TAAdj + ta.PeriodTAAdj
		, OthRevenue
	FROM SumOpex o INNER JOIN dbo.SLTAAdj(@SubmissionList, @FactorSet, @Currency) ta ON ta.Currency = o.Currency
)


