﻿CREATE FUNCTION [dbo].[GetSubmissionInfo](@SubmissionID int)
RETURNS TABLE
AS
RETURN (
	SELECT RefineryID, DataSet, PeriodStart, PeriodEnd, NumDays, FractionOfYear, RptCurrency, RptCurrencyT15, UOM, CalcsNeeded, Submitted, LastCalc
	FROM Submissions
	WHERE SubmissionID = @SubmissionID 
	)
