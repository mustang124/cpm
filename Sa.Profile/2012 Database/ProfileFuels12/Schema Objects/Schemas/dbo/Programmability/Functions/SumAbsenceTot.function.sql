﻿CREATE FUNCTION [dbo].[SumAbsenceTot](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT OCCAbs = SUM(a.OCCAbs), MPSAbs = SUM(a.MPSAbs), TotAbs = SUM(a.TotAbs)
	FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartDate, @EndDate) s 
	INNER JOIN AbsenceTot a ON a.SubmissionID = s.SubmissionID
	)
