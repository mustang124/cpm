﻿
CREATE  FUNCTION [dbo].[SLAverageOpex](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode)
RETURNS @Opex TABLE (FactorSet varchar(8), Scenario dbo.Scenario, Currency dbo.CurrencyCode, TotCashOpexUEDC real NULL, VolOpexUEDC real NULL, NonVolOpexUEDC real NULL, 
	NEOpexUEDC real NULL, NEOpexEDC real NULL, NEI real NULL, TotCashOpexBbl real NULL)
AS
BEGIN

DECLARE @OpexScenario dbo.Scenario; SET @OpexScenario = 'CLIENT' -- Using Client energy prices for all pricing scenarios

; WITH OpexByDataType AS (
	SELECT FactorSet, Currency, Scenario, DataType, TotCashOpex, STNonVol, STVol, NEOpex, TAAdj 
	FROM dbo.SLAverageOpexByDataType(@SubmissionList, @FactorSet, @OpexScenario, @Currency, NULL))
INSERT INTO @Opex (FactorSet, Scenario, Currency, NEOpexEDC, TotCashOpexUEDC, VolOpexUEDC, NonVolOpexUEDC, NEOpexUEDC, NEI, TotCashOpexBbl)
SELECT e.FactorSet, e.Scenario, e.Currency, e.NEOpex, u.TotCashOpex, u.STVol, u.STNonVol, u.NEOpex, nei.NEOpex, bbl.TotCashOpex
FROM OpexByDataType e
LEFT JOIN OpexByDataType u ON u.FactorSet = e.FactorSet AND u.Scenario = e.Scenario AND u.Currency = e.Currency AND u.DataType = 'UEDC'
LEFT JOIN OpexByDataType nei ON nei.FactorSet = e.FactorSet AND nei.Scenario = e.Scenario AND nei.Currency = e.Currency AND nei.DataType = 'NEI'
LEFT JOIN OpexByDataType bbl ON bbl.FactorSet = e.FactorSet AND bbl.Scenario = e.Scenario AND bbl.Currency = e.Currency AND bbl.DataType = 'BBL'
WHERE e.DataType = 'EDC'

RETURN
END

