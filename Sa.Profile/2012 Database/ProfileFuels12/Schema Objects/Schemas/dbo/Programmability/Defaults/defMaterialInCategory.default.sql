﻿CREATE DEFAULT [dbo].[defMaterialInCategory]
    AS 0;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInRMI]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInOTHRM]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInRCHEM]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInRLUBE]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInPROD]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInFLUBE]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInFCHEM]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInASP]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInCOKE]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInSOLV]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[AllowInMPROD]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[Material_LU].[LubesOnly]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defMaterialInCategory]', @objname = N'[dbo].[MaterialInCategory]';

