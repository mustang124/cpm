﻿CREATE       PROC [dbo].[spReportGPVCategories] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod, Currency = @Currency,
RTRIM(m.Category) as Category,
MaterialName = CASE m.Category 
		WHEN 'MPROD' THEN 'Miscellaneous Products' 
		WHEN 'ASP' THEN 'Asphalt' 
		WHEN 'SOLV' THEN 'Speciality Solvents' 
		WHEN 'FCHEM' THEN 'Ref. Feedstocks To Chemical Plant' 
		WHEN 'FLUBE' THEN 'Ref. Feedstocks To Lube Refining' 
		WHEN 'COKE' THEN 'Saleable Petroleum Coke (FOE)' 
		WHEN 'OTHRM' THEN 'Other Raw Materials' 
	END,  
ISNULL(m.NetBBL,0) as Bbl, 
ISNULL(m.NetValueMUS,0)*1000000/ISNULL(m.NetBBL,1)*dbo.ExchangeRate('USD', @Currency, s.PeriodStart) as PricePerBbl, 
ISNULL(m.NetValueMUS,0)*1000*dbo.ExchangeRate('USD', @Currency, s.PeriodStart) As Value
FROM MaterialSTCalc m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE m.Scenario=@Scenario AND m.NetBBL > 0 AND m.Category in ('ASP', 'COKE', 'FCHEM', 'FLUBE', 'MPROD', 'SOLV', 'OTHRM') 
And s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth)
ORDER BY s.PeriodStart DESC
