﻿CREATE  PROC [dbo].[spReportDynamic](@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15),
	@FactorSet varchar(8), @Scenario varchar(8), @Currency varchar(4), @UOM varchar(5),
	@ReportSet smallint, @IncludeTarget bit, @IncludeYTD bit, @IncludeAvg bit)
AS

SET NOCOUNT ON

DECLARE @FieldList varchar(4000)
DECLARE @DataTable varchar(50), @TotField varchar(50), @TargetField varchar(50), @AvgField varchar(50), @YTDField varchar(50)
DECLARE cVars CURSOR LOCAL FAST_FORWARD
FOR	SELECT DataTable, TotField, ISNULL(TargetField, ''), ISNULL(AvgField, ''), ISNULL(YTDField, '')
	FROM Chart_LU
	WHERE SortKey BETWEEN (@ReportSet*100) AND (@ReportSet*100+99)
	ORDER BY SortKey
OPEN cVars
SET @FieldList = ''
FETCH NEXT FROM cVars INTO @DataTable, @TotField, @TargetField, @AvgField, @YTDField 
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @TotField <> ''
		SELECT @FieldList = @FieldList + ', ' + RTRIM(@DataTable) + '.' + @TotField
	IF (@TargetField <> '') AND (@IncludeTarget = 1)
		SELECT @FieldList = @FieldList + ', ' + RTRIM(@DataTable) + '.' + @TargetField
	IF (@YTDField <> '') AND (@IncludeYTD = 1)
		SELECT @FieldList = @FieldList + ', ' + RTRIM(@DataTable) + '.' + @YTDField
	IF (@AvgField <> '') AND (@IncludeAvg = 1)
		SELECT @FieldList = @FieldList + ', ' + RTRIM(@DataTable) + '.' + @AvgField
	SELECT @FieldList = @FieldList + CHAR(13)
	FETCH NEXT FROM cVars INTO @DataTable, @TotField, @TargetField, @AvgField, @YTDField 
END
CLOSE cVars
DEALLOCATE cVars

DECLARE @StartSQL varchar(4000), @EndSQL varchar(4000)
SELECT @StartSQL = 'DECLARE @RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15), ' + CHAR(13)
SELECT @StartSQL = @StartSQL + '@FactorSet varchar(8), @Scenario varchar(8), @Currency varchar(4), @UOM varchar(5) ' + CHAR(13)
SELECT @StartSQL = @StartSQL + 'SET @RefineryID = ''' + @RefineryID + '''' + CHAR(13)
IF @PeriodYear IS NOT NULL
	SELECT @StartSQL = @StartSQL + 'SET @PeriodYear = ' + CAST(@PeriodYear AS varchar(4)) + CHAR(13)
IF @PeriodMonth IS NOT NULL
	SELECT @StartSQL = @StartSQL + 'SET @PeriodMonth = ' + CAST(@PeriodMonth AS varchar(2)) + CHAR(13)
SELECT @StartSQL = @StartSQL + 'SET @DataSet = ''' + @DataSet + '''' + CHAR(13)
SELECT @StartSQL = @StartSQL + 'SET @FactorSet = ''' + @FactorSet + '''' + CHAR(13)
SELECT @StartSQL = @StartSQL + 'SET @Scenario = ''' + @Scenario + '''' + CHAR(13)
SELECT @StartSQL = @StartSQL + 'SET @Currency = ''' + @Currency + '''' + CHAR(13)
SELECT @StartSQL = @StartSQL + 'SET @UOM = ''' + @UOM + '''' + CHAR(13)
SELECT @StartSQL = @StartSQL + 'SELECT Submissions.Location, Submissions.PeriodStart, Submissions.PeriodEnd, Gensum.DaysInPeriod, Gensum.Currency, Gensum.UOM, Gensum.EDC, Gensum.UEDC' + CHAR(13)
SELECT @EndSQL = ' FROM Submissions INNER JOIN Gensum ON Gensum.SubmissionID = Submissions.SubmissionID ' + CHAR(13)
SELECT @EndSQL = @EndSQL + ' LEFT JOIN MaintAvailCalc ON MaintAvailCalc.SubmissionID = Gensum.SubmissionID AND MaintAvailCalc.FactorSet = Gensum.FactorSet ' + CHAR(13)
SELECT @EndSQL = @EndSQL + ' LEFT JOIN MaintIndex ON MaintIndex.SubmissionID = Gensum.SubmissionID AND MaintIndex.FactorSet = Gensum.FactorSet AND MaintIndex.Currency = Gensum.Currency ' + CHAR(13)
SELECT @EndSQL = @EndSQL + ' WHERE Submissions.RefineryID = @RefineryID AND Submissions.PeriodYear = ISNULL(@PeriodYear, Submissions.PeriodYear)  AND Submissions.PeriodMonth = ISNULL(@PeriodMonth, Submissions.PeriodMonth) ' + CHAR(13)
SELECT @EndSQL = @EndSQL + ' AND Gensum.FactorSet = @FactorSet AND Gensum.UOM = @UOM AND Gensum.Currency = @Currency AND Gensum.Scenario = @Scenario' + CHAR(13)
SELECT @EndSQL = @EndSQL + ' ORDER BY Submissions.PeriodStart DESC' + CHAR(13)

SET NOCOUNT OFF
EXEC (@StartSQL + @FieldList + @EndSQL)
