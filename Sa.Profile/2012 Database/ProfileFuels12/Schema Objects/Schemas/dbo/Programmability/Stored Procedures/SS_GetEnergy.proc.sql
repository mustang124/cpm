﻿CREATE PROC [dbo].[SS_GetEnergy]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
	
AS

SELECT RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) As TransferTo, RTRIM(e.EnergyType) as EnergyType,
            e.TransCode,e.RptSource,e.RptPriceLocal,elu.SortKey,e.Hydrogen,e.Methane,
            e.Ethane,e.Ethylene,e.Propane,e.Propylene,e.Butane,e.Isobutane,e.C5Plus,e.CO,e.CO2,e.N2,e.Inerts 
             FROM dbo.Energy e ,Energy_LU elu WHERE 
            elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND  elu.EnergyType=e.EnergyType AND elu.SortKey < 100 AND 
              (SubmissionID IN 
             (SELECT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd))))
