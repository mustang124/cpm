﻿CREATE PROC [dbo].[spLogActivity]
@RefineryID varchar(6), 
@CallerIP varchar(20), 
@Activity varchar(max), 
@Service varchar(50), 
@Method varchar(50), 
@Parameters varchar(200), 
@Status varchar(50) 
AS
INSERT INTO dbo.ActivityLog(ActivityTime, RefineryID, CallerIP, Activity,  [Service], Method, [Parameters], Status)
VALUES(GETDATE(),@RefineryID, @CallerIP, @Activity,  @Service, @Method, @Parameters, @Status)
