﻿CREATE   PROC [dbo].[spReportRosneftKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'RUB', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet

IF @SubmissionID IS NULL
BEGIN
	--RAISERROR (N'Data has not been uploaded for this month.', -- Message text.
 --          10, -- Severity,
 --          1 --State
 --          );
	RETURN 1
END
ELSE IF @CalcsNeeded IS NOT NULL
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
/*SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)
SELECT @StartQTR = dbo.BuildDate(DATEPART(yy, DATEADD(dd, -1, @PeriodEnd)), DATEPART(QUARTER, DATEADD(dd, -1, @PeriodEnd))*3-2, 1)
SELECT @EndQuarter = DATEADD(mm, 3, @StartQTR)*/
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p

--DECLARE @PeriodResults TABLE (
--	Period varchar(3) NOT NULL,
--	NumMonths tinyint NULL,
--	RefUtilPcnt real NULL, 
--	EDC real NULL,
--	UEDC real NULL,
--	UtilUEDC real NULL,
--	ProcessUtilPcnt real NULL,
--	TotProcessEDC real NULL,
--	TotProcessUEDC real NULL,	
--	EII real NULL,
--	EnergyUseDay real NULL,
--	TotStdEnergy real NULL,
--	ProcessEffIndex real NULL,
--	VEI real NULL,
--	ReportLossGain real NULL,
--	EstGain real NULL,
--	NetInputBPD real NULL,
--	Gain real NULL,
--	RawMatl real NULL,
--	ProdYield real NULL,
--	OpAvail real NULL,
--	MechUnavailTA real NULL,
--	NonTAUnavail real NULL,
--	PersIndex real NULL,
--	NonMaintWHr real NULL,
--	TotMaintForceWHr real NULL,
--	TotMaintForceWHrEDC real NULL,
--	MaintTAWHr real NULL,
--	MaintNonTAWHr real NULL,
--	MaintIndex real NULL,
--	AnnTACost real NULL,
--	RoutCost real NULL,
--	NEOpexEDC real NULL,
--	NEOpex real NULL,
--	OpexUEDC real NULL,
--	OpexEDC real NULL,
--	EnergyCost real NULL,
--	TotCashOpex real NULL)


----- Everything Already Available in Gensum
--INSERT @PeriodResults (Period, NumMonths, RefUtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, NetInputBPD, Gain, PersIndex, EDC, UEDC
--	, TotMaintForceWHrEDC, MaintIndex, NEOpexEDC, OpexUEDC)
--SELECT Period = 'MO', NumMonths = 1, g.UtilPcnt, g.ProcessUtilPcnt, g.OpAvail, g.EII, g.VEI, g.NetInputBPD*1000.0, g.GainPcnt, g.TotWHrEDC, g.EDC, g.UEDC
--	, g.TotMaintForceWHrEDC, g.RoutIndex + g.TAIndex_Avg, g.NEOpexEDC, g.TotCashOpexUEDC
--FROM Gensum g
--WHERE g.SubmissionID = @SubmissionID AND g.FactorSet = @FactorSet AND g.Currency = @Currency AND g.UOM = @UOM AND g.Scenario = 'CLIENT'

--INSERT @PeriodResults (Period, NumMonths, RefUtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, NetInputBPD, Gain, PersIndex, EDC, UEDC
--	, TotMaintForceWHrEDC, MaintIndex, NEOpexEDC, OpexUEDC)
--SELECT Period = 'AVG', NumMonths = NULL, g.UtilPcnt_Avg, g.ProcessUtilPcnt_Avg, g.OpAvail_Avg, g.EII_Avg, g.VEI_Avg, g.NetInputBPD_Avg*1000.0, g.GainPcnt_Avg, g.TotWHrEDC_Avg, g.EDC_Avg, g.UEDC_Avg
--	, g.TotMaintForceWHrEDC_Avg, g.MaintIndex_Avg, g.NEOpexEDC_Avg, g.TotCashOpexUEDC_Avg
--FROM Gensum g
--WHERE g.SubmissionID = @SubmissionID AND g.FactorSet = @FactorSet AND g.Currency = @Currency AND g.UOM = @UOM AND g.Scenario = 'CLIENT'

----- Everything Already Available in MaintAvailCalc
--SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_QTR = MechUnavailTA_Ann, @MechUnavailTA_AVG = MechUnavailTA_Ann
--FROM MaintAvailCalc
--WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

--SELECT @OpAvail_QTR=SUM(OpAvail_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear), 
--	@MechUnavailTA_QTR=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
--FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
--INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
--AND m.FactorSet = @FactorSet

--SELECT @MechUnavailTA_AVG=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
--FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
--INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd
--AND m.FactorSet = @FactorSet

--SELECT @NonTAUnavail = 100 - @OpAvail - @MechUnavailTA, @NonTAUnavail_QTR = 100 - @OpAvail_QTR - @MechUnavailTA_QTR, @NonTAUnavail_AVG = 100 - @OpAvail_AVG - @MechUnavailTA_AVG

--EXEC spAverageFactors @RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, 
--		@EII = @EII_QTR OUTPUT, @VEI = @VEI_QTR OUTPUT, @UtilPcnt = @RefUtilPcnt_QTR OUTPUT, @UtilOSTA = NULL, @EDC = @EDC_QTR OUTPUT, @UEDC = @UEDC_QTR OUTPUT, 
--		@ProcessUtilPcnt = @ProcessUtilPcnt_QTR OUTPUT, @TotProcessEDC = @TotProcessEDC_QTR OUTPUT, @TotProcessUEDC = @TotProcessUEDC_QTR OUTPUT
--SELECT @EDC_QTR = @EDC_QTR/1000, @UEDC_QTR = @UEDC_QTR/1000, @TotProcessEDC_QTR = @TotProcessEDC_QTR/1000, @TotProcessUEDC_QTR = @TotProcessUEDC_QTR/1000

--SELECT @TotProcessEDC = TotProcessEDC, @TotProcessUEDC = TotProcessUEDC,
--	@EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
--	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays,
--	@ProcessEffIndex = f.PEI
--FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
--WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

--SELECT @TotProcessEDC_QTR = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
--	@TotProcessUEDC_QTR = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
--	@EnergyUseDay_QTR = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
--	@TotStdEnergy_QTR = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
--	@ReportLossGain_QTR = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
--	@EstGain_QTR = SUM(EstGain)/SUM(s.NumDays*1.0)
--FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
--AND f.FactorSet = @FactorSet

--SELECT @TotProcessEDC_AVG = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
--	@TotProcessUEDC_AVG = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
--	@EnergyUseDay_AVG = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
--	@TotStdEnergy_AVG = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
--	@ReportLossGain_AVG = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
--	@EstGain_AVG = SUM(EstGain)/SUM(s.NumDays*1.0)
--FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd
--AND f.FactorSet = @FactorSet

--SELECT @ProcessEffIndex_QTR = dbo.AvgProcessEffIndex(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet)
--	,  @ProcessEffIndex_AVG = dbo.AvgProcessEffIndex(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd, @FactorSet)

--SELECT @RawMatl = NetInputBbl/(s.NumDays*1000.0), @ProdYield = (NetInputBbl+GainBbl)/(s.NumDays*1000.0)
--FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
--WHERE s.SubmissionID = @SubmissionID

--SELECT @Gain_QTR = SUM(GainBbl)/SUM(NetInputBbl)*100, @RawMatl_QTR = SUM(NetInputBbl)/SUM(s.NumDays*1000.0), @ProdYield_QTR = SUM(NetInputBbl+GainBbl)/SUM(s.NumDays*1000.0),
--	@NetInputBPD_QTR = SUM(NetInputBbl)/SUM(s.NumDays*1.0)
--FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND m.NetInputBbl > 0

--SELECT @Gain_AVG = SUM(GainBbl)/SUM(NetInputBbl)*100, @RawMatl_AVG = SUM(NetInputBbl)/SUM(s.NumDays*1000.0), @ProdYield_AVG = SUM(NetInputBbl+GainBbl)/SUM(s.NumDays*1000.0)
--FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND m.NetInputBbl > 0

--SELECT @TotMaintForceWHrEDC_QTR = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
--, @PersIndex_QTR = SUM(g.TotWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
--, @MaintIndex_QTR = SUM((g.RoutIndex + g.TAIndex_AVG)*g.EDC)/SUM(g.EDC)
--FROM PersTotCalc p INNER JOIN Gensum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
--INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet AND g.UOM = @UOM
--AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

--SELECT @OpexUEDC_QTR = SUM(TotCashOpex*Divisor)/SUM(Divisor)
--FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
--AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
--AND o.DataType = 'UEDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

--SELECT @NEOpexEDC_QTR = SUM(NEOpex*Divisor)/SUM(Divisor)
--FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
--AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
--AND o.DataType = 'EDC' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

--SELECT @NonMaintWHr = NonMaintWHr/1000, @TotMaintForceWHr = TotMaintForceWHr/1000, @MaintTAWHr = (TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr = (TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
--FROM PersTot
--WHERE SubmissionID = @SubmissionID

--SELECT @NonMaintWHr_QTR = SUM(NonMaintWHr/1000), @TotMaintForceWHr_QTR = SUM(TotMaintForceWHr/1000), @MaintTAWHr_QTR = SUM(TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr_QTR = SUM(TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
--FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd

--SELECT @NonMaintWHr_AVG = SUM(NonMaintWHr/1000), @TotMaintForceWHr_AVG = SUM(TotMaintForceWHr/1000), @MaintTAWHr_AVG = SUM(TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr_AVG = SUM(TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
--FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd

--SELECT @AnnTACost = AllocAnnTACost/1000, @RoutCost = CurrRoutCost/1000
--FROM MaintTotCost mtc 
--WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency

--SELECT @AnnTACost_QTR = SUM(AllocAnnTACost)/1000, @RoutCost_QTR = SUM(CurrRoutCost)/1000
--FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

--SELECT @AnnTACost_AVG = SUM(AllocAnnTACost)/1000, @RoutCost_AVG = SUM(CurrRoutCost)/1000
--FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND mtc.Currency = @Currency

--SELECT @NEOpex = NEOpex*Divisor/1000, @OpexEDC = TotCashOpex, @EnergyCost = EnergyCost*Divisor/1000, @TotCashOpex = TotCashOpex*Divisor/1000
--FROM OpexCalc o 
--WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

--SELECT @NEOpex_QTR = SUM(NEOpex*Divisor/1000), @OpexEDC_QTR = SUM(TotCashOpex*Divisor)/SUM(Divisor), @EnergyCost_QTR = SUM(EnergyCost*Divisor/1000), @TotCashOpex_QTR = SUM(TotCashOpex*Divisor/1000)
--FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
--AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
--AND o.DataType = 'EDC'

--SELECT @NEOpex_AVG = SUM(NEOpex*Divisor/1000), @OpexEDC_AVG = SUM(TotCashOpex*Divisor)/SUM(Divisor), @EnergyCost_AVG = SUM(EnergyCost*Divisor/1000), @TotCashOpex_AVG = SUM(TotCashOpex*Divisor/1000)
--FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
--WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
--AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
--AND o.DataType = 'EDC'


SELECT UtilPcnt = m.RefUtilPcnt, UtilPcnt_QTR = avgQtr.RefUtilPcnt, UtilPcnt_3MO = avg3mo.RefUtilPcnt, UtilPcnt_AVG = avg12mo.RefUtilPcnt, 
	EDC = m.EDC/1000.0, EDC_QTR = avgQtr.EDC/1000.0, EDC_3MO = avg3mo.EDC/1000.0, EDC_AVG = avg12mo.EDC/1000.0, 
	UtilUEDC = m.UtilUEDC/1000.0, UtilUEDC_QTR = avgQtr.UtilUEDC/1000.0, UtilUEDC_3MO = avg3mo.UtilUEDC/1000.0, UtilUEDC_AVG = avg12mo.UtilUEDC/1000.0, 
	UEDC = m.UEDC/1000.0, UEDC_QTR = avgQtr.UEDC/1000.0, UEDC_3MO = avg3mo.UEDC/1000.0, UEDC_AVG = avg12mo.UEDC/1000.0, 
	ProcessUtilPcnt = m.ProcessUtilPcnt, ProcessUtilPcnt_QTR = avgQtr.ProcessUtilPcnt, ProcessUtilPcnt_3MO = avg3mo.ProcessUtilPcnt, ProcessUtilPcnt_AVG = avg12mo.ProcessUtilPcnt, 
	TotProcessEDC = m.TotProcessEDC/1000.0, TotProcessEDC_QTR = avgQtr.TotProcessEDC/1000.0, TotProcessEDC_3MO = avg3mo.TotProcessEDC/1000.0, TotProcessEDC_AVG = avg12mo.TotProcessEDC/1000.0, 
	TotProcessUEDC = m.TotProcessUEDC/1000.0, TotProcessUEDC_QTR = avgQtr.TotProcessUEDC/1000.0, TotProcessUEDC_3MO = avg3mo.TotProcessUEDC/1000.0, TotProcessUEDC_AVG = avg12mo.TotProcessUEDC/1000.0, 
	OpAvail = m.OpAvail, OpAvail_QTR = avgQtr.OpAvail, OpAvail_3MO = avg3mo.OpAvail, OpAvail_AVG = avg24mo.OpAvail, 
	MechUnavailTA = m.MechUnavailTA, MechUnavailTA_QTR = avgQtr.MechUnavailTA, MechUnavailTA_3MO = avg3mo.MechUnavailTA, MechUnavailTA_AVG = avg24mo.MechUnavailTA, 
	NonTAUnavail = m.MechUnavailRout + m.RegUnavail, NonTAUnavail_QTR = avgQtr.MechUnavailRout + avgQtr.RegUnavail, NonTAUnavail_3MO = avg3mo.MechUnavailRout + avg3mo.RegUnavail, NonTAUnavail_AVG = avg24mo.MechUnavailRout + avg24mo.RegUnavail, 
	EII = m.EII, EII_QTR = avgQtr.EII, EII_3MO = avg3mo.EII, EII_AVG = avg12mo.EII, 
	EnergyUseDay = m.EnergyUseDay, EnergyUseDay_QTR = avgQtr.EnergyUseDay, EnergyUseDay_3MO = avg3mo.EnergyUseDay, EnergyUseDay_AVG = avg12mo.EnergyUseDay, 
	TotStdEnergy = m.TotStdEnergy, TotStdEnergy_QTR = avgQtr.TotStdEnergy, TotStdEnergy_3MO = avg3mo.TotStdEnergy, TotStdEnergy_AVG = avg12mo.TotStdEnergy, 
	ProcessEffIndex = m.ProcessEffIndex, ProcessEffIndex_QTR = avgQtr.ProcessEffIndex, ProcessEffIndex_3MO = avg3mo.ProcessEffIndex, ProcessEffIndex_AVG = avg12mo.ProcessEffIndex, 
	VEI = m.VEI, VEI_QTR = avgQtr.VEI, VEI_3MO = avg3mo.VEI, VEI_AVG = avg12mo.VEI, 
	ReportLossGain = m.ReportLossGain, ReportLossGain_QTR = avgQtr.ReportLossGain, ReportLossGain_3MO = avg3mo.ReportLossGain, ReportLossGain_AVG = avg12mo.ReportLossGain, 
	EstGain = m.EstGain, EstGain_QTR = avgQtr.EstGain, EstGain_3MO = avg3mo.EstGain, EstGain_AVG = avg12mo.EstGain, 
	NetInputBPD = m.NetInputBPD, NetInputBPD_QTR = avgQtr.NetInputBPD, NetInputBPD_3MO = avg3mo.NetInputBPD, NetInputBPD_AVG = avg12mo.NetInputBPD, 
	Gain = -m.GainPcnt, Gain_QTR = -avgQtr.GainPcnt, Gain_3MO = -avg3mo.GainPcnt, Gain_AVG = -avg12mo.GainPcnt, 
	RawMatl = m.RawMatlKBpD, RawMatl_QTR = avgQtr.RawMatlKBpD, RawMatl_3MO = avg3mo.RawMatlKBpD, RawMatl_AVG = avg12mo.RawMatlKBpD, 
	ProdYield = m.ProdYieldKBpD, ProdYield_QTR = avgQtr.ProdYieldKBpD, ProdYield_3MO = avg3mo.ProdYieldKBpD, ProdYield_AVG = avg12mo.ProdYieldKBpD, 
	PersIndex = m.PersIndex, PersIndex_QTR = avgQtr.PersIndex, PersIndex_3MO = avg3mo.PersIndex, PersIndex_AVG = avg12mo.PersIndex, 
	NonMaintWHr = m.NonMaintWHr, NonMaintWHr_QTR = avgQtr.NonMaintWHr, NonMaintWHr_3MO = avg3mo.NonMaintWHr, NonMaintWHr_AVG = avg12mo.NonMaintWHr, 
	TotMaintForceWHr = m.TotMaintForceWHr, TotMaintForceWHr_QTR = avgQtr.TotMaintForceWHr, TotMaintForceWHr_3MO = avg3mo.TotMaintForceWHr, TotMaintForceWHr_AVG = avg12mo.TotMaintForceWHr, 
	TotMaintForceWHrEDC = m.TotMaintForceWHrEDC, TotMaintForceWHrEDC_QTR = avgQtr.TotMaintForceWHrEDC, TotMaintForceWHrEDC_3MO = avg3mo.TotMaintForceWHrEDC, TotMaintForceWHrEDC_AVG = avg12mo.TotMaintForceWHrEDC, 
	MaintTAWhr = m.MaintTAWhr, MaintTAWhr_QTR = avgQtr.MaintTAWhr, MaintTAWhr_3MO = avg3mo.MaintTAWhr, MaintTAWhr_AVG = avg12mo.MaintTAWhr, 
	MaintNonTAWHr = m.MaintNonTAWHr, MaintNonTAWHr_QTR = avgQtr.MaintNonTAWHr, MaintNonTAWHr_3MO = avg3mo.MaintNonTAWHr, MaintNonTAWHr_AVG = avg12mo.MaintNonTAWHr, 
	MaintIndex = m.MaintIndex, MaintIndex_QTR = avgQtr.MaintIndex, MaintIndex_3MO = avg3mo.MaintIndex, MaintIndex_AVG = avg24mo.MaintIndex, 
	AnnTACost = m.AnnTACost, AnnTACost_QTR = avgQtr.AnnTACost, AnnTACost_3MO = avg3mo.AnnTACost, AnnTACost_AVG = avg24mo.AnnTACost, 
	RoutCost = m.RoutCost, RoutCost_QTR = avgQtr.RoutCost, RoutCost_3MO = avg3mo.RoutCost, RoutCost_AVG = avg24mo.RoutCost, 
	NEOpexEDC = m.NEOpexEDC, NEOpexEDC_QTR = avgQtr.NEOpexEDC, NEOpexEDC_3MO = avg3mo.NEOpexEDC, NEOpexEDC_AVG = avg12mo.NEOpexEDC, 
	NEOpex = m.NEOpex, NEOpex_QTR = avgQtr.NEOpex, NEOpex_3MO = avg3mo.NEOpex, NEOpex_AVG = avg12mo.NEOpex, 
	TotCashOpexUEDC = m.OpexUEDC, TotCashOpexUEDC_QTR = avgQtr.OpexUEDC, TotCashOpexUEDC_3MO = avg3mo.OpexUEDC, TotCashOpexUEDC_AVG = avg12mo.OpexUEDC, 
	OpexEDC = m.OpexEDC, OpexEDC_QTR = avgQtr.OpexEDC, OpexEDC_3MO = avg3mo.OpexEDC, OpexEDC_AVG = avg12mo.OpexEDC, 
	EnergyCost = m.EnergyCost, EnergyCost_QTR = avgQtr.EnergyCost, EnergyCost_3MO = avg3mo.EnergyCost, EnergyCost_AVG = avg12mo.EnergyCost, 
	TotCashOpex = m.TotCashOpex, TotCashOpex_QTR = avgQtr.TotCashOpex, TotCashOpex_3MO = avg3mo.TotCashOpex, TotCashOpex_AVG = avg12mo.TotCashOpex
FROM dbo.GetProfileLiteKPIs(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.GetProfileLiteKPIs(@RefineryID, @Dataset, @Start3Mo, @PeriodEnd, @FactorSet, @Scenario, @Currency, @UOM) avg3mo ON 1=1
LEFT JOIN dbo.GetProfileLiteKPIs(@RefineryID, @Dataset, @StartQTR, @EndQuarter, @FactorSet, @Scenario, @Currency, @UOM) avgQtr ON 1=1
LEFT JOIN dbo.GetProfileLiteKPIs(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.GetProfileLiteKPIs(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1

--FROM @PeriodResults m LEFT JOIN @PeriodResults a ON a.Period = 'AVG' LEFT JOIN @PeriodResults qtr ON qtr.Period = 'TY' LEFT JOIN @PeriodResults m3 ON m3.Period = '3MO'
--WHERE m.Period = 'MO'
