﻿CREATE         PROC [dbo].[spMaintCosts](@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @tblMaintCost TABLE (
	[SubmissionID] [int] NOT NULL ,
	[UnitID] [int] NOT NULL ,
	[Currency] [varchar](4) NOT NULL ,
	[ProcessID] [varchar](8) NULL ,
	[AnnTACost] [real] NULL ,
	[AnnTAMatl] [real] NULL ,
	[CurrTACost] [real] NULL ,
	[CurrRoutCost] [real] NULL ,
	[CurrMaintCost] [real] NULL ,
	[CurrTAMatl] [real] NULL ,
	[CurrRoutMatl] [real] NULL ,
	[CurrMaintMatl] [real] NULL ,
	[AllocAnnTACost] [real] NULL ,
	[AllocAnnTAMatl] [real] NULL ,
	[AllocCurrRoutCost] [real] NULL ,
	[AllocCurrRoutMatl] [real] NULL
)
DELETE FROM MaintCost WHERE SubmissionID = @SubmissionID
DELETE FROM MaintTotCost WHERE SubmissionID = @SubmissionID
DELETE FROM MaintIndex WHERE SubmissionID = @SubmissionID
INSERT INTO @tblMaintCost (SubmissionID, UnitID, Currency, ProcessID, CurrRoutCost, CurrRoutMatl)
SELECT m.SubmissionID, m.UnitID, c.Currency, m.ProcessID, 
m.RoutCostLocal*dbo.ExchangeRate(s.RptCurrency, c.Currency, s.PeriodStart), 
m.RoutMatlLocal*dbo.ExchangeRate(s.RptCurrency, c.Currency, s.PeriodStart)
FROM Submissions s INNER JOIN CurrenciesToCalc c ON c.RefineryID = s.RefineryID
INNER JOIN MaintRout m ON m.SubmissionID = s.SubmissionID
WHERE s.SubmissionID = @SubmissionID
SELECT SubmissionID, UnitID, ProcessID, Currency, CurrTACost, CurrTAMatl, AnnTACost, AnnTAMatl
INTO #SubTA
FROM TACostsForSubmission
WHERE SubmissionID = @SubmissionID
UPDATE a
SET CurrTACost = ta.CurrTACost, CurrTAMatl = ta.CurrTAMatl, 
AnnTACost = ta.AnnTACost, AnnTAMatl = ta.AnnTAMatl
FROM @tblMaintCost a INNER JOIN #SubTA ta ON a.UnitID = ta.UnitID AND a.Currency = ta.Currency
INSERT INTO @tblMaintCost (SubmissionID, UnitID, Currency, CurrTACost, CurrTAMatl, AnnTACost, AnnTAMatl)
SELECT SubmissionID, UnitID, Currency, CurrTACost, CurrTAMatl, AnnTACost, AnnTAMatl
FROM #SubTA ta
WHERE NOT EXISTS (SELECT * FROM @tblMaintCost a WHERE a.UnitID = ta.UnitID)
DROP TABLE #SubTA
UPDATE @tblMaintCost
SET CurrMaintCost = ISNULL(CurrTACost, 0) + ISNULL(CurrRoutCost, 0),
CurrMaintMatl = ISNULL(CurrTAMatl, 0) + ISNULL(CurrRoutMatl, 0),
AllocAnnTACost = ISNULL(AnnTACost, 0), AllocAnnTAMatl = ISNULL(AnnTAMatl, 0),
AllocCurrRoutCost = ISNULL(CurrRoutCost, 0), AllocCurrRoutMatl = ISNULL(CurrRoutMatl, 0)
UPDATE a
SET AllocAnnTACost = AllocAnnTACost*(c.InservicePcnt/100), 
AllocAnnTAMatl = AllocAnnTAMatl*(c.InservicePcnt/100)
FROM @tblMaintCost a INNER JOIN Config c ON c.SubmissionID = a.SubmissionID AND c.UnitID = a.UnitID
WHERE c.SubmissionID = @SubmissionID AND c.InservicePcnt < 100
INSERT INTO MaintCost(SubmissionID, UnitID, Currency, ProcessID,
	AnnTACost, AnnTAMatl, CurrTACost, CurrRoutCost, CurrMaintCost,
	CurrTAMatl, CurrRoutMatl, CurrMaintMatl, AllocAnnTACost,
	AllocAnnTAMatl, AllocCurrRoutCost, AllocCurrRoutMatl)
SELECT SubmissionID, UnitID, Currency, ProcessID,
	AnnTACost, AnnTAMatl, CurrTACost, CurrRoutCost, CurrMaintCost,
	CurrTAMatl, CurrRoutMatl, CurrMaintMatl, AllocAnnTACost,
	AllocAnnTAMatl, AllocCurrRoutCost, AllocCurrRoutMatl
FROM @tblMaintCost
INSERT INTO MaintTotCost(SubmissionID, Currency, AnnTACost, AnnTAMatl, CurrTACost, CurrRoutCost, CurrMaintCost, CurrTAMatl, CurrRoutMatl, CurrMaintMatl, AllocAnnTACost, AllocAnnTAMatl, AllocCurrRoutCost, AllocCurrRoutMatl)
SELECT SubmissionID, Currency, SUM(AnnTACost), SUM(AnnTAMatl), SUM(CurrTACost), SUM(CurrRoutCost), SUM(CurrMaintCost), SUM(CurrTAMatl), SUM(CurrRoutMatl), SUM(CurrMaintMatl), SUM(AllocAnnTACost), SUM(AllocAnnTAMatl), SUM(AllocCurrRoutCost), SUM(AllocCurrRoutMatl)
FROM @tblMaintCost
GROUP BY SubmissionID, Currency
INSERT INTO MaintIndex (SubmissionID, Currency, FactorSet,
	TAIndex, RoutIndex, MaintIndex, AdjMaintIndex,
	TAMatlIndex, RoutMatlIndex, MaintMatlIndex,
	TAIndex_Avg, TAMatlIndex_Avg, 
	TAEffIndex, RoutEffIndex, MaintEffIndex, AdjMaintEffIndex, TAEffIndex_Avg)
SELECT m.SubmissionID, m.Currency, f.FactorSet,
	m.CurrTACost*1000/(f.PlantEDC*s.FractionOfYear),
	m.CurrRoutCost*1000/(f.PlantEDC*s.FractionOfYear),
	m.CurrMaintCost*1000/(f.PlantEDC*s.FractionOfYear),
	(m.CurrRoutCost+m.AllocAnnTACost*s.FractionOfYear)*1000/(f.PlantEDC*s.FractionOfYear),
	m.CurrTAMatl*1000/(f.PlantEDC*s.FractionOfYear),
	m.CurrRoutMatl*1000/(f.PlantEDC*s.FractionOfYear),
	m.CurrMaintMatl*1000/(f.PlantEDC*s.FractionOfYear),
	m.AllocAnnTACost*1000/(f.PlantEDC),
	m.AllocAnnTAMatl*1000/(f.PlantEDC),
	m.CurrTACost*100000/(CASE WHEN f.PlantMaintEffDiv > 0 THEN f.PlantMaintEffDiv*s.FractionOfYear END),
	m.CurrRoutCost*100000/(CASE WHEN f.PlantMaintEffDiv > 0 THEN f.PlantMaintEffDiv*s.FractionOfYear END),
	m.CurrMaintCost*100000/(CASE WHEN f.PlantMaintEffDiv > 0 THEN f.PlantMaintEffDiv*s.FractionOfYear END),
	(m.CurrRoutCost+m.AllocAnnTACost*s.FractionOfYear)*100000/(CASE WHEN f.PlantMaintEffDiv > 0 THEN f.PlantMaintEffDiv*s.FractionOfYear END),
	m.AllocAnnTACost*100000/(CASE WHEN f.PlantMaintEffDiv > 0 THEN f.PlantMaintEffDiv END)
FROM MaintTotCost m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID
WHERE m.SubmissionID = @SubmissionID AND f.EDC > 0
DECLARE @RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime
SELECT @RefineryID = RefineryID, @DataSet = DataSet, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd
FROM Submissions WHERE SubmissionID = @SubmissionID
DELETE FROM MaintRoutHist
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodStart = @PeriodStart
INSERT INTO MaintRoutHist (RefineryID, DataSet, Currency, PeriodStart, PeriodEnd, RoutCost, RoutMatl, Reported)
SELECT @RefineryID, @DataSet, Currency, @PeriodStart, @PeriodEnd, CurrRoutCost, CurrRoutMatl, 0
FROM MaintTotCost 
WHERE SubmissionID = @SubmissionID
