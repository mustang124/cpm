﻿CREATE PROC [dbo].[spUpdateNextTADate] (@RefineryID varchar(6), @DataSet varchar(15))
AS
UPDATE MaintTA
SET NextTADate = (SELECT MIN(n.TADate) FROM MaintTA n
	WHERE n.RefineryID = MaintTA.RefineryID AND n.DataSet = MaintTA.DataSet
	AND n.UnitID = MaintTA.UnitID AND n.TADate > MaintTA.TADate)
WHERE RefineryID = @RefineryID AND DataSet = @DataSet

UPDATE MaintTACost
SET TADate = ta.TADate, NextTADate = ta.NextTADate
FROM MaintTACost INNER JOIN MaintTA ta 
	ON ta.RefineryID = MaintTACost.RefineryID AND ta.DataSet = MaintTACost.DataSet
	AND ta.UnitID = MaintTACost.UnitID AND ta.TAID = MaintTACost.TAID
WHERE ta.RefineryID = @RefineryID AND ta.DataSet = @DataSet
