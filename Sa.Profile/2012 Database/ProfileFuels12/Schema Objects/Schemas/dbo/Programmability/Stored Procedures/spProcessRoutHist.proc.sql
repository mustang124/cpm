﻿
CREATE    PROC [dbo].[spProcessRoutHist] (@SubmissionID int)
AS

IF NOT EXISTS (SELECT * FROM LoadRoutHist WHERE SubmissionID = @SubmissionID)
	RETURN 0
	
DECLARE @RefineryID varchar(6), @DataSet varchar(15), @RptCurrency CurrencyCode
DECLARE @LoadYear smallint, @LoadMonth tinyint, @LoadCost real, @LoadMatl real
DECLARE @OldYear smallint, @OldMonth tinyint, @OldCost real, @OldMatl real
DECLARE @HistYear smallint, @HistMonth tinyint
DECLARE @UpdateAction char(1), @RecalcFrom smalldatetime, @RecalcTo smalldatetime
DECLARE @hist TABLE (
	HistYear smallint NOT NULL,
	HistMonth tinyint NOT NULL,
	UpdateAction char(1) NULL
)
SELECT @RefineryID = RefineryID, @DataSet = DataSet, @RptCurrency = RptCurrency
FROM Submissions WHERE SubmissionID = @SubmissionID

INSERT INTO @hist (HistYear, HistMonth)
SELECT DATEPART(yy, PeriodStart), DATEPART(mm, PeriodStart)
FROM MaintRoutHist
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND Currency = @RptCurrency
AND PeriodStart < (SELECT MIN(PeriodStart) FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet)
UNION
SELECT DATEPART(yy, PeriodStart), DATEPART(mm, PeriodStart)
FROM LoadRoutHist
WHERE SubmissionID = @SubmissionID 
AND PeriodStart < (SELECT MIN(PeriodStart) FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet)

DECLARE cHist CURSOR SCROLL 
FOR SELECT HistYear, HistMonth FROM @hist
OPEN cHist
FETCH NEXT FROM cHist INTO @HistYear, @HistMonth
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @UpdateAction = NULL, @RecalcFrom = NULL, @RecalcTo = NULL
	SELECT 	@LoadYear = DATEPART(yy, PeriodStart), @LoadMonth = DATEPART(mm, PeriodStart), 
		@LoadCost = RoutCostLocal, @LoadMatl = RoutMatlLocal
	FROM LoadRoutHist 
	WHERE SubmissionID = @SubmissionID
		AND DATEPART(yy, PeriodStart) = @HistYear AND DATEPART(mm, PeriodStart) = @HistMonth
	SELECT @OldYear = DATEPART(yy, PeriodStart), @OldMonth = DATEPART(mm, PeriodStart), 
		@OldCost = RoutCost, @OldMatl = RoutMatl
	FROM MaintRoutHist
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND Currency = @RptCurrency
		AND DATEPART(yy, PeriodStart) = @HistYear AND DATEPART(mm, PeriodStart) = @HistMonth

	IF @OldYear IS NULL
		SELECT @UpdateAction = 'A'
	IF @LoadYear IS NULL
		SELECT @UpdateAction = 'D'
	IF @UpdateAction IS NULL
	BEGIN
		IF NOT (((@OldCost IS NULL AND @LoadCost IS NULL) OR ABS(@OldCost - @LoadCost)<0.01)
			AND ((@OldMatl IS NULL AND @LoadMatl IS NULL) OR ABS(@OldMatl - @LoadMatl)<0.01))
			SELECT @UpdateAction = 'U'
	END
	UPDATE @hist
	SET UpdateAction = @UpdateAction
	WHERE HistYear = @HistYear AND HistMonth = @HistMonth
	IF @UpdateAction IN ('U','D')
	BEGIN
		DELETE FROM MaintRoutHist 
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet 
		AND DATEPART(yy, PeriodStart) = @HistYear AND DATEPART(mm, PeriodStart) = @HistMonth
	END
	IF @UpdateAction IN ('A','U')
	BEGIN
		INSERT INTO MaintRoutHist (RefineryID, DataSet, Currency, PeriodStart, PeriodEnd,
			RoutCost, RoutMatl, Reported)
		VALUES (@RefineryID, @DataSet, @RptCurrency, dbo.BuildDate(@HistYear, @HistMonth, 1), DATEADD(mm, 1, dbo.BuildDate(@HistYear, @HistMonth, 1)),
			@LoadCost, @LoadMatl, 1)

		INSERT INTO MaintRoutHist (RefineryID, DataSet, Currency, PeriodStart, PeriodEnd,
			RoutCost, RoutMatl, Reported)
		SELECT @RefineryID, @DataSet, Currency, dbo.BuildDate(@HistYear, @HistMonth, 1), DATEADD(mm, 1, dbo.BuildDate(@HistYear, @HistMonth, 1)),
			@LoadCost*dbo.ExchangeRate(@RptCurrency, Currency, dbo.BuildDate(@HistYear, @HistMonth, 1)),
			@LoadMatl*dbo.ExchangeRate(@RptCurrency, Currency, dbo.BuildDate(@HistYear, @HistMonth, 1)), 0
		FROM CurrenciesToCalc
		WHERE RefineryID = @RefineryID AND Currency <> @RptCurrency
	END
	FETCH NEXT FROM cHist INTO @HistYear, @HistMonth
END
CLOSE cHist
DEALLOCATE cHist
SELECT 	@RecalcFrom = MIN(dbo.BuildDate(HistYear, HistMonth, 1)), 
	@RecalcTo = DATEADD(yy, 2, MAX(dbo.BuildDate(HistYear, HistMonth, 1)))
FROM @hist
WHERE UpdateAction IS NOT NULL
UPDATE Submissions
SET CalcsNeeded = 'M'
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
AND PeriodStart <= @RecalcTo AND PeriodEnd >= @RecalcFrom
AND CalcsNeeded IS NULL AND UseSubmission = 1

