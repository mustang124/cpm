﻿CREATE                 PROC [dbo].[spDataChecks] (@SubmissionID int)
AS

DECLARE @FactorSet FactorSet
SELECT @FactorSet = '2006'
--IF @Submissionid = 476 
--	RETURN  /* 476 is the example submission. Do not recalculate it. */

DECLARE @DataCheckID varchar(10)
DECLARE  @Datachecks TABLE (
	[SubmissionID] [int] NOT NULL,
	[DataCheckID] [char] (10) NOT NULL,
	[ItemSortKey] [int] NULL,
	[ItemDesc] [varchar] (200) NULL,
	[Value1] [varchar] (50) NULL ,
	[Value2] [varchar] (50) NULL ,
	[Value3] [varchar] (50) NULL ,
	[Value4] [varchar] (50) NULL ,
	[Value5] [varchar] (50) NULL ,
	[Value6] [varchar] (50) NULL ,
	[Value7] [varchar] (50) NULL 
) 


DECLARE @RefineryID varchar(6), @CurrDate smalldatetime, @DataSet varchar(15), @RptCurrency CurrencyCode, @FractionOfYear real
SELECT @RefineryID = RefineryID, @CurrDate = PeriodStart, @DataSet = DataSet, @RptCurrency = RptCurrency, @FractionOfYear = FractionOfYear
FROM Submissions
WHERE SubmissionID = @SubmissionID

DECLARE @PrevSubID int, @PrevDate smalldatetime, @Prev12Date smalldatetime
SELECT TOP 1 @PrevSubID = SubmissionID, @PrevDate = PeriodStart
FROM Submissions 
WHERE RefineryId = @RefineryID AND PeriodStart < @CurrDate
ORDER BY PeriodStart DESC

SELECT @Prev12Date = MIN(PeriodStart) FROM Submissions 
WHERE RefineryID = @RefineryID AND PeriodStart BETWEEN DATEADD(yy, -1, @CurrDate) AND @CurrDate

--SELECT @SubmissionID, @RefineryID, @PrevSubId, @CurrDate, @PrevDate, @Prev12Date

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4, Value5)
SELECT c.SubmissionID, 'UtilPlusDT', c.SortKey, RTRIM(c.UnitName) + ' (' + RTRIM(c.ProcessID) + ')', 
CONVERT(numeric(9,1), c.UtilPcnt), CONVERT(numeric(9,1), c.InservicePcnt), CONVERT(numeric(9,1), m.MechUnavailTA_Act) AS TA, CONVERT(numeric(9,1), 100-m.MechUnavailTA_Act-m.OnStreamSlow_Act) AS NonTA, 
PcntSum = CAST(ISNULL(UtilPcnt, 0)*ISNULL(InservicePcnt/100, 1) + (100-m.OnStreamSlow_Act) AS numeric(9, 1))
FROM Config c INNER JOIN MaintCalc m ON m.SubmissionID = c.SubmissionID AND m.UnitID = c.UnitID
WHERE ISNULL(UtilPcnt, 0)*ISNULL(InservicePcnt/100, 1) + (100-m.OnStreamSlow_Act) NOT BETWEEN 97 AND 103
AND NOT (ISNULL(UtilPcnt, 0)*ISNULL(InservicePcnt/100, 1) > 100 AND m.OnStreamSlow_Act = 100)
AND c.SubmissionID = @SubmissionID

INSERT @Datachecks(SubmissionID, DatacheckID, ItemDesc, Value1)
SELECT SubmissionID, 'TechMaint', 'Percent of Technical Staff dedicated to maintenance', CONVERT(numeric(9,1), MaintPcnt)
FROM Pers
WHERE PersID = 'MPSTS' AND MaintPcnt > 0 AND NOT MaintPcnt BETWEEN 1 AND 100 AND SubmissionID = @SubmissionID

INSERT @Datachecks(SubmissionID, DatacheckID, ItemDesc, ItemSortKey, Value1)
SELECT SubmissionID, 'InvenPcnt', lu.Description, ISNULL(lu.SortKey, i.SortKey), CONVERT(numeric(9,1),AvgLevel)
FROM Inventory i LEFT JOIN TankType_LU lu ON lu.TankType = i.TankType
WHERE FuelsStorage > 0 AND NOT (AvgLevel > 1 AND AvgLevel <= 100) AND SubmissionID = @SubmissionID

INSERT @Datachecks(SubmissionID, DatacheckID, ItemDesc, ItemSortKey, Value1)
SELECT SubmissionID, 'InvenPcnt', '  Total', 11, CONVERT(numeric(9,1), SUM(ISNULL(AvgLevel,0)*FuelsStorage)/SUM(FuelsStorage))
FROM Inventory i 
WHERE FuelsStorage > 0 AND SubmissionID = @SubmissionID
GROUP BY SubmissionID
HAVING SUM(ISNULL(AvgLevel,0)*FuelsStorage)/SUM(FuelsStorage) NOT BETWEEN 25 AND 75


INSERT @Datachecks(SubmissionID, DatacheckID, ItemDesc, ItemSortKey, Value1)
SELECT SubmissionID, 'UtilPcnt', RTRIM(c.UnitName) + ' (' + RTRIM(lu.Description) + ')', c.SortKey, CONVERT(numeric(9,1), c.UtilPcnt)
FROM Config c LEFT JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE UtilPcnt > 0 AND (UtilPcnt < 1 OR UtilPcnt > 120) AND SubmissionID = @SubmissionID

INSERT @Datachecks(SubmissionID, DatacheckID, ItemDesc, ItemSortKey)
SELECT SubmissionID, 'UtilNull', RTRIM(c.UnitName) + ' (' + RTRIM(lu.Description) + ')', c.SortKey
FROM Config c LEFT JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE ((Cap > 0 AND UtilPcnt IS NULL) OR (StmCap > 0 AND StmUtilPcnt IS NULL)) AND SubmissionID = @SubmissionID


INSERT @Datachecks(SubmissionID, DatacheckID, ItemDesc, Value1, Value2, Value3)
SELECT SubmissionID, 'Losses', 'Barrels', CONVERT(numeric(15,0), TotInputBbl), TotYieldBbl = CONVERT(numeric(15,0), TotInputBbl+GainBbl), CONVERT(varchar(20), CONVERT(numeric(15,0), GainBbl))
FROM MaterialTot 
WHERE GainBbl < 0 AND SubmissionID = @SubmissionID

DECLARE @CrudeAPI numeric(9,1), @MinAPI numeric(9,1), @MaxAPI numeric(9,1), @AvgAPI numeric(9,1)
DECLARE @CrudeSulfur numeric(9,2), @MinSulfur numeric(9,2), @MaxSulfur numeric(9,2), @AvgSulfur numeric(9,2)
SELECT @CrudeAPI = CrudeAPI, @CrudeSulfur = CrudeSulfur, @AvgAPI = CrudeAPI_Avg, @AvgSulfur = CrudeSulfur_Avg 
FROM Gensum WHERE SubmissionID = @SubmissionID AND UOM = 'US' AND FactorSet = @FactorSet AND Currency = 'USD' AND Scenario = 'CLIENT'

SELECT @MinAPI = MIN(CrudeAPI), @MaxAPI = MAX(CrudeAPI), @MinSulfur = MIN(CrudeSulfur), @MaxSulfur = MAX(CrudeSulfur) 
FROM Gensum WHERE RefineryID = @RefineryID AND PeriodStart BETWEEN @Prev12Date AND @PrevDate AND UOM = 'US' AND FactorSet = '2004' AND Currency = 'USD' AND Scenario = 'CLIENT'

IF (@CrudeAPI IS NULL OR @CrudeSulfur IS NULL) OR (@CrudeAPI NOT BETWEEN @MinAPI AND @MaxAPI) OR (@CrudeSulfur NOT BETWEEN @MinSulfur AND @MaxSulfur) 
BEGIN
	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
	SELECT @SubmissionID, 'CrudeProps', 1, 'Gravity, API', CASE WHEN @CrudeAPI IS NULL THEN 'Missing' ELSE CONVERT(varchar(15), @CrudeAPI) END, @AvgAPI, @MinAPI, @MaxAPI

	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
	SELECT @SubmissionID, 'CrudeProps', 2, 'Sulfur, wt%', CASE WHEN @CrudeSulfur IS NULL THEN 'Missing' ELSE CONVERT(varchar(15), @CrudeSulfur) END, @AvgSulfur, @MinSulfur, @MaxSulfur
END

IF @PrevSubID IS NOT NULL
BEGIN
	INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
	SELECT @SubmissionID, 'Config', u.UnitID, ISNULL(RTRIM(cc.UnitName) + ' (' + RTRIM(cc.ProcessID) + CASE WHEN ISNULL(cc.ProcessType, '') = '' THEN '' ELSE ' - ' + RTRIM(cc.ProcessType) END + ')', RTRIM(cp.UnitName) + ' (' + RTRIM(cp.ProcessID) + ' - ' + CASE WHEN ISNULL(cp.ProcessType, '') = '' THEN '' ELSE ' - ' + RTRIM(cp.ProcessType) END  + ')')
		, ProcessIDOld = CASE 
			WHEN cc.UnitName IS NULL THEN 'Deleted' 
			WHEN cp.UnitName IS NULL THEN 'Added'
			WHEN ISNULL(cc.ProcessID, '') <> ISNULL(cp.ProcessID, '') OR ISNULL(cc.ProcessType, '') <> ISNULL(cp.ProcessType, '') THEN RTRIM(ISNULL(cp.ProcessID, ''))
			END
		, ProcessTypeOld = CASE 
			WHEN cc.UnitName IS NULL THEN NULL
			WHEN cp.UnitName IS NULL THEN NULL
			WHEN ISNULL(cc.ProcessID, '') <> ISNULL(cp.ProcessID, '') OR ISNULL(cc.ProcessType, '') <> ISNULL(cp.ProcessType, '') THEN CASE WHEN RTRIM(cp.ProcessType) = '' THEN '(blank)' ELSE RTRIM(cp.ProcessType) END
			END
		, CapOld = 	CASE WHEN cc.RptCap <> cp.RptCap THEN CONVERT(varchar(15), ISNULL(cp.RptCap, 0)) ELSE '' END
			+ 	CASE WHEN cc.RptStmCap <> cp.RptStmCap THEN ' Steam ' + CONVERT(varchar(15), ISNULL(cp.RptStmCap, 0)) ELSE '' END
		, CapNew = 	CASE WHEN cc.RptCap <> cp.RptCap THEN CONVERT(varchar(15), ISNULL(cc.RptCap, 0)) + '' ELSE '' END
			+ 	CASE WHEN cc.RptStmCap <> cp.RptStmCap THEN ' Steam ' + CONVERT(varchar(15), ISNULL(cc.RptStmCap, 0)) ELSE '' END
	FROM (SELECT DISTINCT UnitID FROM Config WHERE SubmissionID IN (@SubmissionID, @PrevSubID) AND ProcessID NOT IN ('TNK+BLND','OFFCoke','ELECDIST')) u
	LEFT JOIN Config cc ON cc.SubmissionID = @SubmissionID AND cc.UnitID = u.UnitID AND cc.ProcessID NOT IN ('OFFCOKE','TNK+BLND','ELECDIST')
	LEFT JOIN Config cp ON cp.SubmissionID = @PrevSubID AND cp.UnitID = u.UnitID AND cp.ProcessID NOT IN ('OFFCOKE','TNK+BLND','ELECDIST')
	LEFT JOIN ProcessID_LU lu ON lu.ProcessID = ISNULL(cc.ProcessID, cp.ProcessID)
	WHERE (cc.UnitName IS NULL OR cp.UnitName IS NULL OR ISNULL(cc.ProcessID, '') <> ISNULL(cp.ProcessID, '') OR ISNULL(cc.Cap, 0) <> ISNULL(cp.Cap, 0) OR ISNULL(cc.StmCap, 0) <> ISNULL(cp.StmCap, 0))
END

-- KPI Summary
SELECT @DataCheckID = 'KPISummary'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 1, 'EDC, k', 
CONVERT(numeric(9,0), EDC/1000), CONVERT(numeric(9,0), EDC_Avg/1000), 
CONVERT(numeric(9,0), (SELECT Min(EDC)/1000 FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,0), (SELECT Max(EDC)/1000 FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 2, 'UEDC, k', 
CONVERT(numeric(9,0), UEDC/1000), CONVERT(numeric(9,0), UEDC_Avg/1000), 
CONVERT(numeric(9,0), (SELECT Min(UEDC)/1000 FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,0), (SELECT Max(UEDC)/1000 FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 3, 'Refinery Utilization, %', 
CONVERT(numeric(9,1), UtilPcnt), CONVERT(numeric(9,1), UtilPcnt_Avg), 
CONVERT(numeric(9,1), (SELECT Min(UtilPcnt) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,1), (SELECT Max(UtilPcnt) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 4, 'Cash Operating Expenses, 100 USD/UEDC', CONVERT(numeric(9,1), TotCashOpexUEDC), CONVERT(numeric(9,1), TotCashOpexUEDC_Avg), 
CONVERT(numeric(9,1), (SELECT Min(TotCashOpexUEDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,1), (SELECT Max(TotCashOpexUEDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

/*
INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 5, 'Non-Energy Operating Expenses, US$/EDC', CONVERT(numeric(9,2), NEOpexEDC), CONVERT(numeric(9,2), NEOpexEDC_Avg), 
CONVERT(numeric(9,2), (SELECT Min(NEOpexEDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,2), (SELECT Max(NEOpexEDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND SCenario = 'CLIENT'
*/

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 6, 'Maintenance Index, USD/EDC', CONVERT(numeric(9,2), MaintIndex), 
CONVERT(numeric(9,2), (SELECT SUM(MaintIndex*EDC)/SUM(EDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)), 
CONVERT(numeric(9,2), (SELECT Min(MaintIndex) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,2), (SELECT Max(MaintIndex) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 7, 'Mechanical Availability, %', CONVERT(numeric(9,1), MechAvail), 
CONVERT(numeric(9,1), (SELECT SUM(MechAvail*EDC)/SUM(EDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)), 
CONVERT(numeric(9,1), (SELECT Min(MechAvail) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,1), (SELECT Max(MechAvail) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 8, 'Personnel Index, work hours/100 EDC', CONVERT(numeric(9,1), TotWHrEDC), CONVERT(numeric(9,1), TotWHrEDC_Avg), 
CONVERT(numeric(9,1), (SELECT Min(TotWHrEDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,1), (SELECT Max(TotWHrEDC) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 10, 'Energy Intensity Index, EII' + char(153), 
CONVERT(numeric(9,0), EII), CONVERT(numeric(9,0), EII_Avg), 
CONVERT(numeric(9,0), (SELECT Min(EII) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,0), (SELECT Max(EII) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 11, 'Volumetric Expansion Index, %', CONVERT(numeric(9,1), VEI), CONVERT(numeric(9,1), VEI_Avg), 
CONVERT(numeric(9,1), (SELECT Min(VEI) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,1), (SELECT Max(VEI) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value4, Value2, Value3)
SELECT SubmissionID, @DataCheckID, 12, 'Cash Margin, USD/bbl', CONVERT(numeric(9,2), CashMargin), CONVERT(numeric(9,2), CashMargin_Avg), 
CONVERT(numeric(9,2), (SELECT Min(CashMargin) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate)),
CONVERT(numeric(9,2), (SELECT Max(CashMargin) FROM Gensum m WHERE m.RefineryID = Gensum.RefineryID AND m.DataSet = Gensum.DataSet AND m.Currency = Gensum.Currency AND m.Scenario = Gensum.Scenario AND m.PeriodStart BETWEEN @Prev12Date AND @PrevDate))
FROM Gensum WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = 'US' AND Scenario = 'CLIENT'

DELETE FROM @Datachecks
WHERE SubmissionID = @SubmissionID AND datacheckid = @DataCheckID AND Value1 IS NULL AND Value2 IS NULL AND Value3 IS NULL AND Value4 IS NULL


-- Receipts/Shipments, Yield consistency checks
SELECT @DatacheckID = 'RSPerDay'

SELECT rs.SubmissionID, s.PeriodStart, s.NumDays, 
Receipts = SUM(CASE WHEN ProcessID = 'RSCRUDE' THEN rs.Throughput END),
Shipments = SUM(CASE WHEN ProcessID = 'RSPROD' THEN rs.Throughput END),
ReceiptsPerDay = SUM(CASE WHEN ProcessID = 'RSCRUDE' THEN rs.Throughput END)/s.NumDays,
ShipmentsPerDay = SUM(CASE WHEN ProcessID = 'RSPROD' THEN rs.Throughput END)/s.NumDays,
TotInputBbl = (SELECT TotInputbbl FROM MaterialTot WHERE MaterialTot.SubmissionID = rs.SubmissionID),
TotProdBbl = (SELECT SUM(Bbl) FROM Yield WHERE Yield.SubmissionID = rs.SubmissionID AND Yield.Category IN ('PROD','FCHEM','FLUBE','ASP','COKE','SOLV','MPROD'))
INTO #RSChecks
FROM ConfigRS rs INNER JOIN Submissions s ON s.SubmissionID = rs.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND PeriodStart BETWEEN @Prev12Date AND @CurrDate
GROUP BY rs.SubmissionID, s.PeriodStart, s.NumDays

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, @DatacheckID, 1, 'Raw Material Receipts', 
	CONVERT(numeric(15,0), AVG(CASE WHEN SubmissionID = @SubmissionID THEN ReceiptsPerDay END)),
	CONVERT(numeric(15,0), MIN(CASE WHEN SubmissionID <> @SubmissionID AND ReceiptsPerDay > 0 THEN ReceiptsPerDay END)),
	CONVERT(numeric(15,0), MAX(CASE WHEN SubmissionID <> @SubmissionID AND ReceiptsPerDay > 0 THEN ReceiptsPerDay END)),
	CONVERT(numeric(15,0), SUM(CASE WHEN SubmissionID <> @SubmissionID AND Receipts > 0 THEN Receipts END)/SUM(CASE WHEN SubmissionID <> @SubmissionID AND Receipts > 0 THEN NumDays END))
FROM #RSChecks

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, @DatacheckID, 2, 'Product Shipments', 
	CONVERT(numeric(15,0), AVG(CASE WHEN SubmissionID = @SubmissionID THEN ShipmentsPerDay END)),
	CONVERT(numeric(15,0), MIN(CASE WHEN SubmissionID <> @SubmissionID AND ShipmentsPerDay > 0 THEN ShipmentsPerDay END)),
	CONVERT(numeric(15,0), MAX(CASE WHEN SubmissionID <> @SubmissionID AND ShipmentsPerDay > 0 THEN ShipmentsPerDay END)),
	CONVERT(numeric(15,0), SUM(CASE WHEN SubmissionID <> @SubmissionID AND Shipments > 0 THEN Shipments END)/SUM(CASE WHEN SubmissionID <> @SubmissionID AND Shipments > 0 THEN NumDays END))
FROM #RSChecks

DELETE FROM @Datachecks
WHERE SubmissionID = @SubmissionID AND DataCheckID = @DatacheckID
AND CONVERT(real, Value1) >= CONVERT(real, Value2) AND CONVERT(real, Value1) <= CONVERT(real, Value3)


INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, 'RMRvsMatl', 1, '', CONVERT(numeric(15,0), Receipts), CONVERT(numeric(15,0), TotInputBbl), CONVERT(numeric(15,0), ABS(Receipts-TotInputBbl)), CAST(ABS(Receipts-TotInputBbl)/(Receipts+TotInputBbl)*200 AS Numeric(9,1))
FROM #RSChecks WHERE SubmissionId = @SubmissionID AND Receipts > 0 AND TotInputBbl > 0 

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, 'PSvsYields', 1, '', CONVERT(numeric(15,0), Shipments), CONVERT(numeric(15,0), TotProdBbl), CONVERT(numeric(15,0), ABS(Shipments-TotProdBbl)), CAST(ABS(Shipments-TotProdBbl)/(Shipments+TotProdBbl)*200 AS Numeric(9,1))
FROM #RSChecks WHERE SubmissionId = @SubmissionID AND Shipments > 0 AND TotProdBbl > 0 

DELETE FROM @DataChecks WHERE DatacheckID IN ('RMRvsMatl','PSvsYields') AND CAST(Value4 as real)<=10
DROP TABLE #RSChecks

-- Missing Opex data
DECLARE @OCCSal real, @MPSSal real, @OCCBen real, @MPSBen real, 
	@MaintMatl real, @ContMaintLabor real, @OthCont real, 
	@Equip real, @Tax real, @Insur real, @Envir real, @OthNonVol real, 
	@GAPers real, @Antiknock real, @Chemicals real, @Catalysts real, @Royalties real, 
	@PurElec real, @PurSteam real, @PurOth real, @PurFG real, @PurLiquid real, @PurSolid real, 
	@RefProdFG real, @RefProdOth real, @OthVol real

SELECT @OCCSal = OCCSal, @MPSSal = MPSSal, @OCCBen = OCCBen, @MPSBen = MPSBen, @MaintMatl = MaintMatl, @ContMaintLabor = ContMaintLabor, @OthCont = OthCont, @Equip = Equip, @Tax = Tax, @Insur = Insur, @Envir = Envir, @OthNonVol = OthNonVol, @GAPers = GAPers, @Antiknock = Antiknock, @Chemicals = Chemicals, @Catalysts = Catalysts, @Royalties = Royalties, @PurElec = PurElec, @PurSteam = PurSteam, @PurOth = PurOth, @PurFG = PurFG, @PurLiquid = PurLiquid, @PurSolid = PurSolid, @RefProdFG = RefProdFG, @RefProdOth = RefProdOth, @OthVol = OthVol
FROM Opex
WHERE SubmissionID = @SubmissionID AND DataType = 'ADJ' AND Currency = 'USD'

SELECT @DataCheckID = 'OpexMiss'
IF ISNULL(@OCCSal,0)+ ISNULL(@MPSSal, 0) + ISNULL(@OCCBen, 0) + ISNULL(@MPSBen, 0) + ISNULL(@MaintMatl, 0) + ISNULL(@ContMaintLabor, 0) + ISNULL(@OthCont, 0) 
	+ ISNULL(@Equip, 0) + ISNULL(@Tax, 0) + ISNULL(@Insur, 0) + ISNULL(@Envir, 0) + ISNULL(@OthNonVol, 0) 
	+ ISNULL(@GAPers, 0) + ISNULL(@Antiknock, 0) + ISNULL(@Chemicals, 0) + ISNULL(@Catalysts, 0) + ISNULL(@Royalties, 0) 
	+ ISNULL(@PurOth, 0) + ISNULL(@OthVol,0) > 0
BEGIN
	IF ISNULL(@OCCSal, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 1, 'O,C&C Salaries and Wages')
	IF ISNULL(@MPSSal, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 2, 'M,P&S Salaries and Wages')
	IF ISNULL(@OCCBen, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 3, 'O,C&C Benefits')
	IF ISNULL(@MPSBen, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 4, 'M,P&S Benefits')
	IF ISNULL(@MaintMatl, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 5, 'Maintenance Materials')
	IF ISNULL(@ContMaintLabor, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 6, 'Contract Maintenance Labor')
	IF ISNULL(@OthCont, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 7, 'Other Contract Services')
	IF ISNULL(@Envir, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 11, 'Environmental Expenses')
	IF ISNULL(@OthNonVol, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 12, 'Other Non-Volume-Related Expenses')
	IF ISNULL(@GAPers, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 13, 'Utilized G&A Personnel Costs')
	IF ISNULL(@Chemicals, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 15, 'Chemicals')
	IF ISNULL(@Catalysts, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 16, 'Catalysts')
	IF ISNULL(@PurElec, 0) + ISNULL(@PurSteam, 0) + ISNULL(@PurFG, 0) + ISNULL(@PurLiquid, 0) + ISNULL(@PurSolid, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 17, 'Purchased Energy (Missing Energy Data)')
	IF ISNULL(@PurOth, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 25, 'Purchased Utilities Other than Electric and Steam')
	IF ISNULL(@RefProdFG, 0) + ISNULL(@RefProdOth, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 26, 'Produced Energy (Missing Energy data)')
	IF ISNULL(@OthVol, 0) = 0
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 30, 'Other Volume-Related Expenses')
END
ELSE
		INSERT @Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
		VALUES (@SubmissionID, @DataCheckID, 1, '* No expenses reported.')

IF EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'FCC' AND UtilPcnt > 0.1)
BEGIN
	IF ISNULL((SELECT SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND EnergyType = 'COC' AND TransType = 'PRO'), 0) <= 0
	BEGIN
		INSERT @Datachecks (SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1)
		SELECT @SubmissionID, 'FCCNoCOC', 1, 'Catalytic Cracking Utilized Capacity, b/d', CONVERT(numeric(15, 0), SUM(UtilPcnt*Cap/100))
		FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'FCC' AND UtilPcnt > 0.1
		INSERT @Datachecks (SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1)
		SELECT @SubmissionID, 'FCCNoCOC', 2, 'Coke on Catalyst Reported on Table 16', CONVERT(numeric(15,0), ISNULL(SUM(RptSource), 0))
		FROM Energy WHERE SubmissionID = @SubmissionID AND EnergyType = 'COC' AND TransType = 'PRO'
	END
END

IF EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'CALCIN' AND UtilPcnt > 0.1)
BEGIN
	IF ISNULL((SELECT SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND EnergyType = 'MC' AND TransType = 'PRO'), 0) <= 0
	BEGIN
		INSERT @Datachecks (SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1)
		SELECT @SubmissionID, 'MktCoke', 1, 'Coke Calciner Capacity', CONVERT(numeric(15,0), SUM(UtilPcnt*Cap/100))
		FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'CALCIN' AND UtilPcnt > 0.1

		INSERT @Datachecks (SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value2)
		SELECT @SubmissionID, 'MktCoke', 2, 'Marketable Coke/Calciner Coke Loss', CONVERT(numeric(15,0), ISNULL(SUM(RptSource), 0))
		FROM Energy WHERE SubmissionID = @SubmissionID AND EnergyType = 'MC' AND TransType = 'PRO'
	END
END

-- Missing Non-turnaround maintenance costs
IF EXISTS (SELECT * FROM MaintRout WHERE SubmissionID = @SubmissionID AND RoutCostLocal > 0)
	INSERT @Datachecks (SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1)
	SELECT m.SubmissionID, 'NTARptCost', lu.SortKey, lu.Description, SUBSTRING(c.UnitName, 1, 12)
	FROM MaintRout m LEFT JOIN Config c ON c.SubmissionID = m.SubmissionID AND c.UnitID = m.UnitID
	LEFT JOIN ProcessID_LU lu ON lu.ProcessID = m.ProcessID
	WHERE m.ProcessID NOT IN ('OFFCOKE','MARINE') AND ISNULL(RoutCostLocal, 0) = 0
	AND m.SubmissionID = @SubmissionID
ELSE
	INSERT @Datachecks (SubmissionID, DataCheckID, ItemSortKey, ItemDesc)
	VALUES (@SubmissionID, 'NTARptCost', 1, 'No non-turnaround costs were reported.')

-- Personnel Costs, Company and contractor
SELECT s.SubmissionID, s.RefineryID, s.PeriodStart, p.Currency, p.OCCComp, p.MPSComp, p.ContMaintLaborCost, p.OthContLaborCost, OCCCompNonTASTH, MPSCompNonTASTH, ContMaintLaborWHr, OthContWHr, OCCNumPers, MPSNumPers, GANonMaintWHr
INTO #PersTot
FROM PersTotCalc p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE p.Currency = @RptCurrency AND s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart BETWEEN @Prev12Date AND @CurrDate
AND FactorSet = @FactorSet

SELECT @DataCheckId = 'ContRate'

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, @DatacheckID, 4, '  Imputed Charge Rate for Maintenance', 
	CONVERT(numeric(15,2), AVG(CASE WHEN SubmissionID = @SubmissionID THEN ContMaintLaborCost END)),
	CONVERT(numeric(15,2), MIN(CASE WHEN SubmissionID <> @SubmissionID AND ContMaintLaborCost > 0 THEN ContMaintLaborCost END)),
	CONVERT(numeric(15,2), MAX(CASE WHEN SubmissionID <> @SubmissionID AND ContMaintLaborCost > 0 THEN ContMaintLaborCost END)),
	CONVERT(numeric(15,2), SUM(CASE WHEN SubmissionID <> @SubmissionID AND ContMaintLaborCost > 0 THEN ContMaintLaborCost*ContMaintLaborWHr END)/SUM(CASE WHEN SubmissionID <> @SubmissionID AND ContMaintLaborCost > 0 THEN ContMaintLaborWHr END))
FROM #PersTot 

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, @DatacheckID, 14, '  Imputed Charge Rate for Other Contract Services', 
	CONVERT(numeric(15,2), AVG(CASE WHEN SubmissionID = @SubmissionID THEN OthContLaborCost END)),
	CONVERT(numeric(15,2), MIN(CASE WHEN SubmissionID <> @SubmissionID AND OthContLaborCost > 0 THEN OthContLaborCost END)),
	CONVERT(numeric(15,2), MAX(CASE WHEN SubmissionID <> @SubmissionID AND OthContLaborCost > 0 THEN OthContLaborCost END)),
	CONVERT(numeric(15,2), SUM(CASE WHEN SubmissionID <> @SubmissionID AND OthContLaborCost > 0 THEN OthContLaborCost*OthContWHr END)/SUM(CASE WHEN SubmissionID <> @SubmissionID AND OthContLaborCost > 0 THEN OthContWHr END))
FROM #PersTot 

DELETE FROM @Datachecks
WHERE SubmissionID = @SubmissionID AND DataCheckID = @DatacheckID
AND Value1 IS NOT NULL AND (CONVERT(real, Value1) >= CONVERT(real, Value2) AND CONVERT(real, Value1) <= CONVERT(real, Value3))

DECLARE @ContPersCats TABLE (
	PersID varchar(8) NOT NULL,
	Category varchar(50) NOT NULL,
	SortKey tinyint NOT NULL
)
IF EXISTS (SELECT * FROM @Datachecks WHERE SubmissionID = @SubmissionID AND DatacheckID = @DataCheckID AND ItemSortKey = 4)
BEGIN
	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
	SELECT @SubmissionID, @DatacheckID, 1, 'Contract Maintenance Labor Expenses, ' + RTRIM(Currency) + '/1000', CONVERT(numeric(15,1), ContMaintLabor)
	FROM Opex WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc)
	VALUES (@SubmissionID, @DatacheckID, 10, '')

	INSERT @ContPersCats VALUES ('OCCMA', 'Total O,C&C Maintenance Contractor Hours', 2)
	INSERT @ContPersCats VALUES ('MPSMA', 'Total M,P&S Maintenance Contractor Hours', 3)
END
IF EXISTS (SELECT * FROM @Datachecks WHERE SubmissionID = @SubmissionID AND DatacheckID = @DataCheckID AND ItemSortKey = 14)
BEGIN
	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
	SELECT @SubmissionID, @DatacheckID, 11, 'Other Contract Services, ' + RTRIM(Currency) + '/1000', CONVERT(numeric(15,1), OthCont)
	FROM Opex WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

	INSERT @ContPersCats VALUES ('OCCPO', 'Total O,C&C Non-Maint Contractor Hours', 12)
	INSERT @ContPersCats VALUES ('OCCAS', 'Total O,C&C Non-Maint Contractor Hours', 12)
	INSERT @ContPersCats VALUES ('MPSPO', 'Total M,P&S Non-Maint Contractor Hours', 13)
	INSERT @ContPersCats VALUES ('MPSTS', 'Total M,P&S Non-Maint Contractor Hours', 13)
	INSERT @ContPersCats VALUES ('MPSAS', 'Total M,P&S Non-Maint Contractor Hours', 13)
END
INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
SELECT @SubmissionID, @DataCheckId, cpc.SortKey, cpc.Category, CONVERT(numeric(15,0), SUM(Contract))
FROM Pers p INNER JOIN @ContPersCats cpc ON cpc.PersID = p.PersID
WHERE SubmissionID = @SubmissionID GROUP BY cpc.SortKey, cpc.Category


SELECT @DataCheckId = 'EmpWages'

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, @DatacheckID, 4, '  Average O,C&C Annual Compensation, ' + Currency, 
	CONVERT(numeric(15,0), AVG(CASE WHEN SubmissionID = @SubmissionID THEN OCCComp END)),
	CONVERT(numeric(15,0), MIN(CASE WHEN SubmissionID <> @SubmissionID AND OCCComp > 0 THEN OCCComp END)),
	CONVERT(numeric(15,0), MAX(CASE WHEN SubmissionID <> @SubmissionID AND OCCComp > 0 THEN OCCComp END)),
	CONVERT(numeric(15,0), SUM(CASE WHEN SubmissionID <> @SubmissionID AND OCCComp > 0 THEN OCCComp*OCCNumPers END)/SUM(CASE WHEN SubmissionID <> @SubmissionID AND OCCComp > 0 THEN OCCNumPers END))
FROM #PersTot GROUP BY #PersTot.Currency

INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4)
SELECT @SubmissionID, @DatacheckID, 14, '  Average M,P&S Annual Compensation, ' + Currency, 
	CONVERT(numeric(15,0), AVG(CASE WHEN SubmissionID = @SubmissionID THEN MPSComp END)),
	CONVERT(numeric(15,0), MIN(CASE WHEN SubmissionID <> @SubmissionID AND MPSComp > 0 THEN MPSComp END)),
	CONVERT(numeric(15,0), MAX(CASE WHEN SubmissionID <> @SubmissionID AND MPSComp > 0 THEN MPSComp END)),
	CONVERT(numeric(15,0), SUM(CASE WHEN SubmissionID <> @SubmissionID AND MPSComp > 0 THEN MPSComp*MPSNumPers END)/SUM(CASE WHEN SubmissionID <> @SubmissionID AND MPSComp > 0 THEN MPSNumPers END))
FROM #PersTot GROUP BY #PersTot.Currency

DELETE FROM @Datachecks
WHERE SubmissionID = @SubmissionID AND DataCheckID = @DatacheckID
AND Value1 IS NOT NULL AND CONVERT(real, Value1) >= CONVERT(real, Value2) AND CONVERT(real, Value1) <= CONVERT(real, Value3)


IF EXISTS (SELECT * FROM @Datachecks WHERE SubmissionID = @SubmissionID AND DatacheckID = @DataCheckID AND ItemSortKey = 4)
BEGIN
	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
	SELECT @SubmissionID, @DatacheckID, 1, 'O,C&C Salaries, Wages & Benefits, ' + RTRIM(Currency) + '/1000', CONVERT(numeric(15,0), ISNULL(Opex.OCCSal,0) + ISNULL(Opex.OCCBen,0))
	FROM Opex WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
	SELECT @SubmissionID, @DataCheckId, 3, 'Number of O,C&C Personel', CONVERT(numeric(15,1), OCCNumPers)
	FROM #PersTot
	WHERE SubmissionID = @SubmissionID

	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc)
	VALUES (@SubmissionID, @DatacheckID, 10, '')
END
IF EXISTS (SELECT * FROM @Datachecks WHERE SubmissionID = @SubmissionID AND DatacheckID = @DataCheckID AND ItemSortKey = 14)
BEGIN
	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
	SELECT @SubmissionID, @DatacheckID, 11, 'M,P&S Salaries, Wages & Benefits, ' + RTRIM(Currency) + '/1000', CONVERT(numeric(15,0), ISNULL(Opex.MPSSal,0) + ISNULL(Opex.MPSBen,0))
	FROM Opex 
	WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

	INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
	SELECT @SubmissionID, @DataCheckId, 13, 'Number of M,P&S Personel', CONVERT(numeric(15,1), MPSNumPers)
	FROM #PersTot
	WHERE SubmissionID = @SubmissionID
END
INSERT @Datachecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1)
SELECT @SubmissionID, @DatacheckID, ItemSortKey+1, 'Annual ' + RTRIM(ItemDesc), CONVERT(int, CONVERT(real, Value1)/@FractionOfYear)
FROM @Datachecks
WHERE DataCheckID = @DatacheckID AND ItemSortKey IN (1, 11)

DROP TABLE #PersTot


IF EXISTS (SELECT * FROM Pers WHERE SubmissionID = @SubmissionID AND NumPers > 0 AND PersId IN ('OCCPO','OCCMA','OCCAS','MPSPO','MPSMA','MPSTS','MPSAS'))
BEGIN
	SELECT @DataCheckID = 'HrsPerPers'
	INSERT @DataChecks(SubmissionID, DatacheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3)
	SELECT @SubmissionID, @DatacheckID, lu.PersCode, lu.Description, 
		CASE WHEN NumPers IS NULL THEN 'Missing' ELSE CONVERT(varchar(15), CONVERT(Numeric(9,1), NumPers)) END
		, CONVERT(Numeric(9,0), STH)
		, CASE WHEN NumPers > 0 THEN CONVERT(numeric(9,1), STH/NumPers/s.NumDays*7.0) ELSE NULL END
	FROM Pers p INNER JOIN Pers_LU lu ON lu.PersID = p.PersID
	INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID
	WHERE p.PersId IN ('OCCPO','OCCMA','OCCAS','MPSPO','MPSMA','MPSTS','MPSAS')
	AND p.SubmissionID = @SubmissionID

	DELETE FROM @Datachecks
	WHERE DataCheckID = @DatacheckID  AND CONVERT(real, Value3) BETWEEN 33 AND 50
END

DECLARE @ThermalGE float, @ElecSoldDist float, @AvgGenEff float
SELECT @ThermalGE = SUM(UsageMBTU)
FROM Energy WHERE EnergyType = 'GE'
AND SubmissionID = @SubmissionID

SELECT @ElecSoldDist = SUM(UsageMWH)
FROM Electric WHERE TransType IN ('SOL','DST')
AND SubmissionID = @SubmissionID

SELECT @AvgGenEff = SUM(SourceMWH*GenEff)/SUM(SourceMWH)
FROM Electric WHERE TransType IN ('PRO') AND EnergyType In ('ELE','COG') AND SourceMWH > 0 AND GenEff > 0
AND SubmissionID = @SubmissionID

SELECT @ThermalGE = ISNULL(@ThermalGE, 0), @ElecSoldDist = ISNULL(@ElecSoldDist, 0), @AvgGenEff = ISNULL(@AvgGenEff, 9090)
IF @ElecSoldDist > 0 OR @ThermalGE > 0
BEGIN
	IF @ElecSoldDist = 0 AND @ThermalGE > 0
		INSERT @DataChecks(SubmissionID, DatacheckID, Value1, Value2, Value3, Value4)
		SELECT @SubmissionID, 'EnergyGE', @ThermalGE, @ElecSoldDist, NULL, @AvgGenEff 
	ELSE IF ABS(@ThermalGE/@ElecSoldDist*1000 - @AvgGenEff) > 200
		INSERT @DataChecks(SubmissionID, DatacheckID, Value1, Value2, Value3, Value4)
		SELECT @SubmissionID, 'EnergyGE', @ThermalGE, @ElecSoldDist, @ThermalGE/@ElecSoldDist*1000, @AvgGenEff 
END


UPDATE @Datachecks
SET 	ItemSortKey = 1 
WHERE ItemSortKey IS NULL

UPDATE @Datachecks
SET 	ItemDesc = ''
WHERE ItemDesc IS NULL


-- Beginning of formatting routine to put thousand separators in
UPDATE @Datachecks
SET Value1 = CASE (CASE WHEN CHARINDEX('.', Value1) = 0 THEN LEN(Value1) ELSE CHARINDEX('.', Value1)-1 END - CASE WHEN SUBSTRING(Value1, 1, 1) = '-' THEN 1 ELSE 0 END - 1)/3
	WHEN 1 THEN STUFF(Value1, CASE WHEN CHARINDEX('.', Value1) > 4 THEN CHARINDEX('.', Value1) - 3 ELSE LEN(Value1) - 2 END, 0, ',')
	WHEN 2 THEN CASE WHEN CHARINDEX('.', Value1) > 4 THEN STUFF(STUFF(Value1, CHARINDEX('.', Value1) - 6, 0, ','), CHARINDEX('.', Value1) - 2, 0, ',') ELSE STUFF(STUFF(Value1, LEN(Value1) - 5, 0, ','), LEN(Value1) - 1, 0, ',') END
	WHEN 3 THEN CASE WHEN CHARINDEX('.', Value1) > 4 THEN STUFF(STUFF(STUFF(Value1, CHARINDEX('.', Value1) - 6, 0, ','), CHARINDEX('.', Value1) - 2, 0, ','), CHARINDEX('.', Value1) - 9, 0, ',') ELSE STUFF(STUFF(STUFF(Value1, LEN(Value1) - 2, 0, ','), LEN(Value1) - 5, 0, ','), LEN(Value1) -8, 0, ',') END
	ELSE Value1
	END
WHERE ISNUMERIC(Value1) = 1 AND CHARINDEX(',', Value1) = 0

UPDATE @Datachecks
SET Value2 = CASE (CASE WHEN CHARINDEX('.', Value2) = 0 THEN LEN(Value2) ELSE CHARINDEX('.', Value2)-1 END - CASE WHEN SUBSTRING(Value2, 1, 1) = '-' THEN 1 ELSE 0 END - 1)/3
	WHEN 1 THEN STUFF(Value2, CASE WHEN CHARINDEX('.', Value2) > 4 THEN CHARINDEX('.', Value2) - 3 ELSE LEN(Value2) - 2 END, 0, ',')
	WHEN 2 THEN CASE WHEN CHARINDEX('.', Value2) > 4 THEN STUFF(STUFF(Value2, CHARINDEX('.', Value2) - 6, 0, ','), CHARINDEX('.', Value2) - 2, 0, ',') ELSE STUFF(STUFF(Value2, LEN(Value2) - 5, 0, ','), LEN(Value2) - 1, 0, ',') END
	WHEN 3 THEN CASE WHEN CHARINDEX('.', Value2) > 4 THEN STUFF(STUFF(STUFF(Value2, CHARINDEX('.', Value2) - 6, 0, ','), CHARINDEX('.', Value2) - 2, 0, ','), CHARINDEX('.', Value2) - 9, 0, ',') ELSE STUFF(STUFF(STUFF(Value2, LEN(Value2) - 2, 0, ','), LEN(Value2) - 5, 0, ','), LEN(Value2) -8, 0, ',') END
	ELSE Value2
	END
WHERE ISNUMERIC(Value2) = 1 AND CHARINDEX(',', Value2) = 0

UPDATE @Datachecks
SET Value3 = CASE (CASE WHEN CHARINDEX('.', Value3) = 0 THEN LEN(Value3) ELSE CHARINDEX('.', Value3)-1 END - CASE WHEN SUBSTRING(Value3, 1, 1) = '-' THEN 1 ELSE 0 END - 1)/3
	WHEN 1 THEN STUFF(Value3, CASE WHEN CHARINDEX('.', Value3) > 4 THEN CHARINDEX('.', Value3) - 3 ELSE LEN(Value3) - 2 END, 0, ',')
	WHEN 2 THEN CASE WHEN CHARINDEX('.', Value3) > 4 THEN STUFF(STUFF(Value3, CHARINDEX('.', Value3) - 6, 0, ','), CHARINDEX('.', Value3) - 2, 0, ',') ELSE STUFF(STUFF(Value3, LEN(Value3) - 5, 0, ','), LEN(Value3) - 1, 0, ',') END
	WHEN 3 THEN CASE WHEN CHARINDEX('.', Value3) > 4 THEN STUFF(STUFF(STUFF(Value3, CHARINDEX('.', Value3) - 6, 0, ','), CHARINDEX('.', Value3) - 2, 0, ','), CHARINDEX('.', Value3) - 9, 0, ',') ELSE STUFF(STUFF(STUFF(Value3, LEN(Value3) - 2, 0, ','), LEN(Value3) - 5, 0, ','), LEN(Value3) -8, 0, ',') END
	ELSE Value3
	END
WHERE ISNUMERIC(Value3) = 1 AND CHARINDEX(',', Value3) = 0

UPDATE @Datachecks
SET Value4 = CASE (CASE WHEN CHARINDEX('.', Value4) = 0 THEN LEN(Value4) ELSE CHARINDEX('.', Value4)-1 END - CASE WHEN SUBSTRING(Value4, 1, 1) = '-' THEN 1 ELSE 0 END - 1)/3
	WHEN 1 THEN STUFF(Value4, CASE WHEN CHARINDEX('.', Value4) > 4 THEN CHARINDEX('.', Value4) - 3 ELSE LEN(Value4) - 2 END, 0, ',')
	WHEN 2 THEN CASE WHEN CHARINDEX('.', Value4) > 4 THEN STUFF(STUFF(Value4, CHARINDEX('.', Value4) - 6, 0, ','), CHARINDEX('.', Value4) - 2, 0, ',') ELSE STUFF(STUFF(Value4, LEN(Value4) - 5, 0, ','), LEN(Value4) - 1, 0, ',') END
	WHEN 3 THEN CASE WHEN CHARINDEX('.', Value4) > 4 THEN STUFF(STUFF(STUFF(Value4, CHARINDEX('.', Value4) - 6, 0, ','), CHARINDEX('.', Value4) - 2, 0, ','), CHARINDEX('.', Value4) - 9, 0, ',') ELSE STUFF(STUFF(STUFF(Value4, LEN(Value4) - 2, 0, ','), LEN(Value4) - 5, 0, ','), LEN(Value4) -8, 0, ',') END
	ELSE Value4
	END
WHERE ISNUMERIC(Value4) = 1 AND CHARINDEX(',', Value4) = 0

UPDATE @Datachecks
SET Value5 = CASE (CASE WHEN CHARINDEX('.', Value5) = 0 THEN LEN(Value5) ELSE CHARINDEX('.', Value5)-1 END - CASE WHEN SUBSTRING(Value5, 1, 1) = '-' THEN 1 ELSE 0 END - 1)/3
	WHEN 1 THEN STUFF(Value5, CASE WHEN CHARINDEX('.', Value5) > 4 THEN CHARINDEX('.', Value5) - 3 ELSE LEN(Value5) - 2 END, 0, ',')
	WHEN 2 THEN CASE WHEN CHARINDEX('.', Value5) > 4 THEN STUFF(STUFF(Value5, CHARINDEX('.', Value5) - 6, 0, ','), CHARINDEX('.', Value5) - 2, 0, ',') ELSE STUFF(STUFF(Value5, LEN(Value5) - 5, 0, ','), LEN(Value5) - 1, 0, ',') END
	WHEN 3 THEN CASE WHEN CHARINDEX('.', Value5) > 4 THEN STUFF(STUFF(STUFF(Value5, CHARINDEX('.', Value5) - 6, 0, ','), CHARINDEX('.', Value5) - 2, 0, ','), CHARINDEX('.', Value5) - 9, 0, ',') ELSE STUFF(STUFF(STUFF(Value5, LEN(Value5) - 2, 0, ','), LEN(Value5) - 5, 0, ','), LEN(Value5) -8, 0, ',') END
	ELSE Value5
	END
WHERE ISNUMERIC(Value5) = 1 AND CHARINDEX(',', Value5) = 0

UPDATE @Datachecks
SET Value6 = CASE (CASE WHEN CHARINDEX('.', Value6) = 0 THEN LEN(Value6) ELSE CHARINDEX('.', Value6)-1 END - CASE WHEN SUBSTRING(Value6, 1, 1) = '-' THEN 1 ELSE 0 END - 1)/3
	WHEN 1 THEN STUFF(Value6, CASE WHEN CHARINDEX('.', Value6) > 4 THEN CHARINDEX('.', Value6) - 3 ELSE LEN(Value6) - 2 END, 0, ',')
	WHEN 2 THEN CASE WHEN CHARINDEX('.', Value6) > 4 THEN STUFF(STUFF(Value6, CHARINDEX('.', Value6) - 6, 0, ','), CHARINDEX('.', Value6) - 2, 0, ',') ELSE STUFF(STUFF(Value6, LEN(Value6) - 5, 0, ','), LEN(Value6) - 1, 0, ',') END
	WHEN 3 THEN CASE WHEN CHARINDEX('.', Value6) > 4 THEN STUFF(STUFF(STUFF(Value6, CHARINDEX('.', Value6) - 6, 0, ','), CHARINDEX('.', Value6) - 2, 0, ','), CHARINDEX('.', Value6) - 9, 0, ',') ELSE STUFF(STUFF(STUFF(Value6, LEN(Value6) - 2, 0, ','), LEN(Value6) - 5, 0, ','), LEN(Value6) -8, 0, ',') END
	ELSE Value6
	END
WHERE ISNUMERIC(Value6) = 1 AND CHARINDEX(',', Value6) = 0

UPDATE @Datachecks
SET Value7 = CASE (CASE WHEN CHARINDEX('.', Value7) = 0 THEN LEN(Value7) ELSE CHARINDEX('.', Value7)-1 END - CASE WHEN SUBSTRING(Value7, 1, 1) = '-' THEN 1 ELSE 0 END - 1)/3
	WHEN 1 THEN STUFF(Value7, CASE WHEN CHARINDEX('.', Value7) > 4 THEN CHARINDEX('.', Value7) - 3 ELSE LEN(Value7) - 2 END, 0, ',')
	WHEN 2 THEN CASE WHEN CHARINDEX('.', Value7) > 4 THEN STUFF(STUFF(Value7, CHARINDEX('.', Value7) - 6, 0, ','), CHARINDEX('.', Value7) - 2, 0, ',') ELSE STUFF(STUFF(Value7, LEN(Value7) - 5, 0, ','), LEN(Value7) - 1, 0, ',') END
	WHEN 3 THEN CASE WHEN CHARINDEX('.', Value7) > 4 THEN STUFF(STUFF(STUFF(Value7, CHARINDEX('.', Value7) - 6, 0, ','), CHARINDEX('.', Value7) - 2, 0, ','), CHARINDEX('.', Value7) - 9, 0, ',') ELSE STUFF(STUFF(STUFF(Value7, LEN(Value7) - 2, 0, ','), LEN(Value7) - 5, 0, ','), LEN(Value7) -8, 0, ',') END
	ELSE Value7
	END
WHERE ISNUMERIC(Value7) = 1 AND CHARINDEX(',', Value7) = 0

-- Format negative fields with () instead of -
UPDATE @Datachecks
SET Value3 = '(' + RTRIM(SUBSTRING(Value3, 2, LEN(Value3)-1)) + ')'
WHERE DatacheckId = 'Losses' AND Value3 LIKE '-%'

UPDATE @Datachecks
SET 	Value1 = CASE WHEN Value1 LIKE '-%' THEN '(' + RTRIM(SUBSTRING(Value1, 2, LEN(Value1)-1)) + ')' ELSE Value1 END,
	Value2 = CASE WHEN Value2 LIKE '-%' THEN '(' + RTRIM(SUBSTRING(Value2, 2, LEN(Value2)-1)) + ')' ELSE Value2 END,
	Value3 = CASE WHEN Value3 LIKE '-%' THEN '(' + RTRIM(SUBSTRING(Value3, 2, LEN(Value3)-1)) + ')' ELSE Value3 END,
	Value4 = CASE WHEN Value4 LIKE '-%' THEN '(' + RTRIM(SUBSTRING(Value4, 2, LEN(Value4)-1)) + ')' ELSE Value4 END
WHERE DatacheckId = 'KPISummary' AND ItemDesc LIKE 'Cash Margin%'

DELETE FROM Datachecks WHERE SubmissionID = @SubmissionID AND DataCheckID NOT IN ('CrudeTrans', 'NoStudy$')

INSERT Datachecks(SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4, Value5, Value6, Value7)
SELECT SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value2, Value3, Value4, Value5, Value6, Value7
FROM @DataChecks
