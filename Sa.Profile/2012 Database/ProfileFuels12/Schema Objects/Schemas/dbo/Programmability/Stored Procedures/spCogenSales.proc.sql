﻿CREATE PROC [dbo].[spCogenSales](@RefineryID varchar(6)) 
AS
DECLARE @SubmissionID int, @Cogen real, @ThermalSales real, @ThermalSalesKLocal real, @ElecSalesMWH real, @ElecSalesKLocal real
DECLARE @ElecSalesMBTU real, @TotSales real, @TotT16Value real, @Unallocated real
DECLARE @msg varchar(255)

DECLARE cData CURSOR FAST_FORWARD
FOR
	SELECT o.SubmissionID, ISNULL(o.Cogen, 0) AS Cogen, t.ThermalMBTU, t.ThermalSalesKLocal, e.ElecSalesMWH, e.ElecSalesKLocal
	FROM Opex o LEFT JOIN (SELECT SubmissionID, SUM(ABS(RptSource)) AS ThermalMBTU, SUM(ABS(RptSource*RptPriceLocal))/1000 AS ThermalSalesKLocal
		FROM Energy WHERE TransType = 'SOL' AND EnergyType <> 'GE' GROUP BY SubmissionID) t ON t.SubmissionID = o.SubmissionID
	LEFT JOIN (SELECT SubmissionID, SUM(ABS(RptMWH)) AS ElecSalesMWH, SUM(ABS(RptMWH)*PriceLocal/100) AS ElecSalesKLocal
		FROM Electric WHERE TransType = 'SOL' GROUP BY SubmissionID) e ON e.SubmissionID = o.SubmissionID
	WHERE o.DataType = 'RPT' AND o.SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID)

OPEN cData
FETCH NEXT FROM cData INTO @SubmissionID, @Cogen, @ThermalSales, @ThermalSalesKLocal, @ElecSalesMWH, @ElecSalesKLocal
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @Cogen IS NOT NULL
	BEGIN
		SELECT 	@ElecSalesMBTU = ISNULL(@ElecSalesMWH*9.09,0)
		SELECT 	@TotSales = ISNULL(@ThermalSales, 0) + ISNULL(@ElecSalesMBTU, 0), 
			@TotT16Value = ISNULL(@ThermalSalesKLocal, 0) + ISNULL(@ElecSalesKLocal, 0)
		IF @Cogen < 0
		BEGIN
			UPDATE Opex 
			SET OthRevenue = ISNULL(OthRevenue, 0) + ISNULL(@Cogen, 0), Cogen = NULL
			WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

			SELECT @msg = 'Added ' + CONVERT(varchar(20), @Cogen) + ' to Other Revenue'
			EXEC spLogMessage @SubmissionID, 'spCogenSales', 'I', @msg
		END

		IF @Cogen = 0 
		BEGIN
			IF @TotT16Value <> 0
			BEGIN
				EXEC spLogMessage @SubmissionID, 'spCogenSales', 'I',  'Cogen is 0 and Table 16 is not. Will use table 16 values, but the client should confirm those values.'
				UPDATE Opex SET Cogen = NULL
				WHERE SubmissionID = @SubmissionID
			END
		END

		IF @Cogen > 0
		BEGIN
			IF @TotSales = 0
			BEGIN
				UPDATE Opex 
				SET OthRevenue = ISNULL(OthRevenue, 0) + ISNULL(@Cogen, 0), Cogen = NULL
				WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

				SELECT @msg = 'Added ' + CONVERT(varchar(20), @Cogen) + ' to Other Revenue'
				EXEC spLogMessage @SubmissionID, 'spCogenSales', 'I',  @msg
			END
			ELSE
			BEGIN 
				IF ABS(@Cogen-@TotT16Value)/@Cogen*100 > 5
				BEGIN
					IF (@ThermalSales > 0 AND @ThermalSalesKLocal = 0) OR (@ElecSalesMWH > 0 AND @ElecSalesKLocal = 0)
					BEGIN
						EXEC spLogMessage @SubmissionID, 'spCogenSales', 'I',  'Will estimate prices so that calculated values match old Table 4 values.'
						SELECT @Unallocated = @Cogen
						IF @ThermalSalesKLocal > 0 AND @ElecSalesMWH > 0
						BEGIN
							SELECT @Unallocated = @Unallocated - @ThermalSalesKLocal
							IF @Unallocated < 0
								SELECT @Unallocated = 0
							UPDATE Electric
							SET PriceLocal = @Unallocated*1000/@ElecSalesMWH
							WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND RptMWH <> 0
						END
						IF @ElecSalesKLocal > 0 AND @ThermalSales > 0
						BEGIN
							SELECT @Unallocated = @Unallocated - @ElecSalesKLocal
							IF @Unallocated < 0
								SELECT @Unallocated = 0
							UPDATE Energy
							SET RptPriceLocal = @Unallocated*1000/@ThermalSales
							WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType <> 'GE' AND RptSource <> 0
						END
						IF @ThermalSalesKLocal = 0 AND @ElecSalesKLocal = 0
						BEGIN
							SELECT 	@ThermalSalesKLocal = @Cogen*(@ThermalSales/@TotSales),
								@ElecSalesKLocal = @Cogen*(@ElecSalesMBTU/@TotSales)
							IF @ThermalSales > 0
								UPDATE Energy
								SET RptPriceLocal = @ThermalSalesKLocal*1000/@ThermalSales
								WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType <> 'GE' AND RptSource <> 0
							IF @ElecSalesMWH > 0
								UPDATE Electric
								SET PriceLocal = @ElecSalesKLocal*1000/@ElecSalesMWH
								WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND RptMWH <> 0
						END
					END
					ELSE
						EXEC spLogMessage @SubmissionID, 'spCogenSales', 'I',  'Difference between table 4 and table 16. Will use table 16 values, but the client should confirm.'
				END
			END
		END
	END

	UPDATE Opex SET Cogen = NULL WHERE SubmissionID = @SubmissionID AND DataType = 'RPT' AND Cogen IS NOT NULL

	IF EXISTS (SELECT * FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType <> 'GE' AND RptSource <> 0 AND ISNULL(PriceMBTULocal, 0) = 0)
		EXEC spLogMessage @SubmissionID, 'spCogenSales', 'I',  'Missing price for thermal energy sold to third parties.'
	IF EXISTS (SELECT * FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND RptMWH <> 0 AND ISNULL(PriceLocal, 0) = 0)
		EXEC spLogMessage @SubmissionID, 'spCogenSales', 'I',  'Missing price for electricity sold to third parties.'
	FETCH NEXT FROM cData INTO @SubmissionID, @Cogen, @ThermalSales, @ThermalSalesKLocal, @ElecSalesMBTU, @ElecSalesKLocal
END
CLOSE CData
DEALLOCATE cData
