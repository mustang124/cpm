﻿



CREATE    PROC [dbo].[spReportFactorDetailsOffsites] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency = @Currency, UOM = @UOM,
f.EDC/1000 as TotEDC, f.UEDC/1000 as TotUEDC, f.UtilPcnt AS RefUtilPcnt, TotRSEDC/1000 as kEDC, TotRSUEDC/1000 as kUEDC, SensHeatStdEnergy,AspStdEnergy, 
OffsitesStdEnergy,TotStdEnergy,EstGain/s.NumDays as EstGain, 
TotRsMaintEffDiv/1000 as TotRsMaintEffDiv, f.MaintEffDiv/1000 as MaintEffDiv
,TotRsPersEffDiv/1000 as TotRsPersEffDiv, TotRsMaintPersEffDiv/1000 as TotRsMaintPersEffDiv,TotRsNonMaintPersEffDiv/1000 as TotRsNonMaintPersEffDiv
, f.MaintPersEffDiv/1000 as MaintPersEffDiv, f.NonMaintPersEffDiv/1000 as NonMaintPersEffDiv, f.PersEffDiv/1000 as PersEffDiv
,TotRSNEOpexEffDiv/1000 as TotRSNEOpexEffDiv,  f.NEOpexEffDiv/1000 as NEOpexEffDiv, 
TnkStdCap , TnkStdEDC/1000 AS TnkStdEDC,((PurElecUEDC+PurStmUEDC)/1000) AS PurchasedUtilityUEDC 
, ProcessEDC = pt.EDC/1000, ProcessUEDC = pt.UEDC/1000, ProcessUtilPcnt = pt.UtilPcnt
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
LEFT JOIN FactorProcessCalc pt ON pt.SubmissionID = f.SubmissionID AND pt.FactorSet = f.FactorSet AND pt.ProcessID = 'OperProc'
WHERE f.FactorSet=@FactorSet 
AND s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth)
ORDER BY s.PeriodStart DESC



