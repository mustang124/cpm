﻿CREATE           PROCEDURE [dbo].[CalcCEI2008](@SubmissionID int, @DEBUG bit = 0)
AS

DELETE FROM CEI2008 WHERE SubmissionID = @SubmissionID

DECLARE @CO2_C_Ratio float, @AvgPurEnergyFraction real
SELECT @CO2_C_Ratio = 44.01/12.01, @AvgPurEnergyFraction = 0.075
SELECT @CO2_C_Ratio = 44.0/12.0

DECLARE @StudyYear StudyYear, @NumDays real, @RegionalEEF real, @RefineryID varchar(6), @PeriodStart smalldatetime
SELECT @StudyYear = PeriodYear, @NumDays = NumDays, @RefineryID = RefineryID, @PeriodStart = PeriodStart
FROM Submissions WHERE SubmissionID = @SubmissionID

SELECT @RegionalEEF = c.EEF 
FROM dbo.CountryFactors c INNER JOIN TSort ON c.Country = TSort.Country
WHERE TSort.RefineryID = @RefineryID

DECLARE @DefaultCEF_NG real, @DefaultCEF_FG real, @DefaultCEF_FG_PRO real, @DefaultCEF_COK real, @DefaultCEF_CokerLBG real, @DefaultCEF_LBG real,
	@DefaultCEF_Resid real, @DefaultCEF_CalcineCokeCons real
SELECT @DefaultCEF_NG = CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType = 'NG'
SELECT @DefaultCEF_FG = CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType = 'FG'
SELECT @DefaultCEF_FG_PRO = 0.0172
SELECT @DefaultCEF_LBG = CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType = 'LBG'
SELECT @DefaultCEF_COK = CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType = 'COK'
SELECT @DefaultCEF_CokerLBG = CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType = 'FXG'
SELECT @DefaultCEF_Resid = CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType = 'R20'
SELECT @DefaultCEF_CalcineCokeCons = CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType = 'CaC'

DECLARE @IEC_FXCoke_MBTUPerBbl real, @IEC_FCCoke_MBTUPerBbl real, @IEC_FXLBG_MBTUPerBbl real, @IEC_FCLBG_MBTUPerBbl real
SELECT @IEC_FXCoke_MBTUPerBbl = 0.280, @IEC_FCCoke_MBTUPerBbl = 0.350  -- MBTUs of coke consumed in each process for each barrel of feed
SELECT @IEC_FXLBG_MBTUPerBbl = 0.950, @IEC_FCLBG_MBTUPerBbl = 0.120  -- MBTUs of Low BTU Gas produced for each barrel of feed for each process

DECLARE @InputMT float, @InputBbl float, @StdFlareLoss real, @FlareLoss real
SELECT @InputMT = NetInputBbl*0.159*(SELECT AvgDensity/1000 FROM CrudeTot WHERE CrudeTot.SubmissionID = MaterialTot.SubmissionID),
	@InputBbl = NetInputbbl
FROM MaterialTot WHERE SubmissionID = @SubmissionID
SELECT @StdFlareLoss = 0.0017

-- CO2 Sales
DECLARE @CO2SalesMT float
SELECT @CO2SalesMT = SUM(ISNULL(MT, Bbl/7.7)) FROM Yield WHERE SubmissionID = @SubmissionID AND MaterialID IN ('M126','M237') AND Category IN ('MPROD','NLUBE')
SELECT @CO2SalesMT = ISNULL(@CO2SalesMT,0)


-- Get utilized capacities for processes that have intrinsic energy consumption or production
DECLARE @UtilCapCOKFX float, @UtilCapCOKFC float, @CALCINUtilCap float, @ASP_UtilCap float
DECLARE @H2PURE_UtilCap float, @PSA_UtilCap float, @U70_UtilCap float, @U71_UtilCap float, @U72_UtilCap float, @U73_UtilCap float, @HYGPOX_UtilCap float
SELECT 	@UtilCapCOKFX = SUM(Utilcap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'COK' AND ProcessType = 'FX'
SELECT 	@UtilCapCOKFC = SUM(Utilcap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'COK' AND ProcessType = 'FC'
SELECT 	@CALCINUtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'CALCIN'
SELECT 	@H2PURE_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'H2PURE'
SELECT 	@PSA_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'H2PURE' AND ProcessType = 'PSA'
SELECT 	@U70_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'U70'
SELECT 	@U71_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'U71'
SELECT 	@U72_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'U72'
SELECT 	@U73_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'U73'
SELECT 	@HYGPOX_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'HYG' AND ProcessType = 'POX'
SELECT 	@ASP_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'ASP'
-- Make sure variables are 0 instead of NULL
SELECT	@UtilCapCOKFX = ISNULL(@UtilCapCOKFX, 0), @UtilCapCOKFC = ISNULL(@UtilCapCOKFC, 0), @CALCINUtilCap = ISNULL(@CALCINUtilCap, 0),
	@H2PURE_UtilCap = ISNULL(@H2PURE_UtilCap, 0), @U70_UtilCap = ISNULL(@U70_UtilCap, 0), @U71_UtilCap = ISNULL(@U71_UtilCap, 0), 
	@U72_UtilCap = ISNULL(@U72_UtilCap, 0), @U73_UtilCap = ISNULL(@U73_UtilCap, 0), @HYGPOX_UtilCap = ISNULL(@HYGPOX_UtilCap, 0),
	@ASP_UtilCap = ISNULL(@ASP_UtilCap, 0)

-- Get Feed Hydrogen and Hydrogen Loss on PSA Hydrogen Purification Units 
DECLARE @PSA_FeedH2 real, @PSA_H2Loss real
SELECT @PSA_FeedH2 = SUM(t2.FeedH2*c.UtilCap)/SUM(c.UtilCap)/100
	, @PSA_H2Loss = SUM(t2.H2Loss*c.UtilCap)/SUM(c.UtilCap)/100
FROM Config c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID 
INNER JOIN StudyT2_H2PURE t2 ON t2.RefineryID = s.RefineryID AND t2.UnitID = c.UnitID AND (s.PeriodStart >= t2.EffDate AND s.PeriodStart < t2.EffUntil)
WHERE c.SubmissionID = @SubmissionID AND c.UtilCap > 0 AND c.ProcessID = 'H2PURE' AND c.ProcessType = 'PSA'


-- Load all the carbon emission factors (CEF) for gases from Energy. These are only used in 2008 and later methodologies.
DECLARE @ClientCEF_PUR_NG real, @ClientCEF_PUR_FG real, @ClientCEF_PUR_LBG real, @ClientCEF_PUR_HPF real,
		@ClientCEF_PRO_FG real, @ClientCEF_PRO_FCG real, @ClientCEF_PRO_FXG real, @ClientCEF_PRO_PSG real, @ClientCEF_PRO_PXG real, @ClientCEF_PRO_LBG real,
		@ClientCEF_TXI_NG real, @ClientCEF_TXI_FG real, @ClientCEF_TXI_LBG real, 
		@ClientCEF_TXO_NG real, @ClientCEF_TXO_FG real, @ClientCEF_TXO_LBG real, @ClientCEF_SOL_NG real, @ClientCEF_SOL_FG real, @ClientCEF_SOL_LBG real
IF @StudyYear >= 2008
BEGIN
	SELECT @ClientCEF_PUR_NG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType = 'NG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PUR_FG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType IN ('FG','FG_') AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PUR_LBG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType = 'LBG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PUR_HPF = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType IN ('HPF','NGH') AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PRO_FG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND EnergyType IN ('FG','FG_') AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PRO_FCG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND EnergyType = 'FXG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PRO_FXG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND EnergyType = 'FXG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PRO_PSG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND EnergyType = 'PSG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PRO_PXG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND EnergyType = 'PXG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_PRO_LBG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND EnergyType = 'LBG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_TXI_NG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND SourceMBTU > 0 AND EnergyType = 'NG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_TXI_FG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND SourceMBTU > 0 AND EnergyType IN ('FG','FG_') AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_TXI_LBG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND SourceMBTU > 0 AND EnergyType = 'LBG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_TXO_NG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND UsageMBTU > 0 AND EnergyType = 'NG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_TXO_FG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND UsageMBTU > 0 AND EnergyType IN ('FG','FG_') AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_TXO_LBG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND UsageMBTU > 0 AND EnergyType = 'LBG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_SOL_NG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType = 'NG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_SOL_FG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType IN ('FG','FG_') AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
	SELECT @ClientCEF_SOL_LBG = SUM(CEF*MBTU)/SUM(MBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType = 'LBG' AND Total BETWEEN 95 AND 105 AND MBTU > 0 AND CEF IS NOT NULL
END
/* Get the study values if composition not reported in Profile */
SELECT @ClientCEF_PUR_NG = ISNULL(@ClientCEF_PUR_NG, ClientCEF_PUR_NG),
		@ClientCEF_PUR_FG = ISNULL(@ClientCEF_PUR_FG, ClientCEF_PUR_FG),
		@ClientCEF_PUR_LBG = ISNULL(@ClientCEF_PUR_LBG, ClientCEF_PUR_LBG),
		@ClientCEF_PUR_HPF = ISNULL(@ClientCEF_PUR_HPF, ClientCEF_PUR_HPF),
		@ClientCEF_PRO_FG = ISNULL(@ClientCEF_PRO_FG, ClientCEF_PRO_FG),
		@ClientCEF_PRO_FCG = ISNULL(@ClientCEF_PRO_FCG, ClientCEF_PRO_FCG),
		@ClientCEF_PRO_FXG = ISNULL(@ClientCEF_PRO_FXG, ClientCEF_PRO_FXG),
		@ClientCEF_PRO_PSG = ISNULL(@ClientCEF_PRO_PSG, ClientCEF_PRO_PSG),
		@ClientCEF_PRO_PXG = ISNULL(@ClientCEF_PRO_PXG, ClientCEF_PRO_PXG),
		@ClientCEF_PRO_LBG = ISNULL(@ClientCEF_PRO_LBG, ClientCEF_PRO_LBG),
		@ClientCEF_TXI_NG = ISNULL(@ClientCEF_TXI_NG, ClientCEF_TXI_NG),
		@ClientCEF_TXI_FG = ISNULL(@ClientCEF_TXI_FG, ClientCEF_TXI_FG),
		@ClientCEF_TXI_LBG = ISNULL(@ClientCEF_TXI_LBG, ClientCEF_TXI_LBG),
		@ClientCEF_TXO_NG = ISNULL(@ClientCEF_TXO_NG, ClientCEF_TXO_NG),
		@ClientCEF_TXO_FG = ISNULL(@ClientCEF_TXO_FG, ClientCEF_TXO_FG),
		@ClientCEF_TXO_LBG = ISNULL(@ClientCEF_TXO_LBG, ClientCEF_TXO_LBG),
		@ClientCEF_SOL_NG = ISNULL(@ClientCEF_SOL_NG, ClientCEF_SOL_NG),
		@ClientCEF_SOL_FG = ISNULL(@ClientCEF_SOL_FG, ClientCEF_SOL_FG),
		@ClientCEF_SOL_LBG = ISNULL(@ClientCEF_SOL_LBG, ClientCEF_SOL_LBG),
		@FlareLoss = CASE WHEN FlareLoss > 0 THEN FlareLoss ELSE NULL END
FROM RefineryFactors WHERE RefineryID = @RefineryID AND EffDate <= @PeriodStart AND EffUntil > @PeriodStart


-- Total Energy Consumed by Refinery
DECLARE @TotEnergyConsMBTU float
SELECT @TotEnergyConsMBTU = TotEnergyConsMBTU FROM EnergyTot WHERE SubmissionID = @SubmissionID

DECLARE @AE_PurElec float, @AE_ProdElecAdj float
-- MBTU adjustments for electricity purchased and produced
SELECT 	@AE_PurElec = SUM(CASE WHEN TransType = 'CPU' THEN MBTU ELSE 0 END), 
	@AE_ProdElecAdj = SUM(CASE WHEN TransType = 'CPR' THEN MBTU ELSE 0 END)
FROM EnergyCons WHERE SubmissionID = @SubmissionID AND EnergyType = 'PT'
IF @AE_PurElec IS NULL
	SET @AE_PurElec = 0
IF @AE_ProdElecAdj IS NULL
	SET @AE_ProdElecAdj = 0

-- MWH of electricity purchased, transferred from affiliates, transferred to affiliates, and sold
DECLARE @ElecMWH_Pur float, @ElecMWH_TransIn float, @ElecMWH_TransOut float, @ElecMWH_Sold float
SELECT 	@ElecMWH_Pur = SUM(CASE WHEN TransType = 'PUR' THEN SourceMWH ELSE 0 END)
	, @ElecMWH_TransIn = SUM(CASE WHEN TransType = 'DST' THEN SourceMWH ELSE 0 END)
	, @ElecMWH_TransOut = SUM(CASE WHEN TransType = 'DST' THEN UsageMWH ELSE 0 END)
	, @ElecMWH_Sold = SUM(CASE WHEN TransType = 'SOL' THEN UsageMWH ELSE 0 END)
FROM Electric WHERE SubmissionID = @SubmissionID
SELECT 	@ElecMWH_Pur = ISNULL(@ElecMWH_Pur, 0), @ElecMWH_TransIn = ISNULL(@ElecMWH_TransIn, 0), 
	@ElecMWH_TransOut = ISNULL(@ElecMWH_TransOut, 0), @ElecMWH_Sold = ISNULL(@ElecMWH_Sold, 0)

-- MBTU and Carbon Emissions for fungible fuels (Fuels that have constant carbon factors regardless of refinery or production method)
DECLARE @tblEnergy TABLE
(	EnergyType varchar(3) NOT NULL,
	Pur float NULL,
	Prod float NULL,
	TransIn float NULL,
	TransOut float NULL,
	Sold float NULL,
	PurCons float NULL,
	ProdCons float NULL,
	NetSources float NULL,
	TotCons float NULL,
	TotSources float NULL,
	CEF float NULL
	/*,HeatContent real NULL*/
)
	-- Get the MBTUs purchased, produced, transferred in and out, sold, and consumed for all fuel types.
	-- We will use this table to get all of the values for thermal energy pulled from table 16.
INSERT @tblEnergy (EnergyType, Pur, Prod, TransIn, TransOut, Sold, PurCons, ProdCons, CEF /*, HeatContent*/)
SELECT EnergyType, Pur = SUM(CASE WHEN TransType = 'PUR' THEN SourceMBTU ELSE 0 END)
, Prod = SUM(CASE WHEN TransType = 'PRO' THEN SourceMBTU ELSE 0 END)
, TransIn = SUM(CASE WHEN TransType = 'DST' THEN SourceMBTU ELSE 0 END)
, TransOut = SUM(CASE WHEN TransType = 'DST' THEN UsageMBTU ELSE 0 END)
, Sold = SUM(CASE WHEN TransType = 'SOL' THEN UsageMBTU ELSE 0 END)
, PurCons = (SELECT SUM(MBTU) FROM EnergyCons WHERE EnergyCons.SubmissionID = @SubmissionID AND EnergyCons.EnergyType = Energy.EnergyType AND TransType = 'CPU')
, ProdCons = (SELECT SUM(MBTU) FROM EnergyCons WHERE EnergyCons.SubmissionID = @SubmissionID AND EnergyCons.EnergyType = Energy.EnergyType AND TransType = 'CPR')
, CEF = (SELECT CO_MTperMBTU FROM EnergyType_LU WHERE EnergyType_LU.EnergyType = Energy.EnergyType)
/*, HeatContent = GlobalDB.dbo.WtAvgNZ(HeatContent, Usage)*/
FROM Energy WHERE SubmissionID = @SubmissionID
GROUP BY EnergyType

	-- Make sure none of the fields are null
UPDATE @tblEnergy
SET Pur = ISNULL(Pur, 0), Prod = ISNULL(Prod, 0), TransIn = ISNULL(TransIn, 0), TransOut = ISNULL(TransOut, 0), Sold = ISNULL(Sold, 0), PurCons = ISNULL(PurCons, 0), ProdCons = ISNULL(ProdCons, 0), CEF = ISNULL(CEF, 0)
	
	-- NetSources nets out like for like transfers or sales. TotCons is the total consumed for use by the refinery.
UPDATE @tblEnergy
SET TotSources = Pur + Prod + TransIn, NetSources = Pur + Prod + TransIn - TransOut - Sold, TotCons = PurCons + ProdCons
/* --------- Load variables for the different energy types to be used in calculations below ---------------- */

	-- Actual MBTUs of Steam purchased, transferred in and out, and sold
DECLARE @STM_Pur float, @STM_TransIn float, @STM_TransOut float, @STM_Sales float, @STM_Cons float
SELECT @STM_Pur = Pur, @STM_TransIn = TransIn, @STM_TransOut = TransOut, @STM_Sales = Sold, @STM_Cons = TotCons
FROM @tblEnergy WHERE EnergyType IN ('STM','LLH')

	-- Actual MBTUs of Thermal Energy used to generate electricity for affiliates and sold to others
DECLARE @GE_TransOut float, @GE_Sales float
SELECT @GE_TransOut = TransOut, @GE_Sales = Sold
FROM @tblEnergy WHERE EnergyType = 'GE'

	-- Fungible_NetSources includes all fuel burned to produce steam or electricity that were exported from the refinery.
	-- Fungible_Cons includes only the fuel that was used for the purpose of refining.
	-- Fungible_Out is the fungible fuel used to produce steam or electricity that was exported. (Burned - Consumed)
DECLARE @Fungible_NetSources float, @CE_Fungible_NetSources float, @Fungible_Cons float, @CE_Fungible_Cons float, @Fungible_Out float, @CE_Fungible_Out float
SELECT 	@Fungible_NetSources = SUM(NetSources), @CE_Fungible_NetSources = SUM(NetSources*CEF),
		@Fungible_Cons = SUM(TotCons), @CE_Fungible_Cons = SUM(TotCons*CEF), 
		@Fungible_Out = SUM(NetSources - TotCons), @CE_Fungible_Out = SUM((NetSources - TotCons)*CEF)
FROM @tblEnergy WHERE EnergyType IN ('C2', 'C3', 'C4', 'NAP', 'DST', 'R03', 'R10', 'R20', 'R30', 'RG3', 'RXV', 'COK') -- List of fungible fuels
SELECT @Fungible_NetSources = ISNULL(@Fungible_NetSources, 0), @CE_Fungible_NetSources = ISNULL(@CE_Fungible_NetSources, 0),
	@Fungible_Cons = ISNULL(@Fungible_Cons, 0), @CE_Fungible_Cons = ISNULL(@CE_Fungible_Cons, 0),
	@Fungible_Out = ISNULL(@Fungible_Out, 0), @CE_Fungible_Out = ISNULL(@CE_Fungible_Out, 0)

	-- Get MBTUs and emissions for Fuel Gas
DECLARE @FG_PUR float, @FG_PRO float, @FG_TransIn float, @FG_TransOut float, @FG_DST float, @FG_SOL float, @FG_HeatContent real,
	@CE_FG_PUR float, @CE_FG_PRO float, @CE_FG_DST float, @CE_FG_Sales float,
	@FG_NetSources float, @CE_FG_NetSources float, @FG_Cons float, @CE_FG_Cons float, @CEF_FG_PRO real
SELECT 	@FG_NetSources = NetSources, @FG_Cons = TotCons, @FG_PUR = Pur, @FG_PRO = Prod,
	@FG_TransIn = TransIn, @FG_TransOut = TransOut, @FG_DST = TransIn - TransOut, @FG_SOL = Sold,
	@CE_FG_PUR = Pur*CEF, 
	@CE_FG_NetSources = NetSources*CEF, @CE_FG_Cons = TotCons*CEF
	/*,	@FG_HeatContent = HeatContent*/
FROM @tblEnergy WHERE EnergyType = 'FG'

	-- Get MBTUs and emissions for natural gas
DECLARE @NG_PUR float, @NG_PRO float, @NG_TransIn float, @NG_TransOut float, @NG_DST float, @NG_SOL float, @NG_HeatContent real,
	@CE_NG_PUR float, @CE_NG_PRO float, @CE_DSTNG float, @CE_SOLNG float,
	@NG_NetSources float, @CE_NG_NetSources float, @NG_Cons float, @CE_NG_Cons float, @CEF_NG_PRO real, @CEF_NG_PUR real
SELECT 	@NG_NetSources = NetSources, @NG_Cons = TotCons, @NG_PUR = Pur, @NG_PRO = Prod, 
	@NG_TransIn = TransIn, @NG_TransOut = TransOut, @NG_DST = TransIn - TransOut, @NG_SOL = Sold,
	@CE_NG_PUR = Pur*CEF, @CE_NG_PRO = Prod*CEF,
	@CE_NG_NetSources = NetSources*CEF, @CE_NG_Cons = TotCons*CEF
	/*,	@NG_HeatContent = HeatContent*/
FROM @tblEnergy WHERE EnergyType = 'NG'

	-- For 2006 methodology, Natural Gas and Fuel Gas were reported together 
	-- so we will combine them and calculate the 2006 methodology as if we do not know how much is each one.
	-- The general assumption in the methodology was that purchased was pipeline NG and produced varied for each refinery.
IF @DEBUG = 1
	SELECT * FROM @tblEnergy

DECLARE @FGNG_PUR float, @FGNG_PRO float, @FGNG_TransIn float, @FGNG_TransOut float, @FGNG_DST float, @FGNG_SOL float, 
		@FGNG_NetSources float, @FGNG_Cons float, @FGNG_HeatContent real,
		@CE_FGNG_PUR float, @CE_FGNG_PRO float, @CE_FGNG_DST float, @CE_FGNG_Sales float,
		@CE_FGNG_NetSources float, @CE_FGNG_Cons float
SELECT 	@FGNG_NetSources = SUM(NetSources), 
		@FGNG_Cons = SUM(TotCons), 
		@FGNG_PUR = SUM(Pur), 
		@FGNG_PRO = SUM(Prod),
		@FGNG_DST = SUM(TransIn - TransOut), @FGNG_TransIn = SUM(TransIn), @FGNG_TransOut = SUM(TransOut), 
		@FGNG_SOL = SUM(Sold),
		@CE_FGNG_PUR = SUM(Pur*CEF), 
		@CE_FGNG_NetSources = SUM(NetSources*CEF), 
		@CE_FGNG_Cons = SUM(TotCons*CEF)
		/*, @FGNG_HeatContent = GlobalDB.dbo.WtAvgNZ(HeatContent, ProdCons+PurCons)*/
FROM @tblEnergy WHERE EnergyType IN ('FG','NG')

	-- Low BTU Gas Consumption
DECLARE @LBG_NetSources float, @LBG_PUR float, @LBG_PRO float, @LBG_DST float, @LBG_SOL float, @LBG_Cons float, @LBG_HeatContent real
DECLARE @LBG_PRO_FC float, @LBG_PRO_FX float, @LBG_PRO_POX float, @LBG_PRO_PSA float, @LBG_PRO_Other float, @LBG_TransIn float, @LBG_TransOut float
SELECT 	@LBG_NetSources = SUM(NetSources), 
		@LBG_Cons = SUM(TotCons), 
		@LBG_PUR = SUM(Pur), 
		@LBG_PRO = SUM(Prod),
		@LBG_DST = SUM(TransIn - TransOut), @LBG_TransIn = SUM(TransIn), @LBG_TransOut = SUM(TransOut),
		@LBG_SOL = SUM(Sold)
		/*,	@LBG_HeatContent = GlobalDB.dbo.WtAvgNZ(HeatContent, ProdCons+PurCons)*/
FROM @tblEnergy WHERE EnergyType IN ('LBG','LCV','FCG','FXG','PXG','PSG')
SELECT 	@LBG_PRO_FC = Prod FROM @tblEnergy WHERE EnergyType = 'FCG'
SELECT 	@LBG_PRO_FX = Prod FROM @tblEnergy WHERE EnergyType = 'FXG'
SELECT 	@LBG_PRO_POX = Prod FROM @tblEnergy WHERE EnergyType = 'PXG'
SELECT 	@LBG_PRO_PSA = Prod FROM @tblEnergy WHERE EnergyType = 'PSG'
SELECT 	@LBG_PRO_Other = SUM(Prod) FROM @tblEnergy WHERE EnergyType IN ('LBG','LCV')

	-- Produced marketable coke reported on table 16 (MC = Marketable coke, CaC = coke consumed by Calciner [new in 2008])
DECLARE @AE_MC float, @AE_CaC float, @AE_NMC float, @AE_COC float
SELECT @AE_MC = Prod FROM @tblEnergy WHERE EnergyType = 'MC'
SELECT @AE_CaC = Prod FROM @tblEnergy WHERE EnergyType = 'CaC'
SELECT @AE_NMC = Prod FROM @tblEnergy WHERE EnergyType = 'NMC'
SELECT @AE_COC = Prod FROM @tblEnergy WHERE EnergyType = 'COC' 

	-- Make sure none of the variables from the energy table are NULL
SELECT @FG_NetSources = ISNULL(@FG_NetSources, 0), @FG_PUR = ISNULL(@FG_PUR, 0), @FG_PRO = ISNULL(@FG_PRO, 0), @FG_DST = ISNULL(@FG_DST, 0), @FG_SOL = ISNULL(@FG_SOL, 0), @FG_Cons = ISNULL(@FG_Cons, 0),
	@FG_TransIn = ISNULL(@FG_TransIn, 0), @FG_TransOut = ISNULL(@FG_TransOut, 0),
	@CE_FG_PUR = ISNULL(@CE_FG_PUR, 0), @CE_FG_DST = ISNULL(@CE_FG_DST, 0), @CE_FG_Sales = ISNULL(@CE_FG_Sales, 0), @CE_FG_NetSources = ISNULL(@CE_FG_NetSources, 0), @CE_FG_Cons = ISNULL(@CE_FG_Cons, 0),
	@NG_NetSources = ISNULL(@NG_NetSources, 0), @NG_PUR = ISNULL(@NG_PUR, 0), @NG_PRO = ISNULL(@NG_PRO, 0), @NG_DST = ISNULL(@NG_DST, 0), @NG_SOL = ISNULL(@NG_SOL, 0), @NG_Cons = ISNULL(@NG_Cons, 0), 
	@CE_NG_PUR = ISNULL(@CE_NG_PUR, 0), @CE_NG_PRO = ISNULL(@CE_NG_PRO, 0), @CE_DSTNG = ISNULL(@CE_DSTNG, 0), @CE_SOLNG = ISNULL(@CE_SOLNG, 0), @CE_NG_NetSources = ISNULL(@CE_NG_NetSources, 0), @CE_NG_Cons  = ISNULL(@CE_NG_Cons, 0),
	@NG_TransIn = ISNULL(@NG_TransIn, 0), @NG_TransOut = ISNULL(@NG_TransOut, 0),
	@LBG_NetSources = ISNULL(@LBG_NetSources, 0), @LBG_PUR = ISNULL(@LBG_PUR, 0), @LBG_PRO = ISNULL(@LBG_PRO, 0), 
	@LBG_DST = ISNULL(@LBG_DST, 0), @LBG_SOL = ISNULL(@LBG_SOL, 0), @LBG_Cons = ISNULL(@LBG_Cons, 0),
	@LBG_PRO_FC = ISNULL(@LBG_PRO_FC, 0), @LBG_PRO_FX = ISNULL(@LBG_PRO_FX, 0), @LBG_PRO_POX = ISNULL(@LBG_PRO_POX, 0), @LBG_PRO_PSA = ISNULL(@LBG_PRO_PSA, 0), 
	@LBG_PRO_Other = ISNULL(@LBG_PRO_Other, 0), @LBG_TransIn = ISNULL(@LBG_TransIn, 0), @LBG_TransOut = ISNULL(@LBG_TransOut, 0),
	@AE_MC = ISNULL(@AE_MC, 0), @AE_CaC = ISNULL(@AE_CaC, 0), @AE_NMC = ISNULL(@AE_NMC, 0), @AE_COC = ISNULL(@AE_COC, 0),
 	@STM_Pur = ISNULL(@STM_Pur, 0), @STM_TransIn = ISNULL(@STM_TransIn, 0),
	@STM_TransOut = ISNULL(@STM_TransOut, 0), @STM_Sales = ISNULL(@STM_Sales, 0), @STM_Cons = ISNULL(@STM_Cons, 0),
	@GE_TransOut = ISNULL(@GE_TransOut, 0), @GE_Sales = ISNULL(@GE_Sales, 0),
	@FGNG_PUR = ISNULL(@FGNG_PUR, 0), @FGNG_PRO = ISNULL(@FGNG_PRO, 0), @FGNG_DST = ISNULL(@FGNG_DST, 0), @FGNG_SOL = ISNULL(@FGNG_SOL, 0), 
	@FGNG_NetSources = ISNULL(@FGNG_NetSources, 0), @FGNG_Cons = ISNULL(@FGNG_Cons, 0), @FGNG_TransIn = ISNULL(@FGNG_TransIn, 0), @FGNG_TransOut = ISNULL(@FGNG_TransOut, 0), 
	@CE_FGNG_PUR = ISNULL(@CE_FGNG_PUR, 0), @CE_FGNG_NetSources = ISNULL(@CE_FGNG_NetSources, 0), @CE_FGNG_Cons = ISNULL(@CE_FGNG_Cons, 0)

-- If the net amount of fuel gas or natural gas distributed or sold is neglible, then just ignore it as rounding error.
IF ABS(@FG_DST) < 5 SET @FG_DST = 0
IF ABS(@FG_SOL) < 5	SET @FG_SOL = 0
IF ABS(@NG_DST) < 5 SET @NG_DST = 0
IF ABS(@NG_SOL) < 5	SET @NG_SOL = 0
IF ABS(@FGNG_DST) < 5 SET @FGNG_DST = 0
IF ABS(@FGNG_SOL) < 5	SET @FGNG_SOL = 0

-- Total MBTU leaving refinery as steam or electricity (produced)
DECLARE @TotStmGEOut float
SELECT @TotStmGEOut = @STM_TransOut + @STM_Sales + @GE_TransOut + @GE_Sales

-- Determine how much of net transfers and sales of Fuel Gas/Natural Gas come from purchases and produced fuel gas
DECLARE @NetFGNGTransIn float, @PurFGNGTransOut float, @PurFGNGSold float, @ProdFGNGTransOut float, @ProdFGNGSold float, 
		@PurFGNGLeft float, @ProdFGNGLeft float
SELECT @NetFGNGTransIn = 0, @PurFGNGTransOut = 0, @PurFGNGSold = 0, @ProdFGNGTransOut = 0, @ProdFGNGSold = 0, 
		@PurFGNGLeft = @FGNG_PUR, @ProdFGNGLeft = @FGNG_PRO
IF @FGNG_DST <> 0  -- Net transfer of FG in our out
BEGIN
	IF @FGNG_DST>0 -- more fuel gas came transferred in than transferred out, so use standard
		SELECT @NetFGNGTransIn = @FGNG_DST, @PurFGNGLeft = @PurFGNGLeft + @FGNG_DST
	ELSE BEGIN  -- More fuel gas was distributed to affiliates, so determine how much was from purchases and produced (FG_DST < 0)
		IF -@FGNG_DST <= @FGNG_PUR  -- All from purchases
			SELECT @PurFGNGTransOut = -@FGNG_DST, @PurFGNGLeft = @PurFGNGLeft + @FGNG_DST
		ELSE BEGIN  -- All of purchases and some produced
			SELECT @PurFGNGTransOut = @PurFGNGLeft, @ProdFGNGTransOut = -@FGNG_DST - @PurFGNGLeft
			SELECT @PurFGNGLeft = 0, @ProdFGNGLeft = @ProdFGNGLeft - @ProdFGNGTransOut
		END
	END
END
IF @FGNG_SOL > 0 -- Sales of Fuel Gas were reported
BEGIN
	IF @FGNG_SOL<=@PurFGNGLeft  -- more fuel gas was purchased than sold and distributed so it all comes from purchases
		SELECT @PurFGNGSold = @FGNG_SOL, @PurFGNGLeft = @PurFGNGLeft - @FGNG_SOL
	ELSE BEGIN-- some sales come from produced fuel gas
		SELECT @PurFGNGSold = @PurFGNGLeft, @ProdFGNGSold = @FGNG_SOL - @PurFGNGLeft
		SELECT @PurFGNGLeft = 0, @ProdFGNGLeft = @ProdFGNGLeft - @ProdFGNGSold
	END
END


/* ------------------------------------------------*/
/* ----- Begin calculations  ----------------------*/
/* ------------------------------------------------*/


-- CEF varies for produced Fuel Gas depending on its gas composition.
-- If the composition was not reported then calculate it based on default heat content to 1037.333
SELECT @CEF_FG_PRO = @ClientCEF_PRO_FG
IF @CEF_FG_PRO IS NULL 
	SELECT @CEF_FG_PRO = 0.0202-3.112/CASE WHEN @FGNG_HeatContent > 0 THEN @FGNG_HeatContent ELSE 1037.333 END

-- CE factor for purchased fuel gas
DECLARE @CEF_PURFGNG real
SELECT @CEF_PURFGNG = @DefaultCEF_NG

DECLARE @SE_FXCoke float, @SCE_FXCoke float, @SE_FCCoke float, @SCE_FCCoke float
	-- Calculate estimated coke yield by fluid and flexi cokers
SELECT 	@SE_FXCoke = @UtilCapCOKFX*@NumDays*@IEC_FXCoke_MBTUPerBbl, 
		@SE_FCCoke = @UtilCapCOKFC*@NumDays*@IEC_FCCoke_MBTUPerBbl
	-- Calculate standard carbon emissions from estimated coke
SELECT 	@SCE_FXCoke = @SE_FXCoke*@DefaultCEF_COK*@CO2_C_Ratio, 
		@SCE_FCCoke = @SE_FCCoke*@DefaultCEF_COK*@CO2_C_Ratio 

	-- Calculate actual and standard emissions for coke consumed in calciner.
	-- Prior to 2008, this coke was reported as Marketable Coke which could include other sources as well,
	-- so we calculate the max that could have been produced by the calciner and limit the energy that we charge to calcin coke consumption.
	-- Beginning in 2008, Coke consumed by calcining is a separate line and should be used.

-- Estimate Calciner coke consumption
/* --- Not necessary since we have Calciner Coke consumption reported on table 16 ----
DECLARE @CALCIN_CalcMaxMBTU float, @CALCIN_CalcMBTU float
SELECT @CALCIN_CalcMaxMBTU = dbo.UnitsConv(@CALCINUtilCap, 'ST', 'LB')*@NumDays*(0.12/0.82)/8.45/42.0*6.43  --12% volatiles/82% yield, additional assumed to be pet coke
SELECT @CALCIN_CalcMBTU = CASE WHEN (@AE_MC + @AE_CaC) < @CALCIN_CalcMaxMBTU THEN (@AE_MC + @AE_CaC) ELSE @CALCIN_CalcMaxMBTU END
*/

-- Actual Emissions from Low BTU Gas reported on Table 16 using constant CEF
DECLARE @CE_LBG float, @EstCEF_LBG real, @CE_LBGCons float
SELECT 	@CE_LBG = @LBG_NetSources*@DefaultCEF_LBG, -- Based on NetSources which includes fuel that may have been converted to steam or electricity and sold
		@CE_LBGCons = @LBG_Cons*@DefaultCEF_LBG  -- Consumed purchased and produced Low BTU Gas for refinery purposes

DECLARE @PSA_FeedCH4 real, @PSA_FeedCO2 real, @PSA_Tailgas_BtuPerSCF real
DECLARE @PSA_TailGasMBTU float, @PSA_TailGasMBTU_H2CH4 float, @SE_PSATailGasLBG float, @CE_PSATailGasLBG float
DECLARE	@SE_U73LBG float, @CE_U73LBG float, @SE_U71LBG float, @CE_U71LBG float 
DECLARE	@SE_U72POX float, @SE_U73POX float, @SE_HYGPOX float, 
	@SCE_U72POX float, @SCE_U73POX float, @SCE_HYGPOX float,
	@SCE_H2_U72 float
DECLARE @OriginalLBG_MBTU float, @OriginalLBG_CE float, @OriginalLBG_CEFactor float

SELECT @PSA_FeedCH4 = ISNULL(0.14*(1-@PSA_FeedH2), 0)
SELECT @PSA_FeedCO2 = 1-ISNULL(@PSA_FeedH2,0)-@PSA_FeedCH4

SELECT 	@SE_U73POX = @U73_UtilCap*@NumDays*0.369, 
		@SE_U72POX = @U72_UtilCap*@NumDays*60/1000,
		@SE_HYGPOX = @HYGPOX_UtilCap*@NumDays*60/1000,
		@SE_U73LBG = @U73_UtilCap*@NumDays*0.29,
		@SE_U71LBG = @U71_UtilCap*@NumDays*0.027
SELECT 	@SCE_U73POX = @SE_U73POX*@DefaultCEF_Resid*@CO2_C_Ratio, 
		@SCE_U72POX = @SE_U72POX*@DefaultCEF_COK*@CO2_C_Ratio, 
		@SCE_HYGPOX = @SE_HYGPOX*@DefaultCEF_COK*@CO2_C_Ratio,
		@CE_U73LBG = @SE_U73LBG * 0.029*@CO2_C_Ratio,
		@CE_U71LBG = @SE_U71LBG * 0.0289*@CO2_C_Ratio,
		@SCE_H2_U72 = (@U72_UtilCap*@NumDays*0.369*0.0232 - @SE_U72POX*0.0293)*@CO2_C_Ratio
DECLARE @SE_FXLBG float, @SCE_FXLBG float, @SE_FCLBG float, @SCE_FCLBG float
SELECT 	@SE_FXLBG = @UtilCapCOKFX*@NumDays*0.95,
	@SE_FCLBG = @UtilCapCOKFC*@NumDays*0.12
SELECT 	@SCE_FXLBG = @SE_FXLBG*0.0344*@CO2_C_Ratio,
	@SCE_FCLBG = @SE_FCLBG*0.0344*@CO2_C_Ratio

SELECT @SE_FCLBG = ISNULL(@SE_FCLBG, 0), @SE_FXLBG = ISNULL(@SE_FXLBG, 0), @SE_U71LBG = ISNULL(@SE_U71LBG, 0), @SE_U73LBG = ISNULL(@SE_U73LBG, 0)
SELECT @PSA_Tailgas_BtuPerSCF = @PSA_H2Loss*@PSA_FeedH2/(@PSA_H2Loss*@PSA_FeedH2+@PSA_FeedCH4+@PSA_FeedCO2)*275+@PSA_FeedCH4/(@PSA_H2Loss*@PSA_FeedH2+@PSA_FeedCH4+@PSA_FeedCO2)*894
SELECT @PSA_TailGasMBTU = @LBG_NetSources-@SE_FCLBG-@SE_FXLBG-@SE_U71LBG-@SE_U73LBG
IF @PSA_TailGasMBTU < 0  
	SET @PSA_TailGasMBTU = 0
SELECT @PSA_TailGasMBTU_H2CH4 = ISNULL(@PSA_UtilCap*(@PSA_FeedH2*@PSA_H2Loss+@PSA_FeedCH4+@PSA_FeedCO2)*@PSA_Tailgas_BtuPerSCF*@NumDays/1000, 0)

IF @DEBUG = 1
BEGIN
	SELECT LBG_HeatContent = @LBG_HeatContent, PSA_UtilCap = @PSA_UtilCap, PSA_FeedH2 = @PSA_FeedH2, PSA_H2Loss = @PSA_H2Loss, PSA_FeedCH4 = @PSA_FeedCH4, PSA_FeedCO2 = @PSA_FeedCO2, PSA_Tailgas_BtuPerSCF = @PSA_Tailgas_BtuPerSCF, PSA_TailGasMBTU = @PSA_TailGasMBTU, PSA_TailGasMBTU_H2CH4 = @PSA_TailGasMBTU_H2CH4
	SELECT U73UtilCap = @U73_UtilCap, SE_U73LBG = @SE_U73LBG, CE_U73LBG = @CE_U73LBG/@CO2_C_Ratio
	SELECT U71UtilCap = @U71_UtilCap, SE_U71LBG = @SE_U71LBG, CE_U71LBG = @CE_U71LBG/@CO2_C_Ratio
END

SELECT @SE_PSATailGasLBG = CASE WHEN @PSA_TailGasMBTU_H2CH4 < @PSA_TailGasMBTU THEN @PSA_TailGasMBTU_H2CH4 ELSE @PSA_TailGasMBTU END
IF @PSA_TailGasMBTU_H2CH4 > 0
	SELECT @CE_PSATailGasLBG = @PSA_UtilCap*(CASE WHEN @PSA_FeedCH4+@PSA_FeedCO2 > 0.1 THEN 0.1 ELSE @PSA_FeedCH4+@PSA_FeedCO2 END)/0.3794*12/2204.6*@NumDays*@SE_PSATailGasLBG/@PSA_TailGasMBTU_H2CH4*@CO2_C_Ratio
ELSE
	SELECT @CE_PSATailGasLBG = 0
SELECT 	@OriginalLBG_MBTU = ISNULL(@SE_U73LBG,0)+ISNULL(@SE_PSATailGasLBG,0)+ISNULL(@SE_FCLBG,0)+ISNULL(@SE_FXLBG,0),
	@OriginalLBG_CE = ISNULL(@CE_U73LBG,0)+ISNULL(@CE_PSATailGasLBG,0)+ISNULL(@SCE_FCLBG,0)+ISNULL(@SCE_FXLBG,0)
SELECT 	@OriginalLBG_CEFactor = CASE WHEN @OriginalLBG_MBTU > 0 THEN @OriginalLBG_CE/@OriginalLBG_MBTU ELSE 0 END

IF @DEBUG = 1
	SELECT SE_PSATailGasLBG = @SE_PSATailGasLBG, CE_PSATailGasLBG = @CE_PSATailGasLBG/@CO2_C_Ratio, OriginalLBG_MBTU = @OriginalLBG_MBTU, OriginalLBG_CE = @OriginalLBG_CE/@CO2_C_Ratio, OriginalLBG_CEFactor = @OriginalLBG_CEFactor/@CO2_C_Ratio

DECLARE @tblHYG TABLE (UnitID int NOT NULL, UtilCap real NOT NULL, FeedH2 real NULL, H2Loss real NULL, CO2RejectType varchar(4) NULL, FeedH2KSCFpD real NULL, H2LossKSCFpD real NULL,
			FeedC1 real NULL, FeedC2 real NULL, FeedC3 real NULL, FeedC4 real NULL, FeedCO real NULL, FeedCO2 real NULL, FeedN2 real NULL, FeedOth real NULL,
			FeedLPG real NULL, FeedNap real NULL, FeedKero real NULL, FeedDiesel real NULL, FeedHGO real NULL, FeedARC real NULL, FeedVacResid real NULL, FeedLubeExtracts real NULL, FeedAsp real NULL, FeedCoke real NULL)
INSERT @tblHYG (UnitID, UtilCap)
SELECT UnitID, UtilCap
FROM Config WHERE SubmissionID = @SubmissionID AND UtilCap > 0 AND ProcessID = 'HYG'
IF EXISTS (SELECT * FROM @tblHYG)
BEGIN
	UPDATE hyg
	SET FeedH2 = (SELECT SAValue FROM ProcessData WHERE SubmissionID = @SubmissionID AND ProcessData.UnitID = hyg.UnitID AND ProcessData.Property = 'FeedH2')
	, H2Loss = (SELECT SAValue FROM ProcessData WHERE SubmissionID = @SubmissionID AND ProcessData.UnitID = hyg.UnitID AND ProcessData.Property = 'H2Loss')
	, CO2RejectType = ISNULL((SELECT CASE SAValue WHEN 0 THEN '' WHEN 1 THEN 'CRYO' WHEN 2 THEN 'PRSM' WHEN 3 THEN 'PSA' WHEN 4 THEN 'SOLV' END FROM ProcessData WHERE SubmissionID = @SubmissionID AND ProcessData.UnitID = hyg.UnitID AND ProcessData.Property = 'CO2RejectType'), '')
	FROM @tblHYG hyg

	UPDATE hyg
	SET H2Loss = CASE WHEN hyg.H2Loss IS NULL THEN t2.H2Loss ELSE hyg.H2Loss END
	, FeedH2 = CASE WHEN ISNULL(hyg.FeedH2, 0) = 0 THEN t2.FeedH2 ELSE hyg.FeedH2 END
	, CO2RejectType = CASE WHEN ISNULL(hyg.CO2RejectType, '') = '' THEN ISNULL(t2.CO2RejectType, '') ELSE hyg.CO2RejectType END
	, FeedC1 = t2.FeedC1, FeedC2 = t2.FeedC2, FeedC3 = t2.FeedC3, FeedC4 = t2.FeedC4, FeedCO = t2.FeedCO, FeedCO2 = t2.FeedCO2, FeedN2 = t2.FeedN2, FeedOth = t2.FeedOth
	, FeedLPG = t2.FeedLPG, FeedNap = t2.FeedNap, FeedKero = t2.FeedKero, FeedDiesel = t2.FeedDiesel, FeedHGO = t2.FeedHGO, FeedARC = t2.FeedARC, FeedVacResid = t2.FeedVacResid, FeedLubeExtracts = t2.FeedLubeExtracts, FeedAsp = t2.FeedAsp, FeedCoke = t2.FeedCoke
	FROM @tblHYG hyg INNER JOIN Submissions s ON s.SubmissionID = @SubmissionID 
	INNER JOIN StudyT2_HYG t2 ON t2.RefineryID = s.RefineryID AND t2.UnitID = hyg.UnitID AND s.PeriodStart >= t2.EffDate AND s.PeriodStart < t2.EffUntil

	UPDATE @tblHYG SET H2Loss = 12.5 WHERE H2Loss = 0 AND ISNULL(CO2RejectType, '') <> 'SOLV'
	UPDATE @tblHYG SET H2Loss = 2 WHERE H2Loss IS NULL AND CO2RejectType = 'SOLV'
	UPDATE @tblHYG SET H2Loss = 12.5 WHERE H2Loss IS NULL
	UPDATE @tblHYG SET FeedH2KSCFpD = UtilCap*FeedH2/100, H2LossKSCFpD = UtilCap*H2Loss/(100-H2Loss)
END
DECLARE @HYG_UtilCap float, @HYG_FeedH2_KSCFpD float, @HYG_H2Loss_KSCFpD float, @HYG_H2Loss_MBTU float, @HYG_CE float
SELECT 	@HYG_UtilCap = SUM(UtilCap), @HYG_FeedH2_KSCFpD = SUM(FeedH2KSCFpD), @HYG_H2Loss_KSCFpD = SUM(H2LossKSCFpD)
FROM @tblHYG 

SELECT @HYG_H2Loss_MBTU = @HYG_H2Loss_KSCFpD*@NumDays*0.275
SELECT @HYG_CE = @HYG_H2Loss_MBTU*0.049*@CO2_C_Ratio
IF @DEBUG = 1
	SELECT HYG_UtilCap = @HYG_UtilCap, HYG_FeedH2 = @HYG_FeedH2_KSCFpD, HYG_H2Loss_SCFD = @HYG_H2Loss_KSCFpD, HYG_H2Loss_MBTU = @HYG_H2Loss_MBTU, HYG_CE = @HYG_CE/@CO2_C_Ratio

DECLARE @SecondaryLBG_MBTU float, @SecondaryLBG_CE float, @SecondaryLBG_CEFactor float
SELECT @SecondaryLBG_MBTU = ISNULL(@SE_U71LBG, 0) + ISNULL(@HYG_H2Loss_MBTU, 0),
	@SecondaryLBG_CE = ISNULL(@CE_U71LBG, 0) + ISNULL(@HYG_CE, 0)
SELECT @SecondaryLBG_CEFactor = ISNULL(@SecondaryLBG_CE, 0)/CASE WHEN @SecondaryLBG_MBTU > 0 THEN @SecondaryLBG_MBTU END
IF @DEBUG = 1
	SELECT SecondaryLBG_MBTU = @SecondaryLBG_MBTU, SecondaryLBG_CE = @SecondaryLBG_CE/@CO2_C_Ratio, SecondaryLBG_CEFactor = @SecondaryLBG_CEFactor/@CO2_C_Ratio

IF @OriginalLBG_MBTU > @LBG_NetSources
	SET @OriginalLBG_MBTU = @LBG_NetSources
SELECT @OriginalLBG_CE = @OriginalLBG_CEFactor*@OriginalLBG_MBTU
IF @SecondaryLBG_MBTU > (@LBG_NetSources-@OriginalLBG_MBTU)
	SELECT @SecondaryLBG_MBTU = (@LBG_NetSources-@OriginalLBG_MBTU)
SELECT @SecondaryLBG_CE = @SecondaryLBG_MBTU*@SecondaryLBG_CEFactor
DECLARE @OtherLBG_MBTU float, @OtherLBG_CE float, @LBG_CE float, @LBG_CE_Factor real
SELECT @OtherLBG_MBTU = @LBG_NetSources - @OriginalLBG_MBTU - @SecondaryLBG_MBTU
SELECT @OtherLBG_CE = @DefaultCEF_FG*@OtherLBG_MBTU*@CO2_C_Ratio
SELECT @LBG_CE = ISNULL(@OriginalLBG_CE, 0) + ISNULL(@SecondaryLBG_CE, 0) + ISNULL(@OtherLBG_CE, 0)
SELECT @LBG_CE_Factor = CASE WHEN @LBG_NetSources > 0 THEN @LBG_CE/@LBG_NetSources END

IF @DEBUG = 1
	SELECT OriginalLBG_CE = @OriginalLBG_CE/@CO2_C_Ratio, 
		SecondaryLBG_CE = @SecondaryLBG_CE/@CO2_C_Ratio, 
		OtherLBG_CE = @OtherLBG_CE/@CO2_C_Ratio, 
		LBG_CE = @LBG_CE/@CO2_C_Ratio, 
		LBG_CEF = @LBG_CE_Factor/@CO2_C_Ratio

DECLARE @NetLBG_MBTU float, @NetLBG_CE float, @NetLBG_CEFactor real
SELECT @NetLBG_MBTU = ISNULL(@LBG_NetSources, 0) - ISNULL(@SecondaryLBG_MBTU, 0)
SELECT @NetLBG_CE = ISNULL(@LBG_CE, 0) - ISNULL(@SecondaryLBG_CE, 0)
IF @NetLBG_MBTU > 0
	SELECT @NetLBG_CEFactor = @NetLBG_CE/@NetLBG_MBTU
IF @DEBUG = 1
BEGIN
	SELECT LBG_PRO = @LBG_PRO, LBG_NetSources = @LBG_NetSources, SecondaryLBG_MBTU = @SecondaryLBG_MBTU, LBG_CE = @LBG_CE/@CO2_C_Ratio, SecondaryLBG_CE = @SecondaryLBG_CE/@CO2_C_Ratio, NetLBG_CEFactor = @NetLBG_CEFactor/@CO2_C_Ratio, NetLBG_CE = @NetLBG_CE/@CO2_C_Ratio, NetLBG_MBTU = @NetLBG_MBTU
	SELECT CEF = @NetLBG_CEFactor/@CO2_C_Ratio , CE_NetLBG = @NetLBG_CE/@CO2_C_Ratio, NetLBG = @NetLBG_MBTU
END

IF @LBG_PRO > 0 AND ISNULL(@NetLBG_CEFactor, 0) > 0
	SELECT @EstCEF_LBG = @NetLBG_CEFactor
ELSE IF ISNULL(@SecondaryLBG_MBTU, 0) > 0
	SELECT @EstCEF_LBG = 0
ELSE
	SELECT @EstCEF_LBG = 0.03*@CO2_C_Ratio


SELECT c.UnitID, c.ProcessID, c.ProcessType, c.UtilCap, H2Loss = CASE WHEN t2.H2Loss IS NULL OR (t2.H2Loss = 0 /*AND t2.@CO2RejectType <> 'SOLV'*/) THEN 12.5 ELSE t2.H2Loss END/100
	, SE_POX = CASE WHEN c.ProcessType = 'POX' THEN c.UtilCap*@NumDays*60/1000 ELSE 0 END
	, Carbon = CASE WHEN c.ProcessType IN ('HSM','HSN') THEN (ISNULL(0*FeedH2,0)+ISNULL(1*FeedC1,0)+ISNULL(2*FeedC2,0)+ISNULL(3*FeedC3,0)+ISNULL(4*FeedC4,0)
					+ ISNULL(4*FeedLPG,0)+ISNULL(8*FeedNap,0)+ISNULL(12*FeedKero,0)+ISNULL(17*FeedDiesel,0)+ISNULL(28*FeedHGO,0)+ISNULL(28*FeedARC,0)+ISNULL(28*FeedVacResid,0)+ISNULL(28*FeedAsp,0)+ISNULL(28*FeedCoke,0))
			WHEN c.ProcessType = 'POX' THEN (ISNULL(0*FeedH2,0)+ISNULL(1*FeedC1,0)+ISNULL(2*FeedC2,0)+ISNULL(3*FeedC3,0)+ISNULL(4*FeedC4,0)
					+ISNULL(4*FeedLPG,0)+ISNULL(8*FeedNap,0)+ISNULL(12*FeedKero,0)+ISNULL(17*FeedDiesel,0)+ISNULL(28*FeedHGO,0)+ISNULL(28*FeedARC,0)+ISNULL(28*FeedVacResid,0)+ISNULL(28*FeedAsp,0)+ISNULL(28*FeedCoke,0))
			ELSE 1 END
	, H2Yield = CASE WHEN c.ProcessType IN ('HSM','HSN') THEN (ISNULL(1*FeedH2,0)+ISNULL(4*FeedC1,0)+ISNULL(7*FeedC2,0)+ISNULL(10*FeedC3,0)+ISNULL(13*FeedC4,0)
					+ ISNULL(13*FeedLPG,0)+ISNULL(25.5*FeedNap,0)+ISNULL(36*FeedKero,0)+ISNULL(49.5*FeedDiesel,0)+ISNULL(75*FeedHGO,0)+ISNULL(79.5*FeedARC,0)+ISNULL(75*FeedVacResid,0)+ISNULL(68.5*FeedAsp,0)+ISNULL(66.5*FeedCoke,0))
			WHEN c.ProcessType = 'POX' THEN (ISNULL(1*FeedH2,0)+ISNULL(3*FeedC1,0)+ISNULL(4.9*FeedC2,0)+ISNULL(6.9*FeedC3,0)+ISNULL(8.8*FeedC4,0)
					+ISNULL(8.8*FeedLPG,0)+ISNULL(17.1*FeedNap,0)+ISNULL(23.4*FeedKero,0)+ISNULL(31.7*FeedDiesel,0)+ISNULL(45.6*FeedHGO,0)+ISNULL(50.1*FeedARC,0)+ISNULL(45.6*FeedVacResid,0)+ISNULL(46.8*FeedAsp,0)+ISNULL(37.1*FeedCoke,0))
			ELSE 1 END
	, FeedFractionGas = (ISNULL(FeedH2,0)+ISNULL(FeedC1,0) + ISNULL(FeedC2,0) + ISNULL(FeedC3,0) + ISNULL(FeedC4,0))/100
	, FeedFractionOther = (ISNULL(FeedLPG,0)+ISNULL(FeedNap,0) + ISNULL(FeedKero,0) + ISNULL(FeedDiesel,0) + ISNULL(FeedHGO,0) + ISNULL(FeedARC,0) + ISNULL(FeedVacResid,0) + ISNULL(FeedLubeExtracts,0) + ISNULL(FeedAsp,0) + ISNULL(FeedCoke,0))/100
	, FeedFractionLiquid = CAST(NULL AS real)
	, GasFeedH2 = (ISNULL(FeedH2*1,0)+ISNULL(FeedC1*4,0) + ISNULL(FeedC2*7,0) + ISNULL(FeedC3*10,0) + ISNULL(FeedC4*13,0))
	, GasFeedCO2 = (ISNULL(FeedH2*0,0)+ISNULL(FeedC1*1,0) + ISNULL(FeedC2*2,0) + ISNULL(FeedC3*3,0) + ISNULL(FeedC4*4,0))
	, LiquidFeedCE = CASE WHEN ProcessType = 'HSN' THEN (ISNULL(FeedLPG*4.41,0)+ISNULL(FeedNap*4.5,0) + ISNULL(FeedKero*4.78,0) + ISNULL(FeedDiesel*4.93,0) + ISNULL(FeedHGO*5.4,0) + ISNULL(FeedARC*5.1,0) + ISNULL(FeedVacResid*5.4,0) + ISNULL(FeedLubeExtracts*5.5 /*Guess by GLC*/,0) + ISNULL(FeedAsp*5.86,0) + ISNULL(FeedCoke*6,0))
			ELSE (ISNULL(FeedLPG*6.5,0)+ISNULL(FeedNap*6.7,0) + ISNULL(FeedKero*7.4,0) + ISNULL(FeedDiesel*7.7,0) + ISNULL(FeedHGO*8.8,0) + ISNULL(FeedARC*8,0) + ISNULL(FeedVacResid*8.8,0) + ISNULL(FeedLubeExtracts*8.8 /*Guess by GLC*/,0) + ISNULL(FeedAsp*10.3,0) + ISNULL(FeedCoke*10.8,0)) END/1000/100
	, CE_KSCF_GasFeed = CAST(NULL AS Float), CE_KSCF_GasFeedPlusLosses = CAST(NULL AS Float)
	, CE_KSCF_LiquidFeed = CAST(NULL AS Float), CE_KSCF_LiquidPlusLosses = CAST(NULL AS Float)
	, GrossCE = CAST(NULL AS Float), CE_POX = CAST(NULL AS Float), CE_H2Prod = CAST(NULL AS Float)
	, ACEF = CAST(NULL AS float), ACE = CAST(NULL AS float), SCEF = CAST(NULL AS float), SCE = CAST(NULL AS float)
	, ACELessSCE_NonPOX = CAST(NULL AS float), ACELessSCE_POX = CAST(NULL AS float)
INTO #CalcHYG
FROM Config c LEFT JOIN @tblHYG t2 ON t2.UnitID = c.UnitID
WHERE ((c.ProcessID = 'HYG' AND c.ProcessType IN ('HSM','HSN','POX')))
AND c.SubmissionID = @SubmissionID 

UPDATE #CalcHYG SET FeedFractionLiquid = CASE WHEN ProcessType = 'HSM' THEN 0 ELSE (1-FeedFractionGas) END
UPDATE #CalcHYG SET CE_KSCF_GasFeed = CASE WHEN FeedFractionLiquid = 1 THEN 0 WHEN FeedFractionGas = 0 THEN 0.0036 ELSE GasFeedCO2/GasFeedH2*(12.01/379.4)*1000/2204.6 END
UPDATE #CalcHYG SET CE_KSCF_GasFeedPlusLosses = CE_KSCF_GasFeed/(1-H2Loss)
UPDATE #CalcHYG SET CE_KSCF_LiquidFeed = CASE WHEN FeedFractionOther = 0 THEN CASE ProcessType WHEN 'HSN' THEN 0.0045 WHEN 'POX' THEN 0.0088 ELSE 0 END ELSE LiquidFeedCE/FeedFractionOther END
UPDATE #CalcHYG SET CE_KSCF_LiquidPlusLosses = CE_KSCF_LiquidFeed/(1-H2Loss)
UPDATE #CalcHYG SET ACEF = CE_KSCF_LiquidPlusLosses*FeedFractionLiquid+(1-FeedFractionLiquid)*CE_KSCF_GasFeedPlusLosses
UPDATE #CalcHYG SET SCEF = (CE_KSCF_LiquidFeed*FeedFractionLiquid+(1-FeedFractionLiquid)*CE_KSCF_GasFeed)/(1-0.125)
UPDATE #CalcHYG SET ACE = ACEF*UtilCap*@NumDays*@CO2_C_Ratio, SCE = SCEF*UtilCap*@NumDays*@CO2_C_Ratio
UPDATE #CalcHYG SET ACELessSCE_NonPOX = CASE WHEN ProcessType IN ('HSM','HSN') THEN ACE-SCE ELSE 0 END, ACELessSCE_POX = CASE WHEN ProcessType IN ('HSM','HSN') THEN 0 ELSE ACE-SCE END
UPDATE #CalcHYG SET Carbon = 0, H2Yield = 20 WHERE H2Yield = 0 AND ProcessType = 'HSM'
UPDATE #CalcHYG SET Carbon = Carbon + (100-H2Yield), H2Yield = H2Yield + 4*(100-H2Yield) WHERE H2Yield < 100 AND ProcessType = 'HSM'
UPDATE #CalcHYG SET Carbon = 4, H2Yield = 13 WHERE H2Yield = 0 AND ProcessType = 'HSN'
UPDATE #CalcHYG SET Carbon = 28, H2Yield = 45.6 WHERE H2Yield = 0 AND ProcessType = 'POX'
UPDATE #CalcHYG
SET 	CE_POX = SE_POX*0.0293*@CO2_C_Ratio,
	GrossCE = UtilCap*@NumDays*Carbon/H2Yield*CASE WHEN ProcessID = 'U72' THEN 0.369*0.0233 ELSE 12/0.3794/2204.6/.875 END*@CO2_C_Ratio
UPDATE #CalcHYG SET CE_H2Prod = GrossCE - ISNULL(CE_POX, 0)

-- Hydrogen Production Emissions
DECLARE @ACE_H2_SMR float, @SCE_H2_SMR float
DECLARE @ACE_H2_POX float, @SCE_H2_POX float
SELECT @SCE_H2_SMR = SUM(SCE) FROM #CalcHYG WHERE ProcessType <> 'POX'
SELECT @SCE_H2_POX = SUM(SCE) FROM #CalcHYG WHERE ProcessType = 'POX'
SELECT @ACE_H2_SMR = SUM(ACELessSCE_NonPox), @ACE_H2_POX = SUM(ACELessSCE_POX)
FROM #CalcHYG
SELECT 	@SCE_H2_SMR = ISNULL(@SCE_H2_SMR, 0), 
	@SCE_H2_POX = ISNULL(@SCE_H2_POX, 0) - ISNULL(@SCE_HYGPOX, 0)
SELECT 	@ACE_H2_SMR = ISNULL(@ACE_H2_SMR, 0) + @SCE_H2_SMR,
	@ACE_H2_POX = ISNULL(@ACE_H2_POX, 0) + ISNULL(@SCE_H2_U72, 0) + @SCE_H2_POX

IF @DEBUG = 1
BEGIN
	SELECT SCE_HYGPOX = ISNULL(@SCE_HYGPOX, 0)/@CO2_C_Ratio, ACE_H2_POX = @ACE_H2_POX/@CO2_C_Ratio
	SELECT * FROM #CalcHYG
END

DROP TABLE #CalcHYG
DECLARE @SCE_MEOH float
SELECT @SCE_MEOH = -@U70_UtilCap*@NumDays*0.0472*@CO2_C_Ratio
IF @DEBUG = 1
	SELECT U70_UtilCap = @U70_UtilCap, SCE_MEOH = @SCE_MEOH/@CO2_C_Ratio

-- Indirect Emissions Related to Hydrogen
DECLARE @ACE_H2Pur float, @ACE_H2TransIn float, @ACE_H2TransOut float, @ACE_H2Sales float, @ACE_H2Deficit float
SELECT TransType = CASE Category WHEN 'RMI' THEN CASE WHEN MaterialID LIKE 'L%' THEN 'XIn' ELSE 'P' END WHEN 'RLUBE' THEN 'Xin' WHEN 'FFUEL' THEN 'Xout' WHEN 'OTHRM' THEN 'P' WHEN 'RCHEM' THEN 'Xin' WHEN 'FLUBE' THEN 'Xout' WHEN 'MPROD' THEN 'S' WHEN 'FCHEM' THEN 'Xout' END,
CE = CASE WHEN Bbl > 0 THEN Bbl*CASE WHEN MaterialId LIKE '[M,L]26' THEN 0.0488*1.15
			WHEN MaterialId LIKE '[M,L]78' THEN 0.0208
			WHEN MaterialId LIKE '[M,L]118' THEN 0.0279
			WHEN MaterialId LIKE '[M,L]119' THEN 0.0358 END*@CO2_C_Ratio
	ELSE MT*CASE WHEN MaterialId LIKE '[M,L]26' THEN .81*1.15
			WHEN MaterialId LIKE '[M,L]78' THEN .168
			WHEN MaterialId LIKE '[M,L]118' THEN .248
			WHEN MaterialId LIKE '[M,L]119' THEN .388 END
	END*1.5
INTO #H2Indirect
FROM Yield 
WHERE SubmissionID = @SubmissionID AND MaterialID IN ('M26','M78','M118','M119','L26','L78','L118','L119')

SELECT 	@ACE_H2Pur = SUM(CASE WHEN TransType = 'P' THEN CE ELSE 0 END),
	@ACE_H2TransIn = SUM(CASE WHEN TransType = 'XIn' THEN CE ELSE 0 END),
	@ACE_H2TransOut = -SUM(CASE WHEN TransType = 'XOut' THEN CE ELSE 0 END),
	@ACE_H2Sales = -SUM(CASE WHEN TransType = 'S' THEN CE ELSE 0 END)
FROM #H2Indirect
SELECT 	@ACE_H2Pur = ISNULL(@ACE_H2Pur, 0),
	@ACE_H2TransIn = ISNULL(@ACE_H2TransIn, 0),
	@ACE_H2TransOut = ISNULL(@ACE_H2TransOut, 0),
	@ACE_H2Sales = ISNULL(@ACE_H2Sales, 0)

DROP TABLE #H2Indirect

-- Methane Annex F Basis
DECLARE @CombustionCH4 float, @FlareCH4Emissions float
SELECT @CombustionCH4 = @NumDays*SUM(UtilCap*CH4_MTperMBbl/1E6) 
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID 
WHERE c.SubmissionID = @SubmissionID AND lu.CH4_MTperMBbl IS NOT NULL --AND c.ProcessID <> 'ASP'
SELECT 	@CombustionCH4 = ISNULL(@CombustionCH4, 0), 
	@FlareCH4Emissions = @InputBbl/1000*0.189/379.3*16/2205

DECLARE @FactorSet FactorSet
DECLARE @SE_EII float, @SE_CEI float, @EII real, @AE_EII float
DECLARE @ACE_N2O float, @SCE_N2O float
DECLARE @ACE_H2Prod float, @SCE_H2Prod float
DECLARE @SE_ReferenceFuel float, @SCE_ReferenceFuel float
DECLARE @ACE_TotalCO2_Energy float, @SCE_TotalCO2_Energy float
DECLARE @ACE_TotalCO2 float, @SCE_TotalCO2 float
DECLARE	@SE_NonReferenceFuel float, @SCE_NonReferenceFuel float
DECLARE @AE_EII_Total float,   @AE_Total float,   @ACE_Total float,   @SE_Total float,   @SCE_Total float,   @CEI_Total real
DECLARE @CEF_PUR_NG real, @CEF_PUR_FG real, @CEF_PUR_FGNG real, @CEF_PUR_LBG real, @CEF_PUR_HPF real,
	@CEF_TXI_NG real, @CEF_TXI_FG real, @CEF_TXI_FGNG real, @CEF_TXI_LBG real, 
	@CEF_TXO_NG real, @CEF_TXO_FG real, @CEF_TXO_FGNG real, @CEF_TXO_LBG real, 
	@CEF_SOL_NG real, @CEF_SOL_FG real, @CEF_SOL_FGNG real, @CEF_SOL_LBG real,
	@CEF_PRO_FG real, @CEF_PRO_FCG real, @CEF_PRO_FXG real, @CEF_PRO_PSG real, @CEF_PRO_PXG real, @CEF_PRO_LBG real,
	@CEF_STM_PUR real, @CEF_FG real, @CEF_NG real, @CEF_LBG real
DECLARE @AE_Fungible float, @ACE_Fungible float, @AE_FG float, @ACE_FG float, @SE_FG float, @SCE_FG float, @AE_NG float, @ACE_NG float, 
	@SE_FG_Directs float, @SCE_FG_Directs float, @SE_FG_Indirects float, @SCE_FG_Indirects float, @AE_LBG float, @ACE_LBG float,
	@AE_StmPur float, @ACE_StmPur float, @AE_StmTransIn float, @ACE_StmTransIn float, 
	@AE_StmDist float, @AE_StmTransOut float, @AE_StmSales float, @AE_GEDist float, @AE_GESales float,
	@ACE_NMC float, @ACE_COC float, @SE_COC float, @SCE_COC float
DECLARE @ACE_MC float, @ACE_CaC float, @SCE_CALCIN float, @SE_CALCIN float
DECLARE @ACE_ASP_CO2 float, @SCE_ASP_CO2 float, @ACE_ASP_CH4 float, @SCE_ASP_CH4 float
DECLARE @SCE_CO2Sales float, @ACE_CO2Sales float, @SCE_FlareLoss float, @ACE_FlareLoss float, @SCE_CH4 float
DECLARE @ACE_OthProcessCO2 float, @SCE_OthProcessCO2 float

-- Start calculations for each factorset since 2008
DECLARE cFactorSets CURSOR LOCAL FAST_FORWARD
FOR 	SELECT FactorSet, TotStdEnergy*@NumDays, EII
	FROM FactorTotCalc WHERE SubmissionID = @SubmissionID AND FactorSet IN ('2008','2010') AND EII <> 0

OPEN cFactorSets
FETCH NEXT FROM cFactorSets INTO @FactorSet, @SE_EII, @EII
WHILE @@FETCH_STATUS = 0 
BEGIN
	SELECT @AE_EII = @TotEnergyConsMBTU --@SE_EII*@EII/100

	-- Purchased Electricity using regional emissions factors
	DECLARE @ACE_PurElec float, @SE_PurElec float, @SCE_PurElec float, @ACE_ElecTransIn float
	SELECT @ACE_ElecTransIn = 0
	SELECT 	@ACE_PurElec = @AE_PurElec/9.09*@RegionalEEF*@CO2_C_Ratio
	-- Standard consumption and emissions for purchased electricity 
	-- based on average electricity consumption as a percent of total energy 
	-- and regional average emissions for electric generators
	SELECT	@SE_PurElec = @AvgPurEnergyFraction*@SE_EII --Avg of 7.5% of energy is purchased electricity
	SELECT 	@SCE_PurElec = @SE_PurElec/9.09*@RegionalEEF*@CO2_C_Ratio


	-- Standard Energy + actual energy consumed to produce steam and electricity distributed/sold from refinery
	SELECT @SE_CEI = @SE_EII
	SELECT @AE_Fungible = @Fungible_Cons, @ACE_Fungible = @CE_Fungible_Cons*@CO2_C_Ratio, 
			@AE_FG = @FG_Cons, @AE_NG = @NG_Cons, @AE_LBG = @LBG_Cons,
			@AE_StmTransOut = NULL, @AE_StmSales = NULL
	IF (@FG_PUR + @FG_PRO + @FG_TransIn - @FG_TransOut - @FG_SOL) > 0
		SELECT @CEF_FG = (@FG_PUR*ISNULL(@ClientCEF_PUR_FG, @DefaultCEF_FG)
				+ @FG_PRO*ISNULL(@CEF_PRO_FG, @CEF_FG_PRO)
				+ @FG_TransIn*ISNULL(@ClientCEF_TXI_FG, @DefaultCEF_FG)
				- @FG_TransOut*ISNULL(@ClientCEF_TXO_FG, @DefaultCEF_FG)
				- @FG_SOL*ISNULL(@ClientCEF_SOL_FG, @DefaultCEF_FG))
				/(@FG_PUR + @FG_PRO + @FG_TransIn - @FG_TransOut - @FG_SOL)
	ELSE
		SELECT @CEF_FG = @DefaultCEF_FG
	IF (@NG_PUR + @NG_TransIn - @NG_TransOut - @NG_SOL) > 0
		SELECT @CEF_NG = (@NG_PUR*ISNULL(@ClientCEF_PUR_NG, @DefaultCEF_NG)
				+ @NG_TransIn*ISNULL(@ClientCEF_TXI_NG, @DefaultCEF_NG)
				- @NG_TransOut*ISNULL(@ClientCEF_TXO_NG, @DefaultCEF_NG)
				- @NG_SOL*ISNULL(@ClientCEF_SOL_NG, @DefaultCEF_NG))
				/(@NG_PUR + @NG_TransIn - @NG_TransOut - @NG_SOL)
	ELSE
		SELECT @CEF_NG = @DefaultCEF_NG

	IF @StudyYear < 2008
		SELECT @CEF_LBG = @EstCEF_LBG/@CO2_C_Ratio
	ELSE IF (@LBG_PUR + @LBG_PRO_FX + @LBG_PRO_FC + @LBG_PRO_POX + @LBG_PRO_PSA + @LBG_PRO_Other + @LBG_TransIn - @LBG_TransOut - @LBG_SOL) > 0
		SELECT @CEF_LBG = (@LBG_PUR*ISNULL(@ClientCEF_PUR_LBG, @DefaultCEF_LBG)
				+ @LBG_PRO_FX*ISNULL(@ClientCEF_PRO_FXG, @DefaultCEF_CokerLBG)
				+ @LBG_PRO_FC*ISNULL(@ClientCEF_PRO_FCG, @DefaultCEF_CokerLBG) 
				+ @LBG_PRO_POX*ISNULL(@ClientCEF_PRO_PXG, @DefaultCEF_LBG)
				+ @LBG_PRO_PSA*ISNULL(@ClientCEF_PRO_PSG, @DefaultCEF_LBG)
				+ @LBG_PRO_Other*ISNULL(@ClientCEF_PRO_LBG, @DefaultCEF_LBG)
				+ @LBG_TransIn*ISNULL(@ClientCEF_TXI_LBG, @DefaultCEF_LBG)
				- @LBG_TransOut*ISNULL(@ClientCEF_TXO_LBG, @DefaultCEF_LBG)
				- @LBG_SOL*ISNULL(@ClientCEF_SOL_LBG, @DefaultCEF_LBG))
				/(@LBG_PUR + @LBG_PRO_FX + @LBG_PRO_FC + @LBG_PRO_POX + @LBG_PRO_PSA + @LBG_PRO_Other + @LBG_TransIn - @LBG_TransOut - @LBG_SOL)
	ELSE
		SELECT @CEF_LBG = @EstCEF_LBG/@CO2_C_Ratio

	SELECT @ACE_FG = @FG_Cons*@CEF_FG*@CO2_C_Ratio, @ACE_NG = @NG_Cons*@CEF_NG*@CO2_C_Ratio, @ACE_LBG = @LBG_Cons*@CEF_LBG*@CO2_C_Ratio

	SELECT @CEF_PUR_NG = @ClientCEF_PUR_NG, @CEF_PUR_FG = @ClientCEF_PUR_FG, @CEF_PUR_LBG = @ClientCEF_PUR_LBG, @CEF_PUR_HPF = @ClientCEF_PUR_HPF, @CEF_PUR_FGNG = NULL, 
	@CEF_PRO_FG = @ClientCEF_PRO_FG, @CEF_PRO_FCG = @ClientCEF_PRO_FCG, @CEF_PRO_FXG = @ClientCEF_PRO_FXG, @CEF_PRO_PSG = @ClientCEF_PRO_PSG, @CEF_PRO_PXG = @ClientCEF_PRO_PXG, @CEF_PRO_LBG = @ClientCEF_PRO_LBG,
	@CEF_TXI_NG = @ClientCEF_TXI_NG, @CEF_TXI_FG = @ClientCEF_TXI_FG, @CEF_TXI_LBG = @ClientCEF_TXI_LBG, @CEF_TXI_FGNG = NULL, 
	@CEF_TXO_NG = @ClientCEF_TXO_NG, @CEF_TXO_FG = @ClientCEF_TXO_FG, @CEF_TXO_LBG = @ClientCEF_TXO_LBG, @CEF_TXO_FGNG = NULL, 
	@CEF_SOL_NG = @ClientCEF_SOL_NG, @CEF_SOL_FG = @ClientCEF_SOL_FG, @CEF_SOL_LBG = @ClientCEF_SOL_LBG, @CEF_SOL_FGNG = NULL

	-- Marketable Coke consumed in calcining
	SELECT @ACE_MC = (@AE_MC*@DefaultCEF_COK)*@CO2_C_Ratio, @ACE_CaC = (@AE_CaC*@DefaultCEF_CalcineCokeCons)*@CO2_C_Ratio
	SELECT @SE_CALCIN = 0.22*dbo.UnitsConv(@CALCINUtilCap, 'ST', 'LB')*@NumDays*(14500/1E6)
	SELECT @SCE_CALCIN = @SE_CALCIN*@DefaultCEF_CalcineCokeCons*@CO2_C_Ratio


	-- Non-Marketable Coke Emissions
	SELECT @ACE_NMC = @AE_NMC*@DefaultCEF_COK*@CO2_C_Ratio


	-- Coke on Catalyst
	EXEC CalcCEICOC @SubmissionID, @FactorSet, @AE_COC OUTPUT, @ACE_COC OUTPUT, @SE_COC OUTPUT, @SCE_COC OUTPUT, @DEBUG
	
	-- Steam Purchases Emissions
	SELECT @CEF_STM_PUR = @DefaultCEF_NG, @AE_StmPur = @STM_Cons, @AE_StmTransIn = 0
	SELECT @ACE_StmPur = @AE_StmPur * @CEF_STM_PUR * @CO2_C_Ratio, @ACE_StmTransIn = @AE_StmTransIn * @CEF_STM_PUR * @CO2_C_Ratio

	-- Asphalt blowing
	SELECT 	@ACE_ASP_CO2 = ISNULL(@ASP_UtilCap, 0)*@NumDays/6.06*0.03*0.223,  -- 0.223 MT CO2e, not CO with most factors
		@ACE_ASP_CH4 = ISNULL(@ASP_UtilCap, 0)*@NumDays/6.06*0.03*0.117*23
	SELECT @SCE_ASP_CO2 = @ACE_ASP_CO2, @SCE_ASP_CH4 = @ACE_ASP_CH4

	-- CO2 Sales
	SELECT @SCE_CO2Sales = -@CO2SalesMT, @ACE_CO2Sales = -@CO2SalesMT

	-- Flare Losses
	SELECT 	@SCE_FlareLoss = 0.77*@InputMT*@StdFlareLoss*@CO2_C_Ratio, 
		@ACE_FlareLoss = 0.77*@InputMT*ISNULL(@FlareLoss/100, @StdFlareLoss)*@CO2_C_Ratio

	-- Nitrous Oxide
	SELECT @ACE_N2O = (298/@CO2_C_Ratio) * (@AE_Fungible*1.6E-6 + (@AE_FG + @AE_NG + @AE_LBG)*2.0E-6) * @CO2_C_Ratio  --296MTCO2e/MTN2O
	SELECT @SCE_N2O = (298/@CO2_C_Ratio) * (@SE_CEI - ISNULL(@SE_COC, 0) - ISNULL(@SE_FCCOKE, 0) - ISNULL(@SE_FXCOKE, 0) - ISNULL(@SE_CALCIN, 0) - ISNULL(@SE_U73POX, 0) - ISNULL(@SE_U72POX, 0) - ISNULL(@SE_HYGPOX, 0) - ISNULL(@SE_PurElec, 0))*2.0E-6*@CO2_C_Ratio


	-- Methane Annex F Basis
	SELECT @SCE_CH4 = 25.0*(8.4 + (0.3941+2.6209+0.036+0.0451)*@InputBbl/1E6 + ISNULL(@CombustionCH4,0) + @FlareCH4Emissions)

	-- Hydrogen Deficit
	SELECT @ACE_H2Deficit = 0

	IF @DEBUG = 1
	BEGIN
		SELECT ACE_FG = @ACE_FG, CE_FG_PUR = @CE_FG_PUR, FG_PRO = @FG_PRO, CEF_FG_PRO = @CEF_FG_PRO, CE_FG_PRO = @CE_FG_PRO , CE_FG_DST = @CE_FG_DST , CE_FG_Sales = @CE_FG_Sales
		SELECT ACE_LBG = @ACE_LBG/@CO2_C_Ratio , AE_LBG = @AE_LBG, CEF_LBG = @CEF_LBG/@CO2_C_Ratio
		SELECT AE_MC = @AE_MC, AE_CaC = @AE_CaC, ACE_MC = @ACE_MC/@CO2_C_Ratio, ACE_CaC = @ACE_CaC/@CO2_C_Ratio, CALCINUtilCap = @CALCINUtilCap, SCE_CALCIN = @SCE_CALCIN/@CO2_C_Ratio, SE_CALCIN = @SE_CALCIN
		SELECT CO2SalesMT = @CO2SalesMT, @ACE_CO2Sales

		SELECT c.ProcessID, UtilCap = SUM(UtilCap) , CombustionCH4 = SUM(UtilCap*CH4_MTperMBbl)
		FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID 
		WHERE c.SubmissionID = @SubmissionID AND lu.CH4_MTperMBbl IS NOT NULL
		GROUP BY c.ProcessID
		SELECT SCE_CH4 = @SCE_CH4, AllInputfactors = (8.4 + (0.3941+2.6209+0.036+0.0451)*@InputBbl/1E6), Combustion = ISNULL(@CombustionCH4,0), ASP = ISNULL(@SCE_ASP_CH4, 0), Flare = @FlareCH4Emissions
	END

	
	SELECT 	@ACE_H2Prod = ISNULL(@ACE_H2_SMR, 0) + ISNULL(@ACE_H2_POX, 0) + ISNULL(@SCE_MEOH, 0),
		@SCE_H2Prod = ISNULL(@SCE_H2_SMR, 0) + ISNULL(@SCE_H2_POX, 0) + ISNULL(@SCE_H2_U72, 0) + ISNULL(@SCE_MEOH, 0)


	SELECT 	@SE_NonReferenceFuel = ISNULL(@SE_COC,0) + ISNULL(@SE_FCCOKE, 0) + ISNULL(@SE_FXCOKE,0) + ISNULL(@SE_FCLBG, 0) + ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_CALCIN, 0) + ISNULL(@SE_U73POX, 0) + ISNULL(@SE_U72POX, 0),
		@SCE_NonReferenceFuel = ISNULL(@SCE_COC,0) + ISNULL(@SCE_FCCOKE, 0) + ISNULL(@SCE_FXCOKE,0) + ISNULL(@SCE_FCLBG, 0) + ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_CALCIN, 0) + ISNULL(@SCE_U73POX, 0) + ISNULL(@SCE_U72POX, 0)
	SELECT 	@SE_ReferenceFuel = @SE_CEI - @SE_NonReferenceFuel - @SE_PurElec
	SELECT 	@SCE_ReferenceFuel = @SE_ReferenceFuel*@DefaultCEF_NG*@CO2_C_Ratio

	SELECT 	@ACE_OthProcessCO2 = ISNULL(@ACE_ASP_CO2, 0) + ISNULL(@ACE_FlareLoss, 0) + ISNULL(@ACE_CO2Sales, 0), 
		@SCE_OthProcessCO2 = ISNULL(@SCE_ASP_CO2, 0) + ISNULL(@SCE_FlareLoss, 0) + ISNULL(@SCE_CO2Sales, 0)
	SELECT	@ACE_TotalCO2_Energy = @ACE_Fungible + @ACE_FG + @ACE_NG + @ACE_LBG + @ACE_MC + ISNULL(@ACE_CaC, 0) + @ACE_NMC + @ACE_COC + @ACE_StmPur + @ACE_StmTransIn + @ACE_PurElec,
		@SCE_TotalCO2_Energy = @SCE_ReferenceFuel + ISNULL(@SCE_FCLBG, 0) + ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_HYGPOX, 0) + ISNULL(@SCE_U72POX, 0) + ISNULL(@SCE_U73POX, 0) + ISNULL(@SCE_CALCIN, 0) + ISNULL(@SCE_FCCoke, 0) + ISNULL(@SCE_FXCoke,0) + ISNULL(@SCE_COC,0) + ISNULL(@SCE_PurElec,0)
	SELECT	@ACE_TotalCO2 = @ACE_TotalCO2_Energy + ISNULL(@ACE_H2Prod, 0) + ISNULL(@ACE_H2Deficit, 0) + ISNULL(@ACE_OthProcessCO2, 0),
		@SCE_TotalCO2 = @SCE_TotalCO2_Energy + ISNULL(@SCE_H2Prod, 0) + ISNULL(@ACE_H2Deficit, 0) + ISNULL(@SCE_OthProcessCO2, 0)
	SELECT 	@AE_Total = @AE_Fungible + @AE_FG + @AE_NG + @AE_LBG + @AE_MC + @AE_CaC + @AE_NMC + @AE_COC + @AE_StmPur + @AE_StmTransIn + @AE_PurElec + @AE_ProdElecAdj, 
		@SE_Total = @SE_ReferenceFuel + ISNULL(@SE_FCLBG, 0) + ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_HYGPOX, 0) + ISNULL(@SE_U72POX, 0) + ISNULL(@SE_U73POX, 0) + ISNULL(@SE_CALCIN, 0) + ISNULL(@SE_FCCOKE, 0) + ISNULL(@SE_FXCOKE,0) + ISNULL(@SE_COC,0) + ISNULL(@SE_PurElec,0), 
		@ACE_Total = @ACE_TotalCO2 + ISNULL(@SCE_CH4, 0) + ISNULL(@ACE_N2O, 0),
		@SCE_Total = @SCE_TotalCO2 + ISNULL(@SCE_CH4, 0) + ISNULL(@SCE_N2O, 0)
	IF @SCE_Total <> 0 AND @EII > 0
		SELECT @CEI_Total = @ACE_Total/@SCE_Total*100
	ELSE
		SELECT @CEI_Total = NULL
	SELECT 	@AE_EII_Total = @AE_Total

	INSERT INTO CEI2008 (SubmissionID, FactorSet, CEI, AE_Total, ACE_Total, SE_Total, SCE_Total, AE_EII_Total, SE_EII_Total, 
		AE_Fungible, ACE_Fungible, AE_FG, ACE_FG, AE_NG, ACE_NG, SE_FG, SCE_FG, AE_LBG, ACE_LBG, 
		SE_COKLBG, SCE_COKLBG, SE_U73POX, SCE_U73POX, SE_POX, SCE_POX, 
		AE_MC, ACE_MC, AE_CaC, ACE_CaC, SE_CALCIN, SCE_CALCIN, AE_NMC, ACE_NMC, SE_FCFXCoke, SCE_FCFXCoke, AE_COC, ACE_COC, SE_COC, SCE_COC, 
		AE_StmPur, ACE_StmPur, AE_StmTransIn, ACE_StmTransIn, AE_PurElec, ACE_PurElec, SE_PurElec, SCE_PurElec, AE_ProdElecAdj, ACE_ElecTransIn, 
		ACE_H2_SMR, SCE_H2_SMR, ACE_H2_POX, SCE_H2_POX, SCE_MEOH, ACE_H2Prod, SCE_H2Prod, ACE_H2Pur, ACE_H2TransIn, ACE_H2TransOut, ACE_H2Sales, ACE_H2Deficit, 
		ACE_ASP_CO2, SCE_ASP_CO2, ACE_FlareLoss, SCE_FlareLoss, ACE_CO2Sales, SCE_CO2Sales, ACE_OthProcessCO2, SCE_OthProcessCO2, SCE_CH4, ACE_N2O, SCE_N2O,
		ACE_TotalCO2_Energy, SCE_TotalCO2_Energy, ACE_TotalCO2, SCE_TotalCO2)
	SELECT @SubmissionID, @FactorSet, @CEI_Total, @AE_Total, @ACE_Total, @SE_Total, @SCE_Total, @AE_EII_Total, @SE_EII
		, @AE_Fungible, @ACE_Fungible, @AE_FG, @ACE_FG, @AE_NG, @ACE_NG, @SE_ReferenceFuel, @SCE_ReferenceFuel, @AE_LBG, @ACE_LBG
		, ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_FCLBG, 0), ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_FCLBG, 0)
		, @SE_U73POX, @SCE_U73POX, ISNULL(@SE_U72POX, 0) + ISNULL(@SE_HYGPOX, 0), ISNULL(@SCE_U72POX, 0) + ISNULL(@SCE_HYGPOX, 0)
		, @AE_MC, @ACE_MC, @AE_CaC, @ACE_CaC, @SE_CALCIN, @SCE_CALCIN, @AE_NMC, @ACE_NMC
		, ISNULL(@SE_FXCoke, 0) + ISNULL(@SE_FCCoke, 0), ISNULL(@SCE_FXCoke, 0) + ISNULL(@SCE_FCCoke, 0)
		, @AE_COC, @ACE_COC, @SE_COC, @SCE_COC
		, @AE_StmPur, @ACE_StmPur, @AE_StmTransIn, @ACE_StmTransIn, @AE_PurElec, @ACE_PurElec, @SE_PurElec, @SCE_PurElec, @AE_ProdElecAdj, @ACE_ElecTransIn
		, @ACE_H2_SMR, @SCE_H2_SMR, ISNULL(@ACE_H2_POX, 0), ISNULL(@SCE_H2_POX, 0) + ISNULL(@SCE_H2_U72, 0), @SCE_MEOH, @ACE_H2Prod, @SCE_H2Prod
		, @ACE_H2Pur, @ACE_H2TransIn, @ACE_H2TransOut, @ACE_H2Sales, @ACE_H2Deficit
		, @ACE_ASP_CO2, @SCE_ASP_CO2, @ACE_FlareLoss, @SCE_FlareLoss, @ACE_CO2Sales, @SCE_CO2Sales, @ACE_OthProcessCO2, @SCE_OthProcessCO2
		, @SCE_CH4, @ACE_N2O, @SCE_N2O
		, @ACE_TotalCO2_Energy, @SCE_TotalCO2_Energy, @ACE_TotalCO2, @SCE_TotalCO2

	FETCH NEXT FROM cFactorSets INTO @FactorSet, @SE_EII, @EII
END
CLOSE cFactorSets
DEALLOCATE cFactorSets

IF @DEBUG = 1
	SELECT * FROM CEI2008 WHERE SubmissionID = @SubmissionID
