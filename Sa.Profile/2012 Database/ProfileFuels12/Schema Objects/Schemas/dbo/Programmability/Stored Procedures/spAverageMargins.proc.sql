﻿CREATE PROC [dbo].[spAverageMargins](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@Currency CurrencyCode, @Scenario Scenario, @GPV real OUTPUT, @RMC real OUTPUT, @GM real OUTPUT, @Opex real OUTPUT, @CM real OUTPUT)
AS
SELECT @GPV = SUM(m.GPV*m.Divisor)/SUM(m.Divisor), 
@RMC = SUM(m.RMC*m.Divisor)/SUM(m.Divisor), 
@GM = SUM(m.GrossMargin*m.Divisor)/SUM(m.Divisor), 
@Opex = SUM(m.CashOpex*m.Divisor)/SUM(m.Divisor), 
@CM = SUM(m.CashMargin*m.Divisor)/SUM(m.Divisor)
FROM MarginCalc m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND m.DataType = 'BBL'
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND m.Scenario = @Scenario AND m.Currency = @Currency
