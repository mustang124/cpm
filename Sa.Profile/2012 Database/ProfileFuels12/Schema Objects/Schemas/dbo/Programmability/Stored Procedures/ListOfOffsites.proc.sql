﻿CREATE PROC [dbo].[ListOfOffsites] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = NULL, @Scenario Scenario = NULL, @Currency CurrencyCode = NULL, @UOM varchar(5) = NULL)
AS
SELECT c2.ProcessID, c2.ProcessType, 
TabName = CASE WHEN c2.ProcessID = 'FCCPOWER' THEN 'FCC_Power_Recovery'
	ELSE RTRIM(c2.ProcessID) + CASE c2.ProcessType 
		WHEN '' THEN  ''
		WHEN 'SFB' THEN '_Solid_Fired'
		WHEN 'LGFB' THEN '_Liquid_Gas_Fired'
		WHEN 'DSL' THEN '_Diesel_Engine'
		WHEN 'EXP' THEN '_Expander'
		WHEN 'EXTR' THEN '_Extraction'
		WHEN 'FT' THEN '_Fired_Turbine'
		WHEN 'STT' THEN '_Steam_Topping'
		ELSE '_' + RTRIM(c2.ProcessType)
	END END
FROM Submissions s2 INNER JOIN Config c2 ON c2.SubmissionID = s2.SubmissionID
INNER JOIN (SELECT c.ProcessID, c.ProcessType, MAX(s.PeriodStart) AS LastEntry
	FROM Submissions s INNER JOIN Config c ON c.SubmissionID = s.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
	AND c.ProcessID IN ('STEAMGEN', 'ELECGEN', 'FCCPOWER')
	GROUP BY c.ProcessID, c.ProcessType) x ON x.ProcessID = c2.ProcessID AND x.ProcessType = c2.ProcessType AND x.LastEntry = s2.PeriodStart
WHERE s2.RefineryID = @RefineryID AND s2.DataSet = @DataSet
GROUP BY c2.ProcessID, c2.ProcessType
ORDER BY MIN(c2.SortKey)
