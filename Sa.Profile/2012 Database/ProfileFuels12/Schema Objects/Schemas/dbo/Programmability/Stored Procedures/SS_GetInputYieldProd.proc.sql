﻿CREATE PROC [dbo].[SS_GetInputYieldProd]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            y.Category,y.MaterialID,y.MaterialName,y.BBL,y.PriceLocal,m.Sortkey 
            FROM  
            dbo.Yield y, Material_LU m  
            ,dbo.Submissions s  
            WHERE  
            y.SubmissionID = s.SubmissionID AND 
            m.MaterialID=y.MaterialID AND  y.SubmissionID IN  
            (SELECT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1)
            AND Category NOT IN ('OTHRM','RCHEM','RLUBE','RMI','RPF') AND y.MaterialID <> 'GAIN'
