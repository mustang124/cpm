﻿CREATE PROC [dbo].[SS_GetProcessData]
	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
AS

	SELECT pd.UnitID,pd.Property,pd.RptValue,t.SortKey AS SortKey, RTRIM(cfg.ProcessID) AS ProcessID, RTRIM(cfg.UnitName) AS UnitName
             FROM dbo.ProcessData pd, Config cfg, Table2_LU  t   WHERE   cfg.UnitID=pd.UnitID AND 
             cfg.SubmissionID = pd.SubmissionID AND t.ProcessID=cfg.ProcessID AND t.Property=pd.Property AND (pd.SubmissionID IN 
             (SELECT SubmissionID FROM dbo.Submissions 
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            (DateAdd(Day, -1, @periodEnd)))))
