﻿CREATE     PROC [dbo].[spReportGensum] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2006', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT *
FROM Gensum
WHERE SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID AND DataSet=@DataSet AND PeriodYear = ISNULL(@PeriodYear, PeriodYear) AND PeriodMonth = ISNULL(@PeriodMonth, PeriodMonth))
AND FactorSet = @FactorSet AND Currency = @Currency AND UOM = @UOM AND Scenario = @Scenario
