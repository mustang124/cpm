﻿CREATE                  PROC [dbo].[spMaterialCost](@SubmissionID int)
AS
SET NOCOUNT ON

DELETE FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialNetCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialTotCost WHERE SubmissionID = @SubmissionID
DELETE FROM DataChecks WHERE SubmissionID = @SubmissionID AND DatacheckID = 'NoStudy$'

DECLARE @RptCurrency CurrencyCode, @RptCurrencyT15 CurrencyCode, @PeriodStart smalldatetime
SELECT @RptCurrency = RptCurrency , @RptCurrencyT15 = RptCurrencyT15, @PeriodStart = PeriodStart
FROM Submissions WHERE SubmissionID = @SubmissionID

DECLARE @RFCValue real, @RFCBbl real, @PurFGPrice real, @RFCGValue real
SELECT 	@RFCValue = ISNULL(ProdFGCostK, 0) + ISNULL(ProdOthCostK,0)
FROM EnergyTotCost 
WHERE SubmissionID = @SubmissionID AND Scenario = 'CLIENT' AND Currency = @RptCurrencyT15

SELECT @RFCBbl = Bbl FROM Yield WHERE SubmissionID = @SubmissionID AND MaterialID = 'RFC'

SELECT @PurFGPrice = SUM(PriceMBTULocal*SourceMBTU)/SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType = 'FG' AND PriceMBTULocal > 0 AND SourceMBTU > 0

IF @PurFGPrice IS NULL
	SELECT @PurFGPrice = AVG(PriceMBTULocal)
	FROM Energy
	WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType = 'FG' AND PriceMBTULocal > 0 
IF @PurFGPrice IS NULL
	SELECT @PurFGPrice = SUM(PriceMBTULocal*SourceMBTU)/SUM(SourceMBTU)
	FROM Energy
	WHERE (SubmissionID = @SubmissionID AND EnergyType NOT IN ('STM','LLH'))
	AND ((TransType = 'PUR' AND EnergyType NOT IN ('C2','C3','C4','NAP','DST'))
	OR TransType = 'DST')
	HAVING SUM(SourceMBTU)>0
IF @PurFGPrice IS NULL 
	SELECT @PurFGPrice = 0
ELSE
	SELECT @PurFGPrice = @PurFGPrice*6.05
--SELECT @RFCGValue = Cogen 
--FROM OpexAll WHERE SubmissionID = @SubmissionID AND Currency = @RptCurrency
IF @RFCGValue IS NULL
BEGIN
	SELECT @RFCGValue = SUM(PriceMBTULocal*UsageMBTU)/1000
	FROM Energy
	WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType <> 'GE'

	SELECT @RFCGValue = ISNULL(@RFCGValue, 0) + ISNULL(SUM(PriceLocal*UsageMWH)/100, 0)
	FROM Electric
	WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND PriceLocal > 0 AND UsageMWH > 0

	IF @RFCGValue IS NULL
		SELECT @RFCGValue = 0
END
IF @RptCurrency <> @RptCurrencyT15
	SELECT 	@PurFGPrice = @PurFGPrice * dbo.ExchangeRate(@RptCurrency, @RptCurrencyT15, @PeriodStart),
		@RFCGValue = @RFCGValue * dbo.ExchangeRate(@RptCurrency, @RptCurrencyT15, @PeriodStart)
IF @RFCBbl > 0
	UPDATE Yield
	SET PriceLocal = CASE WHEN @RFCValue > 0 AND @RFCBbl > 0 THEN @RFCValue*1000/@RFCBbl ELSE @PurFGPrice END
	WHERE SubmissionID = @SubmissionID AND MaterialID IN ('RFC','RFS')

UPDATE Yield
SET PriceLocal = @PurFGPrice
WHERE SubmissionID = @SubmissionID AND MaterialID = 'M106'

UPDATE Yield
SET PriceLocal = 1000*@RFCGValue/Bbl
WHERE SubmissionID = @SubmissionID AND MaterialID = 'RFCG' AND Bbl > 0

UPDATE Yield
SET PriceUS = PriceLocal * dbo.ExchangeRate(@RptCurrencyT15, 'USD', @PeriodStart)
WHERE SubmissionID = @SubmissionID

-- Load client-supplied prices
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
SELECT SubmissionID, 'CLIENT', SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl
FROM Yield WHERE SubmissionID = @SubmissionID

-- Load study prices
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
SELECT y.SubmissionID, p.StudyYear, y.SortKey, y.MaterialID, y.Category, ISNULL(p.PricePerbbl*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM Submissions WHERE SubmissionID = @SubmissionID), 0), p.PricePerbbl, y.Bbl
FROM Submissions s INNER JOIN Yield y ON y.SubmissionID = s.SubmissionID
INNER JOIN MaterialPrices p ON p.RefineryId = s.RefineryID AND p.MaterialID = y.MaterialID
WHERE s.SubmissionID = @SubmissionID AND y.MaterialId NOT IN ('CRD','RFC','RFS','RFCG','M106','AKC','ADD')

-- Load Crude
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
SELECT t.SubmissionID, p.Scenario, 101, 'CRD', 'RMI', ISNULL(p.AvgCost*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM Submissions WHERE SubmissionID = @SubmissionID), 0), ISNULL(p.AvgCost, 0), t.TotBbl
FROM CrudeTot t INNER JOIN CrudeTotPrice p ON p.SubmissionID = t.SubmissionID
WHERE t.SubmissionID = @SubmissionID AND p.Scenario <> 'CLIENT'

-- Load Refinery-produced fuels
DECLARE @Scenarios TABLE
	(Scenario varchar(8))
INSERT @Scenarios
SELECT DISTINCT Scenario FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID AND Scenario <> 'CLIENT'

IF EXISTS (SELECT * FROM @Scenarios)
BEGIN
	INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
	SELECT SubmissionID, s.Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl
	FROM MaterialCostCalc m, @Scenarios s
	WHERE m.SubmissionID = @SubmissionID AND m.Scenario = 'CLIENT' AND m.MaterialID IN ('RFC','RFS','RFCG','M106','AKC','ADD')
END

-- Load materials that do not have study pricing
IF EXISTS (SELECT * FROM @Scenarios)
BEGIN
	SELECT c.SubmissionID, s.Scenario, c.SortKey, c.MaterialID, c.Category, c.PriceLocal, c.PriceUS, c.Bbl
	INTO #Missing
	FROM MaterialCostCalc c, @Scenarios s
	WHERE c.SubmissionID = @SubmissionID AND c.Scenario = 'CLIENT' 
	AND NOT EXISTS (SELECT * FROM MaterialCostCalc x WHERE x.SubmissionID = c.SubmissionID AND x.Scenario = s.Scenario AND x.SortKey = c.SortKey)

	INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
	SELECT SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl
	FROM #Missing

	INSERT DataChecks (SubmissionID, DataCheckID, ItemSortKey, ItemDesc, Value1, Value2)
	SELECT y.SubmissionID, 'NoStudy$', y.SortKey, y.MaterialName, y.MaterialID, m.Scenario
	FROM #Missing m INNER JOIN Yield y ON y.SubmissionId = m.SubmissionID AND y.SortKey = m.SortKey
	WHERE y.MaterialID NOT IN ('ADD','AKC','GAIN') AND y.Bbl <> 0

	DROP TABLE #Missing
END

INSERT @Scenarios VALUES ('CLIENT')
DECLARE @RMC real, @GPV real, @TermRMMUS real, @TermProdMUS real, @POXO2MUS real
DECLARE @Scenario varchar(8)
DECLARE cScenarios CURSOR FAST_FORWARD
FOR	SELECT Scenario FROM @Scenarios
OPEN cScenarios
FETCH NEXT FROM cScenarios INTO @Scenario
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO MaterialNetCalc (SubmissionID, Scenario, Category, MaterialID, Bbl, MT, PricePerBbl)
	SELECT  m.SubmissionID, @Scenario, m.Category, m.MaterialID, m.Bbl, m.MT, p.PriceUS
	FROM MaterialNet m LEFT JOIN (SELECT MaterialID, Category, PriceUS = SUM(PriceUS*Bbl)/SUM(Bbl)
				FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID AND Bbl > 0 AND Scenario = @Scenario
				GROUP BY MaterialID, Category) p ON p.Category = m.Category AND p.MaterialID = m.MaterialID
	WHERE m.SubmissionID = @SubmissionID 
	
	-- Calculate the Gross Value in M US$ by category and store in MaterialSTCalc
	INSERT INTO MaterialSTCalc (SubmissionID, Scenario, Category, GrossBbl, GrossMT, NetBbl, NetMT, GrossValueMUS)
	SELECT st.SubmissionID, @Scenario, st.Category, st.GrossBbl, st.GrossMT, st.NetBbl, st.NetMT, p.ValueMUS
	FROM MaterialST st LEFT JOIN (SELECT Category, ValueMUS = SUM(BBL*PriceUS/1000000)
			FROM MaterialCostCalc c WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario
			GROUP BY Category) p ON p.Category = st.Category
	WHERE SubmissionID = @SubmissionID
	
	-- Calculate Net Values in M US$ for categories which are netted based on
	-- MaterialID (Other Raw Material Input, Miscellaneous Products,
	-- Asphalt, Coke) and record in MaterialSTCalc.
	UPDATE MaterialSTCalc
	SET NetValueMUS = (SELECT SUM(n.BBL*n.PricePerBbl)/1000000
		FROM MaterialNetCalc n
		WHERE n.Category = MaterialSTCalc.Category
		AND n.SubmissionID = MaterialSTCalc.SubmissionID
		AND n.Scenario = MaterialSTCalc.Scenario)
	WHERE Category IN ('OTHRM', 'MPROD', 'ASP', 'COKE')
	AND SubmissionID = @SubmissionID AND Scenario = @Scenario
	
	-- For categories that are not netted, set the net value = gross value
	-- (Primary Raw Material Inputs, Primary Products, Solvents)
	UPDATE MaterialSTCalc SET NetValueMUS = GrossValueMUS
	WHERE Category IN ('RMI', 'SOLV', 'PROD', 'RPF')
	AND SubmissionID = @SubmissionID AND Scenario = @Scenario
	
	-- Set Gross value to 0 if it is NULL for calculations later
	UPDATE MaterialSTCalc SET GrossValueMUS = 0
	WHERE SubmissionID = @SubmissionID AND GrossValueMUS IS NULL AND Scenario = @Scenario
	
	-- Calculate the Net Values for Feedstocks to affiliates by subtracting
	-- the gross value of returns from the affiliate from the gross value
	-- of the feedstocks to the affiliate.
	DECLARE @i tinyint, @RCat varchar(5), @FCat varchar(5)
	SELECT @i = 0
	WHILE @i<2
	BEGIN
		IF @i = 0
			-- Feedstocks to Chemical Plants
			SELECT @RCat = 'RCHEM', @FCat = 'FCHEM'
		ELSE
			-- Feedstocks to Lube Plants
			SELECT @RCat = 'RLUBE', @FCat = 'FLUBE'
		IF NOT EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @FCat)
		BEGIN
			IF EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @RCat)
				INSERT INTO MaterialSTCalc (SubmissionID, Scenario, Category, GrossBbl, NetBbl, GrossValueMUS)
				VALUES (@SubmissionID, @Scenario, @FCat, 0, 0, 0)
	 	END
		IF NOT EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @RCat)
		BEGIN
			IF EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @FCat)
				INSERT INTO MaterialSTCalc (SubmissionID, Scenario, Category, GrossBbl, NetBbl, GrossValueMUS)
				VALUES (@SubmissionID, @Scenario, @RCat, 0, 0, 0)
	 	END
	
		UPDATE MaterialSTCalc
		SET NetValueMUS = (SELECT F.GrossValueMUS-R.GrossValueMUS
			FROM MaterialSTCalc F, MaterialSTCalc R
			WHERE R.SubmissionID = F.SubmissionID AND R.Scenario = F.Scenario
			AND (F.Category = @FCat) AND (R.Category = @RCat)
			AND R.SubmissionID = MaterialSTCalc.SubmissionID
			AND R.Scenario = MaterialSTCalc.Scenario)
		WHERE Category = @FCat AND SubmissionID = @SubmissionID AND Scenario = @Scenario
		SELECT @i = @i+1
	END
	DECLARE @RawMatCost real, @ProdValue real
	SELECT 	@RawMatCost = SUM(CASE WHEN L.NetGroup = 'I' THEN T.NetValueMUS ELSE 0 END),
		@ProdValue = SUM(CASE WHEN L.NetGroup = 'Y' THEN T.NetValueMUS ELSE 0 END)
	FROM MaterialSTCalc T INNER JOIN MaterialCategory_LU L ON T.Category = L.Category
	WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario
	DECLARE @TermRM real, @TermProd real, @POXO2 real
	SELECT 	@TermRM = ISNULL(ThirdPartyTerminalRM, 0)/1000 * dbo.ExchangeRate(@RptCurrency, 'USD', @PeriodStart), 
		@TermProd = ISNULL(ThirdPartyTerminalProd, 0)/1000 * dbo.ExchangeRate(@RptCurrency, 'USD', @PeriodStart),  
		@POXO2 = ISNULL(POXO2 , 0)/1000 * dbo.ExchangeRate(@RptCurrency, 'USD', @PeriodStart)
	FROM OpexAll
	WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'
	
	SELECT	@RawMatCost = @RawMatCost + @TermRM + @POXO2,
		@ProdValue = @ProdValue - @TermProd
	
	INSERT MaterialTotCost (SubmissionID, Scenario, Currency, RawMatCost, ProdValue,
		ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2)
	SELECT @SubmissionID, @Scenario, 'USD', @RawMatCost, @ProdValue,
		@TermRM, @TermProd, @POXO2

	FETCH NEXT FROM cScenarios INTO @Scenario
END
CLOSE cScenarios
DEALLOCATE cScenarios

INSERT MaterialTotCost (SubmissionID, Scenario, Currency, RawMatCost, ProdValue,
	ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2)
SELECT m.SubmissionID, m.Scenario, c.Currency, 
m.RawMatCost*dbo.ExchangeRate(m.Currency, c.Currency, s.PeriodStart), 
m.ProdValue*dbo.ExchangeRate(m.Currency, c.Currency, s.PeriodStart), 
m.ThirdPartyTerminalRM*dbo.ExchangeRate(m.Currency, c.Currency, s.PeriodStart), 
m.ThirdPartyTerminalProd*dbo.ExchangeRate(m.Currency, c.Currency, s.PeriodStart),
m.POXO2*dbo.ExchangeRate(m.Currency, c.Currency, s.PeriodStart)
FROM MaterialTotCost m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID 
INNER JOIN CurrenciesToCalc c ON c.RefineryID = s.RefineryID
WHERE c.Currency <> 'USD' AND m.SubmissionID = @SubmissionID AND m.Currency = 'USD'
