﻿CREATE   PROC [dbo].[StartCalcs]
AS
SET ANSI_WARNINGS OFF
DECLARE @RefineryID varchar(6), @DataSet varchar(15)
SELECT @RefineryID = RefineryID, @DataSet = DataSet
FROM ReadyForCalcs
WHERE CalcsStarted = 0 AND Uploading = 0
AND EXISTS (SELECT * FROM Submissions 
	WHERE Submissions.RefineryID = ReadyForCalcs.RefineryID
	AND Submissions.DataSet = ReadyForCalcs.DataSet
	AND Submissions.CalcsNeeded IS NOT NULL)
ORDER BY LastUpdate
IF @RefineryID IS NULL 
	RETURN
UPDATE ReadyForCalcs
SET CalcsStarted = 1, LastUpdate = getdate()
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
AND (CalcsStarted = 0 AND Uploading = 0)
IF @@ROWCOUNT > 0 BEGIN
	DECLARE @SubmissionID int, @CalcsNeeded char(1)
	DECLARE cFull CURSOR FAST_FORWARD FOR
		SELECT SubmissionID, CalcsNeeded 
		FROM Submissions WHERE CalcsNeeded IN ('F', 'T')
		AND RefineryID = @RefineryID AND DataSet = @DataSet
		ORDER BY PeriodStart
	OPEN cFull
	FETCH NEXT FROM cFull INTO @SubmissionID, @CalcsNeeded
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'StartCalcs', @MessageText = 'Started Processing'
		IF @CalcsNeeded = 'F'
			EXEC spFullCalcs @SubmissionID
		IF @CalcsNeeded = 'T'
			EXEC spCalcIndicators @SubmissionID
		EXEC spDatachecks @SubmissionID
		EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'StartCalcs', @MessageText = 'Completed Processing'
		FETCH NEXT FROM cFull INTO @SubmissionID, @CalcsNeeded
	END
	CLOSE cFull
	DEALLOCATE cFull
	DECLARE cAvg CURSOR FAST_FORWARD FOR
		SELECT SubmissionID, CalcsNeeded 
		FROM Submissions WHERE CalcsNeeded IN ('A', 'M')
		AND RefineryID = @RefineryID AND DataSet = @DataSet
		ORDER BY PeriodStart
	OPEN cAvg
	FETCH NEXT FROM cAvg INTO @SubmissionID, @CalcsNeeded
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'StartCalcs', @MessageText = 'Calculating Averages'
		IF @CalcsNeeded = 'A'
			EXEC spAverages @SubmissionID
		IF @CalcsNeeded = 'M'
			EXEC spAverageMaint @SubmissionID
		EXEC spDatachecks @SubmissionID
		EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'StartCalcs', @MessageText = 'Completed Averages'
		FETCH NEXT FROM cAvg INTO @SubmissionID, @CalcsNeeded
	END
	CLOSE cAvg
	DEALLOCATE cAvg
	UPDATE ReadyForCalcs
	SET CalcsStarted = 0, LastUpdate = getdate()
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet
END
