﻿CREATE   PROC [dbo].[spReportThaiOilChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	ProcessUtilPcnt real NULL, 
	OpAvail real NULL, 
	EII real NULL, 
	VEI real NULL, 
	GainPcnt real NULL, 
	PersIndex real NULL, 
	TotMaintForceWHrEDC real NULL, 
	MaintIndex real NULL, 
	NEOpexEDC real NULL,
	OpexEDC real NULL,
	OpexUEDC real NULL,
	ProcessUtilPcnt_3Mo real NULL, 
	OpAvail_3Mo real NULL, 
	EII_3Mo real NULL, 
	VEI_3Mo real NULL, 
	GainPcnt_3Mo real NULL, 
	PersIndex_3Mo real NULL, 
	TotMaintForceWHrEDC_3Mo real NULL, 
	MaintIndex_3Mo real NULL, 
	NEOpexEDC_3Mo real NULL,
	OpexEDC_3Mo real NULL,
	OpexUEDC_3Mo real NULL,
	ProcessUtilPcnt_12Mo real NULL, 
	OpAvail_24Mo real NULL, 
	EII_12Mo real NULL, 
	VEI_12Mo real NULL, 
	GainPcnt_12Mo real NULL, 
	PersIndex_12Mo real NULL, 
	TotMaintForceWHrEDC_12Mo real NULL, 
	MaintIndex_24Mo real NULL, 
	NEOpexEDC_12Mo real NULL,
	OpexEDC_12Mo real NULL,
	OpexUEDC_12Mo real NULL	
)

--- Everything Already Available in Gensum (missing gain and Opex on an EDC basis)
INSERT INTO @data (PeriodStart, PeriodEnd, ProcessUtilPcnt, OpAvail, EII, VEI, GainPcnt, PersIndex, TotMaintForceWHrEDC, MaintIndex, NEOpexEDC, OpexUEDC, OpexEDC,
	ProcessUtilPcnt_12Mo, OpAvail_24Mo, EII_12Mo, VEI_12Mo, GainPcnt_12Mo, PersIndex_12Mo, TotMaintForceWHrEDC_12Mo, MaintIndex_24Mo, NEOpexEDC_12Mo, OpexUEDC_12Mo)
SELECT	PeriodStart, PeriodEnd, ProcessUtilPcnt, OpAvail, EII, VEI, -GainPcnt, PersIndex = TotWHrEDC, TotMaintForceWHrEDC, MaintIndex = RoutIndex + TAIndex_Avg, NEOpexEDC, TotCashOpexUEDC
	, OpexEDC = (SELECT TotCashOpex FROM OpexCalc o WHERE o.SubmissionID = Gensum.SubmissionID AND o.Currency = Gensum.Currency AND o.Scenario = 'CLIENT' AND o.FactorSet = Gensum.FactorSet AND o.DataType = 'EDC')
	, ProcessUtilPcnt_Avg, OpAvail_Avg, EII_Avg, VEI_Avg, -GainPcnt_Avg, TotWHrEDC_Avg, TotMaintForceWHrEDC_Avg, MaintIndex_Avg, NEOpexEDC_Avg, TotCashOpexUEDC_Avg
FROM Gensum
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario
AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @Start24Mo smalldatetime
DECLARE @ProcessUtilPcnt_3Mo real, @OpAvail_3Mo real, @EII_3Mo real, @VEI_3Mo real, @GainPcnt_3Mo real, @PersIndex_3Mo real, @TotMaintForceWHrEDC_3Mo real, @MaintIndex_3Mo real, @NEOpexEDC_3Mo real, @OpexUEDC_3Mo real, @OpexEDC_3Mo real, @OpexEDC_12Mo real

DECLARE cMonths CURSOR LOCAL FAST_FORWARD
FOR SELECT PeriodStart, PeriodEnd FROM @data
OPEN cMonths 
FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @Start24Mo = DATEADD(mm, -24, @PeriodEnd),
			@ProcessUtilPcnt_3Mo = NULL, @OpAvail_3Mo = NULL, @EII_3Mo = NULL, @VEI_3Mo = NULL, @GainPcnt_3Mo = NULL, @PersIndex_3Mo = NULL, 
			@TotMaintForceWHrEDC_3Mo = NULL, @MaintIndex_3Mo = NULL, @NEOpexEDC_3Mo = NULL, @OpexUEDC_3Mo = NULL, @OpexEDC_3Mo = NULL, @OpexEDC_12Mo = NULL
			
	EXEC spAverageFactors @RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, 
		@EII = @EII_3Mo OUTPUT, @VEI = @VEI_3Mo OUTPUT, @UtilPcnt = NULL, @UtilOSTA = NULL, @EDC = NULL, @UEDC = NULL, 
		@ProcessUtilPcnt = @ProcessUtilPcnt_3Mo OUTPUT, @TotProcessEDC = NULL, @TotProcessUEDC = NULL

	SELECT @GainPcnt_3Mo = -SUM(GainBbl)/SUM(NetInputBbl)*100
	FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND m.NetInputBbl > 0

	SELECT @TotMaintForceWHrEDC_3Mo = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
	, @PersIndex_3Mo = SUM(g.TotWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
	, @MaintIndex_3Mo = SUM((g.RoutIndex + g.TAIndex_AVG)*g.EDC)/SUM(g.EDC)
	FROM PersTotCalc p INNER JOIN Gensum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
	INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet AND g.UOM = @UOM
	AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

	SELECT @OpexUEDC_3Mo = SUM(TotCashOpex*Divisor)/SUM(Divisor)
	FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
	AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
	AND o.DataType = 'UEDC' AND o.Scenario = 'CLIENT' AND o.FactorSet = @FactorSet AND o.Currency = @Currency

	SELECT @OpexEDC_3Mo = SUM(TotCashOpex*Divisor)/SUM(Divisor)
	, @NEOpexEDC_3Mo = SUM(NEOpex*Divisor)/SUM(Divisor)
	FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
	AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
	AND o.DataType = 'EDC'

	SELECT @OpexEDC_12Mo = SUM(TotCashOpex*Divisor)/SUM(Divisor)
	FROM OpexCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
	AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND o.Currency = @Currency AND o.Scenario = 'CLIENT'
	AND o.DataType = 'EDC'

	SELECT @OpAvail_3Mo=SUM(OpAvail_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
	INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd
	AND m.FactorSet = @FactorSet

	UPDATE @data
	SET ProcessUtilPcnt_3Mo = @ProcessUtilPcnt_3Mo, 
		OpAvail_3Mo = @OpAvail_3Mo, 
		EII_3Mo = @EII_3Mo, 
		VEI_3Mo = @VEI_3Mo, 
		GainPcnt_3Mo = @GainPcnt_3Mo, 
		PersIndex_3Mo = @PersIndex_3Mo, 
		TotMaintForceWHrEDC_3Mo = @TotMaintForceWHrEDC_3Mo, 
		MaintIndex_3Mo = @MaintIndex_3Mo, 
		NEOpexEDC_3Mo = @NEOpexEDC_3Mo,
		OpexUEDC_3Mo = @OpexUEDC_3Mo,
		OpexEDC_3Mo = @OpexEDC_3Mo,
		OpexEDC_12Mo = @OpexEDC_12Mo	
	WHERE PeriodStart = @PeriodStart

	FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
END
CLOSE cMonths
DEALLOCATE cMonths

IF (SELECT COUNT(*) FROM @data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
			INSERT @data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, ProcessUtilPcnt, OpAvail, EII, VEI, /*GainPcnt,*/ OpexUEDC, PersIndex, TotMaintForceWHrEDC, MaintIndex, NEOpexEDC, OpexEDC
	, ProcessUtilPcnt_3Mo, OpAvail_3Mo, EII_3Mo, VEI_3Mo, /*GainPcnt_3Mo,*/ OpexUEDC_3Mo, PersIndex_3Mo, TotMaintForceWHrEDC_3Mo, MaintIndex_3Mo, NEOpexEDC_3Mo, OpexEDC_3Mo
	, ProcessUtilPcnt_12Mo, OpAvail_24Mo, EII_12Mo, VEI_12Mo, /*GainPcnt_12Mo,*/ OpexUEDC_12Mo, PersIndex_12Mo, TotMaintForceWHrEDC_12Mo, MaintIndex_24Mo, NEOpexEDC_12Mo, OpexEDC_12Mo
FROM @Data
ORDER BY PeriodStart ASC
