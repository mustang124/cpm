﻿CREATE TABLE [dbo].[Crude] (
    [SubmissionID] INT              NOT NULL,
    [CrudeID]      [dbo].[CrudeID]  NOT NULL,
    [CNum]         [dbo].[CrudeNum] NOT NULL,
    [CrudeName]    NVARCHAR (50)    NOT NULL,
    [BBL]          FLOAT            NOT NULL,
    [Gravity]      REAL             NULL,
    [Sulfur]       REAL             NULL,
    [CostPerBBL]   REAL             NOT NULL,
    [Density]      REAL             NULL,
    [MT]           FLOAT            NULL
);

