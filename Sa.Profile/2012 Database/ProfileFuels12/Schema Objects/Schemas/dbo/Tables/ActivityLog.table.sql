﻿CREATE TABLE [dbo].[ActivityLog] (
    [ActivityTime] DATETIME2 (7) NOT NULL,
    [RefineryID]   VARCHAR (6)   NOT NULL,
    [CallerIP]     VARCHAR (20)  NOT NULL,
    [Activity]     VARCHAR (MAX) NULL,
    [Service]      VARCHAR (50)  NULL,
    [Method]       VARCHAR (50)  NULL,
    [Parameters]   VARCHAR (400) NULL,
    [Status]       VARCHAR (50)  NULL
);

