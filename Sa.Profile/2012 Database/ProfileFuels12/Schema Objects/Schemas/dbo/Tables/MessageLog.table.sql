﻿CREATE TABLE [dbo].[MessageLog] (
    [SubmissionID] INT           NOT NULL,
    [Source]       VARCHAR (30)  NOT NULL,
    [Severity]     CHAR (1)      NOT NULL,
    [MessageText]  VARCHAR (255) NOT NULL,
    [MessageTime]  DATETIME      NOT NULL
);

