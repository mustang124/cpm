﻿ALTER TABLE [dbo].[Gasoline]
    ADD CONSTRAINT [PK_Gasoline_1__16] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [BlendID] ASC) WITH (FILLFACTOR = 70, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

