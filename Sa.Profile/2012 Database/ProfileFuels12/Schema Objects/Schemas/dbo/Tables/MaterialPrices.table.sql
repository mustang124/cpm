﻿CREATE TABLE [dbo].[MaterialPrices] (
    [RefineryID]  CHAR (6)           NOT NULL,
    [StudyYear]   [dbo].[StudyYear]  NOT NULL,
    [MaterialID]  [dbo].[MaterialID] NOT NULL,
    [PricePerBbl] REAL               NOT NULL
);

