﻿CREATE TABLE [dbo].[UnitTargets_LU] (
    [ProcessID]      VARCHAR (10)  NOT NULL,
    [Property]       VARCHAR (50)  NOT NULL,
    [SortKey]        SMALLINT      NOT NULL,
    [USDescription]  VARCHAR (100) NOT NULL,
    [USDecPlaces]    TINYINT       NOT NULL,
    [MetDescription] VARCHAR (100) NOT NULL,
    [MetDecPlaces]   TINYINT       NOT NULL,
    [USUOM]          VARCHAR (20)  NULL,
    [MetricUOM]      VARCHAR (20)  NULL,
    [CustomGroup]    INT           NOT NULL
);

