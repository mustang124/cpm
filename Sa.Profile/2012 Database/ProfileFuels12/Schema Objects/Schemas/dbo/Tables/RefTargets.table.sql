﻿CREATE TABLE [dbo].[RefTargets] (
    [SubmissionID] INT                  NOT NULL,
    [Property]     VARCHAR (50)         NOT NULL,
    [Target]       REAL                 NULL,
    [CurrencyCode] [dbo].[CurrencyCode] NULL
);

