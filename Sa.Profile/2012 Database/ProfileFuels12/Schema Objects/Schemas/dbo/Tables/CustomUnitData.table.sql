﻿CREATE TABLE [dbo].[CustomUnitData] (
    [SubmissionID]   INT                  NOT NULL,
    [UnitID]         [dbo].[UnitID]       NOT NULL,
    [Property]       VARCHAR (30)         NOT NULL,
    [FactorSet]      [dbo].[FactorSet]    NOT NULL,
    [Currency]       [dbo].[CurrencyCode] NOT NULL,
    [USValue]        REAL                 NULL,
    [MetValue]       REAL                 NULL,
    [USTarget]       REAL                 NULL,
    [MetTarget]      REAL                 NULL,
    [SortKey]        SMALLINT             NULL,
    [USDescription]  VARCHAR (50)         NULL,
    [MetDescription] VARCHAR (50)         NULL
);

