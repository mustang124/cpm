﻿CREATE TABLE [dbo].[EnergyTot] (
    [SubmissionID]      INT   NOT NULL,
    [PurFGMBTU]         REAL  NULL,
    [PurLiquidMBTU]     REAL  NULL,
    [PurSolidMBTU]      REAL  NULL,
    [PurSteamMBTU]      REAL  NULL,
    [PurThermMBTU]      FLOAT NULL,
    [PurPowerMBTU]      FLOAT NULL,
    [PurTotMBTU]        REAL  NULL,
    [ProdFGMBTU]        FLOAT NULL,
    [ProdOthMBTU]       FLOAT NULL,
    [ProdPowerAdj]      FLOAT NULL,
    [ProdTotMBTU]       REAL  NULL,
    [TotEnergyConsMBTU] FLOAT NULL,
    [PurPowerConsMWH]   FLOAT NULL,
    [ProdPowerConsMWH]  FLOAT NULL,
    [TotPowerConsMWH]   REAL  NULL
);

