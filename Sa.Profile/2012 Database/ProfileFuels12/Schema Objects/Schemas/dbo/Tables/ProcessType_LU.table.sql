﻿CREATE TABLE [dbo].[ProcessType_LU] (
    [ProcessID]   [dbo].[ProcessID]   NOT NULL,
    [ProcessType] [dbo].[ProcessType] NOT NULL,
    [Description] VARCHAR (255)       NULL
);

