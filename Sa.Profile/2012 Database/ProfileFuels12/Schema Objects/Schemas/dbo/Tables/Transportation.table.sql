﻿CREATE TABLE [dbo].[Transportation] (
    [StudyYear]   SMALLINT      NOT NULL,
    [CrudeOrigin] SMALLINT      NOT NULL,
    [CrudeDest]   SMALLINT      NOT NULL,
    [AdjType]     CHAR (2)      NOT NULL,
    [AdjValue]    REAL          NULL,
    [SaveDate]    SMALLDATETIME NOT NULL,
    [AdjValueMT]  REAL          NULL
);

