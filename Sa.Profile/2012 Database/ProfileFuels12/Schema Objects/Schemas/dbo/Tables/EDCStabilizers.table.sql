﻿CREATE TABLE [dbo].[EDCStabilizers] (
    [SubmissionID]    INT   NOT NULL,
    [AnnInputBbl]     FLOAT NULL,
    [AnnCokeBbl]      FLOAT NULL,
    [AnnElecConsMWH]  FLOAT NULL,
    [AnnRSCRUDE_RAIL] FLOAT NULL,
    [AnnRSCRUDE_TT]   FLOAT NULL,
    [AnnRSCRUDE_TB]   FLOAT NULL,
    [AnnRSCRUDE_OMB]  FLOAT NULL,
    [AnnRSCRUDE_BB]   FLOAT NULL,
    [AnnRSPROD_RAIL]  FLOAT NULL,
    [AnnRSPROD_TT]    FLOAT NULL,
    [AnnRSPROD_TB]    FLOAT NULL,
    [AnnRSPROD_OMB]   FLOAT NULL,
    [AnnRSPROD_BB]    FLOAT NULL
);

