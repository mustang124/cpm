﻿CREATE TABLE [dbo].[Energy_LU] (
    [EnergySection]  VARCHAR (5)             NOT NULL,
    [Header]         VARCHAR (50)            NOT NULL,
    [TransType]      [dbo].[EnergyTransType] NOT NULL,
    [TransTypeDesc]  VARCHAR (50)            NOT NULL,
    [TransferTo]     VARCHAR (3)             NULL,
    [EnergyType]     [dbo].[EnergyType]      NOT NULL,
    [EnergyTypeDesc] VARCHAR (50)            NOT NULL,
    [SortKey]        DECIMAL (6, 2)          NULL,
    [Composition]    TINYINT                 NOT NULL
);

