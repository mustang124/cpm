﻿CREATE TABLE [dbo].[Absence_LU] (
    [CategoryID]      CHAR (10)    NOT NULL,
    [Description]     VARCHAR (50) NULL,
    [SortKey]         INT          NULL,
    [IncludeForInput] [dbo].[YorN] NULL,
    [ParentID]        CHAR (6)     NULL,
    [DetailStudy]     CHAR (5)     NULL,
    [DetailProfile]   CHAR (5)     NULL,
    [Indent]          TINYINT      NULL
);

