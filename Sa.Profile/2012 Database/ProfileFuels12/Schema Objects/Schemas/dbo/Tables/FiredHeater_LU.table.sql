﻿CREATE TABLE [dbo].[FiredHeater_LU] (
    [HeaterNo]     INT            IDENTITY (1, 1) NOT NULL,
    [RefineryID]   CHAR (6)       NOT NULL,
    [ClientID]     NVARCHAR (50)  NOT NULL,
    [HeaterName]   NVARCHAR (50)  NOT NULL,
    [UnitID]       [dbo].[UnitID] NOT NULL,
    [Service]      VARCHAR (6)    NULL,
    [ProcessFluid] VARCHAR (5)    NULL,
    [Accepted]     BIT            DEFAULT ((0)) NOT NULL
);

