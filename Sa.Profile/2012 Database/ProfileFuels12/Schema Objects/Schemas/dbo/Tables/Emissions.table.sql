﻿CREATE TABLE [dbo].[Emissions] (
    [SubmissionID]     INT            NOT NULL,
    [EmissionType]     CHAR (5)       NOT NULL,
    [SiteEmissions]    REAL           NULL,
    [RefPcnt]          NUMERIC (5, 2) NULL,
    [IndirectIncluded] [dbo].[YorN]   NULL,
    [RefEmissions]     REAL           NULL,
    [GovtRpt]          [dbo].[YorN]   NULL
);

