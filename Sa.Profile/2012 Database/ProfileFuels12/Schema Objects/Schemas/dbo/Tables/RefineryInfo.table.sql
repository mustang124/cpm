﻿CREATE TABLE [dbo].[RefineryInfo] (
    [RefineryID] CHAR (6)      NOT NULL,
    [Company]    VARCHAR (30)  NOT NULL,
    [Location]   VARCHAR (30)  NOT NULL,
    [CoordName]  VARCHAR (50)  NULL,
    [CoordTitle] VARCHAR (50)  NULL,
    [CoordAddr1] VARCHAR (50)  NULL,
    [CoordAddr2] VARCHAR (50)  NULL,
    [CoordAddr3] VARCHAR (50)  NULL,
    [CoordPhone] VARCHAR (40)  NULL,
    [CoordFax]   VARCHAR (40)  NULL,
    [CoordEMail] VARCHAR (255) NULL
);

