﻿CREATE TABLE [dbo].[MaterialTot] (
    [SubmissionID] INT   NOT NULL,
    [PVP]          FLOAT NULL,
    [PVPPcnt]      REAL  NULL,
    [TotInputBbl]  FLOAT NULL,
    [TotInputMT]   FLOAT NULL,
    [NetInputBbl]  FLOAT NULL,
    [NetInputMT]   FLOAT NULL,
    [GainBbl]      REAL  NULL
);

