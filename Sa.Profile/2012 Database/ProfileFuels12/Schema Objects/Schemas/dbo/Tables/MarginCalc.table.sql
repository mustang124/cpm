﻿CREATE TABLE [dbo].[MarginCalc] (
    [SubmissionID]  INT                  NOT NULL,
    [Scenario]      [dbo].[Scenario]     NOT NULL,
    [Currency]      [dbo].[CurrencyCode] NOT NULL,
    [DataType]      CHAR (6)             NOT NULL,
    [GPV]           REAL                 NULL,
    [RMC]           REAL                 NULL,
    [GrossMargin]   REAL                 NULL,
    [OthRev]        REAL                 NULL,
    [CashOpEx]      REAL                 NULL,
    [CashMargin]    REAL                 NULL,
    [InvestCptl4Yr] REAL                 NULL,
    [CorpContrib]   REAL                 NULL,
    [Divisor]       REAL                 NOT NULL
);

