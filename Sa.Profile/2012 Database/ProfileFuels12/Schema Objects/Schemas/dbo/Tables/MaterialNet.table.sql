﻿CREATE TABLE [dbo].[MaterialNet] (
    [SubmissionID] INT                   NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [MaterialID]   [dbo].[MaterialID]    NOT NULL,
    [BBL]          FLOAT                 NULL,
    [MT]           FLOAT                 NULL
);

