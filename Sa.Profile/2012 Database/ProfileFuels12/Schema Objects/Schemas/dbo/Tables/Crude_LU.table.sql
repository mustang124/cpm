﻿CREATE TABLE [dbo].[Crude_LU] (
    [CNum]          [dbo].[CrudeNum] NOT NULL,
    [CrudeName]     VARCHAR (50)     NULL,
    [TypicalAPI]    REAL             NULL,
    [TypicalSulfur] REAL             NULL,
    [ProdCountry]   VARCHAR (25)     NULL,
    [ProdState]     VARCHAR (25)     NULL,
    [SortKey]       SMALLINT         NULL,
    [Display]       [dbo].[YorN]     NULL
);

