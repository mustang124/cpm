﻿CREATE TABLE [dbo].[ReadyForCalcs] (
    [RefineryID]   CHAR (6)      NOT NULL,
    [DataSet]      VARCHAR (15)  NOT NULL,
    [CalcsStarted] BIT           NOT NULL,
    [Uploading]    BIT           NOT NULL,
    [LastUpdate]   SMALLDATETIME NULL
);

