﻿CREATE TABLE [dbo].[MarineBunkers] (
    [SubmissionID] INT                NOT NULL,
    [BlendID]      INT                NOT NULL,
    [Grade]        [dbo].[MaterialID] NULL,
    [Gravity]      REAL               NULL,
    [Density]      REAL               NULL,
    [Sulfur]       REAL               NULL,
    [PourPt]       REAL               NULL,
    [ViscCS]       REAL               NULL,
    [ViscCSAtTemp] REAL               NULL,
    [ViscTemp]     REAL               NULL,
    [CrackedStock] REAL               NULL,
    [Vanadium]     REAL               NULL,
    [KBbl]         REAL               NULL,
    [KMT]          REAL               NULL
);

