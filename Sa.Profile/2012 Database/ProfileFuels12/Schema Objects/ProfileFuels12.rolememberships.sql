﻿EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'Saroy';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'RRH';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CRT';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'MGV';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'EJB';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'JDW';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'SWP';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'FRD';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'HJohnson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'ProfileFuelsAdmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'MGV';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'MRH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'EJB';


GO
EXECUTE sp_addrolemember @rolename = N'Datagrp', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'Datagrp', @membername = N'DMR';


GO
EXECUTE sp_addrolemember @rolename = N'Datagrp', @membername = N'JLS';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'Saroy';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'DMR';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'JLS';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'JBF';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'MGV';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'ProfileFuelsAdmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'TWS';

