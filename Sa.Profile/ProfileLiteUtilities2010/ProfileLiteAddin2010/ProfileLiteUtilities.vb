﻿Imports System.Data
Imports System.Runtime.InteropServices
Imports Excel = Microsoft.Office.Interop.Excel
Imports ExcelTools = Microsoft.Office.Tools.Excel
Imports System.Configuration

Imports System.Security.Cryptography.X509Certificates
Imports ProLiteSecurity

<ComVisible(True)> _
Public Interface IProfileLiteUtilities
    Sub ClearAllData()
    Sub AddSettings(ByVal Company As String, ByVal Location As String, _
                        ByVal CoordName As String, ByVal CoordTitle As String, _
                        ByVal CoordPhone As String, ByVal CoordEmail As String, _
                        ByVal RptCurrency As String, ByVal RptCurrencyT15 As String, _
                        ByVal UOM As String, ByVal FuelsLubesCombo As Boolean, _
                        ByVal PeriodMonth As Integer, ByVal PeriodYear As Integer, _
                        ByVal Dataset As String, ByVal BridgeVersion As String, _
                        ByVal ClientVersion As String)
    Sub AddConfig(ByVal UnitID As Integer, ByVal UnitName As String, ByVal ProcessID As String, ByVal ProcessType As String, _
                         ByVal RptCap As Single, ByVal UtilPcnt As Single, ByVal RptStmCap As Single, ByVal StmUtilPcnt As Single, _
                         Optional ByVal InServicePcnt As Decimal = 100, Optional ByVal YearsOper As Decimal = 0, Optional ByVal MHPerWeek As Single = 0, _
                  Optional ByVal PostPerShift As Decimal = 0, Optional ByVal BlockOp As String = "", Optional ByVal ControlType As String = "", _
                  Optional ByVal CapClass As Byte = Nothing, Optional ByVal UtilCap As Single = 0, Optional ByVal StmUtilCap As Single = 0, _
                  Optional ByVal AllocPcntOfTime As Decimal = 100, Optional ByVal AllocPcntOfCap As Decimal = 100, Optional ByVal AllocUtilPcnt As Decimal = 0)
    Sub AddConfigRS(ByVal ProcessID As String, ByVal RailcarBBL As Single, ByVal TankTruckBBL As Single, ByVal TankerBerthBBL As String, ByVal OffshoreBuoy As Single, ByVal BargeBerthBBL As Single)
    Sub AddInventory(ByVal TankType As String, ByVal SortKey As Byte, ByVal FuelsStorage As Double, ByVal NumTanks As Int16, ByVal AvgLevel As Single, _
                     Optional ByVal Inven As Single = 0, Optional ByVal TotStorage As Double = 0)
    Sub AddProcessData(ByVal UnitID As Integer, ByVal pProperty As String, ByVal RptValue As Single)
    'Sub AddOpexAll(ByVal OCCSal As Single, Optional ByVal ContMaintLabor As Single = 0, Optional ByVal OthCont As Single = 0, Optional ByVal GAPers As Single = 0)
    Sub AddOpexAll(ByVal OCCSal As Single, Optional ContMaintLabor As Single = 0, Optional ByVal OthCont As Single = 0, _
               Optional ByVal GAPers As Single = 0, Optional ByVal STNonVol As Single = 0, _
               Optional ByVal OthNonVol As Single = 0, Optional ByVal ContMaintLaborST As Single = 0)
    Sub AddOpexCategory(ByVal OpexCategory As String, ByVal CostLocal As Single)
    Sub AddPersAll(ByVal PersID As String, ByVal STH As Single, ByVal Contract As Single, ByVal GA As Single)
    Sub AddPers(ByVal PersID As String, ByVal STH As Single, ByVal OVTHours As Single, ByVal OVTPcnt As Single, ByVal Contract As Single, ByVal GA As Single, ByVal AbsHrs As Single, ByVal MaintPcnt As Single)
    Sub AddAbsence(ByVal CategoryID As String, ByVal SortKey As Int16, ByVal OCC As Single, ByVal MPS As Single)
    Sub AddMaintTA(ByVal UnitID As Integer, ByVal TAID As Integer, _
                        ByVal ProcessID As String, ByVal TADate As Date, _
                        ByVal TAHrsDown As Single, ByVal TACostLocal As Single, _
                        ByVal TAOCCSTH As Single, ByVal TAContOCC As Single, _
                        ByVal PrevTADate As Date, ByVal TAException As Integer, Optional ByVal TALaborCostLocal As Single = 0)
    Sub AddMaintRout(ByVal UnitID As Integer, ByVal ProcessID As String, _
                    ByVal RoutCostLocal As Single, ByVal MaintDown As Single)
    Sub AddMaintRoutFull(ByVal UnitID As Integer, ByVal ProcessID As String, _
                    ByVal RoutCostLocal As Single, ByVal RoutMatlLocal As Single, ByVal RoutExpLocal As Single, ByVal RoutCptlLocal As Single, ByVal RoutOvhdLocal As Single, _
                    ByVal RegNum As Int16, ByVal RegDown As Single, ByVal RegSlow As Single, _
                    ByVal MaintNum As Int16, ByVal MaintDown As Single, ByVal MaintSlow As Single, _
                    ByVal OthNum As Int16, ByVal OthDown As Single, ByVal OthSlow As Single)
    Sub AddCrude(ByVal CNum As String, ByVal BBL As Single, Optional ByVal CrudeName As String = "Crude", Optional ByVal PricePerBbl As Single = 0, Optional ByVal Gravity As Single = 0, Optional ByVal Sulfur As Single = 0, Optional ByVal MT As Double = 0)
    Sub AddYield_RM(ByVal Category As String, ByVal MaterialID As String, ByVal BBL As Single, Optional ByVal MaterialName As String = "Other Raw Materials", Optional ByVal PriceLocal As Single = 0, Optional ByVal Density As Single = 0, Optional ByVal MT As Double = 0)
    Sub AddYield_Prod(ByVal Category As String, ByVal MaterialID As String, ByVal BBL As Single, Optional ByVal MaterialName As String = "Products", Optional ByVal PriceLocal As Single = 0, Optional ByVal Density As Single = 0, Optional ByVal MT As Double = 0)
    Sub AddEnergy(ByVal TransType As String, ByVal Transferto As String, _
                    ByVal EnergyType As String, ByVal RptSource As Single, _
                    ByVal RptPriceLocal As Single)
    Sub AddElectric(ByVal TransType As String, ByVal Transferto As String, _
                ByVal EnergyType As String, ByVal RptGenEff As Single, _
                ByVal RptMWH As Single, ByVal PriceLocal As Single)
    Sub SubmitData(ByVal key As String)
    Sub DownloadResults(ByVal key As String, ByVal DataSet As String, ByVal PeriodYear As Integer, ByVal PeriodMonth As Integer, ByVal FactorSet As String, ByVal Currency As String, ByVal UOM As String, ByVal proc As String, ByVal WriteMethod As String, ByVal WriteRange As String, ByVal ws As Excel.Worksheet)

    Sub AddUserDefined(ByVal HeaderText As String, ByVal VariableDesc As String, _
        ByVal RptValue As Double, ByVal DecPlaces As Byte, ByVal RptValue_Target As Double, _
        ByVal RptValue_Avg As Double, ByVal RptValue_YTD As Double)
    Function TestAddIn() As String
    Function TestWebService(Scenario As String, RefineryID As String, CallerIP As String, UserID As String, ComputerName As String, Service As String, Method As String, EntityName As String, PeriodStart As String, PeriodEnd As String, Notes As String, Status As String) As String
End Interface

<ComVisible(True)> _
<ClassInterface(ClassInterfaceType.None)> _
Public Class ProfileLiteUtilities
    Implements IProfileLiteUtilities, IDisposable
    '    <ObsoleteAttribute("ExcelLocale1033ProxyMethodObsolete")> _
    '    Public Shared Function Unwrap(ByVal wrappedExcelObject As Object) As Object
    Private ds As New dsUpload
    Dim tracingInfo As New System.Text.StringBuilder()


    Sub ClearAllData() Implements IProfileLiteUtilities.ClearAllData
        For Each tbl As DataTable In ds.Tables
            tbl.Clear()
        Next
    End Sub

    Public Sub AddSettings(ByVal Company As String, ByVal Location As String, _
                        ByVal CoordName As String, ByVal CoordTitle As String, _
                        ByVal CoordPhone As String, ByVal CoordEmail As String, _
                        ByVal RptCurrency As String, ByVal RptCurrencyT15 As String, _
                        ByVal UOM As String, ByVal FuelsLubesCombo As Boolean, _
                        ByVal PeriodMonth As Integer, ByVal PeriodYear As Integer, _
                        ByVal Dataset As String, ByVal BridgeVersion As String, _
                        ByVal ClientVersion As String) Implements IProfileLiteUtilities.AddSettings
        tracingInfo.AppendLine("Calling AddSettings with " + Company + "," + Location + "," + CoordName + "," + CoordTitle + "," + CoordPhone + "," + CoordEmail + "," + RptCurrency + "," + RptCurrencyT15 + "," + UOM + "," + FuelsLubesCombo.ToString() + "," + PeriodMonth.ToString() + "," + PeriodYear.ToString() + "," + Dataset + "," + BridgeVersion + "," + ClientVersion)
        ds.Settings.AddSettingsRow(Company, Location, CoordName, CoordTitle, CoordPhone, _
            CoordEmail, RptCurrency, RptCurrencyT15, UOM, FuelsLubesCombo, PeriodMonth, _
            PeriodYear, Dataset, BridgeVersion, ClientVersion)
        tracingInfo.AppendLine("Successfully called AddSettings")
    End Sub

    Public Sub AddConfig(ByVal UnitID As Integer, ByVal UnitName As String, ByVal ProcessID As String, ByVal ProcessType As String, _
                         ByVal RptCap As Single, ByVal UtilPcnt As Single, ByVal RptStmCap As Single, ByVal StmUtilPcnt As Single, _
                         Optional ByVal InServicePcnt As Decimal = 100, Optional ByVal YearsOper As Decimal = 0, Optional ByVal MHPerWeek As Single = 0, _
                  Optional ByVal PostPerShift As Decimal = 0, Optional ByVal BlockOp As String = "", Optional ByVal ControlType As String = "", _
                  Optional ByVal CapClass As Byte = Nothing, Optional ByVal UtilCap As Single = 0, Optional ByVal StmUtilCap As Single = 0, _
                  Optional ByVal AllocPcntOfTime As Decimal = 100, Optional ByVal AllocPcntOfCap As Decimal = 100, Optional ByVal AllocUtilPcnt As Decimal = 0) Implements IProfileLiteUtilities.AddConfig
        tracingInfo.AppendLine("Calling AddConfig with " + UnitID.ToString() + "," + UnitName + "," + ProcessID + "," + ProcessType + "," + RptCap.ToString() + "," + UtilPcnt.ToString() + "," + RptStmCap.ToString() + "," + StmUtilPcnt.ToString() + "," + InServicePcnt.ToString() + "," + YearsOper.ToString() + "," + MHPerWeek.ToString() + "," + PostPerShift.ToString() + "," + BlockOp + "," + ControlType + "," + CapClass.ToString() + "," + UtilCap.ToString() + "," + StmUtilCap.ToString() + "," + AllocPcntOfTime.ToString() + "," + AllocPcntOfCap.ToString() + "," + AllocUtilPcnt.ToString())
        ds.Config.AddConfigRow(UnitID, ProcessID, UnitName, ProcessType, RptCap, UtilPcnt, RptStmCap, StmUtilPcnt, InServicePcnt, YearsOper, MHPerWeek, _
                               PostPerShift, BlockOp, ControlType, CapClass, UtilCap, StmUtilCap, AllocPcntOfTime, AllocPcntOfCap, AllocUtilPcnt)
        tracingInfo.AppendLine("Successfully called AddConfig")
    End Sub

    Public Sub AddConfigRS(ByVal ProcessID As String, ByVal RailcarBBL As Single, ByVal TankTruckBBL As Single, ByVal TankerBerthBBL As String, ByVal OffshoreBuoy As Single, ByVal BargeBerthBBL As Single) Implements IProfileLiteUtilities.AddConfigRS
        tracingInfo.AppendLine("Calling AddConfig with " + ProcessID + "," + RailcarBBL.ToString() + "," + TankTruckBBL.ToString() + "," + TankerBerthBBL + "," + OffshoreBuoy.ToString() + "," + BargeBerthBBL.ToString())
        ds.ConfigRS.AddConfigRSRow(ProcessID, RailcarBBL, TankTruckBBL, TankerBerthBBL, OffshoreBuoy, BargeBerthBBL)
        tracingInfo.AppendLine("Successfully called AddConfigRS")
    End Sub

    Public Sub AddInventory(ByVal TankType As String, ByVal SortKey As Byte, ByVal FuelsStorage As Double, ByVal NumTanks As Int16, ByVal AvgLevel As Single, Optional ByVal Inven As Single = 0, Optional ByVal TotStorage As Double = 0) Implements IProfileLiteUtilities.AddInventory
        tracingInfo.AppendLine("Calling AddInventory with " + TankType + "," + SortKey.ToString() + "," + FuelsStorage.ToString() + "," + NumTanks.ToString() + "," + AvgLevel.ToString() + "," + Inven.ToString() + "," + TotStorage.ToString())
        ds.Inventory.AddInventoryRow(TankType, SortKey, FuelsStorage, NumTanks, AvgLevel, Inven, TotStorage)
        tracingInfo.AppendLine("Successfully called AddInventory")
    End Sub

    Public Sub AddProcessData(ByVal UnitID As Integer, ByVal pProperty As String, ByVal RptValue As Single) Implements IProfileLiteUtilities.AddProcessData
        tracingInfo.AppendLine("Calling AddProcessData with " + UnitID.ToString() + "," + pProperty + "," + RptValue.ToString())
        ds.ProcessData.AddProcessDataRow(UnitID, pProperty, RptValue)
        tracingInfo.AppendLine("Successfully called AddProcessData")
    End Sub

    Public Sub AddOpexAll(ByVal OCCSal As Single, Optional ContMaintLabor As Single = 0, _
                          Optional ByVal OthCont As Single = 0, Optional ByVal GAPers As Single = 0, _
                          Optional ByVal STNonVol As Single = 0, Optional ByVal OthNonVol As Single = 0, _
                          Optional ByVal ContMaintLaborST As Single = 0) Implements IProfileLiteUtilities.AddOpexAll
        tracingInfo.AppendLine("Calling AddOpexAll with " + OCCSal.ToString() + "," + ContMaintLabor.ToString() + "," + OthCont.ToString() + "," + GAPers.ToString() + "," + STNonVol.ToString() + "," + OthNonVol.ToString() + "," + ContMaintLaborST.ToString())
        If OCCSal > 0 Then
            ds.OpexAll.AddOpexAllRow("OCCSal", OCCSal)
        End If
        If ContMaintLabor > 0 Then
            ds.OpexAll.AddOpexAllRow("ContMaintLabor", ContMaintLabor)
        End If
        If OthCont > 0 Then
            ds.OpexAll.AddOpexAllRow("OthCont", OthCont)
        End If
        If GAPers > 0 Then
            ds.OpexAll.AddOpexAllRow("GAPers", GAPers)
        End If
        If STNonVol > 0 Then
            ds.OpexAll.AddOpexAllRow("STNonVol", STNonVol)
        End If
        If OthNonVol > 0 Then
            ds.OpexAll.AddOpexAllRow("OthNonVol", OthNonVol)
        End If
        If ContMaintLaborST > 0 Then
            ds.OpexAll.AddOpexAllRow("ContMaintLaborST", ContMaintLaborST)
        End If
        tracingInfo.AppendLine("Successfully called AddOpexAll")
    End Sub

    Public Sub AddOpexCategory(ByVal OpexCategory As String, ByVal CostLocal As Single) Implements IProfileLiteUtilities.AddOpexCategory
        tracingInfo.AppendLine("Calling AddOpexCategory with " + OpexCategory + "," + CostLocal.ToString())
        ds.OpexAll.AddOpexAllRow(OpexCategory, CostLocal)
        tracingInfo.AppendLine("Successfully called AddOpexCategory")
    End Sub

    Public Sub AddPersAll(ByVal PersID As String, ByVal STH As Single, ByVal Contract As Single, ByVal GA As Single) Implements IProfileLiteUtilities.AddPersAll
        tracingInfo.AppendLine("Calling AddPersAll with " + PersID.ToString() + "," + STH.ToString() + "," + Contract.ToString() + "," + GA.ToString())
        ds.Pers.AddPersRow(PersID, STH, 0, 0, Contract, GA, 0, 0)
        tracingInfo.AppendLine("Successfully called AddPersAll")
    End Sub

    Sub AddPers(ByVal PersID As String, ByVal STH As Single, ByVal OVTHours As Single, ByVal OVTPcnt As Single, ByVal Contract As Single, ByVal GA As Single, ByVal AbsHrs As Single, ByVal MaintPcnt As Single) Implements IProfileLiteUtilities.AddPers
        tracingInfo.AppendLine("Calling AddPers with " + PersID + "," + STH.ToString() + "," + OVTHours.ToString() + "," + OVTPcnt.ToString() + "," + Contract.ToString() + "," + GA.ToString() + "," + AbsHrs.ToString() + "," + MaintPcnt.ToString())
        ds.Pers.AddPersRow(PersID, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, MaintPcnt)
        tracingInfo.AppendLine("Successfully called AddPers")
    End Sub

    Sub AddAbsence(ByVal CategoryID As String, ByVal SortKey As Int16, ByVal OCC As Single, ByVal MPS As Single) Implements IProfileLiteUtilities.AddAbsence
        tracingInfo.AppendLine("Calling AddAbsence with " + CategoryID + "," + SortKey.ToString() + "," + OCC.ToString() + "," + MPS.ToString())
        ds.Absence.AddAbsenceRow(CategoryID, SortKey, OCC, MPS)
        tracingInfo.AppendLine("Successfully called AddAbsence")
    End Sub

    Public Sub AddMaintTA(ByVal UnitID As Integer, ByVal TAID As Integer, _
                        ByVal ProcessID As String, ByVal TADate As Date, _
                        ByVal TAHrsDown As Single, ByVal TACostLocal As Single, _
                        ByVal TAOCCSTH As Single, ByVal TAContOCC As Single, _
                        ByVal PrevTADate As Date, ByVal TAException As Integer, _
                        Optional ByVal TALaborCostLocal As Single = 0) Implements IProfileLiteUtilities.AddMaintTA
        tracingInfo.AppendLine("Calling AddMaintTA with " + UnitID.ToString() + "," + TAID.ToString() + "," + ProcessID + "," + TADate.ToString() + "," + TAHrsDown.ToString() + "," + TACostLocal.ToString() + "," + TAOCCSTH.ToString() + "," + TAContOCC.ToString() + "," + PrevTADate.ToString() + "," + TAException.ToString() + "," + TALaborCostLocal.ToString())
        If TAHrsDown > 0 Then
            ds.MaintTA.AddMaintTARow(UnitID, TAID, ProcessID, TADate, TAHrsDown, _
                TACostLocal, TAOCCSTH, TAContOCC, PrevTADate, TAException, TALaborCostLocal)
        Else
            Dim taRow As dsUpload.MaintTARow = ds.MaintTA.NewMaintTARow
            taRow.UnitID = UnitID
            taRow.TAID = TAID
            taRow.ProcessID = ProcessID
            taRow.TAExceptions = TAException
            taRow.TALaborCostLocal = TALaborCostLocal
            ds.MaintTA.AddMaintTARow(taRow)
        End If
        tracingInfo.AppendLine("Successfully called AddMaintTA")
    End Sub

    Public Sub AddMaintRout(ByVal UnitID As Integer, ByVal ProcessID As String, _
                        ByVal RoutCostLocal As Single, ByVal MaintDown As Single) Implements IProfileLiteUtilities.AddMaintRout
        tracingInfo.AppendLine("Calling AddMaintRout with " + UnitID.ToString() & "," & ProcessID.ToString() & "," & RoutCostLocal.ToString() & "," & MaintDown.ToString())
        Dim maintRow As dsUpload.MaintRoutRow = ds.MaintRout.NewMaintRoutRow
        maintRow.UnitID = UnitID
        maintRow.ProcessID = ProcessID
        maintRow.RoutCostLocal = RoutCostLocal
        maintRow.MaintDown = MaintDown
        ds.MaintRout.AddMaintRoutRow(maintRow)
        tracingInfo.AppendLine("Successfully called AddMaintRout")
    End Sub

    Public Sub AddMaintRoutFull(ByVal UnitID As Integer, ByVal ProcessID As String, _
                    ByVal RoutCostLocal As Single, ByVal RoutMatlLocal As Single, ByVal RoutExpLocal As Single, ByVal RoutCptlLocal As Single, ByVal RoutOvhdLocal As Single, _
                    ByVal RegNum As Int16, ByVal RegDown As Single, ByVal RegSlow As Single, _
                    ByVal MaintNum As Int16, ByVal MaintDown As Single, ByVal MaintSlow As Single, _
                    ByVal OthNum As Int16, ByVal OthDown As Single, ByVal OthSlow As Single) Implements IProfileLiteUtilities.AddMaintRoutFull
        tracingInfo.AppendLine("Calling AddMaintRoutFull with " + UnitID.ToString() & "," & ProcessID.ToString() & "," & RoutCostLocal.ToString() & "," & RoutMatlLocal.ToString() & "," & RoutExpLocal.ToString() & "," & RoutCptlLocal.ToString() & "," & RoutOvhdLocal.ToString() & "," & RegNum.ToString() & "," & RegDown.ToString() & "," & RegSlow.ToString() & "," & MaintNum.ToString() & "," & MaintDown.ToString() & "," & MaintSlow.ToString() & "," & OthNum.ToString() & "," & OthDown.ToString() & "," & OthSlow.ToString())
        ds.MaintRout.AddMaintRoutRow(UnitID, ProcessID, RoutCostLocal, RoutMatlLocal, RoutExpLocal, RoutCptlLocal, RoutOvhdLocal, RegNum, RegDown, RegSlow, MaintNum, MaintDown, MaintSlow, OthNum, OthDown, OthSlow)
        tracingInfo.AppendLine("Successfully called AddMaintRoutFull")
    End Sub

    Public Sub AddCrude(ByVal CNum As String, ByVal BBL As Single, Optional ByVal CrudeName As String = "Crude", Optional ByVal PricePerBbl As Single = 0, Optional ByVal Gravity As Single = 0, Optional ByVal Sulfur As Single = 0, Optional ByVal MT As Double = 0) Implements IProfileLiteUtilities.AddCrude
        tracingInfo.AppendLine("Calling AddCrude with " + CNum & "," & BBL.ToString() & "," & CrudeName.ToString() & "," & PricePerBbl.ToString() & "," & Gravity.ToString() & "," & Sulfur.ToString() & "," & MT.ToString())
        ds.Crude.AddCrudeRow(CNum, BBL, CrudeName, PricePerBbl, Gravity, Sulfur, MT)
        tracingInfo.AppendLine("Successfully called AddCrude")
    End Sub

    Public Sub AddYield_RM(ByVal Category As String, ByVal MaterialID As String, ByVal BBL As Single, Optional ByVal MaterialName As String = "Other Raw Materials", Optional ByVal PriceLocal As Single = 0, Optional ByVal Density As Single = 0, Optional ByVal MT As Double = 0) Implements IProfileLiteUtilities.AddYield_RM
        tracingInfo.AppendLine("Calling AddYield_RM with " + Category & "," & MaterialID & "," & BBL.ToString() & "," & MaterialName.ToString() & "," & PriceLocal.ToString() & "," & Density.ToString() & "," & MT.ToString())
        ds.Yield_RM.AddYield_RMRow(Category, MaterialID, BBL, MaterialName, PriceLocal, Density, MT)
        tracingInfo.AppendLine("Successfully called AddYield_RM")
    End Sub

    Public Sub AddYield_Prod(ByVal Category As String, ByVal MaterialID As String, ByVal BBL As Single, Optional ByVal MaterialName As String = "Products", Optional ByVal PriceLocal As Single = 0, Optional ByVal Density As Single = 0, Optional ByVal MT As Double = 0) Implements IProfileLiteUtilities.AddYield_Prod
        tracingInfo.AppendLine("Calling AddYield_Prod with " + Category & "," & MaterialID & "," & BBL.ToString() & "," & MaterialName.ToString() & "," & PriceLocal.ToString() & "," & Density.ToString() & "," & MT.ToString())
        ds.Yield_Prod.AddYield_ProdRow(Category, MaterialID, BBL, MaterialName, PriceLocal, Density, MT)
        tracingInfo.AppendLine("Successfully called AddYield_Prod")
    End Sub

    Public Sub AddUserDefined(ByVal HeaderText As String, ByVal VariableDesc As String, _
        ByVal RptValue As Double, ByVal DecPlaces As Byte, ByVal RptValue_Target As Double, _
        ByVal RptValue_Avg As Double, ByVal RptValue_YTD As Double) _
        Implements IProfileLiteUtilities.AddUserDefined
        tracingInfo.AppendLine("Calling AddUserDefined with " + HeaderText & "," & VariableDesc & "," & RptValue.ToString() & "," & DecPlaces.ToString() & "," & RptValue_Target.ToString() & "," & RptValue_Avg.ToString() & "," & RptValue_YTD.ToString())
        ds.UserDefined.AddUserDefinedRow(HeaderText, VariableDesc, _
         RptValue, DecPlaces, RptValue_Target, _
         RptValue_Avg, RptValue_YTD)
        tracingInfo.AppendLine("Successfully called AddUserDefined")
    End Sub

    Public Sub AddEnergy(ByVal TransType As String, ByVal Transferto As String, _
                    ByVal EnergyType As String, ByVal RptSource As Single, _
                    ByVal RptPriceLocal As Single) Implements IProfileLiteUtilities.AddEnergy
        tracingInfo.AppendLine("Calling AddEnergy with " + TransType & "," & Transferto & "," & EnergyType & "," & RptSource.ToString() & "," & RptPriceLocal.ToString())
        ds.Energy.AddEnergyRow(TransType, Transferto, EnergyType, RptSource, RptPriceLocal)
        tracingInfo.AppendLine("Successfully called AddEnergy")
    End Sub

    Public Sub AddElectric(ByVal TransType As String, ByVal Transferto As String, _
                ByVal EnergyType As String, ByVal RptGenEff As Single, _
                ByVal RptMWH As Single, ByVal PriceLocal As Single) Implements IProfileLiteUtilities.AddElectric
        tracingInfo.AppendLine("Calling AddElectric with " + TransType & "," & Transferto & "," & EnergyType & "," & RptGenEff.ToString() & "," & RptMWH.ToString() & "," & PriceLocal.ToString())
        ds.Electric.AddElectricRow(TransType, Transferto, EnergyType, RptGenEff, RptMWH, PriceLocal)
        tracingInfo.AppendLine("Successfully called AddElectric")
    End Sub

    Private Function GetEncodedSignedCms(ByRef uploadws As ProfileLiteWS.ProfileLiteWebServices, _
                                                      ByVal key As String, ByRef encodedSignedCms() As Byte) As Boolean
        Try
            tracingInfo.AppendLine("Entered AuthenticateByCertificateAndClientId()")
            Dim clientKey As New ProLiteSecurity.Client.ClientKey()
            tracingInfo.AppendLine("Going to call GetClientKeyInfoFromToken")
            clientKey = clientKey.GetClientKeyInfoFromToken(key)
            Dim errors As String = String.Empty
            Dim name As String = clientKey.Company
            Dim refineryId As String = clientKey.RefNum
            Dim search As String = name + " (" + refineryId + ")"
            tracingInfo.AppendLine("Creating new  ProLiteSecurity.Cryptography.Certificate")
            Dim cert As New ProLiteSecurity.Cryptography.Certificate()

            tracingInfo.AppendLine("Going to create X509Certificate2 certificate")
            Dim privateX509Cert As New X509Certificate2()
            tracingInfo.AppendLine("Going to search for certificate by " + search)
            Dim success As Boolean = cert.Get(search, privateX509Cert) 'bool success = cert.Get(name, out privateX509Cert);
            If Not success Then
                tracingInfo.AppendLine("Returning False, cert.Get was False")
                Return False
            End If

            tracingInfo.AppendLine("Creating NonRepudiation object")
            Dim nonRep As New ProLiteSecurity.Cryptography.NonRepudiation()
            Dim errorMsg As String = String.Empty
            Dim messageBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(refineryId)
            tracingInfo.AppendLine("Going to call Sign()")
            If Not nonRep.Sign(errorMsg, messageBytes, privateX509Cert, encodedSignedCms, True) Then 'nonRep.Sign(ref errorMsg, messageBytes, privateX509Cert, out encodedSignedCms, true);
                tracingInfo.AppendLine("Call to Sign() returned false")
                Return False
            End If
            tracingInfo.AppendLine("Returning True and encodedSignedCms from GetEncodedSignedCms()")
            Return True
        Catch ex As Exception
            tracingInfo.AppendLine("Error in AuthenticateByCertificateAndClientId(): " + ex.Message)
            If (ex.InnerException IsNot Nothing) Then tracingInfo.AppendLine(ex.InnerException.ToString())
            Return False
        End Try
    End Function


    Public Sub SubmitData(ByVal key As String) Implements IProfileLiteUtilities.SubmitData
        Dim userName As String = My.User.Name
        Dim uploadws As New ProfileLiteWS.ProfileLiteWebServices
        uploadws.Url = ConfigurationManager.AppSettings("WebServiceUrl").ToString() 'this setting needs to be in unit test project for unit testing to work.
        Try
            Dim errorResponse As String = String.Empty
            Dim encodedSignedCms() As Byte = Nothing
            If Not GetEncodedSignedCms(uploadws, key, encodedSignedCms) Then
                'Removing at Richard's suggestion: uploadws.SendTracing(userName, tracingInfo.ToString())
                Throw New Exception("There was a problem getting your certificate.")
            End If
            tracingInfo.AppendLine("Going to call uploadws.AuthenticateByCertificateAndClientId()")
            If Not uploadws.AuthenticateByCertificateAndClientId(errorResponse, encodedSignedCms, key, False) Then
                tracingInfo.AppendLine("uploadws.AuthenticateByCertificateAndClientId() returned False. Error:" + errorResponse)
                'Removing at Richard's suggestion: uploadws.SendTracing(userName, tracingInfo.ToString())
                Throw New Exception("There was a problem validating your certificate.")
            End If
            tracingInfo.AppendLine("uploadws.AuthenticateByCertificateAndClientId() returned True")
            tracingInfo.AppendLine("Going to call uploadws.SubmitData() with key " + key)
            uploadws.SubmitData(ds, key)
            'Shouldn't need to call uploadws.SendTracing() if this is success.
        Catch e As Exception
            tracingInfo.AppendLine("Error in SubmitData:")
            tracingInfo.AppendLine(e.Message)
            If (e.InnerException IsNot Nothing) Then tracingInfo.AppendLine(e.InnerException.ToString())
            'Removing at Richard's suggestion: uploadws.SendTracing(userName, tracingInfo.ToString())
            MsgBox(e.Message)
        End Try
        tracingInfo.Clear()
    End Sub

    Public Sub DownloadResults(ByVal key As String, ByVal DataSet As String, ByVal PeriodYear As Integer, ByVal PeriodMonth As Integer, ByVal FactorSet As String, ByVal Currency As String, ByVal UOM As String, ByVal proc As String, ByVal WriteMethod As String, ByVal WriteRange As String, ByVal ws As Excel.Worksheet) Implements IProfileLiteUtilities.DownloadResults
        Dim userName As String = My.User.Name
        Dim uploadws As New ProfileLiteWS.ProfileLiteWebServices
        uploadws.Url = ConfigurationManager.AppSettings("WebServiceUrl").ToString()

        Dim errorResponse As String = String.Empty
        Dim encodedSignedCms() As Byte = Nothing
        If Not GetEncodedSignedCms(uploadws, key, encodedSignedCms) Then
            'Removing at Richard's suggestion: uploadws.SendTracing(userName, tracingInfo.ToString())
            Throw New Exception("There was a problem getting your certificate.")
        End If
        If Not uploadws.AuthenticateByCertificateAndClientId(errorResponse, encodedSignedCms, key, False) Then
            'Removing at Richard's suggestion: uploadws.SendTracing(userName, tracingInfo.ToString())
            Throw New Exception("There was a problem validating your certificate.")
        End If

        Dim dsResults As DataSet

        '       Try
        dsResults = uploadws.DataDumpLite(proc, DataSet, "Client", Currency, PeriodYear, PeriodMonth, CStr(FactorSet), UOM, True, True, True, key)
        Dim currentCulture As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture

        If WriteMethod = "Dump" Then
            Dim r As Long, dr As DataRow, rngRow As Excel.Range
            Dim rng As Excel.Range = ws.Range(WriteRange)
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            rng.ClearContents()
            r = 1
            For Each dr In dsResults.Tables(0).Rows
                rngRow = rng.Rows(r)
                rngRow.Value2 = dr.ItemArray
                r = r + 1
            Next dr
            System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture
        ElseIf WriteMethod = "Field" Then
            Dim fld As DataColumn, data As DataRow
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            data = dsResults.Tables(0).Rows(0)
            For Each fld In dsResults.Tables(0).Columns
                Try
                    ws.Range(fld.ColumnName).Value2 = data(fld.ColumnName)
                Catch ex As Exception
                    'Removing at Richard's suggestion: uploadws.SendTracing(userName, tracingInfo.ToString())
                    'MsgBox(data(fld.ColumnName), MsgBoxStyle.OkOnly, fld.ColumnName)
                End Try
            Next
            System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture
        Else
            'Removing at Richard's suggestion: uploadws.SendTracing(userName, tracingInfo.ToString())
            Err.Raise(vbObjectError + 5, "Download", "Invalid write method specified")
        End If
        System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture
        dsResults = Nothing
        '        Catch e As Exception
        '            e.
        '        End Try
        'uploadws.SendTracing(userName, "Successfully called DownloadResults")
    End Sub



    Private Function ReturnEntireDataset() As DataSet
        'for unit testing only
        Return ds
    End Function

    Public Function TestAddIn() As String Implements IProfileLiteUtilities.TestAddIn
        Return "TestAddIn = Success!"
    End Function

    Public Function TestWebService(Scenario As String, RefineryID As String, CallerIP As String, _
                                   UserID As String, ComputerName As String, Service As String, _
                                   Method As String, EntityName As String, PeriodStart As String, _
                                   PeriodEnd As String, Notes As String, Status As String) As String _
                               Implements IProfileLiteUtilities.TestWebService
        Dim uploadws As New ProfileLiteWS.ProfileLiteWebServices
        Try
            uploadws.Url = ConfigurationManager.AppSettings("WebServiceUrl").ToString()
            Return uploadws.ReadActivityLog(Scenario, RefineryID, CallerIP, UserID, ComputerName, Service, Method, EntityName, PeriodStart, PeriodEnd, Notes, Status)
        Catch ex As Exception
            If Not IsNothing(ex.InnerException) AndAlso Not IsNothing(ex.InnerException.Message) AndAlso ex.InnerException.Message.Length > 0 Then
                Return ex.InnerException.ToString()
            Else
                Return ex.Message
            End If
        End Try
    End Function

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ds.Dispose()
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Public Sub New()
        '        EmbeddedAssembly.EnsureInitialized()
    End Sub
End Class
