﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProLiteSecurity.Cryptography;

namespace ProLiteSecurity.Client
{
    [Serializable]
    public class ClientKey
    {
        public string Company { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public string RefNum{ get; set; }

        public ClientKey GetClientKeyInfoFromToken(string token)
        {
            Cryptographer cryptographer = new Cryptographer();
            ClientKey clientKey = new ClientKey();
          
            string inputToken = cryptographer.Decrypt(token);

            if (inputToken.Length > 0)
            {
                string[] tokenProperties = inputToken.Split('$');


            switch (tokenProperties.Length)
	            {
                case 4:
                    
                    clientKey.Company = tokenProperties[0];
                    clientKey.Password = tokenProperties[1];
                    clientKey.Location = tokenProperties[2];
                    clientKey.RefNum = tokenProperties[3];

                    break;
                case 3:

                    clientKey.Company = tokenProperties[0];
                    clientKey.Location = tokenProperties[1];
                    clientKey.RefNum = tokenProperties[2];

                    break;
		         
                default:
                    
                    throw new Exception("Invalid array length in token");
                    
	            }

               
            }


            return clientKey;
          
        }
    }


}
