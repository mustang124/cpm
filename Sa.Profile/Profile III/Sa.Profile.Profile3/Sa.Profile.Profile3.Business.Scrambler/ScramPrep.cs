﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sa.Profile.Profile3.Services.Library;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using Sa.Profile.Profile3.Business.Scrambler.ScramDataSet;
using Sa.Profile.Profile3.Business.PlantFactory;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using Sa.Profile.Profile3.Business.PlantUnits;

namespace Sa.Profile.Profile3.Business.Scrambler
{
    public class ScramPrep
    {
        public FuelsRefineryCompletePlant ClientToBus(NewDataSet dataSetIn)
        {
            string level = "Absence";
            FuelsRefineryCompletePlant retPlant = new FuelsRefineryCompletePlant(new FuelsRefineryPlantUnitFactory(PlantLevel.COMPLETE));
            try
            {
                NewDataSet ds = dataSetIn;
                DataTableExtentions de = new DataTableExtentions();
                if (ds.Absence != null)
                {
                    List<Absence> itms = (from p in de.AsEnumerable2<Absence>(ds.Absence) select p).ToList();
                    foreach (Absence itm in itms)
                    {
                        FuelsRefineryPersonnelAbsenceHoursComplete _FuelsRefineryPersonnelAbsenceHoursComplete = new FuelsRefineryPersonnelAbsenceHoursComplete(); 
                        _FuelsRefineryPersonnelAbsenceHoursComplete.CategoryID = itm.CategoryID; // Absence
                        _FuelsRefineryPersonnelAbsenceHoursComplete.ManagementProfessionalAndStaffAbsence = (decimal?)itm.MPSAbs; // Absence
                        _FuelsRefineryPersonnelAbsenceHoursComplete.OperatorCraftAndClericalAbsence = (decimal?)itm.OCCAbs; // Absence
                        _FuelsRefineryPersonnelAbsenceHoursComplete.SubmissionID = itm.SubmissionID; // Absence
                        retPlant.PersonnelAbsenceHoursCompleteList.Add(_FuelsRefineryPersonnelAbsenceHoursComplete);
                    }
                }
                if (ds.Config != null)
                {
                    level = "Config";
                    List<Config> itms = (from p in de.AsEnumerable2<Config>(ds.Config) select p).ToList();
                    foreach (Config itm in itms)
                    {
                        FuelsRefineryConfigurationComplete _FuelsRefineryConfigurationComplete = new FuelsRefineryConfigurationComplete(); //Config
                        _FuelsRefineryConfigurationComplete.AllocatedPercentOfCapacity = (decimal?)itm.AllocPcntOfCap; // Config
                        _FuelsRefineryConfigurationComplete.BlockOperated = itm.BlockOp; // Config
                        _FuelsRefineryConfigurationComplete.Capacity = (decimal?)itm.Cap; // Config
                        _FuelsRefineryConfigurationComplete.EnergyPercent = (decimal?)itm.EnergyPcnt; // Config
                        _FuelsRefineryConfigurationComplete.InServicePercent = (decimal?)itm.InServicePcnt; // Config
                        _FuelsRefineryConfigurationComplete.ManHourWeek = (decimal?)itm.MHPerWeek; // Config
                        _FuelsRefineryConfigurationComplete.PostPerShift = (decimal?)itm.PostPerShift; // Config
                        _FuelsRefineryConfigurationComplete.ProcessID = itm.ProcessID; // Config
                        _FuelsRefineryConfigurationComplete.ProcessType = itm.ProcessType; // Config
                        _FuelsRefineryConfigurationComplete.SteamCapacity = (decimal?)itm.StmCap; // Config
                        _FuelsRefineryConfigurationComplete.SteamUtilizationPercent = (decimal?)itm.StmUtilPcnt; // Config
                        _FuelsRefineryConfigurationComplete.SubmissionID = itm.SubmissionID; // Config
                        _FuelsRefineryConfigurationComplete.UnitID = itm.UnitID; // Config
                        _FuelsRefineryConfigurationComplete.UnitName = itm.UnitName; // Config
                        _FuelsRefineryConfigurationComplete.UtilizationPercent = (decimal?)itm.UtilPcnt; // Config
                        _FuelsRefineryConfigurationComplete.YearsInOperation = (decimal?)itm.YearsOper; // Config
                        retPlant.ConfigurationCompleteList.Add(_FuelsRefineryConfigurationComplete);
                    }
                }
                if (ds.ConfigBuoy != null)
                {
                    level="ConfigBuoy";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ConfigBuoy> itms = (from p in de.AsEnumerable2<ConfigBuoy>(ds.ConfigBuoy) select p).ToList();
                    foreach (ConfigBuoy itm in itms)
                    {
                        FuelsRefineryConfigurationBuoyComplete _FuelsRefineryConfigurationBuoyComplete = new FuelsRefineryConfigurationBuoyComplete(); //ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.LineSize = (decimal?)itm.LineSize; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.PercentOwnership = (decimal?)itm.PcntOwnership; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.ProcessID = itm.ProcessID; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.ShipCapacity = (decimal?)itm.ShipCap; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.SubmissionID = itm.SubmissionID; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.UnitID = itm.UnitID; // ConfigBuoy
                        _FuelsRefineryConfigurationBuoyComplete.UnitName = itm.UnitName; // ConfigBuoy
                        retPlant.ConfigurationBuoyCompleteList.Add(_FuelsRefineryConfigurationBuoyComplete);
                    }
                }
                if (ds.Crude != null)
                {level="Crude";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude>(ds.Crude) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude itm in itms)
                    {
                        FuelsRefineryCrudeComplete _FuelsRefineryCrudeComplete = new FuelsRefineryCrudeComplete(); //Crude
                        _FuelsRefineryCrudeComplete.Barrels = (decimal?)itm.BBL; // Crude
                        _FuelsRefineryCrudeComplete.CrudeID = itm.CrudeID; // Crude
                        _FuelsRefineryCrudeComplete.CrudeName = itm.CrudeName; // Crude
                        _FuelsRefineryCrudeComplete.CrudeNumber = itm.CNum; // Crude
                        _FuelsRefineryCrudeComplete.Gravity = (decimal?)itm.Gravity; // Crude
                        _FuelsRefineryCrudeComplete.Period = itm.Period; // Crude
                        _FuelsRefineryCrudeComplete.SubmissionID = itm.SubmissionID; // Crude
                        _FuelsRefineryCrudeComplete.Sulfur = (decimal?)itm.Sulfur; // Crude
                        retPlant.CrudeCompleteList.Add(_FuelsRefineryCrudeComplete);
                    }
                }
                if (ds.ConfigRS != null)
                {level="ConfigRS";
                    List<ConfigR> itms = (from p in de.AsEnumerable2<ConfigR>(ds.ConfigRS) select p).ToList();
                    foreach (ConfigR itm in itms)
                    {
                        FuelsRefineryConfigurationReceiptAndShipmentComplete _FuelsRefineryConfigurationReceiptAndShipmentComplete = new FuelsRefineryConfigurationReceiptAndShipmentComplete(); //ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.AverageSize = (decimal?)itm.AvgSize; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessID = itm.ProcessID; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessType = itm.ProcessType; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.SubmissionID = itm.SubmissionID; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.Throughput = (decimal?)itm.Throughput; // ConfigRS
                        _FuelsRefineryConfigurationReceiptAndShipmentComplete.UnitID = itm.UnitID; // ConfigRS
                        retPlant.ConfigurationReceiptAndShipmentCompleteList.Add(_FuelsRefineryConfigurationReceiptAndShipmentComplete);

                    }
                }
                if (ds.Diesel != null)
                {
                    level = "Diesel";
                    List<Diesel> itms = (from p in de.AsEnumerable2<Diesel>(ds.Diesel) select p).ToList();
                    foreach (Diesel itm in itms)
                    {
                        FuelsRefineryDieselComplete _FuelsRefineryDieselComplete = new FuelsRefineryDieselComplete(); //Diesel
                        _FuelsRefineryDieselComplete.AmericanStandardForTestingMaterials90DistillationPoint = (decimal?)itm.ASTM90; // Diesel
                        _FuelsRefineryDieselComplete.BiodieselInBlendPercent = (decimal?)itm.BiodieselPcnt; // Diesel
                        _FuelsRefineryDieselComplete.BlendID = (int?)itm.BlendID; // Diesel
                        _FuelsRefineryDieselComplete.Cetane = (decimal?)itm.Cetane; // Diesel
                        _FuelsRefineryDieselComplete.CloudPointOfBlend = (decimal?)itm.CloudPt; // Diesel
                        _FuelsRefineryDieselComplete.Density = (decimal?)itm.Density; // Diesel
                        _FuelsRefineryDieselComplete.Grade = itm.Grade; // Diesel
                        _FuelsRefineryDieselComplete.Market = itm.Market; // Diesel
                        _FuelsRefineryDieselComplete.PercentEvaporatedAt350C = (decimal?)itm.E350; // Diesel
                        _FuelsRefineryDieselComplete.PourPointOfBlend = (decimal?)itm.PourPt; // Diesel
                        _FuelsRefineryDieselComplete.SubmissionID = itm.SubmissionID; // Diesel
                        _FuelsRefineryDieselComplete.Sulfur = (decimal?)itm.Sulfur; // Diesel
                        _FuelsRefineryDieselComplete.ThousandMetricTons = (decimal?)itm.KMT; // Diesel
                        _FuelsRefineryDieselComplete.Type = itm.Type; // Diesel
                        retPlant.DieselCompleteList.Add(_FuelsRefineryDieselComplete);
                    }
                }
                if (ds.Emissions != null)
                {
                    level = "Emissions";
                    List<Emission> itms = (from p in de.AsEnumerable2<Emission>(ds.Emissions) select p).ToList();
                    foreach (Emission itm in itms)
                    {
                        FuelsRefineryEmissionComplete _FuelsRefineryEmissionComplete = new FuelsRefineryEmissionComplete(); //Emissions
                        _FuelsRefineryEmissionComplete.EmissionType = itm.EmissionType; // Emissions
                        _FuelsRefineryEmissionComplete.RefineryEmission = (decimal?)itm.RefEmissions; // Emissions
                        _FuelsRefineryEmissionComplete.SubmissionID = itm.SubmissionID; // Emissions
                        retPlant.EmissionCompleteList.Add(_FuelsRefineryEmissionComplete);
                    }
                }
                if (ds.Energy != null)
                {
                    int posElect = 1;
                    int posEnerg = 1;
                    level = "Electric";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy>(ds.Energy) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy itm in itms)
                    {
                        if (itm.EnergyType == "ELE" ||
                           itm.EnergyType == "POW" ||
                           itm.EnergyType == "COG")
                        {
                            FuelsRefineryEnergyElectricBasic _FuelsRefineryEnergyElectricBasic = new FuelsRefineryEnergyElectricBasic(); //Electric

                            _FuelsRefineryEnergyElectricBasic.Amount = (decimal?)itm.Amount; // Electric
                            _FuelsRefineryEnergyElectricBasic.EnergyType = itm.EnergyType; // Electric
                            _FuelsRefineryEnergyElectricBasic.GenerationEfficiency = (decimal?)itm.GenEff; // Electric
                            _FuelsRefineryEnergyElectricBasic.PriceLocal = (decimal?)itm.PriceLocal; // Electric
                            _FuelsRefineryEnergyElectricBasic.SubmissionID = itm.SubmissionID; // Electric
                            _FuelsRefineryEnergyElectricBasic.TransactionType = itm.TransType; // Electric
                            _FuelsRefineryEnergyElectricBasic.TransactionCode = posElect;
                            posElect++;
                            retPlant.ElectricBasicList.Add(_FuelsRefineryEnergyElectricBasic);                     
                        }
                        else//Energy
                        {
                            level = "Energy";
                            FuelsRefineryEnergyComplete _FuelsRefineryEnergyComplete = new FuelsRefineryEnergyComplete(); //Energy
                          //  _electric_2.GenerationEfficiency = (decimal?)itm.GenEff; // Energy
                            _FuelsRefineryEnergyComplete.Amount = (decimal?)itm.Amount; // Energy
                            _FuelsRefineryEnergyComplete.Butane = (decimal?)itm.Butane; // Energy
                            _FuelsRefineryEnergyComplete.Butylenes = (decimal?)itm.Butylenes; // Energy
                            _FuelsRefineryEnergyComplete.C5Plus = (decimal?)itm.C5Plus; // Energy
                            _FuelsRefineryEnergyComplete.CO = (decimal?)itm.CO; // Energy
                            _FuelsRefineryEnergyComplete.CO2 = (decimal?)itm.CO2; // Energy
                            _FuelsRefineryEnergyComplete.EnergyType = itm.EnergyType; // Energy
                            _FuelsRefineryEnergyComplete.Ethane = (decimal?)itm.Ethane; // Energy
                            _FuelsRefineryEnergyComplete.Ethylene = (decimal?)itm.Ethylene; // Energy
                            _FuelsRefineryEnergyComplete.H2S = (decimal?)itm.H2S; // Energy
                            _FuelsRefineryEnergyComplete.Hydrogen = (decimal?)itm.Hydrogen; // Energy
                            _FuelsRefineryEnergyComplete.Isobutane = (decimal?)itm.Isobutane; // Energy
                            _FuelsRefineryEnergyComplete.Methane = (decimal?)itm.Methane; // Energy
                            _FuelsRefineryEnergyComplete.MillionBritishThermalUnitsOut = (decimal?)itm.MBTUOut; // Energy
                            _FuelsRefineryEnergyComplete.N2 = (decimal?)itm.N2; // Energy
                            _FuelsRefineryEnergyComplete.NH3 = (decimal?)itm.NH3; // Energy
                            _FuelsRefineryEnergyComplete.OverrideCalculation = itm.OverrideCalcs; // Energy
                            _FuelsRefineryEnergyComplete.PriceLocal = (decimal?)itm.PriceLocal; // Energy
                            _FuelsRefineryEnergyComplete.Propane = (decimal?)itm.Propane; // Energy
                            _FuelsRefineryEnergyComplete.Propylene = (decimal?)itm.Propylene; // Energy
                            _FuelsRefineryEnergyComplete.SO2 = (decimal?)itm.SO2; // Energy
                            _FuelsRefineryEnergyComplete.SubmissionID = itm.SubmissionID; // Energy
                            _FuelsRefineryEnergyComplete.TransactionType = itm.TransType; // Energy
                            _FuelsRefineryEnergyComplete.TransactionCode = posEnerg;
                            posEnerg++;
                            retPlant.EnergyCompleteList.Add(_FuelsRefineryEnergyComplete);
                        }
                    }
                }
                if (ds.FiredHeaters != null)
                {
                    level = "FiredHeaters";
                    List<FiredHeater> itms = (from p in de.AsEnumerable2<FiredHeater>(ds.FiredHeaters) select p).ToList();
                    foreach (FiredHeater itm in itms)
                    {
                        FuelsRefineryFiredHeatersComplete _FuelsRefineryFiredHeatersComplete = new FuelsRefineryFiredHeatersComplete(); //FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.AbsorbedDuty = (decimal?)itm.AbsorbedDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.CombustionAirTemperature = (decimal?)itm.CombAirTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FiredDuty = (decimal?)itm.FiredDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FuelType = itm.FuelType; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FurnaceInTemperature = (decimal?)itm.FurnInTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.FurnaceOutTemperature = (decimal?)itm.FurnOutTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.HeaterName = itm.HeaterName; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.HeaterNumber = itm.HeaterNo; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.HeatLossPercent = (decimal?)itm.HeatLossPcnt; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.OtherCombustionDuty = (decimal?)itm.OthCombDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.OtherDuty = (decimal?)itm.OtherDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ProcessDuty = (decimal?)itm.ProcessDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ProcessFluid = itm.ProcessFluid; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ProcessID = itm.ProcessID; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.Service = itm.Service; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ShaftDuty = (decimal?)itm.ShaftDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.StackO2 = (decimal?)itm.StackO2; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.StackTemperature = (decimal?)itm.StackTemp; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.SteamDuty = (decimal?)itm.SteamDuty; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.SteamSuperHeated = itm.SteamSuperHeated; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.SubmissionID = itm.SubmissionID; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ThroughputReport = (decimal?)itm.ThroughputRpt; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.ThroughputUnitOfMeasure = itm.ThroughputUOM; // FiredHeaters
                        _FuelsRefineryFiredHeatersComplete.UnitID = itm.UnitID; // FiredHeaters
                        retPlant.FiredHeatersCompleteList.Add(_FuelsRefineryFiredHeatersComplete);
                    }
                }
                if (ds.Gasoline != null)
                {
                    level = "Gasoline";
                    List<Gasoline> itms = (from p in de.AsEnumerable2<Gasoline>(ds.Gasoline) select p).ToList();
                    foreach (Gasoline itm in itms)
                    {
                        FuelsRefineryGasolineComplete _FuelsRefineryGasolineComplete = new FuelsRefineryGasolineComplete(); //Gasoline
                        _FuelsRefineryGasolineComplete.BlendID = (int?)itm.BlendID; // Gasoline
                        _FuelsRefineryGasolineComplete.Density = (decimal?)itm.Density; // Gasoline
                        _FuelsRefineryGasolineComplete.Ethanol = (decimal?)itm.Ethanol; // Gasoline
                        _FuelsRefineryGasolineComplete.EthylTertButylEther = (decimal?)itm.ETBE; // Gasoline
                        _FuelsRefineryGasolineComplete.Grade = itm.Grade; // Gasoline
                        _FuelsRefineryGasolineComplete.Lead = (decimal?)itm.Lead; // Gasoline
                        _FuelsRefineryGasolineComplete.Market = itm.Market; // Gasoline
                        _FuelsRefineryGasolineComplete.MethylTertButylEther = (decimal?)itm.MTBE; // Gasoline
                        _FuelsRefineryGasolineComplete.MotorOctaneNumber = (decimal?)itm.MON; // Gasoline
                        _FuelsRefineryGasolineComplete.OtherOxygen = (decimal?)itm.OthOxygen; // Gasoline
                        _FuelsRefineryGasolineComplete.Oxygen = (decimal?)itm.Oxygen; // Gasoline
                        _FuelsRefineryGasolineComplete.ReidVaporPressure = (decimal?)itm.RVP; // Gasoline
                        _FuelsRefineryGasolineComplete.ResearchOctaneNumber = (decimal?)itm.RON; // Gasoline
                        _FuelsRefineryGasolineComplete.SubmissionID = itm.SubmissionID; // Gasoline
                        _FuelsRefineryGasolineComplete.Sulfur = (decimal?)itm.Sulfur; // Gasoline
                        _FuelsRefineryGasolineComplete.TertAmylMethylEther = (decimal?)itm.TAME; // Gasoline
                        _FuelsRefineryGasolineComplete.ThousandMetricTons = (decimal?)itm.KMT; // Gasoline
                        _FuelsRefineryGasolineComplete.Type = itm.Type; // Gasoline
                        retPlant.GasolineCompleteList.Add(_FuelsRefineryGasolineComplete);
                    }
                }

                if (ds.GeneralMisc != null)
                {
                    level = "GeneralMisc";
                    List<GeneralMisc> itms = (from p in de.AsEnumerable2<GeneralMisc>(ds.GeneralMisc) select p).ToList();
                    foreach (GeneralMisc itm in itms)
                    {
                        FuelsRefineryGeneralMiscellaneousComplete _FuelsRefineryGeneralMiscellaneousComplete = new FuelsRefineryGeneralMiscellaneousComplete(); //GeneralMisc
                        _FuelsRefineryGeneralMiscellaneousComplete.FlareLossMetricTons = (decimal?)itm.FlareLossMT; // GeneralMisc
                        _FuelsRefineryGeneralMiscellaneousComplete.SubmissionID = itm.SubmissionID; // GeneralMisc
                        _FuelsRefineryGeneralMiscellaneousComplete.TotalLossMetricTons = (decimal?)itm.TotLossMT; // GeneralMisc
                        retPlant.GeneralMiscellaneousCompleteList.Add(_FuelsRefineryGeneralMiscellaneousComplete);
                    }
                }
                if (ds.Inventory != null)
                {
                    level = "Inventory";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory>(ds.Inventory) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory itm in itms)
                    {
                        FuelsRefineryInventoryComplete _FuelsRefineryInventoryComplete = new FuelsRefineryInventoryComplete(); //Inventory
                        _FuelsRefineryInventoryComplete.AverageLevel = (decimal?)itm.AvgLevel; // Inventory
                        _FuelsRefineryInventoryComplete.LeasedPercent = (decimal?)itm.LeasedPcnt; // Inventory
                        _FuelsRefineryInventoryComplete.ManditoryStorageLevel = (decimal?)itm.MandStorage; // Inventory
                        _FuelsRefineryInventoryComplete.MarketingStorage = (decimal?)itm.MktgStorage; // Inventory
                        _FuelsRefineryInventoryComplete.RefineryStorage = (decimal?)itm.RefStorage; // Inventory
                        _FuelsRefineryInventoryComplete.SubmissionID = itm.SubmissionID; // Inventory
                        _FuelsRefineryInventoryComplete.TankNumber = (int?)itm.NumTank; // Inventory
                        _FuelsRefineryInventoryComplete.TankType = itm.TankType; // Inventory
                        _FuelsRefineryInventoryComplete.TotalStorage = (decimal?)itm.TotStorage; // Inventory
                        retPlant.InventoryCompleteList.Add(_FuelsRefineryInventoryComplete);
                    }
                }
                if (ds.Kerosene != null)
                {
                    level = "Kerosene";
                    List<Kerosene> itms = (from p in de.AsEnumerable2<Kerosene>(ds.Kerosene) select p).ToList();
                    foreach (Kerosene itm in itms)
                    {
                        FuelsRefineryKeroseneComplete _FuelsRefineryKeroseneComplete = new FuelsRefineryKeroseneComplete(); //Kerosene
                        _FuelsRefineryKeroseneComplete.BlendID = (int?)itm.BlendID; // Kerosene
                        _FuelsRefineryKeroseneComplete.Density = (decimal?)itm.Density; // Kerosene
                        _FuelsRefineryKeroseneComplete.Grade = itm.Grade; // Kerosene
                        _FuelsRefineryKeroseneComplete.SubmissionID = itm.SubmissionID; // Kerosene
                        _FuelsRefineryKeroseneComplete.Sulfur = (decimal?)itm.Sulfur; // Kerosene
                        _FuelsRefineryKeroseneComplete.ThousandMetricTons = (decimal?)itm.KMT; // Kerosene
                        _FuelsRefineryKeroseneComplete.Type = itm.Type; // Kerosene
                        retPlant.KeroseneCompleteList.Add(_FuelsRefineryKeroseneComplete);
                    }
                }
                if (ds.LPG != null)
                {
                    level = "LPG";
                    List<LPG> itms = (from p in de.AsEnumerable2<LPG>(ds.LPG) select p).ToList();
                    foreach (LPG itm in itms)
                    {
                        FuelsRefineryLiquifiedPetroleumGasComplete _FuelsRefineryLiquifiedPetroleumGasComplete = new FuelsRefineryLiquifiedPetroleumGasComplete(); //LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.BlendID = (int?)itm.BlendID; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.MoleOrVolume = itm.MolOrVol; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.SubmissionID = itm.SubmissionID; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC2LT = (decimal?)itm.VolC2Lt; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3 = (decimal?)itm.VolC3; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3ene = (decimal?)itm.VolC3ene; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC4ene = (decimal?)itm.VolC4ene; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC5Plus = (decimal?)itm.VolC5Plus; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeiC4 = (decimal?)itm.VoliC4; // LPG
                        _FuelsRefineryLiquifiedPetroleumGasComplete.VolumenC4 = (decimal?)itm.VolnC4; // LPG
                        retPlant.LiquifiedPetroleumGasCompleteList.Add(_FuelsRefineryLiquifiedPetroleumGasComplete);
                    }
                }
                if (ds.MarineBunkers != null)
                {
                    level = "MarineBunkers";
                    List<MarineBunker> itms = (from p in de.AsEnumerable2<MarineBunker>(ds.MarineBunkers) select p).ToList();
                    foreach (MarineBunker itm in itms)
                    {
                        FuelsRefineryMarineBunkersComplete _FuelsRefineryMarineBunkersComplete = new FuelsRefineryMarineBunkersComplete(); //MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.BlendID = (int?)itm.BlendID; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.CrackedStock = (decimal?)itm.CrackedStock; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.Density = (decimal?)itm.Density; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.Grade = itm.Grade; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.PourPointOfBlend = (decimal?)itm.PourPt; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.SubmissionID = itm.SubmissionID; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.Sulfur = (decimal?)itm.Sulfur; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.ThousandMetricTons = (decimal?)itm.KMT; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendCentiStokeAtTemperature = (decimal?)itm.ViscCSAtTemp; // MarineBunkers
                        _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendTemperatureDifferent122 = (decimal?)itm.ViscTemp; // MarineBunkers
                        retPlant.MarineBunkersCompleteList.Add(_FuelsRefineryMarineBunkersComplete);
                    }
                }
                if (ds.MExp != null)
                {level="MExp";
                    List<MExp> itms = (from p in de.AsEnumerable2<MExp>(ds.MExp) select p).ToList();
                    foreach (MExp itm in itms)
                    {
                        FuelsRefineryMaintenanceExpenseAndCapitalComplete _FuelsRefineryMaintenanceExpenseAndCapitalComplete = new FuelsRefineryMaintenanceExpenseAndCapitalComplete(); //MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.ConstraintRemoval = (decimal?)itm.ConstraintRemoval; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.Energy = (decimal?)itm.Energy; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseRoutine = (decimal?)itm.MaintExpRout; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseTurnaround = (decimal?)itm.MaintExpTA; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadRoutine = (decimal?)itm.MaintOvhdRout; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadTurnaround = (decimal?)itm.MaintOvhdTA; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonMaintenenceInvestmentExpense = (decimal?)itm.NonMaintInvestExp; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRefineryExclusions = (decimal?)itm.NonRefExcl; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRegulatoryNewProcessUnitConstruction = (decimal?)itm.NonRegUnit; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.OtherCapitalInvestment = (decimal?)itm.OthInvest; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryDiesel = (decimal?)itm.RegDiesel; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryExpense = (decimal?)itm.RegExp; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryGasoline = (decimal?)itm.RegGaso; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryOther = (decimal?)itm.RegOth; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RoutineMaintenanceCapital = (decimal?)itm.RoutMaintCptl; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SafetyOther = (decimal?)itm.SafetyOth; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SubmissionID = itm.SubmissionID; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalComplexCapital = (decimal?)itm.ComplexCptl; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenance = (decimal?)itm.MaintExp; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceExpense = (decimal?)itm.TotMaint; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceOverhead = (decimal?)itm.MaintOvhd; // MExp
                        _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TurnaroundMaintenanceCapital = (decimal?)itm.TAMaintCptl; // MExp
                        retPlant.MaintenanceExpenseAndCapitalCompleteList.Add(_FuelsRefineryMaintenanceExpenseAndCapitalComplete);
                    }
                }
                if (ds.MiscInput != null)
                {
                    level = "MiscInput";
                    List<MiscInput> itms = (from p in de.AsEnumerable2<MiscInput>(ds.MiscInput) select p).ToList();
                    foreach (MiscInput itm in itms)
                    {
                        FuelsRefineryMiscellaneousInputComplete _FuelsRefineryMiscellaneousInputComplete = new FuelsRefineryMiscellaneousInputComplete(); //MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.BarrelsInputToCrudeUnits = (decimal?)itm.CDUChargeBbl; // MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.MetricTonsInputToCrudeUnits = (decimal?)itm.CDUChargeMT; // MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.OffsiteEnergyPercent = (decimal?)itm.OffsiteEnergyPcnt; // MiscInput
                        _FuelsRefineryMiscellaneousInputComplete.SubmissionID = itm.SubmissionID; // MiscInput
                        retPlant.MiscellaneousInputCompleteList.Add(_FuelsRefineryMiscellaneousInputComplete);
                    }
                }
                if (ds.MaintRout != null)
                {
                    level = "MaintRout";
                    List<MaintRout> itms = (from p in de.AsEnumerable2<MaintRout>(ds.MaintRout) select p).ToList();
                    foreach (MaintRout itm in itms)
                    {
                        FuelsRefineryRoutineMaintenanceComplete _FuelsRefineryRoutineMaintenanceComplete = new FuelsRefineryRoutineMaintenanceComplete(); //MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.CapitalInLocal = (decimal?)itm.RoutCptlLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.CostInLocal = (decimal?)itm.RoutCostLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.ExpenseInLocal = (decimal?)itm.RoutExpLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.InServicePercent = (decimal?)itm.InServicePcnt; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.MaintenanceHoursDown = (decimal?)itm.MaintDown; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.MaintenanceNumberOfDowntime = (int?)itm.MaintNum; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOffsiteUpsets = (decimal?)itm.OthDownOffsiteUpsets; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOther = (decimal?)itm.OthDownOther; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDown = (decimal?)itm.OthDown; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownEconomic = (decimal?)itm.OthDownEconomic; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownExternal = (decimal?)itm.OthDownExternal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownUnitUpsets = (decimal?)itm.OthDownUnitUpsets; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherNumberOfDowntime = (int?)itm.OthNum; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OtherSlowdownHours = (decimal?)itm.OthSlow; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.OverheadInLocal = (decimal?)itm.RoutOvhdLocal; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.PercentUtilization = (decimal?)itm.UtilPcnt; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.ProcessID = itm.ProcessID; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.RegulatoryHoursDown = (decimal?)itm.RegDown; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.RegulatoryNumberOfDowntime = (int?)itm.RegNum; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.SubmissionID = itm.SubmissionID; // MaintRout
                        _FuelsRefineryRoutineMaintenanceComplete.UnitID = itm.UnitID; // MaintRout
                        retPlant.RoutineMaintenanceCompleteList.Add(_FuelsRefineryRoutineMaintenanceComplete);
                    }
                }
                if (ds.MaintTA != null)
                {
                    level = "MaintTA";
                    List<MaintTA> itms = (from p in de.AsEnumerable2<MaintTA>(ds.MaintTA) select p).ToList();
                    foreach (MaintTA itm in itms)
                    {
                        FuelsRefineryTurnaroundMaintenanceComplete _FuelsRefineryTurnaroundMaintenanceComplete = new FuelsRefineryTurnaroundMaintenanceComplete(); //MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.CapitalLocal = (decimal?)itm.TACptlLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ContractorManagementAndProfessionalSalaried = (decimal?)itm.TAContMPS; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ContractorOperatorCraftAndClericalHours = (decimal?)itm.TAContOCC; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.CostLocal = (decimal?)itm.TACostLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.Exceptions = (int?)itm.TAExceptions; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ExpenseLocal = (decimal?)itm.TAExpLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.HoursDown = (decimal?)itm.TAHrsDown; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.LaborCostLocal = (decimal?)itm.TALaborCostLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalOvertimePercent = (decimal?)itm.TAMPSOVTPcnt; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalSalariedStraightTimeHours = (decimal?)itm.TAMPSSTH; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalOvertimeHours = (decimal?)itm.TAOCCOVT; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalStraightTimeHours = (decimal?)itm.TAOCCSTH; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.OverheadLocal = (decimal?)itm.TAOvhdLocal; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.PreviousTurnaroundDate = (DateTime?)itm.PrevTADate; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.ProcessID = itm.ProcessID; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.SubmissionID = itm.SubmissionID; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.TurnaroundDate = (DateTime?)itm.TADate; // MaintTA
                        _FuelsRefineryTurnaroundMaintenanceComplete.UnitID = itm.UnitID; // MaintTA               
                        retPlant.TurnaroundMaintenanceCompleteList.Add(_FuelsRefineryTurnaroundMaintenanceComplete);
                    }
                }
                if (ds.OpexData != null)
                {
                    level = "OpexData";
                    List<OpexData> itms = (from p in de.AsEnumerable2<OpexData>(ds.OpexData) select p).ToList();
                    foreach (OpexData itm in itms)
                    {
                        OperatingExpenseStandard _OperatingExpenseStandard = new OperatingExpenseStandard(); //OpexData
                        _OperatingExpenseStandard.OtherDescription = itm.OthDescription; // OpexData
                        _OperatingExpenseStandard.Property = itm.Property; // OpexData
                        _OperatingExpenseStandard.ReportValue = (decimal?)itm.RptValue; // OpexData
                        _OperatingExpenseStandard.SubmissionID = itm.SubmissionID; // OpexData
                        retPlant.OperatingExpenseStandardList.Add(_OperatingExpenseStandard);
                    }
                }
                if (ds.Pers != null)
                {
                    level = "Pers";
                    List<Per> itms = (from p in de.AsEnumerable2<Per>(ds.Pers) select p).ToList();
                    foreach (Per itm in itms)
                    {
                        FuelsRefineryPersonnelHoursComplete _FuelsRefineryPersonnelHoursComplete = new FuelsRefineryPersonnelHoursComplete(); //Pers
                        _FuelsRefineryPersonnelHoursComplete.AbsenceHours = (decimal?)itm.AbsHrs; // Pers
                        _FuelsRefineryPersonnelHoursComplete.Contract = (decimal?)itm.Contract; // Pers
                        _FuelsRefineryPersonnelHoursComplete.GeneralAndAdministrativeHours = (decimal?)itm.GA; // Pers
                        _FuelsRefineryPersonnelHoursComplete.NumberOfPersonnel = (decimal?)itm.NumPers; // Pers
                        _FuelsRefineryPersonnelHoursComplete.OvertimeHours = (decimal?)itm.OVTHours; // Pers
                        _FuelsRefineryPersonnelHoursComplete.OvertimePercent = (decimal?)itm.OVTPcnt; // Pers
                        _FuelsRefineryPersonnelHoursComplete.PersonnelHoursID = itm.PersID; // Pers
                        _FuelsRefineryPersonnelHoursComplete.StraightTimeHours = (decimal?)itm.STH; // Pers
                        _FuelsRefineryPersonnelHoursComplete.SubmissionID = itm.SubmissionID; // Pers
                        retPlant.PersonnelHoursCompleteList.Add(_FuelsRefineryPersonnelHoursComplete);
                    }
                }
                if (ds.ProcessData != null)
                {
                    level = "ProcessData";
                    List<ProcessData> itms = (from p in de.AsEnumerable2<ProcessData>(ds.ProcessData) select p).ToList();
                    foreach (ProcessData itm in itms)
                    {

                        PlantOperatingConditionBasic _PlantOperatingConditionBasic = new PlantOperatingConditionBasic(); //ProcessData
                        _PlantOperatingConditionBasic.Property = itm.Property; // ProcessData
                        _PlantOperatingConditionBasic.ReportDateValue = (DateTime?)itm.RptDVal; // ProcessData
                        _PlantOperatingConditionBasic.ReportNumericValue = (decimal?)itm.RptNVal; // ProcessData
                        _PlantOperatingConditionBasic.ReportTextValue = itm.RptTVal; // ProcessData
                        _PlantOperatingConditionBasic.SubmissionID = itm.SubmissionID; // ProcessData
                        _PlantOperatingConditionBasic.UnitID = itm.UnitID; // ProcessData
                        _PlantOperatingConditionBasic.UnitOfMeasure = itm.UOM; // ProcessData
                        retPlant.PlantOperatingConditionList.Add(_PlantOperatingConditionBasic);
                    }
                }
                if (ds.Resid != null)
                {
                    level = "Resid";
                    List<Resid> itms = (from p in de.AsEnumerable2<Resid>(ds.Resid) select p).ToList();
                    foreach (Resid itm in itms)
                    {
                        FuelsRefineryFinishedResidualFuelComplete _FuelsRefineryFinishedResidualFuelComplete = new FuelsRefineryFinishedResidualFuelComplete(); //Resid
                        _FuelsRefineryFinishedResidualFuelComplete.BlendID = (int?)itm.BlendID; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.Density = (decimal?)itm.Density; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.Grade = itm.Grade; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.PourPointOfBlend = (decimal?)itm.PourPT; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.SubmissionID = itm.SubmissionID; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.Sulfur = (decimal?)itm.Sulfur; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.ThousandMetricTons = (decimal?)itm.KMT; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendCentiStokeAtTemperature = (decimal?)itm.ViscCSAtTemp; // Resid
                        _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendTemperatureDifferent122 = (decimal?)itm.ViscTemp; // Resid
                        retPlant.FinishedResidualFuelCompleteList.Add(_FuelsRefineryFinishedResidualFuelComplete);
                    }
                }
                if (ds.RPFResid != null)
                {
                    level = "RPFResid";

                    List<RPFResid> itms = (from p in de.AsEnumerable2<RPFResid>(ds.RPFResid) select p).ToList();
                    foreach (RPFResid itm in itms)
                    {
                        FuelsRefineryProducedFuelResidualComplete _FuelsRefineryProducedFuelResidualComplete = new FuelsRefineryProducedFuelResidualComplete(); //RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.Density = (decimal?)itm.Density; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.EnergyType = itm.EnergyType; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.SubmissionID = itm.SubmissionID; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.Sulfur = (decimal?)itm.Sulfur; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.ViscosityCentiStokesAtTemperature = (decimal?)itm.ViscCSAtTemp; // RPFResid
                        _FuelsRefineryProducedFuelResidualComplete.ViscosityTemperature = (decimal?)itm.ViscTemp; // RPFResid
                        retPlant.RefineryProducedFuelResidualCompleteList.Add(_FuelsRefineryProducedFuelResidualComplete);
                    }
                }
                if (ds.SteamSystem != null)
                {
                    level = "SteamSystem";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem>(ds.SteamSystem) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem itm in itms)
                    {
                        FuelsRefinerySteamSystemComplete _FuelsRefinerySteamSystemComplete = new FuelsRefinerySteamSystemComplete(); //SteamSystem
                        _FuelsRefinerySteamSystemComplete.ActualPressure = (decimal?)itm.ActualPress; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.Calciner = (decimal?)itm.Calciner; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedAllOther = (decimal?)itm.ConsOth; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedCombustionAirPreheat = (decimal?)itm.ConsCombAirPreheat; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedCondensingTurbines = (decimal?)itm.ConsCondTurb; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedDeaerators = (decimal?)itm.ConsDeaerators; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedOtherHeaters = (decimal?)itm.ConsOthHeaters; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedPressureControlFromHigherPressure = (decimal?)itm.ConsPressControlHigh; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedPressureControlToLowerPressure = (decimal?)itm.ConsPressControlLow; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessCoker = (decimal?)itm.ConsProcessCOK; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessCrudeAndVacuum = (decimal?)itm.ConsProcessCDU; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessFluidCatalystCracker = (decimal?)itm.ConsProcessFCC; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedProcessOther = (decimal?)itm.ConsProcessOth; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedReboilersEvaporators = (decimal?)itm.ConsReboil; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedSteamToFlares = (decimal?)itm.ConsFlares; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedSteamTracingTankBuildingHeat = (decimal?)itm.ConsTracingHeat; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionFromHigherPressure = (decimal?)itm.ConsTopTurbHigh; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionToLowerPressure = (decimal?)itm.ConsTopTurbLow; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.ConsumedTotalSteam = (decimal?)itm.TotCons; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FiredBoiler = (decimal?)itm.FiredBoiler; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FiredProcessHeaterConvectionSection = (decimal?)itm.FiredProcessHeater; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FiredTurbineCogeneration = (decimal?)itm.FTCogen; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerCoolers = (decimal?)itm.FCCCatCoolers; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerStackGas = (decimal?)itm.FCCStackGas; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.FluidCokerCOBoiler = (decimal?)itm.FluidCokerCOBoiler; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.H2PlantNetExportSteam = (decimal?)itm.H2PlantExport; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.OtherSteamSources = (decimal?)itm.OthSource; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.PressureRange = itm.PressureRange; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.Purchased = (decimal?)itm.Pur; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.Sold = (decimal?)itm.Sold; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.SubmissionID = itm.SubmissionID; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.SubtotalNetPurchased = (decimal?)itm.NetPur; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.SubtotalSteamProduction = (decimal?)itm.STProd; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.TotalSupply = (decimal?)itm.TotSupply; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatCokerHeavyGasOilPumparound = (decimal?)itm.WasteHeatCOK; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatFluidCatalystCracker = (decimal?)itm.WasteHeatFCC; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatOtherBoilers = (decimal?)itm.WasteHeatOth; // SteamSystem
                        _FuelsRefinerySteamSystemComplete.WasteHeatThermalCracker = (decimal?)itm.WasteHeatTCR; // SteamSystem
                        retPlant.SteamSystemCompleteList.Add(_FuelsRefinerySteamSystemComplete);
                    }
                }
                if (ds.Submission != null)
                {
                    level = "Submissions";
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission>(ds.Submission) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission itm in itms)
                    {
                        SubmissionBasic _SubmissionBasic = new SubmissionBasic(); //Submissions
                        _SubmissionBasic.RefineryID = itm.RefID; // Submissions
                        _SubmissionBasic.ReportCurrency = itm.RptCurrency; // Submissions
                        _SubmissionBasic.SubmissionDate = (DateTime?)itm.Date; // Submissions
                        _SubmissionBasic.SubmissionID = itm.SubmissionID; // Submissions
                        _SubmissionBasic.UnitOfMeasure = itm.UOM; // Submissions
                        retPlant.PlantSubmission = _SubmissionBasic;
                    }
                }
                if (ds.Yield != null)
                {
                    level = "Yield";

                    List<Yield> itms = (from p in de.AsEnumerable2<Yield>(ds.Yield) select p).ToList();
                    foreach (Yield itm in itms)
                    {
                        FuelsRefineryMaterialBalanceComplete _FuelsRefineryMaterialBalanceComplete = new FuelsRefineryMaterialBalanceComplete(); //Yield
                        _FuelsRefineryMaterialBalanceComplete.Barrels = (decimal?)itm.BBL; // Yield
                        _FuelsRefineryMaterialBalanceComplete.Category = itm.Category; // Yield
                        _FuelsRefineryMaterialBalanceComplete.Density = (decimal?)itm.Density; // Yield
                        _FuelsRefineryMaterialBalanceComplete.MaterialID = itm.MaterialID; // Yield
                        _FuelsRefineryMaterialBalanceComplete.MaterialName = itm.MaterialName; // Yield
                        _FuelsRefineryMaterialBalanceComplete.MetricTons = (decimal?)itm.MT; // Yield
                        _FuelsRefineryMaterialBalanceComplete.Period = itm.Period; // Yield
                        _FuelsRefineryMaterialBalanceComplete.SubmissionID = itm.SubmissionID; // Yield
                        retPlant.MaterialBalanceCompleteList.Add(_FuelsRefineryMaterialBalanceComplete);
                    }
                }                           
            }         
            catch (Exception ex)
            {
                string template = "ClientToBusError:Level={0} Error:{1}";
                Exception infoException = new Exception(string.Format(template,level,ex.Message));
                throw infoException;
            }
            return retPlant;
        }

        public FuelsRefineryCompletePlant WCFToBus(PL plantIn)
        {
            string level = "CB";
            FuelsRefineryCompletePlant retPlant = new FuelsRefineryCompletePlant(new FuelsRefineryPlantUnitFactory(PlantLevel.COMPLETE));                  
            try
            {
                foreach (CB _CB in plantIn.CB_List)
                {
                    FuelsRefineryConfigurationBuoyComplete _FuelsRefineryConfigurationBuoyComplete = new FuelsRefineryConfigurationBuoyComplete();                  
                    _FuelsRefineryConfigurationBuoyComplete.LineSize = _CB.CB2;
                    _FuelsRefineryConfigurationBuoyComplete.PercentOwnership = _CB.CB1;
                    _FuelsRefineryConfigurationBuoyComplete.ProcessID = _CB.CB4;
                    _FuelsRefineryConfigurationBuoyComplete.ShipCapacity = _CB.CB3;
                    _FuelsRefineryConfigurationBuoyComplete.SubmissionID = _CB.CB7;
                    _FuelsRefineryConfigurationBuoyComplete.UnitID = _CB.CB6;
                    _FuelsRefineryConfigurationBuoyComplete.UnitName = _CB.CB5;
                    retPlant.ConfigurationBuoyCompleteList.Add(_FuelsRefineryConfigurationBuoyComplete);
                }
                level = "C";
                foreach (C _C in plantIn.C_List)
                {
                    FuelsRefineryConfigurationComplete _FuelsRefineryConfigurationComplete = new FuelsRefineryConfigurationComplete();
                    _FuelsRefineryConfigurationComplete.AllocatedPercentOfCapacity = _C.C2;
                    _FuelsRefineryConfigurationComplete.BlockOperated = _C.C3;
                    _FuelsRefineryConfigurationComplete.Capacity = _C.C11;
                    _FuelsRefineryConfigurationComplete.EnergyPercent = _C.C1;
                    _FuelsRefineryConfigurationComplete.InServicePercent = _C.C7;
                    _FuelsRefineryConfigurationComplete.ManHourWeek = _C.C5;
                    _FuelsRefineryConfigurationComplete.PostPerShift = _C.C4;
                    _FuelsRefineryConfigurationComplete.ProcessID = _C.C14;
                    _FuelsRefineryConfigurationComplete.ProcessType = _C.C12;
                    _FuelsRefineryConfigurationComplete.SteamCapacity = _C.C9;
                    _FuelsRefineryConfigurationComplete.SteamUtilizationPercent = _C.C8;
                    _FuelsRefineryConfigurationComplete.SubmissionID = _C.C16;
                    _FuelsRefineryConfigurationComplete.UnitID = _C.C15;
                    _FuelsRefineryConfigurationComplete.UnitName = _C.C13;
                    _FuelsRefineryConfigurationComplete.UtilizationPercent = _C.C10;
                    _FuelsRefineryConfigurationComplete.YearsInOperation = _C.C6;
                    retPlant.ConfigurationCompleteList.Add(_FuelsRefineryConfigurationComplete);
                }
                level = "CR";
                foreach (CR _CR in plantIn.CR_List)
                {
                    FuelsRefineryConfigurationReceiptAndShipmentComplete _FuelsRefineryConfigurationReceiptAndShipmentComplete = new FuelsRefineryConfigurationReceiptAndShipmentComplete();
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.AverageSize = _CR.CR2;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessID = _CR.CR4;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessType = _CR.CR3;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.SubmissionID = _CR.CR6;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.Throughput = _CR.CR1;
                    _FuelsRefineryConfigurationReceiptAndShipmentComplete.UnitID = _CR.CR5;
                    retPlant.ConfigurationReceiptAndShipmentCompleteList.Add(_FuelsRefineryConfigurationReceiptAndShipmentComplete);
                }
                level = "CD";
                foreach (CD _CD in plantIn.CD_List)
                {
                    FuelsRefineryCrudeComplete _FuelsRefineryCrudeComplete = new FuelsRefineryCrudeComplete();
                    _FuelsRefineryCrudeComplete.Barrels = _CD.CD3;
                    _FuelsRefineryCrudeComplete.CrudeID = _CD.CD6;
                    _FuelsRefineryCrudeComplete.CrudeName = _CD.CD4;
                    _FuelsRefineryCrudeComplete.CrudeNumber = _CD.CD5;
                    _FuelsRefineryCrudeComplete.Gravity = _CD.CD2;
                    _FuelsRefineryCrudeComplete.Period = _CD.CD7;
                    _FuelsRefineryCrudeComplete.SubmissionID = _CD.CD8;
                    _FuelsRefineryCrudeComplete.Sulfur = _CD.CD1;
                    retPlant.CrudeCompleteList.Add(_FuelsRefineryCrudeComplete);
                }
                level = "D";
                foreach (D _D in plantIn.D_List)
                {
                    FuelsRefineryDieselComplete _FuelsRefineryDieselComplete = new FuelsRefineryDieselComplete();
                    _FuelsRefineryDieselComplete.AmericanStandardForTestingMaterials90DistillationPoint = _D.D4;
                    _FuelsRefineryDieselComplete.BiodieselInBlendPercent = _D.D2;
                    _FuelsRefineryDieselComplete.BlendID = _D.D13;
                    _FuelsRefineryDieselComplete.Cetane = _D.D8;
                    _FuelsRefineryDieselComplete.CloudPointOfBlend = _D.D5;
                    _FuelsRefineryDieselComplete.Density = _D.D9;
                    _FuelsRefineryDieselComplete.Grade = _D.D12;
                    _FuelsRefineryDieselComplete.Market = _D.D11;
                    _FuelsRefineryDieselComplete.PercentEvaporatedAt350C = _D.D3;
                    _FuelsRefineryDieselComplete.PourPointOfBlend = _D.D7;
                    _FuelsRefineryDieselComplete.SubmissionID = _D.D14;
                    _FuelsRefineryDieselComplete.Sulfur = _D.D6;
                    _FuelsRefineryDieselComplete.ThousandMetricTons = _D.D1;
                    _FuelsRefineryDieselComplete.Type = _D.D10;
                    retPlant.DieselCompleteList.Add(_FuelsRefineryDieselComplete);
                }
                level = "E";
                foreach (E _E in plantIn.E_List)
                {
                    FuelsRefineryEmissionComplete _FuelsRefineryEmissionComplete = new FuelsRefineryEmissionComplete();
                    _FuelsRefineryEmissionComplete.EmissionType = _E.E2;
                    _FuelsRefineryEmissionComplete.RefineryEmission = _E.E1;
                    _FuelsRefineryEmissionComplete.SubmissionID = _E.E3;
                    retPlant.EmissionCompleteList.Add(_FuelsRefineryEmissionComplete);
                }
                level = "S";
                foreach (S _S in plantIn.S_List)
                {
                    FuelsRefinerySteamSystemComplete _FuelsRefinerySteamSystemComplete = new FuelsRefinerySteamSystemComplete();
                    _FuelsRefinerySteamSystemComplete.ActualPressure = _S.S36;
                    _FuelsRefinerySteamSystemComplete.Calciner = _S.S29;
                    _FuelsRefinerySteamSystemComplete.ConsumedAllOther = _S.S2;
                    _FuelsRefinerySteamSystemComplete.ConsumedCombustionAirPreheat = _S.S8;
                    _FuelsRefinerySteamSystemComplete.ConsumedCondensingTurbines = _S.S11;
                    _FuelsRefinerySteamSystemComplete.ConsumedDeaerators = _S.S4;
                    _FuelsRefinerySteamSystemComplete.ConsumedOtherHeaters = _S.S12;
                    _FuelsRefinerySteamSystemComplete.ConsumedPressureControlFromHigherPressure = _S.S7;
                    _FuelsRefinerySteamSystemComplete.ConsumedPressureControlToLowerPressure = _S.S6;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessCoker = _S.S16;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessCrudeAndVacuum = _S.S17;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessFluidCatalystCracker = _S.S15;
                    _FuelsRefinerySteamSystemComplete.ConsumedProcessOther = _S.S14;
                    _FuelsRefinerySteamSystemComplete.ConsumedReboilersEvaporators = _S.S13;
                    _FuelsRefinerySteamSystemComplete.ConsumedSteamToFlares = _S.S3;
                    _FuelsRefinerySteamSystemComplete.ConsumedSteamTracingTankBuildingHeat = _S.S5;
                    _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionFromHigherPressure = _S.S10;
                    _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionToLowerPressure = _S.S9;
                    _FuelsRefinerySteamSystemComplete.ConsumedTotalSteam = _S.S1;
                    _FuelsRefinerySteamSystemComplete.FiredBoiler = _S.S33;
                    _FuelsRefinerySteamSystemComplete.FiredProcessHeaterConvectionSection = _S.S34;
                    _FuelsRefinerySteamSystemComplete.FiredTurbineCogeneration = _S.S28;
                    _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerCoolers = _S.S32;
                    _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerStackGas = _S.S31;
                    _FuelsRefinerySteamSystemComplete.FluidCokerCOBoiler = _S.S30;
                    _FuelsRefinerySteamSystemComplete.H2PlantNetExportSteam = _S.S35;
                    _FuelsRefinerySteamSystemComplete.OtherSteamSources = _S.S23;
                    _FuelsRefinerySteamSystemComplete.PressureRange = _S.S37;
                    _FuelsRefinerySteamSystemComplete.Purchased = _S.S20;
                    _FuelsRefinerySteamSystemComplete.Sold = _S.S21;
                    _FuelsRefinerySteamSystemComplete.SubmissionID = _S.S38;
                    _FuelsRefinerySteamSystemComplete.SubtotalNetPurchased = _S.S19;
                    _FuelsRefinerySteamSystemComplete.SubtotalSteamProduction = _S.S22;
                    _FuelsRefinerySteamSystemComplete.TotalSupply = _S.S18;
                    _FuelsRefinerySteamSystemComplete.WasteHeatCokerHeavyGasOilPumparound = _S.S25;
                    _FuelsRefinerySteamSystemComplete.WasteHeatFluidCatalystCracker = _S.S27;
                    _FuelsRefinerySteamSystemComplete.WasteHeatOtherBoilers = _S.S24;
                    _FuelsRefinerySteamSystemComplete.WasteHeatThermalCracker = _S.S26;
                    retPlant.SteamSystemCompleteList.Add(_FuelsRefinerySteamSystemComplete);
                }
                level = "EG";
                int posEnerg = 1;
                foreach (EG _EG in plantIn.EG_List)
                {
                    FuelsRefineryEnergyComplete _FuelsRefineryEnergyComplete = new FuelsRefineryEnergyComplete();
                    _FuelsRefineryEnergyComplete.Amount = _EG.EG21;
                    _FuelsRefineryEnergyComplete.Butane = _EG.EG10;
                    _FuelsRefineryEnergyComplete.Butylenes = _EG.EG8;
                    _FuelsRefineryEnergyComplete.C5Plus = _EG.EG7;
                    _FuelsRefineryEnergyComplete.CO = _EG.EG5;
                    _FuelsRefineryEnergyComplete.CO2 = _EG.EG2;
                    _FuelsRefineryEnergyComplete.EnergyType = _EG.EG23;
                    _FuelsRefineryEnergyComplete.Ethane = _EG.EG14;
                    _FuelsRefineryEnergyComplete.Ethylene = _EG.EG13;
                   //moved to electric  _FuelsRefineryEnergyComplete.GenerationEfficiency = _EG.EG17;
                    _FuelsRefineryEnergyComplete.H2S = _EG.EG6;
                    _FuelsRefineryEnergyComplete.Hydrogen = _EG.EG16;
                    _FuelsRefineryEnergyComplete.Isobutane = _EG.EG9;
                    _FuelsRefineryEnergyComplete.Methane = _EG.EG15;
                    _FuelsRefineryEnergyComplete.MillionBritishThermalUnitsOut = _EG.EG19;
                    _FuelsRefineryEnergyComplete.N2 = _EG.EG1;
                    _FuelsRefineryEnergyComplete.NH3 = _EG.EG4;
                    _FuelsRefineryEnergyComplete.OverrideCalculation = _EG.EG18;
                    _FuelsRefineryEnergyComplete.PriceLocal = _EG.EG20;
                    _FuelsRefineryEnergyComplete.Propane = _EG.EG12;
                    _FuelsRefineryEnergyComplete.Propylene = _EG.EG11;
                    _FuelsRefineryEnergyComplete.SO2 = _EG.EG3;
                    _FuelsRefineryEnergyComplete.SubmissionID = _EG.EG24;
                    _FuelsRefineryEnergyComplete.TransactionType = _EG.EG22;
                    _FuelsRefineryEnergyComplete.TransactionCode = posEnerg;
                    posEnerg++;
                    retPlant.EnergyCompleteList.Add(_FuelsRefineryEnergyComplete);
                }
                level = "EL";
                int posElect = 1;
                foreach (EL _EL in plantIn.EL_List)
                {
                    FuelsRefineryEnergyElectricBasic _FuelsRefineryEnergyElectricBasic = new FuelsRefineryEnergyElectricBasic();
                       _FuelsRefineryEnergyElectricBasic.Amount = _EL.EL3;
                    _FuelsRefineryEnergyElectricBasic.EnergyType = _EL.EL5;
                    _FuelsRefineryEnergyElectricBasic.GenerationEfficiency = _EL.EL1;
                    _FuelsRefineryEnergyElectricBasic.PriceLocal = _EL.EL2;
                    _FuelsRefineryEnergyElectricBasic.SubmissionID = _EL.EL6;
                    _FuelsRefineryEnergyElectricBasic.TransactionType = _EL.EL4;
                    _FuelsRefineryEnergyElectricBasic.TransactionCode = posElect;
                    posElect++;
                    retPlant.ElectricBasicList.Add(_FuelsRefineryEnergyElectricBasic);

                }
                level = "R";
                foreach (R _R in plantIn.R_List)
                {
                    FuelsRefineryFinishedResidualFuelComplete _FuelsRefineryFinishedResidualFuelComplete = new FuelsRefineryFinishedResidualFuelComplete();
                    _FuelsRefineryFinishedResidualFuelComplete.BlendID = _R.R8;
                    _FuelsRefineryFinishedResidualFuelComplete.Density = _R.R6;
                    _FuelsRefineryFinishedResidualFuelComplete.Grade = _R.R7;
                    _FuelsRefineryFinishedResidualFuelComplete.PourPointOfBlend = _R.R4;
                    _FuelsRefineryFinishedResidualFuelComplete.SubmissionID = _R.R9;
                    _FuelsRefineryFinishedResidualFuelComplete.Sulfur = _R.R5;
                    _FuelsRefineryFinishedResidualFuelComplete.ThousandMetricTons = _R.R1;
                    _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendCentiStokeAtTemperature = _R.R3;
                    _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendTemperatureDifferent122 = _R.R2;
                    retPlant.FinishedResidualFuelCompleteList.Add(_FuelsRefineryFinishedResidualFuelComplete);
                }
                level = "F";
                foreach (F _F in plantIn.F_List)
                {
                    FuelsRefineryFiredHeatersComplete _FuelsRefineryFiredHeatersComplete = new FuelsRefineryFiredHeatersComplete();
                    _FuelsRefineryFiredHeatersComplete.AbsorbedDuty = _F.F6;
                    _FuelsRefineryFiredHeatersComplete.CombustionAirTemperature = _F.F7;
                    _FuelsRefineryFiredHeatersComplete.FiredDuty = _F.F15;
                    _FuelsRefineryFiredHeatersComplete.FuelType = _F.F14;
                    _FuelsRefineryFiredHeatersComplete.FurnaceInTemperature = _F.F12;
                    _FuelsRefineryFiredHeatersComplete.FurnaceOutTemperature = _F.F11;
                    _FuelsRefineryFiredHeatersComplete.HeaterName = _F.F20;
                    _FuelsRefineryFiredHeatersComplete.HeaterNumber = _F.F23;
                    _FuelsRefineryFiredHeatersComplete.HeatLossPercent = _F.F8;
                    _FuelsRefineryFiredHeatersComplete.OtherCombustionDuty = _F.F13;
                    _FuelsRefineryFiredHeatersComplete.OtherDuty = _F.F1;
                    _FuelsRefineryFiredHeatersComplete.ProcessDuty = _F.F5;
                    _FuelsRefineryFiredHeatersComplete.ProcessFluid = _F.F18;
                    _FuelsRefineryFiredHeatersComplete.ProcessID = _F.F21;
                    _FuelsRefineryFiredHeatersComplete.Service = _F.F19;
                    _FuelsRefineryFiredHeatersComplete.ShaftDuty = _F.F2;
                    _FuelsRefineryFiredHeatersComplete.StackO2 = _F.F9;
                    _FuelsRefineryFiredHeatersComplete.StackTemperature = _F.F10;
                    _FuelsRefineryFiredHeatersComplete.SteamDuty = _F.F4;
                    _FuelsRefineryFiredHeatersComplete.SteamSuperHeated = _F.F3;
                    _FuelsRefineryFiredHeatersComplete.SubmissionID = _F.F24;
                    _FuelsRefineryFiredHeatersComplete.ThroughputReport = _F.F17;
                    _FuelsRefineryFiredHeatersComplete.ThroughputUnitOfMeasure = _F.F16;
                    _FuelsRefineryFiredHeatersComplete.UnitID = _F.F22;
                    retPlant.FiredHeatersCompleteList.Add(_FuelsRefineryFiredHeatersComplete);
                }
                level = "G";
                foreach (G _G in plantIn.G_List)
                {
                    FuelsRefineryGasolineComplete _FuelsRefineryGasolineComplete = new FuelsRefineryGasolineComplete();
                    _FuelsRefineryGasolineComplete.BlendID = _G.G17;
                    _FuelsRefineryGasolineComplete.Density = _G.G13;
                    _FuelsRefineryGasolineComplete.Ethanol = _G.G8;
                    _FuelsRefineryGasolineComplete.EthylTertButylEther = _G.G6;
                    _FuelsRefineryGasolineComplete.Grade = _G.G16;
                    _FuelsRefineryGasolineComplete.Lead = _G.G2;
                    _FuelsRefineryGasolineComplete.Market = _G.G15;
                    _FuelsRefineryGasolineComplete.MethylTertButylEther = _G.G7;
                    _FuelsRefineryGasolineComplete.MotorOctaneNumber = _G.G10;
                    _FuelsRefineryGasolineComplete.OtherOxygen = _G.G4;
                    _FuelsRefineryGasolineComplete.Oxygen = _G.G9;
                    _FuelsRefineryGasolineComplete.ReidVaporPressure = _G.G12;
                    _FuelsRefineryGasolineComplete.ResearchOctaneNumber = _G.G11;
                    _FuelsRefineryGasolineComplete.SubmissionID = _G.G18;
                    _FuelsRefineryGasolineComplete.Sulfur = _G.G3;
                    _FuelsRefineryGasolineComplete.TertAmylMethylEther = _G.G5;
                    _FuelsRefineryGasolineComplete.ThousandMetricTons = _G.G1;
                    _FuelsRefineryGasolineComplete.Type = _G.G14;
                    retPlant.GasolineCompleteList.Add(_FuelsRefineryGasolineComplete);
                }
                level = "GM";
                foreach (GM _GM in plantIn.GM_List)
                {
                    FuelsRefineryGeneralMiscellaneousComplete _FuelsRefineryGeneralMiscellaneousComplete = new FuelsRefineryGeneralMiscellaneousComplete();
                    _FuelsRefineryGeneralMiscellaneousComplete.FlareLossMetricTons = _GM.GM2;
                    _FuelsRefineryGeneralMiscellaneousComplete.SubmissionID = _GM.GM3;
                    _FuelsRefineryGeneralMiscellaneousComplete.TotalLossMetricTons = _GM.GM1;
                    retPlant.GeneralMiscellaneousCompleteList.Add(_FuelsRefineryGeneralMiscellaneousComplete);
                }
                level = "K";
                foreach (K _K in plantIn.K_List)
                {
                    FuelsRefineryKeroseneComplete _FuelsRefineryKeroseneComplete = new FuelsRefineryKeroseneComplete();
                    _FuelsRefineryKeroseneComplete.BlendID = _K.K6;
                    _FuelsRefineryKeroseneComplete.Density = _K.K3;
                    _FuelsRefineryKeroseneComplete.Grade = _K.K5;
                    _FuelsRefineryKeroseneComplete.SubmissionID = _K.K7;
                    _FuelsRefineryKeroseneComplete.Sulfur = _K.K2;
                    _FuelsRefineryKeroseneComplete.ThousandMetricTons = _K.K1;
                    _FuelsRefineryKeroseneComplete.Type = _K.K4;
                    retPlant.KeroseneCompleteList.Add(_FuelsRefineryKeroseneComplete);
                }
                level = "L";
                foreach (L _L in plantIn.L_List)
                {
                    FuelsRefineryLiquifiedPetroleumGasComplete _FuelsRefineryLiquifiedPetroleumGasComplete = new FuelsRefineryLiquifiedPetroleumGasComplete();
                    _FuelsRefineryLiquifiedPetroleumGasComplete.BlendID = _L.L9;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.MoleOrVolume = _L.L8;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.SubmissionID = _L.L10;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC2LT = _L.L7;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3 = _L.L6;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3ene = _L.L5;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC4ene = _L.L2;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC5Plus = _L.L1;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeiC4 = _L.L4;
                    _FuelsRefineryLiquifiedPetroleumGasComplete.VolumenC4 = _L.L3;
                    retPlant.LiquifiedPetroleumGasCompleteList.Add(_FuelsRefineryLiquifiedPetroleumGasComplete);
                }
                level = "ME";
                foreach (ME _ME in plantIn.ME_List)
                {
                    FuelsRefineryMaintenanceExpenseAndCapitalComplete _FuelsRefineryMaintenanceExpenseAndCapitalComplete = new FuelsRefineryMaintenanceExpenseAndCapitalComplete();
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.ConstraintRemoval = _ME.ME15;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.Energy = _ME.ME9;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseRoutine = _ME.ME6;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseTurnaround = _ME.ME7;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadRoutine = _ME.ME3;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadTurnaround = _ME.ME4;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonMaintenenceInvestmentExpense = _ME.ME17;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRefineryExclusions = _ME.ME20;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRegulatoryNewProcessUnitConstruction = _ME.ME16;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.OtherCapitalInvestment = _ME.ME8;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryDiesel = _ME.ME12;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryExpense = _ME.ME14;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryGasoline = _ME.ME13;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryOther = _ME.ME11;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RoutineMaintenanceCapital = _ME.ME18;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SafetyOther = _ME.ME10;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SubmissionID = _ME.ME22;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalComplexCapital = _ME.ME21;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenance = _ME.ME5;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceExpense = _ME.ME1;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceOverhead = _ME.ME2;
                    _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TurnaroundMaintenanceCapital = _ME.ME19;
                    retPlant.MaintenanceExpenseAndCapitalCompleteList.Add(_FuelsRefineryMaintenanceExpenseAndCapitalComplete);
                }
                level = "M";
                foreach (M _M in plantIn.M_List)
                {
                    FuelsRefineryMarineBunkersComplete _FuelsRefineryMarineBunkersComplete = new FuelsRefineryMarineBunkersComplete();
                    _FuelsRefineryMarineBunkersComplete.BlendID = _M.M9;
                    _FuelsRefineryMarineBunkersComplete.CrackedStock = _M.M2;
                    _FuelsRefineryMarineBunkersComplete.Density = _M.M7;
                    _FuelsRefineryMarineBunkersComplete.Grade = _M.M8;
                    _FuelsRefineryMarineBunkersComplete.PourPointOfBlend = _M.M5;
                    _FuelsRefineryMarineBunkersComplete.SubmissionID = _M.M10;
                    _FuelsRefineryMarineBunkersComplete.Sulfur = _M.M6;
                    _FuelsRefineryMarineBunkersComplete.ThousandMetricTons = _M.M1;
                    _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendCentiStokeAtTemperature = _M.M4;
                    _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendTemperatureDifferent122 = _M.M3;
                    retPlant.MarineBunkersCompleteList.Add(_FuelsRefineryMarineBunkersComplete);
                }
                level = "MI";
                foreach (MI _MI in plantIn.MI_List)
                {
                    FuelsRefineryMiscellaneousInputComplete _FuelsRefineryMiscellaneousInputComplete = new FuelsRefineryMiscellaneousInputComplete();
                    _FuelsRefineryMiscellaneousInputComplete.BarrelsInputToCrudeUnits = _MI.MI3;
                    _FuelsRefineryMiscellaneousInputComplete.MetricTonsInputToCrudeUnits = _MI.MI2;
                    _FuelsRefineryMiscellaneousInputComplete.OffsiteEnergyPercent = _MI.MI1;
                    _FuelsRefineryMiscellaneousInputComplete.SubmissionID = _MI.MI4;
                    retPlant.MiscellaneousInputCompleteList.Add(_FuelsRefineryMiscellaneousInputComplete);
                }
                level = "A";
                foreach (A _A in plantIn.A_List)
                {
                    FuelsRefineryPersonnelAbsenceHoursComplete _FuelsRefineryPersonnelAbsenseHoursComplete = new FuelsRefineryPersonnelAbsenceHoursComplete();
                    _FuelsRefineryPersonnelAbsenseHoursComplete.CategoryID = _A.A3;
                    _FuelsRefineryPersonnelAbsenseHoursComplete.ManagementProfessionalAndStaffAbsence = _A.A1;
                    _FuelsRefineryPersonnelAbsenseHoursComplete.OperatorCraftAndClericalAbsence = _A.A2;
                    _FuelsRefineryPersonnelAbsenseHoursComplete.SubmissionID = _A.A4;
                    retPlant.PersonnelAbsenceHoursCompleteList.Add(_FuelsRefineryPersonnelAbsenseHoursComplete);
             
                }
                level = "P";
                foreach (P _P in plantIn.P_List)
                {
                    FuelsRefineryPersonnelHoursComplete _FuelsRefineryPersonnelHoursComplete = new FuelsRefineryPersonnelHoursComplete();
                    _FuelsRefineryPersonnelHoursComplete.AbsenceHours = _P.P1;
                    _FuelsRefineryPersonnelHoursComplete.Contract = _P.P3;
                    _FuelsRefineryPersonnelHoursComplete.GeneralAndAdministrativeHours = _P.P2;
                    _FuelsRefineryPersonnelHoursComplete.NumberOfPersonnel = _P.P7;
                    _FuelsRefineryPersonnelHoursComplete.OvertimeHours = _P.P5;
                    _FuelsRefineryPersonnelHoursComplete.OvertimePercent = _P.P4;
                    _FuelsRefineryPersonnelHoursComplete.PersonnelHoursID = _P.P8;
                    _FuelsRefineryPersonnelHoursComplete.StraightTimeHours = _P.P6;
                    _FuelsRefineryPersonnelHoursComplete.SubmissionID = _P.P9;
                    retPlant.PersonnelHoursCompleteList.Add(_FuelsRefineryPersonnelHoursComplete);
                }
                level = "RP";
                foreach (RP _RP in plantIn.RP_List)
                {
                    FuelsRefineryProducedFuelResidualComplete _FuelsRefineryProducedFuelResidualComplete = new FuelsRefineryProducedFuelResidualComplete();
                    _FuelsRefineryProducedFuelResidualComplete.Density = _RP.RP1;
                    _FuelsRefineryProducedFuelResidualComplete.EnergyType = _RP.RP5;
                    _FuelsRefineryProducedFuelResidualComplete.SubmissionID = _RP.RP6;
                    _FuelsRefineryProducedFuelResidualComplete.Sulfur = _RP.RP4;
                    _FuelsRefineryProducedFuelResidualComplete.ViscosityCentiStokesAtTemperature = _RP.RP2;
                    _FuelsRefineryProducedFuelResidualComplete.ViscosityTemperature = _RP.RP3;
                    retPlant.RefineryProducedFuelResidualCompleteList.Add(_FuelsRefineryProducedFuelResidualComplete);
                }
                level = "MR";
                foreach (MR _MR in plantIn.MR_List)
                {
                    FuelsRefineryRoutineMaintenanceComplete _FuelsRefineryRoutineMaintenanceComplete = new FuelsRefineryRoutineMaintenanceComplete();
                    _FuelsRefineryRoutineMaintenanceComplete.CapitalInLocal = _MR.MR9;
                    _FuelsRefineryRoutineMaintenanceComplete.CostInLocal = _MR.MR11;
                    _FuelsRefineryRoutineMaintenanceComplete.ExpenseInLocal = _MR.MR10;
                    _FuelsRefineryRoutineMaintenanceComplete.InServicePercent = _MR.MR1;
                    _FuelsRefineryRoutineMaintenanceComplete.MaintenanceHoursDown = _MR.MR15;
                    _FuelsRefineryRoutineMaintenanceComplete.MaintenanceNumberOfDowntime = _MR.MR16;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOffsiteUpsets = _MR.MR4;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOther = _MR.MR3;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDown = _MR.MR13;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownEconomic = _MR.MR7;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownExternal = _MR.MR6;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownUnitUpsets = _MR.MR5;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherNumberOfDowntime = _MR.MR14;
                    _FuelsRefineryRoutineMaintenanceComplete.OtherSlowdownHours = _MR.MR12;
                    _FuelsRefineryRoutineMaintenanceComplete.OverheadInLocal = _MR.MR8;
                    _FuelsRefineryRoutineMaintenanceComplete.PercentUtilization = _MR.MR2;
                    _FuelsRefineryRoutineMaintenanceComplete.ProcessID = _MR.MR19;
                    _FuelsRefineryRoutineMaintenanceComplete.RegulatoryHoursDown = _MR.MR17;
                    _FuelsRefineryRoutineMaintenanceComplete.RegulatoryNumberOfDowntime = _MR.MR18;
                    _FuelsRefineryRoutineMaintenanceComplete.SubmissionID = _MR.MR21;
                    _FuelsRefineryRoutineMaintenanceComplete.UnitID = _MR.MR20;
                    retPlant.RoutineMaintenanceCompleteList.Add(_FuelsRefineryRoutineMaintenanceComplete);
                }
                level = "MT";
                foreach (MT _MT in plantIn.MT_List)
                {
                    FuelsRefineryTurnaroundMaintenanceComplete _FuelsRefineryTurnaroundMaintenanceComplete = new FuelsRefineryTurnaroundMaintenanceComplete();
                    _FuelsRefineryTurnaroundMaintenanceComplete.CapitalLocal = _MT.MT11;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ContractorManagementAndProfessionalSalaried = _MT.MT3;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ContractorOperatorCraftAndClericalHours = _MT.MT4;
                    _FuelsRefineryTurnaroundMaintenanceComplete.CostLocal = _MT.MT13;
                    _FuelsRefineryTurnaroundMaintenanceComplete.Exceptions = _MT.MT1;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ExpenseLocal = _MT.MT12;
                    _FuelsRefineryTurnaroundMaintenanceComplete.HoursDown = _MT.MT14;
                    _FuelsRefineryTurnaroundMaintenanceComplete.LaborCostLocal = _MT.MT9;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalOvertimePercent = _MT.MT5;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalSalariedStraightTimeHours = _MT.MT6;
                    _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalOvertimeHours = _MT.MT7;
                    _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalStraightTimeHours = _MT.MT8;
                    _FuelsRefineryTurnaroundMaintenanceComplete.OverheadLocal = _MT.MT10;
                    _FuelsRefineryTurnaroundMaintenanceComplete.PreviousTurnaroundDate = _MT.MT2;
                    _FuelsRefineryTurnaroundMaintenanceComplete.ProcessID = _MT.MT16;
                    _FuelsRefineryTurnaroundMaintenanceComplete.SubmissionID = _MT.MT18;
                    _FuelsRefineryTurnaroundMaintenanceComplete.TurnaroundDate = _MT.MT15;
                    _FuelsRefineryTurnaroundMaintenanceComplete.UnitID = _MT.MT17;
                    retPlant.TurnaroundMaintenanceCompleteList.Add(_FuelsRefineryTurnaroundMaintenanceComplete);
                }
                level = "I";
                foreach (I _I in plantIn.I_List)
                {
                    FuelsRefineryInventoryComplete _FuelsRefineryInventoryComplete = new FuelsRefineryInventoryComplete();
                    _FuelsRefineryInventoryComplete.AverageLevel = _I.I1;
                    _FuelsRefineryInventoryComplete.LeasedPercent = _I.I2;
                    _FuelsRefineryInventoryComplete.ManditoryStorageLevel = _I.I5;
                    _FuelsRefineryInventoryComplete.MarketingStorage = _I.I6;
                    _FuelsRefineryInventoryComplete.RefineryStorage = _I.I4;
                    _FuelsRefineryInventoryComplete.SubmissionID = _I.I9;
                    _FuelsRefineryInventoryComplete.TankNumber = _I.I3;
                    _FuelsRefineryInventoryComplete.TankType = _I.I8;
                    _FuelsRefineryInventoryComplete.TotalStorage = _I.I7;
                    retPlant.InventoryCompleteList.Add(_FuelsRefineryInventoryComplete);
                }
                level = "Y";
                foreach (Y _Y in plantIn.Y_List)
                {
                    FuelsRefineryMaterialBalanceComplete _FuelsRefineryMaterialBalanceComplete = new FuelsRefineryMaterialBalanceComplete();
                    _FuelsRefineryMaterialBalanceComplete.Barrels = _Y.Y3;
                    _FuelsRefineryMaterialBalanceComplete.Category = _Y.Y6;
                    _FuelsRefineryMaterialBalanceComplete.Density = _Y.Y1;
                    _FuelsRefineryMaterialBalanceComplete.MaterialID = _Y.Y5;
                    _FuelsRefineryMaterialBalanceComplete.MaterialName = _Y.Y4;
                    _FuelsRefineryMaterialBalanceComplete.MetricTons = _Y.Y2;
                    _FuelsRefineryMaterialBalanceComplete.Period = _Y.Y7;
                    _FuelsRefineryMaterialBalanceComplete.SubmissionID = _Y.Y8;
                    retPlant.MaterialBalanceCompleteList.Add(_FuelsRefineryMaterialBalanceComplete);
                }
                level = "O";
                foreach (O _O in plantIn.O_List)
                {
                    OperatingExpenseStandard _OperatingExpenseStandard = new OperatingExpenseStandard();
                    _OperatingExpenseStandard.OtherDescription = _O.O1;
                    _OperatingExpenseStandard.Property = _O.O3;
                    _OperatingExpenseStandard.ReportValue = _O.O2;
                    _OperatingExpenseStandard.SubmissionID = _O.O4;
                    retPlant.OperatingExpenseStandardList.Add(_OperatingExpenseStandard);
                }
                level = "PD";
                foreach (PD _PD in plantIn.PD_List)
                {
                    PlantOperatingConditionBasic _PlantOperatingConditionBasic = new PlantOperatingConditionBasic();
                    _PlantOperatingConditionBasic.Property = _PD.PD5;
                    _PlantOperatingConditionBasic.ReportDateValue = _PD.PD2;
                    _PlantOperatingConditionBasic.ReportNumericValue = _PD.PD4;
                    _PlantOperatingConditionBasic.ReportTextValue = _PD.PD3;
                    _PlantOperatingConditionBasic.SubmissionID = _PD.PD7;
                    _PlantOperatingConditionBasic.UnitID = _PD.PD6;
                    _PlantOperatingConditionBasic.UnitOfMeasure = _PD.PD1;
                    retPlant.PlantOperatingConditionList.Add(_PlantOperatingConditionBasic);
                }
                level = "SB";
                if (plantIn.SB_List.Count == 0)
                {
                    throw new Exception("No Submissions detected!");
                }
                foreach (SB _SB in plantIn.SB_List)
                {
                    SubmissionBasic _SubmissionBasic = new SubmissionBasic();
                    _SubmissionBasic.RefineryID = _SB.SB4;
                    _SubmissionBasic.ReportCurrency = _SB.SB1;
                    _SubmissionBasic.SubmissionDate = _SB.SB3;
                    _SubmissionBasic.SubmissionID = _SB.SB5;
                    _SubmissionBasic.UnitOfMeasure = _SB.SB2;
                    retPlant.PlantSubmission = _SubmissionBasic;
                    if (plantIn.SB_List.Count > 1)
                    {
                        throw new Exception("Multiple Submissions detected!");
                    }
                }
            }
            catch (Exception ex)
            {
                string template = "WCFToBus:Level={0} Error:{1}";
                Exception infoException = new Exception(string.Format(template, level, ex.Message));
                throw infoException;
            }

            return retPlant;
        }
        public PL BusToWCF(FuelsRefineryCompletePlant plantIn)
        {
            PL retVal = new PL();
            string level = "Absence";
            try
            {
                retVal.A_List = new List<A>();
                retVal.C_List = new List<C>();
                retVal.CB_List = new List<CB>();
                retVal.CD_List = new List<CD>();
                retVal.CR_List = new List<CR>();
                retVal.D_List = new List<D>();
                retVal.E_List = new List<E>();
                retVal.EG_List = new List<EG>();
                retVal.EL_List = new List<EL>();
                retVal.F_List = new List<F>();
                retVal.G_List = new List<G>();
                retVal.GM_List = new List<GM>();
                retVal.I_List = new List<I>();
                retVal.K_List = new List<K>();
                retVal.L_List = new List<L>();
                retVal.M_List = new List<M>();
                retVal.ME_List = new List<ME>();
                retVal.MI_List = new List<MI>();
                retVal.MR_List = new List<MR>();
                retVal.MT_List = new List<MT>();
                retVal.O_List = new List<O>();
                retVal.P_List = new List<P>();
                retVal.PD_List = new List<PD>();
                retVal.R_List = new List<R>();
                retVal.RP_List = new List<RP>();
                retVal.S_List = new List<S>();
                retVal.SB_List = new List<SB>();
                retVal.Y_List = new List<Y>();

                if(plantIn.PersonnelAbsenceHoursCompleteList.Count>0)
                {
                    foreach(FuelsRefineryPersonnelAbsenceHoursComplete _FuelsRefineryPersonnelAbsenceHoursComplete in plantIn.PersonnelAbsenceHoursCompleteList)
                    {
                        A _A = new A();
                        _A.A4 = _FuelsRefineryPersonnelAbsenceHoursComplete.SubmissionID; // Absence
                        _A.A3 = _FuelsRefineryPersonnelAbsenceHoursComplete.CategoryID; // Absence
                        _A.A2 = _FuelsRefineryPersonnelAbsenceHoursComplete.OperatorCraftAndClericalAbsence; // Absence
                        _A.A1 = _FuelsRefineryPersonnelAbsenceHoursComplete.ManagementProfessionalAndStaffAbsence; // Absence
                        retVal.A_List.Add(_A);
                    }
                }
               
                if(plantIn.ConfigurationCompleteList.Count>0)
                {
                    level = "Config";
                     foreach (FuelsRefineryConfigurationComplete _FuelsRefineryConfigurationComplete in plantIn.ConfigurationCompleteList)
                    {
                        C _C = new C();
                        _C.C16 = _FuelsRefineryConfigurationComplete.SubmissionID; // Config
                        _C.C15 = _FuelsRefineryConfigurationComplete.UnitID; // Config
                        _C.C14 = _FuelsRefineryConfigurationComplete.ProcessID; // Config
                        _C.C13 = _FuelsRefineryConfigurationComplete.UnitName; // Config
                        _C.C12 = _FuelsRefineryConfigurationComplete.ProcessType; // Config
                        _C.C11 = _FuelsRefineryConfigurationComplete.Capacity; // Config
                        _C.C10 = _FuelsRefineryConfigurationComplete.UtilizationPercent; // Config
                        _C.C9 = _FuelsRefineryConfigurationComplete.SteamCapacity; // Config
                        _C.C8 = _FuelsRefineryConfigurationComplete.SteamUtilizationPercent; // Config
                        _C.C7 = _FuelsRefineryConfigurationComplete.InServicePercent; // Config
                        _C.C6 = _FuelsRefineryConfigurationComplete.YearsInOperation; // Config
                        _C.C5 = _FuelsRefineryConfigurationComplete.ManHourWeek; // Config
                        _C.C4 = _FuelsRefineryConfigurationComplete.PostPerShift; // Config
                        _C.C3 = _FuelsRefineryConfigurationComplete.BlockOperated; // Config
                        _C.C2 = _FuelsRefineryConfigurationComplete.AllocatedPercentOfCapacity; // Config
                        _C.C1 = _FuelsRefineryConfigurationComplete.EnergyPercent; // Config
                        retVal.C_List.Add(_C);
                    }
                }
                if(plantIn.ConfigurationBuoyCompleteList.Count>0)
                {
                    level = "ConfigBuoy";
                    foreach (FuelsRefineryConfigurationBuoyComplete _FuelsRefineryConfigurationBuoyComplete in plantIn.ConfigurationBuoyCompleteList)
                    {
                        CB _CB = new CB();
                        _CB.CB7 = _FuelsRefineryConfigurationBuoyComplete.SubmissionID; // ConfigBuoy
                        _CB.CB6 = _FuelsRefineryConfigurationBuoyComplete.UnitID; // ConfigBuoy
                        _CB.CB5 = _FuelsRefineryConfigurationBuoyComplete.UnitName; // ConfigBuoy
                        _CB.CB4 = _FuelsRefineryConfigurationBuoyComplete.ProcessID; // ConfigBuoy
                        _CB.CB3 = _FuelsRefineryConfigurationBuoyComplete.ShipCapacity; // ConfigBuoy
                        _CB.CB2 = _FuelsRefineryConfigurationBuoyComplete.LineSize; // ConfigBuoy
                        _CB.CB1 = _FuelsRefineryConfigurationBuoyComplete.PercentOwnership; // ConfigBuoy
                        retVal.CB_List.Add(_CB);
                    }
                }
        
                if(plantIn.CrudeCompleteList.Count>0)
                {
                    level = "Crude";
                    foreach (FuelsRefineryCrudeComplete _FuelsRefineryCrudeComplete in plantIn.CrudeCompleteList)
                    {
                        CD _CD = new CD();
                        _CD.CD8 = _FuelsRefineryCrudeComplete .SubmissionID; // Crude
                        _CD.CD7 = _FuelsRefineryCrudeComplete .Period; // Crude
                        _CD.CD6 = _FuelsRefineryCrudeComplete .CrudeID; // Crude
                        _CD.CD5 = _FuelsRefineryCrudeComplete .CrudeNumber; // Crude
                        _CD.CD4 = _FuelsRefineryCrudeComplete .CrudeName; // Crude
                        _CD.CD3 = _FuelsRefineryCrudeComplete .Barrels; // Crude
                        _CD.CD2 = _FuelsRefineryCrudeComplete .Gravity; // Crude
                        _CD.CD1 = _FuelsRefineryCrudeComplete .Sulfur; // Crude
                        retVal.CD_List.Add(_CD);
                    }
                }

                if (plantIn.ConfigurationReceiptAndShipmentCompleteList.Count>0)                    
                {
                    level = "ConfigRS";
                    foreach (FuelsRefineryConfigurationReceiptAndShipmentComplete _FuelsRefineryConfigurationReceiptAndShipmentComplete in plantIn.ConfigurationReceiptAndShipmentCompleteList)
                    {
                        CR _CR = new CR();
                        _CR.CR6 = _FuelsRefineryConfigurationReceiptAndShipmentComplete.SubmissionID; // ConfigRS
                        _CR.CR5 = _FuelsRefineryConfigurationReceiptAndShipmentComplete.UnitID; // ConfigRS
                        _CR.CR4 = _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessID; // ConfigRS
                        _CR.CR3 = _FuelsRefineryConfigurationReceiptAndShipmentComplete.ProcessType; // ConfigRS
                        _CR.CR2 = _FuelsRefineryConfigurationReceiptAndShipmentComplete.AverageSize; // ConfigRS
                        _CR.CR1 = _FuelsRefineryConfigurationReceiptAndShipmentComplete.Throughput; // ConfigRS

                        retVal.CR_List.Add(_CR);
                    }
                }
                if (plantIn.DieselCompleteList.Count>0)
                {
                    level = "Diesel";
                    foreach (FuelsRefineryDieselComplete _FuelsRefineryDieselComplete in plantIn.DieselCompleteList)
                    {
                        D _D = new D();
                        _D.D14 = _FuelsRefineryDieselComplete.SubmissionID; // Diesel
                        _D.D13 = _FuelsRefineryDieselComplete.BlendID; // Diesel
                        _D.D12 = _FuelsRefineryDieselComplete.Grade; // Diesel
                        _D.D11 = _FuelsRefineryDieselComplete.Market; // Diesel
                        _D.D10 = _FuelsRefineryDieselComplete.Type; // Diesel
                        _D.D9 = _FuelsRefineryDieselComplete.Density; // Diesel
                        _D.D8 = _FuelsRefineryDieselComplete.Cetane; // Diesel
                        _D.D7 = _FuelsRefineryDieselComplete.PourPointOfBlend; // Diesel
                        _D.D6 = _FuelsRefineryDieselComplete.Sulfur; // Diesel
                        _D.D5 = _FuelsRefineryDieselComplete.CloudPointOfBlend; // Diesel
                        _D.D4 = _FuelsRefineryDieselComplete.AmericanStandardForTestingMaterials90DistillationPoint; // Diesel
                        _D.D3 = _FuelsRefineryDieselComplete.PercentEvaporatedAt350C; // Diesel
                        _D.D2 = _FuelsRefineryDieselComplete.BiodieselInBlendPercent; // Diesel
                        _D.D1 = _FuelsRefineryDieselComplete.ThousandMetricTons; // Diesel

                        retVal.D_List.Add(_D);
                    }
                }
                if (plantIn.EmissionCompleteList.Count>0)
                {
                    level = "Emissions";
                     foreach (FuelsRefineryEmissionComplete _FuelsRefineryEmissionComplete in plantIn.EmissionCompleteList)
                    {
                        E _E = new E();
                        _E.E3 = _FuelsRefineryEmissionComplete.SubmissionID; // Emissions
                        _E.E2 = _FuelsRefineryEmissionComplete.EmissionType; // Emissions
                        _E.E1 = _FuelsRefineryEmissionComplete.RefineryEmission; // Emissions

                        retVal.E_List.Add(_E);
                    }
                }
                if (plantIn.ElectricBasicList.Count>0)
                {
                    level = "Electric";
                    foreach (FuelsRefineryEnergyElectricBasic _FuelsRefineryEnergyElectricBasic in plantIn.ElectricBasicList)
                    {                    
                        EL _EL = new EL();
                        _EL.EL6 = _FuelsRefineryEnergyElectricBasic.SubmissionID; // Electric
                        _EL.EL5 = _FuelsRefineryEnergyElectricBasic.EnergyType; // Electric
                        _EL.EL4 = _FuelsRefineryEnergyElectricBasic.TransactionType; // Electric
                        _EL.EL3 = _FuelsRefineryEnergyElectricBasic.Amount; // Electric
                        _EL.EL2 = _FuelsRefineryEnergyElectricBasic.PriceLocal; // Electric
                        _EL.EL1 = _FuelsRefineryEnergyElectricBasic.GenerationEfficiency; // Electric                       
                        retVal.EL_List.Add(_EL);

                    }
                }
                if (plantIn.EnergyCompleteList.Count>0)
                {
                    level = "Energy";
                    foreach (FuelsRefineryEnergyComplete _FuelsRefineryEnergyComplete in plantIn.EnergyCompleteList)
                    {
                        EG _EG = new EG();
                        _EG.EG24 = _FuelsRefineryEnergyComplete.SubmissionID; // Energy
                        _EG.EG23 = _FuelsRefineryEnergyComplete.EnergyType; // Energy
                        _EG.EG22 = _FuelsRefineryEnergyComplete.TransactionType; // Energy
                        _EG.EG21 = _FuelsRefineryEnergyComplete.Amount; // Energy
                        _EG.EG20 = _FuelsRefineryEnergyComplete.PriceLocal; // Energy
                        _EG.EG19 = _FuelsRefineryEnergyComplete.MillionBritishThermalUnitsOut; // Energy
                        _EG.EG18 = _FuelsRefineryEnergyComplete.OverrideCalculation; // Energy
                        //moved to electric _EG.EG17 = _electric_2.GenerationEfficiency; // Energy
                        _EG.EG16 = _FuelsRefineryEnergyComplete.Hydrogen; // Energy
                        _EG.EG15 = _FuelsRefineryEnergyComplete.Methane; // Energy
                        _EG.EG14 = _FuelsRefineryEnergyComplete.Ethane; // Energy
                        _EG.EG13 = _FuelsRefineryEnergyComplete.Ethylene; // Energy
                        _EG.EG12 = _FuelsRefineryEnergyComplete.Propane; // Energy
                        _EG.EG11 = _FuelsRefineryEnergyComplete.Propylene; // Energy
                        _EG.EG10 = _FuelsRefineryEnergyComplete.Butane; // Energy
                        _EG.EG9 = _FuelsRefineryEnergyComplete.Isobutane; // Energy
                        _EG.EG8 = _FuelsRefineryEnergyComplete.Butylenes; // Energy
                        _EG.EG7 = _FuelsRefineryEnergyComplete.C5Plus; // Energy
                        _EG.EG6 = _FuelsRefineryEnergyComplete.H2S; // Energy
                        _EG.EG5 = _FuelsRefineryEnergyComplete.CO; // Energy
                        _EG.EG4 = _FuelsRefineryEnergyComplete.NH3; // Energy
                        _EG.EG3 = _FuelsRefineryEnergyComplete.SO2; // Energy
                        _EG.EG2 = _FuelsRefineryEnergyComplete.CO2; // Energy
                        _EG.EG1 = _FuelsRefineryEnergyComplete.N2; // Energy
                        retVal.EG_List.Add(_EG);
                     }
                }
                

                if (plantIn.FiredHeatersCompleteList.Count>0)
                {
                    level = "FiredHeaters";
                    foreach (FuelsRefineryFiredHeatersComplete _FuelsRefineryFiredHeatersComplete in plantIn.FiredHeatersCompleteList) 
                    {
                        F _F = new F();
                        _F.F24 = _FuelsRefineryFiredHeatersComplete.SubmissionID; // FiredHeaters
                        _F.F23 = _FuelsRefineryFiredHeatersComplete.HeaterNumber; // FiredHeaters
                        _F.F22 = _FuelsRefineryFiredHeatersComplete.UnitID; // FiredHeaters
                        _F.F21 = _FuelsRefineryFiredHeatersComplete.ProcessID; // FiredHeaters
                        _F.F20 = _FuelsRefineryFiredHeatersComplete.HeaterName; // FiredHeaters
                        _F.F19 = _FuelsRefineryFiredHeatersComplete.Service; // FiredHeaters
                        _F.F18 = _FuelsRefineryFiredHeatersComplete.ProcessFluid; // FiredHeaters
                        _F.F17 = _FuelsRefineryFiredHeatersComplete.ThroughputReport; // FiredHeaters
                        _F.F16 = _FuelsRefineryFiredHeatersComplete.ThroughputUnitOfMeasure; // FiredHeaters
                        _F.F15 = _FuelsRefineryFiredHeatersComplete.FiredDuty; // FiredHeaters
                        _F.F14 = _FuelsRefineryFiredHeatersComplete.FuelType; // FiredHeaters
                        _F.F13 = _FuelsRefineryFiredHeatersComplete.OtherCombustionDuty; // FiredHeaters
                        _F.F12 = _FuelsRefineryFiredHeatersComplete.FurnaceInTemperature; // FiredHeaters
                        _F.F11 = _FuelsRefineryFiredHeatersComplete.FurnaceOutTemperature; // FiredHeaters
                        _F.F10 = _FuelsRefineryFiredHeatersComplete.StackTemperature; // FiredHeaters
                        _F.F9 = _FuelsRefineryFiredHeatersComplete.StackO2; // FiredHeaters
                        _F.F8 = _FuelsRefineryFiredHeatersComplete.HeatLossPercent; // FiredHeaters
                        _F.F7 = _FuelsRefineryFiredHeatersComplete.CombustionAirTemperature; // FiredHeaters
                        _F.F6 = _FuelsRefineryFiredHeatersComplete.AbsorbedDuty; // FiredHeaters
                        _F.F5 = _FuelsRefineryFiredHeatersComplete.ProcessDuty; // FiredHeaters
                        _F.F4 = _FuelsRefineryFiredHeatersComplete.SteamDuty; // FiredHeaters
                        _F.F3 = _FuelsRefineryFiredHeatersComplete.SteamSuperHeated; // FiredHeaters
                        _F.F2 = _FuelsRefineryFiredHeatersComplete.ShaftDuty; // FiredHeaters
                        _F.F1 = _FuelsRefineryFiredHeatersComplete.OtherDuty; // FiredHeaters

                        retVal.F_List.Add(_F);
                    }
                }
                if (plantIn.GasolineCompleteList.Count>0)
                {
                    level = "Gasoline";
                     foreach (FuelsRefineryGasolineComplete _FuelsRefineryGasolineComplete in plantIn.GasolineCompleteList)
                    {
                        G _G = new G();
                        _G.G18 = _FuelsRefineryGasolineComplete.SubmissionID; // Gasoline
                        _G.G17 = _FuelsRefineryGasolineComplete.BlendID; // Gasoline
                        _G.G16 = _FuelsRefineryGasolineComplete.Grade; // Gasoline
                        _G.G15 = _FuelsRefineryGasolineComplete.Market; // Gasoline
                        _G.G14 = _FuelsRefineryGasolineComplete.Type; // Gasoline
                        _G.G13 = _FuelsRefineryGasolineComplete.Density; // Gasoline
                        _G.G12 = _FuelsRefineryGasolineComplete.ReidVaporPressure; // Gasoline
                        _G.G11 = _FuelsRefineryGasolineComplete.ResearchOctaneNumber; // Gasoline
                        _G.G10 = _FuelsRefineryGasolineComplete.MotorOctaneNumber; // Gasoline
                        _G.G9 = _FuelsRefineryGasolineComplete.Oxygen; // Gasoline
                        _G.G8 = _FuelsRefineryGasolineComplete.Ethanol; // Gasoline
                        _G.G7 = _FuelsRefineryGasolineComplete.MethylTertButylEther; // Gasoline
                        _G.G6 = _FuelsRefineryGasolineComplete.EthylTertButylEther; // Gasoline
                        _G.G5 = _FuelsRefineryGasolineComplete.TertAmylMethylEther; // Gasoline
                        _G.G4 = _FuelsRefineryGasolineComplete.OtherOxygen; // Gasoline
                        _G.G3 = _FuelsRefineryGasolineComplete.Sulfur; // Gasoline
                        _G.G2 = _FuelsRefineryGasolineComplete.Lead; // Gasoline
                        _G.G1 = _FuelsRefineryGasolineComplete.ThousandMetricTons; // Gasoline
                        retVal.G_List.Add(_G);
                    }
                }

                if (plantIn.GeneralMiscellaneousCompleteList.Count>0)
                {
                    level = "GeneralMisc";
                     foreach (FuelsRefineryGeneralMiscellaneousComplete _FuelsRefineryGeneralMiscellaneousComplete in plantIn.GeneralMiscellaneousCompleteList)
                    {
                        GM _GM = new GM();

                         _GM.GM3 = _FuelsRefineryGeneralMiscellaneousComplete.SubmissionID; // GeneralMisc
                        _GM.GM2 = _FuelsRefineryGeneralMiscellaneousComplete.FlareLossMetricTons; // GeneralMisc
                        _GM.GM1 = _FuelsRefineryGeneralMiscellaneousComplete.TotalLossMetricTons; // GeneralMisc
                        retVal.GM_List.Add(_GM);
                    }
                }
                if (plantIn.InventoryCompleteList.Count>0)
                {
                    level = "Inventory";
                    foreach (FuelsRefineryInventoryComplete _FuelsRefineryInventoryComplete in plantIn.InventoryCompleteList)
                    {
                        I _I = new I();
                        _I.I9 = _FuelsRefineryInventoryComplete.SubmissionID; // Inventory
                        _I.I8 = _FuelsRefineryInventoryComplete.TankType; // Inventory
                        _I.I7 = _FuelsRefineryInventoryComplete.TotalStorage; // Inventory
                        _I.I6 = _FuelsRefineryInventoryComplete.MarketingStorage; // Inventory
                        _I.I5 = _FuelsRefineryInventoryComplete.ManditoryStorageLevel; // Inventory
                        _I.I4 = _FuelsRefineryInventoryComplete.RefineryStorage; // Inventory
                        _I.I3 = _FuelsRefineryInventoryComplete.TankNumber; // Inventory
                        _I.I2 = _FuelsRefineryInventoryComplete.LeasedPercent; // Inventory
                        _I.I1 = _FuelsRefineryInventoryComplete.AverageLevel; // Inventory

                        retVal.I_List.Add(_I);
                    }
                }
                if (plantIn.KeroseneCompleteList.Count>0)
                {
                    level = "Kerosene";
                    foreach (FuelsRefineryKeroseneComplete _FuelsRefineryKeroseneComplete in plantIn.KeroseneCompleteList)
                    {
                        K _K = new K();
                        _K.K7 = _FuelsRefineryKeroseneComplete.SubmissionID; // Kerosene
                        _K.K6 = _FuelsRefineryKeroseneComplete.BlendID; // Kerosene
                        _K.K5 = _FuelsRefineryKeroseneComplete.Grade; // Kerosene
                        _K.K4 = _FuelsRefineryKeroseneComplete.Type; // Kerosene
                        _K.K3 = _FuelsRefineryKeroseneComplete.Density; // Kerosene
                        _K.K2 = _FuelsRefineryKeroseneComplete.Sulfur; // Kerosene
                        _K.K1 = _FuelsRefineryKeroseneComplete.ThousandMetricTons; // Kerosene

                        retVal.K_List.Add(_K);
                    }
                }
                if (plantIn.LiquifiedPetroleumGasCompleteList.Count>0)
                {
                    level = "LPG";
                     foreach (FuelsRefineryLiquifiedPetroleumGasComplete _FuelsRefineryLiquifiedPetroleumGasComplete in plantIn.LiquifiedPetroleumGasCompleteList)
                    {
                        L _L = new L();

                        _L.L10 = _FuelsRefineryLiquifiedPetroleumGasComplete.SubmissionID; // LPG
                        _L.L9 = _FuelsRefineryLiquifiedPetroleumGasComplete.BlendID; // LPG
                        _L.L8 = _FuelsRefineryLiquifiedPetroleumGasComplete.MoleOrVolume; // LPG
                        _L.L7 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC2LT; // LPG
                        _L.L6 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3; // LPG
                        _L.L5 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC3ene; // LPG
                        _L.L4 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeiC4; // LPG
                        _L.L3 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumenC4; // LPG
                        _L.L2 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC4ene; // LPG
                        _L.L1 = _FuelsRefineryLiquifiedPetroleumGasComplete.VolumeC5Plus; // LPG
                        retVal.L_List.Add(_L);
                    }
                }
                if (plantIn.MarineBunkersCompleteList.Count>0)
                {
                    level = "MarineBunkers";
                    foreach (FuelsRefineryMarineBunkersComplete _FuelsRefineryMarineBunkersComplete in plantIn.MarineBunkersCompleteList)
                    {
                        M _M = new M();

                        _M.M10 = _FuelsRefineryMarineBunkersComplete.SubmissionID; // MarineBunkers
                        _M.M9 = _FuelsRefineryMarineBunkersComplete.BlendID; // MarineBunkers
                        _M.M8 = _FuelsRefineryMarineBunkersComplete.Grade; // MarineBunkers
                        _M.M7 = _FuelsRefineryMarineBunkersComplete.Density; // MarineBunkers
                        _M.M6 = _FuelsRefineryMarineBunkersComplete.Sulfur; // MarineBunkers
                        _M.M5 = _FuelsRefineryMarineBunkersComplete.PourPointOfBlend; // MarineBunkers
                        _M.M4 = _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendCentiStokeAtTemperature; // MarineBunkers
                        _M.M3 = _FuelsRefineryMarineBunkersComplete.ViscosityOfBlendTemperatureDifferent122; // MarineBunkers
                        _M.M2 = _FuelsRefineryMarineBunkersComplete.CrackedStock; // MarineBunkers
                        _M.M1 = _FuelsRefineryMarineBunkersComplete.ThousandMetricTons; // MarineBunkers
                        retVal.M_List.Add(_M);
                    }
                }
                if (plantIn.MaintenanceExpenseAndCapitalCompleteList.Count>0)
                {
                    level = "MExp";
                     foreach (FuelsRefineryMaintenanceExpenseAndCapitalComplete _FuelsRefineryMaintenanceExpenseAndCapitalComplete in plantIn.MaintenanceExpenseAndCapitalCompleteList)
                    {
                        ME _ME = new ME();
                        _ME.ME22 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SubmissionID; // MExp
                        _ME.ME21 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalComplexCapital; // MExp
                        _ME.ME20 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRefineryExclusions; // MExp
                        _ME.ME19 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TurnaroundMaintenanceCapital; // MExp
                        _ME.ME18 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RoutineMaintenanceCapital; // MExp
                        _ME.ME17 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonMaintenenceInvestmentExpense; // MExp
                        _ME.ME16 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.NonRegulatoryNewProcessUnitConstruction; // MExp
                        _ME.ME15 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.ConstraintRemoval; // MExp
                        _ME.ME14 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryExpense; // MExp
                        _ME.ME13 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryGasoline; // MExp
                        _ME.ME12 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryDiesel; // MExp
                        _ME.ME11 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.RegulatoryOther; // MExp
                        _ME.ME10 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.SafetyOther; // MExp
                        _ME.ME9 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.Energy; // MExp
                        _ME.ME8 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.OtherCapitalInvestment; // MExp
                        _ME.ME7 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseTurnaround; // MExp
                        _ME.ME6 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceExpenseRoutine; // MExp
                        _ME.ME5 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenance; // MExp
                        _ME.ME4 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadTurnaround; // MExp
                        _ME.ME3 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.MaintenanceOverheadRoutine; // MExp
                        _ME.ME2 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceOverhead; // MExp
                        _ME.ME1 = _FuelsRefineryMaintenanceExpenseAndCapitalComplete.TotalMaintenanceExpense; // MExp

                        retVal.ME_List.Add(_ME);
                    }
                }
                if (plantIn.MiscellaneousInputCompleteList.Count>0)
                {
                    level = "MiscInput";
                    foreach (FuelsRefineryMiscellaneousInputComplete _FuelsRefineryMiscellaneousInputComplete in plantIn.MiscellaneousInputCompleteList)
                    {
                        MI _MI = new MI();
                        _MI.MI4 = _FuelsRefineryMiscellaneousInputComplete.SubmissionID; // MiscInput
                        _MI.MI3 = _FuelsRefineryMiscellaneousInputComplete.BarrelsInputToCrudeUnits; // MiscInput
                        _MI.MI2 = _FuelsRefineryMiscellaneousInputComplete.MetricTonsInputToCrudeUnits; // MiscInput
                        _MI.MI1 = _FuelsRefineryMiscellaneousInputComplete.OffsiteEnergyPercent; // MiscInput
                        retVal.MI_List.Add(_MI);
                    }
                }
                if (plantIn.RoutineMaintenanceCompleteList.Count>0)
                {
                    level = "MaintRout";
                    foreach (FuelsRefineryRoutineMaintenanceComplete _FuelsRefineryRoutineMaintenanceComplete in plantIn.RoutineMaintenanceCompleteList)
                    {
                        MR _MR = new MR();
                        _MR.MR21 = _FuelsRefineryRoutineMaintenanceComplete.SubmissionID; // MaintRout
                        _MR.MR20 = _FuelsRefineryRoutineMaintenanceComplete.UnitID; // MaintRout
                        _MR.MR19 = _FuelsRefineryRoutineMaintenanceComplete.ProcessID; // MaintRout
                        _MR.MR18 = _FuelsRefineryRoutineMaintenanceComplete.RegulatoryNumberOfDowntime; // MaintRout
                        _MR.MR17 = _FuelsRefineryRoutineMaintenanceComplete.RegulatoryHoursDown; // MaintRout
                        _MR.MR16 = _FuelsRefineryRoutineMaintenanceComplete.MaintenanceNumberOfDowntime; // MaintRout
                        _MR.MR15 = _FuelsRefineryRoutineMaintenanceComplete.MaintenanceHoursDown; // MaintRout
                        _MR.MR14 = _FuelsRefineryRoutineMaintenanceComplete.OtherNumberOfDowntime; // MaintRout
                        _MR.MR13 = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDown; // MaintRout
                        _MR.MR12 = _FuelsRefineryRoutineMaintenanceComplete.OtherSlowdownHours; // MaintRout
                        _MR.MR11 = _FuelsRefineryRoutineMaintenanceComplete.CostInLocal; // MaintRout
                        _MR.MR10 = _FuelsRefineryRoutineMaintenanceComplete.ExpenseInLocal; // MaintRout
                        _MR.MR9 = _FuelsRefineryRoutineMaintenanceComplete.CapitalInLocal; // MaintRout
                        _MR.MR8 = _FuelsRefineryRoutineMaintenanceComplete.OverheadInLocal; // MaintRout
                        _MR.MR7 = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownEconomic; // MaintRout
                        _MR.MR6 = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownExternal; // MaintRout
                        _MR.MR5 = _FuelsRefineryRoutineMaintenanceComplete.OtherHoursDownUnitUpsets; // MaintRout
                        _MR.MR4 = _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOffsiteUpsets; // MaintRout
                        _MR.MR3 = _FuelsRefineryRoutineMaintenanceComplete.OtherDownHoursOther; // MaintRout
                        _MR.MR2 = _FuelsRefineryRoutineMaintenanceComplete.PercentUtilization; // MaintRout
                        _MR.MR1 = _FuelsRefineryRoutineMaintenanceComplete.InServicePercent; // MaintRout

                        retVal.MR_List.Add(_MR);
                    }
                }
                if (plantIn.TurnaroundMaintenanceCompleteList.Count>0)
                {
                    level = "MaintTA";
                    foreach (FuelsRefineryTurnaroundMaintenanceComplete _FuelsRefineryTurnaroundMaintenanceComplete in plantIn.TurnaroundMaintenanceCompleteList)
                    {
                        MT _MT = new MT();
                        _MT.MT18 = _FuelsRefineryTurnaroundMaintenanceComplete.SubmissionID; // MaintTA
                        _MT.MT17 = _FuelsRefineryTurnaroundMaintenanceComplete.UnitID; // MaintTA
                        _MT.MT16 = _FuelsRefineryTurnaroundMaintenanceComplete.ProcessID; // MaintTA
                        _MT.MT15 = _FuelsRefineryTurnaroundMaintenanceComplete.TurnaroundDate; // MaintTA
                        _MT.MT14 = _FuelsRefineryTurnaroundMaintenanceComplete.HoursDown; // MaintTA
                        _MT.MT13 = _FuelsRefineryTurnaroundMaintenanceComplete.CostLocal; // MaintTA
                        _MT.MT12 = _FuelsRefineryTurnaroundMaintenanceComplete.ExpenseLocal; // MaintTA
                        _MT.MT11 = _FuelsRefineryTurnaroundMaintenanceComplete.CapitalLocal; // MaintTA
                        _MT.MT10 = _FuelsRefineryTurnaroundMaintenanceComplete.OverheadLocal; // MaintTA
                        _MT.MT9 = _FuelsRefineryTurnaroundMaintenanceComplete.LaborCostLocal; // MaintTA
                        _MT.MT8 = _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalStraightTimeHours; // MaintTA
                        _MT.MT7 = _FuelsRefineryTurnaroundMaintenanceComplete.OperatorCraftAndClericalOvertimeHours; // MaintTA
                        _MT.MT6 = _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalSalariedStraightTimeHours; // MaintTA
                        _MT.MT5 = _FuelsRefineryTurnaroundMaintenanceComplete.ManagementAndProfessionalOvertimePercent; // MaintTA
                        _MT.MT4 = _FuelsRefineryTurnaroundMaintenanceComplete.ContractorOperatorCraftAndClericalHours; // MaintTA
                        _MT.MT3 = _FuelsRefineryTurnaroundMaintenanceComplete.ContractorManagementAndProfessionalSalaried; // MaintTA
                        _MT.MT2 = _FuelsRefineryTurnaroundMaintenanceComplete.PreviousTurnaroundDate; // MaintTA
                        _MT.MT1 = _FuelsRefineryTurnaroundMaintenanceComplete.Exceptions; // MaintTA

                        retVal.MT_List.Add(_MT);
                    }
                }
                if (plantIn.OperatingExpenseStandardList.Count>0)
                {
                    level = "OpexData";
                     foreach (OperatingExpenseStandard _OperatingExpenseStandard in plantIn.OperatingExpenseStandardList)
                    {
                        O _O = new O();
                        _O.O4 = _OperatingExpenseStandard.SubmissionID; // OpexData
                        _O.O3 = _OperatingExpenseStandard.Property; // OpexData
                        _O.O2 = _OperatingExpenseStandard.ReportValue; // OpexData
                        _O.O1 = _OperatingExpenseStandard.OtherDescription; // OpexData

                        retVal.O_List.Add(_O);
                    }
                }
                if (plantIn.PersonnelHoursCompleteList.Count>0)
                {
                    level = "Pers";
                     foreach (FuelsRefineryPersonnelHoursComplete _FuelsRefineryPersonnelHoursComplete in plantIn.PersonnelHoursCompleteList)
                    {
                        P _P = new P();

                        _P.P9 = _FuelsRefineryPersonnelHoursComplete.SubmissionID; // Pers
                        _P.P8 = _FuelsRefineryPersonnelHoursComplete.PersonnelHoursID; // Pers
                        _P.P7 = _FuelsRefineryPersonnelHoursComplete.NumberOfPersonnel; // Pers
                        _P.P6 = _FuelsRefineryPersonnelHoursComplete.StraightTimeHours; // Pers
                        _P.P5 = _FuelsRefineryPersonnelHoursComplete.OvertimeHours; // Pers
                        _P.P4 = _FuelsRefineryPersonnelHoursComplete.OvertimePercent; // Pers
                        _P.P3 = _FuelsRefineryPersonnelHoursComplete.Contract; // Pers
                        _P.P2 = _FuelsRefineryPersonnelHoursComplete.GeneralAndAdministrativeHours; // Pers
                        _P.P1 = _FuelsRefineryPersonnelHoursComplete.AbsenceHours; // Pers
                        retVal.P_List.Add(_P);
                    }
                }
                if (plantIn.PlantOperatingConditionList.Count>0)
                {
                    level = "ProcessData";
                    foreach (PlantOperatingConditionBasic _PlantOperatingConditionBasic in plantIn.PlantOperatingConditionList)
                    {
                        PD _PD = new PD();
                        _PD.PD7 = _PlantOperatingConditionBasic.SubmissionID; // ProcessData
                        _PD.PD6 = _PlantOperatingConditionBasic.UnitID; // ProcessData
                        _PD.PD5 = _PlantOperatingConditionBasic.Property; // ProcessData
                        _PD.PD4 = _PlantOperatingConditionBasic.ReportNumericValue; // ProcessData
                        _PD.PD3 = _PlantOperatingConditionBasic.ReportTextValue; // ProcessData
                        _PD.PD2 = _PlantOperatingConditionBasic.ReportDateValue; // ProcessData
                        _PD.PD1 = _PlantOperatingConditionBasic.UnitOfMeasure; // ProcessData

                        retVal.PD_List.Add(_PD);
                    }
                }
                if (plantIn.FinishedResidualFuelCompleteList.Count>0)
                {
                    level = "Resid";
                       foreach (FuelsRefineryFinishedResidualFuelComplete _FuelsRefineryFinishedResidualFuelComplete in plantIn.FinishedResidualFuelCompleteList)
                       {
                        R _R = new R();
                        _R.R9 = _FuelsRefineryFinishedResidualFuelComplete.SubmissionID; // Resid
                        _R.R8 = _FuelsRefineryFinishedResidualFuelComplete.BlendID; // Resid
                        _R.R7 = _FuelsRefineryFinishedResidualFuelComplete.Grade; // Resid
                        _R.R6 = _FuelsRefineryFinishedResidualFuelComplete.Density; // Resid
                        _R.R5 = _FuelsRefineryFinishedResidualFuelComplete.Sulfur; // Resid
                        _R.R4 = _FuelsRefineryFinishedResidualFuelComplete.PourPointOfBlend; // Resid
                        _R.R3 = _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendCentiStokeAtTemperature; // Resid
                        _R.R2 = _FuelsRefineryFinishedResidualFuelComplete.ViscosityOfBlendTemperatureDifferent122; // Resid
                        _R.R1 = _FuelsRefineryFinishedResidualFuelComplete.ThousandMetricTons; // Resid
                        retVal.R_List.Add(_R);
                    }
                }
                if (plantIn.RefineryProducedFuelResidualCompleteList.Count>0)
                {
                    level = "RPFResid";
                   foreach (FuelsRefineryProducedFuelResidualComplete _FuelsRefineryProducedFuelResidualComplete in plantIn.RefineryProducedFuelResidualCompleteList)               
                    {
                        RP _RP = new RP();

                        _RP.RP6 = _FuelsRefineryProducedFuelResidualComplete.SubmissionID; // RPFResid
                        _RP.RP5 = _FuelsRefineryProducedFuelResidualComplete.EnergyType; // RPFResid
                        _RP.RP4 = _FuelsRefineryProducedFuelResidualComplete.Sulfur; // RPFResid
                        _RP.RP3 = _FuelsRefineryProducedFuelResidualComplete.ViscosityTemperature; // RPFResid
                        _RP.RP2 = _FuelsRefineryProducedFuelResidualComplete.ViscosityCentiStokesAtTemperature; // RPFResid
                        _RP.RP1 = _FuelsRefineryProducedFuelResidualComplete.Density; // RPFResid

                        retVal.RP_List.Add(_RP);
                    }
                }

                if (plantIn.SteamSystemCompleteList.Count>0)
                {
                    level = "SteamSystem";
                    foreach (FuelsRefinerySteamSystemComplete _FuelsRefinerySteamSystemComplete in plantIn.SteamSystemCompleteList)
                    {
                        S _S = new S();

                        _S.S38 = _FuelsRefinerySteamSystemComplete.SubmissionID; // SteamSystem
                        _S.S37 = _FuelsRefinerySteamSystemComplete.PressureRange; // SteamSystem
                        _S.S36 = _FuelsRefinerySteamSystemComplete.ActualPressure; // SteamSystem
                        _S.S35 = _FuelsRefinerySteamSystemComplete.H2PlantNetExportSteam; // SteamSystem
                        _S.S34 = _FuelsRefinerySteamSystemComplete.FiredProcessHeaterConvectionSection; // SteamSystem
                        _S.S33 = _FuelsRefinerySteamSystemComplete.FiredBoiler; // SteamSystem
                        _S.S32 = _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerCoolers; // SteamSystem
                        _S.S31 = _FuelsRefinerySteamSystemComplete.FluidCatalystCrackerStackGas; // SteamSystem
                        _S.S30 = _FuelsRefinerySteamSystemComplete.FluidCokerCOBoiler; // SteamSystem
                        _S.S29 = _FuelsRefinerySteamSystemComplete.Calciner; // SteamSystem
                        _S.S28 = _FuelsRefinerySteamSystemComplete.FiredTurbineCogeneration; // SteamSystem
                        _S.S27 = _FuelsRefinerySteamSystemComplete.WasteHeatFluidCatalystCracker; // SteamSystem
                        _S.S26 = _FuelsRefinerySteamSystemComplete.WasteHeatThermalCracker; // SteamSystem
                        _S.S25 = _FuelsRefinerySteamSystemComplete.WasteHeatCokerHeavyGasOilPumparound; // SteamSystem
                        _S.S24 = _FuelsRefinerySteamSystemComplete.WasteHeatOtherBoilers; // SteamSystem
                        _S.S23 = _FuelsRefinerySteamSystemComplete.OtherSteamSources; // SteamSystem
                        _S.S22 = _FuelsRefinerySteamSystemComplete.SubtotalSteamProduction; // SteamSystem
                        _S.S21 = _FuelsRefinerySteamSystemComplete.Sold; // SteamSystem
                        _S.S20 = _FuelsRefinerySteamSystemComplete.Purchased; // SteamSystem
                        _S.S19 = _FuelsRefinerySteamSystemComplete.SubtotalNetPurchased; // SteamSystem
                        _S.S18 = _FuelsRefinerySteamSystemComplete.TotalSupply; // SteamSystem
                        _S.S17 = _FuelsRefinerySteamSystemComplete.ConsumedProcessCrudeAndVacuum; // SteamSystem
                        _S.S16 = _FuelsRefinerySteamSystemComplete.ConsumedProcessCoker; // SteamSystem
                        _S.S15 = _FuelsRefinerySteamSystemComplete.ConsumedProcessFluidCatalystCracker; // SteamSystem
                        _S.S14 = _FuelsRefinerySteamSystemComplete.ConsumedProcessOther; // SteamSystem
                        _S.S13 = _FuelsRefinerySteamSystemComplete.ConsumedReboilersEvaporators; // SteamSystem
                        _S.S12 = _FuelsRefinerySteamSystemComplete.ConsumedOtherHeaters; // SteamSystem
                        _S.S11 = _FuelsRefinerySteamSystemComplete.ConsumedCondensingTurbines; // SteamSystem
                        _S.S10 = _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionFromHigherPressure; // SteamSystem
                        _S.S9 = _FuelsRefinerySteamSystemComplete.ConsumedToppingExtractionToLowerPressure; // SteamSystem
                        _S.S8 = _FuelsRefinerySteamSystemComplete.ConsumedCombustionAirPreheat; // SteamSystem
                        _S.S7 = _FuelsRefinerySteamSystemComplete.ConsumedPressureControlFromHigherPressure; // SteamSystem
                        _S.S6 = _FuelsRefinerySteamSystemComplete.ConsumedPressureControlToLowerPressure; // SteamSystem
                        _S.S5 = _FuelsRefinerySteamSystemComplete.ConsumedSteamTracingTankBuildingHeat; // SteamSystem
                        _S.S4 = _FuelsRefinerySteamSystemComplete.ConsumedDeaerators; // SteamSystem
                        _S.S3 = _FuelsRefinerySteamSystemComplete.ConsumedSteamToFlares; // SteamSystem
                        _S.S2 = _FuelsRefinerySteamSystemComplete.ConsumedAllOther; // SteamSystem
                        _S.S1 = _FuelsRefinerySteamSystemComplete.ConsumedTotalSteam; // SteamSystem
                        retVal.S_List.Add(_S);
                    }
                }
                if (plantIn.PlantSubmission != null)
                {
                    level = "Submissions";
                    SubmissionBasic _SubmissionBasic = plantIn.PlantSubmission;
          
                        SB _SB = new SB();
                        _SB.SB5 = _SubmissionBasic.SubmissionID; // Submissions
                        _SB.SB4 = _SubmissionBasic.RefineryID; // Submissions
                        _SB.SB3 = _SubmissionBasic.SubmissionDate; // Submissions
                        _SB.SB2 = _SubmissionBasic.UnitOfMeasure; // Submissions
                        _SB.SB1 = _SubmissionBasic.ReportCurrency; // Submissions

                        retVal.SB_List.Add(_SB);
                }
                if (plantIn.MaterialBalanceCompleteList.Count>0)
                {
                    level = "Yield";
                    foreach (FuelsRefineryMaterialBalanceComplete _FuelsRefineryMaterialBalanceComplete in plantIn.MaterialBalanceCompleteList)
                    {
                        Y _Y = new Y();

                        _Y.Y8 = _FuelsRefineryMaterialBalanceComplete .SubmissionID; // Yield
                        _Y.Y7 = _FuelsRefineryMaterialBalanceComplete .Period; // Yield
                        _Y.Y6 = _FuelsRefineryMaterialBalanceComplete .Category; // Yield
                        _Y.Y5 = _FuelsRefineryMaterialBalanceComplete .MaterialID; // Yield
                        _Y.Y4 = _FuelsRefineryMaterialBalanceComplete .MaterialName; // Yield
                        _Y.Y3 = _FuelsRefineryMaterialBalanceComplete .Barrels; // Yield
                        _Y.Y2 = _FuelsRefineryMaterialBalanceComplete .MetricTons; // Yield
                        _Y.Y1 = _FuelsRefineryMaterialBalanceComplete .Density; // Yield
                        retVal.Y_List.Add(_Y);
                    }
                }
            }
            catch (Exception ex)
            {
                string template = "BusToWCF:Level={0} Error:{1}";
                Exception infoException = new Exception(string.Format(template, level, ex.Message));
                throw infoException;
            }
            return retVal;
        }

        //decommiss this
        public PL PrimeScram(string xmlFileIn, NewDataSet dataSetIn)
        {
            PL retVal = new PL();
            try
            {
                retVal.A_List = new List<A>();
                retVal.C_List = new List<C>();
                retVal.CB_List = new List<CB>();
                retVal.CD_List = new List<CD>();
                retVal.CR_List = new List<CR>();
                retVal.D_List = new List<D>();
                retVal.E_List = new List<E>();
                retVal.EG_List = new List<EG>();
                retVal.EL_List = new List<EL>();
                retVal.F_List = new List<F>();
                retVal.G_List = new List<G>();
                retVal.GM_List = new List<GM>();
                retVal.I_List = new List<I>();
                retVal.K_List = new List<K>();
                retVal.L_List = new List<L>();
                retVal.M_List = new List<M>();
                retVal.ME_List = new List<ME>();
                retVal.MI_List = new List<MI>();
                retVal.MR_List = new List<MR>();
                retVal.MT_List = new List<MT>();
                retVal.O_List = new List<O>();
                retVal.P_List = new List<P>();
                retVal.PD_List = new List<PD>();
                retVal.R_List = new List<R>();
                retVal.RP_List = new List<RP>();
                retVal.S_List = new List<S>();
                retVal.SB_List = new List<SB>();
                retVal.Y_List = new List<Y>();

                NewDataSet ds = null;
                if (xmlFileIn.Length == 0)
                {
                    ds = dataSetIn;
                }
                else
                {
                    ds = new NewDataSet();
                    ds.ReadXml(xmlFileIn);
                }
                DataTableExtentions de = new DataTableExtentions();
                if (ds.Absence != null)
                {
                    List<Absence> itms = (from p in de.AsEnumerable2<Absence>(ds.Absence) select p).ToList();
                    foreach (Absence itm in itms)
                    {
                        A _A = new A();
                        _A.A1 = (decimal?)itm.MPSAbs; // Absence
                        _A.A2 = (decimal?)itm.OCCAbs; // Absence
                        _A.A3 = itm.CategoryID; // Absence
                        _A.A4 = itm.SubmissionID; // Absence
                        retVal.A_List.Add(_A);
                    }
                }
                if (ds.Config != null)
                {
                    List<Config> itms = (from p in de.AsEnumerable2<Config>(ds.Config) select p).ToList();
                    foreach (Config itm in itms)
                    {
                        C _C = new C();
                        _C.C1 = (decimal?)itm.EnergyPcnt; // Config
                        _C.C10 = (decimal?)itm.UtilPcnt; // Config
                        _C.C11 = (decimal?)itm.Cap; // Config
                        _C.C12 = itm.ProcessType; // Config
                        _C.C13 = itm.UnitName; // Config
                        _C.C14 = itm.ProcessID; // Config
                        _C.C15 = itm.UnitID; // Config
                        _C.C16 = itm.SubmissionID; // Config
                        _C.C2 = (decimal?)itm.AllocPcntOfCap; // Config
                        _C.C3 = itm.BlockOp; // Config
                        _C.C4 = (decimal?)itm.PostPerShift; // Config
                        _C.C5 = (decimal?)itm.MHPerWeek; // Config
                        _C.C6 = (decimal?)itm.YearsOper; // Config
                        _C.C7 = (decimal?)itm.InServicePcnt; // Config
                        _C.C8 = (decimal?)itm.StmUtilPcnt; // Config
                        _C.C9 = (decimal?)itm.StmCap; // Config
                        retVal.C_List.Add(_C);
                    }
                }
                if (ds.ConfigBuoy != null)
                {
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.ConfigBuoy> itms = (from p in de.AsEnumerable2<ConfigBuoy>(ds.ConfigBuoy) select p).ToList();
                    foreach (ConfigBuoy itm in itms)
                    {
                        CB _CB = new CB();
                        _CB.CB1 = (decimal?)itm.PcntOwnership; // ConfigBuoy
                        _CB.CB2 = (decimal?)itm.LineSize; // ConfigBuoy
                        _CB.CB3 = (decimal?)itm.ShipCap; // ConfigBuoy
                        _CB.CB4 = itm.ProcessID; // ConfigBuoy
                        _CB.CB5 = itm.UnitName; // ConfigBuoy
                        _CB.CB6 = itm.UnitID; // ConfigBuoy
                        _CB.CB7 = itm.SubmissionID; // ConfigBuoy
                        retVal.CB_List.Add(_CB);
                    }
                }
                if (ds.Crude != null)
                {
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude>(ds.Crude) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Crude itm in itms)
                    {
                        CD _CD = new CD();
                        _CD.CD1 = (decimal?)itm.Sulfur; // Crude
                        _CD.CD2 = (decimal?)itm.Gravity; // Crude
                        _CD.CD3 = (decimal?)itm.BBL; // Crude
                        _CD.CD4 = itm.CrudeName; // Crude
                        _CD.CD5 = itm.CNum;  // Crude
                        _CD.CD6 = itm.CrudeID; // Crude
                        _CD.CD7 = itm.Period; // Crude
                        _CD.CD8 = itm.SubmissionID; // Crude
                        retVal.CD_List.Add(_CD);
                    }
                }
                if (ds.ConfigRS != null)
                {
                    List<ConfigR> itms = (from p in de.AsEnumerable2<ConfigR>(ds.ConfigRS) select p).ToList();            
                    foreach (ConfigR itm in itms)
                    {
                        CR _CR = new CR();
                        _CR.CR1 = (decimal?)itm.Throughput; // ConfigRS
                        _CR.CR2 = (decimal?)itm.AvgSize; // ConfigRS
                        _CR.CR3 = itm.ProcessType; // ConfigRS
                        _CR.CR4 = itm.ProcessID; // ConfigRS
                        _CR.CR5 = itm.UnitID; // ConfigRS
                        _CR.CR6 = itm.SubmissionID; // ConfigRS
                        retVal.CR_List.Add(_CR);
                    }
                }
                if (ds.Diesel != null)
                {
                    List<Diesel> itms = (from p in de.AsEnumerable2<Diesel>(ds.Diesel) select p).ToList();
                    foreach (Diesel itm in itms)
                    {
                        D _D = new D();
                        _D.D1 = (decimal?)itm.KMT; // Diesel
                        _D.D10 = itm.Type; // Diesel
                        _D.D11 = itm.Market; // Diesel
                        _D.D12 = itm.Grade; // Diesel
                        _D.D13 = (int?)itm.BlendID; // Diesel
                        _D.D14 = itm.SubmissionID; // Diesel
                        _D.D2 = (decimal?)itm.BiodieselPcnt; // Diesel
                        _D.D3 = (decimal?)itm.E350; // Diesel
                        _D.D4 = (decimal?)itm.ASTM90; // Diesel
                        _D.D5 = (decimal?)itm.CloudPt; // Diesel
                        _D.D6 = (decimal?)itm.Sulfur; // Diesel
                        _D.D7 = (decimal?)itm.PourPt; // Diesel
                        _D.D8 = (decimal?)itm.Cetane; // Diesel
                        _D.D9 = (decimal?)itm.Density; // Diesel
                        retVal.D_List.Add(_D);
                    }
                }
                if (ds.Emissions != null)
                {
                    List<Emission> itms = (from p in de.AsEnumerable2<Emission>(ds.Emissions) select p).ToList();
                    foreach (Emission itm in itms)
                    {
                        E _E = new E();
                        _E.E1 = (decimal?)itm.RefEmissions; // Emissions
                        _E.E2 = itm.EmissionType; // Emissions
                        _E.E3 = itm.SubmissionID; // Emissions
                        retVal.E_List.Add(_E);
                    }
                }
                if (ds.Energy != null)
                {
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy>(ds.Energy) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Energy itm in itms)
                    {
                        if (itm.EnergyType == "ELE" ||
                           itm.EnergyType == "POW" ||
                           itm.EnergyType == "COG")
                        {
                            EL _EL = new EL();
                            _EL.EL1 = (decimal?)itm.GenEff; // Electric
                            _EL.EL2 = (decimal?)itm.PriceLocal; // Electric
                            _EL.EL3 = (decimal?)itm.Amount; // Electric
                            _EL.EL4 = itm.TransType; // Electric
                            _EL.EL5 = itm.EnergyType; // Electric
                            _EL.EL6 = itm.SubmissionID; // Electric

                            retVal.EL_List.Add(_EL);

                        }
                        else//Energy
                        {
                            EG _EG = new EG();
                            _EG.EG1 = (decimal?)itm.N2; // Energy
                            _EG.EG10 = (decimal?)itm.Butane; // Energy
                            _EG.EG11 = (decimal?)itm.Propylene; // Energy
                            _EG.EG12 = (decimal?)itm.Propane; // Energy
                            _EG.EG13 = (decimal?)itm.Ethylene; // Energy
                            _EG.EG14 = (decimal?)itm.Ethane; // Energy
                            _EG.EG15 = (decimal?)itm.Methane; // Energy
                            _EG.EG16 = (decimal?)itm.Hydrogen; // Energy
                           //moved to electric _EG.EG17 = (decimal?)itm.GenEff; // Energy
                            _EG.EG18 = itm.OverrideCalcs; // Energy
                            _EG.EG19 = (decimal?)itm.MBTUOut; // Energy
                            _EG.EG2 = (decimal?)itm.SO2; // Energy
                             _EG.EG20 = (decimal?)itm.PriceLocal; // Energy
                            _EG.EG21 = (decimal?)itm.Amount; // Energy
                            _EG.EG22 = itm.TransType; // Energy
                            _EG.EG23 = itm.EnergyType; // Energy
                            _EG.EG24 = itm.SubmissionID; // Energy
                            _EG.EG3 = (decimal?)itm.SO2; // Energy
                            _EG.EG4 = (decimal?)itm.NH3; // Energy
                            _EG.EG5 = (decimal?)itm.CO; // Energy
                            _EG.EG6 = (decimal?)itm.H2S; // Energy
                            _EG.EG7 = (decimal?)itm.C5Plus; // Energy
                            _EG.EG8 = (decimal?)itm.Butylenes; // Energy
                            _EG.EG9 = (decimal?)itm.Isobutane; // Energy

                            retVal.EG_List.Add(_EG);
                        }
                    }
                }
                if (ds.FiredHeaters != null)
                {
                    List<FiredHeater> itms = (from p in de.AsEnumerable2<FiredHeater>(ds.FiredHeaters) select p).ToList();
                    foreach (FiredHeater itm in itms)
                    {
                        F _F = new F();
                        _F.F1 = (decimal?)itm.OtherDuty; // FiredHeaters
                        _F.F10 = (decimal?)itm.StackTemp; // FiredHeaters
                        _F.F11 = (decimal?)itm.FurnOutTemp; // FiredHeaters
                        _F.F12 = (decimal?)itm.FurnInTemp; // FiredHeaters
                        _F.F13 = (decimal?)itm.OthCombDuty; // FiredHeaters
                        _F.F14 = itm.FuelType; // FiredHeaters
                        _F.F15 = (decimal?)itm.FiredDuty; // FiredHeaters
                        _F.F16 = itm.ThroughputUOM; // FiredHeaters
                        _F.F17 = (decimal?)itm.ThroughputRpt; // FiredHeaters
                        _F.F18 = itm.ProcessFluid; // FiredHeaters
                        _F.F19 = itm.Service; // FiredHeaters
                        _F.F2 = (decimal?)itm.ShaftDuty; // FiredHeaters
                        _F.F20 = itm.HeaterName; // FiredHeaters
                        _F.F21 = itm.ProcessID; // FiredHeaters
                        _F.F22 = itm.UnitID; // FiredHeaters
                        _F.F23 = itm.HeaterNo; // FiredHeaters
                        _F.F24 = itm.SubmissionID; // FiredHeaters
                        _F.F3 = itm.SteamSuperHeated; // FiredHeaters
                        _F.F4 = (decimal?)itm.SteamDuty; // FiredHeaters
                        _F.F5 = (decimal?)itm.ProcessDuty; // FiredHeaters
                        _F.F6 = (decimal?)itm.AbsorbedDuty; // FiredHeaters
                        _F.F7 = (decimal?)itm.CombAirTemp; // FiredHeaters
                        _F.F8 = (decimal?)itm.HeatLossPcnt; // FiredHeaters
                        _F.F9 = (decimal?)itm.StackO2; // FiredHeaters
                        retVal.F_List.Add(_F);
                    }
                }
                if (ds.Gasoline != null)
                {
                    List<Gasoline> itms = (from p in de.AsEnumerable2<Gasoline>(ds.Gasoline) select p).ToList();
                     foreach (Gasoline itm in itms)
                    {
                        G _G = new G();
                        _G.G1 = (decimal?)itm.KMT; // Gasoline
                        _G.G10 = (decimal?)itm.MON; // Gasoline
                        _G.G11 = (decimal?)itm.RON; // Gasoline
                        _G.G12 = (decimal?)itm.RVP; // Gasoline
                        _G.G13 = (decimal?)itm.Density; // Gasoline
                        _G.G14 = itm.Type; // Gasoline
                        _G.G15 = itm.Market; // Gasoline
                        _G.G16 = itm.Grade; // Gasoline
                        _G.G17 = (int?)itm.BlendID; // Gasoline
                        _G.G18 = itm.SubmissionID; // Gasoline
                        _G.G2 = (decimal?)itm.Lead; // Gasoline
                        _G.G3 = (decimal?)itm.Sulfur; // Gasoline
                        _G.G4 = (decimal?)itm.OthOxygen; // Gasoline
                        _G.G5 = (decimal?)itm.TAME; // Gasoline
                        _G.G6 = (decimal?)itm.ETBE; // Gasoline
                        _G.G7 = (decimal?)itm.MTBE; // Gasoline
                        _G.G8 = (decimal?)itm.Ethanol; // Gasoline
                        _G.G9 = (decimal?)itm.Oxygen; // Gasoline
                        retVal.G_List.Add(_G);
                    }
                }

                if (ds.GeneralMisc != null)
                {
                    List<GeneralMisc> itms = (from p in de.AsEnumerable2<GeneralMisc>(ds.GeneralMisc) select p).ToList();
                    foreach (GeneralMisc itm in itms)
                    {
                        GM _GM = new GM();

                        _GM.GM1 = (decimal?)itm.TotLossMT; // GeneralMisc
                        _GM.GM2 = (decimal?)itm.FlareLossMT; // GeneralMisc
                        _GM.GM3 = itm.SubmissionID; // GeneralMisc
                        retVal.GM_List.Add(_GM);
                    }
                }
                if (ds.Inventory != null)
                {
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory>(ds.Inventory) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Inventory itm in itms)
                    {
                        I _I = new I();
                        _I.I1 = (decimal?)itm.AvgLevel; // Inventory
                        _I.I2 = (decimal?)itm.LeasedPcnt; // Inventory
                        _I.I3 = (int?)itm.NumTank; // Inventory
                        _I.I4 = (decimal?)itm.RefStorage; // Inventory
                        _I.I5 = (decimal?)itm.MandStorage; // Inventory
                        _I.I6 = (decimal?)itm.MktgStorage; // Inventory
                        _I.I7 = (decimal?)itm.TotStorage; // Inventory
                        _I.I8 = itm.TankType; // Inventory
                        _I.I9 = itm.SubmissionID; // Inventory
                        retVal.I_List.Add(_I);
                    }
                }
                if (ds.Kerosene != null)
                {
                    List<Kerosene> itms = (from p in de.AsEnumerable2<Kerosene>(ds.Kerosene) select p).ToList();
                   foreach (Kerosene itm in itms)
                    {
                        K _K = new K();
                        _K.K1 = (decimal?)itm.KMT; // Kerosene
                        _K.K2 = (decimal?)itm.Sulfur; // Kerosene
                        _K.K3 = (decimal?)itm.Density; // Kerosene
                        _K.K4 = itm.Type; // Kerosene
                        _K.K5 = itm.Grade; // Kerosene
                        _K.K6 = (int?)itm.BlendID; // Kerosene
                        _K.K7 = itm.SubmissionID; // Kerosene
                        retVal.K_List.Add(_K);
                    }
                }
                if (ds.LPG != null)
                {
                    List<LPG> itms = (from p in de.AsEnumerable2<LPG>(ds.LPG) select p).ToList();
                    foreach (LPG itm in itms)
                    {
                        L _L = new L();
                        _L.L1 = (decimal?)itm.VolC5Plus; // LPG
                        _L.L10 = itm.SubmissionID; // LPG
                        _L.L2 = (decimal?)itm.VolC4ene; // LPG
                        _L.L3 = (decimal?)itm.VolnC4; // LPG
                        _L.L4 = (decimal?)itm.VoliC4; // LPG
                        _L.L5 = (decimal?)itm.VolC3ene; // LPG
                        _L.L6 = (decimal?)itm.VolC3; // LPG
                        _L.L7 = (decimal?)itm.VolC2Lt; // LPG                       
                        _L.L8 = itm.MolOrVol; // LPG
                        _L.L9 = (int?)itm.BlendID; // LPG
                        retVal.L_List.Add(_L);
                    }
                }
                if (ds.MarineBunkers != null)
                {
                    List<MarineBunker> itms = (from p in de.AsEnumerable2<MarineBunker>(ds.MarineBunkers) select p).ToList();
                    foreach (MarineBunker itm in itms)
                    {
                        M _M = new M();
                        _M.M1 = (decimal?)itm.KMT; // MarineBunkers
                        _M.M10 = itm.SubmissionID; // MarineBunkers
                        _M.M2 = (decimal?)itm.CrackedStock; // MarineBunkers
                        _M.M3 = (decimal?)itm.ViscTemp; // MarineBunkers
                        _M.M4 = (decimal?)itm.ViscCSAtTemp; // MarineBunkers
                        _M.M5 = (decimal?)itm.PourPt; // MarineBunkers                        
                        _M.M6 = (decimal?)itm.Sulfur; // MarineBunkers
                        _M.M7 = (decimal?)itm.Density; // MarineBunkers
                        _M.M8 = itm.Grade; // MarineBunkers
                        _M.M9 = (int?)itm.BlendID; // MarineBunkers
                        retVal.M_List.Add(_M);
                    }
                }
                if (ds.MExp != null)
                {
                    List<MExp> itms = (from p in de.AsEnumerable2<MExp>(ds.MExp) select p).ToList();
                    foreach (MExp itm in itms)
                    {
                        ME _ME = new ME();
                        _ME.ME1 = (decimal?)itm.TotMaint; // MExp
                        _ME.ME10 = (decimal?)itm.SafetyOth; // MExp
                        _ME.ME11 = (decimal?)itm.RegOth; // MExp
                        _ME.ME12 = (decimal?)itm.RegDiesel; // MExp
                        _ME.ME13 = (decimal?)itm.RegGaso; // MExp
                        _ME.ME14 = (decimal?)itm.RegExp; // MExp
                        _ME.ME15 = (decimal?)itm.ConstraintRemoval; // MExp
                        _ME.ME16 = (decimal?)itm.NonRegUnit; // MExp
                        _ME.ME17 = (decimal?)itm.NonMaintInvestExp; // MExp
                        _ME.ME18 = (decimal?)itm.RoutMaintCptl; // MExp
                        _ME.ME19 = (decimal?)itm.TAMaintCptl; // MExp
                        _ME.ME2 = (decimal?)itm.MaintOvhd; // MExp
                        _ME.ME20 = (decimal?)itm.NonRefExcl; // MExp
                        _ME.ME21 = (decimal?)itm.ComplexCptl; // MExp
                        _ME.ME22 = itm.SubmissionID; // MExp
                        _ME.ME3 = (decimal?)itm.MaintOvhdRout; // MExp
                        _ME.ME4 = (decimal?)itm.MaintOvhdTA; // MExp
                        _ME.ME5 = (decimal?)itm.MaintExp; // MExp
                        _ME.ME6 = (decimal?)itm.MaintExpRout; // MExp
                        _ME.ME7 = (decimal?)itm.MaintExpTA; // MExp
                        _ME.ME8 = (decimal?)itm.OthInvest; // MExp
                        _ME.ME9 = (decimal?)itm.Energy; // MExp
                        retVal.ME_List.Add(_ME);
                    }
                }
                if (ds.MiscInput != null)
                {
                    List<MiscInput> itms = (from p in de.AsEnumerable2<MiscInput>(ds.MiscInput) select p).ToList();
                    foreach (MiscInput itm in itms)
                    {
                        MI _MI = new MI();
                        _MI.MI1 = (decimal?)itm.OffsiteEnergyPcnt; // MiscInput
                        _MI.MI2 = (decimal?)itm.CDUChargeMT; // MiscInput
                        _MI.MI3 = (decimal?)itm.CDUChargeBbl; // MiscInput
                        _MI.MI4 = itm.SubmissionID; // MiscInput
                        retVal.MI_List.Add(_MI);
                    }
                }
                if (ds.MaintRout != null)
                {
                    List<MaintRout> itms = (from p in de.AsEnumerable2<MaintRout>(ds.MaintRout) select p).ToList();
                    foreach (MaintRout itm in itms)
                    {
                        MR _MR = new MR();
                        _MR.MR1 = (decimal?)itm.InServicePcnt; // MaintRout
                        _MR.MR10 = (decimal?)itm.RoutExpLocal; // MaintRout
                        _MR.MR11 = (decimal?)itm.RoutCostLocal; // MaintRout
                        _MR.MR12 = (decimal?)itm.OthSlow; // MaintRout
                        _MR.MR13 = (decimal?)itm.OthDown; // MaintRout
                        _MR.MR14 = (int?)itm.OthNum; // MaintRout
                        _MR.MR15 = (decimal?)itm.MaintDown; // MaintRout
                        _MR.MR16 = (int?)itm.MaintNum; // MaintRout
                        _MR.MR17 = (decimal?)itm.RegDown; // MaintRout
                        _MR.MR18 = (int?)itm.RegNum; // MaintRout
                        _MR.MR19 = itm.ProcessID; // MaintRout
                        _MR.MR2 = (decimal?)itm.UtilPcnt; // MaintRout
                        _MR.MR20 = itm.UnitID; // MaintRout
                        _MR.MR21 = itm.SubmissionID; // MaintRout
                        _MR.MR3 = (decimal?)itm.OthDownOther; // MaintRout
                        _MR.MR4 = (decimal?)itm.OthDownOffsiteUpsets; // MaintRout
                        _MR.MR5 = (decimal?)itm.OthDownUnitUpsets; // MaintRout
                        _MR.MR6 = (decimal?)itm.OthDownExternal; // MaintRout
                        _MR.MR7 = (decimal?)itm.OthDownEconomic; // MaintRout
                        _MR.MR8 = (decimal?)itm.RoutOvhdLocal; // MaintRout
                        _MR.MR9 = (decimal?)itm.RoutCptlLocal; // MaintRout
                        retVal.MR_List.Add(_MR);
                    }
                }
                if (ds.MaintTA != null)
                {
                    List<MaintTA> itms = (from p in de.AsEnumerable2<MaintTA>(ds.MaintTA) select p).ToList();
                    foreach (MaintTA itm in itms)
                    {
                        MT _MT = new MT();
                        _MT.MT1 = (int?)itm.TAExceptions; // MaintTA
                        _MT.MT10 = (decimal?)itm.TAOvhdLocal; // MaintTA
                        _MT.MT11 = (decimal?)itm.TACptlLocal; // MaintTA
                        _MT.MT12 = (decimal?)itm.TAExpLocal; // MaintTA
                        _MT.MT13 = (decimal?)itm.TACostLocal; // MaintTA
                        _MT.MT14 = (decimal?)itm.TAHrsDown; // MaintTA
                        _MT.MT15 = (DateTime?)itm.TADate; // MaintTA
                        _MT.MT16 = itm.ProcessID; // MaintTA
                        _MT.MT17 = itm.UnitID; // MaintTA
                        _MT.MT18 = itm.SubmissionID; // MaintTA                       
                        _MT.MT2 = (DateTime?)itm.PrevTADate; // MaintTA
                        _MT.MT3 = (decimal?)itm.TAContMPS; // MaintTA
                        _MT.MT4 = (decimal?)itm.TAContOCC; // MaintTA
                        _MT.MT5 = (decimal?)itm.TAMPSOVTPcnt; // MaintTA
                        _MT.MT6 = (decimal?)itm.TAMPSSTH; // MaintTA
                        _MT.MT7 = (decimal?)itm.TAOCCOVT; // MaintTA
                        _MT.MT8 = (decimal?)itm.TAOCCSTH; // MaintTA
                        _MT.MT9 = (decimal?)itm.TALaborCostLocal; // MaintTA
                        retVal.MT_List.Add(_MT);
                    }
                }
                if (ds.OpexData != null)
                {
                    List<OpexData> itms = (from p in de.AsEnumerable2<OpexData>(ds.OpexData) select p).ToList();
                   foreach (OpexData itm in itms)
                    {
                        O _O = new O();
                        _O.O1 = itm.OthDescription; // OpexData
                        _O.O2 = (decimal?)itm.RptValue; // OpexData
                        _O.O3 = itm.Property; // OpexData
                        _O.O4 = itm.SubmissionID; // OpexData
                        retVal.O_List.Add(_O);
                    }
                }
                if (ds.Pers != null)
                {
                   List<Per> itms = (from p in de.AsEnumerable2<Per>(ds.Pers) select p).ToList();
                    foreach (Per itm in itms)
                    {
                        P _P = new P();
                        _P.P1 = (decimal?)itm.AbsHrs;
                        _P.P2 = (decimal?)itm.GA;
                        _P.P3 = (decimal?)itm.Contract;
                        _P.P4 = (decimal?)itm.OVTPcnt;
                        _P.P5 = (decimal?)itm.OVTHours;
                        _P.P6 = (decimal?)itm.STH;
                        _P.P7 = (decimal?)itm.NumPers;
                        _P.P8 = itm.PersID;
                        _P.P9 = itm.SubmissionID;
                        retVal.P_List.Add(_P);
                    }
                }
                if (ds.ProcessData != null)
                {
                    List<ProcessData> itms = (from p in de.AsEnumerable2<ProcessData>(ds.ProcessData) select p).ToList();
                    foreach (ProcessData itm in itms)
                    {
                        PD _PD = new PD();
                        _PD.PD1 = itm.UOM; // ProcessData                       
                        _PD.PD2 = (DateTime?)itm.RptDVal; // ProcessData
                        _PD.PD3 = itm.RptTVal; // ProcessData
                        _PD.PD4 = (decimal?)itm.RptNVal; // ProcessData                       
                        _PD.PD5 = itm.Property; // ProcessData
                        _PD.PD6 = itm.UnitID; // ProcessData
                        _PD.PD7 = itm.SubmissionID; // ProcessData
                        retVal.PD_List.Add(_PD);
                    }
                }
                if (ds.Resid != null)
                {
                    List<Resid> itms = (from p in de.AsEnumerable2<Resid>(ds.Resid) select p).ToList();
                    foreach (Resid itm in itms)
                    {
                        R _R = new R();
                        _R.R1 = (decimal?)itm.KMT; // Resid
                        _R.R2 = (decimal?)itm.ViscTemp; // Resid
                        _R.R3 = (decimal?)itm.ViscCSAtTemp; // Resid
                        _R.R4 = (decimal?)itm.PourPT; // Resid
                        _R.R5 = (decimal?)itm.Sulfur; // Resid
                        _R.R6 = (decimal?)itm.Density; // Resid
                        _R.R7 = itm.Grade; // Resid
                        _R.R8 = (int?)itm.BlendID; // Resid
                        _R.R9 = itm.SubmissionID; // Resid
                        retVal.R_List.Add(_R);
                    }
                }
                if (ds.RPFResid != null)
                {
                    List<RPFResid> itms = (from p in de.AsEnumerable2<RPFResid>(ds.RPFResid) select p).ToList();
                    foreach (RPFResid itm in itms)
                    {
                        RP _RP = new RP();

                        _RP.RP1 = (decimal?)itm.Density; // RPFResid
                        _RP.RP2 = (decimal?)itm.ViscCSAtTemp; // RPFResid
                        _RP.RP3 = (decimal?)itm.ViscTemp; // RPFResid
                        _RP.RP4 = (decimal?)itm.Sulfur; // RPFResid
                        _RP.RP5 = itm.EnergyType; // RPFResid
                        _RP.RP6 = itm.SubmissionID; // RPFResid
                        retVal.RP_List.Add(_RP);
                    }
                }

                if (ds.SteamSystem != null)
                {
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem>(ds.SteamSystem) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.SteamSystem itm in itms)
                    {
                        S _S = new S();
                        _S.S1 = (decimal?)itm.TotCons; // SteamSystem
                        _S.S10 = (decimal?)itm.ConsTopTurbHigh; // SteamSystem
                        _S.S11 = (decimal?)itm.ConsCondTurb; // SteamSystem
                        _S.S12 = (decimal?)itm.ConsOthHeaters; // SteamSystem
                        _S.S13 = (decimal?)itm.ConsReboil; // SteamSystem
                        _S.S14 = (decimal?)itm.ConsProcessOth; // SteamSystem
                        _S.S15 = (decimal?)itm.ConsProcessFCC; // SteamSystem
                        _S.S16 = (decimal?)itm.ConsProcessCOK; // SteamSystem
                        _S.S17 = (decimal?)itm.ConsProcessCDU; // SteamSystem
                        _S.S18 = (decimal?)itm.TotSupply; // SteamSystem
                        _S.S19 = (decimal?)itm.NetPur; // SteamSystem
                        _S.S2 = (decimal?)itm.ConsOth; // SteamSystem
                        _S.S20 = (decimal?)itm.Pur; // SteamSystem
                        _S.S21 = (decimal?)itm.Sold; // SteamSystem
                        _S.S22 = (decimal?)itm.STProd; // SteamSystem
                        _S.S23 = (decimal?)itm.OthSource; // SteamSystem
                        _S.S24 = (decimal?)itm.WasteHeatOth; // SteamSystem
                        _S.S25 = (decimal?)itm.WasteHeatCOK; // SteamSystem
                        _S.S26 = (decimal?)itm.WasteHeatTCR; // SteamSystem
                        _S.S27 = (decimal?)itm.WasteHeatFCC; // SteamSystem
                        _S.S28 = (decimal?)itm.FTCogen; // SteamSystem
                        _S.S29 = (decimal?)itm.Calciner; // SteamSystem
                        _S.S3 = (decimal?)itm.ConsFlares; // SteamSystem
                        _S.S30 = (decimal?)itm.FluidCokerCOBoiler; // SteamSystem
                        _S.S31 = (decimal?)itm.FCCStackGas; // SteamSystem
                        _S.S32 = (decimal?)itm.FCCCatCoolers; // SteamSystem
                        _S.S33 = (decimal?)itm.FiredBoiler; // SteamSystem
                        _S.S34 = (decimal?)itm.FiredProcessHeater; // SteamSystem
                        _S.S35 = (decimal?)itm.H2PlantExport; // SteamSystem
                        _S.S36 = (decimal?)itm.ActualPress; // SteamSystem
                        _S.S37 = itm.PressureRange; // SteamSystem
                        _S.S38 = itm.SubmissionID; // SteamSystem
                        _S.S4 = (decimal?)itm.ConsDeaerators; // SteamSystem
                        _S.S5 = (decimal?)itm.ConsTracingHeat; // SteamSystem
                        _S.S6 = (decimal?)itm.ConsPressControlLow; // SteamSystem
                        _S.S7 = (decimal?)itm.ConsPressControlHigh; // SteamSystem
                        _S.S8 = (decimal?)itm.ConsCombAirPreheat; // SteamSystem
                        _S.S9 = (decimal?)itm.ConsTopTurbLow; // SteamSystem
                        retVal.S_List.Add(_S);
                    }
                }
                if (ds.Submission != null)
                {
                    List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission> itms = (from p in de.AsEnumerable2<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission>(ds.Submission) select p).ToList();
                    foreach (Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Submission itm in itms)
                    {
                        SB _SB = new SB();
                        _SB.SB1 = itm.RptCurrency; // Submissions
                        _SB.SB2 = itm.UOM; // Submissions
                        _SB.SB3 = (DateTime?)itm.Date; // Submissions
                        _SB.SB4 = itm.RefID; // Submissions
                        _SB.SB5 = itm.SubmissionID; // Submissions
                        retVal.SB_List.Add(_SB);
                    }
                }
                if (ds.Yield != null)
                {
                    List<Yield> itms = (from p in de.AsEnumerable2<Yield>(ds.Yield) select p).ToList();
                    foreach (Yield itm in itms)
                    {
                        Y _Y = new Y();

                        _Y.Y1 = (decimal?)itm.Density; // Yield
                        _Y.Y2 = (decimal?)itm.MT; // Yield
                        _Y.Y3 = (decimal?)itm.BBL; // Yield
                        _Y.Y4 = itm.MaterialName; // Yield
                        _Y.Y5 = itm.MaterialID; // Yield
                        _Y.Y6 = itm.Category; // Yield
                        _Y.Y7 = itm.Period; // Yield
                        _Y.Y8 = itm.SubmissionID; // Yield
                        retVal.Y_List.Add(_Y);
                    }
                }              
            }
            catch (Exception ex)
            {
                string template = "PrimeScram:Error={0}";
                Exception infoException = new Exception(string.Format(template, ex.Message));
                throw infoException;
            }
            return retVal;
        }
        //currently not used
        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                 //   MessageBox.Show("Error: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                  //  MessageBox.Show("Warning: {0}", e.Message);
                    break;

            }
        }
    }
}
