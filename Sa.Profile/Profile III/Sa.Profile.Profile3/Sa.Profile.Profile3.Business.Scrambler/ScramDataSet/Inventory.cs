using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    public class Inventory
    {
        public float? AvgLevel { get; set; }
        
        public float? LeasedPcnt { get; set; }
        
        public double? MandStorage { get; set; }
        
        public double? MktgStorage { get; set; }
        
        public short? NumTank { get; set; }
        
        public double? RefStorage { get; set; }
        
        public int SubmissionID { get; set; }
        
        public string TankType { get; set; }
        
        public double? TotStorage { get; set; }
    }
}
