using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
    using Sa.Profile.Profile3.Business.PlantUnits;

namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public class FuelsRefineryPlantUnitFactory : PlantUnitFactory
    {
        public FuelsRefineryPlantUnitFactory()
        {
        }
        
        public FuelsRefineryPlantUnitFactory(PlantLevel plantLevel)
        {
            switch (plantLevel)
            {
                case PlantLevel.COMPLETE:
                    base.ConfigurationList = new List<FuelsRefineryConfigurationComplete>().ToList<ConfigurationBasic>();
                    this.GasolineList = new List<FuelsRefineryGasolineComplete>().ToList<Fuels>();
                    this.LiquifiedPetroleumGasList = new List<FuelsRefineryLiquifiedPetroleumGasComplete>().ToList<FuelsRefineryLiquifiedPetroleumGasComplete>();
                    this.ConfigurationReceiptAndShipmentList = new List<FuelsRefineryConfigurationReceiptAndShipmentComplete>().ToList<ConfigurationReceiptAndShipmentBasic>();
                    this.ConfigurationBuoyList = new List<FuelsRefineryConfigurationBuoyComplete>().ToList<FuelsRefineryConfigurationBuoyComplete>();
                    this.CrudeList = new List<FuelsRefineryCrudeComplete>().ToList<CrudeBasic>();
                    this.DieselList = new List<FuelsRefineryDieselComplete>().ToList<Fuels>();
                    this.EmmissionList = new List<FuelsRefineryEmissionComplete>().ToList<FuelsRefineryEmissionComplete>();
                    this.EnergyList = new List<FuelsRefineryEnergyComplete>().ToList<EnergyBasic>();
                    this.ElectricList = new List<FuelsRefineryEnergyElectricBasic>().ToList<EnergyBasic>();
                    this.FiredHeatersList = new List<FuelsRefineryFiredHeatersComplete>().ToList<FuelsRefineryFiredHeatersComplete>();
                    this.GeneralMiscellaneousList = new List<FuelsRefineryGeneralMiscellaneousComplete>().ToList<FuelsRefineryGeneralMiscellaneousComplete>();
                    this.InventoryList = new List<FuelsRefineryInventoryComplete>().ToList<FuelsRefineryInventoryComplete>();
                    this.KeroseneList = new List<FuelsRefineryKeroseneComplete>().ToList<Fuels>();
                    this.MaintenanceExpenseAndCapitalList = new List<FuelsRefineryMaintenanceExpenseAndCapitalComplete>().ToList<FuelsRefineryMaintenanceExpenseAndCapitalComplete>();
                    this.MaterialBalanceList = new List<FuelsRefineryMaterialBalanceComplete>().ToList<MaterialBalanceBasic>();
                    this.MarineBunkerList = new List<FuelsRefineryMarineBunkersComplete>().ToList<Fuels>();
                    this.MiscellaneousInputList = new List<FuelsRefineryMiscellaneousInputComplete>().ToList<FuelsRefineryMiscellaneousInputComplete>();
                    this.OperatingExpenseList = new List<OperatingExpenseStandard>().ToList<OperatingExpenseBasic>();
                    this.PersonnelAbsenceHoursList = new List<FuelsRefineryPersonnelAbsenceHoursComplete>().ToList<FuelsRefineryPersonnelAbsenceHoursComplete>();
                    this.PersonnelHoursList = new List<FuelsRefineryPersonnelHoursComplete>().ToList<PersonnelHoursBasic>();
                    this.RefineryProducedFuelResidualList = new List<FuelsRefineryProducedFuelResidualComplete>().ToList<FuelsRefineryProducedFuelResidualComplete>();
                    this.FinishedResidualFuelList = new List<FuelsRefineryFinishedResidualFuelComplete>().ToList<Fuels>();
                    this.RoutineMaintenanceList = new List<FuelsRefineryRoutineMaintenanceComplete>().ToList<RoutineMaintenanceBasic>();
                    this.SteamSystemList = new List<FuelsRefinerySteamSystemComplete>().ToList<FuelsRefinerySteamSystemComplete>();
                    this.TurnaroundMaintenanceList = new List<FuelsRefineryTurnaroundMaintenanceComplete>().ToList<TurnaroundMaintenanceBasic>();
                    base.PlantSubmission = new SubmissionBasic();
                    base.PlantOperatingConditionList = new List<PlantOperatingConditionBasic>().ToList<PlantOperatingConditionBasic>();
                    break;
            }
        }
        
        ~FuelsRefineryPlantUnitFactory()
        {
        }

        public List<FuelsRefineryConfigurationBuoyComplete> ConfigurationBuoyList { get; set; }
        
        public List<ConfigurationReceiptAndShipmentBasic> ConfigurationReceiptAndShipmentList { get; set; }

        public List<CrudeBasic> CrudeList { get; set; }
        
        public List<Fuels> DieselList { get; set; }

        public List<FuelsRefineryEmissionComplete> EmmissionList { get; set; }

        public List<EnergyBasic> EnergyList { get; set; }
        
        public List<Fuels> FinishedResidualFuelList { get; set; }

        public List<FuelsRefineryFiredHeatersComplete> FiredHeatersList { get; set; }
        
        public List<Fuels> GasolineList { get; set; }

        public List<FuelsRefineryGeneralMiscellaneousComplete> GeneralMiscellaneousList { get; set; }

        public List<FuelsRefineryInventoryComplete> InventoryList { get; set; }
        
        public List<Fuels> KeroseneList { get; set; }

        public List<FuelsRefineryLiquifiedPetroleumGasComplete> LiquifiedPetroleumGasList { get; set; }

        public List<FuelsRefineryMaintenanceExpenseAndCapitalComplete> MaintenanceExpenseAndCapitalList { get; set; }
        
        public List<Fuels> MarineBunkerList { get; set; }

        public List<MaterialBalanceBasic> MaterialBalanceList { get; set; }

        public List<FuelsRefineryMiscellaneousInputComplete> MiscellaneousInputList { get; set; }

        public List<OperatingExpenseBasic> OperatingExpenseList { get; set; }

        public List<FuelsRefineryPersonnelAbsenceHoursComplete> PersonnelAbsenceHoursList { get; set; }
        
        public List<PersonnelHoursBasic> PersonnelHoursList { get; set; }

        public List<FuelsRefineryProducedFuelResidualComplete> RefineryProducedFuelResidualList { get; set; }

        public List<RoutineMaintenanceBasic> RoutineMaintenanceList { get; set; }

        public List<FuelsRefinerySteamSystemComplete> SteamSystemList { get; set; }

        public List<TurnaroundMaintenanceBasic> TurnaroundMaintenanceList { get; set; }
        
        public List<EnergyBasic> ElectricList
        {
            get;
            set;
        }
    }
}
