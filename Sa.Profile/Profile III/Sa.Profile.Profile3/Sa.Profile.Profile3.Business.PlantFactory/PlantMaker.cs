
namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public abstract class PlantMaker
    {
        protected PlantUnitFactory _plantUnitFactory;
        
        public abstract BasicPlant CreatePlant(string type);
        ~PlantMaker()
        {
        }
    }
}
