USE [ProfileFuels12]
GO

/****** Object:  Table [dbo].[PlantStudyLog]    Script Date: 10/13/2014 2:29:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PlantStudyLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SASubmissionID] [int] NOT NULL,
	[ClientSubmissionID] [int] NOT NULL,
	[PlantName] [varchar](100) NOT NULL,
	[StudyLevel] [varchar](50) NOT NULL,
	[StudyMode] [varchar](10) NOT NULL,
	[XMLFileLocation] [varchar](100) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PlantStudyLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


