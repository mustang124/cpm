ReportCode|ReportName|SortKey|CustomGroup|Template
Avail1    |Monthly Availability|15|0|Avail.tpl
Avail24   |Study Equivalent Availability|16|0|Avail.tpl
BashAvl1  |Bashneft Monthly Availability|37|7|BashneftAvail.tpl
BashAvl24 |Bashneft Study Equivalent Availability|38|7|BashneftAvail.tpl
BashEDCFac|Bashneft Factors|32|7|EDCDetailsRussian.tpl
BashEffFac|Bashneft Efficiency Factors|33|7|EfficiencyFactorsRussian.tpl
BashEII   |Bashneft EII and VEI Breakdown|34|7|EIIRussian.tpl
BashPS    |Bashneft Process Unit Scorecard|31|7|BashProcessUnitScorecard.tpl
BashTA    |Bashneft Turnarounds|35|7|TAReportRussian.tpl
BashTAAnn |Bashneft Turnaround Annualization|36|7|TAAnnReportRussian.tpl
BashTrend |Bashneft Refinery Trend|30|7|RefineryTrendReportRussian.tpl
BURMo     |BUR Monthly Scorecard|11|3|ValeroBURMo.tpl
BURYTD    |BUR YTD Scorecard|12|3|ValeroBURYTD.tpl
CDU       |CDU Report|10|1|ProcessUnitReports.tpl
CVX_AvgKPI|Chevron Rolling Average KPIs|22|2|ChevronKPI_AVG.tpl
CVX_KPI   |Chevron Monthly KPIs|21|2|ChevronKPI.tpl
CVX_RECON |Chevron Reconciliation Report|23|2|ChevronRecon.tpl
EDCFactors|EDC, Utilization, Standard Energy and Gain|6|0|EDCDetails.tpl
EffFactors|Efficiency Factors|5|0|EfficiencyFactors.tpl
EII       |EII and VEI Breakdown|6|0|EII.tpl
EIIPL     |EII and VEI Details|34|255|EIIPL.tpl
EPPS      |EP PetroEcuador Process Unit Scorecard|31|8|EPProcessUnitScorecard.tpl
FCC       |FCC Report|11|1|ProcessUnitReports.tpl
GPV       |Gross Product Value|5|0|GPV.tpl
HuskySC   |Management Scorecard|21|6|HuskySC.tpl
HYC       |HYC Report|12|1|ProcessUnitReports.tpl
MechAvl1  |Monthly Mechanical Availability|37|255|AvailMaintOnly.tpl
MechAvl24 |Study Equivalent Mechanical Availability|38|255|AvailMaintOnly.tpl
PD        |Process Unit Detail|4|0|ProcessDetail.tpl
PERS      |Personnel Report|8|0|PersonnelReport.tpl
PLTrend   |Trend Report|30|255|RefineryTrendReport.tpl
PS        |Process Unit Scorecard|3|0|ProcessUnitScorecard.tpl
PXYL      |PXYL Report|14|1|ProcessUnitReports.tpl
REF       |REF Report|13|1|ProcessUnitReports.tpl
RS        |Refinery Scorecard|1|0|RefineryScorecard.tpl
SENS      |Sensible Heat Materials|19|0|SensibleHeat.tpl
T4        |Operating Expenses|7|0|Table4.tpl
TA        |Turnarounds|11|0|TAReport.tpl
TAAnn     |Turnaround Annualization|12|0|TAAnnReport.tpl
TRE       |Refinery Trends Report|2|0|RefineryTrendReport.tpl
VAL       |Validation Report|20|0|Datacheck.tpl
