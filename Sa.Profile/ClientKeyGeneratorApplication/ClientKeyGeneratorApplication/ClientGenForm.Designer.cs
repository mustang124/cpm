﻿namespace ClientKeyGeneratorApplication
{
    partial class ClientGenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtRefNum = new System.Windows.Forms.TextBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.lblFileSave = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.checkWebServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblPwdMsg = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblResultLabel = new System.Windows.Forms.Label();
            this.lblGenKey = new System.Windows.Forms.Label();
            this.txtGenKey = new System.Windows.Forms.TextBox();
            this.certificateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Location :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ref Num :";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(79, 50);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(286, 20);
            this.txtCompany.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(79, 84);
            this.txtPassword.MaxLength = 32;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(191, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(79, 118);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(166, 20);
            this.txtLocation.TabIndex = 3;
            // 
            // txtRefNum
            // 
            this.txtRefNum.Location = new System.Drawing.Point(79, 151);
            this.txtRefNum.Name = "txtRefNum";
            this.txtRefNum.Size = new System.Drawing.Size(96, 20);
            this.txtRefNum.TabIndex = 4;
            this.txtRefNum.TextChanged += new System.EventHandler(this.txtRefNum_TextChanged);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(371, 254);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(153, 23);
            this.btnGenerate.TabIndex = 6;
            this.btnGenerate.Text = "Generate Key && Save File";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.InitialDirectory = "C:\\ClientKeys\\";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(-3, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 0;
            // 
            // lblFileSave
            // 
            this.lblFileSave.AutoSize = true;
            this.lblFileSave.Location = new System.Drawing.Point(7, 170);
            this.lblFileSave.Name = "lblFileSave";
            this.lblFileSave.Size = new System.Drawing.Size(0, 13);
            this.lblFileSave.TabIndex = 10;
            this.lblFileSave.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkWebServiceToolStripMenuItem,
            this.certificateToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(584, 24);
            this.menuStrip1.TabIndex = 11;
            // 
            // checkWebServiceToolStripMenuItem
            // 
            this.checkWebServiceToolStripMenuItem.Name = "checkWebServiceToolStripMenuItem";
            this.checkWebServiceToolStripMenuItem.Size = new System.Drawing.Size(119, 20);
            this.checkWebServiceToolStripMenuItem.Text = "Check Web Service";
            this.checkWebServiceToolStripMenuItem.Click += new System.EventHandler(this.checkWebServiceToolStripMenuItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(48, 254);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(153, 23);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblPwdMsg
            // 
            this.lblPwdMsg.AutoSize = true;
            this.lblPwdMsg.ForeColor = System.Drawing.Color.Blue;
            this.lblPwdMsg.Location = new System.Drawing.Point(272, 87);
            this.lblPwdMsg.Name = "lblPwdMsg";
            this.lblPwdMsg.Size = new System.Drawing.Size(309, 13);
            this.lblPwdMsg.TabIndex = 13;
            this.lblPwdMsg.Text = " * Must be at least 8 characters, max 32. (No special characters)";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(76, 188);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 14;
            // 
            // lblResultLabel
            // 
            this.lblResultLabel.AutoSize = true;
            this.lblResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultLabel.Location = new System.Drawing.Point(8, 188);
            this.lblResultLabel.Name = "lblResultLabel";
            this.lblResultLabel.Size = new System.Drawing.Size(51, 13);
            this.lblResultLabel.TabIndex = 15;
            this.lblResultLabel.Text = "Result :";
            this.lblResultLabel.Visible = false;
            // 
            // lblGenKey
            // 
            this.lblGenKey.AutoSize = true;
            this.lblGenKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGenKey.Location = new System.Drawing.Point(7, 217);
            this.lblGenKey.Name = "lblGenKey";
            this.lblGenKey.Size = new System.Drawing.Size(36, 13);
            this.lblGenKey.TabIndex = 16;
            this.lblGenKey.Text = "Key :";
            this.lblGenKey.Visible = false;
            // 
            // txtGenKey
            // 
            this.txtGenKey.Location = new System.Drawing.Point(79, 214);
            this.txtGenKey.Name = "txtGenKey";
            this.txtGenKey.ReadOnly = true;
            this.txtGenKey.Size = new System.Drawing.Size(482, 20);
            this.txtGenKey.TabIndex = 17;
            this.txtGenKey.Visible = false;
            // 
            // certificateToolStripMenuItem
            // 
            this.certificateToolStripMenuItem.Name = "certificateToolStripMenuItem";
            this.certificateToolStripMenuItem.Size = new System.Drawing.Size(136, 20);
            this.certificateToolStripMenuItem.Text = "Certificate Verification";
            this.certificateToolStripMenuItem.Click += new System.EventHandler(this.certificateToolStripMenuItem_Click);
            // 
            // ClientGenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 293);
            this.Controls.Add(this.txtGenKey);
            this.Controls.Add(this.lblGenKey);
            this.Controls.Add(this.lblResultLabel);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblPwdMsg);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lblFileSave);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.txtRefNum);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ClientGenForm";
            this.Text = "Solomon Client Key Generator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.TextBox txtRefNum;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblFileSave;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem checkWebServiceToolStripMenuItem;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblPwdMsg;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblResultLabel;
        private System.Windows.Forms.Label lblGenKey;
        private System.Windows.Forms.TextBox txtGenKey;
        private System.Windows.Forms.ToolStripMenuItem certificateToolStripMenuItem;
    }
}

