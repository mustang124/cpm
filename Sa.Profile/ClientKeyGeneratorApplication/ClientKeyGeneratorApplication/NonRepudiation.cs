﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Threading.Tasks;


namespace ClientKeyGeneratorApplication
{
   
	/// <summary>
	/// Signs a message from a specfic sender; increasing confidence that the message came from an expected sender.
	/// </summary>

    [Serializable]
	public class NonRepudiation
	{

        
        public NonRepudiation()
        {

        }

		/// <summary>
		/// Signs a message using a private certificate
		/// </summary>
		/// <param name="message"></param>
		/// <param name="certPrivate"></param>
		/// <param name="encodedSignedCms"></param>
		/// <returns></returns>
		public bool Sign(ref string errorResponse, byte[] message, X509Certificate2 certPrivate, out byte[] encodedSignedCms, bool displaySysException)
		{
			try
			{
				ContentInfo contentInfo = new ContentInfo(message);
                SignedCms signedCms = new SignedCms(contentInfo, false);
				CmsSigner cmsSigner = new CmsSigner(certPrivate);
				signedCms.ComputeSignature(cmsSigner);
				encodedSignedCms = signedCms.Encode();
				return true;
			}
			catch(Exception ex)
			{
                if (displaySysException == true) { errorResponse = ex.Message; } else { errorResponse = "Cannot find certificate"; }
				encodedSignedCms = new byte[0];
				return false;
			}
		}

	}
}