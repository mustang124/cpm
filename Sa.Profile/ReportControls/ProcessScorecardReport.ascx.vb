Partial Class ProcessScorecardReport
    Inherits ReportBase

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        BuildReport()
    End Sub

    Private Sub BuildReport()
        BuildProcessScorecardBody()
        BuildProcessScorecardFooter()
    End Sub

    Private Sub BuildProcessScorecardBody()
        Dim pluQry As String
        Dim ds As DataSet
        Dim m, t As Integer
        Dim row As DataRow
        Dim sectionName, fieldFormat As String
        Dim capColName As String = IIf(Request.QueryString("UOM").StartsWith("US"), "Cap", "RptCap")
        Dim processRows() As String = {"Unit Name", "Process ID", "Process Type", "Capacity,UOM", "EDC", "UEDC", "Utilization,%", "Mechanical Availability,%", "Operational Availability,%", "On-Stream Factor,%", "Non-Turnaround Expenses, CurrencyCode/bbl", "Turnaround Expenses, CurrencyCode/bbl"}
        Dim columns() As String = {"Unitname", "ProcessID", "ProcessType", capColName, "EDC", "UEDC", "UtilPcnt", "MechAvail", "OpAvail", "OnStream", "RoutCost", "TACost"}
        Dim decPlaces() As Integer = {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2}



        pluQry = "SELECT  p.SortKey,p.Description, c.UnitName, c.ProcessID, c.ProcessType, c." + capColName + ", d.DisplayTextUS, d.DisplayTextMet," + _
                 " fc.EDCNoMult * fc.MultiFactor as EDC, fc.UEDCNoMult * fc.MultiFactor as UEDC, " + _
                "fc.UtilPcnt, u.UtilPcnt as UtilPcnt_Target," + _
                "MechAvail_Ann as MechAvail, MechAvail as MechAvail_Target, " + _
                "OpAvail_Ann as OpAvail, OpAvail as OpAvail_Target," + _
                "OnStream_Ann as OnStream, OnStream as OnStream_Target," + _
                "CASE WHEN c.RptCap > 0 THEN 1000*AnnTACost/c." + capColName + " END as TACost, TACost as TACost_Target, " + _
                "CASE WHEN c.RptCap > 0 THEN 1000*AnnRoutCost/c." + capColName + " END as RoutCost, RoutCost as RoutCost_Target, u.CurrencyCode " + _
                " FROM 	Config c " + _
                "INNER JOIN ProcessID_LU p on c.ProcessID = p.ProcessID " + _
                "INNER JOIN FactorCalc fc on fc.SubmissionID = c.SubmissionID and c.UnitID = fc.UnitID " + _
                "INNER JOIN FactorProcessCalc fpc on fc.SubmissionID = fpc.SubmissionID and fpc.ProcessID = c.ProcessID " + _
                "INNER JOIN MaintCalc m on m.SubmissionID = c.SubmissionID and c.UnitID = m.UnitID " + _
                "INNER JOIN MaintCost mc on mc.SubmissionID = c.SubmissionID and c.UnitID = mc.UnitID " + _
                "INNER JOIN DisplayUnits_LU d on p.DisplayUnits = d.DisplayUnits " + _
                "LEFT JOIN UnitTargets u on c.SubmissionID = u.SubmissionID and c.UnitID = u.UnitID " + _
                "WHERE fc.FactorSet='" + Request.QueryString("yr") + "' AND mc.Currency ='" + Request.QueryString("currency") + "' AND fc.FactorSet=fpc.FactorSet AND C.SubmissionID  IN " + _
                "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + RefineryID + "'" + _
                "  ) ORDER BY p.SortKey"

        Dim db As New DbHelper
        db.RefineryID = RefineryID
        ds = db.QueryDb(pluQry)


        Response.Write("<br>")
        Response.Write(" <table class='small'  border=0 bordercolor='darkcyan' width=98% cellspacing=0 cellpadding=2>")
        'Request.QueryString("avg") = False
        ' Request.QueryString("ytd") = False

        'Build headers
        Response.Write("<td width=60%>&nbsp;</td><td align=center colspan=2 width=20%><strong>" + Format(CDate(Request.QueryString("sd")), "MMMM yyyy") + "</strong></td>")
        If Request.QueryString("target") Then
            Response.Write("<td  align=center colspan=2 width=20%><strong>Process Target</strong></td>")
        End If
        'If Request.QueryString("avg") Then
        '    Response.Write("<td align=center width=10%><strong>12 Month Average</strong></td>")
        'End If
        'If Request.QueryString("ytd") Then
        '    Response.Write("<td align=center width=10%><strong>YTD Average</strong></td>")
        'End If


        For m = 0 To ds.Tables(0).Rows.Count - 1
            If m > 0 Then
                If Not ds.Tables(0).Rows(m - 1)("Description").Trim.StartsWith(ds.Tables(0).Rows(m)("Description").Trim) Then

                    Response.Write("<tr><td colspan=5><strong>" + ds.Tables(0).Rows(m)("Description") + "</strong></td></tr>")
                End If
            Else
                Response.Write("<tr><td colspan=5><strong>" + ds.Tables(0).Rows(m)("Description") + "</strong></td></tr>")
            End If

            For t = 0 To processRows.Length - 1

                'Set number format
                Select Case decPlaces(t)
                    Case 0
                        fieldFormat = "{0:#,##0}"
                    Case 1
                        fieldFormat = "{0:#,##0.0}"
                    Case 2
                        fieldFormat = "{0:N}"
                End Select

                'Show Process Type only if it is available
                If (processRows(t) = "Process Type") AndAlso _
                    (IsDBNull(ds.Tables(0).Rows(m)(columns(t))) Or ds.Tables(0).Rows(m)(columns(t)).Trim = "") Then
                    GoTo skipover 'Doing this because there is no continue in VB.NET
                End If

                If processRows(t) = "" Then
                    ' Response.Write("<td>&nbsp</td><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td>")
                Else
                    'Description column
                    Dim desc, unit As String
                    desc = processRows(t).Replace("CurrencyCode", Request.QueryString("currency"))

                    If Request.QueryString("UOM").StartsWith("US") Then
                        unit = "DisplayTextUS"
                    Else
                        unit = "DisplayTextMet"
                    End If

                    desc = desc.Replace("UOM", ds.Tables(0).Rows(m)(unit))
                    Response.Write("<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc + "&nbsp</td>")
                End If

                If columns(t) <> "" Then
                    'Current Month column
                    If ds.Tables(0).Columns.Contains(columns(t)) Then
                        Response.Write("<td nowrap align=right>" + String.Format(fieldFormat, ds.Tables(0).Rows(m)(columns(t))) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                    Else
                        Response.Write("<td colspan=2>&nbsp</td>")
                    End If

                    'Process Target column
                    If (CBool(Request.QueryString("target"))) And _
                         ds.Tables(0).Columns.Contains(columns(t) + "_Target") Then
                        Response.Write("<td align=right>" + String.Format(fieldFormat, ds.Tables(0).Rows(m)(columns(t) + "_Target")) + "</td><td>&nbsp;&nbsp;&nbsp;</td>")
                    Else
                        Response.Write("<td colspan=2>&nbsp</td>")
                    End If

                    ''Yearly Average column
                    'If (CBool(Request.QueryString("avg"))) And _
                    '    ds.Tables(0).Columns.Contains(columns(t) + "_AVG") Then
                    '    Response.Write("<td align=right>" + String.Format(fieldFormat, ds.Tables(0).Rows(m)(columns(t) + "_AVG")) + "&nbsp;&nbsp;&nbsp;</td>")
                    'Else
                    '    Response.Write("<td>&nbsp</td>")
                    'End If

                    ''Year-to-date column
                    'If (CBool(Request.QueryString("ytd"))) And _
                    '    ds.Tables(0).Columns.Contains(columns(t) + "_YTD") Then
                    '    Response.Write("<td align=right>" + String.Format(fieldFormat, ds.Tables(0).Rows(m)(columns(t) + "_YTD")) + "&nbsp;&nbsp;&nbsp;</td>")
                    'Else
                    '    Response.Write("<td>&nbsp</td>")
                    'End If
skipover:
                Else

                    Response.Write("<td>&nbsp</td><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td>")
                End If


                Response.Write("</tr>")
            Next
            Response.Write("<tr><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td><td>&nbsp</td></tr>")

            If m + 1 < ds.Tables(0).Rows.Count Then
                If ds.Tables(0).Rows(m + 1)("Description") <> ds.Tables(0).Rows(m)("Description") Then
                    Response.Write("<tr><td colspan=5><hr width=100% color=""Black"" SIZE=1></td></tr>")
                End If
            End If
        Next

        Response.Write("</table>")
    End Sub


    Sub BuildProcessScorecardFooter()
        Dim sectionName, fieldFormat As String
        Dim o, n, t As Integer
        Dim processRows(,) As String = { _
                                 {"<strong>Steam Generation, UOM</strong>", _
                               "Solid Fired Boilers", _
                               "Liquid/Gas Fired Boilers", _
                                "", _
                               "<strong>Electric Power Generation, kW</strong>", _
                               "Solid Fired Boiler (Condensing Turbines)", _
                               "Liquid/Gas Boiler (Condensing Turbines)", _
                               "Diesel Engine Driver", _
                               "Fired Turbine Driver (No Steam Produced)", _
                               "Expander Turbine Driver other than FCC", _
                               "Steam Topping (Let-Down) Turbine Driver", _
                               "Extraction Turbine Driver", _
                                "", _
                               "<strong>Cat Cracker Power Recovery Train, bhp</strong>", _
                               ""}, _
                               {"<strong>Raw Material Receipts</strong>", _
                               "Railcar", _
                               "Tank Truck", _
                               "Tanker Berth", _
                               "Offshore Buoy", _
                               "Barge Berth", _
                               "Pipeline", _
                               "", _
                               "<strong>Product Shipments</strong>", _
                               "Railcar", _
                               "Tank Truck", _
                               "Tanker Berth", _
                               "Offshore Buoy", _
                               "Barge Berth", _
                               "Pipeline"}}


        '2-dimensional array  of [<process types>,<processid>]
        Dim Proc_ProcType(,,) As String = {{{"", ""}, {"STEAMGEN", "SFB"}, {"STEAMGEN", "LGFB"}, _
{"", ""}, {"", ""}, {"ELECGEN", "SFB"}, {"ELECGEN", "LGFB"}, {"ELECGEN", "DSL"}, {"ELECGEN", "FT"}, _
{"ELECGEN", "EXP"}, {"ELECGEN", "STT"}, {"ELECGEN", "EXTR"}, {"", ""}, {"FCCPOWER", ""}, {"", ""}}, _
{{"", ""}, {"RSCRUDE", "RAIL"}, {"RSCRUDE", "TT"}, {"RSCRUDE", "TB"}, {"RSCRUDE", "OMB"}, _
{"RSCRUDE", "BB"}, {"RSCRUDE", "PL"}, {"", ""}, {"", ""}, {"RSPROD", "RAIL"}, {"RSPROD", "TT"}, _
 {"RSPROD", "TB"}, {"RSPROD", "OMB"}, {"RSPROD", "BB"}, {"RSPROD", "PL"}}}


        Dim decPlaces(,) As Integer = {{0, 1, 0, 0, 0}, {0, 0, 0, 0, 0}}

        'Handle reported Capacity based on unit of measure 
        Dim capColName As String = IIf(Request.QueryString("UOM").StartsWith("US"), "Cap", "RptCap")
        Dim columns(,) As String = {{capColName, "UtilPcnt", "EDCNoMult", "UEDCNoMult", ""}, _
                                     {"", "BDP", "EDCNoMult", "UEDCNoMult", ""}}

        Dim offsitesQry As String = "SELECT c.UnitID, c.ProcessID, c.ProcessType, c." + capColName + ", ISNULL(c.UtilPcnt,0) AS UtilPcnt, " + _
                         "ISNULL(EDCNoMult,0) AS EDCNoMult, ISNULL(UEDCNoMult,0) AS UEDCNoMult, d.DisplayTextUS, d.DisplayTextMET " + _
                         "FROM config c,factorcalc fc, processid_lu p ,displayunits_lu d " + _
                         "WHERE c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits AND " + _
                         "c.unitid = fc.unitid AND c.submissionid = fc.submissionid AND " + _
                         "c.ProcessID IN ('STEAMGEN', 'ELECGEN', 'FCCPOWER') " + _
                         " AND fc.factorset=" + Request.QueryString("yr").ToString + " AND c.SubmissionId IN " + _
                         "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds").ToString + "' AND Month(PeriodStart)=" + _
                         Month(CDate(Request.QueryString("sd").ToString)).ToString + " AND Year(PeriodStart)=" + Year(CDate(Request.QueryString("sd").ToString)).ToString + _
                         " AND RefineryID='" + RefineryID + "'" + _
                         " ) ORDER BY p.SortKey"


        Dim offsitesRSQry As String = "SELECT c.UnitID, c.ProcessID, c.ProcessType, ISNULL(c.Throughput,0) As BDP, ISNULL(EDCNoMult,0) AS EDCNoMult, " + _
                                    " ISNULL(UEDCNoMult,0) AS UEDCNoMult,d.DisplayTextUS, d.DisplayTextMET  " + _
                                    " FROM ConfigRS c, FactorCalc fc,displayunits_lu d,processid_lu p  " + _
                                    "WHERE c.unitid = fc.unitid and c.submissionid = fc.submissionid  AND " + _
                                    "c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits  " + _
                                    " AND fc.factorset=" + Request.QueryString("yr").ToString + " AND c.SubmissionId IN " + _
                                    "(SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet='" + Request.QueryString("ds").ToString + "' AND Month(PeriodStart)=" + _
                                    Month(CDate(Request.QueryString("sd").ToString)).ToString + " AND Year(PeriodStart)=" + Year(CDate(Request.QueryString("sd").ToString)).ToString + _
                                    " AND RefineryID='" + RefineryID + "'" + _
                                    "  )"

        Dim dsOffsites As DataSet
        Dim HasSTEAM As Boolean
        Dim HasELEC As Boolean
        Dim HasFCC As Boolean

        Dim db As New DbHelper
        db.RefineryID = RefineryID
        dsOffsites = db.QueryDb(offsitesQry)

        HasSTEAM = dsOffsites.Tables(0).Select("ProcessID='STEAMGEN'").Length > 0
        HasELEC = dsOffsites.Tables(0).Select("ProcessID='ELECGEN'").Length > 0
        HasFCC = dsOffsites.Tables(0).Select("ProcessID='FCCPOWER'").Length > 0

        Response.Write("<hr width=100% color=""Black"" SIZE=1>")

        Response.Write(" <table class='small'  border=0 bordercolor='black' width=98% cellspacing=2 cellpadding=2>")

        Response.Write("<tr><td align= colspan=5 style='FONT-WEIGHT: bold; FONT-SIZE: 10pt;'>Utilities and Off-Sites</td></tr>")
        Response.Write("<tr><td align=center colspan=5>&nbsp;</td></tr>")

        For t = 0 To processRows.GetUpperBound(0)
            If t > 0 Then
                dsOffsites = db.QueryDb(offsitesRSQry)
            End If


            If dsOffsites.Tables.Count > 0 Then
                If t = 0 Then

                    'Build headers
                    If HasSTEAM Or HasELEC Or HasFCC Then
                        Response.Write("<tr><td width=35%><strong>&nbsp;</strong></td>")
                        Response.Write("<td  align=right width=15%><strong>Capacity</strong></td>")
                        Response.Write("<td  align=right width=15%><strong>Utilization,%</strong></td>")
                        Response.Write("<td  align=right width=15%><strong>EDC</strong></td>")
                        Response.Write("<td  align=right width=15%><strong>UEDC</strong></td></tr>")
                    End If
                Else
                    Response.Write("<tr><td  width=35%><strong>&nbsp;</strong></td>")
                    Response.Write("<td  align=right width=15%><strong>&nbsp;</strong></td>")
                    Response.Write("<td  align=right width=15%><strong>Barrels</strong></td>")
                    Response.Write("<td  align=right width=15%><strong>EDC</strong></td>")
                    Response.Write("<td  align=right width=15%><strong>UEDC</strong></td></tr>")
                End If

                For n = 0 To 14

                    If Not HasSTEAM And t = 0 Then
                        If n >= 0 And n <= 3 Then
                            GoTo skipover
                        End If
                    End If

                    If Not HasELEC And t = 0 Then
                        If n >= 4 And n <= 12 Then
                            GoTo skipover
                        End If
                    End If


                    If Not HasFCC And t = 0 Then
                        If n = 13 Then
                            GoTo skipover
                        End If
                    End If

                    If processRows(t, n) = "HR" Then
                        Response.Write("<tr><td colspan=5><hr width=100% color=""Black"" SIZE=1></td>")
                        GoTo skipover
                    End If

                    ' Build Description column
                    '----------------------------
                    If processRows(t, n) = "" Then
                        Response.Write("<tr>")
                        Response.Write("<td>&nbsp</td>")

                    Else
                        'Description column
                        Dim desc, unit As String
                        desc = processRows(t, n)
                        'Make sub head
                        If n > 0 Then
                            desc = SubHead(processRows(t, n - 1), desc)
                        End If

                        'Handle Currency
                        desc = desc.Replace("CurrencyCode", Request.QueryString("currency"))
                        'Handle Units
                        unit = "DisplayTextUS"
                        If Not Request.QueryString("UOM").StartsWith("US") Then
                            unit = "DisplayTextMET"
                        End If
                        If dsOffsites.Tables(0).Rows.Count > 0 Then
                            desc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc.Replace("UOM", dsOffsites.Tables(0).Rows(0)(unit))
                        Else
                            desc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc.Replace("UOM", "klb / h")
                        End If

                        Response.Write("<tr>")
                        If desc.IndexOf("<strong>") = -1 Then
                            Response.Write("<td valign=top nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc + "&nbsp;</td>")
                        Else
                            Response.Write("<td valign=top>" + desc + "&nbsp;</td>")
                        End If
                    End If


                    'FILTER BY ProcessID and Process Type
                    Dim processID As String = Proc_ProcType(t, n, 0)
                    Dim processType As String = Proc_ProcType(t, n, 1)
                    Dim rows() As DataRow = dsOffsites.Tables(0).Select("ProcessID='" + processID + "' and ProcessType='" + processType + "'")

                    'Build Value Columns
                    ' -------------------------------
                    If rows.Length > 0 Then
                        For o = 0 To 3
                            'Set number format
                            Select Case decPlaces(t, o)
                                Case 0
                                    fieldFormat = "{0:#,##0}"
                                Case 1
                                    fieldFormat = "{0:#,##0.0}"
                                Case 2
                                    fieldFormat = "{0:N}"
                            End Select
                            If columns(t, o) <> "" Then
                                'Current Month column
                                If dsOffsites.Tables(0).Columns.Contains(columns(t, o)) Then
                                    Response.Write("<td  align=right> " + String.Format(fieldFormat, rows(0)(columns(t, o))).Trim + "</td>")
                                Else
                                    Response.Write("<td>&nbsp;</td>")
                                End If 'table contains column name
                            Else
                                Response.Write("<td>&nbsp;</td>")
                            End If 'column name is empty string
                        Next

                    Else
                        Response.Write("<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>")
                    End If 'No Rows in table
skipover:           Response.Write("</tr>")
                    'Add Blank Row 
                    'Response.Write("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>")
                Next
            End If
        Next


        Response.Write("</table>")
    End Sub

End Class
