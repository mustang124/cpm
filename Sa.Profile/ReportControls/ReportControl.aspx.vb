Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports System.IO
Imports GenrateInTableHtml

Partial Class ReportControl
    Inherits System.Web.UI.Page
    'Private Const C_HTTP_HEADER_CONTENT As String = "Content-Disposition"
    'Private Const C_HTTP_ATTACHMENT As String = "attachment;filename="
    'Private Const C_HTTP_INLINE As String = "inline;filename="
    'Private Const C_HTTP_CONTENT_TYPE_OCTET As String = "application/octet-stream"
    'Private Const C_HTTP_CONTENT_TYPE_EXCEL As String = "application/ms-excel"
    'Private Const C_HTTP_CONTENT_LENGTH As String = "Content-Length"
    'Private Const C_QUERY_PARAM_CRITERIA As String = "Criteria"
    'Protected WithEvents C1SubMenu1 As C1.Web.C1Command.C1SubMenu
    'Private Const C_ERROR_NO_RESULT As String = "Data not found."
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected refineryId As String
    Protected baseControl As Control
    Protected WSP As String
    Dim db As New DbHelper


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        System.Net.ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy

        WSP = Request.Headers("WsP")
        If IsNothing(WSP) Then
            WSP = Request.QueryString("WsP")
        End If

        If IsNothing(WSP) Then
            Server.Transfer("InvalidRequest.htm")
        End If

        If Not ValidateKey() And Not IsPostBack Then
            Server.Transfer("InvalidUser.htm")
        End If

        'Get refinery id from key
        refineryId = GetRefineryID()
        db.RefineryID = refineryId
    End Sub

    Private Function GetRefineryID() As String
        Dim refId As String
        Dim refLocation As Integer = Decrypt(WSP).Split("$".ToCharArray).Length - 1
        refId = Decrypt(WSP).Split("$".ToCharArray)(refLocation)
        Return refId
    End Function

    Private Function ValidateKey() As Boolean
        Dim company, readKey, location, path As String
        Dim locIndex As Integer
        Try
            If Not IsNothing(WSP) Then
                company = Decrypt(WSP).Split("$".ToCharArray)(0)

                locIndex = Decrypt(WSP).Split("$".ToCharArray).Length - 2
                location = Decrypt(WSP).Split("$".ToCharArray)(locIndex)

                If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                    path = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Else
                    Return False
                End If

                If path.Trim.Length = 0 Then
                    Return False
                End If

                Dim reader As New StreamReader(path)
                readKey = reader.ReadLine()
                reader.Close()
                If readKey.Length > 0 Then
                    Return (readKey.Trim = WSP.Trim)
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Function AreCalcsDone() As Boolean
        Dim sqlstmt As String = "SELECT Distinct SubmissionID,CalcsNeeded FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'"

        Dim ds As DataSet = db.QueryDb(sqlstmt)
        Dim calcsNeeded As Object

        If ds.Tables(0).Rows.Count > 0 Then
            calcsNeeded = ds.Tables(0).Rows(0)("CalcsNeeded")
            Return IsDBNull(calcsNeeded) Or IsNothing(calcsNeeded)
        Else
            Return False
        End If
    End Function

    Public Function GetCoLoc() As String
        Dim sqlstmt As String = "SELECT Distinct SubmissionID,Company,Location FROM Submissions WHERE  DataSet='" + Request.QueryString("ds") + _
                                "' AND Month(PeriodStart)=" + Month(CDate(Request.QueryString("sd"))).ToString + " AND Year(PeriodStart)=" + _
                                Year(CDate(Request.QueryString("sd"))).ToString + " AND RefineryID='" + refineryId + "'"

        Dim ds As DataSet = db.QueryDb(sqlstmt)
        Dim colocation As String

        If ds.Tables(0).Rows.Count > 0 Then
            colocation = ds.Tables(0).Rows(0)("Company") & " - " & ds.Tables(0).Rows(0)("Location")
            Return colocation
        Else
            Return String.Empty
        End If
    End Function

    Public Sub GetReport()
        If AreCalcsDone() Then
            Dim ds As New DataSet

            ' GMO 10/16/2007
            Dim tReportName As String = Request.QueryString("rn").ToUpper
            Dim tReportDataFilter As String
            Dim htmlText As String
            Dim Scenario As String = "CLIENT"
            ' END GMO

            ' GMO 12/5/2007
            Dim tReportTitle As String
            ' END GMO
            If (Not (Request.QueryString("sn") Is Nothing) AndAlso _
                (Request.QueryString("sn").Length > 0)) Then
                If Scenario <> "BASE" Then
                    Scenario = Request.QueryString("sn")
                End If
            End If
            '"http:\\localhost\ReportControls\ReportControl.aspx?rn=Refinery Scorecard&sd=5/1/2013&ds=ACTUAL&UOM=MET&currency=USD&yr=2010&
            'target=True&avg=True&ytd=True&SN=CLIENT&TS=8/18/2014 09:39:11 AM&WsP=KUBJ9o3kg9MjXyCahjocyYpTWjLMdFYLuFnPQirvhJh3CZThYA8Qwg=="
            ds = GetDataSetForReport(GetRefineryID(), _
                                    Request.QueryString("rn"), _
                                    Year(CDate(Request.QueryString("sd").ToString)).ToString, _
                                    Month(CDate(Request.QueryString("sd").ToString)).ToString, _
                                    Request.QueryString("ds").ToString, _
                                    Request.QueryString("yr").ToString, _
                                    Scenario, _
                                    Request.QueryString("Currency"), _
                                    Request.QueryString("UOM"))

            Select Case Request.QueryString("rn").ToUpper

                Case "REFINERY TRENDS REPORT"

                    Dim bYTD As Boolean = CType(Request.QueryString("ytd"), Boolean)
                    Dim bAVG As Boolean = CType(Request.QueryString("avg"), Boolean)

                    If (bYTD = True And bAVG = True) Or (bYTD = False And bAVG = False) Then
                        ' show total fields only
                        tReportDataFilter = "Current Month"
                        ds.Tables("Chart_LU").Columns("TotField").ColumnName = "ValueField1"
                    Else
                        If bYTD = True Then
                            ' show ytd fields only
                            tReportDataFilter = "Year To Date"
                            ds.Tables("Chart_LU").Columns("YTDField").ColumnName = "ValueField1"
                        Else
                            If bAVG = True Then
                                ' show avg fields only
                                ds.Tables("Chart_LU").Columns("AVGField").ColumnName = "ValueField1"
                                tReportDataFilter = "Rolling Average"
                            End If
                        End If
                    End If



                Case "EII AND VEI BREAKDOWN"

                    'Dim n As Integer
                    'Dim dtFunction As DataTable = ds.Tables("Functions")
                    'Dim dtProcessData As DataTable = ds.Tables("Process Data")
                    'For n = 0 To dtFunction.Rows.Count - 1
                    '    'Do substititons in formulas
                    '    Dim eiiFormula As String = dtFunction.Rows(n)("EIIFormulaForReport").ToString
                    '    Dim veiFormula As String = dtFunction.Rows(n)("VEIFormulaForReport").ToString
                    '    Dim unitID As String = dtFunction.Rows(n)("UnitID").ToString
                    '    ' Dim submissionID As String = dtFunction.Rows(n)("SubmissionID").ToString
                    '    Dim varRows() As DataRow = dtProcessData.Select("UnitId='" + unitID + "'")

                    '    If dtProcessData.Rows.Count > 0 Then
                    '        Dim j As Integer
                    '        Dim variables As String = ""
                    '        Dim fieldFormat As String = ""
                    '        For j = 0 To varRows.Length - 1
                    '            Select Case varRows(j)("UsDecPlaces")
                    '                Case 0
                    '                    fieldFormat = "{0:#,##0}"
                    '                Case 1
                    '                    fieldFormat = "{0:#,##0.0}"
                    '                Case 2
                    '                    fieldFormat = "{0:N}"
                    '            End Select
                    '            eiiFormula = eiiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))
                    '            veiFormula = veiFormula.Replace(varRows(j)("Property").ToString, Chr(Asc("A") + j))

                    '            variables += "<tr>"
                    '            variables += "<td colspan=2>&nbsp;</td>"
                    '            variables += "<td>" + Chr(Asc("A") + j) + " - " + varRows(j)("UsDescription").Trim.ToString + "</td>"
                    '            variables += "<td  align=right>" + String.Format(fieldFormat, varRows(j)("SAValue")).ToString.Trim + "</td>"
                    '            variables += "<td>&nbsp;</td>"
                    '            variables += "</tr>"
                    '        Next
                    '        dtFunction.Rows(n)("EIIFormulaForReport") = eiiFormula
                    '        dtFunction.Rows(n)("VEIFormulaForReport") = veiFormula
                    '        dtFunction.Rows(n)("Variables") = variables

                    '    End If
                    'Next

            End Select

            If ds.Tables.Count = 0 Then
                Exit Sub
            End If

            Dim pg As New GenrateInTableHtml.Page
            Dim replacementValues As New Hashtable
            replacementValues.Add("ReportPeriod", Format(CDate(Request.QueryString("sd")), "MMMM yyyy"))
            replacementValues.Add("IfTargetOn", Request.QueryString("target"))
            replacementValues.Add("IfYtdOn", Request.QueryString("ytd"))
            replacementValues.Add("IfAvgOn", Request.QueryString("avg"))
            replacementValues.Add("CoLoca", GetCoLoc())


            ' GMO 10/16/2007
            If tReportName = "REFINERY TRENDS REPORT" Then
                replacementValues.Add("ReportDataFilter", tReportDataFilter)
            End If
            ' END GMO

            htmlText += pg.createPage(ds, Request.QueryString("rn").ToUpper, Request.QueryString("UOM").Trim(), Request.QueryString("currency").Trim(), replacementValues)

            Response.Write(htmlText)
        Else
            Response.Redirect("CalcsAreNotFinished.htm")
        End If


    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' This section added by GMO 12/6/2007
    ' Function invoked from HTML in order to get long description
    ' for the report title.  Used only for process unit reports.
    Public Function GetReportTitle(ByVal tProcessID As String) As String
        Dim tQuery As String
        Dim tReportTitle As String
        Dim tReportCode As String

        ' First 3 or 4 characters contain code
        tReportCode = Mid(tProcessID, 1, 4)

        ' Construct query with parameters
        tQuery = "SELECT Description FROM ProcessID_LU WHERE ProcessID = '" & Trim(tReportCode) & "'"

        ' Get dataset for the report
        Dim dt As DataTable = db.QueryDb(tQuery).Tables(0)
        tReportTitle = dt.Rows(0).Item("Description") & " Report"

        GetReportTitle = tReportTitle

    End Function
    ' END GMO 12/4/2007
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private Function GetDataSetForReport(ByVal refineryID As String, _
                                            ByVal reportName As String, _
                                            ByVal reportYear As Integer, _
                                            ByVal reportMonth As Integer, _
                                            ByVal dataSet As String, _
                                            ByVal factorSet As String, _
                                            ByVal scenario As String, _
                                            ByVal currency As String, _
                                            ByVal UOM As String) As DataSet
        Dim ds As New DataSet
        Dim dtStProc As DataTable = db.QueryDb("exec spGetReportProcs '" + refineryID + "','" + reportName + "'").Tables(0)
        Dim a As Integer
        Dim execString As String
        Dim tablenames(dtStProc.Rows.Count) As String

        For a = 0 To dtStProc.Rows.Count - 1
            Dim procedureName As String = dtStProc.Rows(a)("ProcName")
            tablenames(a) = dtStProc.Rows(a)("DataTableName")

            execString += "exec " + procedureName + " '" + _
                                           refineryID + "'," + _
                                           reportYear.ToString() + "," + _
                                           reportMonth.ToString() + ",'" + _
                                           dataSet + "','" + _
                                           factorSet + "','" + _
                                           scenario + "','" + _
                                           currency + "','" + _
                                           UOM + "';"
        Next

        ds = db.QueryDb(execString)
        For a = 0 To ds.Tables.Count - 1
            ds.Tables(a).TableName = tablenames(a)
        Next

        Return ds
    End Function

    'Private Function GetSelectList(ByVal tTableName As String) As String
    '    Dim dsSelectList As New DataSet
    '    Dim bYTD As Boolean = Request.QueryString("ytd")
    '    Dim bAvg As Boolean = Request.QueryString("avg")
    '    Dim tNameSuffix As String
    '    Dim tSQL As String
    '    Dim tTable As String
    '    Dim iColumnCount As Integer
    '    Dim i As Integer
    '    Dim tColName As String
    '    Dim tSelectList As String = ""
    '    Dim tSuffix As String

    '    If (bYTD = True And bAvg = True) Or (bYTD = False And bAvg = False) Then
    '        tNameSuffix = "NONE"
    '    Else
    '        If bYTD = True Then
    '            tNameSuffix = "_YTD"
    '        Else
    '            If bAvg = True Then
    '                tNameSuffix = "_Avg"
    '            End If
    '        End If
    '    End If

    '    tSQL = _
    '    "SELECT " _
    '    & "o.name, " _
    '    & "c.name, " _
    '    & "SUBSTRING(c.name, (len(c.name)-3), 4) as NameSuffix, " _
    '    & "SUBSTRING(c.name, 1, (len(c.name)-3)) as NamePrefix " _
    '    & "FROM sysobjects o, syscolumns c " _
    '    & "where " _
    '    & "o.name = '" & tTable & "' " _
    '    & "and " _
    '    & "o.id = c.id " _
    '    & "order by NameSuffix desc "

    '    Dim dtSelectList As DataTable = QueryDb(tSQL).Tables(0)
    '    dsSelectList.Merge(dtSelectList)

    '    iColumnCount = dsSelectList.Tables(0).Columns.Count()

    '    If tNameSuffix <> "NONE" Then
    '        For i = 0 To iColumnCount
    '            tColName = dsSelectList.Tables(0).Columns(i).ToString
    '            tSuffix = Mid(Trim(tColName), (Len(tColName) - 4), 4)

    '            If (tSuffix = tNameSuffix) Then
    '                tSelectList = tSelectList & tColName & ", "
    '            Else
    '                If (tSuffix <> "rget") Then
    '                    tSelectList = tSelectList & tColName & ", "
    '                End If
    '            End If
    '        Next
    '    Else
    '        For i = 0 To iColumnCount
    '            tColName = dsSelectList.Tables(0).Columns(i).ToString
    '            tSuffix = Mid(Trim(tColName), (Len(tColName) - 4), 4)

    '            If (tSuffix <> "rget") And (tSuffix <> "_YTD") And (tSuffix <> "_Avg") Then
    '                tSelectList = tSelectList & tColName & ", "
    '            End If
    '        Next
    '    End If

    'End Function ' GetSelectList()_


    'Private Sub TransmitFile(ByVal [output] As String, ByVal extension As String)
    '    Dim filename As String = Page.MapPath("data\report." & extension)
    '    'Dim c1Report As New C1.Web.C1WebReport.C1WebReport
    '    ' Delete old data for precaution 
    '    'If System.IO.File.Exists(filename) Then
    '    'System.IO.File.Delete(filename)
    '    'End If

    '    ' c1Report.Report.RenderToFile(filename, GetFileType([output]))
    '    'Response.WriteFile(filename)
    '    'Dim file As System.IO.FileInfo = New System.IO.FileInfo(filename)

    '    Response.Clear()
    '    Response.ClearHeaders()
    '    Response.AddHeader(C_HTTP_HEADER_CONTENT, C_HTTP_ATTACHMENT & "report." & extension)
    '    Response.ContentType = "application/octet-stream"
    '    'Response.AddHeader(C_HTTP_CONTENT_LENGTH, File.Length())
    '    'Response.TransmitFile(filename)
    '    Response.Flush()

    'End Sub

    'Private Function GetFileType(ByVal [output] As String) As C1.Win.C1Report.FileFormatEnum
    '    Dim ct As C1.Win.C1Report.FileFormatEnum = C1.Win.C1Report.FileFormatEnum.Text
    '    Try
    '        ct = CType([Enum].Parse(GetType(C1.Win.C1Report.FileFormatEnum), [output]), C1.Win.C1Report.FileFormatEnum)
    '    Catch
    '    End Try
    '    Return ct
    'End Function 'getChartType


    'Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    ' C1WebReport1.ShowPDF()
    'End Sub

    'Private Sub WordMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    'Private Sub RTFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("RTF", "rtf")
    'End Sub

    'Private Sub PDFMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("PDF", "pdf")
    'End Sub

    'Private Sub ExcelMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub

    'Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TransmitFile("Excel", "xls")
    'End Sub


End Class
