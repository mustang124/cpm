'----------------------------------------------------------------------------------------
' Copyright � 2008 HSB Solomon Associates LLC, All rights reserved
' Solomon Associates
' Two Galleria Tower
' 13455 Noel Road, Suite 1500
' Dallas, Texas 75240
'----------------------------------------------------------------------------------------

Option Compare Binary
Option Explicit On
Option Strict On

Friend Module modData

    Friend Sub verifyListMapping(ByVal dt As DataTable)
        ' verifys that one row exists for mapping when the mapping table does not use cell references
        If dt.Rows.Count <> 1 Then
            Dim newDR As DataRow = dt.NewRow
            dt.Rows.Clear()
            dt.Rows.Add(newDR)
        End If
    End Sub

    Friend Function pivotToDataTableOpex(ByVal dt As DataTable) As DataTable


        Dim pbm As New ProfileBusinessManager.Classes.Utility
        Dim pe As New ProfileEnvironment.Culture

        Dim dtDest As New DataTable
        dtDest.Columns.Add("OpexID", GetType(String))
        dtDest.Columns.Add("costsRefinery", GetType(Single))

        Dim foreign As Boolean
        pe = pe.GetCulture()

        If pe.CultureName = "en-US" Then
            foreign = False
        Else
            foreign = True
        End If

        Dim test As String
        Dim index As Integer = 0
        Try
            For Each c As DataColumn In dt.Columns

                'We have to check the data type of the column. Because if the data type is a single this means the user has done a refresh. If is a string data type, 
                'then the user has made changes to the local copy xml from a previous time. In order to write to the Opex.xml correctly upon changes and save, the data type 
                'of the column is converted to a string data type.
                If foreign = True Then
                    Dim costRefineryValue As Single

                    'Have to check if DbNull
                    Dim objItem As Object
                    objItem = dt.Rows(0).Item(c)

                    If IsDBNull(objItem) = False Then
                        costRefineryValue = pbm.StringToSingle(Convert.ToString(dt.Rows(0).Item(c.ToString)))
                    Else
                        costRefineryValue = 0
                    End If

                    If dt.Columns(1).DataType.ToString() = "System.Single" Then

                        'Have to check if DbNull
                        Dim obj As Object
                        obj = dt.Rows(0).Item(c)

                        If IsDBNull(obj) = False Then
                            dtDest.Rows.Add(c.ToString(), dt.Rows(0).Item(c.ToString))
                        Else
                            dtDest.Rows.Add(c.ToString(), Convert.ToSingle(0))
                        End If
                    Else
                        dtDest.Rows.Add(c.ToString(), costRefineryValue)
                    End If
                Else
                    If dt.Columns(1).DataType.ToString() = "System.Single" Then

                        'Have to check if DbNull
                        Dim obj As Object
                        obj = dt.Rows(0).Item(c)

                        If IsDBNull(obj) = False Then
                            dtDest.Rows.Add(c.ToString(), Convert.ToSingle(dt.Rows(0).Item(c.ToString)))
                        Else
                            dtDest.Rows.Add(c.ToString(), Convert.ToSingle(0))
                        End If
                    Else
                        dtDest.Rows.Add(c.ToString(), dt.Rows(0).Item(c.ToString))
                    End If
                End If

                index = index + 1
            Next c

            pivotToDataTableOpex = dtDest
            dtDest.Dispose()

        Catch ex As Exception
            Throw New Exception("Error: pivotToDataTableOpex - " & ex.Message)
            'Dim temp As String = ex.Message & "Item :" & test & "Index: " & index
        End Try


    End Function

    Friend Function pivotToDataTableRev(ByVal dt As DataTable) As DataTable

        Dim dtDest As New DataTable
        Dim drDest As DataRow

        For Each r As DataRow In dt.Rows
            dtDest.Columns.Add(r(0).ToString)
        Next r

        For i As Integer = 2 To dt.Columns.Count - 1

            drDest = dtDest.NewRow()
            For Each r As DataRow In dt.Rows
                drDest(r(0).ToString()) = r(i).ToString()
            Next r
            dtDest.Rows.Add(drDest)

        Next i

        pivotToDataTableRev = dtDest
        dtDest.Dispose()

    End Function

    'Friend Function IsDBNull(ByVal dt As DataTable) As DataTable

    '    Dim dtDest As New DataTable
    '    Dim drDest As DataRow

    '    For Each r As DataRow In dt.Rows
    '        dtDest.Columns.Add(r(0).ToString)
    '    Next r

    '    For i As Integer = 2 To dt.Columns.Count - 1

    '        drDest = dtDest.NewRow()
    '        For Each r As DataRow In dt.Rows
    '            drDest(r(0).ToString()) = r(i).ToString()
    '        Next r
    '        dtDest.Rows.Add(drDest)

    '    Next i

    '    pivotToDataTableRev = dtDest
    '    dtDest.Dispose()

    'End Function



End Module
