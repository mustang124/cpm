Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Solomon Profile� II")> 
<Assembly: AssemblyDescription("Monitoring Refinery Improvement Progress")> 
<Assembly: AssemblyCompany("HSB Solomon Associates LLC")> 
<Assembly: AssemblyProduct("Solomon Profile� II")> 
<Assembly: AssemblyCopyright("� 2011 HSB Solomon Associates LLC")> 
<Assembly: AssemblyTrademark("Solomon Profile� is a Registerd Trademark of HSB Solomon Associates LLC")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("26AFB954-80A3-40B7-BF4A-9B30689E3F33")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("4.2012.06.18")> 

<Assembly: AssemblyDelaySign(False)> 
<Assembly: AssemblyKeyFile("..\..\SolomonProfileII.snk")> 
<Assembly: AssemblyKeyName("")> 
<Assembly: System.Runtime.CompilerServices.InternalsVisibleTo("TestProfile, PublicKey=0024000004800000940000000602000000240000525341310004000001000100b71621633ec112985e010f700a02d8d2afda59f2c0fb58605f3de0d4eeaee5845cb0957fb245a244ea1e976359b22c0f75203f2ac1e9ac52461a5396525a5db5c809835b658dc7fabcb9bc600d778847b44c2e0b68a15dd3607e1588e5bfc82425fcc30ee05a14d03a4d23ca4b133a44d9f7a887517f855d808bb469efa017e5")> 
