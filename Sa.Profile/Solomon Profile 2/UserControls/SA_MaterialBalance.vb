Option Explicit On

Friend Class SA_MaterialBalance
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl1 overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtProdAvgCost As System.Windows.Forms.TextBox
    Friend WithEvents txtProdTotBBL As System.Windows.Forms.TextBox
    Friend WithEvents lblRMCAvg As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tabData As System.Windows.Forms.TabControl
    Friend WithEvents tpRM As System.Windows.Forms.TabPage
    Friend WithEvents tpProd As System.Windows.Forms.TabPage
    Friend WithEvents pnlTools As System.Windows.Forms.Panel
    Friend WithEvents dgvProd As System.Windows.Forms.DataGridView
    Friend WithEvents pnlLU As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents dgvLU As System.Windows.Forms.DataGridView
    Friend WithEvents cmbCategory As System.Windows.Forms.ComboBox
    Friend WithEvents MaterialID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MaterialName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PriceLocal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SortKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MatlID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAIName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SortKey_LU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BPSortKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInRMI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInOTHRM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInRLube As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInRChem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInFLube As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInFChem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInASP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInCoke As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInSolv As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllowInMProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LubesOnly As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbFooterYield As System.Windows.Forms.GroupBox
    Friend WithEvents dgRM_MAP As System.Windows.Forms.DataGridView
    Friend WithEvents lblProdYields As System.Windows.Forms.Label
    Friend WithEvents lblRawMaterialInputs As System.Windows.Forms.Label
    Friend WithEvents gbTools As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbSheets1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents rbLinks As System.Windows.Forms.RadioButton
    Friend WithEvents rbData As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbSheets As System.Windows.Forms.ComboBox
    Friend WithEvents tbFilePath As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents gbFooterRaw As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblRMCAvg1 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtRMTotBBL As System.Windows.Forms.TextBox
    Friend WithEvents txtRMAvgCost As System.Windows.Forms.TextBox
    Friend WithEvents dgProd_MAP As System.Windows.Forms.DataGridView
    Friend WithEvents MaterialIDProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CategoryProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MaterialNameProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BBLProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PriceLocalProd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents pnlHeader As System.Windows.Forms.Panel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblHeader As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkStartRow As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkEndRow As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkMaterialID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkMaterialName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkBBL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lkPriceLocal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvRM As System.Windows.Forms.DataGridView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SA_MaterialBalance))
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlData = New System.Windows.Forms.Panel
        Me.tabData = New System.Windows.Forms.TabControl
        Me.tpRM = New System.Windows.Forms.TabPage
        Me.dgRM_MAP = New System.Windows.Forms.DataGridView
        Me.lkStartRow = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkEndRow = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkMaterialID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkMaterialName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkBBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lkPriceLocal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvRM = New System.Windows.Forms.DataGridView
        Me.MaterialID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Category = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MaterialName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BBL = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PriceLocal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SortKey = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFooterRaw = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblRMCAvg1 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtRMTotBBL = New System.Windows.Forms.TextBox
        Me.txtRMAvgCost = New System.Windows.Forms.TextBox
        Me.lblRawMaterialInputs = New System.Windows.Forms.Label
        Me.tpProd = New System.Windows.Forms.TabPage
        Me.dgProd_MAP = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvProd = New System.Windows.Forms.DataGridView
        Me.MaterialIDProd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CategoryProd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MaterialNameProd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BBLProd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PriceLocalProd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFooterYield = New System.Windows.Forms.GroupBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblRMCAvg = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtProdTotBBL = New System.Windows.Forms.TextBox
        Me.txtProdAvgCost = New System.Windows.Forms.TextBox
        Me.lblProdYields = New System.Windows.Forms.Label
        Me.pnlTools = New System.Windows.Forms.Panel
        Me.gbTools = New System.Windows.Forms.GroupBox
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.lblView = New System.Windows.Forms.Label
        Me.btnEdit = New System.Windows.Forms.Button
        Me.rbLinks = New System.Windows.Forms.RadioButton
        Me.rbData = New System.Windows.Forms.RadioButton
        Me.btnAdd = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.cbSheets1 = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cbSheets = New System.Windows.Forms.ComboBox
        Me.tbFilePath = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.pnlLU = New System.Windows.Forms.Panel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.dgvLU = New System.Windows.Forms.DataGridView
        Me.MatlID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SAIName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SortKey_LU = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BPSortKey = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInRMI = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInOTHRM = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInRLube = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInRChem = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInProd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInFLube = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInFChem = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInASP = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInCoke = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInSolv = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AllowInMProd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LubesOnly = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cmbCategory = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.pnlHeader = New System.Windows.Forms.Panel
        Me.lblHeader = New System.Windows.Forms.Label
        Me.pnlData.SuspendLayout()
        Me.tabData.SuspendLayout()
        Me.tpRM.SuspendLayout()
        CType(Me.dgRM_MAP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFooterRaw.SuspendLayout()
        Me.tpProd.SuspendLayout()
        CType(Me.dgProd_MAP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvProd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFooterYield.SuspendLayout()
        Me.pnlTools.SuspendLayout()
        Me.gbTools.SuspendLayout()
        Me.pnlLU.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvLU, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlData
        '
        Me.pnlData.BackColor = System.Drawing.SystemColors.Control
        Me.pnlData.Controls.Add(Me.tabData)
        Me.pnlData.Controls.Add(Me.pnlTools)
        Me.pnlData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(4, 40)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(742, 556)
        Me.pnlData.TabIndex = 886
        '
        'tabData
        '
        Me.tabData.Controls.Add(Me.tpRM)
        Me.tabData.Controls.Add(Me.tpProd)
        Me.tabData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabData.ItemSize = New System.Drawing.Size(200, 18)
        Me.tabData.Location = New System.Drawing.Point(0, 105)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedIndex = 0
        Me.tabData.Size = New System.Drawing.Size(742, 451)
        Me.tabData.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tabData.TabIndex = 891
        '
        'tpRM
        '
        Me.tpRM.BackColor = System.Drawing.Color.Transparent
        Me.tpRM.Controls.Add(Me.dgvRM)
        Me.tpRM.Controls.Add(Me.dgRM_MAP)
        Me.tpRM.Controls.Add(Me.gbFooterRaw)
        Me.tpRM.Controls.Add(Me.lblRawMaterialInputs)
        Me.tpRM.Location = New System.Drawing.Point(4, 22)
        Me.tpRM.Name = "tpRM"
        Me.tpRM.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.tpRM.Size = New System.Drawing.Size(734, 425)
        Me.tpRM.TabIndex = 0
        Me.tpRM.Text = "Table 15 - Other Raw Materials"
        Me.tpRM.UseVisualStyleBackColor = True
        '
        'dgRM_MAP
        '
        Me.dgRM_MAP.AllowUserToAddRows = False
        Me.dgRM_MAP.AllowUserToDeleteRows = False
        Me.dgRM_MAP.AllowUserToResizeColumns = False
        Me.dgRM_MAP.AllowUserToResizeRows = False
        Me.dgRM_MAP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgRM_MAP.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgRM_MAP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgRM_MAP.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.lkStartRow, Me.lkEndRow, Me.lkCategory, Me.lkMaterialID, Me.lkMaterialName, Me.lkBBL, Me.lkPriceLocal})
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgRM_MAP.DefaultCellStyle = DataGridViewCellStyle15
        Me.dgRM_MAP.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgRM_MAP.Location = New System.Drawing.Point(6, 36)
        Me.dgRM_MAP.MultiSelect = False
        Me.dgRM_MAP.Name = "dgRM_MAP"
        Me.dgRM_MAP.RowHeadersVisible = False
        Me.dgRM_MAP.RowHeadersWidth = 25
        Me.dgRM_MAP.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgRM_MAP.Size = New System.Drawing.Size(722, 305)
        Me.dgRM_MAP.TabIndex = 958
        Me.dgRM_MAP.Visible = False
        '
        'lkStartRow
        '
        Me.lkStartRow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkStartRow.DataPropertyName = "lkStartRow"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkStartRow.DefaultCellStyle = DataGridViewCellStyle8
        Me.lkStartRow.HeaderText = "Starting Row (integer)"
        Me.lkStartRow.Name = "lkStartRow"
        Me.lkStartRow.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.lkStartRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkEndRow
        '
        Me.lkEndRow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkEndRow.DataPropertyName = "lkEndRow"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkEndRow.DefaultCellStyle = DataGridViewCellStyle9
        Me.lkEndRow.HeaderText = "Ending Row (integer)"
        Me.lkEndRow.Name = "lkEndRow"
        Me.lkEndRow.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.lkEndRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkCategory
        '
        Me.lkCategory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkCategory.DataPropertyName = "lkCategory"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkCategory.DefaultCellStyle = DataGridViewCellStyle10
        Me.lkCategory.HeaderText = "Category Column (letter)"
        Me.lkCategory.Name = "lkCategory"
        Me.lkCategory.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.lkCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkMaterialID
        '
        Me.lkMaterialID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkMaterialID.DataPropertyName = "lkMaterialID"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lkMaterialID.DefaultCellStyle = DataGridViewCellStyle11
        Me.lkMaterialID.HeaderText = "Material ID Column (letter)"
        Me.lkMaterialID.Name = "lkMaterialID"
        Me.lkMaterialID.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.lkMaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkMaterialName
        '
        Me.lkMaterialName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkMaterialName.DataPropertyName = "lkMaterialName"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lkMaterialName.DefaultCellStyle = DataGridViewCellStyle12
        Me.lkMaterialName.HeaderText = "Material Name Column (letter)"
        Me.lkMaterialName.Name = "lkMaterialName"
        Me.lkMaterialName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkBBL
        '
        Me.lkBBL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkBBL.DataPropertyName = "lkBBL"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lkBBL.DefaultCellStyle = DataGridViewCellStyle13
        Me.lkBBL.HeaderText = "Barrels Input Column (letter)"
        Me.lkBBL.Name = "lkBBL"
        Me.lkBBL.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.lkBBL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lkPriceLocal
        '
        Me.lkPriceLocal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.lkPriceLocal.DataPropertyName = "lkPriceLocal"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lkPriceLocal.DefaultCellStyle = DataGridViewCellStyle14
        Me.lkPriceLocal.HeaderText = "Price Column (letter)"
        Me.lkPriceLocal.Name = "lkPriceLocal"
        Me.lkPriceLocal.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.lkPriceLocal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgvRM
        '
        Me.dgvRM.AllowUserToAddRows = False
        Me.dgvRM.AllowUserToResizeColumns = False
        Me.dgvRM.AllowUserToResizeRows = False
        Me.dgvRM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRM.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRM.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MaterialID, Me.Category, Me.MaterialName, Me.BBL, Me.PriceLocal, Me.SortKey})
        Me.dgvRM.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvRM.Location = New System.Drawing.Point(6, 36)
        Me.dgvRM.MultiSelect = False
        Me.dgvRM.Name = "dgvRM"
        Me.dgvRM.RowHeadersWidth = 25
        Me.dgvRM.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvRM.Size = New System.Drawing.Size(722, 305)
        Me.dgvRM.TabIndex = 0
        '
        'MaterialID
        '
        Me.MaterialID.DataPropertyName = "MaterialID"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MaterialID.DefaultCellStyle = DataGridViewCellStyle2
        Me.MaterialID.HeaderText = "Material ID"
        Me.MaterialID.Name = "MaterialID"
        Me.MaterialID.ReadOnly = True
        Me.MaterialID.Width = 95
        '
        'Category
        '
        Me.Category.DataPropertyName = "Category"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Category.DefaultCellStyle = DataGridViewCellStyle3
        Me.Category.HeaderText = "Category"
        Me.Category.Name = "Category"
        Me.Category.ReadOnly = True
        Me.Category.Width = 90
        '
        'MaterialName
        '
        Me.MaterialName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MaterialName.DataPropertyName = "MaterialName"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.MediumBlue
        Me.MaterialName.DefaultCellStyle = DataGridViewCellStyle4
        Me.MaterialName.FillWeight = 250.0!
        Me.MaterialName.HeaderText = "Material Name"
        Me.MaterialName.Name = "MaterialName"
        '
        'BBL
        '
        Me.BBL.DataPropertyName = "BBL"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle5.Format = "N0"
        Me.BBL.DefaultCellStyle = DataGridViewCellStyle5
        Me.BBL.HeaderText = "Barrels Input"
        Me.BBL.Name = "BBL"
        Me.BBL.Width = 115
        '
        'PriceLocal
        '
        Me.PriceLocal.DataPropertyName = "PriceLocal"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle6.Format = "N2"
        Me.PriceLocal.DefaultCellStyle = DataGridViewCellStyle6
        Me.PriceLocal.HeaderText = "USD/bbl"
        Me.PriceLocal.Name = "PriceLocal"
        Me.PriceLocal.Width = 90
        '
        'SortKey
        '
        Me.SortKey.DataPropertyName = "SortKey"
        Me.SortKey.HeaderText = "Sort Key"
        Me.SortKey.Name = "SortKey"
        Me.SortKey.ReadOnly = True
        Me.SortKey.Visible = False
        '
        'gbFooterRaw
        '
        Me.gbFooterRaw.Controls.Add(Me.Label2)
        Me.gbFooterRaw.Controls.Add(Me.Label3)
        Me.gbFooterRaw.Controls.Add(Me.lblRMCAvg1)
        Me.gbFooterRaw.Controls.Add(Me.Label17)
        Me.gbFooterRaw.Controls.Add(Me.txtRMTotBBL)
        Me.gbFooterRaw.Controls.Add(Me.txtRMAvgCost)
        Me.gbFooterRaw.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbFooterRaw.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbFooterRaw.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbFooterRaw.Location = New System.Drawing.Point(6, 341)
        Me.gbFooterRaw.Name = "gbFooterRaw"
        Me.gbFooterRaw.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbFooterRaw.Size = New System.Drawing.Size(722, 78)
        Me.gbFooterRaw.TabIndex = 964
        Me.gbFooterRaw.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(6, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 945
        Me.Label2.Text = "Helpful hint:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(6, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(154, 39)
        Me.Label3.TabIndex = 944
        Me.Label3.Text = "To delete a row, simply select" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "the entire row and click delete " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "on your keyboar" & _
            "d."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRMCAvg1
        '
        Me.lblRMCAvg1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRMCAvg1.BackColor = System.Drawing.Color.Transparent
        Me.lblRMCAvg1.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.lblRMCAvg1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblRMCAvg1.Location = New System.Drawing.Point(456, 47)
        Me.lblRMCAvg1.Name = "lblRMCAvg1"
        Me.lblRMCAvg1.Size = New System.Drawing.Size(165, 23)
        Me.lblRMCAvg1.TabIndex = 511
        Me.lblRMCAvg1.Text = "Average Cost, local currency/bbl"
        Me.lblRMCAvg1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label17.Location = New System.Drawing.Point(456, 19)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(165, 23)
        Me.Label17.TabIndex = 510
        Me.Label17.Text = "Total Other Raw Materials, bbl"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRMTotBBL
        '
        Me.txtRMTotBBL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRMTotBBL.BackColor = System.Drawing.SystemColors.Control
        Me.txtRMTotBBL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRMTotBBL.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtRMTotBBL.Location = New System.Drawing.Point(627, 21)
        Me.txtRMTotBBL.Name = "txtRMTotBBL"
        Me.txtRMTotBBL.ReadOnly = True
        Me.txtRMTotBBL.Size = New System.Drawing.Size(88, 21)
        Me.txtRMTotBBL.TabIndex = 2
        Me.txtRMTotBBL.TabStop = False
        Me.txtRMTotBBL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRMAvgCost
        '
        Me.txtRMAvgCost.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRMAvgCost.BackColor = System.Drawing.SystemColors.Control
        Me.txtRMAvgCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRMAvgCost.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtRMAvgCost.Location = New System.Drawing.Point(627, 48)
        Me.txtRMAvgCost.Name = "txtRMAvgCost"
        Me.txtRMAvgCost.ReadOnly = True
        Me.txtRMAvgCost.Size = New System.Drawing.Size(88, 21)
        Me.txtRMAvgCost.TabIndex = 4
        Me.txtRMAvgCost.TabStop = False
        Me.txtRMAvgCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRawMaterialInputs
        '
        Me.lblRawMaterialInputs.BackColor = System.Drawing.Color.Transparent
        Me.lblRawMaterialInputs.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblRawMaterialInputs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRawMaterialInputs.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblRawMaterialInputs.Location = New System.Drawing.Point(6, 0)
        Me.lblRawMaterialInputs.Name = "lblRawMaterialInputs"
        Me.lblRawMaterialInputs.Size = New System.Drawing.Size(722, 36)
        Me.lblRawMaterialInputs.TabIndex = 963
        Me.lblRawMaterialInputs.Text = "Other Raw Materials:   DATA"
        Me.lblRawMaterialInputs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tpProd
        '
        Me.tpProd.BackColor = System.Drawing.Color.Transparent
        Me.tpProd.Controls.Add(Me.dgvProd)
        Me.tpProd.Controls.Add(Me.dgProd_MAP)
        Me.tpProd.Controls.Add(Me.gbFooterYield)
        Me.tpProd.Controls.Add(Me.lblProdYields)
        Me.tpProd.Location = New System.Drawing.Point(4, 22)
        Me.tpProd.Name = "tpProd"
        Me.tpProd.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.tpProd.Size = New System.Drawing.Size(734, 425)
        Me.tpProd.TabIndex = 1
        Me.tpProd.Text = "Table 15 - Product Yield"
        Me.tpProd.UseVisualStyleBackColor = True
        Me.tpProd.Visible = False
        '
        'dgProd_MAP
        '
        Me.dgProd_MAP.AllowUserToAddRows = False
        Me.dgProd_MAP.AllowUserToDeleteRows = False
        Me.dgProd_MAP.AllowUserToResizeColumns = False
        Me.dgProd_MAP.AllowUserToResizeRows = False
        Me.dgProd_MAP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgProd_MAP.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.dgProd_MAP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgProd_MAP.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgProd_MAP.DefaultCellStyle = DataGridViewCellStyle30
        Me.dgProd_MAP.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgProd_MAP.Location = New System.Drawing.Point(6, 36)
        Me.dgProd_MAP.MultiSelect = False
        Me.dgProd_MAP.Name = "dgProd_MAP"
        Me.dgProd_MAP.RowHeadersVisible = False
        Me.dgProd_MAP.RowHeadersWidth = 25
        Me.dgProd_MAP.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgProd_MAP.Size = New System.Drawing.Size(722, 305)
        Me.dgProd_MAP.TabIndex = 963
        Me.dgProd_MAP.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "lkStartRow"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle23
        Me.DataGridViewTextBoxColumn2.HeaderText = "Starting Row (integer)"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "lkEndRow"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle24
        Me.DataGridViewTextBoxColumn3.HeaderText = "Ending Row (integer)"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "lkCategory"
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle25
        Me.DataGridViewTextBoxColumn4.HeaderText = "Category Column (letter)"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "lkMaterialID"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle26
        Me.DataGridViewTextBoxColumn5.HeaderText = "Material ID Column (letter)"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "lkMaterialName"
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle27
        Me.DataGridViewTextBoxColumn6.HeaderText = "Material Name Column (letter)"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "lkBBL"
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle28
        Me.DataGridViewTextBoxColumn7.HeaderText = "Barrels Produced Column (letter)"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "lkPriceLocal"
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle29
        Me.DataGridViewTextBoxColumn8.HeaderText = "Price Column (letter)"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgvProd
        '
        Me.dgvProd.AllowUserToAddRows = False
        Me.dgvProd.AllowUserToResizeColumns = False
        Me.dgvProd.AllowUserToResizeRows = False
        Me.dgvProd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProd.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvProd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MaterialIDProd, Me.CategoryProd, Me.MaterialNameProd, Me.BBLProd, Me.PriceLocalProd, Me.DataGridViewTextBoxColumn1})
        Me.dgvProd.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProd.Location = New System.Drawing.Point(6, 36)
        Me.dgvProd.MultiSelect = False
        Me.dgvProd.Name = "dgvProd"
        Me.dgvProd.RowHeadersWidth = 25
        Me.dgvProd.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvProd.Size = New System.Drawing.Size(722, 305)
        Me.dgvProd.TabIndex = 945
        '
        'MaterialIDProd
        '
        Me.MaterialIDProd.DataPropertyName = "MaterialID"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MaterialIDProd.DefaultCellStyle = DataGridViewCellStyle17
        Me.MaterialIDProd.Frozen = True
        Me.MaterialIDProd.HeaderText = "Material ID"
        Me.MaterialIDProd.Name = "MaterialIDProd"
        Me.MaterialIDProd.ReadOnly = True
        Me.MaterialIDProd.Width = 95
        '
        'CategoryProd
        '
        Me.CategoryProd.DataPropertyName = "Category"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CategoryProd.DefaultCellStyle = DataGridViewCellStyle18
        Me.CategoryProd.HeaderText = "Category"
        Me.CategoryProd.Name = "CategoryProd"
        Me.CategoryProd.ReadOnly = True
        Me.CategoryProd.Width = 90
        '
        'MaterialNameProd
        '
        Me.MaterialNameProd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MaterialNameProd.DataPropertyName = "MaterialName"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.MediumBlue
        Me.MaterialNameProd.DefaultCellStyle = DataGridViewCellStyle19
        Me.MaterialNameProd.FillWeight = 250.0!
        Me.MaterialNameProd.HeaderText = "Material Name"
        Me.MaterialNameProd.Name = "MaterialNameProd"
        '
        'BBLProd
        '
        Me.BBLProd.DataPropertyName = "BBL"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle20.Format = "N0"
        Me.BBLProd.DefaultCellStyle = DataGridViewCellStyle20
        Me.BBLProd.FillWeight = 120.0!
        Me.BBLProd.HeaderText = "Barrels Produced"
        Me.BBLProd.Name = "BBLProd"
        Me.BBLProd.Width = 115
        '
        'PriceLocalProd
        '
        Me.PriceLocalProd.DataPropertyName = "PriceLocal"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.LemonChiffon
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.MediumBlue
        DataGridViewCellStyle21.Format = "N2"
        Me.PriceLocalProd.DefaultCellStyle = DataGridViewCellStyle21
        Me.PriceLocalProd.HeaderText = "USD/bbl"
        Me.PriceLocalProd.Name = "PriceLocalProd"
        Me.PriceLocalProd.Width = 90
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "SortKey"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Sort Key"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'gbFooterYield
        '
        Me.gbFooterYield.Controls.Add(Me.Label11)
        Me.gbFooterYield.Controls.Add(Me.Label10)
        Me.gbFooterYield.Controls.Add(Me.lblRMCAvg)
        Me.gbFooterYield.Controls.Add(Me.Label13)
        Me.gbFooterYield.Controls.Add(Me.txtProdTotBBL)
        Me.gbFooterYield.Controls.Add(Me.txtProdAvgCost)
        Me.gbFooterYield.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbFooterYield.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbFooterYield.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbFooterYield.Location = New System.Drawing.Point(6, 341)
        Me.gbFooterYield.Name = "gbFooterYield"
        Me.gbFooterYield.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.gbFooterYield.Size = New System.Drawing.Size(722, 78)
        Me.gbFooterYield.TabIndex = 957
        Me.gbFooterYield.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(6, 12)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 13)
        Me.Label11.TabIndex = 945
        Me.Label11.Text = "Helpful hint:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(6, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(154, 39)
        Me.Label10.TabIndex = 944
        Me.Label10.Text = "To delete a row, simply select" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "the entire row and click delete " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "on your keyboar" & _
            "d."
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRMCAvg
        '
        Me.lblRMCAvg.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRMCAvg.BackColor = System.Drawing.Color.Transparent
        Me.lblRMCAvg.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.lblRMCAvg.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblRMCAvg.Location = New System.Drawing.Point(456, 47)
        Me.lblRMCAvg.Name = "lblRMCAvg"
        Me.lblRMCAvg.Size = New System.Drawing.Size(165, 23)
        Me.lblRMCAvg.TabIndex = 511
        Me.lblRMCAvg.Text = "Average Cost, local currency/bbl"
        Me.lblRMCAvg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World)
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(456, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(165, 23)
        Me.Label13.TabIndex = 510
        Me.Label13.Text = "Total Product Yield, bbl"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtProdTotBBL
        '
        Me.txtProdTotBBL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProdTotBBL.BackColor = System.Drawing.SystemColors.Control
        Me.txtProdTotBBL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProdTotBBL.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtProdTotBBL.Location = New System.Drawing.Point(627, 21)
        Me.txtProdTotBBL.Name = "txtProdTotBBL"
        Me.txtProdTotBBL.ReadOnly = True
        Me.txtProdTotBBL.Size = New System.Drawing.Size(88, 21)
        Me.txtProdTotBBL.TabIndex = 3
        Me.txtProdTotBBL.TabStop = False
        Me.txtProdTotBBL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtProdAvgCost
        '
        Me.txtProdAvgCost.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProdAvgCost.BackColor = System.Drawing.SystemColors.Control
        Me.txtProdAvgCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProdAvgCost.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.txtProdAvgCost.Location = New System.Drawing.Point(627, 48)
        Me.txtProdAvgCost.Name = "txtProdAvgCost"
        Me.txtProdAvgCost.ReadOnly = True
        Me.txtProdAvgCost.Size = New System.Drawing.Size(88, 21)
        Me.txtProdAvgCost.TabIndex = 5
        Me.txtProdAvgCost.TabStop = False
        Me.txtProdAvgCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblProdYields
        '
        Me.lblProdYields.BackColor = System.Drawing.Color.Transparent
        Me.lblProdYields.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblProdYields.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProdYields.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblProdYields.Location = New System.Drawing.Point(6, 0)
        Me.lblProdYields.Name = "lblProdYields"
        Me.lblProdYields.Size = New System.Drawing.Size(722, 36)
        Me.lblProdYields.TabIndex = 962
        Me.lblProdYields.Text = "Product Yield:   DATA"
        Me.lblProdYields.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTools
        '
        Me.pnlTools.Controls.Add(Me.gbTools)
        Me.pnlTools.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTools.Location = New System.Drawing.Point(0, 0)
        Me.pnlTools.Name = "pnlTools"
        Me.pnlTools.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.pnlTools.Size = New System.Drawing.Size(742, 105)
        Me.pnlTools.TabIndex = 945
        '
        'gbTools
        '
        Me.gbTools.BackColor = System.Drawing.SystemColors.Control
        Me.gbTools.Controls.Add(Me.chkComplete)
        Me.gbTools.Controls.Add(Me.btnBrowse)
        Me.gbTools.Controls.Add(Me.lblView)
        Me.gbTools.Controls.Add(Me.btnEdit)
        Me.gbTools.Controls.Add(Me.rbLinks)
        Me.gbTools.Controls.Add(Me.rbData)
        Me.gbTools.Controls.Add(Me.btnAdd)
        Me.gbTools.Controls.Add(Me.Label8)
        Me.gbTools.Controls.Add(Me.cbSheets1)
        Me.gbTools.Controls.Add(Me.Label4)
        Me.gbTools.Controls.Add(Me.cbSheets)
        Me.gbTools.Controls.Add(Me.tbFilePath)
        Me.gbTools.Controls.Add(Me.Label6)
        Me.gbTools.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTools.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World)
        Me.gbTools.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbTools.Location = New System.Drawing.Point(0, 0)
        Me.gbTools.Name = "gbTools"
        Me.gbTools.Padding = New System.Windows.Forms.Padding(0, 4, 0, 8)
        Me.gbTools.Size = New System.Drawing.Size(742, 99)
        Me.gbTools.TabIndex = 1
        Me.gbTools.TabStop = False
        '
        'chkComplete
        '
        Me.chkComplete.AutoSize = True
        Me.chkComplete.BackColor = System.Drawing.Color.Transparent
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.chkComplete.Location = New System.Drawing.Point(9, 49)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(136, 17)
        Me.chkComplete.TabIndex = 963
        Me.chkComplete.Text = "Check when completed"
        Me.chkComplete.UseVisualStyleBackColor = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Image = CType(resources.GetObject("btnBrowse.Image"), System.Drawing.Image)
        Me.btnBrowse.Location = New System.Drawing.Point(711, 15)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(21, 21)
        Me.btnBrowse.TabIndex = 962
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Browse for bridge workbook")
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'lblView
        '
        Me.lblView.AutoSize = True
        Me.lblView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblView.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblView.Location = New System.Drawing.Point(6, 72)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(33, 13)
        Me.lblView.TabIndex = 949
        Me.lblView.Text = "View"
        Me.lblView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.Image = Global.Solomon_Profile.My.Resources.Resources.log
        Me.btnEdit.Location = New System.Drawing.Point(76, 15)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(64, 28)
        Me.btnEdit.TabIndex = 952
        Me.btnEdit.TabStop = False
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnEdit, "Change the current Material ID")
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'rbLinks
        '
        Me.rbLinks.AutoSize = True
        Me.rbLinks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLinks.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbLinks.Location = New System.Drawing.Point(92, 70)
        Me.rbLinks.Name = "rbLinks"
        Me.rbLinks.Size = New System.Drawing.Size(53, 17)
        Me.rbLinks.TabIndex = 948
        Me.rbLinks.Text = "LINKS"
        Me.rbLinks.UseVisualStyleBackColor = True
        '
        'rbData
        '
        Me.rbData.AutoSize = True
        Me.rbData.Checked = True
        Me.rbData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbData.Location = New System.Drawing.Point(41, 70)
        Me.rbData.Name = "rbData"
        Me.rbData.Size = New System.Drawing.Size(52, 17)
        Me.rbData.TabIndex = 947
        Me.rbData.TabStop = True
        Me.rbData.Text = "DATA"
        Me.rbData.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.Image = Global.Solomon_Profile.My.Resources.Resources.add
        Me.btnAdd.Location = New System.Drawing.Point(6, 15)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(64, 28)
        Me.btnAdd.TabIndex = 953
        Me.btnAdd.TabStop = False
        Me.btnAdd.Text = "Add"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnAdd, "Create a new material entry")
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(312, 72)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(146, 13)
        Me.Label8.TabIndex = 951
        Me.Label8.Text = "Product Yield Worksheet"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbSheets1
        '
        Me.cbSheets1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets1.FormattingEnabled = True
        Me.cbSheets1.Location = New System.Drawing.Point(464, 69)
        Me.cbSheets1.Name = "cbSheets1"
        Me.cbSheets1.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets1.TabIndex = 950
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(389, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 945
        Me.Label4.Text = "Workbook"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbSheets
        '
        Me.cbSheets.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSheets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSheets.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.cbSheets.FormattingEnabled = True
        Me.cbSheets.Location = New System.Drawing.Point(464, 42)
        Me.cbSheets.Name = "cbSheets"
        Me.cbSheets.Size = New System.Drawing.Size(241, 21)
        Me.cbSheets.TabIndex = 8
        '
        'tbFilePath
        '
        Me.tbFilePath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFilePath.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbFilePath.Location = New System.Drawing.Point(464, 15)
        Me.tbFilePath.Name = "tbFilePath"
        Me.tbFilePath.ReadOnly = True
        Me.tbFilePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbFilePath.Size = New System.Drawing.Size(241, 21)
        Me.tbFilePath.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(277, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(181, 13)
        Me.Label6.TabIndex = 946
        Me.Label6.Text = "Other Raw Material Worksheet"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLU
        '
        Me.pnlLU.Controls.Add(Me.TabControl1)
        Me.pnlLU.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLU.Location = New System.Drawing.Point(4, 40)
        Me.pnlLU.Name = "pnlLU"
        Me.pnlLU.Padding = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.pnlLU.Size = New System.Drawing.Size(742, 556)
        Me.pnlLU.TabIndex = 947
        Me.pnlLU.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 6)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(742, 550)
        Me.TabControl1.TabIndex = 14
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnOK)
        Me.TabPage1.Controls.Add(Me.btnCancel)
        Me.TabPage1.Controls.Add(Me.dgvLU)
        Me.TabPage1.Controls.Add(Me.cmbCategory)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.TabPage1.Size = New System.Drawing.Size(734, 524)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Select a Material ID"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.Image = Global.Solomon_Profile.My.Resources.Resources.check
        Me.btnOK.Location = New System.Drawing.Point(594, 4)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(64, 28)
        Me.btnOK.TabIndex = 8
        Me.btnOK.Text = "OK"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.Image = Global.Solomon_Profile.My.Resources.Resources.error_1
        Me.btnCancel.Location = New System.Drawing.Point(664, 4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(64, 28)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'dgvLU
        '
        Me.dgvLU.AllowUserToAddRows = False
        Me.dgvLU.AllowUserToDeleteRows = False
        Me.dgvLU.AllowUserToResizeColumns = False
        Me.dgvLU.AllowUserToResizeRows = False
        Me.dgvLU.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLU.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle31
        Me.dgvLU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLU.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MatlID, Me.SAIName, Me.SortKey_LU, Me.BPSortKey, Me.AllowInRMI, Me.AllowInOTHRM, Me.AllowInRLube, Me.AllowInRChem, Me.AllowInProd, Me.AllowInFLube, Me.AllowInFChem, Me.AllowInASP, Me.AllowInCoke, Me.AllowInSolv, Me.AllowInMProd, Me.LubesOnly})
        Me.dgvLU.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLU.Location = New System.Drawing.Point(6, 36)
        Me.dgvLU.MultiSelect = False
        Me.dgvLU.Name = "dgvLU"
        Me.dgvLU.RowHeadersWidth = 25
        Me.dgvLU.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvLU.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dgvLU.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLU.Size = New System.Drawing.Size(722, 482)
        Me.dgvLU.TabIndex = 6
        '
        'MatlID
        '
        Me.MatlID.DataPropertyName = "MaterialID"
        Me.MatlID.HeaderText = "Material ID"
        Me.MatlID.Name = "MatlID"
        Me.MatlID.ReadOnly = True
        '
        'SAIName
        '
        Me.SAIName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SAIName.DataPropertyName = "SAIName"
        Me.SAIName.FillWeight = 250.0!
        Me.SAIName.HeaderText = "Solomon Name"
        Me.SAIName.Name = "SAIName"
        Me.SAIName.ReadOnly = True
        '
        'SortKey_LU
        '
        Me.SortKey_LU.DataPropertyName = "SortKey"
        Me.SortKey_LU.HeaderText = "Sort Key"
        Me.SortKey_LU.Name = "SortKey_LU"
        Me.SortKey_LU.ReadOnly = True
        '
        'BPSortKey
        '
        Me.BPSortKey.DataPropertyName = "BPSortKey"
        Me.BPSortKey.HeaderText = "Boiling Point Order"
        Me.BPSortKey.Name = "BPSortKey"
        Me.BPSortKey.ReadOnly = True
        Me.BPSortKey.Visible = False
        '
        'AllowInRMI
        '
        Me.AllowInRMI.DataPropertyName = "AllowInRMI"
        Me.AllowInRMI.HeaderText = "AllowInRMI"
        Me.AllowInRMI.Name = "AllowInRMI"
        Me.AllowInRMI.ReadOnly = True
        Me.AllowInRMI.Visible = False
        '
        'AllowInOTHRM
        '
        Me.AllowInOTHRM.DataPropertyName = "AllowInOTHRM"
        Me.AllowInOTHRM.HeaderText = "AllowInOTHRM"
        Me.AllowInOTHRM.Name = "AllowInOTHRM"
        Me.AllowInOTHRM.ReadOnly = True
        Me.AllowInOTHRM.Visible = False
        '
        'AllowInRLube
        '
        Me.AllowInRLube.DataPropertyName = "AllowInRLube"
        Me.AllowInRLube.HeaderText = "AllowInRLube"
        Me.AllowInRLube.Name = "AllowInRLube"
        Me.AllowInRLube.ReadOnly = True
        Me.AllowInRLube.Visible = False
        '
        'AllowInRChem
        '
        Me.AllowInRChem.DataPropertyName = "AllowInRChem"
        Me.AllowInRChem.HeaderText = "AllowInRChem"
        Me.AllowInRChem.Name = "AllowInRChem"
        Me.AllowInRChem.ReadOnly = True
        Me.AllowInRChem.Visible = False
        '
        'AllowInProd
        '
        Me.AllowInProd.DataPropertyName = "AllowInProd"
        Me.AllowInProd.HeaderText = "AllowInProd"
        Me.AllowInProd.Name = "AllowInProd"
        Me.AllowInProd.ReadOnly = True
        Me.AllowInProd.Visible = False
        '
        'AllowInFLube
        '
        Me.AllowInFLube.DataPropertyName = "AllowInFLube"
        Me.AllowInFLube.HeaderText = "AllowInFLube"
        Me.AllowInFLube.Name = "AllowInFLube"
        Me.AllowInFLube.ReadOnly = True
        Me.AllowInFLube.Visible = False
        '
        'AllowInFChem
        '
        Me.AllowInFChem.DataPropertyName = "AllowInFChem"
        Me.AllowInFChem.HeaderText = "AllowInFChem"
        Me.AllowInFChem.Name = "AllowInFChem"
        Me.AllowInFChem.ReadOnly = True
        Me.AllowInFChem.Visible = False
        '
        'AllowInASP
        '
        Me.AllowInASP.DataPropertyName = "AllowInASP"
        Me.AllowInASP.HeaderText = "AllowInASP"
        Me.AllowInASP.Name = "AllowInASP"
        Me.AllowInASP.ReadOnly = True
        Me.AllowInASP.Visible = False
        '
        'AllowInCoke
        '
        Me.AllowInCoke.DataPropertyName = "AllowInCoke"
        Me.AllowInCoke.HeaderText = "AllowInCoke"
        Me.AllowInCoke.Name = "AllowInCoke"
        Me.AllowInCoke.ReadOnly = True
        Me.AllowInCoke.Visible = False
        '
        'AllowInSolv
        '
        Me.AllowInSolv.DataPropertyName = "AllowInSolv"
        Me.AllowInSolv.HeaderText = "AllowInSolv"
        Me.AllowInSolv.Name = "AllowInSolv"
        Me.AllowInSolv.ReadOnly = True
        Me.AllowInSolv.Visible = False
        '
        'AllowInMProd
        '
        Me.AllowInMProd.DataPropertyName = "AllowInMProd"
        Me.AllowInMProd.HeaderText = "AllowInMProd"
        Me.AllowInMProd.Name = "AllowInMProd"
        Me.AllowInMProd.ReadOnly = True
        Me.AllowInMProd.Visible = False
        '
        'LubesOnly
        '
        Me.LubesOnly.DataPropertyName = "LubesOnly"
        Me.LubesOnly.HeaderText = "LubesOnly"
        Me.LubesOnly.Name = "LubesOnly"
        Me.LubesOnly.ReadOnly = True
        Me.LubesOnly.Visible = False
        '
        'cmbCategory
        '
        Me.cmbCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCategory.FormattingEnabled = True
        Me.cmbCategory.Location = New System.Drawing.Point(165, 9)
        Me.cmbCategory.Name = "cmbCategory"
        Me.cmbCategory.Size = New System.Drawing.Size(250, 21)
        Me.cmbCategory.TabIndex = 0
        '
        'Label12
        '
        Me.Label12.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(6, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(722, 36)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Filter by material category"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.SystemColors.Window
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(340, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.TabStop = False
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save everything in the forms below")
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPreview
        '
        Me.btnPreview.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.SystemColors.Window
        Me.btnPreview.Image = Global.Solomon_Profile.My.Resources.Resources.PrintPreview
        Me.btnPreview.Location = New System.Drawing.Point(420, 0)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(80, 34)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.TabStop = False
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print preview")
        Me.btnPreview.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnImport.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.SystemColors.Window
        Me.btnImport.Image = Global.Solomon_Profile.My.Resources.Resources.excel16
        Me.btnImport.Location = New System.Drawing.Point(500, 0)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 34)
        Me.btnImport.TabIndex = 2
        Me.btnImport.TabStop = False
        Me.btnImport.Text = "Import"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnImport, "Update input form with linked values")
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClear.FlatAppearance.BorderSize = 0
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(580, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(80, 34)
        Me.btnClear.TabIndex = 5
        Me.btnClear.TabStop = False
        Me.btnClear.Text = "Clear"
        Me.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clears data or links below")
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.SystemColors.Window
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(660, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 34)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close the form")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Highlight
        Me.pnlHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlHeader.Controls.Add(Me.btnSave)
        Me.pnlHeader.Controls.Add(Me.btnPreview)
        Me.pnlHeader.Controls.Add(Me.lblHeader)
        Me.pnlHeader.Controls.Add(Me.btnImport)
        Me.pnlHeader.Controls.Add(Me.btnClear)
        Me.pnlHeader.Controls.Add(Me.btnClose)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.pnlHeader.Location = New System.Drawing.Point(4, 4)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(742, 36)
        Me.pnlHeader.TabIndex = 1034
        '
        'lblHeader
        '
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.ForeColor = System.Drawing.SystemColors.Window
        Me.lblHeader.Location = New System.Drawing.Point(0, 0)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.lblHeader.Size = New System.Drawing.Size(500, 34)
        Me.lblHeader.TabIndex = 0
        Me.lblHeader.Text = "Material Balance"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SA_MaterialBalance
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.pnlData)
        Me.Controls.Add(Me.pnlLU)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.MediumBlue
        Me.Name = "SA_MaterialBalance"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.Size = New System.Drawing.Size(750, 600)
        Me.pnlData.ResumeLayout(False)
        Me.tabData.ResumeLayout(False)
        Me.tpRM.ResumeLayout(False)
        CType(Me.dgRM_MAP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFooterRaw.ResumeLayout(False)
        Me.gbFooterRaw.PerformLayout()
        Me.tpProd.ResumeLayout(False)
        CType(Me.dgProd_MAP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvProd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFooterYield.ResumeLayout(False)
        Me.gbFooterYield.PerformLayout()
        Me.pnlTools.ResumeLayout(False)
        Me.gbTools.ResumeLayout(False)
        Me.gbTools.PerformLayout()
        Me.pnlLU.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dgvLU, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private dv_LU As New DataView
    Private IsLoaded, HasRights As Boolean
    Friend MaterialNotSaved As Boolean
    Private MyParent As Main
    Private SelectedCategory As String
    Private IsEditing As Boolean
    Private dvWB As New DataView

    Private Sub SetUpCategories()
        cmbCategory.Items.Clear()
        If tabData.SelectedTab Is tpRM Then
            cmbCategory.Items.Add("Other Raw Materials")
            cmbCategory.Items.Add("Returns from Paraffinic Lubes Refinery")
            cmbCategory.Items.Add("Returns from Chemical Plant")
        Else
            cmbCategory.Items.Add("Product Yield")
            cmbCategory.Items.Add("Asphalt")
            cmbCategory.Items.Add("FeedStocks to Chemical Plant")
            cmbCategory.Items.Add("Specialty Solvents")
            cmbCategory.Items.Add("Feedstocks to Paraffinic Lubes Refinery")
            cmbCategory.Items.Add("Saleable Petroleum Coke (FOE)")
            cmbCategory.Items.Add("Miscellaneous Products")
        End If
        cmbCategory.SelectedIndex = 0
    End Sub

    Friend Sub SetDataSet()
        MyParent = CType(Me.ParentForm, Main)
        HasRights = MyParent.MyParent.MatlRights
        dgvRM.DataSource = MyParent.dsYield.Tables("Yield_RM")
        dgvProd.DataSource = MyParent.dsYield.Tables("Yield_Prod")
        dv_LU.Table = MyParent.MyParent.ds_Ref.Tables("Material_LU")
        dgvLU.DataSource = dv_LU
        dv_LU.Sort = "SortKey ASC"

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Material Balance'")
        chkComplete.Checked = CBool(row(0)!Checked)

        dgRM_MAP.DataSource = MyParent.dsMappings.Tables("Yield_RM")
        dgProd_MAP.DataSource = MyParent.dsMappings.Tables("Yield_Prod")

        'tbFilePath.Text = My.Settings.workbookMaterial
        'cbSheets.Text = My.Settings.worksheetMaterial1
        'cbSheets1.Text = My.Settings.worksheetMaterial2

        Dim dt As DataTable = MyParent.dsMappings.Tables("wkbReference")
        Dim dr() As DataRow = dt.Select("frmName = 'Material'")

        If (dr.GetUpperBound(0) = -1) Then
        Else
            tbFilePath.Text = dr(0).Item(tbFilePath.Name).ToString
            cbSheets.Text = dr(0).Item(cbSheets.Name).ToString
            cbSheets1.Text = dr(0).Item(cbSheets1.Name).ToString
        End If

        PopulateSummary()
        IsLoaded = True
    End Sub

    Friend Sub SetCurrencyAndUOM(ByVal Currency As String)
        lblRMCAvg.Text = "Average Cost, " & Currency & "/bbl"
        lblRMCAvg1.Text = "Average Cost, " & Currency & "/bbl"
        dgvRM.Columns("PriceLocal").HeaderText = Currency & "/bbl"
        dgvProd.Columns("PriceLocalProd").HeaderText = Currency & "/bbl"
    End Sub

    Friend Sub PopulateSummary()
        Dim BBL, Cost As Double
        Dim row As DataRow

        'Handle Raw Materials First
        For Each row In MyParent.dsYield.Tables("Yield_RM").Rows
            Try
                BBL = BBL + CDbl(row.Item("BBL"))
            Catch ex As Exception
                BBL = BBL
            End Try

            Try
                Cost = Cost + (CDbl(row.Item("BBL")) * CDbl(row.Item("PriceLocal")))
            Catch ex As Exception
                Cost = Cost
            End Try
        Next

        txtRMTotBBL.Text = FormatDoubles(BBL, 0)
        If BBL > 0 Then
            txtRMAvgCost.Text = FormatDoubles(Cost / BBL, 2)
        Else
            txtRMAvgCost.Text = FormatDoubles(0, 2)
        End If

        BBL = 0
        Cost = 0

        'Handle Product Yields
        For Each row In MyParent.dsYield.Tables("Yield_Prod").Rows
            Try
                BBL = BBL + CDbl(row.Item("BBL"))
            Catch ex As Exception
                BBL = BBL
            End Try

            Try
                Cost = Cost + (CDbl(row.Item("BBL")) * CDbl(row.Item("PriceLocal")))
            Catch ex As Exception
                Cost = Cost
            End Try
        Next

        txtProdTotBBL.Text = FormatDoubles(BBL, 0)
        If BBL > 0 Then
            txtProdAvgCost.Text = FormatDoubles(Cost / BBL, 2)
        Else
            txtProdAvgCost.Text = FormatDoubles(0, 2)
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim result As DialogResult

        Select Case Me.tabData.SelectedTab.Name.ToString()
            Case "tpRM"
                If rbLinks.Checked Then
                    result = ProfileMsgBox("YN", "CLEAR", "raw material mappings")
                    If result = DialogResult.Yes Then
                        For Each col As DataColumn In MyParent.dsMappings.Tables("Yield_RM").Columns
                            MyParent.dsMappings.Tables("Yield_RM").Rows(0).Item(col) = Nothing
                        Next
                    End If
                Else
                    result = ProfileMsgBox("YN", "CLEAR", "raw material table")
                    If result = DialogResult.Yes Then
                        'For Each row As DataRow In MyParent.dsYield.Tables("Yield_RM").Rows
                        '    row.Delete()
                        'Next row
                        For Each row As DataGridViewRow In Me.dgvRM.Rows
                            row.Cells(0).Value = DBNull.Value
                            row.Cells(1).Value = DBNull.Value
                            row.Cells(2).Value = DBNull.Value
                            row.Cells(3).Value = DBNull.Value
                            row.Cells(4).Value = DBNull.Value
                            row.Cells(5).Value = DBNull.Value
                        Next row
                    End If
                End If

            Case "tpProd"
                If rbLinks.Checked Then
                    result = ProfileMsgBox("YN", "CLEAR", "product yield mappings")
                    If result = DialogResult.Yes Then
                        For Each col As DataColumn In MyParent.dsMappings.Tables("Yield_Prod").Columns
                            MyParent.dsMappings.Tables("Yield_Prod").Rows(0).Item(col) = Nothing
                        Next
                    End If
                Else
                    result = ProfileMsgBox("YN", "CLEAR", "product yield table")
                    If result = DialogResult.Yes Then
                        'For Each row As DataRow In MyParent.dsYield.Tables("Yield_Prod").Rows
                        '    row.Delete()
                        'Next row
                        For Each row As DataGridViewRow In Me.dgvProd.Rows
                            row.Cells(0).Value = DBNull.Value
                            row.Cells(1).Value = DBNull.Value
                            row.Cells(2).Value = DBNull.Value
                            row.Cells(3).Value = DBNull.Value
                            row.Cells(4).Value = DBNull.Value
                            row.Cells(5).Value = DBNull.Value                         
                        Next row
                    End If
                End If

        End Select

        If result = DialogResult.Yes Then
            ChangesMade(btnSave, chkComplete, MaterialNotSaved)
            PopulateSummary()
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
    End Sub

    Private Sub cmbCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCategory.SelectedIndexChanged
        Dim tmpstr As String = ""
        Select Case cmbCategory.Text
            Case "Other Raw Materials"
                tmpstr = "AllowInOTHRM = TRUE"
                SelectedCategory = "OTHRM"
            Case "Returns from Paraffinic Lubes Refinery"
                tmpstr = "AllowInRLube = TRUE"
                SelectedCategory = "RLUBE"
            Case "Returns from Chemical Plant"
                tmpstr = "AllowInRChem = TRUE"
                SelectedCategory = "RCHEM"
            Case "Product Yield"
                tmpstr = "AllowInProd = TRUE"
                SelectedCategory = "PROD"
            Case "Asphalt"
                tmpstr = "AllowInASP = TRUE"
                SelectedCategory = "ASP"
            Case "FeedStocks to Chemical Plant"
                tmpstr = "AllowInFChem = TRUE"
                SelectedCategory = "FCHEM"
            Case "Specialty Solvents"
                tmpstr = "AllowInSolv = TRUE"
                SelectedCategory = "SOLV"
            Case "Feedstocks to Paraffinic Lubes Refinery"
                tmpstr = "AllowInFLube = TRUE"
                SelectedCategory = "FLUBE"
            Case "Saleable Petroleum Coke (FOE)"
                tmpstr = "AllowInCoke = TRUE"
                SelectedCategory = "COKE"
            Case "Miscellaneous Products"
                tmpstr = "AllowInMProd = TRUE"
                SelectedCategory = "MPROD"
        End Select
        dv_LU.RowFilter = ""
        dv_LU.RowFilter = tmpstr
    End Sub

    Private Sub ClickOK()
        On Error GoTo NothingSelected
        Dim result As DialogResult = ProfileMsgBox("YN", "MATLDEFAULTS", "")

        Dim dt As DataTable
        If tabData.SelectedTab Is tpRM Then
            dt = MyParent.dsYield.Tables("Yield_RM")
        Else
            dt = MyParent.dsYield.Tables("Yield_Prod")
        End If

        Dim MaterialID, MaterialName As String
        MaterialID = Trim(dgvLU.CurrentRow.Cells("MatlID").Value.ToString)
        MaterialName = Trim(dgvLU.CurrentRow.Cells("SAIName").Value.ToString)

        Select Case result
            Case DialogResult.Cancel
                IsEditing = False
                Exit Sub
            Case Else
                If IsEditing Then
                    If tabData.SelectedTab Is tpRM Then
                        dgvRM.CurrentRow.Cells("MaterialID").Value = MaterialID
                        dgvRM.CurrentRow.Cells("Category").Value = SelectedCategory
                        If result = DialogResult.Yes Then dgvRM.CurrentRow.Cells("MaterialName").Value = MaterialName Else MaterialName = ""
                    Else
                        dgvProd.CurrentRow.Cells("MaterialIDProd").Value = MaterialID
                        dgvProd.CurrentRow.Cells("CategoryProd").Value = SelectedCategory
                        If result = DialogResult.Yes Then dgvProd.CurrentRow.Cells("MaterialNameProd").Value = MaterialName Else MaterialName = ""
                    End If
                Else
                    Dim row As DataRow = dt.NewRow
                    row!MaterialID = MaterialID
                    If result = DialogResult.Yes Then row!MaterialName = MaterialName Else row!MaterialName = ""
                    row!Category = SelectedCategory
                    dt.Rows.Add(row)
                End If
        End Select

        ChangesMade(btnSave, chkComplete, MaterialNotSaved)
        PopulateSummary()
        IsEditing = False

        If tabData.SelectedTab Is tpRM Then
            For Each SelectRow As DataGridViewRow In dgvRM.Rows
                If Trim(SelectRow.Cells("MaterialID").Value.ToString) = MaterialID And Trim(SelectRow.Cells("Category").Value.ToString) = SelectedCategory And Trim(SelectRow.Cells("MaterialName").Value.ToString) = MaterialName Then
                    SelectRow.Selected = True
                    dgvRM.CurrentCell = SelectRow.Cells("MaterialID")
                    Exit For
                Else
                    SelectRow.Selected = False
                End If
            Next
        Else
            For Each SelectRow As DataGridViewRow In dgvProd.Rows
                If SelectRow.Cells("MaterialIDProd").Value.ToString = MaterialID And SelectRow.Cells("CategoryProd").Value.ToString = SelectedCategory And SelectRow.Cells("MaterialNameProd").Value.ToString = MaterialName Then
                    SelectRow.Selected = True
                    dgvProd.CurrentCell = SelectRow.Cells("MaterialIDProd")
                    Exit For
                Else
                    SelectRow.Selected = False
                End If
            Next
        End If
        Exit Sub
NothingSelected:
        HandleNothingSelected()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ClickOK()
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
    End Sub

    Private Sub HandleNothingSelected()
        IsEditing = False
        ViewingLUs(pnlHeader, pnlLU, pnlData, True)
        ProfileMsgBox("CRIT", "SELECT", "material")
    End Sub

    Private Sub dgvProd_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvProd.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, MaterialNotSaved)
            PopulateSummary()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvRM_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvRM.CellBeginEdit
        If HasRights Then
            ChangesMade(btnSave, chkComplete, MaterialNotSaved)
            PopulateSummary()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetUpCategories()
        ViewingLUs(pnlHeader, pnlLU, pnlData, False)
        dgvLU.CurrentCell = dgvLU(0, 0)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim tmpstr As String
        If tabData.SelectedTab Is tpRM Then tmpstr = "Raw Material Inputs" Else tmpstr = "Product Yields"
        MyParent.PrintPreviews("Table 15", tmpstr)
    End Sub

    Private Sub chkComplete_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComplete.CheckedChanged
        If IsLoaded = False Then Exit Sub
        Dim chk As CheckBox = CType(sender, CheckBox)
        MyParent.DataEntryComplete(chk.Checked, "Material Balance")

        Dim row() As DataRow = MyParent.dsIsChecked.Tables(0).Select("Description='Material Balance'")
        row(0)!Checked = chk.Checked
        btnSave.BackColor = Color.Red
        btnSave.ForeColor = Color.White
        MaterialNotSaved = True
    End Sub

    Private Sub dgvRM_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRM.CellEndEdit
        ChangesMade(btnSave, chkComplete, MaterialNotSaved)
        PopulateSummary()
    End Sub

    Private Sub dgvProd_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvProd.CellEndEdit
        ChangesMade(btnSave, chkComplete, MaterialNotSaved)
        PopulateSummary()
    End Sub

    Private Sub EditMaterialID()
        IsEditing = True
        Dim MaterialID, Category As String
        On Error GoTo NothingSelected

        dgvRM.Rows(dgvRM.SelectedCells(0).RowIndex).Selected = True
        dgvProd.Rows(dgvProd.SelectedCells(0).RowIndex).Selected = True

        SetUpCategories()

        If tabData.SelectedTab Is tpRM Then
            MaterialID = dgvRM.CurrentRow.Cells("MaterialID").Value.ToString
            Category = dgvRM.CurrentRow.Cells("Category").Value.ToString
        Else
            MaterialID = dgvProd.CurrentRow.Cells("MaterialIDProd").Value.ToString
            Category = dgvProd.CurrentRow.Cells("CategoryProd").Value.ToString
        End If

        Select Case Category
            Case "OTHRM", "PROD" : cmbCategory.SelectedIndex = 0
            Case "RLUBE", "ASP" : cmbCategory.SelectedIndex = 1
            Case "RCHEM", "FCHEM" : cmbCategory.SelectedIndex = 2
            Case "SOLV" : cmbCategory.SelectedIndex = 3
            Case "FLUBE" : cmbCategory.SelectedIndex = 4
            Case "COKE" : cmbCategory.SelectedIndex = 5
            Case "MPROD" : cmbCategory.SelectedIndex = 6
        End Select

        For Each SelectRow As DataGridViewRow In dgvLU.Rows
            If Trim(SelectRow.Cells("MatlID").Value.ToString) = Trim(MaterialID) Then
                SelectRow.Selected = True
                dgvLU.CurrentCell = SelectRow.Cells("MatlID")
                Exit For
            Else
                SelectRow.Selected = False
            End If
        Next
        Exit Sub
NothingSelected:
        HandleNothingSelected()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EditMaterialID()
        ViewingLUs(pnlHeader, pnlLU, pnlData, False)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MyParent.SaveMaterialBalance()
    End Sub

    Private Sub dgvRM_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRM.CellContentDoubleClick
        If e.ColumnIndex = 0 Then EditMaterialID()
    End Sub

    Private Sub dgvProd_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvProd.CellContentDoubleClick
        If e.ColumnIndex = 0 Then EditMaterialID()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MaterialNotSaved Then
            Dim result As DialogResult = ProfileMsgBox("YNC", "UNSAVED", "Material Balance")
            Select Case result
                Case DialogResult.Yes : MyParent.SaveMaterialBalance()
                Case DialogResult.No
                    MyParent.dsYield.RejectChanges()
                    MyParent.dsMappings.Tables("Yield_RM").RejectChanges()
                    MyParent.dsMappings.Tables("Yield_Prod").RejectChanges()
                    MyParent.SaveMappings()
                    SaveMade(btnSave, MaterialNotSaved)
                Case DialogResult.Cancel : Exit Sub
            End Select
        End If
        MyParent.HideTopLayer(Me)
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim BrowseTo As New OpenFileDialog
        Dim result As DialogResult
        Dim culture As New ProfileEnvironment.Culture()

        result = BrowseTo.ShowDialog()

        If result = DialogResult.OK Then
            Me.Cursor = Cursors.WaitCursor
            MyParent.MyParent.pnlProgress.Text = "Updating worksheet list..."
            tbFilePath.Text = BrowseTo.FileName
            cbSheets.Text = ""
            cbSheets1.Text = ""

            dgvRM.Visible = False

            culture = culture.GetCulture()
            Dim currentUserCulture As String = Convert.ToString(culture.CultureName)
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(currentUserCulture)

            Dim ci As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture

            Dim MyXL, MyWB As Object
            Dim ExcelWasNotRunning As Boolean

            Try
                MyXL = GetObject(, "Excel.Application")
            Catch ex As Exception
                MyXL = CreateObject("Excel.Application")
                ExcelWasNotRunning = True
            End Try

            Try

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
                
                MyWB = MyXL.workbooks.open(BrowseTo.FileName, False)
                Dim ws As Object
                cbSheets.Items.Clear()
                cbSheets1.Items.Clear()
                For Each ws In MyWB.worksheets
                    cbSheets.Items.Add(ws.name)
                    cbSheets1.Items.Add(ws.name)
                Next

                MyWB.close(False)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(MyWB)
                If ExcelWasNotRunning Then
                    MyXL.quit()
                End If

                System.Runtime.InteropServices.Marshal.ReleaseComObject(MyXL)
                MyWB = Nothing
                MyXL = Nothing
                GC.Collect()

                System.Threading.Thread.CurrentThread.CurrentCulture = ci
               
                dgvRM.Visible = True

            Catch ex As Exception
                'HANDLE ERROR
                MyXL = Nothing
                GC.Collect()

                'Dww 11-29-2011 Old code
                'System.Threading.Thread.CurrentThread.CurrentCulture = ci
                culture = culture.GetCulture()
                Dim exUserCulture = Convert.ToString(culture.CultureName)
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(exUserCulture)

                Exit Sub
            End Try
            Me.Cursor = Cursors.Default
            MyParent.MyParent.pnlProgress.Text = "Ready"
        End If
    End Sub

    Friend Function ImportMaterials() As Boolean
        Dim ci As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Dim culture As New ProfileEnvironment.Culture

        Dim MyXL As Object = Nothing
        Dim MyWB As Object = Nothing
        Dim ExcelWasNotRunning As Boolean


        Try
            MyXL = GetObject(, "Excel.Application")
        Catch ex As Exception
            MyXL = CreateObject("Excel.Application")
            ExcelWasNotRunning = True
        End Try

        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")

        Try
            MyWB = MyXL.workbooks.open(tbFilePath.Text, False)
        Catch ex As Exception
            ProfileMsgBox("CRIT", "NOWB", tbFilePath.Text)
            GoTo CleanupExcel
        End Try

        Dim wsRM As Object
        Dim wsPY As Object
        Try
            wsRM = MyWB.worksheets(cbSheets.Text)
            wsPY = MyWB.worksheets(cbSheets1.Text)
        Catch ex As Exception
            ProfileMsgBox("CRIT", "NOWS", "")
            GoTo CleanupExcel
        End Try

        Try
            Dim newDV_LU As New DataView
            newDV_LU.Table = MyParent.MyParent.ds_Ref.Tables("Material_LU")
            Dim rowRM As DataRow = MyParent.dsMappings.Tables("Yield_RM").Rows(0)
            Dim rowPY As DataRow = MyParent.dsMappings.Tables("Yield_Prod").Rows(0)
            Dim newrowRM, newrowPY As DataRow
            Dim dtRM As DataTable = MyParent.dsYield.Tables("Yield_RM")
            Dim dtPY As DataTable = MyParent.dsYield.Tables("Yield_Prod")
            dtRM.Clear()
            dtPY.Clear()
            ChangesMade(btnSave, chkComplete, MaterialNotSaved)
            PopulateSummary()

            Dim MaterialID, Category As String
            Dim BBL As Double

            'HANDLE OTHER RAW MATERIALS FIRST (TWS - 10/14/08)
            For i As Integer = CInt(rowRM!lkStartRow) To CInt(rowRM!lkEndRow)

                MaterialID = CStr(wsRM.Range(rowRM!lkMaterialID.ToString & i.ToString).Value)
                Category = CStr(wsRM.Range(rowRM!lkCategory.ToString & i.ToString).Value)

                Try
                    BBL = CDbl(wsRM.Range(rowRM!lkBBL.ToString & i.ToString).value)
                Catch ex As Exception
                    BBL = 0
                End Try

                If BBL <> 0 And MaterialID <> "" And Category <> "" Then
                    newDV_LU.RowFilter = "AllowIn" & Category & "=1 AND MaterialID = '" & MaterialID & "'"
                    If newDV_LU.Count = 1 Then
                        newrowRM = dtRM.NewRow
                        newrowRM!Category = Category
                        newrowRM!MaterialID = MaterialID

                        newrowRM!MaterialName = wsRM.Range(rowRM!lkMaterialName.ToString & i.ToString).value & ""
                        If newrowRM!MaterialName = "" Then newrowRM!MaterialName = newDV_LU.Item(0)!SAIName.ToString

                        newrowRM!BBL = BBL
                        newrowRM!PriceLocal = wsRM.Range(rowRM!lkPriceLocal.ToString & i.ToString).value + 0
                        dtRM.Rows.Add(newrowRM)
                    End If
                End If
            Next

            'HANDLE PRODUCT YIELDS NEXT (TWS - 10/14/08)
            For i As Integer = CInt(rowPY!lkStartRow) To CInt(rowPY!lkEndRow)

                MaterialID = CStr(wsPY.Range(rowPY!lkMaterialID.ToString & i.ToString).Value)
                Category = CStr(wsPY.Range(rowPY!lkCategory.ToString & i.ToString).Value)

                Try
                    BBL = CDbl(wsPY.Range(rowPY!lkBBL.ToString & i.ToString).Value)
                Catch ex As Exception
                    BBL = 0
                End Try


                If BBL <> 0 And MaterialID <> "" And Category <> "" Then
                    newDV_LU.RowFilter = "AllowIn" & Category & "=1 AND MaterialID = '" & MaterialID & "'"
                    If newDV_LU.Count = 1 Then
                        newrowPY = dtPY.NewRow
                        newrowPY!Category = Category
                        newrowPY!MaterialID = MaterialID

                        newrowPY!MaterialName = wsPY.Range(rowPY!lkMaterialName.ToString & i.ToString).value & ""
                        If newrowPY!MaterialName.ToString = "" Or Category = "PROD" Then newrowPY!MaterialName = newDV_LU.Item(0)!SAIName.ToString

                        newrowPY!BBL = BBL
                        newrowPY!PriceLocal = wsPY.Range(rowPY!lkPriceLocal.ToString & i.ToString).value + 0

                        dtPY.Rows.Add(newrowPY)

                    End If
                End If
            Next
            PopulateSummary()
            ImportMaterials = True

            'Dww 11-29-2011 Old code
            'System.Threading.Thread.CurrentThread.CurrentCulture = ci
            culture = culture.GetCulture()
            Dim currentUserCulture As String = Convert.ToString(culture.CultureName)
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(currentUserCulture)

        Catch ex As Exception
            ProfileMsgBox("CRIT", "MAPPING", "yield")
        End Try

CleanupExcel:
        CloseExcel(MyXL, MyWB, wsRM)
        Return ImportMaterials

    End Function

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If ProfileMsgBox("YN", "IMPORT", "Material Balance") = DialogResult.Yes Then ImportMaterials()

    End Sub

    Private Sub SA_MaterialBalance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        viewLinksData()

        Me.btnSave.TabIndex = 0
        Me.btnEdit.TabIndex = 1
        Me.btnImport.TabIndex = 2
        Me.btnAdd.TabIndex = 3
        Me.btnClear.TabIndex = 5

        Me.tbFilePath.TabIndex = 6
        Me.btnBrowse.TabIndex = 7
        Me.cbSheets.TabIndex = 8
        Me.cbSheets1.TabIndex = 10

        Me.tabData.TabIndex = 11
        Me.dgvRM.TabIndex = 12
        Me.dgvProd.TabIndex = 13

        Me.cmbCategory.TabIndex = 14
        Me.dgvLU.TabIndex = 15

        Me.btnPreview.TabIndex = 16
        Me.chkComplete.TabIndex = 17

        Me.txtRMTotBBL.TabIndex = 18
        Me.txtProdTotBBL.TabIndex = 19
        Me.txtRMAvgCost.TabIndex = 20
        Me.txtProdAvgCost.TabIndex = 21

        Me.btnClose.TabIndex = 22

    End Sub

    Private Sub rbData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbData.CheckedChanged
        viewLinksData()
    End Sub

    Private Sub viewLinksData()
        dgvRM.Visible = rbData.Checked
        dgvProd.Visible = rbData.Checked
        dgRM_MAP.Visible = Not rbData.Checked
        dgProd_MAP.Visible = Not rbData.Checked
        btnPreview.Enabled = rbData.Checked

        If rbData.Checked Then
            lblRawMaterialInputs.Text = "Other Raw Materials:   DATA"
            lblProdYields.Text = "Product Yield:   DATA"
        Else
            lblRawMaterialInputs.Text = "Other Raw Materials:   LINKS"
            lblProdYields.Text = "Product Yield:   LINKS"
        End If

        If IsLoaded And HasRights Then ViewingLinks(pnlHeader, gbTools, rbData.Checked)
    End Sub

    Private Sub dgvRM_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvRM.UserDeletingRow
        Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "other raw material entry")
        If result = DialogResult.Yes Then
            ChangesMade(btnSave, chkComplete, MaterialNotSaved)
            PopulateSummary()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvProd_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvProd.UserDeletingRow
        Dim result As DialogResult = ProfileMsgBox("YN", "DELETE", "product yield entry")
        If result = DialogResult.Yes Then
            ChangesMade(btnSave, chkComplete, MaterialNotSaved)
            PopulateSummary()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub tbFilePath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilePath.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Material", tbFilePath.Name, tbFilePath.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, MaterialNotSaved)

        My.Settings.workbookMaterial = tbFilePath.Text
        My.Settings.Save()
    End Sub

    Private Sub cbSheets_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Material", cbSheets.Name, cbSheets.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, MaterialNotSaved)

        My.Settings.worksheetMaterial1 = cbSheets.Text
        My.Settings.Save()
    End Sub

    Private Sub cbSheets1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSheets1.TextChanged
        saveWkbWksReferences(MyParent.dsMappings, "Material", cbSheets1.Name, cbSheets1.Text)
        If IsLoaded Then ChangesMade(btnSave, chkComplete, MaterialNotSaved)

        My.Settings.worksheetMaterial2 = cbSheets1.Text
        My.Settings.Save()
    End Sub
End Class
