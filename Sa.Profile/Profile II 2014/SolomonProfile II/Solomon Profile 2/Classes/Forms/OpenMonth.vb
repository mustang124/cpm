Option Explicit On
Option Strict On

Imports System
Imports System.IO

Friend Class OpenMonth
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Friend Sub New(ByVal _MyParent As frmMain)
        MyBase.New()
        SetParent(_MyParent)
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Private WithEvents GettingStarted As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnOpen As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnNew As System.Windows.Forms.RadioButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents llRefresh As System.Windows.Forms.LinkLabel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OpenMonth))
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.GettingStarted = New System.Windows.Forms.GroupBox()
        Me.llRefresh = New System.Windows.Forms.LinkLabel()
        Me.rbtnNew = New System.Windows.Forms.RadioButton()
        Me.rbtnOpen = New System.Windows.Forms.RadioButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GettingStarted.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.BackColor = System.Drawing.Color.White
        Me.ListBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.ForeColor = System.Drawing.Color.Black
        Me.ListBox1.FormatString = "y"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(15, 22)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(207, 82)
        Me.ListBox1.TabIndex = 2
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.Image = CType(resources.GetObject("btnOK.Image"), System.Drawing.Image)
        Me.btnOK.Location = New System.Drawing.Point(243, 76)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(64, 28)
        Me.btnOK.TabIndex = 0
        Me.btnOK.TabStop = False
        Me.btnOK.Text = "OK"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'GettingStarted
        '
        Me.GettingStarted.Controls.Add(Me.llRefresh)
        Me.GettingStarted.Controls.Add(Me.btnOK)
        Me.GettingStarted.Controls.Add(Me.rbtnNew)
        Me.GettingStarted.Controls.Add(Me.rbtnOpen)
        Me.GettingStarted.Controls.Add(Me.ListBox1)
        Me.GettingStarted.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GettingStarted.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GettingStarted.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GettingStarted.Location = New System.Drawing.Point(6, 94)
        Me.GettingStarted.Name = "GettingStarted"
        Me.GettingStarted.Padding = New System.Windows.Forms.Padding(0, 0, 0, 6)
        Me.GettingStarted.Size = New System.Drawing.Size(319, 131)
        Me.GettingStarted.TabIndex = 5
        Me.GettingStarted.TabStop = False
        Me.GettingStarted.Text = "Choose Action"
        '
        'llRefresh
        '
        Me.llRefresh.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llRefresh.Location = New System.Drawing.Point(15, 107)
        Me.llRefresh.Name = "llRefresh"
        Me.llRefresh.Size = New System.Drawing.Size(207, 16)
        Me.llRefresh.TabIndex = 6
        Me.llRefresh.TabStop = True
        Me.llRefresh.Text = "Refresh Local Data"
        Me.llRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.llRefresh, "Refresh Local Data:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "1) Reference data" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2) Synchronize new release" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "3) Delete loc" & _
        "al data")
        '
        'rbtnNew
        '
        Me.rbtnNew.AutoSize = True
        Me.rbtnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnNew.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbtnNew.Location = New System.Drawing.Point(243, 45)
        Me.rbtnNew.Name = "rbtnNew"
        Me.rbtnNew.Size = New System.Drawing.Size(64, 17)
        Me.rbtnNew.TabIndex = 3
        Me.rbtnNew.Text = "CREATE"
        '
        'rbtnOpen
        '
        Me.rbtnOpen.AutoSize = True
        Me.rbtnOpen.Checked = True
        Me.rbtnOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnOpen.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbtnOpen.Location = New System.Drawing.Point(243, 22)
        Me.rbtnOpen.Name = "rbtnOpen"
        Me.rbtnOpen.Size = New System.Drawing.Size(52, 17)
        Me.rbtnOpen.TabIndex = 4
        Me.rbtnOpen.TabStop = True
        Me.rbtnOpen.Text = "OPEN"
        '
        'ToolTip1
        '
        Me.ToolTip1.ShowAlways = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.Control
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.GettingStarted)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(2, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Padding = New System.Windows.Forms.Padding(6, 0, 6, 6)
        Me.Panel2.Size = New System.Drawing.Size(333, 233)
        Me.Panel2.TabIndex = 20
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Image = Global.Solomon_Profile.My.Resources.Resources.error_1
        Me.Button2.Location = New System.Drawing.Point(311, -2)
        Me.Button2.Margin = New System.Windows.Forms.Padding(0)
        Me.Button2.Name = "Button2"
        Me.Button2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button2.Size = New System.Drawing.Size(21, 21)
        Me.Button2.TabIndex = 24
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(6, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(6, 12, 6, 6)
        Me.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel1.Size = New System.Drawing.Size(319, 94)
        Me.Panel1.TabIndex = 20
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Gray
        Me.Label6.Location = New System.Drawing.Point(87, 50)
        Me.Label6.Name = "Label6"
        Me.Label6.Padding = New System.Windows.Forms.Padding(10, 0, 10, 0)
        Me.Label6.Size = New System.Drawing.Size(226, 38)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "A Software Service for Improving Refinery Performance"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox2
        '
        Me.PictureBox2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(87, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(226, 38)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 16
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(6, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(81, 76)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.Panel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Padding = New System.Windows.Forms.Padding(2)
        Me.Panel3.Size = New System.Drawing.Size(337, 237)
        Me.Panel3.TabIndex = 21
        '
        'OpenMonth
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(345, 245)
        Me.Controls.Add(Me.Panel3)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "OpenMonth"
        Me.Padding = New System.Windows.Forms.Padding(4)
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "   Getting Started"
        Me.GettingStarted.ResumeLayout(False)
        Me.GettingStarted.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private IsDisabled As Boolean
    Private dvMonths As New DataView
    Private AdminRights As Boolean
    Private MyParent As frmMain
    Friend Event RefreshLocalFiles()


    Friend Sub PopulateListBox(ByVal dv As DataView, ByVal rights As Boolean)
        Dim CurrentEntry As String
        dvMonths = dv
        AdminRights = rights
        ListBox1.Items.Clear()
        If dvMonths.Count = 0 Then btnOK.Enabled = True Else btnOK.Enabled = False
        'ListBox1.Items.Clear()
        Dim dt As DataTable = dv.ToTable()
        For i As Integer = 0 To dt.Rows.Count - 1
            CurrentEntry = CDate(dt.Rows(i)("PeriodStart")).ToString("y")
            ListBox1.Items.Add(CurrentEntry)

        Next

        'With ListBox1

        '    .DisplayMember = "PeriodStart"
        '    .ValueMember = "PeriodStart"
        '    .DataSource = dvMonths

        '    If .Items.Count > 0 Then
        '        .SelectedIndex = 0
        '    Else
        '        .Enabled = False
        '        rbtnOpen.Enabled = False
        '        rbtnNew.Checked = True
        '    End If
        '    'Check for Gaps

        CheckForGaps()
        'End With

        btnOK.Select()
    End Sub
    Private Sub CheckForGaps()
        Dim CurrentValue, NextValue As String
        Dim ListCount As Integer = ListBox1.Items.Count - 1
        For i As Integer = 0 To ListCount
            ListBox1.SelectedIndex = i
            CurrentValue = ListBox1.SelectedItem.ToString
            If i + 1 <= ListCount Then
                ListBox1.SelectedIndex = i + 1
                NextValue = ListBox1.SelectedItem.ToString

                If DateAdd("M", -1, CDate(Replace(CurrentValue, "*", ""))) <> CDate(Replace(NextValue, "*", "")) Then
                    If DateAdd("M", -1, CDate(Replace(CurrentValue, "*", ""))).ToString("y") <> frmMain.PeriodStart.ToString("y") Then
                        ListBox1.Items.Insert(i + 1, "** " & DateAdd("M", -1, CDate(Replace(CurrentValue, "*", ""))).ToString("y"))
                        i = i + 1
                    Else
                        ListBox1.Items.Insert(i + 1, DateAdd("M", -1, CDate(Replace(CurrentValue, "*", ""))).ToString("y"))
                        i = i + 1
                    End If
                End If
            End If
        Next
        ListBox1.SelectedIndex = 0
    End Sub
    Private Sub rbtnOpen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnOpen.CheckedChanged
        If rbtnOpen.Checked Then
            IsDisabled = False
            If ListBox1.SelectedIndex = -1 And ListBox1.Items.Count > 0 Then
                ListBox1.SelectedIndex = 0
            End If
            btnOK.Enabled = True
            ListBox1.Enabled = True
            ListBox1.BackColor = Color.White
        End If
    End Sub

    Private Sub SetParent(ByVal _MDI As frmMain)
        MyParent = _MDI
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        If ListBox1.SelectedItem.ToString.Substring(0, 2) = "**" Then
            RemoveHandler rbtnOpen.CheckedChanged, AddressOf rbtnOpen_CheckedChanged
            RemoveHandler rbtnNew.CheckedChanged, AddressOf rbtnNew_CheckedChanged
            rbtnNew.Checked = True
            rbtnOpen.Checked = False
            rbtnOpen.Enabled = False
            AddHandler rbtnOpen.CheckedChanged, AddressOf rbtnOpen_CheckedChanged
            AddHandler rbtnNew.CheckedChanged, AddressOf rbtnNew_CheckedChanged
        Else
            RemoveHandler rbtnOpen.CheckedChanged, AddressOf rbtnOpen_CheckedChanged
            RemoveHandler rbtnNew.CheckedChanged, AddressOf rbtnNew_CheckedChanged
            rbtnNew.Checked = False
            rbtnOpen.Checked = True
            rbtnOpen.Enabled = True
            AddHandler rbtnOpen.CheckedChanged, AddressOf rbtnOpen_CheckedChanged
            AddHandler rbtnNew.CheckedChanged, AddressOf rbtnNew_CheckedChanged
        End If
        btnOK.Enabled = True
    End Sub

    Private Sub rbtnNew_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNew.CheckedChanged
        If rbtnNew.Checked Then
            IsDisabled = True
            ListBox1.BackColor = Color.LightGray
            ListBox1.SelectedIndex = -1
        End If
    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub llRefresh_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles llRefresh.LinkClicked
        Dim i As Integer = MyParent.MenuWindow.MenuItems.Count
        Me.Cursor = Cursors.WaitCursor
        If i > 0 Then
            ProfileMsgBox("CRIT", "CLOSEMONTHS", "")
            Exit Sub
        End If
        Dim result As DialogResult = ProfileMsgBox("YN", "WAIT", "")
        If result = Windows.Forms.DialogResult.Yes Then
            Me.Refresh()
            ListBox1.Enabled = False
            RaiseEvent RefreshLocalFiles()
            Dim ds As New DataSet
            Dim filename As String

            filename = pathConfig & "LoadedMonths.xml"
            ds.ReadXml(filename)

            Dim dv As New DataView(ds.Tables(0))
            dv.Sort = "PeriodStart DESC"

            ListBox1.Enabled = True
            ListBox1.Refresh()
            PopulateListBox(dv, AdminRights)
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        Dim profileMgr As New ProfileBusinessManager.Import
        Dim ds As New DataSet()

        ds = profileMgr.GetAllData()

    End Sub



End Class
