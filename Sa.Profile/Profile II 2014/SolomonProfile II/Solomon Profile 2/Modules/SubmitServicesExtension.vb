Option Explicit On

Imports System
Imports System.IO
Imports System.Net
Imports System.Reflection
Imports Microsoft.Web.Services2
Imports Microsoft.Web.Services2.Security.Policy
Imports Microsoft.Web.Services2.Security
Imports Microsoft.Web.Services2.Security.Tokens


Friend Class SubmitServicesExtension
    Inherits Solomon.Submit12.SubmitServices

    Dim UToken As UsernameToken

    Friend Sub New()
        MyBase.New()
        ConfigureProxy()
        ' 20081001 RRH Path - SecureWebServiceRequest(frmMain.AppPath & "\_CERT\policy.ct")
        SecureWebServiceRequest(pathCert & Policy.GetFileName())
    End Sub

    Friend Sub New(ByVal keypath As String)
        MyBase.New()
        SecureWebServiceRequest(keypath)
    End Sub

    Private Sub ConfigureProxy()

        Me.PreAuthenticate = True
        Me.Credentials = CredentialCache.DefaultCredentials
        Me.Proxy = WebRequest.GetSystemWebProxy
        'Me.Proxy = GlobalProxySelection.Select
        Me.Proxy.Credentials = CredentialCache.DefaultCredentials
        Dim wp As IWebProxy = WebRequest.GetSystemWebProxy
        'Dim wp As System.Net.WebProxy = GlobalProxySelection.Select
        wp.Credentials = CredentialCache.DefaultCredentials

        Dim username As String = My.Settings.ProxyUsername 'System.Configuration.ConfigurationManager.AppSettings("ProxyUsername")
        If Not IsNothing(username) AndAlso _
          username.Trim().Length > 0 Then
            Dim password As String = My.Settings.ProxyPassword 'System.Configuration.ConfigurationManager.AppSettings("ProxyPassword")
            Dim cr As New System.Net.NetworkCredential(username, password)

            Me.Proxy.Credentials = cr
            Me.Credentials = cr
            wp.Credentials = cr

        End If
    End Sub


    Private Sub SecureWebServiceRequest(ByVal keyPath As String)
        If Not File.Exists(keyPath) Then
            Throw New Exception(Directory.GetCurrentDirectory() + vbCrLf + "Key was not found. If you have a valid key, restart your application. Otherwise, please register your application " + _
                                "before you submit or download data.")
        End If

        Dim key As String

        Dim reader As New StreamReader(keyPath)
        Dim password As String

        If reader.Peek <> -1 Then
            key = reader.ReadLine
            reader.Close()
            password = CalcMiniKey(key)
            UToken = New UsernameToken(key, password, PasswordOption.SendHashed)
            'ConfigureProxy(nproxy, token)
        Else
            Throw New Exception("A key was not found")
        End If

    End Sub


    Private Sub ConfigureProxy(ByVal proxy As  _
        WebServicesClientProtocol, ByVal token As UsernameToken)
        proxy.RequestSoapContext.Security.Timestamp.TtlInSeconds = -1
        proxy.RequestSoapContext.Security.Tokens.Add(token)
        Dim dk As New DerivedKeyToken(token)
        proxy.RequestSoapContext.Security.Tokens.Add(dk)
        proxy.RequestSoapContext.Security.Elements.Add( _
           New MessageSignature(dk))
        proxy.RequestSoapContext.Security.Elements.Add( _
         New EncryptedData(dk))

    End Sub 'ConfigureProxy

    Private Function CalcMiniKey(ByVal key As String) As String
        Dim lastpos As Integer = key.Length - 1
        Dim firstPart As String = key.Substring(0, cint(Math.Round(lastpos * (3 / 8))))
        Dim secondPart As String = key.Substring(CInt(Math.Round(lastpos * 0.75)), CInt(lastpos - (lastpos * 0.75) + 1))

        Return secondPart + firstPart
    End Function


    'Resolves the 'underlying connection is closed' error
    Protected Overrides Function GetWebRequest(ByVal uri As Uri) As System.Net.WebRequest


        Dim requestPropertyInfo As PropertyInfo = Nothing
        'Dim request As WebRequest = WebRequest.Create(uri)

        Dim request As WebRequest = MyBase.GetWebRequest(uri)
        request.Credentials = CredentialCache.DefaultCredentials

        If IsNothing(requestPropertyInfo) Then
            requestPropertyInfo = request.GetType().GetProperty("Request", BindingFlags.Default)
        End If

        ' Retrieve underlying web request 
        Dim webRequest As HttpWebRequest = CType(requestPropertyInfo.GetValue(request, Nothing), HttpWebRequest)

        ' Setting KeepAlive 
        webRequest.KeepAlive = False

        Return request

    End Function
End Class
