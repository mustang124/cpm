
Imports System
Imports System.Collections.Specialized
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text

'/ <summary>
'/ Summary description for Helpers.
'/ </summary>
Public Class RTFImage

#Region "My Enums"

    ' Specifies the flags/options for the unmanaged call to the GDI+ method
    ' Metafile.EmfToWmfBits().
    ' Use the default conversion
    ' 0x00000000 - Embedded the source of the EMF metafiel within the resulting   metafile
    ' 0x00000001 - Place a 22-byte header in the resulting WMF file.  The header is required for the metafile to be considered placeable.
    ' 0x00000002 - Don't simulate clipping by using the XOR operator.
    Private Enum EmfToWmfBitsFlags
        EmfToWmfBitsFlagsDefault = &H0
        EmfToWmfBitsFlagsEmbedEmf = &H1
        EmfToWmfBitsFlagsIncludePlaceable = &H2
        EmfToWmfBitsFlagsNoXORClip = &H4
    End Enum

#End Region

#Region "My Structs"

    ' Definitions for colors in an RTF document
    Private Structure RtfColorDef
        Dim Unknown As String
        Public Const Black As String = "\red0\green0\blue0"
        Public Const Maroon As String = "\red128\green0\blue0"
        Public Const Green As String = "\red0\green128\blue0"
        Public Const Olive As String = "\red128\green128\blue0"
        Public Const Navy As String = "\red0\green0\blue128"
        Public Const Purple As String = "\red128\green0\blue128"
        Public Const Teal As String = "\red0\green128\blue128"
        Public Const Gray As String = "\red128\green128\blue128"
        Public Const Silver As String = "\red192\green192\blue192"
        Public Const Red As String = "\red255\green0\blue0"
        Public Const Lime As String = "\red0\green255\blue0"
        Public Const Yellow As String = "\red255\green255\blue0"
        Public Const Blue As String = "\red0\green0\blue255"
        Public Const Fuchsia As String = "\red255\green0\blue255"
        Public Const Aqua As String = "\red0\green255\blue255"
        Public Const White As String = "\red255\green255\blue255"
    End Structure

    ' Control words for RTF font families
    Private Structure RtfFontFamilyDef
        Dim a As String
        Public Const Unknown As String = "\fnil"
        Public Const Roman As String = "\froman"
        Public Const Swiss As String = "\fswiss"
        Public Const Modern As String = "\fmodern"
        Public Const Script As String = "\fscript"
        Public Const Decor As String = "\fdecor"
        Public Const Technical As String = "\ftech"
        Public Const BiDirect As String = "\fbidi"
    End Structure

#End Region

#Region "My Constants"

    ' Not used in this application.  Descriptions can be found with documentation
    ' of Windows GDI function SetMapMode
    Private Const MM_TEXT As Integer = 1
    Private Const MM_LOMETRIC As Integer = 2
    Private Const MM_HIMETRIC As Integer = 3
    Private Const MM_LOENGLISH As Integer = 4
    Private Const MM_HIENGLISH As Integer = 5
    Private Const MM_TWIPS As Integer = 6

    ' Ensures that the metafile maintains a 1:1 aspect ratio
    Private Const MM_ISOTROPIC As Integer = 7

    ' Allows the x-coordinates and y-coordinates of the metafile to be adjusted
    ' independently
    Private Const MM_ANISOTROPIC As Integer = 8

    ' Represents an unknown font family
    Private Const FF_UNKNOWN As String = "UNKNOWN"

    ' The number of hundredths of millimeters (0.01 mm) in an inch
    ' For more information, see GetImagePrefix() method.
    Private Const HMM_PER_INCH As Integer = 2540

    ' The number of twips in an inch
    ' For more information, see GetImagePrefix() method.
    Private Const TWIPS_PER_INCH As Integer = 1440

#End Region

#Region "My Privates"

    ' The default text color
    Private textColor As RtfColorDef

    ' The default text background color
    Private highlightColor As RtfColorDef

    ' Dictionary that maps color enums to RTF color codes
    Private rtfColor As HybridDictionary

    ' Dictionary that mapas Framework font families to RTF font families
    Private rtfFontFamily As HybridDictionary

    ' The horizontal resolution at which the control is being displayed
    Private xDpi As Decimal

    ' The vertical resolution at which the control is being displayed
    Private yDpi As Decimal

#End Region

#Region "Elements required to create an RTF document"

    ' RTF HEADER
    '----------
    ' 
    ' \rtf[N]        - For text to be considered to be RTF, it must be enclosed in this tag.
    '                  rtf1 is used because the RichTextBox conforms to RTF Specification
    '                 version 1.
    ' \ansi        - The character set.
    ' \ansicpg[N]    - Specifies that unicode characters might be embedded. ansicpg1252
    '                 is the default used by Windows.
    ' \deff[N]        - The default font. \deff0 means the default font is the first font
    '                 found.
    ' \deflang[N]    - The default language. \deflang1033 specifies US English.
    ' 
    Private Const RTF_HEADER As String = "{\rtf1\ansi\ansicpg1252\deff0\deflang1033"

    'RTF DOCUMENT AREA
    '-----------------
    '
    ' \viewkind[N]    - The type of view or zoom level.  \viewkind4 specifies normal view.
    ' \uc[N]        - The number of bytes corresponding to a Unicode character.
    ' \pard        - Resets to default paragraph properties
    ' \cf[N]        - Foreground color.  \cf1 refers to the color at index 1 in
    '                  the color table
    ' \f[N]        - Font number. \f0 refers to the font at index 0 in the font
    '                  table.
    ' \fs[N]        - Font size in half-points.
    ' 
    Private Const RTF_DOCUMENT_PRE As String = "\viewkind4\uc1\pard\cf1\f0\fs20"
    Private Const RTF_DOCUMENT_POST As String = "\cf0\fs17end"
    Private Const RTF_IMAGE_POST As String = "end"

#End Region

    '/ <summary>
    '/ Use the EmfToWmfBits function in the GDI+ specification to convert a 
    '/ Enhanced Metafile to a Windows Metafile
    '/ </summary>
    '/ <param name="_hEmf">
    '/ A handle to the Enhanced Metafile to be converted
    '/ </param>
    '/ <param name="_bufferSize">
    '/ The size of the buffer used to store the Windows Metafile bits returned
    '/ </param>
    '/ <param name="_buffer">
    '/ An array of bytes used to hold the Windows Metafile bits returned
    '/ </param>
    '/ <param name="_mappingMode">
    '/ The mapping mode of the image.  This control uses MM_ANISOTROPIC.
    '/ </param>
    '/ <param name="_flags">
    '/ Flags used to specify the format of the Windows Metafile returned
    '/ </param>
    <DllImport("gdiplus.dll")> _
    Private Shared Function GdipEmfToWmfBits(ByVal _hEmf As IntPtr, ByVal _bufferSize As UInt32, _
    ByVal _buffer() As Byte, ByVal _mappingMode As Integer, ByVal _flags As EmfToWmfBitsFlags) As UInt32
    End Function
    'Private Helpers()


    '/ <summary>
    '/ Gets the RTF image.
    '/ </summary>
    '/ <value></value>
    Public Shared Function GetRtfImage(ByVal image As Image) As String

        Dim _rtf As New StringBuilder

        ' The horizontal resolution at which the control is being displayed
        Dim xDpi As Decimal
        ' The vertical resolution at which the control is being displayed
        Dim yDpi As Decimal

        Dim g As Graphics = Graphics.FromImage(image)

        xDpi = g.DpiX
        yDpi = g.DpiY

        ' Create the image control string and append it to the RTF string
        _rtf.Append(GetImagePrefix(image, xDpi, yDpi))

        ' Create the Windows Metafile and append its bytes in HEX format
        _rtf.Append(CreateRtfImage(image))

        ' Close the RTF image control string
        _rtf.Append(RTF_IMAGE_POST)

        Return _rtf.ToString()
    End Function

    '/ <summary>
    '/ Wraps the image in an Enhanced Metafile by drawing the image onto the
    '/ graphics context, then converts the Enhanced Metafile to a Windows
    '/ Metafile, and finally appends the bits of the Windows Metafile in HEX
    '/ to a string and returns the string.
    '/ </summary>
    '/ <param name="_image"></param>
    '/ <returns>
    '/ A string containing the bits of a Windows Metafile in HEX
    '/ </returns>
    Private Shared Function CreateRtfImage(ByVal _image As Image) As String

        Dim _rtf As StringBuilder

        ' Used to store the enhanced metafile
        Dim _stream As MemoryStream

        ' Used to create the metafile and draw the image
        Dim _graphics As Graphics

        ' The enhanced metafile
        Dim _metaFile As Metafile

        ' Handle to the device context used to create the metafile
        Dim _hdc As IntPtr

        Try
            _rtf = New StringBuilder
            _stream = New MemoryStream

            ' Get a graphics context from the RichTextBox
            _graphics = Graphics.FromImage(_image)

            ' Get the device context from the graphics context
            _hdc = _graphics.GetHdc()

            ' Create a new Enhanced Metafile from the device context
            _metaFile = New Metafile(_stream, _hdc)

            ' Release the device context
            _graphics.ReleaseHdc(_hdc)


            ' Get a graphics context from the Enhanced Metafile
            _graphics = Graphics.FromImage(_metaFile)

            ' Draw the image on the Enhanced Metafile
            _graphics.DrawImage(_image, New Rectangle(0, 0, _image.Width, _image.Height))



            ' Get the handle of the Enhanced Metafile
            Dim _hEmf As IntPtr = _metaFile.GetHenhmetafile()

            ' A call to EmfToWmfBits with a null buffer return the size of the
            ' buffer need to store the WMF bits.  Use this to get the buffer
            ' size.
            Dim _bufferSize As UInt32 = GdipEmfToWmfBits(_hEmf, Convert.ToUInt32(0), Nothing, MM_ANISOTROPIC, _
                    EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)

            ' Create an array to hold the bits
            Dim _buffer() As Byte

            ' A call to EmfToWmfBits with a valid buffer copies the bits into the
            ' buffer an returns the number of bits in the WMF.  
            GdipEmfToWmfBits(_hEmf, _bufferSize, _buffer, MM_ANISOTROPIC, _
                    EmfToWmfBitsFlags.EmfToWmfBitsFlagsDefault)

            ' Append the bits to the RTF string
            Dim i As Integer
            For i = 0 To _buffer.Length
                _rtf.Append(String.Format("{0:X2}", _buffer(i)))
            Next

            Return _rtf.ToString()

        Finally

            If Not IsNothing(_graphics) Then
                _graphics.Dispose()
            End If
            If Not IsNothing(_metaFile) Then
                _metaFile.Dispose()
            End If
            If Not IsNothing(_stream) Then
                _stream.Close()
            End If
        End Try
    End Function

    '/ <summary>
    '/ Creates the RTF control string that describes the image being inserted.
    '/ This description (in this case) specifies that the image is an
    '/ MM_ANISOTROPIC metafile, meaning that both X and Y axes can be scaled
    '/ independently.  The control string also gives the images current dimensions,
    '/ and its target dimensions, so if you want to control the size of the
    '/ image being inserted, this would be the place to do it. The prefix should
    '/ have the form ...
    '/ 
    '/ {\pict\wmetafile8\picw[A]\pich[B]\picwgoal[C]\pichgoal[D]
    '/ 
    '/ where ...
    '/ 
    '/ A    = current width of the metafile in hundredths of millimeters (0.01mm)
    '/        = Image Width in Inches * Number of (0.01mm) per inch
    '/        = (Image Width in Pixels / Graphics Context's Horizontal Resolution) * 2540
    '/        = (Image Width in Pixels / Graphics.DpiX) * 2540
    '/ 
    '/ B    = current height of the metafile in hundredths of millimeters (0.01mm)
    '/        = Image Height in Inches * Number of (0.01mm) per inch
    '/        = (Image Height in Pixels / Graphics Context's Vertical Resolution) * 2540
    '/        = (Image Height in Pixels / Graphics.DpiX) * 2540
    '/ 
    '/ C    = target width of the metafile in twips
    '/        = Image Width in Inches * Number of twips per inch
    '/        = (Image Width in Pixels / Graphics Context's Horizontal Resolution) * 1440
    '/        = (Image Width in Pixels / Graphics.DpiX) * 1440
    '/ 
    '/ D    = target height of the metafile in twips
    '/        = Image Height in Inches * Number of twips per inch
    '/        = (Image Height in Pixels / Graphics Context's Horizontal Resolution) * 1440
    '/        = (Image Height in Pixels / Graphics.DpiX) * 1440
    '/    
    '/ </summary>
    '/ <remarks>
    '/ The Graphics Context's resolution is simply the current resolution at which
    '/ windows is being displayed.  Normally it's 96 dpi, but instead of assuming
    '/ I just added the code.
    '/ 
    '/ According to Ken Howe at pbdr.com, "Twips are screen-independent units
    '/ used to ensure that the placement and proportion of screen elements in
    '/ your screen application are the same on all display systems."
    '/ 
    '/ Units Used
    '/ ----------
    '/ 1 Twip = 1/20 Point
    '/ 1 Point = 1/72 Inch
    '/ 1 Twip = 1/1440 Inch
    '/ 
    '/ 1 Inch = 2.54 cm
    '/ 1 Inch = 25.4 mm
    '/ 1 Inch = 2540 (0.01)mm
    '/ </remarks>
    '/ <param name="_image"></param>
    '/ <param name="xDpi"></param>
    '/ <param name="yDpi"></param>
    '/ <returns></returns>
    Private Shared Function GetImagePrefix(ByVal _image As Image, ByVal xDpi As Decimal, ByVal yDpi As Decimal) As String

        Dim _rtf As New StringBuilder

        ' Calculate the current width of the image in (0.01)mm
        Dim picw As String = CInt(Math.Round((_image.Width / xDpi) * HMM_PER_INCH)).ToString()

        ' Calculate the current height of the image in (0.01)mm
        Dim pich As String = CInt(Math.Round((_image.Height / yDpi) * HMM_PER_INCH)).ToString()

        ' Calculate the target width of the image in twips
        Dim picwgoal As String = CInt(Math.Round((_image.Width / xDpi) * TWIPS_PER_INCH)).ToString()

        ' Calculate the target height of the image in twips
        Dim pichgoal As String = CInt(Math.Round((_image.Height / yDpi) * TWIPS_PER_INCH)).ToString()

        ' Append values to RTF string
        _rtf.Append("{\pict\wmetafile8")
        _rtf.Append("\picw")
        _rtf.Append(picw)
        _rtf.Append("\pich")
        _rtf.Append(pich)
        _rtf.Append("\picwgoal")
        _rtf.Append(picwgoal)
        _rtf.Append("\pichgoal")
        _rtf.Append(pichgoal)
        _rtf.Append(" ")

        Return _rtf.ToString()


    End Function
End Class

