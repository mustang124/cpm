<%@ Register TagPrefix="c1c" Namespace="C1.Web.C1Command" Assembly="C1.Web.C1Command, Version=1.0.20044.68, Culture=neutral, PublicKeyToken=96d8a77dc0c22f6b" %>
<%@ Page Language="vb" AutoEventWireup="false" aspcompat="true" Codebehind="PopUpWin.aspx.vb" Inherits="ProfileII_Corporate.PopUpWin"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>
			<%= Request.QueryString("node")%>
		</title>
		<META http-equiv="PRAGMA" content="NO-CACHE">
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<STYLE type="text/css">BODY { BACKGROUND: url(foo) fixed }
	#fixme { RIGHT: 20px; BOTTOM: 0px; POSITION: absolute; ; TOP: expression(document.body.scrollTop+document.body.clientHeight-this.clientHeight) }
		</STYLE>
	</HEAD>
	<BODY bgColor="#ffffff" MS_POSITIONING="GridLayout">
		<FORM id="Form1" method="post" runat="server">
			<INPUT id=ltPage 
type=hidden 
value='<%= IIf(Not IsNothing(Request.QueryString("page")),Request.QueryString("page"),1) %>' 
name=ltPage> <INPUT id=ltTotal type=hidden 
value='<%= IIf(Not IsNothing(Request.Form("ltTotal")),Request.Form("ltTotal"),1) %>' 
name=ltTotal>
			<c1c:c1webmenu id="C1WebMenu1" style="Z-INDEX: 105; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
				Font-Names="Microsoft Sans Serif" Font-Size="8pt" Width="103%" Height="16px" ItemSpacing="2px"
				DisabledItemStyle-ForeColor="#ACA899" SelectedItemStyle-BorderStyle="Solid" SelectedItemStyle-BorderColor="#7C7C94"
				BorderWidth="1px" BorderStyle="Solid" BackColor="#D9D9E6" MenuLayout="Horizontal" BorderColor="#9D9DA1"
				ItemTextSpacing="2px" SelectedItemStyle-BorderWidth="1px" IconBarWidth="2px" SelectedItemStyle-BackColor="#E6E6EF"
				ItemStyle-ForeColor="Black" CssClass="noshow">
				<c1c:C1MenuItem id="C1MenuItem1" runat="server" CssClass="noshow" Text="Report Options" SubMenuID="C1SubMenu1"></c1c:C1MenuItem>
				<c1c:C1MenuItem id="C1MenuItem6" runat="server" Text="Print" LinkUrl="javascript:window.print();"></c1c:C1MenuItem>
			</c1c:c1webmenu><asp:panel id="pnlContent" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 24px"
				runat="server" Font-Names="Tahoma" Font-Size="XX-Small" Width="98%" Height="87.03%" CssClass="NPanel" Wrap="False">
				<P>
					<asp:PlaceHolder id="phContent" runat="server"></asp:PlaceHolder></P>
			</asp:panel>
			<DIV id="fixme" style="Z-INDEX: 104; VISIBILITY: hidden">
				<% if Request.QueryString("section")="REPORTS" then %>
				<asp:imagebutton id="btnDataDump" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none"
					runat="server" ImageUrl="images\button_email_this.gif"></asp:imagebutton>
				<% end if %>
				<A href="javascript:window.print();"><IMG id="IMG2" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none"
						src="images\button_print_this.gif" runat="server"> </A>
			</DIV>
			<c1c:c1submenu id="C1SubMenu1" style="Z-INDEX: 101" runat="server" ItemSpacing="1px" SelectedItemStyle-BorderStyle="Solid"
				SelectedItemStyle-BorderColor="#4B4B6F" BorderWidth="1px" BorderStyle="Solid" BackColor="#FAFAFA"
				BorderColor="#7C7C94" ItemTextSpacing="3px" SelectedItemStyle-BorderWidth="1px" IconBarWidth="20px"
				SelectedItemStyle-BackColor="#FFEEC2" ParentItemID="C1MenuItem1" BackImageUrl="/c1webcommand_client/v1_1/af_office2003smbk01.jpg">
				<c1c:C1MenuItem id="C1MenuItem7" runat="server" CssClass="noshow" Text="Transfer" SubMenuID="C1SubMenu2"></c1c:C1MenuItem>
				<c1c:C1MenuItem id="C1MenuItem8" runat="server" Text="Download"></c1c:C1MenuItem>
			</c1c:c1submenu><c1c:c1submenu id="C1SubMenu2" style="Z-INDEX: 102" runat="server" ItemSpacing="1px" SelectedItemStyle-BorderStyle="Solid"
				SelectedItemStyle-BorderColor="#4B4B6F" BorderWidth="1px" BorderStyle="Solid" BackColor="#FAFAFA" BorderColor="#7C7C94"
				ItemTextSpacing="3px" SelectedItemStyle-BorderWidth="1px" IconBarWidth="20px" SelectedItemStyle-BackColor="#FFEEC2"
				ParentItemID="C1MenuItem7" BackImageUrl="/c1webcommand_client/v1_1/af_office2003smbk01.jpg">
				<c1c:C1MenuItem id="C1MenuItem10" runat="server" Text="Word"></c1c:C1MenuItem>
				<c1c:C1MenuItem id="C1MenuItem11" runat="server" Text="Excel"></c1c:C1MenuItem>
			</c1c:c1submenu></FORM>
	</BODY>
</HTML>
