Imports System.Drawing.Bitmap
Imports System.Drawing.Image
Imports System.IO
Imports System.Text
'Imports System.Xml
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports System.Text.RegularExpressions
'Imports C1.C1Pdf
'Imports System.Xml.Xsl
'Imports org.xml.sax
'Imports org.apache.fop.apps
'Imports java.io
'Imports Root


Partial Class PopUpWin
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents download As System.Web.UI.WebControls.Panel
    Protected WithEvents pgSection As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgNode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents C1MenuItem12 As C1.Web.C1Command.C1MenuItem
    'Protected WithEvents C1WebReport1 As C1.Web.C1WebReport.C1WebReport

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        If Request.QueryString("section").ToUpper = "CHARTS" Then
            C1MenuItem1.Visible = False
        Else
            C1MenuItem1.Visible = True
        End If

        'Response.Filter = New ResponseFilter(Response.Filter)


        'If Not IsNothing(Request.QueryString("C1MenuItem8")) Then

        'End If
    End Sub

#End Region

    Dim c3 As Control
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        LoadResults()


    End Sub

    Sub LoadResults()
        Dim section As String = Request.QueryString("section")
        Dim selectedNode As String = Request.QueryString("node")
        Dim item As String
        Dim refineries() As String
        Session("ShowAll") = False

        Select Case section.ToUpper
            Case "REPORTS"
                c3 = LoadControl("ReportControl.ascx")
                CType(c3, ReportControl).ReportName = selectedNode
                CType(c3, ReportControl).Data = Request.QueryString("ds")
                CType(c3, ReportControl).StartDate = Request.QueryString("startdate")
                CType(c3, ReportControl).Company = Session("CompID")
                CType(c3, ReportControl).StudyYear = Request.QueryString("methodology")
                CType(c3, ReportControl).Scenario = Request.QueryString("scenario")
                CType(c3, ReportControl).UnitsOfMeasure = Request.QueryString("UOM")
                CType(c3, ReportControl).Currency = Request.QueryString("currency")
                CType(c3, ReportControl).Refineries.Clear()

                Select Case Request.QueryString("rptOptions").ToUpper
                    Case "CURRENT MONTH"
                        CType(c3, ReportControl).CurrentMonth = True
                    Case "ROLLING AVERAGE"
                        CType(c3, ReportControl).RollingAverage = True
                    Case "YTD AVERAGE"
                        CType(c3, ReportControl).YTDAverage = True
                End Select

                If Not IsNothing(Session("Refineries")) Then
                    refineries = Session("Refineries").ToString.Split(",".ToCharArray())
                    For Each item In refineries
                        If item.Length > 0 Then
                            CType(c3, ReportControl).Refineries.Add(item)
                        End If
                    Next
                End If


            Case "CHARTS"
                'Pre-populate chart_lu
                ' sdChartLU.Fill(DsChartLU1)

                ' Dim row() As DataRow = DsChartLU1.Tables(0).Select("ChartTitle='" + selectedNode + "'")

                'If row.Length > 0 Then
                c3 = LoadControl("ChartControl.ascx")
                CType(c3, ChartControl).ChartName = selectedNode
                CType(c3, ChartControl).DataSet = Request.QueryString("ds")
                CType(c3, ChartControl).StartDate = Request.QueryString("startdate")
                CType(c3, ChartControl).EndDate = Request.QueryString("enddate")
                CType(c3, ChartControl).Company = Session("CompID")
                CType(c3, ChartControl).StudyYear = Request.QueryString("methodology")
                CType(c3, ChartControl).UOM = Request.QueryString("UOM")
                CType(c3, ChartControl).Currency = Request.QueryString("currency")
                CType(c3, ChartControl).Scenario = Request.QueryString("scenario")
                CType(c3, ChartControl).TableName = Request.QueryString("table")

                CType(c3, ChartControl).ColumnName = Request.QueryString("column").Replace("|", "'")
                CType(c3, ChartControl).RollingAverageField = Request.QueryString("average")
                CType(c3, ChartControl).YTDAverageField = Request.QueryString("ytd")

                Select Case Request.QueryString("rptOptions").ToUpper
                    Case "CURRENT MONTH"
                        CType(c3, ChartControl).CurrentMonth = True
                    Case "ROLLING AVERAGE"
                        CType(c3, ChartControl).RollingAverage = True
                    Case "YTD AVERAGE"
                        CType(c3, ChartControl).YTDAverage = True
                End Select

                CType(c3, ChartControl).Refineries.Clear()

                If Not IsNothing(Session("Refineries")) Then
                    refineries = Session("Refineries").ToString.Split(",".ToCharArray())
                    For Each item In refineries
                        If item.Length > 0 Then
                            CType(c3, ChartControl).Refineries.Add(item)
                        End If
                    Next
                End If

                'End If
        End Select

        If Not IsNothing(c3) Then
            phContent.Controls.Add(c3)
        End If

    End Sub



    Private Sub btnDataDump_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDataDump.Click
        If TypeOf c3 Is ReportControl Then
            CType(c3, ReportControl).SendDataDump()
        End If

    End Sub

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    IMG2.Visible = False
    '    btnDataDump.Visible = False
    '    Button1.Visible = False
    '    Response.Clear()
    '    Response.Charset = ""
    '    'Response.ClearHeaders()
    '    Page.EnableViewState = False
    '    Response.ContentType = "application/vnd.ms-excel"
    '    'Response.ContentType = "application/msword"
    '    'Response.ContentType = "text/plain"
    '    Response.AddHeader("Content-Disposition", "inline;filename=""YourReportName.xls""")
    'End Sub

    Private Sub Exit_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Pdf_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles C1MenuItem12.Click
        'C1WebMenu1.Visible = False
        'C1SubMenu1.Visible = False
        'C1SubMenu2.Visible = False

        'Response.Clear()
        'Response.ClearHeaders()
        'Response.Charset = ""

        'Dim renderedOutput As StringBuilder = New StringBuilder
        'Dim strWriter As New System.IO.StringWriter(renderedOutput)
        'Dim tWriter As HtmlTextWriter = New HtmlTextWriter(strWriter)
        'Page.RenderControl(tWriter)
        'Dim outputStream As FileStream = New FileStream(Server.MapPath("Report.htm"), _
        '                                FileMode.Create)
        'Dim sWriter As StreamWriter = New StreamWriter(outputStream)
        'sWriter.Write(renderedOutput.ToString())
        'sWriter.Flush()
        'sWriter.Close()
        'strWriter.Close()
        'tWriter.Close()

        '' Dim document As New Document(PageSize.A4, 80, 50, 30, 65)

        ''If File.Exists(Server.MapPath("Report.pdf")) Then
        ''    File.Delete(Server.MapPath("Report.pdf"))
        ''End If
        ''Dim buffer() As Byte
        ''Dim input As System.IO.MemoryStream = New System.IO.MemoryStream(buffer)

        ''Dim m As System.IO.MemoryStream = New System.IO.MemoryStream
        '' PdfWriter.GetInstance(document, m)

        ''Dim strm As FileStream = New FileStream(Server.MapPath("Report.htm"), FileMode.Create)
        ''Dim writer As System.Xml.XmlTextWriter = New System.Xml.XmlTextWriter(Response.Output)
        ''Dim i As Integer
        ''While (i = Response.OutputStream.ReadByte <> -1)
        ''    strm.WriteByte(i)
        ''End While

        ''document.Open()
        ''Dim xmlReader As New System.Xml.XmlReader


        ''Dim c1Pdf As New C1PdfDocument(Printing.PaperKind.A4)
        ''Dim rc As RectangleF = c1Pdf.PageRectangle()
        ''c1Pdf.DrawImage(Bitmap.FromStream(Response.OutputStream), rc, ContentAlignment.MiddleCenter, ImageSizeModeEnum.Clip)
        ''c1Pdf.Save(m)

        ''HtmlParser.Parse(document, Response.OutputStream)

        ''Response.ContentType = "application/octet-stream"
        ''document.Close()

        ''Response.ContentType = "application/pdf"
        ''Response.OutputStream.Write(m.GetBuffer(), 0, m.GetBuffer.Length)
        ''Response.OutputStream.Flush()
        ''Response.Close()

        '' Response.WriteFile(Server.MapPath("Report.pdf"))
        '' Response.TransmitFile(Server.MapPath("Report.pdf"))
        '''Response.ContentType = "application/msword"
        '''Response.ContentType = "text/plain"
        ''
        ''Response.AddHeader("Content-Disposition", "inline;filename=""Report.pdf""")
        ''document.Close()
        ''Response.TransmitFile("Report.pdf")



        'Dim htmlDoc As New HtmlAgilityPack.HtmlDocument

        'Dim xmlFile As String = Request.PhysicalApplicationPath + "Report.htm"

        'htmlDoc.Load(xmlFile)
        'htmlDoc.OptionOutputAsXml = True
        'htmlDoc.Save(Request.PhysicalApplicationPath + "Report.xml")
        'xmlFile = Request.PhysicalApplicationPath + "Report.xml"
        'Dim foFile As String = Request.PhysicalApplicationPath + "Report.fo"
        ''Dim xslFile As String = Request.PhysicalApplicationPath + "xml\xhtml-to-xslfo.xsl"
        'Dim xslFile As String = Request.PhysicalApplicationPath + "xhtml\html2xhtml.xsl"
        'Dim pdfFile As String = Request.PhysicalApplicationPath + "Report.pdf"

        'Dim xslt As New System.Xml.Xsl.XslTransform
        'xslt.Load(xslFile)
        'xslt.Transform(xmlFile, foFile, Nothing)

        'Dim input As New java.io.FileInputStream(foFile)
        'Dim source As New org.xml.sax.InputSource(input)
        'Dim output As New java.io.FileOutputStream(pdfFile)
        'Dim driver = New org.apache.fop.apps.Driver(source, output)


        'driver.setRenderer(driver.RENDER_PDF)
        'driver.run()
        'output.close()

        'Response.ContentType = "application/pdf"
        'Response.AddHeader("Content-Disposition", "inline;filename=""Report.pdf""")
        'Response.Close()
    End Sub

    'Private Function GetUrlHostAndPath() As String
    '    Dim protocol As String = IIf(Request.ServerVariables("HTTPS").ToUpper = "ON", "https://", "http://")
    '    Dim url As String = protocol + Request.Url.Host + "/" + Request.Path.Split("/\".ToCharArray)(1)
    '    Return url
    'End Function

    Private Sub Excel_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles C1MenuItem11.Click
        Session("ShowAll") = True 'Return all refineries
        IMG2.Visible = False
        btnDataDump.Visible = False
        C1WebMenu1.Visible = False
        C1SubMenu1.Visible = False
        C1SubMenu2.Visible = False
        Response.Clear()
        Response.Charset = ""
        Page.EnableViewState = False

        'Dim url As String = GetUrlHostAndPath()
        'Dim sb As StringBuilder = New StringBuilder
        'Dim sw As StringWriter = New StringWriter(sb)
        'Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        'pnlContent.RenderControl(hw)
        'Dim encoding As New System.Text.UTF8Encoding
        'Dim newBuffer As String = Regex.Replace(sb.ToString(), "c1chartimage.aspx", url + "/c1chartimage.aspx")

        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=""Report.xls""")
        'Response.BinaryWrite(encoding.GetBytes(newBuffer))
    End Sub

    Private Sub Word_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles C1MenuItem10.Click

        Session("ShowAll") = False 'Show only refineries on the screen
        IMG2.Visible = False
        btnDataDump.Visible = False
        C1WebMenu1.Visible = False
        C1SubMenu1.Visible = False
        C1SubMenu2.Visible = False
        Response.Clear()
        Response.Charset = ""
        Page.EnableViewState = False

        'Dim url As String = GetUrlHostAndPath()
        'Dim sb As StringBuilder = New StringBuilder
        'Dim sw As StringWriter = New StringWriter(sb)
        'Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
        'pnlContent.RenderControl(hw)
        'Dim encoding As New System.Text.UTF8Encoding

        'Dim newBuffer As String = Regex.Replace(sb.ToString(), "c1chartimage.aspx", url + "/c1chartimage.aspx")

        'Dim wdApp As New Microsoft.Office.Interop.Word.Application
        'Dim doc As Document
        'Dim sessionId As String = Session.SessionID
        'Try

        '    Dim fil As New FileStream(Server.MapPath(sessionId + "Report.html"), FileMode.Create)
        '    fil.Write(encoding.GetBytes(newBuffer), 0, encoding.GetByteCount(newBuffer))
        '    fil.Close()

        '    doc = wdApp.Documents.Open(Server.MapPath(sessionId + "Report.html"))

        '    Dim oScript As Script
        '    Dim df As Integer = doc.Shapes.Count
        '    Dim hj As Integer = doc.InlineShapes.Count
        '    For Each oScript In doc.Scripts
        '        oScript.Delete()
        '    Next

        '    Dim oField As Field
        '    For Each oField In doc.Fields

        '        If oField.Type = WdFieldType.wdFieldIncludePicture Then
        '            oField.LinkFormat.SavePictureWithDocument = True
        '            oField.LinkFormat.BreakLink()
        '        ElseIf oField.Type = WdFieldType.wdFieldHyperlink Or _
        '               oField.Type = WdFieldType.wdFieldHTMLActiveX Then
        '            oField.Delete()
        '        End If
        '    Next

        '    Dim br As InlineShape
        '    For Each br In doc.InlineShapes()
        '        If br.Type = WdInlineShapeType.wdInlineShapePicture Then
        '            br.ScaleHeight = 320
        '            'br.ScaleWidth = 320

        '        End If
        '    Next

        'Catch ex As Exception
        '    Throw ex
        'Finally
        '    doc.SaveAs(Server.MapPath(sessionId + "Report.rtf"), WdSaveFormat.wdFormatRTF)
        '    doc.Close()
        '    wdApp.Quit()
        '    File.Delete(Server.MapPath(sessionId + "Report.html"))
        'End Try

        'Dim encode As New System.Text.UTF8Encoding
        'Dim str As StreamReader = File.OpenText(Server.MapPath(sessionId + "Report.rtf"))
        'Dim rtf As String = str.ReadToEnd
        'str.Close()
        'Dim buffer() As Byte = encode.GetBytes(rtf.ToCharArray)
        'File.Delete(Server.MapPath(sessionId + "Report.rtf"))

        Response.ContentType = "application/vnd.msword"
        Response.AddHeader("Content-Disposition", "attachment;filename=""Report.rtf""")
        'Response.TransmitFile(Server.MapPath(sessionId + "Report.rtf"))
        'Response.BinaryWrite(encoding.GetBytes(newBuffer))


    End Sub


    Private Sub Print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles C1MenuItem6.Click

    End Sub

    Public Function GetHTML(ByVal writer As HtmlTextWriter) As String
        Dim sw As New System.IO.StringWriter
        Dim localWriter As New HtmlTextWriter(sw)
        Dim html As String = sw.ToString()
        Return html
    End Function


    Private Sub DataDump_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles C1MenuItem8.Click
        If TypeOf c3 Is ReportControl Then
            CType(c3, ReportControl).SendDataDump()
        End If
    End Sub

End Class
