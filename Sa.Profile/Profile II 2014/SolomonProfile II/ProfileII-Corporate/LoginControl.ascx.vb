Partial Class LoginControl
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub


    Private Sub Submit1_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Submit1.ServerClick
        Dim auth As New Authentication
        Dim secData As SecurityData
        If auth.Authenticate(txtUsername.Value, txtPassword.Value) Then
            secData = auth.GetUserDataByLogin(txtUsername.Value)
            Session("AppUser") = secData.CompanyName
            Session("CompID") = secData.CompanyID
            Response.Redirect("Main.aspx")
        Else
            ltStatus.Text = "<font size=1px style=""COLOR:RED""> Login failed. Please try again. </font>"
        End If

    End Sub

    Private Sub txtUsername_ServerChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUsername.ServerChange

    End Sub
End Class
